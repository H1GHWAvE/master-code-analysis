.bytecode 50.0
.class public synchronized android/support/v4/view/ViewPager
.super android/view/ViewGroup

.field private static final 'W' I = -1


.field private static final 'af' I = 2


.field private static final 'at' I = 0


.field private static final 'au' I = 1


.field private static final 'av' I = 2


.field private static final 'ay' Landroid/support/v4/view/fa;

.field public static final 'b' I = 0


.field public static final 'c' I = 1


.field public static final 'd' I = 2


.field private static final 'e' Ljava/lang/String; = "ViewPager"

.field private static final 'f' Z = 0


.field private static final 'g' Z = 0


.field private static final 'h' I = 1


.field private static final 'i' I = 600


.field private static final 'j' I = 25


.field private static final 'k' I = 16


.field private static final 'l' I = 400


.field private static final 'm' [I

.field private static final 'o' Ljava/util/Comparator;

.field private static final 'p' Landroid/view/animation/Interpolator;

.field private 'A' I

.field private 'B' Landroid/graphics/drawable/Drawable;

.field private 'C' I

.field private 'D' I

.field private 'E' F

.field private 'F' F

.field private 'G' I

.field private 'H' I

.field private 'I' Z

.field private 'J' Z

.field private 'K' Z

.field private 'L' I

.field private 'M' Z

.field private 'N' Z

.field private 'O' I

.field private 'P' I

.field private 'Q' I

.field private 'R' F

.field private 'S' F

.field private 'T' F

.field private 'U' F

.field private 'V' I

.field public 'a' Ljava/util/List;

.field private 'aA' I

.field private 'aa' Landroid/view/VelocityTracker;

.field private 'ab' I

.field private 'ac' I

.field private 'ad' I

.field private 'ae' I

.field private 'ag' Z

.field private 'ah' J

.field private 'ai' Landroid/support/v4/widget/al;

.field private 'aj' Landroid/support/v4/widget/al;

.field private 'ak' Z

.field private 'al' Z

.field private 'am' Z

.field private 'an' I

.field private 'ao' Landroid/support/v4/view/ev;

.field private 'ap' Landroid/support/v4/view/ev;

.field private 'aq' Landroid/support/v4/view/eu;

.field private 'ar' Landroid/support/v4/view/ew;

.field private 'as' Ljava/lang/reflect/Method;

.field private 'aw' I

.field private 'ax' Ljava/util/ArrayList;

.field private final 'az' Ljava/lang/Runnable;

.field private 'n' I

.field private final 'q' Ljava/util/ArrayList;

.field private final 'r' Landroid/support/v4/view/er;

.field private final 's' Landroid/graphics/Rect;

.field private 't' Landroid/support/v4/view/by;

.field private 'u' I

.field private 'v' I

.field private 'w' Landroid/os/Parcelable;

.field private 'x' Ljava/lang/ClassLoader;

.field private 'y' Landroid/widget/Scroller;

.field private 'z' Landroid/support/v4/view/ex;

.method static <clinit>()V
iconst_1
newarray int
dup
iconst_0
ldc_w 16842931
iastore
putstatic android/support/v4/view/ViewPager/m [I
new android/support/v4/view/en
dup
invokespecial android/support/v4/view/en/<init>()V
putstatic android/support/v4/view/ViewPager/o Ljava/util/Comparator;
new android/support/v4/view/eo
dup
invokespecial android/support/v4/view/eo/<init>()V
putstatic android/support/v4/view/ViewPager/p Landroid/view/animation/Interpolator;
new android/support/v4/view/fa
dup
invokespecial android/support/v4/view/fa/<init>()V
putstatic android/support/v4/view/ViewPager/ay Landroid/support/v4/view/fa;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;)V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
aload 0
new android/support/v4/view/er
dup
invokespecial android/support/v4/view/er/<init>()V
putfield android/support/v4/view/ViewPager/r Landroid/support/v4/view/er;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/view/ViewPager/s Landroid/graphics/Rect;
aload 0
iconst_m1
putfield android/support/v4/view/ViewPager/v I
aload 0
aconst_null
putfield android/support/v4/view/ViewPager/w Landroid/os/Parcelable;
aload 0
aconst_null
putfield android/support/v4/view/ViewPager/x Ljava/lang/ClassLoader;
aload 0
ldc_w -3.4028235E38F
putfield android/support/v4/view/ViewPager/E F
aload 0
ldc_w 3.4028235E38F
putfield android/support/v4/view/ViewPager/F F
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/L I
aload 0
iconst_m1
putfield android/support/v4/view/ViewPager/V I
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/ak Z
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/al Z
aload 0
new android/support/v4/view/ep
dup
aload 0
invokespecial android/support/v4/view/ep/<init>(Landroid/support/v4/view/ViewPager;)V
putfield android/support/v4/view/ViewPager/az Ljava/lang/Runnable;
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/aA I
aload 0
invokespecial android/support/v4/view/ViewPager/d()V
return
.limit locals 2
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
aload 0
new android/support/v4/view/er
dup
invokespecial android/support/v4/view/er/<init>()V
putfield android/support/v4/view/ViewPager/r Landroid/support/v4/view/er;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/view/ViewPager/s Landroid/graphics/Rect;
aload 0
iconst_m1
putfield android/support/v4/view/ViewPager/v I
aload 0
aconst_null
putfield android/support/v4/view/ViewPager/w Landroid/os/Parcelable;
aload 0
aconst_null
putfield android/support/v4/view/ViewPager/x Ljava/lang/ClassLoader;
aload 0
ldc_w -3.4028235E38F
putfield android/support/v4/view/ViewPager/E F
aload 0
ldc_w 3.4028235E38F
putfield android/support/v4/view/ViewPager/F F
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/L I
aload 0
iconst_m1
putfield android/support/v4/view/ViewPager/V I
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/ak Z
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/al Z
aload 0
new android/support/v4/view/ep
dup
aload 0
invokespecial android/support/v4/view/ep/<init>(Landroid/support/v4/view/ViewPager;)V
putfield android/support/v4/view/ViewPager/az Ljava/lang/Runnable;
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/aA I
aload 0
invokespecial android/support/v4/view/ViewPager/d()V
return
.limit locals 3
.limit stack 4
.end method

.method private static a(F)F
fload 0
ldc_w 0.5F
fsub
f2d
ldc2_w 0.4712389167638204D
dmul
d2f
f2d
invokestatic java/lang/Math/sin(D)D
d2f
freturn
.limit locals 1
.limit stack 4
.end method

.method private a(IFII)I
iload 4
invokestatic java/lang/Math/abs(I)I
aload 0
getfield android/support/v4/view/ViewPager/ad I
if_icmple L0
iload 3
invokestatic java/lang/Math/abs(I)I
aload 0
getfield android/support/v4/view/ViewPager/ab I
if_icmple L0
iload 3
ifle L1
L2:
iload 1
istore 3
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L3
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 6
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 7
aload 6
getfield android/support/v4/view/er/b I
iload 1
aload 7
getfield android/support/v4/view/er/b I
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
istore 3
L3:
iload 3
ireturn
L1:
iload 1
iconst_1
iadd
istore 1
goto L2
L0:
iload 1
aload 0
getfield android/support/v4/view/ViewPager/u I
if_icmplt L4
ldc_w 0.4F
fstore 5
L5:
fload 5
iload 1
i2f
fload 2
fadd
fadd
f2i
istore 1
goto L2
L4:
ldc_w 0.6F
fstore 5
goto L5
.limit locals 8
.limit stack 3
.end method

.method private a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
aload 1
ifnonnull L0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
astore 1
L1:
aload 2
ifnonnull L2
aload 1
iconst_0
iconst_0
iconst_0
iconst_0
invokevirtual android/graphics/Rect/set(IIII)V
aload 1
areturn
L2:
aload 1
aload 2
invokevirtual android/view/View/getLeft()I
putfield android/graphics/Rect/left I
aload 1
aload 2
invokevirtual android/view/View/getRight()I
putfield android/graphics/Rect/right I
aload 1
aload 2
invokevirtual android/view/View/getTop()I
putfield android/graphics/Rect/top I
aload 1
aload 2
invokevirtual android/view/View/getBottom()I
putfield android/graphics/Rect/bottom I
aload 2
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 2
L3:
aload 2
instanceof android/view/ViewGroup
ifeq L4
aload 2
aload 0
if_acmpeq L4
aload 2
checkcast android/view/ViewGroup
astore 2
aload 1
aload 1
getfield android/graphics/Rect/left I
aload 2
invokevirtual android/view/ViewGroup/getLeft()I
iadd
putfield android/graphics/Rect/left I
aload 1
aload 1
getfield android/graphics/Rect/right I
aload 2
invokevirtual android/view/ViewGroup/getRight()I
iadd
putfield android/graphics/Rect/right I
aload 1
aload 1
getfield android/graphics/Rect/top I
aload 2
invokevirtual android/view/ViewGroup/getTop()I
iadd
putfield android/graphics/Rect/top I
aload 1
aload 1
getfield android/graphics/Rect/bottom I
aload 2
invokevirtual android/view/ViewGroup/getBottom()I
iadd
putfield android/graphics/Rect/bottom I
aload 2
invokevirtual android/view/ViewGroup/getParent()Landroid/view/ViewParent;
astore 2
goto L3
L4:
aload 1
areturn
L0:
goto L1
.limit locals 3
.limit stack 5
.end method

.method private a(Landroid/view/View;)Landroid/support/v4/view/er;
iconst_0
istore 2
L0:
iload 2
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L1
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 3
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
aload 1
aload 3
getfield android/support/v4/view/er/a Ljava/lang/Object;
invokevirtual android/support/v4/view/by/a(Landroid/view/View;Ljava/lang/Object;)Z
ifeq L2
aload 3
areturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aconst_null
areturn
.limit locals 4
.limit stack 3
.end method

.method private a(I)V
.catch android/content/res/Resources$NotFoundException from L0 to L1 using L2
aload 0
getfield android/support/v4/view/ViewPager/u I
iload 1
if_icmpeq L3
aload 0
getfield android/support/v4/view/ViewPager/u I
iload 1
if_icmpge L4
bipush 66
istore 5
L5:
aload 0
aload 0
getfield android/support/v4/view/ViewPager/u I
invokespecial android/support/v4/view/ViewPager/b(I)Landroid/support/v4/view/er;
astore 15
aload 0
iload 1
putfield android/support/v4/view/ViewPager/u I
iload 5
istore 6
L6:
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
ifnonnull L7
aload 0
invokespecial android/support/v4/view/ViewPager/g()V
L8:
return
L4:
bipush 17
istore 5
goto L5
L7:
aload 0
getfield android/support/v4/view/ViewPager/K Z
ifeq L9
aload 0
invokespecial android/support/v4/view/ViewPager/g()V
return
L9:
aload 0
invokevirtual android/support/v4/view/ViewPager/getWindowToken()Landroid/os/IBinder;
ifnull L8
aload 0
getfield android/support/v4/view/ViewPager/L I
istore 1
iconst_0
aload 0
getfield android/support/v4/view/ViewPager/u I
iload 1
isub
invokestatic java/lang/Math/max(II)I
istore 12
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
istore 10
iload 10
iconst_1
isub
iload 1
aload 0
getfield android/support/v4/view/ViewPager/u I
iadd
invokestatic java/lang/Math/min(II)I
istore 11
iload 10
aload 0
getfield android/support/v4/view/ViewPager/n I
if_icmpeq L10
L0:
aload 0
invokevirtual android/support/v4/view/ViewPager/getResources()Landroid/content/res/Resources;
aload 0
invokevirtual android/support/v4/view/ViewPager/getId()I
invokevirtual android/content/res/Resources/getResourceName(I)Ljava/lang/String;
astore 14
L1:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/view/ViewPager/n I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", found: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 10
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " Pager id: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 14
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " Pager class: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " Problematic adapter: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 14
aload 0
invokevirtual android/support/v4/view/ViewPager/getId()I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
astore 14
goto L1
L10:
iconst_0
istore 1
L11:
iload 1
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L12
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 14
aload 14
getfield android/support/v4/view/er/b I
aload 0
getfield android/support/v4/view/ViewPager/u I
if_icmplt L13
aload 14
getfield android/support/v4/view/er/b I
aload 0
getfield android/support/v4/view/ViewPager/u I
if_icmpne L12
L14:
aload 14
ifnonnull L15
iload 10
ifle L15
aload 0
aload 0
getfield android/support/v4/view/ViewPager/u I
iload 1
invokespecial android/support/v4/view/ViewPager/b(II)Landroid/support/v4/view/er;
astore 16
L16:
aload 16
ifnull L17
iload 1
iconst_1
isub
istore 9
iload 9
iflt L18
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 9
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 14
L19:
aload 0
invokespecial android/support/v4/view/ViewPager/getClientWidth()I
istore 13
iload 13
ifgt L20
fconst_0
fstore 3
L21:
aload 0
getfield android/support/v4/view/ViewPager/u I
istore 5
fconst_0
fstore 4
iload 5
iconst_1
isub
istore 8
iload 1
istore 7
aload 14
astore 17
L22:
iload 8
iflt L23
fload 4
fload 3
fcmpl
iflt L24
iload 8
iload 12
if_icmpge L24
aload 17
ifnull L23
aload 17
astore 14
iload 9
istore 1
fload 4
fstore 2
iload 7
istore 5
iload 8
aload 17
getfield android/support/v4/view/er/b I
if_icmpne L25
aload 17
astore 14
iload 9
istore 1
fload 4
fstore 2
iload 7
istore 5
aload 17
getfield android/support/v4/view/er/c Z
ifne L25
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 9
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
pop
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
iload 8
aload 17
getfield android/support/v4/view/er/a Ljava/lang/Object;
invokevirtual android/support/v4/view/by/a(ILjava/lang/Object;)V
iload 9
iconst_1
isub
istore 1
iload 7
iconst_1
isub
istore 5
iload 1
iflt L26
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 14
fload 4
fstore 2
L25:
iload 8
iconst_1
isub
istore 8
aload 14
astore 17
iload 1
istore 9
fload 2
fstore 4
iload 5
istore 7
goto L22
L13:
iload 1
iconst_1
iadd
istore 1
goto L11
L18:
aconst_null
astore 14
goto L19
L20:
fconst_2
aload 16
getfield android/support/v4/view/er/d F
fsub
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingLeft()I
i2f
iload 13
i2f
fdiv
fadd
fstore 3
goto L21
L26:
aconst_null
astore 14
fload 4
fstore 2
goto L25
L24:
aload 17
ifnull L27
iload 8
aload 17
getfield android/support/v4/view/er/b I
if_icmpne L27
fload 4
aload 17
getfield android/support/v4/view/er/d F
fadd
fstore 2
iload 9
iconst_1
isub
istore 1
iload 1
iflt L28
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 14
iload 7
istore 5
goto L25
L28:
aconst_null
astore 14
iload 7
istore 5
goto L25
L27:
fload 4
aload 0
iload 8
iload 9
iconst_1
iadd
invokespecial android/support/v4/view/ViewPager/b(II)Landroid/support/v4/view/er;
getfield android/support/v4/view/er/d F
fadd
fstore 2
iload 7
iconst_1
iadd
istore 5
iload 9
iflt L29
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 9
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 14
iload 9
istore 1
goto L25
L29:
aconst_null
astore 14
iload 9
istore 1
goto L25
L23:
aload 16
getfield android/support/v4/view/er/d F
fstore 2
iload 7
iconst_1
iadd
istore 1
fload 2
fconst_2
fcmpg
ifge L30
iload 1
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L31
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 14
L32:
iload 13
ifgt L33
fconst_0
fstore 3
L34:
aload 0
getfield android/support/v4/view/ViewPager/u I
istore 5
iload 5
iconst_1
iadd
istore 5
L35:
iload 5
iload 10
if_icmpge L30
fload 2
fload 3
fcmpl
iflt L36
iload 5
iload 11
if_icmple L36
aload 14
ifnull L30
iload 5
aload 14
getfield android/support/v4/view/er/b I
if_icmpne L37
aload 14
getfield android/support/v4/view/er/c Z
ifne L37
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
pop
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
iload 5
aload 14
getfield android/support/v4/view/er/a Ljava/lang/Object;
invokevirtual android/support/v4/view/by/a(ILjava/lang/Object;)V
iload 1
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L38
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 14
L39:
iload 5
iconst_1
iadd
istore 5
goto L35
L31:
aconst_null
astore 14
goto L32
L33:
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingRight()I
i2f
iload 13
i2f
fdiv
fconst_2
fadd
fstore 3
goto L34
L38:
aconst_null
astore 14
goto L39
L36:
aload 14
ifnull L40
iload 5
aload 14
getfield android/support/v4/view/er/b I
if_icmpne L40
aload 14
getfield android/support/v4/view/er/d F
fstore 4
iload 1
iconst_1
iadd
istore 1
iload 1
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L41
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 14
L42:
fload 2
fload 4
fadd
fstore 2
goto L39
L41:
aconst_null
astore 14
goto L42
L40:
aload 0
iload 5
iload 1
invokespecial android/support/v4/view/ViewPager/b(II)Landroid/support/v4/view/er;
astore 14
iload 1
iconst_1
iadd
istore 1
aload 14
getfield android/support/v4/view/er/d F
fstore 4
iload 1
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L43
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 14
L44:
fload 2
fload 4
fadd
fstore 2
goto L39
L43:
aconst_null
astore 14
goto L44
L30:
aload 0
aload 16
iload 7
aload 15
invokespecial android/support/v4/view/ViewPager/a(Landroid/support/v4/view/er;ILandroid/support/v4/view/er;)V
L17:
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
astore 15
aload 16
ifnull L45
aload 16
getfield android/support/v4/view/er/a Ljava/lang/Object;
astore 14
L46:
aload 15
aload 14
invokevirtual android/support/v4/view/by/a(Ljava/lang/Object;)V
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/c()V
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
istore 5
iconst_0
istore 1
L47:
iload 1
iload 5
if_icmpge L48
aload 0
iload 1
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 15
aload 15
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/view/es
astore 14
aload 14
iload 1
putfield android/support/v4/view/es/f I
aload 14
getfield android/support/v4/view/es/a Z
ifne L49
aload 14
getfield android/support/v4/view/es/c F
fconst_0
fcmpl
ifne L49
aload 0
aload 15
invokespecial android/support/v4/view/ViewPager/a(Landroid/view/View;)Landroid/support/v4/view/er;
astore 15
aload 15
ifnull L49
aload 14
aload 15
getfield android/support/v4/view/er/d F
putfield android/support/v4/view/es/c F
aload 14
aload 15
getfield android/support/v4/view/er/b I
putfield android/support/v4/view/es/e I
L49:
iload 1
iconst_1
iadd
istore 1
goto L47
L45:
aconst_null
astore 14
goto L46
L48:
aload 0
invokespecial android/support/v4/view/ViewPager/g()V
aload 0
invokevirtual android/support/v4/view/ViewPager/hasFocus()Z
ifeq L8
aload 0
invokevirtual android/support/v4/view/ViewPager/findFocus()Landroid/view/View;
astore 14
aload 14
ifnull L50
aload 0
aload 14
invokespecial android/support/v4/view/ViewPager/b(Landroid/view/View;)Landroid/support/v4/view/er;
astore 14
L51:
aload 14
ifnull L52
aload 14
getfield android/support/v4/view/er/b I
aload 0
getfield android/support/v4/view/ViewPager/u I
if_icmpeq L8
L52:
iconst_0
istore 1
L53:
iload 1
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
if_icmpge L8
aload 0
iload 1
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 14
aload 0
aload 14
invokespecial android/support/v4/view/ViewPager/a(Landroid/view/View;)Landroid/support/v4/view/er;
astore 15
aload 15
ifnull L54
aload 15
getfield android/support/v4/view/er/b I
aload 0
getfield android/support/v4/view/ViewPager/u I
if_icmpne L54
aload 14
iload 6
invokevirtual android/view/View/requestFocus(I)Z
ifne L8
L54:
iload 1
iconst_1
iadd
istore 1
goto L53
L50:
aconst_null
astore 14
goto L51
L37:
goto L39
L15:
aload 14
astore 16
goto L16
L12:
aconst_null
astore 14
goto L14
L3:
aconst_null
astore 15
iconst_2
istore 6
goto L6
.limit locals 18
.limit stack 5
.end method

.method private a(IF)V
.annotation invisible Landroid/support/a/h;
.end annotation
aload 0
getfield android/support/v4/view/ViewPager/an I
ifle L0
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
istore 8
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingLeft()I
istore 3
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingRight()I
istore 4
aload 0
invokevirtual android/support/v4/view/ViewPager/getWidth()I
istore 9
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
istore 10
iconst_0
istore 7
L1:
iload 7
iload 10
if_icmpge L0
aload 0
iload 7
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 12
aload 12
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/view/es
astore 13
aload 13
getfield android/support/v4/view/es/a Z
ifeq L2
aload 13
getfield android/support/v4/view/es/b I
bipush 7
iand
tableswitch 1
L3
L4
L5
L4
L6
default : L4
L4:
iload 3
istore 5
iload 4
istore 6
iload 3
istore 4
iload 6
istore 3
L7:
iload 5
iload 8
iadd
aload 12
invokevirtual android/view/View/getLeft()I
isub
istore 11
iload 3
istore 5
iload 4
istore 6
iload 11
ifeq L8
aload 12
iload 11
invokevirtual android/view/View/offsetLeftAndRight(I)V
iload 4
istore 6
iload 3
istore 5
L8:
iload 7
iconst_1
iadd
istore 7
iload 6
istore 3
iload 5
istore 4
goto L1
L5:
aload 12
invokevirtual android/view/View/getWidth()I
istore 5
iload 5
iload 3
iadd
istore 6
iload 3
istore 5
iload 4
istore 3
iload 6
istore 4
goto L7
L3:
iload 9
aload 12
invokevirtual android/view/View/getMeasuredWidth()I
isub
iconst_2
idiv
iload 3
invokestatic java/lang/Math/max(II)I
istore 5
iload 3
istore 6
iload 4
istore 3
iload 6
istore 4
goto L7
L6:
iload 9
iload 4
isub
aload 12
invokevirtual android/view/View/getMeasuredWidth()I
isub
istore 5
aload 12
invokevirtual android/view/View/getMeasuredWidth()I
istore 11
iload 3
istore 6
iload 4
iload 11
iadd
istore 3
iload 6
istore 4
goto L7
L0:
aload 0
getfield android/support/v4/view/ViewPager/ao Landroid/support/v4/view/ev;
ifnull L9
aload 0
getfield android/support/v4/view/ViewPager/ao Landroid/support/v4/view/ev;
iload 1
fload 2
invokeinterface android/support/v4/view/ev/a(IF)V 2
L9:
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
ifnull L10
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 4
iconst_0
istore 3
L11:
iload 3
iload 4
if_icmpge L10
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/view/ev
astore 12
aload 12
ifnull L12
aload 12
iload 1
fload 2
invokeinterface android/support/v4/view/ev/a(IF)V 2
L12:
iload 3
iconst_1
iadd
istore 3
goto L11
L10:
aload 0
getfield android/support/v4/view/ViewPager/ap Landroid/support/v4/view/ev;
ifnull L13
aload 0
getfield android/support/v4/view/ViewPager/ap Landroid/support/v4/view/ev;
iload 1
fload 2
invokeinterface android/support/v4/view/ev/a(IF)V 2
L13:
aload 0
getfield android/support/v4/view/ViewPager/ar Landroid/support/v4/view/ew;
ifnull L14
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
pop
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
istore 3
iconst_0
istore 1
L15:
iload 1
iload 3
if_icmpge L14
aload 0
iload 1
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 12
aload 12
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/view/es
getfield android/support/v4/view/es/a Z
ifne L16
aload 12
invokevirtual android/view/View/getLeft()I
pop
aload 0
invokespecial android/support/v4/view/ViewPager/getClientWidth()I
pop
L16:
iload 1
iconst_1
iadd
istore 1
goto L15
L14:
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/am Z
return
L2:
iload 4
istore 5
iload 3
istore 6
goto L8
.limit locals 14
.limit stack 3
.end method

.method private a(II)V
aload 0
iload 1
iload 2
iconst_0
invokespecial android/support/v4/view/ViewPager/a(III)V
return
.limit locals 3
.limit stack 4
.end method

.method private a(III)V
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
ifne L0
aload 0
iconst_0
invokespecial android/support/v4/view/ViewPager/setScrollingCacheEnabled(Z)V
return
L0:
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
istore 7
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollY()I
istore 8
iload 1
iload 7
isub
istore 9
iload 2
iload 8
isub
istore 2
iload 9
ifne L1
iload 2
ifne L1
aload 0
iconst_0
invokespecial android/support/v4/view/ViewPager/a(Z)V
aload 0
invokevirtual android/support/v4/view/ViewPager/b()V
aload 0
iconst_0
invokespecial android/support/v4/view/ViewPager/setScrollState(I)V
return
L1:
aload 0
iconst_1
invokespecial android/support/v4/view/ViewPager/setScrollingCacheEnabled(Z)V
aload 0
iconst_2
invokespecial android/support/v4/view/ViewPager/setScrollState(I)V
aload 0
invokespecial android/support/v4/view/ViewPager/getClientWidth()I
istore 1
iload 1
iconst_2
idiv
istore 10
fconst_1
fconst_1
iload 9
invokestatic java/lang/Math/abs(I)I
i2f
fmul
iload 1
i2f
fdiv
invokestatic java/lang/Math/min(FF)F
fstore 6
iload 10
i2f
fstore 4
iload 10
i2f
fstore 5
fload 6
ldc_w 0.5F
fsub
f2d
ldc2_w 0.4712389167638204D
dmul
d2f
f2d
invokestatic java/lang/Math/sin(D)D
d2f
fstore 6
iload 3
invokestatic java/lang/Math/abs(I)I
istore 3
iload 3
ifle L2
ldc_w 1000.0F
fload 5
fload 6
fmul
fload 4
fadd
iload 3
i2f
fdiv
invokestatic java/lang/Math/abs(F)F
fmul
invokestatic java/lang/Math/round(F)I
iconst_4
imul
istore 1
L3:
iload 1
sipush 600
invokestatic java/lang/Math/min(II)I
istore 1
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
iload 7
iload 8
iload 9
iload 2
iload 1
invokevirtual android/widget/Scroller/startScroll(IIIII)V
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
return
L2:
iload 1
i2f
fstore 4
iload 9
invokestatic java/lang/Math/abs(I)I
i2f
fload 4
fconst_1
fmul
aload 0
getfield android/support/v4/view/ViewPager/A I
i2f
fadd
fdiv
fconst_1
fadd
ldc_w 100.0F
fmul
f2i
istore 1
goto L3
.limit locals 11
.limit stack 6
.end method

.method private a(IIII)V
iload 2
ifle L0
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
ifne L0
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingLeft()I
istore 6
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingRight()I
istore 7
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingLeft()I
istore 8
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingRight()I
istore 9
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
i2f
iload 2
iload 8
isub
iload 9
isub
iload 4
iadd
i2f
fdiv
fstore 5
iload 1
iload 6
isub
iload 7
isub
iload 3
iadd
i2f
fload 5
fmul
f2i
istore 2
aload 0
iload 2
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollY()I
invokevirtual android/support/v4/view/ViewPager/scrollTo(II)V
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/isFinished()Z
ifne L1
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/getDuration()I
istore 3
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/timePassed()I
istore 4
aload 0
aload 0
getfield android/support/v4/view/ViewPager/u I
invokespecial android/support/v4/view/ViewPager/b(I)Landroid/support/v4/view/er;
astore 10
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
iload 2
iconst_0
aload 10
getfield android/support/v4/view/er/e F
iload 1
i2f
fmul
f2i
iconst_0
iload 3
iload 4
isub
invokevirtual android/widget/Scroller/startScroll(IIIII)V
L1:
return
L0:
aload 0
aload 0
getfield android/support/v4/view/ViewPager/u I
invokespecial android/support/v4/view/ViewPager/b(I)Landroid/support/v4/view/er;
astore 10
aload 10
ifnull L2
aload 10
getfield android/support/v4/view/er/e F
aload 0
getfield android/support/v4/view/ViewPager/F F
invokestatic java/lang/Math/min(FF)F
fstore 5
L3:
fload 5
iload 1
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingLeft()I
isub
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingRight()I
isub
i2f
fmul
f2i
istore 1
iload 1
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
if_icmpeq L1
aload 0
iconst_0
invokespecial android/support/v4/view/ViewPager/a(Z)V
aload 0
iload 1
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollY()I
invokevirtual android/support/v4/view/ViewPager/scrollTo(II)V
return
L2:
fconst_0
fstore 5
goto L3
.limit locals 11
.limit stack 7
.end method

.method private a(IZIZ)V
aload 0
iload 1
invokespecial android/support/v4/view/ViewPager/b(I)Landroid/support/v4/view/er;
astore 7
aload 7
ifnull L0
aload 0
invokespecial android/support/v4/view/ViewPager/getClientWidth()I
i2f
fstore 5
aload 0
getfield android/support/v4/view/ViewPager/E F
aload 7
getfield android/support/v4/view/er/e F
aload 0
getfield android/support/v4/view/ViewPager/F F
invokestatic java/lang/Math/min(FF)F
invokestatic java/lang/Math/max(FF)F
fload 5
fmul
f2i
istore 6
L1:
iload 2
ifeq L2
aload 0
iload 6
iconst_0
iload 3
invokespecial android/support/v4/view/ViewPager/a(III)V
iload 4
ifeq L3
aload 0
iload 1
invokespecial android/support/v4/view/ViewPager/d(I)V
L3:
return
L2:
iload 4
ifeq L4
aload 0
iload 1
invokespecial android/support/v4/view/ViewPager/d(I)V
L4:
aload 0
iconst_0
invokespecial android/support/v4/view/ViewPager/a(Z)V
aload 0
iload 6
iconst_0
invokevirtual android/support/v4/view/ViewPager/scrollTo(II)V
aload 0
iload 6
invokespecial android/support/v4/view/ViewPager/c(I)Z
pop
return
L0:
iconst_0
istore 6
goto L1
.limit locals 8
.limit stack 4
.end method

.method private a(IZZ)V
aload 0
iload 1
iload 2
iload 3
iconst_0
invokespecial android/support/v4/view/ViewPager/a(IZZI)V
return
.limit locals 4
.limit stack 5
.end method

.method private a(IZZI)V
iconst_0
istore 6
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
ifnull L0
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
ifgt L1
L0:
aload 0
iconst_0
invokespecial android/support/v4/view/ViewPager/setScrollingCacheEnabled(Z)V
return
L1:
iload 3
ifne L2
aload 0
getfield android/support/v4/view/ViewPager/u I
iload 1
if_icmpne L2
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifeq L2
aload 0
iconst_0
invokespecial android/support/v4/view/ViewPager/setScrollingCacheEnabled(Z)V
return
L2:
iload 1
ifge L3
iconst_0
istore 5
L4:
aload 0
getfield android/support/v4/view/ViewPager/L I
istore 1
iload 5
aload 0
getfield android/support/v4/view/ViewPager/u I
iload 1
iadd
if_icmpgt L5
iload 5
aload 0
getfield android/support/v4/view/ViewPager/u I
iload 1
isub
if_icmpge L6
L5:
iconst_0
istore 1
L7:
iload 1
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L6
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
iconst_1
putfield android/support/v4/view/er/c Z
iload 1
iconst_1
iadd
istore 1
goto L7
L3:
iload 1
istore 5
iload 1
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
if_icmplt L4
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
iconst_1
isub
istore 5
goto L4
L6:
iload 6
istore 3
aload 0
getfield android/support/v4/view/ViewPager/u I
iload 5
if_icmpeq L8
iconst_1
istore 3
L8:
aload 0
getfield android/support/v4/view/ViewPager/ak Z
ifeq L9
aload 0
iload 5
putfield android/support/v4/view/ViewPager/u I
iload 3
ifeq L10
aload 0
iload 5
invokespecial android/support/v4/view/ViewPager/d(I)V
L10:
aload 0
invokevirtual android/support/v4/view/ViewPager/requestLayout()V
return
L9:
aload 0
iload 5
invokespecial android/support/v4/view/ViewPager/a(I)V
aload 0
iload 5
iload 2
iload 4
iload 3
invokespecial android/support/v4/view/ViewPager/a(IZIZ)V
return
.limit locals 7
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v4/view/ViewPager;)V
aload 0
iconst_0
invokespecial android/support/v4/view/ViewPager/setScrollState(I)V
return
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/support/v4/view/er;ILandroid/support/v4/view/er;)V
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
istore 9
aload 0
invokespecial android/support/v4/view/ViewPager/getClientWidth()I
istore 7
iload 7
ifle L0
aload 0
getfield android/support/v4/view/ViewPager/A I
i2f
iload 7
i2f
fdiv
fstore 5
L1:
aload 3
ifnull L2
aload 3
getfield android/support/v4/view/er/b I
istore 7
iload 7
aload 1
getfield android/support/v4/view/er/b I
if_icmpge L3
aload 3
getfield android/support/v4/view/er/e F
aload 3
getfield android/support/v4/view/er/d F
fadd
fload 5
fadd
fstore 4
iconst_0
istore 8
iload 7
iconst_1
iadd
istore 7
L4:
iload 7
aload 1
getfield android/support/v4/view/er/b I
if_icmpgt L2
iload 8
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L2
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 3
L5:
iload 7
aload 3
getfield android/support/v4/view/er/b I
if_icmple L6
iload 8
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
if_icmpge L6
iload 8
iconst_1
iadd
istore 8
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 3
goto L5
L0:
fconst_0
fstore 5
goto L1
L7:
iload 7
aload 3
getfield android/support/v4/view/er/b I
if_icmpge L8
iload 7
iconst_1
iadd
istore 7
fconst_1
fload 5
fadd
fload 4
fadd
fstore 4
goto L7
L8:
aload 3
fload 4
putfield android/support/v4/view/er/e F
fload 4
aload 3
getfield android/support/v4/view/er/d F
fload 5
fadd
fadd
fstore 4
iload 7
iconst_1
iadd
istore 7
goto L4
L3:
iload 7
aload 1
getfield android/support/v4/view/er/b I
if_icmple L2
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 8
aload 3
getfield android/support/v4/view/er/e F
fstore 4
iload 8
iconst_1
isub
istore 8
iload 7
iconst_1
isub
istore 7
L9:
iload 7
aload 1
getfield android/support/v4/view/er/b I
if_icmplt L2
iload 8
iflt L2
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 3
L10:
iload 7
aload 3
getfield android/support/v4/view/er/b I
if_icmpge L11
iload 8
ifle L11
iload 8
iconst_1
isub
istore 8
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 3
goto L10
L12:
iload 7
aload 3
getfield android/support/v4/view/er/b I
if_icmple L13
iload 7
iconst_1
isub
istore 7
fload 4
fconst_1
fload 5
fadd
fsub
fstore 4
goto L12
L13:
fload 4
aload 3
getfield android/support/v4/view/er/d F
fload 5
fadd
fsub
fstore 4
aload 3
fload 4
putfield android/support/v4/view/er/e F
iload 7
iconst_1
isub
istore 7
goto L9
L2:
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 10
aload 1
getfield android/support/v4/view/er/e F
fstore 6
aload 1
getfield android/support/v4/view/er/b I
iconst_1
isub
istore 7
aload 1
getfield android/support/v4/view/er/b I
ifne L14
aload 1
getfield android/support/v4/view/er/e F
fstore 4
L15:
aload 0
fload 4
putfield android/support/v4/view/ViewPager/E F
aload 1
getfield android/support/v4/view/er/b I
iload 9
iconst_1
isub
if_icmpne L16
aload 1
getfield android/support/v4/view/er/e F
aload 1
getfield android/support/v4/view/er/d F
fadd
fconst_1
fsub
fstore 4
L17:
aload 0
fload 4
putfield android/support/v4/view/ViewPager/F F
iload 2
iconst_1
isub
istore 8
fload 6
fstore 4
L18:
iload 8
iflt L19
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 3
L20:
iload 7
aload 3
getfield android/support/v4/view/er/b I
if_icmple L21
iload 7
iconst_1
isub
istore 7
fload 4
fconst_1
fload 5
fadd
fsub
fstore 4
goto L20
L14:
ldc_w -3.4028235E38F
fstore 4
goto L15
L16:
ldc_w 3.4028235E38F
fstore 4
goto L17
L21:
fload 4
aload 3
getfield android/support/v4/view/er/d F
fload 5
fadd
fsub
fstore 4
aload 3
fload 4
putfield android/support/v4/view/er/e F
aload 3
getfield android/support/v4/view/er/b I
ifne L22
aload 0
fload 4
putfield android/support/v4/view/ViewPager/E F
L22:
iload 7
iconst_1
isub
istore 7
iload 8
iconst_1
isub
istore 8
goto L18
L19:
aload 1
getfield android/support/v4/view/er/e F
aload 1
getfield android/support/v4/view/er/d F
fadd
fload 5
fadd
fstore 4
aload 1
getfield android/support/v4/view/er/b I
iconst_1
iadd
istore 8
iload 2
iconst_1
iadd
istore 7
iload 8
istore 2
L23:
iload 7
iload 10
if_icmpge L24
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 1
L25:
iload 2
aload 1
getfield android/support/v4/view/er/b I
if_icmpge L26
iload 2
iconst_1
iadd
istore 2
fload 4
fconst_1
fload 5
fadd
fadd
fstore 4
goto L25
L26:
aload 1
getfield android/support/v4/view/er/b I
iload 9
iconst_1
isub
if_icmpne L27
aload 0
aload 1
getfield android/support/v4/view/er/d F
fload 4
fadd
fconst_1
fsub
putfield android/support/v4/view/ViewPager/F F
L27:
aload 1
fload 4
putfield android/support/v4/view/er/e F
fload 4
aload 1
getfield android/support/v4/view/er/d F
fload 5
fadd
fadd
fstore 4
iload 2
iconst_1
iadd
istore 2
iload 7
iconst_1
iadd
istore 7
goto L23
L24:
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/al Z
return
L11:
goto L12
L6:
goto L7
.limit locals 11
.limit stack 3
.end method

.method private a(Landroid/view/MotionEvent;)V
aload 1
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;)I
istore 2
aload 1
iload 2
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
aload 0
getfield android/support/v4/view/ViewPager/V I
if_icmpne L0
iload 2
ifne L1
iconst_1
istore 2
L2:
aload 0
aload 1
iload 2
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
putfield android/support/v4/view/ViewPager/R F
aload 0
aload 1
iload 2
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/v4/view/ViewPager/V I
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
ifnull L0
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/clear()V
L0:
return
L1:
iconst_0
istore 2
goto L2
.limit locals 3
.limit stack 3
.end method

.method private a(Z)V
aload 0
getfield android/support/v4/view/ViewPager/aA I
iconst_2
if_icmpne L0
iconst_1
istore 2
L1:
iload 2
ifeq L2
aload 0
iconst_0
invokespecial android/support/v4/view/ViewPager/setScrollingCacheEnabled(Z)V
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/abortAnimation()V
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
istore 3
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollY()I
istore 4
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/getCurrX()I
istore 5
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/getCurrY()I
istore 6
iload 3
iload 5
if_icmpne L3
iload 4
iload 6
if_icmpeq L2
L3:
aload 0
iload 5
iload 6
invokevirtual android/support/v4/view/ViewPager/scrollTo(II)V
iload 5
iload 3
if_icmpeq L2
aload 0
iload 5
invokespecial android/support/v4/view/ViewPager/c(I)Z
pop
L2:
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/K Z
iconst_0
istore 4
iload 2
istore 3
iload 4
istore 2
L4:
iload 2
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L5
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 7
aload 7
getfield android/support/v4/view/er/c Z
ifeq L6
aload 7
iconst_0
putfield android/support/v4/view/er/c Z
iconst_1
istore 3
L6:
iload 2
iconst_1
iadd
istore 2
goto L4
L0:
iconst_0
istore 2
goto L1
L5:
iload 3
ifeq L7
iload 1
ifeq L8
aload 0
aload 0
getfield android/support/v4/view/ViewPager/az Ljava/lang/Runnable;
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Ljava/lang/Runnable;)V
L7:
return
L8:
aload 0
getfield android/support/v4/view/ViewPager/az Ljava/lang/Runnable;
invokeinterface java/lang/Runnable/run()V 0
return
.limit locals 8
.limit stack 3
.end method

.method private a(ZLandroid/support/v4/view/ew;)V
iconst_1
istore 4
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
aload 2
ifnull L1
iconst_1
istore 5
L2:
aload 0
getfield android/support/v4/view/ViewPager/ar Landroid/support/v4/view/ew;
ifnull L3
iconst_1
istore 6
L4:
iload 5
iload 6
if_icmpeq L5
iconst_1
istore 3
L6:
aload 0
aload 2
putfield android/support/v4/view/ViewPager/ar Landroid/support/v4/view/ew;
aload 0
iload 5
invokevirtual android/support/v4/view/ViewPager/setChildrenDrawingOrderEnabledCompat(Z)V
iload 5
ifeq L7
iload 1
ifeq L8
iconst_2
istore 4
L8:
aload 0
iload 4
putfield android/support/v4/view/ViewPager/aw I
L9:
iload 3
ifeq L0
aload 0
invokevirtual android/support/v4/view/ViewPager/b()V
L0:
return
L1:
iconst_0
istore 5
goto L2
L3:
iconst_0
istore 6
goto L4
L5:
iconst_0
istore 3
goto L6
L7:
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/aw I
goto L9
.limit locals 7
.limit stack 2
.end method

.method private a(FF)Z
fload 1
aload 0
getfield android/support/v4/view/ViewPager/P I
i2f
fcmpg
ifge L0
fload 2
fconst_0
fcmpl
ifgt L1
L0:
fload 1
aload 0
invokevirtual android/support/v4/view/ViewPager/getWidth()I
aload 0
getfield android/support/v4/view/ViewPager/P I
isub
i2f
fcmpl
ifle L2
fload 2
fconst_0
fcmpg
ifge L2
L1:
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/view/KeyEvent;)Z
aload 1
invokevirtual android/view/KeyEvent/getAction()I
ifne L0
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
lookupswitch
21 : L1
22 : L2
61 : L3
default : L0
L0:
iconst_0
ireturn
L1:
aload 0
bipush 17
invokespecial android/support/v4/view/ViewPager/f(I)Z
ireturn
L2:
aload 0
bipush 66
invokespecial android/support/v4/view/ViewPager/f(I)Z
ireturn
L3:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
aload 1
invokestatic android/support/v4/view/ab/b(Landroid/view/KeyEvent;)Z
ifeq L4
aload 0
iconst_2
invokespecial android/support/v4/view/ViewPager/f(I)Z
ireturn
L4:
aload 1
invokestatic android/support/v4/view/ab/a(Landroid/view/KeyEvent;)Z
ifeq L0
aload 0
iconst_1
invokespecial android/support/v4/view/ViewPager/f(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/View;ZIII)Z
aload 1
instanceof android/view/ViewGroup
ifeq L0
aload 1
checkcast android/view/ViewGroup
astore 9
aload 1
invokevirtual android/view/View/getScrollX()I
istore 7
aload 1
invokevirtual android/view/View/getScrollY()I
istore 8
aload 9
invokevirtual android/view/ViewGroup/getChildCount()I
iconst_1
isub
istore 6
L1:
iload 6
iflt L0
aload 9
iload 6
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
astore 10
iload 4
iload 7
iadd
aload 10
invokevirtual android/view/View/getLeft()I
if_icmplt L2
iload 4
iload 7
iadd
aload 10
invokevirtual android/view/View/getRight()I
if_icmpge L2
iload 5
iload 8
iadd
aload 10
invokevirtual android/view/View/getTop()I
if_icmplt L2
iload 5
iload 8
iadd
aload 10
invokevirtual android/view/View/getBottom()I
if_icmpge L2
aload 0
aload 10
iconst_1
iload 3
iload 4
iload 7
iadd
aload 10
invokevirtual android/view/View/getLeft()I
isub
iload 5
iload 8
iadd
aload 10
invokevirtual android/view/View/getTop()I
isub
invokespecial android/support/v4/view/ViewPager/a(Landroid/view/View;ZIII)Z
ifeq L2
L3:
iconst_1
ireturn
L2:
iload 6
iconst_1
isub
istore 6
goto L1
L0:
iload 2
ifeq L4
aload 1
iload 3
ineg
invokestatic android/support/v4/view/cx/a(Landroid/view/View;I)Z
ifne L3
L4:
iconst_0
ireturn
.limit locals 11
.limit stack 7
.end method

.method static synthetic b(Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/by;
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)Landroid/support/v4/view/er;
iconst_0
istore 2
L0:
iload 2
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L1
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 3
aload 3
getfield android/support/v4/view/er/b I
iload 1
if_icmpne L2
aload 3
areturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aconst_null
areturn
.limit locals 4
.limit stack 2
.end method

.method private b(II)Landroid/support/v4/view/er;
new android/support/v4/view/er
dup
invokespecial android/support/v4/view/er/<init>()V
astore 3
aload 3
iload 1
putfield android/support/v4/view/er/b I
aload 3
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
aload 0
iload 1
invokevirtual android/support/v4/view/by/a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
putfield android/support/v4/view/er/a Ljava/lang/Object;
aload 3
fconst_1
putfield android/support/v4/view/er/d F
iload 2
iflt L0
iload 2
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmplt L1
L0:
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 3
areturn
L1:
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 2
aload 3
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
aload 3
areturn
.limit locals 4
.limit stack 4
.end method

.method private b(Landroid/view/View;)Landroid/support/v4/view/er;
L0:
aload 1
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 2
aload 2
aload 0
if_acmpeq L1
aload 2
ifnull L2
aload 2
instanceof android/view/View
ifne L3
L2:
aconst_null
areturn
L3:
aload 2
checkcast android/view/View
astore 1
goto L0
L1:
aload 0
aload 1
invokespecial android/support/v4/view/ViewPager/a(Landroid/view/View;)Landroid/support/v4/view/er;
areturn
.limit locals 3
.limit stack 2
.end method

.method private b(IF)V
aload 0
getfield android/support/v4/view/ViewPager/ao Landroid/support/v4/view/ev;
ifnull L0
aload 0
getfield android/support/v4/view/ViewPager/ao Landroid/support/v4/view/ev;
iload 1
fload 2
invokeinterface android/support/v4/view/ev/a(IF)V 2
L0:
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
ifnull L1
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 4
iconst_0
istore 3
L2:
iload 3
iload 4
if_icmpge L1
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/view/ev
astore 5
aload 5
ifnull L3
aload 5
iload 1
fload 2
invokeinterface android/support/v4/view/ev/a(IF)V 2
L3:
iload 3
iconst_1
iadd
istore 3
goto L2
L1:
aload 0
getfield android/support/v4/view/ViewPager/ap Landroid/support/v4/view/ev;
ifnull L4
aload 0
getfield android/support/v4/view/ViewPager/ap Landroid/support/v4/view/ev;
iload 1
fload 2
invokeinterface android/support/v4/view/ev/a(IF)V 2
L4:
return
.limit locals 6
.limit stack 3
.end method

.method private b(Landroid/support/v4/view/ev;)V
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
ifnonnull L0
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/view/ViewPager/a Ljava/util/List;
L0:
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 3
.end method

.method private b(Z)V
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
istore 4
iconst_0
istore 2
L0:
iload 2
iload 4
if_icmpge L1
iload 1
ifeq L2
iconst_2
istore 3
L3:
aload 0
iload 2
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
iload 3
aconst_null
invokestatic android/support/v4/view/cx/a(Landroid/view/View;ILandroid/graphics/Paint;)V
iload 2
iconst_1
iadd
istore 2
goto L0
L2:
iconst_0
istore 3
goto L3
L1:
return
.limit locals 5
.limit stack 3
.end method

.method private b(F)Z
iconst_1
istore 6
iconst_0
istore 9
iconst_0
istore 8
aload 0
getfield android/support/v4/view/ViewPager/R F
fstore 2
aload 0
fload 1
putfield android/support/v4/view/ViewPager/R F
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
i2f
fload 2
fload 1
fsub
fadd
fstore 3
aload 0
invokespecial android/support/v4/view/ViewPager/getClientWidth()I
istore 7
iload 7
i2f
aload 0
getfield android/support/v4/view/ViewPager/E F
fmul
fstore 1
iload 7
i2f
fstore 2
aload 0
getfield android/support/v4/view/ViewPager/F F
fstore 4
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 10
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 11
aload 10
getfield android/support/v4/view/er/b I
ifeq L0
aload 10
getfield android/support/v4/view/er/e F
iload 7
i2f
fmul
fstore 1
iconst_0
istore 5
L1:
aload 11
getfield android/support/v4/view/er/b I
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
iconst_1
isub
if_icmpeq L2
aload 11
getfield android/support/v4/view/er/e F
iload 7
i2f
fmul
fstore 2
iconst_0
istore 6
L3:
fload 3
fload 1
fcmpg
ifge L4
fload 1
fstore 2
iload 5
ifeq L5
aload 0
getfield android/support/v4/view/ViewPager/ai Landroid/support/v4/widget/al;
fload 1
fload 3
fsub
invokestatic java/lang/Math/abs(F)F
iload 7
i2f
fdiv
invokevirtual android/support/v4/widget/al/a(F)Z
istore 8
fload 1
fstore 2
L5:
aload 0
aload 0
getfield android/support/v4/view/ViewPager/R F
fload 2
fload 2
f2i
i2f
fsub
fadd
putfield android/support/v4/view/ViewPager/R F
aload 0
fload 2
f2i
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollY()I
invokevirtual android/support/v4/view/ViewPager/scrollTo(II)V
aload 0
fload 2
f2i
invokespecial android/support/v4/view/ViewPager/c(I)Z
pop
iload 8
ireturn
L4:
fload 3
fload 2
fcmpl
ifle L6
iload 9
istore 8
iload 6
ifeq L7
aload 0
getfield android/support/v4/view/ViewPager/aj Landroid/support/v4/widget/al;
fload 3
fload 2
fsub
invokestatic java/lang/Math/abs(F)F
iload 7
i2f
fdiv
invokevirtual android/support/v4/widget/al/a(F)Z
istore 8
L7:
goto L5
L6:
fload 3
fstore 2
goto L5
L2:
fload 2
fload 4
fmul
fstore 2
goto L3
L0:
iconst_1
istore 5
goto L1
.limit locals 12
.limit stack 4
.end method

.method static synthetic c(Landroid/support/v4/view/ViewPager;)I
aload 0
getfield android/support/v4/view/ViewPager/u I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c(F)V
aload 0
getfield android/support/v4/view/ViewPager/ag Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "No fake drag in progress. Call beginFakeDrag first."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 0
getfield android/support/v4/view/ViewPager/R F
fload 1
fadd
putfield android/support/v4/view/ViewPager/R F
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
i2f
fload 1
fsub
fstore 3
aload 0
invokespecial android/support/v4/view/ViewPager/getClientWidth()I
istore 6
iload 6
i2f
fstore 1
aload 0
getfield android/support/v4/view/ViewPager/E F
fstore 5
iload 6
i2f
fstore 2
aload 0
getfield android/support/v4/view/ViewPager/F F
fstore 4
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 9
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 10
aload 9
getfield android/support/v4/view/er/b I
ifeq L1
aload 9
getfield android/support/v4/view/er/e F
iload 6
i2f
fmul
fstore 1
L2:
aload 10
getfield android/support/v4/view/er/b I
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
iconst_1
isub
if_icmpeq L3
aload 10
getfield android/support/v4/view/er/e F
iload 6
i2f
fmul
fstore 2
L4:
fload 3
fload 1
fcmpg
ifge L5
L6:
aload 0
aload 0
getfield android/support/v4/view/ViewPager/R F
fload 1
fload 1
f2i
i2f
fsub
fadd
putfield android/support/v4/view/ViewPager/R F
aload 0
fload 1
f2i
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollY()I
invokevirtual android/support/v4/view/ViewPager/scrollTo(II)V
aload 0
fload 1
f2i
invokespecial android/support/v4/view/ViewPager/c(I)Z
pop
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 7
aload 0
getfield android/support/v4/view/ViewPager/ah J
lload 7
iconst_2
aload 0
getfield android/support/v4/view/ViewPager/R F
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 9
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
aload 9
invokevirtual android/view/VelocityTracker/addMovement(Landroid/view/MotionEvent;)V
aload 9
invokevirtual android/view/MotionEvent/recycle()V
return
L5:
fload 3
fload 2
fcmpl
ifle L7
fload 2
fstore 1
goto L6
L7:
fload 3
fstore 1
goto L6
L3:
fload 2
fload 4
fmul
fstore 2
goto L4
L1:
fload 1
fload 5
fmul
fstore 1
goto L2
.limit locals 11
.limit stack 8
.end method

.method private c(Landroid/support/v4/view/ev;)V
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
ifnull L0
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
aload 1
invokeinterface java/util/List/remove(Ljava/lang/Object;)Z 1
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private c(I)Z
iconst_0
istore 5
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L0
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/am Z
aload 0
iconst_0
fconst_0
invokespecial android/support/v4/view/ViewPager/a(IF)V
aload 0
getfield android/support/v4/view/ViewPager/am Z
ifne L1
new java/lang/IllegalStateException
dup
ldc "onPageScrolled did not call superclass implementation"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokespecial android/support/v4/view/ViewPager/i()Landroid/support/v4/view/er;
astore 6
aload 0
invokespecial android/support/v4/view/ViewPager/getClientWidth()I
istore 3
aload 0
getfield android/support/v4/view/ViewPager/A I
i2f
iload 3
i2f
fdiv
fstore 2
aload 6
getfield android/support/v4/view/er/b I
istore 4
iload 1
i2f
iload 3
i2f
fdiv
aload 6
getfield android/support/v4/view/er/e F
fsub
aload 6
getfield android/support/v4/view/er/d F
fload 2
fadd
fdiv
fstore 2
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/am Z
aload 0
iload 4
fload 2
invokespecial android/support/v4/view/ViewPager/a(IF)V
aload 0
getfield android/support/v4/view/ViewPager/am Z
ifne L2
new java/lang/IllegalStateException
dup
ldc "onPageScrolled did not call superclass implementation"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L2:
iconst_1
istore 5
L1:
iload 5
ireturn
.limit locals 7
.limit stack 3
.end method

.method static synthetic c()[I
getstatic android/support/v4/view/ViewPager/m [I
areturn
.limit locals 0
.limit stack 1
.end method

.method private d()V
aload 0
iconst_0
invokevirtual android/support/v4/view/ViewPager/setWillNotDraw(Z)V
aload 0
ldc_w 262144
invokevirtual android/support/v4/view/ViewPager/setDescendantFocusability(I)V
aload 0
iconst_1
invokevirtual android/support/v4/view/ViewPager/setFocusable(Z)V
aload 0
invokevirtual android/support/v4/view/ViewPager/getContext()Landroid/content/Context;
astore 2
aload 0
new android/widget/Scroller
dup
aload 2
getstatic android/support/v4/view/ViewPager/p Landroid/view/animation/Interpolator;
invokespecial android/widget/Scroller/<init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
putfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
aload 2
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
astore 3
aload 2
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fstore 1
aload 0
aload 3
invokestatic android/support/v4/view/du/a(Landroid/view/ViewConfiguration;)I
putfield android/support/v4/view/ViewPager/Q I
aload 0
ldc_w 400.0F
fload 1
fmul
f2i
putfield android/support/v4/view/ViewPager/ab I
aload 0
aload 3
invokevirtual android/view/ViewConfiguration/getScaledMaximumFlingVelocity()I
putfield android/support/v4/view/ViewPager/ac I
aload 0
new android/support/v4/widget/al
dup
aload 2
invokespecial android/support/v4/widget/al/<init>(Landroid/content/Context;)V
putfield android/support/v4/view/ViewPager/ai Landroid/support/v4/widget/al;
aload 0
new android/support/v4/widget/al
dup
aload 2
invokespecial android/support/v4/widget/al/<init>(Landroid/content/Context;)V
putfield android/support/v4/view/ViewPager/aj Landroid/support/v4/widget/al;
aload 0
ldc_w 25.0F
fload 1
fmul
f2i
putfield android/support/v4/view/ViewPager/ad I
aload 0
fconst_2
fload 1
fmul
f2i
putfield android/support/v4/view/ViewPager/ae I
aload 0
ldc_w 16.0F
fload 1
fmul
f2i
putfield android/support/v4/view/ViewPager/O I
aload 0
new android/support/v4/view/et
dup
aload 0
invokespecial android/support/v4/view/et/<init>(Landroid/support/v4/view/ViewPager;)V
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/support/v4/view/a;)V
aload 0
invokestatic android/support/v4/view/cx/c(Landroid/view/View;)I
ifne L0
aload 0
iconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;I)V
L0:
return
.limit locals 4
.limit stack 5
.end method

.method private d(I)V
aload 0
getfield android/support/v4/view/ViewPager/ao Landroid/support/v4/view/ev;
ifnull L0
aload 0
getfield android/support/v4/view/ViewPager/ao Landroid/support/v4/view/ev;
iload 1
invokeinterface android/support/v4/view/ev/b(I)V 1
L0:
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
ifnull L1
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 3
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/view/ev
astore 4
aload 4
ifnull L3
aload 4
iload 1
invokeinterface android/support/v4/view/ev/b(I)V 1
L3:
iload 2
iconst_1
iadd
istore 2
goto L2
L1:
aload 0
getfield android/support/v4/view/ViewPager/ap Landroid/support/v4/view/ev;
ifnull L4
aload 0
getfield android/support/v4/view/ViewPager/ap Landroid/support/v4/view/ev;
iload 1
invokeinterface android/support/v4/view/ev/b(I)V 1
L4:
return
.limit locals 5
.limit stack 2
.end method

.method private e()V
iconst_0
istore 1
L0:
iload 1
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
if_icmpge L1
iload 1
istore 2
aload 0
iload 1
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/view/es
getfield android/support/v4/view/es/a Z
ifne L2
aload 0
iload 1
invokevirtual android/support/v4/view/ViewPager/removeViewAt(I)V
iload 1
iconst_1
isub
istore 2
L2:
iload 2
iconst_1
iadd
istore 1
goto L0
L1:
return
.limit locals 3
.limit stack 2
.end method

.method private e(I)V
aload 0
getfield android/support/v4/view/ViewPager/ao Landroid/support/v4/view/ev;
ifnull L0
aload 0
getfield android/support/v4/view/ViewPager/ao Landroid/support/v4/view/ev;
iload 1
invokeinterface android/support/v4/view/ev/a(I)V 1
L0:
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
ifnull L1
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 3
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/view/ev
astore 4
aload 4
ifnull L3
aload 4
iload 1
invokeinterface android/support/v4/view/ev/a(I)V 1
L3:
iload 2
iconst_1
iadd
istore 2
goto L2
L1:
aload 0
getfield android/support/v4/view/ViewPager/ap Landroid/support/v4/view/ev;
ifnull L4
aload 0
getfield android/support/v4/view/ViewPager/ap Landroid/support/v4/view/ev;
iload 1
invokeinterface android/support/v4/view/ev/a(I)V 1
L4:
return
.limit locals 5
.limit stack 2
.end method

.method private f()V
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
ifnull L0
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
invokeinterface java/util/List/clear()V 0
L0:
return
.limit locals 1
.limit stack 1
.end method

.method private f(I)Z
iconst_0
istore 4
aload 0
invokevirtual android/support/v4/view/ViewPager/findFocus()Landroid/view/View;
astore 6
aload 6
aload 0
if_acmpne L0
aconst_null
astore 5
L1:
invokestatic android/view/FocusFinder/getInstance()Landroid/view/FocusFinder;
aload 0
aload 5
iload 1
invokevirtual android/view/FocusFinder/findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
astore 6
aload 6
ifnull L2
aload 6
aload 5
if_acmpeq L2
iload 1
bipush 17
if_icmpne L3
aload 0
aload 0
getfield android/support/v4/view/ViewPager/s Landroid/graphics/Rect;
aload 6
invokespecial android/support/v4/view/ViewPager/a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
istore 2
aload 0
aload 0
getfield android/support/v4/view/ViewPager/s Landroid/graphics/Rect;
aload 5
invokespecial android/support/v4/view/ViewPager/a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
istore 3
aload 5
ifnull L4
iload 2
iload 3
if_icmplt L4
aload 0
invokespecial android/support/v4/view/ViewPager/n()Z
istore 4
L5:
iload 4
ifeq L6
aload 0
iload 1
invokestatic android/view/SoundEffectConstants/getContantForFocusDirection(I)I
invokevirtual android/support/v4/view/ViewPager/playSoundEffect(I)V
L6:
iload 4
ireturn
L0:
aload 6
ifnull L7
aload 6
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 5
L8:
aload 5
instanceof android/view/ViewGroup
ifeq L9
aload 5
aload 0
if_acmpne L10
iconst_1
istore 2
L11:
iload 2
ifne L7
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 7
aload 7
aload 6
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 6
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 5
L12:
aload 5
instanceof android/view/ViewGroup
ifeq L13
aload 7
ldc " => "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
invokeinterface android/view/ViewParent/getParent()Landroid/view/ViewParent; 0
astore 5
goto L12
L10:
aload 5
invokeinterface android/view/ViewParent/getParent()Landroid/view/ViewParent; 0
astore 5
goto L8
L13:
ldc "ViewPager"
new java/lang/StringBuilder
dup
ldc "arrowScroll tried to find focus based on non-child current focused view "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
astore 5
goto L1
L4:
aload 6
invokevirtual android/view/View/requestFocus()Z
istore 4
goto L5
L3:
iload 1
bipush 66
if_icmpne L5
aload 0
aload 0
getfield android/support/v4/view/ViewPager/s Landroid/graphics/Rect;
aload 6
invokespecial android/support/v4/view/ViewPager/a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
istore 2
aload 0
aload 0
getfield android/support/v4/view/ViewPager/s Landroid/graphics/Rect;
aload 5
invokespecial android/support/v4/view/ViewPager/a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
istore 3
aload 5
ifnull L14
iload 2
iload 3
if_icmple L15
L14:
aload 6
invokevirtual android/view/View/requestFocus()Z
istore 4
goto L5
L2:
iload 1
bipush 17
if_icmpeq L16
iload 1
iconst_1
if_icmpne L17
L16:
aload 0
invokespecial android/support/v4/view/ViewPager/n()Z
istore 4
goto L5
L17:
iload 1
bipush 66
if_icmpeq L15
iload 1
iconst_2
if_icmpne L5
L15:
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
ifnull L18
aload 0
getfield android/support/v4/view/ViewPager/u I
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
iconst_1
isub
if_icmpge L18
aload 0
aload 0
getfield android/support/v4/view/ViewPager/u I
iconst_1
iadd
invokespecial android/support/v4/view/ViewPager/setCurrentItem$2563266(I)V
iconst_1
istore 4
goto L5
L18:
iconst_0
istore 4
goto L5
L7:
aload 6
astore 5
goto L1
L9:
iconst_0
istore 2
goto L11
.limit locals 8
.limit stack 4
.end method

.method private g()V
aload 0
getfield android/support/v4/view/ViewPager/aw I
ifeq L0
aload 0
getfield android/support/v4/view/ViewPager/ax Ljava/util/ArrayList;
ifnonnull L1
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/view/ViewPager/ax Ljava/util/ArrayList;
L2:
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
istore 2
iconst_0
istore 1
L3:
iload 1
iload 2
if_icmpge L4
aload 0
iload 1
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 3
aload 0
getfield android/support/v4/view/ViewPager/ax Ljava/util/ArrayList;
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 1
iconst_1
iadd
istore 1
goto L3
L1:
aload 0
getfield android/support/v4/view/ViewPager/ax Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
goto L2
L4:
aload 0
getfield android/support/v4/view/ViewPager/ax Ljava/util/ArrayList;
getstatic android/support/v4/view/ViewPager/ay Landroid/support/v4/view/fa;
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
L0:
return
.limit locals 4
.limit stack 3
.end method

.method private getClientWidth()I
aload 0
invokevirtual android/support/v4/view/ViewPager/getMeasuredWidth()I
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingLeft()I
isub
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingRight()I
isub
ireturn
.limit locals 1
.limit stack 2
.end method

.method private h()V
aload 0
invokevirtual android/support/v4/view/ViewPager/getParent()Landroid/view/ViewParent;
astore 1
aload 1
ifnull L0
aload 1
iconst_1
invokeinterface android/view/ViewParent/requestDisallowInterceptTouchEvent(Z)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private i()Landroid/support/v4/view/er;
aload 0
invokespecial android/support/v4/view/ViewPager/getClientWidth()I
istore 5
iload 5
ifle L0
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
i2f
iload 5
i2f
fdiv
fstore 1
L1:
iload 5
ifle L2
aload 0
getfield android/support/v4/view/ViewPager/A I
i2f
iload 5
i2f
fdiv
fstore 2
L3:
fconst_0
fstore 4
fconst_0
fstore 3
iconst_m1
istore 7
iconst_0
istore 5
iconst_1
istore 6
aconst_null
astore 9
L4:
aload 9
astore 10
iload 5
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L5
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 8
iload 6
ifne L6
aload 8
getfield android/support/v4/view/er/b I
iload 7
iconst_1
iadd
if_icmpeq L6
aload 0
getfield android/support/v4/view/ViewPager/r Landroid/support/v4/view/er;
astore 8
aload 8
fload 4
fload 3
fadd
fload 2
fadd
putfield android/support/v4/view/er/e F
aload 8
iload 7
iconst_1
iadd
putfield android/support/v4/view/er/b I
aload 8
fconst_1
putfield android/support/v4/view/er/d F
iload 5
iconst_1
isub
istore 5
L7:
aload 8
getfield android/support/v4/view/er/e F
fstore 3
aload 8
getfield android/support/v4/view/er/d F
fstore 4
iload 6
ifne L8
aload 9
astore 10
fload 1
fload 3
fcmpl
iflt L5
L8:
fload 1
fload 4
fload 3
fadd
fload 2
fadd
fcmpg
iflt L9
iload 5
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
if_icmpne L10
L9:
aload 8
astore 10
L5:
aload 10
areturn
L0:
fconst_0
fstore 1
goto L1
L2:
fconst_0
fstore 2
goto L3
L10:
aload 8
getfield android/support/v4/view/er/b I
istore 7
aload 8
getfield android/support/v4/view/er/d F
fstore 4
iconst_0
istore 6
iload 5
iconst_1
iadd
istore 5
aload 8
astore 9
goto L4
L6:
goto L7
.limit locals 11
.limit stack 3
.end method

.method private j()Z
aload 0
getfield android/support/v4/view/ViewPager/M Z
ifeq L0
iconst_0
ireturn
L0:
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/ag Z
aload 0
iconst_1
invokespecial android/support/v4/view/ViewPager/setScrollState(I)V
aload 0
fconst_0
putfield android/support/v4/view/ViewPager/R F
aload 0
fconst_0
putfield android/support/v4/view/ViewPager/T F
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
ifnonnull L1
aload 0
invokestatic android/view/VelocityTracker/obtain()Landroid/view/VelocityTracker;
putfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
L2:
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 1
lload 1
lload 1
iconst_0
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 3
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
aload 3
invokevirtual android/view/VelocityTracker/addMovement(Landroid/view/MotionEvent;)V
aload 3
invokevirtual android/view/MotionEvent/recycle()V
aload 0
lload 1
putfield android/support/v4/view/ViewPager/ah J
iconst_1
ireturn
L1:
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/clear()V
goto L2
.limit locals 4
.limit stack 8
.end method

.method private k()V
aload 0
getfield android/support/v4/view/ViewPager/ag Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "No fake drag in progress. Call beginFakeDrag first."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
astore 4
aload 4
sipush 1000
aload 0
getfield android/support/v4/view/ViewPager/ac I
i2f
invokevirtual android/view/VelocityTracker/computeCurrentVelocity(IF)V
aload 4
aload 0
getfield android/support/v4/view/ViewPager/V I
invokestatic android/support/v4/view/cs/a(Landroid/view/VelocityTracker;I)F
f2i
istore 1
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/K Z
aload 0
invokespecial android/support/v4/view/ViewPager/getClientWidth()I
istore 2
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
istore 3
aload 0
invokespecial android/support/v4/view/ViewPager/i()Landroid/support/v4/view/er;
astore 4
aload 0
aload 0
aload 4
getfield android/support/v4/view/er/b I
iload 3
i2f
iload 2
i2f
fdiv
aload 4
getfield android/support/v4/view/er/e F
fsub
aload 4
getfield android/support/v4/view/er/d F
fdiv
iload 1
aload 0
getfield android/support/v4/view/ViewPager/R F
aload 0
getfield android/support/v4/view/ViewPager/T F
fsub
f2i
invokespecial android/support/v4/view/ViewPager/a(IFII)I
iconst_1
iconst_1
iload 1
invokespecial android/support/v4/view/ViewPager/a(IZZI)V
aload 0
invokespecial android/support/v4/view/ViewPager/m()V
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/ag Z
return
.limit locals 5
.limit stack 7
.end method

.method private l()Z
aload 0
getfield android/support/v4/view/ViewPager/ag Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private m()V
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/M Z
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/N Z
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
ifnull L0
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/recycle()V
aload 0
aconst_null
putfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private n()Z
aload 0
getfield android/support/v4/view/ViewPager/u I
ifle L0
aload 0
aload 0
getfield android/support/v4/view/ViewPager/u I
iconst_1
isub
invokespecial android/support/v4/view/ViewPager/setCurrentItem$2563266(I)V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 3
.end method

.method private o()Z
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
ifnull L0
aload 0
getfield android/support/v4/view/ViewPager/u I
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
iconst_1
isub
if_icmpge L0
aload 0
aload 0
getfield android/support/v4/view/ViewPager/u I
iconst_1
iadd
invokespecial android/support/v4/view/ViewPager/setCurrentItem$2563266(I)V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 3
.end method

.method private setCurrentItem$2563266(I)V
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/K Z
aload 0
iload 1
iconst_1
iconst_0
invokespecial android/support/v4/view/ViewPager/a(IZZ)V
return
.limit locals 2
.limit stack 4
.end method

.method private setScrollState(I)V
iconst_0
istore 5
aload 0
getfield android/support/v4/view/ViewPager/aA I
iload 1
if_icmpne L0
L1:
return
L0:
aload 0
iload 1
putfield android/support/v4/view/ViewPager/aA I
aload 0
getfield android/support/v4/view/ViewPager/ar Landroid/support/v4/view/ew;
ifnull L2
iload 1
ifeq L3
iconst_1
istore 2
L4:
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
istore 6
iconst_0
istore 3
L5:
iload 3
iload 6
if_icmpge L2
iload 2
ifeq L6
iconst_2
istore 4
L7:
aload 0
iload 3
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
iload 4
aconst_null
invokestatic android/support/v4/view/cx/a(Landroid/view/View;ILandroid/graphics/Paint;)V
iload 3
iconst_1
iadd
istore 3
goto L5
L3:
iconst_0
istore 2
goto L4
L6:
iconst_0
istore 4
goto L7
L2:
aload 0
getfield android/support/v4/view/ViewPager/ao Landroid/support/v4/view/ev;
ifnull L8
aload 0
getfield android/support/v4/view/ViewPager/ao Landroid/support/v4/view/ev;
iload 1
invokeinterface android/support/v4/view/ev/a(I)V 1
L8:
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
ifnull L9
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 3
iload 5
istore 2
L10:
iload 2
iload 3
if_icmpge L9
aload 0
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/view/ev
astore 7
aload 7
ifnull L11
aload 7
iload 1
invokeinterface android/support/v4/view/ev/a(I)V 1
L11:
iload 2
iconst_1
iadd
istore 2
goto L10
L9:
aload 0
getfield android/support/v4/view/ViewPager/ap Landroid/support/v4/view/ev;
ifnull L1
aload 0
getfield android/support/v4/view/ViewPager/ap Landroid/support/v4/view/ev;
iload 1
invokeinterface android/support/v4/view/ev/a(I)V 1
return
.limit locals 8
.limit stack 3
.end method

.method private setScrollingCacheEnabled(Z)V
aload 0
getfield android/support/v4/view/ViewPager/J Z
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v4/view/ViewPager/J Z
L0:
return
.limit locals 2
.limit stack 2
.end method

.method final a(Landroid/support/v4/view/ev;)Landroid/support/v4/view/ev;
aload 0
getfield android/support/v4/view/ViewPager/ap Landroid/support/v4/view/ev;
astore 2
aload 0
aload 1
putfield android/support/v4/view/ViewPager/ap Landroid/support/v4/view/ev;
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method final a()V
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
istore 1
aload 0
iload 1
putfield android/support/v4/view/ViewPager/n I
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
aload 0
getfield android/support/v4/view/ViewPager/L I
iconst_2
imul
iconst_1
iadd
if_icmpge L0
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iload 1
if_icmpge L0
iconst_1
istore 1
L1:
aload 0
getfield android/support/v4/view/ViewPager/u I
istore 3
iconst_0
istore 2
L2:
iload 2
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L3
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
pop
iload 2
iconst_1
iadd
istore 2
goto L2
L0:
iconst_0
istore 1
goto L1
L3:
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
getstatic android/support/v4/view/ViewPager/o Ljava/util/Comparator;
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
iload 1
ifeq L4
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
istore 2
iconst_0
istore 1
L5:
iload 1
iload 2
if_icmpge L6
aload 0
iload 1
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/view/es
astore 4
aload 4
getfield android/support/v4/view/es/a Z
ifne L7
aload 4
fconst_0
putfield android/support/v4/view/es/c F
L7:
iload 1
iconst_1
iadd
istore 1
goto L5
L6:
aload 0
iload 3
iconst_0
iconst_1
invokespecial android/support/v4/view/ViewPager/a(IZZ)V
aload 0
invokevirtual android/support/v4/view/ViewPager/requestLayout()V
L4:
return
.limit locals 5
.limit stack 4
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
aload 1
invokevirtual java/util/ArrayList/size()I
istore 5
aload 0
invokevirtual android/support/v4/view/ViewPager/getDescendantFocusability()I
istore 6
iload 6
ldc_w 393216
if_icmpeq L0
iconst_0
istore 4
L1:
iload 4
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
if_icmpge L0
aload 0
iload 4
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 7
aload 7
invokevirtual android/view/View/getVisibility()I
ifne L2
aload 0
aload 7
invokespecial android/support/v4/view/ViewPager/a(Landroid/view/View;)Landroid/support/v4/view/er;
astore 8
aload 8
ifnull L2
aload 8
getfield android/support/v4/view/er/b I
aload 0
getfield android/support/v4/view/ViewPager/u I
if_icmpne L2
aload 7
aload 1
iload 2
iload 3
invokevirtual android/view/View/addFocusables(Ljava/util/ArrayList;II)V
L2:
iload 4
iconst_1
iadd
istore 4
goto L1
L0:
iload 6
ldc_w 262144
if_icmpne L3
iload 5
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpne L4
L3:
aload 0
invokevirtual android/support/v4/view/ViewPager/isFocusable()Z
ifne L5
L4:
return
L5:
iload 3
iconst_1
iand
iconst_1
if_icmpne L6
aload 0
invokevirtual android/support/v4/view/ViewPager/isInTouchMode()Z
ifeq L6
aload 0
invokevirtual android/support/v4/view/ViewPager/isFocusableInTouchMode()Z
ifeq L4
L6:
aload 1
ifnull L4
aload 1
aload 0
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
return
.limit locals 9
.limit stack 4
.end method

.method public addTouchables(Ljava/util/ArrayList;)V
iconst_0
istore 2
L0:
iload 2
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 3
aload 3
invokevirtual android/view/View/getVisibility()I
ifne L2
aload 0
aload 3
invokespecial android/support/v4/view/ViewPager/a(Landroid/view/View;)Landroid/support/v4/view/er;
astore 4
aload 4
ifnull L2
aload 4
getfield android/support/v4/view/er/b I
aload 0
getfield android/support/v4/view/ViewPager/u I
if_icmpne L2
aload 3
aload 1
invokevirtual android/view/View/addTouchables(Ljava/util/ArrayList;)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 5
.limit stack 2
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 3
invokevirtual android/support/v4/view/ViewPager/checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
ifne L0
aload 0
aload 3
invokevirtual android/support/v4/view/ViewPager/generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
astore 3
L1:
aload 3
checkcast android/support/v4/view/es
astore 4
aload 4
aload 4
getfield android/support/v4/view/es/a Z
aload 1
instanceof android/support/v4/view/eq
ior
putfield android/support/v4/view/es/a Z
aload 0
getfield android/support/v4/view/ViewPager/I Z
ifeq L2
aload 4
ifnull L3
aload 4
getfield android/support/v4/view/es/a Z
ifeq L3
new java/lang/IllegalStateException
dup
ldc "Cannot add pager decor view during layout"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 4
iconst_1
putfield android/support/v4/view/es/d Z
aload 0
aload 1
iload 2
aload 3
invokevirtual android/support/v4/view/ViewPager/addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z
pop
return
L2:
aload 0
aload 1
iload 2
aload 3
invokespecial android/view/ViewGroup/addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
return
L0:
goto L1
.limit locals 5
.limit stack 4
.end method

.method final b()V
aload 0
aload 0
getfield android/support/v4/view/ViewPager/u I
invokespecial android/support/v4/view/ViewPager/a(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public canScrollHorizontally(I)Z
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
ifnonnull L0
L1:
iconst_0
ireturn
L0:
aload 0
invokespecial android/support/v4/view/ViewPager/getClientWidth()I
istore 2
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
istore 3
iload 1
ifge L2
iload 3
iload 2
i2f
aload 0
getfield android/support/v4/view/ViewPager/E F
fmul
f2i
if_icmple L1
iconst_1
ireturn
L2:
iload 1
ifle L1
iload 3
iload 2
i2f
aload 0
getfield android/support/v4/view/ViewPager/F F
fmul
f2i
if_icmpge L1
iconst_1
ireturn
.limit locals 4
.limit stack 3
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 1
instanceof android/support/v4/view/es
ifeq L0
aload 0
aload 1
invokespecial android/view/ViewGroup/checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public computeScroll()V
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/isFinished()Z
ifne L0
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/computeScrollOffset()Z
ifeq L0
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
istore 1
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollY()I
istore 2
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/getCurrX()I
istore 3
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/getCurrY()I
istore 4
iload 1
iload 3
if_icmpne L1
iload 2
iload 4
if_icmpeq L2
L1:
aload 0
iload 3
iload 4
invokevirtual android/support/v4/view/ViewPager/scrollTo(II)V
aload 0
iload 3
invokespecial android/support/v4/view/ViewPager/c(I)Z
ifne L2
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/abortAnimation()V
aload 0
iconst_0
iload 4
invokevirtual android/support/v4/view/ViewPager/scrollTo(II)V
L2:
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
return
L0:
aload 0
iconst_1
invokespecial android/support/v4/view/ViewPager/a(Z)V
return
.limit locals 5
.limit stack 3
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
iconst_0
istore 3
aload 0
aload 1
invokespecial android/view/ViewGroup/dispatchKeyEvent(Landroid/view/KeyEvent;)Z
ifne L0
aload 1
invokevirtual android/view/KeyEvent/getAction()I
ifne L1
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
lookupswitch
21 : L2
22 : L3
61 : L4
default : L1
L1:
iconst_0
istore 2
L5:
iload 2
ifeq L6
L0:
iconst_1
istore 3
L6:
iload 3
ireturn
L2:
aload 0
bipush 17
invokespecial android/support/v4/view/ViewPager/f(I)Z
istore 2
goto L5
L3:
aload 0
bipush 66
invokespecial android/support/v4/view/ViewPager/f(I)Z
istore 2
goto L5
L4:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L1
aload 1
invokestatic android/support/v4/view/ab/b(Landroid/view/KeyEvent;)Z
ifeq L7
aload 0
iconst_2
invokespecial android/support/v4/view/ViewPager/f(I)Z
istore 2
goto L5
L7:
aload 1
invokestatic android/support/v4/view/ab/a(Landroid/view/KeyEvent;)Z
ifeq L1
aload 0
iconst_1
invokespecial android/support/v4/view/ViewPager/f(I)Z
istore 2
goto L5
.limit locals 4
.limit stack 2
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
iconst_0
istore 5
aload 1
invokevirtual android/view/accessibility/AccessibilityEvent/getEventType()I
sipush 4096
if_icmpne L0
aload 0
aload 1
invokespecial android/view/ViewGroup/dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
istore 4
L1:
iload 4
ireturn
L0:
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
istore 3
iconst_0
istore 2
L2:
iload 5
istore 4
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 6
aload 6
invokevirtual android/view/View/getVisibility()I
ifne L3
aload 0
aload 6
invokespecial android/support/v4/view/ViewPager/a(Landroid/view/View;)Landroid/support/v4/view/er;
astore 7
aload 7
ifnull L3
aload 7
getfield android/support/v4/view/er/b I
aload 0
getfield android/support/v4/view/ViewPager/u I
if_icmpne L3
aload 6
aload 1
invokevirtual android/view/View/dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
ifeq L3
iconst_1
ireturn
L3:
iload 2
iconst_1
iadd
istore 2
goto L2
.limit locals 8
.limit stack 2
.end method

.method public draw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/view/ViewGroup/draw(Landroid/graphics/Canvas;)V
iconst_0
istore 3
iconst_0
istore 2
aload 0
invokestatic android/support/v4/view/cx/a(Landroid/view/View;)I
istore 4
iload 4
ifeq L0
iload 4
iconst_1
if_icmpne L1
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
ifnull L1
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
iconst_1
if_icmple L1
L0:
aload 0
getfield android/support/v4/view/ViewPager/ai Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/a()Z
ifne L2
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 3
aload 0
invokevirtual android/support/v4/view/ViewPager/getHeight()I
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingTop()I
isub
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingBottom()I
isub
istore 2
aload 0
invokevirtual android/support/v4/view/ViewPager/getWidth()I
istore 4
aload 1
ldc_w 270.0F
invokevirtual android/graphics/Canvas/rotate(F)V
aload 1
iload 2
ineg
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingTop()I
iadd
i2f
aload 0
getfield android/support/v4/view/ViewPager/E F
iload 4
i2f
fmul
invokevirtual android/graphics/Canvas/translate(FF)V
aload 0
getfield android/support/v4/view/ViewPager/ai Landroid/support/v4/widget/al;
iload 2
iload 4
invokevirtual android/support/v4/widget/al/a(II)V
aload 0
getfield android/support/v4/view/ViewPager/ai Landroid/support/v4/widget/al;
aload 1
invokevirtual android/support/v4/widget/al/a(Landroid/graphics/Canvas;)Z
iconst_0
ior
istore 2
aload 1
iload 3
invokevirtual android/graphics/Canvas/restoreToCount(I)V
L2:
iload 2
istore 3
aload 0
getfield android/support/v4/view/ViewPager/aj Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/a()Z
ifne L3
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 4
aload 0
invokevirtual android/support/v4/view/ViewPager/getWidth()I
istore 3
aload 0
invokevirtual android/support/v4/view/ViewPager/getHeight()I
istore 5
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingTop()I
istore 6
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingBottom()I
istore 7
aload 1
ldc_w 90.0F
invokevirtual android/graphics/Canvas/rotate(F)V
aload 1
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingTop()I
ineg
i2f
aload 0
getfield android/support/v4/view/ViewPager/F F
fconst_1
fadd
fneg
iload 3
i2f
fmul
invokevirtual android/graphics/Canvas/translate(FF)V
aload 0
getfield android/support/v4/view/ViewPager/aj Landroid/support/v4/widget/al;
iload 5
iload 6
isub
iload 7
isub
iload 3
invokevirtual android/support/v4/widget/al/a(II)V
iload 2
aload 0
getfield android/support/v4/view/ViewPager/aj Landroid/support/v4/widget/al;
aload 1
invokevirtual android/support/v4/widget/al/a(Landroid/graphics/Canvas;)Z
ior
istore 3
aload 1
iload 4
invokevirtual android/graphics/Canvas/restoreToCount(I)V
L3:
iload 3
ifeq L4
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L4:
return
L1:
aload 0
getfield android/support/v4/view/ViewPager/ai Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/b()V
aload 0
getfield android/support/v4/view/ViewPager/aj Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/b()V
goto L3
.limit locals 8
.limit stack 4
.end method

.method protected drawableStateChanged()V
aload 0
invokespecial android/view/ViewGroup/drawableStateChanged()V
aload 0
getfield android/support/v4/view/ViewPager/B Landroid/graphics/drawable/Drawable;
astore 1
aload 1
ifnull L0
aload 1
invokevirtual android/graphics/drawable/Drawable/isStateful()Z
ifeq L0
aload 1
aload 0
invokevirtual android/support/v4/view/ViewPager/getDrawableState()[I
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
new android/support/v4/view/es
dup
invokespecial android/support/v4/view/es/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
new android/support/v4/view/es
dup
aload 0
invokevirtual android/support/v4/view/ViewPager/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v4/view/es/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
aload 0
invokevirtual android/support/v4/view/ViewPager/generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
areturn
.limit locals 2
.limit stack 1
.end method

.method public getAdapter()Landroid/support/v4/view/by;
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected getChildDrawingOrder(II)I
iload 2
istore 3
aload 0
getfield android/support/v4/view/ViewPager/aw I
iconst_2
if_icmpne L0
iload 1
iconst_1
isub
iload 2
isub
istore 3
L0:
aload 0
getfield android/support/v4/view/ViewPager/ax Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/view/View
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/view/es
getfield android/support/v4/view/es/f I
ireturn
.limit locals 4
.limit stack 2
.end method

.method public getCurrentItem()I
aload 0
getfield android/support/v4/view/ViewPager/u I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOffscreenPageLimit()I
aload 0
getfield android/support/v4/view/ViewPager/L I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getPageMargin()I
aload 0
getfield android/support/v4/view/ViewPager/A I
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected onAttachedToWindow()V
aload 0
invokespecial android/view/ViewGroup/onAttachedToWindow()V
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/ak Z
return
.limit locals 1
.limit stack 2
.end method

.method protected onDetachedFromWindow()V
aload 0
aload 0
getfield android/support/v4/view/ViewPager/az Ljava/lang/Runnable;
invokevirtual android/support/v4/view/ViewPager/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
invokespecial android/view/ViewGroup/onDetachedFromWindow()V
return
.limit locals 1
.limit stack 2
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/view/ViewGroup/onDraw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v4/view/ViewPager/A I
ifle L0
aload 0
getfield android/support/v4/view/ViewPager/B Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L0
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
ifnull L0
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
istore 7
aload 0
invokevirtual android/support/v4/view/ViewPager/getWidth()I
istore 8
aload 0
getfield android/support/v4/view/ViewPager/A I
i2f
iload 8
i2f
fdiv
fstore 4
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 11
aload 11
getfield android/support/v4/view/er/e F
fstore 2
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 9
aload 11
getfield android/support/v4/view/er/b I
istore 5
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 9
iconst_1
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
getfield android/support/v4/view/er/b I
istore 10
iconst_0
istore 6
L1:
iload 5
iload 10
if_icmpge L0
L2:
iload 5
aload 11
getfield android/support/v4/view/er/b I
if_icmple L3
iload 6
iload 9
if_icmpge L3
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
astore 11
iload 6
iconst_1
iadd
istore 6
aload 11
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 11
goto L2
L3:
iload 5
aload 11
getfield android/support/v4/view/er/b I
if_icmpne L4
aload 11
getfield android/support/v4/view/er/e F
aload 11
getfield android/support/v4/view/er/d F
fadd
iload 8
i2f
fmul
fstore 3
aload 11
getfield android/support/v4/view/er/e F
aload 11
getfield android/support/v4/view/er/d F
fadd
fload 4
fadd
fstore 2
L5:
aload 0
getfield android/support/v4/view/ViewPager/A I
i2f
fload 3
fadd
iload 7
i2f
fcmpl
ifle L6
aload 0
getfield android/support/v4/view/ViewPager/B Landroid/graphics/drawable/Drawable;
fload 3
f2i
aload 0
getfield android/support/v4/view/ViewPager/C I
aload 0
getfield android/support/v4/view/ViewPager/A I
i2f
fload 3
fadd
ldc_w 0.5F
fadd
f2i
aload 0
getfield android/support/v4/view/ViewPager/D I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield android/support/v4/view/ViewPager/B Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
L6:
fload 3
iload 7
iload 8
iadd
i2f
fcmpl
ifgt L0
iload 5
iconst_1
iadd
istore 5
goto L1
L4:
fconst_1
fload 2
fadd
iload 8
i2f
fmul
fstore 3
fload 2
fconst_1
fload 4
fadd
fadd
fstore 2
goto L5
L0:
return
.limit locals 12
.limit stack 5
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
aload 1
invokevirtual android/view/MotionEvent/getAction()I
sipush 255
iand
istore 8
iload 8
iconst_3
if_icmpeq L0
iload 8
iconst_1
if_icmpne L1
L0:
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/M Z
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/N Z
aload 0
iconst_m1
putfield android/support/v4/view/ViewPager/V I
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
ifnull L2
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/recycle()V
aload 0
aconst_null
putfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
L2:
iconst_0
ireturn
L1:
iload 8
ifeq L3
aload 0
getfield android/support/v4/view/ViewPager/M Z
ifeq L4
iconst_1
ireturn
L4:
aload 0
getfield android/support/v4/view/ViewPager/N Z
ifne L2
L3:
iload 8
lookupswitch
0 : L5
2 : L6
6 : L7
default : L8
L8:
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
ifnonnull L9
aload 0
invokestatic android/view/VelocityTracker/obtain()Landroid/view/VelocityTracker;
putfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
L9:
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
aload 1
invokevirtual android/view/VelocityTracker/addMovement(Landroid/view/MotionEvent;)V
aload 0
getfield android/support/v4/view/ViewPager/M Z
ireturn
L6:
aload 0
getfield android/support/v4/view/ViewPager/V I
istore 8
iload 8
iconst_m1
if_icmpeq L8
aload 1
iload 8
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
istore 8
aload 1
iload 8
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
fstore 3
fload 3
aload 0
getfield android/support/v4/view/ViewPager/R F
fsub
fstore 2
fload 2
invokestatic java/lang/Math/abs(F)F
fstore 5
aload 1
iload 8
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
fstore 4
fload 4
aload 0
getfield android/support/v4/view/ViewPager/U F
fsub
invokestatic java/lang/Math/abs(F)F
fstore 6
fload 2
fconst_0
fcmpl
ifeq L10
aload 0
getfield android/support/v4/view/ViewPager/R F
fstore 7
fload 7
aload 0
getfield android/support/v4/view/ViewPager/P I
i2f
fcmpg
ifge L11
fload 2
fconst_0
fcmpl
ifgt L12
L11:
fload 7
aload 0
invokevirtual android/support/v4/view/ViewPager/getWidth()I
aload 0
getfield android/support/v4/view/ViewPager/P I
isub
i2f
fcmpl
ifle L13
fload 2
fconst_0
fcmpg
ifge L13
L12:
iconst_1
istore 8
L14:
iload 8
ifne L10
aload 0
aload 0
iconst_0
fload 2
f2i
fload 3
f2i
fload 4
f2i
invokespecial android/support/v4/view/ViewPager/a(Landroid/view/View;ZIII)Z
ifeq L10
aload 0
fload 3
putfield android/support/v4/view/ViewPager/R F
aload 0
fload 4
putfield android/support/v4/view/ViewPager/S F
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/N Z
iconst_0
ireturn
L13:
iconst_0
istore 8
goto L14
L10:
fload 5
aload 0
getfield android/support/v4/view/ViewPager/Q I
i2f
fcmpl
ifle L15
ldc_w 0.5F
fload 5
fmul
fload 6
fcmpl
ifle L15
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/M Z
aload 0
invokespecial android/support/v4/view/ViewPager/h()V
aload 0
iconst_1
invokespecial android/support/v4/view/ViewPager/setScrollState(I)V
fload 2
fconst_0
fcmpl
ifle L16
aload 0
getfield android/support/v4/view/ViewPager/T F
aload 0
getfield android/support/v4/view/ViewPager/Q I
i2f
fadd
fstore 2
L17:
aload 0
fload 2
putfield android/support/v4/view/ViewPager/R F
aload 0
fload 4
putfield android/support/v4/view/ViewPager/S F
aload 0
iconst_1
invokespecial android/support/v4/view/ViewPager/setScrollingCacheEnabled(Z)V
L18:
aload 0
getfield android/support/v4/view/ViewPager/M Z
ifeq L8
aload 0
fload 3
invokespecial android/support/v4/view/ViewPager/b(F)Z
ifeq L8
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
goto L8
L16:
aload 0
getfield android/support/v4/view/ViewPager/T F
aload 0
getfield android/support/v4/view/ViewPager/Q I
i2f
fsub
fstore 2
goto L17
L15:
fload 6
aload 0
getfield android/support/v4/view/ViewPager/Q I
i2f
fcmpl
ifle L18
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/N Z
goto L18
L5:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 0
fload 2
putfield android/support/v4/view/ViewPager/T F
aload 0
fload 2
putfield android/support/v4/view/ViewPager/R F
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 2
aload 0
fload 2
putfield android/support/v4/view/ViewPager/U F
aload 0
fload 2
putfield android/support/v4/view/ViewPager/S F
aload 0
aload 1
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/v4/view/ViewPager/V I
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/N Z
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/computeScrollOffset()Z
pop
aload 0
getfield android/support/v4/view/ViewPager/aA I
iconst_2
if_icmpne L19
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/getFinalX()I
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/getCurrX()I
isub
invokestatic java/lang/Math/abs(I)I
aload 0
getfield android/support/v4/view/ViewPager/ae I
if_icmple L19
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/abortAnimation()V
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/K Z
aload 0
invokevirtual android/support/v4/view/ViewPager/b()V
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/M Z
aload 0
invokespecial android/support/v4/view/ViewPager/h()V
aload 0
iconst_1
invokespecial android/support/v4/view/ViewPager/setScrollState(I)V
goto L8
L19:
aload 0
iconst_0
invokespecial android/support/v4/view/ViewPager/a(Z)V
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/M Z
goto L8
L7:
aload 0
aload 1
invokespecial android/support/v4/view/ViewPager/a(Landroid/view/MotionEvent;)V
goto L8
.limit locals 9
.limit stack 6
.end method

.method protected onLayout(ZIIII)V
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
istore 12
iload 4
iload 2
isub
istore 14
iload 5
iload 3
isub
istore 13
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingLeft()I
istore 3
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingTop()I
istore 2
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingRight()I
istore 7
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingBottom()I
istore 4
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
istore 15
iconst_0
istore 8
iconst_0
istore 10
L0:
iload 10
iload 12
if_icmpge L1
aload 0
iload 10
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 17
aload 17
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L2
aload 17
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/view/es
astore 18
aload 18
getfield android/support/v4/view/es/a Z
ifeq L2
aload 18
getfield android/support/v4/view/es/b I
istore 5
aload 18
getfield android/support/v4/view/es/b I
istore 16
iload 5
bipush 7
iand
tableswitch 1
L3
L4
L5
L4
L6
default : L4
L4:
iload 3
istore 5
iload 3
istore 9
L7:
iload 16
bipush 112
iand
lookupswitch
16 : L8
48 : L9
80 : L10
default : L11
L11:
iload 2
istore 11
iload 2
istore 3
iload 4
istore 2
iload 11
istore 4
L12:
iload 5
iload 15
iadd
istore 5
aload 17
iload 5
iload 4
aload 17
invokevirtual android/view/View/getMeasuredWidth()I
iload 5
iadd
aload 17
invokevirtual android/view/View/getMeasuredHeight()I
iload 4
iadd
invokevirtual android/view/View/layout(IIII)V
iload 8
iconst_1
iadd
istore 8
iload 7
istore 5
iload 9
istore 4
iload 2
istore 7
iload 8
istore 2
L13:
iload 10
iconst_1
iadd
istore 10
iload 4
istore 9
iload 2
istore 8
iload 3
istore 2
iload 7
istore 4
iload 5
istore 7
iload 9
istore 3
goto L0
L5:
aload 17
invokevirtual android/view/View/getMeasuredWidth()I
istore 9
iload 3
istore 5
iload 9
iload 3
iadd
istore 9
goto L7
L3:
iload 14
aload 17
invokevirtual android/view/View/getMeasuredWidth()I
isub
iconst_2
idiv
iload 3
invokestatic java/lang/Math/max(II)I
istore 5
iload 3
istore 9
goto L7
L6:
aload 17
invokevirtual android/view/View/getMeasuredWidth()I
istore 9
iload 7
aload 17
invokevirtual android/view/View/getMeasuredWidth()I
iadd
istore 5
iload 14
iload 7
isub
iload 9
isub
istore 11
iload 5
istore 7
iload 3
istore 9
iload 11
istore 5
goto L7
L9:
aload 17
invokevirtual android/view/View/getMeasuredHeight()I
istore 11
iload 4
istore 3
iload 11
iload 2
iadd
istore 11
iload 2
istore 4
iload 3
istore 2
iload 11
istore 3
goto L12
L8:
iload 13
aload 17
invokevirtual android/view/View/getMeasuredHeight()I
isub
iconst_2
idiv
iload 2
invokestatic java/lang/Math/max(II)I
istore 11
iload 2
istore 3
iload 4
istore 2
iload 11
istore 4
goto L12
L10:
iload 13
iload 4
isub
aload 17
invokevirtual android/view/View/getMeasuredHeight()I
isub
istore 11
aload 17
invokevirtual android/view/View/getMeasuredHeight()I
istore 16
iload 2
istore 3
iload 4
iload 16
iadd
istore 2
iload 11
istore 4
goto L12
L1:
iload 14
iload 3
isub
iload 7
isub
istore 7
iconst_0
istore 5
L14:
iload 5
iload 12
if_icmpge L15
aload 0
iload 5
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 17
aload 17
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L16
aload 17
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/view/es
astore 18
aload 18
getfield android/support/v4/view/es/a Z
ifne L16
aload 0
aload 17
invokespecial android/support/v4/view/ViewPager/a(Landroid/view/View;)Landroid/support/v4/view/er;
astore 19
aload 19
ifnull L16
iload 7
i2f
fstore 6
aload 19
getfield android/support/v4/view/er/e F
fload 6
fmul
f2i
iload 3
iadd
istore 9
aload 18
getfield android/support/v4/view/es/d Z
ifeq L17
aload 18
iconst_0
putfield android/support/v4/view/es/d Z
iload 7
i2f
fstore 6
aload 17
aload 18
getfield android/support/v4/view/es/c F
fload 6
fmul
f2i
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 13
iload 2
isub
iload 4
isub
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
L17:
aload 17
iload 9
iload 2
aload 17
invokevirtual android/view/View/getMeasuredWidth()I
iload 9
iadd
aload 17
invokevirtual android/view/View/getMeasuredHeight()I
iload 2
iadd
invokevirtual android/view/View/layout(IIII)V
L16:
iload 5
iconst_1
iadd
istore 5
goto L14
L15:
aload 0
iload 2
putfield android/support/v4/view/ViewPager/C I
aload 0
iload 13
iload 4
isub
putfield android/support/v4/view/ViewPager/D I
aload 0
iload 8
putfield android/support/v4/view/ViewPager/an I
aload 0
getfield android/support/v4/view/ViewPager/ak Z
ifeq L18
aload 0
aload 0
getfield android/support/v4/view/ViewPager/u I
iconst_0
iconst_0
iconst_0
invokespecial android/support/v4/view/ViewPager/a(IZIZ)V
L18:
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/ak Z
return
L2:
iload 8
istore 5
iload 2
istore 8
iload 3
istore 9
iload 5
istore 2
iload 7
istore 5
iload 4
istore 7
iload 8
istore 3
iload 9
istore 4
goto L13
.limit locals 20
.limit stack 6
.end method

.method protected onMeasure(II)V
aload 0
iconst_0
iload 1
invokestatic android/support/v4/view/ViewPager/getDefaultSize(II)I
iconst_0
iload 2
invokestatic android/support/v4/view/ViewPager/getDefaultSize(II)I
invokevirtual android/support/v4/view/ViewPager/setMeasuredDimension(II)V
aload 0
invokevirtual android/support/v4/view/ViewPager/getMeasuredWidth()I
istore 1
aload 0
iload 1
bipush 10
idiv
aload 0
getfield android/support/v4/view/ViewPager/O I
invokestatic java/lang/Math/min(II)I
putfield android/support/v4/view/ViewPager/P I
iload 1
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingLeft()I
isub
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingRight()I
isub
istore 1
aload 0
invokevirtual android/support/v4/view/ViewPager/getMeasuredHeight()I
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingTop()I
isub
aload 0
invokevirtual android/support/v4/view/ViewPager/getPaddingBottom()I
isub
istore 2
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
istore 12
iconst_0
istore 6
L0:
iload 6
iload 12
if_icmpge L1
aload 0
iload 6
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 13
iload 1
istore 4
iload 2
istore 5
aload 13
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L2
aload 13
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/view/es
astore 14
iload 1
istore 4
iload 2
istore 5
aload 14
ifnull L2
iload 1
istore 4
iload 2
istore 5
aload 14
getfield android/support/v4/view/es/a Z
ifeq L2
aload 14
getfield android/support/v4/view/es/b I
bipush 7
iand
istore 4
aload 14
getfield android/support/v4/view/es/b I
bipush 112
iand
istore 7
ldc_w -2147483648
istore 9
ldc_w -2147483648
istore 5
iload 7
bipush 48
if_icmpeq L3
iload 7
bipush 80
if_icmpne L4
L3:
iconst_1
istore 7
L5:
iload 4
iconst_3
if_icmpeq L6
iload 4
iconst_5
if_icmpne L7
L6:
iconst_1
istore 8
L8:
iload 7
ifeq L9
ldc_w 1073741824
istore 4
L10:
aload 14
getfield android/support/v4/view/es/width I
bipush -2
if_icmpeq L11
ldc_w 1073741824
istore 9
aload 14
getfield android/support/v4/view/es/width I
iconst_m1
if_icmpeq L12
aload 14
getfield android/support/v4/view/es/width I
istore 4
L13:
aload 14
getfield android/support/v4/view/es/height I
bipush -2
if_icmpeq L14
ldc_w 1073741824
istore 10
iload 10
istore 5
aload 14
getfield android/support/v4/view/es/height I
iconst_m1
if_icmpeq L14
aload 14
getfield android/support/v4/view/es/height I
istore 11
iload 10
istore 5
iload 11
istore 10
L15:
aload 13
iload 4
iload 9
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 10
iload 5
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
iload 7
ifeq L16
iload 2
aload 13
invokevirtual android/view/View/getMeasuredHeight()I
isub
istore 5
iload 1
istore 4
L2:
iload 6
iconst_1
iadd
istore 6
iload 4
istore 1
iload 5
istore 2
goto L0
L4:
iconst_0
istore 7
goto L5
L7:
iconst_0
istore 8
goto L8
L9:
iload 9
istore 4
iload 8
ifeq L10
ldc_w 1073741824
istore 5
iload 9
istore 4
goto L10
L16:
iload 1
istore 4
iload 2
istore 5
iload 8
ifeq L2
iload 1
aload 13
invokevirtual android/view/View/getMeasuredWidth()I
isub
istore 4
iload 2
istore 5
goto L2
L1:
aload 0
iload 1
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
putfield android/support/v4/view/ViewPager/G I
aload 0
iload 2
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
putfield android/support/v4/view/ViewPager/H I
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/I Z
aload 0
invokevirtual android/support/v4/view/ViewPager/b()V
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/I Z
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
istore 4
iconst_0
istore 2
L17:
iload 2
iload 4
if_icmpge L18
aload 0
iload 2
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 13
aload 13
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L19
aload 13
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/view/es
astore 14
aload 14
ifnull L20
aload 14
getfield android/support/v4/view/es/a Z
ifne L19
L20:
iload 1
i2f
fstore 3
aload 13
aload 14
getfield android/support/v4/view/es/c F
fload 3
fmul
f2i
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
aload 0
getfield android/support/v4/view/ViewPager/H I
invokevirtual android/view/View/measure(II)V
L19:
iload 2
iconst_1
iadd
istore 2
goto L17
L18:
return
L14:
iload 2
istore 10
goto L15
L12:
iload 1
istore 4
goto L13
L11:
iload 4
istore 9
iload 1
istore 4
goto L13
.limit locals 15
.limit stack 4
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
iconst_m1
istore 5
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
istore 4
iload 1
iconst_2
iand
ifeq L0
iconst_1
istore 5
iconst_0
istore 3
L1:
iload 3
iload 4
if_icmpeq L2
aload 0
iload 3
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
astore 6
aload 6
invokevirtual android/view/View/getVisibility()I
ifne L3
aload 0
aload 6
invokespecial android/support/v4/view/ViewPager/a(Landroid/view/View;)Landroid/support/v4/view/er;
astore 7
aload 7
ifnull L3
aload 7
getfield android/support/v4/view/er/b I
aload 0
getfield android/support/v4/view/ViewPager/u I
if_icmpne L3
aload 6
iload 1
aload 2
invokevirtual android/view/View/requestFocus(ILandroid/graphics/Rect;)Z
ifeq L3
iconst_1
ireturn
L0:
iload 4
iconst_1
isub
istore 3
iconst_m1
istore 4
goto L1
L3:
iload 3
iload 5
iadd
istore 3
goto L1
L2:
iconst_0
ireturn
.limit locals 8
.limit stack 3
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
instanceof android/support/v4/view/ViewPager$SavedState
ifne L0
aload 0
aload 1
invokespecial android/view/ViewGroup/onRestoreInstanceState(Landroid/os/Parcelable;)V
return
L0:
aload 1
checkcast android/support/v4/view/ViewPager$SavedState
astore 1
aload 0
aload 1
invokevirtual android/support/v4/view/ViewPager$SavedState/getSuperState()Landroid/os/Parcelable;
invokespecial android/view/ViewGroup/onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
ifnull L1
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
aload 1
getfield android/support/v4/view/ViewPager$SavedState/b Landroid/os/Parcelable;
aload 1
getfield android/support/v4/view/ViewPager$SavedState/c Ljava/lang/ClassLoader;
invokevirtual android/support/v4/view/by/a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
aload 0
aload 1
getfield android/support/v4/view/ViewPager$SavedState/a I
iconst_0
iconst_1
invokespecial android/support/v4/view/ViewPager/a(IZZ)V
return
L1:
aload 0
aload 1
getfield android/support/v4/view/ViewPager$SavedState/a I
putfield android/support/v4/view/ViewPager/v I
aload 0
aload 1
getfield android/support/v4/view/ViewPager$SavedState/b Landroid/os/Parcelable;
putfield android/support/v4/view/ViewPager/w Landroid/os/Parcelable;
aload 0
aload 1
getfield android/support/v4/view/ViewPager$SavedState/c Ljava/lang/ClassLoader;
putfield android/support/v4/view/ViewPager/x Ljava/lang/ClassLoader;
return
.limit locals 2
.limit stack 4
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
new android/support/v4/view/ViewPager$SavedState
dup
aload 0
invokespecial android/view/ViewGroup/onSaveInstanceState()Landroid/os/Parcelable;
invokespecial android/support/v4/view/ViewPager$SavedState/<init>(Landroid/os/Parcelable;)V
astore 1
aload 1
aload 0
getfield android/support/v4/view/ViewPager/u I
putfield android/support/v4/view/ViewPager$SavedState/a I
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
ifnull L0
aload 1
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/d()Landroid/os/Parcelable;
putfield android/support/v4/view/ViewPager$SavedState/b Landroid/os/Parcelable;
L0:
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method protected onSizeChanged(IIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
invokespecial android/view/ViewGroup/onSizeChanged(IIII)V
iload 1
iload 3
if_icmpeq L0
aload 0
iload 1
iload 3
aload 0
getfield android/support/v4/view/ViewPager/A I
aload 0
getfield android/support/v4/view/ViewPager/A I
invokespecial android/support/v4/view/ViewPager/a(IIII)V
L0:
return
.limit locals 5
.limit stack 5
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
iconst_0
istore 7
aload 0
getfield android/support/v4/view/ViewPager/ag Z
ifeq L0
iconst_1
ireturn
L0:
aload 1
invokevirtual android/view/MotionEvent/getAction()I
ifne L1
aload 1
invokevirtual android/view/MotionEvent/getEdgeFlags()I
ifeq L1
iconst_0
ireturn
L1:
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
ifnull L2
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
ifne L3
L2:
iconst_0
ireturn
L3:
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
ifnonnull L4
aload 0
invokestatic android/view/VelocityTracker/obtain()Landroid/view/VelocityTracker;
putfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
L4:
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
aload 1
invokevirtual android/view/VelocityTracker/addMovement(Landroid/view/MotionEvent;)V
iload 7
istore 6
aload 1
invokevirtual android/view/MotionEvent/getAction()I
sipush 255
iand
tableswitch 0
L5
L6
L7
L8
L9
L10
L11
default : L12
L12:
iload 7
istore 6
L9:
iload 6
ifeq L13
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L13:
iconst_1
ireturn
L5:
aload 0
getfield android/support/v4/view/ViewPager/y Landroid/widget/Scroller;
invokevirtual android/widget/Scroller/abortAnimation()V
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/K Z
aload 0
invokevirtual android/support/v4/view/ViewPager/b()V
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 0
fload 2
putfield android/support/v4/view/ViewPager/T F
aload 0
fload 2
putfield android/support/v4/view/ViewPager/R F
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 2
aload 0
fload 2
putfield android/support/v4/view/ViewPager/U F
aload 0
fload 2
putfield android/support/v4/view/ViewPager/S F
aload 0
aload 1
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/v4/view/ViewPager/V I
iload 7
istore 6
goto L9
L7:
aload 0
getfield android/support/v4/view/ViewPager/M Z
ifne L14
aload 1
aload 0
getfield android/support/v4/view/ViewPager/V I
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
istore 6
aload 1
iload 6
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
fstore 2
fload 2
aload 0
getfield android/support/v4/view/ViewPager/R F
fsub
invokestatic java/lang/Math/abs(F)F
fstore 4
aload 1
iload 6
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
fstore 3
fload 3
aload 0
getfield android/support/v4/view/ViewPager/S F
fsub
invokestatic java/lang/Math/abs(F)F
fstore 5
fload 4
aload 0
getfield android/support/v4/view/ViewPager/Q I
i2f
fcmpl
ifle L14
fload 4
fload 5
fcmpl
ifle L14
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/M Z
aload 0
invokespecial android/support/v4/view/ViewPager/h()V
fload 2
aload 0
getfield android/support/v4/view/ViewPager/T F
fsub
fconst_0
fcmpl
ifle L15
aload 0
getfield android/support/v4/view/ViewPager/T F
aload 0
getfield android/support/v4/view/ViewPager/Q I
i2f
fadd
fstore 2
L16:
aload 0
fload 2
putfield android/support/v4/view/ViewPager/R F
aload 0
fload 3
putfield android/support/v4/view/ViewPager/S F
aload 0
iconst_1
invokespecial android/support/v4/view/ViewPager/setScrollState(I)V
aload 0
iconst_1
invokespecial android/support/v4/view/ViewPager/setScrollingCacheEnabled(Z)V
aload 0
invokevirtual android/support/v4/view/ViewPager/getParent()Landroid/view/ViewParent;
astore 10
aload 10
ifnull L14
aload 10
iconst_1
invokeinterface android/view/ViewParent/requestDisallowInterceptTouchEvent(Z)V 1
L14:
iload 7
istore 6
aload 0
getfield android/support/v4/view/ViewPager/M Z
ifeq L9
aload 0
aload 1
aload 1
aload 0
getfield android/support/v4/view/ViewPager/V I
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
invokespecial android/support/v4/view/ViewPager/b(F)Z
iconst_0
ior
istore 6
goto L9
L15:
aload 0
getfield android/support/v4/view/ViewPager/T F
aload 0
getfield android/support/v4/view/ViewPager/Q I
i2f
fsub
fstore 2
goto L16
L6:
iload 7
istore 6
aload 0
getfield android/support/v4/view/ViewPager/M Z
ifeq L9
aload 0
getfield android/support/v4/view/ViewPager/aa Landroid/view/VelocityTracker;
astore 10
aload 10
sipush 1000
aload 0
getfield android/support/v4/view/ViewPager/ac I
i2f
invokevirtual android/view/VelocityTracker/computeCurrentVelocity(IF)V
aload 10
aload 0
getfield android/support/v4/view/ViewPager/V I
invokestatic android/support/v4/view/cs/a(Landroid/view/VelocityTracker;I)F
f2i
istore 6
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/K Z
aload 0
invokespecial android/support/v4/view/ViewPager/getClientWidth()I
istore 7
aload 0
invokevirtual android/support/v4/view/ViewPager/getScrollX()I
istore 8
aload 0
invokespecial android/support/v4/view/ViewPager/i()Landroid/support/v4/view/er;
astore 10
aload 0
aload 0
aload 10
getfield android/support/v4/view/er/b I
iload 8
i2f
iload 7
i2f
fdiv
aload 10
getfield android/support/v4/view/er/e F
fsub
aload 10
getfield android/support/v4/view/er/d F
fdiv
iload 6
aload 1
aload 1
aload 0
getfield android/support/v4/view/ViewPager/V I
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
aload 0
getfield android/support/v4/view/ViewPager/T F
fsub
f2i
invokespecial android/support/v4/view/ViewPager/a(IFII)I
iconst_1
iconst_1
iload 6
invokespecial android/support/v4/view/ViewPager/a(IZZI)V
aload 0
iconst_m1
putfield android/support/v4/view/ViewPager/V I
aload 0
invokespecial android/support/v4/view/ViewPager/m()V
aload 0
getfield android/support/v4/view/ViewPager/ai Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/c()Z
istore 9
aload 0
getfield android/support/v4/view/ViewPager/aj Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/c()Z
iload 9
ior
istore 6
goto L9
L8:
iload 7
istore 6
aload 0
getfield android/support/v4/view/ViewPager/M Z
ifeq L9
aload 0
aload 0
getfield android/support/v4/view/ViewPager/u I
iconst_1
iconst_0
iconst_0
invokespecial android/support/v4/view/ViewPager/a(IZIZ)V
aload 0
iconst_m1
putfield android/support/v4/view/ViewPager/V I
aload 0
invokespecial android/support/v4/view/ViewPager/m()V
aload 0
getfield android/support/v4/view/ViewPager/ai Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/c()Z
istore 9
aload 0
getfield android/support/v4/view/ViewPager/aj Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/c()Z
iload 9
ior
istore 6
goto L9
L10:
aload 1
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;)I
istore 6
aload 0
aload 1
iload 6
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
putfield android/support/v4/view/ViewPager/R F
aload 0
aload 1
iload 6
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/v4/view/ViewPager/V I
iload 7
istore 6
goto L9
L11:
aload 0
aload 1
invokespecial android/support/v4/view/ViewPager/a(Landroid/view/MotionEvent;)V
aload 0
aload 1
aload 1
aload 0
getfield android/support/v4/view/ViewPager/V I
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
putfield android/support/v4/view/ViewPager/R F
iload 7
istore 6
goto L9
.limit locals 11
.limit stack 8
.end method

.method public removeView(Landroid/view/View;)V
aload 0
getfield android/support/v4/view/ViewPager/I Z
ifeq L0
aload 0
aload 1
invokevirtual android/support/v4/view/ViewPager/removeViewInLayout(Landroid/view/View;)V
return
L0:
aload 0
aload 1
invokespecial android/view/ViewGroup/removeView(Landroid/view/View;)V
return
.limit locals 2
.limit stack 2
.end method

.method public setAdapter(Landroid/support/v4/view/by;)V
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
ifnull L0
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
aload 0
getfield android/support/v4/view/ViewPager/z Landroid/support/v4/view/ex;
invokevirtual android/support/v4/view/by/b(Landroid/database/DataSetObserver;)V
iconst_0
istore 2
L1:
iload 2
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L2
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/view/er
astore 5
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
aload 5
getfield android/support/v4/view/er/b I
aload 5
getfield android/support/v4/view/er/a Ljava/lang/Object;
invokevirtual android/support/v4/view/by/a(ILjava/lang/Object;)V
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/c()V
aload 0
getfield android/support/v4/view/ViewPager/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
iconst_0
istore 2
L3:
iload 2
aload 0
invokevirtual android/support/v4/view/ViewPager/getChildCount()I
if_icmpge L4
iload 2
istore 3
aload 0
iload 2
invokevirtual android/support/v4/view/ViewPager/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/view/es
getfield android/support/v4/view/es/a Z
ifne L5
aload 0
iload 2
invokevirtual android/support/v4/view/ViewPager/removeViewAt(I)V
iload 2
iconst_1
isub
istore 3
L5:
iload 3
iconst_1
iadd
istore 2
goto L3
L4:
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/u I
aload 0
iconst_0
iconst_0
invokevirtual android/support/v4/view/ViewPager/scrollTo(II)V
L0:
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
astore 5
aload 0
aload 1
putfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/n I
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
ifnull L6
aload 0
getfield android/support/v4/view/ViewPager/z Landroid/support/v4/view/ex;
ifnonnull L7
aload 0
new android/support/v4/view/ex
dup
aload 0
iconst_0
invokespecial android/support/v4/view/ex/<init>(Landroid/support/v4/view/ViewPager;B)V
putfield android/support/v4/view/ViewPager/z Landroid/support/v4/view/ex;
L7:
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
aload 0
getfield android/support/v4/view/ViewPager/z Landroid/support/v4/view/ex;
invokevirtual android/support/v4/view/by/a(Landroid/database/DataSetObserver;)V
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/K Z
aload 0
getfield android/support/v4/view/ViewPager/ak Z
istore 4
aload 0
iconst_1
putfield android/support/v4/view/ViewPager/ak Z
aload 0
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
putfield android/support/v4/view/ViewPager/n I
aload 0
getfield android/support/v4/view/ViewPager/v I
iflt L8
aload 0
getfield android/support/v4/view/ViewPager/t Landroid/support/v4/view/by;
aload 0
getfield android/support/v4/view/ViewPager/w Landroid/os/Parcelable;
aload 0
getfield android/support/v4/view/ViewPager/x Ljava/lang/ClassLoader;
invokevirtual android/support/v4/view/by/a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
aload 0
aload 0
getfield android/support/v4/view/ViewPager/v I
iconst_0
iconst_1
invokespecial android/support/v4/view/ViewPager/a(IZZ)V
aload 0
iconst_m1
putfield android/support/v4/view/ViewPager/v I
aload 0
aconst_null
putfield android/support/v4/view/ViewPager/w Landroid/os/Parcelable;
aload 0
aconst_null
putfield android/support/v4/view/ViewPager/x Ljava/lang/ClassLoader;
L6:
aload 0
getfield android/support/v4/view/ViewPager/aq Landroid/support/v4/view/eu;
ifnull L9
aload 5
aload 1
if_acmpeq L9
aload 0
getfield android/support/v4/view/ViewPager/aq Landroid/support/v4/view/eu;
aload 5
aload 1
invokeinterface android/support/v4/view/eu/a(Landroid/support/v4/view/by;Landroid/support/v4/view/by;)V 2
L9:
return
L8:
iload 4
ifne L10
aload 0
invokevirtual android/support/v4/view/ViewPager/b()V
goto L6
L10:
aload 0
invokevirtual android/support/v4/view/ViewPager/requestLayout()V
goto L6
.limit locals 6
.limit stack 5
.end method

.method setChildrenDrawingOrderEnabledCompat(Z)V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
getstatic android/os/Build$VERSION/SDK_INT I
bipush 7
if_icmplt L3
aload 0
getfield android/support/v4/view/ViewPager/as Ljava/lang/reflect/Method;
ifnonnull L1
L0:
aload 0
ldc android/view/ViewGroup
ldc "setChildrenDrawingOrderEnabled"
iconst_1
anewarray java/lang/Class
dup
iconst_0
getstatic java/lang/Boolean/TYPE Ljava/lang/Class;
aastore
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v4/view/ViewPager/as Ljava/lang/reflect/Method;
L1:
aload 0
getfield android/support/v4/view/ViewPager/as Ljava/lang/reflect/Method;
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L3:
return
L2:
astore 2
ldc "ViewPager"
ldc "Can't find setChildrenDrawingOrderEnabled"
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L4:
astore 2
ldc "ViewPager"
ldc "Error changing children drawing order"
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 3
.limit stack 7
.end method

.method public setCurrentItem(I)V
aload 0
iconst_0
putfield android/support/v4/view/ViewPager/K Z
aload 0
getfield android/support/v4/view/ViewPager/ak Z
ifne L0
iconst_1
istore 2
L1:
aload 0
iload 1
iload 2
iconst_0
invokespecial android/support/v4/view/ViewPager/a(IZZ)V
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 4
.end method

.method public setOffscreenPageLimit(I)V
iload 1
istore 2
iload 1
ifgt L0
ldc "ViewPager"
new java/lang/StringBuilder
dup
ldc "Requested offscreen page limit "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " too small; defaulting to 1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_1
istore 2
L0:
iload 2
aload 0
getfield android/support/v4/view/ViewPager/L I
if_icmpeq L1
aload 0
iload 2
putfield android/support/v4/view/ViewPager/L I
aload 0
invokevirtual android/support/v4/view/ViewPager/b()V
L1:
return
.limit locals 3
.limit stack 4
.end method

.method setOnAdapterChangeListener(Landroid/support/v4/view/eu;)V
aload 0
aload 1
putfield android/support/v4/view/ViewPager/aq Landroid/support/v4/view/eu;
return
.limit locals 2
.limit stack 2
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ev;)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
aload 1
putfield android/support/v4/view/ViewPager/ao Landroid/support/v4/view/ev;
return
.limit locals 2
.limit stack 2
.end method

.method public setPageMargin(I)V
aload 0
getfield android/support/v4/view/ViewPager/A I
istore 2
aload 0
iload 1
putfield android/support/v4/view/ViewPager/A I
aload 0
invokevirtual android/support/v4/view/ViewPager/getWidth()I
istore 3
aload 0
iload 3
iload 3
iload 1
iload 2
invokespecial android/support/v4/view/ViewPager/a(IIII)V
aload 0
invokevirtual android/support/v4/view/ViewPager/requestLayout()V
return
.limit locals 4
.limit stack 5
.end method

.method public setPageMarginDrawable(I)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
aload 0
aload 0
invokevirtual android/support/v4/view/ViewPager/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v4/view/ViewPager/setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 3
.end method

.method public setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/v4/view/ViewPager/B Landroid/graphics/drawable/Drawable;
aload 1
ifnull L0
aload 0
invokevirtual android/support/v4/view/ViewPager/refreshDrawableState()V
L0:
aload 1
ifnonnull L1
iconst_1
istore 2
L2:
aload 0
iload 2
invokevirtual android/support/v4/view/ViewPager/setWillNotDraw(Z)V
aload 0
invokevirtual android/support/v4/view/ViewPager/invalidate()V
return
L1:
iconst_0
istore 2
goto L2
.limit locals 3
.limit stack 2
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
aload 0
aload 1
invokespecial android/view/ViewGroup/verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
ifne L0
aload 1
aload 0
getfield android/support/v4/view/ViewPager/B Landroid/graphics/drawable/Drawable;
if_acmpne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method
