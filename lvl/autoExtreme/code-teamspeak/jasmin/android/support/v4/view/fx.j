.bytecode 50.0
.class final synchronized android/support/v4/view/fx
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/View;)V
aload 0
invokevirtual android/view/View/animate()Landroid/view/ViewPropertyAnimator;
invokevirtual android/view/ViewPropertyAnimator/withLayer()Landroid/view/ViewPropertyAnimator;
pop
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/View;Landroid/support/v4/view/gd;)V
aload 1
ifnull L0
aload 0
invokevirtual android/view/View/animate()Landroid/view/ViewPropertyAnimator;
new android/support/v4/view/fy
dup
aload 1
aload 0
invokespecial android/support/v4/view/fy/<init>(Landroid/support/v4/view/gd;Landroid/view/View;)V
invokevirtual android/view/ViewPropertyAnimator/setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;
pop
return
L0:
aload 0
invokevirtual android/view/View/animate()Landroid/view/ViewPropertyAnimator;
aconst_null
invokevirtual android/view/ViewPropertyAnimator/setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;
pop
return
.limit locals 2
.limit stack 5
.end method

.method private static a(Landroid/view/View;Ljava/lang/Runnable;)V
aload 0
invokevirtual android/view/View/animate()Landroid/view/ViewPropertyAnimator;
aload 1
invokevirtual android/view/ViewPropertyAnimator/withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;
pop
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/view/View;Ljava/lang/Runnable;)V
aload 0
invokevirtual android/view/View/animate()Landroid/view/ViewPropertyAnimator;
aload 1
invokevirtual android/view/ViewPropertyAnimator/withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;
pop
return
.limit locals 2
.limit stack 2
.end method
