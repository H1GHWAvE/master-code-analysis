.bytecode 50.0
.class synchronized android/support/v4/e/a/r
.super android/graphics/drawable/Drawable
.implements android/graphics/drawable/Drawable$Callback
.implements android/support/v4/e/a/q

.field static final 'a' Landroid/graphics/PorterDuff$Mode;

.field 'b' Landroid/graphics/drawable/Drawable;

.field private 'c' Landroid/content/res/ColorStateList;

.field private 'd' Landroid/graphics/PorterDuff$Mode;

.field private 'e' I

.field private 'f' Landroid/graphics/PorterDuff$Mode;

.field private 'g' Z

.method static <clinit>()V
getstatic android/graphics/PorterDuff$Mode/SRC_IN Landroid/graphics/PorterDuff$Mode;
putstatic android/support/v4/e/a/r/a Landroid/graphics/PorterDuff$Mode;
return
.limit locals 0
.limit stack 1
.end method

.method <init>(Landroid/graphics/drawable/Drawable;)V
aload 0
invokespecial android/graphics/drawable/Drawable/<init>()V
aload 0
getstatic android/support/v4/e/a/r/a Landroid/graphics/PorterDuff$Mode;
putfield android/support/v4/e/a/r/d Landroid/graphics/PorterDuff$Mode;
aload 0
aload 1
invokevirtual android/support/v4/e/a/r/a(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a([I)Z
aload 0
getfield android/support/v4/e/a/r/c Landroid/content/res/ColorStateList;
ifnull L0
aload 0
getfield android/support/v4/e/a/r/d Landroid/graphics/PorterDuff$Mode;
ifnull L0
aload 0
getfield android/support/v4/e/a/r/c Landroid/content/res/ColorStateList;
aload 1
aload 0
getfield android/support/v4/e/a/r/c Landroid/content/res/ColorStateList;
invokevirtual android/content/res/ColorStateList/getDefaultColor()I
invokevirtual android/content/res/ColorStateList/getColorForState([II)I
istore 2
aload 0
getfield android/support/v4/e/a/r/d Landroid/graphics/PorterDuff$Mode;
astore 1
aload 0
getfield android/support/v4/e/a/r/g Z
ifeq L1
iload 2
aload 0
getfield android/support/v4/e/a/r/e I
if_icmpne L1
aload 1
aload 0
getfield android/support/v4/e/a/r/f Landroid/graphics/PorterDuff$Mode;
if_acmpeq L2
L1:
aload 0
iload 2
aload 1
invokevirtual android/support/v4/e/a/r/setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
aload 0
iload 2
putfield android/support/v4/e/a/r/e I
aload 0
aload 1
putfield android/support/v4/e/a/r/f Landroid/graphics/PorterDuff$Mode;
aload 0
iconst_1
putfield android/support/v4/e/a/r/g Z
iconst_1
ireturn
L0:
aload 0
iconst_0
putfield android/support/v4/e/a/r/g Z
aload 0
invokevirtual android/support/v4/e/a/r/clearColorFilter()V
L2:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final a()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
aconst_null
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
L0:
aload 0
aload 1
putfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
aload 1
ifnull L1
aload 1
aload 0
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
L1:
aload 0
invokevirtual android/support/v4/e/a/r/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method

.method public draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
return
.limit locals 2
.limit stack 2
.end method

.method public getChangingConfigurations()I
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getChangingConfigurations()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getCurrent()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getCurrent()Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getIntrinsicHeight()I
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getIntrinsicWidth()I
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMinimumHeight()I
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getMinimumHeight()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMinimumWidth()I
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getMinimumWidth()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOpacity()I
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getOpacity()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getState()[I
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getState()[I
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTransparentRegion()Landroid/graphics/Region;
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getTransparentRegion()Landroid/graphics/Region;
areturn
.limit locals 1
.limit stack 1
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
invokevirtual android/support/v4/e/a/r/invalidateSelf()V
return
.limit locals 2
.limit stack 1
.end method

.method public isStateful()Z
aload 0
getfield android/support/v4/e/a/r/c Landroid/content/res/ColorStateList;
ifnull L0
aload 0
getfield android/support/v4/e/a/r/c Landroid/content/res/ColorStateList;
invokevirtual android/content/res/ColorStateList/isStateful()Z
ifne L1
L0:
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/isStateful()Z
ifeq L2
L1:
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
astore 1
aload 1
invokevirtual android/graphics/drawable/Drawable/mutate()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
aload 1
if_acmpeq L0
aload 0
aload 2
invokevirtual android/support/v4/e/a/r/a(Landroid/graphics/drawable/Drawable;)V
L0:
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/setBounds(Landroid/graphics/Rect;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected onLevelChange(I)Z
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setLevel(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
aload 0
aload 2
lload 3
invokevirtual android/support/v4/e/a/r/scheduleSelf(Ljava/lang/Runnable;J)V
return
.limit locals 5
.limit stack 4
.end method

.method public setAlpha(I)V
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setChangingConfigurations(I)V
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setChangingConfigurations(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/setColorFilter(Landroid/graphics/ColorFilter;)V
return
.limit locals 2
.limit stack 2
.end method

.method public setDither(Z)V
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setDither(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public setFilterBitmap(Z)V
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setFilterBitmap(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public setState([I)Z
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
istore 2
aload 0
aload 1
invokespecial android/support/v4/e/a/r/a([I)Z
ifne L0
iload 2
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public setTint(I)V
aload 0
iload 1
invokestatic android/content/res/ColorStateList/valueOf(I)Landroid/content/res/ColorStateList;
invokevirtual android/support/v4/e/a/r/setTintList(Landroid/content/res/ColorStateList;)V
return
.limit locals 2
.limit stack 2
.end method

.method public setTintList(Landroid/content/res/ColorStateList;)V
aload 0
aload 1
putfield android/support/v4/e/a/r/c Landroid/content/res/ColorStateList;
aload 0
aload 0
invokevirtual android/support/v4/e/a/r/getState()[I
invokespecial android/support/v4/e/a/r/a([I)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method public setTintMode(Landroid/graphics/PorterDuff$Mode;)V
aload 0
aload 1
putfield android/support/v4/e/a/r/d Landroid/graphics/PorterDuff$Mode;
aload 0
aload 0
invokevirtual android/support/v4/e/a/r/getState()[I
invokespecial android/support/v4/e/a/r/a([I)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method public setVisible(ZZ)Z
aload 0
iload 1
iload 2
invokespecial android/graphics/drawable/Drawable/setVisible(ZZ)Z
ifne L0
aload 0
getfield android/support/v4/e/a/r/b Landroid/graphics/drawable/Drawable;
iload 1
iload 2
invokevirtual android/graphics/drawable/Drawable/setVisible(ZZ)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
aload 0
aload 2
invokevirtual android/support/v4/e/a/r/unscheduleSelf(Ljava/lang/Runnable;)V
return
.limit locals 3
.limit stack 2
.end method
