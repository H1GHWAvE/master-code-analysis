.bytecode 50.0
.class final synchronized android/support/v4/f/b/b
.super java/lang/Object
.implements android/support/v4/f/b/g

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/support/v4/f/b/m;)Landroid/support/v4/f/b/f;
aload 0
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 0
getfield android/support/v4/f/b/m/b Ljavax/crypto/Cipher;
ifnull L2
new android/support/v4/f/b/f
dup
aload 0
getfield android/support/v4/f/b/m/b Ljavax/crypto/Cipher;
invokespecial android/support/v4/f/b/f/<init>(Ljavax/crypto/Cipher;)V
areturn
L2:
aload 0
getfield android/support/v4/f/b/m/a Ljava/security/Signature;
ifnull L3
new android/support/v4/f/b/f
dup
aload 0
getfield android/support/v4/f/b/m/a Ljava/security/Signature;
invokespecial android/support/v4/f/b/f/<init>(Ljava/security/Signature;)V
areturn
L3:
aload 0
getfield android/support/v4/f/b/m/c Ljavax/crypto/Mac;
ifnull L1
new android/support/v4/f/b/f
dup
aload 0
getfield android/support/v4/f/b/m/c Ljavax/crypto/Mac;
invokespecial android/support/v4/f/b/f/<init>(Ljavax/crypto/Mac;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Landroid/support/v4/f/b/d;)Landroid/support/v4/f/b/k;
new android/support/v4/f/b/c
dup
aload 0
invokespecial android/support/v4/f/b/c/<init>(Landroid/support/v4/f/b/d;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Landroid/support/v4/f/b/f;)Landroid/support/v4/f/b/m;
aload 0
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 0
getfield android/support/v4/f/b/f/b Ljavax/crypto/Cipher;
ifnull L2
new android/support/v4/f/b/m
dup
aload 0
getfield android/support/v4/f/b/f/b Ljavax/crypto/Cipher;
invokespecial android/support/v4/f/b/m/<init>(Ljavax/crypto/Cipher;)V
areturn
L2:
aload 0
getfield android/support/v4/f/b/f/a Ljava/security/Signature;
ifnull L3
new android/support/v4/f/b/m
dup
aload 0
getfield android/support/v4/f/b/f/a Ljava/security/Signature;
invokespecial android/support/v4/f/b/m/<init>(Ljava/security/Signature;)V
areturn
L3:
aload 0
getfield android/support/v4/f/b/f/c Ljavax/crypto/Mac;
ifnull L1
new android/support/v4/f/b/m
dup
aload 0
getfield android/support/v4/f/b/f/c Ljavax/crypto/Mac;
invokespecial android/support/v4/f/b/m/<init>(Ljavax/crypto/Mac;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static synthetic b(Landroid/support/v4/f/b/m;)Landroid/support/v4/f/b/f;
aload 0
ifnull L0
aload 0
getfield android/support/v4/f/b/m/b Ljavax/crypto/Cipher;
ifnull L1
new android/support/v4/f/b/f
dup
aload 0
getfield android/support/v4/f/b/m/b Ljavax/crypto/Cipher;
invokespecial android/support/v4/f/b/f/<init>(Ljavax/crypto/Cipher;)V
areturn
L1:
aload 0
getfield android/support/v4/f/b/m/a Ljava/security/Signature;
ifnull L2
new android/support/v4/f/b/f
dup
aload 0
getfield android/support/v4/f/b/m/a Ljava/security/Signature;
invokespecial android/support/v4/f/b/f/<init>(Ljava/security/Signature;)V
areturn
L2:
aload 0
getfield android/support/v4/f/b/m/c Ljavax/crypto/Mac;
ifnull L0
new android/support/v4/f/b/f
dup
aload 0
getfield android/support/v4/f/b/m/c Ljavax/crypto/Mac;
invokespecial android/support/v4/f/b/f/<init>(Ljavax/crypto/Mac;)V
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 3
.end method

.method public final a(Landroid/content/Context;Landroid/support/v4/f/b/f;ILandroid/support/v4/i/c;Landroid/support/v4/f/b/d;Landroid/os/Handler;)V
aconst_null
astore 7
aload 2
ifnull L0
aload 2
getfield android/support/v4/f/b/f/b Ljavax/crypto/Cipher;
ifnull L1
new android/support/v4/f/b/m
dup
aload 2
getfield android/support/v4/f/b/f/b Ljavax/crypto/Cipher;
invokespecial android/support/v4/f/b/m/<init>(Ljavax/crypto/Cipher;)V
astore 2
L2:
aload 4
ifnull L3
aload 4
invokevirtual android/support/v4/i/c/b()Ljava/lang/Object;
astore 4
L4:
new android/support/v4/f/b/c
dup
aload 5
invokespecial android/support/v4/f/b/c/<init>(Landroid/support/v4/f/b/d;)V
astore 5
aload 1
invokestatic android/support/v4/f/b/i/a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;
astore 8
aload 7
astore 1
aload 2
ifnull L5
aload 2
getfield android/support/v4/f/b/m/b Ljavax/crypto/Cipher;
ifnull L6
new android/hardware/fingerprint/FingerprintManager$CryptoObject
dup
aload 2
getfield android/support/v4/f/b/m/b Ljavax/crypto/Cipher;
invokespecial android/hardware/fingerprint/FingerprintManager$CryptoObject/<init>(Ljavax/crypto/Cipher;)V
astore 1
L5:
aload 8
aload 1
aload 4
checkcast android/os/CancellationSignal
iload 3
new android/support/v4/f/b/j
dup
aload 5
invokespecial android/support/v4/f/b/j/<init>(Landroid/support/v4/f/b/k;)V
aload 6
invokevirtual android/hardware/fingerprint/FingerprintManager/authenticate(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;Landroid/os/CancellationSignal;ILandroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;Landroid/os/Handler;)V
return
L1:
aload 2
getfield android/support/v4/f/b/f/a Ljava/security/Signature;
ifnull L7
new android/support/v4/f/b/m
dup
aload 2
getfield android/support/v4/f/b/f/a Ljava/security/Signature;
invokespecial android/support/v4/f/b/m/<init>(Ljava/security/Signature;)V
astore 2
goto L2
L7:
aload 2
getfield android/support/v4/f/b/f/c Ljavax/crypto/Mac;
ifnull L0
new android/support/v4/f/b/m
dup
aload 2
getfield android/support/v4/f/b/f/c Ljavax/crypto/Mac;
invokespecial android/support/v4/f/b/m/<init>(Ljavax/crypto/Mac;)V
astore 2
goto L2
L0:
aconst_null
astore 2
goto L2
L3:
aconst_null
astore 4
goto L4
L6:
aload 2
getfield android/support/v4/f/b/m/a Ljava/security/Signature;
ifnull L8
new android/hardware/fingerprint/FingerprintManager$CryptoObject
dup
aload 2
getfield android/support/v4/f/b/m/a Ljava/security/Signature;
invokespecial android/hardware/fingerprint/FingerprintManager$CryptoObject/<init>(Ljava/security/Signature;)V
astore 1
goto L5
L8:
aload 7
astore 1
aload 2
getfield android/support/v4/f/b/m/c Ljavax/crypto/Mac;
ifnull L5
new android/hardware/fingerprint/FingerprintManager$CryptoObject
dup
aload 2
getfield android/support/v4/f/b/m/c Ljavax/crypto/Mac;
invokespecial android/hardware/fingerprint/FingerprintManager$CryptoObject/<init>(Ljavax/crypto/Mac;)V
astore 1
goto L5
.limit locals 9
.limit stack 7
.end method

.method public final a(Landroid/content/Context;)Z
aload 1
invokestatic android/support/v4/f/b/i/a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;
invokevirtual android/hardware/fingerprint/FingerprintManager/hasEnrolledFingerprints()Z
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final b(Landroid/content/Context;)Z
aload 1
invokestatic android/support/v4/f/b/i/a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;
invokevirtual android/hardware/fingerprint/FingerprintManager/isHardwareDetected()Z
ireturn
.limit locals 2
.limit stack 1
.end method
