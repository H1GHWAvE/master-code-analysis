.bytecode 50.0
.class public synchronized android/support/v7/widget/SearchView$SearchAutoComplete
.super android/support/v7/widget/p

.field private 'a' I

.field private 'b' Landroid/support/v7/widget/SearchView;

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/widget/SearchView$SearchAutoComplete/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
getstatic android/support/v7/a/d/autoCompleteTextViewStyle I
invokespecial android/support/v7/widget/SearchView$SearchAutoComplete/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/support/v7/widget/p/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 0
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getThreshold()I
putfield android/support/v7/widget/SearchView$SearchAutoComplete/a I
return
.limit locals 4
.limit stack 4
.end method

.method private a()Z
aload 0
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
invokestatic android/text/TextUtils/getTrimmedLength(Ljava/lang/CharSequence;)I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/support/v7/widget/SearchView$SearchAutoComplete;)Z
aload 0
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
invokestatic android/text/TextUtils/getTrimmedLength(Ljava/lang/CharSequence;)I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public enoughToFilter()Z
aload 0
getfield android/support/v7/widget/SearchView$SearchAutoComplete/a I
ifle L0
aload 0
invokespecial android/support/v7/widget/p/enoughToFilter()Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
aload 0
iload 1
iload 2
aload 3
invokespecial android/support/v7/widget/p/onFocusChanged(ZILandroid/graphics/Rect;)V
aload 0
getfield android/support/v7/widget/SearchView$SearchAutoComplete/b Landroid/support/v7/widget/SearchView;
invokevirtual android/support/v7/widget/SearchView/d()V
return
.limit locals 4
.limit stack 4
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
iload 1
iconst_4
if_icmpne L0
aload 2
invokevirtual android/view/KeyEvent/getAction()I
ifne L1
aload 2
invokevirtual android/view/KeyEvent/getRepeatCount()I
ifne L1
aload 0
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;
astore 3
aload 3
ifnull L2
aload 3
aload 2
aload 0
invokevirtual android/view/KeyEvent$DispatcherState/startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V
L2:
iconst_1
ireturn
L1:
aload 2
invokevirtual android/view/KeyEvent/getAction()I
iconst_1
if_icmpne L0
aload 0
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;
astore 3
aload 3
ifnull L3
aload 3
aload 2
invokevirtual android/view/KeyEvent$DispatcherState/handleUpEvent(Landroid/view/KeyEvent;)V
L3:
aload 2
invokevirtual android/view/KeyEvent/isTracking()Z
ifeq L0
aload 2
invokevirtual android/view/KeyEvent/isCanceled()Z
ifne L0
aload 0
getfield android/support/v7/widget/SearchView$SearchAutoComplete/b Landroid/support/v7/widget/SearchView;
invokevirtual android/support/v7/widget/SearchView/clearFocus()V
aload 0
getfield android/support/v7/widget/SearchView$SearchAutoComplete/b Landroid/support/v7/widget/SearchView;
invokestatic android/support/v7/widget/SearchView/p(Landroid/support/v7/widget/SearchView;)V
iconst_1
ireturn
L0:
aload 0
iload 1
aload 2
invokespecial android/support/v7/widget/p/onKeyPreIme(ILandroid/view/KeyEvent;)Z
ireturn
.limit locals 4
.limit stack 3
.end method

.method public onWindowFocusChanged(Z)V
aload 0
iload 1
invokespecial android/support/v7/widget/p/onWindowFocusChanged(Z)V
iload 1
ifeq L0
aload 0
getfield android/support/v7/widget/SearchView$SearchAutoComplete/b Landroid/support/v7/widget/SearchView;
invokevirtual android/support/v7/widget/SearchView/hasFocus()Z
ifeq L0
aload 0
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getVisibility()I
ifne L0
aload 0
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getContext()Landroid/content/Context;
ldc "input_method"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/inputmethod/InputMethodManager
aload 0
iconst_0
invokevirtual android/view/inputmethod/InputMethodManager/showSoftInput(Landroid/view/View;I)Z
pop
aload 0
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getContext()Landroid/content/Context;
invokestatic android/support/v7/widget/SearchView/a(Landroid/content/Context;)Z
ifeq L0
getstatic android/support/v7/widget/SearchView/a Landroid/support/v7/widget/bq;
aload 0
invokevirtual android/support/v7/widget/bq/a(Landroid/widget/AutoCompleteTextView;)V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public performCompletion()V
return
.limit locals 1
.limit stack 0
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 0
.end method

.method setSearchView(Landroid/support/v7/widget/SearchView;)V
aload 0
aload 1
putfield android/support/v7/widget/SearchView$SearchAutoComplete/b Landroid/support/v7/widget/SearchView;
return
.limit locals 2
.limit stack 2
.end method

.method public setThreshold(I)V
aload 0
iload 1
invokespecial android/support/v7/widget/p/setThreshold(I)V
aload 0
iload 1
putfield android/support/v7/widget/SearchView$SearchAutoComplete/a I
return
.limit locals 2
.limit stack 2
.end method
