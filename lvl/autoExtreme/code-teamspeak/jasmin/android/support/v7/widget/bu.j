.bytecode 50.0
.class public final synchronized android/support/v7/widget/bu
.super android/support/v4/view/n

.field public static final 'c' Ljava/lang/String; = "share_history.xml"

.field private static final 'f' I = 4


.field final 'd' Landroid/content/Context;

.field 'e' Ljava/lang/String;

.field private 'g' I

.field private final 'h' Landroid/support/v7/widget/by;

.field private 'i' Landroid/support/v7/widget/bw;

.field private 'j' Landroid/support/v7/internal/widget/s;

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/support/v4/view/n/<init>(Landroid/content/Context;)V
aload 0
iconst_4
putfield android/support/v7/widget/bu/g I
aload 0
new android/support/v7/widget/by
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/by/<init>(Landroid/support/v7/widget/bu;B)V
putfield android/support/v7/widget/bu/h Landroid/support/v7/widget/by;
aload 0
ldc "share_history.xml"
putfield android/support/v7/widget/bu/e Ljava/lang/String;
aload 0
aload 1
putfield android/support/v7/widget/bu/d Landroid/content/Context;
return
.limit locals 2
.limit stack 5
.end method

.method private static synthetic a(Landroid/support/v7/widget/bu;)Landroid/content/Context;
aload 0
getfield android/support/v7/widget/bu/d Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method static a(Landroid/content/Intent;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
ldc_w 134742016
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
return
L0:
aload 0
ldc_w 524288
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
return
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/support/v7/widget/bw;)V
aload 0
aload 1
putfield android/support/v7/widget/bu/i Landroid/support/v7/widget/bw;
aload 0
invokespecial android/support/v7/widget/bu/g()V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;)V
aload 0
aload 1
putfield android/support/v7/widget/bu/e Ljava/lang/String;
aload 0
invokespecial android/support/v7/widget/bu/g()V
return
.limit locals 2
.limit stack 2
.end method

.method private static synthetic b(Landroid/support/v7/widget/bu;)Ljava/lang/String;
aload 0
getfield android/support/v7/widget/bu/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Landroid/content/Intent;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aload 1
ifnull L7
aload 1
invokevirtual android/content/Intent/getAction()Ljava/lang/String;
astore 2
ldc "android.intent.action.SEND"
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L8
ldc "android.intent.action.SEND_MULTIPLE"
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
L8:
aload 1
invokestatic android/support/v7/widget/bu/a(Landroid/content/Intent;)V
L7:
aload 0
getfield android/support/v7/widget/bu/d Landroid/content/Context;
aload 0
getfield android/support/v7/widget/bu/e Ljava/lang/String;
invokestatic android/support/v7/internal/widget/l/a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/l;
astore 3
aload 3
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 3
getfield android/support/v7/internal/widget/l/e Landroid/content/Intent;
aload 1
if_acmpne L3
aload 2
monitorexit
L1:
return
L3:
aload 3
aload 1
putfield android/support/v7/internal/widget/l/e Landroid/content/Intent;
aload 3
iconst_1
putfield android/support/v7/internal/widget/l/f Z
aload 3
invokevirtual android/support/v7/internal/widget/l/d()V
aload 2
monitorexit
L4:
return
L2:
astore 1
L5:
aload 2
monitorexit
L6:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method private static synthetic c(Landroid/support/v7/widget/bu;)Landroid/support/v7/widget/bw;
aload 0
getfield android/support/v7/widget/bu/i Landroid/support/v7/widget/bw;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic c(Landroid/content/Intent;)V
aload 0
invokestatic android/support/v7/widget/bu/a(Landroid/content/Intent;)V
return
.limit locals 1
.limit stack 1
.end method

.method private g()V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield android/support/v7/widget/bu/i Landroid/support/v7/widget/bw;
ifnonnull L5
return
L5:
aload 0
getfield android/support/v7/widget/bu/j Landroid/support/v7/internal/widget/s;
ifnonnull L6
aload 0
new android/support/v7/widget/bx
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/bx/<init>(Landroid/support/v7/widget/bu;B)V
putfield android/support/v7/widget/bu/j Landroid/support/v7/internal/widget/s;
L6:
aload 0
getfield android/support/v7/widget/bu/d Landroid/content/Context;
aload 0
getfield android/support/v7/widget/bu/e Ljava/lang/String;
invokestatic android/support/v7/internal/widget/l/a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/l;
astore 2
aload 0
getfield android/support/v7/widget/bu/j Landroid/support/v7/internal/widget/s;
astore 3
aload 2
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 2
aload 3
putfield android/support/v7/internal/widget/l/g Landroid/support/v7/internal/widget/s;
aload 1
monitorexit
L1:
return
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 4
.limit stack 5
.end method

.method public final a()Landroid/view/View;
new android/support/v7/internal/widget/ActivityChooserView
dup
aload 0
getfield android/support/v7/widget/bu/d Landroid/content/Context;
invokespecial android/support/v7/internal/widget/ActivityChooserView/<init>(Landroid/content/Context;)V
astore 1
aload 1
invokevirtual android/support/v7/internal/widget/ActivityChooserView/isInEditMode()Z
ifne L0
aload 1
aload 0
getfield android/support/v7/widget/bu/d Landroid/content/Context;
aload 0
getfield android/support/v7/widget/bu/e Ljava/lang/String;
invokestatic android/support/v7/internal/widget/l/a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/setActivityChooserModel(Landroid/support/v7/internal/widget/l;)V
L0:
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 2
aload 0
getfield android/support/v7/widget/bu/d Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
getstatic android/support/v7/a/d/actionModeShareDrawable I
aload 2
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
pop
aload 1
aload 0
getfield android/support/v7/widget/bu/d Landroid/content/Context;
aload 2
getfield android/util/TypedValue/resourceId I
invokestatic android/support/v7/internal/widget/av/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/setExpandActivityOverflowButtonDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/setProvider(Landroid/support/v4/view/n;)V
aload 1
getstatic android/support/v7/a/l/abc_shareactionprovider_share_with_application I
invokevirtual android/support/v7/internal/widget/ActivityChooserView/setDefaultActionButtonContentDescription(I)V
aload 1
getstatic android/support/v7/a/l/abc_shareactionprovider_share_with I
invokevirtual android/support/v7/internal/widget/ActivityChooserView/setExpandActivityOverflowButtonContentDescription(I)V
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public final a(Landroid/view/SubMenu;)V
aload 1
invokeinterface android/view/SubMenu/clear()V 0
aload 0
getfield android/support/v7/widget/bu/d Landroid/content/Context;
aload 0
getfield android/support/v7/widget/bu/e Ljava/lang/String;
invokestatic android/support/v7/internal/widget/l/a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/l;
astore 5
aload 0
getfield android/support/v7/widget/bu/d Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
astore 6
aload 5
invokevirtual android/support/v7/internal/widget/l/a()I
istore 3
iload 3
aload 0
getfield android/support/v7/widget/bu/g I
invokestatic java/lang/Math/min(II)I
istore 4
iconst_0
istore 2
L0:
iload 2
iload 4
if_icmpge L1
aload 5
iload 2
invokevirtual android/support/v7/internal/widget/l/a(I)Landroid/content/pm/ResolveInfo;
astore 7
aload 1
iconst_0
iload 2
iload 2
aload 7
aload 6
invokevirtual android/content/pm/ResolveInfo/loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
invokeinterface android/view/SubMenu/add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem; 4
aload 7
aload 6
invokevirtual android/content/pm/ResolveInfo/loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
invokeinterface android/view/MenuItem/setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem; 1
aload 0
getfield android/support/v7/widget/bu/h Landroid/support/v7/widget/by;
invokeinterface android/view/MenuItem/setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem; 1
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iload 4
iload 3
if_icmpge L2
aload 1
iconst_0
iload 4
iload 4
aload 0
getfield android/support/v7/widget/bu/d Landroid/content/Context;
getstatic android/support/v7/a/l/abc_activity_chooser_view_see_all I
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokeinterface android/view/SubMenu/addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu; 4
astore 1
iconst_0
istore 2
L3:
iload 2
iload 3
if_icmpge L2
aload 5
iload 2
invokevirtual android/support/v7/internal/widget/l/a(I)Landroid/content/pm/ResolveInfo;
astore 7
aload 1
iconst_0
iload 2
iload 2
aload 7
aload 6
invokevirtual android/content/pm/ResolveInfo/loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
invokeinterface android/view/SubMenu/add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem; 4
aload 7
aload 6
invokevirtual android/content/pm/ResolveInfo/loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
invokeinterface android/view/MenuItem/setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem; 1
aload 0
getfield android/support/v7/widget/bu/h Landroid/support/v7/widget/by;
invokeinterface android/view/MenuItem/setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem; 1
pop
iload 2
iconst_1
iadd
istore 2
goto L3
L2:
return
.limit locals 8
.limit stack 6
.end method

.method public final f()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method
