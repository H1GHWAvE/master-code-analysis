.bytecode 50.0
.class public final synchronized android/support/v7/widget/ba
.super java/lang/Object
.implements android/support/v7/internal/view/menu/j
.implements android/support/v7/internal/view/menu/y

.field 'a' Landroid/support/v7/internal/view/menu/v;

.field private 'b' Landroid/content/Context;

.field private 'c' Landroid/support/v7/internal/view/menu/i;

.field private 'd' Landroid/view/View;

.field private 'e' Landroid/support/v7/widget/bd;

.field private 'f' Landroid/support/v7/widget/bc;

.field private 'g' Landroid/view/View$OnTouchListener;

.method private <init>(Landroid/content/Context;Landroid/view/View;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v7/widget/ba/<init>(Landroid/content/Context;Landroid/view/View;B)V
return
.limit locals 3
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;Landroid/view/View;B)V
aload 0
aload 1
aload 2
getstatic android/support/v7/a/d/popupMenuStyle I
invokespecial android/support/v7/widget/ba/<init>(Landroid/content/Context;Landroid/view/View;I)V
return
.limit locals 4
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;Landroid/view/View;I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v7/widget/ba/b Landroid/content/Context;
aload 0
new android/support/v7/internal/view/menu/i
dup
aload 1
invokespecial android/support/v7/internal/view/menu/i/<init>(Landroid/content/Context;)V
putfield android/support/v7/widget/ba/c Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/widget/ba/c Landroid/support/v7/internal/view/menu/i;
aload 0
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/j;)V
aload 0
aload 2
putfield android/support/v7/widget/ba/d Landroid/view/View;
aload 0
new android/support/v7/internal/view/menu/v
dup
aload 1
aload 0
getfield android/support/v7/widget/ba/c Landroid/support/v7/internal/view/menu/i;
aload 2
iconst_0
iload 3
iconst_0
invokespecial android/support/v7/internal/view/menu/v/<init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;ZIB)V
putfield android/support/v7/widget/ba/a Landroid/support/v7/internal/view/menu/v;
aload 0
getfield android/support/v7/widget/ba/a Landroid/support/v7/internal/view/menu/v;
iconst_0
putfield android/support/v7/internal/view/menu/v/f I
aload 0
getfield android/support/v7/widget/ba/a Landroid/support/v7/internal/view/menu/v;
aload 0
putfield android/support/v7/internal/view/menu/v/d Landroid/support/v7/internal/view/menu/y;
return
.limit locals 4
.limit stack 9
.end method

.method private a()I
aload 0
getfield android/support/v7/widget/ba/a Landroid/support/v7/internal/view/menu/v;
getfield android/support/v7/internal/view/menu/v/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic a(Landroid/support/v7/widget/ba;)Landroid/support/v7/internal/view/menu/v;
aload 0
getfield android/support/v7/widget/ba/a Landroid/support/v7/internal/view/menu/v;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
getfield android/support/v7/widget/ba/a Landroid/support/v7/internal/view/menu/v;
iload 1
putfield android/support/v7/internal/view/menu/v/f I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v7/widget/bc;)V
aload 0
aload 1
putfield android/support/v7/widget/ba/f Landroid/support/v7/widget/bc;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v7/widget/bd;)V
aload 0
aload 1
putfield android/support/v7/widget/ba/e Landroid/support/v7/widget/bd;
return
.limit locals 2
.limit stack 2
.end method

.method private b()Landroid/view/View$OnTouchListener;
aload 0
getfield android/support/v7/widget/ba/g Landroid/view/View$OnTouchListener;
ifnonnull L0
aload 0
new android/support/v7/widget/bb
dup
aload 0
aload 0
getfield android/support/v7/widget/ba/d Landroid/view/View;
invokespecial android/support/v7/widget/bb/<init>(Landroid/support/v7/widget/ba;Landroid/view/View;)V
putfield android/support/v7/widget/ba/g Landroid/view/View$OnTouchListener;
L0:
aload 0
getfield android/support/v7/widget/ba/g Landroid/view/View$OnTouchListener;
areturn
.limit locals 1
.limit stack 5
.end method

.method private b(I)V
.annotation invisibleparam 1 Landroid/support/a/x;
.end annotation
new android/support/v7/internal/view/f
dup
aload 0
getfield android/support/v7/widget/ba/b Landroid/content/Context;
invokespecial android/support/v7/internal/view/f/<init>(Landroid/content/Context;)V
iload 1
aload 0
getfield android/support/v7/widget/ba/c Landroid/support/v7/internal/view/menu/i;
invokevirtual android/view/MenuInflater/inflate(ILandroid/view/Menu;)V
return
.limit locals 2
.limit stack 3
.end method

.method private c()Landroid/view/Menu;
aload 0
getfield android/support/v7/widget/ba/c Landroid/support/v7/internal/view/menu/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Landroid/view/MenuInflater;
new android/support/v7/internal/view/f
dup
aload 0
getfield android/support/v7/widget/ba/b Landroid/content/Context;
invokespecial android/support/v7/internal/view/f/<init>(Landroid/content/Context;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private e()V
aload 0
getfield android/support/v7/widget/ba/a Landroid/support/v7/internal/view/menu/v;
invokevirtual android/support/v7/internal/view/menu/v/d()V
return
.limit locals 1
.limit stack 1
.end method

.method private f()V
aload 0
getfield android/support/v7/widget/ba/a Landroid/support/v7/internal/view/menu/v;
invokevirtual android/support/v7/internal/view/menu/v/f()V
return
.limit locals 1
.limit stack 1
.end method

.method private static g()V
return
.limit locals 0
.limit stack 0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
return
.limit locals 3
.limit stack 0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
aload 0
getfield android/support/v7/widget/ba/e Landroid/support/v7/widget/bd;
ifnull L0
aload 0
getfield android/support/v7/widget/ba/e Landroid/support/v7/widget/bd;
invokeinterface android/support/v7/widget/bd/a()Z 0
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method

.method public final a_(Landroid/support/v7/internal/view/menu/i;)Z
iconst_1
istore 2
aload 1
ifnonnull L0
iconst_0
istore 2
L1:
iload 2
ireturn
L0:
aload 1
invokevirtual android/support/v7/internal/view/menu/i/hasVisibleItems()Z
ifeq L1
new android/support/v7/internal/view/menu/v
dup
aload 0
getfield android/support/v7/widget/ba/b Landroid/content/Context;
aload 1
aload 0
getfield android/support/v7/widget/ba/d Landroid/view/View;
invokespecial android/support/v7/internal/view/menu/v/<init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;)V
invokevirtual android/support/v7/internal/view/menu/v/d()V
iconst_1
ireturn
.limit locals 3
.limit stack 5
.end method
