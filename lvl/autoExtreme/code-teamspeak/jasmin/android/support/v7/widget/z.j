.bytecode 50.0
.class public final synchronized android/support/v7/widget/z
.super android/widget/RatingBar

.field private static final 'a' [I

.field private 'b' Landroid/graphics/Bitmap;

.method static <clinit>()V
iconst_2
newarray int
dup
iconst_0
ldc_w 16843067
iastore
dup
iconst_1
ldc_w 16843068
iastore
putstatic android/support/v7/widget/z/a [I
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/widget/z/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
getstatic android/support/v7/a/d/ratingBarStyle I
invokespecial android/support/v7/widget/z/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/widget/RatingBar/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
getstatic android/support/v7/internal/widget/av/a Z
ifeq L0
aload 0
invokevirtual android/support/v7/widget/z/getContext()Landroid/content/Context;
aload 2
getstatic android/support/v7/widget/z/a [I
iload 3
invokestatic android/support/v7/internal/widget/ax/a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
astore 1
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/ax/b(I)Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L1
aload 0
aload 0
aload 2
invokespecial android/support/v7/widget/z/a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/z/setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V
L1:
aload 1
iconst_1
invokevirtual android/support/v7/internal/widget/ax/b(I)Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L2
aload 0
aload 0
aload 2
iconst_0
invokespecial android/support/v7/widget/z/a(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/z/setProgressDrawable(Landroid/graphics/drawable/Drawable;)V
L2:
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
L0:
return
.limit locals 4
.limit stack 4
.end method

.method private a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
aload 1
astore 4
aload 1
instanceof android/graphics/drawable/AnimationDrawable
ifeq L0
aload 1
checkcast android/graphics/drawable/AnimationDrawable
astore 1
aload 1
invokevirtual android/graphics/drawable/AnimationDrawable/getNumberOfFrames()I
istore 3
new android/graphics/drawable/AnimationDrawable
dup
invokespecial android/graphics/drawable/AnimationDrawable/<init>()V
astore 4
aload 4
aload 1
invokevirtual android/graphics/drawable/AnimationDrawable/isOneShot()Z
invokevirtual android/graphics/drawable/AnimationDrawable/setOneShot(Z)V
iconst_0
istore 2
L1:
iload 2
iload 3
if_icmpge L2
aload 0
aload 1
iload 2
invokevirtual android/graphics/drawable/AnimationDrawable/getFrame(I)Landroid/graphics/drawable/Drawable;
iconst_1
invokespecial android/support/v7/widget/z/a(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;
astore 5
aload 5
sipush 10000
invokevirtual android/graphics/drawable/Drawable/setLevel(I)Z
pop
aload 4
aload 5
aload 1
iload 2
invokevirtual android/graphics/drawable/AnimationDrawable/getDuration(I)I
invokevirtual android/graphics/drawable/AnimationDrawable/addFrame(Landroid/graphics/drawable/Drawable;I)V
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 4
sipush 10000
invokevirtual android/graphics/drawable/AnimationDrawable/setLevel(I)Z
pop
L0:
aload 4
areturn
.limit locals 6
.limit stack 4
.end method

.method private a(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;
iconst_0
istore 4
aload 1
instanceof android/support/v4/e/a/q
ifeq L0
aload 1
checkcast android/support/v4/e/a/q
invokeinterface android/support/v4/e/a/q/a()Landroid/graphics/drawable/Drawable; 0
astore 7
aload 7
ifnull L1
aload 0
aload 7
iload 2
invokespecial android/support/v7/widget/z/a(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;
astore 7
aload 1
checkcast android/support/v4/e/a/q
aload 7
invokeinterface android/support/v4/e/a/q/a(Landroid/graphics/drawable/Drawable;)V 1
L1:
aload 1
areturn
L0:
aload 1
instanceof android/graphics/drawable/LayerDrawable
ifeq L2
aload 1
checkcast android/graphics/drawable/LayerDrawable
astore 1
aload 1
invokevirtual android/graphics/drawable/LayerDrawable/getNumberOfLayers()I
istore 5
iload 5
anewarray android/graphics/drawable/Drawable
astore 7
iconst_0
istore 3
L3:
iload 3
iload 5
if_icmpge L4
aload 1
iload 3
invokevirtual android/graphics/drawable/LayerDrawable/getId(I)I
istore 6
aload 1
iload 3
invokevirtual android/graphics/drawable/LayerDrawable/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 8
iload 6
ldc_w 16908301
if_icmpeq L5
iload 6
ldc_w 16908303
if_icmpne L6
L5:
iconst_1
istore 2
L7:
aload 7
iload 3
aload 0
aload 8
iload 2
invokespecial android/support/v7/widget/z/a(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;
aastore
iload 3
iconst_1
iadd
istore 3
goto L3
L6:
iconst_0
istore 2
goto L7
L4:
new android/graphics/drawable/LayerDrawable
dup
aload 7
invokespecial android/graphics/drawable/LayerDrawable/<init>([Landroid/graphics/drawable/Drawable;)V
astore 7
iload 4
istore 3
L8:
iload 3
iload 5
if_icmpge L9
aload 7
iload 3
aload 1
iload 3
invokevirtual android/graphics/drawable/LayerDrawable/getId(I)I
invokevirtual android/graphics/drawable/LayerDrawable/setId(II)V
iload 3
iconst_1
iadd
istore 3
goto L8
L9:
aload 7
areturn
L2:
aload 1
instanceof android/graphics/drawable/BitmapDrawable
ifeq L1
aload 1
checkcast android/graphics/drawable/BitmapDrawable
invokevirtual android/graphics/drawable/BitmapDrawable/getBitmap()Landroid/graphics/Bitmap;
astore 7
aload 0
getfield android/support/v7/widget/z/b Landroid/graphics/Bitmap;
ifnonnull L10
aload 0
aload 7
putfield android/support/v7/widget/z/b Landroid/graphics/Bitmap;
L10:
new android/graphics/drawable/ShapeDrawable
dup
aload 0
invokespecial android/support/v7/widget/z/getDrawableShape()Landroid/graphics/drawable/shapes/Shape;
invokespecial android/graphics/drawable/ShapeDrawable/<init>(Landroid/graphics/drawable/shapes/Shape;)V
astore 1
new android/graphics/BitmapShader
dup
aload 7
getstatic android/graphics/Shader$TileMode/REPEAT Landroid/graphics/Shader$TileMode;
getstatic android/graphics/Shader$TileMode/CLAMP Landroid/graphics/Shader$TileMode;
invokespecial android/graphics/BitmapShader/<init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V
astore 7
aload 1
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
aload 7
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
iload 2
ifeq L11
new android/graphics/drawable/ClipDrawable
dup
aload 1
iconst_3
iconst_1
invokespecial android/graphics/drawable/ClipDrawable/<init>(Landroid/graphics/drawable/Drawable;II)V
areturn
L11:
aload 1
areturn
.limit locals 9
.limit stack 5
.end method

.method private getDrawableShape()Landroid/graphics/drawable/shapes/Shape;
new android/graphics/drawable/shapes/RoundRectShape
dup
bipush 8
newarray float
dup
iconst_0
ldc_w 5.0F
fastore
dup
iconst_1
ldc_w 5.0F
fastore
dup
iconst_2
ldc_w 5.0F
fastore
dup
iconst_3
ldc_w 5.0F
fastore
dup
iconst_4
ldc_w 5.0F
fastore
dup
iconst_5
ldc_w 5.0F
fastore
dup
bipush 6
ldc_w 5.0F
fastore
dup
bipush 7
ldc_w 5.0F
fastore
aconst_null
aconst_null
invokespecial android/graphics/drawable/shapes/RoundRectShape/<init>([FLandroid/graphics/RectF;[F)V
areturn
.limit locals 1
.limit stack 6
.end method

.method protected final onMeasure(II)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
iload 1
iload 2
invokespecial android/widget/RatingBar/onMeasure(II)V
aload 0
getfield android/support/v7/widget/z/b Landroid/graphics/Bitmap;
ifnull L1
aload 0
aload 0
getfield android/support/v7/widget/z/b Landroid/graphics/Bitmap;
invokevirtual android/graphics/Bitmap/getWidth()I
aload 0
invokevirtual android/support/v7/widget/z/getNumStars()I
imul
iload 1
iconst_0
invokestatic android/support/v4/view/cx/a(III)I
aload 0
invokevirtual android/support/v7/widget/z/getMeasuredHeight()I
invokevirtual android/support/v7/widget/z/setMeasuredDimension(II)V
L1:
aload 0
monitorexit
return
L2:
astore 3
aload 0
monitorexit
aload 3
athrow
.limit locals 4
.limit stack 4
.end method
