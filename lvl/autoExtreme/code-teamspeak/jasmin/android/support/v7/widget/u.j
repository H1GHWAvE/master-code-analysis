.bytecode 50.0
.class final synchronized android/support/v7/widget/u
.super java/lang/Object

.field 'a' Landroid/content/res/ColorStateList;

.field 'b' Landroid/graphics/PorterDuff$Mode;

.field private final 'c' Landroid/widget/CompoundButton;

.field private final 'd' Landroid/support/v7/internal/widget/av;

.field private 'e' Z

.field private 'f' Z

.field private 'g' Z

.method <init>(Landroid/widget/CompoundButton;Landroid/support/v7/internal/widget/av;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield android/support/v7/widget/u/a Landroid/content/res/ColorStateList;
aload 0
aconst_null
putfield android/support/v7/widget/u/b Landroid/graphics/PorterDuff$Mode;
aload 0
iconst_0
putfield android/support/v7/widget/u/e Z
aload 0
iconst_0
putfield android/support/v7/widget/u/f Z
aload 0
aload 1
putfield android/support/v7/widget/u/c Landroid/widget/CompoundButton;
aload 0
aload 2
putfield android/support/v7/widget/u/d Landroid/support/v7/internal/widget/av;
return
.limit locals 3
.limit stack 2
.end method

.method private b()Landroid/content/res/ColorStateList;
aload 0
getfield android/support/v7/widget/u/a Landroid/content/res/ColorStateList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Landroid/graphics/PorterDuff$Mode;
aload 0
getfield android/support/v7/widget/u/b Landroid/graphics/PorterDuff$Mode;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()V
aload 0
getfield android/support/v7/widget/u/c Landroid/widget/CompoundButton;
invokestatic android/support/v4/widget/g/a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;
astore 1
aload 1
ifnull L0
aload 0
getfield android/support/v7/widget/u/e Z
ifne L1
aload 0
getfield android/support/v7/widget/u/f Z
ifeq L0
L1:
aload 1
invokestatic android/support/v4/e/a/a/c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/mutate()Landroid/graphics/drawable/Drawable;
astore 1
aload 0
getfield android/support/v7/widget/u/e Z
ifeq L2
aload 1
aload 0
getfield android/support/v7/widget/u/a Landroid/content/res/ColorStateList;
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
L2:
aload 0
getfield android/support/v7/widget/u/f Z
ifeq L3
aload 1
aload 0
getfield android/support/v7/widget/u/b Landroid/graphics/PorterDuff$Mode;
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
L3:
aload 1
invokevirtual android/graphics/drawable/Drawable/isStateful()Z
ifeq L4
aload 1
aload 0
getfield android/support/v7/widget/u/c Landroid/widget/CompoundButton;
invokevirtual android/widget/CompoundButton/getDrawableState()[I
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L4:
aload 0
getfield android/support/v7/widget/u/c Landroid/widget/CompoundButton;
aload 1
invokevirtual android/widget/CompoundButton/setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method final a(I)I
iload 1
istore 2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 17
if_icmpge L0
aload 0
getfield android/support/v7/widget/u/c Landroid/widget/CompoundButton;
invokestatic android/support/v4/widget/g/a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;
astore 3
iload 1
istore 2
aload 3
ifnull L0
iload 1
aload 3
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
iadd
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method final a()V
aload 0
getfield android/support/v7/widget/u/g Z
ifeq L0
aload 0
iconst_0
putfield android/support/v7/widget/u/g Z
return
L0:
aload 0
iconst_1
putfield android/support/v7/widget/u/g Z
aload 0
invokespecial android/support/v7/widget/u/d()V
return
.limit locals 1
.limit stack 2
.end method

.method final a(Landroid/content/res/ColorStateList;)V
aload 0
aload 1
putfield android/support/v7/widget/u/a Landroid/content/res/ColorStateList;
aload 0
iconst_1
putfield android/support/v7/widget/u/e Z
aload 0
invokespecial android/support/v7/widget/u/d()V
return
.limit locals 2
.limit stack 2
.end method

.method final a(Landroid/graphics/PorterDuff$Mode;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/v7/widget/u/b Landroid/graphics/PorterDuff$Mode;
aload 0
iconst_1
putfield android/support/v7/widget/u/f Z
aload 0
invokespecial android/support/v7/widget/u/d()V
return
.limit locals 2
.limit stack 2
.end method

.method final a(Landroid/util/AttributeSet;I)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L5 to L6 using L2
aload 0
getfield android/support/v7/widget/u/c Landroid/widget/CompoundButton;
invokevirtual android/widget/CompoundButton/getContext()Landroid/content/Context;
aload 1
getstatic android/support/v7/a/n/CompoundButton [I
iload 2
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
L0:
aload 1
getstatic android/support/v7/a/n/CompoundButton_android_button I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L4
aload 1
getstatic android/support/v7/a/n/CompoundButton_android_button I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 2
L1:
iload 2
ifeq L4
L3:
aload 0
getfield android/support/v7/widget/u/c Landroid/widget/CompoundButton;
aload 0
getfield android/support/v7/widget/u/d Landroid/support/v7/internal/widget/av;
iload 2
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/CompoundButton/setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
L4:
aload 1
getstatic android/support/v7/a/n/CompoundButton_buttonTint I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L5
aload 0
getfield android/support/v7/widget/u/c Landroid/widget/CompoundButton;
aload 1
getstatic android/support/v7/a/n/CompoundButton_buttonTint I
invokevirtual android/content/res/TypedArray/getColorStateList(I)Landroid/content/res/ColorStateList;
invokestatic android/support/v4/widget/g/a(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V
L5:
aload 1
getstatic android/support/v7/a/n/CompoundButton_buttonTintMode I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L6
aload 0
getfield android/support/v7/widget/u/c Landroid/widget/CompoundButton;
aload 1
getstatic android/support/v7/a/n/CompoundButton_buttonTintMode I
iconst_m1
invokevirtual android/content/res/TypedArray/getInt(II)I
invokestatic android/support/v7/b/a/a/a(I)Landroid/graphics/PorterDuff$Mode;
invokestatic android/support/v4/widget/g/a(Landroid/widget/CompoundButton;Landroid/graphics/PorterDuff$Mode;)V
L6:
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
return
L2:
astore 3
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method
