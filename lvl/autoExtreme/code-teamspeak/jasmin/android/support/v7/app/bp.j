.bytecode 50.0
.class final synchronized android/support/v7/app/bp
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "TwilightManager"

.field private static final 'b' I = 6


.field private static final 'c' I = 22


.field private static final 'd' Landroid/support/v7/app/br;

.field private final 'e' Landroid/content/Context;

.field private final 'f' Landroid/location/LocationManager;

.method static <clinit>()V
new android/support/v7/app/br
dup
iconst_0
invokespecial android/support/v7/app/br/<init>(B)V
putstatic android/support/v7/app/bp/d Landroid/support/v7/app/br;
return
.limit locals 0
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v7/app/bp/e Landroid/content/Context;
aload 0
aload 1
ldc "location"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/location/LocationManager
putfield android/support/v7/app/bp/f Landroid/location/LocationManager;
return
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/lang/String;)Landroid/location/Location;
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield android/support/v7/app/bp/f Landroid/location/LocationManager;
ifnull L3
L0:
aload 0
getfield android/support/v7/app/bp/f Landroid/location/LocationManager;
aload 1
invokevirtual android/location/LocationManager/isProviderEnabled(Ljava/lang/String;)Z
ifeq L3
aload 0
getfield android/support/v7/app/bp/f Landroid/location/LocationManager;
aload 1
invokevirtual android/location/LocationManager/getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "TwilightManager"
ldc "Failed to get last known location"
aload 1
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L3:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Landroid/location/Location;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
getstatic android/support/v7/app/bp/d Landroid/support/v7/app/br;
astore 12
invokestatic java/lang/System/currentTimeMillis()J
lstore 1
getstatic android/support/v7/app/bo/a Landroid/support/v7/app/bo;
ifnonnull L0
new android/support/v7/app/bo
dup
invokespecial android/support/v7/app/bo/<init>()V
putstatic android/support/v7/app/bo/a Landroid/support/v7/app/bo;
L0:
getstatic android/support/v7/app/bo/a Landroid/support/v7/app/bo;
astore 13
aload 13
lload 1
ldc2_w 86400000L
lsub
aload 0
invokevirtual android/location/Location/getLatitude()D
aload 0
invokevirtual android/location/Location/getLongitude()D
invokevirtual android/support/v7/app/bo/a(JDD)V
aload 13
getfield android/support/v7/app/bo/d J
lstore 3
aload 13
lload 1
aload 0
invokevirtual android/location/Location/getLatitude()D
aload 0
invokevirtual android/location/Location/getLongitude()D
invokevirtual android/support/v7/app/bo/a(JDD)V
aload 13
getfield android/support/v7/app/bo/f I
iconst_1
if_icmpne L1
iconst_1
istore 11
L2:
aload 13
getfield android/support/v7/app/bo/e J
lstore 5
aload 13
getfield android/support/v7/app/bo/d J
lstore 7
aload 13
ldc2_w 86400000L
lload 1
ladd
aload 0
invokevirtual android/location/Location/getLatitude()D
aload 0
invokevirtual android/location/Location/getLongitude()D
invokevirtual android/support/v7/app/bo/a(JDD)V
aload 13
getfield android/support/v7/app/bo/e J
lstore 9
lload 5
ldc2_w -1L
lcmp
ifeq L3
lload 7
ldc2_w -1L
lcmp
ifne L4
L3:
ldc2_w 43200000L
lload 1
ladd
lstore 1
L5:
aload 12
iload 11
putfield android/support/v7/app/br/a Z
aload 12
lload 3
putfield android/support/v7/app/br/b J
aload 12
lload 5
putfield android/support/v7/app/br/c J
aload 12
lload 7
putfield android/support/v7/app/br/d J
aload 12
lload 9
putfield android/support/v7/app/br/e J
aload 12
lload 1
putfield android/support/v7/app/br/f J
return
L1:
iconst_0
istore 11
goto L2
L4:
lload 1
lload 7
lcmp
ifle L6
lconst_0
lload 9
ladd
lstore 1
L7:
lload 1
ldc2_w 60000L
ladd
lstore 1
goto L5
L6:
lload 1
lload 5
lcmp
ifle L8
lconst_0
lload 7
ladd
lstore 1
goto L7
L8:
lconst_0
lload 5
ladd
lstore 1
goto L7
.limit locals 14
.limit stack 7
.end method

.method private a()Z
getstatic android/support/v7/app/bp/d Landroid/support/v7/app/br;
astore 15
aload 15
ifnull L0
aload 15
getfield android/support/v7/app/br/f J
invokestatic java/lang/System/currentTimeMillis()J
lcmp
ifle L0
iconst_1
istore 1
L1:
iload 1
ifeq L2
aload 15
getfield android/support/v7/app/br/a Z
ireturn
L0:
iconst_0
istore 1
goto L1
L2:
aconst_null
astore 13
aconst_null
astore 14
aload 0
getfield android/support/v7/app/bp/e Landroid/content/Context;
ldc "android.permission.ACCESS_FINE_LOCATION"
invokestatic android/support/v4/c/ar/a(Landroid/content/Context;Ljava/lang/String;)I
ifne L3
aload 0
ldc "network"
invokespecial android/support/v7/app/bp/a(Ljava/lang/String;)Landroid/location/Location;
astore 13
L3:
aload 0
getfield android/support/v7/app/bp/e Landroid/content/Context;
ldc "android.permission.ACCESS_COARSE_LOCATION"
invokestatic android/support/v4/c/ar/a(Landroid/content/Context;Ljava/lang/String;)I
ifne L4
aload 0
ldc "gps"
invokespecial android/support/v7/app/bp/a(Ljava/lang/String;)Landroid/location/Location;
astore 14
L4:
aload 13
ifnull L5
aload 14
ifnull L5
aload 14
invokevirtual android/location/Location/getTime()J
aload 13
invokevirtual android/location/Location/getTime()J
lcmp
ifle L6
aload 14
astore 13
L7:
aload 13
ifnull L8
getstatic android/support/v7/app/bp/d Landroid/support/v7/app/br;
astore 14
invokestatic java/lang/System/currentTimeMillis()J
lstore 2
getstatic android/support/v7/app/bo/a Landroid/support/v7/app/bo;
ifnonnull L9
new android/support/v7/app/bo
dup
invokespecial android/support/v7/app/bo/<init>()V
putstatic android/support/v7/app/bo/a Landroid/support/v7/app/bo;
L9:
getstatic android/support/v7/app/bo/a Landroid/support/v7/app/bo;
astore 16
aload 16
lload 2
ldc2_w 86400000L
lsub
aload 13
invokevirtual android/location/Location/getLatitude()D
aload 13
invokevirtual android/location/Location/getLongitude()D
invokevirtual android/support/v7/app/bo/a(JDD)V
aload 16
getfield android/support/v7/app/bo/d J
lstore 4
aload 16
lload 2
aload 13
invokevirtual android/location/Location/getLatitude()D
aload 13
invokevirtual android/location/Location/getLongitude()D
invokevirtual android/support/v7/app/bo/a(JDD)V
aload 16
getfield android/support/v7/app/bo/f I
iconst_1
if_icmpne L10
iconst_1
istore 12
L11:
aload 16
getfield android/support/v7/app/bo/e J
lstore 6
aload 16
getfield android/support/v7/app/bo/d J
lstore 8
aload 16
ldc2_w 86400000L
lload 2
ladd
aload 13
invokevirtual android/location/Location/getLatitude()D
aload 13
invokevirtual android/location/Location/getLongitude()D
invokevirtual android/support/v7/app/bo/a(JDD)V
aload 16
getfield android/support/v7/app/bo/e J
lstore 10
lload 6
ldc2_w -1L
lcmp
ifeq L12
lload 8
ldc2_w -1L
lcmp
ifne L13
L12:
ldc2_w 43200000L
lload 2
ladd
lstore 2
L14:
aload 14
iload 12
putfield android/support/v7/app/br/a Z
aload 14
lload 4
putfield android/support/v7/app/br/b J
aload 14
lload 6
putfield android/support/v7/app/br/c J
aload 14
lload 8
putfield android/support/v7/app/br/d J
aload 14
lload 10
putfield android/support/v7/app/br/e J
aload 14
lload 2
putfield android/support/v7/app/br/f J
aload 15
getfield android/support/v7/app/br/a Z
ireturn
L6:
goto L7
L5:
aload 14
ifnull L15
aload 14
astore 13
goto L7
L15:
goto L7
L10:
iconst_0
istore 12
goto L11
L13:
lload 2
lload 8
lcmp
ifle L16
lconst_0
lload 10
ladd
lstore 2
L17:
lload 2
ldc2_w 60000L
ladd
lstore 2
goto L14
L16:
lload 2
lload 6
lcmp
ifle L18
lconst_0
lload 8
ladd
lstore 2
goto L17
L18:
lconst_0
lload 6
ladd
lstore 2
goto L17
L8:
ldc "TwilightManager"
ldc "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
invokestatic java/util/Calendar/getInstance()Ljava/util/Calendar;
bipush 11
invokevirtual java/util/Calendar/get(I)I
istore 1
iload 1
bipush 6
if_icmplt L19
iload 1
bipush 22
if_icmplt L20
L19:
iconst_1
ireturn
L20:
iconst_0
ireturn
.limit locals 17
.limit stack 7
.end method

.method private static a(Landroid/support/v7/app/br;)Z
aload 0
ifnull L0
aload 0
getfield android/support/v7/app/br/f J
invokestatic java/lang/System/currentTimeMillis()J
lcmp
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 4
.end method

.method private b()Landroid/location/Location;
aconst_null
astore 2
aload 0
getfield android/support/v7/app/bp/e Landroid/content/Context;
ldc "android.permission.ACCESS_FINE_LOCATION"
invokestatic android/support/v4/c/ar/a(Landroid/content/Context;Ljava/lang/String;)I
ifne L0
aload 0
ldc "network"
invokespecial android/support/v7/app/bp/a(Ljava/lang/String;)Landroid/location/Location;
astore 1
L1:
aload 0
getfield android/support/v7/app/bp/e Landroid/content/Context;
ldc "android.permission.ACCESS_COARSE_LOCATION"
invokestatic android/support/v4/c/ar/a(Landroid/content/Context;Ljava/lang/String;)I
ifne L2
aload 0
ldc "gps"
invokespecial android/support/v7/app/bp/a(Ljava/lang/String;)Landroid/location/Location;
astore 2
L2:
aload 1
ifnull L3
aload 2
ifnull L3
aload 1
astore 3
aload 2
invokevirtual android/location/Location/getTime()J
aload 1
invokevirtual android/location/Location/getTime()J
lcmp
ifle L4
aload 2
astore 3
L4:
aload 3
areturn
L3:
aload 1
astore 3
aload 2
ifnull L4
aload 2
areturn
L0:
aconst_null
astore 1
goto L1
.limit locals 4
.limit stack 4
.end method
