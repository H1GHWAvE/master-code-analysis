.bytecode 50.0
.class public synchronized android/support/v7/app/ah
.super android/support/v4/app/bb
.implements android/support/v4/app/gm
.implements android/support/v7/app/ai
.implements android/support/v7/app/m

.field private 'm' Landroid/support/v7/app/aj;

.method public <init>()V
aload 0
invokespecial android/support/v4/app/bb/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
aload 1
invokevirtual android/support/v7/app/aj/a(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/app/gl;)V
aconst_null
astore 2
aload 0
instanceof android/support/v4/app/gm
ifeq L0
aload 0
checkcast android/support/v4/app/gm
invokeinterface android/support/v4/app/gm/a_()Landroid/content/Intent; 0
astore 2
L0:
aload 2
ifnonnull L1
aload 0
invokestatic android/support/v4/app/cv/a(Landroid/app/Activity;)Landroid/content/Intent;
astore 2
L2:
aload 2
ifnull L3
aload 2
invokevirtual android/content/Intent/getComponent()Landroid/content/ComponentName;
astore 4
aload 4
astore 3
aload 4
ifnonnull L4
aload 2
aload 1
getfield android/support/v4/app/gl/b Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
invokevirtual android/content/Intent/resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;
astore 3
L4:
aload 1
aload 3
invokevirtual android/support/v4/app/gl/a(Landroid/content/ComponentName;)Landroid/support/v4/app/gl;
pop
aload 1
aload 2
invokevirtual android/support/v4/app/gl/a(Landroid/content/Intent;)Landroid/support/v4/app/gl;
pop
L3:
return
L1:
goto L2
.limit locals 5
.limit stack 2
.end method

.method private a(Landroid/support/v7/widget/Toolbar;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
aload 1
invokevirtual android/support/v7/app/aj/a(Landroid/support/v7/widget/Toolbar;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/content/Intent;)Z
aload 0
aload 1
invokestatic android/support/v4/app/cv/a(Landroid/app/Activity;Landroid/content/Intent;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/content/Intent;)V
aload 0
aload 1
invokestatic android/support/v4/app/cv/b(Landroid/app/Activity;Landroid/content/Intent;)V
return
.limit locals 2
.limit stack 2
.end method

.method private b(I)Z
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
iload 1
invokevirtual android/support/v7/app/aj/b(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private j()Landroid/support/v7/app/a;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/a()Landroid/support/v7/app/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static k()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
return
.limit locals 0
.limit stack 0
.end method

.method private static l()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
return
.limit locals 0
.limit stack 0
.end method

.method private static m()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
return
.limit locals 0
.limit stack 0
.end method

.method private static n()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
return
.limit locals 0
.limit stack 0
.end method

.method private static o()V
return
.limit locals 0
.limit stack 0
.end method

.method private static p()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
return
.limit locals 0
.limit stack 0
.end method

.method public final a_()Landroid/content/Intent;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
invokestatic android/support/v4/app/cv/a(Landroid/app/Activity;)Landroid/content/Intent;
areturn
.limit locals 1
.limit stack 1
.end method

.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
aload 1
aload 2
invokevirtual android/support/v7/app/aj/b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final b()V
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/g()V
return
.limit locals 1
.limit stack 1
.end method

.method public final d()Landroid/support/v7/app/l;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/i()Landroid/support/v7/app/l;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e()V
.annotation invisible Landroid/support/a/h;
.end annotation
return
.limit locals 1
.limit stack 0
.end method

.method public final f()V
.annotation invisible Landroid/support/a/h;
.end annotation
return
.limit locals 1
.limit stack 0
.end method

.method public final g()Landroid/support/v7/c/a;
.annotation invisible Landroid/support/a/z;
.end annotation
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/b()Landroid/view/MenuInflater;
areturn
.limit locals 1
.limit stack 1
.end method

.method public h()Z
.catch java/lang/IllegalStateException from L0 to L1 using L2
.catch java/lang/IllegalStateException from L3 to L4 using L2
aload 0
invokestatic android/support/v4/app/cv/a(Landroid/app/Activity;)Landroid/content/Intent;
astore 1
aload 1
ifnull L5
aload 0
aload 1
invokestatic android/support/v4/app/cv/a(Landroid/app/Activity;Landroid/content/Intent;)Z
ifeq L6
aload 0
invokestatic android/support/v4/app/gl/a(Landroid/content/Context;)Landroid/support/v4/app/gl;
astore 4
aconst_null
astore 1
aload 0
instanceof android/support/v4/app/gm
ifeq L7
aload 0
checkcast android/support/v4/app/gm
invokeinterface android/support/v4/app/gm/a_()Landroid/content/Intent; 0
astore 1
L7:
aload 1
ifnonnull L8
aload 0
invokestatic android/support/v4/app/cv/a(Landroid/app/Activity;)Landroid/content/Intent;
astore 1
L9:
aload 1
ifnull L10
aload 1
invokevirtual android/content/Intent/getComponent()Landroid/content/ComponentName;
astore 3
aload 3
astore 2
aload 3
ifnonnull L11
aload 1
aload 4
getfield android/support/v4/app/gl/b Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
invokevirtual android/content/Intent/resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;
astore 2
L11:
aload 4
aload 2
invokevirtual android/support/v4/app/gl/a(Landroid/content/ComponentName;)Landroid/support/v4/app/gl;
pop
aload 4
aload 1
invokevirtual android/support/v4/app/gl/a(Landroid/content/Intent;)Landroid/support/v4/app/gl;
pop
L10:
aload 4
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L12
new java/lang/IllegalStateException
dup
ldc "No intents added to TaskStackBuilder; cannot startActivities"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L12:
aload 4
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
aload 4
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
anewarray android/content/Intent
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Landroid/content/Intent;
astore 1
aload 1
iconst_0
new android/content/Intent
dup
aload 1
iconst_0
aaload
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
ldc_w 268484608
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
aastore
aload 4
getfield android/support/v4/app/gl/b Landroid/content/Context;
aload 1
invokestatic android/support/v4/c/h/a(Landroid/content/Context;[Landroid/content/Intent;)Z
ifne L0
new android/content/Intent
dup
aload 1
aload 1
arraylength
iconst_1
isub
aaload
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
astore 1
aload 1
ldc_w 268435456
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 4
getfield android/support/v4/app/gl/b Landroid/content/Context;
aload 1
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L3
aload 0
invokevirtual android/app/Activity/finishAffinity()V
L1:
goto L13
L3:
aload 0
invokevirtual android/app/Activity/finish()V
L4:
goto L13
L2:
astore 1
aload 0
invokevirtual android/support/v7/app/ah/finish()V
goto L13
L6:
aload 0
aload 1
invokestatic android/support/v4/app/cv/b(Landroid/app/Activity;Landroid/content/Intent;)V
goto L13
L5:
iconst_0
ireturn
L8:
goto L9
L13:
iconst_1
ireturn
.limit locals 5
.limit stack 6
.end method

.method public final i()Landroid/support/v7/app/aj;
aload 0
getfield android/support/v7/app/ah/m Landroid/support/v7/app/aj;
ifnonnull L0
aload 0
aload 0
aload 0
invokevirtual android/app/Activity/getWindow()Landroid/view/Window;
aload 0
invokestatic android/support/v7/app/aj/a(Landroid/content/Context;Landroid/view/Window;Landroid/support/v7/app/ai;)Landroid/support/v7/app/aj;
putfield android/support/v7/app/ah/m Landroid/support/v7/app/aj;
L0:
aload 0
getfield android/support/v7/app/ah/m Landroid/support/v7/app/aj;
areturn
.limit locals 1
.limit stack 4
.end method

.method public invalidateOptionsMenu()V
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/g()V
return
.limit locals 1
.limit stack 1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 0
aload 1
invokespecial android/support/v4/app/bb/onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
aload 1
invokevirtual android/support/v7/app/aj/a(Landroid/content/res/Configuration;)V
return
.limit locals 2
.limit stack 2
.end method

.method public onContentChanged()V
return
.limit locals 1
.limit stack 0
.end method

.method public onCreate(Landroid/os/Bundle;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/j()V
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/c()V
aload 0
aload 1
invokespecial android/support/v4/app/bb/onCreate(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 2
.end method

.method public onDestroy()V
aload 0
invokespecial android/support/v4/app/bb/onDestroy()V
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/h()V
return
.limit locals 1
.limit stack 1
.end method

.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
aload 0
iload 1
aload 2
invokespecial android/support/v4/app/bb/onMenuItemSelected(ILandroid/view/MenuItem;)Z
ifeq L0
iconst_1
ireturn
L0:
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/a()Landroid/support/v7/app/a;
astore 3
aload 2
invokeinterface android/view/MenuItem/getItemId()I 0
ldc_w 16908332
if_icmpne L1
aload 3
ifnull L1
aload 3
invokevirtual android/support/v7/app/a/h()I
iconst_4
iand
ifeq L1
aload 0
invokevirtual android/support/v7/app/ah/h()Z
ireturn
L1:
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
aload 0
iload 1
aload 2
invokespecial android/support/v4/app/bb/onMenuOpened(ILandroid/view/Menu;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
aload 0
iload 1
aload 2
invokespecial android/support/v4/app/bb/onPanelClosed(ILandroid/view/Menu;)V
return
.limit locals 3
.limit stack 3
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
invokespecial android/support/v4/app/bb/onPostCreate(Landroid/os/Bundle;)V
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/d()V
return
.limit locals 2
.limit stack 2
.end method

.method protected onPostResume()V
aload 0
invokespecial android/support/v4/app/bb/onPostResume()V
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/f()V
return
.limit locals 1
.limit stack 1
.end method

.method protected onStop()V
aload 0
invokespecial android/support/v4/app/bb/onStop()V
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/e()V
return
.limit locals 1
.limit stack 1
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
aload 0
aload 1
iload 2
invokespecial android/support/v4/app/bb/onTitleChanged(Ljava/lang/CharSequence;I)V
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
aload 1
invokevirtual android/support/v7/app/aj/a(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 3
.end method

.method public setContentView(I)V
.annotation invisibleparam 1 Landroid/support/a/v;
.end annotation
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
iload 1
invokevirtual android/support/v7/app/aj/a(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setContentView(Landroid/view/View;)V
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
aload 1
invokevirtual android/support/v7/app/aj/a(Landroid/view/View;)V
return
.limit locals 2
.limit stack 2
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
aload 1
aload 2
invokevirtual android/support/v7/app/aj/a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 3
.limit stack 3
.end method
