.bytecode 50.0
.class public synchronized android/support/v7/app/bf
.super android/app/Dialog
.implements android/support/v7/app/ai

.field private 'a' Landroid/support/v7/app/aj;

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/v7/app/bf/<init>(Landroid/content/Context;I)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;I)V
iload 2
istore 3
iload 2
ifne L0
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 4
aload 1
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
getstatic android/support/v7/a/d/dialogTheme I
aload 4
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
pop
aload 4
getfield android/util/TypedValue/resourceId I
istore 3
L0:
aload 0
aload 1
iload 3
invokespecial android/app/Dialog/<init>(Landroid/content/Context;I)V
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/c()V
return
.limit locals 5
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
aload 0
aload 1
iload 2
aload 3
invokespecial android/app/Dialog/<init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
return
.limit locals 4
.limit stack 4
.end method

.method private static a(Landroid/content/Context;I)I
iload 1
istore 2
iload 1
ifne L0
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 3
aload 0
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
getstatic android/support/v7/a/d/dialogTheme I
aload 3
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
pop
aload 3
getfield android/util/TypedValue/resourceId I
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 4
.end method

.method private b()Landroid/support/v7/app/a;
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/a()Landroid/support/v7/app/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Landroid/support/v7/app/aj;
aload 0
getfield android/support/v7/app/bf/a Landroid/support/v7/app/aj;
ifnonnull L0
aload 0
aload 0
invokevirtual android/app/Dialog/getContext()Landroid/content/Context;
aload 0
invokevirtual android/app/Dialog/getWindow()Landroid/view/Window;
aload 0
invokestatic android/support/v7/app/aj/a(Landroid/content/Context;Landroid/view/Window;Landroid/support/v7/app/ai;)Landroid/support/v7/app/aj;
putfield android/support/v7/app/bf/a Landroid/support/v7/app/aj;
L0:
aload 0
getfield android/support/v7/app/bf/a Landroid/support/v7/app/aj;
areturn
.limit locals 1
.limit stack 4
.end method

.method public final a()Z
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
iconst_1
invokevirtual android/support/v7/app/aj/b(I)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
aload 1
aload 2
invokevirtual android/support/v7/app/aj/b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final e()V
return
.limit locals 1
.limit stack 0
.end method

.method public final f()V
return
.limit locals 1
.limit stack 0
.end method

.method public final g()Landroid/support/v7/c/a;
.annotation invisible Landroid/support/a/z;
.end annotation
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public invalidateOptionsMenu()V
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/g()V
return
.limit locals 1
.limit stack 1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/j()V
aload 0
aload 1
invokespecial android/app/Dialog/onCreate(Landroid/os/Bundle;)V
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/c()V
return
.limit locals 2
.limit stack 2
.end method

.method protected onStop()V
aload 0
invokespecial android/app/Dialog/onStop()V
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/e()V
return
.limit locals 1
.limit stack 1
.end method

.method public setContentView(I)V
.annotation invisibleparam 1 Landroid/support/a/v;
.end annotation
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
iload 1
invokevirtual android/support/v7/app/aj/a(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setContentView(Landroid/view/View;)V
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
aload 1
invokevirtual android/support/v7/app/aj/a(Landroid/view/View;)V
return
.limit locals 2
.limit stack 2
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
aload 1
aload 2
invokevirtual android/support/v7/app/aj/a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 3
.limit stack 3
.end method

.method public setTitle(I)V
aload 0
iload 1
invokespecial android/app/Dialog/setTitle(I)V
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
aload 0
invokevirtual android/support/v7/app/bf/getContext()Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokevirtual android/support/v7/app/aj/a(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 3
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 1
invokespecial android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
invokespecial android/support/v7/app/bf/c()Landroid/support/v7/app/aj;
aload 1
invokevirtual android/support/v7/app/aj/a(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method
