.bytecode 50.0
.class public final synchronized android/support/v7/internal/view/menu/ExpandedMenuView
.super android/widget/ListView
.implements android/support/v7/internal/view/menu/k
.implements android/support/v7/internal/view/menu/z
.implements android/widget/AdapterView$OnItemClickListener

.field private static final 'a' [I

.field private 'b' Landroid/support/v7/internal/view/menu/i;

.field private 'c' I

.method static <clinit>()V
iconst_2
newarray int
dup
iconst_0
ldc_w 16842964
iastore
dup
iconst_1
ldc_w 16843049
iastore
putstatic android/support/v7/internal/view/menu/ExpandedMenuView/a [I
return
.limit locals 0
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
ldc_w 16842868
invokespecial android/support/v7/internal/view/menu/ExpandedMenuView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
invokespecial android/widget/ListView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 0
invokevirtual android/support/v7/internal/view/menu/ExpandedMenuView/setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
aload 1
aload 2
getstatic android/support/v7/internal/view/menu/ExpandedMenuView/a [I
iload 3
invokestatic android/support/v7/internal/widget/ax/a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
astore 1
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(I)Z
ifeq L0
aload 0
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/view/menu/ExpandedMenuView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
L0:
aload 1
iconst_1
invokevirtual android/support/v7/internal/widget/ax/e(I)Z
ifeq L1
aload 0
aload 1
iconst_1
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/view/menu/ExpandedMenuView/setDivider(Landroid/graphics/drawable/Drawable;)V
L1:
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 4
.limit stack 4
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/ExpandedMenuView/b Landroid/support/v7/internal/view/menu/i;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)Z
aload 0
getfield android/support/v7/internal/view/menu/ExpandedMenuView/b Landroid/support/v7/internal/view/menu/i;
aload 1
aconst_null
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z
ireturn
.limit locals 2
.limit stack 4
.end method

.method public final getWindowAnimations()I
aload 0
getfield android/support/v7/internal/view/menu/ExpandedMenuView/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected final onDetachedFromWindow()V
aload 0
invokespecial android/widget/ListView/onDetachedFromWindow()V
aload 0
iconst_0
invokevirtual android/support/v7/internal/view/menu/ExpandedMenuView/setChildrenDrawingCacheEnabled(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
aload 0
aload 0
invokevirtual android/support/v7/internal/view/menu/ExpandedMenuView/getAdapter()Landroid/widget/ListAdapter;
iload 3
invokeinterface android/widget/ListAdapter/getItem(I)Ljava/lang/Object; 1
checkcast android/support/v7/internal/view/menu/m
invokevirtual android/support/v7/internal/view/menu/ExpandedMenuView/a(Landroid/support/v7/internal/view/menu/m;)Z
pop
return
.limit locals 6
.limit stack 3
.end method
