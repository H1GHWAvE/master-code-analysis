.bytecode 50.0
.class final synchronized android/support/v7/internal/view/h
.super java/lang/Object

.field private static final 'A' I = 0


.field private static final 'B' I = 0


.field private static final 'C' I = 0


.field private static final 'D' I = 0


.field private static final 'E' I = 0


.field private static final 'F' Z = 0


.field private static final 'G' Z = 1


.field private static final 'H' Z = 1


.field 'a' Landroid/view/Menu;

.field 'b' I

.field 'c' I

.field 'd' I

.field 'e' I

.field 'f' Z

.field 'g' Z

.field 'h' Z

.field 'i' I

.field 'j' I

.field 'k' Ljava/lang/CharSequence;

.field 'l' Ljava/lang/CharSequence;

.field 'm' I

.field 'n' C

.field 'o' C

.field 'p' I

.field 'q' Z

.field 'r' Z

.field 's' Z

.field 't' I

.field 'u' I

.field 'v' Ljava/lang/String;

.field 'w' Ljava/lang/String;

.field 'x' Ljava/lang/String;

.field 'y' Landroid/support/v4/view/n;

.field final synthetic 'z' Landroid/support/v7/internal/view/f;

.method public <init>(Landroid/support/v7/internal/view/f;Landroid/view/Menu;)V
aload 0
aload 1
putfield android/support/v7/internal/view/h/z Landroid/support/v7/internal/view/f;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 2
putfield android/support/v7/internal/view/h/a Landroid/view/Menu;
aload 0
invokevirtual android/support/v7/internal/view/h/a()V
return
.limit locals 3
.limit stack 2
.end method

.method static a(Ljava/lang/String;)C
aload 0
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static synthetic a(Landroid/support/v7/internal/view/h;)Landroid/support/v4/view/n;
aload 0
getfield android/support/v7/internal/view/h/y Landroid/support/v4/view/n;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/util/AttributeSet;)V
aload 0
getfield android/support/v7/internal/view/h/z Landroid/support/v7/internal/view/f;
invokestatic android/support/v7/internal/view/f/a(Landroid/support/v7/internal/view/f;)Landroid/content/Context;
aload 1
getstatic android/support/v7/a/n/MenuGroup [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
getstatic android/support/v7/a/n/MenuGroup_android_id I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/v7/internal/view/h/b I
aload 0
aload 1
getstatic android/support/v7/a/n/MenuGroup_android_menuCategory I
iconst_0
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/v7/internal/view/h/c I
aload 0
aload 1
getstatic android/support/v7/a/n/MenuGroup_android_orderInCategory I
iconst_0
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/v7/internal/view/h/d I
aload 0
aload 1
getstatic android/support/v7/a/n/MenuGroup_android_checkableBehavior I
iconst_0
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/v7/internal/view/h/e I
aload 0
aload 1
getstatic android/support/v7/a/n/MenuGroup_android_visible I
iconst_1
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/v7/internal/view/h/f Z
aload 0
aload 1
getstatic android/support/v7/a/n/MenuGroup_android_enabled I
iconst_1
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/v7/internal/view/h/g Z
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 2
.limit stack 4
.end method

.method private b(Landroid/util/AttributeSet;)V
iconst_1
istore 3
aload 0
getfield android/support/v7/internal/view/h/z Landroid/support/v7/internal/view/f;
invokestatic android/support/v7/internal/view/f/a(Landroid/support/v7/internal/view/f;)Landroid/content/Context;
aload 1
getstatic android/support/v7/a/n/MenuItem [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_android_id I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/v7/internal/view/h/i I
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_android_menuCategory I
aload 0
getfield android/support/v7/internal/view/h/c I
invokevirtual android/content/res/TypedArray/getInt(II)I
ldc_w -65536
iand
aload 1
getstatic android/support/v7/a/n/MenuItem_android_orderInCategory I
aload 0
getfield android/support/v7/internal/view/h/d I
invokevirtual android/content/res/TypedArray/getInt(II)I
ldc_w 65535
iand
ior
putfield android/support/v7/internal/view/h/j I
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_android_title I
invokevirtual android/content/res/TypedArray/getText(I)Ljava/lang/CharSequence;
putfield android/support/v7/internal/view/h/k Ljava/lang/CharSequence;
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_android_titleCondensed I
invokevirtual android/content/res/TypedArray/getText(I)Ljava/lang/CharSequence;
putfield android/support/v7/internal/view/h/l Ljava/lang/CharSequence;
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_android_icon I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/v7/internal/view/h/m I
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_android_alphabeticShortcut I
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
invokestatic android/support/v7/internal/view/h/a(Ljava/lang/String;)C
putfield android/support/v7/internal/view/h/n C
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_android_numericShortcut I
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
invokestatic android/support/v7/internal/view/h/a(Ljava/lang/String;)C
putfield android/support/v7/internal/view/h/o C
aload 1
getstatic android/support/v7/a/n/MenuItem_android_checkable I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L0
aload 1
getstatic android/support/v7/a/n/MenuItem_android_checkable I
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
ifeq L1
iconst_1
istore 2
L2:
aload 0
iload 2
putfield android/support/v7/internal/view/h/p I
L3:
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_android_checked I
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/v7/internal/view/h/q Z
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_android_visible I
aload 0
getfield android/support/v7/internal/view/h/f Z
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/v7/internal/view/h/r Z
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_android_enabled I
aload 0
getfield android/support/v7/internal/view/h/g Z
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/v7/internal/view/h/s Z
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_showAsAction I
iconst_m1
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/v7/internal/view/h/t I
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_android_onClick I
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
putfield android/support/v7/internal/view/h/x Ljava/lang/String;
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_actionLayout I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/v7/internal/view/h/u I
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_actionViewClass I
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
putfield android/support/v7/internal/view/h/v Ljava/lang/String;
aload 0
aload 1
getstatic android/support/v7/a/n/MenuItem_actionProviderClass I
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
putfield android/support/v7/internal/view/h/w Ljava/lang/String;
aload 0
getfield android/support/v7/internal/view/h/w Ljava/lang/String;
ifnull L4
iload 3
istore 2
L5:
iload 2
ifeq L6
aload 0
getfield android/support/v7/internal/view/h/u I
ifne L6
aload 0
getfield android/support/v7/internal/view/h/v Ljava/lang/String;
ifnonnull L6
aload 0
aload 0
aload 0
getfield android/support/v7/internal/view/h/w Ljava/lang/String;
invokestatic android/support/v7/internal/view/f/a()[Ljava/lang/Class;
aload 0
getfield android/support/v7/internal/view/h/z Landroid/support/v7/internal/view/f;
invokestatic android/support/v7/internal/view/f/b(Landroid/support/v7/internal/view/f;)[Ljava/lang/Object;
invokevirtual android/support/v7/internal/view/h/a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/support/v4/view/n
putfield android/support/v7/internal/view/h/y Landroid/support/v4/view/n;
L7:
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
iconst_0
putfield android/support/v7/internal/view/h/h Z
return
L1:
iconst_0
istore 2
goto L2
L0:
aload 0
aload 0
getfield android/support/v7/internal/view/h/e I
putfield android/support/v7/internal/view/h/p I
goto L3
L4:
iconst_0
istore 2
goto L5
L6:
iload 2
ifeq L8
ldc "SupportMenuInflater"
ldc "Ignoring attribute 'actionProviderClass'. Action view already specified."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L8:
aload 0
aconst_null
putfield android/support/v7/internal/view/h/y Landroid/support/v4/view/n;
goto L7
.limit locals 4
.limit stack 5
.end method

.method private c()V
aload 0
iconst_1
putfield android/support/v7/internal/view/h/h Z
aload 0
aload 0
getfield android/support/v7/internal/view/h/a Landroid/view/Menu;
aload 0
getfield android/support/v7/internal/view/h/b I
aload 0
getfield android/support/v7/internal/view/h/i I
aload 0
getfield android/support/v7/internal/view/h/j I
aload 0
getfield android/support/v7/internal/view/h/k Ljava/lang/CharSequence;
invokeinterface android/view/Menu/add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem; 4
invokevirtual android/support/v7/internal/view/h/a(Landroid/view/MenuItem;)V
return
.limit locals 1
.limit stack 6
.end method

.method private d()Z
aload 0
getfield android/support/v7/internal/view/h/h Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method final a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 0
getfield android/support/v7/internal/view/h/z Landroid/support/v7/internal/view/f;
invokestatic android/support/v7/internal/view/f/a(Landroid/support/v7/internal/view/f;)Landroid/content/Context;
invokevirtual android/content/Context/getClassLoader()Ljava/lang/ClassLoader;
aload 1
invokevirtual java/lang/ClassLoader/loadClass(Ljava/lang/String;)Ljava/lang/Class;
aload 2
invokevirtual java/lang/Class/getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
astore 2
aload 2
iconst_1
invokevirtual java/lang/reflect/Constructor/setAccessible(Z)V
aload 2
aload 3
invokevirtual java/lang/reflect/Constructor/newInstance([Ljava/lang/Object;)Ljava/lang/Object;
astore 2
L1:
aload 2
areturn
L2:
astore 2
ldc "SupportMenuInflater"
new java/lang/StringBuilder
dup
ldc "Cannot instantiate class: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
areturn
.limit locals 4
.limit stack 4
.end method

.method public final a()V
aload 0
iconst_0
putfield android/support/v7/internal/view/h/b I
aload 0
iconst_0
putfield android/support/v7/internal/view/h/c I
aload 0
iconst_0
putfield android/support/v7/internal/view/h/d I
aload 0
iconst_0
putfield android/support/v7/internal/view/h/e I
aload 0
iconst_1
putfield android/support/v7/internal/view/h/f Z
aload 0
iconst_1
putfield android/support/v7/internal/view/h/g Z
return
.limit locals 1
.limit stack 2
.end method

.method final a(Landroid/view/MenuItem;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
iconst_1
istore 2
aload 1
aload 0
getfield android/support/v7/internal/view/h/q Z
invokeinterface android/view/MenuItem/setChecked(Z)Landroid/view/MenuItem; 1
aload 0
getfield android/support/v7/internal/view/h/r Z
invokeinterface android/view/MenuItem/setVisible(Z)Landroid/view/MenuItem; 1
aload 0
getfield android/support/v7/internal/view/h/s Z
invokeinterface android/view/MenuItem/setEnabled(Z)Landroid/view/MenuItem; 1
astore 4
aload 0
getfield android/support/v7/internal/view/h/p I
ifle L4
iconst_1
istore 3
L5:
aload 4
iload 3
invokeinterface android/view/MenuItem/setCheckable(Z)Landroid/view/MenuItem; 1
aload 0
getfield android/support/v7/internal/view/h/l Ljava/lang/CharSequence;
invokeinterface android/view/MenuItem/setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem; 1
aload 0
getfield android/support/v7/internal/view/h/m I
invokeinterface android/view/MenuItem/setIcon(I)Landroid/view/MenuItem; 1
aload 0
getfield android/support/v7/internal/view/h/n C
invokeinterface android/view/MenuItem/setAlphabeticShortcut(C)Landroid/view/MenuItem; 1
aload 0
getfield android/support/v7/internal/view/h/o C
invokeinterface android/view/MenuItem/setNumericShortcut(C)Landroid/view/MenuItem; 1
pop
aload 0
getfield android/support/v7/internal/view/h/t I
iflt L6
aload 1
aload 0
getfield android/support/v7/internal/view/h/t I
invokestatic android/support/v4/view/az/a(Landroid/view/MenuItem;I)V
L6:
aload 0
getfield android/support/v7/internal/view/h/x Ljava/lang/String;
ifnull L7
aload 0
getfield android/support/v7/internal/view/h/z Landroid/support/v7/internal/view/f;
invokestatic android/support/v7/internal/view/f/a(Landroid/support/v7/internal/view/f;)Landroid/content/Context;
invokevirtual android/content/Context/isRestricted()Z
ifeq L8
new java/lang/IllegalStateException
dup
ldc "The android:onClick attribute cannot be used within a restricted context"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L4:
iconst_0
istore 3
goto L5
L8:
aload 1
new android/support/v7/internal/view/g
dup
aload 0
getfield android/support/v7/internal/view/h/z Landroid/support/v7/internal/view/f;
invokestatic android/support/v7/internal/view/f/c(Landroid/support/v7/internal/view/f;)Ljava/lang/Object;
aload 0
getfield android/support/v7/internal/view/h/x Ljava/lang/String;
invokespecial android/support/v7/internal/view/g/<init>(Ljava/lang/Object;Ljava/lang/String;)V
invokeinterface android/view/MenuItem/setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem; 1
pop
L7:
aload 0
getfield android/support/v7/internal/view/h/p I
iconst_2
if_icmplt L9
aload 1
instanceof android/support/v7/internal/view/menu/m
ifeq L10
aload 1
checkcast android/support/v7/internal/view/menu/m
iconst_1
invokevirtual android/support/v7/internal/view/menu/m/a(Z)V
L9:
aload 0
getfield android/support/v7/internal/view/h/v Ljava/lang/String;
ifnull L11
aload 1
aload 0
aload 0
getfield android/support/v7/internal/view/h/v Ljava/lang/String;
invokestatic android/support/v7/internal/view/f/b()[Ljava/lang/Class;
aload 0
getfield android/support/v7/internal/view/h/z Landroid/support/v7/internal/view/f;
invokestatic android/support/v7/internal/view/f/d(Landroid/support/v7/internal/view/f;)[Ljava/lang/Object;
invokevirtual android/support/v7/internal/view/h/a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/view/View
invokestatic android/support/v4/view/az/a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;
pop
L12:
aload 0
getfield android/support/v7/internal/view/h/u I
ifle L13
iload 2
ifne L14
aload 1
aload 0
getfield android/support/v7/internal/view/h/u I
invokestatic android/support/v4/view/az/b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;
pop
L13:
aload 0
getfield android/support/v7/internal/view/h/y Landroid/support/v4/view/n;
ifnull L15
aload 1
aload 0
getfield android/support/v7/internal/view/h/y Landroid/support/v4/view/n;
invokestatic android/support/v4/view/az/a(Landroid/view/MenuItem;Landroid/support/v4/view/n;)Landroid/view/MenuItem;
pop
L15:
return
L10:
aload 1
instanceof android/support/v7/internal/view/menu/o
ifeq L9
aload 1
checkcast android/support/v7/internal/view/menu/o
astore 4
L0:
aload 4
getfield android/support/v7/internal/view/menu/o/f Ljava/lang/reflect/Method;
ifnonnull L1
aload 4
aload 4
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
ldc "setExclusiveCheckable"
iconst_1
anewarray java/lang/Class
dup
iconst_0
getstatic java/lang/Boolean/TYPE Ljava/lang/Class;
aastore
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v7/internal/view/menu/o/f Ljava/lang/reflect/Method;
L1:
aload 4
getfield android/support/v7/internal/view/menu/o/f Ljava/lang/reflect/Method;
aload 4
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
iconst_1
anewarray java/lang/Object
dup
iconst_0
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L3:
goto L9
L2:
astore 4
ldc "MenuItemWrapper"
ldc "Error while calling setExclusiveCheckable"
aload 4
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L9
L14:
ldc "SupportMenuInflater"
ldc "Ignoring attribute 'itemActionViewLayout'. Action view already specified."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L13
L11:
iconst_0
istore 2
goto L12
.limit locals 5
.limit stack 7
.end method

.method public final b()Landroid/view/SubMenu;
aload 0
iconst_1
putfield android/support/v7/internal/view/h/h Z
aload 0
getfield android/support/v7/internal/view/h/a Landroid/view/Menu;
aload 0
getfield android/support/v7/internal/view/h/b I
aload 0
getfield android/support/v7/internal/view/h/i I
aload 0
getfield android/support/v7/internal/view/h/j I
aload 0
getfield android/support/v7/internal/view/h/k Ljava/lang/CharSequence;
invokeinterface android/view/Menu/addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu; 4
astore 1
aload 0
aload 1
invokeinterface android/view/SubMenu/getItem()Landroid/view/MenuItem; 0
invokevirtual android/support/v7/internal/view/h/a(Landroid/view/MenuItem;)V
aload 1
areturn
.limit locals 2
.limit stack 5
.end method
