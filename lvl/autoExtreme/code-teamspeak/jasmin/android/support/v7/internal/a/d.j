.bytecode 50.0
.class public final synchronized android/support/v7/internal/a/d
.super java/lang/Object

.field static final 'a' I = 3


.field static final 'b' I = 5


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(I)I
iload 0
iconst_3
if_icmpgt L0
getstatic android/support/v7/a/k/notification_template_big_media_narrow I
ireturn
L0:
getstatic android/support/v7/a/k/notification_template_big_media I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;
aload 1
invokevirtual android/support/v4/app/ek/c()Landroid/app/PendingIntent;
ifnonnull L0
iconst_1
istore 2
L1:
new android/widget/RemoteViews
dup
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
getstatic android/support/v7/a/k/notification_media_action I
invokespecial android/widget/RemoteViews/<init>(Ljava/lang/String;I)V
astore 0
aload 0
getstatic android/support/v7/a/i/action0 I
aload 1
invokevirtual android/support/v4/app/ek/a()I
invokevirtual android/widget/RemoteViews/setImageViewResource(II)V
iload 2
ifne L2
aload 0
getstatic android/support/v7/a/i/action0 I
aload 1
invokevirtual android/support/v4/app/ek/c()Landroid/app/PendingIntent;
invokevirtual android/widget/RemoteViews/setOnClickPendingIntent(ILandroid/app/PendingIntent;)V
L2:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 15
if_icmplt L3
aload 0
getstatic android/support/v7/a/i/action0 I
aload 1
invokevirtual android/support/v4/app/ek/b()Ljava/lang/CharSequence;
invokevirtual android/widget/RemoteViews/setContentDescription(ILjava/lang/CharSequence;)V
L3:
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 4
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;
new android/widget/RemoteViews
dup
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
iload 10
invokespecial android/widget/RemoteViews/<init>(Ljava/lang/String;I)V
astore 14
iconst_0
istore 10
iconst_0
istore 13
aload 5
ifnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
aload 14
getstatic android/support/v7/a/i/icon I
aload 5
invokevirtual android/widget/RemoteViews/setImageViewBitmap(ILandroid/graphics/Bitmap;)V
L1:
aload 1
ifnull L2
aload 14
getstatic android/support/v7/a/i/title I
aload 1
invokevirtual android/widget/RemoteViews/setTextViewText(ILjava/lang/CharSequence;)V
L2:
aload 2
ifnull L3
aload 14
getstatic android/support/v7/a/i/text I
aload 2
invokevirtual android/widget/RemoteViews/setTextViewText(ILjava/lang/CharSequence;)V
iconst_1
istore 10
L3:
aload 3
ifnull L4
aload 14
getstatic android/support/v7/a/i/info I
aload 3
invokevirtual android/widget/RemoteViews/setTextViewText(ILjava/lang/CharSequence;)V
aload 14
getstatic android/support/v7/a/i/info I
iconst_0
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
iconst_1
istore 4
L5:
iload 13
istore 10
aload 6
ifnull L6
iload 13
istore 10
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L6
aload 14
getstatic android/support/v7/a/i/text I
aload 6
invokevirtual android/widget/RemoteViews/setTextViewText(ILjava/lang/CharSequence;)V
aload 2
ifnull L7
aload 14
getstatic android/support/v7/a/i/text2 I
aload 2
invokevirtual android/widget/RemoteViews/setTextViewText(ILjava/lang/CharSequence;)V
aload 14
getstatic android/support/v7/a/i/text2 I
iconst_0
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
iconst_1
istore 10
L6:
iload 10
ifeq L8
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L8
iload 11
ifeq L9
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/g/notification_subtext_size I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
i2f
fstore 12
aload 14
getstatic android/support/v7/a/i/text I
iconst_0
fload 12
invokevirtual android/widget/RemoteViews/setTextViewTextSize(IIF)V
L9:
aload 14
getstatic android/support/v7/a/i/line1 I
iconst_0
iconst_0
iconst_0
iconst_0
invokevirtual android/widget/RemoteViews/setViewPadding(IIIII)V
L8:
lload 8
lconst_0
lcmp
ifeq L10
iload 7
ifeq L11
aload 14
getstatic android/support/v7/a/i/chronometer I
iconst_0
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 14
getstatic android/support/v7/a/i/chronometer I
ldc "setBase"
invokestatic android/os/SystemClock/elapsedRealtime()J
invokestatic java/lang/System/currentTimeMillis()J
lsub
lload 8
ladd
invokevirtual android/widget/RemoteViews/setLong(ILjava/lang/String;J)V
aload 14
getstatic android/support/v7/a/i/chronometer I
ldc "setStarted"
iconst_1
invokevirtual android/widget/RemoteViews/setBoolean(ILjava/lang/String;Z)V
L10:
getstatic android/support/v7/a/i/line3 I
istore 10
iload 4
ifeq L12
iconst_0
istore 4
L13:
aload 14
iload 10
iload 4
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 14
areturn
L0:
aload 14
getstatic android/support/v7/a/i/icon I
bipush 8
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
goto L1
L4:
iload 4
ifle L14
iload 4
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/j/status_bar_notification_info_maxnum I
invokevirtual android/content/res/Resources/getInteger(I)I
if_icmple L15
aload 14
getstatic android/support/v7/a/i/info I
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/l/status_bar_notification_info_overflow I
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokevirtual android/widget/RemoteViews/setTextViewText(ILjava/lang/CharSequence;)V
L16:
aload 14
getstatic android/support/v7/a/i/info I
iconst_0
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
iconst_1
istore 4
goto L5
L15:
invokestatic java/text/NumberFormat/getIntegerInstance()Ljava/text/NumberFormat;
astore 1
aload 14
getstatic android/support/v7/a/i/info I
aload 1
iload 4
i2l
invokevirtual java/text/NumberFormat/format(J)Ljava/lang/String;
invokevirtual android/widget/RemoteViews/setTextViewText(ILjava/lang/CharSequence;)V
goto L16
L14:
aload 14
getstatic android/support/v7/a/i/info I
bipush 8
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
iload 10
istore 4
goto L5
L7:
aload 14
getstatic android/support/v7/a/i/text2 I
bipush 8
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
iload 13
istore 10
goto L6
L11:
aload 14
getstatic android/support/v7/a/i/time I
iconst_0
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 14
getstatic android/support/v7/a/i/time I
ldc "setTime"
lload 8
invokevirtual android/widget/RemoteViews/setLong(ILjava/lang/String;J)V
goto L10
L12:
bipush 8
istore 4
goto L13
.limit locals 15
.limit stack 7
.end method

.method private static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJLjava/util/List;ZLandroid/app/PendingIntent;)Landroid/widget/RemoteViews;
aload 10
invokeinterface java/util/List/size()I 0
iconst_5
invokestatic java/lang/Math/min(II)I
istore 14
iload 14
iconst_3
if_icmpgt L0
getstatic android/support/v7/a/k/notification_template_big_media_narrow I
istore 13
L1:
aload 0
aload 1
aload 2
aload 3
iload 4
aload 5
aload 6
iload 7
lload 8
iload 13
iconst_0
invokestatic android/support/v7/internal/a/d/a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;
astore 1
aload 1
getstatic android/support/v7/a/i/media_actions I
invokevirtual android/widget/RemoteViews/removeAllViews(I)V
iload 14
ifle L2
iconst_0
istore 4
L3:
iload 4
iload 14
if_icmpge L2
aload 0
aload 10
iload 4
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/app/ek
invokestatic android/support/v7/internal/a/d/a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;
astore 2
aload 1
getstatic android/support/v7/a/i/media_actions I
aload 2
invokevirtual android/widget/RemoteViews/addView(ILandroid/widget/RemoteViews;)V
iload 4
iconst_1
iadd
istore 4
goto L3
L0:
getstatic android/support/v7/a/k/notification_template_big_media I
istore 13
goto L1
L2:
iload 11
ifeq L4
aload 1
getstatic android/support/v7/a/i/cancel_action I
iconst_0
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 1
getstatic android/support/v7/a/i/cancel_action I
ldc "setAlpha"
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/j/cancel_button_image_alpha I
invokevirtual android/content/res/Resources/getInteger(I)I
invokevirtual android/widget/RemoteViews/setInt(ILjava/lang/String;I)V
aload 1
getstatic android/support/v7/a/i/cancel_action I
aload 12
invokevirtual android/widget/RemoteViews/setOnClickPendingIntent(ILandroid/app/PendingIntent;)V
aload 1
areturn
L4:
aload 1
getstatic android/support/v7/a/i/cancel_action I
bipush 8
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 1
areturn
.limit locals 15
.limit stack 12
.end method

.method private static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJLjava/util/List;[IZLandroid/app/PendingIntent;)Landroid/widget/RemoteViews;
aload 0
aload 1
aload 2
aload 3
iload 4
aload 5
aload 6
iload 7
lload 8
getstatic android/support/v7/a/k/notification_template_media I
iconst_1
invokestatic android/support/v7/internal/a/d/a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;
astore 1
aload 10
invokeinterface java/util/List/size()I 0
istore 15
aload 11
ifnonnull L0
iconst_0
istore 4
L1:
aload 1
getstatic android/support/v7/a/i/media_actions I
invokevirtual android/widget/RemoteViews/removeAllViews(I)V
iload 4
ifle L2
iconst_0
istore 14
L3:
iload 14
iload 4
if_icmpge L2
iload 14
iload 15
if_icmplt L4
new java/lang/IllegalArgumentException
dup
ldc "setShowActionsInCompactView: action %d out of bounds (max %d)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 14
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 15
iconst_1
isub
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 11
arraylength
iconst_3
invokestatic java/lang/Math/min(II)I
istore 4
goto L1
L4:
aload 0
aload 10
aload 11
iload 14
iaload
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/app/ek
invokestatic android/support/v7/internal/a/d/a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;
astore 2
aload 1
getstatic android/support/v7/a/i/media_actions I
aload 2
invokevirtual android/widget/RemoteViews/addView(ILandroid/widget/RemoteViews;)V
iload 14
iconst_1
iadd
istore 14
goto L3
L2:
iload 12
ifeq L5
aload 1
getstatic android/support/v7/a/i/end_padder I
bipush 8
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 1
getstatic android/support/v7/a/i/cancel_action I
iconst_0
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 1
getstatic android/support/v7/a/i/cancel_action I
aload 13
invokevirtual android/widget/RemoteViews/setOnClickPendingIntent(ILandroid/app/PendingIntent;)V
aload 1
getstatic android/support/v7/a/i/cancel_action I
ldc "setAlpha"
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/j/cancel_button_image_alpha I
invokevirtual android/content/res/Resources/getInteger(I)I
invokevirtual android/widget/RemoteViews/setInt(ILjava/lang/String;I)V
aload 1
areturn
L5:
aload 1
getstatic android/support/v7/a/i/end_padder I
iconst_0
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 1
getstatic android/support/v7/a/i/cancel_action I
bipush 8
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 1
areturn
.limit locals 16
.limit stack 12
.end method

.method private static a(Landroid/app/Notification;Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJLjava/util/List;ZLandroid/app/PendingIntent;)V
aload 11
invokeinterface java/util/List/size()I 0
iconst_5
invokestatic java/lang/Math/min(II)I
istore 15
iload 15
iconst_3
if_icmpgt L0
getstatic android/support/v7/a/k/notification_template_big_media_narrow I
istore 14
L1:
aload 1
aload 2
aload 3
aload 4
iload 5
aload 6
aload 7
iload 8
lload 9
iload 14
iconst_0
invokestatic android/support/v7/internal/a/d/a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;
astore 2
aload 2
getstatic android/support/v7/a/i/media_actions I
invokevirtual android/widget/RemoteViews/removeAllViews(I)V
iload 15
ifle L2
iconst_0
istore 5
L3:
iload 5
iload 15
if_icmpge L2
aload 1
aload 11
iload 5
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/app/ek
invokestatic android/support/v7/internal/a/d/a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;
astore 3
aload 2
getstatic android/support/v7/a/i/media_actions I
aload 3
invokevirtual android/widget/RemoteViews/addView(ILandroid/widget/RemoteViews;)V
iload 5
iconst_1
iadd
istore 5
goto L3
L0:
getstatic android/support/v7/a/k/notification_template_big_media I
istore 14
goto L1
L2:
iload 12
ifeq L4
aload 2
getstatic android/support/v7/a/i/cancel_action I
iconst_0
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 2
getstatic android/support/v7/a/i/cancel_action I
ldc "setAlpha"
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/j/cancel_button_image_alpha I
invokevirtual android/content/res/Resources/getInteger(I)I
invokevirtual android/widget/RemoteViews/setInt(ILjava/lang/String;I)V
aload 2
getstatic android/support/v7/a/i/cancel_action I
aload 13
invokevirtual android/widget/RemoteViews/setOnClickPendingIntent(ILandroid/app/PendingIntent;)V
L5:
aload 0
aload 2
putfield android/app/Notification/bigContentView Landroid/widget/RemoteViews;
iload 12
ifeq L6
aload 0
aload 0
getfield android/app/Notification/flags I
iconst_2
ior
putfield android/app/Notification/flags I
L6:
return
L4:
aload 2
getstatic android/support/v7/a/i/cancel_action I
bipush 8
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
goto L5
.limit locals 16
.limit stack 12
.end method

.method private static a(Landroid/support/v4/app/dc;Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJLjava/util/List;[IZLandroid/app/PendingIntent;)V
aload 1
aload 2
aload 3
aload 4
iload 5
aload 6
aload 7
iload 8
lload 9
getstatic android/support/v7/a/k/notification_template_media I
iconst_1
invokestatic android/support/v7/internal/a/d/a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;
astore 2
aload 11
invokeinterface java/util/List/size()I 0
istore 16
aload 12
ifnonnull L0
iconst_0
istore 5
L1:
aload 2
getstatic android/support/v7/a/i/media_actions I
invokevirtual android/widget/RemoteViews/removeAllViews(I)V
iload 5
ifle L2
iconst_0
istore 15
L3:
iload 15
iload 5
if_icmpge L2
iload 15
iload 16
if_icmplt L4
new java/lang/IllegalArgumentException
dup
ldc "setShowActionsInCompactView: action %d out of bounds (max %d)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 15
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 16
iconst_1
isub
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 12
arraylength
iconst_3
invokestatic java/lang/Math/min(II)I
istore 5
goto L1
L4:
aload 1
aload 11
aload 12
iload 15
iaload
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/app/ek
invokestatic android/support/v7/internal/a/d/a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;
astore 3
aload 2
getstatic android/support/v7/a/i/media_actions I
aload 3
invokevirtual android/widget/RemoteViews/addView(ILandroid/widget/RemoteViews;)V
iload 15
iconst_1
iadd
istore 15
goto L3
L2:
iload 13
ifeq L5
aload 2
getstatic android/support/v7/a/i/end_padder I
bipush 8
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 2
getstatic android/support/v7/a/i/cancel_action I
iconst_0
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 2
getstatic android/support/v7/a/i/cancel_action I
aload 14
invokevirtual android/widget/RemoteViews/setOnClickPendingIntent(ILandroid/app/PendingIntent;)V
aload 2
getstatic android/support/v7/a/i/cancel_action I
ldc "setAlpha"
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/j/cancel_button_image_alpha I
invokevirtual android/content/res/Resources/getInteger(I)I
invokevirtual android/widget/RemoteViews/setInt(ILjava/lang/String;I)V
L6:
aload 0
invokeinterface android/support/v4/app/dc/a()Landroid/app/Notification$Builder; 0
aload 2
invokevirtual android/app/Notification$Builder/setContent(Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;
pop
iload 13
ifeq L7
aload 0
invokeinterface android/support/v4/app/dc/a()Landroid/app/Notification$Builder; 0
iconst_1
invokevirtual android/app/Notification$Builder/setOngoing(Z)Landroid/app/Notification$Builder;
pop
L7:
return
L5:
aload 2
getstatic android/support/v7/a/i/end_padder I
iconst_0
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 2
getstatic android/support/v7/a/i/cancel_action I
bipush 8
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
goto L6
.limit locals 17
.limit stack 12
.end method
