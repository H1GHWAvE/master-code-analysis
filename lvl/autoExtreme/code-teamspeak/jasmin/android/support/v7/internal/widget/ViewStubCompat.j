.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/ViewStubCompat
.super android/view/View

.field private 'a' I

.field private 'b' I

.field private 'c' Ljava/lang/ref/WeakReference;

.field private 'd' Landroid/view/LayoutInflater;

.field private 'e' Landroid/support/v7/internal/widget/bc;

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v7/internal/widget/ViewStubCompat/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/view/View/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
iconst_0
putfield android/support/v7/internal/widget/ViewStubCompat/a I
aload 1
aload 2
getstatic android/support/v7/a/n/ViewStubCompat [I
iload 3
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
getstatic android/support/v7/a/n/ViewStubCompat_android_inflatedId I
iconst_m1
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/v7/internal/widget/ViewStubCompat/b I
aload 0
aload 1
getstatic android/support/v7/a/n/ViewStubCompat_android_layout I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/v7/internal/widget/ViewStubCompat/a I
aload 0
aload 1
getstatic android/support/v7/a/n/ViewStubCompat_android_id I
iconst_m1
invokevirtual android/content/res/TypedArray/getResourceId(II)I
invokevirtual android/support/v7/internal/widget/ViewStubCompat/setId(I)V
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
bipush 8
invokevirtual android/support/v7/internal/widget/ViewStubCompat/setVisibility(I)V
aload 0
iconst_1
invokevirtual android/support/v7/internal/widget/ViewStubCompat/setWillNotDraw(Z)V
return
.limit locals 4
.limit stack 5
.end method

.method public final a()Landroid/view/View;
aload 0
invokevirtual android/support/v7/internal/widget/ViewStubCompat/getParent()Landroid/view/ViewParent;
astore 2
aload 2
ifnull L0
aload 2
instanceof android/view/ViewGroup
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ViewStubCompat/a I
ifeq L1
aload 2
checkcast android/view/ViewGroup
astore 3
aload 0
getfield android/support/v7/internal/widget/ViewStubCompat/d Landroid/view/LayoutInflater;
ifnull L2
aload 0
getfield android/support/v7/internal/widget/ViewStubCompat/d Landroid/view/LayoutInflater;
astore 2
L3:
aload 2
aload 0
getfield android/support/v7/internal/widget/ViewStubCompat/a I
aload 3
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 2
aload 0
getfield android/support/v7/internal/widget/ViewStubCompat/b I
iconst_m1
if_icmpeq L4
aload 2
aload 0
getfield android/support/v7/internal/widget/ViewStubCompat/b I
invokevirtual android/view/View/setId(I)V
L4:
aload 3
aload 0
invokevirtual android/view/ViewGroup/indexOfChild(Landroid/view/View;)I
istore 1
aload 3
aload 0
invokevirtual android/view/ViewGroup/removeViewInLayout(Landroid/view/View;)V
aload 0
invokevirtual android/support/v7/internal/widget/ViewStubCompat/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 4
aload 4
ifnull L5
aload 3
aload 2
iload 1
aload 4
invokevirtual android/view/ViewGroup/addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
L6:
aload 0
new java/lang/ref/WeakReference
dup
aload 2
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
putfield android/support/v7/internal/widget/ViewStubCompat/c Ljava/lang/ref/WeakReference;
aload 2
areturn
L2:
aload 0
invokevirtual android/support/v7/internal/widget/ViewStubCompat/getContext()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
astore 2
goto L3
L5:
aload 3
aload 2
iload 1
invokevirtual android/view/ViewGroup/addView(Landroid/view/View;I)V
goto L6
L1:
new java/lang/IllegalArgumentException
dup
ldc "ViewStub must have a valid layoutResource"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/lang/IllegalStateException
dup
ldc "ViewStub must have a non-null ViewGroup viewParent"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 5
.limit stack 4
.end method

.method protected final dispatchDraw(Landroid/graphics/Canvas;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final getInflatedId()I
aload 0
getfield android/support/v7/internal/widget/ViewStubCompat/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getLayoutInflater()Landroid/view/LayoutInflater;
aload 0
getfield android/support/v7/internal/widget/ViewStubCompat/d Landroid/view/LayoutInflater;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getLayoutResource()I
aload 0
getfield android/support/v7/internal/widget/ViewStubCompat/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected final onMeasure(II)V
aload 0
iconst_0
iconst_0
invokevirtual android/support/v7/internal/widget/ViewStubCompat/setMeasuredDimension(II)V
return
.limit locals 3
.limit stack 3
.end method

.method public final setInflatedId(I)V
aload 0
iload 1
putfield android/support/v7/internal/widget/ViewStubCompat/b I
return
.limit locals 2
.limit stack 2
.end method

.method public final setLayoutInflater(Landroid/view/LayoutInflater;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ViewStubCompat/d Landroid/view/LayoutInflater;
return
.limit locals 2
.limit stack 2
.end method

.method public final setLayoutResource(I)V
aload 0
iload 1
putfield android/support/v7/internal/widget/ViewStubCompat/a I
return
.limit locals 2
.limit stack 2
.end method

.method public final setOnInflateListener(Landroid/support/v7/internal/widget/bc;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ViewStubCompat/e Landroid/support/v7/internal/widget/bc;
return
.limit locals 2
.limit stack 2
.end method

.method public final setVisibility(I)V
aload 0
getfield android/support/v7/internal/widget/ViewStubCompat/c Ljava/lang/ref/WeakReference;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ViewStubCompat/c Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/view/View
astore 2
aload 2
ifnull L1
aload 2
iload 1
invokevirtual android/view/View/setVisibility(I)V
L2:
return
L1:
new java/lang/IllegalStateException
dup
ldc "setVisibility called on un-referenced view"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
invokespecial android/view/View/setVisibility(I)V
iload 1
ifeq L3
iload 1
iconst_4
if_icmpne L2
L3:
aload 0
invokevirtual android/support/v7/internal/widget/ViewStubCompat/a()Landroid/view/View;
pop
return
.limit locals 3
.limit stack 3
.end method
