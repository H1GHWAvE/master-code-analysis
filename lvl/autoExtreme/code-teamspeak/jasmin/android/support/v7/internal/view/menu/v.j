.bytecode 50.0
.class public synchronized android/support/v7/internal/view/menu/v
.super java/lang/Object
.implements android/support/v7/internal/view/menu/x
.implements android/view/View$OnKeyListener
.implements android/view/ViewTreeObserver$OnGlobalLayoutListener
.implements android/widget/AdapterView$OnItemClickListener
.implements android/widget/PopupWindow$OnDismissListener

.field static final 'a' I

.field private static final 'g' Ljava/lang/String; = "MenuPopupHelper"

.field public 'b' Landroid/view/View;

.field public 'c' Landroid/support/v7/widget/an;

.field public 'd' Landroid/support/v7/internal/view/menu/y;

.field public 'e' Z

.field public 'f' I

.field private final 'h' Landroid/content/Context;

.field private final 'i' Landroid/view/LayoutInflater;

.field private final 'j' Landroid/support/v7/internal/view/menu/i;

.field private final 'k' Landroid/support/v7/internal/view/menu/w;

.field private final 'l' Z

.field private final 'm' I

.field private final 'n' I

.field private final 'o' I

.field private 'p' Landroid/view/ViewTreeObserver;

.field private 'q' Landroid/view/ViewGroup;

.field private 'r' Z

.field private 's' I

.method static <clinit>()V
getstatic android/support/v7/a/k/abc_popup_menu_item_layout I
putstatic android/support/v7/internal/view/menu/v/a I
return
.limit locals 0
.limit stack 1
.end method

.method public <init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
aload 0
aload 1
aload 2
aconst_null
iconst_0
getstatic android/support/v7/a/d/popupMenuStyle I
invokespecial android/support/v7/internal/view/menu/v/<init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;ZI)V
return
.limit locals 3
.limit stack 6
.end method

.method public <init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;)V
aload 0
aload 1
aload 2
aload 3
iconst_0
getstatic android/support/v7/a/d/popupMenuStyle I
invokespecial android/support/v7/internal/view/menu/v/<init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;ZI)V
return
.limit locals 4
.limit stack 6
.end method

.method public <init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;ZI)V
aload 0
aload 1
aload 2
aload 3
iload 4
iload 5
iconst_0
invokespecial android/support/v7/internal/view/menu/v/<init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;ZIB)V
return
.limit locals 6
.limit stack 7
.end method

.method public <init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;ZIB)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/v/f I
aload 0
aload 1
putfield android/support/v7/internal/view/menu/v/h Landroid/content/Context;
aload 0
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield android/support/v7/internal/view/menu/v/i Landroid/view/LayoutInflater;
aload 0
aload 2
putfield android/support/v7/internal/view/menu/v/j Landroid/support/v7/internal/view/menu/i;
aload 0
new android/support/v7/internal/view/menu/w
dup
aload 0
aload 0
getfield android/support/v7/internal/view/menu/v/j Landroid/support/v7/internal/view/menu/i;
invokespecial android/support/v7/internal/view/menu/w/<init>(Landroid/support/v7/internal/view/menu/v;Landroid/support/v7/internal/view/menu/i;)V
putfield android/support/v7/internal/view/menu/v/k Landroid/support/v7/internal/view/menu/w;
aload 0
iload 4
putfield android/support/v7/internal/view/menu/v/l Z
aload 0
iload 5
putfield android/support/v7/internal/view/menu/v/n I
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/v/o I
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
astore 7
aload 0
aload 7
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
iconst_2
idiv
aload 7
getstatic android/support/v7/a/g/abc_config_prefDialogWidth I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/internal/view/menu/v/m I
aload 0
aload 3
putfield android/support/v7/internal/view/menu/v/b Landroid/view/View;
aload 2
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V
return
.limit locals 8
.limit stack 5
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/v7/internal/view/menu/v/f I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/View;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/v/b Landroid/view/View;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/internal/view/menu/v;)Z
aload 0
getfield android/support/v7/internal/view/menu/v/l Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Landroid/support/v7/internal/view/menu/v;)Landroid/view/LayoutInflater;
aload 0
getfield android/support/v7/internal/view/menu/v/i Landroid/view/LayoutInflater;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Z)V
aload 0
iload 1
putfield android/support/v7/internal/view/menu/v/e Z
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic c(Landroid/support/v7/internal/view/menu/v;)Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/internal/view/menu/v/j Landroid/support/v7/internal/view/menu/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method private h()I
aload 0
getfield android/support/v7/internal/view/menu/v/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i()Landroid/support/v7/widget/an;
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
areturn
.limit locals 1
.limit stack 1
.end method

.method private j()I
aload 0
getfield android/support/v7/internal/view/menu/v/k Landroid/support/v7/internal/view/menu/w;
astore 9
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 6
aload 9
invokeinterface android/widget/ListAdapter/getCount()I 0
istore 7
iconst_0
istore 2
iconst_0
istore 3
aconst_null
astore 8
iconst_0
istore 1
L0:
iload 1
istore 4
iload 2
iload 7
if_icmpge L1
aload 9
iload 2
invokeinterface android/widget/ListAdapter/getItemViewType(I)I 1
istore 4
iload 4
iload 3
if_icmpeq L2
iload 4
istore 3
aconst_null
astore 8
L3:
aload 0
getfield android/support/v7/internal/view/menu/v/q Landroid/view/ViewGroup;
ifnonnull L4
aload 0
new android/widget/FrameLayout
dup
aload 0
getfield android/support/v7/internal/view/menu/v/h Landroid/content/Context;
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;)V
putfield android/support/v7/internal/view/menu/v/q Landroid/view/ViewGroup;
L4:
aload 9
iload 2
aload 8
aload 0
getfield android/support/v7/internal/view/menu/v/q Landroid/view/ViewGroup;
invokeinterface android/widget/ListAdapter/getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View; 3
astore 8
aload 8
iload 5
iload 6
invokevirtual android/view/View/measure(II)V
aload 8
invokevirtual android/view/View/getMeasuredWidth()I
istore 4
iload 4
aload 0
getfield android/support/v7/internal/view/menu/v/m I
if_icmplt L5
aload 0
getfield android/support/v7/internal/view/menu/v/m I
istore 4
L1:
iload 4
ireturn
L5:
iload 4
iload 1
if_icmple L6
iload 4
istore 1
L7:
iload 2
iconst_1
iadd
istore 2
goto L0
L2:
goto L3
L6:
goto L7
.limit locals 10
.limit stack 4
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
new java/lang/UnsupportedOperationException
dup
ldc "MenuPopupHelpers manage their own views"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
return
.limit locals 3
.limit stack 0
.end method

.method public final a(Landroid/os/Parcelable;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
aload 1
aload 0
getfield android/support/v7/internal/view/menu/v/j Landroid/support/v7/internal/view/menu/i;
if_acmpeq L0
L1:
return
L0:
aload 0
invokevirtual android/support/v7/internal/view/menu/v/f()V
aload 0
getfield android/support/v7/internal/view/menu/v/d Landroid/support/v7/internal/view/menu/y;
ifnull L1
aload 0
getfield android/support/v7/internal/view/menu/v/d Landroid/support/v7/internal/view/menu/y;
aload 1
iload 2
invokeinterface android/support/v7/internal/view/menu/y/a(Landroid/support/v7/internal/view/menu/i;Z)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/support/v7/internal/view/menu/y;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/v/d Landroid/support/v7/internal/view/menu/y;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)V
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/v/r Z
aload 0
getfield android/support/v7/internal/view/menu/v/k Landroid/support/v7/internal/view/menu/w;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/v/k Landroid/support/v7/internal/view/menu/w;
invokevirtual android/support/v7/internal/view/menu/w/notifyDataSetChanged()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final a()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ad;)Z
aload 1
invokevirtual android/support/v7/internal/view/menu/ad/hasVisibleItems()Z
ifeq L0
new android/support/v7/internal/view/menu/v
dup
aload 0
getfield android/support/v7/internal/view/menu/v/h Landroid/content/Context;
aload 1
aload 0
getfield android/support/v7/internal/view/menu/v/b Landroid/view/View;
invokespecial android/support/v7/internal/view/menu/v/<init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;)V
astore 5
aload 5
aload 0
getfield android/support/v7/internal/view/menu/v/d Landroid/support/v7/internal/view/menu/y;
putfield android/support/v7/internal/view/menu/v/d Landroid/support/v7/internal/view/menu/y;
aload 1
invokevirtual android/support/v7/internal/view/menu/ad/size()I
istore 3
iconst_0
istore 2
L1:
iload 2
iload 3
if_icmpge L2
aload 1
iload 2
invokevirtual android/support/v7/internal/view/menu/ad/getItem(I)Landroid/view/MenuItem;
astore 6
aload 6
invokeinterface android/view/MenuItem/isVisible()Z 0
ifeq L3
aload 6
invokeinterface android/view/MenuItem/getIcon()Landroid/graphics/drawable/Drawable; 0
ifnull L3
iconst_1
istore 4
L4:
aload 5
iload 4
putfield android/support/v7/internal/view/menu/v/e Z
aload 5
invokevirtual android/support/v7/internal/view/menu/v/e()Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/v/d Landroid/support/v7/internal/view/menu/y;
ifnull L5
aload 0
getfield android/support/v7/internal/view/menu/v/d Landroid/support/v7/internal/view/menu/y;
aload 1
invokeinterface android/support/v7/internal/view/menu/y/a_(Landroid/support/v7/internal/view/menu/i;)Z 1
pop
L5:
iconst_1
ireturn
L3:
iload 2
iconst_1
iadd
istore 2
goto L1
L0:
iconst_0
ireturn
L2:
iconst_0
istore 4
goto L4
.limit locals 7
.limit stack 5
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final b()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(Landroid/support/v7/internal/view/menu/m;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final c()Landroid/os/Parcelable;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public final d()V
aload 0
invokevirtual android/support/v7/internal/view/menu/v/e()Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "MenuPopupHelper cannot be used without an anchor"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 1
.limit stack 3
.end method

.method public final e()Z
iconst_0
istore 4
aload 0
new android/support/v7/widget/an
dup
aload 0
getfield android/support/v7/internal/view/menu/v/h Landroid/content/Context;
aconst_null
aload 0
getfield android/support/v7/internal/view/menu/v/n I
aload 0
getfield android/support/v7/internal/view/menu/v/o I
invokespecial android/support/v7/widget/an/<init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
putfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
aload 0
invokevirtual android/support/v7/widget/an/a(Landroid/widget/PopupWindow$OnDismissListener;)V
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
aload 0
putfield android/support/v7/widget/an/m Landroid/widget/AdapterView$OnItemClickListener;
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
aload 0
getfield android/support/v7/internal/view/menu/v/k Landroid/support/v7/internal/view/menu/w;
invokevirtual android/support/v7/widget/an/a(Landroid/widget/ListAdapter;)V
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
invokevirtual android/support/v7/widget/an/c()V
aload 0
getfield android/support/v7/internal/view/menu/v/b Landroid/view/View;
astore 8
aload 8
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/v/p Landroid/view/ViewTreeObserver;
ifnonnull L1
iconst_1
istore 1
L2:
aload 0
aload 8
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
putfield android/support/v7/internal/view/menu/v/p Landroid/view/ViewTreeObserver;
iload 1
ifeq L3
aload 0
getfield android/support/v7/internal/view/menu/v/p Landroid/view/ViewTreeObserver;
aload 0
invokevirtual android/view/ViewTreeObserver/addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
L3:
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
aload 8
putfield android/support/v7/widget/an/l Landroid/view/View;
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
aload 0
getfield android/support/v7/internal/view/menu/v/f I
putfield android/support/v7/widget/an/i I
aload 0
getfield android/support/v7/internal/view/menu/v/r Z
ifne L4
aload 0
getfield android/support/v7/internal/view/menu/v/k Landroid/support/v7/internal/view/menu/w;
astore 9
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 6
aload 9
invokeinterface android/widget/ListAdapter/getCount()I 0
istore 7
iconst_0
istore 2
iconst_0
istore 3
aconst_null
astore 8
iload 4
istore 1
L5:
iload 1
istore 4
iload 2
iload 7
if_icmpge L6
aload 9
iload 2
invokeinterface android/widget/ListAdapter/getItemViewType(I)I 1
istore 4
iload 4
iload 3
if_icmpeq L7
iload 4
istore 3
aconst_null
astore 8
L8:
aload 0
getfield android/support/v7/internal/view/menu/v/q Landroid/view/ViewGroup;
ifnonnull L9
aload 0
new android/widget/FrameLayout
dup
aload 0
getfield android/support/v7/internal/view/menu/v/h Landroid/content/Context;
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;)V
putfield android/support/v7/internal/view/menu/v/q Landroid/view/ViewGroup;
L9:
aload 9
iload 2
aload 8
aload 0
getfield android/support/v7/internal/view/menu/v/q Landroid/view/ViewGroup;
invokeinterface android/widget/ListAdapter/getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View; 3
astore 8
aload 8
iload 5
iload 6
invokevirtual android/view/View/measure(II)V
aload 8
invokevirtual android/view/View/getMeasuredWidth()I
istore 4
iload 4
aload 0
getfield android/support/v7/internal/view/menu/v/m I
if_icmplt L10
aload 0
getfield android/support/v7/internal/view/menu/v/m I
istore 4
L6:
aload 0
iload 4
putfield android/support/v7/internal/view/menu/v/s I
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/v/r Z
L4:
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
aload 0
getfield android/support/v7/internal/view/menu/v/s I
invokevirtual android/support/v7/widget/an/a(I)V
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
invokevirtual android/support/v7/widget/an/e()V
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
invokevirtual android/support/v7/widget/an/b()V
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
invokevirtual android/widget/ListView/setOnKeyListener(Landroid/view/View$OnKeyListener;)V
iconst_1
ireturn
L1:
iconst_0
istore 1
goto L2
L0:
iconst_0
ireturn
L10:
iload 4
iload 1
if_icmple L11
iload 4
istore 1
L12:
iload 2
iconst_1
iadd
istore 2
goto L5
L7:
goto L8
L11:
goto L12
.limit locals 10
.limit stack 7
.end method

.method public final f()V
aload 0
invokevirtual android/support/v7/internal/view/menu/v/g()Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
invokevirtual android/support/v7/widget/an/d()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public final g()Z
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public onDismiss()V
aload 0
aconst_null
putfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
aload 0
getfield android/support/v7/internal/view/menu/v/j Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/close()V
aload 0
getfield android/support/v7/internal/view/menu/v/p Landroid/view/ViewTreeObserver;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/v/p Landroid/view/ViewTreeObserver;
invokevirtual android/view/ViewTreeObserver/isAlive()Z
ifne L1
aload 0
aload 0
getfield android/support/v7/internal/view/menu/v/b Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
putfield android/support/v7/internal/view/menu/v/p Landroid/view/ViewTreeObserver;
L1:
aload 0
getfield android/support/v7/internal/view/menu/v/p Landroid/view/ViewTreeObserver;
aload 0
invokevirtual android/view/ViewTreeObserver/removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
aload 0
aconst_null
putfield android/support/v7/internal/view/menu/v/p Landroid/view/ViewTreeObserver;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public onGlobalLayout()V
aload 0
invokevirtual android/support/v7/internal/view/menu/v/g()Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/v/b Landroid/view/View;
astore 1
aload 1
ifnull L1
aload 1
invokevirtual android/view/View/isShown()Z
ifne L2
L1:
aload 0
invokevirtual android/support/v7/internal/view/menu/v/f()V
L0:
return
L2:
aload 0
invokevirtual android/support/v7/internal/view/menu/v/g()Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/v/c Landroid/support/v7/widget/an;
invokevirtual android/support/v7/widget/an/b()V
return
.limit locals 2
.limit stack 1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
aload 0
getfield android/support/v7/internal/view/menu/v/k Landroid/support/v7/internal/view/menu/w;
astore 1
aload 1
invokestatic android/support/v7/internal/view/menu/w/a(Landroid/support/v7/internal/view/menu/w;)Landroid/support/v7/internal/view/menu/i;
aload 1
iload 3
invokevirtual android/support/v7/internal/view/menu/w/a(I)Landroid/support/v7/internal/view/menu/m;
aconst_null
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z
pop
return
.limit locals 6
.limit stack 4
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
aload 3
invokevirtual android/view/KeyEvent/getAction()I
iconst_1
if_icmpne L0
iload 2
bipush 82
if_icmpne L0
aload 0
invokevirtual android/support/v7/internal/view/menu/v/f()V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method
