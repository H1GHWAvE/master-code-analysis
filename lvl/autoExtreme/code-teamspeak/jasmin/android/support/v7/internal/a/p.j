.bytecode 50.0
.class public final synchronized android/support/v7/internal/a/p
.super android/support/v7/c/a
.implements android/support/v7/internal/view/menu/j

.field final synthetic 'a' Landroid/support/v7/internal/a/l;

.field private final 'd' Landroid/content/Context;

.field private final 'e' Landroid/support/v7/internal/view/menu/i;

.field private 'f' Landroid/support/v7/c/b;

.field private 'g' Ljava/lang/ref/WeakReference;

.method public <init>(Landroid/support/v7/internal/a/l;Landroid/content/Context;Landroid/support/v7/c/b;)V
aload 0
aload 1
putfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
aload 0
invokespecial android/support/v7/c/a/<init>()V
aload 0
aload 2
putfield android/support/v7/internal/a/p/d Landroid/content/Context;
aload 0
aload 3
putfield android/support/v7/internal/a/p/f Landroid/support/v7/c/b;
new android/support/v7/internal/view/menu/i
dup
aload 2
invokespecial android/support/v7/internal/view/menu/i/<init>(Landroid/content/Context;)V
astore 1
aload 1
iconst_1
putfield android/support/v7/internal/view/menu/i/i I
aload 0
aload 1
putfield android/support/v7/internal/a/p/e Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/internal/a/p/e Landroid/support/v7/internal/view/menu/i;
aload 0
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/j;)V
return
.limit locals 4
.limit stack 3
.end method

.method private a(Landroid/support/v7/internal/view/menu/ad;)Z
iconst_1
istore 2
aload 0
getfield android/support/v7/internal/a/p/f Landroid/support/v7/c/b;
ifnonnull L0
iconst_0
istore 2
L1:
iload 2
ireturn
L0:
aload 1
invokevirtual android/support/v7/internal/view/menu/ad/hasVisibleItems()Z
ifeq L1
new android/support/v7/internal/view/menu/v
dup
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokevirtual android/support/v7/internal/a/l/r()Landroid/content/Context;
aload 1
invokespecial android/support/v7/internal/view/menu/v/<init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
invokevirtual android/support/v7/internal/view/menu/v/d()V
iconst_1
ireturn
.limit locals 3
.limit stack 4
.end method

.method private static j()V
return
.limit locals 0
.limit stack 0
.end method

.method private static k()V
return
.limit locals 0
.limit stack 0
.end method

.method public final a()Landroid/view/MenuInflater;
new android/support/v7/internal/view/f
dup
aload 0
getfield android/support/v7/internal/a/p/d Landroid/content/Context;
invokespecial android/support/v7/internal/view/f/<init>(Landroid/content/Context;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final a(I)V
aload 0
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/j(Landroid/support/v7/internal/a/l;)Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokevirtual android/support/v7/internal/a/p/b(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;)V
aload 0
getfield android/support/v7/internal/a/p/f Landroid/support/v7/c/b;
ifnonnull L0
return
L0:
aload 0
invokevirtual android/support/v7/internal/a/p/d()V
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/a()Z
pop
return
.limit locals 2
.limit stack 1
.end method

.method public final a(Landroid/view/View;)V
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContextView/setCustomView(Landroid/view/View;)V
aload 0
new java/lang/ref/WeakReference
dup
aload 1
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
putfield android/support/v7/internal/a/p/g Ljava/lang/ref/WeakReference;
return
.limit locals 2
.limit stack 4
.end method

.method public final a(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContextView/setSubtitle(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)V
aload 0
iload 1
invokespecial android/support/v7/c/a/a(Z)V
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;
iload 1
invokevirtual android/support/v7/internal/widget/ActionBarContextView/setTitleOptional(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
aload 0
getfield android/support/v7/internal/a/p/f Landroid/support/v7/c/b;
ifnull L0
aload 0
getfield android/support/v7/internal/a/p/f Landroid/support/v7/c/b;
aload 0
aload 2
invokeinterface android/support/v7/c/b/a(Landroid/support/v7/c/a;Landroid/view/MenuItem;)Z 2
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final b()Landroid/view/Menu;
aload 0
getfield android/support/v7/internal/a/p/e Landroid/support/v7/internal/view/menu/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b(I)V
aload 0
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/j(Landroid/support/v7/internal/a/l;)Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokevirtual android/support/v7/internal/a/p/a(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final b(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContextView/setTitle(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final c()V
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
getfield android/support/v7/internal/a/l/j Landroid/support/v7/internal/a/p;
aload 0
if_acmpeq L0
return
L0:
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/f(Landroid/support/v7/internal/a/l;)Z
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/g(Landroid/support/v7/internal/a/l;)Z
invokestatic android/support/v7/internal/a/l/a(ZZ)Z
ifne L1
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
aload 0
putfield android/support/v7/internal/a/l/k Landroid/support/v7/c/a;
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
aload 0
getfield android/support/v7/internal/a/p/f Landroid/support/v7/c/b;
putfield android/support/v7/internal/a/l/l Landroid/support/v7/c/b;
L2:
aload 0
aconst_null
putfield android/support/v7/internal/a/p/f Landroid/support/v7/c/b;
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
iconst_0
invokevirtual android/support/v7/internal/a/l/j(Z)V
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;
astore 1
aload 1
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
ifnonnull L3
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContextView/i()V
L3:
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/i(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/a()Landroid/view/ViewGroup; 0
bipush 32
invokevirtual android/view/ViewGroup/sendAccessibilityEvent(I)V
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/e(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
getfield android/support/v7/internal/a/l/m Z
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setHideOnContentScrollEnabled(Z)V
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
aconst_null
putfield android/support/v7/internal/a/l/j Landroid/support/v7/internal/a/p;
return
L1:
aload 0
getfield android/support/v7/internal/a/p/f Landroid/support/v7/c/b;
aload 0
invokeinterface android/support/v7/c/b/a(Landroid/support/v7/c/a;)V 1
goto L2
.limit locals 2
.limit stack 2
.end method

.method public final d()V
.catch all from L0 to L1 using L2
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
getfield android/support/v7/internal/a/l/j Landroid/support/v7/internal/a/p;
aload 0
if_acmpeq L3
return
L3:
aload 0
getfield android/support/v7/internal/a/p/e Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/d()V
L0:
aload 0
getfield android/support/v7/internal/a/p/f Landroid/support/v7/c/b;
aload 0
aload 0
getfield android/support/v7/internal/a/p/e Landroid/support/v7/internal/view/menu/i;
invokeinterface android/support/v7/c/b/b(Landroid/support/v7/c/a;Landroid/view/Menu;)Z 2
pop
L1:
aload 0
getfield android/support/v7/internal/a/p/e Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/e()V
return
L2:
astore 1
aload 0
getfield android/support/v7/internal/a/p/e Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/e()V
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public final e()Z
.catch all from L0 to L1 using L2
aload 0
getfield android/support/v7/internal/a/p/e Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/d()V
L0:
aload 0
getfield android/support/v7/internal/a/p/f Landroid/support/v7/c/b;
aload 0
aload 0
getfield android/support/v7/internal/a/p/e Landroid/support/v7/internal/view/menu/i;
invokeinterface android/support/v7/c/b/a(Landroid/support/v7/c/a;Landroid/view/Menu;)Z 2
istore 1
L1:
aload 0
getfield android/support/v7/internal/a/p/e Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/e()V
iload 1
ireturn
L2:
astore 2
aload 0
getfield android/support/v7/internal/a/p/e Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/e()V
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public final f()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getTitle()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final g()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getSubtitle()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final h()Z
aload 0
getfield android/support/v7/internal/a/p/a Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;
getfield android/support/v7/internal/widget/ActionBarContextView/h Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final i()Landroid/view/View;
aload 0
getfield android/support/v7/internal/a/p/g Ljava/lang/ref/WeakReference;
ifnull L0
aload 0
getfield android/support/v7/internal/a/p/g Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/view/View
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method
