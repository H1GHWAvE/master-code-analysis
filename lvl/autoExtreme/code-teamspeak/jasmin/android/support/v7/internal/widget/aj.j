.bytecode 50.0
.class synchronized android/support/v7/internal/widget/aj
.super android/content/res/Resources

.field private final 'a' Landroid/content/res/Resources;

.method public <init>(Landroid/content/res/Resources;)V
aload 0
aload 1
invokevirtual android/content/res/Resources/getAssets()Landroid/content/res/AssetManager;
aload 1
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
aload 1
invokevirtual android/content/res/Resources/getConfiguration()Landroid/content/res/Configuration;
invokespecial android/content/res/Resources/<init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
return
.limit locals 2
.limit stack 4
.end method

.method public getAnimation(I)Landroid/content/res/XmlResourceParser;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getAnimation(I)Landroid/content/res/XmlResourceParser;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getBoolean(I)Z
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getBoolean(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getColor(I)I
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getColor(I)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getColorStateList(I)Landroid/content/res/ColorStateList;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getColorStateList(I)Landroid/content/res/ColorStateList;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getConfiguration()Landroid/content/res/Configuration;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getConfiguration()Landroid/content/res/Configuration;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDimension(I)F
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDimension(I)F
freturn
.limit locals 2
.limit stack 2
.end method

.method public getDimensionPixelOffset(I)I
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDimensionPixelOffset(I)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getDimensionPixelSize(I)I
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getDisplayMetrics()Landroid/util/DisplayMetrics;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDrawable(I)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
aload 2
invokevirtual android/content/res/Resources/getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
areturn
.limit locals 3
.limit stack 3
.end method

.method public getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
iload 2
invokevirtual android/content/res/Resources/getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
areturn
.limit locals 3
.limit stack 3
.end method

.method public getDrawableForDensity(IILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
iload 2
aload 3
invokevirtual android/content/res/Resources/getDrawableForDensity(IILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
areturn
.limit locals 4
.limit stack 4
.end method

.method public getFraction(III)F
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
iload 2
iload 3
invokevirtual android/content/res/Resources/getFraction(III)F
freturn
.limit locals 4
.limit stack 4
.end method

.method public getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
aload 1
aload 2
aload 3
invokevirtual android/content/res/Resources/getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method public getIntArray(I)[I
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getIntArray(I)[I
areturn
.limit locals 2
.limit stack 2
.end method

.method public getInteger(I)I
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getInteger(I)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getLayout(I)Landroid/content/res/XmlResourceParser;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getLayout(I)Landroid/content/res/XmlResourceParser;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getMovie(I)Landroid/graphics/Movie;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getMovie(I)Landroid/graphics/Movie;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getQuantityString(II)Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
iload 2
invokevirtual android/content/res/Resources/getQuantityString(II)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 3
.end method

.method public transient getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
iload 2
aload 3
invokevirtual android/content/res/Resources/getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method public getQuantityText(II)Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
iload 2
invokevirtual android/content/res/Resources/getQuantityText(II)Ljava/lang/CharSequence;
areturn
.limit locals 3
.limit stack 3
.end method

.method public getResourceEntryName(I)Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getResourceEntryName(I)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getResourceName(I)Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getResourceName(I)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getResourcePackageName(I)Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getResourcePackageName(I)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getResourceTypeName(I)Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getResourceTypeName(I)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getString(I)Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method public transient getString(I[Ljava/lang/Object;)Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
aload 2
invokevirtual android/content/res/Resources/getString(I[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 3
.end method

.method public getStringArray(I)[Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getStringArray(I)[Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getText(I)Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getText(I)Ljava/lang/CharSequence;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getText(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
aload 2
invokevirtual android/content/res/Resources/getText(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
areturn
.limit locals 3
.limit stack 3
.end method

.method public getTextArray(I)[Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getTextArray(I)[Ljava/lang/CharSequence;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getValue(ILandroid/util/TypedValue;Z)V
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
aload 2
iload 3
invokevirtual android/content/res/Resources/getValue(ILandroid/util/TypedValue;Z)V
return
.limit locals 4
.limit stack 4
.end method

.method public getValue(Ljava/lang/String;Landroid/util/TypedValue;Z)V
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
aload 1
aload 2
iload 3
invokevirtual android/content/res/Resources/getValue(Ljava/lang/String;Landroid/util/TypedValue;Z)V
return
.limit locals 4
.limit stack 4
.end method

.method public getValueForDensity(IILandroid/util/TypedValue;Z)V
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
iload 2
aload 3
iload 4
invokevirtual android/content/res/Resources/getValueForDensity(IILandroid/util/TypedValue;Z)V
return
.limit locals 5
.limit stack 5
.end method

.method public getXml(I)Landroid/content/res/XmlResourceParser;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getXml(I)Landroid/content/res/XmlResourceParser;
areturn
.limit locals 2
.limit stack 2
.end method

.method public obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
aload 1
aload 2
invokevirtual android/content/res/Resources/obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
areturn
.limit locals 3
.limit stack 3
.end method

.method public obtainTypedArray(I)Landroid/content/res/TypedArray;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/obtainTypedArray(I)Landroid/content/res/TypedArray;
areturn
.limit locals 2
.limit stack 2
.end method

.method public openRawResource(I)Ljava/io/InputStream;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/openRawResource(I)Ljava/io/InputStream;
areturn
.limit locals 2
.limit stack 2
.end method

.method public openRawResource(ILandroid/util/TypedValue;)Ljava/io/InputStream;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
aload 2
invokevirtual android/content/res/Resources/openRawResource(ILandroid/util/TypedValue;)Ljava/io/InputStream;
areturn
.limit locals 3
.limit stack 3
.end method

.method public openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;
areturn
.limit locals 2
.limit stack 2
.end method

.method public parseBundleExtra(Ljava/lang/String;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
aload 1
aload 2
aload 3
invokevirtual android/content/res/Resources/parseBundleExtra(Ljava/lang/String;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
return
.limit locals 4
.limit stack 4
.end method

.method public parseBundleExtras(Landroid/content/res/XmlResourceParser;Landroid/os/Bundle;)V
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
aload 1
aload 2
invokevirtual android/content/res/Resources/parseBundleExtras(Landroid/content/res/XmlResourceParser;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method public updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V
aload 0
aload 1
aload 2
invokespecial android/content/res/Resources/updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/aj/a Landroid/content/res/Resources;
aload 1
aload 2
invokevirtual android/content/res/Resources/updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V
L0:
return
.limit locals 3
.limit stack 3
.end method
