.bytecode 50.0
.class public synchronized android/support/v7/internal/widget/DialogTitle
.super android/widget/TextView

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/widget/TextView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 3
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/widget/TextView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 4
.limit stack 4
.end method

.method protected onMeasure(II)V
aload 0
iload 1
iload 2
invokespecial android/widget/TextView/onMeasure(II)V
aload 0
invokevirtual android/support/v7/internal/widget/DialogTitle/getLayout()Landroid/text/Layout;
astore 4
aload 4
ifnull L0
aload 4
invokevirtual android/text/Layout/getLineCount()I
istore 3
iload 3
ifle L0
aload 4
iload 3
iconst_1
isub
invokevirtual android/text/Layout/getEllipsisCount(I)I
ifle L0
aload 0
iconst_0
invokevirtual android/support/v7/internal/widget/DialogTitle/setSingleLine(Z)V
aload 0
iconst_2
invokevirtual android/support/v7/internal/widget/DialogTitle/setMaxLines(I)V
aload 0
invokevirtual android/support/v7/internal/widget/DialogTitle/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/n/TextAppearance [I
ldc_w 16842817
ldc_w 16973892
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 4
aload 4
getstatic android/support/v7/a/n/TextAppearance_android_textSize I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
istore 3
iload 3
ifeq L1
aload 0
iconst_0
iload 3
i2f
invokevirtual android/support/v7/internal/widget/DialogTitle/setTextSize(IF)V
L1:
aload 4
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
iload 1
iload 2
invokespecial android/widget/TextView/onMeasure(II)V
L0:
return
.limit locals 5
.limit stack 5
.end method
