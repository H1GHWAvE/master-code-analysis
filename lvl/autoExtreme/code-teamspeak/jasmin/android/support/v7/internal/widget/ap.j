.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/ap
.super android/support/v7/widget/aj
.implements android/view/View$OnLongClickListener

.field 'a' Landroid/support/v7/app/g;

.field final synthetic 'b' Landroid/support/v7/internal/widget/al;

.field private final 'c' [I

.field private 'd' Landroid/widget/TextView;

.field private 'e' Landroid/widget/ImageView;

.field private 'f' Landroid/view/View;

.method public <init>(Landroid/support/v7/internal/widget/al;Landroid/content/Context;Landroid/support/v7/app/g;Z)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ap/b Landroid/support/v7/internal/widget/al;
aload 0
aload 2
aconst_null
getstatic android/support/v7/a/d/actionBarTabStyle I
invokespecial android/support/v7/widget/aj/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
iconst_1
newarray int
dup
iconst_0
ldc_w 16842964
iastore
putfield android/support/v7/internal/widget/ap/c [I
aload 0
aload 3
putfield android/support/v7/internal/widget/ap/a Landroid/support/v7/app/g;
aload 2
aconst_null
aload 0
getfield android/support/v7/internal/widget/ap/c [I
getstatic android/support/v7/a/d/actionBarTabStyle I
invokestatic android/support/v7/internal/widget/ax/a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
astore 1
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(I)Z
ifeq L0
aload 0
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/widget/ap/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
L0:
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
iload 4
ifeq L1
aload 0
ldc_w 8388627
invokevirtual android/support/v7/internal/widget/ap/setGravity(I)V
L1:
aload 0
invokevirtual android/support/v7/internal/widget/ap/a()V
return
.limit locals 5
.limit stack 5
.end method

.method private a(Landroid/support/v7/app/g;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ap/a Landroid/support/v7/app/g;
aload 0
invokevirtual android/support/v7/internal/widget/ap/a()V
return
.limit locals 2
.limit stack 2
.end method

.method public final a()V
aload 0
getfield android/support/v7/internal/widget/ap/a Landroid/support/v7/app/g;
astore 2
aload 2
invokevirtual android/support/v7/app/g/d()Landroid/view/View;
astore 3
aload 3
ifnull L0
aload 3
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 2
aload 2
aload 0
if_acmpeq L1
aload 2
ifnull L2
aload 2
checkcast android/view/ViewGroup
aload 3
invokevirtual android/view/ViewGroup/removeView(Landroid/view/View;)V
L2:
aload 0
aload 3
invokevirtual android/support/v7/internal/widget/ap/addView(Landroid/view/View;)V
L1:
aload 0
aload 3
putfield android/support/v7/internal/widget/ap/f Landroid/view/View;
aload 0
getfield android/support/v7/internal/widget/ap/d Landroid/widget/TextView;
ifnull L3
aload 0
getfield android/support/v7/internal/widget/ap/d Landroid/widget/TextView;
bipush 8
invokevirtual android/widget/TextView/setVisibility(I)V
L3:
aload 0
getfield android/support/v7/internal/widget/ap/e Landroid/widget/ImageView;
ifnull L4
aload 0
getfield android/support/v7/internal/widget/ap/e Landroid/widget/ImageView;
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 0
getfield android/support/v7/internal/widget/ap/e Landroid/widget/ImageView;
aconst_null
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
L4:
return
L0:
aload 0
getfield android/support/v7/internal/widget/ap/f Landroid/view/View;
ifnull L5
aload 0
aload 0
getfield android/support/v7/internal/widget/ap/f Landroid/view/View;
invokevirtual android/support/v7/internal/widget/ap/removeView(Landroid/view/View;)V
aload 0
aconst_null
putfield android/support/v7/internal/widget/ap/f Landroid/view/View;
L5:
aload 2
invokevirtual android/support/v7/app/g/b()Landroid/graphics/drawable/Drawable;
astore 4
aload 2
invokevirtual android/support/v7/app/g/c()Ljava/lang/CharSequence;
astore 3
aload 4
ifnull L6
aload 0
getfield android/support/v7/internal/widget/ap/e Landroid/widget/ImageView;
ifnonnull L7
new android/widget/ImageView
dup
aload 0
invokevirtual android/support/v7/internal/widget/ap/getContext()Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 5
new android/support/v7/widget/al
dup
bipush -2
bipush -2
invokespecial android/support/v7/widget/al/<init>(II)V
astore 6
aload 6
bipush 16
putfield android/support/v7/widget/al/h I
aload 5
aload 6
invokevirtual android/widget/ImageView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 5
iconst_0
invokevirtual android/support/v7/internal/widget/ap/addView(Landroid/view/View;I)V
aload 0
aload 5
putfield android/support/v7/internal/widget/ap/e Landroid/widget/ImageView;
L7:
aload 0
getfield android/support/v7/internal/widget/ap/e Landroid/widget/ImageView;
aload 4
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/widget/ap/e Landroid/widget/ImageView;
iconst_0
invokevirtual android/widget/ImageView/setVisibility(I)V
L8:
aload 3
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L9
iconst_1
istore 1
L10:
iload 1
ifeq L11
aload 0
getfield android/support/v7/internal/widget/ap/d Landroid/widget/TextView;
ifnonnull L12
new android/support/v7/widget/ai
dup
aload 0
invokevirtual android/support/v7/internal/widget/ap/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/d/actionBarTabTextStyle I
invokespecial android/support/v7/widget/ai/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
astore 4
aload 4
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
new android/support/v7/widget/al
dup
bipush -2
bipush -2
invokespecial android/support/v7/widget/al/<init>(II)V
astore 5
aload 5
bipush 16
putfield android/support/v7/widget/al/h I
aload 4
aload 5
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 4
invokevirtual android/support/v7/internal/widget/ap/addView(Landroid/view/View;)V
aload 0
aload 4
putfield android/support/v7/internal/widget/ap/d Landroid/widget/TextView;
L12:
aload 0
getfield android/support/v7/internal/widget/ap/d Landroid/widget/TextView;
aload 3
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/widget/ap/d Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setVisibility(I)V
L13:
aload 0
getfield android/support/v7/internal/widget/ap/e Landroid/widget/ImageView;
ifnull L14
aload 0
getfield android/support/v7/internal/widget/ap/e Landroid/widget/ImageView;
aload 2
invokevirtual android/support/v7/app/g/g()Ljava/lang/CharSequence;
invokevirtual android/widget/ImageView/setContentDescription(Ljava/lang/CharSequence;)V
L14:
iload 1
ifne L15
aload 2
invokevirtual android/support/v7/app/g/g()Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L15
aload 0
aload 0
invokevirtual android/support/v7/internal/widget/ap/setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
return
L6:
aload 0
getfield android/support/v7/internal/widget/ap/e Landroid/widget/ImageView;
ifnull L8
aload 0
getfield android/support/v7/internal/widget/ap/e Landroid/widget/ImageView;
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 0
getfield android/support/v7/internal/widget/ap/e Landroid/widget/ImageView;
aconst_null
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
goto L8
L9:
iconst_0
istore 1
goto L10
L11:
aload 0
getfield android/support/v7/internal/widget/ap/d Landroid/widget/TextView;
ifnull L13
aload 0
getfield android/support/v7/internal/widget/ap/d Landroid/widget/TextView;
bipush 8
invokevirtual android/widget/TextView/setVisibility(I)V
aload 0
getfield android/support/v7/internal/widget/ap/d Landroid/widget/TextView;
aconst_null
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
goto L13
L15:
aload 0
aconst_null
invokevirtual android/support/v7/internal/widget/ap/setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
aload 0
iconst_0
invokevirtual android/support/v7/internal/widget/ap/setLongClickable(Z)V
return
.limit locals 7
.limit stack 5
.end method

.method public final getTab()Landroid/support/v7/app/g;
aload 0
getfield android/support/v7/internal/widget/ap/a Landroid/support/v7/app/g;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
aload 1
invokespecial android/support/v7/widget/aj/onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
aload 1
ldc android/support/v7/app/g
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
aload 0
aload 1
invokespecial android/support/v7/widget/aj/onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
aload 1
ldc android/support/v7/app/g
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityNodeInfo/setClassName(Ljava/lang/CharSequence;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final onLongClick(Landroid/view/View;)Z
iconst_2
newarray int
astore 1
aload 0
aload 1
invokevirtual android/support/v7/internal/widget/ap/getLocationOnScreen([I)V
aload 0
invokevirtual android/support/v7/internal/widget/ap/getContext()Landroid/content/Context;
astore 5
aload 0
invokevirtual android/support/v7/internal/widget/ap/getWidth()I
istore 2
aload 0
invokevirtual android/support/v7/internal/widget/ap/getHeight()I
istore 3
aload 5
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
istore 4
aload 5
aload 0
getfield android/support/v7/internal/widget/ap/a Landroid/support/v7/app/g;
invokevirtual android/support/v7/app/g/g()Ljava/lang/CharSequence;
iconst_0
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
astore 5
aload 5
bipush 49
aload 1
iconst_0
iaload
iload 2
iconst_2
idiv
iadd
iload 4
iconst_2
idiv
isub
iload 3
invokevirtual android/widget/Toast/setGravity(III)V
aload 5
invokevirtual android/widget/Toast/show()V
iconst_1
ireturn
.limit locals 6
.limit stack 5
.end method

.method public final onMeasure(II)V
aload 0
iload 1
iload 2
invokespecial android/support/v7/widget/aj/onMeasure(II)V
aload 0
getfield android/support/v7/internal/widget/ap/b Landroid/support/v7/internal/widget/al;
getfield android/support/v7/internal/widget/al/e I
ifle L0
aload 0
invokevirtual android/support/v7/internal/widget/ap/getMeasuredWidth()I
aload 0
getfield android/support/v7/internal/widget/ap/b Landroid/support/v7/internal/widget/al;
getfield android/support/v7/internal/widget/al/e I
if_icmple L0
aload 0
aload 0
getfield android/support/v7/internal/widget/ap/b Landroid/support/v7/internal/widget/al;
getfield android/support/v7/internal/widget/al/e I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 2
invokespecial android/support/v7/widget/aj/onMeasure(II)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method public final setSelected(Z)V
aload 0
invokevirtual android/support/v7/internal/widget/ap/isSelected()Z
iload 1
if_icmpeq L0
iconst_1
istore 2
L1:
aload 0
iload 1
invokespecial android/support/v7/widget/aj/setSelected(Z)V
iload 2
ifeq L2
iload 1
ifeq L2
aload 0
iconst_4
invokevirtual android/support/v7/internal/widget/ap/sendAccessibilityEvent(I)V
L2:
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method
