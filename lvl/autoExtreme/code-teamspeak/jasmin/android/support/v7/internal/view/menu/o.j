.bytecode 50.0
.class public synchronized android/support/v7/internal/view/menu/o
.super android/support/v7/internal/view/menu/e
.implements android/view/MenuItem
.annotation invisible Landroid/annotation/TargetApi;
value I = 14
.end annotation

.field static final 'e' Ljava/lang/String; = "MenuItemWrapper"

.field public 'f' Ljava/lang/reflect/Method;

.method <init>(Landroid/content/Context;Landroid/support/v4/g/a/b;)V
aload 0
aload 1
aload 2
invokespecial android/support/v7/internal/view/menu/e/<init>(Landroid/content/Context;Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 3
.end method

.method private b()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
L0:
aload 0
getfield android/support/v7/internal/view/menu/o/f Ljava/lang/reflect/Method;
ifnonnull L1
aload 0
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
ldc "setExclusiveCheckable"
iconst_1
anewarray java/lang/Class
dup
iconst_0
getstatic java/lang/Boolean/TYPE Ljava/lang/Class;
aastore
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v7/internal/view/menu/o/f Ljava/lang/reflect/Method;
L1:
aload 0
getfield android/support/v7/internal/view/menu/o/f Ljava/lang/reflect/Method;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
iconst_1
anewarray java/lang/Object
dup
iconst_0
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L3:
return
L2:
astore 1
ldc "MenuItemWrapper"
ldc "Error while calling setExclusiveCheckable"
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 2
.limit stack 7
.end method

.method a(Landroid/view/ActionProvider;)Landroid/support/v7/internal/view/menu/p;
new android/support/v7/internal/view/menu/p
dup
aload 0
aload 0
getfield android/support/v7/internal/view/menu/o/a Landroid/content/Context;
aload 1
invokespecial android/support/v7/internal/view/menu/p/<init>(Landroid/support/v7/internal/view/menu/o;Landroid/content/Context;Landroid/view/ActionProvider;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public collapseActionView()Z
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/collapseActionView()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public expandActionView()Z
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/expandActionView()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/a()Landroid/support/v4/view/n; 0
astore 1
aload 1
instanceof android/support/v7/internal/view/menu/p
ifeq L0
aload 1
checkcast android/support/v7/internal/view/menu/p
getfield android/support/v7/internal/view/menu/p/c Landroid/view/ActionProvider;
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public getActionView()Landroid/view/View;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getActionView()Landroid/view/View; 0
astore 2
aload 2
astore 1
aload 2
instanceof android/support/v7/internal/view/menu/q
ifeq L0
aload 2
checkcast android/support/v7/internal/view/menu/q
getfield android/support/v7/internal/view/menu/q/a Landroid/view/CollapsibleActionView;
checkcast android/view/View
astore 1
L0:
aload 1
areturn
.limit locals 3
.limit stack 1
.end method

.method public getAlphabeticShortcut()C
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getAlphabeticShortcut()C 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getGroupId()I
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getGroupId()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getIcon()Landroid/graphics/drawable/Drawable; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public getIntent()Landroid/content/Intent;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getIntent()Landroid/content/Intent; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public getItemId()I
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getItemId()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public getNumericShortcut()C
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getNumericShortcut()C 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOrder()I
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getOrder()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSubMenu()Landroid/view/SubMenu;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getSubMenu()Landroid/view/SubMenu; 0
invokevirtual android/support/v7/internal/view/menu/o/a(Landroid/view/SubMenu;)Landroid/view/SubMenu;
areturn
.limit locals 1
.limit stack 2
.end method

.method public getTitle()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getTitle()Ljava/lang/CharSequence; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getTitleCondensed()Ljava/lang/CharSequence; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public hasSubMenu()Z
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/hasSubMenu()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isActionViewExpanded()Z
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/isActionViewExpanded()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isCheckable()Z
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/isCheckable()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isChecked()Z
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/isChecked()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isEnabled()Z
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/isEnabled()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isVisible()Z
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/isVisible()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
astore 2
aload 1
ifnull L0
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/o/a(Landroid/view/ActionProvider;)Landroid/support/v7/internal/view/menu/p;
astore 1
L1:
aload 2
aload 1
invokeinterface android/support/v4/g/a/b/a(Landroid/support/v4/view/n;)Landroid/support/v4/g/a/b; 1
pop
aload 0
areturn
L0:
aconst_null
astore 1
goto L1
.limit locals 3
.limit stack 2
.end method

.method public setActionView(I)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setActionView(I)Landroid/view/MenuItem; 1
pop
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getActionView()Landroid/view/View; 0
astore 2
aload 2
instanceof android/view/CollapsibleActionView
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
new android/support/v7/internal/view/menu/q
dup
aload 2
invokespecial android/support/v7/internal/view/menu/q/<init>(Landroid/view/View;)V
invokeinterface android/support/v4/g/a/b/setActionView(Landroid/view/View;)Landroid/view/MenuItem; 1
pop
L0:
aload 0
areturn
.limit locals 3
.limit stack 4
.end method

.method public setActionView(Landroid/view/View;)Landroid/view/MenuItem;
aload 1
astore 2
aload 1
instanceof android/view/CollapsibleActionView
ifeq L0
new android/support/v7/internal/view/menu/q
dup
aload 1
invokespecial android/support/v7/internal/view/menu/q/<init>(Landroid/view/View;)V
astore 2
L0:
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
aload 2
invokeinterface android/support/v4/g/a/b/setActionView(Landroid/view/View;)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setAlphabeticShortcut(C)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setCheckable(Z)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setChecked(Z)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setEnabled(Z)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setIcon(I)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setIcon(I)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
aload 1
invokeinterface android/support/v4/g/a/b/setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
aload 1
invokeinterface android/support/v4/g/a/b/setIntent(Landroid/content/Intent;)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setNumericShortcut(C)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
astore 2
aload 1
ifnull L0
new android/support/v7/internal/view/menu/r
dup
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/r/<init>(Landroid/support/v7/internal/view/menu/o;Landroid/view/MenuItem$OnActionExpandListener;)V
astore 1
L1:
aload 2
aload 1
invokeinterface android/support/v4/g/a/b/a(Landroid/support/v4/view/bf;)Landroid/support/v4/g/a/b; 1
pop
aload 0
areturn
L0:
aconst_null
astore 1
goto L1
.limit locals 3
.limit stack 4
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
astore 2
aload 1
ifnull L0
new android/support/v7/internal/view/menu/s
dup
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/s/<init>(Landroid/support/v7/internal/view/menu/o;Landroid/view/MenuItem$OnMenuItemClickListener;)V
astore 1
L1:
aload 2
aload 1
invokeinterface android/support/v4/g/a/b/setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem; 1
pop
aload 0
areturn
L0:
aconst_null
astore 1
goto L1
.limit locals 3
.limit stack 4
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
iload 1
iload 2
invokeinterface android/support/v4/g/a/b/setShortcut(CC)Landroid/view/MenuItem; 2
pop
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public setShowAsAction(I)V
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setShowAsAction(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public setShowAsActionFlags(I)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setShowAsActionFlags(I)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setTitle(I)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setTitle(I)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
aload 1
invokeinterface android/support/v4/g/a/b/setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
aload 1
invokeinterface android/support/v4/g/a/b/setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/o/d Ljava/lang/Object;
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setVisible(Z)Landroid/view/MenuItem; 1
areturn
.limit locals 2
.limit stack 2
.end method
