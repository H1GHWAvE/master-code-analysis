.bytecode 50.0
.class public synchronized android/support/v7/b/a/c
.super android/graphics/drawable/Drawable

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field public static final 'd' I = 3


.field private static final 'g' F

.field public 'e' F

.field private final 'f' Landroid/graphics/Paint;

.field private 'h' F

.field private 'i' F

.field private 'j' F

.field private 'k' F

.field private 'l' Z

.field private final 'm' Landroid/graphics/Path;

.field private final 'n' I

.field private 'o' Z

.field private 'p' F

.field private 'q' I

.method static <clinit>()V
ldc2_w 45.0D
invokestatic java/lang/Math/toRadians(D)D
d2f
putstatic android/support/v7/b/a/c/g F
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial android/graphics/drawable/Drawable/<init>()V
aload 0
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
aload 0
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
putfield android/support/v7/b/a/c/m Landroid/graphics/Path;
aload 0
iconst_0
putfield android/support/v7/b/a/c/o Z
aload 0
iconst_2
putfield android/support/v7/b/a/c/q I
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
getstatic android/graphics/Paint$Style/STROKE Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
getstatic android/graphics/Paint$Join/MITER Landroid/graphics/Paint$Join;
invokevirtual android/graphics/Paint/setStrokeJoin(Landroid/graphics/Paint$Join;)V
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
getstatic android/graphics/Paint$Cap/BUTT Landroid/graphics/Paint$Cap;
invokevirtual android/graphics/Paint/setStrokeCap(Landroid/graphics/Paint$Cap;)V
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
iconst_1
invokevirtual android/graphics/Paint/setAntiAlias(Z)V
aload 1
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
aconst_null
getstatic android/support/v7/a/n/DrawerArrowToggle [I
getstatic android/support/v7/a/d/drawerArrowStyle I
getstatic android/support/v7/a/m/Base_Widget_AppCompat_DrawerArrowToggle I
invokevirtual android/content/res/Resources$Theme/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 1
getstatic android/support/v7/a/n/DrawerArrowToggle_color I
iconst_0
invokevirtual android/content/res/TypedArray/getColor(II)I
istore 3
iload 3
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getColor()I
if_icmpeq L0
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
iload 3
invokevirtual android/graphics/Paint/setColor(I)V
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L0:
aload 1
getstatic android/support/v7/a/n/DrawerArrowToggle_thickness I
fconst_0
invokevirtual android/content/res/TypedArray/getDimension(IF)F
fstore 2
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getStrokeWidth()F
fload 2
fcmpl
ifeq L1
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
fload 2
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 0
fload 2
fconst_2
fdiv
f2d
getstatic android/support/v7/b/a/c/g F
f2d
invokestatic java/lang/Math/cos(D)D
dmul
d2f
putfield android/support/v7/b/a/c/p F
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L1:
aload 1
getstatic android/support/v7/a/n/DrawerArrowToggle_spinBars I
iconst_1
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
istore 4
aload 0
getfield android/support/v7/b/a/c/l Z
iload 4
if_icmpeq L2
aload 0
iload 4
putfield android/support/v7/b/a/c/l Z
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L2:
aload 1
getstatic android/support/v7/a/n/DrawerArrowToggle_gapBetweenBars I
fconst_0
invokevirtual android/content/res/TypedArray/getDimension(IF)F
invokestatic java/lang/Math/round(F)I
i2f
fstore 2
fload 2
aload 0
getfield android/support/v7/b/a/c/k F
fcmpl
ifeq L3
aload 0
fload 2
putfield android/support/v7/b/a/c/k F
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L3:
aload 0
aload 1
getstatic android/support/v7/a/n/DrawerArrowToggle_drawableSize I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/v7/b/a/c/n I
aload 0
aload 1
getstatic android/support/v7/a/n/DrawerArrowToggle_barLength I
fconst_0
invokevirtual android/content/res/TypedArray/getDimension(IF)F
invokestatic java/lang/Math/round(F)I
i2f
putfield android/support/v7/b/a/c/i F
aload 0
aload 1
getstatic android/support/v7/a/n/DrawerArrowToggle_arrowHeadLength I
fconst_0
invokevirtual android/content/res/TypedArray/getDimension(IF)F
invokestatic java/lang/Math/round(F)I
i2f
putfield android/support/v7/b/a/c/h F
aload 0
aload 1
getstatic android/support/v7/a/n/DrawerArrowToggle_arrowShaftLength I
fconst_0
invokevirtual android/content/res/TypedArray/getDimension(IF)F
putfield android/support/v7/b/a/c/j F
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 5
.limit stack 5
.end method

.method private a()F
aload 0
getfield android/support/v7/b/a/c/h F
freturn
.limit locals 1
.limit stack 1
.end method

.method private static a(FFF)F
fload 1
fload 0
fsub
fload 2
fmul
fload 0
fadd
freturn
.limit locals 3
.limit stack 2
.end method

.method private a(F)V
aload 0
getfield android/support/v7/b/a/c/h F
fload 1
fcmpl
ifeq L0
aload 0
fload 1
putfield android/support/v7/b/a/c/h F
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private a(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
iload 1
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getColor()I
if_icmpeq L0
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
iload 1
invokevirtual android/graphics/Paint/setColor(I)V
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private b()F
aload 0
getfield android/support/v7/b/a/c/j F
freturn
.limit locals 1
.limit stack 1
.end method

.method private b(F)V
aload 0
getfield android/support/v7/b/a/c/j F
fload 1
fcmpl
ifeq L0
aload 0
fload 1
putfield android/support/v7/b/a/c/j F
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private b(I)V
iload 1
aload 0
getfield android/support/v7/b/a/c/q I
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/b/a/c/q I
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private b(Z)V
aload 0
getfield android/support/v7/b/a/c/l Z
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/b/a/c/l Z
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private c()F
aload 0
getfield android/support/v7/b/a/c/i F
freturn
.limit locals 1
.limit stack 1
.end method

.method private c(F)V
aload 0
getfield android/support/v7/b/a/c/i F
fload 1
fcmpl
ifeq L0
aload 0
fload 1
putfield android/support/v7/b/a/c/i F
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private d()I
.annotation invisible Landroid/support/a/j;
.end annotation
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getColor()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d(F)V
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getStrokeWidth()F
fload 1
fcmpl
ifeq L0
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
fload 1
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 0
fload 1
fconst_2
fdiv
f2d
getstatic android/support/v7/b/a/c/g F
f2d
invokestatic java/lang/Math/cos(D)D
dmul
d2f
putfield android/support/v7/b/a/c/p F
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method private e()F
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getStrokeWidth()F
freturn
.limit locals 1
.limit stack 1
.end method

.method private e(F)V
fload 1
aload 0
getfield android/support/v7/b/a/c/k F
fcmpl
ifeq L0
aload 0
fload 1
putfield android/support/v7/b/a/c/k F
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private f()F
aload 0
getfield android/support/v7/b/a/c/k F
freturn
.limit locals 1
.limit stack 1
.end method

.method private f(F)V
.annotation invisibleparam 1 Landroid/support/a/n;
a D = 0.0D
b D = 1.0D
.end annotation
aload 0
getfield android/support/v7/b/a/c/e F
fload 1
fcmpl
ifeq L0
aload 0
fload 1
putfield android/support/v7/b/a/c/e F
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private g()Z
aload 0
getfield android/support/v7/b/a/c/l Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()I
aload 0
getfield android/support/v7/b/a/c/q I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i()F
.annotation invisible Landroid/support/a/n;
a D = 0.0D
b D = 1.0D
.end annotation
aload 0
getfield android/support/v7/b/a/c/e F
freturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Z)V
aload 0
getfield android/support/v7/b/a/c/o Z
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/b/a/c/o Z
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public draw(Landroid/graphics/Canvas;)V
aload 0
invokevirtual android/support/v7/b/a/c/getBounds()Landroid/graphics/Rect;
astore 12
aload 0
getfield android/support/v7/b/a/c/q I
tableswitch 0
L0
L1
L2
L3
default : L2
L2:
aload 0
invokestatic android/support/v4/e/a/a/e(Landroid/graphics/drawable/Drawable;)I
iconst_1
if_icmpne L4
iconst_1
istore 11
L5:
aload 0
getfield android/support/v7/b/a/c/h F
aload 0
getfield android/support/v7/b/a/c/h F
fmul
fconst_2
fmul
f2d
invokestatic java/lang/Math/sqrt(D)D
d2f
fstore 2
aload 0
getfield android/support/v7/b/a/c/i F
fstore 3
fload 3
fload 2
fload 3
fsub
aload 0
getfield android/support/v7/b/a/c/e F
fmul
fadd
fstore 7
aload 0
getfield android/support/v7/b/a/c/i F
fstore 2
fload 2
aload 0
getfield android/support/v7/b/a/c/j F
fload 2
fsub
aload 0
getfield android/support/v7/b/a/c/e F
fmul
fadd
fstore 4
fconst_0
aload 0
getfield android/support/v7/b/a/c/p F
fconst_0
fsub
aload 0
getfield android/support/v7/b/a/c/e F
fmul
fadd
invokestatic java/lang/Math/round(F)I
i2f
fstore 5
fconst_0
getstatic android/support/v7/b/a/c/g F
fconst_0
fsub
aload 0
getfield android/support/v7/b/a/c/e F
fmul
fadd
fstore 8
iload 11
ifeq L6
fconst_0
fstore 2
L7:
iload 11
ifeq L8
ldc_w 180.0F
fstore 3
L9:
aload 0
getfield android/support/v7/b/a/c/e F
fstore 6
fload 7
f2d
fload 8
f2d
invokestatic java/lang/Math/cos(D)D
dmul
invokestatic java/lang/Math/round(D)J
l2f
fstore 9
fload 7
f2d
fload 8
f2d
invokestatic java/lang/Math/sin(D)D
dmul
invokestatic java/lang/Math/round(D)J
l2f
fstore 7
aload 0
getfield android/support/v7/b/a/c/m Landroid/graphics/Path;
invokevirtual android/graphics/Path/rewind()V
aload 0
getfield android/support/v7/b/a/c/k F
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getStrokeWidth()F
fadd
fstore 8
fload 8
aload 0
getfield android/support/v7/b/a/c/p F
fneg
fload 8
fsub
aload 0
getfield android/support/v7/b/a/c/e F
fmul
fadd
fstore 8
fload 4
fneg
fconst_2
fdiv
fstore 10
aload 0
getfield android/support/v7/b/a/c/m Landroid/graphics/Path;
fload 10
fload 5
fadd
fconst_0
invokevirtual android/graphics/Path/moveTo(FF)V
aload 0
getfield android/support/v7/b/a/c/m Landroid/graphics/Path;
fload 4
fload 5
fconst_2
fmul
fsub
fconst_0
invokevirtual android/graphics/Path/rLineTo(FF)V
aload 0
getfield android/support/v7/b/a/c/m Landroid/graphics/Path;
fload 10
fload 8
invokevirtual android/graphics/Path/moveTo(FF)V
aload 0
getfield android/support/v7/b/a/c/m Landroid/graphics/Path;
fload 9
fload 7
invokevirtual android/graphics/Path/rLineTo(FF)V
aload 0
getfield android/support/v7/b/a/c/m Landroid/graphics/Path;
fload 10
fload 8
fneg
invokevirtual android/graphics/Path/moveTo(FF)V
aload 0
getfield android/support/v7/b/a/c/m Landroid/graphics/Path;
fload 9
fload 7
fneg
invokevirtual android/graphics/Path/rLineTo(FF)V
aload 0
getfield android/support/v7/b/a/c/m Landroid/graphics/Path;
invokevirtual android/graphics/Path/close()V
aload 1
invokevirtual android/graphics/Canvas/save()I
pop
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getStrokeWidth()F
fstore 4
aload 12
invokevirtual android/graphics/Rect/height()I
i2f
ldc_w 3.0F
fload 4
fmul
fsub
aload 0
getfield android/support/v7/b/a/c/k F
fconst_2
fmul
fsub
f2i
iconst_4
idiv
iconst_2
imul
i2f
f2d
fload 4
f2d
ldc2_w 1.5D
dmul
aload 0
getfield android/support/v7/b/a/c/k F
f2d
dadd
dadd
d2f
fstore 4
aload 1
aload 12
invokevirtual android/graphics/Rect/centerX()I
i2f
fload 4
invokevirtual android/graphics/Canvas/translate(FF)V
aload 0
getfield android/support/v7/b/a/c/l Z
ifeq L10
iload 11
aload 0
getfield android/support/v7/b/a/c/o Z
ixor
ifeq L11
iconst_m1
istore 11
L12:
aload 1
iload 11
i2f
fload 3
fload 2
fsub
fload 6
fmul
fload 2
fadd
fmul
invokevirtual android/graphics/Canvas/rotate(F)V
L13:
aload 1
aload 0
getfield android/support/v7/b/a/c/m Landroid/graphics/Path;
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
aload 1
invokevirtual android/graphics/Canvas/restore()V
return
L0:
iconst_0
istore 11
goto L5
L1:
iconst_1
istore 11
goto L5
L3:
aload 0
invokestatic android/support/v4/e/a/a/e(Landroid/graphics/drawable/Drawable;)I
ifne L14
iconst_1
istore 11
goto L5
L14:
iconst_0
istore 11
goto L5
L4:
iconst_0
istore 11
goto L5
L6:
ldc_w -180.0F
fstore 2
goto L7
L8:
fconst_0
fstore 3
goto L9
L11:
iconst_1
istore 11
goto L12
L10:
iload 11
ifeq L13
aload 1
ldc_w 180.0F
invokevirtual android/graphics/Canvas/rotate(F)V
goto L13
.limit locals 13
.limit stack 6
.end method

.method public getIntrinsicHeight()I
aload 0
getfield android/support/v7/b/a/c/n I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getIntrinsicWidth()I
aload 0
getfield android/support/v7/b/a/c/n I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOpacity()I
bipush -3
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setAlpha(I)V
iload 1
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getAlpha()I
if_icmpeq L0
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
iload 1
invokevirtual android/graphics/Paint/setAlpha(I)V
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
aload 0
getfield android/support/v7/b/a/c/f Landroid/graphics/Paint;
aload 1
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
aload 0
invokevirtual android/support/v7/b/a/c/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method
