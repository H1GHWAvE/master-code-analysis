.bytecode 50.0
.class public final synchronized net/hockeyapp/android/d/p
.super net/hockeyapp/android/d/k

.field public 'a' Landroid/content/Context;

.field public 'b' Landroid/os/Handler;

.field public 'c' Landroid/app/ProgressDialog;

.field public 'd' Z

.field private final 'e' I

.field private final 'f' Ljava/lang/String;

.field private final 'g' Ljava/util/Map;

.method public <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;ILjava/util/Map;)V
aload 0
invokespecial net/hockeyapp/android/d/k/<init>()V
aload 0
aload 1
putfield net/hockeyapp/android/d/p/a Landroid/content/Context;
aload 0
aload 2
putfield net/hockeyapp/android/d/p/b Landroid/os/Handler;
aload 0
aload 3
putfield net/hockeyapp/android/d/p/f Ljava/lang/String;
aload 0
iload 4
putfield net/hockeyapp/android/d/p/e I
aload 0
aload 5
putfield net/hockeyapp/android/d/p/g Ljava/util/Map;
aload 0
iconst_1
putfield net/hockeyapp/android/d/p/d Z
aload 1
ifnull L0
aload 1
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;)V
L0:
return
.limit locals 6
.limit stack 2
.end method

.method private a(ILjava/util/Map;)Ljava/net/HttpURLConnection;
iload 1
iconst_1
if_icmpne L0
new net/hockeyapp/android/e/l
dup
aload 0
getfield net/hockeyapp/android/d/p/f Ljava/lang/String;
invokespecial net/hockeyapp/android/e/l/<init>(Ljava/lang/String;)V
astore 3
aload 3
ldc "POST"
putfield net/hockeyapp/android/e/l/b Ljava/lang/String;
aload 3
aload 2
invokevirtual net/hockeyapp/android/e/l/a(Ljava/util/Map;)Lnet/hockeyapp/android/e/l;
invokevirtual net/hockeyapp/android/e/l/a()Ljava/net/HttpURLConnection;
areturn
L0:
iload 1
iconst_2
if_icmpne L1
new net/hockeyapp/android/e/l
dup
aload 0
getfield net/hockeyapp/android/d/p/f Ljava/lang/String;
invokespecial net/hockeyapp/android/e/l/<init>(Ljava/lang/String;)V
astore 3
aload 3
ldc "POST"
putfield net/hockeyapp/android/e/l/b Ljava/lang/String;
aload 2
ldc "email"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 4
aload 2
ldc "password"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 2
aload 3
ldc "Authorization"
new java/lang/StringBuilder
dup
ldc "Basic "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokestatic net/hockeyapp/android/e/b/a([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual net/hockeyapp/android/e/l/a(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
pop
aload 3
invokevirtual net/hockeyapp/android/e/l/a()Ljava/net/HttpURLConnection;
areturn
L1:
iload 1
iconst_3
if_icmpne L2
aload 2
ldc "type"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 3
aload 2
ldc "id"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 2
new net/hockeyapp/android/e/l
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/d/p/f Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/hockeyapp/android/e/l/<init>(Ljava/lang/String;)V
invokevirtual net/hockeyapp/android/e/l/a()Ljava/net/HttpURLConnection;
areturn
L2:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Login mode "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " not supported."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 5
.limit stack 5
.end method

.method private a()V
aload 0
iconst_0
putfield net/hockeyapp/android/d/p/d Z
return
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/content/Context;Landroid/os/Handler;)V
aload 0
aload 1
putfield net/hockeyapp/android/d/p/a Landroid/content/Context;
aload 0
aload 2
putfield net/hockeyapp/android/d/p/b Landroid/os/Handler;
return
.limit locals 3
.limit stack 2
.end method

.method private a(Ljava/lang/Boolean;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield net/hockeyapp/android/d/p/c Landroid/app/ProgressDialog;
ifnull L1
L0:
aload 0
getfield net/hockeyapp/android/d/p/c Landroid/app/ProgressDialog;
invokevirtual android/app/ProgressDialog/dismiss()V
L1:
aload 0
getfield net/hockeyapp/android/d/p/b Landroid/os/Handler;
ifnull L3
new android/os/Message
dup
invokespecial android/os/Message/<init>()V
astore 2
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 3
aload 3
ldc "success"
aload 1
invokevirtual java/lang/Boolean/booleanValue()Z
invokevirtual android/os/Bundle/putBoolean(Ljava/lang/String;Z)V
aload 2
aload 3
invokevirtual android/os/Message/setData(Landroid/os/Bundle;)V
aload 0
getfield net/hockeyapp/android/d/p/b Landroid/os/Handler;
aload 2
invokevirtual android/os/Handler/sendMessage(Landroid/os/Message;)Z
pop
L3:
return
L2:
astore 2
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
goto L1
.limit locals 4
.limit stack 3
.end method

.method private a(Ljava/lang/String;)Z
.catch org/json/JSONException from L0 to L1 using L2
.catch org/json/JSONException from L3 to L4 using L2
.catch org/json/JSONException from L5 to L6 using L2
.catch org/json/JSONException from L7 to L8 using L2
.catch org/json/JSONException from L9 to L10 using L2
.catch org/json/JSONException from L11 to L12 using L2
aload 0
getfield net/hockeyapp/android/d/p/a Landroid/content/Context;
ldc "net.hockeyapp.android.login"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 2
L0:
new org/json/JSONObject
dup
aload 1
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
astore 1
aload 1
ldc "status"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 3
aload 3
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L3
L1:
iconst_0
ireturn
L3:
aload 0
getfield net/hockeyapp/android/d/p/e I
iconst_1
if_icmpne L5
aload 3
ldc "identified"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L12
aload 1
ldc "iuid"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L12
aload 2
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "iuid"
aload 1
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L4:
iconst_1
ireturn
L5:
aload 0
getfield net/hockeyapp/android/d/p/e I
iconst_2
if_icmpne L7
aload 3
ldc "authorized"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L12
aload 1
ldc "auid"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L12
aload 2
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "auid"
aload 1
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L6:
iconst_1
ireturn
L7:
aload 0
getfield net/hockeyapp/android/d/p/e I
iconst_3
if_icmpne L11
aload 3
ldc "validated"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L9
L8:
iconst_1
ireturn
L9:
aload 2
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "iuid"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
ldc "auid"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L10:
iconst_0
ireturn
L2:
astore 1
aload 1
invokevirtual org/json/JSONException/printStackTrace()V
iconst_0
ireturn
L11:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Login mode "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/d/p/e I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " not supported."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L12:
iconst_0
ireturn
.limit locals 4
.limit stack 5
.end method

.method private b()V
aload 0
aconst_null
putfield net/hockeyapp/android/d/p/a Landroid/content/Context;
aload 0
aconst_null
putfield net/hockeyapp/android/d/p/b Landroid/os/Handler;
aload 0
aconst_null
putfield net/hockeyapp/android/d/p/c Landroid/app/ProgressDialog;
return
.limit locals 1
.limit stack 2
.end method

.method private transient c()Ljava/lang/Boolean;
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch all from L0 to L1 using L4
.catch java/io/UnsupportedEncodingException from L5 to L6 using L2
.catch java/io/IOException from L5 to L6 using L3
.catch all from L5 to L6 using L4
.catch java/io/UnsupportedEncodingException from L7 to L8 using L2
.catch java/io/IOException from L7 to L8 using L3
.catch all from L7 to L8 using L4
.catch java/io/UnsupportedEncodingException from L9 to L10 using L2
.catch java/io/IOException from L9 to L10 using L3
.catch all from L9 to L10 using L4
.catch java/io/UnsupportedEncodingException from L11 to L12 using L2
.catch java/io/IOException from L11 to L12 using L3
.catch all from L11 to L12 using L4
.catch java/io/UnsupportedEncodingException from L13 to L14 using L15
.catch java/io/IOException from L13 to L14 using L16
.catch all from L13 to L14 using L17
.catch java/io/UnsupportedEncodingException from L18 to L19 using L15
.catch java/io/IOException from L18 to L19 using L16
.catch all from L18 to L19 using L17
.catch java/io/UnsupportedEncodingException from L20 to L21 using L15
.catch java/io/IOException from L20 to L21 using L16
.catch all from L20 to L21 using L17
.catch java/io/UnsupportedEncodingException from L22 to L23 using L15
.catch java/io/IOException from L22 to L23 using L16
.catch all from L22 to L23 using L17
.catch java/io/UnsupportedEncodingException from L24 to L25 using L15
.catch java/io/IOException from L24 to L25 using L16
.catch all from L24 to L25 using L17
.catch java/io/UnsupportedEncodingException from L26 to L27 using L2
.catch java/io/IOException from L26 to L27 using L3
.catch all from L26 to L27 using L4
.catch java/io/UnsupportedEncodingException from L28 to L29 using L2
.catch java/io/IOException from L28 to L29 using L3
.catch all from L28 to L29 using L4
.catch java/io/UnsupportedEncodingException from L30 to L31 using L2
.catch java/io/IOException from L30 to L31 using L3
.catch all from L30 to L31 using L4
.catch java/io/UnsupportedEncodingException from L32 to L33 using L2
.catch java/io/IOException from L32 to L33 using L3
.catch all from L32 to L33 using L4
.catch java/io/UnsupportedEncodingException from L34 to L35 using L2
.catch java/io/IOException from L34 to L35 using L3
.catch all from L34 to L35 using L4
.catch java/io/UnsupportedEncodingException from L36 to L37 using L2
.catch java/io/IOException from L36 to L37 using L3
.catch all from L36 to L37 using L4
.catch java/io/UnsupportedEncodingException from L38 to L39 using L2
.catch java/io/IOException from L38 to L39 using L3
.catch all from L38 to L39 using L4
.catch java/io/UnsupportedEncodingException from L40 to L41 using L2
.catch java/io/IOException from L40 to L41 using L3
.catch all from L40 to L41 using L4
.catch java/io/UnsupportedEncodingException from L42 to L43 using L2
.catch java/io/IOException from L42 to L43 using L3
.catch all from L42 to L43 using L4
.catch java/io/UnsupportedEncodingException from L44 to L2 using L2
.catch java/io/IOException from L44 to L2 using L3
.catch all from L44 to L2 using L4
.catch all from L45 to L46 using L17
.catch all from L47 to L48 using L4
aconst_null
astore 5
aconst_null
astore 4
aload 5
astore 3
L0:
aload 0
getfield net/hockeyapp/android/d/p/e I
istore 1
L1:
aload 5
astore 3
L5:
aload 0
getfield net/hockeyapp/android/d/p/g Ljava/util/Map;
astore 6
L6:
iload 1
iconst_1
if_icmpne L49
aload 5
astore 3
L7:
new net/hockeyapp/android/e/l
dup
aload 0
getfield net/hockeyapp/android/d/p/f Ljava/lang/String;
invokespecial net/hockeyapp/android/e/l/<init>(Ljava/lang/String;)V
astore 7
L8:
aload 5
astore 3
L9:
aload 7
ldc "POST"
putfield net/hockeyapp/android/e/l/b Ljava/lang/String;
L10:
aload 5
astore 3
L11:
aload 7
aload 6
invokevirtual net/hockeyapp/android/e/l/a(Ljava/util/Map;)Lnet/hockeyapp/android/e/l;
invokevirtual net/hockeyapp/android/e/l/a()Ljava/net/HttpURLConnection;
astore 5
L12:
aload 5
astore 3
L50:
aload 3
astore 4
L13:
aload 3
invokevirtual java/net/HttpURLConnection/connect()V
L14:
aload 3
astore 4
L18:
aload 3
invokevirtual java/net/HttpURLConnection/getResponseCode()I
sipush 200
if_icmpne L51
L19:
aload 3
astore 4
L20:
aload 3
invokestatic net/hockeyapp/android/d/p/a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
astore 5
L21:
aload 3
astore 4
L22:
aload 5
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L51
L23:
aload 3
astore 4
L24:
aload 0
aload 5
invokespecial net/hockeyapp/android/d/p/a(Ljava/lang/String;)Z
istore 2
L25:
aload 3
ifnull L52
aload 3
invokevirtual java/net/HttpURLConnection/disconnect()V
L52:
iload 2
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L49:
iload 1
iconst_2
if_icmpne L53
aload 5
astore 3
L26:
new net/hockeyapp/android/e/l
dup
aload 0
getfield net/hockeyapp/android/d/p/f Ljava/lang/String;
invokespecial net/hockeyapp/android/e/l/<init>(Ljava/lang/String;)V
astore 7
L27:
aload 5
astore 3
L28:
aload 7
ldc "POST"
putfield net/hockeyapp/android/e/l/b Ljava/lang/String;
L29:
aload 5
astore 3
L30:
aload 6
ldc "email"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 8
L31:
aload 5
astore 3
L32:
aload 6
ldc "password"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 6
L33:
aload 5
astore 3
L34:
aload 7
ldc "Authorization"
new java/lang/StringBuilder
dup
ldc "Basic "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokestatic net/hockeyapp/android/e/b/a([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual net/hockeyapp/android/e/l/a(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
pop
L35:
aload 5
astore 3
L36:
aload 7
invokevirtual net/hockeyapp/android/e/l/a()Ljava/net/HttpURLConnection;
astore 5
L37:
aload 5
astore 3
goto L50
L53:
iload 1
iconst_3
if_icmpne L54
aload 5
astore 3
L38:
aload 6
ldc "type"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 7
L39:
aload 5
astore 3
L40:
aload 6
ldc "id"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 6
L41:
aload 5
astore 3
L42:
new net/hockeyapp/android/e/l
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/d/p/f Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/hockeyapp/android/e/l/<init>(Ljava/lang/String;)V
invokevirtual net/hockeyapp/android/e/l/a()Ljava/net/HttpURLConnection;
astore 5
L43:
aload 5
astore 3
goto L50
L54:
aload 5
astore 3
L44:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Login mode "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " not supported."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 5
aconst_null
astore 3
L55:
aload 3
astore 4
L45:
aload 5
invokevirtual java/io/UnsupportedEncodingException/printStackTrace()V
L46:
aload 3
ifnull L56
aload 3
invokevirtual java/net/HttpURLConnection/disconnect()V
L56:
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L51:
aload 3
ifnull L56
aload 3
invokevirtual java/net/HttpURLConnection/disconnect()V
goto L56
L3:
astore 5
L57:
aload 4
astore 3
L47:
aload 5
invokevirtual java/io/IOException/printStackTrace()V
L48:
aload 4
ifnull L56
aload 4
invokevirtual java/net/HttpURLConnection/disconnect()V
goto L56
L4:
astore 5
aload 3
astore 4
aload 5
astore 3
L58:
aload 4
ifnull L59
aload 4
invokevirtual java/net/HttpURLConnection/disconnect()V
L59:
aload 3
athrow
L17:
astore 3
goto L58
L16:
astore 5
aload 3
astore 4
goto L57
L15:
astore 5
goto L55
.limit locals 9
.limit stack 5
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial net/hockeyapp/android/d/p/c()Ljava/lang/Boolean;
areturn
.limit locals 2
.limit stack 1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 1
checkcast java/lang/Boolean
astore 1
aload 0
getfield net/hockeyapp/android/d/p/c Landroid/app/ProgressDialog;
ifnull L1
L0:
aload 0
getfield net/hockeyapp/android/d/p/c Landroid/app/ProgressDialog;
invokevirtual android/app/ProgressDialog/dismiss()V
L1:
aload 0
getfield net/hockeyapp/android/d/p/b Landroid/os/Handler;
ifnull L3
new android/os/Message
dup
invokespecial android/os/Message/<init>()V
astore 2
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 3
aload 3
ldc "success"
aload 1
invokevirtual java/lang/Boolean/booleanValue()Z
invokevirtual android/os/Bundle/putBoolean(Ljava/lang/String;Z)V
aload 2
aload 3
invokevirtual android/os/Message/setData(Landroid/os/Bundle;)V
aload 0
getfield net/hockeyapp/android/d/p/b Landroid/os/Handler;
aload 2
invokevirtual android/os/Handler/sendMessage(Landroid/os/Message;)Z
pop
L3:
return
L2:
astore 2
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
goto L1
.limit locals 4
.limit stack 3
.end method

.method protected final onPreExecute()V
aload 0
getfield net/hockeyapp/android/d/p/c Landroid/app/ProgressDialog;
ifnull L0
aload 0
getfield net/hockeyapp/android/d/p/c Landroid/app/ProgressDialog;
invokevirtual android/app/ProgressDialog/isShowing()Z
ifne L1
L0:
aload 0
getfield net/hockeyapp/android/d/p/d Z
ifeq L1
aload 0
aload 0
getfield net/hockeyapp/android/d/p/a Landroid/content/Context;
ldc ""
ldc "Please wait..."
iconst_1
iconst_0
invokestatic android/app/ProgressDialog/show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;
putfield net/hockeyapp/android/d/p/c Landroid/app/ProgressDialog;
L1:
return
.limit locals 1
.limit stack 6
.end method
