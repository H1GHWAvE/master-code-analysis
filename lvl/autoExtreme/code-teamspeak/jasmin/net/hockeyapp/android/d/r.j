.bytecode 50.0
.class public final synchronized net/hockeyapp/android/d/r
.super net/hockeyapp/android/d/k

.field public 'a' Z

.field public 'b' I

.field private 'c' Landroid/content/Context;

.field private 'd' Landroid/os/Handler;

.field private 'e' Ljava/lang/String;

.field private 'f' Ljava/lang/String;

.field private 'g' Ljava/lang/String;

.field private 'h' Ljava/lang/String;

.field private 'i' Ljava/lang/String;

.field private 'j' Ljava/util/List;

.field private 'k' Ljava/lang/String;

.field private 'l' Z

.field private 'm' Landroid/app/ProgressDialog;

.field private 'n' Ljava/net/HttpURLConnection;

.method public <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V
aload 0
invokespecial net/hockeyapp/android/d/k/<init>()V
aload 0
aload 1
putfield net/hockeyapp/android/d/r/c Landroid/content/Context;
aload 0
aload 2
putfield net/hockeyapp/android/d/r/e Ljava/lang/String;
aload 0
aload 3
putfield net/hockeyapp/android/d/r/f Ljava/lang/String;
aload 0
aload 4
putfield net/hockeyapp/android/d/r/g Ljava/lang/String;
aload 0
aload 5
putfield net/hockeyapp/android/d/r/h Ljava/lang/String;
aload 0
aload 6
putfield net/hockeyapp/android/d/r/i Ljava/lang/String;
aload 0
aload 7
putfield net/hockeyapp/android/d/r/j Ljava/util/List;
aload 0
aload 8
putfield net/hockeyapp/android/d/r/k Ljava/lang/String;
aload 0
aload 9
putfield net/hockeyapp/android/d/r/d Landroid/os/Handler;
aload 0
iload 10
putfield net/hockeyapp/android/d/r/l Z
aload 0
iconst_1
putfield net/hockeyapp/android/d/r/a Z
aload 0
iconst_m1
putfield net/hockeyapp/android/d/r/b I
aload 1
ifnull L0
aload 1
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;)V
L0:
return
.limit locals 11
.limit stack 2
.end method

.method private a(I)V
aload 0
iload 1
putfield net/hockeyapp/android/d/r/b I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/content/Context;)V
aload 0
aload 1
putfield net/hockeyapp/android/d/r/c Landroid/content/Context;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/util/HashMap;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield net/hockeyapp/android/d/r/m Landroid/app/ProgressDialog;
ifnull L1
L0:
aload 0
getfield net/hockeyapp/android/d/r/m Landroid/app/ProgressDialog;
invokevirtual android/app/ProgressDialog/dismiss()V
L1:
aload 0
getfield net/hockeyapp/android/d/r/d Landroid/os/Handler;
ifnull L3
new android/os/Message
dup
invokespecial android/os/Message/<init>()V
astore 2
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 3
aload 1
ifnull L4
aload 3
ldc "request_type"
aload 1
ldc "type"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 3
ldc "feedback_response"
aload 1
ldc "response"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 3
ldc "feedback_status"
aload 1
ldc "status"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
L5:
aload 2
aload 3
invokevirtual android/os/Message/setData(Landroid/os/Bundle;)V
aload 0
getfield net/hockeyapp/android/d/r/d Landroid/os/Handler;
aload 2
invokevirtual android/os/Handler/sendMessage(Landroid/os/Message;)Z
pop
L3:
return
L2:
astore 2
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
goto L1
L4:
aload 3
ldc "request_type"
ldc "unknown"
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
goto L5
.limit locals 4
.limit stack 4
.end method

.method private b()V
aload 0
iconst_0
putfield net/hockeyapp/android/d/r/a Z
return
.limit locals 1
.limit stack 2
.end method

.method private transient c()Ljava/util/HashMap;
aload 0
getfield net/hockeyapp/android/d/r/l Z
ifeq L0
aload 0
getfield net/hockeyapp/android/d/r/k Ljava/lang/String;
ifnull L0
aload 0
invokespecial net/hockeyapp/android/d/r/f()Ljava/util/HashMap;
areturn
L0:
aload 0
getfield net/hockeyapp/android/d/r/l Z
ifne L1
aload 0
getfield net/hockeyapp/android/d/r/j Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifeq L2
aload 0
invokespecial net/hockeyapp/android/d/r/d()Ljava/util/HashMap;
areturn
L2:
aload 0
invokespecial net/hockeyapp/android/d/r/e()Ljava/util/HashMap;
astore 3
aload 3
ldc "status"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
astore 4
aload 4
ifnull L3
aload 4
ldc "2"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L3
aload 0
getfield net/hockeyapp/android/d/r/c Landroid/content/Context;
ifnull L3
new java/io/File
dup
aload 0
getfield net/hockeyapp/android/d/r/c Landroid/content/Context;
invokevirtual android/content/Context/getCacheDir()Ljava/io/File;
ldc "HockeyApp"
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 4
aload 4
invokevirtual java/io/File/exists()Z
ifeq L3
aload 4
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 4
aload 4
arraylength
istore 2
iconst_0
istore 1
L4:
iload 1
iload 2
if_icmpge L3
aload 4
iload 1
aaload
invokevirtual java/io/File/delete()Z
pop
iload 1
iconst_1
iadd
istore 1
goto L4
L3:
aload 3
areturn
L1:
aconst_null
areturn
.limit locals 5
.limit stack 4
.end method

.method private d()Ljava/util/HashMap;
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch java/io/IOException from L6 to L7 using L2
.catch all from L6 to L7 using L3
.catch java/io/IOException from L8 to L9 using L2
.catch all from L8 to L9 using L3
.catch java/io/IOException from L10 to L11 using L2
.catch all from L10 to L11 using L3
.catch java/io/IOException from L12 to L13 using L2
.catch all from L12 to L13 using L3
.catch java/io/IOException from L14 to L15 using L2
.catch all from L14 to L15 using L3
.catch java/io/IOException from L16 to L17 using L2
.catch all from L16 to L17 using L3
.catch java/io/IOException from L18 to L19 using L2
.catch all from L18 to L19 using L3
.catch java/io/IOException from L20 to L21 using L2
.catch all from L20 to L21 using L3
.catch java/io/IOException from L22 to L23 using L2
.catch all from L22 to L23 using L3
.catch java/io/IOException from L24 to L25 using L2
.catch all from L24 to L25 using L3
.catch java/io/IOException from L26 to L27 using L2
.catch all from L26 to L27 using L3
.catch java/io/IOException from L28 to L29 using L2
.catch all from L28 to L29 using L3
.catch java/io/IOException from L30 to L31 using L2
.catch all from L30 to L31 using L3
.catch java/io/IOException from L32 to L33 using L2
.catch all from L32 to L33 using L3
.catch java/io/IOException from L34 to L35 using L2
.catch all from L34 to L35 using L3
.catch java/io/IOException from L36 to L37 using L2
.catch all from L36 to L37 using L3
.catch java/io/IOException from L38 to L39 using L2
.catch all from L38 to L39 using L3
.catch java/io/IOException from L40 to L41 using L2
.catch all from L40 to L41 using L3
.catch all from L42 to L43 using L3
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 6
aload 6
ldc "type"
ldc "send"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aconst_null
astore 5
aconst_null
astore 4
aload 4
astore 2
aload 5
astore 1
L0:
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 7
L1:
aload 4
astore 2
aload 5
astore 1
L4:
aload 7
ldc "name"
aload 0
getfield net/hockeyapp/android/d/r/f Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L5:
aload 4
astore 2
aload 5
astore 1
L6:
aload 7
ldc "email"
aload 0
getfield net/hockeyapp/android/d/r/g Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L7:
aload 4
astore 2
aload 5
astore 1
L8:
aload 7
ldc "subject"
aload 0
getfield net/hockeyapp/android/d/r/h Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L9:
aload 4
astore 2
aload 5
astore 1
L10:
aload 7
ldc "text"
aload 0
getfield net/hockeyapp/android/d/r/i Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L11:
aload 4
astore 2
aload 5
astore 1
L12:
aload 7
ldc "bundle_identifier"
getstatic net/hockeyapp/android/a/d Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L13:
aload 4
astore 2
aload 5
astore 1
L14:
aload 7
ldc "bundle_short_version"
getstatic net/hockeyapp/android/a/c Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L15:
aload 4
astore 2
aload 5
astore 1
L16:
aload 7
ldc "bundle_version"
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L17:
aload 4
astore 2
aload 5
astore 1
L18:
aload 7
ldc "os_version"
getstatic net/hockeyapp/android/a/e Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L19:
aload 4
astore 2
aload 5
astore 1
L20:
aload 7
ldc "oem"
getstatic net/hockeyapp/android/a/g Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L21:
aload 4
astore 2
aload 5
astore 1
L22:
aload 7
ldc "model"
getstatic net/hockeyapp/android/a/f Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L23:
aload 4
astore 2
aload 5
astore 1
L24:
aload 0
getfield net/hockeyapp/android/d/r/k Ljava/lang/String;
ifnull L27
L25:
aload 4
astore 2
aload 5
astore 1
L26:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/d/r/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/hockeyapp/android/d/r/k Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield net/hockeyapp/android/d/r/e Ljava/lang/String;
L27:
aload 4
astore 2
aload 5
astore 1
L28:
new net/hockeyapp/android/e/l
dup
aload 0
getfield net/hockeyapp/android/d/r/e Ljava/lang/String;
invokespecial net/hockeyapp/android/e/l/<init>(Ljava/lang/String;)V
astore 8
L29:
aload 4
astore 2
aload 5
astore 1
L30:
aload 0
getfield net/hockeyapp/android/d/r/k Ljava/lang/String;
ifnull L44
L31:
ldc "PUT"
astore 3
L45:
aload 4
astore 2
aload 5
astore 1
L32:
aload 8
aload 3
putfield net/hockeyapp/android/e/l/b Ljava/lang/String;
L33:
aload 4
astore 2
aload 5
astore 1
L34:
aload 8
aload 7
invokevirtual net/hockeyapp/android/e/l/a(Ljava/util/Map;)Lnet/hockeyapp/android/e/l;
invokevirtual net/hockeyapp/android/e/l/a()Ljava/net/HttpURLConnection;
astore 3
L35:
aload 3
astore 2
aload 3
astore 1
L36:
aload 3
invokevirtual java/net/HttpURLConnection/connect()V
L37:
aload 3
astore 2
aload 3
astore 1
L38:
aload 6
ldc "status"
aload 3
invokevirtual java/net/HttpURLConnection/getResponseCode()I
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L39:
aload 3
astore 2
aload 3
astore 1
L40:
aload 6
ldc "response"
aload 3
invokestatic net/hockeyapp/android/d/r/a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L41:
aload 3
ifnull L46
aload 3
invokevirtual java/net/HttpURLConnection/disconnect()V
L46:
aload 6
areturn
L44:
ldc "POST"
astore 3
goto L45
L2:
astore 3
aload 2
astore 1
L42:
aload 3
invokevirtual java/io/IOException/printStackTrace()V
L43:
aload 2
ifnull L46
aload 2
invokevirtual java/net/HttpURLConnection/disconnect()V
aload 6
areturn
L3:
astore 2
aload 1
ifnull L47
aload 1
invokevirtual java/net/HttpURLConnection/disconnect()V
L47:
aload 2
athrow
.limit locals 9
.limit stack 3
.end method

.method private e()Ljava/util/HashMap;
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch java/io/IOException from L6 to L7 using L2
.catch all from L6 to L7 using L3
.catch java/io/IOException from L8 to L9 using L2
.catch all from L8 to L9 using L3
.catch java/io/IOException from L10 to L11 using L2
.catch all from L10 to L11 using L3
.catch java/io/IOException from L12 to L13 using L2
.catch all from L12 to L13 using L3
.catch java/io/IOException from L14 to L15 using L2
.catch all from L14 to L15 using L3
.catch java/io/IOException from L16 to L17 using L2
.catch all from L16 to L17 using L3
.catch java/io/IOException from L18 to L19 using L2
.catch all from L18 to L19 using L3
.catch java/io/IOException from L20 to L21 using L2
.catch all from L20 to L21 using L3
.catch java/io/IOException from L22 to L23 using L2
.catch all from L22 to L23 using L3
.catch java/io/IOException from L24 to L25 using L2
.catch all from L24 to L25 using L3
.catch java/io/IOException from L26 to L27 using L2
.catch all from L26 to L27 using L3
.catch java/io/IOException from L28 to L29 using L2
.catch all from L28 to L29 using L3
.catch java/io/IOException from L30 to L31 using L2
.catch all from L30 to L31 using L3
.catch java/io/IOException from L32 to L33 using L2
.catch all from L32 to L33 using L3
.catch java/io/IOException from L34 to L35 using L2
.catch all from L34 to L35 using L3
.catch java/io/IOException from L36 to L37 using L2
.catch all from L36 to L37 using L3
.catch java/io/IOException from L38 to L39 using L2
.catch all from L38 to L39 using L3
.catch java/io/IOException from L40 to L41 using L2
.catch all from L40 to L41 using L3
.catch all from L42 to L43 using L3
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 6
aload 6
ldc "type"
ldc "send"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aconst_null
astore 5
aconst_null
astore 4
aload 4
astore 2
aload 5
astore 1
L0:
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 7
L1:
aload 4
astore 2
aload 5
astore 1
L4:
aload 7
ldc "name"
aload 0
getfield net/hockeyapp/android/d/r/f Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L5:
aload 4
astore 2
aload 5
astore 1
L6:
aload 7
ldc "email"
aload 0
getfield net/hockeyapp/android/d/r/g Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L7:
aload 4
astore 2
aload 5
astore 1
L8:
aload 7
ldc "subject"
aload 0
getfield net/hockeyapp/android/d/r/h Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L9:
aload 4
astore 2
aload 5
astore 1
L10:
aload 7
ldc "text"
aload 0
getfield net/hockeyapp/android/d/r/i Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L11:
aload 4
astore 2
aload 5
astore 1
L12:
aload 7
ldc "bundle_identifier"
getstatic net/hockeyapp/android/a/d Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L13:
aload 4
astore 2
aload 5
astore 1
L14:
aload 7
ldc "bundle_short_version"
getstatic net/hockeyapp/android/a/c Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L15:
aload 4
astore 2
aload 5
astore 1
L16:
aload 7
ldc "bundle_version"
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L17:
aload 4
astore 2
aload 5
astore 1
L18:
aload 7
ldc "os_version"
getstatic net/hockeyapp/android/a/e Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L19:
aload 4
astore 2
aload 5
astore 1
L20:
aload 7
ldc "oem"
getstatic net/hockeyapp/android/a/g Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L21:
aload 4
astore 2
aload 5
astore 1
L22:
aload 7
ldc "model"
getstatic net/hockeyapp/android/a/f Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L23:
aload 4
astore 2
aload 5
astore 1
L24:
aload 0
getfield net/hockeyapp/android/d/r/k Ljava/lang/String;
ifnull L27
L25:
aload 4
astore 2
aload 5
astore 1
L26:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/d/r/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/hockeyapp/android/d/r/k Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield net/hockeyapp/android/d/r/e Ljava/lang/String;
L27:
aload 4
astore 2
aload 5
astore 1
L28:
new net/hockeyapp/android/e/l
dup
aload 0
getfield net/hockeyapp/android/d/r/e Ljava/lang/String;
invokespecial net/hockeyapp/android/e/l/<init>(Ljava/lang/String;)V
astore 8
L29:
aload 4
astore 2
aload 5
astore 1
L30:
aload 0
getfield net/hockeyapp/android/d/r/k Ljava/lang/String;
ifnull L44
L31:
ldc "PUT"
astore 3
L45:
aload 4
astore 2
aload 5
astore 1
L32:
aload 8
aload 3
putfield net/hockeyapp/android/e/l/b Ljava/lang/String;
L33:
aload 4
astore 2
aload 5
astore 1
L34:
aload 8
aload 7
aload 0
getfield net/hockeyapp/android/d/r/c Landroid/content/Context;
aload 0
getfield net/hockeyapp/android/d/r/j Ljava/util/List;
invokevirtual net/hockeyapp/android/e/l/a(Ljava/util/Map;Landroid/content/Context;Ljava/util/List;)Lnet/hockeyapp/android/e/l;
invokevirtual net/hockeyapp/android/e/l/a()Ljava/net/HttpURLConnection;
astore 3
L35:
aload 3
astore 2
aload 3
astore 1
L36:
aload 3
invokevirtual java/net/HttpURLConnection/connect()V
L37:
aload 3
astore 2
aload 3
astore 1
L38:
aload 6
ldc "status"
aload 3
invokevirtual java/net/HttpURLConnection/getResponseCode()I
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L39:
aload 3
astore 2
aload 3
astore 1
L40:
aload 6
ldc "response"
aload 3
invokestatic net/hockeyapp/android/d/r/a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L41:
aload 3
ifnull L46
aload 3
invokevirtual java/net/HttpURLConnection/disconnect()V
L46:
aload 6
areturn
L44:
ldc "POST"
astore 3
goto L45
L2:
astore 3
aload 2
astore 1
L42:
aload 3
invokevirtual java/io/IOException/printStackTrace()V
L43:
aload 2
ifnull L46
aload 2
invokevirtual java/net/HttpURLConnection/disconnect()V
aload 6
areturn
L3:
astore 2
aload 1
ifnull L47
aload 1
invokevirtual java/net/HttpURLConnection/disconnect()V
L47:
aload 2
athrow
.limit locals 9
.limit stack 4
.end method

.method private f()Ljava/util/HashMap;
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch java/io/IOException from L6 to L7 using L2
.catch all from L6 to L7 using L3
.catch java/io/IOException from L8 to L9 using L2
.catch all from L8 to L9 using L3
.catch java/io/IOException from L10 to L11 using L2
.catch all from L10 to L11 using L3
.catch all from L12 to L13 using L3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 3
aload 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/d/r/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/hockeyapp/android/d/r/k Ljava/lang/String;
invokestatic net/hockeyapp/android/e/w/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
getfield net/hockeyapp/android/d/r/b I
iconst_m1
if_icmpeq L14
aload 3
new java/lang/StringBuilder
dup
ldc "?last_message_id="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/d/r/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L14:
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 4
aconst_null
astore 2
aconst_null
astore 1
L0:
new net/hockeyapp/android/e/l
dup
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/hockeyapp/android/e/l/<init>(Ljava/lang/String;)V
invokevirtual net/hockeyapp/android/e/l/a()Ljava/net/HttpURLConnection;
astore 3
L1:
aload 3
astore 1
aload 3
astore 2
L4:
aload 4
ldc "type"
ldc "fetch"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L5:
aload 3
astore 1
aload 3
astore 2
L6:
aload 3
invokevirtual java/net/HttpURLConnection/connect()V
L7:
aload 3
astore 1
aload 3
astore 2
L8:
aload 4
ldc "status"
aload 3
invokevirtual java/net/HttpURLConnection/getResponseCode()I
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L9:
aload 3
astore 1
aload 3
astore 2
L10:
aload 4
ldc "response"
aload 3
invokestatic net/hockeyapp/android/d/r/a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L11:
aload 3
ifnull L15
aload 3
invokevirtual java/net/HttpURLConnection/disconnect()V
L15:
aload 4
areturn
L2:
astore 3
aload 1
astore 2
L12:
aload 3
invokevirtual java/io/IOException/printStackTrace()V
L13:
aload 1
ifnull L15
aload 1
invokevirtual java/net/HttpURLConnection/disconnect()V
aload 4
areturn
L3:
astore 1
aload 2
ifnull L16
aload 2
invokevirtual java/net/HttpURLConnection/disconnect()V
L16:
aload 1
athrow
.limit locals 5
.limit stack 4
.end method

.method public final a()V
aload 0
aconst_null
putfield net/hockeyapp/android/d/r/c Landroid/content/Context;
aload 0
getfield net/hockeyapp/android/d/r/m Landroid/app/ProgressDialog;
ifnull L0
aload 0
getfield net/hockeyapp/android/d/r/m Landroid/app/ProgressDialog;
invokevirtual android/app/ProgressDialog/dismiss()V
L0:
aload 0
aconst_null
putfield net/hockeyapp/android/d/r/m Landroid/app/ProgressDialog;
return
.limit locals 1
.limit stack 2
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield net/hockeyapp/android/d/r/l Z
ifeq L0
aload 0
getfield net/hockeyapp/android/d/r/k Ljava/lang/String;
ifnull L0
aload 0
invokespecial net/hockeyapp/android/d/r/f()Ljava/util/HashMap;
areturn
L0:
aload 0
getfield net/hockeyapp/android/d/r/l Z
ifne L1
aload 0
getfield net/hockeyapp/android/d/r/j Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifeq L2
aload 0
invokespecial net/hockeyapp/android/d/r/d()Ljava/util/HashMap;
areturn
L2:
aload 0
invokespecial net/hockeyapp/android/d/r/e()Ljava/util/HashMap;
astore 1
aload 1
ldc "status"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
astore 4
aload 4
ifnull L3
aload 4
ldc "2"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L3
aload 0
getfield net/hockeyapp/android/d/r/c Landroid/content/Context;
ifnull L3
new java/io/File
dup
aload 0
getfield net/hockeyapp/android/d/r/c Landroid/content/Context;
invokevirtual android/content/Context/getCacheDir()Ljava/io/File;
ldc "HockeyApp"
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 4
aload 4
invokevirtual java/io/File/exists()Z
ifeq L3
aload 4
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 4
aload 4
arraylength
istore 3
iconst_0
istore 2
L4:
iload 2
iload 3
if_icmpge L3
aload 4
iload 2
aaload
invokevirtual java/io/File/delete()Z
pop
iload 2
iconst_1
iadd
istore 2
goto L4
L3:
aload 1
areturn
L1:
aconst_null
areturn
.limit locals 5
.limit stack 4
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 1
checkcast java/util/HashMap
astore 1
aload 0
getfield net/hockeyapp/android/d/r/m Landroid/app/ProgressDialog;
ifnull L1
L0:
aload 0
getfield net/hockeyapp/android/d/r/m Landroid/app/ProgressDialog;
invokevirtual android/app/ProgressDialog/dismiss()V
L1:
aload 0
getfield net/hockeyapp/android/d/r/d Landroid/os/Handler;
ifnull L3
new android/os/Message
dup
invokespecial android/os/Message/<init>()V
astore 2
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 3
aload 1
ifnull L4
aload 3
ldc "request_type"
aload 1
ldc "type"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 3
ldc "feedback_response"
aload 1
ldc "response"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 3
ldc "feedback_status"
aload 1
ldc "status"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
L5:
aload 2
aload 3
invokevirtual android/os/Message/setData(Landroid/os/Bundle;)V
aload 0
getfield net/hockeyapp/android/d/r/d Landroid/os/Handler;
aload 2
invokevirtual android/os/Handler/sendMessage(Landroid/os/Message;)Z
pop
L3:
return
L2:
astore 2
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
goto L1
L4:
aload 3
ldc "request_type"
ldc "unknown"
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
goto L5
.limit locals 4
.limit stack 4
.end method

.method protected final onPreExecute()V
ldc "Sending feedback.."
astore 1
aload 0
getfield net/hockeyapp/android/d/r/l Z
ifeq L0
ldc "Retrieving discussions..."
astore 1
L0:
aload 0
getfield net/hockeyapp/android/d/r/m Landroid/app/ProgressDialog;
ifnull L1
aload 0
getfield net/hockeyapp/android/d/r/m Landroid/app/ProgressDialog;
invokevirtual android/app/ProgressDialog/isShowing()Z
ifne L2
L1:
aload 0
getfield net/hockeyapp/android/d/r/a Z
ifeq L2
aload 0
aload 0
getfield net/hockeyapp/android/d/r/c Landroid/content/Context;
ldc ""
aload 1
iconst_1
iconst_0
invokestatic android/app/ProgressDialog/show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;
putfield net/hockeyapp/android/d/r/m Landroid/app/ProgressDialog;
L2:
return
.limit locals 2
.limit stack 6
.end method
