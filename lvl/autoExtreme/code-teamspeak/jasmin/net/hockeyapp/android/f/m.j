.bytecode 50.0
.class public final synchronized net/hockeyapp/android/f/m
.super android/widget/RelativeLayout

.field public static final 'a' I = 4097


.field public static final 'b' I = 4098


.field public static final 'c' I = 4099


.field public static final 'd' I = 4100


.field public static final 'e' I = 4101


.field protected 'f' Landroid/widget/RelativeLayout;

.field protected 'g' Z

.field protected 'h' Z

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial net/hockeyapp/android/f/m/<init>(Landroid/content/Context;C)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_1
iconst_0
invokespecial net/hockeyapp/android/f/m/<init>(Landroid/content/Context;ZZ)V
return
.limit locals 3
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;C)V
aload 0
aload 1
iconst_1
iconst_0
invokespecial net/hockeyapp/android/f/m/<init>(Landroid/content/Context;ZZ)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;ZZ)V
aload 0
aload 1
invokespecial android/widget/RelativeLayout/<init>(Landroid/content/Context;)V
aload 0
aconst_null
putfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
aload 0
iconst_0
putfield net/hockeyapp/android/f/m/g Z
aload 0
iconst_0
putfield net/hockeyapp/android/f/m/h Z
iload 2
ifeq L0
aload 0
aload 1
invokespecial net/hockeyapp/android/f/m/setLayoutHorizontally(Landroid/content/Context;)V
L1:
aload 0
iload 3
putfield net/hockeyapp/android/f/m/h Z
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 5
aload 0
iconst_m1
invokevirtual net/hockeyapp/android/f/m/setBackgroundColor(I)V
aload 0
aload 5
invokevirtual net/hockeyapp/android/f/m/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
new android/widget/RelativeLayout
dup
aload 1
invokespecial android/widget/RelativeLayout/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
sipush 4097
invokevirtual android/widget/RelativeLayout/setId(I)V
aload 0
getfield net/hockeyapp/android/f/m/g Z
ifeq L2
new android/widget/RelativeLayout$LayoutParams
dup
iconst_1
ldc_w 250.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
iconst_m1
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 5
aload 5
bipush 9
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
iconst_0
iconst_0
iconst_0
iconst_0
invokevirtual android/widget/RelativeLayout/setPadding(IIII)V
L3:
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
aload 5
invokevirtual android/widget/RelativeLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
sipush 230
sipush 236
sipush 239
invokestatic android/graphics/Color/rgb(III)I
invokevirtual android/widget/RelativeLayout/setBackgroundColor(I)V
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
astore 5
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 6
aload 6
sipush 4098
invokevirtual android/widget/TextView/setId(I)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 7
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
aload 7
iload 4
iload 4
iload 4
iconst_0
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
aload 6
sipush 230
sipush 236
sipush 239
invokestatic android/graphics/Color/rgb(III)I
invokevirtual android/widget/TextView/setBackgroundColor(I)V
aload 6
aload 7
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 6
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 6
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 6
iconst_1
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 6
ldc_w -16777216
invokevirtual android/widget/TextView/setTextColor(I)V
aload 6
iconst_2
ldc_w 20.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 6
aconst_null
iconst_1
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 5
aload 6
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;)V
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
astore 5
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 6
aload 6
sipush 4099
invokevirtual android/widget/TextView/setId(I)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 7
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
aload 7
iload 4
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
iload 4
iconst_0
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
aload 7
iconst_3
sipush 4098
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 6
sipush 230
sipush 236
sipush 239
invokestatic android/graphics/Color/rgb(III)I
invokevirtual android/widget/TextView/setBackgroundColor(I)V
aload 6
aload 7
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 6
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 6
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 6
iconst_2
invokevirtual android/widget/TextView/setLines(I)V
aload 6
fconst_0
ldc_w 1.1F
invokevirtual android/widget/TextView/setLineSpacing(FF)V
aload 6
ldc_w -16777216
invokevirtual android/widget/TextView/setTextColor(I)V
aload 6
iconst_2
ldc_w 16.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 6
aconst_null
iconst_1
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 5
aload 6
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;)V
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
astore 5
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 6
aload 6
sipush 4100
invokevirtual android/widget/Button/setId(I)V
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
new android/widget/RelativeLayout$LayoutParams
dup
iconst_1
ldc_w 120.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 7
aload 7
iload 4
iload 4
iload 4
iload 4
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
aload 7
bipush 9
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 7
iconst_3
sipush 4099
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 6
aload 7
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 6
aload 0
invokespecial net/hockeyapp/android/f/m/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 6
ldc "Update"
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 6
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 6
iconst_2
ldc_w 16.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 5
aload 6
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;)V
aload 0
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
invokevirtual net/hockeyapp/android/f/m/addView(Landroid/view/View;)V
new android/webkit/WebView
dup
aload 1
invokespecial android/webkit/WebView/<init>(Landroid/content/Context;)V
astore 5
aload 5
sipush 4101
invokevirtual android/webkit/WebView/setId(I)V
iconst_1
ldc_w 400.0F
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
aload 0
getfield net/hockeyapp/android/f/m/h Z
ifeq L4
L5:
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
iload 4
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 6
aload 0
getfield net/hockeyapp/android/f/m/g Z
ifeq L6
aload 6
iconst_1
sipush 4097
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
L7:
aload 6
iconst_0
iconst_0
iconst_0
iconst_0
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
aload 5
aload 6
invokevirtual android/webkit/WebView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 5
iconst_m1
invokevirtual android/webkit/WebView/setBackgroundColor(I)V
aload 0
aload 5
invokevirtual net/hockeyapp/android/f/m/addView(Landroid/view/View;)V
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
astore 6
iconst_1
ldc_w 3.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
new android/widget/ImageView
dup
aload 1
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 7
aload 0
getfield net/hockeyapp/android/f/m/g Z
ifeq L8
new android/widget/RelativeLayout$LayoutParams
dup
iconst_1
iconst_m1
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 5
aload 5
bipush 11
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 7
new android/graphics/drawable/ColorDrawable
dup
ldc_w -16777216
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
L9:
aload 7
aload 5
invokevirtual android/widget/ImageView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 6
aload 7
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;)V
new android/widget/ImageView
dup
aload 1
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 1
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
iload 4
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 5
aload 0
getfield net/hockeyapp/android/f/m/g Z
ifeq L10
aload 5
bipush 10
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
L11:
aload 1
aload 5
invokevirtual android/widget/ImageView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
invokestatic net/hockeyapp/android/e/aa/a()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
invokevirtual net/hockeyapp/android/f/m/addView(Landroid/view/View;)V
return
L0:
aload 0
iconst_0
putfield net/hockeyapp/android/f/m/g Z
goto L1
L2:
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 5
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/RelativeLayout/setPadding(IIII)V
goto L3
L4:
iconst_m1
istore 4
goto L5
L6:
aload 6
iconst_3
sipush 4097
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
goto L7
L8:
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
iload 4
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 5
aload 5
bipush 10
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 7
invokestatic net/hockeyapp/android/e/aa/a()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
goto L9
L10:
aload 5
iconst_3
sipush 4097
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
goto L11
.limit locals 8
.limit stack 7
.end method

.method private a()V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 1
aload 0
iconst_m1
invokevirtual net/hockeyapp/android/f/m/setBackgroundColor(I)V
aload 0
aload 1
invokevirtual net/hockeyapp/android/f/m/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 2
.limit stack 4
.end method

.method private a(Landroid/content/Context;)V
aload 0
new android/widget/RelativeLayout
dup
aload 1
invokespecial android/widget/RelativeLayout/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
sipush 4097
invokevirtual android/widget/RelativeLayout/setId(I)V
aload 0
getfield net/hockeyapp/android/f/m/g Z
ifeq L0
new android/widget/RelativeLayout$LayoutParams
dup
iconst_1
ldc_w 250.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
iconst_m1
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 3
aload 3
bipush 9
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
iconst_0
iconst_0
iconst_0
iconst_0
invokevirtual android/widget/RelativeLayout/setPadding(IIII)V
L1:
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
aload 3
invokevirtual android/widget/RelativeLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
sipush 230
sipush 236
sipush 239
invokestatic android/graphics/Color/rgb(III)I
invokevirtual android/widget/RelativeLayout/setBackgroundColor(I)V
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
astore 3
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 4
aload 4
sipush 4098
invokevirtual android/widget/TextView/setId(I)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 5
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 5
iload 2
iload 2
iload 2
iconst_0
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
aload 4
sipush 230
sipush 236
sipush 239
invokestatic android/graphics/Color/rgb(III)I
invokevirtual android/widget/TextView/setBackgroundColor(I)V
aload 4
aload 5
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 4
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 4
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 4
iconst_1
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 4
ldc_w -16777216
invokevirtual android/widget/TextView/setTextColor(I)V
aload 4
iconst_2
ldc_w 20.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 4
aconst_null
iconst_1
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 3
aload 4
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;)V
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
astore 3
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 4
aload 4
sipush 4099
invokevirtual android/widget/TextView/setId(I)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 5
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 5
iload 2
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
iload 2
iconst_0
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
aload 5
iconst_3
sipush 4098
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 4
sipush 230
sipush 236
sipush 239
invokestatic android/graphics/Color/rgb(III)I
invokevirtual android/widget/TextView/setBackgroundColor(I)V
aload 4
aload 5
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 4
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 4
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 4
iconst_2
invokevirtual android/widget/TextView/setLines(I)V
aload 4
fconst_0
ldc_w 1.1F
invokevirtual android/widget/TextView/setLineSpacing(FF)V
aload 4
ldc_w -16777216
invokevirtual android/widget/TextView/setTextColor(I)V
aload 4
iconst_2
ldc_w 16.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 4
aconst_null
iconst_1
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 3
aload 4
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;)V
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
astore 3
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 1
aload 1
sipush 4100
invokevirtual android/widget/Button/setId(I)V
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
new android/widget/RelativeLayout$LayoutParams
dup
iconst_1
ldc_w 120.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
aload 4
iload 2
iload 2
iload 2
iload 2
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
aload 4
bipush 9
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 4
iconst_3
sipush 4099
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 1
aload 4
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 0
invokespecial net/hockeyapp/android/f/m/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
ldc "Update"
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 1
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 1
iconst_2
ldc_w 16.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 3
aload 1
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;)V
aload 0
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
invokevirtual net/hockeyapp/android/f/m/addView(Landroid/view/View;)V
return
L0:
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 3
aload 0
getfield net/hockeyapp/android/f/m/f Landroid/widget/RelativeLayout;
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/RelativeLayout/setPadding(IIII)V
goto L1
.limit locals 6
.limit stack 7
.end method

.method private a(Landroid/widget/RelativeLayout;Landroid/content/Context;)V
new android/widget/TextView
dup
aload 2
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 2
aload 2
sipush 4098
invokevirtual android/widget/TextView/setId(I)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
aload 4
iload 3
iload 3
iload 3
iconst_0
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
aload 2
sipush 230
sipush 236
sipush 239
invokestatic android/graphics/Color/rgb(III)I
invokevirtual android/widget/TextView/setBackgroundColor(I)V
aload 2
aload 4
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 2
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 2
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 2
iconst_1
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 2
ldc_w -16777216
invokevirtual android/widget/TextView/setTextColor(I)V
aload 2
iconst_2
ldc_w 20.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 2
aconst_null
iconst_1
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 1
aload 2
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;)V
return
.limit locals 5
.limit stack 5
.end method

.method private b(Landroid/content/Context;)V
new android/webkit/WebView
dup
aload 1
invokespecial android/webkit/WebView/<init>(Landroid/content/Context;)V
astore 3
aload 3
sipush 4101
invokevirtual android/webkit/WebView/setId(I)V
iconst_1
ldc_w 400.0F
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 0
getfield net/hockeyapp/android/f/m/h Z
ifeq L0
L1:
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
iload 2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 1
aload 0
getfield net/hockeyapp/android/f/m/g Z
ifeq L2
aload 1
iconst_1
sipush 4097
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
L3:
aload 1
iconst_0
iconst_0
iconst_0
iconst_0
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
aload 3
aload 1
invokevirtual android/webkit/WebView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 3
iconst_m1
invokevirtual android/webkit/WebView/setBackgroundColor(I)V
aload 0
aload 3
invokevirtual net/hockeyapp/android/f/m/addView(Landroid/view/View;)V
return
L0:
iconst_m1
istore 2
goto L1
L2:
aload 1
iconst_3
sipush 4097
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
goto L3
.limit locals 4
.limit stack 5
.end method

.method private b(Landroid/widget/RelativeLayout;Landroid/content/Context;)V
new android/widget/TextView
dup
aload 2
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 2
aload 2
sipush 4099
invokevirtual android/widget/TextView/setId(I)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
aload 4
iload 3
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
iload 3
iconst_0
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
aload 4
iconst_3
sipush 4098
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 2
sipush 230
sipush 236
sipush 239
invokestatic android/graphics/Color/rgb(III)I
invokevirtual android/widget/TextView/setBackgroundColor(I)V
aload 2
aload 4
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 2
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 2
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 2
iconst_2
invokevirtual android/widget/TextView/setLines(I)V
aload 2
fconst_0
ldc_w 1.1F
invokevirtual android/widget/TextView/setLineSpacing(FF)V
aload 2
ldc_w -16777216
invokevirtual android/widget/TextView/setTextColor(I)V
aload 2
iconst_2
ldc_w 16.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 2
aconst_null
iconst_1
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 1
aload 2
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;)V
return
.limit locals 5
.limit stack 5
.end method

.method private c(Landroid/widget/RelativeLayout;Landroid/content/Context;)V
new android/widget/Button
dup
aload 2
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
sipush 4100
invokevirtual android/widget/Button/setId(I)V
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
new android/widget/RelativeLayout$LayoutParams
dup
iconst_1
ldc_w 120.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
aload 4
iload 3
iload 3
iload 3
iload 3
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
aload 4
bipush 9
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 4
iconst_3
sipush 4099
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 2
aload 4
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 2
aload 0
invokespecial net/hockeyapp/android/f/m/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 2
ldc "Update"
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 2
iconst_2
ldc_w 16.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 1
aload 2
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;)V
return
.limit locals 5
.limit stack 5
.end method

.method private d(Landroid/widget/RelativeLayout;Landroid/content/Context;)V
iconst_1
ldc_w 3.0F
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
new android/widget/ImageView
dup
aload 2
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 5
aload 0
getfield net/hockeyapp/android/f/m/g Z
ifeq L0
new android/widget/RelativeLayout$LayoutParams
dup
iconst_1
iconst_m1
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
aload 4
bipush 11
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 5
new android/graphics/drawable/ColorDrawable
dup
ldc_w -16777216
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
L1:
aload 5
aload 4
invokevirtual android/widget/ImageView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 5
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;)V
new android/widget/ImageView
dup
aload 2
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 1
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
iload 3
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 2
aload 0
getfield net/hockeyapp/android/f/m/g Z
ifeq L2
aload 2
bipush 10
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
L3:
aload 1
aload 2
invokevirtual android/widget/ImageView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
invokestatic net/hockeyapp/android/e/aa/a()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
invokevirtual net/hockeyapp/android/f/m/addView(Landroid/view/View;)V
return
L0:
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
iload 3
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
aload 4
bipush 10
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 5
invokestatic net/hockeyapp/android/e/aa/a()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
goto L1
L2:
aload 2
iconst_3
sipush 4097
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
goto L3
.limit locals 6
.limit stack 4
.end method

.method private getButtonSelector()Landroid/graphics/drawable/Drawable;
new android/graphics/drawable/StateListDrawable
dup
invokespecial android/graphics/drawable/StateListDrawable/<init>()V
astore 1
new android/graphics/drawable/ColorDrawable
dup
ldc_w -16777216
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
astore 2
aload 1
iconst_1
newarray int
dup
iconst_0
ldc_w -16842919
iastore
aload 2
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
new android/graphics/drawable/ColorDrawable
dup
ldc_w -12303292
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
astore 2
aload 1
iconst_2
newarray int
dup
iconst_0
ldc_w -16842919
iastore
dup
iconst_1
ldc_w 16842908
iastore
aload 2
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
new android/graphics/drawable/ColorDrawable
dup
ldc_w -7829368
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
astore 2
aload 1
iconst_1
newarray int
dup
iconst_0
ldc_w 16842919
iastore
aload 2
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
aload 1
areturn
.limit locals 3
.limit stack 5
.end method

.method private setLayoutHorizontally(Landroid/content/Context;)V
aload 0
invokevirtual net/hockeyapp/android/f/m/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getConfiguration()Landroid/content/res/Configuration;
getfield android/content/res/Configuration/orientation I
iconst_2
if_icmpne L0
aload 0
iconst_1
putfield net/hockeyapp/android/f/m/g Z
return
L0:
aload 0
iconst_0
putfield net/hockeyapp/android/f/m/g Z
return
.limit locals 2
.limit stack 2
.end method
