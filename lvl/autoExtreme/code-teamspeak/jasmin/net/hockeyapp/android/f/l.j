.bytecode 50.0
.class final synchronized net/hockeyapp/android/f/l
.super android/os/AsyncTask

.field final synthetic 'a' Lnet/hockeyapp/android/f/k;

.method <init>(Lnet/hockeyapp/android/f/k;)V
aload 0
aload 1
putfield net/hockeyapp/android/f/l/a Lnet/hockeyapp/android/f/k;
aload 0
invokespecial android/os/AsyncTask/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method private static transient a([Ljava/lang/Object;)Landroid/graphics/Bitmap;
.catch java/lang/Exception from L0 to L1 using L2
aload 0
iconst_0
aaload
checkcast android/content/Context
astore 1
aload 0
iconst_1
aaload
checkcast android/net/Uri
astore 2
aload 0
iconst_2
aaload
checkcast java/lang/Integer
astore 3
aload 0
iconst_3
aaload
checkcast java/lang/Integer
astore 0
L0:
aload 1
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 2
aload 3
invokevirtual java/lang/Integer/intValue()I
aload 0
invokevirtual java/lang/Integer/intValue()I
invokestatic net/hockeyapp/android/f/k/a(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
astore 0
L1:
aload 0
areturn
L2:
astore 0
ldc "HockeyApp"
ldc "Could not load image into ImageView."
aload 0
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
areturn
.limit locals 4
.limit stack 4
.end method

.method private a(Landroid/graphics/Bitmap;)V
aload 1
ifnonnull L0
return
L0:
aload 0
getfield net/hockeyapp/android/f/l/a Lnet/hockeyapp/android/f/k;
aload 1
invokevirtual net/hockeyapp/android/f/k/setImageBitmap(Landroid/graphics/Bitmap;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 1
invokestatic net/hockeyapp/android/f/l/a([Ljava/lang/Object;)Landroid/graphics/Bitmap;
areturn
.limit locals 2
.limit stack 1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
aload 1
checkcast android/graphics/Bitmap
astore 1
aload 1
ifnull L0
aload 0
getfield net/hockeyapp/android/f/l/a Lnet/hockeyapp/android/f/k;
aload 1
invokevirtual net/hockeyapp/android/f/k/setImageBitmap(Landroid/graphics/Bitmap;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method protected final onPreExecute()V
aload 0
getfield net/hockeyapp/android/f/l/a Lnet/hockeyapp/android/f/k;
iconst_1
invokevirtual net/hockeyapp/android/f/k/setAdjustViewBounds(Z)V
return
.limit locals 1
.limit stack 2
.end method
