.bytecode 50.0
.class public final synchronized net/hockeyapp/android/f/h
.super android/widget/LinearLayout

.field public static final 'a' I = 12289


.field public static final 'b' I = 12290


.field public static final 'c' I = 12291


.field public static final 'd' I = 12292


.field private 'e' Landroid/widget/TextView;

.field private 'f' Landroid/widget/TextView;

.field private 'g' Landroid/widget/TextView;

.field private 'h' Lnet/hockeyapp/android/f/a;

.field private 'i' Z

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial net/hockeyapp/android/f/h/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
aload 0
iconst_1
putfield net/hockeyapp/android/f/h/i Z
aload 0
iconst_1
invokevirtual net/hockeyapp/android/f/h/setOrientation(I)V
aload 0
iconst_3
invokevirtual net/hockeyapp/android/f/h/setGravity(I)V
aload 0
ldc_w -3355444
invokevirtual net/hockeyapp/android/f/h/setBackgroundColor(I)V
aload 0
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
sipush 12289
invokevirtual android/widget/TextView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 3
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/h/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 3
iload 2
iload 2
iload 2
iconst_0
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
aload 3
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
iconst_1
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
ldc_w -7829368
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
iconst_2
ldc_w 15.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
aconst_null
iconst_0
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 0
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
invokevirtual net/hockeyapp/android/f/h/addView(Landroid/view/View;)V
aload 0
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
sipush 12290
invokevirtual android/widget/TextView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 3
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/h/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 3
iload 2
iconst_0
iload 2
iconst_0
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
aload 3
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
iconst_1
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
ldc_w -7829368
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
iconst_2
ldc_w 15.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
aconst_null
iconst_2
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 0
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
invokevirtual net/hockeyapp/android/f/h/addView(Landroid/view/View;)V
aload 0
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
sipush 12291
invokevirtual android/widget/TextView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 3
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/h/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 3
iload 2
iconst_0
iload 2
iload 2
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
aload 3
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
ldc_w -16777216
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
iconst_2
ldc_w 18.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
aconst_null
iconst_0
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 0
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
invokevirtual net/hockeyapp/android/f/h/addView(Landroid/view/View;)V
aload 0
new net/hockeyapp/android/f/a
dup
aload 1
invokespecial net/hockeyapp/android/f/a/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/h/h Lnet/hockeyapp/android/f/a;
aload 0
getfield net/hockeyapp/android/f/h/h Lnet/hockeyapp/android/f/a;
sipush 12292
invokevirtual net/hockeyapp/android/f/a/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/h/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 1
iload 2
iconst_0
iload 2
iload 2
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 0
getfield net/hockeyapp/android/f/h/h Lnet/hockeyapp/android/f/a;
aload 1
invokevirtual net/hockeyapp/android/f/a/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 0
getfield net/hockeyapp/android/f/h/h Lnet/hockeyapp/android/f/a;
invokevirtual net/hockeyapp/android/f/h/addView(Landroid/view/View;)V
return
.limit locals 4
.limit stack 5
.end method

.method private a()V
aload 0
iconst_1
invokevirtual net/hockeyapp/android/f/h/setOrientation(I)V
aload 0
iconst_3
invokevirtual net/hockeyapp/android/f/h/setGravity(I)V
aload 0
ldc_w -3355444
invokevirtual net/hockeyapp/android/f/h/setBackgroundColor(I)V
return
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/content/Context;)V
aload 0
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
sipush 12289
invokevirtual android/widget/TextView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/h/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 1
iload 2
iload 2
iload 2
iconst_0
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
iconst_1
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
ldc_w -7829368
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
iconst_2
ldc_w 15.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
aconst_null
iconst_0
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 0
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
invokevirtual net/hockeyapp/android/f/h/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 5
.end method

.method private b(Landroid/content/Context;)V
aload 0
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
sipush 12290
invokevirtual android/widget/TextView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/h/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 1
iload 2
iconst_0
iload 2
iconst_0
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
iconst_1
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
ldc_w -7829368
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
iconst_2
ldc_w 15.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
aconst_null
iconst_2
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 0
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
invokevirtual net/hockeyapp/android/f/h/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 5
.end method

.method private c(Landroid/content/Context;)V
aload 0
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
sipush 12291
invokevirtual android/widget/TextView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/h/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 1
iload 2
iconst_0
iload 2
iload 2
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
ldc_w -16777216
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
iconst_2
ldc_w 18.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
aconst_null
iconst_0
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 0
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
invokevirtual net/hockeyapp/android/f/h/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 5
.end method

.method private d(Landroid/content/Context;)V
aload 0
new net/hockeyapp/android/f/a
dup
aload 1
invokespecial net/hockeyapp/android/f/a/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/h/h Lnet/hockeyapp/android/f/a;
aload 0
getfield net/hockeyapp/android/f/h/h Lnet/hockeyapp/android/f/a;
sipush 12292
invokevirtual net/hockeyapp/android/f/a/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/h/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 1
iload 2
iconst_0
iload 2
iload 2
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 0
getfield net/hockeyapp/android/f/h/h Lnet/hockeyapp/android/f/a;
aload 1
invokevirtual net/hockeyapp/android/f/a/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 0
getfield net/hockeyapp/android/f/h/h Lnet/hockeyapp/android/f/a;
invokevirtual net/hockeyapp/android/f/h/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 5
.end method

.method private setAuthorLaberColor(I)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
ifnull L0
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
iload 1
invokevirtual android/widget/TextView/setTextColor(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private setDateLaberColor(I)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
ifnull L0
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
iload 1
invokevirtual android/widget/TextView/setTextColor(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private setMessageLaberColor(I)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
ifnull L0
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
iload 1
invokevirtual android/widget/TextView/setTextColor(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final setAuthorLabelText(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
ifnull L0
aload 1
ifnull L0
aload 0
getfield net/hockeyapp/android/f/h/e Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final setDateLabelText(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
ifnull L0
aload 1
ifnull L0
aload 0
getfield net/hockeyapp/android/f/h/f Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final setFeedbackMessageViewBgAndTextColor(I)V
iload 1
ifne L0
aload 0
ldc_w -3355444
invokevirtual net/hockeyapp/android/f/h/setBackgroundColor(I)V
aload 0
iconst_m1
invokespecial net/hockeyapp/android/f/h/setAuthorLaberColor(I)V
aload 0
iconst_m1
invokespecial net/hockeyapp/android/f/h/setDateLaberColor(I)V
L1:
aload 0
ldc_w -16777216
invokespecial net/hockeyapp/android/f/h/setMessageLaberColor(I)V
return
L0:
iload 1
iconst_1
if_icmpne L1
aload 0
iconst_m1
invokevirtual net/hockeyapp/android/f/h/setBackgroundColor(I)V
aload 0
ldc_w -3355444
invokespecial net/hockeyapp/android/f/h/setAuthorLaberColor(I)V
aload 0
ldc_w -3355444
invokespecial net/hockeyapp/android/f/h/setDateLaberColor(I)V
goto L1
.limit locals 2
.limit stack 2
.end method

.method public final setMessageLabelText(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
ifnull L0
aload 1
ifnull L0
aload 0
getfield net/hockeyapp/android/f/h/g Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L0:
return
.limit locals 2
.limit stack 2
.end method
