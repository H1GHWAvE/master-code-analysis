.bytecode 50.0
.class public synchronized net/hockeyapp/android/aa
.super android/app/Activity
.implements android/view/View$OnClickListener

.field private 'a' Ljava/lang/String;

.field private 'b' Ljava/lang/String;

.field private 'c' I

.field private 'd' Lnet/hockeyapp/android/d/p;

.field private 'e' Landroid/os/Handler;

.method public <init>()V
aload 0
invokespecial android/app/Activity/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/NoSuchAlgorithmException from L3 to L4 using L2
.catch java/security/NoSuchAlgorithmException from L4 to L5 using L2
.catch java/security/NoSuchAlgorithmException from L6 to L7 using L2
.catch java/security/NoSuchAlgorithmException from L8 to L9 using L2
L0:
ldc "MD5"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 3
aload 3
aload 0
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/security/MessageDigest/update([B)V
aload 3
invokevirtual java/security/MessageDigest/digest()[B
astore 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 4
aload 3
arraylength
istore 2
L1:
iconst_0
istore 1
L10:
iload 1
iload 2
if_icmpge L8
L3:
aload 3
iload 1
baload
sipush 255
iand
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
astore 0
L4:
aload 0
invokevirtual java/lang/String/length()I
iconst_2
if_icmpge L6
new java/lang/StringBuilder
dup
ldc "0"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L5:
goto L4
L6:
aload 4
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L7:
iload 1
iconst_1
iadd
istore 1
goto L10
L8:
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L9:
aload 0
areturn
L2:
astore 0
aload 0
invokevirtual java/security/NoSuchAlgorithmException/printStackTrace()V
ldc ""
areturn
.limit locals 5
.limit stack 3
.end method

.method private a()V
aload 0
getfield net/hockeyapp/android/aa/c I
iconst_1
if_icmpne L0
aload 0
sipush 12292
invokevirtual net/hockeyapp/android/aa/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
iconst_4
invokevirtual android/widget/EditText/setVisibility(I)V
L0:
aload 0
sipush 12293
invokevirtual net/hockeyapp/android/aa/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
aload 0
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
.limit locals 1
.limit stack 2
.end method

.method private b()V
aload 0
new net/hockeyapp/android/ab
dup
aload 0
invokespecial net/hockeyapp/android/ab/<init>(Lnet/hockeyapp/android/aa;)V
putfield net/hockeyapp/android/aa/e Landroid/os/Handler;
return
.limit locals 1
.limit stack 4
.end method

.method private c()V
iconst_1
istore 1
aload 0
sipush 12291
invokevirtual net/hockeyapp/android/aa/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
astore 2
aload 0
sipush 12292
invokevirtual net/hockeyapp/android/aa/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
astore 3
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 4
aload 0
getfield net/hockeyapp/android/aa/c I
iconst_1
if_icmpne L0
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L1
iconst_1
istore 1
L2:
aload 4
ldc "email"
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 4
ldc "authcode"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/aa/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic net/hockeyapp/android/aa/a(Ljava/lang/String;)Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L3:
iload 1
ifeq L4
aload 0
new net/hockeyapp/android/d/p
dup
aload 0
aload 0
getfield net/hockeyapp/android/aa/e Landroid/os/Handler;
aload 0
getfield net/hockeyapp/android/aa/a Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/aa/c I
aload 4
invokespecial net/hockeyapp/android/d/p/<init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;ILjava/util/Map;)V
putfield net/hockeyapp/android/aa/d Lnet/hockeyapp/android/d/p;
aload 0
getfield net/hockeyapp/android/aa/d Lnet/hockeyapp/android/d/p;
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
L1:
iconst_0
istore 1
goto L2
L0:
aload 0
getfield net/hockeyapp/android/aa/c I
iconst_2
if_icmpne L5
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L6
aload 3
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L6
L7:
aload 4
ldc "email"
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 4
ldc "password"
aload 3
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L3
L6:
iconst_0
istore 1
goto L7
L4:
aload 0
sipush 1281
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
sipush 1000
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
return
L5:
iconst_0
istore 1
goto L3
.limit locals 5
.limit stack 8
.end method

.method public onClick(Landroid/view/View;)V
iconst_1
istore 2
aload 1
invokevirtual android/view/View/getId()I
tableswitch 12293
L0
default : L1
L1:
return
L0:
aload 0
sipush 12291
invokevirtual net/hockeyapp/android/aa/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
astore 1
aload 0
sipush 12292
invokevirtual net/hockeyapp/android/aa/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
astore 3
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 4
aload 0
getfield net/hockeyapp/android/aa/c I
iconst_1
if_icmpne L2
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L3
iconst_1
istore 2
L4:
aload 4
ldc "email"
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 4
ldc "authcode"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/aa/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic net/hockeyapp/android/aa/a(Ljava/lang/String;)Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L5:
iload 2
ifeq L6
aload 0
new net/hockeyapp/android/d/p
dup
aload 0
aload 0
getfield net/hockeyapp/android/aa/e Landroid/os/Handler;
aload 0
getfield net/hockeyapp/android/aa/a Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/aa/c I
aload 4
invokespecial net/hockeyapp/android/d/p/<init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;ILjava/util/Map;)V
putfield net/hockeyapp/android/aa/d Lnet/hockeyapp/android/d/p;
aload 0
getfield net/hockeyapp/android/aa/d Lnet/hockeyapp/android/d/p;
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
L3:
iconst_0
istore 2
goto L4
L2:
aload 0
getfield net/hockeyapp/android/aa/c I
iconst_2
if_icmpne L7
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L8
aload 3
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L8
L9:
aload 4
ldc "email"
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 4
ldc "password"
aload 3
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L5
L8:
iconst_0
istore 2
goto L9
L6:
aload 0
sipush 1281
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
sipush 1000
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
return
L7:
iconst_0
istore 2
goto L5
.limit locals 5
.limit stack 8
.end method

.method protected onCreate(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/app/Activity/onCreate(Landroid/os/Bundle;)V
aload 0
new net/hockeyapp/android/f/j
dup
aload 0
invokespecial net/hockeyapp/android/f/j/<init>(Landroid/content/Context;)V
invokevirtual net/hockeyapp/android/aa/setContentView(Landroid/view/View;)V
aload 0
invokevirtual net/hockeyapp/android/aa/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
astore 1
aload 1
ifnull L0
aload 0
aload 1
ldc "url"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield net/hockeyapp/android/aa/a Ljava/lang/String;
aload 0
aload 1
ldc "secret"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield net/hockeyapp/android/aa/b Ljava/lang/String;
aload 0
aload 1
ldc "mode"
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;)I
putfield net/hockeyapp/android/aa/c I
L0:
aload 0
getfield net/hockeyapp/android/aa/c I
iconst_1
if_icmpne L1
aload 0
sipush 12292
invokevirtual net/hockeyapp/android/aa/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
iconst_4
invokevirtual android/widget/EditText/setVisibility(I)V
L1:
aload 0
sipush 12293
invokevirtual net/hockeyapp/android/aa/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
aload 0
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
new net/hockeyapp/android/ab
dup
aload 0
invokespecial net/hockeyapp/android/ab/<init>(Lnet/hockeyapp/android/aa;)V
putfield net/hockeyapp/android/aa/e Landroid/os/Handler;
aload 0
invokevirtual net/hockeyapp/android/aa/getLastNonConfigurationInstance()Ljava/lang/Object;
astore 1
aload 1
ifnull L2
aload 0
aload 1
checkcast net/hockeyapp/android/d/p
putfield net/hockeyapp/android/aa/d Lnet/hockeyapp/android/d/p;
aload 0
getfield net/hockeyapp/android/aa/d Lnet/hockeyapp/android/d/p;
astore 1
aload 0
getfield net/hockeyapp/android/aa/e Landroid/os/Handler;
astore 2
aload 1
aload 0
putfield net/hockeyapp/android/d/p/a Landroid/content/Context;
aload 1
aload 2
putfield net/hockeyapp/android/d/p/b Landroid/os/Handler;
L2:
return
.limit locals 3
.limit stack 4
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
iload 1
iconst_4
if_icmpne L0
getstatic net/hockeyapp/android/ac/g Lnet/hockeyapp/android/ae;
ifnonnull L0
new android/content/Intent
dup
aload 0
getstatic net/hockeyapp/android/ac/f Ljava/lang/Class;
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 2
aload 2
ldc_w 67108864
invokevirtual android/content/Intent/setFlags(I)Landroid/content/Intent;
pop
aload 2
ldc "net.hockeyapp.android.EXIT"
iconst_1
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
pop
aload 0
aload 2
invokevirtual net/hockeyapp/android/aa/startActivity(Landroid/content/Intent;)V
iconst_1
ireturn
L0:
aload 0
iload 1
aload 2
invokespecial android/app/Activity/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
.limit locals 3
.limit stack 4
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
aload 0
getfield net/hockeyapp/android/aa/d Lnet/hockeyapp/android/d/p;
ifnull L0
aload 0
getfield net/hockeyapp/android/aa/d Lnet/hockeyapp/android/d/p;
astore 1
aload 1
aconst_null
putfield net/hockeyapp/android/d/p/a Landroid/content/Context;
aload 1
aconst_null
putfield net/hockeyapp/android/d/p/b Landroid/os/Handler;
aload 1
aconst_null
putfield net/hockeyapp/android/d/p/c Landroid/app/ProgressDialog;
L0:
aload 0
getfield net/hockeyapp/android/aa/d Lnet/hockeyapp/android/d/p;
areturn
.limit locals 2
.limit stack 2
.end method
