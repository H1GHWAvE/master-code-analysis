.bytecode 50.0
.class public final synchronized net/hockeyapp/android/e/w
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "net.hockeyapp.android.prefs_feedback_token"

.field public static final 'b' Ljava/lang/String; = "net.hockeyapp.android.prefs_key_feedback_token"

.field public static final 'c' Ljava/lang/String; = "net.hockeyapp.android.prefs_name_email"

.field public static final 'd' Ljava/lang/String; = "net.hockeyapp.android.prefs_key_name_email"

.field public static final 'e' Ljava/lang/String; = "[0-9a-f]+"

.field public static final 'f' I = 32


.field public static final 'g' Ljava/lang/String; = "HockeyApp"

.field private static final 'h' Ljava/util/regex/Pattern;

.method static <clinit>()V
ldc "[0-9a-f]+"
iconst_2
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;
putstatic net/hockeyapp/android/e/w/h Ljava/util/regex/Pattern;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
ldc "android.app.Notification.Builder"
invokestatic net/hockeyapp/android/e/w/d(Ljava/lang/String;)Z
ifeq L0
iconst_1
istore 5
L1:
iload 5
ifeq L2
new android/app/Notification$Builder
dup
aload 0
invokespecial android/app/Notification$Builder/<init>(Landroid/content/Context;)V
aload 2
invokevirtual android/app/Notification$Builder/setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
aload 3
invokevirtual android/app/Notification$Builder/setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
aload 1
invokevirtual android/app/Notification$Builder/setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
iload 4
invokevirtual android/app/Notification$Builder/setSmallIcon(I)Landroid/app/Notification$Builder;
astore 0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmpge L3
aload 0
invokevirtual android/app/Notification$Builder/getNotification()Landroid/app/Notification;
areturn
L0:
iconst_0
istore 5
goto L1
L3:
aload 0
invokevirtual android/app/Notification$Builder/build()Landroid/app/Notification;
areturn
L2:
aload 0
aload 1
aload 2
aload 3
iload 4
invokestatic net/hockeyapp/android/e/w/b(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;
areturn
.limit locals 6
.limit stack 5
.end method

.method public static a()Ljava/lang/Boolean;
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "NewApi" 
.end annotation
.catch java/lang/NoClassDefFoundError from L0 to L1 using L2
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L3
ldc "android.app.Fragment"
invokestatic net/hockeyapp/android/e/w/d(Ljava/lang/String;)Z
ifeq L3
L1:
iconst_1
istore 0
L4:
iload 0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L3:
iconst_0
istore 0
goto L4
L2:
astore 1
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/lang/ref/WeakReference;)Ljava/lang/Boolean;
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/app/Activity
astore 0
aload 0
ifnull L0
aload 0
invokevirtual android/app/Activity/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getConfiguration()Landroid/content/res/Configuration;
astore 0
aload 0
getfield android/content/res/Configuration/screenLayout I
bipush 15
iand
iconst_3
if_icmpeq L1
aload 0
getfield android/content/res/Configuration/screenLayout I
bipush 15
iand
iconst_4
if_icmpne L2
L1:
iconst_1
istore 1
L3:
iload 1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L2:
iconst_0
istore 1
goto L3
L0:
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
L0:
aload 0
ldc "UTF-8"
invokestatic java/net/URLEncoder/encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
L1:
aload 0
areturn
L2:
astore 0
aload 0
invokevirtual java/io/UnsupportedEncodingException/printStackTrace()V
ldc ""
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/util/Map;)Ljava/lang/String;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 1
aload 0
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 4
aload 0
aload 4
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 3
aload 4
ldc "UTF-8"
invokestatic java/net/URLEncoder/encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 4
aload 3
ldc "UTF-8"
invokestatic java/net/URLEncoder/encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 3
aload 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L1:
ldc "&"
aload 1
invokestatic android/text/TextUtils/join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
areturn
.limit locals 5
.limit stack 3
.end method

.method private static b(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;
.catch java/lang/Exception from L0 to L1 using L2
new android/app/Notification
dup
iload 4
ldc ""
invokestatic java/lang/System/currentTimeMillis()J
invokespecial android/app/Notification/<init>(ILjava/lang/CharSequence;J)V
astore 5
L0:
aload 5
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
ldc "setLatestEventInfo"
iconst_4
anewarray java/lang/Class
dup
iconst_0
ldc android/content/Context
aastore
dup
iconst_1
ldc java/lang/CharSequence
aastore
dup
iconst_2
ldc java/lang/CharSequence
aastore
dup
iconst_3
ldc android/app/PendingIntent
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
aload 5
iconst_4
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 2
aastore
dup
iconst_2
aload 3
aastore
dup
iconst_3
aload 1
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 5
areturn
L2:
astore 0
aload 5
areturn
.limit locals 6
.limit stack 6
.end method

.method private static b()Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
ldc "android.app.Notification.Builder"
invokestatic net/hockeyapp/android/e/w/d(Ljava/lang/String;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 0
.limit stack 2
.end method

.method public static final b(Ljava/lang/String;)Z
.annotation invisible Landroid/annotation/TargetApi;
value I = 8
.end annotation
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L0
aload 0
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L1
getstatic android/util/Patterns/EMAIL_ADDRESS Ljava/util/regex/Pattern;
aload 0
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
invokevirtual java/util/regex/Matcher/matches()Z
ifeq L1
L2:
iconst_1
ireturn
L1:
iconst_0
ireturn
L0:
aload 0
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L2
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static c(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;
.annotation invisible Landroid/annotation/TargetApi;
value I = 11
.end annotation
new android/app/Notification$Builder
dup
aload 0
invokespecial android/app/Notification$Builder/<init>(Landroid/content/Context;)V
aload 2
invokevirtual android/app/Notification$Builder/setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
aload 3
invokevirtual android/app/Notification$Builder/setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
aload 1
invokevirtual android/app/Notification$Builder/setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
iload 4
invokevirtual android/app/Notification$Builder/setSmallIcon(I)Landroid/app/Notification$Builder;
astore 0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmpge L0
aload 0
invokevirtual android/app/Notification$Builder/getNotification()Landroid/app/Notification;
areturn
L0:
aload 0
invokevirtual android/app/Notification$Builder/build()Landroid/app/Notification;
areturn
.limit locals 5
.limit stack 3
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
aload 0
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "App ID must not be null."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 0
getstatic net/hockeyapp/android/e/w/h Ljava/util/regex/Pattern;
aload 0
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 1
aload 0
invokevirtual java/lang/String/length()I
bipush 32
if_icmpeq L1
new java/lang/IllegalArgumentException
dup
ldc "App ID length must be 32 characters."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
invokevirtual java/util/regex/Matcher/matches()Z
ifne L2
new java/lang/IllegalArgumentException
dup
ldc "App ID must match regex pattern /[0-9a-f]+/i"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private static d(Ljava/lang/String;)Z
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
iconst_0
istore 1
L0:
aload 0
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
astore 0
L1:
aload 0
ifnull L3
iconst_1
istore 1
L3:
iload 1
ireturn
L2:
astore 0
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method
