.bytecode 50.0
.class public final synchronized net/hockeyapp/android/a/a
.super android/widget/BaseAdapter

.field public 'a' Ljava/util/ArrayList;

.field private 'b' Landroid/content/Context;

.field private 'c' Ljava/text/SimpleDateFormat;

.field private 'd' Ljava/text/SimpleDateFormat;

.field private 'e' Ljava/util/Date;

.field private 'f' Landroid/widget/TextView;

.field private 'g' Landroid/widget/TextView;

.field private 'h' Landroid/widget/TextView;

.field private 'i' Lnet/hockeyapp/android/f/a;

.method public <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
aload 1
putfield net/hockeyapp/android/a/a/b Landroid/content/Context;
aload 0
aload 2
putfield net/hockeyapp/android/a/a/a Ljava/util/ArrayList;
aload 0
new java/text/SimpleDateFormat
dup
ldc "yyyy-MM-dd'T'HH:mm:ss'Z'"
invokespecial java/text/SimpleDateFormat/<init>(Ljava/lang/String;)V
putfield net/hockeyapp/android/a/a/c Ljava/text/SimpleDateFormat;
aload 0
new java/text/SimpleDateFormat
dup
ldc "d MMM h:mm a"
invokespecial java/text/SimpleDateFormat/<init>(Ljava/lang/String;)V
putfield net/hockeyapp/android/a/a/d Ljava/text/SimpleDateFormat;
return
.limit locals 3
.limit stack 4
.end method

.method private a()V
aload 0
getfield net/hockeyapp/android/a/a/a Ljava/util/ArrayList;
ifnull L0
aload 0
getfield net/hockeyapp/android/a/a/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method private a(Lnet/hockeyapp/android/c/g;)V
aload 1
ifnull L0
aload 0
getfield net/hockeyapp/android/a/a/a Ljava/util/ArrayList;
ifnull L0
aload 0
getfield net/hockeyapp/android/a/a/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final getCount()I
aload 0
getfield net/hockeyapp/android/a/a/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getItem(I)Ljava/lang/Object;
aload 0
getfield net/hockeyapp/android/a/a/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemId(I)J
iload 1
i2l
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.catch java/text/ParseException from L0 to L1 using L2
aload 0
getfield net/hockeyapp/android/a/a/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/hockeyapp/android/c/g
astore 3
aload 2
ifnonnull L3
new net/hockeyapp/android/f/h
dup
aload 0
getfield net/hockeyapp/android/a/a/b Landroid/content/Context;
invokespecial net/hockeyapp/android/f/h/<init>(Landroid/content/Context;)V
astore 2
L4:
aload 3
ifnull L5
aload 0
aload 2
sipush 12289
invokevirtual net/hockeyapp/android/f/h/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield net/hockeyapp/android/a/a/f Landroid/widget/TextView;
aload 0
aload 2
sipush 12290
invokevirtual net/hockeyapp/android/f/h/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield net/hockeyapp/android/a/a/g Landroid/widget/TextView;
aload 0
aload 2
sipush 12291
invokevirtual net/hockeyapp/android/f/h/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield net/hockeyapp/android/a/a/h Landroid/widget/TextView;
aload 0
aload 2
sipush 12292
invokevirtual net/hockeyapp/android/f/h/findViewById(I)Landroid/view/View;
checkcast net/hockeyapp/android/f/a
putfield net/hockeyapp/android/a/a/i Lnet/hockeyapp/android/f/a;
L0:
aload 0
aload 0
getfield net/hockeyapp/android/a/a/c Ljava/text/SimpleDateFormat;
aload 3
getfield net/hockeyapp/android/c/g/f Ljava/lang/String;
invokevirtual java/text/SimpleDateFormat/parse(Ljava/lang/String;)Ljava/util/Date;
putfield net/hockeyapp/android/a/a/e Ljava/util/Date;
aload 0
getfield net/hockeyapp/android/a/a/g Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/a/a/d Ljava/text/SimpleDateFormat;
aload 0
getfield net/hockeyapp/android/a/a/e Ljava/util/Date;
invokevirtual java/text/SimpleDateFormat/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L1:
aload 0
getfield net/hockeyapp/android/a/a/f Landroid/widget/TextView;
aload 3
getfield net/hockeyapp/android/c/g/l Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield net/hockeyapp/android/a/a/h Landroid/widget/TextView;
aload 3
getfield net/hockeyapp/android/c/g/b Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield net/hockeyapp/android/a/a/i Lnet/hockeyapp/android/f/a;
invokevirtual net/hockeyapp/android/f/a/removeAllViews()V
aload 3
getfield net/hockeyapp/android/c/g/n Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L6:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast net/hockeyapp/android/c/e
astore 4
new net/hockeyapp/android/f/b
dup
aload 0
getfield net/hockeyapp/android/a/a/b Landroid/content/Context;
aload 0
getfield net/hockeyapp/android/a/a/i Lnet/hockeyapp/android/f/a;
aload 4
invokespecial net/hockeyapp/android/f/b/<init>(Landroid/content/Context;Landroid/view/ViewGroup;Lnet/hockeyapp/android/c/e;)V
astore 5
getstatic net/hockeyapp/android/d/d/a Lnet/hockeyapp/android/d/a;
astore 6
aload 6
getfield net/hockeyapp/android/d/a/a Ljava/util/Queue;
new net/hockeyapp/android/d/e
dup
aload 4
aload 5
iconst_0
invokespecial net/hockeyapp/android/d/e/<init>(Lnet/hockeyapp/android/c/e;Lnet/hockeyapp/android/f/b;B)V
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
aload 6
invokevirtual net/hockeyapp/android/d/a/a()V
aload 0
getfield net/hockeyapp/android/a/a/i Lnet/hockeyapp/android/f/a;
aload 5
invokevirtual net/hockeyapp/android/f/a/addView(Landroid/view/View;)V
goto L6
L3:
aload 2
checkcast net/hockeyapp/android/f/h
astore 2
goto L4
L2:
astore 4
aload 4
invokevirtual java/text/ParseException/printStackTrace()V
goto L1
L5:
iload 1
iconst_2
irem
ifne L7
iconst_0
istore 1
L8:
aload 2
iload 1
invokevirtual net/hockeyapp/android/f/h/setFeedbackMessageViewBgAndTextColor(I)V
aload 2
areturn
L7:
iconst_1
istore 1
goto L8
.limit locals 7
.limit stack 6
.end method
