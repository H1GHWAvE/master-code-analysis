.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/c/a
.super java/lang/Object

.field public 'a' Ljava/lang/String;

.field public 'b' I

.field public 'c' Ljava/lang/String;

.field public 'd' I

.field public 'e' I

.field public 'f' Z

.field public 'g' Z

.field public 'h' Z

.field public 'i' Z

.field public 'j' Z

.field public 'k' Z

.field public 'l' Z

.field public 'm' F

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc ""
putfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
aload 0
iconst_0
putfield com/teamspeak/ts3client/c/a/b I
aload 0
ldc ""
putfield com/teamspeak/ts3client/c/a/c Ljava/lang/String;
aload 0
iconst_2
putfield com/teamspeak/ts3client/c/a/d I
aload 0
iconst_0
putfield com/teamspeak/ts3client/c/a/e I
aload 0
iconst_0
putfield com/teamspeak/ts3client/c/a/f Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/c/a/g Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/c/a/h Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/c/a/i Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/c/a/j Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/c/a/k Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/c/a/l Z
aload 0
fconst_0
putfield com/teamspeak/ts3client/c/a/m F
return
.limit locals 1
.limit stack 2
.end method

.method private a(F)V
aload 0
fload 1
putfield com/teamspeak/ts3client/c/a/m F
return
.limit locals 2
.limit stack 2
.end method

.method private a(I)V
aload 0
iload 1
putfield com/teamspeak/ts3client/c/a/b I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Z)V
aload 0
iload 1
putfield com/teamspeak/ts3client/c/a/k Z
return
.limit locals 2
.limit stack 2
.end method

.method private b(I)V
aload 0
iload 1
putfield com/teamspeak/ts3client/c/a/d I
return
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/String;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private b(Z)V
aload 0
iload 1
putfield com/teamspeak/ts3client/c/a/j Z
return
.limit locals 2
.limit stack 2
.end method

.method private c(I)V
aload 0
iload 1
putfield com/teamspeak/ts3client/c/a/e I
return
.limit locals 2
.limit stack 2
.end method

.method private c(Z)V
aload 0
iload 1
putfield com/teamspeak/ts3client/c/a/i Z
return
.limit locals 2
.limit stack 2
.end method

.method private d(Z)V
aload 0
iload 1
putfield com/teamspeak/ts3client/c/a/h Z
return
.limit locals 2
.limit stack 2
.end method

.method private e(Z)V
aload 0
iload 1
putfield com/teamspeak/ts3client/c/a/g Z
return
.limit locals 2
.limit stack 2
.end method

.method private f()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/c/a/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f(Z)V
aload 0
iload 1
putfield com/teamspeak/ts3client/c/a/f Z
return
.limit locals 2
.limit stack 2
.end method

.method private g()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g(Z)V
aload 0
iload 1
putfield com/teamspeak/ts3client/c/a/l Z
return
.limit locals 2
.limit stack 2
.end method

.method private h()I
aload 0
getfield com/teamspeak/ts3client/c/a/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i()I
aload 0
getfield com/teamspeak/ts3client/c/a/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private j()Z
aload 0
getfield com/teamspeak/ts3client/c/a/k Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private k()Z
aload 0
getfield com/teamspeak/ts3client/c/a/j Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private l()Z
aload 0
getfield com/teamspeak/ts3client/c/a/f Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private m()F
aload 0
getfield com/teamspeak/ts3client/c/a/m F
freturn
.limit locals 1
.limit stack 1
.end method

.method public final a()I
aload 0
getfield com/teamspeak/ts3client/c/a/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/String;)V
aload 0
aload 1
ldc "'"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokestatic android/database/DatabaseUtils/sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;
putfield com/teamspeak/ts3client/c/a/c Ljava/lang/String;
return
.limit locals 2
.limit stack 4
.end method

.method public final b()Z
aload 0
getfield com/teamspeak/ts3client/c/a/i Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Z
aload 0
getfield com/teamspeak/ts3client/c/a/h Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final d()Z
aload 0
getfield com/teamspeak/ts3client/c/a/g Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Z
aload 0
getfield com/teamspeak/ts3client/c/a/l Z
ireturn
.limit locals 1
.limit stack 1
.end method
