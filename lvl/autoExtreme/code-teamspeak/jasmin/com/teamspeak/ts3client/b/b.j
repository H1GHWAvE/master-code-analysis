.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/b/b
.super android/support/v4/app/Fragment
.implements com/teamspeak/ts3client/data/w

.field private 'a' Lcom/teamspeak/ts3client/b/f;

.field private 'b' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'c' Ljava/util/Vector;

.method public <init>()V
aload 0
invokespecial android/support/v4/app/Fragment/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/b/b;)Lcom/teamspeak/ts3client/b/f;
aload 0
getfield com/teamspeak/ts3client/b/b/a Lcom/teamspeak/ts3client/b/f;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a()V
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/teamspeak/ts3client/b/b/c Ljava/util/Vector;
aload 0
getfield com/teamspeak/ts3client/b/b/a Lcom/teamspeak/ts3client/b/f;
invokevirtual com/teamspeak/ts3client/b/f/clear()V
aload 0
getfield com/teamspeak/ts3client/b/b/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 0
getfield com/teamspeak/ts3client/b/b/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/b/b/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "requestbannlist"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestBanList(JLjava/lang/String;)I
pop
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/b/b;)Ljava/util/Vector;
aload 0
getfield com/teamspeak/ts3client/b/b/c Ljava/util/Vector;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/b/b;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/b/b/b Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/b/b;)V
aload 0
invokespecial com/teamspeak/ts3client/b/b/a()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 1
ldc_w 2130903068
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 1
aload 1
ldc_w 2131493002
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ListView
astore 2
aload 1
ldc_w 2131493003
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
new com/teamspeak/ts3client/b/c
dup
aload 0
invokespecial com/teamspeak/ts3client/b/c/<init>(Lcom/teamspeak/ts3client/b/b;)V
invokevirtual android/widget/EditText/addTextChangedListener(Landroid/text/TextWatcher;)V
aload 0
new com/teamspeak/ts3client/b/f
dup
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/b/b/i()Landroid/support/v4/app/bb;
invokespecial com/teamspeak/ts3client/b/f/<init>(Lcom/teamspeak/ts3client/b/b;Landroid/content/Context;)V
putfield com/teamspeak/ts3client/b/b/a Lcom/teamspeak/ts3client/b/f;
aload 2
aload 0
getfield com/teamspeak/ts3client/b/b/a Lcom/teamspeak/ts3client/b/f;
invokevirtual android/widget/ListView/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
putfield com/teamspeak/ts3client/b/b/b Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/b/b/b Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 0
invokevirtual com/teamspeak/ts3client/b/b/n()V
aload 0
getfield com/teamspeak/ts3client/b/b/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
ldc "menu.banlist"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v7/app/a/b(Ljava/lang/CharSequence;)V
aload 1
areturn
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
aload 1
invokeinterface android/view/Menu/clear()V 0
aload 0
aload 1
aload 2
invokespecial android/support/v4/app/Fragment/a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/BanList
ifeq L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/BanList
astore 2
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/b/e
dup
aload 0
aload 2
invokespecial com/teamspeak/ts3client/b/e/<init>(Lcom/teamspeak/ts3client/b/b;Lcom/teamspeak/ts3client/jni/events/rare/BanList;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ServerError
ifeq L1
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
ldc "requestbannlist"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 0
getfield com/teamspeak/ts3client/b/b/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
L1:
return
.limit locals 3
.limit stack 5
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/Fragment/e()V
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/teamspeak/ts3client/b/b/c Ljava/util/Vector;
aload 0
getfield com/teamspeak/ts3client/b/b/a Lcom/teamspeak/ts3client/b/f;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/b/b/a Lcom/teamspeak/ts3client/b/f;
invokevirtual com/teamspeak/ts3client/b/f/clear()V
L0:
return
.limit locals 1
.limit stack 3
.end method

.method public final f()V
aload 0
invokespecial android/support/v4/app/Fragment/f()V
return
.limit locals 1
.limit stack 1
.end method

.method public final s()V
aload 0
invokespecial android/support/v4/app/Fragment/s()V
aload 0
invokespecial com/teamspeak/ts3client/b/b/a()V
return
.limit locals 1
.limit stack 1
.end method
