.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/ConnectionBackground
.super android/app/Service
.implements com/teamspeak/ts3client/data/w
.implements com/teamspeak/ts3client/tsdns/i

.field private static 'j' Landroid/app/Notification;

.field private static 'k' Z

.field private static 'l' Landroid/support/v4/app/dm;

.field protected 'a' J

.field protected 'b' Z

.field protected 'c' Z

.field protected 'd' I

.field 'e' Lcom/teamspeak/ts3client/Ts3Application;

.field 'f' Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field 'g' Z

.field 'h' Z

.field public 'i' Landroid/content/BroadcastReceiver;

.field private 'm' Z

.field private 'n' I

.field private 'o' Z

.field private 'p' Z

.field private 'q' Ljava/util/ArrayList;

.field private 'r' Z

.field private 's' Landroid/view/WindowManager;

.field private 't' Landroid/widget/ImageView;

.field private 'u' Landroid/view/WindowManager$LayoutParams;

.field private 'v' Z

.field private 'w' Landroid/view/animation/Animation;

.field private 'x' Landroid/content/IntentFilter;

.method public <init>()V
aload 0
invokespecial android/app/Service/<init>()V
aload 0
lconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/a J
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/g Z
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/h Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/m Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/n I
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/o Z
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/ConnectionBackground/q Ljava/util/ArrayList;
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/r Z
aload 0
new com/teamspeak/ts3client/l
dup
aload 0
invokespecial com/teamspeak/ts3client/l/<init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V
putfield com/teamspeak/ts3client/ConnectionBackground/i Landroid/content/BroadcastReceiver;
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
putfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
aload 0
putfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 0
new com/teamspeak/ts3client/s
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/s/<init>(Lcom/teamspeak/ts3client/Ts3Application;)V
putfield com/teamspeak/ts3client/ConnectionBackground/f Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/f Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
invokeinterface android/content/SharedPreferences/registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "talk_notification"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
putstatic com/teamspeak/ts3client/ConnectionBackground/k Z
return
.limit locals 1
.limit stack 4
.end method

.method private a(Ljava/util/SortedMap;[Lcom/teamspeak/ts3client/data/a;JIILjava/util/concurrent/ConcurrentHashMap;Ljava/lang/Boolean;)I
aload 1
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokeinterface java/util/SortedMap/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/SortedMap
astore 12
aload 12
invokeinterface java/util/SortedMap/size()I 0
anewarray com/teamspeak/ts3client/data/a
astore 13
lconst_0
lstore 3
iconst_0
istore 9
L0:
iload 9
aload 12
invokeinterface java/util/SortedMap/size()I 0
if_icmpge L1
aload 12
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokeinterface java/util/SortedMap/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
astore 14
aload 13
iload 9
aload 14
aastore
iload 9
iconst_1
iadd
istore 9
aload 14
getfield com/teamspeak/ts3client/data/a/b J
lstore 3
goto L0
L1:
aload 8
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L2
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 12
L3:
aload 13
arraylength
istore 11
iconst_0
istore 9
L4:
iload 9
iload 11
if_icmpge L5
aload 13
iload 9
aaload
astore 14
aload 14
iconst_0
putfield com/teamspeak/ts3client/data/a/q Z
aload 1
aload 14
getfield com/teamspeak/ts3client/data/a/b J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokeinterface java/util/SortedMap/containsKey(Ljava/lang/Object;)Z 1
ifeq L6
aload 14
iload 6
putfield com/teamspeak/ts3client/data/a/f I
aload 14
aload 12
invokevirtual java/lang/Boolean/booleanValue()Z
putfield com/teamspeak/ts3client/data/a/p Z
aload 14
getfield com/teamspeak/ts3client/data/a/p Z
ifne L7
aload 2
iload 5
aload 14
aastore
L7:
aload 14
getfield com/teamspeak/ts3client/data/a/o Z
ifne L8
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 12
L8:
aload 0
aload 1
aload 2
aload 14
getfield com/teamspeak/ts3client/data/a/b J
iload 5
iconst_1
iadd
iload 6
iconst_1
iadd
aload 7
aload 12
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(Ljava/util/SortedMap;[Lcom/teamspeak/ts3client/data/a;JIILjava/util/concurrent/ConcurrentHashMap;Ljava/lang/Boolean;)I
istore 10
iload 10
istore 5
aload 8
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L9
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 12
iload 10
istore 5
L9:
iload 9
iconst_1
iadd
istore 9
goto L4
L6:
aload 14
iload 6
putfield com/teamspeak/ts3client/data/a/f I
aload 14
aload 12
invokevirtual java/lang/Boolean/booleanValue()Z
putfield com/teamspeak/ts3client/data/a/p Z
aload 14
getfield com/teamspeak/ts3client/data/a/p Z
ifne L10
aload 2
iload 5
aload 14
aastore
aload 2
iload 5
aaload
invokevirtual com/teamspeak/ts3client/data/a/j()Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/size()I
ifne L10
aload 2
iload 5
aaload
iconst_1
putfield com/teamspeak/ts3client/data/a/q Z
L10:
iload 5
iconst_1
iadd
istore 5
goto L9
L5:
iload 5
ireturn
L2:
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 12
goto L3
.limit locals 15
.limit stack 9
.end method

.method public static a()Landroid/app/Notification;
getstatic com/teamspeak/ts3client/ConnectionBackground/j Landroid/app/Notification;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/u Landroid/view/WindowManager$LayoutParams;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifne L0
return
L0:
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/b Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/data/d/x/a(Ljava/lang/String;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 5
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/f Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_0
lcmp
ifne L1
iconst_0
istore 3
L2:
aload 5
iload 3
putfield com/teamspeak/ts3client/data/c/f Z
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 5
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/g Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_0
lcmp
ifne L3
iconst_0
istore 3
L4:
aload 5
iload 3
invokevirtual com/teamspeak/ts3client/data/c/a(Z)V
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 5
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/j Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_1
lcmp
ifne L5
iconst_0
istore 3
L6:
aload 5
iload 3
putfield com/teamspeak/ts3client/data/c/i Z
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 5
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/i Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_1
lcmp
ifne L7
iconst_0
istore 3
L8:
aload 5
iload 3
putfield com/teamspeak/ts3client/data/c/h Z
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/I Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/t Ljava/lang/String;
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/H Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/u Ljava/lang/String;
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/O Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
putfield com/teamspeak/ts3client/data/c/v I
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 5
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/M Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
iconst_1
if_icmpne L9
iconst_1
istore 3
L10:
aload 5
iload 3
putfield com/teamspeak/ts3client/data/c/j Z
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/N Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/m Ljava/lang/String;
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 5
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/af Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
iconst_1
if_icmpne L11
iconst_1
istore 3
L12:
aload 5
iload 3
putfield com/teamspeak/ts3client/data/c/n Z
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/a Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/c/b(Ljava/lang/String;)V
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 5
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/Z Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_0
lcmp
ifne L13
iconst_0
istore 3
L14:
aload 5
iload 3
putfield com/teamspeak/ts3client/data/c/k Z
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/Q Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
putfield com/teamspeak/ts3client/data/c/o I
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/R Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/p Ljava/lang/String;
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/S Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/q Ljava/lang/String;
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/ae Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
invokevirtual com/teamspeak/ts3client/data/c/a(J)V
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/s Z
istore 4
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/r Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_0
lcmp
ifne L15
iconst_0
istore 3
L16:
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
iload 3
putfield com/teamspeak/ts3client/data/c/s Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
astore 5
iload 4
ifeq L17
iload 3
ifne L17
aload 5
getfield com/teamspeak/ts3client/data/a/b J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/g J
lcmp
ifne L17
ldc "event.client.recording.stop"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/av Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
iconst_0
aload 5
getfield com/teamspeak/ts3client/data/a/a Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L17:
iload 4
ifne L18
iload 3
ifeq L18
aload 5
getfield com/teamspeak/ts3client/data/a/b J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/g J
lcmp
ifne L18
ldc "event.client.recording.started"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/au Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
iconst_0
aload 5
getfield com/teamspeak/ts3client/data/a/a Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L18:
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/b()Ljava/lang/String;
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/q Ljava/util/ArrayList;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L19
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
iconst_1
putfield com/teamspeak/ts3client/data/c/A I
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
iconst_0
putfield com/teamspeak/ts3client/data/c/d I
L19:
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/r Z
istore 4
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 6
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/U Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_0
lcmp
ifne L20
iconst_0
istore 3
L21:
aload 6
iload 3
putfield com/teamspeak/ts3client/data/c/r Z
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/r Z
istore 3
aload 5
getfield com/teamspeak/ts3client/data/a/h I
istore 2
iload 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/h I
if_icmpeq L22
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/p Ljava/lang/String;
ldc "0"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L22
aload 5
getfield com/teamspeak/ts3client/data/a/b J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/g J
lcmp
ifne L22
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/do Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifeq L22
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/ax Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L22:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/h I
iload 1
if_icmpne L23
iload 2
ifle L23
iload 4
iload 3
if_icmpeq L23
iload 3
ifeq L24
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aD Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L23:
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/v I
iconst_1
if_icmpne L25
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/ac Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
putfield com/teamspeak/ts3client/data/c/e I
L25:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iconst_1
putfield com/teamspeak/ts3client/data/a/d Z
return
L1:
iconst_1
istore 3
goto L2
L3:
iconst_1
istore 3
goto L4
L5:
iconst_1
istore 3
goto L6
L7:
iconst_1
istore 3
goto L8
L9:
iconst_0
istore 3
goto L10
L11:
iconst_0
istore 3
goto L12
L13:
iconst_1
istore 3
goto L14
L15:
iconst_1
istore 3
goto L16
L20:
iconst_1
istore 3
goto L21
L24:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aE Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L23
.limit locals 7
.limit stack 8
.end method

.method private a(IJJ)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iload 1
invokevirtual com/teamspeak/ts3client/data/a/e(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 4
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iload 1
invokevirtual com/teamspeak/ts3client/data/a/d(I)V
return
.limit locals 6
.limit stack 3
.end method

.method private a(J)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/j()Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 4
L0:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 3
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/d Lcom/teamspeak/ts3client/data/d;
iload 3
invokevirtual com/teamspeak/ts3client/data/d/b(I)Lcom/teamspeak/ts3client/data/c;
astore 5
aload 5
ifnull L0
aload 5
getfield com/teamspeak/ts3client/data/c/s Z
ifeq L0
ldc "event.client.recording.isrecording"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 5
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aw Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 5
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/a Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L0
L1:
return
.limit locals 6
.limit stack 9
.end method

.method private a(Landroid/view/WindowManager$LayoutParams;)V
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getConfiguration()Landroid/content/res/Configuration;
getfield android/content/res/Configuration/orientation I
iconst_2
if_icmpne L0
aload 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "overlay_last_x_l"
bipush 100
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
putfield android/view/WindowManager$LayoutParams/x I
aload 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "overlay_last_y_l"
bipush 100
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
putfield android/view/WindowManager$LayoutParams/y I
return
L0:
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getConfiguration()Landroid/content/res/Configuration;
getfield android/content/res/Configuration/orientation I
iconst_1
if_icmpne L1
aload 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "overlay_last_x_p"
bipush 100
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
putfield android/view/WindowManager$LayoutParams/x I
aload 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "overlay_last_y_p"
bipush 100
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
putfield android/view/WindowManager$LayoutParams/y I
return
L1:
aload 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "overlay_last_x_u"
bipush 100
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
putfield android/view/WindowManager$LayoutParams/x I
aload 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "overlay_last_y_u"
bipush 100
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
putfield android/view/WindowManager$LayoutParams/y I
return
.limit locals 2
.limit stack 4
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/ConnectionBackground;Landroid/view/WindowManager$LayoutParams;)V
aload 0
aload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(Landroid/view/WindowManager$LayoutParams;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Lcom/teamspeak/ts3client/data/a;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/g J
lstore 2
aload 0
aload 1
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(Lcom/teamspeak/ts3client/data/a;)V
return
.limit locals 4
.limit stack 2
.end method

.method public static a(Z)V
iload 0
ifeq L0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/d Lcom/teamspeak/ts3client/data/d;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/h I
invokevirtual com/teamspeak/ts3client/data/d/b(I)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/d I
ifne L1
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
ldc_w 2130837622
invokevirtual android/support/v4/app/dm/a(I)Landroid/support/v4/app/dm;
pop
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
putstatic com/teamspeak/ts3client/ConnectionBackground/j Landroid/app/Notification;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
iconst_1
getstatic com/teamspeak/ts3client/ConnectionBackground/j Landroid/app/Notification;
invokevirtual android/app/NotificationManager/notify(ILandroid/app/Notification;)V
return
L1:
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
ldc_w 2130837662
invokevirtual android/support/v4/app/dm/a(I)Landroid/support/v4/app/dm;
pop
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
ldc_w 2130837662
ldc_w 48.0F
ldc_w 48.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
putfield android/support/v4/app/dm/g Landroid/graphics/Bitmap;
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
astore 1
aload 1
ldc_w -16776961
putfield android/app/Notification/ledARGB I
aload 1
aload 1
getfield android/app/Notification/flags I
iconst_3
ior
putfield android/app/Notification/flags I
aload 1
iconst_0
putfield android/app/Notification/ledOffMS I
aload 1
iconst_1
putfield android/app/Notification/ledOnMS I
aload 1
putstatic com/teamspeak/ts3client/ConnectionBackground/j Landroid/app/Notification;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
iconst_1
getstatic com/teamspeak/ts3client/ConnectionBackground/j Landroid/app/Notification;
invokevirtual android/app/NotificationManager/notify(ILandroid/app/Notification;)V
return
L0:
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
ldc_w 2130837622
invokevirtual android/support/v4/app/dm/a(I)Landroid/support/v4/app/dm;
pop
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837622
invokestatic android/graphics/BitmapFactory/decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
putfield android/support/v4/app/dm/g Landroid/graphics/Bitmap;
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
putstatic com/teamspeak/ts3client/ConnectionBackground/j Landroid/app/Notification;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
iconst_1
getstatic com/teamspeak/ts3client/ConnectionBackground/j Landroid/app/Notification;
invokevirtual android/app/NotificationManager/notify(ILandroid/app/Notification;)V
return
.limit locals 2
.limit stack 4
.end method

.method private a(IJ)Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/c Ljava/util/concurrent/CopyOnWriteArrayList;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/contains(Ljava/lang/Object;)Z
ireturn
.limit locals 4
.limit stack 3
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/ConnectionBackground;Z)Z
aload 0
iload 1
putfield com/teamspeak/ts3client/ConnectionBackground/v Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/widget/ImageView;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/t Landroid/widget/ImageView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/d Lcom/teamspeak/ts3client/data/d;
iload 1
invokevirtual com/teamspeak/ts3client/data/d/b(I)Lcom/teamspeak/ts3client/data/c;
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(J)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/j()Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 4
aload 4
ifnonnull L2
L1:
return
L2:
aload 4
iconst_0
putfield com/teamspeak/ts3client/data/c/d I
goto L0
.limit locals 5
.limit stack 3
.end method

.method private b(Lcom/teamspeak/ts3client/data/a;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 1
getfield com/teamspeak/ts3client/data/a/b J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getChannelClientList(JJ)[I
astore 6
aload 6
arraylength
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 6
iload 2
iaload
istore 4
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/d Lcom/teamspeak/ts3client/data/d;
iload 4
invokevirtual com/teamspeak/ts3client/data/d/a(I)Z
ifne L2
new com/teamspeak/ts3client/data/c
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 4
getstatic com/teamspeak/ts3client/jni/d/b Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
iload 4
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/c/<init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V
astore 7
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/f Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_0
lcmp
ifne L3
iconst_0
istore 5
L4:
aload 7
iload 5
putfield com/teamspeak/ts3client/data/c/f Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/g Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_0
lcmp
ifne L5
iconst_0
istore 5
L6:
aload 7
iload 5
invokevirtual com/teamspeak/ts3client/data/c/a(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/j Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_1
lcmp
ifne L7
iconst_0
istore 5
L8:
aload 7
iload 5
putfield com/teamspeak/ts3client/data/c/i Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/i Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_1
lcmp
ifne L9
iconst_0
istore 5
L10:
aload 7
iload 5
putfield com/teamspeak/ts3client/data/c/h Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/r Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_0
lcmp
ifne L11
iconst_0
istore 5
L12:
aload 7
iload 5
putfield com/teamspeak/ts3client/data/c/s Z
aload 1
getfield com/teamspeak/ts3client/data/a/b J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/g J
lcmp
ifne L13
aload 7
getfield com/teamspeak/ts3client/data/c/c I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/h I
if_icmpeq L14
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/q Ljava/util/ArrayList;
aload 7
getfield com/teamspeak/ts3client/data/c/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L15
aload 7
iconst_1
putfield com/teamspeak/ts3client/data/c/A I
L14:
aload 7
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/I Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/t Ljava/lang/String;
aload 7
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/H Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/u Ljava/lang/String;
aload 7
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/O Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
putfield com/teamspeak/ts3client/data/c/v I
aload 7
getfield com/teamspeak/ts3client/data/c/v I
iconst_1
if_icmpne L16
aload 7
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/ac Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
putfield com/teamspeak/ts3client/data/c/e I
L16:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/M Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
iconst_1
if_icmpne L17
iconst_1
istore 5
L18:
aload 7
iload 5
putfield com/teamspeak/ts3client/data/c/j Z
aload 7
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/N Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/m Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/af Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
iconst_1
if_icmpne L19
iconst_1
istore 5
L20:
aload 7
iload 5
putfield com/teamspeak/ts3client/data/c/n Z
aload 7
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/a Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/c/b(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/Z Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_0
lcmp
ifne L21
iconst_0
istore 5
L22:
aload 7
iload 5
putfield com/teamspeak/ts3client/data/c/k Z
aload 7
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/Q Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
putfield com/teamspeak/ts3client/data/c/o I
aload 7
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/R Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/p Ljava/lang/String;
aload 7
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/S Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/q Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/U Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lconst_0
lcmp
ifne L23
iconst_0
istore 5
L24:
aload 7
iload 5
putfield com/teamspeak/ts3client/data/c/r Z
aload 7
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 7
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/ae Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
invokevirtual com/teamspeak/ts3client/data/c/a(J)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/d Lcom/teamspeak/ts3client/data/d;
aload 7
invokevirtual com/teamspeak/ts3client/data/d/a(Lcom/teamspeak/ts3client/data/c;)V
aload 7
invokevirtual com/teamspeak/ts3client/data/c/a()V
L2:
aload 1
iload 4
invokevirtual com/teamspeak/ts3client/data/a/d(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 1
getfield com/teamspeak/ts3client/data/a/b J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L3:
iconst_1
istore 5
goto L4
L5:
iconst_1
istore 5
goto L6
L7:
iconst_1
istore 5
goto L8
L9:
iconst_1
istore 5
goto L10
L11:
iconst_1
istore 5
goto L12
L15:
aload 7
iconst_0
putfield com/teamspeak/ts3client/data/c/A I
goto L14
L13:
aload 7
getfield com/teamspeak/ts3client/data/c/c I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/h I
if_icmpeq L14
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/q Ljava/util/ArrayList;
aload 7
getfield com/teamspeak/ts3client/data/c/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L25
aload 7
iconst_1
putfield com/teamspeak/ts3client/data/c/A I
goto L14
L25:
aload 7
iconst_0
putfield com/teamspeak/ts3client/data/c/A I
goto L14
L17:
iconst_0
istore 5
goto L18
L19:
iconst_0
istore 5
goto L20
L21:
iconst_1
istore 5
goto L22
L23:
iconst_1
istore 5
goto L24
L1:
return
.limit locals 8
.limit stack 7
.end method

.method static synthetic b(Z)Z
iload 0
putstatic com/teamspeak/ts3client/ConnectionBackground/k Z
iload 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/s Landroid/view/WindowManager;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/content/IntentFilter;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/x Landroid/content/IntentFilter;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e()Z
getstatic com/teamspeak/ts3client/ConnectionBackground/k Z
ireturn
.limit locals 0
.limit stack 1
.end method

.method private f()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_ptt"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L0
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/r Z
L1:
return
L0:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/cw Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifeq L2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/r Z
ifne L2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/H()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/k Lcom/teamspeak/ts3client/jni/d;
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "voiceactivation_level"
ldc "-50"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
iconst_1
invokevirtual com/teamspeak/ts3client/t/i(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
astore 2
aload 2
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/ai
dup
aload 2
invokespecial com/teamspeak/ts3client/ai/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
ldc "messages.forceptt.on"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "messages.forceptt.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/r Z
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/c()V
return
L2:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/r Z
ifeq L1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/d()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/k Lcom/teamspeak/ts3client/jni/d;
iconst_0
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "voiceactivation_level"
bipush 10
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "voiceactivation_level"
iload 1
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
iconst_0
invokevirtual com/teamspeak/ts3client/t/i(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/C()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
ldc "messages.forceptt.off"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "messages.forceptt.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/d()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/r Z
return
.limit locals 3
.limit stack 7
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/ConnectionBackground;)V
aload 0
invokespecial com/teamspeak/ts3client/ConnectionBackground/f()V
return
.limit locals 1
.limit stack 1
.end method

.method private g()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getChannelList(J)[J
astore 11
aload 11
arraylength
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 11
iload 1
laload
lstore 4
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lload 4
getstatic com/teamspeak/ts3client/jni/c/a Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JJLcom/teamspeak/ts3client/jni/c;)Ljava/lang/String;
astore 12
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lload 4
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getParentChannelOfChannel(JJ)J
lstore 6
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lload 4
getstatic com/teamspeak/ts3client/jni/c/i Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JJLcom/teamspeak/ts3client/jni/c;)J
lstore 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lload 4
getstatic com/teamspeak/ts3client/jni/c/m Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
istore 3
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lload 4
getstatic com/teamspeak/ts3client/jni/c/i Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JJLcom/teamspeak/ts3client/jni/c;)J
pop2
new com/teamspeak/ts3client/data/a
dup
aload 12
lload 4
lload 6
lload 8
invokespecial com/teamspeak/ts3client/data/a/<init>(Ljava/lang/String;JJJ)V
astore 12
aload 12
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lload 4
getstatic com/teamspeak/ts3client/jni/c/C Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
putfield com/teamspeak/ts3client/data/a/h I
aload 12
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lload 4
getstatic com/teamspeak/ts3client/jni/c/e Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
putfield com/teamspeak/ts3client/data/a/l I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lload 4
getstatic com/teamspeak/ts3client/jni/c/l Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
iconst_1
if_icmpne L2
iconst_1
istore 10
L3:
aload 12
iload 10
putfield com/teamspeak/ts3client/data/a/r Z
aload 12
getfield com/teamspeak/ts3client/data/a/r Z
ifeq L4
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 12
putfield com/teamspeak/ts3client/data/b/b Lcom/teamspeak/ts3client/data/a;
L4:
aload 12
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lload 4
getstatic com/teamspeak/ts3client/jni/c/g Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
putfield com/teamspeak/ts3client/data/a/i I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lload 4
getstatic com/teamspeak/ts3client/jni/c/G Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
iconst_1
if_icmpne L5
iconst_1
istore 10
L6:
aload 12
iload 10
putfield com/teamspeak/ts3client/data/a/j Z
aload 12
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lload 4
getstatic com/teamspeak/ts3client/jni/c/F Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JJLcom/teamspeak/ts3client/jni/c;)J
invokevirtual com/teamspeak/ts3client/data/a/c(J)V
iload 3
iconst_1
if_icmpne L7
aload 12
iconst_1
putfield com/teamspeak/ts3client/data/a/m Z
L7:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 12
invokevirtual com/teamspeak/ts3client/data/b/a(Lcom/teamspeak/ts3client/data/a;)V
iload 1
iconst_1
iadd
istore 1
goto L0
L2:
iconst_0
istore 10
goto L3
L5:
iconst_0
istore 10
goto L6
L1:
return
.limit locals 13
.limit stack 9
.end method

.method private h()Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/g Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i()Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/o Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private j()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getClientID(J)I
istore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
getstatic com/teamspeak/ts3client/jni/d/G Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
lstore 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getChannelOfClient(JI)J
lstore 4
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
iload 1
putfield com/teamspeak/ts3client/data/e/h I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
lload 2
putfield com/teamspeak/ts3client/data/e/z J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
lload 4
putfield com/teamspeak/ts3client/data/e/g J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 4
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iconst_1
putfield com/teamspeak/ts3client/data/a/k Z
new android/content/Intent
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
ldc com/teamspeak/ts3client/StartGUIFragment
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 6
aload 6
ldc "android.intent.action.MAIN"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 6
ldc "android.intent.category.LAUNCHER"
invokevirtual android/content/Intent/addCategory(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
iconst_0
aload 6
iconst_0
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 6
new android/support/v4/app/dm
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
invokespecial android/support/v4/app/dm/<init>(Landroid/content/Context;)V
astore 7
aload 7
putstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
aload 7
ldc_w 2130837622
invokevirtual android/support/v4/app/dm/a(I)Landroid/support/v4/app/dm;
ldc "messages.notification.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
invokestatic java/lang/System/currentTimeMillis()J
invokevirtual android/support/v4/app/dm/a(J)Landroid/support/v4/app/dm;
astore 7
aload 7
iconst_2
iconst_1
invokevirtual android/support/v4/app/dm/a(IZ)V
aload 7
invokevirtual android/support/v4/app/dm/a()Landroid/support/v4/app/dm;
ldc "messages.notification.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/b Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 6
putfield android/support/v4/app/dm/d Landroid/app/PendingIntent;
aload 0
iconst_1
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
invokevirtual com/teamspeak/ts3client/ConnectionBackground/startForeground(ILandroid/app/Notification;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
lload 4
putfield com/teamspeak/ts3client/data/ab/q J
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/j Ljava/util/HashMap;
lload 4
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/j Ljava/util/HashMap;
lload 4
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/r Ljava/lang/String;
return
L0:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
ldc ""
putfield com/teamspeak/ts3client/data/ab/r Ljava/lang/String;
return
.limit locals 8
.limit stack 5
.end method

.method private k()V
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/b()V
return
.limit locals 1
.limit stack 1
.end method

.method private l()V
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/h Z
return
.limit locals 1
.limit stack 2
.end method

.method private m()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/f Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
invokeinterface android/content/SharedPreferences/unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V 1
return
.limit locals 1
.limit stack 2
.end method

.method private n()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
getfield com/teamspeak/ts3client/data/b/a Ljava/util/concurrent/ConcurrentHashMap;
invokevirtual java/util/concurrent/ConcurrentHashMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/teamspeak/ts3client/data/a
astore 4
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/g J
lstore 1
aload 0
aload 4
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(Lcom/teamspeak/ts3client/data/a;)V
goto L0
L1:
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/b()V
return
.limit locals 5
.limit stack 2
.end method

.method private o()Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/m Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L8
.catch java/lang/Exception from L9 to L10 using L5
.catch java/lang/Exception from L11 to L12 using L13
.catch java/lang/Exception from L14 to L15 using L16
.catch java/lang/Exception from L17 to L18 using L19
.catch java/lang/Exception from L18 to L20 using L19
.catch java/lang/Exception from L21 to L22 using L23
.catch java/lang/Exception from L22 to L24 using L23
.catch java/lang/Exception from L25 to L26 using L27
.catch java/lang/Exception from L28 to L29 using L30
aload 1
instanceof com/teamspeak/ts3client/jni/events/ServerStop
ifeq L31
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerStop
astore 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/b Lcom/teamspeak/ts3client/jni/h;
aconst_null
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
new android/content/Intent
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
ldc com/teamspeak/ts3client/StartGUIFragment
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 9
aload 9
ldc "android.intent.action.MAIN"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 9
ldc "android.intent.category.LAUNCHER"
invokevirtual android/content/Intent/addCategory(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
iconst_0
aload 9
iconst_0
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 9
new android/support/v4/app/dm
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
invokespecial android/support/v4/app/dm/<init>(Landroid/content/Context;)V
astore 10
aload 10
ldc_w 2130837622
invokevirtual android/support/v4/app/dm/a(I)Landroid/support/v4/app/dm;
ldc "messages.servershutdown.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
invokestatic java/lang/System/currentTimeMillis()J
invokevirtual android/support/v4/app/dm/a(J)Landroid/support/v4/app/dm;
invokevirtual android/support/v4/app/dm/a()Landroid/support/v4/app/dm;
ldc "messages.servershutdown.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/ServerStop/a()Ljava/lang/String;
invokevirtual android/support/v4/app/dm/b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 9
invokevirtual android/support/v4/app/dm/a(Landroid/app/PendingIntent;)Landroid/support/v4/app/dm;
pop
aload 10
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
astore 8
aload 8
aload 8
getfield android/app/Notification/flags I
bipush 20
ior
putfield android/app/Notification/flags I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/j()Landroid/app/NotificationManager;
aconst_null
bipush 102
aload 8
invokevirtual android/app/NotificationManager/notify(Ljava/lang/String;ILandroid/app/Notification;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/d()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
astore 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
pop
aload 8
invokevirtual com/teamspeak/ts3client/t/E()V
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/m Z
L31:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ServerPermissionError
ifeq L32
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ServerPermissionError
astore 8
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerPermissionError/c()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/j()Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/bK Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
if_icmpne L33
L34:
return
L33:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/G Lcom/teamspeak/ts3client/jni/h;
aconst_null
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerPermissionError/b()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerPermissionError/a()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc "messages.serverpermission.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.close"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
L32:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ServerEdited
ifeq L35
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
getstatic com/teamspeak/ts3client/jni/i/b Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/e/a(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
getstatic com/teamspeak/ts3client/jni/i/ap Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JLcom/teamspeak/ts3client/jni/i;)J
invokevirtual com/teamspeak/ts3client/data/ab/a(J)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getBaseContext()Landroid/content/Context;
invokestatic com/teamspeak/ts3client/chat/d/a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;
invokevirtual com/teamspeak/ts3client/chat/d/c()Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/chat/a/a(Ljava/lang/String;)V
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/b()V
return
L35:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ServerError
ifeq L36
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
astore 1
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/a()I
lookupswitch
0 : L37
512 : L38
516 : L39
519 : L40
521 : L41
522 : L42
524 : L43
768 : L34
770 : L44
771 : L45
774 : L46
775 : L47
776 : L48
777 : L49
778 : L50
781 : L51
1027 : L52
1028 : L53
1281 : L34
1541 : L54
2051 : L34
2054 : L34
2561 : L55
2563 : L44
2570 : L56
3329 : L57
3840 : L58
default : L59
L59:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/d()V
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/c()Ljava/lang/String;
ifnull L60
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/c()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc "messages.error.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
L61:
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/m Z
L44:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/a()I
ifeq L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/G Lcom/teamspeak/ts3client/jni/h;
aconst_null
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
return
L37:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "SubscribeAll"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L62
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
iconst_1
invokevirtual com/teamspeak/ts3client/data/ab/a(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
iconst_1
invokevirtual com/teamspeak/ts3client/t/h(Z)V
L62:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "UnsubscribeAll"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L63
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
iconst_0
invokevirtual com/teamspeak/ts3client/data/ab/a(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
iconst_0
invokevirtual com/teamspeak/ts3client/t/h(Z)V
L63:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "Subscribe Channel:"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L64
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/g()Z
ifne L64
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc ":"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
iconst_1
aaload
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/Long;)V
L64:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "InputMute"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L65
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
iconst_1
invokevirtual com/teamspeak/ts3client/t/f(Z)V
L65:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "InputUnMute"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L44
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
iconst_0
invokevirtual com/teamspeak/ts3client/t/f(Z)V
goto L44
L38:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "Composing"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L34
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "Send Message to Client"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L34
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "Chat closed"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L34
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "connectioninfo_"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.clienterror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L39:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.clienterror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L41:
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/p Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/a()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.disconnected.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L42:
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/p Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/a()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ldc "messages.clientoutdated.text2"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "messages.clientoutdated.text1"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L43:
ldc "Action currently not possible due to spam protection. Please wait a few seconds and try again."
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
goto L44
L40:
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/p Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/a()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ldc "messages.error.securitylevel"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/c()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
ldc "messages.disconnected.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L51:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
invokevirtual java/lang/String/isEmpty()Z
ifne L66
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "invalid channel password"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L66
L0:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 3
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/o()Ljava/util/HashMap;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.pwerror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
L1:
return
L2:
astore 8
L66:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "skip"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L67
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
iconst_5
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 3
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/o()Ljava/util/HashMap;
aload 1
invokevirtual com/teamspeak/ts3client/data/a/b()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L68
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/data/a/b()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/o()Ljava/util/HashMap;
aload 1
invokevirtual com/teamspeak/ts3client/data/a/b()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
new java/lang/StringBuilder
dup
ldc "join "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual com/teamspeak/ts3client/data/a/b()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestClientMove(JIJLjava/lang/String;Ljava/lang/String;)I
pop
return
L68:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/t/a(Lcom/teamspeak/ts3client/data/a;)V
return
L67:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "join"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L69
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
iconst_5
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 3
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/o()Ljava/util/HashMap;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/t/a(Lcom/teamspeak/ts3client/data/a;)V
return
L69:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "checkPW"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L34
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/d()Ljava/lang/String;
ldc "Request Image"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.pwerror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L45:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.channelerror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L46:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.channelerror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L47:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.channelerror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L48:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.channelerror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L49:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.channelerror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L50:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.channelerror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L52:
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/p Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/a()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.disconnected.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L53:
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/p Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/a()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.disconnected.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L54:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.channelerror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L56:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.permerror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L55:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.error.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L57:
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/p Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/a()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/c()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc "messages.ban.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L58:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ldc "privilegekey.error.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "privilegekey.error"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L44
L60:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ServerError/b()Ljava/lang/String;
ldc "messages.error.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
goto L61
L36:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ConnectStatusChange
ifeq L70
aload 1
checkcast com/teamspeak/ts3client/jni/events/ConnectStatusChange
astore 1
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ConnectStatusChange/b()I
tableswitch 0
L71
L72
L73
L74
L75
default : L76
L76:
getstatic com/teamspeak/ts3client/jni/e/a Lcom/teamspeak/ts3client/jni/e;
invokevirtual com/teamspeak/ts3client/jni/e/a()I
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ConnectStatusChange/b()I
if_icmpne L77
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/m Z
ifne L77
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/g Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ifnull L77
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ConnectStatusChange/a()I
sipush 1793
if_icmpeq L78
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/i()I
ifle L79
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ConnectStatusChange/a()I
sipush 1797
if_icmpne L79
L78:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/c Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
ldc ""
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/d()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
pop
aload 1
invokevirtual com/teamspeak/ts3client/t/E()V
return
L71:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/e()Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Connect status: STATUS_DISCONNECTED"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L76
L72:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/e()Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Connect status: STATUS_CONNECTING"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L76
L73:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/e()Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Connect status: STATUS_CONNECTED"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L76
L74:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/e()Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Connect status: STATUS_CONNECTION_ESTABLISHING"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L76
L75:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/e()Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Connect status: STATUS_CONNECTION_ESTABLISHED"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L76
L79:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ConnectStatusChange/a()I
ifle L80
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/d()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "messages.error.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ConnectStatusChange/a()I
invokestatic com/teamspeak/ts3client/data/aa/a(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc "messages.conerror.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
getstatic com/teamspeak/ts3client/jni/h/c Lcom/teamspeak/ts3client/jni/h;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/teamspeak/ts3client/jni/h;)V
return
L80:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/p Z
ifne L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ldc "messages.disconnected.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "messages.disconnected.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
getstatic com/teamspeak/ts3client/jni/h/b Lcom/teamspeak/ts3client/jni/h;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/teamspeak/ts3client/jni/h;)V
return
L77:
getstatic com/teamspeak/ts3client/jni/e/b Lcom/teamspeak/ts3client/jni/e;
invokevirtual com/teamspeak/ts3client/jni/e/a()I
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ConnectStatusChange/b()I
if_icmpne L81
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ifnull L82
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ldc "connectiondialog.step0"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/Boolean;)V
L82:
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/g Z
L81:
getstatic com/teamspeak/ts3client/jni/e/c Lcom/teamspeak/ts3client/jni/e;
invokevirtual com/teamspeak/ts3client/jni/e/a()I
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ConnectStatusChange/b()I
if_icmpne L83
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ifnull L84
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ldc "connectiondialog.step1"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/Boolean;)V
L84:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
getstatic com/teamspeak/ts3client/jni/i/a Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/e/b(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
getstatic com/teamspeak/ts3client/jni/i/b Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/e/a(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
getstatic com/teamspeak/ts3client/jni/i/ap Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JLcom/teamspeak/ts3client/jni/i;)J
invokevirtual com/teamspeak/ts3client/data/ab/a(J)V
ldc "chat.connected"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
new com/teamspeak/ts3client/data/c/d
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/c/d/<init>(Landroid/content/Context;)V
invokevirtual com/teamspeak/ts3client/data/e/a(Lcom/teamspeak/ts3client/data/c/d;)V
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/g Z
L83:
getstatic com/teamspeak/ts3client/jni/e/d Lcom/teamspeak/ts3client/jni/e;
invokevirtual com/teamspeak/ts3client/jni/e/a()I
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ConnectStatusChange/b()I
if_icmpne L85
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ifnull L86
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ldc "connectiondialog.step2"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/Boolean;)V
L86:
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/g Z
L85:
getstatic com/teamspeak/ts3client/jni/e/e Lcom/teamspeak/ts3client/jni/e;
invokevirtual com/teamspeak/ts3client/jni/e/a()I
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ConnectStatusChange/b()I
if_icmpne L34
aload 0
invokespecial com/teamspeak/ts3client/ConnectionBackground/g()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ifnull L87
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ldc "connectiondialog.step3"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/Boolean;)V
L87:
aload 0
invokespecial com/teamspeak/ts3client/ConnectionBackground/n()V
aload 0
invokespecial com/teamspeak/ts3client/ConnectionBackground/j()V
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/g Z
invokestatic com/teamspeak/ts3client/data/b/c/a()Lcom/teamspeak/ts3client/data/b/c;
invokevirtual com/teamspeak/ts3client/data/b/c/c()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/a Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
ldc ""
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/b()Z
ifeq L88
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/d()Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/m
dup
aload 0
invokespecial com/teamspeak/ts3client/m/<init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
L88:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/j()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/d()Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/n
dup
aload 0
invokespecial com/teamspeak/ts3client/n/<init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
new com/teamspeak/ts3client/data/d/v
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/d/v/<init>(Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual com/teamspeak/ts3client/data/e/a(Lcom/teamspeak/ts3client/data/d/v;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ifnull L89
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/d()V
L89:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
getstatic com/teamspeak/ts3client/jni/i/az Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/i;)I
iconst_1
if_icmpne L90
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
iconst_1
invokevirtual com/teamspeak/ts3client/t/a(Z)V
L90:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getBaseContext()Landroid/content/Context;
invokestatic com/teamspeak/ts3client/chat/d/a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;
invokevirtual com/teamspeak/ts3client/chat/d/c()Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/chat/a/a(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getBaseContext()Landroid/content/Context;
invokestatic com/teamspeak/ts3client/chat/d/a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;
invokevirtual com/teamspeak/ts3client/chat/d/b()Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/chat/a/a(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/g()Z
ifeq L91
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
ldc "SubscribeAll"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelSubscribeAll(JLjava/lang/String;)I
pop
L92:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/D()V
return
L91:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/e()[J
ifnull L92
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/e()[J
ldc "Subscribe last set"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelSubscribe(J[JLjava/lang/String;)I
pop
goto L92
L70:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer
ifeq L93
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer
astore 9
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L94
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/e()J
lconst_0
lcmp
ifle L95
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/e()J
lstore 5
ldc ""
astore 8
lload 5
ldc2_w 86400L
ldiv
lstore 3
lload 5
ldc2_w 86400L
lload 3
lmul
lsub
lstore 5
lload 3
lconst_0
lcmp
ifle L96
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 3
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "d"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
L96:
lload 5
ldc2_w 3600L
ldiv
lstore 3
lload 5
ldc2_w 3600L
lload 3
lmul
lsub
lstore 5
aload 8
astore 1
lload 3
lconst_0
lcmp
ifle L97
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 3
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "h"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L97:
lload 5
ldc2_w 60L
ldiv
lstore 3
lload 5
ldc2_w 60L
lload 3
lmul
lsub
lstore 5
aload 1
astore 8
lload 3
lconst_0
lcmp
ifle L98
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 3
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "m"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
L98:
lload 5
lconst_0
lcmp
ifle L99
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 5
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "s"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L100:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ldc "messages.ban.text"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/b()Ljava/lang/String;
aastore
dup
iconst_1
aload 1
aastore
dup
iconst_2
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/c()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
ldc "messages.ban.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
getstatic com/teamspeak/ts3client/jni/h/aC Lcom/teamspeak/ts3client/jni/h;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/teamspeak/ts3client/jni/h;)V
L101:
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/m Z
return
L95:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ldc "messages.ban.text.permanent"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/c()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
ldc "messages.ban.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
getstatic com/teamspeak/ts3client/jni/h/aC Lcom/teamspeak/ts3client/jni/h;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/teamspeak/ts3client/jni/h;)V
goto L101
L94:
ldc "event.client.ban"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/a()I
invokevirtual com/teamspeak/ts3client/data/d/b(I)Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_1
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/b()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/d()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L102
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/an Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/a()I
invokevirtual com/teamspeak/ts3client/data/d/b(I)Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L102:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/a()I
invokevirtual com/teamspeak/ts3client/data/a/e(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/a()I
invokevirtual com/teamspeak/ts3client/data/d/c(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/d()Ljava/util/HashMap;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientBanFromServer/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/b()V
return
L93:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ChannelSubscribeFinished
ifeq L103
aload 1
checkcast com/teamspeak/ts3client/jni/events/ChannelSubscribeFinished
invokevirtual com/teamspeak/ts3client/jni/events/ChannelSubscribeFinished/a()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
lcmp
ifne L104
aload 0
invokespecial com/teamspeak/ts3client/ConnectionBackground/n()V
L104:
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/o Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/h Z
ifeq L105
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/h Z
L105:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/y()V
return
L103:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ChannelSubscribe
ifeq L106
aload 1
checkcast com/teamspeak/ts3client/jni/events/ChannelSubscribe
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelSubscribe/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iconst_1
invokevirtual com/teamspeak/ts3client/data/a/c(Z)V
return
L106:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ChannelUnsubscribe
ifeq L107
aload 1
checkcast com/teamspeak/ts3client/jni/events/ChannelUnsubscribe
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelUnsubscribe/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iconst_0
invokevirtual com/teamspeak/ts3client/data/a/c(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelUnsubscribe/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/j()Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 8
L108:
aload 8
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L109
aload 8
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
iload 2
invokevirtual com/teamspeak/ts3client/data/d/c(I)V
goto L108
L109:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelUnsubscribe/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/a()V
return
L107:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ChannelUnsubscribeFinished
ifeq L110
aload 1
checkcast com/teamspeak/ts3client/jni/events/ChannelUnsubscribeFinished
invokevirtual com/teamspeak/ts3client/jni/events/ChannelUnsubscribeFinished/a()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
lcmp
ifne L111
aload 0
invokespecial com/teamspeak/ts3client/ConnectionBackground/n()V
L111:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/h Z
ifeq L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/e()[J
ifnonnull L34
aload 0
iconst_0
putfield com/teamspeak/ts3client/ConnectionBackground/h Z
return
L110:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ChannelMove
ifeq L112
aload 1
checkcast com/teamspeak/ts3client/jni/events/ChannelMove
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/b(Ljava/lang/Long;)Ljava/lang/Long;
astore 8
aload 8
ifnull L113
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 8
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/f()J
invokevirtual com/teamspeak/ts3client/data/a/a(J)V
L113:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/d()J
invokevirtual com/teamspeak/ts3client/data/a/b(J)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
invokevirtual com/teamspeak/ts3client/data/b/a()Ljava/util/concurrent/ConcurrentHashMap;
invokevirtual java/util/concurrent/ConcurrentHashMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 8
L114:
aload 8
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L115
aload 8
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/teamspeak/ts3client/data/a
astore 9
aload 9
invokevirtual com/teamspeak/ts3client/data/a/g()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/d()J
lcmp
ifne L114
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/data/a/b()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 9
invokevirtual com/teamspeak/ts3client/data/a/b()J
getstatic com/teamspeak/ts3client/jni/c/i Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JJLcom/teamspeak/ts3client/jni/c;)J
invokevirtual com/teamspeak/ts3client/data/a/a(J)V
goto L114
L115:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/d()V
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/b()V
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/a()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L116
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/b()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L116
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/B Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
ldc ""
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L117:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
astore 8
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/b()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L118
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/g()J
lconst_0
lcmp
ifne L119
ldc "event.channel.moved.self"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 8
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
return
L116:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/a()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L117
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/b()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpeq L117
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/C Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
ldc ""
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L117
L119:
ldc "event.channel.moved.self"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 8
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 8
invokevirtual com/teamspeak/ts3client/data/a/g()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
return
L118:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/g()J
lconst_0
lcmp
ifne L120
ldc "event.channel.moved"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 8
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_1
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/c()Ljava/lang/String;
aastore
dup
iconst_2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
return
L120:
ldc "event.channel.moved"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 8
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_1
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelMove/c()Ljava/lang/String;
aastore
dup
iconst_2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 8
invokevirtual com/teamspeak/ts3client/data/a/g()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
return
L112:
aload 1
instanceof com/teamspeak/ts3client/jni/events/UpdateChannelEdited
ifeq L121
aload 1
checkcast com/teamspeak/ts3client/jni/events/UpdateChannelEdited
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
getstatic com/teamspeak/ts3client/jni/c/a Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JJLcom/teamspeak/ts3client/jni/c;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/a/a(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
getstatic com/teamspeak/ts3client/jni/c/C Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
invokevirtual com/teamspeak/ts3client/data/a/b(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
getstatic com/teamspeak/ts3client/jni/c/e Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
invokevirtual com/teamspeak/ts3client/data/a/c(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
getstatic com/teamspeak/ts3client/jni/c/l Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
iconst_1
if_icmpne L122
iconst_1
istore 7
L123:
iload 7
ifeq L124
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
invokevirtual com/teamspeak/ts3client/data/b/b()Lcom/teamspeak/ts3client/data/a;
iconst_0
invokevirtual com/teamspeak/ts3client/data/a/a(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/b/b(Lcom/teamspeak/ts3client/data/a;)V
L124:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iload 7
invokevirtual com/teamspeak/ts3client/data/a/a(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
getstatic com/teamspeak/ts3client/jni/c/g Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
invokevirtual com/teamspeak/ts3client/data/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
getstatic com/teamspeak/ts3client/jni/c/F Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JJLcom/teamspeak/ts3client/jni/c;)J
invokevirtual com/teamspeak/ts3client/data/a/c(J)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
astore 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
getstatic com/teamspeak/ts3client/jni/c/G Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
iconst_1
if_icmpne L125
iconst_1
istore 7
L126:
aload 8
iload 7
invokevirtual com/teamspeak/ts3client/data/a/b(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
getstatic com/teamspeak/ts3client/jni/c/m Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
iconst_1
if_icmpne L127
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iconst_1
invokevirtual com/teamspeak/ts3client/data/a/d(Z)V
L128:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/c()I
istore 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
getstatic com/teamspeak/ts3client/jni/c/i Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JJLcom/teamspeak/ts3client/jni/c;)J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 8
aload 8
invokevirtual java/lang/Long/longValue()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/f()J
lcmp
ifeq L129
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
iload 2
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;I)Ljava/lang/Long;
astore 9
aload 9
ifnull L130
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/f()J
invokevirtual com/teamspeak/ts3client/data/a/a(J)V
L130:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 8
iload 2
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;I)Ljava/lang/Long;
astore 9
aload 9
ifnull L131
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokevirtual com/teamspeak/ts3client/data/a/a(J)V
L131:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 8
invokevirtual java/lang/Long/longValue()J
invokevirtual com/teamspeak/ts3client/data/a/a(J)V
L129:
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/b()V
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L132
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getBaseContext()Landroid/content/Context;
invokestatic com/teamspeak/ts3client/chat/d/a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;
invokevirtual com/teamspeak/ts3client/chat/d/b()Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/chat/a/a(Ljava/lang/String;)V
L132:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L133
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/b()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L133
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/w Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/c()Ljava/lang/String;
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L134:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/b()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L135
ldc "event.channel.edit.self"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
return
L122:
iconst_0
istore 7
goto L123
L125:
iconst_0
istore 7
goto L126
L127:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iconst_0
invokevirtual com/teamspeak/ts3client/data/a/d(Z)V
goto L128
L133:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L136
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/b()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpeq L136
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/x Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/c()Ljava/lang/String;
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L134
L136:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifeq L137
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/b()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L137
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/y Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/c()Ljava/lang/String;
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L134
L137:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L134
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/b()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpeq L134
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/z Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/c()Ljava/lang/String;
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L134
L135:
ldc "event.channel.edit"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_1
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateChannelEdited/c()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
return
L121:
aload 1
instanceof com/teamspeak/ts3client/jni/events/DelChannel
ifeq L138
aload 1
checkcast com/teamspeak/ts3client/jni/events/DelChannel
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/DelChannel/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/b(Ljava/lang/Long;)Ljava/lang/Long;
astore 8
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/DelChannel/b()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L139
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/t Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/DelChannel/c()Ljava/lang/String;
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/DelChannel/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
ldc "event.channel.deleted.self"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/DelChannel/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
L140:
aload 8
ifnull L141
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 8
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/DelChannel/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/f()J
invokevirtual com/teamspeak/ts3client/data/a/a(J)V
L141:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/DelChannel/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/c(Ljava/lang/Long;)V
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/b()V
return
L139:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/DelChannel/b()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpeq L140
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/u Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/DelChannel/c()Ljava/lang/String;
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/DelChannel/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
ldc "event.channel.deleted"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/DelChannel/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_1
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/DelChannel/c()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
goto L140
L138:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ClientMoveMoved
ifeq L142
aload 1
checkcast com/teamspeak/ts3client/jni/events/ClientMoveMoved
astore 9
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/d()J
lconst_0
lcmp
ifeq L143
getstatic com/teamspeak/ts3client/jni/j/a Lcom/teamspeak/ts3client/jni/j;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/e()Lcom/teamspeak/ts3client/jni/j;
if_acmpne L144
new com/teamspeak/ts3client/data/c
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/a()I
getstatic com/teamspeak/ts3client/jni/d/b Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/c/<init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/data/d/a(Lcom/teamspeak/ts3client/data/c;)V
L145:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/q Ljava/util/ArrayList;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L146
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
iconst_1
invokevirtual com/teamspeak/ts3client/data/c/b(I)V
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
iconst_0
invokevirtual com/teamspeak/ts3client/data/c/a(I)V
L146:
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/d()J
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(IJ)Z
ifeq L147
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/a/e(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/d()Ljava/util/HashMap;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
L147:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
lconst_0
lcmp
ifeq L148
getstatic com/teamspeak/ts3client/jni/j/a Lcom/teamspeak/ts3client/jni/j;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/e()Lcom/teamspeak/ts3client/jni/j;
if_acmpeq L149
getstatic com/teamspeak/ts3client/jni/j/b Lcom/teamspeak/ts3client/jni/j;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/e()Lcom/teamspeak/ts3client/jni/j;
if_acmpne L150
L149:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/a/d(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/d()Ljava/util/HashMap;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L151:
iconst_0
istore 2
aload 1
invokevirtual com/teamspeak/ts3client/data/c/e()Lcom/teamspeak/ts3client/c/a;
ifnull L152
aload 1
invokevirtual com/teamspeak/ts3client/data/c/e()Lcom/teamspeak/ts3client/c/a;
invokevirtual com/teamspeak/ts3client/c/a/a()I
istore 2
L152:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifeq L153
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L153
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/az Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L154:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L155
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lstore 3
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/ab/b(Ljava/lang/Long;)Z
ifne L156
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/g()Z
ifne L156
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
iconst_2
newarray long
dup
iconst_0
lload 3
lastore
dup
iconst_1
lconst_0
lastore
ldc "Unsubscribe old Channel"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelUnsubscribe(J[JLjava/lang/String;)I
pop
L156:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokevirtual com/teamspeak/ts3client/data/e/a(J)V
ldc "event.client.movemoved.self"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_1
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/b()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
ldc "event.client.movemoved.self.info"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/a(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getBaseContext()Landroid/content/Context;
invokestatic com/teamspeak/ts3client/chat/d/a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;
invokevirtual com/teamspeak/ts3client/chat/d/b()Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/chat/a/a(Ljava/lang/String;)V
aload 0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(Lcom/teamspeak/ts3client/data/a;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/a()V
aload 0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(Lcom/teamspeak/ts3client/data/a;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iconst_1
invokevirtual com/teamspeak/ts3client/data/a/c(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/h()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokevirtual com/teamspeak/ts3client/data/ab/b(J)V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/o()Ljava/util/HashMap;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L157
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/o()Ljava/util/HashMap;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual com/teamspeak/ts3client/data/ab/d(Ljava/lang/String;)V
L158:
aload 0
lload 3
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(J)V
aload 0
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(J)V
L159:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/y()V
return
L144:
aload 0
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/a()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 8
aload 8
astore 1
aload 8
ifnonnull L145
return
L150:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/d/c(I)V
goto L151
L148:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/d/c(I)V
goto L151
L143:
new com/teamspeak/ts3client/data/c
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/a()I
getstatic com/teamspeak/ts3client/jni/d/b Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/c/<init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V
astore 8
aload 8
astore 1
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
lconst_0
lcmp
ifeq L151
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 8
invokevirtual com/teamspeak/ts3client/data/d/a(Lcom/teamspeak/ts3client/data/c;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 8
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/a/d(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/d()Ljava/util/HashMap;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 8
astore 1
goto L151
L153:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L160
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/e()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/a Lcom/teamspeak/ts3client/jni/j;
if_acmpne L160
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/U Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L154
L160:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L161
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/e()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/b Lcom/teamspeak/ts3client/jni/j;
if_acmpne L161
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/V Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L154
L161:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/d()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L162
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/e()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/c Lcom/teamspeak/ts3client/jni/j;
if_acmpne L162
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/W Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L154
L162:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/d()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L154
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/e()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/b Lcom/teamspeak/ts3client/jni/j;
if_acmpne L154
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/X Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L154
L157:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
ldc ""
invokevirtual com/teamspeak/ts3client/data/ab/d(Ljava/lang/String;)V
goto L158
L155:
ldc "event.client.movemoved"
iconst_4
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_3
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveMoved/b()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
goto L159
L142:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ClientMove
ifeq L163
aload 1
checkcast com/teamspeak/ts3client/jni/events/ClientMove
astore 9
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/d()J
lconst_0
lcmp
ifeq L164
getstatic com/teamspeak/ts3client/jni/j/a Lcom/teamspeak/ts3client/jni/j;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/f()Lcom/teamspeak/ts3client/jni/j;
if_acmpne L6
new com/teamspeak/ts3client/data/c
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
getstatic com/teamspeak/ts3client/jni/d/b Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/c/<init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/data/d/a(Lcom/teamspeak/ts3client/data/c;)V
L165:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/q Ljava/util/ArrayList;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L166
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
iconst_1
invokevirtual com/teamspeak/ts3client/data/c/b(I)V
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
iconst_0
invokevirtual com/teamspeak/ts3client/data/c/a(I)V
L166:
aload 0
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/d()J
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(IJ)Z
ifeq L167
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/a/e(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/d()Ljava/util/HashMap;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
L167:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
lconst_0
lcmp
ifeq L168
getstatic com/teamspeak/ts3client/jni/j/a Lcom/teamspeak/ts3client/jni/j;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/f()Lcom/teamspeak/ts3client/jni/j;
if_acmpeq L169
getstatic com/teamspeak/ts3client/jni/j/b Lcom/teamspeak/ts3client/jni/j;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/f()Lcom/teamspeak/ts3client/jni/j;
if_acmpne L170
L169:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/a/d(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/d()Ljava/util/HashMap;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L171:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L172
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
lconst_0
lcmp
ifeq L172
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lstore 3
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/ab/b(Ljava/lang/Long;)Z
ifne L173
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/g()Z
ifne L173
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
iconst_2
newarray long
dup
iconst_0
lload 3
lastore
dup
iconst_1
lconst_0
lastore
ldc "Unsubscribe old Channel"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelUnsubscribe(J[JLjava/lang/String;)I
pop
L173:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokevirtual com/teamspeak/ts3client/data/e/a(J)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getBaseContext()Landroid/content/Context;
invokestatic com/teamspeak/ts3client/chat/d/a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;
invokevirtual com/teamspeak/ts3client/chat/d/b()Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/chat/a/a(Ljava/lang/String;)V
ldc "event.client.movemoved.self.info"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/a(Ljava/lang/String;)V
aload 0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(Lcom/teamspeak/ts3client/data/a;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/a()V
aload 0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(Lcom/teamspeak/ts3client/data/a;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iconst_1
invokevirtual com/teamspeak/ts3client/data/a/c(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/h()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokevirtual com/teamspeak/ts3client/data/ab/b(J)V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/o()Ljava/util/HashMap;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L174
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/o()Ljava/util/HashMap;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual com/teamspeak/ts3client/data/ab/d(Ljava/lang/String;)V
L175:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/i()Z
ifeq L176
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iconst_1
invokevirtual com/teamspeak/ts3client/data/a/e(Z)V
L176:
aload 0
lload 3
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(J)V
aload 0
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(J)V
L172:
iconst_0
istore 2
aload 1
invokevirtual com/teamspeak/ts3client/data/c/e()Lcom/teamspeak/ts3client/c/a;
ifnull L177
aload 1
invokevirtual com/teamspeak/ts3client/data/c/e()Lcom/teamspeak/ts3client/c/a;
invokevirtual com/teamspeak/ts3client/c/a/a()I
istore 2
L177:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L178
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
lconst_0
lcmp
ifeq L178
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/ay Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
ldc "event.client.move.self"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
L178:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpeq L4
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/e()J
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
getstatic com/teamspeak/ts3client/jni/d/O Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
ifne L4
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L179
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/f()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/b Lcom/teamspeak/ts3client/jni/j;
if_acmpne L180
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/O Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L180:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/f()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/a Lcom/teamspeak/ts3client/jni/j;
if_acmpne L4
L3:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/d()J
lconst_0
lcmp
ifne L9
ldc "event.client.move.a"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/I Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L4:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/y()V
return
L6:
aload 0
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 8
L7:
aload 8
astore 1
aload 8
ifnonnull L165
return
L8:
astore 1
new com/teamspeak/ts3client/data/c
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
getstatic com/teamspeak/ts3client/jni/d/b Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/c/<init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/data/d/a(Lcom/teamspeak/ts3client/data/c;)V
goto L165
L170:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/d/c(I)V
goto L171
L168:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/d/c(I)V
goto L171
L164:
new com/teamspeak/ts3client/data/c
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
getstatic com/teamspeak/ts3client/jni/d/b Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/c/<init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V
astore 1
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
lconst_0
lcmp
ifeq L181
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/data/d/a(Lcom/teamspeak/ts3client/data/c;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/a/d(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/d()Ljava/util/HashMap;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L171
L181:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/d/c(I)V
goto L171
L174:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
ldc ""
invokevirtual com/teamspeak/ts3client/data/ab/d(Ljava/lang/String;)V
goto L175
L9:
ldc "event.client.move.b"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/N Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L10:
goto L4
L5:
astore 1
goto L4
L179:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/d()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L17
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/f()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/b Lcom/teamspeak/ts3client/jni/j;
if_acmpne L182
L11:
ldc "event.client.move.c"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
L12:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/Q Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L182:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/f()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/c Lcom/teamspeak/ts3client/jni/j;
if_acmpne L4
L14:
ldc "event.client.move.d"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
L15:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
lconst_0
lcmp
ifne L183
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/K Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
ldc "event.client.disconnected"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_1
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/b()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
goto L4
L183:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/P Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L4
L17:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/f()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/b Lcom/teamspeak/ts3client/jni/j;
if_acmpne L18
ldc "event.client.move.e"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
L18:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/f()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/c Lcom/teamspeak/ts3client/jni/j;
if_acmpne L4
ldc "event.client.move.f"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientMove/c()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
L20:
goto L4
L19:
astore 1
goto L4
L163:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ClientMoveTimeout
ifeq L184
aload 1
checkcast com/teamspeak/ts3client/jni/events/ClientMoveTimeout
astore 1
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveTimeout/a()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 8
aload 8
ifnull L34
L21:
aload 8
invokevirtual com/teamspeak/ts3client/data/c/c()I
iconst_1
if_icmpeq L22
ldc "event.client.dropped"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 8
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
L22:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveTimeout/b()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 8
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/a/e(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/d()Ljava/util/HashMap;
aload 8
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 8
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/d/c(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/y()V
L24:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveTimeout/b()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L34
iconst_0
istore 2
aload 8
invokevirtual com/teamspeak/ts3client/data/c/e()Lcom/teamspeak/ts3client/c/a;
ifnull L185
aload 8
invokevirtual com/teamspeak/ts3client/data/c/e()Lcom/teamspeak/ts3client/c/a;
invokevirtual com/teamspeak/ts3client/c/a/a()I
istore 2
L185:
aload 8
invokevirtual com/teamspeak/ts3client/data/c/c()I
iconst_1
if_icmpeq L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/M Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 8
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientMoveTimeout/b()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
return
L184:
aload 1
instanceof com/teamspeak/ts3client/jni/events/TalkStatusChange
ifeq L186
aload 1
checkcast com/teamspeak/ts3client/jni/events/TalkStatusChange
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/g Z
ifeq L34
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TalkStatusChange/b()I
ifne L187
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TalkStatusChange/a()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 8
aload 8
ifnull L188
aload 8
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TalkStatusChange/c()I
invokevirtual com/teamspeak/ts3client/data/c/a(I)V
L188:
getstatic com/teamspeak/ts3client/ConnectionBackground/k Z
ifeq L189
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TalkStatusChange/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L189
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TalkStatusChange/c()I
iconst_1
if_icmpne L190
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
ldc_w 2130837662
invokevirtual android/support/v4/app/dm/a(I)Landroid/support/v4/app/dm;
pop
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
ldc_w 2130837662
ldc_w 48.0F
ldc_w 48.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/support/v4/app/dm/a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/dm;
pop
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
astore 1
aload 1
ldc_w -16776961
putfield android/app/Notification/ledARGB I
aload 1
aload 1
getfield android/app/Notification/flags I
iconst_3
ior
putfield android/app/Notification/flags I
aload 1
iconst_0
putfield android/app/Notification/ledOffMS I
aload 1
iconst_1
putfield android/app/Notification/ledOnMS I
aload 1
putstatic com/teamspeak/ts3client/ConnectionBackground/j Landroid/app/Notification;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/j()Landroid/app/NotificationManager;
iconst_1
getstatic com/teamspeak/ts3client/ConnectionBackground/j Landroid/app/Notification;
invokevirtual android/app/NotificationManager/notify(ILandroid/app/Notification;)V
L189:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ifnull L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/y()V
return
L190:
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
ldc_w 2130837622
invokevirtual android/support/v4/app/dm/a(I)Landroid/support/v4/app/dm;
pop
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837622
invokestatic android/graphics/BitmapFactory/decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
invokevirtual android/support/v4/app/dm/a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/dm;
pop
getstatic com/teamspeak/ts3client/ConnectionBackground/l Landroid/support/v4/app/dm;
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
putstatic com/teamspeak/ts3client/ConnectionBackground/j Landroid/app/Notification;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/j()Landroid/app/NotificationManager;
iconst_1
getstatic com/teamspeak/ts3client/ConnectionBackground/j Landroid/app/Notification;
invokevirtual android/app/NotificationManager/notify(ILandroid/app/Notification;)V
goto L189
L187:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TalkStatusChange/c()I
iconst_1
if_icmpne L191
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/q Ljava/util/ArrayList;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TalkStatusChange/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/j Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TalkStatusChange/a()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_0
ldc ""
ldc ""
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L192:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TalkStatusChange/a()I
invokevirtual com/teamspeak/ts3client/data/d/a(I)Z
ifeq L34
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TalkStatusChange/a()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 8
aload 8
ifnull L189
aload 8
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TalkStatusChange/c()I
invokevirtual com/teamspeak/ts3client/data/c/b(I)V
goto L189
L191:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/q Ljava/util/ArrayList;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TalkStatusChange/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
goto L192
L186:
aload 1
instanceof com/teamspeak/ts3client/jni/events/NewChannelCreated
ifeq L193
aload 1
checkcast com/teamspeak/ts3client/jni/events/NewChannelCreated
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/a()J
getstatic com/teamspeak/ts3client/jni/c/a Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JJLcom/teamspeak/ts3client/jni/c;)Ljava/lang/String;
astore 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/a()J
getstatic com/teamspeak/ts3client/jni/c/i Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JJLcom/teamspeak/ts3client/jni/c;)J
lstore 3
new com/teamspeak/ts3client/data/a
dup
aload 8
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/a()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/b()J
lload 3
invokespecial com/teamspeak/ts3client/data/a/<init>(Ljava/lang/String;JJJ)V
astore 8
aload 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/a()J
getstatic com/teamspeak/ts3client/jni/c/C Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
invokevirtual com/teamspeak/ts3client/data/a/b(I)V
aload 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/a()J
getstatic com/teamspeak/ts3client/jni/c/e Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
invokevirtual com/teamspeak/ts3client/data/a/c(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/a()J
getstatic com/teamspeak/ts3client/jni/c/l Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
iconst_1
if_icmpne L194
iconst_1
istore 7
L195:
iload 7
ifeq L196
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
invokevirtual com/teamspeak/ts3client/data/b/b()Lcom/teamspeak/ts3client/data/a;
iconst_0
invokevirtual com/teamspeak/ts3client/data/a/a(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 8
invokevirtual com/teamspeak/ts3client/data/b/b(Lcom/teamspeak/ts3client/data/a;)V
L196:
aload 8
iload 7
invokevirtual com/teamspeak/ts3client/data/a/a(Z)V
aload 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/a()J
getstatic com/teamspeak/ts3client/jni/c/g Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
invokevirtual com/teamspeak/ts3client/data/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/a()J
getstatic com/teamspeak/ts3client/jni/c/G Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
iconst_1
if_icmpne L197
iconst_1
istore 7
L198:
aload 8
iload 7
invokevirtual com/teamspeak/ts3client/data/a/b(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/a()J
getstatic com/teamspeak/ts3client/jni/c/m Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JJLcom/teamspeak/ts3client/jni/c;)I
iconst_1
if_icmpne L199
aload 8
iconst_1
invokevirtual com/teamspeak/ts3client/data/a/d(Z)V
L199:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 8
invokevirtual com/teamspeak/ts3client/data/b/a(Lcom/teamspeak/ts3client/data/a;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/g()Z
ifeq L200
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/a()J
lstore 3
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
astore 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
lstore 5
new java/lang/StringBuilder
dup
ldc "Subscribe "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/a()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 9
aload 8
lload 5
iconst_1
newarray long
dup
iconst_0
lload 3
lastore
aload 9
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelSubscribe(J[JLjava/lang/String;)I
pop
L200:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
invokevirtual com/teamspeak/ts3client/data/b/a()Ljava/util/concurrent/ConcurrentHashMap;
invokevirtual java/util/concurrent/ConcurrentHashMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 8
L201:
aload 8
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L202
aload 8
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/teamspeak/ts3client/data/a
astore 9
aload 9
invokevirtual com/teamspeak/ts3client/data/a/g()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/b()J
lcmp
ifne L201
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/data/a/b()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 9
invokevirtual com/teamspeak/ts3client/data/a/b()J
getstatic com/teamspeak/ts3client/jni/c/i Lcom/teamspeak/ts3client/jni/c;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JJLcom/teamspeak/ts3client/jni/c;)J
invokevirtual com/teamspeak/ts3client/data/a/a(J)V
goto L201
L194:
iconst_0
istore 7
goto L195
L197:
iconst_0
istore 7
goto L198
L202:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/NewChannelCreated/c()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L203
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/r Lcom/teamspeak/ts3client/jni/h;
aconst_null
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L203:
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/b()V
return
L193:
aload 1
instanceof com/teamspeak/ts3client/jni/events/UpdateClient
ifeq L204
aload 1
checkcast com/teamspeak/ts3client/jni/events/UpdateClient
astore 1
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateClient/a()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
ifnull L205
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/UpdateClient/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L205
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/D()V
L205:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/y()V
return
L204:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ClientKickFromChannel
ifeq L206
aload 1
checkcast com/teamspeak/ts3client/jni/events/ClientKickFromChannel
astore 9
getstatic com/teamspeak/ts3client/jni/j/a Lcom/teamspeak/ts3client/jni/j;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/f()Lcom/teamspeak/ts3client/jni/j;
if_acmpeq L207
getstatic com/teamspeak/ts3client/jni/j/b Lcom/teamspeak/ts3client/jni/j;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/f()Lcom/teamspeak/ts3client/jni/j;
if_acmpne L208
L207:
new com/teamspeak/ts3client/data/c
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/a()I
getstatic com/teamspeak/ts3client/jni/d/b Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/c/<init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/data/d/a(Lcom/teamspeak/ts3client/data/c;)V
L209:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/e()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/a/e(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/d()Ljava/util/HashMap;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/d()J
lconst_0
lcmp
ifeq L210
getstatic com/teamspeak/ts3client/jni/j/a Lcom/teamspeak/ts3client/jni/j;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/f()Lcom/teamspeak/ts3client/jni/j;
if_acmpeq L211
getstatic com/teamspeak/ts3client/jni/j/b Lcom/teamspeak/ts3client/jni/j;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/f()Lcom/teamspeak/ts3client/jni/j;
if_acmpne L212
L211:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/a/d(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/d()Ljava/util/HashMap;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L210:
iconst_0
istore 2
aload 1
invokevirtual com/teamspeak/ts3client/data/c/e()Lcom/teamspeak/ts3client/c/a;
ifnull L213
aload 1
invokevirtual com/teamspeak/ts3client/data/c/e()Lcom/teamspeak/ts3client/c/a;
invokevirtual com/teamspeak/ts3client/c/a/a()I
istore 2
L213:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L214
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lstore 3
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/ab/b(Ljava/lang/Long;)Z
ifne L215
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/g()Z
ifne L215
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
iconst_2
newarray long
dup
iconst_0
lload 3
lastore
dup
iconst_1
lconst_0
lastore
ldc "Unsubscribe old Channel"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelUnsubscribe(J[JLjava/lang/String;)I
pop
L215:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/d()J
invokevirtual com/teamspeak/ts3client/data/e/a(J)V
L25:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getBaseContext()Landroid/content/Context;
invokestatic com/teamspeak/ts3client/chat/d/a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;
invokevirtual com/teamspeak/ts3client/chat/d/b()Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/chat/a/a(Ljava/lang/String;)V
L26:
aload 0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(Lcom/teamspeak/ts3client/data/a;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/a()V
aload 0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(Lcom/teamspeak/ts3client/data/a;)V
aload 0
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/d()J
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(J)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iconst_1
invokevirtual com/teamspeak/ts3client/data/a/c(Z)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aB Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/b()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/e()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/c()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L216
ldc "event.client.kicked.self"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/e()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_1
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/b()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
L217:
aload 0
invokevirtual com/teamspeak/ts3client/ConnectionBackground/b()V
return
L208:
aload 0
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/a()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 8
aload 8
astore 1
aload 8
ifnonnull L209
return
L212:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/d/c(I)V
goto L210
L216:
ldc "event.client.kicked.self.reason"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/e()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_1
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/b()Ljava/lang/String;
aastore
dup
iconst_2
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/c()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
goto L217
L214:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/e()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L218
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/f()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/c Lcom/teamspeak/ts3client/jni/j;
if_acmpne L219
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/af Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L219:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/f()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/b Lcom/teamspeak/ts3client/jni/j;
if_acmpne L220
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/ag Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L220:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/c()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L221
ldc "event.client.kicked"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/e()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_2
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/b()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
goto L217
L218:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/d()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L220
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/f()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/a Lcom/teamspeak/ts3client/jni/j;
if_acmpne L222
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/ad Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L222:
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/f()Lcom/teamspeak/ts3client/jni/j;
getstatic com/teamspeak/ts3client/jni/j/b Lcom/teamspeak/ts3client/jni/j;
if_acmpne L220
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/ae Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L220
L221:
ldc "event.client.kicked.reason"
iconst_4
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/e()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aastore
dup
iconst_2
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/b()Ljava/lang/String;
aastore
dup
iconst_3
aload 9
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromChannel/c()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
goto L217
L206:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ClientKickFromServer
ifeq L223
aload 1
checkcast com/teamspeak/ts3client/jni/events/ClientKickFromServer
astore 1
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/a()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 8
aload 8
ifnull L34
aload 8
invokevirtual com/teamspeak/ts3client/data/c/e()Lcom/teamspeak/ts3client/c/a;
ifnull L224
aload 8
invokevirtual com/teamspeak/ts3client/data/c/e()Lcom/teamspeak/ts3client/c/a;
invokevirtual com/teamspeak/ts3client/c/a/a()I
istore 2
L225:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L226
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/c()Ljava/lang/String;
ldc "messages.kicked.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
getstatic com/teamspeak/ts3client/jni/h/aA Lcom/teamspeak/ts3client/jni/h;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/teamspeak/ts3client/jni/h;)V
aload 0
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/m Z
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aA Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/b()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L227:
ldc "event.client.kick"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/a()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_1
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/b()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 8
invokevirtual com/teamspeak/ts3client/data/c/f()I
invokevirtual com/teamspeak/ts3client/data/a/e(I)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/d()Ljava/util/HashMap;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/y()V
return
L226:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/d()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifne L228
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/al Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 8
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L228:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/d()J
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/f()J
lcmp
ifeq L227
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/ak Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 8
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
iload 2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ClientKickFromServer/d()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
invokevirtual com/teamspeak/ts3client/data/a/e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L227
L223:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ServerGroupList
ifeq L229
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ServerGroupList
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/l()Lcom/teamspeak/ts3client/data/g/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupList/a()Lcom/teamspeak/ts3client/data/g/a;
invokevirtual com/teamspeak/ts3client/data/g/b/a(Lcom/teamspeak/ts3client/data/g/a;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/g()Lcom/teamspeak/ts3client/data/c/d;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupList/b()J
invokevirtual com/teamspeak/ts3client/data/c/d/a(J)Lcom/teamspeak/ts3client/data/c/c;
pop
return
L229:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ChannelGroupList
ifeq L230
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ChannelGroupList
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/b()Lcom/teamspeak/ts3client/data/a/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/a()Lcom/teamspeak/ts3client/data/a/a;
invokevirtual com/teamspeak/ts3client/data/a/b/a(Lcom/teamspeak/ts3client/data/a/a;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/g()Lcom/teamspeak/ts3client/data/c/d;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/b()J
invokevirtual com/teamspeak/ts3client/data/c/d/a(J)Lcom/teamspeak/ts3client/data/c/c;
pop
return
L230:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged
ifeq L231
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged
astore 1
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged/b()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 8
aload 8
ifnull L34
aload 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged/b()I
getstatic com/teamspeak/ts3client/jni/d/H Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/c/a(Ljava/lang/String;)V
ldc "event.group.channel.assigned"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/b()Lcom/teamspeak/ts3client/data/a/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged/a()J
invokevirtual com/teamspeak/ts3client/data/a/b/b(J)Lcom/teamspeak/ts3client/data/a/a;
invokevirtual com/teamspeak/ts3client/data/a/a/a()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged/b()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/d()Ljava/lang/String;
aastore
dup
iconst_2
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged/d()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged/b()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L232
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged/c()I
ifeq L232
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aK Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged/d()Ljava/lang/String;
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/b()Lcom/teamspeak/ts3client/data/a/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged/a()J
invokevirtual com/teamspeak/ts3client/data/a/b/b(J)Lcom/teamspeak/ts3client/data/a/a;
invokevirtual com/teamspeak/ts3client/data/a/a/a()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L232:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/y()V
return
L231:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ClientPoke
ifeq L233
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ClientPoke
astore 1
invokestatic com/teamspeak/ts3client/data/b/c/a()Lcom/teamspeak/ts3client/data/b/c;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientPoke/c()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/b/c/a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;
astore 8
aload 8
ifnull L234
aload 8
invokevirtual com/teamspeak/ts3client/c/a/b()Z
ifne L34
L234:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientPoke/a()Ljava/lang/String;
ldc "messages.poke.info"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientPoke/b()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.close"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/d Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientPoke/b()Ljava/lang/String;
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/f()Landroid/content/SharedPreferences;
ldc "vibrate_poke"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L34
invokestatic com/teamspeak/ts3client/data/ai/a()Lcom/teamspeak/ts3client/data/ai;
pop
getstatic com/teamspeak/ts3client/data/aj/a [J
invokestatic com/teamspeak/ts3client/data/ai/a([J)V
return
L233:
aload 1
instanceof com/teamspeak/ts3client/jni/events/TextMessage
ifeq L235
aload 1
checkcast com/teamspeak/ts3client/jni/events/TextMessage
astore 1
invokestatic com/teamspeak/ts3client/data/b/c/a()Lcom/teamspeak/ts3client/data/b/c;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/c()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/b/c/a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;
astore 8
aload 8
ifnull L236
aload 8
invokevirtual com/teamspeak/ts3client/c/a/c()Z
ifne L34
aload 8
invokevirtual com/teamspeak/ts3client/c/a/d()Z
ifne L34
L236:
iconst_0
istore 7
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L237
iconst_1
istore 7
L237:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/d()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L34
new com/teamspeak/ts3client/chat/y
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/b()Ljava/lang/String;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/c()Ljava/lang/String;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/d()Ljava/lang/String;
iload 7
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial com/teamspeak/ts3client/chat/y/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
astore 8
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/e()I
tableswitch 1
L238
L239
L240
default : L241
L241:
aload 8
invokevirtual com/teamspeak/ts3client/chat/y/a()Z
ifne L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
iconst_1
invokevirtual com/teamspeak/ts3client/t/e(Z)V
return
L238:
iload 7
ifne L241
invokestatic com/teamspeak/ts3client/chat/d/a()Lcom/teamspeak/ts3client/chat/d;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/c()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/chat/d/c(Ljava/lang/String;)Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L242
invokestatic com/teamspeak/ts3client/chat/d/a()Lcom/teamspeak/ts3client/chat/d;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/c()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/chat/d/d(Ljava/lang/String;)Lcom/teamspeak/ts3client/chat/a;
aload 8
invokevirtual com/teamspeak/ts3client/chat/a/a(Lcom/teamspeak/ts3client/chat/y;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aM Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/b()Ljava/lang/String;
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/f()Landroid/content/SharedPreferences;
ldc "vibrate_chat"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L243
invokestatic com/teamspeak/ts3client/data/ai/a()Lcom/teamspeak/ts3client/data/ai;
pop
getstatic com/teamspeak/ts3client/data/aj/c [J
invokestatic com/teamspeak/ts3client/data/ai/a([J)V
L243:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aM Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/b()Ljava/lang/String;
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L241
L242:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/q()Lcom/teamspeak/ts3client/data/d;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/a()I
invokevirtual com/teamspeak/ts3client/data/d/a(I)Z
ifeq L244
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/a()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 9
new com/teamspeak/ts3client/chat/a
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/b()Ljava/lang/String;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/c()Ljava/lang/String;
aload 9
invokespecial com/teamspeak/ts3client/chat/a/<init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;)V
astore 9
aload 9
aload 8
invokevirtual com/teamspeak/ts3client/chat/a/a(Lcom/teamspeak/ts3client/chat/y;)V
invokestatic com/teamspeak/ts3client/chat/d/a()Lcom/teamspeak/ts3client/chat/d;
aload 9
invokevirtual com/teamspeak/ts3client/chat/d/a(Lcom/teamspeak/ts3client/chat/a;)V
L245:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/f()Landroid/content/SharedPreferences;
ldc "vibrate_chatnew"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L246
invokestatic com/teamspeak/ts3client/data/ai/a()Lcom/teamspeak/ts3client/data/ai;
pop
getstatic com/teamspeak/ts3client/data/aj/b [J
invokestatic com/teamspeak/ts3client/data/ai/a([J)V
goto L243
L244:
new com/teamspeak/ts3client/data/c
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/b()Ljava/lang/String;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
iconst_1
invokespecial com/teamspeak/ts3client/data/c/<init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;Z)V
astore 9
aload 9
invokevirtual com/teamspeak/ts3client/data/c/g()V
new com/teamspeak/ts3client/chat/a
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/b()Ljava/lang/String;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/c()Ljava/lang/String;
aload 9
iconst_1
invokespecial com/teamspeak/ts3client/chat/a/<init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;Z)V
astore 9
aload 9
aload 8
invokevirtual com/teamspeak/ts3client/chat/a/a(Lcom/teamspeak/ts3client/chat/y;)V
invokestatic com/teamspeak/ts3client/chat/d/a()Lcom/teamspeak/ts3client/chat/d;
aload 9
invokevirtual com/teamspeak/ts3client/chat/d/a(Lcom/teamspeak/ts3client/chat/a;)V
goto L245
L246:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/f()Landroid/content/SharedPreferences;
ldc "vibrate_chat"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L243
invokestatic com/teamspeak/ts3client/data/ai/a()Lcom/teamspeak/ts3client/data/ai;
pop
getstatic com/teamspeak/ts3client/data/aj/c [J
invokestatic com/teamspeak/ts3client/data/ai/a([J)V
goto L243
L239:
invokestatic com/teamspeak/ts3client/chat/d/a()Lcom/teamspeak/ts3client/chat/d;
invokevirtual com/teamspeak/ts3client/chat/d/b()Lcom/teamspeak/ts3client/chat/a;
aload 8
invokevirtual com/teamspeak/ts3client/chat/a/a(Lcom/teamspeak/ts3client/chat/y;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpeq L241
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aO Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/b()Ljava/lang/String;
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L241
L240:
invokestatic com/teamspeak/ts3client/chat/d/a()Lcom/teamspeak/ts3client/chat/d;
invokevirtual com/teamspeak/ts3client/chat/d/c()Lcom/teamspeak/ts3client/chat/a;
aload 8
invokevirtual com/teamspeak/ts3client/chat/a/a(Lcom/teamspeak/ts3client/chat/y;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpeq L241
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aQ Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/TextMessage/b()Ljava/lang/String;
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L241
L235:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ClientChatClosed
ifeq L247
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ClientChatClosed
astore 1
new com/teamspeak/ts3client/chat/y
dup
ldc ""
ldc ""
ldc "event.chat.closed"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial com/teamspeak/ts3client/chat/y/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
astore 8
invokestatic com/teamspeak/ts3client/chat/d/a()Lcom/teamspeak/ts3client/chat/d;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientChatClosed/a()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/chat/d/c(Ljava/lang/String;)Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L34
invokestatic com/teamspeak/ts3client/chat/d/a()Lcom/teamspeak/ts3client/chat/d;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientChatClosed/a()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/chat/d/d(Ljava/lang/String;)Lcom/teamspeak/ts3client/chat/a;
aload 8
invokevirtual com/teamspeak/ts3client/chat/a/a(Lcom/teamspeak/ts3client/chat/y;)V
return
L247:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions
ifeq L248
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions
astore 1
L28:
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/j()Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/cw Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
if_icmpne L29
aload 0
invokespecial com/teamspeak/ts3client/ConnectionBackground/f()V
L29:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
ifnull L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/D()V
return
L30:
astore 1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/e()Ljava/util/logging/Logger;
aload 1
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/warning(Ljava/lang/String;)V
goto L29
L248:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded
ifeq L249
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded
astore 1
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded/a()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 8
aload 8
ifnull L34
aload 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded/a()I
getstatic com/teamspeak/ts3client/jni/d/I Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/c/c(Ljava/lang/String;)V
ldc "event.group.server.assigned"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded/b()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/l()Lcom/teamspeak/ts3client/data/g/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded/c()J
invokevirtual com/teamspeak/ts3client/data/g/b/b(J)Lcom/teamspeak/ts3client/data/g/a;
invokevirtual com/teamspeak/ts3client/data/g/a/a()Ljava/lang/String;
aastore
dup
iconst_2
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded/d()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aG Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded/b()Ljava/lang/String;
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/l()Lcom/teamspeak/ts3client/data/g/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded/c()J
invokevirtual com/teamspeak/ts3client/data/g/b/b(J)Lcom/teamspeak/ts3client/data/g/a;
invokevirtual com/teamspeak/ts3client/data/g/a/a()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
return
L249:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted
ifeq L250
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted
astore 1
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/a()I
invokespecial com/teamspeak/ts3client/ConnectionBackground/b(I)Lcom/teamspeak/ts3client/data/c;
astore 8
aload 8
ifnull L34
aload 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/k()J
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/a()I
getstatic com/teamspeak/ts3client/jni/d/I Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/c/c(Ljava/lang/String;)V
ldc "event.group.server.deleted"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/b()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/l()Lcom/teamspeak/ts3client/data/g/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/c()J
invokevirtual com/teamspeak/ts3client/data/g/b/b(J)Lcom/teamspeak/ts3client/data/g/a;
invokevirtual com/teamspeak/ts3client/data/g/a/a()Ljava/lang/String;
aastore
dup
iconst_2
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/d()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/a()I
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/i()I
if_icmpne L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aI Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/b()Ljava/lang/String;
iconst_0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/l()Lcom/teamspeak/ts3client/data/g/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/c()J
invokevirtual com/teamspeak/ts3client/data/g/b/b(J)Lcom/teamspeak/ts3client/data/g/a;
invokevirtual com/teamspeak/ts3client/data/g/a/a()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
return
L250:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ChannelPasswordChanged
ifeq L251
aload 1
checkcast com/teamspeak/ts3client/jni/events/ChannelPasswordChanged
astore 1
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/o()Ljava/util/HashMap;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelPasswordChanged/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/ChannelPasswordChanged/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iconst_0
invokevirtual com/teamspeak/ts3client/data/a/e(Z)V
return
L251:
aload 1
instanceof com/teamspeak/ts3client/jni/events/IgnoredWhisper
ifeq L252
aload 1
checkcast com/teamspeak/ts3client/jni/events/IgnoredWhisper
astore 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/f()Landroid/content/SharedPreferences;
ldc "whisper"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
iconst_2
if_icmpeq L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/a()Lcom/a/b/d/bw;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/IgnoredWhisper/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface com/a/b/d/bw/containsKey(Ljava/lang/Object;)Z 1
ifne L34
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/IgnoredWhisper/b()J
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/IgnoredWhisper/a()I
new java/lang/StringBuilder
dup
ldc "IgnoredWhisper: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/IgnoredWhisper/a()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestClientUIDfromClientID(JILjava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/a()Lcom/a/b/d/bw;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/IgnoredWhisper/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface com/a/b/d/bw/containsKey(Ljava/lang/Object;)Z 1
ifne L252
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/a()Lcom/a/b/d/bw;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/IgnoredWhisper/a()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc ""
invokeinterface com/a/b/d/bw/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L252:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID
ifeq L253
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID
astore 8
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/f()Landroid/content/SharedPreferences;
ldc "whisper"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
ifne L254
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/a()Lcom/a/b/d/bw;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/b()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface com/a/b/d/bw/containsKey(Ljava/lang/Object;)Z 1
ifeq L253
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/a()Lcom/a/b/d/bw;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/b()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface com/a/b/d/bw/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/a()Lcom/a/b/d/bw;
invokeinterface com/a/b/d/bw/b()Lcom/a/b/d/bw; 0
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/c()Ljava/lang/String;
invokeinterface com/a/b/d/bw/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/a()Lcom/a/b/d/bw;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/b()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/c()Ljava/lang/String;
invokeinterface com/a/b/d/bw/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
invokestatic com/teamspeak/ts3client/data/b/c/a()Lcom/teamspeak/ts3client/data/b/c;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/c()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/b/c/a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;
astore 9
aload 9
ifnull L255
aload 9
ifnull L253
aload 9
invokevirtual com/teamspeak/ts3client/c/a/e()Z
ifeq L253
L255:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/a()J
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/b()I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_allowWhispersFrom(JI)I
pop
L253:
aload 1
instanceof com/teamspeak/ts3client/jni/events/special/TSDNSResolv
ifeq L34
aload 1
checkcast com/teamspeak/ts3client/jni/events/special/TSDNSResolv
astore 1
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/special/TSDNSResolv/a()I
ifne L256
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
astore 8
aload 8
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/special/TSDNSResolv/b()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/c(Ljava/lang/String;)V
aload 8
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/special/TSDNSResolv/c()I
invokevirtual com/teamspeak/ts3client/data/ab/a(I)V
new com/teamspeak/ts3client/data/d/h
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/special/TSDNSResolv/b()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/data/d/h/<init>(Lcom/teamspeak/ts3client/Ts3Application;Ljava/lang/String;)V
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/i()I
ifle L257
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/special/TSDNSResolv/b()Ljava/lang/String;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/special/TSDNSResolv/c()I
aload 8
invokevirtual com/teamspeak/ts3client/data/ab/c()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "/"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/k()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/p()Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/l()Ljava/lang/String;
aload 8
invokevirtual com/teamspeak/ts3client/data/ab/d()Ljava/lang/String;
aload 8
invokevirtual com/teamspeak/ts3client/data/ab/f()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/e/a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
return
L254:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/f()Landroid/content/SharedPreferences;
ldc "whisper"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
iconst_1
if_icmpne L253
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/a()Lcom/a/b/d/bw;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/b()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface com/a/b/d/bw/containsKey(Ljava/lang/Object;)Z 1
ifeq L253
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/a()Lcom/a/b/d/bw;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/b()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface com/a/b/d/bw/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/a()Lcom/a/b/d/bw;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/b()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/c()Ljava/lang/String;
invokeinterface com/a/b/d/bw/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
invokestatic com/teamspeak/ts3client/data/b/c/a()Lcom/teamspeak/ts3client/data/b/c;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/c()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/b/c/a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;
astore 9
aload 9
ifnull L34
aload 9
ifnull L253
aload 9
invokevirtual com/teamspeak/ts3client/c/a/e()Z
ifeq L253
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/a()J
aload 8
invokevirtual com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/b()I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_allowWhispersFrom(JI)I
pop
goto L253
L257:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/special/TSDNSResolv/b()Ljava/lang/String;
aload 1
invokevirtual com/teamspeak/ts3client/jni/events/special/TSDNSResolv/c()I
aload 8
invokevirtual com/teamspeak/ts3client/data/ab/c()Ljava/lang/String;
aload 8
invokevirtual com/teamspeak/ts3client/data/ab/a()Ljava/lang/String;
aload 8
invokevirtual com/teamspeak/ts3client/data/ab/b()Ljava/lang/String;
aload 8
invokevirtual com/teamspeak/ts3client/data/ab/d()Ljava/lang/String;
aload 8
invokevirtual com/teamspeak/ts3client/data/ab/f()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/e/a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
return
L256:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/a()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/c()Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/e()Lcom/teamspeak/ts3client/t;
ldc "tsdns.error.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "tsdns.error"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
return
L27:
astore 1
goto L26
L23:
astore 9
goto L24
L16:
astore 8
goto L15
L13:
astore 8
goto L12
L224:
iconst_0
istore 2
goto L225
L99:
aload 8
astore 1
goto L100
.limit locals 11
.limit stack 10
.end method

.method public final a(Lcom/teamspeak/ts3client/tsdns/h;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnonnull L3
return
L3:
aload 1
getfield com/teamspeak/ts3client/tsdns/h/d I
ifne L4
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
astore 2
aload 2
aload 1
getfield com/teamspeak/ts3client/tsdns/h/a Ljava/lang/String;
putfield com/teamspeak/ts3client/data/ab/o Ljava/lang/String;
aload 2
aload 1
getfield com/teamspeak/ts3client/tsdns/h/c I
putfield com/teamspeak/ts3client/data/ab/p I
L0:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/r
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/r/<init>(Lcom/teamspeak/ts3client/ConnectionBackground;Lcom/teamspeak/ts3client/tsdns/h;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L1:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/n I
ifle L5
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
aload 1
getfield com/teamspeak/ts3client/tsdns/h/a Ljava/lang/String;
aload 1
getfield com/teamspeak/ts3client/tsdns/h/c I
aload 2
getfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "/"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/q J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/r Ljava/lang/String;
aload 2
getfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
aload 2
getfield com/teamspeak/ts3client/data/ab/i Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/e/a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 3
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Request Error 1"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L1
L5:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
aload 1
getfield com/teamspeak/ts3client/tsdns/h/a Ljava/lang/String;
aload 1
getfield com/teamspeak/ts3client/tsdns/h/c I
aload 2
getfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
aload 2
getfield com/teamspeak/ts3client/data/ab/g Ljava/lang/String;
aload 2
getfield com/teamspeak/ts3client/data/ab/h Ljava/lang/String;
aload 2
getfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
aload 2
getfield com/teamspeak/ts3client/data/ab/i Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/e/a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
return
L4:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/a()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
ldc "tsdns.error.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "tsdns.error"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
return
.limit locals 4
.limit stack 8
.end method

.method public final b()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
getfield com/teamspeak/ts3client/data/b/a Ljava/util/concurrent/ConcurrentHashMap;
astore 11
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
invokestatic java/util/Collections/synchronizedSortedMap(Ljava/util/SortedMap;)Ljava/util/SortedMap;
astore 12
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
invokestatic java/util/Collections/synchronizedSortedMap(Ljava/util/SortedMap;)Ljava/util/SortedMap;
astore 13
aload 11
invokevirtual java/util/concurrent/ConcurrentHashMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 14
L13:
aload 14
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L14
aload 14
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/teamspeak/ts3client/data/a
astore 15
aload 15
getfield com/teamspeak/ts3client/data/a/e J
lconst_0
lcmp
ifne L15
aload 12
aload 15
getfield com/teamspeak/ts3client/data/a/g J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aload 15
invokeinterface java/util/SortedMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L13
L15:
aload 13
aload 15
getfield com/teamspeak/ts3client/data/a/e J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokeinterface java/util/SortedMap/containsKey(Ljava/lang/Object;)Z 1
ifeq L16
aload 13
aload 15
getfield com/teamspeak/ts3client/data/a/e J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokeinterface java/util/SortedMap/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/SortedMap
aload 15
getfield com/teamspeak/ts3client/data/a/g J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aload 15
invokeinterface java/util/SortedMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L13
L16:
aload 13
aload 15
getfield com/teamspeak/ts3client/data/a/e J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
invokeinterface java/util/SortedMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 13
aload 15
getfield com/teamspeak/ts3client/data/a/e J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokeinterface java/util/SortedMap/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/SortedMap
aload 15
getfield com/teamspeak/ts3client/data/a/g J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aload 15
invokeinterface java/util/SortedMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L13
L14:
aload 11
invokevirtual java/util/concurrent/ConcurrentHashMap/size()I
iconst_1
iadd
anewarray com/teamspeak/ts3client/data/a
astore 14
new com/teamspeak/ts3client/data/a
dup
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
lconst_0
lconst_0
lconst_0
invokespecial com/teamspeak/ts3client/data/a/<init>(Ljava/lang/String;JJJ)V
astore 15
aload 15
iconst_1
putfield com/teamspeak/ts3client/data/a/q Z
aload 14
iconst_0
aload 15
aastore
lconst_0
lstore 2
iconst_1
istore 1
lconst_0
lstore 8
L17:
iload 1
aload 11
invokevirtual java/util/concurrent/ConcurrentHashMap/size()I
iconst_1
iadd
if_icmpge L18
lload 2
lconst_0
lcmp
ifne L19
aload 12
lload 8
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokeinterface java/util/SortedMap/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
astore 15
aload 15
iconst_1
putfield com/teamspeak/ts3client/data/a/q Z
aload 14
iload 1
aload 15
aastore
iload 1
iconst_1
iadd
istore 1
lload 2
lstore 4
lload 2
lstore 6
L0:
aload 13
aload 15
getfield com/teamspeak/ts3client/data/a/b J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokeinterface java/util/SortedMap/containsKey(Ljava/lang/Object;)Z 1
ifeq L6
L1:
lload 2
lstore 6
L3:
aload 15
getfield com/teamspeak/ts3client/data/a/b J
lstore 4
L4:
lload 4
lstore 6
L5:
aload 15
iconst_0
putfield com/teamspeak/ts3client/data/a/q Z
L6:
lload 4
lstore 6
L7:
aload 15
invokevirtual com/teamspeak/ts3client/data/a/j()Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/size()I
ifeq L10
L8:
lload 4
lstore 6
L9:
aload 15
iconst_0
putfield com/teamspeak/ts3client/data/a/q Z
L10:
lload 4
lstore 6
L11:
aload 15
getfield com/teamspeak/ts3client/data/a/b J
lstore 2
L12:
lload 2
lstore 8
lload 4
lstore 2
goto L17
L19:
aload 14
iload 1
iconst_1
isub
aaload
getfield com/teamspeak/ts3client/data/a/o Z
ifne L20
iconst_1
istore 10
L21:
aload 0
aload 13
aload 14
lload 2
iload 1
iconst_1
aload 11
iload 10
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(Ljava/util/SortedMap;[Lcom/teamspeak/ts3client/data/a;JIILjava/util/concurrent/ConcurrentHashMap;Ljava/lang/Boolean;)I
istore 1
lconst_0
lstore 2
goto L17
L20:
iconst_0
istore 10
goto L21
L18:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
ifnull L22
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
astore 11
aload 11
invokevirtual com/teamspeak/ts3client/t/l()Z
ifeq L22
aload 11
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/v
dup
aload 11
aload 14
invokespecial com/teamspeak/ts3client/v/<init>(Lcom/teamspeak/ts3client/t;[Lcom/teamspeak/ts3client/data/a;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L22:
return
L2:
astore 15
lload 6
lstore 2
goto L17
.limit locals 16
.limit stack 9
.end method

.method public final c()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/g Z
ifeq L0
L1:
return
L0:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_ptt"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "android_overlay"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/r Z
ifeq L3
L2:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_ptt"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "android_overlay"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/r Z
ifeq L1
L3:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/p
dup
aload 0
invokespecial com/teamspeak/ts3client/p/<init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 4
.end method

.method public final d()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/v Z
ifne L0
L1:
return
L0:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_ptt"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "android_overlay"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L2
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/r Z
ifeq L3
L2:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_ptt"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "android_overlay"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L1
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/r Z
ifeq L1
L3:
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/q
dup
aload 0
invokespecial com/teamspeak/ts3client/q/<init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 4
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public onCreate()V
aload 0
invokespecial android/app/Service/onCreate()V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokevirtual com/teamspeak/ts3client/StartGUIFragment/l()V
aload 0
aload 0
ldc "window"
invokevirtual com/teamspeak/ts3client/ConnectionBackground/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/WindowManager
putfield com/teamspeak/ts3client/ConnectionBackground/s Landroid/view/WindowManager;
aload 0
new android/widget/ImageView
dup
aload 0
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/ConnectionBackground/t Landroid/widget/ImageView;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/t Landroid/widget/ImageView;
ldc_w 2130837622
invokevirtual android/widget/ImageView/setImageResource(I)V
aload 0
new android/content/IntentFilter
dup
invokespecial android/content/IntentFilter/<init>()V
putfield com/teamspeak/ts3client/ConnectionBackground/x Landroid/content/IntentFilter;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/x Landroid/content/IntentFilter;
ldc "android.intent.action.CONFIGURATION_CHANGED"
invokevirtual android/content/IntentFilter/addAction(Ljava/lang/String;)V
aload 0
new android/view/WindowManager$LayoutParams
dup
bipush -2
bipush -2
sipush 2002
bipush 8
bipush -3
invokespecial android/view/WindowManager$LayoutParams/<init>(IIIII)V
putfield com/teamspeak/ts3client/ConnectionBackground/u Landroid/view/WindowManager$LayoutParams;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/u Landroid/view/WindowManager$LayoutParams;
bipush 51
putfield android/view/WindowManager$LayoutParams/gravity I
aload 0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/u Landroid/view/WindowManager$LayoutParams;
invokespecial com/teamspeak/ts3client/ConnectionBackground/a(Landroid/view/WindowManager$LayoutParams;)V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/t Landroid/widget/ImageView;
new com/teamspeak/ts3client/o
dup
aload 0
invokespecial com/teamspeak/ts3client/o/<init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V
invokevirtual android/widget/ImageView/setOnTouchListener(Landroid/view/View$OnTouchListener;)V
return
.limit locals 1
.limit stack 8
.end method

.method public onDestroy()V
aload 0
invokespecial android/app/Service/onDestroy()V
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/t Landroid/widget/ImageView;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/v Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/s Landroid/view/WindowManager;
aload 0
getfield com/teamspeak/ts3client/ConnectionBackground/t Landroid/widget/ImageView;
invokeinterface android/view/WindowManager/removeView(Landroid/view/View;)V 1
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
iconst_2
ireturn
.limit locals 4
.limit stack 1
.end method
