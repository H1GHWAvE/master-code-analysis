.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/e/b
.super android/support/v4/app/Fragment
.implements com/teamspeak/ts3client/data/b/e

.field private 'a' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'b' Lcom/teamspeak/ts3client/data/b/f;

.field private 'c' Lcom/teamspeak/ts3client/e/e;

.field private 'd' Landroid/widget/ListView;

.field private 'e' Lcom/teamspeak/ts3client/customs/FloatingButton;

.method public <init>()V
aload 0
invokespecial android/support/v4/app/Fragment/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/e/b/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/e/b/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
invokevirtual com/teamspeak/ts3client/e/b/n()V
aload 0
getfield com/teamspeak/ts3client/e/b/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
ldc "menu.identity"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v7/app/a/b(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/e/b/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 1
ldc_w 2130903105
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 2
aload 0
aload 2
ldc_w 2131493254
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ListView
putfield com/teamspeak/ts3client/e/b/d Landroid/widget/ListView;
aload 0
aload 2
ldc_w 2131493255
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/customs/FloatingButton
putfield com/teamspeak/ts3client/e/b/e Lcom/teamspeak/ts3client/customs/FloatingButton;
aload 0
getfield com/teamspeak/ts3client/e/b/e Lcom/teamspeak/ts3client/customs/FloatingButton;
aload 0
invokevirtual com/teamspeak/ts3client/e/b/j()Landroid/content/res/Resources;
ldc_w 2130837630
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield com/teamspeak/ts3client/e/b/e Lcom/teamspeak/ts3client/customs/FloatingButton;
new com/teamspeak/ts3client/e/c
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/e/c/<init>(Lcom/teamspeak/ts3client/e/b;Landroid/view/LayoutInflater;)V
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method public final a()V
aload 0
invokevirtual com/teamspeak/ts3client/e/b/b()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/a(Landroid/os/Bundle;)V
aload 0
invokestatic com/teamspeak/ts3client/data/b/f/a()Lcom/teamspeak/ts3client/data/b/f;
putfield com/teamspeak/ts3client/e/b/b Lcom/teamspeak/ts3client/data/b/f;
aload 0
invokestatic com/teamspeak/ts3client/data/b/f/a(Lcom/teamspeak/ts3client/data/b/e;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
aload 1
invokeinterface android/view/Menu/clear()V 0
aload 0
aload 1
aload 2
invokespecial android/support/v4/app/Fragment/a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final b()V
aload 0
new com/teamspeak/ts3client/e/e
dup
aload 0
invokevirtual com/teamspeak/ts3client/e/b/i()Landroid/support/v4/app/bb;
invokespecial com/teamspeak/ts3client/e/e/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/e/b/c Lcom/teamspeak/ts3client/e/e;
aload 0
getfield com/teamspeak/ts3client/e/b/b Lcom/teamspeak/ts3client/data/b/f;
invokevirtual com/teamspeak/ts3client/data/b/f/c()Ljava/util/ArrayList;
astore 2
aload 2
ifnull L0
aload 2
invokevirtual java/util/ArrayList/size()I
ifne L1
L0:
aload 0
getfield com/teamspeak/ts3client/e/b/b Lcom/teamspeak/ts3client/data/b/f;
invokevirtual com/teamspeak/ts3client/data/b/f/b()V
aload 0
getfield com/teamspeak/ts3client/e/b/b Lcom/teamspeak/ts3client/data/b/f;
invokevirtual com/teamspeak/ts3client/data/b/f/c()Ljava/util/ArrayList;
astore 2
L2:
aload 2
ifnull L3
iconst_0
istore 1
L4:
iload 1
aload 2
invokevirtual java/util/ArrayList/size()I
if_icmpge L3
aload 0
getfield com/teamspeak/ts3client/e/b/c Lcom/teamspeak/ts3client/e/e;
astore 3
aload 2
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/e/a
astore 4
aload 3
getfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
aload 4
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L5
aload 3
getfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L5:
iload 1
iconst_1
iadd
istore 1
goto L4
L3:
aload 0
getfield com/teamspeak/ts3client/e/b/d Landroid/widget/ListView;
aload 0
getfield com/teamspeak/ts3client/e/b/c Lcom/teamspeak/ts3client/e/e;
invokevirtual android/widget/ListView/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield com/teamspeak/ts3client/e/b/c Lcom/teamspeak/ts3client/e/e;
invokevirtual com/teamspeak/ts3client/e/e/notifyDataSetChanged()V
return
L1:
goto L2
.limit locals 5
.limit stack 4
.end method

.method public final c(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/c(Landroid/os/Bundle;)V
aload 0
invokevirtual com/teamspeak/ts3client/e/b/b()V
return
.limit locals 2
.limit stack 2
.end method
