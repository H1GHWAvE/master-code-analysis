.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/h
.super android/support/v4/app/ax
.implements com/teamspeak/ts3client/data/d/s
.implements com/teamspeak/ts3client/data/w

.field private static 'at' Lcom/teamspeak/ts3client/h;

.field private static 'au' Ljava/util/regex/Pattern;

.field private static 'av' Lcom/teamspeak/ts3client/data/c;

.field private 'aA' Landroid/widget/TableLayout;

.field private 'aB' Landroid/support/v4/app/bi;

.field private 'aC' Lcom/teamspeak/ts3client/d/b/a;

.field private 'aD' Landroid/widget/TextView;

.field private 'aE' I

.field private 'aF' Landroid/widget/LinearLayout;

.field private 'aG' Landroid/content/Context;

.field private 'aH' Landroid/widget/TextView;

.field private 'aI' Landroid/widget/TableRow;

.field private 'aJ' Lcom/teamspeak/ts3client/customs/FloatingButton;

.field private 'aw' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'ax' Landroid/widget/TextView;

.field private 'ay' Landroid/widget/TextView;

.field private 'az' Landroid/widget/TableLayout;

.method public <init>()V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic A()Ljava/util/regex/Pattern;
getstatic com/teamspeak/ts3client/h/au Ljava/util/regex/Pattern;
areturn
.limit locals 0
.limit stack 1
.end method

.method private B()I
aload 0
getfield com/teamspeak/ts3client/h/aw Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
ldc_w 16.0F
fmul
ldc_w 0.5F
fadd
f2i
ireturn
.limit locals 1
.limit stack 2
.end method

.method private C()V
aload 0
invokevirtual com/teamspeak/ts3client/h/l()Z
ifeq L0
aload 0
invokevirtual com/teamspeak/ts3client/h/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/j
dup
aload 0
invokespecial com/teamspeak/ts3client/j/<init>(Lcom/teamspeak/ts3client/h;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/h;)Landroid/support/v4/app/bi;
aload 0
getfield com/teamspeak/ts3client/h/aB Landroid/support/v4/app/bi;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/h;Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/d/b/a;
aload 0
aload 1
putfield com/teamspeak/ts3client/h/aC Lcom/teamspeak/ts3client/d/b/a;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Lcom/teamspeak/ts3client/data/c;)Lcom/teamspeak/ts3client/h;
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
aload 0
if_acmpne L0
getstatic com/teamspeak/ts3client/h/at Lcom/teamspeak/ts3client/h;
areturn
L0:
new com/teamspeak/ts3client/h
dup
invokespecial com/teamspeak/ts3client/h/<init>()V
putstatic com/teamspeak/ts3client/h/at Lcom/teamspeak/ts3client/h;
aload 0
putstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
ldc ".*(\\[Build: (\\d+)\\]).*"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putstatic com/teamspeak/ts3client/h/au Ljava/util/regex/Pattern;
getstatic com/teamspeak/ts3client/h/at Lcom/teamspeak/ts3client/h;
areturn
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/widget/TableLayout;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
aload 0
invokevirtual com/teamspeak/ts3client/h/i()Landroid/support/v4/app/bb;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
ldc_w 2130903087
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
checkcast android/widget/TableRow
astore 4
aload 3
ifnull L0
aload 4
ldc_w 2131493210
invokevirtual android/widget/TableRow/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
astore 5
aload 5
aload 0
invokespecial com/teamspeak/ts3client/h/B()I
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 5
aload 0
invokespecial com/teamspeak/ts3client/h/B()I
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 5
aload 3
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
L0:
aload 4
ldc_w 2131493211
invokevirtual android/widget/TableRow/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
aload 2
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 1
aload 4
invokevirtual android/widget/TableLayout/addView(Landroid/view/View;)V
return
.limit locals 6
.limit stack 3
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/h;Landroid/widget/TableLayout;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
aload 0
invokevirtual com/teamspeak/ts3client/h/i()Landroid/support/v4/app/bb;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
ldc_w 2130903087
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
checkcast android/widget/TableRow
astore 4
aload 3
ifnull L0
aload 4
ldc_w 2131493210
invokevirtual android/widget/TableRow/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
astore 5
aload 5
aload 0
invokespecial com/teamspeak/ts3client/h/B()I
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 5
aload 0
invokespecial com/teamspeak/ts3client/h/B()I
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 5
aload 3
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
L0:
aload 4
ldc_w 2131493211
invokevirtual android/widget/TableRow/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
aload 2
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 1
aload 4
invokevirtual android/widget/TableLayout/addView(Landroid/view/View;)V
return
.limit locals 6
.limit stack 3
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/h;)Lcom/teamspeak/ts3client/d/b/a;
aload 0
getfield com/teamspeak/ts3client/h/aC Lcom/teamspeak/ts3client/d/b/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/h;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/h/ax Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/h;)I
aload 0
getfield com/teamspeak/ts3client/h/aE I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/h;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/h/aw Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/h;)Landroid/widget/TableRow;
aload 0
getfield com/teamspeak/ts3client/h/aI Landroid/widget/TableRow;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/h;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/h/aH Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/h;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/h/ay Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/h;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/h/aD Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/h;)Landroid/widget/TableLayout;
aload 0
getfield com/teamspeak/ts3client/h/az Landroid/widget/TableLayout;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/h;)Landroid/widget/TableLayout;
aload 0
getfield com/teamspeak/ts3client/h/aA Landroid/widget/TableLayout;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/h;)Landroid/widget/LinearLayout;
aload 0
getfield com/teamspeak/ts3client/h/aF Landroid/widget/LinearLayout;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/h;)Landroid/content/Context;
aload 0
getfield com/teamspeak/ts3client/h/aG Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic z()Lcom/teamspeak/ts3client/data/c;
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 1
ldc_w 2130903086
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 1
aload 0
aload 1
invokevirtual android/view/View/getContext()Landroid/content/Context;
putfield com/teamspeak/ts3client/h/aG Landroid/content/Context;
aload 0
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
putfield com/teamspeak/ts3client/h/aB Landroid/support/v4/app/bi;
aload 0
aload 1
ldc_w 2131493197
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/h/ax Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493201
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/h/ay Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493203
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/h/aD Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493205
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TableLayout
putfield com/teamspeak/ts3client/h/az Landroid/widget/TableLayout;
aload 0
aload 1
ldc_w 2131493207
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TableLayout
putfield com/teamspeak/ts3client/h/aA Landroid/widget/TableLayout;
aload 0
aload 1
ldc_w 2131493208
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/LinearLayout
putfield com/teamspeak/ts3client/h/aF Landroid/widget/LinearLayout;
aload 0
aload 1
ldc_w 2131493209
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/customs/FloatingButton
putfield com/teamspeak/ts3client/h/aJ Lcom/teamspeak/ts3client/customs/FloatingButton;
aload 0
getfield com/teamspeak/ts3client/h/aJ Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837621
ldc_w 22.0F
ldc_w 22.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
aload 1
ldc_w 2131493199
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/h/aH Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493025
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TableRow
putfield com/teamspeak/ts3client/h/aI Landroid/widget/TableRow;
ldc "clientinfo.nickname"
aload 1
ldc_w 2131493196
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "clientinfo.version"
aload 1
ldc_w 2131493200
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "clientinfo.connected"
aload 1
ldc_w 2131493202
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "clientinfo.sgroup"
aload 1
ldc_w 2131493204
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "clientinfo.cgroup"
aload 1
ldc_w 2131493206
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "clientinfo.description"
aload 1
ldc_w 2131493198
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
aload 0
invokevirtual com/teamspeak/ts3client/h/n()V
aload 0
getfield com/teamspeak/ts3client/h/aw Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
ldc "dialog.client.info.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v7/app/a/b(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/h/aJ Lcom/teamspeak/ts3client/customs/FloatingButton;
new com/teamspeak/ts3client/i
dup
aload 0
invokespecial com/teamspeak/ts3client/i/<init>(Lcom/teamspeak/ts3client/h;)V
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/ax/a(Landroid/os/Bundle;)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/h/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/h/aw Lcom/teamspeak/ts3client/Ts3Application;
aload 0
new android/widget/TextView
dup
aload 0
invokevirtual com/teamspeak/ts3client/h/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getBaseContext()Landroid/content/Context;
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
invokevirtual android/widget/TextView/getTextColors()Landroid/content/res/ColorStateList;
invokevirtual android/content/res/ColorStateList/getDefaultColor()I
putfield com/teamspeak/ts3client/h/aE I
return
.limit locals 2
.limit stack 4
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
aload 1
invokeinterface android/view/Menu/clear()V 0
aload 0
aload 1
aload 2
invokespecial android/support/v4/app/ax/a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
aload 1
instanceof com/teamspeak/ts3client/jni/events/UpdateClient
ifeq L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/UpdateClient
getfield com/teamspeak/ts3client/jni/events/UpdateClient/a I
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
if_icmpne L0
aload 0
invokevirtual com/teamspeak/ts3client/h/l()Z
ifeq L1
aload 0
invokevirtual com/teamspeak/ts3client/h/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/j
dup
aload 0
invokespecial com/teamspeak/ts3client/j/<init>(Lcom/teamspeak/ts3client/h;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L1:
aload 0
getfield com/teamspeak/ts3client/h/aw Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
L0:
return
.limit locals 2
.limit stack 4
.end method

.method public final c(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/ax/c(Landroid/os/Bundle;)V
aload 0
getfield com/teamspeak/ts3client/h/aw Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/h/aw Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
ifnull L1
aload 0
getfield com/teamspeak/ts3client/h/aw Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/h/aw Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
ldc "ClientInfoFragment"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestClientVariables(JILjava/lang/String;)I
pop
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
ifnull L2
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/k Z
ifne L1
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/y Landroid/graphics/Bitmap;
ifnonnull L1
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/b()Ljava/lang/String;
pop
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/b()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
aload 0
invokevirtual com/teamspeak/ts3client/data/c/a(Lcom/teamspeak/ts3client/data/d/s;)V
L1:
aload 0
getfield com/teamspeak/ts3client/h/aw Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
L0:
return
L2:
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/y Landroid/graphics/Bitmap;
ifnonnull L1
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/b()Ljava/lang/String;
pop
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/b()Ljava/lang/String;
ifnull L1
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/b()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
getstatic com/teamspeak/ts3client/h/av Lcom/teamspeak/ts3client/data/c;
aload 0
invokevirtual com/teamspeak/ts3client/data/c/a(Lcom/teamspeak/ts3client/data/d/s;)V
goto L1
.limit locals 2
.limit stack 5
.end method

.method public final y()V
aload 0
invokevirtual com/teamspeak/ts3client/h/l()Z
ifeq L0
aload 0
invokevirtual com/teamspeak/ts3client/h/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/k
dup
aload 0
invokespecial com/teamspeak/ts3client/k/<init>(Lcom/teamspeak/ts3client/h;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
return
.limit locals 1
.limit stack 4
.end method
