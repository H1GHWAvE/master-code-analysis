.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/b/ab
.super android/support/v4/app/ax

.field private 'at' Lcom/teamspeak/ts3client/data/c;

.field private 'au' Landroid/widget/Button;

.field private 'av' Landroid/widget/TextView;

.field private 'aw' Landroid/widget/TextView;

.field private 'ax' Landroid/widget/TextView;

.field private 'ay' Landroid/widget/Spinner;

.method public <init>(Lcom/teamspeak/ts3client/data/c;)V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/b/ab/at Lcom/teamspeak/ts3client/data/c;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/b/ab/ax Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/d/b/ab/ay Landroid/widget/Spinner;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/b/ab;)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/d/b/ab/at Lcom/teamspeak/ts3client/data/c;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/b/ab/aw Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
astore 3
aload 1
ldc_w 2130903085
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/LinearLayout
astore 1
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ldc "clientdialog.ban.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
ldc "clientdialog.ban.name"
aload 1
ldc_w 2131493186
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "clientdialog.ban.reason"
aload 1
ldc_w 2131493188
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "clientdialog.ban.duration"
aload 1
ldc_w 2131493190
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 1
ldc_w 2131493193
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Spinner
ldc "clientdialog.ban.duration.array"
aload 0
invokevirtual com/teamspeak/ts3client/d/b/ab/i()Landroid/support/v4/app/bb;
iconst_4
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
aload 0
aload 1
ldc_w 2131493194
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/ab/au Landroid/widget/Button;
aload 0
getfield com/teamspeak/ts3client/d/b/ab/au Landroid/widget/Button;
ldc "clientdialog.ban.button"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 0
aload 1
ldc_w 2131493187
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/b/ab/av Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493189
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/b/ab/aw Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493192
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/b/ab/ax Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493193
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Spinner
putfield com/teamspeak/ts3client/d/b/ab/ay Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/d/b/ab/av Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/d/b/ab/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " / "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/d/b/ab/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/b/ab/au Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/ac
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/d/b/ac/<init>(Lcom/teamspeak/ts3client/d/b/ab;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
areturn
.limit locals 4
.limit stack 5
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/ax/e()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final f()V
aload 0
invokespecial android/support/v4/app/ax/f()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/b(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method
