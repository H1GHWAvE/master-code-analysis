.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/z
.super android/support/v4/app/ax
.implements com/teamspeak/ts3client/data/w

.field private static 'at' Ljava/util/regex/Pattern;

.field private 'aA' Landroid/widget/TextView;

.field private 'aB' Landroid/widget/TextView;

.field private 'aC' Landroid/widget/TextView;

.field private 'aD' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'aE' Ljava/lang/Thread;

.field private 'aF' Z

.field private 'aG' Landroid/widget/TextView;

.field private 'aH' Landroid/widget/TextView;

.field private 'aI' Landroid/widget/TextView;

.field private 'aJ' Landroid/widget/TextView;

.field private 'aK' Landroid/widget/TextView;

.field private 'aL' Landroid/widget/TextView;

.field private 'aM' Landroid/widget/TextView;

.field private 'aN' Landroid/widget/TextView;

.field private 'aO' Landroid/widget/TextView;

.field private 'aP' Landroid/widget/TextView;

.field private 'aQ' Landroid/widget/TextView;

.field private 'aR' Landroid/widget/TextView;

.field private 'au' Landroid/widget/TextView;

.field private 'av' Landroid/widget/TextView;

.field private 'aw' Landroid/widget/TextView;

.field private 'ax' Landroid/widget/TextView;

.field private 'ay' Landroid/widget/TextView;

.field private 'az' Landroid/widget/TextView;

.method static <clinit>()V
ldc ".*(\\[Build: (\\d+)\\]).*"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putstatic com/teamspeak/ts3client/d/z/at Ljava/util/regex/Pattern;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
iconst_1
putfield com/teamspeak/ts3client/d/z/aF Z
return
.limit locals 1
.limit stack 2
.end method

.method private A()V
aload 0
invokevirtual com/teamspeak/ts3client/d/z/l()Z
ifne L0
return
L0:
new java/text/DecimalFormat
dup
ldc "0.00%"
invokespecial java/text/DecimalFormat/<init>(Ljava/lang/String;)V
astore 1
aload 0
invokevirtual com/teamspeak/ts3client/d/z/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/d/ac
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/ac/<init>(Lcom/teamspeak/ts3client/d/z;Ljava/text/DecimalFormat;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 2
.limit stack 5
.end method

.method private B()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getServerLicenseType(J)I
tableswitch 0
L0
L1
L2
L3
default : L4
L4:
ldc "Error"
areturn
L0:
ldc "license.no"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
areturn
L1:
ldc "license.licensed"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
areturn
L2:
ldc "license.offline"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
areturn
L3:
ldc "license.npl"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/z;)Z
aload 0
getfield com/teamspeak/ts3client/d/z/aF Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/ay Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/au Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aB Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aC Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aG Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aH Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aJ Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aI Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aK Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aL Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aM Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic n(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aN Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic o(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aO Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic p(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aP Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic q(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aQ Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic r(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aR Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private y()V
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/e Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
astore 2
getstatic com/teamspeak/ts3client/d/z/at Ljava/util/regex/Pattern;
aload 2
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 3
aload 2
astore 1
aload 3
invokevirtual java/util/regex/Matcher/find()Z
ifeq L0
aload 2
astore 1
aload 3
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
ldc2_w 20000L
lcmp
ifle L0
aload 2
aload 3
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "("
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iconst_2
iconst_2
invokestatic java/text/DateFormat/getDateTimeInstance(II)Ljava/text/DateFormat;
new java/util/Date
dup
aload 3
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
ldc2_w 1000L
lmul
invokespecial java/util/Date/<init>(J)V
invokevirtual java/text/DateFormat/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 1
L0:
aload 0
getfield com/teamspeak/ts3client/d/z/av Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/z/aw Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/d Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/z/ax Landroid/widget/TextView;
astore 2
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getServerLicenseType(J)I
tableswitch 0
L1
L2
L3
L4
default : L5
L5:
ldc "Error"
astore 1
L6:
aload 2
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/z/aA Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/o Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/p I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/z/az Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
L1:
ldc "license.no"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 1
goto L6
L2:
ldc "license.licensed"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 1
goto L6
L3:
ldc "license.offline"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 1
goto L6
L4:
ldc "license.npl"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 1
goto L6
.limit locals 4
.limit stack 10
.end method

.method private z()V
aload 0
invokevirtual com/teamspeak/ts3client/d/z/l()Z
ifne L0
return
L0:
aload 0
invokevirtual com/teamspeak/ts3client/d/z/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/d/ab
dup
aload 0
invokespecial com/teamspeak/ts3client/d/ab/<init>(Lcom/teamspeak/ts3client/d/z;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 4
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 1
ldc_w 2130903122
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/HorizontalScrollView
astore 1
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ldc "connectioninfo.title"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
putfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
aload 0
aload 1
ldc_w 2131493279
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/au Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493281
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/av Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493283
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aw Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493285
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/ax Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493287
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/ay Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493289
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/az Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493291
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aA Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493293
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aB Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493295
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aC Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493299
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aG Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493300
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aH Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493302
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aJ Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493303
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aI Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493305
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aK Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493306
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aL Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493308
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aM Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493309
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aN Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493311
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aO Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493312
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aP Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493314
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aQ Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493315
invokevirtual android/widget/HorizontalScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/z/aR Landroid/widget/TextView;
ldc "connectioninfo.name"
aload 1
ldc_w 2131493278
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.version"
aload 1
ldc_w 2131493280
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.platform"
aload 1
ldc_w 2131493282
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.license"
aload 1
ldc_w 2131493284
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.uptime"
aload 1
ldc_w 2131493286
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.address"
aload 1
ldc_w 2131493288
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.ip"
aload 1
ldc_w 2131493290
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.ping"
aload 1
ldc_w 2131493292
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.packetloss"
aload 1
ldc_w 2131493294
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.packettrans"
aload 1
ldc_w 2131493298
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.bytetrans"
aload 1
ldc_w 2131493301
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.bandwidthsec"
aload 1
ldc_w 2131493304
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.bandwidthmin"
aload 1
ldc_w 2131493307
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.filebandwidth"
aload 1
ldc_w 2131493310
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.filebyte"
aload 1
ldc_w 2131493313
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.tablein"
aload 1
ldc_w 2131493296
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "connectioninfo.tableout"
aload 1
ldc_w 2131493297
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/ax/a(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
aload 1
instanceof com/teamspeak/ts3client/jni/events/ServerUpdated
ifeq L0
aload 0
invokevirtual com/teamspeak/ts3client/d/z/l()Z
ifeq L0
aload 0
invokevirtual com/teamspeak/ts3client/d/z/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/d/ab
dup
aload 0
invokespecial com/teamspeak/ts3client/d/ab/<init>(Lcom/teamspeak/ts3client/d/z;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ServerConnectionInfo
ifeq L1
aload 0
invokevirtual com/teamspeak/ts3client/d/z/l()Z
ifeq L1
new java/text/DecimalFormat
dup
ldc "0.00%"
invokespecial java/text/DecimalFormat/<init>(Ljava/lang/String;)V
astore 2
aload 0
invokevirtual com/teamspeak/ts3client/d/z/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/d/ac
dup
aload 0
aload 2
invokespecial com/teamspeak/ts3client/d/ac/<init>(Lcom/teamspeak/ts3client/d/z;Ljava/text/DecimalFormat;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L1:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ServerPermissionError
ifeq L2
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ServerPermissionError
getfield com/teamspeak/ts3client/jni/events/rare/ServerPermissionError/a I
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/x Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
if_icmpne L2
aload 0
invokevirtual com/teamspeak/ts3client/d/z/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/d/ad
dup
aload 0
invokespecial com/teamspeak/ts3client/d/ad/<init>(Lcom/teamspeak/ts3client/d/z;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L2:
return
.limit locals 3
.limit stack 5
.end method

.method public final b()V
aload 0
invokespecial android/support/v4/app/ax/b()V
return
.limit locals 1
.limit stack 1
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/ax/e()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final f()V
aload 0
invokespecial android/support/v4/app/ax/f()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/b(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final s()V
aload 0
invokespecial android/support/v4/app/ax/s()V
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/e Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
astore 2
getstatic com/teamspeak/ts3client/d/z/at Ljava/util/regex/Pattern;
aload 2
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 3
aload 2
astore 1
aload 3
invokevirtual java/util/regex/Matcher/find()Z
ifeq L0
aload 2
astore 1
aload 3
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
ldc2_w 20000L
lcmp
ifle L0
aload 2
aload 3
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "("
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iconst_2
iconst_2
invokestatic java/text/DateFormat/getDateTimeInstance(II)Ljava/text/DateFormat;
new java/util/Date
dup
aload 3
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
ldc2_w 1000L
lmul
invokespecial java/util/Date/<init>(J)V
invokevirtual java/text/DateFormat/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 1
L0:
aload 0
getfield com/teamspeak/ts3client/d/z/av Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/z/aw Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/d Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/z/ax Landroid/widget/TextView;
astore 2
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getServerLicenseType(J)I
tableswitch 0
L1
L2
L3
L4
default : L5
L5:
ldc "Error"
astore 1
L6:
aload 2
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/z/aA Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/o Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/p I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/z/az Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestServerVariables(J)I
pop
aload 0
iconst_1
putfield com/teamspeak/ts3client/d/z/aF Z
aload 0
new java/lang/Thread
dup
new com/teamspeak/ts3client/d/aa
dup
aload 0
invokespecial com/teamspeak/ts3client/d/aa/<init>(Lcom/teamspeak/ts3client/d/z;)V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
putfield com/teamspeak/ts3client/d/z/aE Ljava/lang/Thread;
aload 0
getfield com/teamspeak/ts3client/d/z/aE Ljava/lang/Thread;
invokevirtual java/lang/Thread/start()V
return
L1:
ldc "license.no"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 1
goto L6
L2:
ldc "license.licensed"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 1
goto L6
L3:
ldc "license.offline"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 1
goto L6
L4:
ldc "license.npl"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 1
goto L6
.limit locals 4
.limit stack 10
.end method

.method public final t()V
aload 0
invokespecial android/support/v4/app/ax/t()V
aload 0
getfield com/teamspeak/ts3client/d/z/aD Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/z/aF Z
aload 0
getfield com/teamspeak/ts3client/d/z/aE Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
return
.limit locals 1
.limit stack 2
.end method
