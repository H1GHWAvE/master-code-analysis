.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/a/t
.super android/support/v4/app/ax

.field private 'at' Lcom/teamspeak/ts3client/d/a/m;

.field private 'au' Landroid/widget/EditText;

.field private 'av' Landroid/widget/Button;

.field private 'aw' Landroid/view/View;

.field private 'ax' Landroid/view/View;

.field private 'ay' Landroid/view/View;

.method public <init>(Lcom/teamspeak/ts3client/d/a/m;)V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/a/t/at Lcom/teamspeak/ts3client/d/a/m;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/a/t;)Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/d/a/t/au Landroid/widget/EditText;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/a/t;)Lcom/teamspeak/ts3client/d/a/m;
aload 0
getfield com/teamspeak/ts3client/d/a/t/at Lcom/teamspeak/ts3client/d/a/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 1
ldc_w 2130903074
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/LinearLayout
astore 1
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ldc "channeldialog.editdesc.dialog"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 1
ldc_w 2131493064
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
putfield com/teamspeak/ts3client/d/a/t/ay Landroid/view/View;
aload 0
getfield com/teamspeak/ts3client/d/a/t/ay Landroid/view/View;
new com/teamspeak/ts3client/d/a/u
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/u/<init>(Lcom/teamspeak/ts3client/d/a/t;)V
invokevirtual android/view/View/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493063
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
putfield com/teamspeak/ts3client/d/a/t/ax Landroid/view/View;
aload 0
getfield com/teamspeak/ts3client/d/a/t/ax Landroid/view/View;
new com/teamspeak/ts3client/d/a/v
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/v/<init>(Lcom/teamspeak/ts3client/d/a/t;)V
invokevirtual android/view/View/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493065
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
putfield com/teamspeak/ts3client/d/a/t/aw Landroid/view/View;
aload 0
getfield com/teamspeak/ts3client/d/a/t/aw Landroid/view/View;
new com/teamspeak/ts3client/d/a/w
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/w/<init>(Lcom/teamspeak/ts3client/d/a/t;)V
invokevirtual android/view/View/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493066
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
putfield com/teamspeak/ts3client/d/a/t/au Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/d/a/t/au Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/d/a/t/at Lcom/teamspeak/ts3client/d/a/m;
getfield com/teamspeak/ts3client/d/a/m/at Ljava/lang/String;
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 0
aload 1
ldc_w 2131493067
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/a/t/av Landroid/widget/Button;
aload 0
getfield com/teamspeak/ts3client/d/a/t/av Landroid/widget/Button;
ldc "button.save"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/a/t/av Landroid/widget/Button;
new com/teamspeak/ts3client/d/a/x
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/x/<init>(Lcom/teamspeak/ts3client/d/a/t;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method protected final b(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/d/a/t/au Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
astore 4
aload 0
getfield com/teamspeak/ts3client/d/a/t/au Landroid/widget/EditText;
invokevirtual android/widget/EditText/getSelectionStart()I
istore 2
aload 0
getfield com/teamspeak/ts3client/d/a/t/au Landroid/widget/EditText;
invokevirtual android/widget/EditText/getSelectionEnd()I
istore 3
iload 2
iload 3
if_icmpeq L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
iconst_0
iload 2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iconst_0
iconst_3
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iload 2
iload 3
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iconst_3
bipush 7
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iload 3
aload 4
invokevirtual java/lang/String/length()I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
aload 0
getfield com/teamspeak/ts3client/d/a/t/au Landroid/widget/EditText;
aload 1
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/a/t/au Landroid/widget/EditText;
iload 3
iload 2
iconst_3
iadd
iadd
iload 2
isub
invokevirtual android/widget/EditText/setSelection(I)V
return
L0:
aload 0
getfield com/teamspeak/ts3client/d/a/t/au Landroid/widget/EditText;
invokevirtual android/widget/EditText/getSelectionStart()I
aload 4
invokevirtual java/lang/String/length()I
if_icmpne L1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L2:
aload 0
getfield com/teamspeak/ts3client/d/a/t/au Landroid/widget/EditText;
aload 1
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/a/t/au Landroid/widget/EditText;
iload 2
iconst_3
iadd
invokevirtual android/widget/EditText/setSelection(I)V
return
L1:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
iconst_0
iload 2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iload 2
aload 4
invokevirtual java/lang/String/length()I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
goto L2
.limit locals 5
.limit stack 4
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/ax/e()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final f()V
aload 0
invokespecial android/support/v4/app/ax/f()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/b(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method
