.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/m
.super android/app/Dialog

.field private 'a' Ljava/io/File;

.field private 'b' Landroid/content/Context;

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/app/Dialog/<init>(Landroid/content/Context;)V
aload 0
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Environment/DIRECTORY_DOWNLOADS Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putfield com/teamspeak/ts3client/d/m/a Ljava/io/File;
return
.limit locals 2
.limit stack 5
.end method

.method private a(F)I
aload 0
invokevirtual com/teamspeak/ts3client/d/m/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fload 1
fmul
ldc_w 0.5F
fadd
f2i
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/m;Ljava/lang/String;)Lcom/teamspeak/ts3client/e/a;
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/m/a(Ljava/lang/String;)Lcom/teamspeak/ts3client/e/a;
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;)Lcom/teamspeak/ts3client/e/a;
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch java/io/FileNotFoundException from L1 to L4 using L2
.catch java/io/IOException from L1 to L4 using L3
.catch java/io/FileNotFoundException from L5 to L6 using L2
.catch java/io/IOException from L5 to L6 using L3
.catch java/io/FileNotFoundException from L7 to L8 using L2
.catch java/io/IOException from L7 to L8 using L3
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/d/m/a Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual java/io/File/exists()Z
ifne L9
new android/app/AlertDialog$Builder
dup
aload 0
getfield com/teamspeak/ts3client/d/m/b Landroid/content/Context;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 1
aload 1
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/app/Dialog;)V
aload 1
ldc "ident.file.error.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 1
ldc "ident.file.error.text2"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 1
bipush -2
ldc "button.cancel"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/d/p
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/p/<init>(Lcom/teamspeak/ts3client/d/m;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 1
invokevirtual android/app/AlertDialog/show()V
aconst_null
areturn
L9:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 1
L0:
new java/io/FileInputStream
dup
aload 2
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 2
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 2
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 3
L1:
aload 3
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 4
L4:
aload 4
ifnull L7
L5:
aload 1
aload 4
invokestatic com/teamspeak/ts3client/data/d/x/b(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L6:
goto L1
L2:
astore 2
aload 2
invokevirtual java/io/FileNotFoundException/printStackTrace()V
L10:
aload 1
invokevirtual java/util/ArrayList/size()I
ifle L11
aload 1
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
ldc "[Identity]"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L11
aload 1
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
ldc "id"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L11
aload 1
iconst_2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
ldc "identity="
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L11
aload 1
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
ldc "nickname="
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L12
L11:
aload 0
invokespecial com/teamspeak/ts3client/d/m/a()V
aconst_null
areturn
L7:
aload 3
invokevirtual java/io/BufferedReader/close()V
aload 2
invokevirtual java/io/FileInputStream/close()V
L8:
goto L10
L3:
astore 2
aload 2
invokevirtual java/io/IOException/printStackTrace()V
goto L10
L12:
new com/teamspeak/ts3client/e/a
dup
invokespecial com/teamspeak/ts3client/e/a/<init>()V
astore 2
aload 2
aload 1
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
ldc "id="
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/e/a/b Ljava/lang/String;
aload 1
iconst_2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
ldc "identity="
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
astore 3
aload 2
aload 3
iconst_1
aload 3
invokevirtual java/lang/String/length()I
iconst_1
isub
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
putfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
aload 2
getfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L13
aload 2
getfield com/teamspeak/ts3client/e/a/b Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L14
L13:
aload 0
invokespecial com/teamspeak/ts3client/d/m/a()V
aconst_null
areturn
L14:
aload 2
aload 1
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
ldc "nickname="
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/e/a/d Ljava/lang/String;
aload 2
areturn
.limit locals 5
.limit stack 7
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/m;)Ljava/io/File;
aload 0
getfield com/teamspeak/ts3client/d/m/a Ljava/io/File;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a()V
new android/app/AlertDialog$Builder
dup
aload 0
getfield com/teamspeak/ts3client/d/m/b Landroid/content/Context;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 1
aload 1
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/app/Dialog;)V
aload 1
ldc "ident.file.error.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 1
ldc "ident.file.error.text1"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 1
bipush -2
ldc "button.cancel"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/d/n
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/n/<init>(Lcom/teamspeak/ts3client/d/m;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 1
invokevirtual android/app/AlertDialog/show()V
return
.limit locals 2
.limit stack 7
.end method

.method private b()[Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/d/m/a Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifeq L0
new com/teamspeak/ts3client/d/o
dup
aload 0
invokespecial com/teamspeak/ts3client/d/o/<init>(Lcom/teamspeak/ts3client/d/m;)V
astore 1
aload 0
getfield com/teamspeak/ts3client/d/m/a Ljava/io/File;
aload 1
invokevirtual java/io/File/list(Ljava/io/FilenameFilter;)[Ljava/lang/String;
areturn
L0:
iconst_0
anewarray java/lang/String
areturn
.limit locals 2
.limit stack 3
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
iconst_0
istore 2
aload 0
aload 1
invokespecial android/app/Dialog/onCreate(Landroid/os/Bundle;)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/d/m/getContext()Landroid/content/Context;
putfield com/teamspeak/ts3client/d/m/b Landroid/content/Context;
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/app/Dialog;)V
new android/widget/ScrollView
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/m/getContext()Landroid/content/Context;
invokespecial android/widget/ScrollView/<init>(Landroid/content/Context;)V
astore 3
invokestatic android/os/Environment/getExternalStorageState()Ljava/lang/String;
ldc "mounted"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
aload 0
ldc "ident.file.error.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/d/m/setTitle(Ljava/lang/CharSequence;)V
new android/widget/TextView
dup
aload 0
getfield com/teamspeak/ts3client/d/m/b Landroid/content/Context;
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 1
aload 1
ldc "ident.file.error.dialog.text1"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 3
aload 1
invokevirtual android/widget/ScrollView/addView(Landroid/view/View;)V
aload 0
aload 3
invokevirtual com/teamspeak/ts3client/d/m/setContentView(Landroid/view/View;)V
return
L0:
aload 0
ldc "ident.file.error.dialog.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/d/m/setTitle(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/m/a Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifeq L1
new com/teamspeak/ts3client/d/o
dup
aload 0
invokespecial com/teamspeak/ts3client/d/o/<init>(Lcom/teamspeak/ts3client/d/m;)V
astore 1
aload 0
getfield com/teamspeak/ts3client/d/m/a Ljava/io/File;
aload 1
invokevirtual java/io/File/list(Ljava/io/FilenameFilter;)[Ljava/lang/String;
astore 1
L2:
aload 1
arraylength
ifgt L3
aload 0
ldc "ident.file.error.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/d/m/setTitle(Ljava/lang/CharSequence;)V
new android/widget/TextView
dup
aload 0
getfield com/teamspeak/ts3client/d/m/b Landroid/content/Context;
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 1
aload 1
ldc "ident.file.error.dialog.text2"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/d/m/a Ljava/io/File;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 3
aload 1
invokevirtual android/widget/ScrollView/addView(Landroid/view/View;)V
aload 0
aload 3
invokevirtual com/teamspeak/ts3client/d/m/setContentView(Landroid/view/View;)V
return
L1:
iconst_0
anewarray java/lang/String
astore 1
goto L2
L3:
new android/widget/RadioGroup
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/m/getContext()Landroid/content/Context;
invokespecial android/widget/RadioGroup/<init>(Landroid/content/Context;)V
astore 4
L4:
iload 2
aload 1
arraylength
if_icmpge L5
new android/widget/RadioButton
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/m/getContext()Landroid/content/Context;
invokespecial android/widget/RadioButton/<init>(Landroid/content/Context;)V
astore 5
aload 5
aload 1
iload 2
aaload
invokevirtual android/widget/RadioButton/setText(Ljava/lang/CharSequence;)V
aload 5
iload 2
invokevirtual android/widget/RadioButton/setId(I)V
aload 4
aload 5
invokevirtual android/widget/RadioGroup/addView(Landroid/view/View;)V
iload 2
iconst_1
iadd
istore 2
goto L4
L5:
aload 3
aload 4
invokevirtual android/widget/ScrollView/addView(Landroid/view/View;)V
aload 3
iconst_1
invokevirtual android/widget/ScrollView/setId(I)V
aload 4
new com/teamspeak/ts3client/d/q
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/q/<init>(Lcom/teamspeak/ts3client/d/m;[Ljava/lang/String;)V
invokevirtual android/widget/RadioGroup/setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V
aload 0
aload 3
invokevirtual com/teamspeak/ts3client/d/m/setContentView(Landroid/view/View;)V
return
.limit locals 6
.limit stack 6
.end method
