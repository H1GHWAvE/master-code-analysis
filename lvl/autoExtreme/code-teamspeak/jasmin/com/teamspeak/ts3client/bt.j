.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/bt
.super java/lang/Object

.field public static final 'ActionBar' [I

.field public static final 'ActionBarLayout' [I

.field public static final 'ActionBarLayout_android_layout_gravity' I = 0


.field public static final 'ActionBar_background' I = 10


.field public static final 'ActionBar_backgroundSplit' I = 12


.field public static final 'ActionBar_backgroundStacked' I = 11


.field public static final 'ActionBar_contentInsetEnd' I = 21


.field public static final 'ActionBar_contentInsetLeft' I = 22


.field public static final 'ActionBar_contentInsetRight' I = 23


.field public static final 'ActionBar_contentInsetStart' I = 20


.field public static final 'ActionBar_customNavigationLayout' I = 13


.field public static final 'ActionBar_displayOptions' I = 3


.field public static final 'ActionBar_divider' I = 9


.field public static final 'ActionBar_elevation' I = 24


.field public static final 'ActionBar_height' I = 0


.field public static final 'ActionBar_hideOnContentScroll' I = 19


.field public static final 'ActionBar_homeAsUpIndicator' I = 26


.field public static final 'ActionBar_homeLayout' I = 14


.field public static final 'ActionBar_icon' I = 7


.field public static final 'ActionBar_indeterminateProgressStyle' I = 16


.field public static final 'ActionBar_itemPadding' I = 18


.field public static final 'ActionBar_logo' I = 8


.field public static final 'ActionBar_navigationMode' I = 2


.field public static final 'ActionBar_popupTheme' I = 25


.field public static final 'ActionBar_progressBarPadding' I = 17


.field public static final 'ActionBar_progressBarStyle' I = 15


.field public static final 'ActionBar_subtitle' I = 4


.field public static final 'ActionBar_subtitleTextStyle' I = 6


.field public static final 'ActionBar_title' I = 1


.field public static final 'ActionBar_titleTextStyle' I = 5


.field public static final 'ActionMenuItemView' [I

.field public static final 'ActionMenuItemView_android_minWidth' I = 0


.field public static final 'ActionMenuView' [I

.field public static final 'ActionMode' [I

.field public static final 'ActionMode_background' I = 3


.field public static final 'ActionMode_backgroundSplit' I = 4


.field public static final 'ActionMode_closeItemLayout' I = 5


.field public static final 'ActionMode_height' I = 0


.field public static final 'ActionMode_subtitleTextStyle' I = 2


.field public static final 'ActionMode_titleTextStyle' I = 1


.field public static final 'ActivityChooserView' [I

.field public static final 'ActivityChooserView_expandActivityOverflowButtonDrawable' I = 1


.field public static final 'ActivityChooserView_initialActivityCount' I = 0


.field public static final 'AlertDialog' [I

.field public static final 'AlertDialog_android_layout' I = 0


.field public static final 'AlertDialog_buttonPanelSideLayout' I = 1


.field public static final 'AlertDialog_listItemLayout' I = 5


.field public static final 'AlertDialog_listLayout' I = 2


.field public static final 'AlertDialog_multiChoiceItemLayout' I = 3


.field public static final 'AlertDialog_singleChoiceItemLayout' I = 4


.field public static final 'AppBarLayout' [I

.field public static final 'AppBarLayout_LayoutParams' [I

.field public static final 'AppBarLayout_LayoutParams_layout_scrollFlags' I = 0


.field public static final 'AppBarLayout_LayoutParams_layout_scrollInterpolator' I = 1


.field public static final 'AppBarLayout_android_background' I = 0


.field public static final 'AppBarLayout_elevation' I = 1


.field public static final 'AppBarLayout_expanded' I = 2


.field public static final 'AppCompatTextView' [I

.field public static final 'AppCompatTextView_android_textAppearance' I = 0


.field public static final 'AppCompatTextView_textAllCaps' I = 1


.field public static final 'CollapsingAppBarLayout_LayoutParams' [I

.field public static final 'CollapsingAppBarLayout_LayoutParams_layout_collapseMode' I = 0


.field public static final 'CollapsingAppBarLayout_LayoutParams_layout_collapseParallaxMultiplier' I = 1


.field public static final 'CollapsingToolbarLayout' [I

.field public static final 'CollapsingToolbarLayout_collapsedTitleGravity' I = 11


.field public static final 'CollapsingToolbarLayout_collapsedTitleTextAppearance' I = 7


.field public static final 'CollapsingToolbarLayout_contentScrim' I = 8


.field public static final 'CollapsingToolbarLayout_expandedTitleGravity' I = 12


.field public static final 'CollapsingToolbarLayout_expandedTitleMargin' I = 1


.field public static final 'CollapsingToolbarLayout_expandedTitleMarginBottom' I = 5


.field public static final 'CollapsingToolbarLayout_expandedTitleMarginEnd' I = 4


.field public static final 'CollapsingToolbarLayout_expandedTitleMarginStart' I = 2


.field public static final 'CollapsingToolbarLayout_expandedTitleMarginTop' I = 3


.field public static final 'CollapsingToolbarLayout_expandedTitleTextAppearance' I = 6


.field public static final 'CollapsingToolbarLayout_statusBarScrim' I = 9


.field public static final 'CollapsingToolbarLayout_title' I = 0


.field public static final 'CollapsingToolbarLayout_titleEnabled' I = 13


.field public static final 'CollapsingToolbarLayout_toolbarId' I = 10


.field public static final 'CompoundButton' [I

.field public static final 'CompoundButton_android_button' I = 0


.field public static final 'CompoundButton_buttonTint' I = 1


.field public static final 'CompoundButton_buttonTintMode' I = 2


.field public static final 'CoordinatorLayout' [I

.field public static final 'CoordinatorLayout_LayoutParams' [I

.field public static final 'CoordinatorLayout_LayoutParams_android_layout_gravity' I = 0


.field public static final 'CoordinatorLayout_LayoutParams_layout_anchor' I = 2


.field public static final 'CoordinatorLayout_LayoutParams_layout_anchorGravity' I = 4


.field public static final 'CoordinatorLayout_LayoutParams_layout_behavior' I = 1


.field public static final 'CoordinatorLayout_LayoutParams_layout_keyline' I = 3


.field public static final 'CoordinatorLayout_keylines' I = 0


.field public static final 'CoordinatorLayout_statusBarBackground' I = 1


.field public static final 'DrawerArrowToggle' [I

.field public static final 'DrawerArrowToggle_arrowHeadLength' I = 4


.field public static final 'DrawerArrowToggle_arrowShaftLength' I = 5


.field public static final 'DrawerArrowToggle_barLength' I = 6


.field public static final 'DrawerArrowToggle_color' I = 0


.field public static final 'DrawerArrowToggle_drawableSize' I = 2


.field public static final 'DrawerArrowToggle_gapBetweenBars' I = 3


.field public static final 'DrawerArrowToggle_spinBars' I = 1


.field public static final 'DrawerArrowToggle_thickness' I = 7


.field public static final 'FloatingActionButton' [I

.field public static final 'FloatingActionButton_android_background' I = 0


.field public static final 'FloatingActionButton_backgroundTint' I = 6


.field public static final 'FloatingActionButton_backgroundTintMode' I = 7


.field public static final 'FloatingActionButton_borderWidth' I = 5


.field public static final 'FloatingActionButton_elevation' I = 1


.field public static final 'FloatingActionButton_fabSize' I = 3


.field public static final 'FloatingActionButton_pressedTranslationZ' I = 4


.field public static final 'FloatingActionButton_rippleColor' I = 2


.field public static final 'LinearLayoutCompat' [I

.field public static final 'LinearLayoutCompat_Layout' [I

.field public static final 'LinearLayoutCompat_Layout_android_layout_gravity' I = 0


.field public static final 'LinearLayoutCompat_Layout_android_layout_height' I = 2


.field public static final 'LinearLayoutCompat_Layout_android_layout_weight' I = 3


.field public static final 'LinearLayoutCompat_Layout_android_layout_width' I = 1


.field public static final 'LinearLayoutCompat_android_baselineAligned' I = 2


.field public static final 'LinearLayoutCompat_android_baselineAlignedChildIndex' I = 3


.field public static final 'LinearLayoutCompat_android_gravity' I = 0


.field public static final 'LinearLayoutCompat_android_orientation' I = 1


.field public static final 'LinearLayoutCompat_android_weightSum' I = 4


.field public static final 'LinearLayoutCompat_divider' I = 5


.field public static final 'LinearLayoutCompat_dividerPadding' I = 8


.field public static final 'LinearLayoutCompat_measureWithLargestChild' I = 6


.field public static final 'LinearLayoutCompat_showDividers' I = 7


.field public static final 'ListPopupWindow' [I

.field public static final 'ListPopupWindow_android_dropDownHorizontalOffset' I = 0


.field public static final 'ListPopupWindow_android_dropDownVerticalOffset' I = 1


.field public static final 'MenuGroup' [I

.field public static final 'MenuGroup_android_checkableBehavior' I = 5


.field public static final 'MenuGroup_android_enabled' I = 0


.field public static final 'MenuGroup_android_id' I = 1


.field public static final 'MenuGroup_android_menuCategory' I = 3


.field public static final 'MenuGroup_android_orderInCategory' I = 4


.field public static final 'MenuGroup_android_visible' I = 2


.field public static final 'MenuItem' [I

.field public static final 'MenuItem_actionLayout' I = 14


.field public static final 'MenuItem_actionProviderClass' I = 16


.field public static final 'MenuItem_actionViewClass' I = 15


.field public static final 'MenuItem_android_alphabeticShortcut' I = 9


.field public static final 'MenuItem_android_checkable' I = 11


.field public static final 'MenuItem_android_checked' I = 3


.field public static final 'MenuItem_android_enabled' I = 1


.field public static final 'MenuItem_android_icon' I = 0


.field public static final 'MenuItem_android_id' I = 2


.field public static final 'MenuItem_android_menuCategory' I = 5


.field public static final 'MenuItem_android_numericShortcut' I = 10


.field public static final 'MenuItem_android_onClick' I = 12


.field public static final 'MenuItem_android_orderInCategory' I = 6


.field public static final 'MenuItem_android_title' I = 7


.field public static final 'MenuItem_android_titleCondensed' I = 8


.field public static final 'MenuItem_android_visible' I = 4


.field public static final 'MenuItem_showAsAction' I = 13


.field public static final 'MenuView' [I

.field public static final 'MenuView_android_headerBackground' I = 4


.field public static final 'MenuView_android_horizontalDivider' I = 2


.field public static final 'MenuView_android_itemBackground' I = 5


.field public static final 'MenuView_android_itemIconDisabledAlpha' I = 6


.field public static final 'MenuView_android_itemTextAppearance' I = 1


.field public static final 'MenuView_android_verticalDivider' I = 3


.field public static final 'MenuView_android_windowAnimationStyle' I = 0


.field public static final 'MenuView_preserveIconSpacing' I = 7


.field public static final 'NavigationView' [I

.field public static final 'NavigationView_android_background' I = 0


.field public static final 'NavigationView_android_fitsSystemWindows' I = 1


.field public static final 'NavigationView_android_maxWidth' I = 2


.field public static final 'NavigationView_elevation' I = 3


.field public static final 'NavigationView_headerLayout' I = 9


.field public static final 'NavigationView_itemBackground' I = 7


.field public static final 'NavigationView_itemIconTint' I = 5


.field public static final 'NavigationView_itemTextAppearance' I = 8


.field public static final 'NavigationView_itemTextColor' I = 6


.field public static final 'NavigationView_menu' I = 4


.field public static final 'PopupWindow' [I

.field public static final 'PopupWindowBackgroundState' [I

.field public static final 'PopupWindowBackgroundState_state_above_anchor' I = 0


.field public static final 'PopupWindow_android_popupBackground' I = 0


.field public static final 'PopupWindow_overlapAnchor' I = 1


.field public static final 'ScrimInsetsFrameLayout' [I

.field public static final 'ScrimInsetsFrameLayout_insetForeground' I = 0


.field public static final 'ScrollingViewBehavior_Params' [I

.field public static final 'ScrollingViewBehavior_Params_behavior_overlapTop' I = 0


.field public static final 'SearchView' [I

.field public static final 'SearchView_android_focusable' I = 0


.field public static final 'SearchView_android_imeOptions' I = 3


.field public static final 'SearchView_android_inputType' I = 2


.field public static final 'SearchView_android_maxWidth' I = 1


.field public static final 'SearchView_closeIcon' I = 8


.field public static final 'SearchView_commitIcon' I = 13


.field public static final 'SearchView_defaultQueryHint' I = 7


.field public static final 'SearchView_goIcon' I = 9


.field public static final 'SearchView_iconifiedByDefault' I = 5


.field public static final 'SearchView_layout' I = 4


.field public static final 'SearchView_queryBackground' I = 15


.field public static final 'SearchView_queryHint' I = 6


.field public static final 'SearchView_searchHintIcon' I = 11


.field public static final 'SearchView_searchIcon' I = 10


.field public static final 'SearchView_submitBackground' I = 16


.field public static final 'SearchView_suggestionRowLayout' I = 14


.field public static final 'SearchView_voiceIcon' I = 12


.field public static final 'SnackbarLayout' [I

.field public static final 'SnackbarLayout_android_maxWidth' I = 0


.field public static final 'SnackbarLayout_elevation' I = 1


.field public static final 'SnackbarLayout_maxActionInlineWidth' I = 2


.field public static final 'Spinner' [I

.field public static final 'Spinner_android_dropDownWidth' I = 2


.field public static final 'Spinner_android_popupBackground' I = 0


.field public static final 'Spinner_android_prompt' I = 1


.field public static final 'Spinner_popupTheme' I = 3


.field public static final 'SwitchCompat' [I

.field public static final 'SwitchCompat_android_textOff' I = 1


.field public static final 'SwitchCompat_android_textOn' I = 0


.field public static final 'SwitchCompat_android_thumb' I = 2


.field public static final 'SwitchCompat_showText' I = 9


.field public static final 'SwitchCompat_splitTrack' I = 8


.field public static final 'SwitchCompat_switchMinWidth' I = 6


.field public static final 'SwitchCompat_switchPadding' I = 7


.field public static final 'SwitchCompat_switchTextAppearance' I = 5


.field public static final 'SwitchCompat_thumbTextPadding' I = 4


.field public static final 'SwitchCompat_track' I = 3


.field public static final 'TabLayout' [I

.field public static final 'TabLayout_tabBackground' I = 3


.field public static final 'TabLayout_tabContentStart' I = 2


.field public static final 'TabLayout_tabGravity' I = 5


.field public static final 'TabLayout_tabIndicatorColor' I = 0


.field public static final 'TabLayout_tabIndicatorHeight' I = 1


.field public static final 'TabLayout_tabMaxWidth' I = 7


.field public static final 'TabLayout_tabMinWidth' I = 6


.field public static final 'TabLayout_tabMode' I = 4


.field public static final 'TabLayout_tabPadding' I = 15


.field public static final 'TabLayout_tabPaddingBottom' I = 14


.field public static final 'TabLayout_tabPaddingEnd' I = 13


.field public static final 'TabLayout_tabPaddingStart' I = 11


.field public static final 'TabLayout_tabPaddingTop' I = 12


.field public static final 'TabLayout_tabSelectedTextColor' I = 10


.field public static final 'TabLayout_tabTextAppearance' I = 8


.field public static final 'TabLayout_tabTextColor' I = 9


.field public static final 'TextAppearance' [I

.field public static final 'TextAppearance_android_textColor' I = 3


.field public static final 'TextAppearance_android_textSize' I = 0


.field public static final 'TextAppearance_android_textStyle' I = 2


.field public static final 'TextAppearance_android_typeface' I = 1


.field public static final 'TextAppearance_textAllCaps' I = 4


.field public static final 'TextInputLayout' [I

.field public static final 'TextInputLayout_android_hint' I = 1


.field public static final 'TextInputLayout_android_textColorHint' I = 0


.field public static final 'TextInputLayout_errorEnabled' I = 3


.field public static final 'TextInputLayout_errorTextAppearance' I = 4


.field public static final 'TextInputLayout_hintAnimationEnabled' I = 5


.field public static final 'TextInputLayout_hintTextAppearance' I = 2


.field public static final 'Theme' [I

.field public static final 'Theme_actionBarDivider' I = 23


.field public static final 'Theme_actionBarItemBackground' I = 24


.field public static final 'Theme_actionBarPopupTheme' I = 17


.field public static final 'Theme_actionBarSize' I = 22


.field public static final 'Theme_actionBarSplitStyle' I = 19


.field public static final 'Theme_actionBarStyle' I = 18


.field public static final 'Theme_actionBarTabBarStyle' I = 13


.field public static final 'Theme_actionBarTabStyle' I = 12


.field public static final 'Theme_actionBarTabTextStyle' I = 14


.field public static final 'Theme_actionBarTheme' I = 20


.field public static final 'Theme_actionBarWidgetTheme' I = 21


.field public static final 'Theme_actionButtonStyle' I = 49


.field public static final 'Theme_actionDropDownStyle' I = 45


.field public static final 'Theme_actionMenuTextAppearance' I = 25


.field public static final 'Theme_actionMenuTextColor' I = 26


.field public static final 'Theme_actionModeBackground' I = 29


.field public static final 'Theme_actionModeCloseButtonStyle' I = 28


.field public static final 'Theme_actionModeCloseDrawable' I = 31


.field public static final 'Theme_actionModeCopyDrawable' I = 33


.field public static final 'Theme_actionModeCutDrawable' I = 32


.field public static final 'Theme_actionModeFindDrawable' I = 37


.field public static final 'Theme_actionModePasteDrawable' I = 34


.field public static final 'Theme_actionModePopupWindowStyle' I = 39


.field public static final 'Theme_actionModeSelectAllDrawable' I = 35


.field public static final 'Theme_actionModeShareDrawable' I = 36


.field public static final 'Theme_actionModeSplitBackground' I = 30


.field public static final 'Theme_actionModeStyle' I = 27


.field public static final 'Theme_actionModeWebSearchDrawable' I = 38


.field public static final 'Theme_actionOverflowButtonStyle' I = 15


.field public static final 'Theme_actionOverflowMenuStyle' I = 16


.field public static final 'Theme_activityChooserViewStyle' I = 57


.field public static final 'Theme_alertDialogButtonGroupStyle' I = 91


.field public static final 'Theme_alertDialogCenterButtons' I = 92


.field public static final 'Theme_alertDialogStyle' I = 90


.field public static final 'Theme_alertDialogTheme' I = 93


.field public static final 'Theme_android_windowAnimationStyle' I = 1


.field public static final 'Theme_android_windowIsFloating' I = 0


.field public static final 'Theme_autoCompleteTextViewStyle' I = 98


.field public static final 'Theme_borderlessButtonStyle' I = 54


.field public static final 'Theme_buttonBarButtonStyle' I = 51


.field public static final 'Theme_buttonBarNegativeButtonStyle' I = 96


.field public static final 'Theme_buttonBarNeutralButtonStyle' I = 97


.field public static final 'Theme_buttonBarPositiveButtonStyle' I = 95


.field public static final 'Theme_buttonBarStyle' I = 50


.field public static final 'Theme_buttonStyle' I = 99


.field public static final 'Theme_buttonStyleSmall' I = 100


.field public static final 'Theme_checkboxStyle' I = 101


.field public static final 'Theme_checkedTextViewStyle' I = 102


.field public static final 'Theme_colorAccent' I = 83


.field public static final 'Theme_colorButtonNormal' I = 87


.field public static final 'Theme_colorControlActivated' I = 85


.field public static final 'Theme_colorControlHighlight' I = 86


.field public static final 'Theme_colorControlNormal' I = 84


.field public static final 'Theme_colorPrimary' I = 81


.field public static final 'Theme_colorPrimaryDark' I = 82


.field public static final 'Theme_colorSwitchThumbNormal' I = 88


.field public static final 'Theme_controlBackground' I = 89


.field public static final 'Theme_dialogPreferredPadding' I = 43


.field public static final 'Theme_dialogTheme' I = 42


.field public static final 'Theme_dividerHorizontal' I = 56


.field public static final 'Theme_dividerVertical' I = 55


.field public static final 'Theme_dropDownListViewStyle' I = 73


.field public static final 'Theme_dropdownListPreferredItemHeight' I = 46


.field public static final 'Theme_editTextBackground' I = 63


.field public static final 'Theme_editTextColor' I = 62


.field public static final 'Theme_editTextStyle' I = 103


.field public static final 'Theme_homeAsUpIndicator' I = 48


.field public static final 'Theme_listChoiceBackgroundIndicator' I = 80


.field public static final 'Theme_listDividerAlertDialog' I = 44


.field public static final 'Theme_listPopupWindowStyle' I = 74


.field public static final 'Theme_listPreferredItemHeight' I = 68


.field public static final 'Theme_listPreferredItemHeightLarge' I = 70


.field public static final 'Theme_listPreferredItemHeightSmall' I = 69


.field public static final 'Theme_listPreferredItemPaddingLeft' I = 71


.field public static final 'Theme_listPreferredItemPaddingRight' I = 72


.field public static final 'Theme_panelBackground' I = 77


.field public static final 'Theme_panelMenuListTheme' I = 79


.field public static final 'Theme_panelMenuListWidth' I = 78


.field public static final 'Theme_popupMenuStyle' I = 60


.field public static final 'Theme_popupWindowStyle' I = 61


.field public static final 'Theme_radioButtonStyle' I = 104


.field public static final 'Theme_ratingBarStyle' I = 105


.field public static final 'Theme_searchViewStyle' I = 67


.field public static final 'Theme_selectableItemBackground' I = 52


.field public static final 'Theme_selectableItemBackgroundBorderless' I = 53


.field public static final 'Theme_spinnerDropDownItemStyle' I = 47


.field public static final 'Theme_spinnerStyle' I = 106


.field public static final 'Theme_switchStyle' I = 107


.field public static final 'Theme_textAppearanceLargePopupMenu' I = 40


.field public static final 'Theme_textAppearanceListItem' I = 75


.field public static final 'Theme_textAppearanceListItemSmall' I = 76


.field public static final 'Theme_textAppearanceSearchResultSubtitle' I = 65


.field public static final 'Theme_textAppearanceSearchResultTitle' I = 64


.field public static final 'Theme_textAppearanceSmallPopupMenu' I = 41


.field public static final 'Theme_textColorAlertDialogListItem' I = 94


.field public static final 'Theme_textColorSearchUrl' I = 66


.field public static final 'Theme_toolbarNavigationButtonStyle' I = 59


.field public static final 'Theme_toolbarStyle' I = 58


.field public static final 'Theme_windowActionBar' I = 2


.field public static final 'Theme_windowActionBarOverlay' I = 4


.field public static final 'Theme_windowActionModeOverlay' I = 5


.field public static final 'Theme_windowFixedHeightMajor' I = 9


.field public static final 'Theme_windowFixedHeightMinor' I = 7


.field public static final 'Theme_windowFixedWidthMajor' I = 6


.field public static final 'Theme_windowFixedWidthMinor' I = 8


.field public static final 'Theme_windowMinWidthMajor' I = 10


.field public static final 'Theme_windowMinWidthMinor' I = 11


.field public static final 'Theme_windowNoTitle' I = 3


.field public static final 'Toolbar' [I

.field public static final 'Toolbar_android_gravity' I = 0


.field public static final 'Toolbar_android_minHeight' I = 1


.field public static final 'Toolbar_collapseContentDescription' I = 19


.field public static final 'Toolbar_collapseIcon' I = 18


.field public static final 'Toolbar_contentInsetEnd' I = 6


.field public static final 'Toolbar_contentInsetLeft' I = 7


.field public static final 'Toolbar_contentInsetRight' I = 8


.field public static final 'Toolbar_contentInsetStart' I = 5


.field public static final 'Toolbar_logo' I = 4


.field public static final 'Toolbar_logoDescription' I = 22


.field public static final 'Toolbar_maxButtonHeight' I = 17


.field public static final 'Toolbar_navigationContentDescription' I = 21


.field public static final 'Toolbar_navigationIcon' I = 20


.field public static final 'Toolbar_popupTheme' I = 9


.field public static final 'Toolbar_subtitle' I = 3


.field public static final 'Toolbar_subtitleTextAppearance' I = 11


.field public static final 'Toolbar_subtitleTextColor' I = 24


.field public static final 'Toolbar_title' I = 2


.field public static final 'Toolbar_titleMarginBottom' I = 16


.field public static final 'Toolbar_titleMarginEnd' I = 14


.field public static final 'Toolbar_titleMarginStart' I = 13


.field public static final 'Toolbar_titleMarginTop' I = 15


.field public static final 'Toolbar_titleMargins' I = 12


.field public static final 'Toolbar_titleTextAppearance' I = 10


.field public static final 'Toolbar_titleTextColor' I = 23


.field public static final 'View' [I

.field public static final 'ViewBackgroundHelper' [I

.field public static final 'ViewBackgroundHelper_android_background' I = 0


.field public static final 'ViewBackgroundHelper_backgroundTint' I = 1


.field public static final 'ViewBackgroundHelper_backgroundTintMode' I = 2


.field public static final 'ViewStubCompat' [I

.field public static final 'ViewStubCompat_android_id' I = 0


.field public static final 'ViewStubCompat_android_inflatedId' I = 2


.field public static final 'ViewStubCompat_android_layout' I = 1


.field public static final 'View_android_focusable' I = 1


.field public static final 'View_android_theme' I = 0


.field public static final 'View_paddingEnd' I = 3


.field public static final 'View_paddingStart' I = 2


.field public static final 'View_theme' I = 4


.method static <clinit>()V
bipush 27
newarray int
dup
iconst_0
ldc_w 2130771969
iastore
dup
iconst_1
ldc_w 2130771971
iastore
dup
iconst_2
ldc_w 2130771972
iastore
dup
iconst_3
ldc_w 2130771973
iastore
dup
iconst_4
ldc_w 2130771974
iastore
dup
iconst_5
ldc_w 2130771975
iastore
dup
bipush 6
ldc_w 2130771976
iastore
dup
bipush 7
ldc_w 2130771977
iastore
dup
bipush 8
ldc_w 2130771978
iastore
dup
bipush 9
ldc_w 2130771979
iastore
dup
bipush 10
ldc_w 2130771980
iastore
dup
bipush 11
ldc_w 2130771981
iastore
dup
bipush 12
ldc_w 2130771982
iastore
dup
bipush 13
ldc_w 2130771983
iastore
dup
bipush 14
ldc_w 2130771984
iastore
dup
bipush 15
ldc_w 2130771985
iastore
dup
bipush 16
ldc_w 2130771986
iastore
dup
bipush 17
ldc_w 2130771987
iastore
dup
bipush 18
ldc_w 2130771988
iastore
dup
bipush 19
ldc_w 2130771989
iastore
dup
bipush 20
ldc_w 2130771990
iastore
dup
bipush 21
ldc_w 2130771991
iastore
dup
bipush 22
ldc_w 2130771992
iastore
dup
bipush 23
ldc_w 2130771993
iastore
dup
bipush 24
ldc_w 2130771994
iastore
dup
bipush 25
ldc_w 2130771995
iastore
dup
bipush 26
ldc_w 2130772148
iastore
putstatic com/teamspeak/ts3client/bt/ActionBar [I
iconst_1
newarray int
dup
iconst_0
ldc_w 16842931
iastore
putstatic com/teamspeak/ts3client/bt/ActionBarLayout [I
iconst_1
newarray int
dup
iconst_0
ldc_w 16843071
iastore
putstatic com/teamspeak/ts3client/bt/ActionMenuItemView [I
iconst_0
newarray int
putstatic com/teamspeak/ts3client/bt/ActionMenuView [I
bipush 6
newarray int
dup
iconst_0
ldc_w 2130771969
iastore
dup
iconst_1
ldc_w 2130771975
iastore
dup
iconst_2
ldc_w 2130771976
iastore
dup
iconst_3
ldc_w 2130771980
iastore
dup
iconst_4
ldc_w 2130771982
iastore
dup
iconst_5
ldc_w 2130771996
iastore
putstatic com/teamspeak/ts3client/bt/ActionMode [I
iconst_2
newarray int
dup
iconst_0
ldc_w 2130771997
iastore
dup
iconst_1
ldc_w 2130771998
iastore
putstatic com/teamspeak/ts3client/bt/ActivityChooserView [I
bipush 6
newarray int
dup
iconst_0
ldc_w 16842994
iastore
dup
iconst_1
ldc_w 2130771999
iastore
dup
iconst_2
ldc_w 2130772000
iastore
dup
iconst_3
ldc_w 2130772001
iastore
dup
iconst_4
ldc_w 2130772002
iastore
dup
iconst_5
ldc_w 2130772003
iastore
putstatic com/teamspeak/ts3client/bt/AlertDialog [I
iconst_3
newarray int
dup
iconst_0
ldc_w 16842964
iastore
dup
iconst_1
ldc_w 2130771994
iastore
dup
iconst_2
ldc_w 2130772004
iastore
putstatic com/teamspeak/ts3client/bt/AppBarLayout [I
iconst_2
newarray int
dup
iconst_0
ldc_w 2130772005
iastore
dup
iconst_1
ldc_w 2130772006
iastore
putstatic com/teamspeak/ts3client/bt/AppBarLayout_LayoutParams [I
iconst_2
newarray int
dup
iconst_0
ldc_w 16842804
iastore
dup
iconst_1
ldc_w 2130772007
iastore
putstatic com/teamspeak/ts3client/bt/AppCompatTextView [I
iconst_2
newarray int
dup
iconst_0
ldc_w 2130772008
iastore
dup
iconst_1
ldc_w 2130772009
iastore
putstatic com/teamspeak/ts3client/bt/CollapsingAppBarLayout_LayoutParams [I
bipush 14
newarray int
dup
iconst_0
ldc_w 2130771971
iastore
dup
iconst_1
ldc_w 2130772010
iastore
dup
iconst_2
ldc_w 2130772011
iastore
dup
iconst_3
ldc_w 2130772012
iastore
dup
iconst_4
ldc_w 2130772013
iastore
dup
iconst_5
ldc_w 2130772014
iastore
dup
bipush 6
ldc_w 2130772015
iastore
dup
bipush 7
ldc_w 2130772016
iastore
dup
bipush 8
ldc_w 2130772017
iastore
dup
bipush 9
ldc_w 2130772018
iastore
dup
bipush 10
ldc_w 2130772019
iastore
dup
bipush 11
ldc_w 2130772020
iastore
dup
bipush 12
ldc_w 2130772021
iastore
dup
bipush 13
ldc_w 2130772022
iastore
putstatic com/teamspeak/ts3client/bt/CollapsingToolbarLayout [I
iconst_3
newarray int
dup
iconst_0
ldc_w 16843015
iastore
dup
iconst_1
ldc_w 2130772023
iastore
dup
iconst_2
ldc_w 2130772024
iastore
putstatic com/teamspeak/ts3client/bt/CompoundButton [I
iconst_2
newarray int
dup
iconst_0
ldc_w 2130772025
iastore
dup
iconst_1
ldc_w 2130772026
iastore
putstatic com/teamspeak/ts3client/bt/CoordinatorLayout [I
iconst_5
newarray int
dup
iconst_0
ldc_w 16842931
iastore
dup
iconst_1
ldc_w 2130772027
iastore
dup
iconst_2
ldc_w 2130772028
iastore
dup
iconst_3
ldc_w 2130772029
iastore
dup
iconst_4
ldc_w 2130772030
iastore
putstatic com/teamspeak/ts3client/bt/CoordinatorLayout_LayoutParams [I
bipush 8
newarray int
dup
iconst_0
ldc_w 2130772031
iastore
dup
iconst_1
ldc_w 2130772032
iastore
dup
iconst_2
ldc_w 2130772033
iastore
dup
iconst_3
ldc_w 2130772034
iastore
dup
iconst_4
ldc_w 2130772035
iastore
dup
iconst_5
ldc_w 2130772036
iastore
dup
bipush 6
ldc_w 2130772037
iastore
dup
bipush 7
ldc_w 2130772038
iastore
putstatic com/teamspeak/ts3client/bt/DrawerArrowToggle [I
bipush 8
newarray int
dup
iconst_0
ldc_w 16842964
iastore
dup
iconst_1
ldc_w 2130771994
iastore
dup
iconst_2
ldc_w 2130772039
iastore
dup
iconst_3
ldc_w 2130772040
iastore
dup
iconst_4
ldc_w 2130772041
iastore
dup
iconst_5
ldc_w 2130772042
iastore
dup
bipush 6
ldc_w 2130772226
iastore
dup
bipush 7
ldc_w 2130772227
iastore
putstatic com/teamspeak/ts3client/bt/FloatingActionButton [I
bipush 9
newarray int
dup
iconst_0
ldc_w 16842927
iastore
dup
iconst_1
ldc_w 16842948
iastore
dup
iconst_2
ldc_w 16843046
iastore
dup
iconst_3
ldc_w 16843047
iastore
dup
iconst_4
ldc_w 16843048
iastore
dup
iconst_5
ldc_w 2130771979
iastore
dup
bipush 6
ldc_w 2130772043
iastore
dup
bipush 7
ldc_w 2130772044
iastore
dup
bipush 8
ldc_w 2130772045
iastore
putstatic com/teamspeak/ts3client/bt/LinearLayoutCompat [I
iconst_4
newarray int
dup
iconst_0
ldc_w 16842931
iastore
dup
iconst_1
ldc_w 16842996
iastore
dup
iconst_2
ldc_w 16842997
iastore
dup
iconst_3
ldc_w 16843137
iastore
putstatic com/teamspeak/ts3client/bt/LinearLayoutCompat_Layout [I
iconst_2
newarray int
dup
iconst_0
ldc_w 16843436
iastore
dup
iconst_1
ldc_w 16843437
iastore
putstatic com/teamspeak/ts3client/bt/ListPopupWindow [I
bipush 6
newarray int
dup
iconst_0
ldc_w 16842766
iastore
dup
iconst_1
ldc_w 16842960
iastore
dup
iconst_2
ldc_w 16843156
iastore
dup
iconst_3
ldc_w 16843230
iastore
dup
iconst_4
ldc_w 16843231
iastore
dup
iconst_5
ldc_w 16843232
iastore
putstatic com/teamspeak/ts3client/bt/MenuGroup [I
bipush 17
newarray int
dup
iconst_0
ldc_w 16842754
iastore
dup
iconst_1
ldc_w 16842766
iastore
dup
iconst_2
ldc_w 16842960
iastore
dup
iconst_3
ldc_w 16843014
iastore
dup
iconst_4
ldc_w 16843156
iastore
dup
iconst_5
ldc_w 16843230
iastore
dup
bipush 6
ldc_w 16843231
iastore
dup
bipush 7
ldc_w 16843233
iastore
dup
bipush 8
ldc_w 16843234
iastore
dup
bipush 9
ldc_w 16843235
iastore
dup
bipush 10
ldc_w 16843236
iastore
dup
bipush 11
ldc_w 16843237
iastore
dup
bipush 12
ldc_w 16843375
iastore
dup
bipush 13
ldc_w 2130772046
iastore
dup
bipush 14
ldc_w 2130772047
iastore
dup
bipush 15
ldc_w 2130772048
iastore
dup
bipush 16
ldc_w 2130772049
iastore
putstatic com/teamspeak/ts3client/bt/MenuItem [I
bipush 8
newarray int
dup
iconst_0
ldc_w 16842926
iastore
dup
iconst_1
ldc_w 16843052
iastore
dup
iconst_2
ldc_w 16843053
iastore
dup
iconst_3
ldc_w 16843054
iastore
dup
iconst_4
ldc_w 16843055
iastore
dup
iconst_5
ldc_w 16843056
iastore
dup
bipush 6
ldc_w 16843057
iastore
dup
bipush 7
ldc_w 2130772050
iastore
putstatic com/teamspeak/ts3client/bt/MenuView [I
bipush 10
newarray int
dup
iconst_0
ldc_w 16842964
iastore
dup
iconst_1
ldc_w 16842973
iastore
dup
iconst_2
ldc_w 16843039
iastore
dup
iconst_3
ldc_w 2130771994
iastore
dup
iconst_4
ldc_w 2130772051
iastore
dup
iconst_5
ldc_w 2130772052
iastore
dup
bipush 6
ldc_w 2130772053
iastore
dup
bipush 7
ldc_w 2130772054
iastore
dup
bipush 8
ldc_w 2130772055
iastore
dup
bipush 9
ldc_w 2130772056
iastore
putstatic com/teamspeak/ts3client/bt/NavigationView [I
iconst_2
newarray int
dup
iconst_0
ldc_w 16843126
iastore
dup
iconst_1
ldc_w 2130772057
iastore
putstatic com/teamspeak/ts3client/bt/PopupWindow [I
iconst_1
newarray int
dup
iconst_0
ldc_w 2130772058
iastore
putstatic com/teamspeak/ts3client/bt/PopupWindowBackgroundState [I
iconst_1
newarray int
dup
iconst_0
ldc_w 2130772059
iastore
putstatic com/teamspeak/ts3client/bt/ScrimInsetsFrameLayout [I
iconst_1
newarray int
dup
iconst_0
ldc_w 2130772060
iastore
putstatic com/teamspeak/ts3client/bt/ScrollingViewBehavior_Params [I
bipush 17
newarray int
dup
iconst_0
ldc_w 16842970
iastore
dup
iconst_1
ldc_w 16843039
iastore
dup
iconst_2
ldc_w 16843296
iastore
dup
iconst_3
ldc_w 16843364
iastore
dup
iconst_4
ldc_w 2130772061
iastore
dup
iconst_5
ldc_w 2130772062
iastore
dup
bipush 6
ldc_w 2130772063
iastore
dup
bipush 7
ldc_w 2130772064
iastore
dup
bipush 8
ldc_w 2130772065
iastore
dup
bipush 9
ldc_w 2130772066
iastore
dup
bipush 10
ldc_w 2130772067
iastore
dup
bipush 11
ldc_w 2130772068
iastore
dup
bipush 12
ldc_w 2130772069
iastore
dup
bipush 13
ldc_w 2130772070
iastore
dup
bipush 14
ldc_w 2130772071
iastore
dup
bipush 15
ldc_w 2130772072
iastore
dup
bipush 16
ldc_w 2130772073
iastore
putstatic com/teamspeak/ts3client/bt/SearchView [I
iconst_3
newarray int
dup
iconst_0
ldc_w 16843039
iastore
dup
iconst_1
ldc_w 2130771994
iastore
dup
iconst_2
ldc_w 2130772074
iastore
putstatic com/teamspeak/ts3client/bt/SnackbarLayout [I
iconst_4
newarray int
dup
iconst_0
ldc_w 16843126
iastore
dup
iconst_1
ldc_w 16843131
iastore
dup
iconst_2
ldc_w 16843362
iastore
dup
iconst_3
ldc_w 2130771995
iastore
putstatic com/teamspeak/ts3client/bt/Spinner [I
bipush 10
newarray int
dup
iconst_0
ldc_w 16843044
iastore
dup
iconst_1
ldc_w 16843045
iastore
dup
iconst_2
ldc_w 16843074
iastore
dup
iconst_3
ldc_w 2130772075
iastore
dup
iconst_4
ldc_w 2130772076
iastore
dup
iconst_5
ldc_w 2130772077
iastore
dup
bipush 6
ldc_w 2130772078
iastore
dup
bipush 7
ldc_w 2130772079
iastore
dup
bipush 8
ldc_w 2130772080
iastore
dup
bipush 9
ldc_w 2130772081
iastore
putstatic com/teamspeak/ts3client/bt/SwitchCompat [I
bipush 16
newarray int
dup
iconst_0
ldc_w 2130772082
iastore
dup
iconst_1
ldc_w 2130772083
iastore
dup
iconst_2
ldc_w 2130772084
iastore
dup
iconst_3
ldc_w 2130772085
iastore
dup
iconst_4
ldc_w 2130772086
iastore
dup
iconst_5
ldc_w 2130772087
iastore
dup
bipush 6
ldc_w 2130772088
iastore
dup
bipush 7
ldc_w 2130772089
iastore
dup
bipush 8
ldc_w 2130772090
iastore
dup
bipush 9
ldc_w 2130772091
iastore
dup
bipush 10
ldc_w 2130772092
iastore
dup
bipush 11
ldc_w 2130772093
iastore
dup
bipush 12
ldc_w 2130772094
iastore
dup
bipush 13
ldc_w 2130772095
iastore
dup
bipush 14
ldc_w 2130772096
iastore
dup
bipush 15
ldc_w 2130772097
iastore
putstatic com/teamspeak/ts3client/bt/TabLayout [I
iconst_5
newarray int
dup
iconst_0
ldc_w 16842901
iastore
dup
iconst_1
ldc_w 16842902
iastore
dup
iconst_2
ldc_w 16842903
iastore
dup
iconst_3
ldc_w 16842904
iastore
dup
iconst_4
ldc_w 2130772007
iastore
putstatic com/teamspeak/ts3client/bt/TextAppearance [I
bipush 6
newarray int
dup
iconst_0
ldc_w 16842906
iastore
dup
iconst_1
ldc_w 16843088
iastore
dup
iconst_2
ldc_w 2130772098
iastore
dup
iconst_3
ldc_w 2130772099
iastore
dup
iconst_4
ldc_w 2130772100
iastore
dup
iconst_5
ldc_w 2130772101
iastore
putstatic com/teamspeak/ts3client/bt/TextInputLayout [I
bipush 108
newarray int
dup
iconst_0
ldc_w 16842839
iastore
dup
iconst_1
ldc_w 16842926
iastore
dup
iconst_2
ldc_w 2130772102
iastore
dup
iconst_3
ldc_w 2130772103
iastore
dup
iconst_4
ldc_w 2130772104
iastore
dup
iconst_5
ldc_w 2130772105
iastore
dup
bipush 6
ldc_w 2130772106
iastore
dup
bipush 7
ldc_w 2130772107
iastore
dup
bipush 8
ldc_w 2130772108
iastore
dup
bipush 9
ldc_w 2130772109
iastore
dup
bipush 10
ldc_w 2130772110
iastore
dup
bipush 11
ldc_w 2130772111
iastore
dup
bipush 12
ldc_w 2130772112
iastore
dup
bipush 13
ldc_w 2130772113
iastore
dup
bipush 14
ldc_w 2130772114
iastore
dup
bipush 15
ldc_w 2130772115
iastore
dup
bipush 16
ldc_w 2130772116
iastore
dup
bipush 17
ldc_w 2130772117
iastore
dup
bipush 18
ldc_w 2130772118
iastore
dup
bipush 19
ldc_w 2130772119
iastore
dup
bipush 20
ldc_w 2130772120
iastore
dup
bipush 21
ldc_w 2130772121
iastore
dup
bipush 22
ldc_w 2130772122
iastore
dup
bipush 23
ldc_w 2130772123
iastore
dup
bipush 24
ldc_w 2130772124
iastore
dup
bipush 25
ldc_w 2130772125
iastore
dup
bipush 26
ldc_w 2130772126
iastore
dup
bipush 27
ldc_w 2130772127
iastore
dup
bipush 28
ldc_w 2130772128
iastore
dup
bipush 29
ldc_w 2130772129
iastore
dup
bipush 30
ldc_w 2130772130
iastore
dup
bipush 31
ldc_w 2130772131
iastore
dup
bipush 32
ldc_w 2130772132
iastore
dup
bipush 33
ldc_w 2130772133
iastore
dup
bipush 34
ldc_w 2130772134
iastore
dup
bipush 35
ldc_w 2130772135
iastore
dup
bipush 36
ldc_w 2130772136
iastore
dup
bipush 37
ldc_w 2130772137
iastore
dup
bipush 38
ldc_w 2130772138
iastore
dup
bipush 39
ldc_w 2130772139
iastore
dup
bipush 40
ldc_w 2130772140
iastore
dup
bipush 41
ldc_w 2130772141
iastore
dup
bipush 42
ldc_w 2130772142
iastore
dup
bipush 43
ldc_w 2130772143
iastore
dup
bipush 44
ldc_w 2130772144
iastore
dup
bipush 45
ldc_w 2130772145
iastore
dup
bipush 46
ldc_w 2130772146
iastore
dup
bipush 47
ldc_w 2130772147
iastore
dup
bipush 48
ldc_w 2130772148
iastore
dup
bipush 49
ldc_w 2130772149
iastore
dup
bipush 50
ldc_w 2130772150
iastore
dup
bipush 51
ldc_w 2130772151
iastore
dup
bipush 52
ldc_w 2130772152
iastore
dup
bipush 53
ldc_w 2130772153
iastore
dup
bipush 54
ldc_w 2130772154
iastore
dup
bipush 55
ldc_w 2130772155
iastore
dup
bipush 56
ldc_w 2130772156
iastore
dup
bipush 57
ldc_w 2130772157
iastore
dup
bipush 58
ldc_w 2130772158
iastore
dup
bipush 59
ldc_w 2130772159
iastore
dup
bipush 60
ldc_w 2130772160
iastore
dup
bipush 61
ldc_w 2130772161
iastore
dup
bipush 62
ldc_w 2130772162
iastore
dup
bipush 63
ldc_w 2130772163
iastore
dup
bipush 64
ldc_w 2130772164
iastore
dup
bipush 65
ldc_w 2130772165
iastore
dup
bipush 66
ldc_w 2130772166
iastore
dup
bipush 67
ldc_w 2130772167
iastore
dup
bipush 68
ldc_w 2130772168
iastore
dup
bipush 69
ldc_w 2130772169
iastore
dup
bipush 70
ldc_w 2130772170
iastore
dup
bipush 71
ldc_w 2130772171
iastore
dup
bipush 72
ldc_w 2130772172
iastore
dup
bipush 73
ldc_w 2130772173
iastore
dup
bipush 74
ldc_w 2130772174
iastore
dup
bipush 75
ldc_w 2130772175
iastore
dup
bipush 76
ldc_w 2130772176
iastore
dup
bipush 77
ldc_w 2130772177
iastore
dup
bipush 78
ldc_w 2130772178
iastore
dup
bipush 79
ldc_w 2130772179
iastore
dup
bipush 80
ldc_w 2130772180
iastore
dup
bipush 81
ldc_w 2130772181
iastore
dup
bipush 82
ldc_w 2130772182
iastore
dup
bipush 83
ldc_w 2130772183
iastore
dup
bipush 84
ldc_w 2130772184
iastore
dup
bipush 85
ldc_w 2130772185
iastore
dup
bipush 86
ldc_w 2130772186
iastore
dup
bipush 87
ldc_w 2130772187
iastore
dup
bipush 88
ldc_w 2130772188
iastore
dup
bipush 89
ldc_w 2130772189
iastore
dup
bipush 90
ldc_w 2130772190
iastore
dup
bipush 91
ldc_w 2130772191
iastore
dup
bipush 92
ldc_w 2130772192
iastore
dup
bipush 93
ldc_w 2130772193
iastore
dup
bipush 94
ldc_w 2130772194
iastore
dup
bipush 95
ldc_w 2130772195
iastore
dup
bipush 96
ldc_w 2130772196
iastore
dup
bipush 97
ldc_w 2130772197
iastore
dup
bipush 98
ldc_w 2130772198
iastore
dup
bipush 99
ldc_w 2130772199
iastore
dup
bipush 100
ldc_w 2130772200
iastore
dup
bipush 101
ldc_w 2130772201
iastore
dup
bipush 102
ldc_w 2130772202
iastore
dup
bipush 103
ldc_w 2130772203
iastore
dup
bipush 104
ldc_w 2130772204
iastore
dup
bipush 105
ldc_w 2130772205
iastore
dup
bipush 106
ldc_w 2130772206
iastore
dup
bipush 107
ldc_w 2130772207
iastore
putstatic com/teamspeak/ts3client/bt/Theme [I
bipush 25
newarray int
dup
iconst_0
ldc_w 16842927
iastore
dup
iconst_1
ldc_w 16843072
iastore
dup
iconst_2
ldc_w 2130771971
iastore
dup
iconst_3
ldc_w 2130771974
iastore
dup
iconst_4
ldc_w 2130771978
iastore
dup
iconst_5
ldc_w 2130771990
iastore
dup
bipush 6
ldc_w 2130771991
iastore
dup
bipush 7
ldc_w 2130771992
iastore
dup
bipush 8
ldc_w 2130771993
iastore
dup
bipush 9
ldc_w 2130771995
iastore
dup
bipush 10
ldc_w 2130772208
iastore
dup
bipush 11
ldc_w 2130772209
iastore
dup
bipush 12
ldc_w 2130772210
iastore
dup
bipush 13
ldc_w 2130772211
iastore
dup
bipush 14
ldc_w 2130772212
iastore
dup
bipush 15
ldc_w 2130772213
iastore
dup
bipush 16
ldc_w 2130772214
iastore
dup
bipush 17
ldc_w 2130772215
iastore
dup
bipush 18
ldc_w 2130772216
iastore
dup
bipush 19
ldc_w 2130772217
iastore
dup
bipush 20
ldc_w 2130772218
iastore
dup
bipush 21
ldc_w 2130772219
iastore
dup
bipush 22
ldc_w 2130772220
iastore
dup
bipush 23
ldc_w 2130772221
iastore
dup
bipush 24
ldc_w 2130772222
iastore
putstatic com/teamspeak/ts3client/bt/Toolbar [I
iconst_5
newarray int
dup
iconst_0
ldc_w 16842752
iastore
dup
iconst_1
ldc_w 16842970
iastore
dup
iconst_2
ldc_w 2130772223
iastore
dup
iconst_3
ldc_w 2130772224
iastore
dup
iconst_4
ldc_w 2130772225
iastore
putstatic com/teamspeak/ts3client/bt/View [I
iconst_3
newarray int
dup
iconst_0
ldc_w 16842964
iastore
dup
iconst_1
ldc_w 2130772226
iastore
dup
iconst_2
ldc_w 2130772227
iastore
putstatic com/teamspeak/ts3client/bt/ViewBackgroundHelper [I
iconst_3
newarray int
dup
iconst_0
ldc_w 16842960
iastore
dup
iconst_1
ldc_w 16842994
iastore
dup
iconst_2
ldc_w 16842995
iastore
putstatic com/teamspeak/ts3client/bt/ViewStubCompat [I
return
.limit locals 0
.limit stack 4
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method
