.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/a/a
.super android/support/v4/app/ax

.field private static 'au' Lcom/teamspeak/ts3client/a/a;

.field private 'aA' I

.field private 'aB' Landroid/widget/Spinner;

.field private 'aC' Landroid/widget/Spinner;

.field private 'aD' Z

.field private 'aE' Landroid/widget/Spinner;

.field private 'aF' Landroid/widget/Spinner;

.field private 'aG' Landroid/widget/Button;

.field private 'aH' Landroid/widget/Button;

.field private 'aI' Z

.field private 'aJ' Landroid/widget/Button;

.field private 'aK' Landroid/widget/TextView;

.field private 'aL' Landroid/widget/TextView;

.field private 'aM' Landroid/widget/TextView;

.field private 'aN' Landroid/widget/TextView;

.field private 'aO' Z

.field private 'aP' Landroid/content/BroadcastReceiver;

.field private 'aQ' Landroid/widget/TextView;

.field public 'at' Z

.field private 'av' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'aw' Lcom/teamspeak/ts3client/jni/Ts3Jni;

.field private 'ax' Lcom/teamspeak/ts3client/a/k;

.field private 'ay' J

.field private 'az' I

.method private <init>()V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
new com/teamspeak/ts3client/a/b
dup
aload 0
invokespecial com/teamspeak/ts3client/a/b/<init>(Lcom/teamspeak/ts3client/a/a;)V
putfield com/teamspeak/ts3client/a/a/aP Landroid/content/BroadcastReceiver;
return
.limit locals 1
.limit stack 4
.end method

.method private A()V
aload 0
getfield com/teamspeak/ts3client/a/a/aG Landroid/widget/Button;
ldc "audiosettings.button.start"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aH Landroid/widget/Button;
ldc "audiosettings.button.stop"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aJ Landroid/widget/Button;
ldc "button.save"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aN Landroid/widget/TextView;
ldc "audiosettings.text.play"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aM Landroid/widget/TextView;
ldc "audiosettings.text.rec"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aK Landroid/widget/TextView;
ldc "audiosettings.text.playbackstream"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aL Landroid/widget/TextView;
ldc "audiosettings.text.recordstream"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aQ Landroid/widget/TextView;
ldc "audiosettings.error"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 1
.limit stack 2
.end method

.method private B()V
aload 0
getfield com/teamspeak/ts3client/a/a/aD Z
ifne L0
L1:
return
L0:
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/a/aD Z
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L2
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
ifnonnull L3
aload 0
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
putfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
L3:
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closeCaptureDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closePlaybackDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
iconst_0
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setLocalTestMode(JI)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_destroyServerConnectionHandler(J)I
pop
aload 0
aconst_null
putfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
aload 0
aconst_null
putfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
return
L2:
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
ifnull L1
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
ifnull L1
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/e()V
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/f()V
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/b()V
aload 0
aconst_null
putfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closeCaptureDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closePlaybackDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_destroyServerConnectionHandler(J)I
pop
aload 0
aconst_null
putfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
return
.limit locals 1
.limit stack 4
.end method

.method private C()V
aload 0
aload 0
getfield com/teamspeak/ts3client/a/a/aB Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aC Landroid/widget/Spinner;
invokespecial com/teamspeak/ts3client/a/a/a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "recordStream"
aload 0
getfield com/teamspeak/ts3client/a/a/aA I
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "playbackStream"
aload 0
getfield com/teamspeak/ts3client/a/a/az I
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "sampleRec"
aload 0
getfield com/teamspeak/ts3client/a/a/aE Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getSelectedItem()Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "samplePlay"
aload 0
getfield com/teamspeak/ts3client/a/a/aF Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getSelectedItem()Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
invokevirtual com/teamspeak/ts3client/a/a/b()V
return
.limit locals 1
.limit stack 3
.end method

.method private D()V
aload 0
invokevirtual com/teamspeak/ts3client/a/a/b()V
return
.limit locals 1
.limit stack 1
.end method

.method private E()Z
aload 0
getfield com/teamspeak/ts3client/a/a/at Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/a;J)J
aload 0
lload 1
putfield com/teamspeak/ts3client/a/a/ay J
lload 1
lreturn
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/a;Lcom/teamspeak/ts3client/jni/Ts3Jni;)Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
aload 1
putfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/a/az I
aload 1
invokevirtual android/widget/Spinner/getSelectedItemId()J
lconst_0
lcmp
ifne L0
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/a/az I
L0:
aload 1
invokevirtual android/widget/Spinner/getSelectedItemId()J
lconst_1
lcmp
ifne L1
aload 0
iconst_3
putfield com/teamspeak/ts3client/a/a/az I
L1:
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/a/aA I
aload 2
invokevirtual android/widget/Spinner/getSelectedItemId()J
lconst_0
lcmp
ifne L2
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/a/aA I
L2:
aload 2
invokevirtual android/widget/Spinner/getSelectedItemId()J
lconst_1
lcmp
ifne L3
aload 0
iconst_1
putfield com/teamspeak/ts3client/a/a/aA I
L3:
aload 2
invokevirtual android/widget/Spinner/getSelectedItemId()J
ldc2_w 2L
lcmp
ifne L4
aload 0
bipush 7
putfield com/teamspeak/ts3client/a/a/aA I
L4:
return
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/a;Landroid/widget/Spinner;Landroid/widget/Spinner;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aD Z
ifeq L0
aload 0
aload 0
getfield com/teamspeak/ts3client/a/a/aB Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aC Landroid/widget/Spinner;
invokespecial com/teamspeak/ts3client/a/a/a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closeCaptureDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closePlaybackDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 2
invokevirtual android/widget/Spinner/getSelectedItem()Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
aload 1
invokevirtual android/widget/Spinner/getSelectedItem()Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_prepareAudioDevice(II)I
pop
aload 0
invokestatic com/teamspeak/ts3client/a/k/a()Lcom/teamspeak/ts3client/a/k;
putfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
putfield com/teamspeak/ts3client/a/k/d Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
aload 2
invokevirtual android/widget/Spinner/getSelectedItem()Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
aload 1
invokevirtual android/widget/Spinner/getSelectedItem()Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
aload 0
getfield com/teamspeak/ts3client/a/a/az I
aload 0
getfield com/teamspeak/ts3client/a/a/aA I
invokevirtual com/teamspeak/ts3client/a/k/a(IIII)Z
ifne L1
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/b()V
aload 0
aconst_null
putfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_destroyServerConnectionHandler(J)I
pop
aload 0
aconst_null
putfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/aH Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/a/a/aG Landroid/widget/Button;
iconst_1
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/a/a/aQ Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setVisibility(I)V
L0:
return
L1:
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual com/teamspeak/ts3client/a/k/a(Ljava/lang/Boolean;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aO Z
ifeq L2
invokestatic com/teamspeak/ts3client/a/j/a()Lcom/teamspeak/ts3client/a/j;
iconst_1
invokevirtual com/teamspeak/ts3client/a/j/a(I)V
L3:
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_activateCaptureDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
ldc ""
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_openCaptureDevice(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
ldc ""
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_openPlaybackDevice(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
getstatic com/teamspeak/ts3client/jni/d/k Lcom/teamspeak/ts3client/jni/d;
iconst_0
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
ldc "vad"
ldc "true"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
ldc "voiceactivation_level"
ldc "-50"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setLocalTestMode(JI)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/d()V
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/c()V
return
L2:
invokestatic com/teamspeak/ts3client/a/j/a()Lcom/teamspeak/ts3client/a/j;
iconst_1
invokevirtual com/teamspeak/ts3client/a/j/b(I)V
goto L3
.limit locals 3
.limit stack 5
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/a;)Z
aload 0
getfield com/teamspeak/ts3client/a/a/aO Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/a;Z)Z
aload 0
iload 1
putfield com/teamspeak/ts3client/a/a/aO Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/a;Ljava/util/Vector;)[Ljava/lang/String;
aload 0
aload 1
invokespecial com/teamspeak/ts3client/a/a/a(Ljava/util/Vector;)[Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/util/Vector;)[Ljava/lang/String;
aload 1
invokevirtual java/util/Vector/size()I
ifgt L0
iconst_0
anewarray java/lang/String
areturn
L0:
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
astore 3
aload 1
invokevirtual java/util/Vector/iterator()Ljava/util/Iterator;
astore 1
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 2
iload 2
ifeq L1
aload 3
iload 2
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
goto L1
L2:
aload 3
invokevirtual java/util/Vector/size()I
ifne L3
aload 0
getfield com/teamspeak/ts3client/a/a/aG Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/a/a/aJ Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/a/a/aQ Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setVisibility(I)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "ERROR"
aastore
areturn
L3:
aload 3
aload 3
invokevirtual java/util/Vector/size()I
anewarray java/lang/String
invokevirtual java/util/Vector/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/a/a/aQ Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Landroid/widget/Spinner;Landroid/widget/Spinner;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aD Z
ifne L0
return
L0:
aload 0
aload 0
getfield com/teamspeak/ts3client/a/a/aB Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aC Landroid/widget/Spinner;
invokespecial com/teamspeak/ts3client/a/a/a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closeCaptureDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closePlaybackDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 2
invokevirtual android/widget/Spinner/getSelectedItem()Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
aload 1
invokevirtual android/widget/Spinner/getSelectedItem()Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_prepareAudioDevice(II)I
pop
aload 0
invokestatic com/teamspeak/ts3client/a/k/a()Lcom/teamspeak/ts3client/a/k;
putfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
putfield com/teamspeak/ts3client/a/k/d Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
aload 2
invokevirtual android/widget/Spinner/getSelectedItem()Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
aload 1
invokevirtual android/widget/Spinner/getSelectedItem()Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
aload 0
getfield com/teamspeak/ts3client/a/a/az I
aload 0
getfield com/teamspeak/ts3client/a/a/aA I
invokevirtual com/teamspeak/ts3client/a/k/a(IIII)Z
ifne L1
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/b()V
aload 0
aconst_null
putfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_destroyServerConnectionHandler(J)I
pop
aload 0
aconst_null
putfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/aH Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/a/a/aG Landroid/widget/Button;
iconst_1
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/a/a/aQ Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setVisibility(I)V
return
L1:
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual com/teamspeak/ts3client/a/k/a(Ljava/lang/Boolean;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aO Z
ifeq L2
invokestatic com/teamspeak/ts3client/a/j/a()Lcom/teamspeak/ts3client/a/j;
iconst_1
invokevirtual com/teamspeak/ts3client/a/j/a(I)V
L3:
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_activateCaptureDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
ldc ""
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_openCaptureDevice(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
ldc ""
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_openPlaybackDevice(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
getstatic com/teamspeak/ts3client/jni/d/k Lcom/teamspeak/ts3client/jni/d;
iconst_0
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
ldc "vad"
ldc "true"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
ldc "voiceactivation_level"
ldc "-50"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/ay J
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setLocalTestMode(JI)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/d()V
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/c()V
return
L2:
invokestatic com/teamspeak/ts3client/a/j/a()Lcom/teamspeak/ts3client/a/j;
iconst_1
invokevirtual com/teamspeak/ts3client/a/j/b(I)V
goto L3
.limit locals 3
.limit stack 5
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/a/a;Landroid/widget/Spinner;Landroid/widget/Spinner;)V
aload 0
aload 1
aload 2
invokespecial com/teamspeak/ts3client/a/a/a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/a/a;)Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/a/aI Z
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/aw Lcom/teamspeak/ts3client/jni/Ts3Jni;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/a/a;)Z
aload 0
iconst_1
putfield com/teamspeak/ts3client/a/a/aD Z
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aF Landroid/widget/Spinner;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aE Landroid/widget/Spinner;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Button;
aload 0
getfield com/teamspeak/ts3client/a/a/aH Landroid/widget/Button;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Button;
aload 0
getfield com/teamspeak/ts3client/a/a/aG Landroid/widget/Button;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/a/a;)V
aload 0
aload 0
getfield com/teamspeak/ts3client/a/a/aB Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aC Landroid/widget/Spinner;
invokespecial com/teamspeak/ts3client/a/a/a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "recordStream"
aload 0
getfield com/teamspeak/ts3client/a/a/aA I
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "playbackStream"
aload 0
getfield com/teamspeak/ts3client/a/a/az I
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "sampleRec"
aload 0
getfield com/teamspeak/ts3client/a/a/aE Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getSelectedItem()Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "samplePlay"
aload 0
getfield com/teamspeak/ts3client/a/a/aF Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getSelectedItem()Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
invokevirtual com/teamspeak/ts3client/a/a/b()V
return
.limit locals 1
.limit stack 3
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aB Landroid/widget/Spinner;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aC Landroid/widget/Spinner;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic n(Lcom/teamspeak/ts3client/a/a;)Z
aload 0
getfield com/teamspeak/ts3client/a/a/aI Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic o(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic p(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/a/k;
aload 0
aconst_null
putfield com/teamspeak/ts3client/a/a/ax Lcom/teamspeak/ts3client/a/k;
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic q(Lcom/teamspeak/ts3client/a/a;)I
aload 0
getfield com/teamspeak/ts3client/a/a/az I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic r(Lcom/teamspeak/ts3client/a/a;)I
aload 0
getfield com/teamspeak/ts3client/a/a/aA I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic s(Lcom/teamspeak/ts3client/a/a;)V
aload 0
invokespecial com/teamspeak/ts3client/a/a/B()V
return
.limit locals 1
.limit stack 1
.end method

.method public static y()Lcom/teamspeak/ts3client/a/a;
getstatic com/teamspeak/ts3client/a/a/au Lcom/teamspeak/ts3client/a/a;
ifnonnull L0
new com/teamspeak/ts3client/a/a
dup
invokespecial com/teamspeak/ts3client/a/a/<init>()V
putstatic com/teamspeak/ts3client/a/a/au Lcom/teamspeak/ts3client/a/a;
L0:
getstatic com/teamspeak/ts3client/a/a/au Lcom/teamspeak/ts3client/a/a;
areturn
.limit locals 0
.limit stack 2
.end method

.method private z()V
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "playbackStream"
invokeinterface android/content/SharedPreferences/contains(Ljava/lang/String;)Z 1
ifeq L0
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "playbackStream"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
ifne L1
aload 0
getfield com/teamspeak/ts3client/a/a/aB Landroid/widget/Spinner;
iconst_0
invokevirtual android/widget/Spinner/setSelection(I)V
L2:
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "recordStream"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
ifne L3
aload 0
getfield com/teamspeak/ts3client/a/a/aC Landroid/widget/Spinner;
iconst_0
invokevirtual android/widget/Spinner/setSelection(I)V
L4:
aload 0
aload 0
getfield com/teamspeak/ts3client/a/a/aB Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aC Landroid/widget/Spinner;
invokespecial com/teamspeak/ts3client/a/a/a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
aload 0
getfield com/teamspeak/ts3client/a/a/az I
aload 0
getfield com/teamspeak/ts3client/a/a/aA I
invokestatic com/teamspeak/ts3client/a/k/a(Landroid/content/Context;II)Ljava/util/Vector;
astore 2
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
aload 0
getfield com/teamspeak/ts3client/a/a/az I
invokestatic com/teamspeak/ts3client/a/k/a(Landroid/content/Context;I)Ljava/util/Vector;
astore 3
aload 0
getfield com/teamspeak/ts3client/a/a/aF Landroid/widget/Spinner;
new android/widget/ArrayAdapter
dup
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
ldc_w 17367049
aload 0
aload 3
invokespecial com/teamspeak/ts3client/a/a/a(Ljava/util/Vector;)[Ljava/lang/String;
invokespecial android/widget/ArrayAdapter/<init>(Landroid/content/Context;I[Ljava/lang/Object;)V
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aE Landroid/widget/Spinner;
new android/widget/ArrayAdapter
dup
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
ldc_w 17367049
aload 0
aload 2
invokespecial com/teamspeak/ts3client/a/a/a(Ljava/util/Vector;)[Ljava/lang/String;
invokespecial android/widget/ArrayAdapter/<init>(Landroid/content/Context;I[Ljava/lang/Object;)V
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
iconst_0
istore 1
L5:
iload 1
aload 3
invokevirtual java/util/Vector/size()I
if_icmpge L6
aload 3
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
invokevirtual java/util/Vector/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "samplePlay"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
if_icmpne L7
aload 0
getfield com/teamspeak/ts3client/a/a/aF Landroid/widget/Spinner;
iload 1
invokevirtual android/widget/Spinner/setSelection(I)V
L6:
iconst_0
istore 1
L8:
iload 1
aload 2
invokevirtual java/util/Vector/size()I
if_icmpge L0
aload 2
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
invokevirtual java/util/Vector/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "sampleRec"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
if_icmpne L9
aload 0
getfield com/teamspeak/ts3client/a/a/aE Landroid/widget/Spinner;
iload 1
invokevirtual android/widget/Spinner/setSelection(I)V
L0:
return
L1:
aload 0
getfield com/teamspeak/ts3client/a/a/aB Landroid/widget/Spinner;
iconst_1
invokevirtual android/widget/Spinner/setSelection(I)V
goto L2
L3:
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "recordStream"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
iconst_1
if_icmpne L10
aload 0
getfield com/teamspeak/ts3client/a/a/aC Landroid/widget/Spinner;
iconst_1
invokevirtual android/widget/Spinner/setSelection(I)V
goto L4
L10:
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "recordStream"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
bipush 7
if_icmpne L4
aload 0
getfield com/teamspeak/ts3client/a/a/aC Landroid/widget/Spinner;
iconst_2
invokevirtual android/widget/Spinner/setSelection(I)V
goto L4
L7:
iload 1
iconst_1
iadd
istore 1
goto L5
L9:
iload 1
iconst_1
iadd
istore 1
goto L8
.limit locals 4
.limit stack 7
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
iconst_1
putfield com/teamspeak/ts3client/a/a/at Z
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
iconst_0
invokevirtual android/app/Dialog/setCanceledOnTouchOutside(Z)V
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ldc "audiosettings.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closeCaptureDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closePlaybackDevice(J)I
pop
L0:
aload 0
iconst_1
putfield com/teamspeak/ts3client/a/a/aI Z
aload 1
ldc_w 2130903064
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 1
aload 0
aload 1
ldc_w 2131492981
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/a/a/aQ Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/a/a/aQ Landroid/widget/TextView;
bipush 8
invokevirtual android/widget/TextView/setVisibility(I)V
aload 0
aload 1
ldc_w 2131492967
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/a/a/aG Landroid/widget/Button;
aload 0
getfield com/teamspeak/ts3client/a/a/aG Landroid/widget/Button;
new com/teamspeak/ts3client/a/c
dup
aload 0
invokespecial com/teamspeak/ts3client/a/c/<init>(Lcom/teamspeak/ts3client/a/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131492982
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/a/a/aJ Landroid/widget/Button;
aload 0
getfield com/teamspeak/ts3client/a/a/aJ Landroid/widget/Button;
new com/teamspeak/ts3client/a/d
dup
aload 0
invokespecial com/teamspeak/ts3client/a/d/<init>(Lcom/teamspeak/ts3client/a/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131492970
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/a/a/aK Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131492971
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/a/a/aL Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131492977
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/a/a/aM Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131492976
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/a/a/aN Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131492979
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Spinner
putfield com/teamspeak/ts3client/a/a/aF Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aF Landroid/widget/Spinner;
new android/widget/ArrayAdapter
dup
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
ldc_w 17367049
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
iconst_0
iconst_0
invokestatic com/teamspeak/ts3client/a/k/a(Landroid/content/Context;II)Ljava/util/Vector;
invokespecial com/teamspeak/ts3client/a/a/a(Ljava/util/Vector;)[Ljava/lang/String;
invokespecial android/widget/ArrayAdapter/<init>(Landroid/content/Context;I[Ljava/lang/Object;)V
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aF Landroid/widget/Spinner;
new com/teamspeak/ts3client/a/e
dup
aload 0
invokespecial com/teamspeak/ts3client/a/e/<init>(Lcom/teamspeak/ts3client/a/a;)V
invokevirtual android/widget/Spinner/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
aload 1
ldc_w 2131492980
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Spinner
putfield com/teamspeak/ts3client/a/a/aE Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aE Landroid/widget/Spinner;
new android/widget/ArrayAdapter
dup
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
ldc_w 17367049
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
iconst_0
invokestatic com/teamspeak/ts3client/a/k/a(Landroid/content/Context;I)Ljava/util/Vector;
invokespecial com/teamspeak/ts3client/a/a/a(Ljava/util/Vector;)[Ljava/lang/String;
invokespecial android/widget/ArrayAdapter/<init>(Landroid/content/Context;I[Ljava/lang/Object;)V
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aE Landroid/widget/Spinner;
new com/teamspeak/ts3client/a/f
dup
aload 0
invokespecial com/teamspeak/ts3client/a/f/<init>(Lcom/teamspeak/ts3client/a/a;)V
invokevirtual android/widget/Spinner/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
aload 1
ldc_w 2131492973
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Spinner
putfield com/teamspeak/ts3client/a/a/aB Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aB Landroid/widget/Spinner;
ldc "audiosettings.array.play"
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
iconst_2
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aB Landroid/widget/Spinner;
new com/teamspeak/ts3client/a/g
dup
aload 0
invokespecial com/teamspeak/ts3client/a/g/<init>(Lcom/teamspeak/ts3client/a/a;)V
invokevirtual android/widget/Spinner/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
aload 1
ldc_w 2131492974
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Spinner
putfield com/teamspeak/ts3client/a/a/aC Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/a/a/aC Landroid/widget/Spinner;
ldc "audiosettings.array.rec"
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
iconst_3
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aC Landroid/widget/Spinner;
new com/teamspeak/ts3client/a/h
dup
aload 0
invokespecial com/teamspeak/ts3client/a/h/<init>(Lcom/teamspeak/ts3client/a/a;)V
invokevirtual android/widget/Spinner/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
aload 1
ldc_w 2131492968
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/a/a/aH Landroid/widget/Button;
aload 0
getfield com/teamspeak/ts3client/a/a/aH Landroid/widget/Button;
new com/teamspeak/ts3client/a/i
dup
aload 0
invokespecial com/teamspeak/ts3client/a/i/<init>(Lcom/teamspeak/ts3client/a/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aH Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/a/a/aG Landroid/widget/Button;
ldc "audiosettings.button.start"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aH Landroid/widget/Button;
ldc "audiosettings.button.stop"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aJ Landroid/widget/Button;
ldc "button.save"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aN Landroid/widget/TextView;
ldc "audiosettings.text.play"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aM Landroid/widget/TextView;
ldc "audiosettings.text.rec"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aK Landroid/widget/TextView;
ldc "audiosettings.text.playbackstream"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aL Landroid/widget/TextView;
ldc "audiosettings.text.recordstream"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/a/a/aQ Landroid/widget/TextView;
ldc "audiosettings.error"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
invokespecial com/teamspeak/ts3client/a/a/z()V
aload 1
areturn
.limit locals 3
.limit stack 9
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/ax/a(Landroid/os/Bundle;)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
return
.limit locals 2
.limit stack 2
.end method

.method public final c(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/ax/c(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/ax/e()V
return
.limit locals 1
.limit stack 1
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
aload 0
invokespecial com/teamspeak/ts3client/a/a/B()V
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/a/a/av Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/r()V
L0:
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/a/at Z
aconst_null
putstatic com/teamspeak/ts3client/a/a/au Lcom/teamspeak/ts3client/a/a;
aload 0
aload 1
invokespecial android/support/v4/app/ax/onDismiss(Landroid/content/DialogInterface;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final s()V
new android/content/IntentFilter
dup
invokespecial android/content/IntentFilter/<init>()V
astore 1
aload 1
ldc "android.intent.action.HEADSET_PLUG"
invokevirtual android/content/IntentFilter/addAction(Ljava/lang/String;)V
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
aload 0
getfield com/teamspeak/ts3client/a/a/aP Landroid/content/BroadcastReceiver;
aload 1
invokevirtual android/support/v4/app/bb/registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
pop
aload 0
invokespecial android/support/v4/app/ax/s()V
return
.limit locals 2
.limit stack 3
.end method

.method public final t()V
aload 0
invokevirtual com/teamspeak/ts3client/a/a/i()Landroid/support/v4/app/bb;
aload 0
getfield com/teamspeak/ts3client/a/a/aP Landroid/content/BroadcastReceiver;
invokevirtual android/support/v4/app/bb/unregisterReceiver(Landroid/content/BroadcastReceiver;)V
aload 0
invokespecial android/support/v4/app/ax/t()V
return
.limit locals 1
.limit stack 2
.end method
