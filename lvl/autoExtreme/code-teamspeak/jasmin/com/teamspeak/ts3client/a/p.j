.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/a/p
.super java/lang/Object
.implements android/content/SharedPreferences$OnSharedPreferenceChangeListener

.field public 'a' Z

.field private 'b' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'c' Lcom/teamspeak/ts3client/a/n;

.field private 'd' I

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/p/a Z
aload 0
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/a/p/b Lcom/teamspeak/ts3client/Ts3Application;
aload 0
aload 0
getfield com/teamspeak/ts3client/a/p/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "sound_pack"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
putfield com/teamspeak/ts3client/a/p/d I
aload 0
iconst_0
invokespecial com/teamspeak/ts3client/a/p/a(Z)V
aload 0
getfield com/teamspeak/ts3client/a/p/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
aload 0
invokeinterface android/content/SharedPreferences/registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V 1
return
.limit locals 2
.limit stack 4
.end method

.method private a()V
aload 0
new com/teamspeak/ts3client/a/r
dup
invokespecial com/teamspeak/ts3client/a/r/<init>()V
putfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
aload 0
getfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
aload 0
getfield com/teamspeak/ts3client/a/p/b Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
invokeinterface com/teamspeak/ts3client/a/n/a(Landroid/content/Context;)V 1
return
.limit locals 1
.limit stack 3
.end method

.method private a(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/a/p/a Z
ifeq L0
return
L0:
aload 0
getfield com/teamspeak/ts3client/a/p/d I
tableswitch 1
L1
default : L2
L2:
return
L1:
aload 0
getfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
aload 1
invokeinterface com/teamspeak/ts3client/a/n/a(Ljava/lang/String;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private a(Z)V
iload 1
ifeq L0
aload 0
getfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
invokeinterface com/teamspeak/ts3client/a/n/b()V 0
L0:
aload 0
getfield com/teamspeak/ts3client/a/p/d I
tableswitch 0
L1
L2
default : L3
L3:
return
L2:
aload 0
new com/teamspeak/ts3client/a/r
dup
invokespecial com/teamspeak/ts3client/a/r/<init>()V
putfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
aload 0
getfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
aload 0
getfield com/teamspeak/ts3client/a/p/b Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
invokeinterface com/teamspeak/ts3client/a/n/a(Landroid/content/Context;)V 1
return
L1:
aload 0
new com/teamspeak/ts3client/a/t
dup
invokespecial com/teamspeak/ts3client/a/t/<init>()V
putfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
aload 0
getfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
aload 0
getfield com/teamspeak/ts3client/a/p/b Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
invokeinterface com/teamspeak/ts3client/a/n/a(Landroid/content/Context;)V 1
return
.limit locals 2
.limit stack 3
.end method

.method private b()V
aload 0
new com/teamspeak/ts3client/a/t
dup
invokespecial com/teamspeak/ts3client/a/t/<init>()V
putfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
aload 0
getfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
aload 0
getfield com/teamspeak/ts3client/a/p/b Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
invokeinterface com/teamspeak/ts3client/a/n/a(Landroid/content/Context;)V 1
return
.limit locals 1
.limit stack 3
.end method

.method private b(Z)V
aload 0
iload 1
putfield com/teamspeak/ts3client/a/p/a Z
return
.limit locals 2
.limit stack 2
.end method

.method private c()V
aload 0
getfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
invokeinterface com/teamspeak/ts3client/a/n/a()V 0
return
.limit locals 1
.limit stack 1
.end method

.method private d()V
aload 0
getfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
invokeinterface com/teamspeak/ts3client/a/n/b()V 0
L0:
return
.limit locals 1
.limit stack 1
.end method

.method private e()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/p/a Z
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
aload 0
getfield com/teamspeak/ts3client/a/p/a Z
ifeq L0
return
L0:
aload 0
getfield com/teamspeak/ts3client/a/p/d I
tableswitch 0
L1
L2
default : L3
L3:
return
L1:
aload 0
getfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
aload 1
aload 2
invokeinterface com/teamspeak/ts3client/a/n/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V 2
return
L2:
aload 0
getfield com/teamspeak/ts3client/a/p/c Lcom/teamspeak/ts3client/a/n;
aload 1
aload 2
invokeinterface com/teamspeak/ts3client/a/n/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 2
ldc "sound_pack"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 0
aload 0
getfield com/teamspeak/ts3client/a/p/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "sound_pack"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
putfield com/teamspeak/ts3client/a/p/d I
L0:
aload 0
iconst_1
invokespecial com/teamspeak/ts3client/a/p/a(Z)V
L1:
return
L2:
astore 1
aload 0
getfield com/teamspeak/ts3client/a/p/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "sound_pack"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
iconst_1
if_icmpne L1
aload 0
getfield com/teamspeak/ts3client/a/p/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "sound_pack"
iconst_0
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/a/p/b Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
pop
new android/app/AlertDialog$Builder
dup
aload 0
getfield com/teamspeak/ts3client/a/p/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 1
aload 1
ldc "critical.tts"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 1
ldc "critical.tts.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 1
bipush -3
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/a/q
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/a/q/<init>(Lcom/teamspeak/ts3client/a/p;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 1
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 1
invokevirtual android/app/AlertDialog/show()V
return
.limit locals 3
.limit stack 7
.end method
