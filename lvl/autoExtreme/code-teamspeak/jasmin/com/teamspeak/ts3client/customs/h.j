.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/customs/h
.super android/widget/LinearLayout

.field private 'a' Landroid/widget/ImageView;

.field private 'b' Landroid/widget/TextView;

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
ldc_w 2130903104
aconst_null
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 1
aload 0
aload 1
ldc_w 2131493252
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield com/teamspeak/ts3client/customs/h/a Landroid/widget/ImageView;
aload 0
aload 1
ldc_w 2131493253
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/customs/h/b Landroid/widget/TextView;
aload 0
ldc_w 2130837686
invokevirtual com/teamspeak/ts3client/customs/h/setBackgroundResource(I)V
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/customs/h/addView(Landroid/view/View;)V
return
.limit locals 2
.limit stack 4
.end method

.method private a(ILjava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/customs/h/a Landroid/widget/ImageView;
iload 1
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/customs/h/b Landroid/widget/TextView;
aload 2
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 4
.end method

.method private a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/customs/h/a Landroid/widget/ImageView;
aload 1
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield com/teamspeak/ts3client/customs/h/b Landroid/widget/TextView;
aload 2
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final setEnabled(Z)V
aload 0
iload 1
invokespecial android/widget/LinearLayout/setEnabled(Z)V
iload 1
ifne L0
aload 0
getfield com/teamspeak/ts3client/customs/h/b Landroid/widget/TextView;
ldc_w -12303292
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield com/teamspeak/ts3client/customs/h/a Landroid/widget/ImageView;
ldc_w -12303292
getstatic android/graphics/PorterDuff$Mode/MULTIPLY Landroid/graphics/PorterDuff$Mode;
invokevirtual android/widget/ImageView/setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
L0:
return
.limit locals 2
.limit stack 3
.end method
