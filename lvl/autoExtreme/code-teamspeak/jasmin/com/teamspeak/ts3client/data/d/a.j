.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/d/a
.super java/lang/Object

.field static 'a' Landroid/text/Html$ImageGetter;

.field private static 'b' Ljava/util/regex/Pattern;

.field private static 'c' Ljava/util/regex/Pattern;

.field private static 'd' Ljava/util/regex/Pattern;

.field private static 'e' [Ljava/lang/String;

.field private static 'f' I

.method static <clinit>()V
new com/teamspeak/ts3client/data/d/b
dup
invokespecial com/teamspeak/ts3client/data/d/b/<init>()V
putstatic com/teamspeak/ts3client/data/d/a/a Landroid/text/Html$ImageGetter;
ldc "<img src=\"(ts3image://.*?)\">"
bipush 34
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;
putstatic com/teamspeak/ts3client/data/d/a/b Ljava/util/regex/Pattern;
ldc "<span style=\\\".*? font-size:([+|-]\\d+?pt);.*?\\\">"
iconst_2
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;
putstatic com/teamspeak/ts3client/data/d/a/c Ljava/util/regex/Pattern;
ldc "ts3image://(.*)\\?channel=(\\d*)(?:&|&amp;)path=(.*)"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putstatic com/teamspeak/ts3client/data/d/a/d Ljava/util/regex/Pattern;
bipush 7
anewarray java/lang/String
dup
iconst_0
ldc "xx-small"
aastore
dup
iconst_1
ldc "x-small"
aastore
dup
iconst_2
ldc "small"
aastore
dup
iconst_3
ldc "medium"
aastore
dup
iconst_4
ldc "large"
aastore
dup
iconst_5
ldc "x-large"
aastore
dup
bipush 6
ldc "xx-large"
aastore
putstatic com/teamspeak/ts3client/data/d/a/e [Ljava/lang/String;
iconst_3
putstatic com/teamspeak/ts3client/data/d/a/f I
return
.limit locals 0
.limit stack 4
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/String;)Landroid/text/Spanned;
aload 0
ldc "&"
ldc "&amp;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "<"
ldc "&lt;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc ">"
ldc "&gt;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "\""
ldc "&quot;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getstatic com/teamspeak/ts3client/jni/b/y Lcom/teamspeak/ts3client/jni/b;
getfield com/teamspeak/ts3client/jni/b/B I
iconst_0
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_convertBBCodetoHTMLtags_v2(Ljava/lang/String;IZ)Ljava/lang/String;
ldc "\n"
ldc "<br>"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "\\n"
ldc "<br>"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokestatic android/text/Html/fromHtml(Ljava/lang/String;)Landroid/text/Spanned;
areturn
.limit locals 1
.limit stack 4
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;
aload 1
ldc "&"
ldc "&amp;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "<"
ldc "&lt;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc ">"
ldc "&gt;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "\""
ldc "&quot;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 1
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 1
getstatic com/teamspeak/ts3client/jni/b/y Lcom/teamspeak/ts3client/jni/b;
getfield com/teamspeak/ts3client/jni/b/B I
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_convertBBCodetoHTMLtags_v2(Ljava/lang/String;IZ)Ljava/lang/String;
ldc "\n"
ldc "<br>"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/text/Html/fromHtml(Ljava/lang/String;)Landroid/text/Spanned;
areturn
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/lang/String;JLcom/teamspeak/ts3client/data/d/c;)Ljava/lang/String;
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "NewApi" 
.end annotation
aload 0
ldc "&"
ldc "&amp;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "<"
ldc "&lt;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc ">"
ldc "&gt;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "\""
ldc "&quot;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getstatic com/teamspeak/ts3client/jni/b/A Lcom/teamspeak/ts3client/jni/b;
getfield com/teamspeak/ts3client/jni/b/B I
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_convertBBCodetoHTMLtags_v2(Ljava/lang/String;IZ)Ljava/lang/String;
astore 0
new java/lang/StringBuilder
dup
ldc "<body text=\"white\">"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "</body>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc "\\["
ldc "["
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "\\]"
ldc "]"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 6
getstatic com/teamspeak/ts3client/data/d/a/c Ljava/util/regex/Pattern;
aload 6
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 0
new java/lang/StringBuffer
dup
aload 6
invokevirtual java/lang/String/length()I
invokespecial java/lang/StringBuffer/<init>(I)V
astore 6
L0:
aload 0
invokevirtual java/util/regex/Matcher/find()Z
ifeq L1
aload 0
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ldc "pt"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "+"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
getstatic com/teamspeak/ts3client/data/d/a/f I
iadd
istore 5
iload 5
istore 4
iload 5
bipush 6
if_icmple L2
bipush 6
istore 4
L2:
iload 4
istore 5
iload 4
ifge L3
iconst_0
istore 5
L3:
aload 0
aload 6
aload 0
iconst_0
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
aload 0
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
getstatic com/teamspeak/ts3client/data/d/a/e [Ljava/lang/String;
iload 5
aaload
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokestatic java/util/regex/Matcher/quoteReplacement(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/util/regex/Matcher/appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;
pop
goto L0
L1:
aload 0
aload 6
invokevirtual java/util/regex/Matcher/appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
pop
aload 6
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
astore 0
getstatic com/teamspeak/ts3client/data/d/a/b Ljava/util/regex/Pattern;
aload 0
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 6
L4:
aload 6
invokevirtual java/util/regex/Matcher/find()Z
ifeq L5
new com/teamspeak/ts3client/data/d/d
dup
aload 6
iconst_0
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
aload 6
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
aload 3
lload 1
invokespecial com/teamspeak/ts3client/data/d/d/<init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/d/c;J)V
astore 7
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L6
new com/teamspeak/ts3client/data/d/e
dup
invokespecial com/teamspeak/ts3client/data/d/e/<init>()V
getstatic android/os/AsyncTask/THREAD_POOL_EXECUTOR Ljava/util/concurrent/Executor;
iconst_1
anewarray com/teamspeak/ts3client/data/d/d
dup
iconst_0
aload 7
aastore
invokevirtual com/teamspeak/ts3client/data/d/e/executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
goto L4
L6:
new com/teamspeak/ts3client/data/d/e
dup
invokespecial com/teamspeak/ts3client/data/d/e/<init>()V
iconst_1
anewarray com/teamspeak/ts3client/data/d/d
dup
iconst_0
aload 7
aastore
invokevirtual com/teamspeak/ts3client/data/d/e/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
goto L4
L5:
aload 0
areturn
.limit locals 8
.limit stack 7
.end method

.method static synthetic a()Ljava/util/regex/Pattern;
getstatic com/teamspeak/ts3client/data/d/a/d Ljava/util/regex/Pattern;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;
aload 1
ifnonnull L0
aload 0
invokestatic com/teamspeak/ts3client/data/d/a/a(Ljava/lang/String;)Landroid/text/Spanned;
areturn
L0:
new java/lang/StringBuilder
dup
ldc "\""
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
aload 0
ldc "&"
ldc "&amp;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "<"
ldc "&lt;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc ">"
ldc "&gt;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "\""
ldc "&quot;"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getstatic com/teamspeak/ts3client/jni/b/y Lcom/teamspeak/ts3client/jni/b;
getfield com/teamspeak/ts3client/jni/b/B I
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_convertBBCodetoHTMLtags_v2(Ljava/lang/String;IZ)Ljava/lang/String;
ldc "\n"
ldc "<br>"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/text/Html/fromHtml(Ljava/lang/String;)Landroid/text/Spanned;
areturn
.limit locals 2
.limit stack 4
.end method
