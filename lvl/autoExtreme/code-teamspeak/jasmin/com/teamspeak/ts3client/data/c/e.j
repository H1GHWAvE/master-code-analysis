.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/c/e
.super java/lang/Object
.implements com/teamspeak/ts3client/data/w

.field private static 'c' Ljava/util/regex/Pattern;

.field 'a' Ljava/util/concurrent/CountDownLatch;

.field 'b' Landroid/graphics/drawable/Drawable;

.field private 'd' I

.field private 'e' Ljava/io/File;

.field private 'f' Z

.field private 'g' Z

.field private 'h' Ljava/lang/String;

.field private 'i' Ljava/lang/String;

.method static <clinit>()V
ldc "ts3image://(.*)\\?channel=(\\d*)(?:&|&amp;)path=(.*)"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putstatic com/teamspeak/ts3client/data/c/e/c Ljava/util/regex/Pattern;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield com/teamspeak/ts3client/data/c/e/a Ljava/util/concurrent/CountDownLatch;
aload 0
aconst_null
putfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
aload 0
iconst_0
putfield com/teamspeak/ts3client/data/c/e/f Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/data/c/e/g Z
aload 0
ldc ""
putfield com/teamspeak/ts3client/data/c/e/h Ljava/lang/String;
aload 0
ldc ""
putfield com/teamspeak/ts3client/data/c/e/i Ljava/lang/String;
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Ljava/lang/String;Ljava/io/File;)Landroid/graphics/drawable/Drawable;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/InterruptedException from L1 to L3 using L4
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/InterruptedException from L9 to L10 using L11
.catch java/lang/Exception from L12 to L13 using L14
getstatic com/teamspeak/ts3client/data/c/e/c Ljava/util/regex/Pattern;
aload 1
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 4
aload 4
invokevirtual java/util/regex/Matcher/find()Z
ifeq L15
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 4
iconst_3
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
iconst_1
if_icmple L16
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
iconst_3
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/e/i Ljava/lang/String;
L17:
ldc ""
astore 3
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/j Ljava/util/HashMap;
aload 5
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L18
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/j Ljava/util/HashMap;
aload 5
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
astore 3
L18:
aload 0
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putfield com/teamspeak/ts3client/data/c/e/e Ljava/io/File;
aload 0
getfield com/teamspeak/ts3client/data/c/e/e Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifeq L19
aload 0
getfield com/teamspeak/ts3client/data/c/e/e Ljava/io/File;
invokevirtual java/io/File/isFile()Z
ifeq L19
L0:
aload 0
aload 0
getfield com/teamspeak/ts3client/data/c/e/e Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokestatic android/graphics/drawable/Drawable/createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
putfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
iconst_0
iadd
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
iconst_0
iadd
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 0
new java/util/concurrent/CountDownLatch
dup
iconst_1
invokespecial java/util/concurrent/CountDownLatch/<init>(I)V
putfield com/teamspeak/ts3client/data/c/e/a Ljava/util/concurrent/CountDownLatch;
aload 0
aload 0
getfield com/teamspeak/ts3client/data/c/e/i Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/e/h Ljava/lang/String;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
aload 3
aload 0
getfield com/teamspeak/ts3client/data/c/e/i Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/c/e/h Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestFileInfo(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 0
getfield com/teamspeak/ts3client/data/c/e/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/await()V
L3:
aload 0
getfield com/teamspeak/ts3client/data/c/e/f Z
ifeq L7
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
astore 5
L5:
aload 5
areturn
L16:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
iconst_3
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/e/i Ljava/lang/String;
goto L17
L4:
astore 5
L6:
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Failed to load Image1: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837580
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
iconst_0
iadd
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
iconst_0
iadd
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
areturn
L7:
aload 0
getfield com/teamspeak/ts3client/data/c/e/g Z
ifeq L19
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837580
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
iconst_0
iadd
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
iconst_0
iadd
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
astore 5
L8:
aload 5
areturn
L2:
astore 5
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Failed to load Image2: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
L19:
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 0
new java/util/concurrent/CountDownLatch
dup
iconst_1
invokespecial java/util/concurrent/CountDownLatch/<init>(I)V
putfield com/teamspeak/ts3client/data/c/e/a Ljava/util/concurrent/CountDownLatch;
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
aload 3
aload 0
getfield com/teamspeak/ts3client/data/c/e/i Ljava/lang/String;
iconst_1
iconst_0
aload 2
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "loadImage:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/e/i Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestFile(JJLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)I
putfield com/teamspeak/ts3client/data/c/e/d I
L9:
aload 0
getfield com/teamspeak/ts3client/data/c/e/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/await()V
L10:
aload 0
getfield com/teamspeak/ts3client/data/c/e/e Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifeq L15
aload 0
getfield com/teamspeak/ts3client/data/c/e/e Ljava/io/File;
invokevirtual java/io/File/isFile()Z
ifeq L15
L12:
aload 0
aload 0
getfield com/teamspeak/ts3client/data/c/e/e Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokestatic android/graphics/drawable/Drawable/createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
putfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
iconst_0
iadd
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
iconst_0
iadd
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
astore 2
L13:
aload 2
areturn
L11:
astore 2
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Failed to load Image3: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837580
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
iconst_0
iadd
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
iconst_0
iadd
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
areturn
L14:
astore 2
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Failed to load Image4: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837580
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
iconst_0
iadd
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
iconst_0
iadd
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
areturn
L15:
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837580
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
iconst_0
iadd
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
iconst_0
iadd
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield com/teamspeak/ts3client/data/c/e/b Landroid/graphics/drawable/Drawable;
areturn
.limit locals 6
.limit stack 14
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/FileTransferStatus
ifeq L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/FileTransferStatus
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/a I
aload 0
getfield com/teamspeak/ts3client/data/c/e/d I
if_icmpne L0
aload 0
getfield com/teamspeak/ts3client/data/c/e/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/countDown()V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
L0:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/FileInfo
ifeq L1
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/FileInfo
astore 2
aload 2
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/a Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/c/e/h Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 2
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/b J
aload 0
getfield com/teamspeak/ts3client/data/c/e/e Ljava/io/File;
invokevirtual java/io/File/length()J
lcmp
ifne L2
aload 0
getfield com/teamspeak/ts3client/data/c/e/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/countDown()V
aload 0
iconst_1
putfield com/teamspeak/ts3client/data/c/e/f Z
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
L1:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ServerError
ifeq L3
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/c/e/h Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
getfield com/teamspeak/ts3client/jni/events/ServerError/b I
sipush 2051
if_icmpne L4
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/c/e/h Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
aload 0
iconst_1
putfield com/teamspeak/ts3client/data/c/e/g Z
aload 0
getfield com/teamspeak/ts3client/data/c/e/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/countDown()V
L4:
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
getfield com/teamspeak/ts3client/jni/events/ServerError/b I
sipush 2054
if_icmpne L5
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/c/e/h Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
aload 0
iconst_1
putfield com/teamspeak/ts3client/data/c/e/g Z
aload 0
getfield com/teamspeak/ts3client/data/c/e/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/countDown()V
L5:
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
getfield com/teamspeak/ts3client/jni/events/ServerError/b I
sipush 781
if_icmpne L3
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/c/e/h Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
aload 0
iconst_1
putfield com/teamspeak/ts3client/data/c/e/g Z
aload 0
getfield com/teamspeak/ts3client/data/c/e/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/countDown()V
L3:
return
L2:
aload 0
getfield com/teamspeak/ts3client/data/c/e/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/countDown()V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
goto L1
.limit locals 3
.limit stack 4
.end method
