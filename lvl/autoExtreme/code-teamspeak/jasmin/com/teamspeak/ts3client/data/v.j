.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/v
.super java/lang/Object
.implements android/content/SharedPreferences$OnSharedPreferenceChangeListener
.implements com/teamspeak/ts3client/data/x

.field private static 'd' Lcom/teamspeak/ts3client/data/v;

.field public 'a' Ljava/util/BitSet;

.field public 'b' Z

.field public 'c' Lcom/teamspeak/ts3client/data/y;

.field private 'e' Ljava/util/BitSet;

.field private 'f' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'g' Z

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/BitSet
dup
invokespecial java/util/BitSet/<init>()V
putfield com/teamspeak/ts3client/data/v/a Ljava/util/BitSet;
aload 0
new java/util/BitSet
dup
invokespecial java/util/BitSet/<init>()V
putfield com/teamspeak/ts3client/data/v/e Ljava/util/BitSet;
aload 0
iconst_0
putfield com/teamspeak/ts3client/data/v/b Z
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
putfield com/teamspeak/ts3client/data/v/f Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/data/v/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
aload 0
invokeinterface android/content/SharedPreferences/registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V 1
aload 0
aload 0
getfield com/teamspeak/ts3client/data/v/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_ptt"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
putfield com/teamspeak/ts3client/data/v/b Z
return
.limit locals 1
.limit stack 4
.end method

.method public static a()Lcom/teamspeak/ts3client/data/v;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
ldc com/teamspeak/ts3client/data/v
monitorenter
L0:
getstatic com/teamspeak/ts3client/data/v/d Lcom/teamspeak/ts3client/data/v;
ifnonnull L1
new com/teamspeak/ts3client/data/v
dup
invokespecial com/teamspeak/ts3client/data/v/<init>()V
putstatic com/teamspeak/ts3client/data/v/d Lcom/teamspeak/ts3client/data/v;
L1:
getstatic com/teamspeak/ts3client/data/v/d Lcom/teamspeak/ts3client/data/v;
astore 0
L3:
ldc com/teamspeak/ts3client/data/v
monitorexit
aload 0
areturn
L2:
astore 0
ldc com/teamspeak/ts3client/data/v
monitorexit
aload 0
athrow
.limit locals 1
.limit stack 2
.end method

.method private a(Lcom/teamspeak/ts3client/data/y;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/v/c Lcom/teamspeak/ts3client/data/y;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Z)V
aload 0
iload 1
putfield com/teamspeak/ts3client/data/v/b Z
return
.limit locals 2
.limit stack 2
.end method

.method private b()Z
aload 0
getfield com/teamspeak/ts3client/data/v/e Ljava/util/BitSet;
invokevirtual java/util/BitSet/isEmpty()Z
ifeq L0
L1:
iconst_0
ireturn
L0:
iconst_1
istore 3
aload 0
getfield com/teamspeak/ts3client/data/v/e Ljava/util/BitSet;
iconst_0
invokevirtual java/util/BitSet/nextSetBit(I)I
istore 1
L2:
iload 3
istore 2
iload 1
iflt L3
aload 0
getfield com/teamspeak/ts3client/data/v/a Ljava/util/BitSet;
iload 1
invokevirtual java/util/BitSet/get(I)Z
ifne L4
iconst_0
istore 2
L3:
aload 0
getfield com/teamspeak/ts3client/data/v/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L1
aload 0
getfield com/teamspeak/ts3client/data/v/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
iload 2
invokevirtual com/teamspeak/ts3client/t/g(Z)V
aload 0
getfield com/teamspeak/ts3client/data/v/g Z
ifeq L1
iload 2
ireturn
L4:
aload 0
getfield com/teamspeak/ts3client/data/v/e Ljava/util/BitSet;
iload 1
iconst_1
iadd
invokevirtual java/util/BitSet/nextSetBit(I)I
istore 1
goto L2
.limit locals 4
.limit stack 3
.end method

.method private c()V
aload 0
aconst_null
putfield com/teamspeak/ts3client/data/v/c Lcom/teamspeak/ts3client/data/y;
return
.limit locals 1
.limit stack 2
.end method

.method private d()Ljava/util/BitSet;
aload 0
getfield com/teamspeak/ts3client/data/v/a Ljava/util/BitSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Z
aload 0
getfield com/teamspeak/ts3client/data/v/b Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()Ljava/util/BitSet;
aload 0
getfield com/teamspeak/ts3client/data/v/e Ljava/util/BitSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/util/BitSet;Z)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/v/e Ljava/util/BitSet;
aload 0
iload 2
putfield com/teamspeak/ts3client/data/v/g Z
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/view/KeyEvent;)Z
aload 0
getfield com/teamspeak/ts3client/data/v/b Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/data/v/c Lcom/teamspeak/ts3client/data/y;
ifnonnull L0
L1:
iconst_0
ireturn
L0:
aload 1
invokevirtual android/view/KeyEvent/getAction()I
ifne L2
aload 0
getfield com/teamspeak/ts3client/data/v/a Ljava/util/BitSet;
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
invokevirtual java/util/BitSet/get(I)Z
ifne L3
aload 0
getfield com/teamspeak/ts3client/data/v/a Ljava/util/BitSet;
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
invokevirtual java/util/BitSet/set(I)V
L3:
aload 0
getfield com/teamspeak/ts3client/data/v/b Z
ifeq L4
aload 0
getfield com/teamspeak/ts3client/data/v/c Lcom/teamspeak/ts3client/data/y;
ifnonnull L4
aload 0
getfield com/teamspeak/ts3client/data/v/e Ljava/util/BitSet;
invokevirtual java/util/BitSet/isEmpty()Z
ifne L1
iconst_1
istore 4
aload 0
getfield com/teamspeak/ts3client/data/v/e Ljava/util/BitSet;
iconst_0
invokevirtual java/util/BitSet/nextSetBit(I)I
istore 2
L5:
iload 4
istore 3
iload 2
iflt L6
aload 0
getfield com/teamspeak/ts3client/data/v/a Ljava/util/BitSet;
iload 2
invokevirtual java/util/BitSet/get(I)Z
ifne L7
iconst_0
istore 3
L6:
aload 0
getfield com/teamspeak/ts3client/data/v/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L1
aload 0
getfield com/teamspeak/ts3client/data/v/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
iload 3
invokevirtual com/teamspeak/ts3client/t/g(Z)V
aload 0
getfield com/teamspeak/ts3client/data/v/g Z
ifeq L1
iload 3
ireturn
L2:
aload 0
getfield com/teamspeak/ts3client/data/v/a Ljava/util/BitSet;
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
invokevirtual java/util/BitSet/get(I)Z
ifeq L3
aload 0
getfield com/teamspeak/ts3client/data/v/a Ljava/util/BitSet;
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
invokevirtual java/util/BitSet/clear(I)V
aload 0
getfield com/teamspeak/ts3client/data/v/c Lcom/teamspeak/ts3client/data/y;
ifnull L3
aload 0
getfield com/teamspeak/ts3client/data/v/c Lcom/teamspeak/ts3client/data/y;
invokeinterface com/teamspeak/ts3client/data/y/p_()V 0
goto L3
L7:
aload 0
getfield com/teamspeak/ts3client/data/v/e Ljava/util/BitSet;
iload 2
iconst_1
iadd
invokevirtual java/util/BitSet/nextSetBit(I)I
istore 2
goto L5
L4:
aload 0
getfield com/teamspeak/ts3client/data/v/c Lcom/teamspeak/ts3client/data/y;
ifnull L1
aload 0
getfield com/teamspeak/ts3client/data/v/c Lcom/teamspeak/ts3client/data/y;
invokeinterface com/teamspeak/ts3client/data/y/a()V 0
iconst_0
ireturn
.limit locals 5
.limit stack 3
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
aload 2
ldc "audio_ptt"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
aload 0
getfield com/teamspeak/ts3client/data/v/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_ptt"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
putfield com/teamspeak/ts3client/data/v/b Z
L0:
return
.limit locals 3
.limit stack 4
.end method
