.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/c/d
.super java/lang/Object

.field 'a' Ljava/lang/String;

.field 'b' Landroid/graphics/drawable/Drawable;

.field 'c' Ljava/util/HashMap;

.field private 'd' Ljava/io/File;

.field private 'e' Lcom/teamspeak/ts3client/Ts3Application;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/cache/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/d/a Ljava/lang/String;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/data/c/d/c Ljava/util/HashMap;
aload 0
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/data/c/d/e Lcom/teamspeak/ts3client/Ts3Application;
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/data/c/d/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/c/d/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/q Ljava/lang/String;
ldc "/"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/icons/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/d/a Ljava/lang/String;
aload 0
new java/io/File
dup
aload 0
getfield com/teamspeak/ts3client/data/c/d/a Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putfield com/teamspeak/ts3client/data/c/d/d Ljava/io/File;
aload 0
getfield com/teamspeak/ts3client/data/c/d/d Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/data/c/d/d Ljava/io/File;
invokevirtual java/io/File/mkdirs()Z
pop
L0:
aload 0
getfield com/teamspeak/ts3client/data/c/d/c Ljava/util/HashMap;
lconst_0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
new com/teamspeak/ts3client/data/c/c
dup
lconst_0
aload 0
getfield com/teamspeak/ts3client/data/c/d/d Ljava/io/File;
aload 0
getfield com/teamspeak/ts3client/data/c/d/e Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/c/c/<init>(JLjava/io/File;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 2
.limit stack 8
.end method

.method private b(J)Z
aload 0
getfield com/teamspeak/ts3client/data/c/d/c Ljava/util/HashMap;
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final a(J)Lcom/teamspeak/ts3client/data/c/c;
aload 0
getfield com/teamspeak/ts3client/data/c/d/c Ljava/util/HashMap;
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/data/c/d/c Ljava/util/HashMap;
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/data/c/c
areturn
L0:
new com/teamspeak/ts3client/data/c/c
dup
lload 1
aload 0
getfield com/teamspeak/ts3client/data/c/d/d Ljava/io/File;
aload 0
getfield com/teamspeak/ts3client/data/c/d/e Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/c/c/<init>(JLjava/io/File;Lcom/teamspeak/ts3client/Ts3Application;)V
astore 3
aload 0
getfield com/teamspeak/ts3client/data/c/d/c Ljava/util/HashMap;
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aload 3
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/teamspeak/ts3client/data/c/d/c Ljava/util/HashMap;
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/data/c/c
areturn
.limit locals 4
.limit stack 6
.end method
