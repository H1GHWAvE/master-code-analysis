.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/d/q
.super java/lang/Object

.field private static 'a' Ljava/util/Vector;

.field private static 'b' Ljava/util/Vector;

.field private static 'c' Lcom/a/b/d/bw;

.method static <clinit>()V
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putstatic com/teamspeak/ts3client/data/d/q/a Ljava/util/Vector;
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putstatic com/teamspeak/ts3client/data/d/q/b Ljava/util/Vector;
invokestatic com/a/b/d/hy/a()Lcom/a/b/d/hy;
putstatic com/teamspeak/ts3client/data/d/q/c Lcom/a/b/d/bw;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a()V
getstatic com/teamspeak/ts3client/data/d/q/a Ljava/util/Vector;
invokevirtual java/util/Vector/iterator()Ljava/util/Iterator;
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/app/Dialog
invokevirtual android/app/Dialog/dismiss()V
goto L0
L1:
getstatic com/teamspeak/ts3client/data/d/q/a Ljava/util/Vector;
invokevirtual java/util/Vector/clear()V
getstatic com/teamspeak/ts3client/data/d/q/b Ljava/util/Vector;
invokevirtual java/util/Vector/iterator()Ljava/util/Iterator;
astore 0
L2:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/v4/app/ax
astore 1
aload 1
invokevirtual android/support/v4/app/ax/k()Z
ifeq L2
aload 1
invokevirtual android/support/v4/app/ax/b()V
goto L2
L3:
getstatic com/teamspeak/ts3client/data/d/q/b Ljava/util/Vector;
invokevirtual java/util/Vector/clear()V
getstatic com/teamspeak/ts3client/data/d/q/c Lcom/a/b/d/bw;
invokeinterface com/a/b/d/bw/clear()V 0
return
.limit locals 2
.limit stack 1
.end method

.method public static a(Landroid/app/Dialog;)V
aload 0
new com/teamspeak/ts3client/data/d/r
dup
aload 0
invokespecial com/teamspeak/ts3client/data/d/r/<init>(Landroid/app/Dialog;)V
invokevirtual android/app/Dialog/setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
getstatic com/teamspeak/ts3client/data/d/q/a Ljava/util/Vector;
aload 0
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method public static a(Landroid/support/v4/app/ax;)V
getstatic com/teamspeak/ts3client/data/d/q/b Ljava/util/Vector;
aload 0
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
return
.limit locals 1
.limit stack 2
.end method

.method public static a(Landroid/support/v4/app/ax;Ljava/lang/String;)V
getstatic com/teamspeak/ts3client/data/d/q/b Ljava/util/Vector;
aload 0
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
getstatic com/teamspeak/ts3client/data/d/q/c Lcom/a/b/d/bw;
aload 1
aload 0
invokeinterface com/a/b/d/bw/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 2
.limit stack 3
.end method

.method public static a(Ljava/lang/String;)V
getstatic com/teamspeak/ts3client/data/d/q/c Lcom/a/b/d/bw;
aload 0
invokeinterface com/a/b/d/bw/containsKey(Ljava/lang/Object;)Z 1
ifeq L0
getstatic com/teamspeak/ts3client/data/d/q/c Lcom/a/b/d/bw;
aload 0
invokeinterface com/a/b/d/bw/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast android/support/v4/app/ax
invokevirtual android/support/v4/app/ax/b()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic b()Ljava/util/Vector;
getstatic com/teamspeak/ts3client/data/d/q/a Ljava/util/Vector;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static b(Landroid/support/v4/app/ax;)V
getstatic com/teamspeak/ts3client/data/d/q/b Ljava/util/Vector;
aload 0
invokevirtual java/util/Vector/remove(Ljava/lang/Object;)Z
pop
getstatic com/teamspeak/ts3client/data/d/q/c Lcom/a/b/d/bw;
invokeinterface com/a/b/d/bw/b()Lcom/a/b/d/bw; 0
aload 0
invokeinterface com/a/b/d/bw/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
return
.limit locals 1
.limit stack 2
.end method

.method private static b(Ljava/lang/String;)V
getstatic com/teamspeak/ts3client/data/d/q/c Lcom/a/b/d/bw;
aload 0
invokeinterface com/a/b/d/bw/containsKey(Ljava/lang/Object;)Z 1
ifeq L0
getstatic com/teamspeak/ts3client/data/d/q/b Ljava/util/Vector;
getstatic com/teamspeak/ts3client/data/d/q/c Lcom/a/b/d/bw;
getstatic com/teamspeak/ts3client/data/d/q/c Lcom/a/b/d/bw;
invokeinterface com/a/b/d/bw/get(Ljava/lang/Object;)Ljava/lang/Object; 1
invokevirtual java/util/Vector/remove(Ljava/lang/Object;)Z
pop
getstatic com/teamspeak/ts3client/data/d/q/c Lcom/a/b/d/bw;
aload 0
invokeinterface com/a/b/d/bw/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
L0:
return
.limit locals 1
.limit stack 3
.end method
