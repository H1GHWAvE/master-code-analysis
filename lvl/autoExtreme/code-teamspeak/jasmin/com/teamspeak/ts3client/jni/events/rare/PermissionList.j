.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/rare/PermissionList
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field public 'a' I

.field public 'b' Ljava/lang/String;

.field private 'c' J

.field private 'd' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JILjava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/rare/PermissionList/c J
aload 0
iload 3
putfield com/teamspeak/ts3client/jni/events/rare/PermissionList/a I
aload 0
aload 4
putfield com/teamspeak/ts3client/jni/events/rare/PermissionList/b Ljava/lang/String;
aload 0
aload 5
putfield com/teamspeak/ts3client/jni/events/rare/PermissionList/d Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 6
.limit stack 3
.end method

.method private a()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/PermissionList/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/PermissionList/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/PermissionList/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/PermissionList/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "PermissionList [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/PermissionList/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", permissionID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/PermissionList/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", permissionName="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/PermissionList/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", permissionDescription="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/PermissionList/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
