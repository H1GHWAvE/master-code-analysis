.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field private 'a' J

.field private 'b' I

.field private 'c' Ljava/lang/String;

.field private 'd' Ljava/lang/String;

.field private 'e' J

.field private 'f' I

.field private 'g' Ljava/lang/String;

.field private 'h' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JILjava/lang/String;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/a J
aload 0
iload 3
putfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/b I
aload 0
aload 4
putfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/c Ljava/lang/String;
aload 0
aload 5
putfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/d Ljava/lang/String;
aload 0
lload 6
putfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/e J
aload 0
iload 8
putfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/f I
aload 0
aload 9
putfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/g Ljava/lang/String;
aload 0
aload 10
putfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/h Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 11
.limit stack 3
.end method

.method private e()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private f()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/h Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/e J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final d()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/g Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ServerGroupClientDeleted [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/a J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", clientID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", clientName="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", clientUniqueIdentity="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", serverGroupID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/e J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", invokerClientID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/f I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", invokerName="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/g Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", invokerUniqueIdentity="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted/h Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
