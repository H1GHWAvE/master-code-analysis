.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/NewChannel
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field 'a' J

.field 'b' J

.field 'c' J

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JJJ)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/NewChannel/a J
aload 0
lload 3
putfield com/teamspeak/ts3client/jni/events/NewChannel/b J
aload 0
lload 5
putfield com/teamspeak/ts3client/jni/events/NewChannel/c J
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 7
.limit stack 3
.end method

.method private a()J
aload 0
getfield com/teamspeak/ts3client/jni/events/NewChannel/b J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private b()J
aload 0
getfield com/teamspeak/ts3client/jni/events/NewChannel/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private c()J
aload 0
getfield com/teamspeak/ts3client/jni/events/NewChannel/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "NewChannel [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/NewChannel/a J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", channelID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/NewChannel/b J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", channelParentID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/NewChannel/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
