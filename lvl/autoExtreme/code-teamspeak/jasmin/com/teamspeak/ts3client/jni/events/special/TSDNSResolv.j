.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/special/TSDNSResolv
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field private 'a' Ljava/lang/String;

.field private 'b' I

.field private 'c' Ljava/lang/String;

.field private 'd' Z

.field private 'e' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(Ljava/lang/String;ILjava/lang/String;ZI)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/a Ljava/lang/String;
aload 0
iload 2
putfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/b I
aload 0
aload 3
putfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/c Ljava/lang/String;
aload 0
iload 4
putfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/d Z
aload 0
iload 5
putfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/e I
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 6
.limit stack 2
.end method

.method private d()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Z
aload 0
getfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/d Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a()I
aload 0
getfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()I
aload 0
getfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "TSDNSResolv [ip="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", port="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", query="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", wasTSDNS="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/d Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
ldc ", error="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/special/TSDNSResolv/e I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
