.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/ServerError
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field public 'a' Ljava/lang/String;

.field public 'b' I

.field public 'c' Ljava/lang/String;

.field private 'd' J

.field private 'e' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/ServerError/d J
aload 0
aload 3
putfield com/teamspeak/ts3client/jni/events/ServerError/a Ljava/lang/String;
aload 0
iload 4
putfield com/teamspeak/ts3client/jni/events/ServerError/b I
aload 0
aload 5
putfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
aload 0
aload 6
putfield com/teamspeak/ts3client/jni/events/ServerError/e Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 7
.limit stack 3
.end method

.method private e()J
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerError/d J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final a()I
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerError/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerError/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerError/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final d()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ServerError [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerError/d J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", errorMessage="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerError/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", error="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerError/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", returnCode="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", extraMessage="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerError/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
