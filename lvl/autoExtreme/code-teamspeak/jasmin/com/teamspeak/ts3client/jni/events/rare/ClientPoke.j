.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/rare/ClientPoke
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field private 'a' J

.field private 'b' I

.field private 'c' Ljava/lang/String;

.field private 'd' Ljava/lang/String;

.field private 'e' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/a J
aload 0
iload 3
putfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/b I
aload 0
aload 4
putfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/c Ljava/lang/String;
aload 0
aload 5
putfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/d Ljava/lang/String;
aload 0
aload 6
putfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/e Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 7
.limit stack 3
.end method

.method private d()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final a()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ClientPoke [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/a J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", fromClientID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", pokerName="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", pokerUniqueIdentity="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", message="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientPoke/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
