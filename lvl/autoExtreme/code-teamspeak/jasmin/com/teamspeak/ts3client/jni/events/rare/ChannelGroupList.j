.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/rare/ChannelGroupList
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field private 'a' J

.field private 'b' J

.field private 'c' Ljava/lang/String;

.field private 'd' I

.field private 'e' J

.field private 'f' I

.field private 'g' I

.field private 'h' I

.field private 'i' I

.field private 'j' I

.field private 'k' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JJLjava/lang/String;IJIIIIII)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/a J
aload 0
lload 3
putfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/b J
aload 0
aload 5
putfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/c Ljava/lang/String;
aload 0
iload 6
putfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/d I
aload 0
ldc2_w 4294967295L
lload 7
land
putfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/e J
aload 0
iload 9
putfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/f I
aload 0
iload 10
putfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/g I
aload 0
iload 11
putfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/h I
aload 0
iload 12
putfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/i I
aload 0
iload 13
putfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/j I
aload 0
iload 14
putfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/k I
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 15
.limit stack 5
.end method

.method private c()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/b J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private d()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/h I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/j I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private g()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/k I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/i I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private j()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private k()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/g I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private l()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Lcom/teamspeak/ts3client/data/a/a;
new com/teamspeak/ts3client/data/a/a
dup
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/a J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/b J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/c Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/d I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/e J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/f I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/g I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/h I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/i I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/j I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/k I
invokespecial com/teamspeak/ts3client/data/a/a/<init>(JJLjava/lang/String;IJIIIIII)V
areturn
.limit locals 1
.limit stack 16
.end method

.method public final b()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/e J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ChannelGroupList [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/a J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", channelGroupID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/b J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", name="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", type="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/d I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", iconID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/e J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", saveDB="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/f I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", sortID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/g I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", nameMode="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/h I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", neededModifyPower="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/i I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", neededMemberAddPower="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/j I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", neededMemberRemovePower="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ChannelGroupList/k I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
