.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/chat/j
.super android/support/v4/app/Fragment
.implements com/teamspeak/ts3client/data/d/s
.implements com/teamspeak/ts3client/data/w
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "ValidFragment" 
.end annotation

.field 'a' Ljava/util/regex/Pattern;

.field private 'at' Ljava/lang/String;

.field private 'au' Z

.field private 'av' Z

.field private 'aw' I

.field 'b' Ljava/util/regex/Pattern;

.field 'c' Landroid/database/DataSetObserver;

.field private 'd' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'e' Lcom/teamspeak/ts3client/chat/MListView;

.field private 'f' Lcom/teamspeak/ts3client/chat/h;

.field private 'g' Lcom/teamspeak/ts3client/chat/a;

.field private 'h' Landroid/widget/TextView;

.field private 'i' Landroid/widget/EditText;

.field private 'j' Landroid/widget/ImageButton;

.field private 'k' Landroid/widget/ImageButton;

.field private 'l' Landroid/view/View;

.field private 'm' Landroid/view/View;

.method public <init>(Lcom/teamspeak/ts3client/chat/a;)V
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "ValidFragment" 
.end annotation
aload 0
invokespecial android/support/v4/app/Fragment/<init>()V
aload 0
ldc "(ts3server://[^\\s\"]{3,}|(?:(?:(?:http://|https://|ftp://)(?:\\w+:\\w+@)?)|www\\.|ftp\\.)[^ !\"#$%&'()*+,/:;<=>?@\\\\\\[\\\\\\]^_`{|}~\u0008\u0009\n\u000c\r]{3,}(?::[0-9]+)?(?:/[^\\s\"]*)?)"
iconst_2
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;
putfield com/teamspeak/ts3client/chat/j/a Ljava/util/regex/Pattern;
aload 0
ldc "((^|\\s)([A-Z0-9._%+-]+@[A-Z0-9\u00c4\u00d6\u00dc.-]+\\.[A-Z]{2,4})(\\s|$)?)"
iconst_2
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;
putfield com/teamspeak/ts3client/chat/j/b Ljava/util/regex/Pattern;
aload 0
new com/teamspeak/ts3client/chat/k
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/k/<init>(Lcom/teamspeak/ts3client/chat/j;)V
putfield com/teamspeak/ts3client/chat/j/c Landroid/database/DataSetObserver;
aload 0
iconst_0
putfield com/teamspeak/ts3client/chat/j/aw I
aload 0
aload 1
putfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
return
.limit locals 2
.limit stack 4
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/chat/j;I)I
aload 0
iload 1
putfield com/teamspeak/ts3client/chat/j/aw I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a()V
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
return
L0:
new java/lang/StringBuffer
dup
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/String/length()I
invokespecial java/lang/StringBuffer/<init>(I)V
astore 1
aload 0
getfield com/teamspeak/ts3client/chat/j/a Ljava/util/regex/Pattern;
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 2
L1:
aload 2
invokevirtual java/util/regex/Matcher/find()Z
ifeq L2
aload 2
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 3
aload 2
aload 1
new java/lang/StringBuilder
dup
ldc "[URL]"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "[/URL]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic java/util/regex/Matcher/quoteReplacement(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/util/regex/Matcher/appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;
pop
goto L1
L2:
aload 2
aload 1
invokevirtual java/util/regex/Matcher/appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
pop
new java/lang/StringBuffer
dup
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/String/length()I
invokespecial java/lang/StringBuffer/<init>(I)V
astore 2
aload 0
getfield com/teamspeak/ts3client/chat/j/b Ljava/util/regex/Pattern;
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 1
L3:
aload 1
invokevirtual java/util/regex/Matcher/find()Z
ifeq L4
aload 1
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 3
aload 1
aload 2
new java/lang/StringBuilder
dup
ldc "[URL=mailto:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "[/URL]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic java/util/regex/Matcher/quoteReplacement(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/util/regex/Matcher/appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;
pop
goto L3
L4:
aload 1
aload 2
invokevirtual java/util/regex/Matcher/appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
pop
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/e Ljava/lang/String;
ldc "CHANNEL"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/g J
ldc "Send Message to Channel"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestSendChannelTextMsg(JLjava/lang/String;JLjava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aN Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
ldc ""
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L6:
aload 0
getfield com/teamspeak/ts3client/chat/j/h Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/a Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
ldc ""
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
return
L5:
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/e Ljava/lang/String;
ldc "SERVER"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
ldc "Send Message to Server"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestSendServerTextMsg(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aP Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
ldc ""
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L6
L7:
aload 0
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
putfield com/teamspeak/ts3client/chat/j/at Ljava/lang/String;
aload 0
iconst_1
invokespecial com/teamspeak/ts3client/chat/j/a(Z)V
goto L6
.limit locals 4
.limit stack 8
.end method

.method private a(Z)V
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 0
getfield com/teamspeak/ts3client/chat/j/at Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
new java/lang/StringBuilder
dup
ldc "Send Message to Client "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestSendPrivateTextMsg(JLjava/lang/String;ILjava/lang/String;)I
pop
iload 1
ifeq L0
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aL Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/a Ljava/lang/String;
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L0:
return
.limit locals 2
.limit stack 8
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/chat/j;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/chat/j/h Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/chat/MListView;
aload 0
getfield com/teamspeak/ts3client/chat/j/e Lcom/teamspeak/ts3client/chat/MListView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/chat/j;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
new java/lang/StringBuffer
dup
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/String/length()I
invokespecial java/lang/StringBuffer/<init>(I)V
astore 1
aload 0
getfield com/teamspeak/ts3client/chat/j/a Ljava/util/regex/Pattern;
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 2
L1:
aload 2
invokevirtual java/util/regex/Matcher/find()Z
ifeq L2
aload 2
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 3
aload 2
aload 1
new java/lang/StringBuilder
dup
ldc "[URL]"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "[/URL]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic java/util/regex/Matcher/quoteReplacement(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/util/regex/Matcher/appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;
pop
goto L1
L2:
aload 2
aload 1
invokevirtual java/util/regex/Matcher/appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
pop
new java/lang/StringBuffer
dup
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/String/length()I
invokespecial java/lang/StringBuffer/<init>(I)V
astore 2
aload 0
getfield com/teamspeak/ts3client/chat/j/b Ljava/util/regex/Pattern;
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 1
L3:
aload 1
invokevirtual java/util/regex/Matcher/find()Z
ifeq L4
aload 1
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 3
aload 1
aload 2
new java/lang/StringBuilder
dup
ldc "[URL=mailto:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "[/URL]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic java/util/regex/Matcher/quoteReplacement(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/util/regex/Matcher/appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;
pop
goto L3
L4:
aload 1
aload 2
invokevirtual java/util/regex/Matcher/appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
pop
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/e Ljava/lang/String;
ldc "CHANNEL"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/g J
ldc "Send Message to Channel"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestSendChannelTextMsg(JLjava/lang/String;JLjava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aN Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
ldc ""
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
L6:
aload 0
getfield com/teamspeak/ts3client/chat/j/h Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/a Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
ldc ""
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
L0:
return
L5:
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/e Ljava/lang/String;
ldc "SERVER"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
ldc "Send Message to Server"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestSendServerTextMsg(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aP Lcom/teamspeak/ts3client/jni/h;
new com/teamspeak/ts3client/a/o
dup
ldc ""
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
goto L6
L7:
aload 0
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
putfield com/teamspeak/ts3client/chat/j/at Ljava/lang/String;
aload 0
iconst_1
invokespecial com/teamspeak/ts3client/chat/j/a(Z)V
goto L6
.limit locals 4
.limit stack 8
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/chat/j;)Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/chat/j;)I
aload 0
getfield com/teamspeak/ts3client/chat/j/aw I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/chat/h;
aload 0
getfield com/teamspeak/ts3client/chat/j/f Lcom/teamspeak/ts3client/chat/h;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/chat/j/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 0
invokevirtual com/teamspeak/ts3client/chat/j/n()V
aload 1
ldc_w 2130903079
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 1
aload 0
aload 1
ldc_w 2131493104
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/chat/j/h Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/chat/j/h Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/a Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
aload 1
ldc_w 2131493107
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
putfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
new com/teamspeak/ts3client/chat/n
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/n/<init>(Lcom/teamspeak/ts3client/chat/j;)V
invokevirtual android/widget/EditText/addTextChangedListener(Landroid/text/TextWatcher;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
new com/teamspeak/ts3client/chat/o
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/o/<init>(Lcom/teamspeak/ts3client/chat/j;)V
invokevirtual android/widget/EditText/setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V
aload 0
aload 1
ldc_w 2131493109
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageButton
putfield com/teamspeak/ts3client/chat/j/j Landroid/widget/ImageButton;
aload 0
aload 1
ldc_w 2131493108
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageButton
putfield com/teamspeak/ts3client/chat/j/k Landroid/widget/ImageButton;
aload 0
getfield com/teamspeak/ts3client/chat/j/k Landroid/widget/ImageButton;
new com/teamspeak/ts3client/chat/q
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/q/<init>(Lcom/teamspeak/ts3client/chat/j;)V
invokevirtual android/widget/ImageButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493110
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
putfield com/teamspeak/ts3client/chat/j/m Landroid/view/View;
aload 0
getfield com/teamspeak/ts3client/chat/j/m Landroid/view/View;
new com/teamspeak/ts3client/chat/r
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/r/<init>(Lcom/teamspeak/ts3client/chat/j;)V
invokevirtual android/view/View/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493111
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
putfield com/teamspeak/ts3client/chat/j/l Landroid/view/View;
aload 0
getfield com/teamspeak/ts3client/chat/j/l Landroid/view/View;
new com/teamspeak/ts3client/chat/s
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/s/<init>(Lcom/teamspeak/ts3client/chat/j;)V
invokevirtual android/view/View/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493105
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/chat/MListView
putfield com/teamspeak/ts3client/chat/j/e Lcom/teamspeak/ts3client/chat/MListView;
aload 0
getfield com/teamspeak/ts3client/chat/j/f Lcom/teamspeak/ts3client/chat/h;
ifnonnull L0
aload 0
new com/teamspeak/ts3client/chat/h
dup
aload 0
invokevirtual com/teamspeak/ts3client/chat/j/i()Landroid/support/v4/app/bb;
invokespecial com/teamspeak/ts3client/chat/h/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/chat/j/f Lcom/teamspeak/ts3client/chat/h;
L0:
aload 0
getfield com/teamspeak/ts3client/chat/j/f Lcom/teamspeak/ts3client/chat/h;
astore 2
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
astore 3
aload 2
aload 3
putfield com/teamspeak/ts3client/chat/h/b Lcom/teamspeak/ts3client/chat/a;
aload 3
aload 2
putfield com/teamspeak/ts3client/chat/a/f Lcom/teamspeak/ts3client/chat/h;
aload 2
aload 3
getfield com/teamspeak/ts3client/chat/a/c Ljava/util/ArrayList;
putfield com/teamspeak/ts3client/chat/h/a Ljava/util/ArrayList;
aload 2
invokevirtual com/teamspeak/ts3client/chat/h/a()V
aload 0
getfield com/teamspeak/ts3client/chat/j/e Lcom/teamspeak/ts3client/chat/MListView;
aload 0
getfield com/teamspeak/ts3client/chat/j/f Lcom/teamspeak/ts3client/chat/h;
invokevirtual com/teamspeak/ts3client/chat/MListView/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/e Lcom/teamspeak/ts3client/chat/MListView;
iconst_1
invokevirtual com/teamspeak/ts3client/chat/MListView/setItemsCanFocus(Z)V
aload 0
getfield com/teamspeak/ts3client/chat/j/e Lcom/teamspeak/ts3client/chat/MListView;
new com/teamspeak/ts3client/chat/t
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/t/<init>(Lcom/teamspeak/ts3client/chat/j;)V
invokevirtual com/teamspeak/ts3client/chat/MListView/setOnSizeChangedListener(Lcom/teamspeak/ts3client/chat/x;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/e Lcom/teamspeak/ts3client/chat/MListView;
new com/teamspeak/ts3client/chat/v
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/v/<init>(Lcom/teamspeak/ts3client/chat/j;)V
invokevirtual com/teamspeak/ts3client/chat/MListView/setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/e Lcom/teamspeak/ts3client/chat/MListView;
iconst_1
invokevirtual com/teamspeak/ts3client/chat/MListView/setTranscriptMode(I)V
aload 0
getfield com/teamspeak/ts3client/chat/j/j Landroid/widget/ImageButton;
new com/teamspeak/ts3client/chat/w
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/w/<init>(Lcom/teamspeak/ts3client/chat/j;)V
invokevirtual android/widget/ImageButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
invokevirtual com/teamspeak/ts3client/chat/a/b()V
aload 1
areturn
.limit locals 4
.limit stack 4
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/a(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
aload 1
invokeinterface android/view/Menu/clear()V 0
aload 0
aload 1
aload 2
invokespecial android/support/v4/app/Fragment/a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
aload 1
instanceof com/teamspeak/ts3client/jni/events/TextMessage
ifeq L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/TextMessage
astore 2
aload 1
checkcast com/teamspeak/ts3client/jni/events/TextMessage
getfield com/teamspeak/ts3client/jni/events/TextMessage/a I
iconst_1
if_icmpne L0
aload 2
getfield com/teamspeak/ts3client/jni/events/TextMessage/c I
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/h I
if_icmpne L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/TextMessage
getfield com/teamspeak/ts3client/jni/events/TextMessage/b I
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
if_icmpne L0
new com/teamspeak/ts3client/chat/y
dup
aload 2
getfield com/teamspeak/ts3client/jni/events/TextMessage/d Ljava/lang/String;
aload 2
getfield com/teamspeak/ts3client/jni/events/TextMessage/e Ljava/lang/String;
aload 2
getfield com/teamspeak/ts3client/jni/events/TextMessage/f Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial com/teamspeak/ts3client/chat/y/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
astore 2
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
aload 2
invokevirtual com/teamspeak/ts3client/chat/a/a(Lcom/teamspeak/ts3client/chat/y;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
aload 0
iconst_0
putfield com/teamspeak/ts3client/chat/j/au Z
L0:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ServerError
ifeq L1
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
astore 2
aload 2
getfield com/teamspeak/ts3client/jni/events/ServerError/a Ljava/lang/String;
ldc "invalid clientID"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 2
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Send Message to Client "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 0
getfield com/teamspeak/ts3client/chat/j/au Z
ifeq L3
aload 0
getfield com/teamspeak/ts3client/chat/j/av Z
ifne L3
aload 0
iconst_1
putfield com/teamspeak/ts3client/chat/j/av Z
new com/teamspeak/ts3client/chat/y
dup
aconst_null
ldc ""
ldc "Chat partner disconnected out of view"
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial com/teamspeak/ts3client/chat/y/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
astore 1
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
aload 1
invokevirtual com/teamspeak/ts3client/chat/a/a(Lcom/teamspeak/ts3client/chat/y;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
L4:
return
L3:
aload 0
iconst_1
putfield com/teamspeak/ts3client/chat/j/au Z
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/b Ljava/lang/String;
ldc "Request clientid"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestClientIDs(JLjava/lang/String;Ljava/lang/String;)I
pop
L2:
aload 2
getfield com/teamspeak/ts3client/jni/events/ServerError/a Ljava/lang/String;
ldc "database empty result set"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 2
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
ldc "Request clientid"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 0
getfield com/teamspeak/ts3client/chat/j/av Z
ifne L1
aload 0
iconst_1
putfield com/teamspeak/ts3client/chat/j/av Z
new com/teamspeak/ts3client/chat/y
dup
aconst_null
ldc ""
ldc "Chat partner disconnected out of view"
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial com/teamspeak/ts3client/chat/y/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
astore 2
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
aload 2
invokevirtual com/teamspeak/ts3client/chat/a/a(Lcom/teamspeak/ts3client/chat/y;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
L1:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ClientIDs
ifeq L5
aload 1
checkcast com/teamspeak/ts3client/jni/events/ClientIDs
astore 2
aload 2
getfield com/teamspeak/ts3client/jni/events/ClientIDs/a Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/b Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
aload 0
getfield com/teamspeak/ts3client/chat/j/g Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
aload 2
getfield com/teamspeak/ts3client/jni/events/ClientIDs/b I
putfield com/teamspeak/ts3client/data/c/c I
aload 0
iconst_0
putfield com/teamspeak/ts3client/chat/j/av Z
L5:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ClientIDsFinished
ifeq L4
aload 0
iconst_0
invokespecial com/teamspeak/ts3client/chat/j/a(Z)V
return
.limit locals 3
.limit stack 7
.end method

.method protected final b(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
astore 4
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getSelectionStart()I
istore 2
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getSelectionEnd()I
istore 3
iload 2
iload 3
if_icmpeq L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
iconst_0
iload 2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iconst_0
iconst_3
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iload 2
iload 3
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iconst_3
bipush 7
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iload 3
aload 4
invokevirtual java/lang/String/length()I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
aload 1
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
iload 3
iload 2
iconst_3
iadd
iadd
iload 2
isub
invokevirtual android/widget/EditText/setSelection(I)V
return
L0:
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getSelectionStart()I
aload 4
invokevirtual java/lang/String/length()I
if_icmpne L1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L2:
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
aload 1
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
iload 2
iconst_3
iadd
invokevirtual android/widget/EditText/setSelection(I)V
return
L1:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
iconst_0
iload 2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iload 2
aload 4
invokevirtual java/lang/String/length()I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
goto L2
.limit locals 5
.limit stack 4
.end method

.method public final c(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/c(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final d(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final s()V
aload 0
invokespecial android/support/v4/app/Fragment/s()V
aload 0
getfield com/teamspeak/ts3client/chat/j/f Lcom/teamspeak/ts3client/chat/h;
aload 0
getfield com/teamspeak/ts3client/chat/j/c Landroid/database/DataSetObserver;
invokevirtual com/teamspeak/ts3client/chat/h/registerDataSetObserver(Landroid/database/DataSetObserver;)V
aload 0
getfield com/teamspeak/ts3client/chat/j/e Lcom/teamspeak/ts3client/chat/MListView;
new com/teamspeak/ts3client/chat/m
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/m/<init>(Lcom/teamspeak/ts3client/chat/j;)V
ldc2_w 100L
invokevirtual com/teamspeak/ts3client/chat/MListView/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method public final t()V
aload 0
invokespecial android/support/v4/app/Fragment/t()V
aload 0
getfield com/teamspeak/ts3client/chat/j/d Lcom/teamspeak/ts3client/Ts3Application;
ldc "input_method"
invokevirtual com/teamspeak/ts3client/Ts3Application/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/inputmethod/InputMethodManager
aload 0
getfield com/teamspeak/ts3client/chat/j/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getWindowToken()Landroid/os/IBinder;
iconst_0
invokevirtual android/view/inputmethod/InputMethodManager/hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
pop
aload 0
getfield com/teamspeak/ts3client/chat/j/f Lcom/teamspeak/ts3client/chat/h;
aload 0
getfield com/teamspeak/ts3client/chat/j/c Landroid/database/DataSetObserver;
invokevirtual com/teamspeak/ts3client/chat/h/unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
return
.limit locals 1
.limit stack 3
.end method

.method public final y()V
return
.limit locals 1
.limit stack 0
.end method
