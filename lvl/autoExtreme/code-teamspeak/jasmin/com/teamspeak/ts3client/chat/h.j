.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/chat/h
.super android/widget/BaseAdapter

.field 'a' Ljava/util/ArrayList;

.field 'b' Lcom/teamspeak/ts3client/chat/a;

.field private 'c' Landroid/content/Context;

.field private 'd' Landroid/view/LayoutInflater;

.field private 'e' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'f' Ljava/text/DateFormat;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/chat/h/c Landroid/content/Context;
aload 0
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/chat/h/e Lcom/teamspeak/ts3client/Ts3Application;
aload 0
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield com/teamspeak/ts3client/chat/h/d Landroid/view/LayoutInflater;
aload 0
invokestatic java/text/DateFormat/getDateTimeInstance()Ljava/text/DateFormat;
putfield com/teamspeak/ts3client/chat/h/f Ljava/text/DateFormat;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Lcom/teamspeak/ts3client/chat/a;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/chat/h/b Lcom/teamspeak/ts3client/chat/a;
aload 1
aload 0
putfield com/teamspeak/ts3client/chat/a/f Lcom/teamspeak/ts3client/chat/h;
aload 0
aload 1
getfield com/teamspeak/ts3client/chat/a/c Ljava/util/ArrayList;
putfield com/teamspeak/ts3client/chat/h/a Ljava/util/ArrayList;
aload 0
invokevirtual com/teamspeak/ts3client/chat/h/a()V
return
.limit locals 2
.limit stack 2
.end method

.method public final a()V
aload 0
getfield com/teamspeak/ts3client/chat/h/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/chat/i
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/i/<init>(Lcom/teamspeak/ts3client/chat/h;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 4
.end method

.method public final getCount()I
aload 0
getfield com/teamspeak/ts3client/chat/h/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getItem(I)Ljava/lang/Object;
aload 0
getfield com/teamspeak/ts3client/chat/h/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemId(I)J
lconst_0
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield com/teamspeak/ts3client/chat/h/d Landroid/view/LayoutInflater;
ldc_w 2130903080
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
astore 2
aload 2
ldc_w 2131493112
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
astore 3
aload 2
ldc_w 2131493114
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
astore 4
aload 2
ldc_w 2131493117
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 5
aload 5
invokestatic com/teamspeak/ts3client/data/d/n/a()Landroid/text/method/MovementMethod;
invokevirtual android/widget/TextView/setMovementMethod(Landroid/text/method/MovementMethod;)V
aload 2
ldc_w 2131493118
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 6
aload 2
ldc_w 2131493116
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 7
aload 2
ldc_w 2131493113
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/RelativeLayout
astore 8
aload 0
getfield com/teamspeak/ts3client/chat/h/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/y
getfield com/teamspeak/ts3client/chat/y/f Z
ifeq L0
aload 4
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 3
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 7
ldc ""
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 5
aload 0
getfield com/teamspeak/ts3client/chat/h/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/y
getfield com/teamspeak/ts3client/chat/y/b Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 6
aload 0
getfield com/teamspeak/ts3client/chat/h/f Ljava/text/DateFormat;
aload 0
getfield com/teamspeak/ts3client/chat/h/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/y
getfield com/teamspeak/ts3client/chat/y/e Ljava/util/Date;
invokevirtual java/text/DateFormat/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/chat/h/b Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/e Ljava/lang/String;
ldc "CHANNEL"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
aload 0
getfield com/teamspeak/ts3client/chat/h/b Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/e Ljava/lang/String;
ldc "SERVER"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
aload 8
ldc_w 2130837648
invokevirtual android/widget/RelativeLayout/setBackgroundResource(I)V
L1:
aload 5
iconst_1
invokevirtual android/widget/TextView/setFocusable(Z)V
aload 5
iconst_1
invokevirtual android/widget/TextView/setFocusableInTouchMode(Z)V
aload 2
areturn
L0:
aload 0
getfield com/teamspeak/ts3client/chat/h/b Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/e Ljava/lang/String;
ldc "CHANNEL"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 4
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 3
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 7
new java/lang/StringBuilder
dup
ldc "\""
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/chat/h/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/y
getfield com/teamspeak/ts3client/chat/y/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L3:
aload 5
aload 0
getfield com/teamspeak/ts3client/chat/h/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/y
getfield com/teamspeak/ts3client/chat/y/c Landroid/text/Spanned;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 6
aload 0
getfield com/teamspeak/ts3client/chat/h/f Ljava/text/DateFormat;
aload 0
getfield com/teamspeak/ts3client/chat/h/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/y
getfield com/teamspeak/ts3client/chat/y/e Ljava/util/Date;
invokevirtual java/text/DateFormat/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
goto L1
L2:
aload 0
getfield com/teamspeak/ts3client/chat/h/b Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/e Ljava/lang/String;
ldc "SERVER"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
aload 4
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 3
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 7
new java/lang/StringBuilder
dup
ldc "\""
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/chat/h/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/y
getfield com/teamspeak/ts3client/chat/y/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
goto L3
L4:
aload 7
ldc ""
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/chat/h/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/y
getfield com/teamspeak/ts3client/chat/y/d Z
ifeq L5
aload 3
iconst_4
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 8
ldc_w 2130837649
invokevirtual android/widget/RelativeLayout/setBackgroundResource(I)V
goto L3
L5:
aload 0
getfield com/teamspeak/ts3client/chat/h/b Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/y Landroid/graphics/Bitmap;
ifnull L6
aload 3
aload 0
getfield com/teamspeak/ts3client/chat/h/b Lcom/teamspeak/ts3client/chat/a;
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/y Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
L6:
aload 4
iconst_4
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 8
ldc_w 2130837647
invokevirtual android/widget/RelativeLayout/setBackgroundResource(I)V
goto L3
.limit locals 9
.limit stack 4
.end method
