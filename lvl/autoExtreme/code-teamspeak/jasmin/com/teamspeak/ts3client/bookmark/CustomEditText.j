.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/bookmark/CustomEditText
.super android/widget/EditText

.field 'a' Z

.field 'b' Landroid/graphics/Paint;

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/widget/EditText/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_0
putfield com/teamspeak/ts3client/bookmark/CustomEditText/a Z
aload 0
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield com/teamspeak/ts3client/bookmark/CustomEditText/b Landroid/graphics/Paint;
aload 0
getfield com/teamspeak/ts3client/bookmark/CustomEditText/b Landroid/graphics/Paint;
iconst_1
invokevirtual android/graphics/Paint/setAntiAlias(Z)V
aload 0
getfield com/teamspeak/ts3client/bookmark/CustomEditText/b Landroid/graphics/Paint;
ldc_w -65536
invokevirtual android/graphics/Paint/setColor(I)V
aload 0
getfield com/teamspeak/ts3client/bookmark/CustomEditText/b Landroid/graphics/Paint;
bipush 100
invokevirtual android/graphics/Paint/setAlpha(I)V
aload 0
new com/teamspeak/ts3client/bookmark/d
dup
aload 0
invokespecial com/teamspeak/ts3client/bookmark/d/<init>(Lcom/teamspeak/ts3client/bookmark/CustomEditText;)V
invokevirtual com/teamspeak/ts3client/bookmark/CustomEditText/addTextChangedListener(Landroid/text/TextWatcher;)V
return
.limit locals 3
.limit stack 4
.end method

.method private a()Z
aload 0
getfield com/teamspeak/ts3client/bookmark/CustomEditText/a Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public draw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/widget/EditText/draw(Landroid/graphics/Canvas;)V
aload 0
getfield com/teamspeak/ts3client/bookmark/CustomEditText/a Z
ifeq L0
aload 1
ldc_w 5.0F
ldc_w 5.0F
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/CustomEditText/getWidth()I
iconst_5
isub
i2f
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/CustomEditText/getHeight()I
iconst_5
isub
i2f
aload 0
getfield com/teamspeak/ts3client/bookmark/CustomEditText/b Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L0:
return
.limit locals 2
.limit stack 6
.end method

.method public setInputerror(Z)V
aload 0
iload 1
putfield com/teamspeak/ts3client/bookmark/CustomEditText/a Z
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/CustomEditText/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method
