.bytecode 50.0
.class final synchronized com/a/a/a/a/n
.super java/lang/Object
.implements java/lang/Runnable

.field final synthetic 'a' I

.field final synthetic 'b' Ljava/lang/String;

.field final synthetic 'c' Ljava/lang/String;

.field final synthetic 'd' Lcom/a/a/a/a/l;

.method <init>(Lcom/a/a/a/a/l;ILjava/lang/String;Ljava/lang/String;)V
aload 0
aload 1
putfield com/a/a/a/a/n/d Lcom/a/a/a/a/l;
aload 0
iload 2
putfield com/a/a/a/a/n/a I
aload 0
aload 3
putfield com/a/a/a/a/n/b Ljava/lang/String;
aload 0
aload 4
putfield com/a/a/a/a/n/c Ljava/lang/String;
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 5
.limit stack 2
.end method

.method public final run()V
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/InvalidKeyException from L0 to L1 using L3
.catch java/security/SignatureException from L0 to L1 using L4
.catch com/a/a/a/a/a/b from L0 to L1 using L5
.catch java/lang/IllegalArgumentException from L6 to L7 using L8
.catch java/lang/IllegalArgumentException from L9 to L8 using L8
.catch java/lang/IllegalArgumentException from L10 to L11 using L8
.catch java/lang/IllegalArgumentException from L12 to L13 using L8
.catch java/lang/IllegalArgumentException from L14 to L15 using L8
ldc "LicenseChecker"
ldc "Received response."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/a/a/a/a/n/d Lcom/a/a/a/a/l;
getfield com/a/a/a/a/l/b Lcom/a/a/a/a/k;
invokestatic com/a/a/a/a/k/a(Lcom/a/a/a/a/k;)Ljava/util/Set;
aload 0
getfield com/a/a/a/a/n/d Lcom/a/a/a/a/l;
invokestatic com/a/a/a/a/l/a(Lcom/a/a/a/a/l;)Lcom/a/a/a/a/p;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifeq L16
aload 0
getfield com/a/a/a/a/n/d Lcom/a/a/a/a/l;
invokestatic com/a/a/a/a/l/b(Lcom/a/a/a/a/l;)V
aload 0
getfield com/a/a/a/a/n/d Lcom/a/a/a/a/l;
invokestatic com/a/a/a/a/l/a(Lcom/a/a/a/a/l;)Lcom/a/a/a/a/p;
astore 8
aload 0
getfield com/a/a/a/a/n/d Lcom/a/a/a/a/l;
getfield com/a/a/a/a/l/b Lcom/a/a/a/a/k;
invokestatic com/a/a/a/a/k/b(Lcom/a/a/a/a/k;)Ljava/security/PublicKey;
astore 6
aload 0
getfield com/a/a/a/a/n/a I
istore 1
aload 0
getfield com/a/a/a/a/n/b Ljava/lang/String;
astore 7
aload 0
getfield com/a/a/a/a/n/c Ljava/lang/String;
astore 9
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 5
iload 1
sipush 259
if_icmpne L17
aload 5
iload 1
iconst_1
iadd
invokevirtual java/util/zip/CRC32/update(I)V
L18:
aload 5
invokevirtual java/util/zip/CRC32/getValue()J
lstore 3
aconst_null
astore 5
iload 1
ifeq L0
iload 1
iconst_1
if_icmpeq L0
iload 1
iconst_2
if_icmpne L19
L0:
ldc "SHA1withRSA"
invokestatic java/security/Signature/getInstance(Ljava/lang/String;)Ljava/security/Signature;
astore 5
aload 5
aload 6
invokevirtual java/security/Signature/initVerify(Ljava/security/PublicKey;)V
aload 5
aload 7
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/security/Signature/update([B)V
aload 5
aload 9
invokestatic com/a/a/a/a/a/a/a(Ljava/lang/String;)[B
invokevirtual java/security/Signature/verify([B)Z
ifne L6
ldc "LicenseValidator"
ldc "Signature verification failed."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 8
invokevirtual com/a/a/a/a/p/a()V
L1:
aload 0
getfield com/a/a/a/a/n/d Lcom/a/a/a/a/l;
getfield com/a/a/a/a/l/b Lcom/a/a/a/a/k;
aload 0
getfield com/a/a/a/a/n/d Lcom/a/a/a/a/l;
invokestatic com/a/a/a/a/l/a(Lcom/a/a/a/a/l;)Lcom/a/a/a/a/p;
invokestatic com/a/a/a/a/k/b(Lcom/a/a/a/a/k;Lcom/a/a/a/a/p;)V
L16:
return
L17:
aload 5
iload 1
invokevirtual java/util/zip/CRC32/update(I)V
goto L18
L2:
astore 5
new java/lang/RuntimeException
dup
aload 5
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 5
aload 8
iconst_5
invokevirtual com/a/a/a/a/p/a(I)V
goto L1
L4:
astore 5
new java/lang/RuntimeException
dup
aload 5
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L5:
astore 5
ldc "LicenseValidator"
ldc "Could not Base64-decode signature."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 8
invokevirtual com/a/a/a/a/p/a()V
goto L1
L6:
aload 7
bipush 58
invokevirtual java/lang/String/indexOf(I)I
istore 2
L7:
iconst_m1
iload 2
if_icmpne L10
ldc ""
astore 5
aload 7
astore 6
L9:
aload 6
ldc "|"
invokestatic java/util/regex/Pattern/quote(Ljava/lang/String;)Ljava/lang/String;
invokestatic android/text/TextUtils/split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
astore 10
aload 10
arraylength
bipush 6
if_icmpge L14
new java/lang/IllegalArgumentException
dup
ldc "Wrong number of fields."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L8:
astore 5
ldc "LicenseValidator"
ldc "Could not parse response."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 8
invokevirtual com/a/a/a/a/p/a()V
goto L1
L10:
aload 7
iconst_0
iload 2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 6
iload 2
aload 7
invokevirtual java/lang/String/length()I
if_icmplt L12
L11:
ldc ""
astore 5
goto L9
L12:
aload 7
iload 2
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 5
L13:
goto L9
L14:
new com/a/a/a/a/u
dup
invokespecial com/a/a/a/a/u/<init>()V
astore 6
aload 6
aload 5
putfield com/a/a/a/a/u/g Ljava/lang/String;
aload 6
aload 10
iconst_0
aaload
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putfield com/a/a/a/a/u/a I
aload 6
aload 10
iconst_1
aaload
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putfield com/a/a/a/a/u/b I
aload 6
aload 10
iconst_2
aaload
putfield com/a/a/a/a/u/c Ljava/lang/String;
aload 6
aload 10
iconst_3
aaload
putfield com/a/a/a/a/u/d Ljava/lang/String;
aload 6
aload 10
iconst_4
aaload
putfield com/a/a/a/a/u/e Ljava/lang/String;
aload 6
aload 10
iconst_5
aaload
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/a/a/a/a/u/f J
L15:
aload 6
getfield com/a/a/a/a/u/a I
iload 1
if_icmpeq L20
ldc "LicenseValidator"
ldc "Response codes don't match."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 8
invokevirtual com/a/a/a/a/p/a()V
goto L1
L20:
aload 6
getfield com/a/a/a/a/u/b I
aload 8
getfield com/a/a/a/a/p/b I
if_icmpeq L21
ldc "LicenseValidator"
ldc "Nonce doesn't match."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 8
invokevirtual com/a/a/a/a/p/a()V
goto L1
L21:
aload 6
getfield com/a/a/a/a/u/c Ljava/lang/String;
aload 8
getfield com/a/a/a/a/p/c Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L22
ldc "LicenseValidator"
ldc "Package name doesn't match."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 8
invokevirtual com/a/a/a/a/p/a()V
goto L1
L22:
aload 6
getfield com/a/a/a/a/u/d Ljava/lang/String;
aload 8
getfield com/a/a/a/a/p/d Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L23
ldc "LicenseValidator"
ldc "Version codes don't match."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 8
invokevirtual com/a/a/a/a/p/a()V
goto L1
L23:
aload 6
astore 5
aload 6
getfield com/a/a/a/a/u/e Ljava/lang/String;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L19
ldc "LicenseValidator"
ldc "User identifier is empty."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 8
invokevirtual com/a/a/a/a/p/a()V
goto L1
L19:
lload 3
ldc2_w 3523407757L
lcmp
ifne L24
aload 8
sipush 256
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L24:
lload 3
ldc2_w 3523417757L
lcmp
ifne L25
aload 8
sipush 256
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L25:
lload 3
ldc2_w 3523417787L
lcmp
ifne L26
aload 8
sipush 561
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L26:
lload 3
ldc2_w 2523417757L
lcmp
ifne L27
aload 8
iconst_3
invokevirtual com/a/a/a/a/p/a(I)V
goto L1
L27:
lload 3
ldc2_w 1007455905L
lcmp
ifne L28
aload 8
sipush 256
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L28:
lload 3
ldc2_w 1027455905L
lcmp
ifne L29
aload 8
sipush 291
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L29:
lload 3
ldc2_w 1027455305L
lcmp
ifne L30
aload 8
sipush 256
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L30:
lload 3
ldc2_w 1127455905L
lcmp
ifne L31
aload 8
sipush 256
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L31:
lload 3
ldc2_w 1007455905L
lcmp
ifne L32
aload 8
sipush 561
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L32:
lload 3
ldc2_w 1007455995L
lcmp
ifne L33
aload 8
sipush 561
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L33:
lload 3
ldc2_w 10174558235L
lcmp
ifne L34
aload 8
sipush 291
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L34:
lload 3
ldc2_w 1307455805L
lcmp
ifne L35
aload 8
sipush 561
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L35:
lload 3
ldc2_w 2768625435L
lcmp
ifne L36
aload 8
sipush 291
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L36:
lload 3
ldc2_w 2268628435L
lcmp
ifne L37
aload 8
iconst_3
invokevirtual com/a/a/a/a/p/a(I)V
goto L1
L37:
lload 3
ldc2_w 2168628435L
lcmp
ifne L38
aload 8
sipush 291
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L38:
lload 3
ldc2_w 2768688435L
lcmp
ifne L39
aload 8
sipush 291
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L39:
lload 3
ldc2_w 3580832660L
lcmp
ifne L40
aload 8
sipush 291
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L40:
lload 3
ldc2_w 1580832660L
lcmp
ifne L41
aload 8
sipush 256
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L41:
lload 3
ldc2_w 3580432660L
lcmp
ifne L42
aload 8
sipush 291
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L42:
lload 3
ldc2_w 3580772660L
lcmp
ifne L43
aload 8
sipush 291
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L43:
lload 3
ldc2_w 2724731650L
lcmp
ifne L44
aload 8
sipush 291
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L44:
lload 3
ldc2_w 2724731612L
lcmp
ifne L45
aload 8
iconst_3
invokevirtual com/a/a/a/a/p/a(I)V
goto L1
L45:
lload 3
ldc2_w 2424731612L
lcmp
ifne L46
aload 8
sipush 291
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L46:
lload 3
ldc2_w 2324731612L
lcmp
ifne L47
aload 8
sipush 291
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L47:
lload 3
ldc2_w 1007455905L
lcmp
ifne L48
aload 8
sipush 291
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L48:
lload 3
ldc2_w 1007459905L
lcmp
ifne L49
aload 8
iconst_1
invokevirtual com/a/a/a/a/p/a(I)V
goto L1
L49:
lload 3
ldc2_w 1207455905L
lcmp
ifne L50
aload 8
sipush 256
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
L50:
lload 3
ldc2_w 1037455905L
lcmp
ifne L51
aload 8
iconst_1
invokevirtual com/a/a/a/a/p/a(I)V
goto L1
L51:
lload 3
ldc2_w 3580832660L
lcmp
ifne L52
aload 8
iconst_2
invokevirtual com/a/a/a/a/p/a(I)V
goto L1
L52:
lload 3
ldc2_w 1256060791L
lcmp
ifne L53
aload 8
sipush 561
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L53:
lload 3
ldc2_w 1259062791L
lcmp
ifne L54
aload 8
iconst_2
invokevirtual com/a/a/a/a/p/a(I)V
goto L1
L54:
lload 3
ldc2_w 1259060711L
lcmp
ifne L55
aload 8
sipush 256
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
L55:
lload 3
ldc2_w 1259060791L
lcmp
ifne L56
aload 8
iconst_3
invokevirtual com/a/a/a/a/p/a(I)V
goto L1
L56:
lload 3
ldc2_w 1249060791L
lcmp
ifne L57
aload 8
iconst_3
invokevirtual com/a/a/a/a/p/a(I)V
goto L1
L57:
lload 3
ldc2_w 1259069691L
lcmp
ifne L58
aload 8
iconst_3
invokevirtual com/a/a/a/a/p/a(I)V
goto L1
L58:
lload 3
ldc2_w 1253060741L
lcmp
ifne L59
aload 8
sipush 561
aload 5
aload 7
aload 9
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
goto L1
L59:
ldc "LicenseValidator"
ldc "Unknown response code for license check."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 8
invokevirtual com/a/a/a/a/p/a()V
goto L1
.limit locals 11
.limit stack 5
.end method
