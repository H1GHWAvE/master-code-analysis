.bytecode 50.0
.class public final synchronized com/a/c/b/s
.super java/lang/Object
.implements com/a/c/ap
.implements java/lang/Cloneable

.field public static final 'a' Lcom/a/c/b/s;

.field private static final 'h' D = -1.0D


.field public 'b' D

.field public 'c' I

.field public 'd' Z

.field public 'e' Z

.field public 'f' Ljava/util/List;

.field public 'g' Ljava/util/List;

.method static <clinit>()V
new com/a/c/b/s
dup
invokespecial com/a/c/b/s/<init>()V
putstatic com/a/c/b/s/a Lcom/a/c/b/s;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc2_w -1.0D
putfield com/a/c/b/s/b D
aload 0
sipush 136
putfield com/a/c/b/s/c I
aload 0
iconst_1
putfield com/a/c/b/s/d Z
aload 0
invokestatic java/util/Collections/emptyList()Ljava/util/List;
putfield com/a/c/b/s/f Ljava/util/List;
aload 0
invokestatic java/util/Collections/emptyList()Ljava/util/List;
putfield com/a/c/b/s/g Ljava/util/List;
return
.limit locals 1
.limit stack 3
.end method

.method private a(D)Lcom/a/c/b/s;
aload 0
invokevirtual com/a/c/b/s/a()Lcom/a/c/b/s;
astore 3
aload 3
dload 1
putfield com/a/c/b/s/b D
aload 3
areturn
.limit locals 4
.limit stack 3
.end method

.method private transient a([I)Lcom/a/c/b/s;
iconst_0
istore 2
aload 0
invokevirtual com/a/c/b/s/a()Lcom/a/c/b/s;
astore 4
aload 4
iconst_0
putfield com/a/c/b/s/c I
aload 1
arraylength
istore 3
L0:
iload 2
iload 3
if_icmpge L1
aload 4
aload 1
iload 2
iaload
aload 4
getfield com/a/c/b/s/c I
ior
putfield com/a/c/b/s/c I
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 4
areturn
.limit locals 5
.limit stack 3
.end method

.method private a(Lcom/a/c/a/d;)Z
aload 1
ifnull L0
aload 1
invokeinterface com/a/c/a/d/a()D 0
aload 0
getfield com/a/c/b/s/b D
dcmpl
ifle L0
iconst_0
ireturn
L0:
iconst_1
ireturn
.limit locals 2
.limit stack 4
.end method

.method private a(Lcom/a/c/a/e;)Z
aload 1
ifnull L0
aload 1
invokeinterface com/a/c/a/e/a()D 0
aload 0
getfield com/a/c/b/s/b D
dcmpg
ifgt L0
iconst_0
ireturn
L0:
iconst_1
ireturn
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/lang/Class;)Z
ldc java/lang/Enum
aload 0
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifne L0
aload 0
invokevirtual java/lang/Class/isAnonymousClass()Z
ifne L1
aload 0
invokevirtual java/lang/Class/isLocalClass()Z
ifeq L0
L1:
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private a(Ljava/lang/reflect/Field;Z)Z
aload 0
getfield com/a/c/b/s/c I
aload 1
invokevirtual java/lang/reflect/Field/getModifiers()I
iand
ifeq L0
iconst_1
ireturn
L0:
aload 0
getfield com/a/c/b/s/b D
ldc2_w -1.0D
dcmpl
ifeq L1
aload 0
aload 1
ldc com/a/c/a/d
invokevirtual java/lang/reflect/Field/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast com/a/c/a/d
aload 1
ldc com/a/c/a/e
invokevirtual java/lang/reflect/Field/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast com/a/c/a/e
invokevirtual com/a/c/b/s/a(Lcom/a/c/a/d;Lcom/a/c/a/e;)Z
ifne L1
iconst_1
ireturn
L1:
aload 1
invokevirtual java/lang/reflect/Field/isSynthetic()Z
ifeq L2
iconst_1
ireturn
L2:
aload 0
getfield com/a/c/b/s/e Z
ifeq L3
aload 1
ldc com/a/c/a/a
invokevirtual java/lang/reflect/Field/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast com/a/c/a/a
astore 3
aload 3
ifnull L4
iload 2
ifeq L5
aload 3
invokeinterface com/a/c/a/a/a()Z 0
ifne L3
L4:
iconst_1
ireturn
L5:
aload 3
invokeinterface com/a/c/a/a/b()Z 0
ifeq L4
L3:
aload 0
getfield com/a/c/b/s/d Z
ifne L6
aload 1
invokevirtual java/lang/reflect/Field/getType()Ljava/lang/Class;
invokestatic com/a/c/b/s/b(Ljava/lang/Class;)Z
ifeq L6
iconst_1
ireturn
L6:
aload 1
invokevirtual java/lang/reflect/Field/getType()Ljava/lang/Class;
invokestatic com/a/c/b/s/a(Ljava/lang/Class;)Z
ifeq L7
iconst_1
ireturn
L7:
iload 2
ifeq L8
aload 0
getfield com/a/c/b/s/f Ljava/util/List;
astore 3
L9:
aload 3
invokeinterface java/util/List/isEmpty()Z 0
ifne L10
new com/a/c/c
dup
aload 1
invokespecial com/a/c/c/<init>(Ljava/lang/reflect/Field;)V
pop
aload 3
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L11:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L10
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/c/b
invokeinterface com/a/c/b/a()Z 0
ifeq L11
iconst_1
ireturn
L8:
aload 0
getfield com/a/c/b/s/g Ljava/util/List;
astore 3
goto L9
L10:
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method

.method private b()Lcom/a/c/b/s;
aload 0
invokevirtual com/a/c/b/s/a()Lcom/a/c/b/s;
astore 1
aload 1
iconst_0
putfield com/a/c/b/s/d Z
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public static b(Ljava/lang/Class;)Z
aload 0
invokevirtual java/lang/Class/isMemberClass()Z
ifeq L0
aload 0
invokevirtual java/lang/Class/getModifiers()I
bipush 8
iand
ifeq L1
iconst_1
istore 1
L2:
iload 1
ifne L0
iconst_1
ireturn
L1:
iconst_0
istore 1
goto L2
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private c()Lcom/a/c/b/s;
aload 0
invokevirtual com/a/c/b/s/a()Lcom/a/c/b/s;
astore 1
aload 1
iconst_1
putfield com/a/c/b/s/e Z
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/lang/Class;)Z
aload 0
invokevirtual java/lang/Class/getModifiers()I
bipush 8
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an;
aload 2
getfield com/a/c/c/a/a Ljava/lang/Class;
astore 5
aload 0
aload 5
iconst_1
invokevirtual com/a/c/b/s/a(Ljava/lang/Class;Z)Z
istore 3
aload 0
aload 5
iconst_0
invokevirtual com/a/c/b/s/a(Ljava/lang/Class;Z)Z
istore 4
iload 3
ifne L0
iload 4
ifne L0
aconst_null
areturn
L0:
new com/a/c/b/t
dup
aload 0
iload 4
iload 3
aload 1
aload 2
invokespecial com/a/c/b/t/<init>(Lcom/a/c/b/s;ZZLcom/a/c/k;Lcom/a/c/c/a;)V
areturn
.limit locals 6
.limit stack 7
.end method

.method public final a()Lcom/a/c/b/s;
.catch java/lang/CloneNotSupportedException from L0 to L1 using L2
L0:
aload 0
invokespecial java/lang/Object/clone()Ljava/lang/Object;
checkcast com/a/c/b/s
astore 1
L1:
aload 1
areturn
L2:
astore 1
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/a/c/b;ZZ)Lcom/a/c/b/s;
aload 0
invokevirtual com/a/c/b/s/a()Lcom/a/c/b/s;
astore 4
iload 2
ifeq L0
aload 4
new java/util/ArrayList
dup
aload 0
getfield com/a/c/b/s/f Ljava/util/List;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
putfield com/a/c/b/s/f Ljava/util/List;
aload 4
getfield com/a/c/b/s/f Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L0:
iload 3
ifeq L1
aload 4
new java/util/ArrayList
dup
aload 0
getfield com/a/c/b/s/g Ljava/util/List;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
putfield com/a/c/b/s/g Ljava/util/List;
aload 4
getfield com/a/c/b/s/g Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L1:
aload 4
areturn
.limit locals 5
.limit stack 4
.end method

.method public final a(Lcom/a/c/a/d;Lcom/a/c/a/e;)Z
aload 1
ifnull L0
aload 1
invokeinterface com/a/c/a/d/a()D 0
aload 0
getfield com/a/c/b/s/b D
dcmpl
ifle L0
iconst_0
istore 3
L1:
iload 3
ifeq L2
aload 2
ifnull L3
aload 2
invokeinterface com/a/c/a/e/a()D 0
aload 0
getfield com/a/c/b/s/b D
dcmpg
ifgt L3
iconst_0
istore 3
L4:
iload 3
ifeq L2
iconst_1
ireturn
L0:
iconst_1
istore 3
goto L1
L3:
iconst_1
istore 3
goto L4
L2:
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final a(Ljava/lang/Class;Z)Z
aload 0
getfield com/a/c/b/s/b D
ldc2_w -1.0D
dcmpl
ifeq L0
aload 0
aload 1
ldc com/a/c/a/d
invokevirtual java/lang/Class/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast com/a/c/a/d
aload 1
ldc com/a/c/a/e
invokevirtual java/lang/Class/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast com/a/c/a/e
invokevirtual com/a/c/b/s/a(Lcom/a/c/a/d;Lcom/a/c/a/e;)Z
ifne L0
iconst_1
ireturn
L0:
aload 0
getfield com/a/c/b/s/d Z
ifne L1
aload 1
invokestatic com/a/c/b/s/b(Ljava/lang/Class;)Z
ifeq L1
iconst_1
ireturn
L1:
aload 1
invokestatic com/a/c/b/s/a(Ljava/lang/Class;)Z
ifeq L2
iconst_1
ireturn
L2:
iload 2
ifeq L3
aload 0
getfield com/a/c/b/s/f Ljava/util/List;
astore 1
L4:
aload 1
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L5:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/c/b
invokeinterface com/a/c/b/b()Z 0
ifeq L5
iconst_1
ireturn
L3:
aload 0
getfield com/a/c/b/s/g Ljava/util/List;
astore 1
goto L4
L6:
iconst_0
ireturn
.limit locals 3
.limit stack 4
.end method

.method protected final synthetic clone()Ljava/lang/Object;
aload 0
invokevirtual com/a/c/b/s/a()Lcom/a/c/b/s;
areturn
.limit locals 1
.limit stack 1
.end method
