.bytecode 50.0
.class final synchronized com/a/b/f/b
.super java/lang/Object
.implements com/a/b/f/r

.field private static final 'a' Lcom/a/b/c/an;

.method static <clinit>()V
invokestatic com/a/b/c/f/a()Lcom/a/b/c/f;
getstatic com/a/b/c/bw/c Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
new com/a/b/f/c
dup
invokespecial com/a/b/f/c/<init>()V
invokevirtual com/a/b/c/f/a(Lcom/a/b/c/ab;)Lcom/a/b/c/an;
putstatic com/a/b/f/b/a Lcom/a/b/c/an;
return
.limit locals 0
.limit stack 3
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Ljava/lang/Class;)Lcom/a/b/d/jl;
aload 0
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
invokevirtual com/a/b/m/ae/b()Lcom/a/b/m/aw;
invokevirtual com/a/b/m/aw/d()Ljava/util/Set;
astore 3
invokestatic com/a/b/d/sz/c()Ljava/util/HashMap;
astore 0
aload 3
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Class
invokevirtual java/lang/Class/getMethods()[Ljava/lang/reflect/Method;
astore 4
aload 4
arraylength
istore 2
iconst_0
istore 1
L2:
iload 1
iload 2
if_icmpge L0
aload 4
iload 1
aaload
astore 5
aload 5
ldc com/a/b/f/o
invokevirtual java/lang/reflect/Method/isAnnotationPresent(Ljava/lang/Class;)Z
ifeq L3
aload 5
invokevirtual java/lang/reflect/Method/isBridge()Z
ifne L3
aload 5
invokevirtual java/lang/reflect/Method/getParameterTypes()[Ljava/lang/Class;
astore 6
aload 6
arraylength
iconst_1
if_icmpeq L4
aload 5
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 6
arraylength
istore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
sipush 128
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Method "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " has @Subscribe annotation, but requires "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " arguments.  Event subscriber methods must require a single argument."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
new com/a/b/f/d
dup
aload 5
invokespecial com/a/b/f/d/<init>(Ljava/lang/reflect/Method;)V
astore 6
aload 0
aload 6
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L3
aload 0
aload 6
aload 5
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L3:
iload 1
iconst_1
iadd
istore 1
goto L2
L1:
aload 0
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokestatic com/a/b/d/jl/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
areturn
.limit locals 7
.limit stack 6
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/reflect/Method;)Lcom/a/b/f/n;
aload 1
ldc com/a/b/f/a
invokevirtual java/lang/reflect/Method/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
ifnull L0
iconst_1
istore 2
L1:
iload 2
ifeq L2
new com/a/b/f/n
dup
aload 0
aload 1
invokespecial com/a/b/f/n/<init>(Ljava/lang/Object;Ljava/lang/reflect/Method;)V
areturn
L0:
iconst_0
istore 2
goto L1
L2:
new com/a/b/f/s
dup
aload 0
aload 1
invokespecial com/a/b/f/s/<init>(Ljava/lang/Object;Ljava/lang/reflect/Method;)V
areturn
.limit locals 3
.limit stack 4
.end method

.method private static a(Ljava/lang/reflect/Method;)Z
aload 0
ldc com/a/b/f/a
invokevirtual java/lang/reflect/Method/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static b(Ljava/lang/Class;)Lcom/a/b/d/jl;
.catch com/a/b/n/a/gq from L0 to L1 using L2
L0:
getstatic com/a/b/f/b/a Lcom/a/b/c/an;
aload 0
invokeinterface com/a/b/c/an/b(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/jl
astore 0
L1:
aload 0
areturn
L2:
astore 0
aload 0
invokevirtual com/a/b/n/a/gq/getCause()Ljava/lang/Throwable;
invokestatic com/a/b/b/ei/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
.limit locals 1
.limit stack 2
.end method

.method private static c(Ljava/lang/Class;)Lcom/a/b/d/jl;
aload 0
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
invokevirtual com/a/b/m/ae/b()Lcom/a/b/m/aw;
invokevirtual com/a/b/m/aw/d()Ljava/util/Set;
astore 3
invokestatic com/a/b/d/sz/c()Ljava/util/HashMap;
astore 0
aload 3
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Class
invokevirtual java/lang/Class/getMethods()[Ljava/lang/reflect/Method;
astore 4
aload 4
arraylength
istore 2
iconst_0
istore 1
L2:
iload 1
iload 2
if_icmpge L0
aload 4
iload 1
aaload
astore 5
aload 5
ldc com/a/b/f/o
invokevirtual java/lang/reflect/Method/isAnnotationPresent(Ljava/lang/Class;)Z
ifeq L3
aload 5
invokevirtual java/lang/reflect/Method/isBridge()Z
ifne L3
aload 5
invokevirtual java/lang/reflect/Method/getParameterTypes()[Ljava/lang/Class;
astore 6
aload 6
arraylength
iconst_1
if_icmpeq L4
aload 5
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 6
arraylength
istore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
sipush 128
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Method "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " has @Subscribe annotation, but requires "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " arguments.  Event subscriber methods must require a single argument."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
new com/a/b/f/d
dup
aload 5
invokespecial com/a/b/f/d/<init>(Ljava/lang/reflect/Method;)V
astore 6
aload 0
aload 6
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L3
aload 0
aload 6
aload 5
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L3:
iload 1
iconst_1
iadd
istore 1
goto L2
L1:
aload 0
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokestatic com/a/b/d/jl/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
areturn
.limit locals 7
.limit stack 6
.end method

.method public final a(Ljava/lang/Object;)Lcom/a/b/d/vi;
invokestatic com/a/b/d/io/v()Lcom/a/b/d/io;
astore 4
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokestatic com/a/b/f/b/b(Ljava/lang/Class;)Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/iterator()Ljava/util/Iterator;
astore 5
L0:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/reflect/Method
astore 3
aload 3
invokevirtual java/lang/reflect/Method/getParameterTypes()[Ljava/lang/Class;
iconst_0
aaload
astore 6
aload 3
ldc com/a/b/f/a
invokevirtual java/lang/reflect/Method/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
ifnull L2
iconst_1
istore 2
L3:
iload 2
ifeq L4
new com/a/b/f/n
dup
aload 1
aload 3
invokespecial com/a/b/f/n/<init>(Ljava/lang/Object;Ljava/lang/reflect/Method;)V
astore 3
L5:
aload 4
aload 6
aload 3
invokeinterface com/a/b/d/vi/a(Ljava/lang/Object;Ljava/lang/Object;)Z 2
pop
goto L0
L2:
iconst_0
istore 2
goto L3
L4:
new com/a/b/f/s
dup
aload 1
aload 3
invokespecial com/a/b/f/s/<init>(Ljava/lang/Object;Ljava/lang/reflect/Method;)V
astore 3
goto L5
L1:
aload 4
areturn
.limit locals 7
.limit stack 4
.end method
