.bytecode 50.0
.class public synchronized com/a/b/f/h
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'a' Lcom/a/b/c/an;

.field private final 'b' Lcom/a/b/d/aac;

.field private final 'c' Ljava/util/concurrent/locks/ReadWriteLock;

.field private final 'd' Lcom/a/b/f/r;

.field private final 'e' Ljava/lang/ThreadLocal;

.field private final 'f' Ljava/lang/ThreadLocal;

.field private 'g' Lcom/a/b/f/q;

.method static <clinit>()V
invokestatic com/a/b/c/f/a()Lcom/a/b/c/f;
getstatic com/a/b/c/bw/c Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
new com/a/b/f/i
dup
invokespecial com/a/b/f/i/<init>()V
invokevirtual com/a/b/c/f/a(Lcom/a/b/c/ab;)Lcom/a/b/c/an;
putstatic com/a/b/f/h/a Lcom/a/b/c/an;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
ldc "default"
invokespecial com/a/b/f/h/<init>(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Lcom/a/b/f/q;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokestatic com/a/b/d/io/v()Lcom/a/b/d/io;
putfield com/a/b/f/h/b Lcom/a/b/d/aac;
aload 0
new java/util/concurrent/locks/ReentrantReadWriteLock
dup
invokespecial java/util/concurrent/locks/ReentrantReadWriteLock/<init>()V
putfield com/a/b/f/h/c Ljava/util/concurrent/locks/ReadWriteLock;
aload 0
new com/a/b/f/b
dup
invokespecial com/a/b/f/b/<init>()V
putfield com/a/b/f/h/d Lcom/a/b/f/r;
aload 0
new com/a/b/f/j
dup
aload 0
invokespecial com/a/b/f/j/<init>(Lcom/a/b/f/h;)V
putfield com/a/b/f/h/e Ljava/lang/ThreadLocal;
aload 0
new com/a/b/f/k
dup
aload 0
invokespecial com/a/b/f/k/<init>(Lcom/a/b/f/h;)V
putfield com/a/b/f/h/f Ljava/lang/ThreadLocal;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/f/q
putfield com/a/b/f/h/g Lcom/a/b/f/q;
return
.limit locals 2
.limit stack 4
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
new com/a/b/f/m
dup
aload 1
invokespecial com/a/b/f/m/<init>(Ljava/lang/String;)V
invokespecial com/a/b/f/h/<init>(Lcom/a/b/f/q;)V
return
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/lang/Class;)Ljava/util/Set;
.annotation invisible Lcom/a/b/a/d;
.end annotation
.catch com/a/b/n/a/gq from L0 to L1 using L2
L0:
getstatic com/a/b/f/h/a Lcom/a/b/c/an;
aload 0
invokeinterface com/a/b/c/an/b(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Set
astore 0
L1:
aload 0
areturn
L2:
astore 0
aload 0
invokevirtual com/a/b/n/a/gq/getCause()Ljava/lang/Throwable;
invokestatic com/a/b/b/ei/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
.limit locals 1
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)V
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/f/h/d Lcom/a/b/f/r;
aload 1
invokeinterface com/a/b/f/r/a(Ljava/lang/Object;)Lcom/a/b/d/vi; 1
astore 1
aload 0
getfield com/a/b/f/h/c Ljava/util/concurrent/locks/ReadWriteLock;
invokeinterface java/util/concurrent/locks/ReadWriteLock/writeLock()Ljava/util/concurrent/locks/Lock; 0
invokeinterface java/util/concurrent/locks/Lock/lock()V 0
L0:
aload 0
getfield com/a/b/f/h/b Lcom/a/b/d/aac;
aload 1
invokeinterface com/a/b/d/aac/a(Lcom/a/b/d/vi;)Z 1
pop
L1:
aload 0
getfield com/a/b/f/h/c Ljava/util/concurrent/locks/ReadWriteLock;
invokeinterface java/util/concurrent/locks/ReadWriteLock/writeLock()Ljava/util/concurrent/locks/Lock; 0
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
return
L2:
astore 1
aload 0
getfield com/a/b/f/h/c Ljava/util/concurrent/locks/ReadWriteLock;
invokeinterface java/util/concurrent/locks/ReadWriteLock/writeLock()Ljava/util/concurrent/locks/Lock; 0
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/Object;)V
.catch all from L0 to L1 using L1
.catch all from L2 to L3 using L1
aload 0
getfield com/a/b/f/h/d Lcom/a/b/f/r;
aload 1
invokeinterface com/a/b/f/r/a(Ljava/lang/Object;)Lcom/a/b/d/vi; 1
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L4:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 4
aload 4
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/Class
astore 3
aload 4
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/util/Collection
astore 4
aload 0
getfield com/a/b/f/h/c Ljava/util/concurrent/locks/ReadWriteLock;
invokeinterface java/util/concurrent/locks/ReadWriteLock/writeLock()Ljava/util/concurrent/locks/Lock; 0
invokeinterface java/util/concurrent/locks/Lock/lock()V 0
L0:
aload 0
getfield com/a/b/f/h/b Lcom/a/b/d/aac;
aload 3
invokeinterface com/a/b/d/aac/a(Ljava/lang/Object;)Ljava/util/Set; 1
astore 3
aload 3
aload 4
invokeinterface java/util/Set/containsAll(Ljava/util/Collection;)Z 1
ifne L2
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 65
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "missing event subscriber for an annotated method. Is "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " registered?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
aload 0
getfield com/a/b/f/h/c Ljava/util/concurrent/locks/ReadWriteLock;
invokeinterface java/util/concurrent/locks/ReadWriteLock/writeLock()Ljava/util/concurrent/locks/Lock; 0
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
aload 1
athrow
L2:
aload 3
aload 4
invokeinterface java/util/Set/removeAll(Ljava/util/Collection;)Z 1
pop
L3:
aload 0
getfield com/a/b/f/h/c Ljava/util/concurrent/locks/ReadWriteLock;
invokeinterface java/util/concurrent/locks/ReadWriteLock/writeLock()Ljava/util/concurrent/locks/Lock; 0
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
goto L4
L5:
return
.limit locals 5
.limit stack 6
.end method

.method private c(Ljava/lang/Object;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokestatic com/a/b/f/h/a(Ljava/lang/Class;)Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L4:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Class
astore 3
aload 0
getfield com/a/b/f/h/c Ljava/util/concurrent/locks/ReadWriteLock;
invokeinterface java/util/concurrent/locks/ReadWriteLock/readLock()Ljava/util/concurrent/locks/Lock; 0
invokeinterface java/util/concurrent/locks/Lock/lock()V 0
L0:
aload 0
getfield com/a/b/f/h/b Lcom/a/b/d/aac;
aload 3
invokeinterface com/a/b/d/aac/a(Ljava/lang/Object;)Ljava/util/Set; 1
astore 3
aload 3
invokeinterface java/util/Set/isEmpty()Z 0
ifne L6
aload 3
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L1:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 0
aload 1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/f/n
invokevirtual com/a/b/f/h/a(Ljava/lang/Object;Lcom/a/b/f/n;)V
L3:
goto L1
L2:
astore 1
aload 0
getfield com/a/b/f/h/c Ljava/util/concurrent/locks/ReadWriteLock;
invokeinterface java/util/concurrent/locks/ReadWriteLock/readLock()Ljava/util/concurrent/locks/Lock; 0
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
aload 1
athrow
L6:
aload 0
getfield com/a/b/f/h/c Ljava/util/concurrent/locks/ReadWriteLock;
invokeinterface java/util/concurrent/locks/ReadWriteLock/readLock()Ljava/util/concurrent/locks/Lock; 0
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
goto L4
L5:
aload 0
invokevirtual com/a/b/f/h/a()V
return
.limit locals 4
.limit stack 3
.end method

.method a()V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/f/h/f Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/get()Ljava/lang/Object;
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L6
return
L6:
aload 0
getfield com/a/b/f/h/f Ljava/lang/ThreadLocal;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/lang/ThreadLocal/set(Ljava/lang/Object;)V
L0:
aload 0
getfield com/a/b/f/h/e Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/get()Ljava/lang/Object;
checkcast java/util/Queue
astore 1
L1:
aload 1
invokeinterface java/util/Queue/poll()Ljava/lang/Object; 0
checkcast com/a/b/f/l
astore 2
L3:
aload 2
ifnull L7
L4:
aload 0
aload 2
getfield com/a/b/f/l/a Ljava/lang/Object;
aload 2
getfield com/a/b/f/l/b Lcom/a/b/f/n;
invokevirtual com/a/b/f/h/b(Ljava/lang/Object;Lcom/a/b/f/n;)V
L5:
goto L1
L2:
astore 1
aload 0
getfield com/a/b/f/h/f Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/remove()V
aload 0
getfield com/a/b/f/h/e Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/remove()V
aload 1
athrow
L7:
aload 0
getfield com/a/b/f/h/f Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/remove()V
aload 0
getfield com/a/b/f/h/e Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/remove()V
return
.limit locals 3
.limit stack 3
.end method

.method a(Ljava/lang/Object;Lcom/a/b/f/n;)V
aload 0
getfield com/a/b/f/h/e Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/get()Ljava/lang/Object;
checkcast java/util/Queue
new com/a/b/f/l
dup
aload 1
aload 2
invokespecial com/a/b/f/l/<init>(Ljava/lang/Object;Lcom/a/b/f/n;)V
invokeinterface java/util/Queue/offer(Ljava/lang/Object;)Z 1
pop
return
.limit locals 3
.limit stack 5
.end method

.method b(Ljava/lang/Object;Lcom/a/b/f/n;)V
.catch java/lang/reflect/InvocationTargetException from L0 to L1 using L2
.catch java/lang/Throwable from L3 to L4 using L5
L0:
aload 2
aload 1
invokevirtual com/a/b/f/n/a(Ljava/lang/Object;)V
L1:
return
L2:
astore 3
L3:
aload 0
getfield com/a/b/f/h/g Lcom/a/b/f/q;
aload 3
invokevirtual java/lang/reflect/InvocationTargetException/getCause()Ljava/lang/Throwable;
new com/a/b/f/p
dup
aload 0
aload 1
aload 2
getfield com/a/b/f/n/a Ljava/lang/Object;
aload 2
getfield com/a/b/f/n/b Ljava/lang/reflect/Method;
invokespecial com/a/b/f/p/<init>(Lcom/a/b/f/h;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Method;)V
invokeinterface com/a/b/f/q/a(Ljava/lang/Throwable;Lcom/a/b/f/p;)V 2
L4:
return
L5:
astore 1
ldc com/a/b/f/h
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
ldc "Exception %s thrown while handling exception: %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 3
invokevirtual java/lang/reflect/InvocationTargetException/getCause()Ljava/lang/Throwable;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
aload 1
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 4
.limit stack 8
.end method
