.bytecode 50.0
.class final synchronized com/a/b/j/k
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/String;D)D
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
dload 1
dconst_0
dcmpl
ifge L0
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 40
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
dload 1
invokevirtual java/lang/StringBuilder/append(D)Ljava/lang/StringBuilder;
ldc ") must be >= 0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
dload 1
dreturn
.limit locals 3
.limit stack 6
.end method

.method static a(Ljava/lang/String;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iload 1
ifgt L0
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 26
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ") must be > 0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 1
ireturn
.limit locals 2
.limit stack 6
.end method

.method static a(Ljava/lang/String;J)J
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
lload 1
lconst_0
lcmp
ifgt L0
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 35
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ") must be > 0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
lload 1
lreturn
.limit locals 3
.limit stack 6
.end method

.method static a(Ljava/lang/String;Ljava/math/BigInteger;)Ljava/math/BigInteger;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
invokevirtual java/math/BigInteger/signum()I
ifgt L0
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 15
iadd
aload 1
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ") must be > 0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
areturn
.limit locals 2
.limit stack 6
.end method

.method static a(Z)V
iload 0
ifne L0
new java/lang/ArithmeticException
dup
ldc "mode was UNNECESSARY, but rounding was necessary"
invokespecial java/lang/ArithmeticException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 1
.limit stack 3
.end method

.method static b(Ljava/lang/String;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iload 1
ifge L0
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 27
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ") must be >= 0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 1
ireturn
.limit locals 2
.limit stack 6
.end method

.method static b(Ljava/lang/String;J)J
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
lload 1
lconst_0
lcmp
ifge L0
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 36
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ") must be >= 0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
lload 1
lreturn
.limit locals 3
.limit stack 6
.end method

.method private static b(Ljava/lang/String;Ljava/math/BigInteger;)Ljava/math/BigInteger;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
invokevirtual java/math/BigInteger/signum()I
ifge L0
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 16
iadd
aload 1
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ") must be >= 0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
areturn
.limit locals 2
.limit stack 6
.end method

.method static b(Z)V
iload 0
ifne L0
new java/lang/ArithmeticException
dup
ldc "not in range"
invokespecial java/lang/ArithmeticException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 1
.limit stack 3
.end method

.method static c(Z)V
iload 0
ifne L0
new java/lang/ArithmeticException
dup
ldc "overflow"
invokespecial java/lang/ArithmeticException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 1
.limit stack 3
.end method
