.bytecode 50.0
.class final synchronized com/a/b/b/bo
.super java/lang/Object
.implements com/a/b/b/bj
.implements java/io/Serializable

.field private static final 'c' J = 0L


.field final 'a' Ljava/util/Map;

.field final 'b' Ljava/lang/Object;

.method <init>(Ljava/util/Map;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Map
putfield com/a/b/b/bo/a Ljava/util/Map;
aload 0
aload 2
putfield com/a/b/b/bo/b Ljava/lang/Object;
return
.limit locals 3
.limit stack 2
.end method

.method public final e(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/b/bo/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 2
aload 2
ifnonnull L0
aload 0
getfield com/a/b/b/bo/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L1
L0:
aload 2
areturn
L1:
aload 0
getfield com/a/b/b/bo/b Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/b/bo
ifeq L0
aload 1
checkcast com/a/b/b/bo
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/b/bo/a Ljava/util/Map;
aload 1
getfield com/a/b/b/bo/a Ljava/util/Map;
invokeinterface java/util/Map/equals(Ljava/lang/Object;)Z 1
ifeq L0
iload 3
istore 2
aload 0
getfield com/a/b/b/bo/b Ljava/lang/Object;
aload 1
getfield com/a/b/b/bo/b Ljava/lang/Object;
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final hashCode()I
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/b/bo/a Ljava/util/Map;
aastore
dup
iconst_1
aload 0
getfield com/a/b/b/bo/b Ljava/lang/Object;
aastore
invokestatic java/util/Arrays/hashCode([Ljava/lang/Object;)I
ireturn
.limit locals 1
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/b/bo/a Ljava/util/Map;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 0
getfield com/a/b/b/bo/b Ljava/lang/Object;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 23
iadd
aload 2
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "forMap("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", defaultValue="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method
