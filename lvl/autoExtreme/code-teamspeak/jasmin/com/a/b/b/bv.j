.bytecode 50.0
.class public synchronized com/a/b/b/bv
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field final 'a' Ljava/lang/String;

.method private <init>(Lcom/a/b/b/bv;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
getfield com/a/b/b/bv/a Ljava/lang/String;
putfield com/a/b/b/bv/a Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Lcom/a/b/b/bv;B)V
aload 0
aload 1
invokespecial com/a/b/b/bv/<init>(Lcom/a/b/b/bv;)V
return
.limit locals 3
.limit stack 2
.end method

.method private <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/a/b/b/bv/a Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public static a(C)Lcom/a/b/b/bv;
new com/a/b/b/bv
dup
iload 0
invokestatic java/lang/String/valueOf(C)Ljava/lang/String;
invokespecial com/a/b/b/bv/<init>(Ljava/lang/String;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/lang/String;)Lcom/a/b/b/bv;
new com/a/b/b/bv
dup
aload 0
invokespecial com/a/b/b/bv/<init>(Ljava/lang/String;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Ljava/lang/Appendable;Ljava/lang/Iterable;)Ljava/lang/Appendable;
aload 0
aload 1
aload 2
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokevirtual com/a/b/b/bv/a(Ljava/lang/Appendable;Ljava/util/Iterator;)Ljava/lang/Appendable;
areturn
.limit locals 3
.limit stack 3
.end method

.method private transient a(Ljava/lang/Appendable;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Appendable;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
aload 3
aload 4
invokestatic com/a/b/b/bv/b(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Iterable;
invokespecial com/a/b/b/bv/a(Ljava/lang/Appendable;Ljava/lang/Iterable;)Ljava/lang/Appendable;
areturn
.limit locals 5
.limit stack 5
.end method

.method private a(Ljava/lang/Appendable;[Ljava/lang/Object;)Ljava/lang/Appendable;
aload 0
aload 1
aload 2
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokespecial com/a/b/b/bv/a(Ljava/lang/Appendable;Ljava/lang/Iterable;)Ljava/lang/Appendable;
areturn
.limit locals 3
.limit stack 3
.end method

.method private static synthetic a(Lcom/a/b/b/bv;)Ljava/lang/String;
aload 0
getfield com/a/b/b/bv/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private transient a(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
aload 3
invokestatic com/a/b/b/bv/b(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Iterable;
invokevirtual com/a/b/b/bv/a(Ljava/lang/Iterable;)Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method private a(Ljava/util/Iterator;)Ljava/lang/String;
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual com/a/b/b/bv/a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method private transient a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/StringBuilder;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
aload 3
aload 4
invokestatic com/a/b/b/bv/b(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Iterable;
invokevirtual com/a/b/b/bv/a(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;
areturn
.limit locals 5
.limit stack 5
.end method

.method private a(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 0
aload 1
aload 2
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokevirtual com/a/b/b/bv/a(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;
areturn
.limit locals 3
.limit stack 3
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Iterable;
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/b/by
dup
aload 2
aload 0
aload 1
invokespecial com/a/b/b/by/<init>([Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method public a()Lcom/a/b/b/bv;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
new com/a/b/b/bx
dup
aload 0
aload 0
invokespecial com/a/b/b/bx/<init>(Lcom/a/b/b/bv;Lcom/a/b/b/bv;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public a(Ljava/lang/Appendable;Ljava/util/Iterator;)Ljava/lang/Appendable;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
aload 0
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokevirtual com/a/b/b/bv/a(Ljava/lang/Object;)Ljava/lang/CharSequence;
invokeinterface java/lang/Appendable/append(Ljava/lang/CharSequence;)Ljava/lang/Appendable; 1
pop
L1:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
aload 0
getfield com/a/b/b/bv/a Ljava/lang/String;
invokeinterface java/lang/Appendable/append(Ljava/lang/CharSequence;)Ljava/lang/Appendable; 1
pop
aload 1
aload 0
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokevirtual com/a/b/b/bv/a(Ljava/lang/Object;)Ljava/lang/CharSequence;
invokeinterface java/lang/Appendable/append(Ljava/lang/CharSequence;)Ljava/lang/Appendable; 1
pop
goto L1
L0:
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method a(Ljava/lang/Object;)Ljava/lang/CharSequence;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
instanceof java/lang/CharSequence
ifeq L0
aload 1
checkcast java/lang/CharSequence
areturn
L0:
aload 1
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Ljava/lang/Iterable;)Ljava/lang/String;
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual com/a/b/b/bv/a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final a([Ljava/lang/Object;)Ljava/lang/String;
aload 0
aload 1
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokevirtual com/a/b/b/bv/a(Ljava/lang/Iterable;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;
aload 0
aload 1
aload 2
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokevirtual com/a/b/b/bv/a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
aload 1
aload 2
invokevirtual com/a/b/b/bv/a(Ljava/lang/Appendable;Ljava/util/Iterator;)Ljava/lang/Appendable;
pop
L1:
aload 1
areturn
L2:
astore 1
new java/lang/AssertionError
dup
aload 1
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public b(Ljava/lang/String;)Lcom/a/b/b/bv;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/b/bw
dup
aload 0
aload 0
aload 1
invokespecial com/a/b/b/bw/<init>(Lcom/a/b/b/bv;Lcom/a/b/b/bv;Ljava/lang/String;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public c(Ljava/lang/String;)Lcom/a/b/b/bz;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
new com/a/b/b/bz
dup
aload 0
aload 1
iconst_0
invokespecial com/a/b/b/bz/<init>(Lcom/a/b/b/bv;Ljava/lang/String;B)V
areturn
.limit locals 2
.limit stack 5
.end method
