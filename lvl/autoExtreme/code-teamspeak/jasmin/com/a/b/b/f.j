.bytecode 50.0
.class public synchronized abstract enum com/a/b/b/f
.super java/lang/Enum
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field public static final enum 'a' Lcom/a/b/b/f;

.field public static final enum 'b' Lcom/a/b/b/f;

.field public static final enum 'c' Lcom/a/b/b/f;

.field public static final enum 'd' Lcom/a/b/b/f;

.field public static final enum 'e' Lcom/a/b/b/f;

.field private static final synthetic 'h' [Lcom/a/b/b/f;

.field private final 'f' Lcom/a/b/b/m;

.field private final 'g' Ljava/lang/String;

.method static <clinit>()V
new com/a/b/b/g
dup
ldc "LOWER_HYPHEN"
bipush 45
invokestatic com/a/b/b/m/a(C)Lcom/a/b/b/m;
ldc "-"
invokespecial com/a/b/b/g/<init>(Ljava/lang/String;Lcom/a/b/b/m;Ljava/lang/String;)V
putstatic com/a/b/b/f/a Lcom/a/b/b/f;
new com/a/b/b/h
dup
ldc "LOWER_UNDERSCORE"
bipush 95
invokestatic com/a/b/b/m/a(C)Lcom/a/b/b/m;
ldc "_"
invokespecial com/a/b/b/h/<init>(Ljava/lang/String;Lcom/a/b/b/m;Ljava/lang/String;)V
putstatic com/a/b/b/f/b Lcom/a/b/b/f;
new com/a/b/b/i
dup
ldc "LOWER_CAMEL"
bipush 65
bipush 90
invokestatic com/a/b/b/m/a(CC)Lcom/a/b/b/m;
ldc ""
invokespecial com/a/b/b/i/<init>(Ljava/lang/String;Lcom/a/b/b/m;Ljava/lang/String;)V
putstatic com/a/b/b/f/c Lcom/a/b/b/f;
new com/a/b/b/j
dup
ldc "UPPER_CAMEL"
bipush 65
bipush 90
invokestatic com/a/b/b/m/a(CC)Lcom/a/b/b/m;
ldc ""
invokespecial com/a/b/b/j/<init>(Ljava/lang/String;Lcom/a/b/b/m;Ljava/lang/String;)V
putstatic com/a/b/b/f/d Lcom/a/b/b/f;
new com/a/b/b/k
dup
ldc "UPPER_UNDERSCORE"
bipush 95
invokestatic com/a/b/b/m/a(C)Lcom/a/b/b/m;
ldc "_"
invokespecial com/a/b/b/k/<init>(Ljava/lang/String;Lcom/a/b/b/m;Ljava/lang/String;)V
putstatic com/a/b/b/f/e Lcom/a/b/b/f;
iconst_5
anewarray com/a/b/b/f
dup
iconst_0
getstatic com/a/b/b/f/a Lcom/a/b/b/f;
aastore
dup
iconst_1
getstatic com/a/b/b/f/b Lcom/a/b/b/f;
aastore
dup
iconst_2
getstatic com/a/b/b/f/c Lcom/a/b/b/f;
aastore
dup
iconst_3
getstatic com/a/b/b/f/d Lcom/a/b/b/f;
aastore
dup
iconst_4
getstatic com/a/b/b/f/e Lcom/a/b/b/f;
aastore
putstatic com/a/b/b/f/h [Lcom/a/b/b/f;
return
.limit locals 0
.limit stack 5
.end method

.method private <init>(Ljava/lang/String;ILcom/a/b/b/m;Ljava/lang/String;)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
aload 0
aload 3
putfield com/a/b/b/f/f Lcom/a/b/b/m;
aload 0
aload 4
putfield com/a/b/b/f/g Ljava/lang/String;
return
.limit locals 5
.limit stack 3
.end method

.method synthetic <init>(Ljava/lang/String;ILcom/a/b/b/m;Ljava/lang/String;B)V
aload 0
aload 1
iload 2
aload 3
aload 4
invokespecial com/a/b/b/f/<init>(Ljava/lang/String;ILcom/a/b/b/m;Ljava/lang/String;)V
return
.limit locals 6
.limit stack 5
.end method

.method private a(Lcom/a/b/b/f;)Lcom/a/b/b/ak;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/b/l
dup
aload 0
aload 1
invokespecial com/a/b/b/l/<init>(Lcom/a/b/b/f;Lcom/a/b/b/f;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method static synthetic b(Ljava/lang/String;)Ljava/lang/String;
aload 0
invokevirtual java/lang/String/isEmpty()Z
ifeq L0
aload 0
areturn
L0:
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
invokespecial java/lang/StringBuilder/<init>(I)V
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
invokestatic com/a/b/b/e/b(C)C
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
aload 0
iconst_1
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic com/a/b/b/e/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
aload 0
getstatic com/a/b/b/f/c Lcom/a/b/b/f;
if_acmpne L0
aload 1
invokestatic com/a/b/b/e/a(Ljava/lang/String;)Ljava/lang/String;
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/b/f/a(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
aload 0
invokevirtual java/lang/String/isEmpty()Z
ifeq L0
aload 0
areturn
L0:
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
invokespecial java/lang/StringBuilder/<init>(I)V
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
invokestatic com/a/b/b/e/b(C)C
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
aload 0
iconst_1
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic com/a/b/b/e/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/b/f;
ldc com/a/b/b/f
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/a/b/b/f
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/a/b/b/f;
getstatic com/a/b/b/f/h [Lcom/a/b/b/f;
invokevirtual [Lcom/a/b/b/f;/clone()Ljava/lang/Object;
checkcast [Lcom/a/b/b/f;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final a(Lcom/a/b/b/f;Ljava/lang/String;)Ljava/lang/String;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
aload 0
if_acmpne L0
aload 2
areturn
L0:
aload 0
aload 1
aload 2
invokevirtual com/a/b/b/f/b(Lcom/a/b/b/f;Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 3
.end method

.method abstract a(Ljava/lang/String;)Ljava/lang/String;
.end method

.method b(Lcom/a/b/b/f;Ljava/lang/String;)Ljava/lang/String;
iconst_0
istore 3
aconst_null
astore 5
iconst_m1
istore 4
L0:
aload 0
getfield com/a/b/b/f/f Lcom/a/b/b/m;
aload 2
iload 4
iconst_1
iadd
invokevirtual com/a/b/b/m/a(Ljava/lang/CharSequence;I)I
istore 4
iload 4
iconst_m1
if_icmpeq L1
iload 3
ifne L2
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
aload 0
getfield com/a/b/b/f/g Ljava/lang/String;
invokevirtual java/lang/String/length()I
iconst_4
imul
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
astore 5
aload 5
aload 1
aload 2
iload 3
iload 4
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokespecial com/a/b/b/f/c(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L3:
aload 5
aload 1
getfield com/a/b/b/f/g Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
getfield com/a/b/b/f/g Ljava/lang/String;
invokevirtual java/lang/String/length()I
iload 4
iadd
istore 3
goto L0
L2:
aload 5
aload 1
aload 2
iload 3
iload 4
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual com/a/b/b/f/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L3
L1:
iload 3
ifne L4
aload 1
aload 2
invokespecial com/a/b/b/f/c(Ljava/lang/String;)Ljava/lang/String;
areturn
L4:
aload 5
aload 1
aload 2
iload 3
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokevirtual com/a/b/b/f/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 6
.limit stack 5
.end method
