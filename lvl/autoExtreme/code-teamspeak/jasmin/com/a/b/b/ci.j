.bytecode 50.0
.class public synchronized abstract com/a/b/b/ci
.super java/lang/Object
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation

.field private static final 'a' J = 0L


.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/b/cj
dup
aload 0
invokespecial com/a/b/b/cj/<init>(Ljava/lang/Iterable;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static b(Ljava/lang/Object;)Lcom/a/b/b/ci;
new com/a/b/b/dg
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
invokespecial com/a/b/b/dg/<init>(Ljava/lang/Object;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static c(Ljava/lang/Object;)Lcom/a/b/b/ci;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
ifnonnull L0
invokestatic com/a/b/b/a/a()Lcom/a/b/b/ci;
areturn
L0:
new com/a/b/b/dg
dup
aload 0
invokespecial com/a/b/b/dg/<init>(Ljava/lang/Object;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static f()Lcom/a/b/b/ci;
invokestatic com/a/b/b/a/a()Lcom/a/b/b/ci;
areturn
.limit locals 0
.limit stack 1
.end method

.method public abstract a(Lcom/a/b/b/bj;)Lcom/a/b/b/ci;
.end method

.method public abstract a(Lcom/a/b/b/ci;)Lcom/a/b/b/ci;
.end method

.method public abstract a(Lcom/a/b/b/dz;)Ljava/lang/Object;
.annotation invisible Lcom/a/b/a/a;
.end annotation
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract b()Z
.end method

.method public abstract c()Ljava/lang/Object;
.end method

.method public abstract d()Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract e()Ljava/util/Set;
.end method

.method public abstract equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract hashCode()I
.end method

.method public abstract toString()Ljava/lang/String;
.end method
