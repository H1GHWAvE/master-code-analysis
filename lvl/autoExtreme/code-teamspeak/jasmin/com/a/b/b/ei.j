.bytecode 50.0
.class public final synchronized com/a/b/b/ei
.super java/lang/Object

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Throwable
invokestatic com/a/b/b/ei/b(Ljava/lang/Throwable;)V
new java/lang/RuntimeException
dup
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/lang/Throwable;Ljava/lang/Class;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
ifnull L0
aload 1
aload 0
invokevirtual java/lang/Class/isInstance(Ljava/lang/Object;)Z
ifeq L0
aload 1
aload 0
invokevirtual java/lang/Class/cast(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Throwable
athrow
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Throwable;Ljava/lang/Class;Ljava/lang/Class;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
invokestatic com/a/b/b/ei/a(Ljava/lang/Throwable;Ljava/lang/Class;)V
aload 0
aload 2
invokestatic com/a/b/b/ei/b(Ljava/lang/Throwable;Ljava/lang/Class;)V
return
.limit locals 3
.limit stack 2
.end method

.method private static b(Ljava/lang/Throwable;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
ldc java/lang/Error
invokestatic com/a/b/b/ei/a(Ljava/lang/Throwable;Ljava/lang/Class;)V
aload 0
ldc java/lang/RuntimeException
invokestatic com/a/b/b/ei/a(Ljava/lang/Throwable;Ljava/lang/Class;)V
return
.limit locals 1
.limit stack 2
.end method

.method public static b(Ljava/lang/Throwable;Ljava/lang/Class;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/b/ei/a(Ljava/lang/Throwable;Ljava/lang/Class;)V
aload 0
invokestatic com/a/b/b/ei/b(Ljava/lang/Throwable;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/lang/Throwable;)Ljava/lang/Throwable;
L0:
aload 0
invokevirtual java/lang/Throwable/getCause()Ljava/lang/Throwable;
astore 1
aload 1
ifnull L1
aload 1
astore 0
goto L0
L1:
aload 0
areturn
.limit locals 2
.limit stack 1
.end method

.method private static d(Ljava/lang/Throwable;)Ljava/util/List;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new java/util/ArrayList
dup
iconst_4
invokespecial java/util/ArrayList/<init>(I)V
astore 1
L0:
aload 0
ifnull L1
aload 1
aload 0
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
invokevirtual java/lang/Throwable/getCause()Ljava/lang/Throwable;
astore 0
goto L0
L1:
aload 1
invokestatic java/util/Collections/unmodifiableList(Ljava/util/List;)Ljava/util/List;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static e(Ljava/lang/Throwable;)Ljava/lang/String;
new java/io/StringWriter
dup
invokespecial java/io/StringWriter/<init>()V
astore 1
aload 0
new java/io/PrintWriter
dup
aload 1
invokespecial java/io/PrintWriter/<init>(Ljava/io/Writer;)V
invokevirtual java/lang/Throwable/printStackTrace(Ljava/io/PrintWriter;)V
aload 1
invokevirtual java/io/StringWriter/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
