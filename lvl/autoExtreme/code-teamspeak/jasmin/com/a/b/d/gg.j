.bytecode 50.0
.class public synchronized abstract com/a/b/d/gg
.super com/a/b/d/gj
.implements java/util/concurrent/BlockingDeque

.method protected <init>()V
aload 0
invokespecial com/a/b/d/gj/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected final synthetic a()Ljava/util/Queue;
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final synthetic b()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected abstract c()Ljava/util/concurrent/BlockingDeque;
.end method

.method protected final synthetic d()Ljava/util/Deque;
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
areturn
.limit locals 1
.limit stack 1
.end method

.method public drainTo(Ljava/util/Collection;)I
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
aload 1
invokeinterface java/util/concurrent/BlockingDeque/drainTo(Ljava/util/Collection;)I 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public drainTo(Ljava/util/Collection;I)I
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
aload 1
iload 2
invokeinterface java/util/concurrent/BlockingDeque/drainTo(Ljava/util/Collection;I)I 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method protected final synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
areturn
.limit locals 1
.limit stack 1
.end method

.method public offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
aload 1
lload 2
aload 4
invokeinterface java/util/concurrent/BlockingDeque/offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z 4
ireturn
.limit locals 5
.limit stack 5
.end method

.method public offerFirst(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
aload 1
lload 2
aload 4
invokeinterface java/util/concurrent/BlockingDeque/offerFirst(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z 4
ireturn
.limit locals 5
.limit stack 5
.end method

.method public offerLast(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
aload 1
lload 2
aload 4
invokeinterface java/util/concurrent/BlockingDeque/offerLast(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z 4
ireturn
.limit locals 5
.limit stack 5
.end method

.method public poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
lload 1
aload 3
invokeinterface java/util/concurrent/BlockingDeque/poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 3
areturn
.limit locals 4
.limit stack 4
.end method

.method public pollFirst(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
lload 1
aload 3
invokeinterface java/util/concurrent/BlockingDeque/pollFirst(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 3
areturn
.limit locals 4
.limit stack 4
.end method

.method public pollLast(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
lload 1
aload 3
invokeinterface java/util/concurrent/BlockingDeque/pollLast(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 3
areturn
.limit locals 4
.limit stack 4
.end method

.method public put(Ljava/lang/Object;)V
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
aload 1
invokeinterface java/util/concurrent/BlockingDeque/put(Ljava/lang/Object;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public putFirst(Ljava/lang/Object;)V
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
aload 1
invokeinterface java/util/concurrent/BlockingDeque/putFirst(Ljava/lang/Object;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public putLast(Ljava/lang/Object;)V
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
aload 1
invokeinterface java/util/concurrent/BlockingDeque/putLast(Ljava/lang/Object;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public remainingCapacity()I
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
invokeinterface java/util/concurrent/BlockingDeque/remainingCapacity()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public take()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
invokeinterface java/util/concurrent/BlockingDeque/take()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public takeFirst()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
invokeinterface java/util/concurrent/BlockingDeque/takeFirst()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public takeLast()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gg/c()Ljava/util/concurrent/BlockingDeque;
invokeinterface java/util/concurrent/BlockingDeque/takeLast()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method
