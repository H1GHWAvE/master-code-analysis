.bytecode 50.0
.class synchronized com/a/b/d/vb
.super java/util/AbstractCollection

.field final 'c' Ljava/util/Map;

.method <init>(Ljava/util/Map;)V
aload 0
invokespecial java/util/AbstractCollection/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Map
putfield com/a/b/d/vb/c Ljava/util/Map;
return
.limit locals 2
.limit stack 2
.end method

.method private a()Ljava/util/Map;
aload 0
getfield com/a/b/d/vb/c Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public clear()V
aload 0
getfield com/a/b/d/vb/c Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public contains(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/vb/c Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsValue(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public isEmpty()Z
aload 0
getfield com/a/b/d/vb/c Ljava/util/Map;
invokeinterface java/util/Map/isEmpty()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public iterator()Ljava/util/Iterator;
aload 0
getfield com/a/b/d/vb/c Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/sz/b(Ljava/util/Iterator;)Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public remove(Ljava/lang/Object;)Z
.catch java/lang/UnsupportedOperationException from L0 to L1 using L2
L0:
aload 0
aload 1
invokespecial java/util/AbstractCollection/remove(Ljava/lang/Object;)Z
istore 2
L1:
iload 2
ireturn
L2:
astore 3
aload 0
getfield com/a/b/d/vb/c Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L3:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 4
aload 1
aload 4
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L3
aload 0
getfield com/a/b/d/vb/c Ljava/util/Map;
aload 4
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
iconst_1
ireturn
L4:
iconst_0
ireturn
.limit locals 5
.limit stack 2
.end method

.method public removeAll(Ljava/util/Collection;)Z
.catch java/lang/UnsupportedOperationException from L0 to L1 using L2
L0:
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
invokespecial java/util/AbstractCollection/removeAll(Ljava/util/Collection;)Z
istore 2
L1:
iload 2
ireturn
L2:
astore 3
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
astore 3
aload 0
getfield com/a/b/d/vb/c Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 4
L3:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 5
aload 1
aload 5
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokeinterface java/util/Collection/contains(Ljava/lang/Object;)Z 1
ifeq L3
aload 3
aload 5
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
goto L3
L4:
aload 0
getfield com/a/b/d/vb/c Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
aload 3
invokeinterface java/util/Set/removeAll(Ljava/util/Collection;)Z 1
ireturn
.limit locals 6
.limit stack 2
.end method

.method public retainAll(Ljava/util/Collection;)Z
.catch java/lang/UnsupportedOperationException from L0 to L1 using L2
L0:
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
invokespecial java/util/AbstractCollection/retainAll(Ljava/util/Collection;)Z
istore 2
L1:
iload 2
ireturn
L2:
astore 3
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
astore 3
aload 0
getfield com/a/b/d/vb/c Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 4
L3:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 5
aload 1
aload 5
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokeinterface java/util/Collection/contains(Ljava/lang/Object;)Z 1
ifeq L3
aload 3
aload 5
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
goto L3
L4:
aload 0
getfield com/a/b/d/vb/c Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
aload 3
invokeinterface java/util/Set/retainAll(Ljava/util/Collection;)Z 1
ireturn
.limit locals 6
.limit stack 2
.end method

.method public size()I
aload 0
getfield com/a/b/d/vb/c Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method
