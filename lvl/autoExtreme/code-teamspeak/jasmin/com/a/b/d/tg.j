.bytecode 50.0
.class final synchronized com/a/b/d/tg
.super com/a/b/d/he

.field final synthetic 'a' Ljava/util/NavigableSet;

.method <init>(Ljava/util/NavigableSet;)V
aload 0
aload 1
putfield com/a/b/d/tg/a Ljava/util/NavigableSet;
aload 0
invokespecial com/a/b/d/he/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method protected final volatile synthetic a()Ljava/util/Set;
aload 0
getfield com/a/b/d/tg/a Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final add(Ljava/lang/Object;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final addAll(Ljava/util/Collection;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method protected final volatile synthetic b()Ljava/util/Collection;
aload 0
getfield com/a/b/d/tg/a Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final volatile synthetic c()Ljava/util/SortedSet;
aload 0
getfield com/a/b/d/tg/a Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final d()Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/tg/a Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final descendingSet()Ljava/util/NavigableSet;
aload 0
invokespecial com/a/b/d/he/descendingSet()Ljava/util/NavigableSet;
invokestatic com/a/b/d/sz/a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
aload 1
iload 2
invokespecial com/a/b/d/he/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
invokestatic com/a/b/d/sz/a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
invokespecial com/a/b/d/he/headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
invokestatic com/a/b/d/sz/a(Ljava/util/SortedSet;)Ljava/util/SortedSet;
areturn
.limit locals 2
.limit stack 2
.end method

.method protected final volatile synthetic k_()Ljava/lang/Object;
aload 0
getfield com/a/b/d/tg/a Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
aload 1
iload 2
aload 3
iload 4
invokespecial com/a/b/d/he/subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
invokestatic com/a/b/d/sz/a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
areturn
.limit locals 5
.limit stack 5
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
aload 2
invokespecial com/a/b/d/he/subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
invokestatic com/a/b/d/sz/a(Ljava/util/SortedSet;)Ljava/util/SortedSet;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
aload 1
iload 2
invokespecial com/a/b/d/he/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
invokestatic com/a/b/d/sz/a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
invokespecial com/a/b/d/he/tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
invokestatic com/a/b/d/sz/a(Ljava/util/SortedSet;)Ljava/util/SortedSet;
areturn
.limit locals 2
.limit stack 2
.end method
