.bytecode 50.0
.class public final synchronized com/a/b/d/oc
.super com/a/b/d/ba
.annotation invisible Lcom/a/b/a/b;
a Z = 1
b Z = 1
.end annotation

.field static final 'a' D = 1.0D

.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.field private static final 'c' I = 16


.field private static final 'd' I = 2


.field private static final 'f' J = 1L

.annotation invisible Lcom/a/b/a/c;
a s = "java serialization not supported"
.end annotation
.end field

.field transient 'b' I
.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.field private transient 'e' Lcom/a/b/d/oe;

.method private <init>(II)V
aload 0
new java/util/LinkedHashMap
dup
iload 1
invokespecial java/util/LinkedHashMap/<init>(I)V
invokespecial com/a/b/d/ba/<init>(Ljava/util/Map;)V
aload 0
iconst_2
putfield com/a/b/d/oc/b I
iload 2
ldc "expectedValuesPerKey"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
aload 0
iload 2
putfield com/a/b/d/oc/b I
aload 0
new com/a/b/d/oe
dup
aconst_null
aconst_null
iconst_0
aconst_null
invokespecial com/a/b/d/oe/<init>(Ljava/lang/Object;Ljava/lang/Object;ILcom/a/b/d/oe;)V
putfield com/a/b/d/oc/e Lcom/a/b/d/oe;
aload 0
getfield com/a/b/d/oc/e Lcom/a/b/d/oe;
aload 0
getfield com/a/b/d/oc/e Lcom/a/b/d/oe;
invokestatic com/a/b/d/oc/b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V
return
.limit locals 3
.limit stack 7
.end method

.method private static a(I)Lcom/a/b/d/oc;
new com/a/b/d/oc
dup
iload 0
invokestatic com/a/b/d/sz/b(I)I
iconst_2
invokestatic com/a/b/d/sz/b(I)I
invokespecial com/a/b/d/oc/<init>(II)V
areturn
.limit locals 1
.limit stack 4
.end method

.method static synthetic a(Lcom/a/b/d/oc;)Lcom/a/b/d/oe;
aload 0
getfield com/a/b/d/oc/e Lcom/a/b/d/oe;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/a/b/d/oe;)V
aload 0
getfield com/a/b/d/oe/g Lcom/a/b/d/oe;
aload 0
getfield com/a/b/d/oe/h Lcom/a/b/d/oe;
invokestatic com/a/b/d/oc/b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V
aload 0
aload 1
invokestatic com/a/b/d/oc/b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/oh;)V
aload 0
invokeinterface com/a/b/d/oh/a()Lcom/a/b/d/oh; 0
aload 0
invokeinterface com/a/b/d/oh/b()Lcom/a/b/d/oh; 0
invokestatic com/a/b/d/oc/b(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V
aload 0
aload 1
invokestatic com/a/b/d/oc/b(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/io/ObjectInputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectInputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 0
new com/a/b/d/oe
dup
aconst_null
aconst_null
iconst_0
aconst_null
invokespecial com/a/b/d/oe/<init>(Ljava/lang/Object;Ljava/lang/Object;ILcom/a/b/d/oe;)V
putfield com/a/b/d/oc/e Lcom/a/b/d/oe;
aload 0
getfield com/a/b/d/oc/e Lcom/a/b/d/oe;
aload 0
getfield com/a/b/d/oc/e Lcom/a/b/d/oe;
invokestatic com/a/b/d/oc/b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V
aload 0
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
putfield com/a/b/d/oc/b I
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
istore 3
new java/util/LinkedHashMap
dup
iload 3
invokestatic com/a/b/d/sz/b(I)I
invokespecial java/util/LinkedHashMap/<init>(I)V
astore 4
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
astore 5
aload 4
aload 5
aload 0
aload 5
invokevirtual com/a/b/d/oc/e(Ljava/lang/Object;)Ljava/util/Collection;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
istore 3
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L3
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
astore 5
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
astore 6
aload 4
aload 5
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
aload 6
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
pop
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aload 0
aload 4
invokevirtual com/a/b/d/oc/a(Ljava/util/Map;)V
return
.limit locals 7
.limit stack 7
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 1
aload 0
getfield com/a/b/d/oc/b I
invokevirtual java/io/ObjectOutputStream/writeInt(I)V
aload 1
aload 0
invokevirtual com/a/b/d/oc/p()Ljava/util/Set;
invokeinterface java/util/Set/size()I 0
invokevirtual java/io/ObjectOutputStream/writeInt(I)V
aload 0
invokevirtual com/a/b/d/oc/p()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
goto L0
L1:
aload 1
aload 0
invokevirtual com/a/b/d/oc/f()I
invokevirtual java/io/ObjectOutputStream/writeInt(I)V
aload 0
invokevirtual com/a/b/d/oc/u()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L2:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 1
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 1
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
goto L2
L3:
return
.limit locals 4
.limit stack 2
.end method

.method private static b(Lcom/a/b/d/vi;)Lcom/a/b/d/oc;
new com/a/b/d/oc
dup
aload 0
invokeinterface com/a/b/d/vi/p()Ljava/util/Set; 0
invokeinterface java/util/Set/size()I 0
invokestatic com/a/b/d/sz/b(I)I
iconst_2
invokestatic com/a/b/d/sz/b(I)I
invokespecial com/a/b/d/oc/<init>(II)V
astore 1
aload 1
aload 0
invokevirtual com/a/b/d/oc/a(Lcom/a/b/d/vi;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 4
.end method

.method private static b(Lcom/a/b/d/oe;)V
aload 0
getfield com/a/b/d/oe/g Lcom/a/b/d/oe;
aload 0
getfield com/a/b/d/oe/h Lcom/a/b/d/oe;
invokestatic com/a/b/d/oc/b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V
return
.limit locals 1
.limit stack 2
.end method

.method private static b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V
aload 0
aload 1
putfield com/a/b/d/oe/h Lcom/a/b/d/oe;
aload 1
aload 0
putfield com/a/b/d/oe/g Lcom/a/b/d/oe;
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Lcom/a/b/d/oh;)V
aload 0
invokeinterface com/a/b/d/oh/a()Lcom/a/b/d/oh; 0
aload 0
invokeinterface com/a/b/d/oh/b()Lcom/a/b/d/oh; 0
invokestatic com/a/b/d/oc/b(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V
return
.limit locals 1
.limit stack 2
.end method

.method private static b(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V
aload 0
aload 1
invokeinterface com/a/b/d/oh/b(Lcom/a/b/d/oh;)V 1
aload 1
aload 0
invokeinterface com/a/b/d/oh/a(Lcom/a/b/d/oh;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private static v()Lcom/a/b/d/oc;
new com/a/b/d/oc
dup
bipush 16
iconst_2
invokespecial com/a/b/d/oc/<init>(II)V
areturn
.limit locals 0
.limit stack 4
.end method

.method final a()Ljava/util/Set;
new java/util/LinkedHashSet
dup
aload 0
getfield com/a/b/d/oc/b I
invokespecial java/util/LinkedHashSet/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final volatile synthetic a(Ljava/lang/Object;)Ljava/util/Set;
aload 0
aload 1
invokespecial com/a/b/d/ba/a(Ljava/lang/Object;)Ljava/util/Set;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokespecial com/a/b/d/ba/a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic a(Lcom/a/b/d/vi;)Z
aload 0
aload 1
invokespecial com/a/b/d/ba/a(Lcom/a/b/d/vi;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/ba/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/oc/a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic b()Ljava/util/Map;
aload 0
invokespecial com/a/b/d/ba/b()Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic b(Ljava/lang/Object;)Ljava/util/Set;
aload 0
aload 1
invokespecial com/a/b/d/ba/b(Ljava/lang/Object;)Ljava/util/Set;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/ba/b(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method final synthetic c()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/oc/a()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/ba/c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/ba/c(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method final e(Ljava/lang/Object;)Ljava/util/Collection;
new com/a/b/d/of
dup
aload 0
aload 1
aload 0
getfield com/a/b/d/oc/b I
invokespecial com/a/b/d/of/<init>(Lcom/a/b/d/oc;Ljava/lang/Object;I)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public final volatile synthetic equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ba/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic f()I
aload 0
invokespecial com/a/b/d/ba/f()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic f(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ba/f(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final g()V
aload 0
invokespecial com/a/b/d/ba/g()V
aload 0
getfield com/a/b/d/oc/e Lcom/a/b/d/oe;
aload 0
getfield com/a/b/d/oc/e Lcom/a/b/d/oe;
invokestatic com/a/b/d/oc/b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V
return
.limit locals 1
.limit stack 2
.end method

.method public final volatile synthetic g(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ba/g(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic hashCode()I
aload 0
invokespecial com/a/b/d/ba/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final i()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/ba/i()Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 1
.end method

.method final j()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/oc/l()Ljava/util/Iterator;
invokestatic com/a/b/d/sz/b(Ljava/util/Iterator;)Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic k()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/oc/u()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method final l()Ljava/util/Iterator;
new com/a/b/d/od
dup
aload 0
invokespecial com/a/b/d/od/<init>(Lcom/a/b/d/oc;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final volatile synthetic n()Z
aload 0
invokespecial com/a/b/d/ba/n()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic p()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/ba/p()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic q()Lcom/a/b/d/xc;
aload 0
invokespecial com/a/b/d/ba/q()Lcom/a/b/d/xc;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic toString()Ljava/lang/String;
aload 0
invokespecial com/a/b/d/ba/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final u()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/ba/u()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method
