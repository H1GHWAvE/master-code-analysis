.bytecode 50.0
.class synchronized com/a/b/d/pf
.super java/util/AbstractList

.field final 'a' Ljava/util/List;

.method <init>(Ljava/util/List;)V
aload 0
invokespecial java/util/AbstractList/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/List
putfield com/a/b/d/pf/a Ljava/util/List;
return
.limit locals 2
.limit stack 2
.end method

.method private a(I)I
aload 0
invokevirtual com/a/b/d/pf/size()I
istore 2
iload 1
iload 2
invokestatic com/a/b/b/cn/a(II)I
pop
iload 2
iconst_1
isub
iload 1
isub
ireturn
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/pf;I)I
aload 0
iload 1
invokespecial com/a/b/d/pf/b(I)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method private a()Ljava/util/List;
aload 0
getfield com/a/b/d/pf/a Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)I
aload 0
invokevirtual com/a/b/d/pf/size()I
istore 2
iload 1
iload 2
invokestatic com/a/b/b/cn/b(II)I
pop
iload 2
iload 1
isub
ireturn
.limit locals 3
.limit stack 2
.end method

.method public add(ILjava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/pf/a Ljava/util/List;
aload 0
iload 1
invokespecial com/a/b/d/pf/b(I)I
aload 2
invokeinterface java/util/List/add(ILjava/lang/Object;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public clear()V
aload 0
getfield com/a/b/d/pf/a Ljava/util/List;
invokeinterface java/util/List/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public get(I)Ljava/lang/Object;
aload 0
getfield com/a/b/d/pf/a Ljava/util/List;
aload 0
iload 1
invokespecial com/a/b/d/pf/a(I)I
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public iterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/pf/listIterator()Ljava/util/ListIterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public listIterator(I)Ljava/util/ListIterator;
aload 0
iload 1
invokespecial com/a/b/d/pf/b(I)I
istore 1
new com/a/b/d/pg
dup
aload 0
aload 0
getfield com/a/b/d/pf/a Ljava/util/List;
iload 1
invokeinterface java/util/List/listIterator(I)Ljava/util/ListIterator; 1
invokespecial com/a/b/d/pg/<init>(Lcom/a/b/d/pf;Ljava/util/ListIterator;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public remove(I)Ljava/lang/Object;
aload 0
getfield com/a/b/d/pf/a Ljava/util/List;
aload 0
iload 1
invokespecial com/a/b/d/pf/a(I)I
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 3
.end method

.method protected removeRange(II)V
aload 0
iload 1
iload 2
invokevirtual com/a/b/d/pf/subList(II)Ljava/util/List;
invokeinterface java/util/List/clear()V 0
return
.limit locals 3
.limit stack 3
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/pf/a Ljava/util/List;
aload 0
iload 1
invokespecial com/a/b/d/pf/a(I)I
aload 2
invokeinterface java/util/List/set(ILjava/lang/Object;)Ljava/lang/Object; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public size()I
aload 0
getfield com/a/b/d/pf/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public subList(II)Ljava/util/List;
iload 1
iload 2
aload 0
invokevirtual com/a/b/d/pf/size()I
invokestatic com/a/b/b/cn/a(III)V
aload 0
getfield com/a/b/d/pf/a Ljava/util/List;
aload 0
iload 2
invokespecial com/a/b/d/pf/b(I)I
aload 0
iload 1
invokespecial com/a/b/d/pf/b(I)I
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
astore 3
aload 3
instanceof com/a/b/d/jl
ifeq L0
aload 3
checkcast com/a/b/d/jl
invokevirtual com/a/b/d/jl/e()Lcom/a/b/d/jl;
areturn
L0:
aload 3
instanceof com/a/b/d/pf
ifeq L1
aload 3
checkcast com/a/b/d/pf
getfield com/a/b/d/pf/a Ljava/util/List;
areturn
L1:
aload 3
instanceof java/util/RandomAccess
ifeq L2
new com/a/b/d/pe
dup
aload 3
invokespecial com/a/b/d/pe/<init>(Ljava/util/List;)V
areturn
L2:
new com/a/b/d/pf
dup
aload 3
invokespecial com/a/b/d/pf/<init>(Ljava/util/List;)V
areturn
.limit locals 4
.limit stack 4
.end method
