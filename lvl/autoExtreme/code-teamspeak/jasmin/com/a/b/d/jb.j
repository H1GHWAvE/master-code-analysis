.bytecode 50.0
.class public synchronized abstract com/a/b/d/jb
.super java/lang/Object

.field static final 'c' I = 4


.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(II)I
iload 1
ifge L0
new java/lang/AssertionError
dup
ldc "cannot store more than MAX_VALUE elements"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
L0:
iload 0
iconst_1
ishr
iload 0
iadd
iconst_1
iadd
istore 2
iload 2
istore 0
iload 2
iload 1
if_icmpge L1
iload 1
iconst_1
isub
invokestatic java/lang/Integer/highestOneBit(I)I
iconst_1
ishl
istore 0
L1:
iload 0
istore 1
iload 0
ifge L2
ldc_w 2147483647
istore 1
L2:
iload 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public abstract a()Lcom/a/b/d/iz;
.end method

.method public a(Ljava/lang/Iterable;)Lcom/a/b/d/jb;
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokevirtual com/a/b/d/jb/b(Ljava/lang/Object;)Lcom/a/b/d/jb;
pop
goto L0
L1:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public a(Ljava/util/Iterator;)Lcom/a/b/d/jb;
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokevirtual com/a/b/d/jb/b(Ljava/lang/Object;)Lcom/a/b/d/jb;
pop
goto L0
L1:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public transient a([Ljava/lang/Object;)Lcom/a/b/d/jb;
aload 1
arraylength
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
aload 1
iload 2
aaload
invokevirtual com/a/b/d/jb/b(Ljava/lang/Object;)Lcom/a/b/d/jb;
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
areturn
.limit locals 4
.limit stack 3
.end method

.method public abstract b(Ljava/lang/Object;)Lcom/a/b/d/jb;
.end method
