.bytecode 50.0
.class synchronized com/a/b/d/add
.super com/a/b/d/adn
.implements java/util/Collection
.annotation invisible Lcom/a/b/a/d;
.end annotation

.field private static final 'a' J = 0L


.method private <init>(Ljava/util/Collection;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokespecial com/a/b/d/adn/<init>(Ljava/lang/Object;Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 3
.end method

.method synthetic <init>(Ljava/util/Collection;Ljava/lang/Object;B)V
aload 0
aload 1
aload 2
invokespecial com/a/b/d/add/<init>(Ljava/util/Collection;Ljava/lang/Object;)V
return
.limit locals 4
.limit stack 3
.end method

.method public add(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/add/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public addAll(Ljava/util/Collection;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/add/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/addAll(Ljava/util/Collection;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method b()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/adn/d()Ljava/lang/Object;
checkcast java/util/Collection
areturn
.limit locals 1
.limit stack 1
.end method

.method public clear()V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/add/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
invokeinterface java/util/Collection/clear()V 0
aload 1
monitorexit
L1:
return
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public contains(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/add/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/contains(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public containsAll(Ljava/util/Collection;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/add/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/containsAll(Ljava/util/Collection;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method synthetic d()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 1
.end method

.method public isEmpty()Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/add/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
invokeinterface java/util/Collection/isEmpty()Z 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public iterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public remove(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/add/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/remove(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public removeAll(Ljava/util/Collection;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/add/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/removeAll(Ljava/util/Collection;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public retainAll(Ljava/util/Collection;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/add/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/retainAll(Ljava/util/Collection;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public size()I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/add/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
invokeinterface java/util/Collection/size()I 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public toArray()[Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/add/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/add/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/add/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method
