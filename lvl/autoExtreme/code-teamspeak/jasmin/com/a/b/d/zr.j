.bytecode 50.0
.class synchronized abstract com/a/b/d/zr
.super com/a/b/d/mi
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method <init>()V
aload 0
invokespecial com/a/b/d/mi/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/zr;
aload 0
aconst_null
aconst_null
invokestatic com/a/b/d/zr/a(Ljava/lang/Iterable;Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/zr;
areturn
.limit locals 1
.limit stack 3
.end method

.method static final a(Ljava/lang/Iterable;Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/zr;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
invokestatic com/a/b/d/lo/i()Lcom/a/b/d/lp;
astore 3
invokestatic com/a/b/d/lo/i()Lcom/a/b/d/lp;
astore 5
aload 0
invokestatic com/a/b/d/jl/a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
astore 4
aload 4
invokevirtual com/a/b/d/jl/iterator()Ljava/util/Iterator;
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/adw
astore 6
aload 3
aload 6
invokeinterface com/a/b/d/adw/a()Ljava/lang/Object; 0
invokevirtual com/a/b/d/lp/c(Ljava/lang/Object;)Lcom/a/b/d/lp;
pop
aload 5
aload 6
invokeinterface com/a/b/d/adw/b()Ljava/lang/Object; 0
invokevirtual com/a/b/d/lp/c(Ljava/lang/Object;)Lcom/a/b/d/lp;
pop
goto L0
L1:
aload 3
invokevirtual com/a/b/d/lp/b()Lcom/a/b/d/lo;
astore 3
aload 3
astore 0
aload 1
ifnull L2
aload 3
invokestatic com/a/b/d/ov/a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
astore 0
aload 0
aload 1
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
aload 0
invokestatic com/a/b/d/lo/a(Ljava/util/Collection;)Lcom/a/b/d/lo;
astore 0
L2:
aload 5
invokevirtual com/a/b/d/lp/b()Lcom/a/b/d/lo;
astore 3
aload 3
astore 1
aload 2
ifnull L3
aload 3
invokestatic com/a/b/d/ov/a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
astore 1
aload 1
aload 2
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
aload 1
invokestatic com/a/b/d/lo/a(Ljava/util/Collection;)Lcom/a/b/d/lo;
astore 1
L3:
aload 4
invokevirtual com/a/b/d/jl/size()I
i2l
aload 0
invokevirtual com/a/b/d/lo/size()I
i2l
aload 1
invokevirtual com/a/b/d/lo/size()I
i2l
lmul
ldc2_w 2L
ldiv
lcmp
ifle L4
new com/a/b/d/ec
dup
aload 4
aload 0
aload 1
invokespecial com/a/b/d/ec/<init>(Lcom/a/b/d/jl;Lcom/a/b/d/lo;Lcom/a/b/d/lo;)V
areturn
L4:
new com/a/b/d/abt
dup
aload 4
aload 0
aload 1
invokespecial com/a/b/d/abt/<init>(Lcom/a/b/d/jl;Lcom/a/b/d/lo;Lcom/a/b/d/lo;)V
areturn
.limit locals 7
.limit stack 6
.end method

.method static a(Ljava/util/List;Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/zr;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ifnonnull L0
aload 2
ifnull L1
L0:
aload 0
new com/a/b/d/zs
dup
aload 1
aload 2
invokespecial com/a/b/d/zs/<init>(Ljava/util/Comparator;Ljava/util/Comparator;)V
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
L1:
aload 0
aload 1
aload 2
invokestatic com/a/b/d/zr/a(Ljava/lang/Iterable;Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/zr;
areturn
.limit locals 3
.limit stack 5
.end method

.method abstract a(I)Lcom/a/b/d/adw;
.end method

.method abstract b(I)Ljava/lang/Object;
.end method

.method final synthetic f()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/zr/q()Lcom/a/b/d/lo;
areturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic i()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/zr/r()Lcom/a/b/d/iz;
areturn
.limit locals 1
.limit stack 1
.end method

.method final q()Lcom/a/b/d/lo;
aload 0
invokevirtual com/a/b/d/zr/c()Z
ifeq L0
invokestatic com/a/b/d/lo/h()Lcom/a/b/d/lo;
areturn
L0:
new com/a/b/d/zt
dup
aload 0
iconst_0
invokespecial com/a/b/d/zt/<init>(Lcom/a/b/d/zr;B)V
areturn
.limit locals 1
.limit stack 4
.end method

.method final r()Lcom/a/b/d/iz;
aload 0
invokevirtual com/a/b/d/zr/c()Z
ifeq L0
invokestatic com/a/b/d/jl/d()Lcom/a/b/d/jl;
areturn
L0:
new com/a/b/d/zv
dup
aload 0
iconst_0
invokespecial com/a/b/d/zv/<init>(Lcom/a/b/d/zr;B)V
areturn
.limit locals 1
.limit stack 4
.end method
