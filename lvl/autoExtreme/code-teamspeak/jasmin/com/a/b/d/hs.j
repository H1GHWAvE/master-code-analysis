.bytecode 50.0
.class final synchronized com/a/b/d/hs
.super java/lang/Object
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation

.field final 'a' Ljava/util/Comparator;

.field final 'b' Z

.field final 'c' Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.end field

.field final 'd' Lcom/a/b/d/ce;

.field final 'e' Z

.field final 'f' Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.end field

.field final 'g' Lcom/a/b/d/ce;

.field private transient 'h' Lcom/a/b/d/hs;

.method <init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 6 Ljavax/annotation/Nullable;
.end annotation
iconst_1
istore 9
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
putfield com/a/b/d/hs/a Ljava/util/Comparator;
aload 0
iload 2
putfield com/a/b/d/hs/b Z
aload 0
iload 5
putfield com/a/b/d/hs/e Z
aload 0
aload 3
putfield com/a/b/d/hs/c Ljava/lang/Object;
aload 0
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/ce
putfield com/a/b/d/hs/d Lcom/a/b/d/ce;
aload 0
aload 6
putfield com/a/b/d/hs/f Ljava/lang/Object;
aload 0
aload 7
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/ce
putfield com/a/b/d/hs/g Lcom/a/b/d/ce;
iload 2
ifeq L0
aload 1
aload 3
aload 3
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
pop
L0:
iload 5
ifeq L1
aload 1
aload 6
aload 6
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
pop
L1:
iload 2
ifeq L2
iload 5
ifeq L2
aload 1
aload 3
aload 6
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
istore 8
iload 8
ifgt L3
iconst_1
istore 2
L4:
iload 2
ldc "lowerEndpoint (%s) > upperEndpoint (%s)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 3
aastore
dup
iconst_1
aload 6
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 8
ifne L2
aload 4
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
if_acmpeq L5
iconst_1
istore 8
L6:
aload 7
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
if_acmpeq L7
L8:
iload 8
iload 9
ior
invokestatic com/a/b/b/cn/a(Z)V
L2:
return
L3:
iconst_0
istore 2
goto L4
L5:
iconst_0
istore 8
goto L6
L7:
iconst_0
istore 9
goto L8
.limit locals 10
.limit stack 6
.end method

.method private static a(Lcom/a/b/d/yl;)Lcom/a/b/d/hs;
aload 0
invokevirtual com/a/b/d/yl/d()Z
ifeq L0
aload 0
getfield com/a/b/d/yl/b Lcom/a/b/d/dw;
invokevirtual com/a/b/d/dw/c()Ljava/lang/Comparable;
astore 1
L1:
aload 0
invokevirtual com/a/b/d/yl/d()Z
ifeq L2
aload 0
getfield com/a/b/d/yl/b Lcom/a/b/d/dw;
invokevirtual com/a/b/d/dw/a()Lcom/a/b/d/ce;
astore 2
L3:
aload 0
invokevirtual com/a/b/d/yl/e()Z
ifeq L4
aload 0
getfield com/a/b/d/yl/c Lcom/a/b/d/dw;
invokevirtual com/a/b/d/dw/c()Ljava/lang/Comparable;
astore 3
L5:
aload 0
invokevirtual com/a/b/d/yl/e()Z
ifeq L6
aload 0
getfield com/a/b/d/yl/c Lcom/a/b/d/dw;
invokevirtual com/a/b/d/dw/b()Lcom/a/b/d/ce;
astore 4
L7:
new com/a/b/d/hs
dup
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
aload 0
invokevirtual com/a/b/d/yl/d()Z
aload 1
aload 2
aload 0
invokevirtual com/a/b/d/yl/e()Z
aload 3
aload 4
invokespecial com/a/b/d/hs/<init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V
areturn
L0:
aconst_null
astore 1
goto L1
L2:
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
astore 2
goto L3
L4:
aconst_null
astore 3
goto L5
L6:
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
astore 4
goto L7
.limit locals 5
.limit stack 9
.end method

.method static a(Ljava/util/Comparator;)Lcom/a/b/d/hs;
new com/a/b/d/hs
dup
aload 0
iconst_0
aconst_null
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
iconst_0
aconst_null
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
invokespecial com/a/b/d/hs/<init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V
areturn
.limit locals 1
.limit stack 9
.end method

.method private static a(Ljava/util/Comparator;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/hs;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/hs
dup
aload 0
iconst_1
aload 1
aload 2
iconst_0
aconst_null
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
invokespecial com/a/b/d/hs/<init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V
areturn
.limit locals 3
.limit stack 9
.end method

.method private static a(Ljava/util/Comparator;Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/hs;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 4 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/hs
dup
aload 0
iconst_1
aload 1
aload 2
iconst_1
aload 3
aload 4
invokespecial com/a/b/d/hs/<init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V
areturn
.limit locals 5
.limit stack 9
.end method

.method private a()Ljava/util/Comparator;
aload 0
getfield com/a/b/d/hs/a Ljava/util/Comparator;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/util/Comparator;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/hs;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/hs
dup
aload 0
iconst_0
aconst_null
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
iconst_1
aload 1
aload 2
invokespecial com/a/b/d/hs/<init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V
areturn
.limit locals 3
.limit stack 9
.end method

.method private b()Z
aload 0
getfield com/a/b/d/hs/b Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()Z
aload 0
getfield com/a/b/d/hs/e Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()Z
aload 0
getfield com/a/b/d/hs/e Z
ifeq L0
aload 0
aload 0
getfield com/a/b/d/hs/f Ljava/lang/Object;
invokevirtual com/a/b/d/hs/a(Ljava/lang/Object;)Z
ifne L1
L0:
aload 0
getfield com/a/b/d/hs/b Z
ifeq L2
aload 0
aload 0
getfield com/a/b/d/hs/c Ljava/lang/Object;
invokevirtual com/a/b/d/hs/b(Ljava/lang/Object;)Z
ifeq L2
L1:
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private e()Lcom/a/b/d/hs;
aload 0
getfield com/a/b/d/hs/h Lcom/a/b/d/hs;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
new com/a/b/d/hs
dup
aload 0
getfield com/a/b/d/hs/a Ljava/util/Comparator;
invokestatic com/a/b/d/yd/a(Ljava/util/Comparator;)Lcom/a/b/d/yd;
invokevirtual com/a/b/d/yd/a()Lcom/a/b/d/yd;
aload 0
getfield com/a/b/d/hs/e Z
aload 0
getfield com/a/b/d/hs/f Ljava/lang/Object;
aload 0
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
aload 0
getfield com/a/b/d/hs/b Z
aload 0
getfield com/a/b/d/hs/c Ljava/lang/Object;
aload 0
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
invokespecial com/a/b/d/hs/<init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V
astore 1
aload 1
aload 0
putfield com/a/b/d/hs/h Lcom/a/b/d/hs;
aload 0
aload 1
putfield com/a/b/d/hs/h Lcom/a/b/d/hs;
L0:
aload 1
areturn
.limit locals 3
.limit stack 9
.end method

.method private f()Ljava/lang/Object;
aload 0
getfield com/a/b/d/hs/c Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Lcom/a/b/d/ce;
aload 0
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
areturn
.limit locals 1
.limit stack 1
.end method

.method private h()Ljava/lang/Object;
aload 0
getfield com/a/b/d/hs/f Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private i()Lcom/a/b/d/ce;
aload 0
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
areturn
.limit locals 1
.limit stack 1
.end method

.method final a(Lcom/a/b/d/hs;)Lcom/a/b/d/hs;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/d/hs/a Ljava/util/Comparator;
aload 1
getfield com/a/b/d/hs/a Ljava/util/Comparator;
invokeinterface java/util/Comparator/equals(Ljava/lang/Object;)Z 1
invokestatic com/a/b/b/cn/a(Z)V
aload 0
getfield com/a/b/d/hs/b Z
istore 4
aload 0
getfield com/a/b/d/hs/c Ljava/lang/Object;
astore 6
aload 0
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
astore 7
aload 0
getfield com/a/b/d/hs/b Z
ifne L0
aload 1
getfield com/a/b/d/hs/b Z
istore 3
L1:
aload 1
getfield com/a/b/d/hs/c Ljava/lang/Object;
astore 6
aload 1
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
astore 7
iload 3
istore 4
L2:
aload 0
getfield com/a/b/d/hs/e Z
istore 5
aload 0
getfield com/a/b/d/hs/f Ljava/lang/Object;
astore 9
aload 0
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
astore 8
aload 0
getfield com/a/b/d/hs/e Z
ifne L3
aload 1
getfield com/a/b/d/hs/e Z
istore 3
L4:
aload 1
getfield com/a/b/d/hs/f Ljava/lang/Object;
astore 9
aload 1
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
astore 8
aload 9
astore 1
L5:
iload 4
ifeq L6
iload 3
ifeq L6
aload 0
getfield com/a/b/d/hs/a Ljava/util/Comparator;
aload 6
aload 1
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
istore 2
iload 2
ifgt L7
iload 2
ifne L6
aload 7
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
if_acmpne L6
aload 8
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
if_acmpne L6
L7:
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
astore 7
getstatic com/a/b/d/ce/b Lcom/a/b/d/ce;
astore 8
aload 1
astore 6
L8:
new com/a/b/d/hs
dup
aload 0
getfield com/a/b/d/hs/a Ljava/util/Comparator;
iload 4
aload 6
aload 7
iload 3
aload 1
aload 8
invokespecial com/a/b/d/hs/<init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V
areturn
L0:
aload 1
getfield com/a/b/d/hs/b Z
ifeq L9
aload 0
getfield com/a/b/d/hs/a Ljava/util/Comparator;
aload 0
getfield com/a/b/d/hs/c Ljava/lang/Object;
aload 1
getfield com/a/b/d/hs/c Ljava/lang/Object;
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
istore 2
iload 4
istore 3
iload 2
iflt L1
iload 2
ifne L9
iload 4
istore 3
aload 1
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
if_acmpeq L1
L9:
goto L2
L3:
aload 1
getfield com/a/b/d/hs/e Z
ifeq L10
aload 0
getfield com/a/b/d/hs/a Ljava/util/Comparator;
aload 0
getfield com/a/b/d/hs/f Ljava/lang/Object;
aload 1
getfield com/a/b/d/hs/f Ljava/lang/Object;
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
istore 2
iload 5
istore 3
iload 2
ifgt L4
iload 2
ifne L10
iload 5
istore 3
aload 1
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
if_acmpeq L4
L10:
iload 5
istore 3
aload 9
astore 1
goto L5
L6:
goto L8
.limit locals 10
.limit stack 9
.end method

.method final a(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_1
istore 4
aload 0
getfield com/a/b/d/hs/b Z
ifne L0
iconst_0
ireturn
L0:
aload 0
getfield com/a/b/d/hs/c Ljava/lang/Object;
astore 5
aload 0
getfield com/a/b/d/hs/a Ljava/util/Comparator;
aload 1
aload 5
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
istore 3
iload 3
ifge L1
iconst_1
istore 2
L2:
iload 3
ifne L3
iconst_1
istore 3
L4:
aload 0
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
if_acmpne L5
L6:
iload 2
iload 3
iload 4
iand
ior
ireturn
L1:
iconst_0
istore 2
goto L2
L3:
iconst_0
istore 3
goto L4
L5:
iconst_0
istore 4
goto L6
.limit locals 6
.limit stack 3
.end method

.method final b(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_1
istore 4
aload 0
getfield com/a/b/d/hs/e Z
ifne L0
iconst_0
ireturn
L0:
aload 0
getfield com/a/b/d/hs/f Ljava/lang/Object;
astore 5
aload 0
getfield com/a/b/d/hs/a Ljava/util/Comparator;
aload 1
aload 5
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
istore 3
iload 3
ifle L1
iconst_1
istore 2
L2:
iload 3
ifne L3
iconst_1
istore 3
L4:
aload 0
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
if_acmpne L5
L6:
iload 2
iload 3
iload 4
iand
ior
ireturn
L1:
iconst_0
istore 2
goto L2
L3:
iconst_0
istore 3
goto L4
L5:
iconst_0
istore 4
goto L6
.limit locals 6
.limit stack 3
.end method

.method final c(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokevirtual com/a/b/d/hs/a(Ljava/lang/Object;)Z
ifne L0
aload 0
aload 1
invokevirtual com/a/b/d/hs/b(Ljava/lang/Object;)Z
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/d/hs
ifeq L0
aload 1
checkcast com/a/b/d/hs
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/d/hs/a Ljava/util/Comparator;
aload 1
getfield com/a/b/d/hs/a Ljava/util/Comparator;
invokeinterface java/util/Comparator/equals(Ljava/lang/Object;)Z 1
ifeq L0
iload 3
istore 2
aload 0
getfield com/a/b/d/hs/b Z
aload 1
getfield com/a/b/d/hs/b Z
if_icmpne L0
iload 3
istore 2
aload 0
getfield com/a/b/d/hs/e Z
aload 1
getfield com/a/b/d/hs/e Z
if_icmpne L0
iload 3
istore 2
aload 0
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
aload 1
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
invokevirtual com/a/b/d/ce/equals(Ljava/lang/Object;)Z
ifeq L0
iload 3
istore 2
aload 0
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
aload 1
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
invokevirtual com/a/b/d/ce/equals(Ljava/lang/Object;)Z
ifeq L0
iload 3
istore 2
aload 0
getfield com/a/b/d/hs/c Ljava/lang/Object;
aload 1
getfield com/a/b/d/hs/c Ljava/lang/Object;
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
iload 3
istore 2
aload 0
getfield com/a/b/d/hs/f Ljava/lang/Object;
aload 1
getfield com/a/b/d/hs/f Ljava/lang/Object;
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final hashCode()I
iconst_5
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/d/hs/a Ljava/util/Comparator;
aastore
dup
iconst_1
aload 0
getfield com/a/b/d/hs/c Ljava/lang/Object;
aastore
dup
iconst_2
aload 0
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
aastore
dup
iconst_3
aload 0
getfield com/a/b/d/hs/f Ljava/lang/Object;
aastore
dup
iconst_4
aload 0
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
aastore
invokestatic java/util/Arrays/hashCode([Ljava/lang/Object;)I
ireturn
.limit locals 1
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/a/b/d/hs/a Ljava/util/Comparator;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 2
aload 0
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
getstatic com/a/b/d/ce/b Lcom/a/b/d/ce;
if_acmpne L0
bipush 91
istore 1
L1:
aload 2
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
astore 3
aload 0
getfield com/a/b/d/hs/b Z
ifeq L2
aload 0
getfield com/a/b/d/hs/c Ljava/lang/Object;
astore 2
L3:
aload 3
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
bipush 44
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
astore 3
aload 0
getfield com/a/b/d/hs/e Z
ifeq L4
aload 0
getfield com/a/b/d/hs/f Ljava/lang/Object;
astore 2
L5:
aload 3
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
astore 2
aload 0
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
getstatic com/a/b/d/ce/b Lcom/a/b/d/ce;
if_acmpne L6
bipush 93
istore 1
L7:
aload 2
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
bipush 40
istore 1
goto L1
L2:
ldc "-\u221e"
astore 2
goto L3
L4:
ldc "\u221e"
astore 2
goto L5
L6:
bipush 41
istore 1
goto L7
.limit locals 4
.limit stack 2
.end method
