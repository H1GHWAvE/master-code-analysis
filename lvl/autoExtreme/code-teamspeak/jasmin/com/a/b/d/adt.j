.bytecode 50.0
.class synchronized com/a/b/d/adt
.super com/a/b/d/adq
.implements java/util/SortedSet

.field private static final 'a' J = 0L


.method <init>(Ljava/util/SortedSet;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokespecial com/a/b/d/adq/<init>(Ljava/util/Set;Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 3
.end method

.method a()Ljava/util/SortedSet;
aload 0
invokespecial com/a/b/d/adq/c()Ljava/util/Set;
checkcast java/util/SortedSet
areturn
.limit locals 1
.limit stack 1
.end method

.method synthetic b()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/adt/a()Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method synthetic c()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/adt/a()Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public comparator()Ljava/util/Comparator;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adt/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adt/a()Ljava/util/SortedSet;
invokeinterface java/util/SortedSet/comparator()Ljava/util/Comparator; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method synthetic d()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/adt/a()Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public first()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adt/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adt/a()Ljava/util/SortedSet;
invokeinterface java/util/SortedSet/first()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adt/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adt/a()Ljava/util/SortedSet;
aload 1
invokeinterface java/util/SortedSet/headSet(Ljava/lang/Object;)Ljava/util/SortedSet; 1
aload 0
getfield com/a/b/d/adt/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public last()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adt/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adt/a()Ljava/util/SortedSet;
invokeinterface java/util/SortedSet/last()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adt/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adt/a()Ljava/util/SortedSet;
aload 1
aload 2
invokeinterface java/util/SortedSet/subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet; 2
aload 0
getfield com/a/b/d/adt/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;
astore 1
aload 3
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adt/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adt/a()Ljava/util/SortedSet;
aload 1
invokeinterface java/util/SortedSet/tailSet(Ljava/lang/Object;)Ljava/util/SortedSet; 1
aload 0
getfield com/a/b/d/adt/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method
