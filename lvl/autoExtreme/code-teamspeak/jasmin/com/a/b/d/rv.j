.bytecode 50.0
.class synchronized abstract com/a/b/d/rv
.super java/lang/Object
.implements java/util/Iterator

.field 'b' I

.field 'c' I

.field 'd' Lcom/a/b/d/sa;

.field 'e' Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field 'f' Lcom/a/b/d/rz;

.field 'g' Lcom/a/b/d/sy;

.field 'h' Lcom/a/b/d/sy;

.field final synthetic 'i' Lcom/a/b/d/qy;

.method <init>(Lcom/a/b/d/qy;)V
aload 0
aload 1
putfield com/a/b/d/rv/i Lcom/a/b/d/qy;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
getfield com/a/b/d/qy/k [Lcom/a/b/d/sa;
arraylength
iconst_1
isub
putfield com/a/b/d/rv/b I
aload 0
iconst_m1
putfield com/a/b/d/rv/c I
aload 0
invokespecial com/a/b/d/rv/b()V
return
.limit locals 2
.limit stack 3
.end method

.method private a(Lcom/a/b/d/rz;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aconst_null
astore 4
L0:
aload 1
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
astore 6
aload 0
getfield com/a/b/d/rv/i Lcom/a/b/d/qy;
astore 7
aload 1
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
ifnonnull L5
L1:
aload 4
astore 3
L9:
aload 3
ifnull L10
L3:
aload 0
new com/a/b/d/sy
dup
aload 0
getfield com/a/b/d/rv/i Lcom/a/b/d/qy;
aload 6
aload 3
invokespecial com/a/b/d/sy/<init>(Lcom/a/b/d/qy;Ljava/lang/Object;Ljava/lang/Object;)V
putfield com/a/b/d/rv/g Lcom/a/b/d/sy;
L4:
aload 0
getfield com/a/b/d/rv/d Lcom/a/b/d/sa;
invokevirtual com/a/b/d/sa/a()V
iconst_1
ireturn
L5:
aload 1
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
astore 5
L6:
aload 4
astore 3
aload 5
ifnull L9
L7:
aload 7
invokevirtual com/a/b/d/qy/c()Z
ifeq L11
aload 7
aload 1
invokevirtual com/a/b/d/qy/a(Lcom/a/b/d/rz;)Z
istore 2
L8:
aload 4
astore 3
iload 2
ifne L9
L11:
aload 5
astore 3
goto L9
L10:
aload 0
getfield com/a/b/d/rv/d Lcom/a/b/d/sa;
invokevirtual com/a/b/d/sa/a()V
iconst_0
ireturn
L2:
astore 1
aload 0
getfield com/a/b/d/rv/d Lcom/a/b/d/sa;
invokevirtual com/a/b/d/sa/a()V
aload 1
athrow
.limit locals 8
.limit stack 6
.end method

.method private b()V
aload 0
aconst_null
putfield com/a/b/d/rv/g Lcom/a/b/d/sy;
aload 0
invokespecial com/a/b/d/rv/c()Z
ifeq L0
L1:
return
L0:
aload 0
invokespecial com/a/b/d/rv/d()Z
ifne L1
L2:
aload 0
getfield com/a/b/d/rv/b I
iflt L1
aload 0
getfield com/a/b/d/rv/i Lcom/a/b/d/qy;
getfield com/a/b/d/qy/k [Lcom/a/b/d/sa;
astore 2
aload 0
getfield com/a/b/d/rv/b I
istore 1
aload 0
iload 1
iconst_1
isub
putfield com/a/b/d/rv/b I
aload 0
aload 2
iload 1
aaload
putfield com/a/b/d/rv/d Lcom/a/b/d/sa;
aload 0
getfield com/a/b/d/rv/d Lcom/a/b/d/sa;
getfield com/a/b/d/sa/b I
ifeq L2
aload 0
aload 0
getfield com/a/b/d/rv/d Lcom/a/b/d/sa;
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
putfield com/a/b/d/rv/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
aload 0
aload 0
getfield com/a/b/d/rv/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
putfield com/a/b/d/rv/c I
aload 0
invokespecial com/a/b/d/rv/d()Z
ifeq L2
return
.limit locals 3
.limit stack 3
.end method

.method private c()Z
aload 0
getfield com/a/b/d/rv/f Lcom/a/b/d/rz;
ifnull L0
aload 0
aload 0
getfield com/a/b/d/rv/f Lcom/a/b/d/rz;
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
putfield com/a/b/d/rv/f Lcom/a/b/d/rz;
L1:
aload 0
getfield com/a/b/d/rv/f Lcom/a/b/d/rz;
ifnull L0
aload 0
aload 0
getfield com/a/b/d/rv/f Lcom/a/b/d/rz;
invokespecial com/a/b/d/rv/a(Lcom/a/b/d/rz;)Z
ifeq L2
iconst_1
ireturn
L2:
aload 0
aload 0
getfield com/a/b/d/rv/f Lcom/a/b/d/rz;
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
putfield com/a/b/d/rv/f Lcom/a/b/d/rz;
goto L1
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private d()Z
L0:
aload 0
getfield com/a/b/d/rv/c I
iflt L1
aload 0
getfield com/a/b/d/rv/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 2
aload 0
getfield com/a/b/d/rv/c I
istore 1
aload 0
iload 1
iconst_1
isub
putfield com/a/b/d/rv/c I
aload 2
iload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 2
aload 0
aload 2
putfield com/a/b/d/rv/f Lcom/a/b/d/rz;
aload 2
ifnull L0
aload 0
aload 0
getfield com/a/b/d/rv/f Lcom/a/b/d/rz;
invokespecial com/a/b/d/rv/a(Lcom/a/b/d/rz;)Z
ifne L2
aload 0
invokespecial com/a/b/d/rv/c()Z
ifeq L0
L2:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method final a()Lcom/a/b/d/sy;
aload 0
getfield com/a/b/d/rv/g Lcom/a/b/d/sy;
ifnonnull L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
aload 0
getfield com/a/b/d/rv/g Lcom/a/b/d/sy;
putfield com/a/b/d/rv/h Lcom/a/b/d/sy;
aload 0
invokespecial com/a/b/d/rv/b()V
aload 0
getfield com/a/b/d/rv/h Lcom/a/b/d/sy;
areturn
.limit locals 1
.limit stack 2
.end method

.method public hasNext()Z
aload 0
getfield com/a/b/d/rv/g Lcom/a/b/d/sy;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public abstract next()Ljava/lang/Object;
.end method

.method public remove()V
aload 0
getfield com/a/b/d/rv/h Lcom/a/b/d/sy;
ifnull L0
iconst_1
istore 1
L1:
iload 1
ldc "no calls to next() since the last call to remove()"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/d/rv/i Lcom/a/b/d/qy;
aload 0
getfield com/a/b/d/rv/h Lcom/a/b/d/sy;
invokevirtual com/a/b/d/sy/getKey()Ljava/lang/Object;
invokevirtual com/a/b/d/qy/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aconst_null
putfield com/a/b/d/rv/h Lcom/a/b/d/sy;
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method
