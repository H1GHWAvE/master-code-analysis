.bytecode 50.0
.class final synchronized com/a/b/d/xv
.super java/lang/Object
.implements java/util/Iterator

.field private final 'a' Lcom/a/b/d/xc;

.field private final 'b' Ljava/util/Iterator;

.field private 'c' Lcom/a/b/d/xd;

.field private 'd' I

.field private 'e' I

.field private 'f' Z

.method <init>(Lcom/a/b/d/xc;Ljava/util/Iterator;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/b/d/xv/a Lcom/a/b/d/xc;
aload 0
aload 2
putfield com/a/b/d/xv/b Ljava/util/Iterator;
return
.limit locals 3
.limit stack 2
.end method

.method public final hasNext()Z
aload 0
getfield com/a/b/d/xv/d I
ifgt L0
aload 0
getfield com/a/b/d/xv/b Ljava/util/Iterator;
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final next()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/xv/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getfield com/a/b/d/xv/d I
ifne L1
aload 0
aload 0
getfield com/a/b/d/xv/b Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/xd
putfield com/a/b/d/xv/c Lcom/a/b/d/xd;
aload 0
getfield com/a/b/d/xv/c Lcom/a/b/d/xd;
invokeinterface com/a/b/d/xd/b()I 0
istore 1
aload 0
iload 1
putfield com/a/b/d/xv/d I
aload 0
iload 1
putfield com/a/b/d/xv/e I
L1:
aload 0
aload 0
getfield com/a/b/d/xv/d I
iconst_1
isub
putfield com/a/b/d/xv/d I
aload 0
iconst_1
putfield com/a/b/d/xv/f Z
aload 0
getfield com/a/b/d/xv/c Lcom/a/b/d/xd;
invokeinterface com/a/b/d/xd/a()Ljava/lang/Object; 0
areturn
.limit locals 2
.limit stack 3
.end method

.method public final remove()V
aload 0
getfield com/a/b/d/xv/f Z
ldc "no calls to next() since the last call to remove()"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/d/xv/e I
iconst_1
if_icmpne L0
aload 0
getfield com/a/b/d/xv/b Ljava/util/Iterator;
invokeinterface java/util/Iterator/remove()V 0
L1:
aload 0
aload 0
getfield com/a/b/d/xv/e I
iconst_1
isub
putfield com/a/b/d/xv/e I
aload 0
iconst_0
putfield com/a/b/d/xv/f Z
return
L0:
aload 0
getfield com/a/b/d/xv/a Lcom/a/b/d/xc;
aload 0
getfield com/a/b/d/xv/c Lcom/a/b/d/xd;
invokeinterface com/a/b/d/xd/a()Ljava/lang/Object; 0
invokeinterface com/a/b/d/xc/remove(Ljava/lang/Object;)Z 1
pop
goto L1
.limit locals 1
.limit stack 3
.end method
