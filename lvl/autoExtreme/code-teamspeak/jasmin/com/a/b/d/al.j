.bytecode 50.0
.class final synchronized com/a/b/d/al
.super java/lang/Object
.implements java/util/Iterator

.field final 'a' Ljava/util/Iterator;

.field 'b' Ljava/util/Map$Entry;

.field 'c' I

.field 'd' Z

.field final synthetic 'e' Lcom/a/b/d/ai;

.method <init>(Lcom/a/b/d/ai;)V
aload 0
aload 1
putfield com/a/b/d/al/e Lcom/a/b/d/ai;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/d/ai/a(Lcom/a/b/d/ai;)Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
putfield com/a/b/d/al/a Ljava/util/Iterator;
return
.limit locals 2
.limit stack 2
.end method

.method public final hasNext()Z
aload 0
getfield com/a/b/d/al/c I
ifgt L0
aload 0
getfield com/a/b/d/al/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final next()Ljava/lang/Object;
aload 0
getfield com/a/b/d/al/c I
ifne L0
aload 0
aload 0
getfield com/a/b/d/al/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
putfield com/a/b/d/al/b Ljava/util/Map$Entry;
aload 0
aload 0
getfield com/a/b/d/al/b Ljava/util/Map$Entry;
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast com/a/b/d/dv
getfield com/a/b/d/dv/a I
putfield com/a/b/d/al/c I
L0:
aload 0
aload 0
getfield com/a/b/d/al/c I
iconst_1
isub
putfield com/a/b/d/al/c I
aload 0
iconst_1
putfield com/a/b/d/al/d Z
aload 0
getfield com/a/b/d/al/b Ljava/util/Map$Entry;
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 3
.end method

.method public final remove()V
aload 0
getfield com/a/b/d/al/d Z
ldc "no calls to next() since the last call to remove()"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/d/al/b Ljava/util/Map$Entry;
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast com/a/b/d/dv
getfield com/a/b/d/dv/a I
ifgt L0
new java/util/ConcurrentModificationException
dup
invokespecial java/util/ConcurrentModificationException/<init>()V
athrow
L0:
aload 0
getfield com/a/b/d/al/b Ljava/util/Map$Entry;
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast com/a/b/d/dv
iconst_m1
invokevirtual com/a/b/d/dv/a(I)I
ifne L1
aload 0
getfield com/a/b/d/al/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/remove()V 0
L1:
aload 0
getfield com/a/b/d/al/e Lcom/a/b/d/ai;
invokestatic com/a/b/d/ai/b(Lcom/a/b/d/ai;)J
pop2
aload 0
iconst_0
putfield com/a/b/d/al/d Z
return
.limit locals 1
.limit stack 2
.end method
