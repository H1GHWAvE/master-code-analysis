.bytecode 50.0
.class public synchronized abstract com/a/b/d/kk
.super com/a/b/d/an
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field private static final 'a' J = 0L


.field public final transient 'b' Lcom/a/b/d/jt;

.field final transient 'c' I

.method <init>(Lcom/a/b/d/jt;I)V
aload 0
invokespecial com/a/b/d/an/<init>()V
aload 0
aload 1
putfield com/a/b/d/kk/b Lcom/a/b/d/jt;
aload 0
iload 2
putfield com/a/b/d/kk/c I
return
.limit locals 3
.limit stack 2
.end method

.method private A()Lcom/a/b/d/jt;
aload 0
getfield com/a/b/d/kk/b Lcom/a/b/d/jt;
areturn
.limit locals 1
.limit stack 1
.end method

.method private B()Lcom/a/b/d/iz;
new com/a/b/d/kp
dup
aload 0
invokespecial com/a/b/d/kp/<init>(Lcom/a/b/d/kk;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private C()Lcom/a/b/d/ku;
aload 0
invokespecial com/a/b/d/an/q()Lcom/a/b/d/xc;
checkcast com/a/b/d/ku
areturn
.limit locals 1
.limit stack 1
.end method

.method private D()Lcom/a/b/d/ku;
new com/a/b/d/ks
dup
aload 0
invokespecial com/a/b/d/ks/<init>(Lcom/a/b/d/kk;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private E()Lcom/a/b/d/iz;
new com/a/b/d/kt
dup
aload 0
invokespecial com/a/b/d/kt/<init>(Lcom/a/b/d/kk;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a()Lcom/a/b/d/kk;
getstatic com/a/b/d/ex/a Lcom/a/b/d/ex;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kk;
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
astore 4
aload 4
aload 0
aload 1
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 4
aload 2
aload 3
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 4
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
areturn
.limit locals 5
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kk;
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
astore 6
aload 6
aload 0
aload 1
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 6
aload 2
aload 3
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 6
aload 4
aload 5
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 6
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
areturn
.limit locals 7
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kk;
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
astore 8
aload 8
aload 0
aload 1
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 8
aload 2
aload 3
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 8
aload 4
aload 5
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 8
aload 6
aload 7
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 8
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
areturn
.limit locals 9
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kk;
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
astore 10
aload 10
aload 0
aload 1
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 10
aload 2
aload 3
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 10
aload 4
aload 5
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 10
aload 6
aload 7
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 10
aload 8
aload 9
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 10
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
areturn
.limit locals 11
.limit stack 3
.end method

.method public static b(Lcom/a/b/d/vi;)Lcom/a/b/d/kk;
aload 0
instanceof com/a/b/d/kk
ifeq L0
aload 0
checkcast com/a/b/d/kk
astore 2
aload 2
getfield com/a/b/d/kk/b Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/i_()Z
ifne L0
L1:
aload 2
areturn
L0:
aload 0
invokeinterface com/a/b/d/vi/n()Z 0
ifeq L2
getstatic com/a/b/d/ex/a Lcom/a/b/d/ex;
areturn
L2:
aload 0
instanceof com/a/b/d/jr
ifeq L3
aload 0
checkcast com/a/b/d/jr
astore 3
aload 3
astore 2
aload 3
getfield com/a/b/d/kk/b Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/i_()Z
ifeq L1
L3:
invokestatic com/a/b/d/jt/l()Lcom/a/b/d/ju;
astore 2
aload 0
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 0
iconst_0
istore 1
L4:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/util/Collection
invokestatic com/a/b/d/jl/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
astore 4
aload 4
invokevirtual com/a/b/d/jl/isEmpty()Z
ifne L6
aload 2
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 4
invokevirtual com/a/b/d/ju/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;
pop
aload 4
invokevirtual com/a/b/d/jl/size()I
iload 1
iadd
istore 1
L7:
goto L4
L5:
new com/a/b/d/jr
dup
aload 2
invokevirtual com/a/b/d/ju/a()Lcom/a/b/d/jt;
iload 1
invokespecial com/a/b/d/jr/<init>(Lcom/a/b/d/jt;I)V
areturn
L6:
goto L7
.limit locals 5
.limit stack 4
.end method

.method private static c()Lcom/a/b/d/kn;
new com/a/b/d/kn
dup
invokespecial com/a/b/d/kn/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kk;
aload 0
aload 1
invokestatic com/a/b/d/jr/d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;
areturn
.limit locals 2
.limit stack 2
.end method

.method private u()Z
aload 0
getfield com/a/b/d/kk/b Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/i_()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private z()Lcom/a/b/d/lo;
aload 0
getfield com/a/b/d/kk/b Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/g()Lcom/a/b/d/lo;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Lcom/a/b/d/vi;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/kk/e()Lcom/a/b/d/iz;
areturn
.limit locals 3
.limit stack 1
.end method

.method public final volatile synthetic b()Ljava/util/Map;
aload 0
getfield com/a/b/d/kk/b Lcom/a/b/d/jt;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/an/b(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
invokevirtual com/a/b/d/kk/h(Ljava/lang/Object;)Lcom/a/b/d/iz;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public abstract d()Lcom/a/b/d/kk;
.end method

.method public synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/kk/t()Lcom/a/b/d/iz;
areturn
.limit locals 2
.limit stack 1
.end method

.method public e()Lcom/a/b/d/iz;
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public volatile synthetic equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/an/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final f()I
aload 0
getfield com/a/b/d/kk/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final f(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/kk/b Lcom/a/b/d/jt;
aload 1
invokevirtual com/a/b/d/jt/containsKey(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final g()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public final g(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
ifnull L0
aload 0
aload 1
invokespecial com/a/b/d/an/g(Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public abstract h(Ljava/lang/Object;)Lcom/a/b/d/iz;
.end method

.method public volatile synthetic hashCode()I
aload 0
invokespecial com/a/b/d/an/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic i()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/an/i()Ljava/util/Collection;
checkcast com/a/b/d/iz
areturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic j()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/kk/y()Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic k()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/kk/v()Lcom/a/b/d/iz;
areturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic l()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/kk/w()Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 1
.end method

.method final m()Ljava/util/Map;
new java/lang/AssertionError
dup
ldc "should never be called"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public final volatile synthetic n()Z
aload 0
invokespecial com/a/b/d/an/n()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic o()Ljava/util/Collection;
new com/a/b/d/kp
dup
aload 0
invokespecial com/a/b/d/kp/<init>(Lcom/a/b/d/kk;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final synthetic p()Ljava/util/Set;
aload 0
getfield com/a/b/d/kk/b Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/g()Lcom/a/b/d/lo;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic q()Lcom/a/b/d/xc;
aload 0
invokespecial com/a/b/d/an/q()Lcom/a/b/d/xc;
checkcast com/a/b/d/ku
areturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic r()Lcom/a/b/d/xc;
new com/a/b/d/ks
dup
aload 0
invokespecial com/a/b/d/ks/<init>(Lcom/a/b/d/kk;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method final synthetic s()Ljava/util/Collection;
new com/a/b/d/kt
dup
aload 0
invokespecial com/a/b/d/kt/<init>(Lcom/a/b/d/kk;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public t()Lcom/a/b/d/iz;
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public volatile synthetic toString()Ljava/lang/String;
aload 0
invokespecial com/a/b/d/an/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public v()Lcom/a/b/d/iz;
aload 0
invokespecial com/a/b/d/an/k()Ljava/util/Collection;
checkcast com/a/b/d/iz
areturn
.limit locals 1
.limit stack 1
.end method

.method final w()Lcom/a/b/d/agi;
new com/a/b/d/kl
dup
aload 0
invokespecial com/a/b/d/kl/<init>(Lcom/a/b/d/kk;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final x()Lcom/a/b/d/iz;
aload 0
invokespecial com/a/b/d/an/i()Ljava/util/Collection;
checkcast com/a/b/d/iz
areturn
.limit locals 1
.limit stack 1
.end method

.method final y()Lcom/a/b/d/agi;
new com/a/b/d/km
dup
aload 0
invokespecial com/a/b/d/km/<init>(Lcom/a/b/d/kk;)V
areturn
.limit locals 1
.limit stack 3
.end method
