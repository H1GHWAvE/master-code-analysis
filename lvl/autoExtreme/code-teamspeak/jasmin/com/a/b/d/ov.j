.bytecode 50.0
.class public final synchronized com/a/b/d/ov
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/d/jl;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/d/ph
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokespecial com/a/b/d/ph/<init>(Ljava/lang/String;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a()Ljava/util/ArrayList;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method public static a(I)Ljava/util/ArrayList;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
iload 0
ldc "initialArraySize"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
new java/util/ArrayList
dup
iload 0
invokespecial java/util/ArrayList/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
instanceof java/util/Collection
ifeq L0
new java/util/ArrayList
dup
aload 0
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
areturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/ov/a(Ljava/util/Iterator;)Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/util/Iterator;)Ljava/util/ArrayList;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/nj/a(Ljava/util/Collection;Ljava/util/Iterator;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public static transient a([Ljava/lang/Object;)Ljava/util/ArrayList;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new java/util/ArrayList
dup
aload 0
arraylength
invokestatic com/a/b/d/ov/c(I)I
invokespecial java/util/ArrayList/<init>(I)V
astore 1
aload 1
aload 0
invokestatic java/util/Collections/addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Ljava/lang/CharSequence;)Ljava/util/List;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/d/oz
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/CharSequence
invokespecial com/a/b/d/oz/<init>(Ljava/lang/CharSequence;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/pm
dup
aload 0
aload 1
aload 2
invokespecial com/a/b/d/pm/<init>(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method private static a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/pa
dup
aload 0
aload 1
invokespecial com/a/b/d/pa/<init>(Ljava/lang/Object;[Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
aload 0
invokestatic com/a/b/d/ci/a(Ljava/util/List;)Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/List;I)Ljava/util/List;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
ifle L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
aload 0
instanceof java/util/RandomAccess
ifeq L2
new com/a/b/d/pd
dup
aload 0
iload 1
invokespecial com/a/b/d/pd/<init>(Ljava/util/List;I)V
areturn
L0:
iconst_0
istore 2
goto L1
L2:
new com/a/b/d/pb
dup
aload 0
iload 1
invokespecial com/a/b/d/pb/<init>(Ljava/util/List;I)V
areturn
.limit locals 3
.limit stack 4
.end method

.method private static a(Ljava/util/List;II)Ljava/util/List;
aload 0
instanceof java/util/RandomAccess
ifeq L0
new com/a/b/d/ow
dup
aload 0
invokespecial com/a/b/d/ow/<init>(Ljava/util/List;)V
astore 0
L1:
aload 0
iload 1
iload 2
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
areturn
L0:
new com/a/b/d/ox
dup
aload 0
invokespecial com/a/b/d/ox/<init>(Ljava/util/List;)V
astore 0
goto L1
.limit locals 3
.limit stack 3
.end method

.method public static a(Ljava/util/List;Lcom/a/b/b/bj;)Ljava/util/List;
aload 0
instanceof java/util/RandomAccess
ifeq L0
new com/a/b/d/pi
dup
aload 0
aload 1
invokespecial com/a/b/d/pi/<init>(Ljava/util/List;Lcom/a/b/b/bj;)V
areturn
L0:
new com/a/b/d/pk
dup
aload 0
aload 1
invokespecial com/a/b/d/pk/<init>(Ljava/util/List;Lcom/a/b/b/bj;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static transient a([Ljava/util/List;)Ljava/util/List;
aload 0
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ci/a(Ljava/util/List;)Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/List;ILjava/lang/Iterable;)Z
iconst_0
istore 3
aload 0
iload 1
invokeinterface java/util/List/listIterator(I)Ljava/util/ListIterator; 1
astore 0
aload 2
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/ListIterator/add(Ljava/lang/Object;)V 1
iconst_1
istore 3
goto L0
L1:
iload 3
ireturn
.limit locals 4
.limit stack 2
.end method

.method static a(Ljava/util/List;Ljava/lang/Object;)Z
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
if_acmpne L0
L1:
iconst_1
ireturn
L0:
aload 1
instanceof java/util/List
ifne L2
iconst_0
ireturn
L2:
aload 1
checkcast java/util/List
astore 1
aload 0
invokeinterface java/util/List/size()I 0
aload 1
invokeinterface java/util/List/size()I 0
if_icmpne L3
aload 0
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
aload 1
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Ljava/util/Iterator;)Z
ifne L1
L3:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method static b(Ljava/util/List;Ljava/lang/Object;)I
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokeinterface java/util/List/listIterator()Ljava/util/ListIterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/ListIterator/hasNext()Z 0
ifeq L1
aload 1
aload 0
invokeinterface java/util/ListIterator/next()Ljava/lang/Object; 0
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 0
invokeinterface java/util/ListIterator/previousIndex()I 0
ireturn
L1:
iconst_m1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static b(I)Ljava/util/ArrayList;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new java/util/ArrayList
dup
iload 0
invokestatic com/a/b/d/ov/c(I)I
invokespecial java/util/ArrayList/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b()Ljava/util/LinkedList;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/util/LinkedList;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/util/List;)Ljava/util/List;
aload 0
instanceof com/a/b/d/jl
ifeq L0
aload 0
checkcast com/a/b/d/jl
invokevirtual com/a/b/d/jl/e()Lcom/a/b/d/jl;
areturn
L0:
aload 0
instanceof com/a/b/d/pf
ifeq L1
aload 0
checkcast com/a/b/d/pf
getfield com/a/b/d/pf/a Ljava/util/List;
areturn
L1:
aload 0
instanceof java/util/RandomAccess
ifeq L2
new com/a/b/d/pe
dup
aload 0
invokespecial com/a/b/d/pe/<init>(Ljava/util/List;)V
areturn
L2:
new com/a/b/d/pf
dup
aload 0
invokespecial com/a/b/d/pf/<init>(Ljava/util/List;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b(Ljava/util/List;I)Ljava/util/ListIterator;
new com/a/b/d/oy
dup
aload 0
invokespecial com/a/b/d/oy/<init>(Ljava/util/List;)V
iload 1
invokevirtual com/a/b/d/oy/listIterator(I)Ljava/util/ListIterator;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static c(I)I
.annotation invisible Lcom/a/b/a/d;
.end annotation
iload 0
ldc "arraySize"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
ldc2_w 5L
iload 0
i2l
ladd
iload 0
bipush 10
idiv
i2l
ladd
invokestatic com/a/b/l/q/b(J)I
ireturn
.limit locals 1
.limit stack 4
.end method

.method private static c(Ljava/util/List;)I
iconst_1
istore 1
aload 0
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 3
aload 3
ifnonnull L2
iconst_0
istore 2
L3:
iload 2
iload 1
bipush 31
imul
iadd
iconst_m1
ixor
iconst_m1
ixor
istore 1
goto L0
L2:
aload 3
invokevirtual java/lang/Object/hashCode()I
istore 2
goto L3
L1:
iload 1
ireturn
.limit locals 4
.limit stack 3
.end method

.method static c(Ljava/util/List;Ljava/lang/Object;)I
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 0
invokeinterface java/util/List/size()I 0
invokeinterface java/util/List/listIterator(I)Ljava/util/ListIterator; 1
astore 0
L0:
aload 0
invokeinterface java/util/ListIterator/hasPrevious()Z 0
ifeq L1
aload 1
aload 0
invokeinterface java/util/ListIterator/previous()Ljava/lang/Object; 0
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 0
invokeinterface java/util/ListIterator/nextIndex()I 0
ireturn
L1:
iconst_m1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static c()Ljava/util/concurrent/CopyOnWriteArrayList;
.annotation invisible Lcom/a/b/a/c;
a s = "CopyOnWriteArrayList"
.end annotation
new java/util/concurrent/CopyOnWriteArrayList
dup
invokespecial java/util/concurrent/CopyOnWriteArrayList/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static c(Ljava/lang/Iterable;)Ljava/util/concurrent/CopyOnWriteArrayList;
.annotation invisible Lcom/a/b/a/c;
a s = "CopyOnWriteArrayList"
.end annotation
aload 0
instanceof java/util/Collection
ifeq L0
aload 0
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
astore 0
L1:
new java/util/concurrent/CopyOnWriteArrayList
dup
aload 0
invokespecial java/util/concurrent/CopyOnWriteArrayList/<init>(Ljava/util/Collection;)V
areturn
L0:
aload 0
invokestatic com/a/b/d/ov/a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
astore 0
goto L1
.limit locals 1
.limit stack 3
.end method

.method private static d(Ljava/lang/Iterable;)Ljava/util/List;
aload 0
checkcast java/util/List
areturn
.limit locals 1
.limit stack 1
.end method
