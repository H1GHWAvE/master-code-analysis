.bytecode 50.0
.class public synchronized com/a/b/d/jr
.super com/a/b/d/kk
.implements com/a/b/d/ou
.annotation invisible Lcom/a/b/a/b;
a Z = 1
b Z = 1
.end annotation

.field private static final 'd' J = 0L

.annotation invisible Lcom/a/b/a/c;
a s = "Not needed in emulated source"
.end annotation
.end field

.field private transient 'a' Lcom/a/b/d/jr;

.method <init>(Lcom/a/b/d/jt;I)V
aload 0
aload 1
iload 2
invokespecial com/a/b/d/kk/<init>(Lcom/a/b/d/jt;I)V
return
.limit locals 3
.limit stack 3
.end method

.method private static A()Lcom/a/b/d/jl;
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 0
.limit stack 2
.end method

.method private static B()Lcom/a/b/d/jl;
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 0
.limit stack 2
.end method

.method public static a()Lcom/a/b/d/jr;
getstatic com/a/b/d/ex/a Lcom/a/b/d/ex;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
astore 4
aload 4
aload 0
aload 1
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 4
aload 2
aload 3
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 4
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
areturn
.limit locals 5
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
astore 6
aload 6
aload 0
aload 1
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 6
aload 2
aload 3
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 6
aload 4
aload 5
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 6
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
areturn
.limit locals 7
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
astore 8
aload 8
aload 0
aload 1
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 8
aload 2
aload 3
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 8
aload 4
aload 5
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 8
aload 6
aload 7
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 8
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
areturn
.limit locals 9
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
astore 10
aload 10
aload 0
aload 1
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 10
aload 2
aload 3
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 10
aload 4
aload 5
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 10
aload 6
aload 7
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 10
aload 8
aload 9
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 10
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
areturn
.limit locals 11
.limit stack 3
.end method

.method private a(Ljava/io/ObjectInputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectInputStream"
.end annotation
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
istore 5
iload 5
ifge L3
new java/io/InvalidObjectException
dup
new java/lang/StringBuilder
dup
bipush 29
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Invalid key count "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 5
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/InvalidObjectException/<init>(Ljava/lang/String;)V
athrow
L3:
invokestatic com/a/b/d/jt/l()Lcom/a/b/d/ju;
astore 7
iconst_0
istore 2
iconst_0
istore 3
L4:
iload 2
iload 5
if_icmpge L0
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
astore 8
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
istore 6
iload 6
ifgt L5
new java/io/InvalidObjectException
dup
new java/lang/StringBuilder
dup
bipush 31
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Invalid value count "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 6
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/InvalidObjectException/<init>(Ljava/lang/String;)V
athrow
L5:
iload 6
anewarray java/lang/Object
astore 9
iconst_0
istore 4
L6:
iload 4
iload 6
if_icmpge L7
aload 9
iload 4
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
aastore
iload 4
iconst_1
iadd
istore 4
goto L6
L7:
aload 7
aload 8
aload 9
invokestatic com/a/b/d/jl/a([Ljava/lang/Object;)Lcom/a/b/d/jl;
invokevirtual com/a/b/d/ju/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;
pop
iload 3
iload 6
iadd
istore 3
iload 2
iconst_1
iadd
istore 2
goto L4
L0:
aload 7
invokevirtual com/a/b/d/ju/a()Lcom/a/b/d/jt;
astore 1
L1:
getstatic com/a/b/d/kq/a Lcom/a/b/d/aab;
aload 0
aload 1
invokevirtual com/a/b/d/aab/a(Ljava/lang/Object;Ljava/lang/Object;)V
getstatic com/a/b/d/kq/b Lcom/a/b/d/aab;
aload 0
iload 3
invokevirtual com/a/b/d/aab/a(Ljava/lang/Object;I)V
return
L2:
astore 1
new java/io/InvalidObjectException
dup
aload 1
invokevirtual java/lang/IllegalArgumentException/getMessage()Ljava/lang/String;
invokespecial java/io/InvalidObjectException/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/io/InvalidObjectException/initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
checkcast java/io/InvalidObjectException
athrow
.limit locals 10
.limit stack 5
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/vi;Ljava/io/ObjectOutputStream;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Lcom/a/b/d/vi;)Lcom/a/b/d/jr;
aload 0
invokeinterface com/a/b/d/vi/n()Z 0
ifeq L0
getstatic com/a/b/d/ex/a Lcom/a/b/d/ex;
astore 2
L1:
aload 2
areturn
L0:
aload 0
instanceof com/a/b/d/jr
ifeq L2
aload 0
checkcast com/a/b/d/jr
astore 3
aload 3
astore 2
aload 3
getfield com/a/b/d/kk/b Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/i_()Z
ifeq L1
L2:
invokestatic com/a/b/d/jt/l()Lcom/a/b/d/ju;
astore 2
aload 0
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 0
iconst_0
istore 1
L3:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/util/Collection
invokestatic com/a/b/d/jl/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
astore 4
aload 4
invokevirtual com/a/b/d/jl/isEmpty()Z
ifne L5
aload 2
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 4
invokevirtual com/a/b/d/ju/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;
pop
aload 4
invokevirtual com/a/b/d/jl/size()I
iload 1
iadd
istore 1
L6:
goto L3
L4:
new com/a/b/d/jr
dup
aload 2
invokevirtual com/a/b/d/ju/a()Lcom/a/b/d/jt;
iload 1
invokespecial com/a/b/d/jr/<init>(Lcom/a/b/d/jt;I)V
areturn
L5:
goto L6
.limit locals 5
.limit stack 4
.end method

.method public static c()Lcom/a/b/d/js;
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method public static d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
astore 2
aload 2
aload 0
aload 1
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
aload 2
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
areturn
.limit locals 3
.limit stack 3
.end method

.method private u()Lcom/a/b/d/jr;
aload 0
getfield com/a/b/d/jr/a Lcom/a/b/d/jr;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
astore 1
aload 0
invokevirtual com/a/b/d/jr/v()Lcom/a/b/d/iz;
invokevirtual com/a/b/d/iz/iterator()Ljava/util/Iterator;
astore 2
L1:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 1
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
goto L1
L2:
aload 1
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
astore 1
aload 1
aload 0
putfield com/a/b/d/jr/a Lcom/a/b/d/jr;
aload 0
aload 1
putfield com/a/b/d/jr/a Lcom/a/b/d/jr;
L0:
aload 1
areturn
.limit locals 4
.limit stack 3
.end method

.method private z()Lcom/a/b/d/jr;
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
astore 1
aload 0
invokevirtual com/a/b/d/jr/v()Lcom/a/b/d/iz;
invokevirtual com/a/b/d/iz/iterator()Ljava/util/Iterator;
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 1
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
goto L0
L1:
aload 1
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
astore 1
aload 1
aload 0
putfield com/a/b/d/jr/a Lcom/a/b/d/jr;
aload 1
areturn
.limit locals 4
.limit stack 3
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/util/List;
aload 0
aload 1
invokevirtual com/a/b/d/jr/e(Ljava/lang/Object;)Lcom/a/b/d/jl;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/util/List;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
invokevirtual com/a/b/d/jr/e(Ljava/lang/Object;)Lcom/a/b/d/jl;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic d()Lcom/a/b/d/kk;
aload 0
getfield com/a/b/d/jr/a Lcom/a/b/d/jr;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
new com/a/b/d/js
dup
invokespecial com/a/b/d/js/<init>()V
astore 1
aload 0
invokevirtual com/a/b/d/jr/v()Lcom/a/b/d/iz;
invokevirtual com/a/b/d/iz/iterator()Ljava/util/Iterator;
astore 2
L1:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 1
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
goto L1
L2:
aload 1
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
astore 1
aload 1
aload 0
putfield com/a/b/d/jr/a Lcom/a/b/d/jr;
aload 0
aload 1
putfield com/a/b/d/jr/a Lcom/a/b/d/jr;
L0:
aload 1
areturn
.limit locals 4
.limit stack 3
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final synthetic e()Lcom/a/b/d/iz;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public final e(Ljava/lang/Object;)Lcom/a/b/d/jl;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/jr/b Lcom/a/b/d/jt;
aload 1
invokevirtual com/a/b/d/jt/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/jl
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
invokestatic com/a/b/d/jl/d()Lcom/a/b/d/jl;
astore 1
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public final synthetic h(Ljava/lang/Object;)Lcom/a/b/d/iz;
aload 0
aload 1
invokevirtual com/a/b/d/jr/e(Ljava/lang/Object;)Lcom/a/b/d/jl;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic t()Lcom/a/b/d/iz;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method
