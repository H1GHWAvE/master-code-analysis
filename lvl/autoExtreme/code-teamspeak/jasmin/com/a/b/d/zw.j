.bytecode 50.0
.class final synchronized com/a/b/d/zw
.super com/a/b/d/yd
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation

.field static final 'a' Lcom/a/b/d/zw;

.field private static final 'b' J = 0L


.method static <clinit>()V
new com/a/b/d/zw
dup
invokespecial com/a/b/d/zw/<init>()V
putstatic com/a/b/d/zw/a Lcom/a/b/d/zw;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial com/a/b/d/yd/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
if_acmpne L0
iconst_0
ireturn
L0:
aload 1
aload 0
invokeinterface java/lang/Comparable/compareTo(Ljava/lang/Object;)I 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static transient a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;[Ljava/lang/Comparable;)Ljava/lang/Comparable;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 0
aload 1
aload 2
aload 3
invokevirtual com/a/b/d/xz/b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 4
.limit stack 5
.end method

.method private static b(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 0
aload 1
invokevirtual com/a/b/d/xz/b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 2
.limit stack 3
.end method

.method private static transient b(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;[Ljava/lang/Comparable;)Ljava/lang/Comparable;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 0
aload 1
aload 2
aload 3
invokevirtual com/a/b/d/xz/a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 4
.limit stack 5
.end method

.method private static c(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 0
aload 1
invokevirtual com/a/b/d/xz/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 2
.limit stack 3
.end method

.method private static c(Ljava/util/Iterator;)Ljava/lang/Comparable;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 0
invokevirtual com/a/b/d/xz/b(Ljava/util/Iterator;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 1
.limit stack 2
.end method

.method private static d(Ljava/util/Iterator;)Ljava/lang/Comparable;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 0
invokevirtual com/a/b/d/xz/a(Ljava/util/Iterator;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 1
.limit stack 2
.end method

.method private static e(Ljava/lang/Iterable;)Ljava/lang/Comparable;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 0
invokevirtual com/a/b/d/xz/d(Ljava/lang/Iterable;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 1
.limit stack 2
.end method

.method private static f(Ljava/lang/Iterable;)Ljava/lang/Comparable;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 0
invokevirtual com/a/b/d/xz/c(Ljava/lang/Iterable;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 1
.limit stack 2
.end method

.method private static f()Ljava/lang/Object;
getstatic com/a/b/d/zw/a Lcom/a/b/d/zw;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final a()Lcom/a/b/d/yd;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast java/lang/Comparable
astore 1
aload 2
checkcast java/lang/Comparable
astore 2
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 1
aload 2
invokevirtual com/a/b/d/xz/b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 3
.limit stack 3
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast java/lang/Comparable
astore 1
aload 2
checkcast java/lang/Comparable
astore 2
aload 3
checkcast java/lang/Comparable
astore 3
aload 4
checkcast [Ljava/lang/Comparable;
astore 4
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 1
aload 2
aload 3
aload 4
invokevirtual com/a/b/d/xz/b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 5
.limit stack 5
.end method

.method public final synthetic a(Ljava/util/Iterator;)Ljava/lang/Object;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 1
invokevirtual com/a/b/d/xz/b(Ljava/util/Iterator;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast java/lang/Comparable
astore 1
aload 2
checkcast java/lang/Comparable
astore 2
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 1
aload 2
invokevirtual com/a/b/d/xz/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 3
.limit stack 3
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast java/lang/Comparable
astore 1
aload 2
checkcast java/lang/Comparable
astore 2
aload 3
checkcast java/lang/Comparable
astore 3
aload 4
checkcast [Ljava/lang/Comparable;
astore 4
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 1
aload 2
aload 3
aload 4
invokevirtual com/a/b/d/xz/a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 5
.limit stack 5
.end method

.method public final synthetic b(Ljava/util/Iterator;)Ljava/lang/Object;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 1
invokevirtual com/a/b/d/xz/a(Ljava/util/Iterator;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic c(Ljava/lang/Iterable;)Ljava/lang/Object;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 1
invokevirtual com/a/b/d/xz/d(Ljava/lang/Iterable;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
aload 1
checkcast java/lang/Comparable
astore 1
aload 2
checkcast java/lang/Comparable
astore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
aload 2
if_acmpne L0
iconst_0
ireturn
L0:
aload 2
aload 1
invokeinterface java/lang/Comparable/compareTo(Ljava/lang/Object;)I 1
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final synthetic d(Ljava/lang/Iterable;)Ljava/lang/Object;
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
aload 1
invokevirtual com/a/b/d/xz/c(Ljava/lang/Iterable;)Ljava/lang/Object;
checkcast java/lang/Comparable
areturn
.limit locals 2
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
ldc "Ordering.natural().reverse()"
areturn
.limit locals 1
.limit stack 1
.end method
