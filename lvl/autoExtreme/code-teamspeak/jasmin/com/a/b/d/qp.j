.bytecode 50.0
.class synchronized com/a/b/d/qp
.super java/util/AbstractMap
.implements java/io/Serializable
.implements java/util/concurrent/ConcurrentMap

.field private static final 'a' J = 0L


.field private final 'b' Lcom/a/b/d/qw;

.field private final 'c' Lcom/a/b/d/qq;

.method <init>(Lcom/a/b/d/ql;)V
aload 0
invokespecial java/util/AbstractMap/<init>()V
aload 0
aload 1
invokevirtual com/a/b/d/ql/d()Lcom/a/b/d/qw;
putfield com/a/b/d/qp/b Lcom/a/b/d/qw;
aload 0
aload 1
getfield com/a/b/d/ql/k Lcom/a/b/d/qq;
putfield com/a/b/d/qp/c Lcom/a/b/d/qq;
return
.limit locals 2
.limit stack 2
.end method

.method final a(Ljava/lang/Object;Ljava/lang/Object;)V
new com/a/b/d/qx
dup
aload 1
aload 2
aload 0
getfield com/a/b/d/qp/c Lcom/a/b/d/qq;
invokespecial com/a/b/d/qx/<init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
pop
return
.limit locals 3
.limit stack 5
.end method

.method public containsKey(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public containsValue(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public entrySet()Ljava/util/Set;
invokestatic java/util/Collections/emptySet()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/qp/a(Ljava/lang/Object;Ljava/lang/Object;)V
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/qp/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 3
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aconst_null
areturn
.limit locals 3
.limit stack 1
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iconst_0
ireturn
.limit locals 4
.limit stack 1
.end method
