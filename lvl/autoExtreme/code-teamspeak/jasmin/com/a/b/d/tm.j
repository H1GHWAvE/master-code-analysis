.bytecode 50.0
.class synchronized com/a/b/d/tm
.super com/a/b/d/uj

.field final 'a' Lcom/a/b/b/bj;

.field private final 'b' Ljava/util/Set;

.method <init>(Ljava/util/Set;Lcom/a/b/b/bj;)V
aload 0
invokespecial com/a/b/d/uj/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Set
putfield com/a/b/d/tm/b Ljava/util/Set;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/bj
putfield com/a/b/d/tm/a Lcom/a/b/b/bj;
return
.limit locals 3
.limit stack 2
.end method

.method protected final a()Ljava/util/Set;
new com/a/b/d/tn
dup
aload 0
invokespecial com/a/b/d/tn/<init>(Lcom/a/b/d/tm;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method c()Ljava/util/Set;
aload 0
getfield com/a/b/d/tm/b Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method final c_()Ljava/util/Collection;
aload 0
getfield com/a/b/d/tm/b Ljava/util/Set;
aload 0
getfield com/a/b/d/tm/a Lcom/a/b/b/bj;
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Lcom/a/b/b/bj;)Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 2
.end method

.method public clear()V
aload 0
invokevirtual com/a/b/d/tm/c()Ljava/util/Set;
invokeinterface java/util/Set/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public containsKey(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/tm/c()Ljava/util/Set;
aload 1
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final e()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/tm/c()Ljava/util/Set;
invokestatic com/a/b/d/sz/b(Ljava/util/Set;)Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/tm/c()Ljava/util/Set;
aload 1
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/a/b/d/tm/a Lcom/a/b/b/bj;
aload 1
invokeinterface com/a/b/b/bj/e(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/tm/c()Ljava/util/Set;
aload 1
invokeinterface java/util/Set/remove(Ljava/lang/Object;)Z 1
ifeq L0
aload 0
getfield com/a/b/d/tm/a Lcom/a/b/b/bj;
aload 1
invokeinterface com/a/b/b/bj/e(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public size()I
aload 0
invokevirtual com/a/b/d/tm/c()Ljava/util/Set;
invokeinterface java/util/Set/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method
