.bytecode 50.0
.class synchronized abstract com/a/b/d/i
.super com/a/b/d/agj
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private final 'a' I

.field private 'b' I

.method protected <init>(I)V
aload 0
iload 1
iconst_0
invokespecial com/a/b/d/i/<init>(II)V
return
.limit locals 2
.limit stack 3
.end method

.method protected <init>(II)V
aload 0
invokespecial com/a/b/d/agj/<init>()V
iload 2
iload 1
invokestatic com/a/b/b/cn/b(II)I
pop
aload 0
iload 1
putfield com/a/b/d/i/a I
aload 0
iload 2
putfield com/a/b/d/i/b I
return
.limit locals 3
.limit stack 2
.end method

.method protected abstract a(I)Ljava/lang/Object;
.end method

.method public final hasNext()Z
aload 0
getfield com/a/b/d/i/b I
aload 0
getfield com/a/b/d/i/a I
if_icmpge L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final hasPrevious()Z
aload 0
getfield com/a/b/d/i/b I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final next()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/i/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getfield com/a/b/d/i/b I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield com/a/b/d/i/b I
aload 0
iload 1
invokevirtual com/a/b/d/i/a(I)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final nextIndex()I
aload 0
getfield com/a/b/d/i/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final previous()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/i/hasPrevious()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getfield com/a/b/d/i/b I
iconst_1
isub
istore 1
aload 0
iload 1
putfield com/a/b/d/i/b I
aload 0
iload 1
invokevirtual com/a/b/d/i/a(I)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final previousIndex()I
aload 0
getfield com/a/b/d/i/b I
iconst_1
isub
ireturn
.limit locals 1
.limit stack 2
.end method
