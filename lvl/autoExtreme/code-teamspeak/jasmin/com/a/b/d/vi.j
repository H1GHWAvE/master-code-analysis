.bytecode 50.0
.class public abstract interface com/a/b/d/vi
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method public abstract a(Lcom/a/b/d/vi;)Z
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract b()Ljava/util/Map;
.end method

.method public abstract b(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract c(Ljava/lang/Object;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract c(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract d(Ljava/lang/Object;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract f()I
.end method

.method public abstract f(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract g()V
.end method

.method public abstract g(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract hashCode()I
.end method

.method public abstract i()Ljava/util/Collection;
.end method

.method public abstract k()Ljava/util/Collection;
.end method

.method public abstract n()Z
.end method

.method public abstract p()Ljava/util/Set;
.end method

.method public abstract q()Lcom/a/b/d/xc;
.end method
