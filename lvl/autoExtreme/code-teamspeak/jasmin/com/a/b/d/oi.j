.bytecode 50.0
.class public final synchronized com/a/b/d/oi
.super com/a/b/d/ai
.annotation invisible Lcom/a/b/a/b;
a Z = 1
b Z = 1
.end annotation

.field private static final 'b' J = 0L

.annotation invisible Lcom/a/b/a/c;
a s = "not needed in emulated source"
.end annotation
.end field

.method <init>()V
aload 0
new java/util/LinkedHashMap
dup
invokespecial java/util/LinkedHashMap/<init>()V
invokespecial com/a/b/d/ai/<init>(Ljava/util/Map;)V
return
.limit locals 1
.limit stack 3
.end method

.method private <init>(I)V
aload 0
new java/util/LinkedHashMap
dup
iload 1
invokestatic com/a/b/d/sz/b(I)I
invokespecial java/util/LinkedHashMap/<init>(I)V
invokespecial com/a/b/d/ai/<init>(Ljava/util/Map;)V
return
.limit locals 2
.limit stack 4
.end method

.method public static a(I)Lcom/a/b/d/oi;
new com/a/b/d/oi
dup
iload 0
invokespecial com/a/b/d/oi/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/oi;
aload 0
invokestatic com/a/b/d/xe/a(Ljava/lang/Iterable;)I
invokestatic com/a/b/d/oi/a(I)Lcom/a/b/d/oi;
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/io/ObjectInputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectInputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
istore 2
aload 0
new java/util/LinkedHashMap
dup
iload 2
invokestatic com/a/b/d/sz/b(I)I
invokespecial java/util/LinkedHashMap/<init>(I)V
putfield com/a/b/d/ai/a Ljava/util/Map;
aload 0
aload 1
iload 2
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/xc;Ljava/io/ObjectInputStream;I)V
return
.limit locals 3
.limit stack 4
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/xc;Ljava/io/ObjectOutputStream;)V
return
.limit locals 2
.limit stack 2
.end method

.method public static g()Lcom/a/b/d/oi;
new com/a/b/d/oi
dup
invokespecial com/a/b/d/oi/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method public final volatile synthetic a(Ljava/lang/Object;)I
aload 0
aload 1
invokespecial com/a/b/d/ai/a(Ljava/lang/Object;)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a(Ljava/lang/Object;I)I
aload 0
aload 1
iload 2
invokespecial com/a/b/d/ai/a(Ljava/lang/Object;I)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic a()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/ai/a()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic a(Ljava/lang/Object;II)Z
aload 0
aload 1
iload 2
iload 3
invokespecial com/a/b/d/ai/a(Ljava/lang/Object;II)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final volatile synthetic add(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/add(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic addAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/addAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic b(Ljava/lang/Object;I)I
aload 0
aload 1
iload 2
invokespecial com/a/b/d/ai/b(Ljava/lang/Object;I)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic c(Ljava/lang/Object;I)I
aload 0
aload 1
iload 2
invokespecial com/a/b/d/ai/c(Ljava/lang/Object;I)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic clear()V
aload 0
invokespecial com/a/b/d/ai/clear()V
return
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic contains(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/contains(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic hashCode()I
aload 0
invokespecial com/a/b/d/ai/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic isEmpty()Z
aload 0
invokespecial com/a/b/d/ai/isEmpty()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic iterator()Ljava/util/Iterator;
aload 0
invokespecial com/a/b/d/ai/iterator()Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic n_()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/ai/n_()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic remove(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/remove(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic removeAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/removeAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic retainAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/retainAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic size()I
aload 0
invokespecial com/a/b/d/ai/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic toString()Ljava/lang/String;
aload 0
invokespecial com/a/b/d/ai/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
