.bytecode 50.0
.class synchronized com/a/b/d/acm
.super com/a/b/d/uj

.field final 'a' Ljava/lang/Object;

.field 'b' Ljava/util/Map;

.field final synthetic 'c' Lcom/a/b/d/abx;

.method <init>(Lcom/a/b/d/abx;Ljava/lang/Object;)V
aload 0
aload 1
putfield com/a/b/d/acm/c Lcom/a/b/d/abx;
aload 0
invokespecial com/a/b/d/uj/<init>()V
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
putfield com/a/b/d/acm/a Ljava/lang/Object;
return
.limit locals 3
.limit stack 2
.end method

.method protected final a()Ljava/util/Set;
new com/a/b/d/acn
dup
aload 0
iconst_0
invokespecial com/a/b/d/acn/<init>(Lcom/a/b/d/acm;B)V
areturn
.limit locals 1
.limit stack 4
.end method

.method c()Ljava/util/Map;
aload 0
getfield com/a/b/d/acm/b Ljava/util/Map;
ifnull L0
aload 0
getfield com/a/b/d/acm/b Ljava/util/Map;
invokeinterface java/util/Map/isEmpty()Z 0
ifeq L1
aload 0
getfield com/a/b/d/acm/c Lcom/a/b/d/abx;
getfield com/a/b/d/abx/a Ljava/util/Map;
aload 0
getfield com/a/b/d/acm/a Ljava/lang/Object;
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L1
L0:
aload 0
invokevirtual com/a/b/d/acm/d()Ljava/util/Map;
astore 1
aload 0
aload 1
putfield com/a/b/d/acm/b Ljava/util/Map;
aload 1
areturn
L1:
aload 0
getfield com/a/b/d/acm/b Ljava/util/Map;
areturn
.limit locals 2
.limit stack 2
.end method

.method public clear()V
aload 0
invokevirtual com/a/b/d/acm/c()Ljava/util/Map;
astore 1
aload 1
ifnull L0
aload 1
invokeinterface java/util/Map/clear()V 0
L0:
aload 0
invokevirtual com/a/b/d/acm/f()V
return
.limit locals 2
.limit stack 1
.end method

.method public containsKey(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/acm/c()Ljava/util/Map;
astore 2
aload 1
ifnull L0
aload 2
ifnull L0
aload 2
aload 1
invokestatic com/a/b/d/sz/b(Ljava/util/Map;Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method d()Ljava/util/Map;
aload 0
getfield com/a/b/d/acm/c Lcom/a/b/d/abx;
getfield com/a/b/d/abx/a Ljava/util/Map;
aload 0
getfield com/a/b/d/acm/a Ljava/lang/Object;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Map
areturn
.limit locals 1
.limit stack 2
.end method

.method f()V
aload 0
invokevirtual com/a/b/d/acm/c()Ljava/util/Map;
ifnull L0
aload 0
getfield com/a/b/d/acm/b Ljava/util/Map;
invokeinterface java/util/Map/isEmpty()Z 0
ifeq L0
aload 0
getfield com/a/b/d/acm/c Lcom/a/b/d/abx;
getfield com/a/b/d/abx/a Ljava/util/Map;
aload 0
getfield com/a/b/d/acm/a Ljava/lang/Object;
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
aload 0
aconst_null
putfield com/a/b/d/acm/b Ljava/util/Map;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/acm/c()Ljava/util/Map;
astore 2
aload 1
ifnull L0
aload 2
ifnull L0
aload 2
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 2
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/d/acm/b Ljava/util/Map;
ifnull L0
aload 0
getfield com/a/b/d/acm/b Ljava/util/Map;
invokeinterface java/util/Map/isEmpty()Z 0
ifne L0
aload 0
getfield com/a/b/d/acm/b Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
areturn
L0:
aload 0
getfield com/a/b/d/acm/c Lcom/a/b/d/abx;
aload 0
getfield com/a/b/d/acm/a Ljava/lang/Object;
aload 1
aload 2
invokevirtual com/a/b/d/abx/a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 4
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/acm/c()Ljava/util/Map;
astore 2
aload 2
ifnonnull L0
aconst_null
areturn
L0:
aload 2
aload 1
invokestatic com/a/b/d/sz/c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
astore 1
aload 0
invokevirtual com/a/b/d/acm/f()V
aload 1
areturn
.limit locals 3
.limit stack 2
.end method
