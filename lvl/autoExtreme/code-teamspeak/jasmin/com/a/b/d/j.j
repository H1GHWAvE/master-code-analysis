.bytecode 50.0
.class public synchronized abstract com/a/b/d/j
.super com/a/b/d/agi
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private 'a' I

.field private 'b' Ljava/lang/Object;

.method public <init>()V
aload 0
invokespecial com/a/b/d/agi/<init>()V
aload 0
getstatic com/a/b/d/l/b I
putfield com/a/b/d/j/a I
return
.limit locals 1
.limit stack 2
.end method

.method private c()Z
aload 0
getstatic com/a/b/d/l/d I
putfield com/a/b/d/j/a I
aload 0
aload 0
invokevirtual com/a/b/d/j/a()Ljava/lang/Object;
putfield com/a/b/d/j/b Ljava/lang/Object;
aload 0
getfield com/a/b/d/j/a I
getstatic com/a/b/d/l/c I
if_icmpeq L0
aload 0
getstatic com/a/b/d/l/a I
putfield com/a/b/d/j/a I
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private d()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/j/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getfield com/a/b/d/j/b Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public abstract a()Ljava/lang/Object;
.end method

.method public final b()Ljava/lang/Object;
aload 0
getstatic com/a/b/d/l/c I
putfield com/a/b/d/j/a I
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method public final hasNext()Z
iconst_0
istore 2
aload 0
getfield com/a/b/d/j/a I
getstatic com/a/b/d/l/d I
if_icmpeq L0
iconst_1
istore 1
L1:
iload 1
invokestatic com/a/b/b/cn/b(Z)V
iload 2
istore 1
getstatic com/a/b/d/k/a [I
aload 0
getfield com/a/b/d/j/a I
iconst_1
isub
iaload
tableswitch 1
L2
L3
default : L4
L4:
aload 0
getstatic com/a/b/d/l/d I
putfield com/a/b/d/j/a I
aload 0
aload 0
invokevirtual com/a/b/d/j/a()Ljava/lang/Object;
putfield com/a/b/d/j/b Ljava/lang/Object;
iload 2
istore 1
aload 0
getfield com/a/b/d/j/a I
getstatic com/a/b/d/l/c I
if_icmpeq L2
aload 0
getstatic com/a/b/d/l/a I
putfield com/a/b/d/j/a I
iconst_1
istore 1
L2:
iload 1
ireturn
L0:
iconst_0
istore 1
goto L1
L3:
iconst_1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final next()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/j/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getstatic com/a/b/d/l/b I
putfield com/a/b/d/j/a I
aload 0
getfield com/a/b/d/j/b Ljava/lang/Object;
astore 1
aload 0
aconst_null
putfield com/a/b/d/j/b Ljava/lang/Object;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method
