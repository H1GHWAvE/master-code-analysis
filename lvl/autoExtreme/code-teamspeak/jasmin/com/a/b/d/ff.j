.bytecode 50.0
.class public final synchronized com/a/b/d/ff
.super com/a/b/d/ai
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field private static final 'c' J = 0L

.annotation invisible Lcom/a/b/a/c;
a s = "Not needed in emulated source"
.end annotation
.end field

.field private transient 'b' Ljava/lang/Class;

.method private <init>(Ljava/lang/Class;)V
aload 0
new java/util/EnumMap
dup
aload 1
invokespecial java/util/EnumMap/<init>(Ljava/lang/Class;)V
invokestatic com/a/b/d/agm/a(Ljava/util/Map;)Lcom/a/b/d/agm;
invokespecial com/a/b/d/ai/<init>(Ljava/util/Map;)V
aload 0
aload 1
putfield com/a/b/d/ff/b Ljava/lang/Class;
return
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/lang/Class;)Lcom/a/b/d/ff;
new com/a/b/d/ff
dup
aload 0
invokespecial com/a/b/d/ff/<init>(Ljava/lang/Class;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/ff;
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ldc "EnumMultiset constructor passed empty Iterable"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
new com/a/b/d/ff
dup
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Enum
invokevirtual java/lang/Enum/getDeclaringClass()Ljava/lang/Class;
invokespecial com/a/b/d/ff/<init>(Ljava/lang/Class;)V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Class;)Lcom/a/b/d/ff;
new com/a/b/d/ff
dup
aload 1
invokespecial com/a/b/d/ff/<init>(Ljava/lang/Class;)V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/io/ObjectInputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectInputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 0
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
checkcast java/lang/Class
putfield com/a/b/d/ff/b Ljava/lang/Class;
aload 0
new java/util/EnumMap
dup
aload 0
getfield com/a/b/d/ff/b Ljava/lang/Class;
invokespecial java/util/EnumMap/<init>(Ljava/lang/Class;)V
invokestatic com/a/b/d/agm/a(Ljava/util/Map;)Lcom/a/b/d/agm;
putfield com/a/b/d/ai/a Ljava/util/Map;
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/xc;Ljava/io/ObjectInputStream;)V
return
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 1
aload 0
getfield com/a/b/d/ff/b Ljava/lang/Class;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/xc;Ljava/io/ObjectOutputStream;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a(Ljava/lang/Object;)I
aload 0
aload 1
invokespecial com/a/b/d/ai/a(Ljava/lang/Object;)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/ai/a()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic addAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/addAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic b(Ljava/lang/Object;I)I
aload 0
aload 1
iload 2
invokespecial com/a/b/d/ai/b(Ljava/lang/Object;I)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic clear()V
aload 0
invokespecial com/a/b/d/ai/clear()V
return
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic contains(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/contains(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic hashCode()I
aload 0
invokespecial com/a/b/d/ai/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic isEmpty()Z
aload 0
invokespecial com/a/b/d/ai/isEmpty()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic iterator()Ljava/util/Iterator;
aload 0
invokespecial com/a/b/d/ai/iterator()Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic n_()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/ai/n_()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic remove(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/remove(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic removeAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/removeAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic retainAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokespecial com/a/b/d/ai/retainAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic size()I
aload 0
invokespecial com/a/b/d/ai/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic toString()Ljava/lang/String;
aload 0
invokespecial com/a/b/d/ai/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
