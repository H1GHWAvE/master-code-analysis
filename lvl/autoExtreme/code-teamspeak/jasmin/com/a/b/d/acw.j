.bytecode 50.0
.class final synchronized com/a/b/d/acw
.super com/a/b/d/adi

.field private static final 'f' J = 0L


.field transient 'a' Ljava/util/Set;

.field transient 'b' Ljava/util/Collection;

.method <init>(Ljava/util/Map;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokespecial com/a/b/d/adi/<init>(Ljava/util/Map;Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/lang/Object;)Ljava/util/Collection;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aload 0
getfield com/a/b/d/acw/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
aload 1
invokespecial com/a/b/d/adi/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
astore 1
L1:
aload 1
ifnonnull L5
aconst_null
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
areturn
L5:
aload 1
aload 0
getfield com/a/b/d/acw/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/b(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
astore 1
L6:
goto L3
L2:
astore 1
L7:
aload 2
monitorexit
L8:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public final containsValue(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/acw/values()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/contains(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final entrySet()Ljava/util/Set;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/d/acw/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/acw/a Ljava/util/Set;
ifnonnull L1
aload 0
new com/a/b/d/acx
dup
aload 0
invokevirtual com/a/b/d/acw/a()Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
aload 0
getfield com/a/b/d/acw/h Ljava/lang/Object;
invokespecial com/a/b/d/acx/<init>(Ljava/util/Set;Ljava/lang/Object;)V
putfield com/a/b/d/acw/a Ljava/util/Set;
L1:
aload 0
getfield com/a/b/d/acw/a Ljava/util/Set;
astore 2
aload 1
monitorexit
L3:
aload 2
areturn
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 5
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokespecial com/a/b/d/acw/a(Ljava/lang/Object;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final values()Ljava/util/Collection;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/d/acw/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/acw/b Ljava/util/Collection;
ifnonnull L1
aload 0
new com/a/b/d/ada
dup
aload 0
invokevirtual com/a/b/d/acw/a()Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
aload 0
getfield com/a/b/d/acw/h Ljava/lang/Object;
invokespecial com/a/b/d/ada/<init>(Ljava/util/Collection;Ljava/lang/Object;)V
putfield com/a/b/d/acw/b Ljava/util/Collection;
L1:
aload 0
getfield com/a/b/d/acw/b Ljava/util/Collection;
astore 2
aload 1
monitorexit
L3:
aload 2
areturn
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 5
.end method
