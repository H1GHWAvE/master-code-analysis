.bytecode 50.0
.class public final synchronized com/a/b/d/fg
.super com/a/b/d/hh
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/c;
a s = "java.util.ArrayDeque"
.end annotation

.field private static final 'c' J = 0L


.field final 'a' I
.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.field private final 'b' Ljava/util/Queue;

.method private <init>(I)V
aload 0
invokespecial com/a/b/d/hh/<init>()V
iload 1
iflt L0
iconst_1
istore 2
L1:
iload 2
ldc "maxSize (%s) must >= 0"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
new java/util/ArrayDeque
dup
iload 1
invokespecial java/util/ArrayDeque/<init>(I)V
putfield com/a/b/d/fg/b Ljava/util/Queue;
aload 0
iload 1
putfield com/a/b/d/fg/a I
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 6
.end method

.method private static a(I)Lcom/a/b/d/fg;
new com/a/b/d/fg
dup
iload 0
invokespecial com/a/b/d/fg/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private c()I
aload 0
getfield com/a/b/d/fg/a I
aload 0
invokevirtual com/a/b/d/fg/size()I
isub
ireturn
.limit locals 1
.limit stack 2
.end method

.method protected final a()Ljava/util/Queue;
aload 0
getfield com/a/b/d/fg/b Ljava/util/Queue;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final add(Ljava/lang/Object;)Z
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/d/fg/a I
ifne L0
iconst_1
ireturn
L0:
aload 0
invokevirtual com/a/b/d/fg/size()I
aload 0
getfield com/a/b/d/fg/a I
if_icmpne L1
aload 0
getfield com/a/b/d/fg/b Ljava/util/Queue;
invokeinterface java/util/Queue/remove()Ljava/lang/Object; 0
pop
L1:
aload 0
getfield com/a/b/d/fg/b Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final addAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokevirtual com/a/b/d/fg/a(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected final volatile synthetic b()Ljava/util/Collection;
aload 0
getfield com/a/b/d/fg/b Ljava/util/Queue;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final contains(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/fg/b Ljava/util/Queue;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
invokeinterface java/util/Queue/contains(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected final volatile synthetic k_()Ljava/lang/Object;
aload 0
getfield com/a/b/d/fg/b Ljava/util/Queue;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final offer(Ljava/lang/Object;)Z
aload 0
aload 1
invokevirtual com/a/b/d/fg/add(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final remove(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/fg/b Ljava/util/Queue;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
invokeinterface java/util/Queue/remove(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method
