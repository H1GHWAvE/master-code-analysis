.bytecode 50.0
.class synchronized com/a/b/d/co
.super java/util/AbstractCollection

.field final 'a' Ljava/util/Collection;

.field final 'b' Lcom/a/b/b/co;

.method <init>(Ljava/util/Collection;Lcom/a/b/b/co;)V
aload 0
invokespecial java/util/AbstractCollection/<init>()V
aload 0
aload 1
putfield com/a/b/d/co/a Ljava/util/Collection;
aload 0
aload 2
putfield com/a/b/d/co/b Lcom/a/b/b/co;
return
.limit locals 3
.limit stack 2
.end method

.method private a(Lcom/a/b/b/co;)Lcom/a/b/d/co;
new com/a/b/d/co
dup
aload 0
getfield com/a/b/d/co/a Ljava/util/Collection;
aload 0
getfield com/a/b/d/co/b Lcom/a/b/b/co;
aload 1
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokespecial com/a/b/d/co/<init>(Ljava/util/Collection;Lcom/a/b/b/co;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public add(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/co/b Lcom/a/b/b/co;
aload 1
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
invokestatic com/a/b/b/cn/a(Z)V
aload 0
getfield com/a/b/d/co/a Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public addAll(Ljava/util/Collection;)Z
aload 1
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 3
aload 0
getfield com/a/b/d/co/b Lcom/a/b/b/co;
aload 3
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
invokestatic com/a/b/b/cn/a(Z)V
goto L0
L1:
aload 0
getfield com/a/b/d/co/a Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/addAll(Ljava/util/Collection;)Z 1
ireturn
.limit locals 4
.limit stack 2
.end method

.method public clear()V
aload 0
getfield com/a/b/d/co/a Ljava/util/Collection;
aload 0
getfield com/a/b/d/co/b Lcom/a/b/b/co;
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
pop
return
.limit locals 1
.limit stack 2
.end method

.method public contains(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/co/a Ljava/util/Collection;
aload 1
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/a/b/d/co/b Lcom/a/b/b/co;
aload 1
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public containsAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public isEmpty()Z
aload 0
getfield com/a/b/d/co/a Ljava/util/Collection;
aload 0
getfield com/a/b/d/co/b Lcom/a/b/b/co;
invokestatic com/a/b/d/mq/d(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public iterator()Ljava/util/Iterator;
aload 0
getfield com/a/b/d/co/a Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
aload 0
getfield com/a/b/d/co/b Lcom/a/b/b/co;
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 2
.end method

.method public remove(Ljava/lang/Object;)Z
aload 0
aload 1
invokevirtual com/a/b/d/co/contains(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/a/b/d/co/a Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/remove(Ljava/lang/Object;)Z 1
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public removeAll(Ljava/util/Collection;)Z
aload 0
getfield com/a/b/d/co/a Ljava/util/Collection;
aload 0
getfield com/a/b/d/co/b Lcom/a/b/b/co;
aload 1
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 3
.end method

.method public retainAll(Ljava/util/Collection;)Z
aload 0
getfield com/a/b/d/co/a Ljava/util/Collection;
aload 0
getfield com/a/b/d/co/b Lcom/a/b/b/co;
aload 1
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 3
.end method

.method public size()I
aload 0
invokevirtual com/a/b/d/co/iterator()Ljava/util/Iterator;
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toArray()[Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/co/iterator()Ljava/util/Iterator;
invokestatic com/a/b/d/ov/a(Ljava/util/Iterator;)Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/toArray()[Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/co/iterator()Ljava/util/Iterator;
invokestatic com/a/b/d/ov/a(Ljava/util/Iterator;)Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method
