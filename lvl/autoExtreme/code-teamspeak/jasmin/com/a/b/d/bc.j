.bytecode 50.0
.class synchronized abstract com/a/b/d/bc
.super com/a/b/d/as
.implements com/a/b/d/abn
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field final 'a' Ljava/util/Comparator;
.annotation visible Lcom/a/b/d/hv;
.end annotation
.end field

.field private transient 'b' Lcom/a/b/d/abn;

.method <init>()V
aload 0
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
invokespecial com/a/b/d/bc/<init>(Ljava/util/Comparator;)V
return
.limit locals 1
.limit stack 2
.end method

.method <init>(Ljava/util/Comparator;)V
aload 0
invokespecial com/a/b/d/as/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
putfield com/a/b/d/bc/a Ljava/util/Comparator;
return
.limit locals 2
.limit stack 2
.end method

.method private o()Ljava/util/NavigableSet;
new com/a/b/d/abr
dup
aload 0
invokespecial com/a/b/d/abr/<init>(Lcom/a/b/d/abn;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private p()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/bc/m()Lcom/a/b/d/abn;
invokestatic com/a/b/d/xe/b(Lcom/a/b/d/xc;)Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method private q()Lcom/a/b/d/abn;
new com/a/b/d/bd
dup
aload 0
invokespecial com/a/b/d/bd/<init>(Lcom/a/b/d/bc;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/bc/c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
aload 3
aload 4
invokeinterface com/a/b/d/abn/d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn; 2
areturn
.limit locals 5
.limit stack 3
.end method

.method public comparator()Ljava/util/Comparator;
aload 0
getfield com/a/b/d/bc/a Ljava/util/Comparator;
areturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic e()Ljava/util/Set;
new com/a/b/d/abr
dup
aload 0
invokespecial com/a/b/d/abr/<init>(Lcom/a/b/d/abn;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public e_()Ljava/util/NavigableSet;
aload 0
invokespecial com/a/b/d/as/n_()Ljava/util/Set;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method public h()Lcom/a/b/d/xd;
aload 0
invokevirtual com/a/b/d/bc/b()Ljava/util/Iterator;
astore 1
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/xd
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public i()Lcom/a/b/d/xd;
aload 0
invokevirtual com/a/b/d/bc/l()Ljava/util/Iterator;
astore 1
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/xd
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public j()Lcom/a/b/d/xd;
aload 0
invokevirtual com/a/b/d/bc/b()Ljava/util/Iterator;
astore 1
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/xd
astore 2
aload 2
invokeinterface com/a/b/d/xd/a()Ljava/lang/Object; 0
aload 2
invokeinterface com/a/b/d/xd/b()I 0
invokestatic com/a/b/d/xe/a(Ljava/lang/Object;I)Lcom/a/b/d/xd;
astore 2
aload 1
invokeinterface java/util/Iterator/remove()V 0
aload 2
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 2
.end method

.method public k()Lcom/a/b/d/xd;
aload 0
invokevirtual com/a/b/d/bc/l()Ljava/util/Iterator;
astore 1
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/xd
astore 2
aload 2
invokeinterface com/a/b/d/xd/a()Ljava/lang/Object; 0
aload 2
invokeinterface com/a/b/d/xd/b()I 0
invokestatic com/a/b/d/xe/a(Ljava/lang/Object;I)Lcom/a/b/d/xd;
astore 2
aload 1
invokeinterface java/util/Iterator/remove()V 0
aload 2
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 2
.end method

.method abstract l()Ljava/util/Iterator;
.end method

.method public m()Lcom/a/b/d/abn;
aload 0
getfield com/a/b/d/bc/b Lcom/a/b/d/abn;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
new com/a/b/d/bd
dup
aload 0
invokespecial com/a/b/d/bd/<init>(Lcom/a/b/d/bc;)V
astore 1
aload 0
aload 1
putfield com/a/b/d/bc/b Lcom/a/b/d/abn;
L0:
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method public final synthetic n()Ljava/util/SortedSet;
aload 0
invokevirtual com/a/b/d/bc/e_()Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic n_()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/bc/e_()Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method
