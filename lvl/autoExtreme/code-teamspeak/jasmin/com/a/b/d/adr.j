.bytecode 50.0
.class synchronized com/a/b/d/adr
.super com/a/b/d/adj
.implements com/a/b/d/aac

.field private static final 'i' J = 0L


.field transient 'f' Ljava/util/Set;

.method <init>(Lcom/a/b/d/aac;)V
aload 0
aload 1
invokespecial com/a/b/d/adj/<init>(Lcom/a/b/d/vi;)V
return
.limit locals 2
.limit stack 2
.end method

.method synthetic a()Lcom/a/b/d/vi;
aload 0
invokevirtual com/a/b/d/adr/c()Lcom/a/b/d/aac;
areturn
.limit locals 1
.limit stack 1
.end method

.method public a(Ljava/lang/Object;)Ljava/util/Set;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adr/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adr/c()Lcom/a/b/d/aac;
aload 1
invokeinterface com/a/b/d/aac/a(Ljava/lang/Object;)Ljava/util/Set; 1
aload 0
getfield com/a/b/d/adr/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adr/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adr/c()Lcom/a/b/d/aac;
aload 1
aload 2
invokeinterface com/a/b/d/aac/a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set; 2
astore 1
aload 3
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method public synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/adr/a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
areturn
.limit locals 3
.limit stack 3
.end method

.method public b(Ljava/lang/Object;)Ljava/util/Set;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adr/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adr/c()Lcom/a/b/d/aac;
aload 1
invokeinterface com/a/b/d/aac/b(Ljava/lang/Object;)Ljava/util/Set; 1
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method c()Lcom/a/b/d/aac;
aload 0
invokespecial com/a/b/d/adj/a()Lcom/a/b/d/vi;
checkcast com/a/b/d/aac
areturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
invokevirtual com/a/b/d/adr/a(Ljava/lang/Object;)Ljava/util/Set;
areturn
.limit locals 2
.limit stack 2
.end method

.method synthetic d()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/adr/c()Lcom/a/b/d/aac;
areturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
invokevirtual com/a/b/d/adr/b(Ljava/lang/Object;)Ljava/util/Set;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic k()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/adr/u()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final u()Ljava/util/Set;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/d/adr/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/adr/f Ljava/util/Set;
ifnonnull L1
aload 0
aload 0
invokevirtual com/a/b/d/adr/c()Lcom/a/b/d/aac;
invokeinterface com/a/b/d/aac/u()Ljava/util/Set; 0
aload 0
getfield com/a/b/d/adr/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
putfield com/a/b/d/adr/f Ljava/util/Set;
L1:
aload 0
getfield com/a/b/d/adr/f Ljava/util/Set;
astore 2
aload 1
monitorexit
L3:
aload 2
areturn
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 3
.end method
