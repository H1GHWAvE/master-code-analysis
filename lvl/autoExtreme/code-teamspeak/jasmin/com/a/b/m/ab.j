.bytecode 50.0
.class final synchronized com/a/b/m/ab
.super java/lang/Object

.field private final 'a' Ljava/lang/reflect/TypeVariable;

.method <init>(Ljava/lang/reflect/TypeVariable;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/TypeVariable
putfield com/a/b/m/ab/a Ljava/lang/reflect/TypeVariable;
return
.limit locals 2
.limit stack 2
.end method

.method static a(Ljava/lang/reflect/Type;)Ljava/lang/Object;
aload 0
instanceof java/lang/reflect/TypeVariable
ifeq L0
new com/a/b/m/ab
dup
aload 0
checkcast java/lang/reflect/TypeVariable
invokespecial com/a/b/m/ab/<init>(Ljava/lang/reflect/TypeVariable;)V
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Ljava/lang/reflect/TypeVariable;)Z
aload 0
getfield com/a/b/m/ab/a Ljava/lang/reflect/TypeVariable;
invokeinterface java/lang/reflect/TypeVariable/getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration; 0
aload 1
invokeinterface java/lang/reflect/TypeVariable/getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration; 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/a/b/m/ab/a Ljava/lang/reflect/TypeVariable;
invokeinterface java/lang/reflect/TypeVariable/getName()Ljava/lang/String; 0
aload 1
invokeinterface java/lang/reflect/TypeVariable/getName()Ljava/lang/String; 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method final b(Ljava/lang/reflect/Type;)Z
aload 1
instanceof java/lang/reflect/TypeVariable
ifeq L0
aload 0
aload 1
checkcast java/lang/reflect/TypeVariable
invokespecial com/a/b/m/ab/a(Ljava/lang/reflect/TypeVariable;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 1
instanceof com/a/b/m/ab
ifeq L0
aload 0
aload 1
checkcast com/a/b/m/ab
getfield com/a/b/m/ab/a Ljava/lang/reflect/TypeVariable;
invokespecial com/a/b/m/ab/a(Ljava/lang/reflect/TypeVariable;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/m/ab/a Ljava/lang/reflect/TypeVariable;
invokeinterface java/lang/reflect/TypeVariable/getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration; 0
aastore
dup
iconst_1
aload 0
getfield com/a/b/m/ab/a Ljava/lang/reflect/TypeVariable;
invokeinterface java/lang/reflect/TypeVariable/getName()Ljava/lang/String; 0
aastore
invokestatic java/util/Arrays/hashCode([Ljava/lang/Object;)I
ireturn
.limit locals 1
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/m/ab/a Ljava/lang/reflect/TypeVariable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
