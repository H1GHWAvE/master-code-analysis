.bytecode 50.0
.class public synchronized abstract com/a/b/m/ae
.super com/a/b/m/u
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field final 'a' Ljava/lang/reflect/Type;

.field private transient 'b' Lcom/a/b/m/w;

.method protected <init>()V
aload 0
invokespecial com/a/b/m/u/<init>()V
aload 0
aload 0
invokevirtual com/a/b/m/ae/a()Ljava/lang/reflect/Type;
putfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/TypeVariable
ifne L0
iconst_1
istore 1
L1:
iload 1
ldc "Cannot construct a TypeToken for a type variable.\nYou probably meant to call new TypeToken<%s>(getClass()) that can resolve the type variable for you.\nIf you do need to create a TypeToken of a type variable, please use TypeToken.of() instead."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method private <init>(Ljava/lang/Class;)V
aload 0
invokespecial com/a/b/m/u/<init>()V
aload 0
invokespecial com/a/b/m/u/a()Ljava/lang/reflect/Type;
astore 2
aload 2
instanceof java/lang/Class
ifeq L0
aload 0
aload 2
putfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
return
L0:
aload 0
aload 1
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
aload 2
invokevirtual com/a/b/m/ae/b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
putfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Ljava/lang/reflect/Type;)V
aload 0
invokespecial com/a/b/m/u/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/Type
putfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Ljava/lang/reflect/Type;B)V
aload 0
aload 1
invokespecial com/a/b/m/ae/<init>(Ljava/lang/reflect/Type;)V
return
.limit locals 3
.limit stack 2
.end method

.method static a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
invokestatic com/a/b/d/jl/h()Lcom/a/b/d/jn;
astore 3
aload 0
arraylength
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
aaload
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
astore 4
aload 4
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/isInterface()Z
ifeq L2
aload 3
aload 4
invokevirtual com/a/b/d/jn/c(Ljava/lang/Object;)Lcom/a/b/d/jn;
pop
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 3
invokevirtual com/a/b/d/jn/b()Lcom/a/b/d/jl;
areturn
.limit locals 5
.limit stack 2
.end method

.method private a(Lcom/a/b/m/v;Lcom/a/b/m/ae;)Lcom/a/b/m/ae;
new com/a/b/m/am
dup
new com/a/b/m/w
dup
invokespecial com/a/b/m/w/<init>()V
new com/a/b/m/ab
dup
aload 1
getfield com/a/b/m/v/a Ljava/lang/reflect/TypeVariable;
invokespecial com/a/b/m/ab/<init>(Ljava/lang/reflect/TypeVariable;)V
aload 2
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/d/jt/c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;
invokevirtual com/a/b/m/w/a(Ljava/util/Map;)Lcom/a/b/m/w;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokespecial com/a/b/m/am/<init>(Ljava/lang/reflect/Type;)V
areturn
.limit locals 3
.limit stack 6
.end method

.method private a(Lcom/a/b/m/v;Ljava/lang/Class;)Lcom/a/b/m/ae;
aload 2
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
astore 2
new com/a/b/m/am
dup
new com/a/b/m/w
dup
invokespecial com/a/b/m/w/<init>()V
new com/a/b/m/ab
dup
aload 1
getfield com/a/b/m/v/a Ljava/lang/reflect/TypeVariable;
invokespecial com/a/b/m/ab/<init>(Ljava/lang/reflect/TypeVariable;)V
aload 2
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/d/jt/c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;
invokevirtual com/a/b/m/w/a(Ljava/util/Map;)Lcom/a/b/m/w;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokespecial com/a/b/m/am/<init>(Ljava/lang/reflect/Type;)V
areturn
.limit locals 3
.limit stack 6
.end method

.method public static a(Ljava/lang/Class;)Lcom/a/b/m/ae;
new com/a/b/m/am
dup
aload 0
invokespecial com/a/b/m/am/<init>(Ljava/lang/reflect/Type;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
aload 2
arraylength
istore 4
iconst_0
istore 3
L0:
iload 3
iload 4
if_icmpge L1
aload 2
iload 3
aaload
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
astore 5
aload 1
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
aload 5
invokevirtual com/a/b/m/ae/a(Lcom/a/b/m/ae;)Z
ifeq L2
aload 5
aload 1
invokespecial com/a/b/m/ae/b(Ljava/lang/Class;)Lcom/a/b/m/ae;
areturn
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 23
iadd
aload 2
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " isn't a super type of "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 6
.limit stack 6
.end method

.method public static a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
new com/a/b/m/am
dup
aload 0
invokespecial com/a/b/m/am/<init>(Ljava/lang/reflect/Type;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Ljava/lang/reflect/Constructor;)Lcom/a/b/m/k;
aload 1
invokevirtual java/lang/reflect/Constructor/getDeclaringClass()Ljava/lang/Class;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
if_acmpne L0
iconst_1
istore 2
L1:
iload 2
ldc "%s not declared by %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
new com/a/b/m/ag
dup
aload 0
aload 1
invokespecial com/a/b/m/ag/<init>(Lcom/a/b/m/ae;Ljava/lang/reflect/Constructor;)V
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 6
.end method

.method private a(Ljava/lang/reflect/Method;)Lcom/a/b/m/k;
aload 1
invokevirtual java/lang/reflect/Method/getDeclaringClass()Ljava/lang/Class;
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
aload 0
invokevirtual com/a/b/m/ae/a(Lcom/a/b/m/ae;)Z
ldc "%s not declared by %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
new com/a/b/m/af
dup
aload 0
aload 1
invokespecial com/a/b/m/af/<init>(Lcom/a/b/m/ae;Ljava/lang/reflect/Method;)V
areturn
.limit locals 2
.limit stack 6
.end method

.method private static a(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;
L0:
aload 0
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
astore 1
aload 1
arraylength
iconst_1
if_icmpne L1
aload 1
iconst_0
aaload
astore 1
aload 1
astore 0
aload 1
instanceof java/lang/reflect/WildcardType
ifeq L2
aload 1
checkcast java/lang/reflect/WildcardType
astore 0
goto L0
L1:
aload 1
arraylength
ifne L3
ldc java/lang/Object
astore 0
L2:
aload 0
areturn
L3:
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/AssertionError
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 59
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "There should be at most one upper bound for wildcard type: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 2
.limit stack 6
.end method

.method private static a(Ljava/lang/reflect/GenericArrayType;Ljava/lang/reflect/Type;)Z
iconst_0
istore 2
aload 1
instanceof java/lang/Class
ifeq L0
aload 1
checkcast java/lang/Class
astore 1
aload 1
invokevirtual java/lang/Class/isArray()Z
ifne L1
aload 1
ldc java/lang/Object
if_acmpne L2
iconst_1
istore 2
L2:
iload 2
ireturn
L1:
aload 0
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
aload 1
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ireturn
L0:
aload 1
instanceof java/lang/reflect/GenericArrayType
ifeq L2
aload 1
checkcast java/lang/reflect/GenericArrayType
astore 1
aload 0
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
aload 1
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ireturn
.limit locals 3
.limit stack 2
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Z
aload 1
aload 0
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/GenericArrayType;)Z
aload 0
instanceof java/lang/Class
ifeq L0
aload 0
checkcast java/lang/Class
astore 0
aload 0
invokevirtual java/lang/Class/isArray()Z
ifne L1
L2:
iconst_0
ireturn
L1:
aload 0
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
aload 1
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ireturn
L0:
aload 0
instanceof java/lang/reflect/GenericArrayType
ifeq L2
aload 0
checkcast java/lang/reflect/GenericArrayType
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
aload 1
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/ParameterizedType;)Z
aload 1
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
astore 4
aload 4
aload 0
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifne L0
L1:
iconst_0
ireturn
L0:
aload 4
invokevirtual java/lang/Class/getTypeParameters()[Ljava/lang/reflect/TypeVariable;
astore 4
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
astore 1
aload 0
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
astore 0
iconst_0
istore 2
L2:
iload 2
aload 4
arraylength
if_icmpge L3
aload 0
aload 4
iload 2
aaload
invokevirtual com/a/b/m/ae/b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
astore 5
aload 1
iload 2
aaload
astore 6
aload 5
aload 6
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L4
iconst_1
istore 3
L5:
iload 3
ifeq L1
iload 2
iconst_1
iadd
istore 2
goto L2
L4:
aload 6
instanceof java/lang/reflect/WildcardType
ifeq L6
aload 5
aload 6
checkcast java/lang/reflect/WildcardType
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/WildcardType;)Z
istore 3
goto L5
L6:
iconst_0
istore 3
goto L5
L3:
iconst_1
ireturn
.limit locals 7
.limit stack 3
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
iconst_0
istore 5
L0:
aload 1
aload 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L1
iconst_1
istore 4
L2:
iload 4
ireturn
L1:
aload 1
instanceof java/lang/reflect/WildcardType
ifeq L3
aload 0
aload 1
checkcast java/lang/reflect/WildcardType
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/WildcardType;)Z
ireturn
L3:
aload 0
instanceof java/lang/reflect/TypeVariable
ifeq L4
aload 0
checkcast java/lang/reflect/TypeVariable
invokeinterface java/lang/reflect/TypeVariable/getBounds()[Ljava/lang/reflect/Type; 0
aload 1
invokestatic com/a/b/m/ae/a([Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ireturn
L4:
aload 0
instanceof java/lang/reflect/WildcardType
ifeq L5
aload 0
checkcast java/lang/reflect/WildcardType
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
aload 1
invokestatic com/a/b/m/ae/a([Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ireturn
L5:
aload 0
instanceof java/lang/reflect/GenericArrayType
ifeq L6
aload 0
checkcast java/lang/reflect/GenericArrayType
astore 0
aload 1
instanceof java/lang/Class
ifeq L7
aload 1
checkcast java/lang/Class
astore 1
aload 1
invokevirtual java/lang/Class/isArray()Z
ifne L8
iload 5
istore 4
aload 1
ldc java/lang/Object
if_acmpne L2
iconst_1
ireturn
L8:
aload 0
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
astore 0
aload 1
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
astore 1
goto L0
L7:
iload 5
istore 4
aload 1
instanceof java/lang/reflect/GenericArrayType
ifeq L2
aload 1
checkcast java/lang/reflect/GenericArrayType
astore 1
aload 0
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
astore 0
aload 1
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
astore 1
goto L0
L6:
aload 1
instanceof java/lang/Class
ifeq L9
aload 1
checkcast java/lang/Class
aload 0
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ireturn
L9:
aload 1
instanceof java/lang/reflect/ParameterizedType
ifeq L10
aload 1
checkcast java/lang/reflect/ParameterizedType
astore 6
aload 6
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
astore 1
iload 5
istore 4
aload 1
aload 0
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L2
aload 1
invokevirtual java/lang/Class/getTypeParameters()[Ljava/lang/reflect/TypeVariable;
astore 1
aload 6
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
astore 6
aload 0
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
astore 0
iconst_0
istore 2
L11:
iload 2
aload 1
arraylength
if_icmpge L12
aload 0
aload 1
iload 2
aaload
invokevirtual com/a/b/m/ae/b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
astore 7
aload 6
iload 2
aaload
astore 8
aload 7
aload 8
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L13
iconst_1
istore 3
L14:
iload 5
istore 4
iload 3
ifeq L2
iload 2
iconst_1
iadd
istore 2
goto L11
L13:
aload 8
instanceof java/lang/reflect/WildcardType
ifeq L15
aload 7
aload 8
checkcast java/lang/reflect/WildcardType
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/WildcardType;)Z
istore 3
goto L14
L15:
iconst_0
istore 3
goto L14
L12:
iconst_1
ireturn
L10:
iload 5
istore 4
aload 1
instanceof java/lang/reflect/GenericArrayType
ifeq L2
aload 1
checkcast java/lang/reflect/GenericArrayType
astore 1
aload 0
instanceof java/lang/Class
ifeq L16
aload 0
checkcast java/lang/Class
astore 0
iload 5
istore 4
aload 0
invokevirtual java/lang/Class/isArray()Z
ifeq L2
aload 0
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
astore 0
aload 1
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
astore 1
goto L0
L16:
iload 5
istore 4
aload 0
instanceof java/lang/reflect/GenericArrayType
ifeq L2
aload 0
checkcast java/lang/reflect/GenericArrayType
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
astore 0
aload 1
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
astore 1
goto L0
.limit locals 9
.limit stack 3
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/WildcardType;)Z
aload 0
aload 1
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ifeq L0
aload 1
invokestatic com/a/b/m/ae/b(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;
astore 1
aload 1
ifnonnull L1
iconst_1
istore 2
L2:
iload 2
ifeq L0
iconst_1
ireturn
L1:
aload 0
invokestatic com/a/b/m/ae/i(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 0
aload 0
ifnonnull L3
iconst_0
istore 2
goto L2
L3:
aload 1
aload 0
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
istore 2
goto L2
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method private static a([Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
iconst_0
istore 5
aload 0
arraylength
istore 3
iconst_0
istore 2
L0:
iload 5
istore 4
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
aaload
aload 1
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ifeq L2
iconst_1
istore 4
L1:
iload 4
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 6
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/m/ae;[Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
iconst_0
istore 2
L0:
iload 2
aload 1
arraylength
if_icmpge L1
aload 1
iload 2
aload 0
aload 1
iload 2
aaload
invokevirtual com/a/b/m/ae/b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
aastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 1
areturn
.limit locals 3
.limit stack 5
.end method

.method private b(Ljava/lang/Class;)Lcom/a/b/m/ae;
aload 1
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ldc "%s is not a super class of %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/TypeVariable
ifeq L0
aload 0
aload 1
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/TypeVariable
invokeinterface java/lang/reflect/TypeVariable/getBounds()[Ljava/lang/reflect/Type; 0
invokespecial com/a/b/m/ae/a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L0:
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/WildcardType
ifeq L1
aload 0
aload 1
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/WildcardType
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
invokespecial com/a/b/m/ae/a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L1:
aload 1
invokevirtual java/lang/Class/isArray()Z
ifeq L2
aload 0
invokespecial com/a/b/m/ae/n()Lcom/a/b/m/ae;
ldc "%s isn't a super type of %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 0
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/m/ae
aload 1
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
invokespecial com/a/b/m/ae/b(Ljava/lang/Class;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
astore 1
getstatic com/a/b/m/bh/b Lcom/a/b/m/bh;
aload 1
invokevirtual com/a/b/m/bh/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L2:
aload 0
aload 1
invokestatic com/a/b/m/ae/d(Ljava/lang/Class;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokevirtual com/a/b/m/ae/c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
.limit locals 2
.limit stack 6
.end method

.method private b(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
aload 2
arraylength
ifle L0
aload 2
iconst_0
aaload
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
aload 1
invokespecial com/a/b/m/ae/c(Ljava/lang/Class;)Lcom/a/b/m/ae;
areturn
L0:
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 21
iadd
aload 2
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " isn't a subclass of "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 6
.end method

.method static synthetic b(Lcom/a/b/m/ae;)Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
astore 1
aload 1
arraylength
iconst_1
if_icmpne L0
aload 1
iconst_0
aaload
invokestatic com/a/b/m/ae/i(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
areturn
L0:
aload 1
arraylength
ifne L1
aconst_null
areturn
L1:
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/AssertionError
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 46
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Wildcard should have at most one lower bound: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 2
.limit stack 6
.end method

.method private static b(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
aload 0
aload 1
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L0:
aload 1
instanceof java/lang/reflect/WildcardType
ifeq L1
aload 0
aload 1
checkcast java/lang/reflect/WildcardType
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/WildcardType;)Z
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/reflect/Type;Ljava/lang/reflect/WildcardType;)Z
aload 1
invokestatic com/a/b/m/ae/b(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;
astore 1
aload 1
ifnonnull L0
iconst_1
ireturn
L0:
aload 0
invokestatic com/a/b/m/ae/i(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 0
aload 0
ifnonnull L1
iconst_0
ireturn
L1:
aload 1
aload 0
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
iconst_0
istore 2
L0:
iload 2
aload 1
arraylength
if_icmpge L1
aload 1
iload 2
aload 0
aload 1
iload 2
aaload
invokevirtual com/a/b/m/ae/b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
aastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 1
areturn
.limit locals 3
.limit stack 5
.end method

.method private static synthetic c(Lcom/a/b/m/ae;)Lcom/a/b/d/lo;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/f(Ljava/lang/reflect/Type;)Lcom/a/b/d/lo;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(Ljava/lang/Class;)Lcom/a/b/m/ae;
iconst_1
istore 2
aload 0
astore 4
L0:
aload 4
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/TypeVariable
ifne L1
iconst_1
istore 3
L2:
iload 3
ldc "Cannot get subtype of type variable <%s>"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 4
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 4
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/WildcardType
ifeq L3
aload 4
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/WildcardType
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
astore 5
aload 5
arraylength
ifle L4
aload 5
iconst_0
aaload
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
astore 4
goto L0
L1:
iconst_0
istore 3
goto L2
L4:
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 4
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 4
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 21
iadd
aload 4
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " isn't a subclass of "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 4
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
aload 1
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ldc "%s isn't a subclass of %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 4
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 4
invokespecial com/a/b/m/ae/n()Lcom/a/b/m/ae;
ifnull L5
L6:
iload 2
ifeq L7
aload 4
invokespecial com/a/b/m/ae/n()Lcom/a/b/m/ae;
aload 1
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
invokespecial com/a/b/m/ae/c(Ljava/lang/Class;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
astore 1
getstatic com/a/b/m/bh/b Lcom/a/b/m/bh;
aload 1
invokevirtual com/a/b/m/bh/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L5:
iconst_0
istore 2
goto L6
L7:
aload 4
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/Class
ifeq L8
L9:
aload 1
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L8:
aload 1
invokestatic com/a/b/m/ae/d(Ljava/lang/Class;)Lcom/a/b/m/ae;
astore 1
aload 1
aload 4
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokespecial com/a/b/m/ae/b(Ljava/lang/Class;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
astore 5
new com/a/b/m/w
dup
invokespecial com/a/b/m/w/<init>()V
astore 6
aload 4
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
astore 4
invokestatic com/a/b/d/sz/c()Ljava/util/HashMap;
astore 7
aload 7
aload 5
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/Type
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/Type
invokestatic com/a/b/m/w/a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
aload 6
aload 7
invokevirtual com/a/b/m/w/a(Ljava/util/Map;)Lcom/a/b/m/w;
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 1
goto L9
.limit locals 8
.limit stack 6
.end method

.method private static d(Ljava/lang/Class;)Lcom/a/b/m/ae;
.annotation invisible Lcom/a/b/a/d;
.end annotation
aload 0
invokevirtual java/lang/Class/isArray()Z
ifeq L0
aload 0
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
invokestatic com/a/b/m/ae/d(Ljava/lang/Class;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ay/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L0:
aload 0
invokevirtual java/lang/Class/getTypeParameters()[Ljava/lang/reflect/TypeVariable;
astore 1
aload 1
arraylength
ifle L1
aload 0
aload 1
invokestatic com/a/b/m/ay/a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L1:
aload 0
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
areturn
.limit locals 2
.limit stack 2
.end method

.method static d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
astore 1
aload 1
astore 0
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/isInterface()Z
ifeq L0
aconst_null
astore 0
L0:
aload 0
areturn
.limit locals 2
.limit stack 1
.end method

.method private d()Ljava/lang/Class;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Lcom/a/b/d/lo;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/f(Ljava/lang/reflect/Type;)Lcom/a/b/d/lo;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e(Ljava/lang/Class;)Lcom/a/b/m/ae;
aload 0
invokespecial com/a/b/m/ae/n()Lcom/a/b/m/ae;
ldc "%s isn't a super type of %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 0
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/m/ae
aload 1
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
invokespecial com/a/b/m/ae/b(Ljava/lang/Class;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
astore 1
getstatic com/a/b/m/bh/b Lcom/a/b/m/bh;
aload 1
invokevirtual com/a/b/m/bh/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
.limit locals 2
.limit stack 6
.end method

.method static e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
.annotation invisible Lcom/a/b/a/d;
.end annotation
aload 0
invokestatic com/a/b/m/ae/f(Ljava/lang/reflect/Type;)Lcom/a/b/d/lo;
invokevirtual com/a/b/d/lo/c()Lcom/a/b/d/agi;
invokevirtual com/a/b/d/agi/next()Ljava/lang/Object;
checkcast java/lang/Class
areturn
.limit locals 1
.limit stack 1
.end method

.method static f(Ljava/lang/reflect/Type;)Lcom/a/b/d/lo;
.annotation invisible Lcom/a/b/a/d;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/d/lo/i()Lcom/a/b/d/lp;
astore 1
new com/a/b/m/ai
dup
aload 1
invokespecial com/a/b/m/ai/<init>(Lcom/a/b/d/lp;)V
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 0
aastore
invokevirtual com/a/b/m/ai/a([Ljava/lang/reflect/Type;)V
aload 1
invokevirtual com/a/b/d/lp/b()Lcom/a/b/d/lo;
areturn
.limit locals 2
.limit stack 5
.end method

.method private f(Ljava/lang/Class;)Lcom/a/b/m/ae;
aload 0
invokespecial com/a/b/m/ae/n()Lcom/a/b/m/ae;
aload 1
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
invokespecial com/a/b/m/ae/c(Ljava/lang/Class;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
astore 1
getstatic com/a/b/m/bh/b Lcom/a/b/m/bh;
aload 1
invokevirtual com/a/b/m/bh/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
.limit locals 2
.limit stack 2
.end method

.method private f()Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Lcom/a/b/m/ae;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/TypeVariable
ifeq L0
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/TypeVariable
invokeinterface java/lang/reflect/TypeVariable/getBounds()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
invokestatic com/a/b/m/ae/d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L0:
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/WildcardType
ifeq L1
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/WildcardType
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
invokestatic com/a/b/m/ae/d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L1:
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/getGenericSuperclass()Ljava/lang/reflect/Type;
astore 1
aload 1
ifnonnull L2
aconst_null
areturn
L2:
aload 0
aload 1
invokevirtual com/a/b/m/ae/c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
.limit locals 2
.limit stack 2
.end method

.method private g(Ljava/lang/Class;)Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/Class
ifeq L0
aload 1
areturn
L0:
aload 1
invokestatic com/a/b/m/ae/d(Ljava/lang/Class;)Lcom/a/b/m/ae;
astore 1
aload 1
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokespecial com/a/b/m/ae/b(Ljava/lang/Class;)Lcom/a/b/m/ae;
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
astore 2
new com/a/b/m/w
dup
invokespecial com/a/b/m/w/<init>()V
astore 3
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
astore 4
invokestatic com/a/b/d/sz/c()Ljava/util/HashMap;
astore 5
aload 5
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/Type
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/Type
invokestatic com/a/b/m/w/a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
aload 3
aload 5
invokevirtual com/a/b/m/w/a(Ljava/util/Map;)Lcom/a/b/m/w;
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
areturn
.limit locals 6
.limit stack 3
.end method

.method private g(Ljava/lang/reflect/Type;)Z
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/Type
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private h()Lcom/a/b/d/jl;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/TypeVariable
ifeq L0
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/TypeVariable
invokeinterface java/lang/reflect/TypeVariable/getBounds()[Ljava/lang/reflect/Type; 0
invokestatic com/a/b/m/ae/a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
areturn
L0:
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/WildcardType
ifeq L1
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/WildcardType
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
invokestatic com/a/b/m/ae/a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
areturn
L1:
invokestatic com/a/b/d/jl/h()Lcom/a/b/d/jn;
astore 3
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/getGenericInterfaces()[Ljava/lang/reflect/Type;
astore 4
aload 4
arraylength
istore 2
iconst_0
istore 1
L2:
iload 1
iload 2
if_icmpge L3
aload 3
aload 0
aload 4
iload 1
aaload
invokevirtual com/a/b/m/ae/c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
invokevirtual com/a/b/d/jn/c(Ljava/lang/Object;)Lcom/a/b/d/jn;
pop
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 3
invokevirtual com/a/b/d/jn/b()Lcom/a/b/d/jl;
areturn
.limit locals 5
.limit stack 4
.end method

.method private static h(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
aload 0
astore 1
aload 0
instanceof java/lang/reflect/WildcardType
ifeq L0
aload 0
checkcast java/lang/reflect/WildcardType
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;
astore 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 1
.end method

.method private static i(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
astore 1
aload 0
instanceof java/lang/reflect/WildcardType
ifeq L0
aload 0
checkcast java/lang/reflect/WildcardType
invokestatic com/a/b/m/ae/b(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;
astore 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 1
.end method

.method private i()Z
aload 0
invokespecial com/a/b/m/ae/n()Lcom/a/b/m/ae;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static j(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
getstatic com/a/b/m/bh/b Lcom/a/b/m/bh;
aload 0
invokevirtual com/a/b/m/bh/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 2
.end method

.method private j()Z
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/Class
ifeq L0
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/Class
invokevirtual java/lang/Class/isPrimitive()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private k()Lcom/a/b/m/ae;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/Class
ifeq L0
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/Class
invokevirtual java/lang/Class/isPrimitive()Z
ifeq L0
iconst_1
istore 1
L1:
aload 0
astore 2
iload 1
ifeq L2
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/Class
invokestatic com/a/b/l/z/a(Ljava/lang/Class;)Ljava/lang/Class;
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
astore 2
L2:
aload 2
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 3
.limit stack 1
.end method

.method private l()Z
invokestatic com/a/b/l/z/a()Ljava/util/Set;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private m()Lcom/a/b/m/ae;
aload 0
astore 1
invokestatic com/a/b/l/z/a()Ljava/util/Set;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifeq L0
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/Class
invokestatic com/a/b/l/z/b(Ljava/lang/Class;)Ljava/lang/Class;
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
astore 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private n()Lcom/a/b/m/ae;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ay/c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 1
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
.limit locals 2
.limit stack 1
.end method

.method private o()Ljava/lang/Object;
new com/a/b/m/w
dup
invokespecial com/a/b/m/w/<init>()V
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final a(Lcom/a/b/m/ae;)Z
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/Type
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/m/ae/b Lcom/a/b/m/w;
astore 3
aload 3
astore 2
aload 3
ifnonnull L0
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
astore 2
new com/a/b/m/w
dup
invokespecial com/a/b/m/w/<init>()V
aload 2
invokestatic com/a/b/m/y/a(Ljava/lang/reflect/Type;)Lcom/a/b/d/jt;
invokevirtual com/a/b/m/w/a(Ljava/util/Map;)Lcom/a/b/m/w;
astore 2
aload 0
aload 2
putfield com/a/b/m/ae/b Lcom/a/b/m/w;
L0:
aload 2
aload 1
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
.limit locals 4
.limit stack 2
.end method

.method public final b()Lcom/a/b/m/aw;
new com/a/b/m/aw
dup
aload 0
invokespecial com/a/b/m/aw/<init>(Lcom/a/b/m/ae;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method final c()Lcom/a/b/m/ae;
new com/a/b/m/ah
dup
aload 0
invokespecial com/a/b/m/ah/<init>(Lcom/a/b/m/ae;)V
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
aastore
invokevirtual com/a/b/m/ah/a([Ljava/lang/reflect/Type;)V
aload 0
areturn
.limit locals 1
.limit stack 5
.end method

.method final c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
aload 0
aload 1
invokevirtual com/a/b/m/ae/b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
astore 1
aload 1
aload 0
getfield com/a/b/m/ae/b Lcom/a/b/m/w;
putfield com/a/b/m/ae/b Lcom/a/b/m/w;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/m/ae
ifeq L0
aload 1
checkcast com/a/b/m/ae
astore 1
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public hashCode()I
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokevirtual java/lang/Object/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ay/b(Ljava/lang/reflect/Type;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
