.bytecode 50.0
.class final synchronized com/a/b/m/ay
.super java/lang/Object

.field private static final 'a' Lcom/a/b/b/bj;

.field private static final 'b' Lcom/a/b/b/bv;

.method static <clinit>()V
new com/a/b/m/az
dup
invokespecial com/a/b/m/az/<init>()V
putstatic com/a/b/m/ay/a Lcom/a/b/b/bj;
ldc ", "
invokestatic com/a/b/b/bv/a(Ljava/lang/String;)Lcom/a/b/b/bv;
ldc "null"
invokevirtual com/a/b/b/bv/b(Ljava/lang/String;)Lcom/a/b/b/bv;
putstatic com/a/b/m/ay/b Lcom/a/b/b/bv;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic a()Lcom/a/b/b/bj;
getstatic com/a/b/m/ay/a Lcom/a/b/b/bj;
areturn
.limit locals 0
.limit stack 1
.end method

.method static a(Ljava/lang/Class;)Ljava/lang/Class;
aload 0
iconst_0
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;I)Ljava/lang/Object;
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
areturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
aload 0
ldc java/lang/Object
invokestatic com/a/b/b/cp/a(Ljava/lang/Object;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/d/mq/c(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Iterable;
areturn
.limit locals 1
.limit stack 2
.end method

.method static transient a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;
new com/a/b/m/bn
dup
getstatic com/a/b/m/bb/c Lcom/a/b/m/bb;
aload 0
invokevirtual com/a/b/m/bb/a(Ljava/lang/Class;)Ljava/lang/Class;
aload 0
aload 1
invokespecial com/a/b/m/bn/<init>(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method static transient a(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
ifnonnull L0
aload 1
aload 2
invokestatic com/a/b/m/ay/a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;
areturn
L0:
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokevirtual java/lang/Class/getEnclosingClass()Ljava/lang/Class;
ifnull L1
iconst_1
istore 3
L2:
iload 3
ldc "Owner type for unenclosed %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
new com/a/b/m/bn
dup
aload 0
aload 1
aload 2
invokespecial com/a/b/m/bn/<init>(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)V
areturn
L1:
iconst_0
istore 3
goto L2
.limit locals 4
.limit stack 6
.end method

.method static a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
iconst_1
istore 2
aload 0
instanceof java/lang/reflect/WildcardType
ifeq L0
aload 0
checkcast java/lang/reflect/WildcardType
astore 0
aload 0
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
astore 3
aload 3
arraylength
iconst_1
if_icmpgt L1
iconst_1
istore 1
L2:
iload 1
ldc "Wildcard cannot have more than one lower bounds."
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 3
arraylength
iconst_1
if_icmpne L3
new com/a/b/m/bp
dup
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 3
iconst_0
aaload
invokestatic com/a/b/m/ay/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
aastore
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
ldc java/lang/Object
aastore
invokespecial com/a/b/m/bp/<init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
L1:
iconst_0
istore 1
goto L2
L3:
aload 0
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
astore 0
aload 0
arraylength
iconst_1
if_icmpne L4
iload 2
istore 1
L5:
iload 1
ldc "Wildcard should have only one upper bound."
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 0
iconst_0
aaload
invokestatic com/a/b/m/ay/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ay/d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;
areturn
L4:
iconst_0
istore 1
goto L5
L0:
getstatic com/a/b/m/bh/d Lcom/a/b/m/bh;
aload 0
invokevirtual com/a/b/m/bh/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
areturn
.limit locals 4
.limit stack 7
.end method

.method static synthetic a([Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
aload 0
arraylength
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
aaload
invokestatic com/a/b/m/ay/c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 3
aload 3
ifnull L2
aload 3
instanceof java/lang/Class
ifeq L3
aload 3
checkcast java/lang/Class
astore 0
aload 0
invokevirtual java/lang/Class/isPrimitive()Z
ifeq L3
aload 0
areturn
L3:
aload 3
invokestatic com/a/b/m/ay/d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;
areturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aconst_null
areturn
.limit locals 4
.limit stack 2
.end method

.method static transient a(Ljava/lang/reflect/GenericDeclaration;Ljava/lang/String;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/TypeVariable;
aload 2
astore 3
aload 2
arraylength
ifne L0
iconst_1
anewarray java/lang/reflect/Type
astore 3
aload 3
iconst_0
ldc java/lang/Object
aastore
L0:
new com/a/b/m/bo
dup
aload 0
aload 1
aload 3
invokespecial com/a/b/m/bo/<init>(Ljava/lang/reflect/GenericDeclaration;Ljava/lang/String;[Ljava/lang/reflect/Type;)V
areturn
.limit locals 4
.limit stack 5
.end method

.method static synthetic a([Ljava/lang/reflect/Type;Ljava/lang/String;)V
aload 0
arraylength
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
aaload
astore 5
aload 5
instanceof java/lang/Class
ifeq L2
aload 5
checkcast java/lang/Class
astore 5
aload 5
invokevirtual java/lang/Class/isPrimitive()Z
ifne L3
iconst_1
istore 4
L4:
iload 4
ldc "Primitive type '%s' used as %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 5
aastore
dup
iconst_1
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L3:
iconst_0
istore 4
goto L4
L1:
return
.limit locals 6
.limit stack 6
.end method

.method static synthetic a(Ljava/util/Collection;)[Ljava/lang/reflect/Type;
aload 0
aload 0
invokeinterface java/util/Collection/size()I 0
anewarray java/lang/reflect/Type
invokeinterface java/util/Collection/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic b()Lcom/a/b/b/bv;
getstatic com/a/b/m/ay/b Lcom/a/b/b/bv;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/lang/Iterable;
aload 0
ldc java/lang/Object
invokestatic com/a/b/b/cp/a(Ljava/lang/Object;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/d/mq/c(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Iterable;
areturn
.limit locals 1
.limit stack 2
.end method

.method static b(Ljava/lang/reflect/Type;)Ljava/lang/String;
aload 0
instanceof java/lang/Class
ifeq L0
aload 0
checkcast java/lang/Class
invokevirtual java/lang/Class/getName()Ljava/lang/String;
areturn
L0:
aload 0
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static b([Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
arraylength
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
aaload
invokestatic com/a/b/m/ay/c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 3
aload 3
ifnull L2
aload 3
instanceof java/lang/Class
ifeq L3
aload 3
checkcast java/lang/Class
astore 0
aload 0
invokevirtual java/lang/Class/isPrimitive()Z
ifeq L3
aload 0
areturn
L3:
aload 3
invokestatic com/a/b/m/ay/d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;
areturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aconst_null
areturn
.limit locals 4
.limit stack 2
.end method

.method private static b([Ljava/lang/reflect/Type;Ljava/lang/String;)V
aload 0
arraylength
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
aaload
astore 5
aload 5
instanceof java/lang/Class
ifeq L2
aload 5
checkcast java/lang/Class
astore 5
aload 5
invokevirtual java/lang/Class/isPrimitive()Z
ifne L3
iconst_1
istore 4
L4:
iload 4
ldc "Primitive type '%s' used as %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 5
aastore
dup
iconst_1
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L3:
iconst_0
istore 4
goto L4
L1:
return
.limit locals 6
.limit stack 6
.end method

.method private static b(Ljava/util/Collection;)[Ljava/lang/reflect/Type;
aload 0
aload 0
invokeinterface java/util/Collection/size()I 0
anewarray java/lang/reflect/Type
invokeinterface java/util/Collection/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 2
.end method

.method static c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new java/util/concurrent/atomic/AtomicReference
dup
invokespecial java/util/concurrent/atomic/AtomicReference/<init>()V
astore 1
new com/a/b/m/ba
dup
aload 1
invokespecial com/a/b/m/ba/<init>(Ljava/util/concurrent/atomic/AtomicReference;)V
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 0
aastore
invokevirtual com/a/b/m/ba/a([Ljava/lang/reflect/Type;)V
aload 1
invokevirtual java/util/concurrent/atomic/AtomicReference/get()Ljava/lang/Object;
checkcast java/lang/reflect/Type
areturn
.limit locals 2
.limit stack 5
.end method

.method private static d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;
.annotation invisible Lcom/a/b/a/d;
.end annotation
new com/a/b/m/bp
dup
iconst_0
anewarray java/lang/reflect/Type
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 0
aastore
invokespecial com/a/b/m/bp/<init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
.limit locals 1
.limit stack 7
.end method

.method private static e(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;
.annotation invisible Lcom/a/b/a/d;
.end annotation
new com/a/b/m/bp
dup
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 0
aastore
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
ldc java/lang/Object
aastore
invokespecial com/a/b/m/bp/<init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
.limit locals 1
.limit stack 7
.end method
