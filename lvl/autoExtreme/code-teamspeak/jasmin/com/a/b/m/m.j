.bytecode 50.0
.class synchronized com/a/b/m/m
.super com/a/b/m/k

.field final 'a' Ljava/lang/reflect/Method;

.method <init>(Ljava/lang/reflect/Method;)V
aload 0
aload 1
invokespecial com/a/b/m/k/<init>(Ljava/lang/reflect/AccessibleObject;)V
aload 0
aload 1
putfield com/a/b/m/m/a Ljava/lang/reflect/Method;
return
.limit locals 2
.limit stack 2
.end method

.method final a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/m/m/a Ljava/lang/reflect/Method;
aload 1
aload 2
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final b()Z
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isFinal(I)Z
ifne L0
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isPrivate(I)Z
ifne L0
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isStatic(I)Z
ifne L0
aload 0
invokevirtual com/a/b/m/m/getDeclaringClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getModifiers()I
invokestatic java/lang/reflect/Modifier/isFinal(I)Z
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Z
aload 0
getfield com/a/b/m/m/a Ljava/lang/reflect/Method;
invokevirtual java/lang/reflect/Method/isVarArgs()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method d()[Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/m/a Ljava/lang/reflect/Method;
invokevirtual java/lang/reflect/Method/getGenericParameterTypes()[Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method e()[Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/m/a Ljava/lang/reflect/Method;
invokevirtual java/lang/reflect/Method/getGenericExceptionTypes()[Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method final f()[[Ljava/lang/annotation/Annotation;
aload 0
getfield com/a/b/m/m/a Ljava/lang/reflect/Method;
invokevirtual java/lang/reflect/Method/getParameterAnnotations()[[Ljava/lang/annotation/Annotation;
areturn
.limit locals 1
.limit stack 1
.end method

.method g()Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/m/a Ljava/lang/reflect/Method;
invokevirtual java/lang/reflect/Method/getGenericReturnType()Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getTypeParameters()[Ljava/lang/reflect/TypeVariable;
aload 0
getfield com/a/b/m/m/a Ljava/lang/reflect/Method;
invokevirtual java/lang/reflect/Method/getTypeParameters()[Ljava/lang/reflect/TypeVariable;
areturn
.limit locals 1
.limit stack 1
.end method
