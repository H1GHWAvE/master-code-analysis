.bytecode 50.0
.class final synchronized com/a/b/m/bn
.super java/lang/Object
.implements java/io/Serializable
.implements java/lang/reflect/ParameterizedType

.field private static final 'd' J = 0L


.field private final 'a' Ljava/lang/reflect/Type;

.field private final 'b' Lcom/a/b/d/jl;

.field private final 'c' Ljava/lang/Class;

.method <init>(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
arraylength
aload 2
invokevirtual java/lang/Class/getTypeParameters()[Ljava/lang/reflect/TypeVariable;
arraylength
if_icmpne L0
iconst_1
istore 4
L1:
iload 4
invokestatic com/a/b/b/cn/a(Z)V
aload 3
ldc "type parameter"
invokestatic com/a/b/m/ay/a([Ljava/lang/reflect/Type;Ljava/lang/String;)V
aload 0
aload 1
putfield com/a/b/m/bn/a Ljava/lang/reflect/Type;
aload 0
aload 2
putfield com/a/b/m/bn/c Ljava/lang/Class;
aload 0
getstatic com/a/b/m/bh/d Lcom/a/b/m/bh;
aload 3
invokevirtual com/a/b/m/bh/a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
putfield com/a/b/m/bn/b Lcom/a/b/d/jl;
return
L0:
iconst_0
istore 4
goto L1
.limit locals 5
.limit stack 3
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 1
instanceof java/lang/reflect/ParameterizedType
ifne L0
L1:
iconst_0
ireturn
L0:
aload 1
checkcast java/lang/reflect/ParameterizedType
astore 1
aload 0
invokevirtual com/a/b/m/bn/getRawType()Ljava/lang/reflect/Type;
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L1
aload 0
invokevirtual com/a/b/m/bn/getOwnerType()Ljava/lang/reflect/Type;
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getOwnerType()Ljava/lang/reflect/Type; 0
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L1
aload 0
invokevirtual com/a/b/m/bn/getActualTypeArguments()[Ljava/lang/reflect/Type;
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
invokestatic java/util/Arrays/equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
ifeq L1
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final getActualTypeArguments()[Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/bn/b Lcom/a/b/d/jl;
invokestatic com/a/b/m/ay/a(Ljava/util/Collection;)[Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getOwnerType()Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/bn/a Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getRawType()Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/bn/c Ljava/lang/Class;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/m/bn/a Ljava/lang/reflect/Type;
ifnonnull L0
iconst_0
istore 1
L1:
iload 1
aload 0
getfield com/a/b/m/bn/b Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/hashCode()I
ixor
aload 0
getfield com/a/b/m/bn/c Ljava/lang/Class;
invokevirtual java/lang/Object/hashCode()I
ixor
ireturn
L0:
aload 0
getfield com/a/b/m/bn/a Ljava/lang/reflect/Type;
invokevirtual java/lang/Object/hashCode()I
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
aload 0
getfield com/a/b/m/bn/a Ljava/lang/reflect/Type;
ifnull L0
aload 1
getstatic com/a/b/m/bh/d Lcom/a/b/m/bh;
aload 0
getfield com/a/b/m/bn/a Ljava/lang/reflect/Type;
invokevirtual com/a/b/m/bh/c(Ljava/lang/reflect/Type;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
bipush 46
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L0:
aload 1
aload 0
getfield com/a/b/m/bn/c Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
bipush 60
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokestatic com/a/b/m/ay/b()Lcom/a/b/b/bv;
aload 0
getfield com/a/b/m/bn/b Lcom/a/b/d/jl;
invokestatic com/a/b/m/ay/a()Lcom/a/b/b/bj;
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Ljava/lang/Iterable;
invokevirtual com/a/b/b/bv/a(Ljava/lang/Iterable;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
bipush 62
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
