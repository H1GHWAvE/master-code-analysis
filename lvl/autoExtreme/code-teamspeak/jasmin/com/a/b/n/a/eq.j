.bytecode 50.0
.class final synchronized com/a/b/n/a/eq
.super java/lang/Object
.implements java/util/concurrent/Executor

.field private static final 'a' Ljava/util/logging/Logger;

.field private final 'b' Ljava/util/concurrent/Executor;

.field private final 'c' Ljava/util/Queue;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "internalLock"
.end annotation
.end field

.field private 'd' Z
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "internalLock"
.end annotation
.end field

.field private final 'e' Lcom/a/b/n/a/es;

.field private final 'f' Ljava/lang/Object;

.method static <clinit>()V
ldc com/a/b/n/a/eq
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putstatic com/a/b/n/a/eq/a Ljava/util/logging/Logger;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>(Ljava/util/concurrent/Executor;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayDeque
dup
invokespecial java/util/ArrayDeque/<init>()V
putfield com/a/b/n/a/eq/c Ljava/util/Queue;
aload 0
iconst_0
putfield com/a/b/n/a/eq/d Z
aload 0
new com/a/b/n/a/es
dup
aload 0
iconst_0
invokespecial com/a/b/n/a/es/<init>(Lcom/a/b/n/a/eq;B)V
putfield com/a/b/n/a/eq/e Lcom/a/b/n/a/es;
aload 0
new com/a/b/n/a/er
dup
aload 0
invokespecial com/a/b/n/a/er/<init>(Lcom/a/b/n/a/eq;)V
putfield com/a/b/n/a/eq/f Ljava/lang/Object;
aload 1
ldc "'executor' must not be null."
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
putfield com/a/b/n/a/eq/b Ljava/util/concurrent/Executor;
return
.limit locals 2
.limit stack 5
.end method

.method static synthetic a()Ljava/util/logging/Logger;
getstatic com/a/b/n/a/eq/a Ljava/util/logging/Logger;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic a(Lcom/a/b/n/a/eq;)Z
aload 0
getfield com/a/b/n/a/eq/d Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/a/b/n/a/eq;)Ljava/lang/Object;
aload 0
getfield com/a/b/n/a/eq/f Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/a/b/n/a/eq;)Ljava/util/Queue;
aload 0
getfield com/a/b/n/a/eq/c Ljava/util/Queue;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/a/b/n/a/eq;)Z
aload 0
iconst_0
putfield com/a/b/n/a/eq/d Z
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final execute(Ljava/lang/Runnable;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L6
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L11
.catch all from L12 to L13 using L11
iconst_1
istore 2
aload 1
ldc "'r' must not be null."
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/n/a/eq/f Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
getfield com/a/b/n/a/eq/c Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/a/b/n/a/eq/d Z
ifne L14
aload 0
iconst_1
putfield com/a/b/n/a/eq/d Z
L1:
aload 3
monitorexit
L3:
iload 2
ifeq L5
L4:
aload 0
getfield com/a/b/n/a/eq/b Ljava/util/concurrent/Executor;
aload 0
getfield com/a/b/n/a/eq/e Lcom/a/b/n/a/es;
invokeinterface java/util/concurrent/Executor/execute(Ljava/lang/Runnable;)V 1
L5:
return
L2:
astore 1
L7:
aload 3
monitorexit
L8:
aload 1
athrow
L6:
astore 3
aload 0
getfield com/a/b/n/a/eq/f Ljava/lang/Object;
astore 1
aload 1
monitorenter
L9:
aload 0
iconst_0
putfield com/a/b/n/a/eq/d Z
aload 1
monitorexit
L10:
aload 3
athrow
L11:
astore 3
L12:
aload 1
monitorexit
L13:
aload 3
athrow
L14:
iconst_0
istore 2
goto L1
.limit locals 4
.limit stack 2
.end method
