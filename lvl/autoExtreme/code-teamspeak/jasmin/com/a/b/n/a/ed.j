.bytecode 50.0
.class final synchronized com/a/b/n/a/ed
.super java/lang/Object
.annotation invisible Lcom/a/b/a/d;
.end annotation

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ExecutorService;
aload 0
aload 1
ldc2_w 120L
getstatic java/util/concurrent/TimeUnit/SECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/n/a/ed/a(Ljava/util/concurrent/ThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ExecutorService;
areturn
.limit locals 2
.limit stack 5
.end method

.method private a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;)Ljava/util/concurrent/ScheduledExecutorService;
aload 0
aload 1
ldc2_w 120L
getstatic java/util/concurrent/TimeUnit/SECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/n/a/ed/a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledExecutorService;
areturn
.limit locals 2
.limit stack 5
.end method

.method private static a(Ljava/lang/Thread;)V
.annotation invisible Lcom/a/b/a/d;
.end annotation
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
aload 0
invokevirtual java/lang/Runtime/addShutdownHook(Ljava/lang/Thread;)V
return
.limit locals 1
.limit stack 2
.end method

.method final a(Ljava/util/concurrent/ThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ExecutorService;
aload 1
invokestatic com/a/b/n/a/dy/a(Ljava/util/concurrent/ThreadPoolExecutor;)V
aload 1
invokestatic java/util/concurrent/Executors/unconfigurableExecutorService(Ljava/util/concurrent/ExecutorService;)Ljava/util/concurrent/ExecutorService;
astore 1
aload 0
aload 1
lload 2
aload 4
invokevirtual com/a/b/n/a/ed/a(Ljava/util/concurrent/ExecutorService;JLjava/util/concurrent/TimeUnit;)V
aload 1
areturn
.limit locals 5
.limit stack 5
.end method

.method final a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledExecutorService;
aload 1
invokestatic com/a/b/n/a/dy/a(Ljava/util/concurrent/ThreadPoolExecutor;)V
aload 1
invokestatic java/util/concurrent/Executors/unconfigurableScheduledExecutorService(Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;
astore 1
aload 0
aload 1
lload 2
aload 4
invokevirtual com/a/b/n/a/ed/a(Ljava/util/concurrent/ExecutorService;JLjava/util/concurrent/TimeUnit;)V
aload 1
areturn
.limit locals 5
.limit stack 5
.end method

.method final a(Ljava/util/concurrent/ExecutorService;JLjava/util/concurrent/TimeUnit;)V
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 5
new java/lang/StringBuilder
dup
aload 5
invokevirtual java/lang/String/length()I
bipush 24
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "DelayedShutdownHook-for-"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
new com/a/b/n/a/ee
dup
aload 0
aload 1
lload 2
aload 4
invokespecial com/a/b/n/a/ee/<init>(Lcom/a/b/n/a/ed;Ljava/util/concurrent/ExecutorService;JLjava/util/concurrent/TimeUnit;)V
invokestatic com/a/b/n/a/dy/a(Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Thread;
astore 1
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
aload 1
invokevirtual java/lang/Runtime/addShutdownHook(Ljava/lang/Thread;)V
return
.limit locals 6
.limit stack 8
.end method
