.bytecode 50.0
.class public final synchronized com/a/b/n/a/bu
.super java/lang/Object

.field static final 'a' Ljava/util/logging/Logger;
.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.field private 'b' Lcom/a/b/n/a/bv;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "this"
.end annotation
.end field

.field private 'c' Z
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "this"
.end annotation
.end field

.method static <clinit>()V
ldc com/a/b/n/a/bu
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putstatic com/a/b/n/a/bu/a Ljava/util/logging/Logger;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
.catch java/lang/RuntimeException from L0 to L1 using L2
L0:
aload 1
aload 0
invokeinterface java/util/concurrent/Executor/execute(Ljava/lang/Runnable;)V 1
L1:
return
L2:
astore 2
getstatic com/a/b/n/a/bu/a Ljava/util/logging/Logger;
astore 3
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
astore 4
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 3
aload 4
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 57
iadd
aload 1
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "RuntimeException while executing runnable "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " with executor "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 5
.limit stack 6
.end method

.method public final a()V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aconst_null
astore 1
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/n/a/bu/c Z
ifeq L3
aload 0
monitorexit
L1:
return
L3:
aload 0
iconst_1
putfield com/a/b/n/a/bu/c Z
aload 0
getfield com/a/b/n/a/bu/b Lcom/a/b/n/a/bv;
astore 2
aload 0
aconst_null
putfield com/a/b/n/a/bu/b Lcom/a/b/n/a/bv;
aload 0
monitorexit
L4:
aload 1
astore 3
aload 2
ifnull L7
aload 2
getfield com/a/b/n/a/bv/c Lcom/a/b/n/a/bv;
astore 3
aload 2
aload 1
putfield com/a/b/n/a/bv/c Lcom/a/b/n/a/bv;
aload 2
astore 1
aload 3
astore 2
goto L4
L2:
astore 1
L5:
aload 0
monitorexit
L6:
aload 1
athrow
L7:
aload 3
ifnull L8
aload 3
getfield com/a/b/n/a/bv/a Ljava/lang/Runnable;
aload 3
getfield com/a/b/n/a/bv/b Ljava/util/concurrent/Executor;
invokestatic com/a/b/n/a/bu/b(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
aload 3
getfield com/a/b/n/a/bv/c Lcom/a/b/n/a/bv;
astore 3
goto L7
L8:
return
.limit locals 4
.limit stack 2
.end method

.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aload 1
ldc "Runnable was null."
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
ldc "Executor was null."
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/n/a/bu/c Z
ifne L3
aload 0
new com/a/b/n/a/bv
dup
aload 1
aload 2
aload 0
getfield com/a/b/n/a/bu/b Lcom/a/b/n/a/bv;
invokespecial com/a/b/n/a/bv/<init>(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;Lcom/a/b/n/a/bv;)V
putfield com/a/b/n/a/bu/b Lcom/a/b/n/a/bv;
aload 0
monitorexit
L1:
return
L3:
aload 0
monitorexit
L4:
aload 1
aload 2
invokestatic com/a/b/n/a/bu/b(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
return
L2:
astore 1
L5:
aload 0
monitorexit
L6:
aload 1
athrow
.limit locals 3
.limit stack 6
.end method
