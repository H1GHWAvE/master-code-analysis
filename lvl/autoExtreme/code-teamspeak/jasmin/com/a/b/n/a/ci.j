.bytecode 50.0
.class public final synchronized com/a/b/n/a/ci
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'a' Lcom/a/b/n/a/ap;

.field private static final 'b' Lcom/a/b/d/yd;

.method static <clinit>()V
new com/a/b/n/a/cn
dup
invokespecial com/a/b/n/a/cn/<init>()V
putstatic com/a/b/n/a/ci/a Lcom/a/b/n/a/ap;
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
new com/a/b/n/a/cq
dup
invokespecial com/a/b/n/a/cq/<init>()V
invokevirtual com/a/b/d/yd/a(Lcom/a/b/b/bj;)Lcom/a/b/d/yd;
invokevirtual com/a/b/d/yd/a()Lcom/a/b/d/yd;
putstatic com/a/b/n/a/ci/b Lcom/a/b/d/yd;
return
.limit locals 0
.limit stack 3
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Lcom/a/b/b/bj;)Lcom/a/b/n/a/ap;
new com/a/b/n/a/cl
dup
aload 0
invokespecial com/a/b/n/a/cl/<init>(Lcom/a/b/b/bj;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/Exception;)Lcom/a/b/n/a/bc;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/n/a/dd
dup
aload 0
invokespecial com/a/b/n/a/dd/<init>(Ljava/lang/Exception;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a()Lcom/a/b/n/a/dp;
new com/a/b/n/a/dc
dup
invokespecial com/a/b/n/a/dc/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static a(Lcom/a/b/d/jl;ZLjava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
new com/a/b/n/a/cu
dup
aload 0
iload 1
aload 2
new com/a/b/n/a/cr
dup
invokespecial com/a/b/n/a/cr/<init>()V
invokespecial com/a/b/n/a/cu/<init>(Lcom/a/b/d/iz;ZLjava/util/concurrent/Executor;Lcom/a/b/n/a/db;)V
areturn
.limit locals 3
.limit stack 7
.end method

.method public static a(Lcom/a/b/n/a/dp;)Lcom/a/b/n/a/dp;
new com/a/b/n/a/cs
dup
getstatic com/a/b/n/a/ci/a Lcom/a/b/n/a/ap;
aload 0
iconst_0
invokespecial com/a/b/n/a/cs/<init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;B)V
astore 1
aload 0
aload 1
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokeinterface com/a/b/n/a/dp/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V 2
aload 1
areturn
.limit locals 2
.limit stack 5
.end method

.method public static a(Lcom/a/b/n/a/dp;Lcom/a/b/b/bj;)Lcom/a/b/n/a/dp;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/n/a/cs
dup
aload 1
invokestatic com/a/b/n/a/ci/a(Lcom/a/b/b/bj;)Lcom/a/b/n/a/ap;
aload 0
iconst_0
invokespecial com/a/b/n/a/cs/<init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;B)V
astore 1
aload 0
aload 1
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokeinterface com/a/b/n/a/dp/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V 2
aload 1
areturn
.limit locals 2
.limit stack 5
.end method

.method private static a(Lcom/a/b/n/a/dp;Lcom/a/b/b/bj;Ljava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/n/a/ci/a(Lcom/a/b/b/bj;)Lcom/a/b/n/a/ap;
astore 1
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/n/a/cs
dup
aload 1
aload 0
iconst_0
invokespecial com/a/b/n/a/cs/<init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;B)V
astore 1
aload 0
new com/a/b/n/a/cj
dup
aload 2
aload 1
aload 1
invokespecial com/a/b/n/a/cj/<init>(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;Lcom/a/b/n/a/g;)V
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokeinterface com/a/b/n/a/dp/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V 2
aload 1
areturn
.limit locals 3
.limit stack 6
.end method

.method private static a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/ap;)Lcom/a/b/n/a/dp;
new com/a/b/n/a/cs
dup
aload 1
aload 0
iconst_0
invokespecial com/a/b/n/a/cs/<init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;B)V
astore 1
aload 0
aload 1
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokeinterface com/a/b/n/a/dp/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V 2
aload 1
areturn
.limit locals 2
.limit stack 5
.end method

.method private static a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/ap;Ljava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/n/a/cs
dup
aload 1
aload 0
iconst_0
invokespecial com/a/b/n/a/cs/<init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;B)V
astore 1
aload 0
new com/a/b/n/a/cj
dup
aload 2
aload 1
aload 1
invokespecial com/a/b/n/a/cj/<init>(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;Lcom/a/b/n/a/g;)V
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokeinterface com/a/b/n/a/dp/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V 2
aload 1
areturn
.limit locals 3
.limit stack 6
.end method

.method private static a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/ch;)Lcom/a/b/n/a/dp;
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
astore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/n/a/cy
dup
aload 0
aload 1
aload 2
invokespecial com/a/b/n/a/cy/<init>(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/ch;Ljava/util/concurrent/Executor;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method private static a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/ch;Ljava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/n/a/cy
dup
aload 0
aload 1
aload 2
invokespecial com/a/b/n/a/cy/<init>(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/ch;Ljava/util/concurrent/Executor;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/n/a/dp;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokestatic com/a/b/d/jl/a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
iconst_1
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokestatic com/a/b/n/a/ci/a(Lcom/a/b/d/jl;ZLjava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/lang/Object;)Lcom/a/b/n/a/dp;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/n/a/dh
dup
aload 0
invokespecial com/a/b/n/a/dh/<init>(Ljava/lang/Object;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/lang/Throwable;)Lcom/a/b/n/a/dp;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/n/a/de
dup
aload 0
invokespecial com/a/b/n/a/de/<init>(Ljava/lang/Throwable;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static transient a([Lcom/a/b/n/a/dp;)Lcom/a/b/n/a/dp;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokestatic com/a/b/d/jl/a([Ljava/lang/Object;)Lcom/a/b/d/jl;
iconst_1
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokestatic com/a/b/n/a/ci/a(Lcom/a/b/d/jl;ZLjava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Throwable;)Ljava/lang/Exception;
aload 0
invokevirtual java/lang/Class/getConstructors()[Ljava/lang/reflect/Constructor;
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
astore 2
getstatic com/a/b/n/a/ci/b Lcom/a/b/d/yd;
aload 2
invokevirtual com/a/b/d/yd/a(Ljava/lang/Iterable;)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/reflect/Constructor
aload 1
invokestatic com/a/b/n/a/ci/a(Ljava/lang/reflect/Constructor;Ljava/lang/Throwable;)Ljava/lang/Object;
checkcast java/lang/Exception
astore 3
aload 3
ifnull L0
aload 3
invokevirtual java/lang/Exception/getCause()Ljava/lang/Throwable;
ifnonnull L2
aload 3
aload 1
invokevirtual java/lang/Exception/initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
pop
L2:
aload 3
areturn
L1:
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 82
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "No appropriate constructor for exception of type "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " in response to chained exception"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 6
.end method

.method private static a(Ljava/lang/reflect/Constructor;Ljava/lang/Throwable;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
.catch java/lang/InstantiationException from L0 to L1 using L3
.catch java/lang/IllegalAccessException from L0 to L1 using L4
.catch java/lang/reflect/InvocationTargetException from L0 to L1 using L5
aload 0
invokevirtual java/lang/reflect/Constructor/getParameterTypes()[Ljava/lang/Class;
astore 3
aload 3
arraylength
anewarray java/lang/Object
astore 4
iconst_0
istore 2
L6:
iload 2
aload 3
arraylength
if_icmpge L0
aload 3
iload 2
aaload
astore 5
aload 5
ldc java/lang/String
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L7
aload 4
iload 2
aload 1
invokevirtual java/lang/Throwable/toString()Ljava/lang/String;
aastore
L8:
iload 2
iconst_1
iadd
istore 2
goto L6
L7:
aload 5
ldc java/lang/Throwable
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L9
aload 4
iload 2
aload 1
aastore
goto L8
L9:
aconst_null
areturn
L0:
aload 0
aload 4
invokevirtual java/lang/reflect/Constructor/newInstance([Ljava/lang/Object;)Ljava/lang/Object;
astore 0
L1:
aload 0
areturn
L2:
astore 0
aconst_null
areturn
L3:
astore 0
aconst_null
areturn
L4:
astore 0
aconst_null
areturn
L5:
astore 0
aconst_null
areturn
.limit locals 6
.limit stack 3
.end method

.method private static a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
.catch java/util/concurrent/ExecutionException from L0 to L1 using L2
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
aload 0
invokestatic com/a/b/n/a/gs/a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
astore 0
L1:
aload 0
areturn
L2:
astore 0
aload 0
invokevirtual java/util/concurrent/ExecutionException/getCause()Ljava/lang/Throwable;
astore 0
aload 0
instanceof java/lang/Error
ifeq L3
new com/a/b/n/a/bt
dup
aload 0
checkcast java/lang/Error
invokespecial com/a/b/n/a/bt/<init>(Ljava/lang/Error;)V
athrow
L3:
new com/a/b/n/a/gq
dup
aload 0
invokespecial com/a/b/n/a/gq/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;Ljava/lang/Class;)Ljava/lang/Object;
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch java/util/concurrent/TimeoutException from L0 to L1 using L3
.catch java/util/concurrent/ExecutionException from L0 to L1 using L4
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
ldc java/lang/RuntimeException
aload 4
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifne L5
iconst_1
istore 5
L6:
iload 5
ldc "Futures.get exception type (%s) must not be a RuntimeException"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 4
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
L0:
aload 0
lload 1
aload 3
invokeinterface java/util/concurrent/Future/get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 3
astore 0
L1:
aload 0
areturn
L5:
iconst_0
istore 5
goto L6
L2:
astore 0
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
aload 4
aload 0
invokestatic com/a/b/n/a/ci/a(Ljava/lang/Class;Ljava/lang/Throwable;)Ljava/lang/Exception;
athrow
L3:
astore 0
aload 4
aload 0
invokestatic com/a/b/n/a/ci/a(Ljava/lang/Class;Ljava/lang/Throwable;)Ljava/lang/Exception;
athrow
L4:
astore 0
aload 0
invokevirtual java/util/concurrent/ExecutionException/getCause()Ljava/lang/Throwable;
aload 4
invokestatic com/a/b/n/a/ci/a(Ljava/lang/Throwable;Ljava/lang/Class;)V
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
.limit locals 6
.limit stack 6
.end method

.method private static a(Ljava/util/concurrent/Future;Ljava/lang/Class;)Ljava/lang/Object;
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch java/util/concurrent/ExecutionException from L0 to L1 using L3
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
ldc java/lang/RuntimeException
aload 1
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifne L4
iconst_1
istore 2
L5:
iload 2
ldc "Futures.get exception type (%s) must not be a RuntimeException"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
L0:
aload 0
invokeinterface java/util/concurrent/Future/get()Ljava/lang/Object; 0
astore 0
L1:
aload 0
areturn
L4:
iconst_0
istore 2
goto L5
L2:
astore 0
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
aload 1
aload 0
invokestatic com/a/b/n/a/ci/a(Ljava/lang/Class;Ljava/lang/Throwable;)Ljava/lang/Exception;
athrow
L3:
astore 0
aload 0
invokevirtual java/util/concurrent/ExecutionException/getCause()Ljava/lang/Throwable;
aload 1
invokestatic com/a/b/n/a/ci/a(Ljava/lang/Throwable;Ljava/lang/Class;)V
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
.limit locals 3
.limit stack 6
.end method

.method private static a(Lcom/a/b/n/a/g;Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)Ljava/lang/Runnable;
new com/a/b/n/a/cj
dup
aload 2
aload 1
aload 0
invokespecial com/a/b/n/a/cj/<init>(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;Lcom/a/b/n/a/g;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
getstatic com/a/b/n/a/ci/b Lcom/a/b/d/yd;
aload 0
invokevirtual com/a/b/d/yd/a(Ljava/lang/Iterable;)Ljava/util/List;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/util/concurrent/Future;Lcom/a/b/b/bj;)Ljava/util/concurrent/Future;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/n/a/cm
dup
aload 0
aload 1
invokespecial com/a/b/n/a/cm/<init>(Ljava/util/concurrent/Future;Lcom/a/b/b/bj;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/cg;)V
aload 0
aload 1
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokestatic com/a/b/n/a/ci/a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/cg;Ljava/util/concurrent/Executor;)V
return
.limit locals 2
.limit stack 3
.end method

.method public static a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/cg;Ljava/util/concurrent/Executor;)V
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
new com/a/b/n/a/cp
dup
aload 0
aload 1
invokespecial com/a/b/n/a/cp/<init>(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/cg;)V
aload 2
invokeinterface com/a/b/n/a/dp/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V 2
return
.limit locals 3
.limit stack 5
.end method

.method private static a(Ljava/lang/Throwable;Ljava/lang/Class;)V
aload 0
instanceof java/lang/Error
ifeq L0
new com/a/b/n/a/bt
dup
aload 0
checkcast java/lang/Error
invokespecial com/a/b/n/a/bt/<init>(Ljava/lang/Error;)V
athrow
L0:
aload 0
instanceof java/lang/RuntimeException
ifeq L1
new com/a/b/n/a/gq
dup
aload 0
invokespecial com/a/b/n/a/gq/<init>(Ljava/lang/Throwable;)V
athrow
L1:
aload 1
aload 0
invokestatic com/a/b/n/a/ci/a(Ljava/lang/Class;Ljava/lang/Throwable;)Ljava/lang/Exception;
athrow
.limit locals 2
.limit stack 3
.end method

.method private static b(Lcom/a/b/n/a/dp;Lcom/a/b/b/bj;)Lcom/a/b/n/a/bc;
new com/a/b/n/a/di
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/n/a/dp
aload 1
invokespecial com/a/b/n/a/di/<init>(Lcom/a/b/n/a/dp;Lcom/a/b/b/bj;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static b(Ljava/lang/Object;)Lcom/a/b/n/a/bc;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/n/a/dg
dup
aload 0
invokespecial com/a/b/n/a/dg/<init>(Ljava/lang/Object;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b(Lcom/a/b/n/a/dp;)Lcom/a/b/n/a/dp;
new com/a/b/n/a/dj
dup
aload 0
invokespecial com/a/b/n/a/dj/<init>(Lcom/a/b/n/a/dp;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b(Ljava/lang/Iterable;)Lcom/a/b/n/a/dp;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokestatic com/a/b/d/jl/a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
iconst_0
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokestatic com/a/b/n/a/ci/a(Lcom/a/b/d/jl;ZLjava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
areturn
.limit locals 1
.limit stack 3
.end method

.method private static transient b([Lcom/a/b/n/a/dp;)Lcom/a/b/n/a/dp;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokestatic com/a/b/d/jl/a([Ljava/lang/Object;)Lcom/a/b/d/jl;
iconst_0
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokestatic com/a/b/n/a/ci/a(Lcom/a/b/d/jl;ZLjava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b(Ljava/lang/Throwable;)V
aload 0
instanceof java/lang/Error
ifeq L0
new com/a/b/n/a/bt
dup
aload 0
checkcast java/lang/Error
invokespecial com/a/b/n/a/bt/<init>(Ljava/lang/Error;)V
athrow
L0:
new com/a/b/n/a/gq
dup
aload 0
invokespecial com/a/b/n/a/gq/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method private static c(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new java/util/concurrent/ConcurrentLinkedQueue
dup
invokespecial java/util/concurrent/ConcurrentLinkedQueue/<init>()V
astore 1
invokestatic com/a/b/d/jl/h()Lcom/a/b/d/jn;
astore 2
new com/a/b/n/a/eq
dup
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokespecial com/a/b/n/a/eq/<init>(Ljava/util/concurrent/Executor;)V
astore 3
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/n/a/dp
astore 4
invokestatic com/a/b/n/a/aq/a()Lcom/a/b/n/a/aq;
astore 5
aload 1
aload 5
invokevirtual java/util/concurrent/ConcurrentLinkedQueue/add(Ljava/lang/Object;)Z
pop
aload 4
new com/a/b/n/a/co
dup
aload 1
aload 4
invokespecial com/a/b/n/a/co/<init>(Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/a/b/n/a/dp;)V
aload 3
invokeinterface com/a/b/n/a/dp/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V 2
aload 2
aload 5
invokevirtual com/a/b/d/jn/c(Ljava/lang/Object;)Lcom/a/b/d/jn;
pop
goto L0
L1:
aload 2
invokevirtual com/a/b/d/jn/b()Lcom/a/b/d/jl;
areturn
.limit locals 6
.limit stack 5
.end method
