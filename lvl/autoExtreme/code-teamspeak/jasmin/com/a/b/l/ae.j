.bytecode 50.0
.class public final synchronized com/a/b/l/ae
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field public static final 'a' B = 64


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(J)B
lload 0
l2i
i2b
istore 2
iload 2
i2l
lload 0
lcmp
ifeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 34
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Out of range: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 0
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 2
ireturn
.limit locals 3
.limit stack 5
.end method

.method private static transient a([B)B
iconst_1
istore 3
aload 0
arraylength
ifle L0
iconst_1
istore 4
L1:
iload 4
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
baload
istore 1
L2:
iload 3
aload 0
arraylength
if_icmpge L3
iload 1
istore 2
aload 0
iload 3
baload
iload 1
if_icmpge L4
aload 0
iload 3
baload
istore 2
L4:
iload 3
iconst_1
iadd
istore 3
iload 2
istore 1
goto L2
L0:
iconst_0
istore 4
goto L1
L3:
iload 1
ireturn
.limit locals 5
.limit stack 2
.end method

.method private static a(BB)I
iload 0
iload 1
isub
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static transient a(Ljava/lang/String;[B)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
ldc ""
areturn
L0:
new java/lang/StringBuilder
dup
aload 1
arraylength
iconst_5
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 3
aload 3
aload 1
iconst_0
baload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
iconst_1
istore 2
L1:
iload 2
aload 1
arraylength
if_icmpge L2
aload 3
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iload 2
baload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a()Ljava/util/Comparator;
getstatic com/a/b/l/af/a Lcom/a/b/l/af;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static b(J)B
lload 0
ldc2_w 127L
lcmp
ifle L0
bipush 127
ireturn
L0:
lload 0
ldc2_w -128L
lcmp
ifge L1
bipush -128
ireturn
L1:
lload 0
l2i
i2b
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static transient b([B)B
iconst_1
istore 3
aload 0
arraylength
ifle L0
iconst_1
istore 4
L1:
iload 4
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
baload
istore 1
L2:
iload 3
aload 0
arraylength
if_icmpge L3
iload 1
istore 2
aload 0
iload 3
baload
iload 1
if_icmple L4
aload 0
iload 3
baload
istore 2
L4:
iload 3
iconst_1
iadd
istore 3
iload 2
istore 1
goto L2
L0:
iconst_0
istore 4
goto L1
L3:
iload 1
ireturn
.limit locals 5
.limit stack 2
.end method
