.bytecode 50.0
.class public final synchronized com/a/b/l/al
.super java/lang/Number
.implements java/lang/Comparable
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field public static final 'a' Lcom/a/b/l/al;

.field public static final 'b' Lcom/a/b/l/al;

.field public static final 'c' Lcom/a/b/l/al;

.field private final 'd' I

.method static <clinit>()V
iconst_0
invokestatic com/a/b/l/al/a(I)Lcom/a/b/l/al;
putstatic com/a/b/l/al/a Lcom/a/b/l/al;
iconst_1
invokestatic com/a/b/l/al/a(I)Lcom/a/b/l/al;
putstatic com/a/b/l/al/b Lcom/a/b/l/al;
iconst_m1
invokestatic com/a/b/l/al/a(I)Lcom/a/b/l/al;
putstatic com/a/b/l/al/c Lcom/a/b/l/al;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>(I)V
aload 0
invokespecial java/lang/Number/<init>()V
aload 0
iload 1
iconst_m1
iand
putfield com/a/b/l/al/d I
return
.limit locals 2
.limit stack 3
.end method

.method private static a(I)Lcom/a/b/l/al;
new com/a/b/l/al
dup
iload 0
invokespecial com/a/b/l/al/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(J)Lcom/a/b/l/al;
ldc2_w 4294967295L
lload 0
land
lload 0
lcmp
ifne L0
iconst_1
istore 2
L1:
iload 2
ldc "value (%s) is outside the range for an unsigned integer value"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
lload 0
l2i
invokestatic com/a/b/l/al/a(I)Lcom/a/b/l/al;
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 7
.end method

.method private a(Lcom/a/b/l/al;)Lcom/a/b/l/al;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 0
getfield com/a/b/l/al/d I
istore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/l/al
getfield com/a/b/l/al/d I
iload 2
iadd
invokestatic com/a/b/l/al/a(I)Lcom/a/b/l/al;
areturn
.limit locals 3
.limit stack 2
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/l/al;
aload 0
bipush 10
invokestatic com/a/b/l/am/a(Ljava/lang/String;I)I
invokestatic com/a/b/l/al/a(I)Lcom/a/b/l/al;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/math/BigInteger;)Lcom/a/b/l/al;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual java/math/BigInteger/signum()I
iflt L0
aload 0
invokevirtual java/math/BigInteger/bitLength()I
bipush 32
if_icmpgt L0
iconst_1
istore 1
L1:
iload 1
ldc "value (%s) is outside the range for an unsigned integer value"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/math/BigInteger/intValue()I
invokestatic com/a/b/l/al/a(I)Lcom/a/b/l/al;
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method private a()Ljava/math/BigInteger;
aload 0
invokevirtual com/a/b/l/al/longValue()J
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
areturn
.limit locals 1
.limit stack 2
.end method

.method private b(Lcom/a/b/l/al;)Lcom/a/b/l/al;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 0
getfield com/a/b/l/al/d I
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/l/al
getfield com/a/b/l/al/d I
isub
invokestatic com/a/b/l/al/a(I)Lcom/a/b/l/al;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/String;)Lcom/a/b/l/al;
aload 0
bipush 10
invokestatic com/a/b/l/am/a(Ljava/lang/String;I)I
invokestatic com/a/b/l/al/a(I)Lcom/a/b/l/al;
areturn
.limit locals 1
.limit stack 2
.end method

.method private b()Ljava/lang/String;
aload 0
getfield com/a/b/l/al/d I
invokestatic com/a/b/l/am/a(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(Lcom/a/b/l/al;)Lcom/a/b/l/al;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
.annotation invisible Lcom/a/b/a/c;
a s = "Does not truncate correctly"
.end annotation
aload 0
getfield com/a/b/l/al/d I
istore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/l/al
getfield com/a/b/l/al/d I
iload 2
imul
invokestatic com/a/b/l/al/a(I)Lcom/a/b/l/al;
areturn
.limit locals 3
.limit stack 2
.end method

.method private d(Lcom/a/b/l/al;)Lcom/a/b/l/al;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 0
getfield com/a/b/l/al/d I
istore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/l/al
getfield com/a/b/l/al/d I
istore 3
iload 2
i2l
ldc2_w 4294967295L
land
iload 3
i2l
ldc2_w 4294967295L
land
ldiv
l2i
invokestatic com/a/b/l/al/a(I)Lcom/a/b/l/al;
areturn
.limit locals 4
.limit stack 6
.end method

.method private e(Lcom/a/b/l/al;)Lcom/a/b/l/al;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 0
getfield com/a/b/l/al/d I
istore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/l/al
getfield com/a/b/l/al/d I
istore 3
iload 2
i2l
ldc2_w 4294967295L
land
iload 3
i2l
ldc2_w 4294967295L
land
lrem
l2i
invokestatic com/a/b/l/al/a(I)Lcom/a/b/l/al;
areturn
.limit locals 4
.limit stack 6
.end method

.method private f(Lcom/a/b/l/al;)I
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/l/al/d I
aload 1
getfield com/a/b/l/al/d I
invokestatic com/a/b/l/am/a(II)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
aload 1
checkcast com/a/b/l/al
astore 1
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/l/al/d I
aload 1
getfield com/a/b/l/al/d I
invokestatic com/a/b/l/am/a(II)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final doubleValue()D
aload 0
invokevirtual com/a/b/l/al/longValue()J
l2d
dreturn
.limit locals 1
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/l/al
ifeq L0
aload 1
checkcast com/a/b/l/al
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/l/al/d I
aload 1
getfield com/a/b/l/al/d I
if_icmpne L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final floatValue()F
aload 0
invokevirtual com/a/b/l/al/longValue()J
l2f
freturn
.limit locals 1
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/l/al/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final intValue()I
aload 0
getfield com/a/b/l/al/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final longValue()J
aload 0
getfield com/a/b/l/al/d I
i2l
ldc2_w 4294967295L
land
lreturn
.limit locals 1
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/l/al/d I
invokestatic com/a/b/l/am/a(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
