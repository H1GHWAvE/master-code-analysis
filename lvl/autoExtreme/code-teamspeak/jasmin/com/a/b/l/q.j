.bytecode 50.0
.class public final synchronized com/a/b/l/q
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field public static final 'a' I = 4


.field public static final 'b' I = 1073741824


.field private static final 'c' [B

.method static <clinit>()V
iconst_0
istore 2
sipush 128
newarray byte
astore 3
aload 3
putstatic com/a/b/l/q/c [B
aload 3
iconst_m1
invokestatic java/util/Arrays/fill([BB)V
iconst_0
istore 0
L0:
iload 2
istore 1
iload 0
bipush 9
if_icmpgt L1
getstatic com/a/b/l/q/c [B
iload 0
bipush 48
iadd
iload 0
i2b
bastore
iload 0
iconst_1
iadd
istore 0
goto L0
L1:
iload 1
bipush 26
if_icmpgt L2
getstatic com/a/b/l/q/c [B
iload 1
bipush 65
iadd
iload 1
bipush 10
iadd
i2b
bastore
getstatic com/a/b/l/q/c [B
iload 1
bipush 97
iadd
iload 1
bipush 10
iadd
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
return
.limit locals 4
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(BBBB)I
.annotation invisible Lcom/a/b/a/c;
a s = "doesn't work"
.end annotation
iload 0
bipush 24
ishl
iload 1
sipush 255
iand
bipush 16
ishl
ior
iload 2
sipush 255
iand
bipush 8
ishl
ior
iload 3
sipush 255
iand
ior
ireturn
.limit locals 4
.limit stack 3
.end method

.method private static a(C)I
iload 0
sipush 128
if_icmpge L0
getstatic com/a/b/l/q/c [B
iload 0
baload
ireturn
L0:
iconst_m1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static a(I)I
iload 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static a(II)I
iload 0
iload 1
if_icmpge L0
iconst_m1
ireturn
L0:
iload 0
iload 1
if_icmple L1
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static a(J)I
lload 0
l2i
istore 2
iload 2
i2l
lload 0
lcmp
ifeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 34
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Out of range: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 0
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 2
ireturn
.limit locals 3
.limit stack 5
.end method

.method private static a([B)I
.annotation invisible Lcom/a/b/a/c;
a s = "doesn't work"
.end annotation
aload 0
arraylength
iconst_4
if_icmplt L0
iconst_1
istore 1
L1:
iload 1
ldc "array too small: %s < %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iconst_4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
iconst_0
baload
aload 0
iconst_1
baload
aload 0
iconst_2
baload
aload 0
iconst_3
baload
invokestatic com/a/b/l/q/a(BBBB)I
ireturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method public static transient a([I)I
iconst_1
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
iaload
istore 2
iconst_1
istore 1
L0:
iload 1
iconst_2
if_icmpge L1
iload 2
istore 3
aload 0
iconst_1
iaload
iload 2
if_icmpge L2
aload 0
iconst_1
iaload
istore 3
L2:
iload 1
iconst_1
iadd
istore 1
iload 3
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method static synthetic a([IIII)I
aload 0
iload 1
iload 2
iload 3
invokestatic com/a/b/l/q/c([IIII)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static a([I[I)I
aload 0
ldc "array"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "target"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
iconst_0
ireturn
L0:
iconst_0
istore 2
L1:
iload 2
aload 0
arraylength
aload 1
arraylength
isub
iconst_1
iadd
if_icmpge L2
iconst_0
istore 3
L3:
iload 3
aload 1
arraylength
if_icmpge L4
aload 0
iload 2
iload 3
iadd
iaload
aload 1
iload 3
iaload
if_icmpne L5
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
iload 2
ireturn
L5:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
iconst_m1
ireturn
.limit locals 4
.limit stack 3
.end method

.method private static a()Lcom/a/b/b/ak;
.annotation invisible Lcom/a/b/a/a;
.end annotation
getstatic com/a/b/l/s/a Lcom/a/b/l/s;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Integer;
.annotation visible Ljavax/annotation/CheckForNull;
.end annotation
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/String/isEmpty()Z
ifeq L0
aconst_null
areturn
L0:
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
bipush 45
if_icmpne L1
iconst_1
istore 1
L2:
iload 1
ifeq L3
iconst_1
istore 2
L4:
iload 2
aload 0
invokevirtual java/lang/String/length()I
if_icmpne L5
aconst_null
areturn
L1:
iconst_0
istore 1
goto L2
L3:
iconst_0
istore 2
goto L4
L5:
iload 2
iconst_1
iadd
istore 3
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
invokestatic com/a/b/l/q/a(C)I
istore 2
iload 2
iflt L6
iload 2
bipush 10
if_icmplt L7
L6:
aconst_null
areturn
L7:
iload 2
ineg
istore 4
iload 3
istore 2
iload 4
istore 3
L8:
iload 2
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L9
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
invokestatic com/a/b/l/q/a(C)I
istore 4
iload 4
iflt L10
iload 4
bipush 10
if_icmpge L10
iload 3
ldc_w -214748364
if_icmpge L11
L10:
aconst_null
areturn
L11:
iload 3
bipush 10
imul
istore 3
iload 3
ldc_w -2147483648
iload 4
iadd
if_icmpge L12
aconst_null
areturn
L12:
iload 3
iload 4
isub
istore 3
iload 2
iconst_1
iadd
istore 2
goto L8
L9:
iload 1
ifeq L13
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
L13:
iload 3
ldc_w -2147483648
if_icmpne L14
aconst_null
areturn
L14:
iload 3
ineg
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
.limit locals 5
.limit stack 3
.end method

.method private static transient a(Ljava/lang/String;[I)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
ldc ""
areturn
L0:
new java/lang/StringBuilder
dup
aload 1
arraylength
iconst_5
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 3
aload 3
aload 1
iconst_0
iaload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
iconst_1
istore 2
L1:
iload 2
aload 1
arraylength
if_icmpge L2
aload 3
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iload 2
iaload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a([II)Z
iconst_0
istore 5
aload 0
arraylength
istore 3
iconst_0
istore 2
L0:
iload 5
istore 4
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
iaload
iload 1
if_icmpne L2
iconst_1
istore 4
L1:
iload 4
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 6
.limit stack 2
.end method

.method private static a(Ljava/util/Collection;)[I
iconst_0
istore 1
aload 0
instanceof com/a/b/l/r
ifeq L0
aload 0
checkcast com/a/b/l/r
astore 0
aload 0
invokevirtual com/a/b/l/r/size()I
istore 1
iload 1
newarray int
astore 3
aload 0
getfield com/a/b/l/r/a [I
aload 0
getfield com/a/b/l/r/b I
aload 3
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
L0:
aload 0
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
astore 0
aload 0
arraylength
istore 2
iload 2
newarray int
astore 3
L1:
iload 1
iload 2
if_icmpge L2
aload 3
iload 1
aload 0
iload 1
aaload
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Number
invokevirtual java/lang/Number/intValue()I
iastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a([III)[I
iload 1
iflt L0
iconst_1
istore 3
L1:
iload 3
ldc "Invalid minLength: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 2
iflt L2
iconst_1
istore 3
L3:
iload 3
ldc "Invalid padding: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
astore 4
aload 0
arraylength
iload 1
if_icmpge L4
iload 1
iload 2
iadd
istore 1
iload 1
newarray int
astore 4
aload 0
iconst_0
aload 4
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L4:
aload 4
areturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 3
goto L3
.limit locals 5
.limit stack 6
.end method

.method private static transient a([[I)[I
aload 0
arraylength
istore 3
iconst_0
istore 1
iconst_0
istore 2
L0:
iload 1
iload 3
if_icmpge L1
iload 2
aload 0
iload 1
aaload
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
newarray int
astore 4
aload 0
arraylength
istore 3
iconst_0
istore 2
iconst_0
istore 1
L2:
iload 1
iload 3
if_icmpge L3
aload 0
iload 1
aaload
astore 5
aload 5
iconst_0
aload 4
iload 2
aload 5
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 2
aload 5
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 4
areturn
.limit locals 6
.limit stack 5
.end method

.method public static b(J)I
lload 0
ldc2_w 2147483647L
lcmp
ifle L0
ldc_w 2147483647
ireturn
L0:
lload 0
ldc2_w -2147483648L
lcmp
ifge L1
ldc_w -2147483648
ireturn
L1:
lload 0
l2i
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static transient b([I)I
iconst_1
istore 1
aload 0
arraylength
ifle L0
iconst_1
istore 4
L1:
iload 4
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
iaload
istore 2
L2:
iload 1
aload 0
arraylength
if_icmpge L3
iload 2
istore 3
aload 0
iload 1
iaload
iload 2
if_icmple L4
aload 0
iload 1
iaload
istore 3
L4:
iload 1
iconst_1
iadd
istore 1
iload 3
istore 2
goto L2
L0:
iconst_0
istore 4
goto L1
L3:
iload 2
ireturn
.limit locals 5
.limit stack 2
.end method

.method private static b([II)I
aload 0
iload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/q/c([IIII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method static synthetic b([IIII)I
aload 0
iload 1
iload 2
iload 3
invokestatic com/a/b/l/q/d([IIII)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/Integer;
.annotation visible Ljavax/annotation/CheckForNull;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/String/isEmpty()Z
ifeq L0
aconst_null
areturn
L0:
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
bipush 45
if_icmpne L1
iconst_1
istore 1
L2:
iload 1
ifeq L3
iconst_1
istore 2
L4:
iload 2
aload 0
invokevirtual java/lang/String/length()I
if_icmpne L5
aconst_null
areturn
L1:
iconst_0
istore 1
goto L2
L3:
iconst_0
istore 2
goto L4
L5:
iload 2
iconst_1
iadd
istore 3
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
invokestatic com/a/b/l/q/a(C)I
istore 2
iload 2
iflt L6
iload 2
bipush 10
if_icmplt L7
L6:
aconst_null
areturn
L7:
iload 2
ineg
istore 4
iload 3
istore 2
iload 4
istore 3
L8:
iload 2
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L9
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
invokestatic com/a/b/l/q/a(C)I
istore 4
iload 4
iflt L10
iload 4
bipush 10
if_icmpge L10
iload 3
ldc_w -214748364
if_icmpge L11
L10:
aconst_null
areturn
L11:
iload 3
bipush 10
imul
istore 3
iload 3
ldc_w -2147483648
iload 4
iadd
if_icmpge L12
aconst_null
areturn
L12:
iload 3
iload 4
isub
istore 3
iload 2
iconst_1
iadd
istore 2
goto L8
L9:
iload 1
ifeq L13
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
L13:
iload 3
ldc_w -2147483648
if_icmpne L14
aconst_null
areturn
L14:
iload 3
ineg
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
.limit locals 5
.limit stack 3
.end method

.method private static b()Ljava/util/Comparator;
getstatic com/a/b/l/t/a Lcom/a/b/l/t;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static b(I)[B
.annotation invisible Lcom/a/b/a/c;
a s = "doesn't work"
.end annotation
iconst_4
newarray byte
dup
iconst_0
iload 0
bipush 24
ishr
i2b
bastore
dup
iconst_1
iload 0
bipush 16
ishr
i2b
bastore
dup
iconst_2
iload 0
bipush 8
ishr
i2b
bastore
dup
iconst_3
iload 0
i2b
bastore
areturn
.limit locals 1
.limit stack 5
.end method

.method private static c([II)I
aload 0
iload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/q/d([IIII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static c([IIII)I
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
iaload
iload 1
if_icmpne L2
iload 2
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static transient c([I)Ljava/util/List;
aload 0
arraylength
ifne L0
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
L0:
new com/a/b/l/r
dup
aload 0
invokespecial com/a/b/l/r/<init>([I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static d([IIII)I
iload 3
iconst_1
isub
istore 3
L0:
iload 3
iload 2
if_icmplt L1
aload 0
iload 3
iaload
iload 1
if_icmpne L2
iload 3
ireturn
L2:
iload 3
iconst_1
isub
istore 3
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static d([II)[I
iload 1
newarray int
astore 2
aload 0
iconst_0
aload 2
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 6
.end method
