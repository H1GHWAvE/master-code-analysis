.bytecode 50.0
.class public final synchronized com/a/b/l/m
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field public static final 'a' I = 4


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static transient a([F)F
iconst_1
istore 2
aload 0
arraylength
ifle L0
iconst_1
istore 3
L1:
iload 3
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
faload
fstore 1
L2:
iload 2
aload 0
arraylength
if_icmpge L3
fload 1
aload 0
iload 2
faload
invokestatic java/lang/Math/min(FF)F
fstore 1
iload 2
iconst_1
iadd
istore 2
goto L2
L0:
iconst_0
istore 3
goto L1
L3:
fload 1
freturn
.limit locals 4
.limit stack 3
.end method

.method private static a(F)I
fload 0
invokestatic java/lang/Float/valueOf(F)Ljava/lang/Float;
invokevirtual java/lang/Float/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a(FF)I
fload 0
fload 1
invokestatic java/lang/Float/compare(FF)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method static a([FFII)I
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
faload
fload 1
fcmpl
ifne L2
iload 2
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static a([F[F)I
aload 0
ldc "array"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "target"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
iconst_0
ireturn
L0:
iconst_0
istore 2
L1:
iload 2
aload 0
arraylength
aload 1
arraylength
isub
iconst_1
iadd
if_icmpge L2
iconst_0
istore 3
L3:
iload 3
aload 1
arraylength
if_icmpge L4
aload 0
iload 2
iload 3
iadd
faload
aload 1
iload 3
faload
fcmpl
ifne L5
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
iload 2
ireturn
L5:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
iconst_m1
ireturn
.limit locals 4
.limit stack 3
.end method

.method private static a()Lcom/a/b/b/ak;
.annotation invisible Lcom/a/b/a/a;
.end annotation
getstatic com/a/b/l/o/a Lcom/a/b/l/o;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Float;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/c;
a s = "regular expressions"
.end annotation
.catch java/lang/NumberFormatException from L0 to L1 using L2
getstatic com/a/b/l/i/b Ljava/util/regex/Pattern;
aload 0
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
invokevirtual java/util/regex/Matcher/matches()Z
ifeq L3
L0:
aload 0
invokestatic java/lang/Float/parseFloat(Ljava/lang/String;)F
fstore 1
L1:
fload 1
invokestatic java/lang/Float/valueOf(F)Ljava/lang/Float;
areturn
L2:
astore 0
L3:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method private static transient a(Ljava/lang/String;[F)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
ldc ""
areturn
L0:
new java/lang/StringBuilder
dup
aload 1
arraylength
bipush 12
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 3
aload 3
aload 1
iconst_0
faload
invokevirtual java/lang/StringBuilder/append(F)Ljava/lang/StringBuilder;
pop
iconst_1
istore 2
L1:
iload 2
aload 1
arraylength
if_icmpge L2
aload 3
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iload 2
faload
invokevirtual java/lang/StringBuilder/append(F)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a([FF)Z
iconst_0
istore 5
aload 0
arraylength
istore 3
iconst_0
istore 2
L0:
iload 5
istore 4
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
faload
fload 1
fcmpl
ifne L2
iconst_1
istore 4
L1:
iload 4
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 6
.limit stack 2
.end method

.method private static a(Ljava/util/Collection;)[F
iconst_0
istore 1
aload 0
instanceof com/a/b/l/n
ifeq L0
aload 0
checkcast com/a/b/l/n
astore 0
aload 0
invokevirtual com/a/b/l/n/size()I
istore 1
iload 1
newarray float
astore 3
aload 0
getfield com/a/b/l/n/a [F
aload 0
getfield com/a/b/l/n/b I
aload 3
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
L0:
aload 0
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
astore 0
aload 0
arraylength
istore 2
iload 2
newarray float
astore 3
L1:
iload 1
iload 2
if_icmpge L2
aload 3
iload 1
aload 0
iload 1
aaload
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Number
invokevirtual java/lang/Number/floatValue()F
fastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a([FI)[F
iload 1
newarray float
astore 2
aload 0
iconst_0
aload 2
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 6
.end method

.method private static a([FII)[F
iload 1
iflt L0
iconst_1
istore 3
L1:
iload 3
ldc "Invalid minLength: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 2
iflt L2
iconst_1
istore 3
L3:
iload 3
ldc "Invalid padding: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
astore 4
aload 0
arraylength
iload 1
if_icmpge L4
iload 1
iload 2
iadd
istore 1
iload 1
newarray float
astore 4
aload 0
iconst_0
aload 4
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L4:
aload 4
areturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 3
goto L3
.limit locals 5
.limit stack 6
.end method

.method private static transient a([[F)[F
aload 0
arraylength
istore 3
iconst_0
istore 1
iconst_0
istore 2
L0:
iload 1
iload 3
if_icmpge L1
iload 2
aload 0
iload 1
aaload
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
newarray float
astore 4
aload 0
arraylength
istore 3
iconst_0
istore 2
iconst_0
istore 1
L2:
iload 1
iload 3
if_icmpge L3
aload 0
iload 1
aaload
astore 5
aload 5
iconst_0
aload 4
iload 2
aload 5
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 2
aload 5
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 4
areturn
.limit locals 6
.limit stack 5
.end method

.method private static transient b([F)F
iconst_1
istore 2
aload 0
arraylength
ifle L0
iconst_1
istore 3
L1:
iload 3
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
faload
fstore 1
L2:
iload 2
aload 0
arraylength
if_icmpge L3
fload 1
aload 0
iload 2
faload
invokestatic java/lang/Math/max(FF)F
fstore 1
iload 2
iconst_1
iadd
istore 2
goto L2
L0:
iconst_0
istore 3
goto L1
L3:
fload 1
freturn
.limit locals 4
.limit stack 3
.end method

.method private static b([FF)I
aload 0
fload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/m/a([FFII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method static b([FFII)I
iload 3
iconst_1
isub
istore 3
L0:
iload 3
iload 2
if_icmplt L1
aload 0
iload 3
faload
fload 1
fcmpl
ifne L2
iload 3
ireturn
L2:
iload 3
iconst_1
isub
istore 3
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static b()Ljava/util/Comparator;
getstatic com/a/b/l/p/a Lcom/a/b/l/p;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static b(F)Z
iconst_1
istore 2
ldc_w -floatinfinity
fload 0
fcmpg
ifge L0
iconst_1
istore 1
L1:
fload 0
ldc_w +floatinfinity
fcmpg
ifge L2
L3:
iload 2
iload 1
iand
ireturn
L0:
iconst_0
istore 1
goto L1
L2:
iconst_0
istore 2
goto L3
.limit locals 3
.limit stack 2
.end method

.method private static c([FF)I
aload 0
fload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/m/b([FFII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static synthetic c([FFII)I
aload 0
fload 1
iload 2
iload 3
invokestatic com/a/b/l/m/a([FFII)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static transient c([F)Ljava/util/List;
aload 0
arraylength
ifne L0
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
L0:
new com/a/b/l/n
dup
aload 0
invokespecial com/a/b/l/n/<init>([F)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static synthetic d([FFII)I
aload 0
fload 1
iload 2
iload 3
invokestatic com/a/b/l/m/b([FFII)I
ireturn
.limit locals 4
.limit stack 4
.end method
