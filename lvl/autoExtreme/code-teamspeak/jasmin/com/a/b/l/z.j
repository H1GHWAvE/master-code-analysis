.bytecode 50.0
.class public final synchronized com/a/b/l/z
.super java/lang/Object

.field private static final 'a' Ljava/util/Map;

.field private static final 'b' Ljava/util/Map;

.method static <clinit>()V
new java/util/HashMap
dup
bipush 16
invokespecial java/util/HashMap/<init>(I)V
astore 0
new java/util/HashMap
dup
bipush 16
invokespecial java/util/HashMap/<init>(I)V
astore 1
aload 0
aload 1
getstatic java/lang/Boolean/TYPE Ljava/lang/Class;
ldc java/lang/Boolean
invokestatic com/a/b/l/z/a(Ljava/util/Map;Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Class;)V
aload 0
aload 1
getstatic java/lang/Byte/TYPE Ljava/lang/Class;
ldc java/lang/Byte
invokestatic com/a/b/l/z/a(Ljava/util/Map;Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Class;)V
aload 0
aload 1
getstatic java/lang/Character/TYPE Ljava/lang/Class;
ldc java/lang/Character
invokestatic com/a/b/l/z/a(Ljava/util/Map;Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Class;)V
aload 0
aload 1
getstatic java/lang/Double/TYPE Ljava/lang/Class;
ldc java/lang/Double
invokestatic com/a/b/l/z/a(Ljava/util/Map;Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Class;)V
aload 0
aload 1
getstatic java/lang/Float/TYPE Ljava/lang/Class;
ldc java/lang/Float
invokestatic com/a/b/l/z/a(Ljava/util/Map;Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Class;)V
aload 0
aload 1
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
ldc java/lang/Integer
invokestatic com/a/b/l/z/a(Ljava/util/Map;Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Class;)V
aload 0
aload 1
getstatic java/lang/Long/TYPE Ljava/lang/Class;
ldc java/lang/Long
invokestatic com/a/b/l/z/a(Ljava/util/Map;Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Class;)V
aload 0
aload 1
getstatic java/lang/Short/TYPE Ljava/lang/Class;
ldc java/lang/Short
invokestatic com/a/b/l/z/a(Ljava/util/Map;Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Class;)V
aload 0
aload 1
getstatic java/lang/Void/TYPE Ljava/lang/Class;
ldc java/lang/Void
invokestatic com/a/b/l/z/a(Ljava/util/Map;Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Class;)V
aload 0
invokestatic java/util/Collections/unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
putstatic com/a/b/l/z/a Ljava/util/Map;
aload 1
invokestatic java/util/Collections/unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
putstatic com/a/b/l/z/b Ljava/util/Map;
return
.limit locals 2
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/Class;)Ljava/lang/Class;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/a/b/l/z/a Ljava/util/Map;
aload 0
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/Class
astore 1
aload 1
ifnonnull L0
aload 0
areturn
L0:
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public static a()Ljava/util/Set;
getstatic com/a/b/l/z/b Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Ljava/util/Map;Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Class;)V
aload 0
aload 2
aload 3
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 1
aload 3
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 4
.limit stack 3
.end method

.method public static b(Ljava/lang/Class;)Ljava/lang/Class;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/a/b/l/z/b Ljava/util/Map;
aload 0
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/Class
astore 1
aload 1
ifnonnull L0
aload 0
areturn
L0:
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b()Ljava/util/Set;
getstatic com/a/b/l/z/a Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
areturn
.limit locals 0
.limit stack 1
.end method

.method private static c(Ljava/lang/Class;)Z
getstatic com/a/b/l/z/b Ljava/util/Map;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method
