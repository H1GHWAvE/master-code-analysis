.bytecode 50.0
.class final synchronized com/a/b/l/w
.super java/util/AbstractList
.implements java/io/Serializable
.implements java/util/RandomAccess
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private static final 'd' J = 0L


.field final 'a' [J

.field final 'b' I

.field final 'c' I

.method <init>([J)V
aload 0
aload 1
iconst_0
aload 1
arraylength
invokespecial com/a/b/l/w/<init>([JII)V
return
.limit locals 2
.limit stack 4
.end method

.method private <init>([JII)V
aload 0
invokespecial java/util/AbstractList/<init>()V
aload 0
aload 1
putfield com/a/b/l/w/a [J
aload 0
iload 2
putfield com/a/b/l/w/b I
aload 0
iload 3
putfield com/a/b/l/w/c I
return
.limit locals 4
.limit stack 2
.end method

.method private a(I)Ljava/lang/Long;
iload 1
aload 0
invokevirtual com/a/b/l/w/size()I
invokestatic com/a/b/b/cn/a(II)I
pop
aload 0
getfield com/a/b/l/w/a [J
aload 0
getfield com/a/b/l/w/b I
iload 1
iadd
laload
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(ILjava/lang/Long;)Ljava/lang/Long;
iload 1
aload 0
invokevirtual com/a/b/l/w/size()I
invokestatic com/a/b/b/cn/a(II)I
pop
aload 0
getfield com/a/b/l/w/a [J
aload 0
getfield com/a/b/l/w/b I
iload 1
iadd
laload
lstore 3
aload 0
getfield com/a/b/l/w/a [J
aload 0
getfield com/a/b/l/w/b I
iload 1
iadd
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
lastore
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 5
.limit stack 4
.end method

.method private a()[J
aload 0
invokevirtual com/a/b/l/w/size()I
istore 1
iload 1
newarray long
astore 2
aload 0
getfield com/a/b/l/w/a [J
aload 0
getfield com/a/b/l/w/b I
aload 2
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method public final contains(Ljava/lang/Object;)Z
aload 1
instanceof java/lang/Long
ifeq L0
aload 0
getfield com/a/b/l/w/a [J
aload 1
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
aload 0
getfield com/a/b/l/w/b I
aload 0
getfield com/a/b/l/w/c I
invokestatic com/a/b/l/u/b([JJII)I
iconst_m1
if_icmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 5
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 1
aload 0
if_acmpne L0
L1:
iconst_1
ireturn
L0:
aload 1
instanceof com/a/b/l/w
ifeq L2
aload 1
checkcast com/a/b/l/w
astore 1
aload 0
invokevirtual com/a/b/l/w/size()I
istore 3
aload 1
invokevirtual com/a/b/l/w/size()I
iload 3
if_icmpeq L3
iconst_0
ireturn
L3:
iconst_0
istore 2
L4:
iload 2
iload 3
if_icmpge L1
aload 0
getfield com/a/b/l/w/a [J
aload 0
getfield com/a/b/l/w/b I
iload 2
iadd
laload
aload 1
getfield com/a/b/l/w/a [J
aload 1
getfield com/a/b/l/w/b I
iload 2
iadd
laload
lcmp
ifeq L5
iconst_0
ireturn
L5:
iload 2
iconst_1
iadd
istore 2
goto L4
L2:
aload 0
aload 1
invokespecial java/util/AbstractList/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 4
.limit stack 5
.end method

.method public final synthetic get(I)Ljava/lang/Object;
iload 1
aload 0
invokevirtual com/a/b/l/w/size()I
invokestatic com/a/b/b/cn/a(II)I
pop
aload 0
getfield com/a/b/l/w/a [J
aload 0
getfield com/a/b/l/w/b I
iload 1
iadd
laload
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final hashCode()I
iconst_1
istore 2
aload 0
getfield com/a/b/l/w/b I
istore 1
L0:
iload 1
aload 0
getfield com/a/b/l/w/c I
if_icmpge L1
aload 0
getfield com/a/b/l/w/a [J
iload 1
laload
lstore 3
iload 2
bipush 31
imul
lload 3
lload 3
bipush 32
lushr
lxor
l2i
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
ireturn
.limit locals 5
.limit stack 6
.end method

.method public final indexOf(Ljava/lang/Object;)I
aload 1
instanceof java/lang/Long
ifeq L0
aload 0
getfield com/a/b/l/w/a [J
aload 1
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
aload 0
getfield com/a/b/l/w/b I
aload 0
getfield com/a/b/l/w/c I
invokestatic com/a/b/l/u/b([JJII)I
istore 2
iload 2
iflt L0
iload 2
aload 0
getfield com/a/b/l/w/b I
isub
ireturn
L0:
iconst_m1
ireturn
.limit locals 3
.limit stack 5
.end method

.method public final isEmpty()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
aload 1
instanceof java/lang/Long
ifeq L0
aload 0
getfield com/a/b/l/w/a [J
aload 1
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
aload 0
getfield com/a/b/l/w/b I
aload 0
getfield com/a/b/l/w/c I
invokestatic com/a/b/l/u/a([JJII)I
istore 2
iload 2
iflt L0
iload 2
aload 0
getfield com/a/b/l/w/b I
isub
ireturn
L0:
iconst_m1
ireturn
.limit locals 3
.limit stack 5
.end method

.method public final synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
aload 2
checkcast java/lang/Long
astore 2
iload 1
aload 0
invokevirtual com/a/b/l/w/size()I
invokestatic com/a/b/b/cn/a(II)I
pop
aload 0
getfield com/a/b/l/w/a [J
aload 0
getfield com/a/b/l/w/b I
iload 1
iadd
laload
lstore 3
aload 0
getfield com/a/b/l/w/a [J
aload 0
getfield com/a/b/l/w/b I
iload 1
iadd
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
lastore
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 5
.limit stack 4
.end method

.method public final size()I
aload 0
getfield com/a/b/l/w/c I
aload 0
getfield com/a/b/l/w/b I
isub
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final subList(II)Ljava/util/List;
iload 1
iload 2
aload 0
invokevirtual com/a/b/l/w/size()I
invokestatic com/a/b/b/cn/a(III)V
iload 1
iload 2
if_icmpne L0
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
L0:
new com/a/b/l/w
dup
aload 0
getfield com/a/b/l/w/a [J
aload 0
getfield com/a/b/l/w/b I
iload 1
iadd
aload 0
getfield com/a/b/l/w/b I
iload 2
iadd
invokespecial com/a/b/l/w/<init>([JII)V
areturn
.limit locals 3
.limit stack 6
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
aload 0
invokevirtual com/a/b/l/w/size()I
bipush 10
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 2
aload 2
bipush 91
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
aload 0
getfield com/a/b/l/w/a [J
aload 0
getfield com/a/b/l/w/b I
laload
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
pop
aload 0
getfield com/a/b/l/w/b I
iconst_1
iadd
istore 1
L0:
iload 1
aload 0
getfield com/a/b/l/w/c I
if_icmpge L1
aload 2
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/a/b/l/w/a [J
iload 1
laload
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
bipush 93
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method
