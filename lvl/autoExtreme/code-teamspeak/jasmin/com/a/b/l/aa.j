.bytecode 50.0
.class public final synchronized com/a/b/l/aa
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field public static final 'a' I = 2


.field public static final 'b' S = 16384


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(S)I
iload 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a(SS)I
iload 0
iload 1
isub
ireturn
.limit locals 2
.limit stack 2
.end method

.method static a([SSII)I
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
saload
iload 1
if_icmpne L2
iload 2
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static a([S[S)I
aload 0
ldc "array"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "target"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
iconst_0
ireturn
L0:
iconst_0
istore 2
L1:
iload 2
aload 0
arraylength
aload 1
arraylength
isub
iconst_1
iadd
if_icmpge L2
iconst_0
istore 3
L3:
iload 3
aload 1
arraylength
if_icmpge L4
aload 0
iload 2
iload 3
iadd
saload
aload 1
iload 3
saload
if_icmpne L5
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
iload 2
ireturn
L5:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
iconst_m1
ireturn
.limit locals 4
.limit stack 3
.end method

.method private static a()Lcom/a/b/b/ak;
.annotation invisible Lcom/a/b/a/a;
.end annotation
getstatic com/a/b/l/ad/a Lcom/a/b/l/ad;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static transient a(Ljava/lang/String;[S)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
ldc ""
areturn
L0:
new java/lang/StringBuilder
dup
aload 1
arraylength
bipush 6
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 3
aload 3
aload 1
iconst_0
saload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
iconst_1
istore 2
L1:
iload 2
aload 1
arraylength
if_icmpge L2
aload 3
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iload 2
saload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a(BB)S
.annotation invisible Lcom/a/b/a/c;
a s = "doesn't work"
.end annotation
iload 0
bipush 8
ishl
iload 1
sipush 255
iand
ior
i2s
ireturn
.limit locals 2
.limit stack 3
.end method

.method private static a(J)S
lload 0
l2i
i2s
istore 2
iload 2
i2l
lload 0
lcmp
ifeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 34
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Out of range: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 0
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 2
ireturn
.limit locals 3
.limit stack 5
.end method

.method private static a([B)S
.annotation invisible Lcom/a/b/a/c;
a s = "doesn't work"
.end annotation
aload 0
arraylength
iconst_2
if_icmplt L0
iconst_1
istore 1
L1:
iload 1
ldc "array too small: %s < %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iconst_2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
iconst_0
baload
bipush 8
ishl
aload 0
iconst_1
baload
sipush 255
iand
ior
i2s
ireturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method private static transient a([S)S
iconst_1
istore 3
aload 0
arraylength
ifle L0
iconst_1
istore 4
L1:
iload 4
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
saload
istore 1
L2:
iload 3
aload 0
arraylength
if_icmpge L3
iload 1
istore 2
aload 0
iload 3
saload
iload 1
if_icmpge L4
aload 0
iload 3
saload
istore 2
L4:
iload 3
iconst_1
iadd
istore 3
iload 2
istore 1
goto L2
L0:
iconst_0
istore 4
goto L1
L3:
iload 1
ireturn
.limit locals 5
.limit stack 2
.end method

.method private static a([SS)Z
iconst_0
istore 5
aload 0
arraylength
istore 3
iconst_0
istore 2
L0:
iload 5
istore 4
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
saload
iload 1
if_icmpne L2
iconst_1
istore 4
L1:
iload 4
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 6
.limit stack 2
.end method

.method private static a(Ljava/util/Collection;)[S
iconst_0
istore 1
aload 0
instanceof com/a/b/l/ac
ifeq L0
aload 0
checkcast com/a/b/l/ac
astore 0
aload 0
invokevirtual com/a/b/l/ac/size()I
istore 1
iload 1
newarray short
astore 3
aload 0
getfield com/a/b/l/ac/a [S
aload 0
getfield com/a/b/l/ac/b I
aload 3
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
L0:
aload 0
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
astore 0
aload 0
arraylength
istore 2
iload 2
newarray short
astore 3
L1:
iload 1
iload 2
if_icmpge L2
aload 3
iload 1
aload 0
iload 1
aaload
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Number
invokevirtual java/lang/Number/shortValue()S
sastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a([SI)[S
iload 1
newarray short
astore 2
aload 0
iconst_0
aload 2
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 6
.end method

.method private static a([SII)[S
iload 1
iflt L0
iconst_1
istore 3
L1:
iload 3
ldc "Invalid minLength: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 2
iflt L2
iconst_1
istore 3
L3:
iload 3
ldc "Invalid padding: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
astore 4
aload 0
arraylength
iload 1
if_icmpge L4
iload 1
iload 2
iadd
istore 1
iload 1
newarray short
astore 4
aload 0
iconst_0
aload 4
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L4:
aload 4
areturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 3
goto L3
.limit locals 5
.limit stack 6
.end method

.method private static transient a([[S)[S
aload 0
arraylength
istore 3
iconst_0
istore 1
iconst_0
istore 2
L0:
iload 1
iload 3
if_icmpge L1
iload 2
aload 0
iload 1
aaload
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
newarray short
astore 4
aload 0
arraylength
istore 3
iconst_0
istore 2
iconst_0
istore 1
L2:
iload 1
iload 3
if_icmpge L3
aload 0
iload 1
aaload
astore 5
aload 5
iconst_0
aload 4
iload 2
aload 5
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 2
aload 5
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 4
areturn
.limit locals 6
.limit stack 5
.end method

.method private static b([SS)I
aload 0
iload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/aa/a([SSII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method static b([SSII)I
iload 3
iconst_1
isub
istore 3
L0:
iload 3
iload 2
if_icmplt L1
aload 0
iload 3
saload
iload 1
if_icmpne L2
iload 3
ireturn
L2:
iload 3
iconst_1
isub
istore 3
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static b()Ljava/util/Comparator;
getstatic com/a/b/l/ab/a Lcom/a/b/l/ab;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static b(J)S
lload 0
ldc2_w 32767L
lcmp
ifle L0
sipush 32767
ireturn
L0:
lload 0
ldc2_w -32768L
lcmp
ifge L1
sipush -32768
ireturn
L1:
lload 0
l2i
i2s
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static transient b([S)S
iconst_1
istore 3
aload 0
arraylength
ifle L0
iconst_1
istore 4
L1:
iload 4
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
saload
istore 1
L2:
iload 3
aload 0
arraylength
if_icmpge L3
iload 1
istore 2
aload 0
iload 3
saload
iload 1
if_icmple L4
aload 0
iload 3
saload
istore 2
L4:
iload 3
iconst_1
iadd
istore 3
iload 2
istore 1
goto L2
L0:
iconst_0
istore 4
goto L1
L3:
iload 1
ireturn
.limit locals 5
.limit stack 2
.end method

.method private static b(S)[B
.annotation invisible Lcom/a/b/a/c;
a s = "doesn't work"
.end annotation
iconst_2
newarray byte
dup
iconst_0
iload 0
bipush 8
ishr
i2b
bastore
dup
iconst_1
iload 0
i2b
bastore
areturn
.limit locals 1
.limit stack 5
.end method

.method private static c([SS)I
aload 0
iload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/aa/b([SSII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static synthetic c([SSII)I
aload 0
iload 1
iload 2
iload 3
invokestatic com/a/b/l/aa/a([SSII)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static transient c([S)Ljava/util/List;
aload 0
arraylength
ifne L0
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
L0:
new com/a/b/l/ac
dup
aload 0
invokespecial com/a/b/l/ac/<init>([S)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static synthetic d([SSII)I
aload 0
iload 1
iload 2
iload 3
invokestatic com/a/b/l/aa/b([SSII)I
ireturn
.limit locals 4
.limit stack 4
.end method
