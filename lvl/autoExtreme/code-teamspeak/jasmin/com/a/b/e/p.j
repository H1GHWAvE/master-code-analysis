.bytecode 50.0
.class public synchronized abstract com/a/b/e/p
.super com/a/b/e/g
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private static final 'a' I = 32


.method public <init>()V
aload 0
invokespecial com/a/b/e/g/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a([CII)[C
iload 2
newarray char
astore 3
iload 1
ifle L0
aload 0
iconst_0
aload 3
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L0:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private static b(Ljava/lang/CharSequence;II)I
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
iload 2
if_icmpge L0
iload 1
iconst_1
iadd
istore 5
aload 0
iload 1
invokeinterface java/lang/CharSequence/charAt(I)C 1
istore 3
iload 3
ldc_w 55296
if_icmplt L1
iload 3
ldc_w 57343
if_icmple L2
L1:
iload 3
ireturn
L2:
iload 3
ldc_w 56319
if_icmpgt L3
iload 5
iload 2
if_icmpne L4
iload 3
ineg
ireturn
L4:
aload 0
iload 5
invokeinterface java/lang/CharSequence/charAt(I)C 1
istore 4
iload 4
invokestatic java/lang/Character/isLowSurrogate(C)Z
ifeq L5
iload 3
iload 4
invokestatic java/lang/Character/toCodePoint(CC)I
ireturn
L5:
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 89
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Expected low surrogate but got char '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
ldc "' with value "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " at index "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 5
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " in '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 88
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Unexpected low surrogate character '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
ldc "' with value "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " at index "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 5
iconst_1
isub
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " in '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/lang/IndexOutOfBoundsException
dup
ldc "Index exceeds specified range"
invokespecial java/lang/IndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
.limit locals 6
.limit stack 6
.end method

.method public a(Ljava/lang/CharSequence;II)I
L0:
iload 2
iload 3
if_icmpge L1
aload 1
iload 2
iload 3
invokestatic com/a/b/e/p/b(Ljava/lang/CharSequence;II)I
istore 4
iload 4
iflt L1
aload 0
iload 4
invokevirtual com/a/b/e/p/a(I)[C
ifnonnull L1
iload 4
invokestatic java/lang/Character/isSupplementaryCodePoint(I)Z
ifeq L2
iconst_2
istore 4
L3:
iload 2
iload 4
iadd
istore 2
goto L0
L2:
iconst_1
istore 4
goto L3
L1:
iload 2
ireturn
.limit locals 5
.limit stack 3
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokevirtual java/lang/String/length()I
istore 2
aload 0
aload 1
iconst_0
iload 2
invokevirtual com/a/b/e/p/a(Ljava/lang/CharSequence;II)I
istore 3
iload 3
iload 2
if_icmpne L0
aload 1
areturn
L0:
aload 0
aload 1
iload 3
invokevirtual com/a/b/e/p/a(Ljava/lang/String;I)Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final a(Ljava/lang/String;I)Ljava/lang/String;
aload 1
invokevirtual java/lang/String/length()I
istore 7
invokestatic com/a/b/e/n/a()[C
astore 9
iconst_0
istore 3
iconst_0
istore 4
iload 2
istore 5
iload 4
istore 2
L0:
iload 5
iload 7
if_icmpge L1
aload 1
iload 5
iload 7
invokestatic com/a/b/e/p/b(Ljava/lang/CharSequence;II)I
istore 4
iload 4
ifge L2
new java/lang/IllegalArgumentException
dup
ldc "Trailing high surrogate at end of input"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
iload 4
invokevirtual com/a/b/e/p/a(I)[C
astore 11
iload 4
invokestatic java/lang/Character/isSupplementaryCodePoint(I)Z
ifeq L3
iconst_2
istore 4
L4:
iload 4
iload 5
iadd
istore 6
aload 11
ifnull L5
iload 5
iload 3
isub
istore 8
iload 2
iload 8
iadd
aload 11
arraylength
iadd
istore 4
aload 9
astore 10
aload 9
arraylength
iload 4
if_icmpge L6
aload 9
iload 2
iload 4
iload 7
iload 5
isub
iadd
bipush 32
iadd
invokestatic com/a/b/e/p/a([CII)[C
astore 10
L6:
iload 2
istore 4
iload 8
ifle L7
aload 1
iload 3
iload 5
aload 10
iload 2
invokevirtual java/lang/String/getChars(II[CI)V
iload 2
iload 8
iadd
istore 4
L7:
iload 4
istore 2
aload 11
arraylength
ifle L8
aload 11
iconst_0
aload 10
iload 4
aload 11
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 4
aload 11
arraylength
iadd
istore 2
L8:
iload 2
istore 3
iload 6
istore 2
aload 10
astore 9
L9:
aload 0
aload 1
iload 6
iload 7
invokevirtual com/a/b/e/p/a(Ljava/lang/CharSequence;II)I
istore 5
iload 2
istore 4
iload 3
istore 2
iload 4
istore 3
goto L0
L3:
iconst_1
istore 4
goto L4
L1:
iload 7
iload 3
isub
istore 5
iload 2
istore 4
aload 9
astore 10
iload 5
ifle L10
iload 5
iload 2
iadd
istore 4
aload 9
astore 10
aload 9
arraylength
iload 4
if_icmpge L11
aload 9
iload 2
iload 4
invokestatic com/a/b/e/p/a([CII)[C
astore 10
L11:
aload 1
iload 3
iload 7
aload 10
iload 2
invokevirtual java/lang/String/getChars(II[CI)V
L10:
new java/lang/String
dup
aload 10
iconst_0
iload 4
invokespecial java/lang/String/<init>([CII)V
areturn
L5:
iload 2
istore 4
iload 3
istore 2
iload 4
istore 3
goto L9
.limit locals 12
.limit stack 5
.end method

.method public abstract a(I)[C
.end method
