.bytecode 50.0
.class public final synchronized com/a/b/k/b
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private final 'a' Ljava/lang/String;

.method private <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/b/k/b/a Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/k/b;
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
aload 0
invokestatic com/a/b/k/a/a(Ljava/lang/String;)Lcom/a/b/k/a;
astore 0
aload 0
invokevirtual com/a/b/k/a/a()Z
ifne L3
iconst_1
istore 1
L4:
iload 1
invokestatic com/a/b/b/cn/a(Z)V
aload 0
getfield com/a/b/k/a/a Ljava/lang/String;
astore 2
L0:
aload 2
invokestatic com/a/b/k/d/a(Ljava/lang/String;)Ljava/net/InetAddress;
astore 0
L1:
aload 0
ifnull L5
new com/a/b/k/b
dup
aload 0
invokestatic com/a/b/k/d/a(Ljava/net/InetAddress;)Ljava/lang/String;
invokespecial com/a/b/k/b/<init>(Ljava/lang/String;)V
areturn
L3:
iconst_0
istore 1
goto L4
L2:
astore 0
aconst_null
astore 0
goto L1
L5:
aload 2
invokestatic com/a/b/k/f/a(Ljava/lang/String;)Lcom/a/b/k/f;
astore 0
aload 0
invokevirtual com/a/b/k/f/a()Z
ifeq L6
new com/a/b/k/b
dup
aload 0
invokevirtual com/a/b/k/f/toString()Ljava/lang/String;
invokespecial com/a/b/k/b/<init>(Ljava/lang/String;)V
areturn
L6:
aload 2
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 0
invokevirtual java/lang/String/length()I
ifeq L7
ldc "Domain name does not have a recognized public suffix: "
aload 0
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 0
L8:
new java/lang/IllegalArgumentException
dup
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L7:
new java/lang/String
dup
ldc "Domain name does not have a recognized public suffix: "
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
astore 0
goto L8
.limit locals 3
.limit stack 3
.end method

.method private static b(Ljava/lang/String;)Lcom/a/b/k/b;
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
L0:
aload 0
invokestatic com/a/b/k/b/a(Ljava/lang/String;)Lcom/a/b/k/b;
astore 1
L1:
aload 1
areturn
L2:
astore 1
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 0
invokevirtual java/lang/String/length()I
ifeq L3
ldc "Invalid host specifier: "
aload 0
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 0
L4:
new java/text/ParseException
dup
aload 0
iconst_0
invokespecial java/text/ParseException/<init>(Ljava/lang/String;I)V
astore 0
aload 0
aload 1
invokevirtual java/text/ParseException/initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
pop
aload 0
athrow
L3:
new java/lang/String
dup
ldc "Invalid host specifier: "
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
astore 0
goto L4
.limit locals 2
.limit stack 4
.end method

.method private static c(Ljava/lang/String;)Z
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
L0:
aload 0
invokestatic com/a/b/k/b/a(Ljava/lang/String;)Lcom/a/b/k/b;
pop
L1:
iconst_1
ireturn
L2:
astore 0
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
if_acmpne L0
iconst_1
ireturn
L0:
aload 1
instanceof com/a/b/k/b
ifeq L1
aload 1
checkcast com/a/b/k/b
astore 1
aload 0
getfield com/a/b/k/b/a Ljava/lang/String;
aload 1
getfield com/a/b/k/b/a Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/k/b/a Ljava/lang/String;
invokevirtual java/lang/String/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/k/b/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
