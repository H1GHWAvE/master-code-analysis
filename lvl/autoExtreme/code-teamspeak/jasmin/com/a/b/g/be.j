.bytecode 50.0
.class public final synchronized com/a/b/g/be
.super java/io/FilterOutputStream
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private final 'a' Lcom/a/b/g/al;

.method private <init>(Lcom/a/b/g/ak;Ljava/io/OutputStream;)V
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/io/OutputStream
invokespecial java/io/FilterOutputStream/<init>(Ljava/io/OutputStream;)V
aload 0
aload 1
invokeinterface com/a/b/g/ak/a()Lcom/a/b/g/al; 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/g/al
putfield com/a/b/g/be/a Lcom/a/b/g/al;
return
.limit locals 3
.limit stack 2
.end method

.method private a()Lcom/a/b/g/ag;
aload 0
getfield com/a/b/g/be/a Lcom/a/b/g/al;
invokeinterface com/a/b/g/al/a()Lcom/a/b/g/ag; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final close()V
aload 0
getfield com/a/b/g/be/out Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/close()V
return
.limit locals 1
.limit stack 1
.end method

.method public final write(I)V
aload 0
getfield com/a/b/g/be/a Lcom/a/b/g/al;
iload 1
i2b
invokeinterface com/a/b/g/al/b(B)Lcom/a/b/g/al; 1
pop
aload 0
getfield com/a/b/g/be/out Ljava/io/OutputStream;
iload 1
invokevirtual java/io/OutputStream/write(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final write([BII)V
aload 0
getfield com/a/b/g/be/a Lcom/a/b/g/al;
aload 1
iload 2
iload 3
invokeinterface com/a/b/g/al/b([BII)Lcom/a/b/g/al; 3
pop
aload 0
getfield com/a/b/g/be/out Ljava/io/OutputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/OutputStream/write([BII)V
return
.limit locals 4
.limit stack 4
.end method
