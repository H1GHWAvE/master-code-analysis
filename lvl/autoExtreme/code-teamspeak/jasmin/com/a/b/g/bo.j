.bytecode 50.0
.class final synchronized com/a/b/g/bo
.super com/a/b/g/h
.implements java/io/Serializable

.field private static final 'e' J = 0L


.field private final 'a' I

.field private final 'b' I

.field private final 'c' J

.field private final 'd' J

.method <init>(JJ)V
aload 0
invokespecial com/a/b/g/h/<init>()V
iconst_1
ldc "The number of SipRound iterations (c=%s) during Compression must be positive."
iconst_1
anewarray java/lang/Object
dup
iconst_0
iconst_2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iconst_1
ldc "The number of SipRound iterations (d=%s) during Finalization must be positive."
iconst_1
anewarray java/lang/Object
dup
iconst_0
iconst_4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
iconst_2
putfield com/a/b/g/bo/a I
aload 0
iconst_4
putfield com/a/b/g/bo/b I
aload 0
lload 1
putfield com/a/b/g/bo/c J
aload 0
lload 3
putfield com/a/b/g/bo/d J
return
.limit locals 5
.limit stack 6
.end method

.method public final a()Lcom/a/b/g/al;
new com/a/b/g/bp
dup
aload 0
getfield com/a/b/g/bo/a I
aload 0
getfield com/a/b/g/bo/b I
aload 0
getfield com/a/b/g/bo/c J
aload 0
getfield com/a/b/g/bo/d J
invokespecial com/a/b/g/bp/<init>(IIJJ)V
areturn
.limit locals 1
.limit stack 8
.end method

.method public final b()I
bipush 64
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/g/bo
ifeq L0
aload 1
checkcast com/a/b/g/bo
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/g/bo/a I
aload 1
getfield com/a/b/g/bo/a I
if_icmpne L0
iload 3
istore 2
aload 0
getfield com/a/b/g/bo/b I
aload 1
getfield com/a/b/g/bo/b I
if_icmpne L0
iload 3
istore 2
aload 0
getfield com/a/b/g/bo/c J
aload 1
getfield com/a/b/g/bo/c J
lcmp
ifne L0
iload 3
istore 2
aload 0
getfield com/a/b/g/bo/d J
aload 1
getfield com/a/b/g/bo/d J
lcmp
ifne L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final hashCode()I
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Object/hashCode()I
aload 0
getfield com/a/b/g/bo/a I
ixor
aload 0
getfield com/a/b/g/bo/b I
ixor
i2l
aload 0
getfield com/a/b/g/bo/c J
lxor
aload 0
getfield com/a/b/g/bo/d J
lxor
l2i
ireturn
.limit locals 1
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/g/bo/a I
istore 1
aload 0
getfield com/a/b/g/bo/b I
istore 2
aload 0
getfield com/a/b/g/bo/c J
lstore 3
aload 0
getfield com/a/b/g/bo/d J
lstore 5
new java/lang/StringBuilder
dup
bipush 81
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Hashing.sipHash"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 3
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 5
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 7
.limit stack 3
.end method
