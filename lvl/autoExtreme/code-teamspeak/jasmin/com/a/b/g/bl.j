.bytecode 50.0
.class final synchronized com/a/b/g/bl
.super com/a/b/g/h
.implements java/io/Serializable

.field private static final 'a' I = -862048943


.field private static final 'b' I = 461845907


.field private static final 'd' J = 0L


.field private final 'c' I

.method <init>(I)V
aload 0
invokespecial com/a/b/g/h/<init>()V
aload 0
iload 1
putfield com/a/b/g/bl/c I
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(II)I
iload 0
iload 1
invokestatic com/a/b/g/bl/c(II)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(II)Lcom/a/b/g/ag;
iload 0
iload 1
invokestatic com/a/b/g/bl/d(II)Lcom/a/b/g/ag;
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic c(I)I
iload 0
invokestatic com/a/b/g/bl/d(I)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static c(II)I
iload 0
iload 1
ixor
bipush 13
invokestatic java/lang/Integer/rotateLeft(II)I
iconst_5
imul
ldc_w 430675100
isub
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static d(I)I
ldc_w -862048943
iload 0
imul
bipush 15
invokestatic java/lang/Integer/rotateLeft(II)I
ldc_w 461845907
imul
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static d(II)Lcom/a/b/g/ag;
iload 0
iload 1
ixor
istore 0
iload 0
iload 0
bipush 16
iushr
ixor
ldc_w -2048144789
imul
istore 0
iload 0
iload 0
bipush 13
iushr
ixor
ldc_w -1028477387
imul
istore 0
iload 0
iload 0
bipush 16
iushr
ixor
invokestatic com/a/b/g/ag/a(I)Lcom/a/b/g/ag;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final a(J)Lcom/a/b/g/ag;
lload 1
l2i
istore 4
lload 1
bipush 32
lushr
l2i
istore 3
iload 4
invokestatic com/a/b/g/bl/d(I)I
istore 4
aload 0
getfield com/a/b/g/bl/c I
iload 4
invokestatic com/a/b/g/bl/c(II)I
iload 3
invokestatic com/a/b/g/bl/d(I)I
invokestatic com/a/b/g/bl/c(II)I
bipush 8
invokestatic com/a/b/g/bl/d(II)Lcom/a/b/g/ag;
areturn
.limit locals 5
.limit stack 3
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/a/b/g/ag;
aload 0
getfield com/a/b/g/bl/c I
istore 2
iconst_1
istore 3
L0:
iload 3
aload 1
invokeinterface java/lang/CharSequence/length()I 0
if_icmpge L1
iload 2
aload 1
iload 3
iconst_1
isub
invokeinterface java/lang/CharSequence/charAt(I)C 1
aload 1
iload 3
invokeinterface java/lang/CharSequence/charAt(I)C 1
bipush 16
ishl
ior
invokestatic com/a/b/g/bl/d(I)I
invokestatic com/a/b/g/bl/c(II)I
istore 2
iload 3
iconst_2
iadd
istore 3
goto L0
L1:
iload 2
istore 3
aload 1
invokeinterface java/lang/CharSequence/length()I 0
iconst_1
iand
iconst_1
if_icmpne L2
iload 2
aload 1
aload 1
invokeinterface java/lang/CharSequence/length()I 0
iconst_1
isub
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokestatic com/a/b/g/bl/d(I)I
ixor
istore 3
L2:
iload 3
aload 1
invokeinterface java/lang/CharSequence/length()I 0
iconst_2
imul
invokestatic com/a/b/g/bl/d(II)Lcom/a/b/g/ag;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final a()Lcom/a/b/g/al;
new com/a/b/g/bm
dup
aload 0
getfield com/a/b/g/bl/c I
invokespecial com/a/b/g/bm/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final b()I
bipush 32
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(I)Lcom/a/b/g/ag;
iload 1
invokestatic com/a/b/g/bl/d(I)I
istore 1
aload 0
getfield com/a/b/g/bl/c I
iload 1
invokestatic com/a/b/g/bl/c(II)I
iconst_4
invokestatic com/a/b/g/bl/d(II)Lcom/a/b/g/ag;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/g/bl
ifeq L0
aload 1
checkcast com/a/b/g/bl
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/g/bl/c I
aload 1
getfield com/a/b/g/bl/c I
if_icmpne L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final hashCode()I
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Object/hashCode()I
aload 0
getfield com/a/b/g/bl/c I
ixor
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/g/bl/c I
istore 1
new java/lang/StringBuilder
dup
bipush 31
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Hashing.murmur3_32("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method
