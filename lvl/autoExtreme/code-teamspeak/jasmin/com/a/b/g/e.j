.bytecode 50.0
.class synchronized abstract com/a/b/g/e
.super java/lang/Object
.implements com/a/b/g/ak

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(J)Lcom/a/b/g/ag;
aload 0
bipush 8
invokevirtual com/a/b/g/e/a(I)Lcom/a/b/g/al;
lload 1
invokeinterface com/a/b/g/al/a(J)Lcom/a/b/g/al; 2
invokeinterface com/a/b/g/al/a()Lcom/a/b/g/ag; 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/a/b/g/ag;
aload 1
invokeinterface java/lang/CharSequence/length()I 0
istore 3
aload 0
iload 3
iconst_2
imul
invokevirtual com/a/b/g/e/a(I)Lcom/a/b/g/al;
astore 4
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 4
aload 1
iload 2
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokeinterface com/a/b/g/al/a(C)Lcom/a/b/g/al; 1
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 4
invokeinterface com/a/b/g/al/a()Lcom/a/b/g/ag; 0
areturn
.limit locals 5
.limit stack 3
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/ag;
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
aload 2
invokevirtual java/lang/String/getBytes(Ljava/nio/charset/Charset;)[B
astore 1
aload 0
aload 1
aload 1
arraylength
invokevirtual com/a/b/g/e/a([BI)Lcom/a/b/g/ag;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/ag;
aload 0
invokevirtual com/a/b/g/e/a()Lcom/a/b/g/al;
aload 1
aload 2
invokeinterface com/a/b/g/al/a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/al; 2
invokeinterface com/a/b/g/al/a()Lcom/a/b/g/ag; 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a([B)Lcom/a/b/g/ag;
aload 0
aload 1
aload 1
arraylength
invokevirtual com/a/b/g/e/a([BI)Lcom/a/b/g/ag;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final a()Lcom/a/b/g/al;
new com/a/b/g/f
dup
aload 0
bipush 32
invokespecial com/a/b/g/f/<init>(Lcom/a/b/g/e;I)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final a(I)Lcom/a/b/g/al;
iload 1
iflt L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
new com/a/b/g/f
dup
aload 0
iload 1
invokespecial com/a/b/g/f/<init>(Lcom/a/b/g/e;I)V
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 4
.end method

.method public final b(I)Lcom/a/b/g/ag;
aload 0
iconst_4
invokevirtual com/a/b/g/e/a(I)Lcom/a/b/g/al;
iload 1
invokeinterface com/a/b/g/al/a(I)Lcom/a/b/g/al; 1
invokeinterface com/a/b/g/al/a()Lcom/a/b/g/ag; 0
areturn
.limit locals 2
.limit stack 2
.end method
