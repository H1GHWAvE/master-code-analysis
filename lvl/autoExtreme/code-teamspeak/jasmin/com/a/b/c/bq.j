.bytecode 50.0
.class synchronized com/a/b/c/bq
.super com/a/b/c/aj
.implements java/io/Serializable

.field private static final 'a' J = 1L


.field final 'b' Lcom/a/b/c/bw;

.field final 'c' Lcom/a/b/c/bw;

.field final 'd' Lcom/a/b/b/au;

.field final 'e' Lcom/a/b/b/au;

.field final 'f' J

.field final 'g' J

.field final 'h' J

.field final 'i' Lcom/a/b/c/do;

.field final 'j' I

.field final 'k' Lcom/a/b/c/dg;

.field final 'l' Lcom/a/b/b/ej;

.field final 'm' Lcom/a/b/c/ab;

.field transient 'n' Lcom/a/b/c/e;

.method <init>(Lcom/a/b/c/ao;)V
aload 0
aload 1
getfield com/a/b/c/ao/m Lcom/a/b/c/bw;
aload 1
getfield com/a/b/c/ao/n Lcom/a/b/c/bw;
aload 1
getfield com/a/b/c/ao/k Lcom/a/b/b/au;
aload 1
getfield com/a/b/c/ao/l Lcom/a/b/b/au;
aload 1
getfield com/a/b/c/ao/r J
aload 1
getfield com/a/b/c/ao/q J
aload 1
getfield com/a/b/c/ao/o J
aload 1
getfield com/a/b/c/ao/p Lcom/a/b/c/do;
aload 1
getfield com/a/b/c/ao/j I
aload 1
getfield com/a/b/c/ao/u Lcom/a/b/c/dg;
aload 1
getfield com/a/b/c/ao/v Lcom/a/b/b/ej;
aload 1
getfield com/a/b/c/ao/y Lcom/a/b/c/ab;
invokespecial com/a/b/c/bq/<init>(Lcom/a/b/c/bw;Lcom/a/b/c/bw;Lcom/a/b/b/au;Lcom/a/b/b/au;JJJLcom/a/b/c/do;ILcom/a/b/c/dg;Lcom/a/b/b/ej;Lcom/a/b/c/ab;)V
return
.limit locals 2
.limit stack 16
.end method

.method private <init>(Lcom/a/b/c/bw;Lcom/a/b/c/bw;Lcom/a/b/b/au;Lcom/a/b/b/au;JJJLcom/a/b/c/do;ILcom/a/b/c/dg;Lcom/a/b/b/ej;Lcom/a/b/c/ab;)V
aload 0
invokespecial com/a/b/c/aj/<init>()V
aload 0
aload 1
putfield com/a/b/c/bq/b Lcom/a/b/c/bw;
aload 0
aload 2
putfield com/a/b/c/bq/c Lcom/a/b/c/bw;
aload 0
aload 3
putfield com/a/b/c/bq/d Lcom/a/b/b/au;
aload 0
aload 4
putfield com/a/b/c/bq/e Lcom/a/b/b/au;
aload 0
lload 5
putfield com/a/b/c/bq/f J
aload 0
lload 7
putfield com/a/b/c/bq/g J
aload 0
lload 9
putfield com/a/b/c/bq/h J
aload 0
aload 11
putfield com/a/b/c/bq/i Lcom/a/b/c/do;
aload 0
iload 12
putfield com/a/b/c/bq/j I
aload 0
aload 13
putfield com/a/b/c/bq/k Lcom/a/b/c/dg;
aload 14
invokestatic com/a/b/b/ej/b()Lcom/a/b/b/ej;
if_acmpeq L0
aload 14
astore 1
aload 14
getstatic com/a/b/c/f/d Lcom/a/b/b/ej;
if_acmpne L1
L0:
aconst_null
astore 1
L1:
aload 0
aload 1
putfield com/a/b/c/bq/l Lcom/a/b/b/ej;
aload 0
aload 15
putfield com/a/b/c/bq/m Lcom/a/b/c/ab;
return
.limit locals 16
.limit stack 3
.end method

.method private a(Ljava/io/ObjectInputStream;)V
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 0
invokevirtual com/a/b/c/bq/h()Lcom/a/b/c/f;
astore 1
aload 1
invokevirtual com/a/b/c/f/d()V
aload 1
getfield com/a/b/c/f/p J
ldc2_w -1L
lcmp
ifne L0
iconst_1
istore 2
L1:
iload 2
ldc "refreshAfterWrite requires a LoadingCache"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
new com/a/b/c/bo
dup
aload 1
invokespecial com/a/b/c/bo/<init>(Lcom/a/b/c/f;)V
putfield com/a/b/c/bq/n Lcom/a/b/c/e;
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 4
.end method

.method private i()Ljava/lang/Object;
aload 0
getfield com/a/b/c/bq/n Lcom/a/b/c/e;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final f()Lcom/a/b/c/e;
aload 0
getfield com/a/b/c/bq/n Lcom/a/b/c/e;
areturn
.limit locals 1
.limit stack 1
.end method

.method final h()Lcom/a/b/c/f;
iconst_1
istore 2
invokestatic com/a/b/c/f/a()Lcom/a/b/c/f;
aload 0
getfield com/a/b/c/bq/b Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
aload 0
getfield com/a/b/c/bq/c Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
astore 3
aload 0
getfield com/a/b/c/bq/d Lcom/a/b/b/au;
astore 4
aload 3
getfield com/a/b/c/f/q Lcom/a/b/b/au;
ifnonnull L0
iconst_1
istore 1
L1:
iload 1
ldc "key equivalence was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 3
getfield com/a/b/c/f/q Lcom/a/b/b/au;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 3
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/au
putfield com/a/b/c/f/q Lcom/a/b/b/au;
aload 0
getfield com/a/b/c/bq/e Lcom/a/b/b/au;
astore 4
aload 3
getfield com/a/b/c/f/r Lcom/a/b/b/au;
ifnonnull L2
iconst_1
istore 1
L3:
iload 1
ldc "value equivalence was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 3
getfield com/a/b/c/f/r Lcom/a/b/b/au;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 3
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/au
putfield com/a/b/c/f/r Lcom/a/b/b/au;
aload 3
aload 0
getfield com/a/b/c/bq/j I
invokevirtual com/a/b/c/f/a(I)Lcom/a/b/c/f;
astore 3
aload 0
getfield com/a/b/c/bq/k Lcom/a/b/c/dg;
astore 4
aload 3
getfield com/a/b/c/f/s Lcom/a/b/c/dg;
ifnonnull L4
iconst_1
istore 1
L5:
iload 1
invokestatic com/a/b/b/cn/b(Z)V
aload 3
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/c/dg
putfield com/a/b/c/f/s Lcom/a/b/c/dg;
aload 3
iconst_0
putfield com/a/b/c/f/f Z
aload 0
getfield com/a/b/c/bq/f J
lconst_0
lcmp
ifle L6
aload 3
aload 0
getfield com/a/b/c/bq/f J
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/c/f/a(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;
pop
L6:
aload 0
getfield com/a/b/c/bq/g J
lconst_0
lcmp
ifle L7
aload 3
aload 0
getfield com/a/b/c/bq/g J
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/c/f/b(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;
pop
L7:
aload 0
getfield com/a/b/c/bq/i Lcom/a/b/c/do;
getstatic com/a/b/c/k/a Lcom/a/b/c/k;
if_acmpeq L8
aload 0
getfield com/a/b/c/bq/i Lcom/a/b/c/do;
astore 4
aload 3
getfield com/a/b/c/f/k Lcom/a/b/c/do;
ifnonnull L9
iconst_1
istore 1
L10:
iload 1
invokestatic com/a/b/b/cn/b(Z)V
aload 3
getfield com/a/b/c/f/f Z
ifeq L11
aload 3
getfield com/a/b/c/f/i J
ldc2_w -1L
lcmp
ifne L12
iconst_1
istore 1
L13:
iload 1
ldc "weigher can not be combined with maximum size"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 3
getfield com/a/b/c/f/i J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
L11:
aload 3
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/c/do
putfield com/a/b/c/f/k Lcom/a/b/c/do;
aload 0
getfield com/a/b/c/bq/h J
ldc2_w -1L
lcmp
ifeq L14
aload 3
aload 0
getfield com/a/b/c/bq/h J
invokevirtual com/a/b/c/f/b(J)Lcom/a/b/c/f;
pop
L14:
aload 0
getfield com/a/b/c/bq/l Lcom/a/b/b/ej;
ifnull L15
aload 0
getfield com/a/b/c/bq/l Lcom/a/b/b/ej;
astore 4
aload 3
getfield com/a/b/c/f/t Lcom/a/b/b/ej;
ifnonnull L16
iload 2
istore 1
L17:
iload 1
invokestatic com/a/b/b/cn/b(Z)V
aload 3
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/ej
putfield com/a/b/c/f/t Lcom/a/b/b/ej;
L15:
aload 3
areturn
L0:
iconst_0
istore 1
goto L1
L2:
iconst_0
istore 1
goto L3
L4:
iconst_0
istore 1
goto L5
L9:
iconst_0
istore 1
goto L10
L12:
iconst_0
istore 1
goto L13
L8:
aload 0
getfield com/a/b/c/bq/h J
ldc2_w -1L
lcmp
ifeq L14
aload 3
aload 0
getfield com/a/b/c/bq/h J
invokevirtual com/a/b/c/f/a(J)Lcom/a/b/c/f;
pop
goto L14
L16:
iconst_0
istore 1
goto L17
.limit locals 5
.limit stack 7
.end method

.method protected final volatile synthetic k_()Ljava/lang/Object;
aload 0
getfield com/a/b/c/bq/n Lcom/a/b/c/e;
areturn
.limit locals 1
.limit stack 1
.end method
