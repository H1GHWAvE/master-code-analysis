.bytecode 50.0
.class final synchronized com/a/b/i/cd
.super java/io/InputStream

.field private 'a' Ljava/util/Iterator;

.field private 'b' Ljava/io/InputStream;

.method public <init>(Ljava/util/Iterator;)V
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Iterator
putfield com/a/b/i/cd/a Ljava/util/Iterator;
aload 0
invokespecial com/a/b/i/cd/a()V
return
.limit locals 2
.limit stack 2
.end method

.method private a()V
aload 0
invokevirtual com/a/b/i/cd/close()V
aload 0
getfield com/a/b/i/cd/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 0
aload 0
getfield com/a/b/i/cd/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/i/s
invokevirtual com/a/b/i/s/a()Ljava/io/InputStream;
putfield com/a/b/i/cd/b Ljava/io/InputStream;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final available()I
aload 0
getfield com/a/b/i/cd/b Ljava/io/InputStream;
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
getfield com/a/b/i/cd/b Ljava/io/InputStream;
invokevirtual java/io/InputStream/available()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final close()V
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/i/cd/b Ljava/io/InputStream;
ifnull L3
L0:
aload 0
getfield com/a/b/i/cd/b Ljava/io/InputStream;
invokevirtual java/io/InputStream/close()V
L1:
aload 0
aconst_null
putfield com/a/b/i/cd/b Ljava/io/InputStream;
L3:
return
L2:
astore 1
aload 0
aconst_null
putfield com/a/b/i/cd/b Ljava/io/InputStream;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public final markSupported()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final read()I
L0:
aload 0
getfield com/a/b/i/cd/b Ljava/io/InputStream;
ifnonnull L1
iconst_m1
ireturn
L1:
aload 0
getfield com/a/b/i/cd/b Ljava/io/InputStream;
invokevirtual java/io/InputStream/read()I
istore 1
iload 1
iconst_m1
if_icmpne L2
aload 0
invokespecial com/a/b/i/cd/a()V
goto L0
L2:
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final read([BII)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
L0:
aload 0
getfield com/a/b/i/cd/b Ljava/io/InputStream;
ifnonnull L1
iconst_m1
ireturn
L1:
aload 0
getfield com/a/b/i/cd/b Ljava/io/InputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/InputStream/read([BII)I
istore 4
iload 4
iconst_m1
if_icmpne L2
aload 0
invokespecial com/a/b/i/cd/a()V
goto L0
L2:
iload 4
ireturn
.limit locals 5
.limit stack 4
.end method

.method public final skip(J)J
aload 0
getfield com/a/b/i/cd/b Ljava/io/InputStream;
ifnull L0
lload 1
lconst_0
lcmp
ifgt L1
L0:
lconst_0
lstore 3
L2:
lload 3
lreturn
L1:
aload 0
getfield com/a/b/i/cd/b Ljava/io/InputStream;
lload 1
invokevirtual java/io/InputStream/skip(J)J
lstore 5
lload 5
lstore 3
lload 5
lconst_0
lcmp
ifne L2
aload 0
invokevirtual com/a/b/i/cd/read()I
iconst_m1
if_icmpne L3
lconst_0
lreturn
L3:
aload 0
getfield com/a/b/i/cd/b Ljava/io/InputStream;
lload 1
lconst_1
lsub
invokevirtual java/io/InputStream/skip(J)J
lconst_1
ladd
lreturn
.limit locals 7
.limit stack 5
.end method
