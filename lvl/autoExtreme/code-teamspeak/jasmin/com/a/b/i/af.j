.bytecode 50.0
.class final synchronized com/a/b/i/af
.super java/io/Reader

.field private 'a' Ljava/lang/CharSequence;

.field private 'b' I

.field private 'c' I

.method public <init>(Ljava/lang/CharSequence;)V
aload 0
invokespecial java/io/Reader/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/CharSequence
putfield com/a/b/i/af/a Ljava/lang/CharSequence;
return
.limit locals 2
.limit stack 2
.end method

.method private a()V
aload 0
getfield com/a/b/i/af/a Ljava/lang/CharSequence;
ifnonnull L0
new java/io/IOException
dup
ldc "reader closed"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 1
.limit stack 3
.end method

.method private b()Z
aload 0
invokespecial com/a/b/i/af/c()I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()I
aload 0
getfield com/a/b/i/af/a Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
aload 0
getfield com/a/b/i/af/b I
isub
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final close()V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
aconst_null
putfield com/a/b/i/af/a Ljava/lang/CharSequence;
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public final mark(I)V
.catch all from L0 to L1 using L2
iconst_1
istore 2
aload 0
monitorenter
iload 1
iflt L3
L0:
iload 2
ldc "readAheadLimit (%s) may not be negative"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokespecial com/a/b/i/af/a()V
aload 0
aload 0
getfield com/a/b/i/af/b I
putfield com/a/b/i/af/c I
L1:
aload 0
monitorexit
return
L3:
iconst_0
istore 2
goto L0
L2:
astore 3
aload 0
monitorexit
aload 3
athrow
.limit locals 4
.limit stack 6
.end method

.method public final markSupported()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final read()I
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
invokespecial com/a/b/i/af/a()V
aload 0
invokespecial com/a/b/i/af/b()Z
ifeq L3
aload 0
getfield com/a/b/i/af/a Ljava/lang/CharSequence;
astore 2
aload 0
getfield com/a/b/i/af/b I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield com/a/b/i/af/b I
aload 2
iload 1
invokeinterface java/lang/CharSequence/charAt(I)C 1
istore 1
L1:
aload 0
monitorexit
iload 1
ireturn
L3:
iconst_m1
istore 1
goto L1
L2:
astore 2
aload 0
monitorexit
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public final read(Ljava/nio/CharBuffer;)I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aload 0
monitorenter
L0:
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokespecial com/a/b/i/af/a()V
aload 0
invokespecial com/a/b/i/af/b()Z
istore 5
L1:
iload 5
ifne L3
iconst_m1
istore 3
L7:
aload 0
monitorexit
iload 3
ireturn
L3:
aload 1
invokevirtual java/nio/CharBuffer/remaining()I
aload 0
invokespecial com/a/b/i/af/c()I
invokestatic java/lang/Math/min(II)I
istore 4
L4:
iconst_0
istore 2
L8:
iload 4
istore 3
iload 2
iload 4
if_icmpge L7
L5:
aload 0
getfield com/a/b/i/af/a Ljava/lang/CharSequence;
astore 6
aload 0
getfield com/a/b/i/af/b I
istore 3
aload 0
iload 3
iconst_1
iadd
putfield com/a/b/i/af/b I
aload 1
aload 6
iload 3
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokevirtual java/nio/CharBuffer/put(C)Ljava/nio/CharBuffer;
pop
L6:
iload 2
iconst_1
iadd
istore 2
goto L8
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 7
.limit stack 3
.end method

.method public final read([CII)I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aload 0
monitorenter
L0:
iload 2
iload 2
iload 3
iadd
aload 1
arraylength
invokestatic com/a/b/b/cn/a(III)V
aload 0
invokespecial com/a/b/i/af/a()V
aload 0
invokespecial com/a/b/i/af/b()Z
istore 6
L1:
iload 6
ifne L3
iconst_m1
istore 4
L7:
aload 0
monitorexit
iload 4
ireturn
L3:
iload 3
aload 0
invokespecial com/a/b/i/af/c()I
invokestatic java/lang/Math/min(II)I
istore 5
L4:
iconst_0
istore 3
L8:
iload 5
istore 4
iload 3
iload 5
if_icmpge L7
L5:
aload 0
getfield com/a/b/i/af/a Ljava/lang/CharSequence;
astore 7
aload 0
getfield com/a/b/i/af/b I
istore 4
aload 0
iload 4
iconst_1
iadd
putfield com/a/b/i/af/b I
aload 1
iload 2
iload 3
iadd
aload 7
iload 4
invokeinterface java/lang/CharSequence/charAt(I)C 1
castore
L6:
iload 3
iconst_1
iadd
istore 3
goto L8
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 8
.limit stack 4
.end method

.method public final ready()Z
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
invokespecial com/a/b/i/af/a()V
L1:
aload 0
monitorexit
iconst_1
ireturn
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 1
.end method

.method public final reset()V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
invokespecial com/a/b/i/af/a()V
aload 0
aload 0
getfield com/a/b/i/af/c I
putfield com/a/b/i/af/b I
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public final skip(J)J
.catch all from L0 to L1 using L2
iconst_1
istore 4
aload 0
monitorenter
lload 1
lconst_0
lcmp
iflt L3
L0:
iload 4
ldc "n (%s) may not be negative"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokespecial com/a/b/i/af/a()V
aload 0
invokespecial com/a/b/i/af/c()I
i2l
lload 1
invokestatic java/lang/Math/min(JJ)J
l2i
istore 3
aload 0
aload 0
getfield com/a/b/i/af/b I
iload 3
iadd
putfield com/a/b/i/af/b I
L1:
iload 3
i2l
lstore 1
aload 0
monitorexit
lload 1
lreturn
L3:
iconst_0
istore 4
goto L0
L2:
astore 5
aload 0
monitorexit
aload 5
athrow
.limit locals 6
.limit stack 7
.end method
