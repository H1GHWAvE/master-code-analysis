.bytecode 50.0
.class final synchronized com/a/b/i/i
.super com/a/b/i/b

.field private final 'a' Lcom/a/b/i/b;

.field private final 'b' Ljava/lang/String;

.field private final 'c' I

.field private final 'd' Lcom/a/b/b/m;

.method <init>(Lcom/a/b/i/b;Ljava/lang/String;I)V
aload 0
invokespecial com/a/b/i/b/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/i/b
putfield com/a/b/i/i/a Lcom/a/b/i/b;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/a/b/i/i/b Ljava/lang/String;
aload 0
iload 3
putfield com/a/b/i/i/c I
iload 3
ifle L0
iconst_1
istore 4
L1:
iload 4
ldc "Cannot add a separator after every %s chars"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 2
invokestatic com/a/b/b/m/a(Ljava/lang/CharSequence;)Lcom/a/b/b/m;
invokevirtual com/a/b/b/m/b()Lcom/a/b/b/m;
putfield com/a/b/i/i/d Lcom/a/b/b/m;
return
L0:
iconst_0
istore 4
goto L1
.limit locals 5
.limit stack 6
.end method

.method final a(I)I
aload 0
getfield com/a/b/i/i/a Lcom/a/b/i/b;
iload 1
invokevirtual com/a/b/i/b/a(I)I
istore 1
iload 1
aload 0
getfield com/a/b/i/i/b Ljava/lang/String;
invokevirtual java/lang/String/length()I
iconst_0
iload 1
iconst_1
isub
invokestatic java/lang/Math/max(II)I
aload 0
getfield com/a/b/i/i/c I
getstatic java/math/RoundingMode/FLOOR Ljava/math/RoundingMode;
invokestatic com/a/b/j/g/a(IILjava/math/RoundingMode;)I
imul
iadd
ireturn
.limit locals 2
.limit stack 5
.end method

.method final a()Lcom/a/b/b/m;
aload 0
getfield com/a/b/i/i/a Lcom/a/b/i/b;
invokevirtual com/a/b/i/b/a()Lcom/a/b/b/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(C)Lcom/a/b/i/b;
aload 0
getfield com/a/b/i/i/a Lcom/a/b/i/b;
iload 1
invokevirtual com/a/b/i/b/a(C)Lcom/a/b/i/b;
aload 0
getfield com/a/b/i/i/b Ljava/lang/String;
aload 0
getfield com/a/b/i/i/c I
invokevirtual com/a/b/i/b/a(Ljava/lang/String;I)Lcom/a/b/i/b;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final a(Ljava/lang/String;I)Lcom/a/b/i/b;
new java/lang/UnsupportedOperationException
dup
ldc "Already have a separator"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method final a(Lcom/a/b/i/bu;)Lcom/a/b/i/bs;
aload 0
getfield com/a/b/i/i/a Lcom/a/b/i/b;
astore 2
aload 0
getfield com/a/b/i/i/d Lcom/a/b/b/m;
astore 3
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
new com/a/b/i/e
dup
aload 1
aload 3
invokespecial com/a/b/i/e/<init>(Lcom/a/b/i/bu;Lcom/a/b/b/m;)V
invokevirtual com/a/b/i/b/a(Lcom/a/b/i/bu;)Lcom/a/b/i/bs;
areturn
.limit locals 4
.limit stack 5
.end method

.method final a(Lcom/a/b/i/bv;)Lcom/a/b/i/bt;
aload 0
getfield com/a/b/i/i/a Lcom/a/b/i/b;
astore 4
aload 0
getfield com/a/b/i/i/b Ljava/lang/String;
astore 5
aload 0
getfield com/a/b/i/i/c I
istore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 5
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 2
ifle L0
iconst_1
istore 3
L1:
iload 3
invokestatic com/a/b/b/cn/a(Z)V
aload 4
new com/a/b/i/f
dup
iload 2
aload 5
aload 1
invokespecial com/a/b/i/f/<init>(ILjava/lang/String;Lcom/a/b/i/bv;)V
invokevirtual com/a/b/i/b/a(Lcom/a/b/i/bv;)Lcom/a/b/i/bt;
areturn
L0:
iconst_0
istore 3
goto L1
.limit locals 6
.limit stack 6
.end method

.method final b(I)I
aload 0
getfield com/a/b/i/i/a Lcom/a/b/i/b;
iload 1
invokevirtual com/a/b/i/b/b(I)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b()Lcom/a/b/i/b;
aload 0
getfield com/a/b/i/i/a Lcom/a/b/i/b;
invokevirtual com/a/b/i/b/b()Lcom/a/b/i/b;
aload 0
getfield com/a/b/i/i/b Ljava/lang/String;
aload 0
getfield com/a/b/i/i/c I
invokevirtual com/a/b/i/b/a(Ljava/lang/String;I)Lcom/a/b/i/b;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final c()Lcom/a/b/i/b;
aload 0
getfield com/a/b/i/i/a Lcom/a/b/i/b;
invokevirtual com/a/b/i/b/c()Lcom/a/b/i/b;
aload 0
getfield com/a/b/i/i/b Ljava/lang/String;
aload 0
getfield com/a/b/i/i/c I
invokevirtual com/a/b/i/b/a(Ljava/lang/String;I)Lcom/a/b/i/b;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final d()Lcom/a/b/i/b;
aload 0
getfield com/a/b/i/i/a Lcom/a/b/i/b;
invokevirtual com/a/b/i/b/d()Lcom/a/b/i/b;
aload 0
getfield com/a/b/i/i/b Ljava/lang/String;
aload 0
getfield com/a/b/i/i/c I
invokevirtual com/a/b/i/b/a(Ljava/lang/String;I)Lcom/a/b/i/b;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/i/i/a Lcom/a/b/i/b;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
aload 0
getfield com/a/b/i/i/b Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 3
aload 0
getfield com/a/b/i/i/c I
istore 1
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
bipush 31
iadd
aload 3
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".withSeparator(\""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method
