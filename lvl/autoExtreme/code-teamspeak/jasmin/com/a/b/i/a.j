.bytecode 50.0
.class final synchronized com/a/b/i/a
.super java/io/Writer

.field private final 'a' Ljava/lang/Appendable;

.field private 'b' Z

.method <init>(Ljava/lang/Appendable;)V
aload 0
invokespecial java/io/Writer/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Appendable
putfield com/a/b/i/a/a Ljava/lang/Appendable;
return
.limit locals 2
.limit stack 2
.end method

.method private a()V
aload 0
getfield com/a/b/i/a/b Z
ifeq L0
new java/io/IOException
dup
ldc "Cannot write to a closed writer."
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 1
.limit stack 3
.end method

.method public final append(C)Ljava/io/Writer;
aload 0
invokespecial com/a/b/i/a/a()V
aload 0
getfield com/a/b/i/a/a Ljava/lang/Appendable;
iload 1
invokeinterface java/lang/Appendable/append(C)Ljava/lang/Appendable; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final append(Ljava/lang/CharSequence;)Ljava/io/Writer;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokespecial com/a/b/i/a/a()V
aload 0
getfield com/a/b/i/a/a Ljava/lang/Appendable;
aload 1
invokeinterface java/lang/Appendable/append(Ljava/lang/CharSequence;)Ljava/lang/Appendable; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final append(Ljava/lang/CharSequence;II)Ljava/io/Writer;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokespecial com/a/b/i/a/a()V
aload 0
getfield com/a/b/i/a/a Ljava/lang/Appendable;
aload 1
iload 2
iload 3
invokeinterface java/lang/Appendable/append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable; 3
pop
aload 0
areturn
.limit locals 4
.limit stack 4
.end method

.method public final volatile synthetic append(C)Ljava/lang/Appendable;
aload 0
iload 1
invokevirtual com/a/b/i/a/append(C)Ljava/io/Writer;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
aload 0
aload 1
invokevirtual com/a/b/i/a/append(Ljava/lang/CharSequence;)Ljava/io/Writer;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
aload 0
aload 1
iload 2
iload 3
invokevirtual com/a/b/i/a/append(Ljava/lang/CharSequence;II)Ljava/io/Writer;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final close()V
aload 0
iconst_1
putfield com/a/b/i/a/b Z
aload 0
getfield com/a/b/i/a/a Ljava/lang/Appendable;
instanceof java/io/Closeable
ifeq L0
aload 0
getfield com/a/b/i/a/a Ljava/lang/Appendable;
checkcast java/io/Closeable
invokeinterface java/io/Closeable/close()V 0
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final flush()V
aload 0
invokespecial com/a/b/i/a/a()V
aload 0
getfield com/a/b/i/a/a Ljava/lang/Appendable;
instanceof java/io/Flushable
ifeq L0
aload 0
getfield com/a/b/i/a/a Ljava/lang/Appendable;
checkcast java/io/Flushable
invokeinterface java/io/Flushable/flush()V 0
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public final write(I)V
aload 0
invokespecial com/a/b/i/a/a()V
aload 0
getfield com/a/b/i/a/a Ljava/lang/Appendable;
iload 1
i2c
invokeinterface java/lang/Appendable/append(C)Ljava/lang/Appendable; 1
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final write(Ljava/lang/String;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokespecial com/a/b/i/a/a()V
aload 0
getfield com/a/b/i/a/a Ljava/lang/Appendable;
aload 1
invokeinterface java/lang/Appendable/append(Ljava/lang/CharSequence;)Ljava/lang/Appendable; 1
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final write(Ljava/lang/String;II)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokespecial com/a/b/i/a/a()V
aload 0
getfield com/a/b/i/a/a Ljava/lang/Appendable;
aload 1
iload 2
iload 2
iload 3
iadd
invokeinterface java/lang/Appendable/append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable; 3
pop
return
.limit locals 4
.limit stack 5
.end method

.method public final write([CII)V
aload 0
invokespecial com/a/b/i/a/a()V
aload 0
getfield com/a/b/i/a/a Ljava/lang/Appendable;
new java/lang/String
dup
aload 1
iload 2
iload 3
invokespecial java/lang/String/<init>([CII)V
invokeinterface java/lang/Appendable/append(Ljava/lang/CharSequence;)Ljava/lang/Appendable; 1
pop
return
.limit locals 4
.limit stack 6
.end method
