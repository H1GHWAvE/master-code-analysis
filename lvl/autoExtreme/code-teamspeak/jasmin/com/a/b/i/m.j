.bytecode 50.0
.class public abstract interface com/a/b/i/m
.super java/lang/Object
.implements java/io/DataInput

.method public abstract readBoolean()Z
.end method

.method public abstract readByte()B
.end method

.method public abstract readChar()C
.end method

.method public abstract readDouble()D
.end method

.method public abstract readFloat()F
.end method

.method public abstract readFully([B)V
.end method

.method public abstract readFully([BII)V
.end method

.method public abstract readInt()I
.end method

.method public abstract readLine()Ljava/lang/String;
.end method

.method public abstract readLong()J
.end method

.method public abstract readShort()S
.end method

.method public abstract readUTF()Ljava/lang/String;
.end method

.method public abstract readUnsignedByte()I
.end method

.method public abstract readUnsignedShort()I
.end method

.method public abstract skipBytes(I)I
.end method
