.bytecode 50.0
.class public synchronized abstract com/a/b/i/ag
.super java/lang/Object

.method protected <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/Readable;)J
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L3 using L3
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 4
L0:
aload 4
aload 0
invokevirtual com/a/b/i/ag/a()Ljava/io/Writer;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/Writer
astore 5
aload 1
aload 5
invokestatic com/a/b/i/an/a(Ljava/lang/Readable;Ljava/lang/Appendable;)J
lstore 2
aload 5
invokevirtual java/io/Writer/flush()V
L1:
aload 4
invokevirtual com/a/b/i/ar/close()V
lload 2
lreturn
L2:
astore 1
L4:
aload 4
aload 1
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 1
aload 4
invokevirtual com/a/b/i/ar/close()V
aload 1
athrow
.limit locals 6
.limit stack 2
.end method

.method private a(Ljava/lang/Iterable;)V
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Throwable from L1 to L4 using L2
.catch all from L1 to L4 using L3
.catch java/lang/Throwable from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch all from L6 to L3 using L3
.catch java/lang/Throwable from L7 to L8 using L2
.catch all from L7 to L8 using L3
.catch java/lang/Throwable from L9 to L10 using L2
.catch all from L9 to L10 using L3
ldc "line.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
astore 4
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 3
L0:
aload 0
invokevirtual com/a/b/i/ag/a()Ljava/io/Writer;
astore 2
aload 2
instanceof java/io/BufferedWriter
ifeq L7
aload 2
checkcast java/io/BufferedWriter
astore 2
L1:
aload 3
aload 2
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/Writer
astore 2
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
L4:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L9
aload 2
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/CharSequence
invokevirtual java/io/Writer/append(Ljava/lang/CharSequence;)Ljava/io/Writer;
aload 4
invokevirtual java/io/Writer/append(Ljava/lang/CharSequence;)Ljava/io/Writer;
pop
L5:
goto L4
L2:
astore 1
L6:
aload 3
aload 1
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 1
aload 3
invokevirtual com/a/b/i/ar/close()V
aload 1
athrow
L7:
new java/io/BufferedWriter
dup
aload 2
invokespecial java/io/BufferedWriter/<init>(Ljava/io/Writer;)V
astore 2
L8:
goto L1
L9:
aload 2
invokevirtual java/io/Writer/flush()V
L10:
aload 3
invokevirtual com/a/b/i/ar/close()V
return
.limit locals 5
.limit stack 3
.end method

.method private a(Ljava/lang/Iterable;Ljava/lang/String;)V
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Throwable from L1 to L4 using L2
.catch all from L1 to L4 using L3
.catch java/lang/Throwable from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch all from L6 to L3 using L3
.catch java/lang/Throwable from L7 to L8 using L2
.catch all from L7 to L8 using L3
.catch java/lang/Throwable from L9 to L10 using L2
.catch all from L9 to L10 using L3
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 4
L0:
aload 0
invokevirtual com/a/b/i/ag/a()Ljava/io/Writer;
astore 3
aload 3
instanceof java/io/BufferedWriter
ifeq L7
aload 3
checkcast java/io/BufferedWriter
astore 3
L1:
aload 4
aload 3
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/Writer
astore 3
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
L4:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L9
aload 3
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/CharSequence
invokevirtual java/io/Writer/append(Ljava/lang/CharSequence;)Ljava/io/Writer;
aload 2
invokevirtual java/io/Writer/append(Ljava/lang/CharSequence;)Ljava/io/Writer;
pop
L5:
goto L4
L2:
astore 1
L6:
aload 4
aload 1
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 1
aload 4
invokevirtual com/a/b/i/ar/close()V
aload 1
athrow
L7:
new java/io/BufferedWriter
dup
aload 3
invokespecial java/io/BufferedWriter/<init>(Ljava/io/Writer;)V
astore 3
L8:
goto L1
L9:
aload 3
invokevirtual java/io/Writer/flush()V
L10:
aload 4
invokevirtual com/a/b/i/ar/close()V
return
.limit locals 5
.limit stack 3
.end method

.method private b()Ljava/io/Writer;
aload 0
invokevirtual com/a/b/i/ag/a()Ljava/io/Writer;
astore 1
aload 1
instanceof java/io/BufferedWriter
ifeq L0
aload 1
checkcast java/io/BufferedWriter
areturn
L0:
new java/io/BufferedWriter
dup
aload 1
invokespecial java/io/BufferedWriter/<init>(Ljava/io/Writer;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public abstract a()Ljava/io/Writer;
.end method

.method public final a(Ljava/lang/CharSequence;)V
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L3 using L3
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 2
L0:
aload 2
aload 0
invokevirtual com/a/b/i/ag/a()Ljava/io/Writer;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/Writer
astore 3
aload 3
aload 1
invokevirtual java/io/Writer/append(Ljava/lang/CharSequence;)Ljava/io/Writer;
pop
aload 3
invokevirtual java/io/Writer/flush()V
L1:
aload 2
invokevirtual com/a/b/i/ar/close()V
return
L2:
astore 1
L4:
aload 2
aload 1
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 1
aload 2
invokevirtual com/a/b/i/ar/close()V
aload 1
athrow
.limit locals 4
.limit stack 2
.end method
