.bytecode 50.0
.class public final synchronized com/a/b/i/an
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'a' I = 2048


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/Readable;Ljava/lang/Appendable;)J
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
sipush 2048
invokestatic java/nio/CharBuffer/allocate(I)Ljava/nio/CharBuffer;
astore 4
lconst_0
lstore 2
L0:
aload 0
aload 4
invokeinterface java/lang/Readable/read(Ljava/nio/CharBuffer;)I 1
iconst_m1
if_icmpeq L1
aload 4
invokevirtual java/nio/CharBuffer/flip()Ljava/nio/Buffer;
pop
aload 1
aload 4
invokeinterface java/lang/Appendable/append(Ljava/lang/CharSequence;)Ljava/lang/Appendable; 1
pop
lload 2
aload 4
invokevirtual java/nio/CharBuffer/remaining()I
i2l
ladd
lstore 2
aload 4
invokevirtual java/nio/CharBuffer/clear()Ljava/nio/Buffer;
pop
goto L0
L1:
lload 2
lreturn
.limit locals 5
.limit stack 4
.end method

.method private static a()Ljava/io/Writer;
invokestatic com/a/b/i/ap/a()Lcom/a/b/i/ap;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Ljava/lang/Appendable;)Ljava/io/Writer;
aload 0
instanceof java/io/Writer
ifeq L0
aload 0
checkcast java/io/Writer
areturn
L0:
new com/a/b/i/a
dup
aload 0
invokespecial com/a/b/i/a/<init>(Ljava/lang/Appendable;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/Readable;Lcom/a/b/i/by;)Ljava/lang/Object;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/i/bz
dup
aload 0
invokespecial com/a/b/i/bz/<init>(Ljava/lang/Readable;)V
astore 0
L0:
aload 0
invokevirtual com/a/b/i/bz/a()Ljava/lang/String;
astore 2
aload 2
ifnull L1
aload 1
aload 2
invokeinterface com/a/b/i/by/a(Ljava/lang/String;)Z 1
pop
goto L0
L1:
aload 1
invokeinterface com/a/b/i/by/a()Ljava/lang/Object; 0
areturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Readable;)Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
aload 0
aload 1
invokestatic com/a/b/i/an/a(Ljava/lang/Readable;Ljava/lang/Appendable;)J
pop2
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/io/Reader;J)V
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
lload 1
lconst_0
lcmp
ifle L1
aload 0
lload 1
invokevirtual java/io/Reader/skip(J)J
lstore 3
lload 3
lconst_0
lcmp
ifne L2
aload 0
invokevirtual java/io/Reader/read()I
iconst_m1
if_icmpne L3
new java/io/EOFException
dup
invokespecial java/io/EOFException/<init>()V
athrow
L3:
lload 1
lconst_1
lsub
lstore 1
goto L0
L2:
lload 1
lload 3
lsub
lstore 1
goto L0
L1:
return
.limit locals 5
.limit stack 4
.end method

.method private static b(Ljava/lang/Readable;)Ljava/lang/StringBuilder;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
aload 0
aload 1
invokestatic com/a/b/i/an/a(Ljava/lang/Readable;Ljava/lang/Appendable;)J
pop2
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/lang/Readable;)Ljava/util/List;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 1
new com/a/b/i/bz
dup
aload 0
invokespecial com/a/b/i/bz/<init>(Ljava/lang/Readable;)V
astore 0
L0:
aload 0
invokevirtual com/a/b/i/bz/a()Ljava/lang/String;
astore 2
aload 2
ifnull L1
aload 1
aload 2
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L1:
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method private static d(Ljava/lang/Readable;)Ljava/io/Reader;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
instanceof java/io/Reader
ifeq L0
aload 0
checkcast java/io/Reader
areturn
L0:
new com/a/b/i/ao
dup
aload 0
invokespecial com/a/b/i/ao/<init>(Ljava/lang/Readable;)V
areturn
.limit locals 1
.limit stack 3
.end method
