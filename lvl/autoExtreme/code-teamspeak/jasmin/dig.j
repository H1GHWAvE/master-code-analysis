.bytecode 50.0
.class public synchronized dig
.super java/lang/Object

.field static 'a' Lorg/xbill/DNS/Name;

.field static 'b' I

.field static 'c' I

.method static <clinit>()V
aconst_null
putstatic dig/a Lorg/xbill/DNS/Name;
iconst_1
putstatic dig/b I
iconst_1
putstatic dig/c I
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a()V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Usage: dig [@server] name [<type>] [<class>] [options]"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
invokestatic java/lang/System/exit(I)V
return
.limit locals 0
.limit stack 2
.end method

.method private static a(Lorg/xbill/DNS/Message;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "; java dig 0.0 <> "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
getstatic dig/a Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc " axfr"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokevirtual org/xbill/DNS/Message/isSigned()Z
ifeq L0
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc ";; TSIG "
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
aload 0
invokevirtual org/xbill/DNS/Message/isVerified()Z
ifeq L1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "ok"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
aload 0
invokevirtual org/xbill/DNS/Message/getRcode()I
ifeq L2
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
return
L1:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "failed"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L0
L2:
aload 0
iconst_1
invokevirtual org/xbill/DNS/Message/getSectionArray(I)[Lorg/xbill/DNS/Record;
astore 2
iconst_0
istore 1
L3:
iload 1
aload 2
arraylength
if_icmpge L4
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 2
iload 1
aaload
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
iload 1
iconst_1
iadd
istore 1
goto L3
L4:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc ";; done ("
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
iconst_1
invokevirtual org/xbill/DNS/Header/getCount(I)I
invokevirtual java/io/PrintStream/print(I)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc " records, "
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
iconst_3
invokevirtual org/xbill/DNS/Header/getCount(I)I
invokevirtual java/io/PrintStream/print(I)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc " additional)"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method private static a(Lorg/xbill/DNS/Message;J)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "; java dig 0.0"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc ";; Query time: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc " ms"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method public static main([Ljava/lang/String;)V
.catch java/lang/ArrayIndexOutOfBoundsException from L0 to L1 using L2
.catch java/lang/ArrayIndexOutOfBoundsException from L3 to L4 using L2
.catch java/lang/ArrayIndexOutOfBoundsException from L5 to L6 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L8 to L9 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L10 to L11 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L12 to L13 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L14 to L15 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L16 to L17 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L18 to L19 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L20 to L21 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L22 to L23 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L24 to L25 using L2
.catch java/lang/ArrayIndexOutOfBoundsException from L26 to L27 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L28 to L29 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L30 to L31 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L32 to L33 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L34 to L35 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L36 to L37 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L38 to L39 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L40 to L41 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L42 to L43 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L44 to L45 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L46 to L47 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L48 to L49 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L50 to L51 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L52 to L53 using L7
.catch java/lang/Exception from L54 to L55 using L56
.catch java/lang/ArrayIndexOutOfBoundsException from L54 to L55 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L57 to L58 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L59 to L60 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L61 to L62 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L63 to L64 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L65 to L66 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L67 to L68 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L69 to L70 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L71 to L72 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L73 to L74 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L75 to L76 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L77 to L78 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L79 to L80 using L7
.catch java/lang/ArrayIndexOutOfBoundsException from L81 to L82 using L7
iconst_0
istore 3
iconst_0
istore 4
iconst_0
istore 2
aload 0
arraylength
ifgt L0
invokestatic dig/a()V
L0:
aload 0
iconst_0
aaload
ldc "@"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L83
aload 0
iconst_0
aaload
iconst_1
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 10
L1:
iconst_1
istore 1
L84:
aload 10
ifnull L24
L3:
new org/xbill/DNS/SimpleResolver
dup
aload 10
invokespecial org/xbill/DNS/SimpleResolver/<init>(Ljava/lang/String;)V
astore 10
L4:
iload 1
iconst_1
iadd
istore 5
aload 0
iload 1
aaload
astore 11
iload 4
istore 3
L5:
aload 11
ldc "-x"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L85
L6:
iload 5
iconst_1
iadd
istore 1
iload 4
istore 3
L8:
aload 0
iload 5
aaload
invokestatic org/xbill/DNS/ReverseMap/fromAddress(Ljava/lang/String;)Lorg/xbill/DNS/Name;
putstatic dig/a Lorg/xbill/DNS/Name;
L9:
iload 4
istore 3
L10:
bipush 12
putstatic dig/b I
L11:
iload 4
istore 3
L12:
iconst_1
putstatic dig/c I
L13:
iload 2
istore 3
L14:
aload 0
iload 1
aaload
ldc "-"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L86
L15:
iload 2
istore 3
L16:
aload 0
iload 1
aaload
invokevirtual java/lang/String/length()I
iconst_1
if_icmple L86
L17:
iload 2
istore 3
L18:
aload 0
iload 1
aaload
iconst_1
invokevirtual java/lang/String/charAt(I)C
tableswitch 98
L87
L19
L88
L89
L19
L19
L19
L90
L19
L91
L19
L19
L19
L19
L92
L93
L19
L19
L94
default : L95
L19:
iload 2
istore 3
L20:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Invalid option: "
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
L21:
iload 2
istore 3
L22:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
iload 1
aaload
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L23:
iload 1
iconst_1
iadd
istore 1
goto L13
L24:
new org/xbill/DNS/SimpleResolver
dup
invokespecial org/xbill/DNS/SimpleResolver/<init>()V
astore 10
L25:
goto L4
L2:
astore 0
aconst_null
astore 0
iload 3
istore 1
L96:
getstatic dig/a Lorg/xbill/DNS/Name;
ifnonnull L97
invokestatic dig/a()V
L98:
aload 0
astore 10
aload 0
ifnonnull L99
new org/xbill/DNS/SimpleResolver
dup
invokespecial org/xbill/DNS/SimpleResolver/<init>()V
astore 10
L99:
getstatic dig/a Lorg/xbill/DNS/Name;
getstatic dig/b I
getstatic dig/c I
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Record;
invokestatic org/xbill/DNS/Message/newQuery(Lorg/xbill/DNS/Record;)Lorg/xbill/DNS/Message;
astore 0
iload 1
ifeq L100
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
L100:
invokestatic java/lang/System/currentTimeMillis()J
lstore 6
aload 10
aload 0
invokevirtual org/xbill/DNS/SimpleResolver/send(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/Message;
astore 0
invokestatic java/lang/System/currentTimeMillis()J
lstore 8
getstatic dig/b I
sipush 252
if_icmpne L101
aload 0
invokestatic dig/a(Lorg/xbill/DNS/Message;)V
return
L85:
iload 4
istore 3
L26:
aload 11
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putstatic dig/a Lorg/xbill/DNS/Name;
L27:
iload 4
istore 3
L28:
aload 0
iload 5
aaload
invokestatic org/xbill/DNS/Type/value(Ljava/lang/String;)I
istore 1
L29:
iload 4
istore 3
L30:
iload 1
putstatic dig/b I
L31:
iload 1
ifge L102
iload 4
istore 3
L32:
iconst_1
putstatic dig/b I
L33:
iload 5
istore 1
L103:
iload 4
istore 3
L34:
aload 0
iload 1
aaload
invokestatic org/xbill/DNS/DClass/value(Ljava/lang/String;)I
istore 5
L35:
iload 4
istore 3
L36:
iload 5
putstatic dig/c I
L37:
iload 5
ifge L104
iload 4
istore 3
L38:
iconst_1
putstatic dig/c I
L39:
goto L13
L92:
iload 2
istore 3
L40:
aload 0
iload 1
aaload
invokevirtual java/lang/String/length()I
iconst_2
if_icmple L105
L41:
iload 2
istore 3
L42:
aload 0
iload 1
aaload
iconst_2
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 11
L43:
iload 2
istore 3
L44:
aload 11
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 4
L45:
iload 4
iflt L106
iload 4
ldc_w 65536
if_icmple L107
L106:
iload 2
istore 3
L46:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Invalid port"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L47:
return
L107:
iload 2
istore 3
L48:
aload 10
iload 4
invokevirtual org/xbill/DNS/SimpleResolver/setPort(I)V
L49:
goto L23
L87:
iload 2
istore 3
L50:
aload 0
iload 1
aaload
invokevirtual java/lang/String/length()I
iconst_2
if_icmple L108
L51:
iload 2
istore 3
L52:
aload 0
iload 1
aaload
iconst_2
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 11
L53:
iload 2
istore 3
L54:
aload 11
invokestatic java/net/InetAddress/getByName(Ljava/lang/String;)Ljava/net/InetAddress;
astore 11
L55:
iload 2
istore 3
L57:
aload 10
aload 11
invokevirtual org/xbill/DNS/SimpleResolver/setLocalAddress(Ljava/net/InetAddress;)V
L58:
goto L23
L56:
astore 0
iload 2
istore 3
L59:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Invalid address"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L60:
return
L91:
iload 2
istore 3
L61:
aload 0
iload 1
aaload
invokevirtual java/lang/String/length()I
iconst_2
if_icmple L109
L62:
iload 2
istore 3
L63:
aload 0
iload 1
aaload
iconst_2
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 11
L64:
iload 2
istore 3
L65:
aload 10
aload 11
invokestatic org/xbill/DNS/TSIG/fromString(Ljava/lang/String;)Lorg/xbill/DNS/TSIG;
invokevirtual org/xbill/DNS/SimpleResolver/setTSIGKey(Lorg/xbill/DNS/TSIG;)V
L66:
goto L23
L94:
iload 2
istore 3
L67:
aload 10
iconst_1
invokevirtual org/xbill/DNS/SimpleResolver/setTCP(Z)V
L68:
goto L23
L90:
iload 2
istore 3
L69:
aload 10
iconst_1
invokevirtual org/xbill/DNS/SimpleResolver/setIgnoreTruncation(Z)V
L70:
goto L23
L89:
iload 2
istore 3
L71:
aload 0
iload 1
aaload
invokevirtual java/lang/String/length()I
iconst_2
if_icmple L110
L72:
iload 2
istore 3
L73:
aload 0
iload 1
aaload
iconst_2
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 11
L74:
iload 2
istore 3
L75:
aload 11
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 4
L76:
iload 4
iflt L111
iload 4
iconst_1
if_icmple L112
L111:
iload 2
istore 3
L77:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "Unsupported EDNS level: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 4
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L78:
return
L112:
iload 2
istore 3
L79:
aload 10
iload 4
invokevirtual org/xbill/DNS/SimpleResolver/setEDNS(I)V
L80:
goto L23
L88:
iload 2
istore 3
L81:
aload 10
iconst_0
iconst_0
ldc_w 32768
aconst_null
invokevirtual org/xbill/DNS/SimpleResolver/setEDNS(IIILjava/util/List;)V
L82:
goto L23
L93:
iconst_1
istore 2
goto L23
L86:
iload 2
istore 1
aload 10
astore 0
goto L98
L101:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "; java dig 0.0"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc ";; Query time: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
lload 8
lload 6
lsub
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc " ms"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
L97:
goto L98
L83:
iconst_0
istore 1
aconst_null
astore 10
goto L84
L95:
goto L19
L7:
astore 0
aload 10
astore 0
iload 3
istore 1
goto L96
L102:
iload 5
iconst_1
iadd
istore 1
goto L103
L104:
iload 1
iconst_1
iadd
istore 1
goto L13
L105:
iload 1
iconst_1
iadd
istore 1
aload 0
iload 1
aaload
astore 11
goto L43
L108:
iload 1
iconst_1
iadd
istore 1
aload 0
iload 1
aaload
astore 11
goto L53
L109:
iload 1
iconst_1
iadd
istore 1
aload 0
iload 1
aaload
astore 11
goto L64
L110:
iload 1
iconst_1
iadd
istore 1
aload 0
iload 1
aaload
astore 11
goto L74
.limit locals 12
.limit stack 6
.end method
