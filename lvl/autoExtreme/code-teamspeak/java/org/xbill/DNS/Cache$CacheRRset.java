package org.xbill.DNS;
 class Cache$CacheRRset extends org.xbill.DNS.RRset implements org.xbill.DNS.Cache$Element {
    private static final long serialVersionUID = 5971755205903597024;
    int credibility;
    int expire;

    public Cache$CacheRRset(org.xbill.DNS.RRset p4, int p5, long p6)
    {
        this(p4);
        this.credibility = p5;
        this.expire = org.xbill.DNS.Cache.access$000(p4.getTTL(), p6);
        return;
    }

    public Cache$CacheRRset(org.xbill.DNS.Record p4, int p5, long p6)
    {
        this.credibility = p5;
        this.expire = org.xbill.DNS.Cache.access$000(p4.getTTL(), p6);
        this.addRR(p4);
        return;
    }

    public final int compareCredibility(int p2)
    {
        return (this.credibility - p2);
    }

    public final boolean expired()
    {
        int v0_3;
        if (((int) (System.currentTimeMillis() / 1000)) < this.expire) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public String toString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(super.toString());
        v0_1.append(" cl = ");
        v0_1.append(this.credibility);
        return v0_1.toString();
    }
}
