package org.xbill.DNS;
abstract class KEYBase extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 3469321722693285454;
    protected int alg;
    protected int flags;
    protected int footprint;
    protected byte[] key;
    protected int proto;
    protected java.security.PublicKey publicKey;

    protected KEYBase()
    {
        this.footprint = -1;
        this.publicKey = 0;
        return;
    }

    public KEYBase(org.xbill.DNS.Name p3, int p4, int p5, long p6, int p8, int p9, int p10, byte[] p11)
    {
        org.xbill.DNS.KEYBase v2_1 = this(p3, p4, p5, p6);
        v2_1.footprint = -1;
        v2_1.publicKey = 0;
        v2_1.flags = org.xbill.DNS.KEYBase.checkU16("flags", p8);
        v2_1.proto = org.xbill.DNS.KEYBase.checkU8("proto", p9);
        v2_1.alg = org.xbill.DNS.KEYBase.checkU8("alg", p10);
        v2_1.key = p11;
        return;
    }

    public int getAlgorithm()
    {
        return this.alg;
    }

    public int getFlags()
    {
        return this.flags;
    }

    public int getFootprint()
    {
        int v0_13;
        int v0_0 = 0;
        if (this.footprint < 0) {
            int v0_6;
            int v1_2 = new org.xbill.DNS.DNSOutput();
            this.rrToWire(v1_2, 0, 0);
            byte[] v2_1 = v1_2.toByteArray();
            if (this.alg != 1) {
                int v1_4 = 0;
                while (v0_0 < (v2_1.length - 1)) {
                    v1_4 += (((v2_1[v0_0] & 255) << 8) + (v2_1[(v0_0 + 1)] & 255));
                    v0_0 += 2;
                }
                if (v0_0 < v2_1.length) {
                    v1_4 += ((v2_1[v0_0] & 255) << 8);
                }
                v0_6 = (((v1_4 >> 16) & 65535) + v1_4);
            } else {
                v0_6 = (((v2_1[(v2_1.length - 3)] & 255) << 8) + (v2_1[(v2_1.length - 2)] & 255));
            }
            this.footprint = (v0_6 & 65535);
            v0_13 = this.footprint;
        } else {
            v0_13 = this.footprint;
        }
        return v0_13;
    }

    public byte[] getKey()
    {
        return this.key;
    }

    public int getProtocol()
    {
        return this.proto;
    }

    public java.security.PublicKey getPublicKey()
    {
        java.security.PublicKey v0_2;
        if (this.publicKey == null) {
            this.publicKey = org.xbill.DNS.DNSSEC.toPublicKey(this);
            v0_2 = this.publicKey;
        } else {
            v0_2 = this.publicKey;
        }
        return v0_2;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.flags = p2.readU16();
        this.proto = p2.readU8();
        this.alg = p2.readU8();
        if (p2.remaining() > 0) {
            this.key = p2.readByteArray();
        }
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.flags);
        v0_1.append(" ");
        v0_1.append(this.proto);
        v0_1.append(" ");
        v0_1.append(this.alg);
        if (this.key != null) {
            if (!org.xbill.DNS.Options.check("multiline")) {
                v0_1.append(" ");
                v0_1.append(org.xbill.DNS.utils.base64.toString(this.key));
            } else {
                v0_1.append(" (\n");
                v0_1.append(org.xbill.DNS.utils.base64.formatString(this.key, 64, "\t", 1));
                v0_1.append(" ; key_tag = ");
                v0_1.append(this.getFootprint());
            }
        }
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeU16(this.flags);
        p2.writeU8(this.proto);
        p2.writeU8(this.alg);
        if (this.key != null) {
            p2.writeByteArray(this.key);
        }
        return;
    }
}
