package org.xbill.DNS;
final class TypeBitmap implements java.io.Serializable {
    private static final long serialVersionUID = 18321390015974162613;
    private java.util.TreeSet types;

    private TypeBitmap()
    {
        this.types = new java.util.TreeSet();
        return;
    }

    public TypeBitmap(org.xbill.DNS.DNSInput p9)
    {
        while (p9.remaining() > 0) {
            if (p9.remaining() >= 2) {
                int v3 = p9.readU8();
                if (v3 >= -1) {
                    int v4 = p9.readU8();
                    if (v4 <= p9.remaining()) {
                        int v2_1 = 0;
                        while (v2_1 < v4) {
                            int v5 = p9.readU8();
                            if (v5 != 0) {
                                int v0_4 = 0;
                                while (v0_4 < 8) {
                                    if (((1 << (7 - v0_4)) & v5) != 0) {
                                        this.types.add(org.xbill.DNS.Mnemonic.toInteger((((v3 * 256) + (v2_1 * 8)) + v0_4)));
                                    }
                                    v0_4++;
                                }
                            }
                            v2_1++;
                        }
                    } else {
                        throw new org.xbill.DNS.WireParseException("invalid bitmap");
                    }
                } else {
                    throw new org.xbill.DNS.WireParseException("invalid ordering");
                }
            } else {
                throw new org.xbill.DNS.WireParseException("invalid bitmap descriptor");
            }
        }
        return;
    }

    public TypeBitmap(org.xbill.DNS.Tokenizer p4)
    {
        while(true) {
            java.util.TreeSet v0_0 = p4.get();
            if (!v0_0.isString()) {
                p4.unget();
                return;
            } else {
                Integer v1_2 = org.xbill.DNS.Type.value(v0_0.value);
                if (v1_2 < null) {
                    break;
                }
                this.types.add(org.xbill.DNS.Mnemonic.toInteger(v1_2));
            }
        }
        throw p4.exception(new StringBuffer("Invalid type: ").append(v0_0.value).toString());
    }

    public TypeBitmap(int[] p5)
    {
        int v0 = 0;
        while (v0 < p5.length) {
            org.xbill.DNS.Type.check(p5[v0]);
            this.types.add(new Integer(p5[v0]));
            v0++;
        }
        return;
    }

    private static void mapToWire(org.xbill.DNS.DNSOutput p7, java.util.TreeSet p8, int p9)
    {
        int v1 = (((((Integer) p8.last()).intValue() & 255) / 8) + 1);
        int[] v2 = new int[v1];
        p7.writeU8(p9);
        p7.writeU8(v1);
        int v3_0 = p8.iterator();
        while (v3_0.hasNext()) {
            int v0_9 = ((Integer) v3_0.next()).intValue();
            int v4_1 = ((v0_9 & 255) / 8);
            v2[v4_1] = ((1 << (7 - (v0_9 % 8))) | v2[v4_1]);
        }
        int v0_6 = 0;
        while (v0_6 < v1) {
            p7.writeU8(v2[v0_6]);
            v0_6++;
        }
        return;
    }

    public final boolean contains(int p3)
    {
        return this.types.contains(org.xbill.DNS.Mnemonic.toInteger(p3));
    }

    public final boolean empty()
    {
        return this.types.isEmpty();
    }

    public final int[] toArray()
    {
        int[] v3 = new int[this.types.size()];
        java.util.Iterator v4 = this.types.iterator();
        int v1_1 = 0;
        while (v4.hasNext()) {
            int v2 = (v1_1 + 1);
            v3[v1_1] = ((Integer) v4.next()).intValue();
            v1_1 = v2;
        }
        return v3;
    }

    public final String toString()
    {
        StringBuffer v1_1 = new StringBuffer();
        java.util.Iterator v2 = this.types.iterator();
        while (v2.hasNext()) {
            v1_1.append(org.xbill.DNS.Type.string(((Integer) v2.next()).intValue()));
            if (v2.hasNext()) {
                v1_1.append(32);
            }
        }
        return v1_1.toString();
    }

    public final void toWire(org.xbill.DNS.DNSOutput p7)
    {
        if (this.types.size() != 0) {
            java.util.TreeSet v2_1 = new java.util.TreeSet();
            java.util.Iterator v3 = this.types.iterator();
            int v1_1 = -1;
            while (v3.hasNext()) {
                int v4 = ((Integer) v3.next()).intValue();
                int v0_6 = (v4 >> 8);
                if (v0_6 == v1_1) {
                    v0_6 = v1_1;
                } else {
                    if (v2_1.size() > 0) {
                        org.xbill.DNS.TypeBitmap.mapToWire(p7, v2_1, v1_1);
                        v2_1.clear();
                    }
                }
                v2_1.add(new Integer(v4));
                v1_1 = v0_6;
            }
            org.xbill.DNS.TypeBitmap.mapToWire(p7, v2_1, v1_1);
        }
        return;
    }
}
