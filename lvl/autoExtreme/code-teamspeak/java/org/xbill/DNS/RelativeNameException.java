package org.xbill.DNS;
public class RelativeNameException extends java.lang.IllegalArgumentException {

    public RelativeNameException(String p1)
    {
        this(p1);
        return;
    }

    public RelativeNameException(org.xbill.DNS.Name p3)
    {
        this(new StringBuffer("\'").append(p3).append("\' is not an absolute name").toString());
        return;
    }
}
