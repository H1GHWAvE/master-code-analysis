package org.xbill.DNS;
public class APLRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 17098570281996615752;
    private java.util.List elements;

    APLRecord()
    {
        return;
    }

    public APLRecord(org.xbill.DNS.Name p8, int p9, long p10, java.util.List p12)
    {
        this(p8, 42, p9, p10);
        this.elements = new java.util.ArrayList(p12.size());
        String v1_2 = p12.iterator();
        while (v1_2.hasNext()) {
            IllegalArgumentException v0_4 = v1_2.next();
            if ((v0_4 instanceof org.xbill.DNS.APLRecord$Element)) {
                IllegalArgumentException v0_5 = ((org.xbill.DNS.APLRecord$Element) v0_4);
                if ((v0_5.family == 1) || (v0_5.family == 2)) {
                    this.elements.add(v0_5);
                } else {
                    throw new IllegalArgumentException("unknown family");
                }
            } else {
                throw new IllegalArgumentException("illegal element");
            }
        }
        return;
    }

    static boolean access$000(int p1, int p2)
    {
        return org.xbill.DNS.APLRecord.validatePrefixLength(p1, p2);
    }

    private static int addressLength(byte[] p2)
    {
        int v0_1 = (p2.length - 1);
        while (v0_1 >= 0) {
            if (p2[v0_1] == 0) {
                v0_1--;
            } else {
                int v0_2 = (v0_1 + 1);
            }
            return v0_2;
        }
        v0_2 = 0;
        return v0_2;
    }

    private static byte[] parseAddress(byte[] p3, int p4)
    {
        if (p3.length <= p4) {
            if (p3.length != p4) {
                byte[] v0_2 = new byte[p4];
                System.arraycopy(p3, 0, v0_2, 0, p3.length);
                p3 = v0_2;
            }
            return p3;
        } else {
            throw new org.xbill.DNS.WireParseException("invalid address length");
        }
    }

    private static boolean validatePrefixLength(int p3, int p4)
    {
        int v0 = 0;
        if (((p4 >= 0) && ((p4 < 256) && ((p3 != 1) || (p4 <= 32)))) && ((p3 != 2) || (p4 <= 128))) {
            v0 = 1;
        }
        return v0;
    }

    public java.util.List getElements()
    {
        return this.elements;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.APLRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p8, org.xbill.DNS.Name p9)
    {
        this.elements = new java.util.ArrayList(1);
        while(true) {
            java.net.InetAddress v0_2 = p8.get();
            if (!v0_2.isString()) {
                p8.unget();
                return;
            } else {
                java.net.InetAddress v0_5;
                int v3_1;
                int v4_0 = v0_2.value;
                if (!v4_0.startsWith("!")) {
                    v0_5 = 0;
                    v3_1 = 0;
                } else {
                    v0_5 = 1;
                    v3_1 = 1;
                }
                java.util.List v5_1 = v4_0.indexOf(58, v0_5);
                if (v5_1 >= null) {
                    org.xbill.DNS.APLRecord$Element v6_1 = v4_0.indexOf(47, v5_1);
                    if (v6_1 >= null) {
                        java.net.InetAddress v0_6 = v4_0.substring(v0_5, v5_1);
                        java.util.List v5_3 = v4_0.substring((v5_1 + 1), v6_1);
                        int v4_1 = v4_0.substring((v6_1 + 1));
                        try {
                            java.net.InetAddress v0_7 = Integer.parseInt(v0_6);
                        } catch (java.net.InetAddress v0) {
                            throw p8.exception("invalid family");
                        }
                        if ((v0_7 == 1) || (v0_7 == 2)) {
                            try {
                                int v4_2 = Integer.parseInt(v4_1);
                            } catch (java.net.InetAddress v0) {
                                throw p8.exception("invalid prefix length");
                            }
                            if (org.xbill.DNS.APLRecord.validatePrefixLength(v0_7, v4_2)) {
                                java.net.InetAddress v0_12 = org.xbill.DNS.Address.toByteArray(v5_3, v0_7);
                                if (v0_12 == null) {
                                    break;
                                }
                                this.elements.add(new org.xbill.DNS.APLRecord$Element(v3_1, java.net.InetAddress.getByAddress(v0_12), v4_2));
                            } else {
                                throw p8.exception("invalid prefix length");
                            }
                        } else {
                            throw p8.exception("unknown family");
                        }
                    } else {
                        throw p8.exception("invalid address prefix element");
                    }
                } else {
                    throw p8.exception("invalid address prefix element");
                }
            }
        }
        throw p8.exception(new StringBuffer("invalid IP address ").append(v5_3).toString());
    }

    void rrFromWire(org.xbill.DNS.DNSInput p8)
    {
        this.elements = new java.util.ArrayList(1);
        while (p8.remaining() != 0) {
            int v2_1;
            java.util.List v1_0 = p8.readU16();
            int v4 = p8.readU8();
            org.xbill.DNS.APLRecord$Element v0_3 = p8.readU8();
            if ((v0_3 & 128) == 0) {
                v2_1 = 0;
            } else {
                v2_1 = 1;
            }
            byte[] v3 = p8.readByteArray((v0_3 & -129));
            if (org.xbill.DNS.APLRecord.validatePrefixLength(v1_0, v4)) {
                if ((v1_0 != 1) && (v1_0 != 2)) {
                    org.xbill.DNS.APLRecord$Element v0_8 = new org.xbill.DNS.APLRecord$Element(v1_0, v2_1, v3, v4, 0);
                } else {
                    v0_8 = new org.xbill.DNS.APLRecord$Element(v2_1, java.net.InetAddress.getByAddress(org.xbill.DNS.APLRecord.parseAddress(v3, org.xbill.DNS.Address.addressLength(v1_0))), v4);
                }
                this.elements.add(v0_8);
            } else {
                throw new org.xbill.DNS.WireParseException("invalid prefix length");
            }
        }
        return;
    }

    String rrToString()
    {
        StringBuffer v1_1 = new StringBuffer();
        java.util.Iterator v2 = this.elements.iterator();
        while (v2.hasNext()) {
            v1_1.append(((org.xbill.DNS.APLRecord$Element) v2.next()));
            if (v2.hasNext()) {
                v1_1.append(" ");
            }
        }
        return v1_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p7, org.xbill.DNS.Compression p8, boolean p9)
    {
        java.util.Iterator v4 = this.elements.iterator();
        while (v4.hasNext()) {
            byte[] v0_7;
            int v2_2;
            int v1_1 = ((org.xbill.DNS.APLRecord$Element) v4.next());
            if ((v1_1.family != 1) && (v1_1.family != 2)) {
                v0_7 = ((byte[]) ((byte[]) v1_1.address));
                v2_2 = v0_7.length;
            } else {
                v0_7 = ((java.net.InetAddress) v1_1.address).getAddress();
                v2_2 = org.xbill.DNS.APLRecord.addressLength(v0_7);
            }
            int v3_1;
            if (!v1_1.negative) {
                v3_1 = v2_2;
            } else {
                v3_1 = (v2_2 | 128);
            }
            p7.writeU16(v1_1.family);
            p7.writeU8(v1_1.prefixLength);
            p7.writeU8(v3_1);
            p7.writeByteArray(v0_7, 0, v2_2);
        }
        return;
    }
}
