package org.xbill.DNS;
public class TSIG {
    public static final short FUDGE = 300;
    public static final org.xbill.DNS.Name HMAC = None;
    public static final org.xbill.DNS.Name HMAC_MD5 = None;
    private static final String HMAC_MD5_STR = "HMAC-MD5.SIG-ALG.REG.INT.";
    public static final org.xbill.DNS.Name HMAC_SHA1 = None;
    private static final String HMAC_SHA1_STR = "hmac-sha1.";
    public static final org.xbill.DNS.Name HMAC_SHA224 = None;
    private static final String HMAC_SHA224_STR = "hmac-sha224.";
    public static final org.xbill.DNS.Name HMAC_SHA256 = None;
    private static final String HMAC_SHA256_STR = "hmac-sha256.";
    public static final org.xbill.DNS.Name HMAC_SHA384 = None;
    private static final String HMAC_SHA384_STR = "hmac-sha384.";
    public static final org.xbill.DNS.Name HMAC_SHA512 = None;
    private static final String HMAC_SHA512_STR = "hmac-sha512.";
    private org.xbill.DNS.Name alg;
    private String digest;
    private int digestBlockLength;
    private byte[] key;
    private org.xbill.DNS.Name name;

    static TSIG()
    {
        org.xbill.DNS.Name v0_1 = org.xbill.DNS.Name.fromConstantString("HMAC-MD5.SIG-ALG.REG.INT.");
        org.xbill.DNS.TSIG.HMAC_MD5 = v0_1;
        org.xbill.DNS.TSIG.HMAC = v0_1;
        org.xbill.DNS.TSIG.HMAC_SHA1 = org.xbill.DNS.Name.fromConstantString("hmac-sha1.");
        org.xbill.DNS.TSIG.HMAC_SHA224 = org.xbill.DNS.Name.fromConstantString("hmac-sha224.");
        org.xbill.DNS.TSIG.HMAC_SHA256 = org.xbill.DNS.Name.fromConstantString("hmac-sha256.");
        org.xbill.DNS.TSIG.HMAC_SHA384 = org.xbill.DNS.Name.fromConstantString("hmac-sha384.");
        org.xbill.DNS.TSIG.HMAC_SHA512 = org.xbill.DNS.Name.fromConstantString("hmac-sha512.");
        return;
    }

    public TSIG(String p2, String p3)
    {
        this(org.xbill.DNS.TSIG.HMAC_MD5, p2, p3);
        return;
    }

    public TSIG(String p3, String p4, String p5)
    {
        this(org.xbill.DNS.TSIG.HMAC_MD5, p4, p5);
        if (!p3.equalsIgnoreCase("hmac-md5")) {
            if (!p3.equalsIgnoreCase("hmac-sha1")) {
                if (!p3.equalsIgnoreCase("hmac-sha224")) {
                    if (!p3.equalsIgnoreCase("hmac-sha256")) {
                        if (!p3.equalsIgnoreCase("hmac-sha384")) {
                            if (!p3.equalsIgnoreCase("hmac-sha512")) {
                                throw new IllegalArgumentException("Invalid TSIG algorithm");
                            } else {
                                this.alg = org.xbill.DNS.TSIG.HMAC_SHA512;
                            }
                        } else {
                            this.alg = org.xbill.DNS.TSIG.HMAC_SHA384;
                        }
                    } else {
                        this.alg = org.xbill.DNS.TSIG.HMAC_SHA256;
                    }
                } else {
                    this.alg = org.xbill.DNS.TSIG.HMAC_SHA224;
                }
            } else {
                this.alg = org.xbill.DNS.TSIG.HMAC_SHA1;
            }
        } else {
            this.alg = org.xbill.DNS.TSIG.HMAC_MD5;
        }
        this.getDigest();
        return;
    }

    public TSIG(org.xbill.DNS.Name p3, String p4, String p5)
    {
        this.key = org.xbill.DNS.utils.base64.fromString(p5);
        if (this.key != null) {
            try {
                this.name = org.xbill.DNS.Name.fromString(p4, org.xbill.DNS.Name.root);
                this.alg = p3;
                this.getDigest();
                return;
            } catch (IllegalArgumentException v0) {
                throw new IllegalArgumentException("Invalid TSIG key name");
            }
        } else {
            throw new IllegalArgumentException("Invalid TSIG key string");
        }
    }

    public TSIG(org.xbill.DNS.Name p1, org.xbill.DNS.Name p2, byte[] p3)
    {
        this.name = p2;
        this.alg = p1;
        this.key = p3;
        this.getDigest();
        return;
    }

    public TSIG(org.xbill.DNS.Name p2, byte[] p3)
    {
        this(org.xbill.DNS.TSIG.HMAC_MD5, p2, p3);
        return;
    }

    static String access$000(org.xbill.DNS.TSIG p1)
    {
        return p1.digest;
    }

    static int access$100(org.xbill.DNS.TSIG p1)
    {
        return p1.digestBlockLength;
    }

    static byte[] access$200(org.xbill.DNS.TSIG p1)
    {
        return p1.key;
    }

    static org.xbill.DNS.Name access$300(org.xbill.DNS.TSIG p1)
    {
        return p1.name;
    }

    static org.xbill.DNS.Name access$400(org.xbill.DNS.TSIG p1)
    {
        return p1.alg;
    }

    public static org.xbill.DNS.TSIG fromString(String p8)
    {
        String[] v0_1 = p8.split("[:/]", 3);
        if (v0_1.length >= 2) {
            String[] v0_3;
            if (v0_1.length != 3) {
                v0_3 = new org.xbill.DNS.TSIG(org.xbill.DNS.TSIG.HMAC_MD5, v0_1[0], v0_1[1]);
            } else {
                try {
                    v0_3 = new org.xbill.DNS.TSIG(v0_1[0], v0_1[1], v0_1[2]);
                } catch (String[] v0) {
                    v0_1 = p8.split("[:/]", 2);
                }
            }
            return v0_3;
        } else {
            throw new IllegalArgumentException("Invalid TSIG key specification");
        }
    }

    private void getDigest()
    {
        if (!this.alg.equals(org.xbill.DNS.TSIG.HMAC_MD5)) {
            if (!this.alg.equals(org.xbill.DNS.TSIG.HMAC_SHA1)) {
                if (!this.alg.equals(org.xbill.DNS.TSIG.HMAC_SHA224)) {
                    if (!this.alg.equals(org.xbill.DNS.TSIG.HMAC_SHA256)) {
                        if (!this.alg.equals(org.xbill.DNS.TSIG.HMAC_SHA512)) {
                            if (!this.alg.equals(org.xbill.DNS.TSIG.HMAC_SHA384)) {
                                throw new IllegalArgumentException("Invalid algorithm");
                            } else {
                                this.digest = "sha-384";
                                this.digestBlockLength = 128;
                            }
                        } else {
                            this.digest = "sha-512";
                            this.digestBlockLength = 128;
                        }
                    } else {
                        this.digest = "sha-256";
                        this.digestBlockLength = 64;
                    }
                } else {
                    this.digest = "sha-224";
                    this.digestBlockLength = 64;
                }
            } else {
                this.digest = "sha-1";
                this.digestBlockLength = 64;
            }
        } else {
            this.digest = "md5";
            this.digestBlockLength = 64;
        }
        return;
    }

    public void apply(org.xbill.DNS.Message p3, int p4, org.xbill.DNS.TSIGRecord p5)
    {
        p3.addRecord(this.generate(p3, p3.toWire(), p4, p5), 3);
        p3.tsigState = 3;
        return;
    }

    public void apply(org.xbill.DNS.Message p2, org.xbill.DNS.TSIGRecord p3)
    {
        this.apply(p2, 0, p3);
        return;
    }

    public void applyStream(org.xbill.DNS.Message p14, org.xbill.DNS.TSIGRecord p15, boolean p16)
    {
        if (!p16) {
            java.util.Date v7_1 = new java.util.Date();
            int v0_1 = new org.xbill.DNS.utils.HMAC(this.digest, this.digestBlockLength, this.key);
            int v8 = org.xbill.DNS.Options.intValue("tsigfudge");
            if ((v8 < 0) || (v8 > 32767)) {
                v8 = 300;
            }
            org.xbill.DNS.TSIGRecord v1_4 = new org.xbill.DNS.DNSOutput();
            v1_4.writeU16(p15.getSignature().length);
            v0_1.update(v1_4.toByteArray());
            v0_1.update(p15.getSignature());
            v0_1.update(p14.toWire());
            org.xbill.DNS.TSIGRecord v1_9 = new org.xbill.DNS.DNSOutput();
            org.xbill.DNS.Name v2_4 = (v7_1.getTime() / 1000);
            org.xbill.DNS.Name v2_5 = (v2_4 & 2.1219957905e-314);
            v1_9.writeU16(((int) (v2_4 >> 32)));
            v1_9.writeU32(v2_5);
            v1_9.writeU16(v8);
            v0_1.update(v1_9.toByteArray());
            p14.addRecord(new org.xbill.DNS.TSIGRecord(this.name, 255, 0, this.alg, v7_1, v8, v0_1.sign(), p14.getHeader().getID(), 0, 0), 3);
            p14.tsigState = 3;
        } else {
            this.apply(p14, p15);
        }
        return;
    }

    public org.xbill.DNS.TSIGRecord generate(org.xbill.DNS.Message p16, byte[] p17, int p18, org.xbill.DNS.TSIGRecord p19)
    {
        java.util.Date v9_0;
        if (p18 == 18) {
            v9_0 = p19.getTimeSigned();
        } else {
            v9_0 = new java.util.Date();
        }
        org.xbill.DNS.Header v2_1 = 0;
        if ((p18 == 0) || (p18 == 18)) {
            v2_1 = new org.xbill.DNS.utils.HMAC(this.digest, this.digestBlockLength, this.key);
        }
        int v10 = org.xbill.DNS.Options.intValue("tsigfudge");
        if ((v10 < 0) || (v10 > 32767)) {
            v10 = 300;
        }
        if (p19 != null) {
            org.xbill.DNS.TSIGRecord v3_5 = new org.xbill.DNS.DNSOutput();
            v3_5.writeU16(p19.getSignature().length);
            if (v2_1 != null) {
                v2_1.update(v3_5.toByteArray());
                v2_1.update(p19.getSignature());
            }
        }
        if (v2_1 != null) {
            v2_1.update(p17);
        }
        org.xbill.DNS.TSIGRecord v3_9 = new org.xbill.DNS.DNSOutput();
        this.name.toWireCanonical(v3_9);
        v3_9.writeU16(255);
        v3_9.writeU32(0);
        this.alg.toWireCanonical(v3_9);
        org.xbill.DNS.Name v4_8 = (v9_0.getTime() / 1000);
        org.xbill.DNS.Name v4_9 = (v4_8 & 2.1219957905e-314);
        v3_9.writeU16(((int) (v4_8 >> 32)));
        v3_9.writeU32(v4_9);
        v3_9.writeU16(v10);
        v3_9.writeU16(p18);
        v3_9.writeU16(0);
        if (v2_1 != null) {
            v2_1.update(v3_9.toByteArray());
        }
        byte[] v11;
        if (v2_1 == null) {
            v11 = new byte[0];
        } else {
            v11 = v2_1.sign();
        }
        byte[] v14 = 0;
        if (p18 == 18) {
            org.xbill.DNS.Header v2_6 = new org.xbill.DNS.DNSOutput();
            org.xbill.DNS.Name v4_12 = (new java.util.Date().getTime() / 1000);
            org.xbill.DNS.Name v4_13 = (v4_12 & 2.1219957905e-314);
            v2_6.writeU16(((int) (v4_12 >> 32)));
            v2_6.writeU32(v4_13);
            v14 = v2_6.toByteArray();
        }
        return new org.xbill.DNS.TSIGRecord(this.name, 255, 0, this.alg, v9_0, v10, v11, p16.getHeader().getID(), p18, v14);
    }

    public int recordLength()
    {
        return ((((((this.name.length() + 10) + this.alg.length()) + 8) + 18) + 4) + 8);
    }

    public byte verify(org.xbill.DNS.Message p9, byte[] p10, int p11, org.xbill.DNS.TSIGRecord p12)
    {
        int v0_8;
        p9.tsigState = 4;
        int v0_1 = p9.getTSIG();
        String v1_1 = new org.xbill.DNS.utils.HMAC(this.digest, this.digestBlockLength, this.key);
        if (v0_1 != 0) {
            if ((v0_1.getName().equals(this.name)) && (v0_1.getAlgorithm().equals(this.alg))) {
                if (Math.abs((System.currentTimeMillis() - v0_1.getTimeSigned().getTime())) <= (1000 * ((long) v0_1.getFudge()))) {
                    if ((p12 != null) && ((v0_1.getError() != 17) && (v0_1.getError() != 16))) {
                        byte[] v2_12 = new org.xbill.DNS.DNSOutput();
                        v2_12.writeU16(p12.getSignature().length);
                        v1_1.update(v2_12.toByteArray());
                        v1_1.update(p12.getSignature());
                    }
                    p9.getHeader().decCount(3);
                    byte[] v2_17 = p9.getHeader().toWire();
                    p9.getHeader().incCount(3);
                    v1_1.update(v2_17);
                    v1_1.update(p10, v2_17.length, (p9.tsigstart - v2_17.length));
                    byte[] v2_20 = new org.xbill.DNS.DNSOutput();
                    v0_1.getName().toWireCanonical(v2_20);
                    v2_20.writeU16(v0_1.dclass);
                    v2_20.writeU32(v0_1.ttl);
                    v0_1.getAlgorithm().toWireCanonical(v2_20);
                    int v4_9 = (v0_1.getTimeSigned().getTime() / 1000);
                    int v4_10 = (v4_9 & 2.1219957905e-314);
                    v2_20.writeU16(((int) (v4_9 >> 32)));
                    v2_20.writeU32(v4_10);
                    v2_20.writeU16(v0_1.getFudge());
                    v2_20.writeU16(v0_1.getError());
                    if (v0_1.getOther() == null) {
                        v2_20.writeU16(0);
                    } else {
                        v2_20.writeU16(v0_1.getOther().length);
                        v2_20.writeByteArray(v0_1.getOther());
                    }
                    int v0_4;
                    v1_1.update(v2_20.toByteArray());
                    byte[] v2_22 = v0_1.getSignature();
                    int v3_24 = v1_1.digestLength();
                    if (!this.digest.equals("md5")) {
                        v0_4 = (v3_24 / 2);
                    } else {
                        v0_4 = 10;
                    }
                    if (v2_22.length <= v3_24) {
                        if (v2_22.length >= v0_4) {
                            if (v1_1.verify(v2_22, 1)) {
                                p9.tsigState = 1;
                                v0_8 = 0;
                            } else {
                                if (org.xbill.DNS.Options.check("verbose")) {
                                    System.err.println("BADSIG: signature verification");
                                }
                                v0_8 = 16;
                            }
                        } else {
                            if (org.xbill.DNS.Options.check("verbose")) {
                                System.err.println("BADSIG: signature too short");
                            }
                            v0_8 = 16;
                        }
                    } else {
                        if (org.xbill.DNS.Options.check("verbose")) {
                            System.err.println("BADSIG: signature too long");
                        }
                        v0_8 = 16;
                    }
                } else {
                    if (org.xbill.DNS.Options.check("verbose")) {
                        System.err.println("BADTIME failure");
                    }
                    v0_8 = 18;
                }
            } else {
                if (org.xbill.DNS.Options.check("verbose")) {
                    System.err.println("BADKEY failure");
                }
                v0_8 = 17;
            }
        } else {
            v0_8 = 1;
        }
        return v0_8;
    }

    public int verify(org.xbill.DNS.Message p2, byte[] p3, org.xbill.DNS.TSIGRecord p4)
    {
        return this.verify(p2, p3, p3.length, p4);
    }
}
