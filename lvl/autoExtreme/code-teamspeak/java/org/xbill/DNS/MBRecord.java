package org.xbill.DNS;
public class MBRecord extends org.xbill.DNS.SingleNameBase {
    private static final long serialVersionUID = 532349543479150419;

    MBRecord()
    {
        return;
    }

    public MBRecord(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Name p14)
    {
        this(p10, 7, p11, p12, p14, "mailbox");
        return;
    }

    public org.xbill.DNS.Name getAdditionalName()
    {
        return this.getSingleName();
    }

    public org.xbill.DNS.Name getMailbox()
    {
        return this.getSingleName();
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.MBRecord();
    }
}
