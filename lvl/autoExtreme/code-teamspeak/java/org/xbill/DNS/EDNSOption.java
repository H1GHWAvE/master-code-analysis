package org.xbill.DNS;
public abstract class EDNSOption {
    private final int code;

    public EDNSOption(int p2)
    {
        this.code = org.xbill.DNS.Record.checkU16("code", p2);
        return;
    }

    static org.xbill.DNS.EDNSOption fromWire(org.xbill.DNS.DNSInput p3)
    {
        String v1_0 = p3.readU16();
        org.xbill.DNS.ClientSubnetOption v0_0 = p3.readU16();
        if (p3.remaining() >= v0_0) {
            org.xbill.DNS.ClientSubnetOption v0_2;
            int v2_1 = p3.saveActive();
            p3.setActive(v0_0);
            switch (v1_0) {
                case 3:
                    v0_2 = new org.xbill.DNS.NSIDOption();
                    break;
                case 8:
                    v0_2 = new org.xbill.DNS.ClientSubnetOption();
                    break;
                default:
                    v0_2 = new org.xbill.DNS.GenericEDNSOption(v1_0);
            }
            v0_2.optionFromWire(p3);
            p3.restoreActive(v2_1);
            return v0_2;
        } else {
            throw new org.xbill.DNS.WireParseException("truncated option");
        }
    }

    public static org.xbill.DNS.EDNSOption fromWire(byte[] p1)
    {
        return org.xbill.DNS.EDNSOption.fromWire(new org.xbill.DNS.DNSInput(p1));
    }

    public boolean equals(Object p4)
    {
        boolean v0_0 = 0;
        if ((p4 != null) && (((p4 instanceof org.xbill.DNS.EDNSOption)) && (this.code == ((org.xbill.DNS.EDNSOption) p4).code))) {
            v0_0 = java.util.Arrays.equals(this.getData(), ((org.xbill.DNS.EDNSOption) p4).getData());
        }
        return v0_0;
    }

    public int getCode()
    {
        return this.code;
    }

    byte[] getData()
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        this.optionToWire(v0_1);
        return v0_1.toByteArray();
    }

    public int hashCode()
    {
        int v0 = 0;
        byte[] v2 = this.getData();
        int v1 = 0;
        while (v0 < v2.length) {
            v1 += ((v1 << 3) + (v2[v0] & 255));
            v0++;
        }
        return v1;
    }

    abstract void optionFromWire();

    abstract String optionToString();

    abstract void optionToWire();

    public String toString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append("{");
        v0_1.append(org.xbill.DNS.EDNSOption$Code.string(this.code));
        v0_1.append(": ");
        v0_1.append(this.optionToString());
        v0_1.append("}");
        return v0_1.toString();
    }

    void toWire(org.xbill.DNS.DNSOutput p3)
    {
        p3.writeU16(this.code);
        int v0_1 = p3.current();
        p3.writeU16(0);
        this.optionToWire(p3);
        p3.writeU16At(((p3.current() - v0_1) - 2), v0_1);
        return;
    }

    public byte[] toWire()
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        this.toWire(v0_1);
        return v0_1.toByteArray();
    }
}
