package org.xbill.DNS;
 class Tokenizer$TokenizerException extends org.xbill.DNS.TextParseException {
    String message;

    public Tokenizer$TokenizerException(String p3, int p4, String p5)
    {
        this(new StringBuffer().append(p3).append(":").append(p4).append(": ").append(p5).toString());
        this.message = p5;
        return;
    }

    public String getBaseMessage()
    {
        return this.message;
    }
}
