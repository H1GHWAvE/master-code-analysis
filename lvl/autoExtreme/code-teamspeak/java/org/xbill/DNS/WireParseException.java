package org.xbill.DNS;
public class WireParseException extends java.io.IOException {

    public WireParseException()
    {
        return;
    }

    public WireParseException(String p1)
    {
        this(p1);
        return;
    }

    public WireParseException(String p1, Throwable p2)
    {
        this(p1);
        this.initCause(p2);
        return;
    }
}
