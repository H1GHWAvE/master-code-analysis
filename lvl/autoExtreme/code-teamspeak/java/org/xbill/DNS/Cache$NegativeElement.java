package org.xbill.DNS;
 class Cache$NegativeElement implements org.xbill.DNS.Cache$Element {
    int credibility;
    int expire;
    org.xbill.DNS.Name name;
    int type;

    public Cache$NegativeElement(org.xbill.DNS.Name p4, int p5, org.xbill.DNS.SOARecord p6, int p7, long p8)
    {
        this.name = p4;
        this.type = p5;
        int v0_0 = 0;
        if (p6 != null) {
            v0_0 = p6.getMinimum();
        }
        this.credibility = p7;
        this.expire = org.xbill.DNS.Cache.access$000(v0_0, p8);
        return;
    }

    public final int compareCredibility(int p2)
    {
        return (this.credibility - p2);
    }

    public final boolean expired()
    {
        int v0_3;
        if (((int) (System.currentTimeMillis() / 1000)) < this.expire) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public int getType()
    {
        return this.type;
    }

    public String toString()
    {
        String v0_1 = new StringBuffer();
        if (this.type != 0) {
            v0_1.append(new StringBuffer("NXRRSET ").append(this.name).append(" ").append(org.xbill.DNS.Type.string(this.type)).toString());
        } else {
            v0_1.append(new StringBuffer("NXDOMAIN ").append(this.name).toString());
        }
        v0_1.append(" cl = ");
        v0_1.append(this.credibility);
        return v0_1.toString();
    }
}
