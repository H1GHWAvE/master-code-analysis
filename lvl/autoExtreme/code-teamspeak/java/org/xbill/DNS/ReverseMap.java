package org.xbill.DNS;
public final class ReverseMap {
    private static org.xbill.DNS.Name inaddr4;
    private static org.xbill.DNS.Name inaddr6;

    static ReverseMap()
    {
        org.xbill.DNS.ReverseMap.inaddr4 = org.xbill.DNS.Name.fromConstantString("in-addr.arpa.");
        org.xbill.DNS.ReverseMap.inaddr6 = org.xbill.DNS.Name.fromConstantString("ip6.arpa.");
        return;
    }

    private ReverseMap()
    {
        return;
    }

    public static org.xbill.DNS.Name fromAddress(String p2)
    {
        org.xbill.DNS.Name v0_1 = org.xbill.DNS.Address.toByteArray(p2, 1);
        if (v0_1 == null) {
            v0_1 = org.xbill.DNS.Address.toByteArray(p2, 2);
        }
        if (v0_1 != null) {
            return org.xbill.DNS.ReverseMap.fromAddress(v0_1);
        } else {
            throw new java.net.UnknownHostException("Invalid IP address");
        }
    }

    public static org.xbill.DNS.Name fromAddress(String p2, int p3)
    {
        org.xbill.DNS.Name v0_0 = org.xbill.DNS.Address.toByteArray(p2, p3);
        if (v0_0 != null) {
            return org.xbill.DNS.ReverseMap.fromAddress(v0_0);
        } else {
            throw new java.net.UnknownHostException("Invalid IP address");
        }
    }

    public static org.xbill.DNS.Name fromAddress(java.net.InetAddress p1)
    {
        return org.xbill.DNS.ReverseMap.fromAddress(p1.getAddress());
    }

    public static org.xbill.DNS.Name fromAddress(byte[] p7)
    {
        if ((p7.length == 4) || (p7.length == 16)) {
            StringBuffer v3_1 = new StringBuffer();
            if (p7.length != 4) {
                int[] v4 = new int[2];
                int v2_1 = (p7.length - 1);
                while (v2_1 >= 0) {
                    v4[0] = ((p7[v2_1] & 255) >> 4);
                    v4[1] = ((p7[v2_1] & 255) & 15);
                    int v0_10 = 1;
                    while (v0_10 >= 0) {
                        v3_1.append(Integer.toHexString(v4[v0_10]));
                        if ((v2_1 > 0) || (v0_10 > 0)) {
                            v3_1.append(".");
                        }
                        v0_10--;
                    }
                    v2_1--;
                }
            } else {
                int v0_13 = (p7.length - 1);
                while (v0_13 >= 0) {
                    v3_1.append((p7[v0_13] & 255));
                    if (v0_13 > 0) {
                        v3_1.append(".");
                    }
                    v0_13--;
                }
            }
            try {
                int v0_16;
                if (p7.length != 4) {
                    v0_16 = org.xbill.DNS.Name.fromString(v3_1.toString(), org.xbill.DNS.ReverseMap.inaddr6);
                } else {
                    v0_16 = org.xbill.DNS.Name.fromString(v3_1.toString(), org.xbill.DNS.ReverseMap.inaddr4);
                }
            } catch (int v0) {
                throw new IllegalStateException("name cannot be invalid");
            }
            return v0_16;
        } else {
            throw new IllegalArgumentException("array must contain 4 or 16 elements");
        }
    }

    public static org.xbill.DNS.Name fromAddress(int[] p4)
    {
        String v1_0 = new byte[p4.length];
        int v0_1 = 0;
        while (v0_1 < p4.length) {
            if ((p4[v0_1] >= 0) && (p4[v0_1] <= 255)) {
                v1_0[v0_1] = ((byte) p4[v0_1]);
                v0_1++;
            } else {
                throw new IllegalArgumentException("array must contain values between 0 and 255");
            }
        }
        return org.xbill.DNS.ReverseMap.fromAddress(v1_0);
    }
}
