package org.xbill.DNS;
public class ISDNRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 9715942688530582818;
    private byte[] address;
    private byte[] subAddress;

    ISDNRecord()
    {
        return;
    }

    public ISDNRecord(org.xbill.DNS.Name p8, int p9, long p10, String p12, String p13)
    {
        this(p8, 20, p9, p10);
        try {
            this.address = org.xbill.DNS.ISDNRecord.byteArrayFromString(p12);
        } catch (byte[] v0_3) {
            throw new IllegalArgumentException(v0_3.getMessage());
        }
        if (p13 != null) {
            this.subAddress = org.xbill.DNS.ISDNRecord.byteArrayFromString(p13);
        }
        return;
    }

    public String getAddress()
    {
        return org.xbill.DNS.ISDNRecord.byteArrayToString(this.address, 0);
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.ISDNRecord();
    }

    public String getSubAddress()
    {
        String v0_2;
        if (this.subAddress != null) {
            v0_2 = org.xbill.DNS.ISDNRecord.byteArrayToString(this.subAddress, 0);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p3, org.xbill.DNS.Name p4)
    {
        try {
            this.address = org.xbill.DNS.ISDNRecord.byteArrayFromString(p3.getString());
            byte[] v0_2 = p3.get();
        } catch (byte[] v0_5) {
            throw p3.exception(v0_5.getMessage());
        }
        if (!v0_2.isString()) {
            p3.unget();
        } else {
            this.subAddress = org.xbill.DNS.ISDNRecord.byteArrayFromString(v0_2.value);
        }
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.address = p2.readCountedString();
        if (p2.remaining() > 0) {
            this.subAddress = p2.readCountedString();
        }
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(org.xbill.DNS.ISDNRecord.byteArrayToString(this.address, 1));
        if (this.subAddress != null) {
            v0_1.append(" ");
            v0_1.append(org.xbill.DNS.ISDNRecord.byteArrayToString(this.subAddress, 1));
        }
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeCountedString(this.address);
        if (this.subAddress != null) {
            p2.writeCountedString(this.subAddress);
        }
        return;
    }
}
