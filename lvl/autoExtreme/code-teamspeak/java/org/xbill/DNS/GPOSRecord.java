package org.xbill.DNS;
public class GPOSRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 12097029115623800911;
    private byte[] altitude;
    private byte[] latitude;
    private byte[] longitude;

    GPOSRecord()
    {
        return;
    }

    public GPOSRecord(org.xbill.DNS.Name p8, int p9, long p10, double p12, double p14, double p16)
    {
        this(p8, 27, p9, p10);
        this.validate(p12, p14);
        this.longitude = Double.toString(p12).getBytes();
        this.latitude = Double.toString(p14).getBytes();
        this.altitude = Double.toString(p16).getBytes();
        return;
    }

    public GPOSRecord(org.xbill.DNS.Name p8, int p9, long p10, String p12, String p13, String p14)
    {
        this(p8, 27, p9, p10);
        try {
            this.longitude = org.xbill.DNS.GPOSRecord.byteArrayFromString(p12);
            this.latitude = org.xbill.DNS.GPOSRecord.byteArrayFromString(p13);
            this.validate(this.getLongitude(), this.getLatitude());
            this.altitude = org.xbill.DNS.GPOSRecord.byteArrayFromString(p14);
            return;
        } catch (String v0_5) {
            throw new IllegalArgumentException(v0_5.getMessage());
        }
    }

    private void validate(double p4, double p6)
    {
        if ((p4 >= -90.0) && (p4 <= 90.0)) {
            if ((p6 >= -180.0) && (p6 <= 180.0)) {
                return;
            } else {
                throw new IllegalArgumentException(new StringBuffer("illegal latitude ").append(p6).toString());
            }
        } else {
            throw new IllegalArgumentException(new StringBuffer("illegal longitude ").append(p4).toString());
        }
    }

    public double getAltitude()
    {
        return Double.parseDouble(this.getAltitudeString());
    }

    public String getAltitudeString()
    {
        return org.xbill.DNS.GPOSRecord.byteArrayToString(this.altitude, 0);
    }

    public double getLatitude()
    {
        return Double.parseDouble(this.getLatitudeString());
    }

    public String getLatitudeString()
    {
        return org.xbill.DNS.GPOSRecord.byteArrayToString(this.latitude, 0);
    }

    public double getLongitude()
    {
        return Double.parseDouble(this.getLongitudeString());
    }

    public String getLongitudeString()
    {
        return org.xbill.DNS.GPOSRecord.byteArrayToString(this.longitude, 0);
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.GPOSRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p5, org.xbill.DNS.Name p6)
    {
        try {
            this.longitude = org.xbill.DNS.GPOSRecord.byteArrayFromString(p5.getString());
            this.latitude = org.xbill.DNS.GPOSRecord.byteArrayFromString(p5.getString());
            this.altitude = org.xbill.DNS.GPOSRecord.byteArrayFromString(p5.getString());
            try {
                this.validate(this.getLongitude(), this.getLatitude());
                return;
            } catch (String v0_10) {
                throw new org.xbill.DNS.WireParseException(v0_10.getMessage());
            }
        } catch (String v0_6) {
            throw p5.exception(v0_6.getMessage());
        }
        this.validate(this.getLongitude(), this.getLatitude());
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p5)
    {
        this.longitude = p5.readCountedString();
        this.latitude = p5.readCountedString();
        this.altitude = p5.readCountedString();
        try {
            this.validate(this.getLongitude(), this.getLatitude());
            return;
        } catch (String v0_4) {
            throw new org.xbill.DNS.WireParseException(v0_4.getMessage());
        }
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(org.xbill.DNS.GPOSRecord.byteArrayToString(this.longitude, 1));
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.GPOSRecord.byteArrayToString(this.latitude, 1));
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.GPOSRecord.byteArrayToString(this.altitude, 1));
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeCountedString(this.longitude);
        p2.writeCountedString(this.latitude);
        p2.writeCountedString(this.altitude);
        return;
    }
}
