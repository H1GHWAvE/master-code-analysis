package org.xbill.DNS;
public final class Lookup {
    public static final int HOST_NOT_FOUND = 3;
    public static final int SUCCESSFUL = 0;
    public static final int TRY_AGAIN = 2;
    public static final int TYPE_NOT_FOUND = 4;
    public static final int UNRECOVERABLE = 1;
    static Class class$org$xbill$DNS$Lookup;
    private static java.util.Map defaultCaches;
    private static int defaultNdots;
    private static org.xbill.DNS.Resolver defaultResolver;
    private static org.xbill.DNS.Name[] defaultSearchPath;
    private static final org.xbill.DNS.Name[] noAliases;
    private java.util.List aliases;
    private org.xbill.DNS.Record[] answers;
    private boolean badresponse;
    private String badresponse_error;
    private org.xbill.DNS.Cache cache;
    private int credibility;
    private int dclass;
    private boolean done;
    private boolean doneCurrent;
    private String error;
    private boolean foundAlias;
    private int iterations;
    private org.xbill.DNS.Name name;
    private boolean nametoolong;
    private boolean networkerror;
    private boolean nxdomain;
    private boolean referral;
    private org.xbill.DNS.Resolver resolver;
    private int result;
    private org.xbill.DNS.Name[] searchPath;
    private boolean temporary_cache;
    private boolean timedout;
    private int type;
    private boolean verbose;

    static Lookup()
    {
        org.xbill.DNS.Name[] v0_1 = new org.xbill.DNS.Name[0];
        org.xbill.DNS.Lookup.noAliases = v0_1;
        org.xbill.DNS.Lookup.refreshDefault();
        return;
    }

    public Lookup(String p3)
    {
        this(org.xbill.DNS.Name.fromString(p3), 1, 1);
        return;
    }

    public Lookup(String p3, int p4)
    {
        this(org.xbill.DNS.Name.fromString(p3), p4, 1);
        return;
    }

    public Lookup(String p2, int p3, int p4)
    {
        this(org.xbill.DNS.Name.fromString(p2), p3, p4);
        return;
    }

    public Lookup(org.xbill.DNS.Name p2)
    {
        this(p2, 1, 1);
        return;
    }

    public Lookup(org.xbill.DNS.Name p2, int p3)
    {
        this(p2, p3, 1);
        return;
    }

    public Lookup(org.xbill.DNS.Name p3, int p4, int p5)
    {
        org.xbill.DNS.Type.check(p4);
        org.xbill.DNS.DClass.check(p5);
        if ((org.xbill.DNS.Type.isRR(p4)) || (p4 == 255)) {
            this.name = p3;
            this.type = p4;
            this.dclass = p5;
            if (org.xbill.DNS.Lookup.class$org$xbill$DNS$Lookup == null) {
                org.xbill.DNS.Lookup.class$org$xbill$DNS$Lookup = org.xbill.DNS.Lookup.class$("org.xbill.DNS.Lookup");
            }
            this.resolver = org.xbill.DNS.Lookup.getDefaultResolver();
            this.searchPath = org.xbill.DNS.Lookup.getDefaultSearchPath();
            this.cache = org.xbill.DNS.Lookup.getDefaultCache(p5);
            this.credibility = 3;
            this.verbose = org.xbill.DNS.Options.check("verbose");
            this.result = -1;
            return;
        } else {
            throw new IllegalArgumentException("Cannot query for meta-types other than ANY");
        }
    }

    private void checkDone()
    {
        if ((!this.done) || (this.result == -1)) {
            String v0_3 = new StringBuffer(new StringBuffer("Lookup of ").append(this.name).append(" ").toString());
            if (this.dclass != 1) {
                v0_3.append(new StringBuffer().append(org.xbill.DNS.DClass.string(this.dclass)).append(" ").toString());
            }
            v0_3.append(new StringBuffer().append(org.xbill.DNS.Type.string(this.type)).append(" isn\'t done").toString());
            throw new IllegalStateException(v0_3.toString());
        } else {
            return;
        }
    }

    static Class class$(String p2)
    {
        try {
            return Class.forName(p2);
        } catch (Throwable v0_1) {
            throw new NoClassDefFoundError().initCause(v0_1);
        }
    }

    private void follow(org.xbill.DNS.Name p4, org.xbill.DNS.Name p5)
    {
        this.foundAlias = 1;
        this.badresponse = 0;
        this.networkerror = 0;
        this.timedout = 0;
        this.nxdomain = 0;
        this.referral = 0;
        this.iterations = (this.iterations + 1);
        if ((this.iterations < 6) && (!p4.equals(p5))) {
            if (this.aliases == null) {
                this.aliases = new java.util.ArrayList();
            }
            this.aliases.add(p5);
            this.lookup(p4);
        } else {
            this.result = 1;
            this.error = "CNAME loop";
            this.done = 1;
        }
        return;
    }

    public static declared_synchronized org.xbill.DNS.Cache getDefaultCache(int p4)
    {
        try {
            org.xbill.DNS.DClass.check(p4);
            org.xbill.DNS.Cache v0_2 = ((org.xbill.DNS.Cache) org.xbill.DNS.Lookup.defaultCaches.get(org.xbill.DNS.Mnemonic.toInteger(p4)));
        } catch (org.xbill.DNS.Cache v0_4) {
            throw v0_4;
        }
        if (v0_2 == null) {
            v0_2 = new org.xbill.DNS.Cache(p4);
            org.xbill.DNS.Lookup.defaultCaches.put(org.xbill.DNS.Mnemonic.toInteger(p4), v0_2);
        }
        return v0_2;
    }

    public static declared_synchronized org.xbill.DNS.Resolver getDefaultResolver()
    {
        try {
            return org.xbill.DNS.Lookup.defaultResolver;
        } catch (Throwable v1_1) {
            throw v1_1;
        }
    }

    public static declared_synchronized org.xbill.DNS.Name[] getDefaultSearchPath()
    {
        try {
            return org.xbill.DNS.Lookup.defaultSearchPath;
        } catch (Throwable v1_1) {
            throw v1_1;
        }
    }

    private void lookup(org.xbill.DNS.Name p6)
    {
        org.xbill.DNS.SetResponse v0_1 = this.cache.lookupRecords(p6, this.type, this.credibility);
        if (this.verbose) {
            System.err.println(new StringBuffer("lookup ").append(p6).append(" ").append(org.xbill.DNS.Type.string(this.type)).toString());
            System.err.println(v0_1);
        }
        this.processResponse(p6, v0_1);
        if ((!this.done) && (!this.doneCurrent)) {
            org.xbill.DNS.SetResponse v0_6 = org.xbill.DNS.Message.newQuery(org.xbill.DNS.Record.newRecord(p6, this.type, this.dclass));
            try {
                java.io.PrintStream v1_6 = this.resolver.send(v0_6);
                String v2_8 = v1_6.getHeader().getRcode();
            } catch (org.xbill.DNS.SetResponse v0_7) {
                if (!(v0_7 instanceof java.io.InterruptedIOException)) {
                    this.networkerror = 1;
                } else {
                    this.timedout = 1;
                }
            }
            if ((v2_8 == null) || (v2_8 == 3)) {
                if (v0_6.getQuestion().equals(v1_6.getQuestion())) {
                    org.xbill.DNS.SetResponse v0_12 = this.cache.addMessage(v1_6);
                    if (v0_12 == null) {
                        v0_12 = this.cache.lookupRecords(p6, this.type, this.credibility);
                    }
                    if (this.verbose) {
                        System.err.println(new StringBuffer("queried ").append(p6).append(" ").append(org.xbill.DNS.Type.string(this.type)).toString());
                        System.err.println(v0_12);
                    }
                    this.processResponse(p6, v0_12);
                } else {
                    this.badresponse = 1;
                    this.badresponse_error = "response does not match query";
                }
            } else {
                this.badresponse = 1;
                this.badresponse_error = org.xbill.DNS.Rcode.string(v2_8);
            }
        }
        return;
    }

    private void processResponse(org.xbill.DNS.Name p8, org.xbill.DNS.SetResponse p9)
    {
        if (!p9.isSuccessful()) {
            if (!p9.isNXDOMAIN()) {
                if (!p9.isNXRRSET()) {
                    if (!p9.isCNAME()) {
                        if (!p9.isDNAME()) {
                            if (p9.isDelegation()) {
                                this.referral = 1;
                            }
                        } else {
                            try {
                                this.follow(p8.fromDNAME(p9.getDNAME()), p8);
                            } catch (String v0) {
                                this.result = 1;
                                this.error = "Invalid DNAME target";
                                this.done = 1;
                            }
                        }
                    } else {
                        this.follow(p9.getCNAME().getTarget(), p8);
                    }
                } else {
                    this.result = 4;
                    this.answers = 0;
                    this.done = 1;
                }
            } else {
                this.nxdomain = 1;
                this.doneCurrent = 1;
                if (this.iterations > 0) {
                    this.result = 3;
                    this.done = 1;
                }
            }
        } else {
            org.xbill.DNS.RRset[] v2 = p9.answers();
            java.util.ArrayList v3_1 = new java.util.ArrayList();
            String v0_15 = 0;
            while (v0_15 < v2.length) {
                java.util.Iterator v4_2 = v2[v0_15].rrs();
                while (v4_2.hasNext()) {
                    v3_1.add(v4_2.next());
                }
                v0_15++;
            }
            this.result = 0;
            String v0_17 = new org.xbill.DNS.Record[v3_1.size()];
            this.answers = ((org.xbill.DNS.Record[]) ((org.xbill.DNS.Record[]) v3_1.toArray(v0_17)));
            this.done = 1;
        }
        return;
    }

    public static declared_synchronized void refreshDefault()
    {
        try {
            org.xbill.DNS.Lookup.defaultResolver = new org.xbill.DNS.ExtendedResolver();
        } catch (RuntimeException v0_10) {
            throw v0_10;
        } catch (RuntimeException v0) {
            throw new RuntimeException("Failed to initialize resolver");
        }
        org.xbill.DNS.Lookup.defaultSearchPath = org.xbill.DNS.ResolverConfig.getCurrentConfig().searchPath();
        org.xbill.DNS.Lookup.defaultCaches = new java.util.HashMap();
        org.xbill.DNS.Lookup.defaultNdots = org.xbill.DNS.ResolverConfig.getCurrentConfig().ndots();
        return;
    }

    private final void reset()
    {
        this.iterations = 0;
        this.foundAlias = 0;
        this.done = 0;
        this.doneCurrent = 0;
        this.aliases = 0;
        this.answers = 0;
        this.result = -1;
        this.error = 0;
        this.nxdomain = 0;
        this.badresponse = 0;
        this.badresponse_error = 0;
        this.networkerror = 0;
        this.timedout = 0;
        this.nametoolong = 0;
        this.referral = 0;
        if (this.temporary_cache) {
            this.cache.clearCache();
        }
        return;
    }

    private void resolve(org.xbill.DNS.Name p2, org.xbill.DNS.Name p3)
    {
        this.doneCurrent = 0;
        if (p3 == null) {
            this.lookup(p2);
        } else {
            try {
                p2 = org.xbill.DNS.Name.concatenate(p2, p3);
            } catch (int v0) {
                this.nametoolong = 1;
            }
        }
        return;
    }

    public static declared_synchronized void setDefaultCache(org.xbill.DNS.Cache p3, int p4)
    {
        try {
            org.xbill.DNS.DClass.check(p4);
            org.xbill.DNS.Lookup.defaultCaches.put(org.xbill.DNS.Mnemonic.toInteger(p4), p3);
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public static declared_synchronized void setDefaultResolver(org.xbill.DNS.Resolver p2)
    {
        try {
            org.xbill.DNS.Lookup.defaultResolver = p2;
            return;
        } catch (Throwable v1) {
            throw v1;
        }
    }

    public static declared_synchronized void setDefaultSearchPath(String[] p5)
    {
        try {
            if (p5 != null) {
                org.xbill.DNS.Name[] v2 = new org.xbill.DNS.Name[p5.length];
                int v0_1 = 0;
                while (v0_1 < p5.length) {
                    v2[v0_1] = org.xbill.DNS.Name.fromString(p5[v0_1], org.xbill.DNS.Name.root);
                    v0_1++;
                }
                org.xbill.DNS.Lookup.defaultSearchPath = v2;
            } else {
                org.xbill.DNS.Lookup.defaultSearchPath = 0;
            }
        } catch (int v0_3) {
            throw v0_3;
        }
        return;
    }

    public static declared_synchronized void setDefaultSearchPath(org.xbill.DNS.Name[] p2)
    {
        try {
            org.xbill.DNS.Lookup.defaultSearchPath = p2;
            return;
        } catch (Throwable v1) {
            throw v1;
        }
    }

    public static declared_synchronized void setPacketLogger(org.xbill.DNS.PacketLogger p2)
    {
        try {
            org.xbill.DNS.Client.setPacketLogger(p2);
            return;
        } catch (Throwable v1) {
            throw v1;
        }
    }

    public final org.xbill.DNS.Name[] getAliases()
    {
        org.xbill.DNS.Name[] v0_4;
        this.checkDone();
        if (this.aliases != null) {
            org.xbill.DNS.Name[] v1_2 = new org.xbill.DNS.Name[this.aliases.size()];
            v0_4 = ((org.xbill.DNS.Name[]) ((org.xbill.DNS.Name[]) this.aliases.toArray(v1_2)));
        } else {
            v0_4 = org.xbill.DNS.Lookup.noAliases;
        }
        return v0_4;
    }

    public final org.xbill.DNS.Record[] getAnswers()
    {
        this.checkDone();
        return this.answers;
    }

    public final String getErrorString()
    {
        String v0_2;
        this.checkDone();
        if (this.error == null) {
            switch (this.result) {
                case 0:
                    v0_2 = "successful";
                    break;
                case 1:
                    v0_2 = "unrecoverable error";
                    break;
                case 2:
                    v0_2 = "try again";
                    break;
                case 3:
                    v0_2 = "host not found";
                    break;
                case 4:
                    v0_2 = "type not found";
                    break;
                default:
                    throw new IllegalStateException("unknown result");
            }
        } else {
            v0_2 = this.error;
        }
        return v0_2;
    }

    public final int getResult()
    {
        this.checkDone();
        return this.result;
    }

    public final org.xbill.DNS.Record[] run()
    {
        if (this.done) {
            this.reset();
        }
        String v0_9;
        if (!this.name.isAbsolute()) {
            if (this.searchPath != null) {
                if (this.name.labels() > org.xbill.DNS.Lookup.defaultNdots) {
                    this.resolve(this.name, org.xbill.DNS.Name.root);
                }
                if (!this.done) {
                    String v0_8 = 0;
                    while (v0_8 < this.searchPath.length) {
                        this.resolve(this.name, this.searchPath[v0_8]);
                        if (!this.done) {
                            if (this.foundAlias) {
                                break;
                            }
                            v0_8++;
                        } else {
                            v0_9 = this.answers;
                        }
                    }
                    if (!this.done) {
                        if (!this.badresponse) {
                            if (!this.timedout) {
                                if (!this.networkerror) {
                                    if (!this.nxdomain) {
                                        if (!this.referral) {
                                            if (this.nametoolong) {
                                                this.result = 1;
                                                this.error = "name too long";
                                                this.done = 1;
                                            }
                                        } else {
                                            this.result = 1;
                                            this.error = "referral";
                                            this.done = 1;
                                        }
                                    } else {
                                        this.result = 3;
                                        this.done = 1;
                                    }
                                } else {
                                    this.result = 2;
                                    this.error = "network error";
                                    this.done = 1;
                                }
                            } else {
                                this.result = 2;
                                this.error = "timed out";
                                this.done = 1;
                            }
                        } else {
                            this.result = 2;
                            this.error = this.badresponse_error;
                            this.done = 1;
                        }
                    }
                    v0_9 = this.answers;
                } else {
                    v0_9 = this.answers;
                }
            } else {
                this.resolve(this.name, org.xbill.DNS.Name.root);
            }
        } else {
            this.resolve(this.name, 0);
        }
        return v0_9;
    }

    public final void setCache(org.xbill.DNS.Cache p3)
    {
        if (p3 != null) {
            this.cache = p3;
            this.temporary_cache = 0;
        } else {
            this.cache = new org.xbill.DNS.Cache(this.dclass);
            this.temporary_cache = 1;
        }
        return;
    }

    public final void setCredibility(int p1)
    {
        this.credibility = p1;
        return;
    }

    public final void setNdots(int p4)
    {
        if (p4 >= 0) {
            org.xbill.DNS.Lookup.defaultNdots = p4;
            return;
        } else {
            throw new IllegalArgumentException(new StringBuffer("Illegal ndots value: ").append(p4).toString());
        }
    }

    public final void setResolver(org.xbill.DNS.Resolver p1)
    {
        this.resolver = p1;
        return;
    }

    public final void setSearchPath(String[] p5)
    {
        if (p5 != null) {
            org.xbill.DNS.Name[] v1 = new org.xbill.DNS.Name[p5.length];
            int v0_1 = 0;
            while (v0_1 < p5.length) {
                v1[v0_1] = org.xbill.DNS.Name.fromString(p5[v0_1], org.xbill.DNS.Name.root);
                v0_1++;
            }
            this.searchPath = v1;
        } else {
            this.searchPath = 0;
        }
        return;
    }

    public final void setSearchPath(org.xbill.DNS.Name[] p1)
    {
        this.searchPath = p1;
        return;
    }
}
