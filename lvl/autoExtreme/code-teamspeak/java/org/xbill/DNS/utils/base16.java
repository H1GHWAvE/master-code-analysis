package org.xbill.DNS.utils;
public class base16 {
    private static final String Base16 = "0123456789ABCDEF";

    private base16()
    {
        return;
    }

    public static byte[] fromString(String p7)
    {
        int v1 = 0;
        java.io.ByteArrayOutputStream v2_1 = new java.io.ByteArrayOutputStream();
        java.io.DataOutputStream v3_0 = p7.getBytes();
        byte[] v0_0 = 0;
        while (v0_0 < v3_0.length) {
            if (!Character.isWhitespace(((char) v3_0[v0_0]))) {
                v2_1.write(v3_0[v0_0]);
            }
            v0_0++;
        }
        byte[] v0_2;
        byte[] v0_1 = v2_1.toByteArray();
        if ((v0_1.length % 2) == 0) {
            v2_1.reset();
            java.io.DataOutputStream v3_4 = new java.io.DataOutputStream(v2_1);
            while (v1 < v0_1.length) {
                try {
                    v3_4.writeByte(((((byte) "0123456789ABCDEF".indexOf(Character.toUpperCase(((char) v0_1[v1])))) << 4) + ((byte) "0123456789ABCDEF".indexOf(Character.toUpperCase(((char) v0_1[(v1 + 1)]))))));
                } catch (java.io.IOException v4) {
                }
                v1 += 2;
            }
            v0_2 = v2_1.toByteArray();
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public static String toString(byte[] p5)
    {
        byte[] v1_1 = new java.io.ByteArrayOutputStream();
        String v0_0 = 0;
        while (v0_0 < p5.length) {
            char v2_3 = ((short) (p5[v0_0] & 255));
            char v2_5 = ((byte) (v2_3 & 15));
            v1_1.write("0123456789ABCDEF".charAt(((byte) (v2_3 >> 4))));
            v1_1.write("0123456789ABCDEF".charAt(v2_5));
            v0_0++;
        }
        return new String(v1_1.toByteArray());
    }
}
