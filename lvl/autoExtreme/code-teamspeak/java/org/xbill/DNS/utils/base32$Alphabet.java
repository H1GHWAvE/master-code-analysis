package org.xbill.DNS.utils;
public class base32$Alphabet {
    public static final String BASE32 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567=";
    public static final String BASE32HEX = "0123456789ABCDEFGHIJKLMNOPQRSTUV=";

    private base32$Alphabet()
    {
        return;
    }
}
