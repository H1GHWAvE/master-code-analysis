package org.xbill.DNS;
abstract class SingleCompressedNameBase extends org.xbill.DNS.SingleNameBase {
    private static final long serialVersionUID = 18210308676894090939;

    protected SingleCompressedNameBase()
    {
        return;
    }

    protected SingleCompressedNameBase(org.xbill.DNS.Name p1, int p2, int p3, long p4, org.xbill.DNS.Name p6, String p7)
    {
        this(p1, p2, p3, p4, p6, p7);
        return;
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        this.singleName.toWire(p2, p3, p4);
        return;
    }
}
