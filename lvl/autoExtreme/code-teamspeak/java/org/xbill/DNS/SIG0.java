package org.xbill.DNS;
public class SIG0 {
    private static final short VALIDITY = 300;

    private SIG0()
    {
        return;
    }

    public static void signMessage(org.xbill.DNS.Message p6, org.xbill.DNS.KEYRecord p7, java.security.PrivateKey p8, org.xbill.DNS.SIGRecord p9)
    {
        org.xbill.DNS.SIGRecord v0_1 = org.xbill.DNS.Options.intValue("sig0validity");
        if (v0_1 < null) {
            v0_1 = 300;
        }
        org.xbill.DNS.KEYRecord v2_0 = System.currentTimeMillis();
        p6.addRecord(org.xbill.DNS.DNSSEC.signMessage(p6, p9, p7, p8, new java.util.Date(v2_0), new java.util.Date((((long) (v0_1 * 1000)) + v2_0))), 3);
        return;
    }

    public static void verifyMessage(org.xbill.DNS.Message p5, byte[] p6, org.xbill.DNS.KEYRecord p7, org.xbill.DNS.SIGRecord p8)
    {
        org.xbill.DNS.Record[] v3 = p5.getSectionArray(3);
        int v1 = 0;
        while (v1 < v3.length) {
            if ((v3[v1].getType() != 24) || (((org.xbill.DNS.SIGRecord) v3[v1]).getTypeCovered() != 0)) {
                v1++;
            } else {
                org.xbill.DNS.SIGRecord v0_3 = ((org.xbill.DNS.SIGRecord) v3[v1]);
            }
            org.xbill.DNS.DNSSEC.verifyMessage(p5, p6, v0_3, p8, p7);
            return;
        }
        v0_3 = 0;
        org.xbill.DNS.DNSSEC.verifyMessage(p5, p6, v0_3, p8, p7);
        return;
    }
}
