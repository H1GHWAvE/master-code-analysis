package org.xbill.DNS;
public class DNSKEYRecord$Flags {
    public static final int REVOKE = 128;
    public static final int SEP_KEY = 1;
    public static final int ZONE_KEY = 256;

    private DNSKEYRecord$Flags()
    {
        return;
    }
}
