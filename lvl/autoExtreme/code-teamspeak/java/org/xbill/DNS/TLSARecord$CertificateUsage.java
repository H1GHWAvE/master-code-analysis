package org.xbill.DNS;
public class TLSARecord$CertificateUsage {
    public static final int CA_CONSTRAINT = 0;
    public static final int DOMAIN_ISSUED_CERTIFICATE = 3;
    public static final int SERVICE_CERTIFICATE_CONSTRAINT = 1;
    public static final int TRUST_ANCHOR_ASSERTION = 2;

    private TLSARecord$CertificateUsage()
    {
        return;
    }
}
