package net.hockeyapp.android.f;
public final class m extends android.widget.RelativeLayout {
    public static final int a = 4097;
    public static final int b = 4098;
    public static final int c = 4099;
    public static final int d = 4100;
    public static final int e = 4101;
    protected android.widget.RelativeLayout f;
    protected boolean g;
    protected boolean h;

    public m(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private m(android.content.Context p3, byte p4)
    {
        this(p3, 1, 0);
        return;
    }

    private m(android.content.Context p3, char p4)
    {
        this(p3, 1, 0);
        return;
    }

    public m(android.content.Context p12, boolean p13, boolean p14)
    {
        this(p12);
        this.f = 0;
        this.g = 0;
        this.h = 0;
        if (!p13) {
            this.g = 0;
        } else {
            this.setLayoutHorizontally(p12);
        }
        android.widget.ImageView v0_8;
        this.h = p14;
        android.widget.ImageView v0_2 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        this.setBackgroundColor(-1);
        this.setLayoutParams(v0_2);
        this.f = new android.widget.RelativeLayout(p12);
        this.f.setId(4097);
        if (!this.g) {
            v0_8 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
            this.f.setPadding(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())));
        } else {
            v0_8 = new android.widget.RelativeLayout$LayoutParams(((int) android.util.TypedValue.applyDimension(1, 1132068864, this.getResources().getDisplayMetrics())), -1);
            v0_8.addRule(9, -1);
            this.f.setPadding(0, 0, 0, 0);
        }
        this.f.setLayoutParams(v0_8);
        this.f.setBackgroundColor(android.graphics.Color.rgb(230, 236, 239));
        android.widget.ImageView v0_11 = this.f;
        android.widget.RelativeLayout$LayoutParams v2_11 = new android.widget.TextView(p12);
        v2_11.setId(4098);
        int v3_9 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v3_9.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0);
        v2_11.setBackgroundColor(android.graphics.Color.rgb(230, 236, 239));
        v2_11.setLayoutParams(v3_9);
        v2_11.setEllipsize(android.text.TextUtils$TruncateAt.END);
        v2_11.setShadowLayer(1065353216, 0, 1065353216, -1);
        v2_11.setSingleLine(1);
        v2_11.setTextColor(-16777216);
        v2_11.setTextSize(2, 1101004800);
        v2_11.setTypeface(0, 1);
        v0_11.addView(v2_11);
        android.widget.ImageView v0_12 = this.f;
        android.widget.RelativeLayout$LayoutParams v2_13 = new android.widget.TextView(p12);
        v2_13.setId(4099);
        int v3_17 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v3_17.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0);
        v3_17.addRule(3, 4098);
        v2_13.setBackgroundColor(android.graphics.Color.rgb(230, 236, 239));
        v2_13.setLayoutParams(v3_17);
        v2_13.setEllipsize(android.text.TextUtils$TruncateAt.END);
        v2_13.setShadowLayer(1065353216, 0, 1065353216, -1);
        v2_13.setLines(2);
        v2_13.setLineSpacing(0, 1066192077);
        v2_13.setTextColor(-16777216);
        v2_13.setTextSize(2, 1098907648);
        v2_13.setTypeface(0, 1);
        v0_12.addView(v2_13);
        android.widget.ImageView v0_13 = this.f;
        android.widget.RelativeLayout$LayoutParams v2_15 = new android.widget.Button(p12);
        v2_15.setId(4100);
        int v3_29 = ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics()));
        android.graphics.drawable.Drawable v5_11 = new android.widget.RelativeLayout$LayoutParams(((int) android.util.TypedValue.applyDimension(1, 1123024896, this.getResources().getDisplayMetrics())), -2);
        v5_11.setMargins(v3_29, v3_29, v3_29, v3_29);
        v5_11.addRule(9, -1);
        v5_11.addRule(3, 4099);
        v2_15.setLayoutParams(v5_11);
        v2_15.setBackgroundDrawable(this.getButtonSelector());
        v2_15.setText("Update");
        v2_15.setTextColor(-1);
        v2_15.setTextSize(2, 1098907648);
        v0_13.addView(v2_15);
        this.addView(this.f);
        android.widget.RelativeLayout$LayoutParams v2_17 = new android.webkit.WebView(p12);
        v2_17.setId(4101);
        android.widget.ImageView v0_18 = ((int) android.util.TypedValue.applyDimension(1, 1137180672, p12.getResources().getDisplayMetrics()));
        if (!this.h) {
            v0_18 = -1;
        }
        int v3_38 = new android.widget.RelativeLayout$LayoutParams(-1, v0_18);
        if (!this.g) {
            v3_38.addRule(3, 4097);
        } else {
            v3_38.addRule(1, 4097);
        }
        android.widget.ImageView v0_26;
        v3_38.setMargins(0, 0, 0, 0);
        v2_17.setLayoutParams(v3_38);
        v2_17.setBackgroundColor(-1);
        this.addView(v2_17);
        android.widget.RelativeLayout$LayoutParams v2_18 = this.f;
        int v3_41 = ((int) android.util.TypedValue.applyDimension(1, 1077936128, this.getResources().getDisplayMetrics()));
        android.widget.ImageView v4_26 = new android.widget.ImageView(p12);
        if (!this.g) {
            v0_26 = new android.widget.RelativeLayout$LayoutParams(-1, v3_41);
            v0_26.addRule(10, -1);
            v4_26.setBackgroundDrawable(net.hockeyapp.android.e.aa.a());
        } else {
            v0_26 = new android.widget.RelativeLayout$LayoutParams(1, -1);
            v0_26.addRule(11, -1);
            v4_26.setBackgroundDrawable(new android.graphics.drawable.ColorDrawable(-16777216));
        }
        v4_26.setLayoutParams(v0_26);
        v2_18.addView(v4_26);
        android.widget.ImageView v0_29 = new android.widget.ImageView(p12);
        android.widget.RelativeLayout$LayoutParams v2_20 = new android.widget.RelativeLayout$LayoutParams(-1, v3_41);
        if (!this.g) {
            v2_20.addRule(3, 4097);
        } else {
            v2_20.addRule(10, -1);
        }
        v0_29.setLayoutParams(v2_20);
        v0_29.setBackgroundDrawable(net.hockeyapp.android.e.aa.a());
        this.addView(v0_29);
        return;
    }

    private void a()
    {
        android.widget.RelativeLayout$LayoutParams v0_1 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        this.setBackgroundColor(-1);
        this.setLayoutParams(v0_1);
        return;
    }

    private void a(android.content.Context p12)
    {
        android.widget.RelativeLayout v0_5;
        this.f = new android.widget.RelativeLayout(p12);
        this.f.setId(4097);
        if (!this.g) {
            v0_5 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
            this.f.setPadding(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())));
        } else {
            v0_5 = new android.widget.RelativeLayout$LayoutParams(((int) android.util.TypedValue.applyDimension(1, 1132068864, this.getResources().getDisplayMetrics())), -1);
            v0_5.addRule(9, -1);
            this.f.setPadding(0, 0, 0, 0);
        }
        this.f.setLayoutParams(v0_5);
        this.f.setBackgroundColor(android.graphics.Color.rgb(230, 236, 239));
        android.widget.RelativeLayout v0_8 = this.f;
        android.widget.Button v1_11 = new android.widget.TextView(p12);
        v1_11.setId(4098);
        int v2_9 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_9.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0);
        v1_11.setBackgroundColor(android.graphics.Color.rgb(230, 236, 239));
        v1_11.setLayoutParams(v2_9);
        v1_11.setEllipsize(android.text.TextUtils$TruncateAt.END);
        v1_11.setShadowLayer(1065353216, 0, 1065353216, -1);
        v1_11.setSingleLine(1);
        v1_11.setTextColor(-16777216);
        v1_11.setTextSize(2, 1101004800);
        v1_11.setTypeface(0, 1);
        v0_8.addView(v1_11);
        android.widget.RelativeLayout v0_9 = this.f;
        android.widget.Button v1_13 = new android.widget.TextView(p12);
        v1_13.setId(4099);
        int v2_17 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_17.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0);
        v2_17.addRule(3, 4098);
        v1_13.setBackgroundColor(android.graphics.Color.rgb(230, 236, 239));
        v1_13.setLayoutParams(v2_17);
        v1_13.setEllipsize(android.text.TextUtils$TruncateAt.END);
        v1_13.setShadowLayer(1065353216, 0, 1065353216, -1);
        v1_13.setLines(2);
        v1_13.setLineSpacing(0, 1066192077);
        v1_13.setTextColor(-16777216);
        v1_13.setTextSize(2, 1098907648);
        v1_13.setTypeface(0, 1);
        v0_9.addView(v1_13);
        android.widget.RelativeLayout v0_10 = this.f;
        android.widget.Button v1_15 = new android.widget.Button(p12);
        v1_15.setId(4100);
        int v2_29 = ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics()));
        android.widget.RelativeLayout$LayoutParams v4_11 = new android.widget.RelativeLayout$LayoutParams(((int) android.util.TypedValue.applyDimension(1, 1123024896, this.getResources().getDisplayMetrics())), -2);
        v4_11.setMargins(v2_29, v2_29, v2_29, v2_29);
        v4_11.addRule(9, -1);
        v4_11.addRule(3, 4099);
        v1_15.setLayoutParams(v4_11);
        v1_15.setBackgroundDrawable(this.getButtonSelector());
        v1_15.setText("Update");
        v1_15.setTextColor(-1);
        v1_15.setTextSize(2, 1098907648);
        v0_10.addView(v1_15);
        this.addView(this.f);
        return;
    }

    private void a(android.widget.RelativeLayout p9, android.content.Context p10)
    {
        android.widget.TextView v0_1 = new android.widget.TextView(p10);
        v0_1.setId(4098);
        int v1_2 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v1_2.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0);
        v0_1.setBackgroundColor(android.graphics.Color.rgb(230, 236, 239));
        v0_1.setLayoutParams(v1_2);
        v0_1.setEllipsize(android.text.TextUtils$TruncateAt.END);
        v0_1.setShadowLayer(1065353216, 0, 1065353216, -1);
        v0_1.setSingleLine(1);
        v0_1.setTextColor(-16777216);
        v0_1.setTextSize(2, 1101004800);
        v0_1.setTypeface(0, 1);
        p9.addView(v0_1);
        return;
    }

    private void b(android.content.Context p9)
    {
        android.webkit.WebView v2_1 = new android.webkit.WebView(p9);
        v2_1.setId(4101);
        int v0_3 = ((int) android.util.TypedValue.applyDimension(1, 1137180672, p9.getResources().getDisplayMetrics()));
        if (!this.h) {
            v0_3 = -1;
        }
        android.widget.RelativeLayout$LayoutParams v3_3 = new android.widget.RelativeLayout$LayoutParams(-1, v0_3);
        if (!this.g) {
            v3_3.addRule(3, 4097);
        } else {
            v3_3.addRule(1, 4097);
        }
        v3_3.setMargins(0, 0, 0, 0);
        v2_1.setLayoutParams(v3_3);
        v2_1.setBackgroundColor(-1);
        this.addView(v2_1);
        return;
    }

    private void b(android.widget.RelativeLayout p10, android.content.Context p11)
    {
        android.widget.TextView v0_1 = new android.widget.TextView(p11);
        v0_1.setId(4099);
        int v1_2 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v1_2.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0);
        v1_2.addRule(3, 4098);
        v0_1.setBackgroundColor(android.graphics.Color.rgb(230, 236, 239));
        v0_1.setLayoutParams(v1_2);
        v0_1.setEllipsize(android.text.TextUtils$TruncateAt.END);
        v0_1.setShadowLayer(1065353216, 0, 1065353216, -1);
        v0_1.setLines(2);
        v0_1.setLineSpacing(0, 1066192077);
        v0_1.setTextColor(-16777216);
        v0_1.setTextSize(2, 1098907648);
        v0_1.setTypeface(0, 1);
        p10.addView(v0_1);
        return;
    }

    private void c(android.widget.RelativeLayout p7, android.content.Context p8)
    {
        android.widget.Button v0_1 = new android.widget.Button(p8);
        v0_1.setId(4100);
        int v1_3 = ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics()));
        android.widget.RelativeLayout$LayoutParams v3_3 = new android.widget.RelativeLayout$LayoutParams(((int) android.util.TypedValue.applyDimension(1, 1123024896, this.getResources().getDisplayMetrics())), -2);
        v3_3.setMargins(v1_3, v1_3, v1_3, v1_3);
        v3_3.addRule(9, -1);
        v3_3.addRule(3, 4099);
        v0_1.setLayoutParams(v3_3);
        v0_1.setBackgroundDrawable(this.getButtonSelector());
        v0_1.setText("Update");
        v0_1.setTextColor(-1);
        v0_1.setTextSize(2, 1098907648);
        p7.addView(v0_1);
        return;
    }

    private void d(android.widget.RelativeLayout p8, android.content.Context p9)
    {
        android.widget.ImageView v0_4;
        android.graphics.drawable.Drawable v1_2 = ((int) android.util.TypedValue.applyDimension(1, 1077936128, this.getResources().getDisplayMetrics()));
        android.widget.RelativeLayout$LayoutParams v2_1 = new android.widget.ImageView(p9);
        if (!this.g) {
            v0_4 = new android.widget.RelativeLayout$LayoutParams(-1, v1_2);
            v0_4.addRule(10, -1);
            v2_1.setBackgroundDrawable(net.hockeyapp.android.e.aa.a());
        } else {
            v0_4 = new android.widget.RelativeLayout$LayoutParams(1, -1);
            v0_4.addRule(11, -1);
            v2_1.setBackgroundDrawable(new android.graphics.drawable.ColorDrawable(-16777216));
        }
        v2_1.setLayoutParams(v0_4);
        p8.addView(v2_1);
        android.widget.ImageView v0_7 = new android.widget.ImageView(p9);
        android.widget.RelativeLayout$LayoutParams v2_3 = new android.widget.RelativeLayout$LayoutParams(-1, v1_2);
        if (!this.g) {
            v2_3.addRule(3, 4097);
        } else {
            v2_3.addRule(10, -1);
        }
        v0_7.setLayoutParams(v2_3);
        v0_7.setBackgroundDrawable(net.hockeyapp.android.e.aa.a());
        this.addView(v0_7);
        return;
    }

    private android.graphics.drawable.Drawable getButtonSelector()
    {
        android.graphics.drawable.StateListDrawable v0_1 = new android.graphics.drawable.StateListDrawable();
        int[] v1_0 = new int[1];
        v1_0[0] = -16842919;
        v0_1.addState(v1_0, new android.graphics.drawable.ColorDrawable(-16777216));
        int[] v1_2 = new int[2];
        v1_2 = {-16842919, 16842908};
        v0_1.addState(v1_2, new android.graphics.drawable.ColorDrawable(-12303292));
        int[] v1_3 = new int[1];
        v1_3[0] = 16842919;
        v0_1.addState(v1_3, new android.graphics.drawable.ColorDrawable(-7829368));
        return v0_1;
    }

    private void setLayoutHorizontally(android.content.Context p3)
    {
        if (this.getResources().getConfiguration().orientation != 2) {
            this.g = 0;
        } else {
            this.g = 1;
        }
        return;
    }
}
