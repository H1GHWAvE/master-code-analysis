package net.hockeyapp.android.d;
public final class o extends net.hockeyapp.android.d.l {
    public long h;

    public o(android.content.Context p1, String p2, net.hockeyapp.android.b.a p3)
    {
        this(p1, p2, p3);
        return;
    }

    private long d()
    {
        return this.h;
    }

    protected final void a(Long p5)
    {
        this.h = p5.longValue();
        if (this.h <= 0) {
            this.c.a(Boolean.valueOf(0));
        } else {
            this.c.a(this);
        }
        return;
    }

    protected final varargs void a(Integer[] p1)
    {
        return;
    }

    protected final varargs Long b()
    {
        try {
            Long v0_5 = Long.valueOf(((long) net.hockeyapp.android.d.o.a(new java.net.URL(this.c()), 6).getContentLength()));
        } catch (Long v0_6) {
            v0_6.printStackTrace();
            v0_5 = Long.valueOf(0);
        }
        return v0_5;
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return this.b();
    }

    protected final synthetic void onPostExecute(Object p1)
    {
        this.a(((Long) p1));
        return;
    }

    protected final bridge synthetic void onProgressUpdate(Object[] p1)
    {
        return;
    }
}
