package net.hockeyapp.android;
final class q implements java.lang.Runnable {
    final synthetic net.hockeyapp.android.c.h a;
    final synthetic net.hockeyapp.android.FeedbackActivity b;

    q(net.hockeyapp.android.FeedbackActivity p1, net.hockeyapp.android.c.h p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void run()
    {
        this.b.b(1);
        java.util.Iterator v1_2 = new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss\'Z\'");
        java.util.ArrayList v2_1 = new java.text.SimpleDateFormat("d MMM h:mm a");
        if ((this.a != null) && ((this.a.b != null) && ((this.a.b.e != null) && (this.a.b.e.size() > 0)))) {
            net.hockeyapp.android.FeedbackActivity.a(this.b, this.a.b.e);
            java.util.Collections.reverse(net.hockeyapp.android.FeedbackActivity.e(this.b));
            try {
                net.hockeyapp.android.a.a v0_21 = v1_2.parse(((net.hockeyapp.android.c.g) net.hockeyapp.android.FeedbackActivity.e(this.b).get(0)).f);
                java.util.Iterator v1_4 = net.hockeyapp.android.FeedbackActivity.f(this.b);
                java.util.ArrayList v3_8 = new StringBuilder().append(net.hockeyapp.android.aj.a(1030)).append(" %s").toString();
                Object[] v4_4 = new Object[1];
                v4_4[0] = v2_1.format(v0_21);
                v1_4.setText(String.format(v3_8, v4_4));
            } catch (net.hockeyapp.android.a.a v0_24) {
                v0_24.printStackTrace();
            }
            if (net.hockeyapp.android.FeedbackActivity.g(this.b) != null) {
                net.hockeyapp.android.a.a v0_28 = net.hockeyapp.android.FeedbackActivity.g(this.b);
                if (v0_28.a != null) {
                    v0_28.a.clear();
                }
                java.util.Iterator v1_6 = net.hockeyapp.android.FeedbackActivity.e(this.b).iterator();
                while (v1_6.hasNext()) {
                    net.hockeyapp.android.a.a v0_36 = ((net.hockeyapp.android.c.g) v1_6.next());
                    java.util.ArrayList v2_3 = net.hockeyapp.android.FeedbackActivity.g(this.b);
                    if ((v0_36 != null) && (v2_3.a != null)) {
                        v2_3.a.add(v0_36);
                    }
                }
                net.hockeyapp.android.FeedbackActivity.g(this.b).notifyDataSetChanged();
            } else {
                net.hockeyapp.android.FeedbackActivity.a(this.b, new net.hockeyapp.android.a.a(net.hockeyapp.android.FeedbackActivity.c(this.b), net.hockeyapp.android.FeedbackActivity.e(this.b)));
            }
            net.hockeyapp.android.FeedbackActivity.h(this.b).setAdapter(net.hockeyapp.android.FeedbackActivity.g(this.b));
        }
        return;
    }
}
