package net.hockeyapp.android.e;
public final class m {
    public static final int a = 0;
    public static final int b = 1;

    public m()
    {
        return;
    }

    public static int a(android.content.Context p2, android.net.Uri p3)
    {
        try {
            java.io.InputStream v1 = p2.getContentResolver().openInputStream(p3);
            Throwable v0_1 = net.hockeyapp.android.e.m.a(v1);
        } catch (Throwable v0_2) {
            if (v1 != null) {
                v1.close();
            }
            throw v0_2;
        }
        if (v1 != null) {
            v1.close();
        }
        return v0_1;
    }

    public static int a(android.graphics.BitmapFactory$Options p4, int p5, int p6)
    {
        int v1_0 = p4.outHeight;
        int v2_0 = p4.outWidth;
        int v0 = 1;
        if ((v1_0 > p6) || (v2_0 > p5)) {
            while ((((v1_0 / 2) / v0) > p6) && (((v2_0 / 2) / v0) > p5)) {
                v0 *= 2;
            }
        }
        return v0;
    }

    public static int a(java.io.File p3)
    {
        try {
            int v1_1 = new java.io.FileInputStream(p3);
            try {
                Throwable v0_1 = net.hockeyapp.android.e.m.a(v1_1);
                v1_1.close();
                return v0_1;
            } catch (Throwable v0_0) {
                if (v1_1 != 0) {
                    v1_1.close();
                }
                throw v0_0;
            }
        } catch (Throwable v0_0) {
            v1_1 = 0;
        }
        v0_1 = net.hockeyapp.android.e.m.a(v1_1);
        v1_1.close();
        return v0_1;
    }

    private static int a(java.io.InputStream p5)
    {
        int v0 = 1;
        float v2_1 = new android.graphics.BitmapFactory$Options();
        v2_1.inJustDecodeBounds = 1;
        android.graphics.BitmapFactory.decodeStream(p5, 0, v2_1);
        if ((v2_1.outWidth != -1) && (v2_1.outHeight != -1)) {
            if ((((float) v2_1.outWidth) / ((float) v2_1.outHeight)) <= 1065353216) {
                v0 = 0;
            }
        } else {
            v0 = 0;
        }
        return v0;
    }

    private static android.graphics.Bitmap a(android.content.Context p3, android.net.Uri p4, int p5, int p6)
    {
        android.graphics.Bitmap v0_1 = new android.graphics.BitmapFactory$Options();
        v0_1.inJustDecodeBounds = 1;
        android.graphics.BitmapFactory.decodeStream(p3.getContentResolver().openInputStream(p4), 0, v0_1);
        v0_1.inSampleSize = net.hockeyapp.android.e.m.a(v0_1, p5, p6);
        v0_1.inJustDecodeBounds = 0;
        return android.graphics.BitmapFactory.decodeStream(p3.getContentResolver().openInputStream(p4), 0, v0_1);
    }

    private static android.graphics.Bitmap a(java.io.File p2, int p3, int p4)
    {
        android.graphics.Bitmap v0_1 = new android.graphics.BitmapFactory$Options();
        v0_1.inJustDecodeBounds = 1;
        android.graphics.BitmapFactory.decodeFile(p2.getAbsolutePath(), v0_1);
        v0_1.inSampleSize = net.hockeyapp.android.e.m.a(v0_1, p3, p4);
        v0_1.inJustDecodeBounds = 0;
        return android.graphics.BitmapFactory.decodeFile(p2.getAbsolutePath(), v0_1);
    }
}
