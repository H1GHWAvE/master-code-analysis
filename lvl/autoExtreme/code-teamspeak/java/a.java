final class La implements java.lang.Runnable {
    private final java.net.Socket a;
    private final jnamed b;

    a(jnamed p1, java.net.Socket p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void run()
    {
        java.io.IOException v0_0 = this.b;
        java.io.IOException v1 = this.a;
        try {
            int v3_1 = new java.io.DataInputStream(v1.getInputStream());
            java.io.DataOutputStream v2_1 = v3_1.readUnsignedShort();
            String v4_0 = new byte[v2_1];
            v3_1.readFully(v4_0);
            try {
                java.io.IOException v0_1 = v0_0.a(new org.xbill.DNS.Message(v4_0), v4_0, v2_1, v1);
            } catch (java.io.IOException v0) {
                v0_1 = jnamed.a(v4_0);
                java.io.DataOutputStream v2_3 = new java.io.DataOutputStream(v1.getOutputStream());
                v2_3.writeShort(v0_1.length);
                v2_3.write(v0_1);
                try {
                    v1.close();
                } catch (java.io.IOException v0) {
                }
                return;
            }
            if (v0_1 != null) {
            } else {
                try {
                    v1.close();
                } catch (java.io.IOException v0) {
                }
                return;
            }
        } catch (java.io.IOException v0_5) {
            try {
                v1.close();
            } catch (java.io.IOException v1) {
            }
            throw v0_5;
        } catch (java.io.IOException v0_2) {
            System.out.println(new StringBuffer("TCPclient(").append(jnamed.a(v1.getLocalAddress(), v1.getLocalPort())).append("): ").append(v0_2).toString());
            try {
                v1.close();
            } catch (java.io.IOException v0) {
            }
            return;
        }
    }
}
