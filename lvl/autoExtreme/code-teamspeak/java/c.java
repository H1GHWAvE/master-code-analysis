final class Lc implements java.lang.Runnable {
    private final java.net.InetAddress a;
    private final int b;
    private final jnamed c;

    c(jnamed p1, java.net.InetAddress p2, int p3)
    {
        this.c = p1;
        this.a = p2;
        this.b = p3;
        return;
    }

    public final void run()
    {
        StringBuffer v2_0 = this.c;
        String v3_0 = this.a;
        int v4 = this.b;
        try {
            String v5_1 = new java.net.DatagramSocket(v4, v3_0);
            byte[] v6 = new byte[512];
            java.net.DatagramPacket v7_1 = new java.net.DatagramPacket(v6, 512);
            java.net.DatagramPacket v1_2 = 0;
        } catch (java.net.DatagramPacket v0_9) {
            System.out.println(new StringBuffer("serveUDP(").append(jnamed.a(v3_0, v4)).append("): ").append(v0_9).toString());
            return;
        }
        do {
            v7_1.setLength(512);
            v5_1.receive(v7_1);
            java.net.DatagramPacket v0_4 = v2_0.a(new org.xbill.DNS.Message(v6), v6, v7_1.getLength(), 0);
        } while(v0_4 == null);
        java.net.DatagramPacket v0_8;
        if (v1_2 != null) {
            v1_2.setData(v0_4);
            v1_2.setLength(v0_4.length);
            v1_2.setAddress(v7_1.getAddress());
            v1_2.setPort(v7_1.getPort());
            v0_8 = v1_2;
        } else {
            v0_8 = new java.net.DatagramPacket(v0_4, v0_4.length, v7_1.getAddress(), v7_1.getPort());
        }
        v5_1.send(v0_8);
        v1_2 = v0_8;
    }
}
