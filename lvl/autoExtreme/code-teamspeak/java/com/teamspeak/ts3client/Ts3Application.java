package com.teamspeak.ts3client;
public class Ts3Application extends android.app.Application {
    private static com.teamspeak.ts3client.Ts3Application p;
    public com.teamspeak.ts3client.data.e a;
    public android.os.PowerManager$WakeLock b;
    public android.support.v4.app.Fragment c;
    public java.util.logging.Logger d;
    public android.content.SharedPreferences e;
    public com.teamspeak.ts3client.StartGUIFragment f;
    boolean g;
    public android.net.ConnectivityManager h;
    java.util.Vector i;
    java.util.Vector j;
    public android.content.Intent k;
    public android.app.NotificationManager l;
    public android.os.PowerManager$WakeLock m;
    public android.net.wifi.WifiManager$WifiLock n;
    public android.support.v7.app.a o;
    private boolean q;
    private com.teamspeak.ts3client.data.d.f r;
    private com.teamspeak.ts3client.a.p s;

    public Ts3Application()
    {
        return;
    }

    public static com.teamspeak.ts3client.Ts3Application a()
    {
        return com.teamspeak.ts3client.Ts3Application.p;
    }

    private void a(android.app.NotificationManager p1)
    {
        this.l = p1;
        return;
    }

    private void a(android.content.Intent p1)
    {
        this.k = p1;
        return;
    }

    private void a(android.content.SharedPreferences p1)
    {
        this.e = p1;
        return;
    }

    private void a(android.net.ConnectivityManager p1)
    {
        this.h = p1;
        return;
    }

    private void a(android.net.wifi.WifiManager$WifiLock p1)
    {
        this.n = p1;
        return;
    }

    private void a(android.os.PowerManager$WakeLock p1)
    {
        this.b = p1;
        return;
    }

    private void a(android.support.v4.app.Fragment p1)
    {
        this.c = p1;
        return;
    }

    private void a(android.support.v7.app.a p1)
    {
        this.o = p1;
        return;
    }

    private void a(com.teamspeak.ts3client.StartGUIFragment p1)
    {
        this.f = p1;
        return;
    }

    private void a(com.teamspeak.ts3client.data.e p1)
    {
        this.a = p1;
        return;
    }

    private void a(java.util.Vector p1)
    {
        this.j = p1;
        return;
    }

    private void a(java.util.logging.Logger p1)
    {
        this.d = p1;
        return;
    }

    private void a(boolean p1)
    {
        this.q = p1;
        return;
    }

    private void b(android.os.PowerManager$WakeLock p1)
    {
        this.m = p1;
        return;
    }

    private void b(java.util.Vector p1)
    {
        this.i = p1;
        return;
    }

    private void k()
    {
        this.g = 0;
        return;
    }

    private void l()
    {
        this.g = 1;
        return;
    }

    private android.net.ConnectivityManager m()
    {
        return this.h;
    }

    private com.teamspeak.ts3client.StartGUIFragment n()
    {
        return this.f;
    }

    private android.os.PowerManager$WakeLock o()
    {
        return this.b;
    }

    private android.os.PowerManager$WakeLock p()
    {
        return this.m;
    }

    private android.net.wifi.WifiManager$WifiLock q()
    {
        return this.n;
    }

    private boolean r()
    {
        return this.g;
    }

    private boolean s()
    {
        return this.q;
    }

    private static boolean t()
    {
        return 0;
    }

    private java.util.Vector u()
    {
        return this.j;
    }

    private java.util.Vector v()
    {
        return this.i;
    }

    private android.content.Intent w()
    {
        return this.k;
    }

    private android.support.v7.app.a x()
    {
        return this.o;
    }

    public final com.teamspeak.ts3client.data.d.f b()
    {
        if (this.r == null) {
            this.r = new com.teamspeak.ts3client.data.d.f(this);
        }
        return this.r;
    }

    public final com.teamspeak.ts3client.data.e c()
    {
        return this.a;
    }

    public final android.support.v4.app.Fragment d()
    {
        return this.c;
    }

    public final java.util.logging.Logger e()
    {
        return this.d;
    }

    public final android.content.SharedPreferences f()
    {
        return this.e;
    }

    public final com.teamspeak.ts3client.a.p g()
    {
        if (this.s == null) {
            this.s = new com.teamspeak.ts3client.a.p(this);
        }
        return this.s;
    }

    public final int h()
    {
        return ((Integer) this.j.get(0)).intValue();
    }

    public final int i()
    {
        return ((Integer) this.i.get(0)).intValue();
    }

    public final android.app.NotificationManager j()
    {
        return this.l;
    }

    public void onCreate()
    {
        com.teamspeak.ts3client.Ts3Application.p = this;
        super.onCreate();
        return;
    }
}
