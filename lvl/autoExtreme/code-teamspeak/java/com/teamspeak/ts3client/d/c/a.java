package com.teamspeak.ts3client.d.c;
public final class a extends com.teamspeak.ts3client.d.c.i {

    public a()
    {
        return;
    }

    public final void a()
    {
        return;
    }

    public final void a(android.view.Menu p8, com.teamspeak.ts3client.Ts3Application p9)
    {
        com.teamspeak.ts3client.d.c.a.a(p8, 2130837679, com.teamspeak.ts3client.data.e.a.a("menu.settings"), 1, 1);
        com.teamspeak.ts3client.d.c.a.a(p8, 2130837631, com.teamspeak.ts3client.data.e.a.a("menu.identity"), 2, 1);
        com.teamspeak.ts3client.d.c.a.a(p8, 2130837600, com.teamspeak.ts3client.data.e.a.a("menu.contact"), 3, 1);
        com.teamspeak.ts3client.d.c.a.a(p8, 2130837604, com.teamspeak.ts3client.data.e.a.a("menu.copyright"), 4, 2);
        com.teamspeak.ts3client.d.c.a.a(p8, 2130837601, com.teamspeak.ts3client.data.e.a.a("menu.contactbug"), 5, 2);
        com.teamspeak.ts3client.d.c.a.a(p8, 2130837632, com.teamspeak.ts3client.data.e.a.a("menu.changelog"), 6, 1);
        com.teamspeak.ts3client.d.c.a.a(p8, 2130837632, com.teamspeak.ts3client.data.e.a.a("menu.licenseagreement"), 7, 3);
        return;
    }

    public final boolean a(android.view.MenuItem p7, com.teamspeak.ts3client.Ts3Application p8)
    {
        int v0 = 1;
        com.teamspeak.ts3client.StartGUIFragment v2_2 = p8.f.c().a();
        switch (p7.getItemId()) {
            case 1:
                if (p8.f.c().a("Settings") != null) {
                    v2_2.a(p8.f.c().a("Settings"));
                } else {
                    v2_2.b(new com.teamspeak.ts3client.f.as(), "Settings");
                }
                v2_2.f();
                v2_2.a(4097);
                v2_2.i();
                break;
            case 2:
                if (p8.f.c().a("IdentManager") != null) {
                    v2_2.a(p8.f.c().a("IdentManager"));
                } else {
                    v2_2.b(new com.teamspeak.ts3client.e.b(), "IdentManager");
                }
                v2_2.f();
                v2_2.a(4097);
                v2_2.i();
                break;
            case 3:
                if (p8.f.c().a("Contact") != null) {
                    v2_2.a(p8.f.c().a("Contact"));
                } else {
                    v2_2.b(new com.teamspeak.ts3client.c.b(), "Contact");
                }
                v2_2.f();
                v2_2.a(4097);
                v2_2.i();
                break;
            case 4:
                android.content.Intent v1_7 = new android.app.Dialog(p8.f, 2131165312);
                com.teamspeak.ts3client.data.d.q.a(v1_7);
                com.teamspeak.ts3client.StartGUIFragment v2_15 = new android.widget.LinearLayout(p8.f);
                v2_15.setOrientation(1);
                v1_7.setContentView(v2_15);
                String v3_15 = new android.webkit.WebView(p8.f);
                v3_15.loadUrl("file:///android_asset/copyright/copyright.html");
                v2_15.addView(v3_15);
                v1_7.setTitle(com.teamspeak.ts3client.data.e.a.a("menu.copyright.text"));
                v1_7.show();
                break;
            case 5:
                com.teamspeak.ts3client.StartGUIFragment v2_12 = new android.app.AlertDialog$Builder(p8.f).create();
                v2_12.setTitle(com.teamspeak.ts3client.data.e.a.a("contactbug.info"));
                v2_12.setMessage(com.teamspeak.ts3client.data.e.a.a("contactbug.text"));
                v2_12.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.d.c.b(this, v2_12));
                v2_12.setButton(-1, com.teamspeak.ts3client.data.e.a.a("button.open"), new com.teamspeak.ts3client.d.c.c(this, p8, v2_12));
                v2_12.setCancelable(0);
                v2_12.show();
                break;
            case 6:
                p8.f.q.show();
                break;
            case 7:
                com.teamspeak.ts3client.StartGUIFragment v2_5 = com.teamspeak.ts3client.Ts3Application.a().e.getString("la_url", "");
                if (!v2_5.isEmpty()) {
                    android.content.Intent v1_2 = new android.content.Intent("android.intent.action.VIEW");
                    v1_2.setData(android.net.Uri.parse(v2_5));
                    p8.f.startActivity(v1_2);
                } else {
                    p8.e.edit().putInt("LicenseAgreement", 0).commit();
                }
                break;
            default:
                v0 = 0;
        }
        return v0;
    }
}
