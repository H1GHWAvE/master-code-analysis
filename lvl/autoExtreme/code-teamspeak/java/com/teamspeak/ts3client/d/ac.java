package com.teamspeak.ts3client.d;
final class ac implements java.lang.Runnable {
    final synthetic java.text.DecimalFormat a;
    final synthetic com.teamspeak.ts3client.d.z b;

    ac(com.teamspeak.ts3client.d.z p1, java.text.DecimalFormat p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void run()
    {
        com.teamspeak.ts3client.d.z.d(this.b).setText(com.teamspeak.ts3client.d.z.b(this.b).a.a.b(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.i.b));
        com.teamspeak.ts3client.d.z.e(this.b).setText(new StringBuilder().append(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.a)).append("ms").toString());
        com.teamspeak.ts3client.d.z.f(this.b).setText(this.a.format(((double) com.teamspeak.ts3client.d.z.b(this.b).a.a.ts3client_getServerConnectionVariableAsFloat(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.B.ap))));
        com.teamspeak.ts3client.d.z.g(this.b).setText(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.t), 0));
        com.teamspeak.ts3client.d.z.h(this.b).setText(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.l), 0));
        com.teamspeak.ts3client.d.z.i(this.b).setText(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.x), 1));
        com.teamspeak.ts3client.d.z.j(this.b).setText(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.p), 1));
        com.teamspeak.ts3client.d.z.k(this.b).setText(new StringBuilder().append(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.V), 1)).append("/s").toString());
        com.teamspeak.ts3client.d.z.l(this.b).setText(new StringBuilder().append(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.N), 1)).append("/s").toString());
        com.teamspeak.ts3client.d.z.m(this.b).setText(new StringBuilder().append(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.Z), 1)).append("/s").toString());
        com.teamspeak.ts3client.d.z.n(this.b).setText(new StringBuilder().append(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.R), 1)).append("/s").toString());
        com.teamspeak.ts3client.d.z.o(this.b).setText(new StringBuilder().append(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.al), 1)).append("/s").toString());
        com.teamspeak.ts3client.d.z.p(this.b).setText(new StringBuilder().append(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.ak), 1)).append("/s").toString());
        com.teamspeak.ts3client.d.z.q(this.b).setText(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.am), 1));
        com.teamspeak.ts3client.d.z.r(this.b).setText(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.z.b(this.b).a.a.a(com.teamspeak.ts3client.d.z.b(this.b).a.e, com.teamspeak.ts3client.jni.f.an), 1));
        return;
    }
}
