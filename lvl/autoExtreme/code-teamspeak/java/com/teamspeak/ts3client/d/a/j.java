package com.teamspeak.ts3client.d.a;
final class j implements android.view.View$OnClickListener {
    final synthetic android.widget.ArrayAdapter a;
    final synthetic android.widget.Spinner b;
    final synthetic android.app.Dialog c;
    final synthetic com.teamspeak.ts3client.d.a.h d;

    j(com.teamspeak.ts3client.d.a.h p1, android.widget.ArrayAdapter p2, android.widget.Spinner p3, android.app.Dialog p4)
    {
        this.d = p1;
        this.a = p2;
        this.b = p3;
        this.c = p4;
        return;
    }

    public final void onClick(android.view.View p12)
    {
        android.app.Dialog v0_2 = ((com.teamspeak.ts3client.data.a) this.a.getItem(this.b.getSelectedItemPosition()));
        if (v0_2.e != com.teamspeak.ts3client.d.a.a.a(this.d.a).e) {
            com.teamspeak.ts3client.d.a.a.y().a.a.ts3client_requestChannelMove(com.teamspeak.ts3client.d.a.a.y().a.e, com.teamspeak.ts3client.d.a.a.a(this.d.a).b, v0_2.e, v0_2.b, "Move Channel");
        } else {
            com.teamspeak.ts3client.d.a.a.y().a.a.a(com.teamspeak.ts3client.d.a.a.y().a.e, com.teamspeak.ts3client.d.a.a.a(this.d.a).b, com.teamspeak.ts3client.jni.c.i, v0_2.b);
            com.teamspeak.ts3client.d.a.a.y().a.a.ts3client_flushChannelUpdates(com.teamspeak.ts3client.d.a.a.y().a.e, com.teamspeak.ts3client.d.a.a.a(this.d.a).b, "Move channel");
        }
        this.c.dismiss();
        return;
    }
}
