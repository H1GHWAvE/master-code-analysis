package com.teamspeak.ts3client.d.c;
public final class d extends com.teamspeak.ts3client.d.c.i {
    boolean a;
    boolean b;

    public d()
    {
        this.a = 0;
        this.b = 0;
        return;
    }

    public final void a()
    {
        return;
    }

    public final void a(android.view.Menu p9, com.teamspeak.ts3client.Ts3Application p10)
    {
        String v2_0 = 0;
        com.teamspeak.ts3client.d.c.d.a(p9, 2130837679, com.teamspeak.ts3client.data.e.a.a("menu.settings"), 101, 1);
        if (p10.a != null) {
            int v0_5;
            if (!p10.a.s.a(com.teamspeak.ts3client.jni.g.aE)) {
                v0_5 = 0;
            } else {
                v0_5 = 1;
            }
            if (p10.a.s.a(com.teamspeak.ts3client.jni.g.aF)) {
                v0_5 = 1;
            }
            if (p10.a.s.a(com.teamspeak.ts3client.jni.g.aG)) {
                v0_5 = 1;
            }
            if (p10.a.s.a(com.teamspeak.ts3client.jni.g.aQ)) {
                v0_5 |= 2;
            }
            if (p10.a.s.a(com.teamspeak.ts3client.jni.g.aP)) {
                v0_5 |= 2;
            }
            if (p10.a.s.a(com.teamspeak.ts3client.jni.g.aN)) {
                v0_5 |= 2;
            }
            if (p10.a.s.a(com.teamspeak.ts3client.jni.g.aM)) {
                v0_5 |= 2;
            }
            if (p10.a.s.a(com.teamspeak.ts3client.jni.g.aL)) {
                v0_5 |= 2;
            }
            if (p10.a.s.a(com.teamspeak.ts3client.jni.g.aO)) {
                v0_5 |= 2;
            }
            if (((v0_5 & 1) == 0) || ((v0_5 & 2) == 0)) {
                this.b = 0;
            } else {
                this.b = 1;
            }
            com.teamspeak.ts3client.d.c.d.a(p9, 2130837627, com.teamspeak.ts3client.data.e.a.a("menu.createchannel"), 102, 1);
            com.teamspeak.ts3client.d.c.d.a(p9, 102, this.b);
            if (p10.a.s.a(com.teamspeak.ts3client.jni.g.aD)) {
                v0_5 |= 4;
            }
            if (((v0_5 & 1) == 0) || (((v0_5 & 2) == 0) || ((v0_5 & 4) == 0))) {
                this.b = 0;
            } else {
                this.b = 1;
            }
            com.teamspeak.ts3client.d.c.d.a(p9, 2130837628, com.teamspeak.ts3client.data.e.a.a("menu.createsubchannel"), 103, 1);
            com.teamspeak.ts3client.d.c.d.a(p9, 103, this.b);
            if (p10.a.a.a(p10.a.e, com.teamspeak.ts3client.jni.d.M) != 0) {
                com.teamspeak.ts3client.d.c.d.a(p9, 2130837658, com.teamspeak.ts3client.data.e.a.a("menu.away.revoke"), 105, 0);
            } else {
                com.teamspeak.ts3client.d.c.d.a(p9, 2130837658, com.teamspeak.ts3client.data.e.a.a("menu.away.set"), 104, 1);
            }
            if ((p10.a.s.a(com.teamspeak.ts3client.jni.g.au)) || (p10.a.s.a(com.teamspeak.ts3client.jni.g.av))) {
                v2_0 = 1;
            }
            com.teamspeak.ts3client.d.c.d.a(p9, 2130837701, com.teamspeak.ts3client.data.e.a.a("menu.temporarypasswords"), 106, 1);
            com.teamspeak.ts3client.d.c.d.a(p9, 106, v2_0);
            com.teamspeak.ts3client.d.c.d.a(p9, 2130837600, com.teamspeak.ts3client.data.e.a.a("menu.contact"), 107, 1);
            int v0_24 = p10.a.c().a(Long.valueOf(p10.a.g));
            try {
                String v2_8 = p10.a.d.b(p10.a.h);
            } catch (int v0) {
                int v0_33 = p10.a.s.a(com.teamspeak.ts3client.jni.g.da);
                com.teamspeak.ts3client.d.c.d.a(p9, 2130837574, com.teamspeak.ts3client.data.e.a.a("menu.banlist"), 110, 1);
                com.teamspeak.ts3client.d.c.d.a(p9, 110, v0_33);
                com.teamspeak.ts3client.d.c.d.a(p9, 2130837701, com.teamspeak.ts3client.data.e.a.a("menu.privkey"), 111, 1);
            }
            if ((v2_8.o >= v0_24.h) || (v2_8.r)) {
            } else {
                if (!v2_8.p.equals("0")) {
                    com.teamspeak.ts3client.d.c.d.a(p9, 2130837674, com.teamspeak.ts3client.data.e.a.a("menu.talkpower.cancel"), 109, 1);
                } else {
                    com.teamspeak.ts3client.d.c.d.a(p9, 2130837673, com.teamspeak.ts3client.data.e.a.a("menu.talkpower.request"), 108, 1);
                }
            }
        }
        return;
    }

    public final boolean a(android.view.MenuItem p11, com.teamspeak.ts3client.Ts3Application p12)
    {
        String v0_5;
        int v4_0 = p12.f.c().a();
        switch (p11.getItemId()) {
            case 101:
                if (p12.f.c().a("Settings") != null) {
                    v4_0.a(p12.f.c().a("Settings"));
                } else {
                    v4_0.b(new com.teamspeak.ts3client.f.as(), "Settings");
                }
                v4_0.f();
                v4_0.a(4097);
                v4_0.i();
                v0_5 = 1;
                break;
            case 102:
                if (!p12.a.s.a(com.teamspeak.ts3client.jni.g.aV)) {
                    String v0_75 = new com.teamspeak.ts3client.data.a();
                    v0_75.g = -1;
                    new com.teamspeak.ts3client.d.a.m(v0_75, 1).a(v4_0, "ChannelDialog");
                } else {
                    String v0_78 = new java.util.ArrayList();
                    v0_78.addAll(p12.a.f.a);
                    com.teamspeak.ts3client.d.c.d v1_38 = new java.util.ArrayList();
                    android.widget.ArrayAdapter v2_27 = v0_78.iterator();
                    while (v2_27.hasNext()) {
                        String v0_97 = ((com.teamspeak.ts3client.data.a) v2_27.next());
                        if ((v0_97.b != 0) && (v0_97.f == 0)) {
                            v1_38.add(v0_97);
                        }
                    }
                    String v0_81 = new com.teamspeak.ts3client.data.a();
                    v0_81.a("---");
                    v0_81.b = -1;
                    v1_38.add(0, v0_81);
                    android.app.Dialog v5_8 = new android.app.Dialog(p12.f, 2131165365);
                    com.teamspeak.ts3client.data.d.q.a(v5_8);
                    android.widget.LinearLayout v6_6 = new android.widget.LinearLayout(p12.f);
                    v6_6.setOrientation(1);
                    String v0_88 = ((android.view.LayoutInflater) p12.f.getSystemService("layout_inflater")).inflate(2130903110, 0);
                    int v3_22 = ((android.widget.Spinner) v0_88.findViewById(2131493264));
                    v0_88.setLayoutParams(new android.view.ViewGroup$LayoutParams(-1, -2));
                    android.widget.ArrayAdapter v2_38 = new android.widget.ArrayAdapter(p12.f, 17367049, v1_38);
                    v3_22.setAdapter(v2_38);
                    v6_6.addView(v0_88);
                    android.widget.Button v7_7 = new android.widget.Button(p12.f);
                    v7_7.setText(com.teamspeak.ts3client.data.e.a.a("button.ok"));
                    v7_7.setOnClickListener(new com.teamspeak.ts3client.d.c.e(this, v2_38, v3_22, v4_0, v5_8));
                    v6_6.addView(v7_7);
                    v5_8.setContentView(v6_6);
                    v5_8.setTitle(com.teamspeak.ts3client.data.e.a.a("menu.createchannel.text"));
                    v5_8.show();
                }
                v0_5 = 1;
                break;
            case 103:
                com.teamspeak.ts3client.d.c.d v1_29 = new java.util.ArrayList();
                v1_29.addAll(p12.a.f.a);
                v1_29.remove(0);
                v1_29.remove((v1_29.size() - 1));
                android.app.Dialog v5_6 = new android.app.Dialog(p12.f, 2131165365);
                com.teamspeak.ts3client.data.d.q.a(v5_6);
                android.widget.LinearLayout v6_4 = new android.widget.LinearLayout(p12.f);
                v6_4.setOrientation(1);
                String v0_63 = ((android.view.LayoutInflater) p12.f.getSystemService("layout_inflater")).inflate(2130903110, 0);
                int v3_19 = ((android.widget.Spinner) v0_63.findViewById(2131493264));
                v0_63.setLayoutParams(new android.view.ViewGroup$LayoutParams(-1, -2));
                android.widget.ArrayAdapter v2_24 = new android.widget.ArrayAdapter(p12.f, 17367049, v1_29);
                v3_19.setAdapter(v2_24);
                v6_4.addView(v0_63);
                android.widget.Button v7_3 = new android.widget.Button(p12.f);
                v7_3.setText(com.teamspeak.ts3client.data.e.a.a("button.ok"));
                v7_3.setOnClickListener(new com.teamspeak.ts3client.d.c.f(this, v2_24, v3_19, v4_0, v5_6));
                v6_4.addView(v7_3);
                v5_6.setContentView(v6_4);
                v5_6.setTitle(com.teamspeak.ts3client.data.e.a.a("menu.createsubchannel.text"));
                v5_6.show();
                v0_5 = 1;
                break;
            case 104:
                String v0_50 = new android.app.Dialog(p12.f, 2131165365);
                com.teamspeak.ts3client.data.d.q.a(v0_50);
                com.teamspeak.ts3client.d.c.d v1_25 = new android.widget.LinearLayout(p12.f);
                v1_25.setOrientation(1);
                v0_50.setContentView(v1_25);
                android.widget.ArrayAdapter v2_16 = new android.widget.EditText(p12.f);
                v2_16.setMaxLines(1);
                int v3_14 = new android.text.InputFilter[1];
                v3_14[0] = new android.text.InputFilter$LengthFilter(80);
                v2_16.setFilters(v3_14);
                v1_25.addView(v2_16);
                int v3_16 = new android.widget.Button(p12.f);
                v3_16.setText(com.teamspeak.ts3client.data.e.a.a("button.ok"));
                v3_16.setOnClickListener(new com.teamspeak.ts3client.d.c.g(this, p12, v2_16, v0_50));
                v1_25.addView(v3_16);
                v0_50.setTitle(com.teamspeak.ts3client.data.e.a.a("menu.away.text"));
                v0_50.show();
                v0_5 = 1;
                break;
            case 105:
                p12.a.a.a(p12.a.e, com.teamspeak.ts3client.jni.d.M, 0);
                p12.a.a.a(p12.a.e, com.teamspeak.ts3client.jni.d.N, "");
                p12.a.a.ts3client_flushClientSelfUpdates(p12.a.e, "ReturnAway");
                p12.a.v.s.clear(2);
                p12.a.v.t = "";
                p12.g().a(com.teamspeak.ts3client.jni.h.aW, 0);
                v0_5 = 1;
                break;
            case 106:
                if (p12.f.c().a("TempPassList") != null) {
                    v4_0.a(p12.f.c().a("TempPassList"));
                } else {
                    v4_0.b(new com.teamspeak.ts3client.d.d.i(), "TempPassList");
                }
                v4_0.f();
                v4_0.a(4097);
                v4_0.i();
                v0_5 = 1;
                break;
            case 107:
                if (p12.f.c().a("Contact") != null) {
                    v4_0.a(p12.f.c().a("Contact"));
                } else {
                    v4_0.b(new com.teamspeak.ts3client.c.b(), "Contact");
                }
                v4_0.f();
                v4_0.a(4097);
                v4_0.i();
                v0_5 = 1;
                break;
            case 108:
                String v0_18 = new android.app.Dialog(p12.f, 2131165365);
                com.teamspeak.ts3client.data.d.q.a(v0_18);
                com.teamspeak.ts3client.d.c.d v1_7 = new android.widget.LinearLayout(p12.f);
                v1_7.setOrientation(1);
                android.widget.ArrayAdapter v2_5 = new android.widget.TextView(p12.f);
                v2_5.setText(com.teamspeak.ts3client.data.e.a.a("menu.talkpower.reason.text"));
                v1_7.addView(v2_5);
                android.widget.ArrayAdapter v2_7 = new android.widget.EditText(p12.f);
                v2_7.setLayoutParams(new android.view.ViewGroup$LayoutParams(-1, -2));
                v2_7.setSingleLine(1);
                v1_7.addView(v2_7);
                v0_18.setTitle(com.teamspeak.ts3client.data.e.a.a("menu.talkpower.reason"));
                int v3_10 = new android.widget.Button(p12.f);
                v3_10.setText(com.teamspeak.ts3client.data.e.a.a("button.ok"));
                v3_10.setLayoutParams(new android.view.ViewGroup$LayoutParams(-1, -2));
                v3_10.setOnClickListener(new com.teamspeak.ts3client.d.c.h(this, p12, v2_7, v0_18));
                v1_7.addView(v3_10);
                v0_18.setContentView(v1_7);
                v0_18.show();
                v0_5 = 1;
                break;
            case 109:
                p12.a.a.ts3client_requestIsTalker(p12.a.e, 0, "", "cancel request talkpower");
                v0_5 = 1;
                break;
            case 110:
                if (p12.f.c().a("Banlist") != null) {
                    v4_0.a(p12.f.c().a("Banlist"));
                } else {
                    v4_0.b(new com.teamspeak.ts3client.b.b(), "Banlist");
                }
                v4_0.f();
                v4_0.a(4097);
                v4_0.i();
                v0_5 = 1;
                break;
            case 111:
                p12.a.k.a(0);
                v0_5 = 1;
                break;
            default:
                v0_5 = 0;
        }
        return v0_5;
    }
}
