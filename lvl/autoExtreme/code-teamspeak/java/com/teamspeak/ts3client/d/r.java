package com.teamspeak.ts3client.d;
public final class r extends android.support.v4.app.ax {
    private com.teamspeak.ts3client.Ts3Application at;
    private android.widget.Button au;
    private com.teamspeak.ts3client.data.a av;
    private android.widget.EditText aw;
    private int ax;
    private android.widget.TextView ay;

    public r(android.content.Context p2, com.teamspeak.ts3client.data.a p3, int p4)
    {
        this.au = new android.widget.Button(p2);
        this.aw = new android.widget.EditText(p2);
        this.av = p3;
        this.ax = p4;
        return;
    }

    static synthetic com.teamspeak.ts3client.data.a a(com.teamspeak.ts3client.d.r p1)
    {
        return p1.av;
    }

    static synthetic android.widget.EditText b(com.teamspeak.ts3client.d.r p1)
    {
        return p1.aw;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application c(com.teamspeak.ts3client.d.r p1)
    {
        return p1.at;
    }

    static synthetic int d(com.teamspeak.ts3client.d.r p1)
    {
        return p1.ax;
    }

    static synthetic void e(com.teamspeak.ts3client.d.r p0)
    {
        p0.b();
        return;
    }

    private void y()
    {
        this.b();
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p11, android.view.ViewGroup p12)
    {
        this.j.requestWindowFeature(1);
        this.at = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        android.widget.RelativeLayout v0_5 = new android.widget.RelativeLayout(p11.getContext());
        v0_5.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.ay = new android.widget.TextView(this.i());
        this.ay.setId(10);
        this.ay.setBackgroundDrawable(this.i().getResources().getDrawable(2130837577));
        this.ay.setTextAppearance(this.i(), 2131165363);
        this.ay.setSingleLine(1);
        android.widget.RelativeLayout$LayoutParams v1_9 = this.ay;
        com.teamspeak.ts3client.d.s v3_2 = new Object[1];
        v3_2[0] = this.av.a;
        v1_9.setText(com.teamspeak.ts3client.data.e.a.a("passworddialog.text", v3_2));
        this.ay.setEllipsize(android.text.TextUtils$TruncateAt.END);
        this.ay.setPadding(15, 15, 15, 15);
        android.widget.RelativeLayout$LayoutParams v1_13 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_13.addRule(10);
        v0_5.addView(this.ay, v1_13);
        v0_5.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.aw.setId(2);
        this.aw.setInputType(524289);
        android.widget.RelativeLayout$LayoutParams v1_19 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_19.addRule(3, this.ay.getId());
        v0_5.addView(this.aw, v1_19);
        android.widget.RelativeLayout$LayoutParams v1_21 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_21.addRule(3, this.aw.getId());
        this.au.setText(com.teamspeak.ts3client.data.e.a.a("dialog.channel.join.text"));
        this.au.setId(3);
        this.au.setOnClickListener(new com.teamspeak.ts3client.d.s(this, v0_5));
        v0_5.addView(this.au, v1_21);
        return v0_5;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
