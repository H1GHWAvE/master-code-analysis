package com.teamspeak.ts3client;
final class j implements java.lang.Runnable {
    final synthetic com.teamspeak.ts3client.h a;

    j(com.teamspeak.ts3client.h p1)
    {
        this.a = p1;
        return;
    }

    public final void run()
    {
        if (com.teamspeak.ts3client.h.z().w == null) {
            com.teamspeak.ts3client.h.c(this.a).setText(com.teamspeak.ts3client.h.z().a);
        } else {
            switch (com.teamspeak.ts3client.h.z().w.d) {
                case 0:
                    com.teamspeak.ts3client.h.c(this.a).setText(new StringBuilder("[").append(com.teamspeak.ts3client.h.z().w.a).append("] ").append(com.teamspeak.ts3client.h.z().a).toString());
                    break;
                case 1:
                    com.teamspeak.ts3client.h.c(this.a).setText(com.teamspeak.ts3client.h.z().w.a);
                    break;
                case 2:
                    com.teamspeak.ts3client.h.c(this.a).setText(com.teamspeak.ts3client.h.z().a);
                    break;
            }
        }
        if (com.teamspeak.ts3client.h.z().w != null) {
            switch (com.teamspeak.ts3client.h.z().w.e) {
                case 0:
                    com.teamspeak.ts3client.h.c(this.a).setTextColor(com.teamspeak.ts3client.h.d(this.a));
                    break;
                case 1:
                    com.teamspeak.ts3client.h.c(this.a).setTextColor(-65536);
                    break;
                case 2:
                    com.teamspeak.ts3client.h.c(this.a).setTextColor(-16711936);
                    break;
            }
        }
        android.widget.ImageView v0_28 = com.teamspeak.ts3client.h.e(this.a).a.a.b(com.teamspeak.ts3client.h.e(this.a).a.e, com.teamspeak.ts3client.h.z().c, com.teamspeak.ts3client.jni.d.T);
        if (!v0_28.equals("")) {
            com.teamspeak.ts3client.h.g(this.a).setText(v0_28);
        } else {
            com.teamspeak.ts3client.h.f(this.a).setVisibility(8);
        }
        android.widget.ImageView v0_36 = new StringBuilder().append(com.teamspeak.ts3client.h.e(this.a).a.a.b(com.teamspeak.ts3client.h.e(this.a).a.e, com.teamspeak.ts3client.h.z().c, com.teamspeak.ts3client.jni.d.c)).append(" on ").append(com.teamspeak.ts3client.h.e(this.a).a.a.b(com.teamspeak.ts3client.h.e(this.a).a.e, com.teamspeak.ts3client.h.z().c, com.teamspeak.ts3client.jni.d.d)).toString();
        android.graphics.Bitmap v1_39 = com.teamspeak.ts3client.h.A().matcher(v0_36);
        if ((v1_39.find()) && (Long.parseLong(v1_39.group(2)) > 20000)) {
            v0_36 = v0_36.replace(v1_39.group(1), new StringBuilder("(").append(java.text.DateFormat.getDateTimeInstance(2, 2).format(new java.util.Date((Long.parseLong(v1_39.group(2)) * 1000)))).append(")").toString());
        }
        com.teamspeak.ts3client.h.h(this.a).setText(v0_36);
        com.teamspeak.ts3client.h.i(this.a).setText(java.text.DateFormat.getDateTimeInstance(2, 2).format(new java.util.Date((com.teamspeak.ts3client.h.e(this.a).a.a.c(com.teamspeak.ts3client.h.e(this.a).a.e, com.teamspeak.ts3client.h.z().c, com.teamspeak.ts3client.jni.d.K) * 1000))));
        android.graphics.Bitmap v1_58 = com.teamspeak.ts3client.h.e(this.a).a.a.b(com.teamspeak.ts3client.h.e(this.a).a.e, com.teamspeak.ts3client.h.z().c, com.teamspeak.ts3client.jni.d.I).split(",");
        int v2_27 = v1_58.length;
        android.widget.ImageView v0_49 = 0;
        while (v0_49 < v2_27) {
            int v3_12 = v1_58[v0_49];
            String v4_24 = Integer.parseInt(v3_12);
            if (!com.teamspeak.ts3client.h.e(this.a).a.o.a(((long) v4_24))) {
                String v4_25 = this.a;
                java.util.Date v5_22 = com.teamspeak.ts3client.h.j(this.a);
                Object[] v7_7 = new Object[1];
                v7_7[0] = v3_12;
                com.teamspeak.ts3client.h.a(v4_25, v5_22, com.teamspeak.ts3client.data.e.a.a("clientinfo.value.grouperror", v7_7), 0);
            } else {
                com.teamspeak.ts3client.h.a(this.a, com.teamspeak.ts3client.h.j(this.a), com.teamspeak.ts3client.h.e(this.a).a.o.b(((long) v4_24)).b, com.teamspeak.ts3client.h.e(this.a).a.r.a(com.teamspeak.ts3client.h.e(this.a).a.o.b(((long) v4_24)).d).a());
            }
            v0_49++;
        }
        android.graphics.Bitmap v1_65 = com.teamspeak.ts3client.h.e(this.a).a.a.b(com.teamspeak.ts3client.h.e(this.a).a.e, com.teamspeak.ts3client.h.z().c, com.teamspeak.ts3client.jni.d.H).split(",");
        int v2_29 = v1_65.length;
        android.widget.ImageView v0_55 = 0;
        while (v0_55 < v2_29) {
            int v3_9 = v1_65[v0_55];
            String v4_19 = Integer.parseInt(v3_9);
            if (!com.teamspeak.ts3client.h.e(this.a).a.p.a(((long) v4_19))) {
                String v4_20 = this.a;
                java.util.Date v5_13 = com.teamspeak.ts3client.h.k(this.a);
                Object[] v7_1 = new Object[1];
                v7_1[0] = v3_9;
                com.teamspeak.ts3client.h.a(v4_20, v5_13, com.teamspeak.ts3client.data.e.a.a("clientinfo.value.grouperror", v7_1), 0);
            } else {
                com.teamspeak.ts3client.h.a(this.a, com.teamspeak.ts3client.h.k(this.a), com.teamspeak.ts3client.h.e(this.a).a.p.b(((long) v4_19)).b, com.teamspeak.ts3client.h.e(this.a).a.r.a(com.teamspeak.ts3client.h.e(this.a).a.p.b(((long) v4_19)).d).a());
            }
            v0_55++;
        }
        if (com.teamspeak.ts3client.h.z().w == null) {
            if (com.teamspeak.ts3client.h.z().y != null) {
                android.widget.ImageView v0_63 = ((android.widget.ImageView) com.teamspeak.ts3client.h.l(this.a).findViewById(100));
                if (v0_63 == null) {
                    v0_63 = new android.widget.ImageView(this.a.i());
                    v0_63.setId(100);
                    android.graphics.Bitmap v1_71 = new android.widget.LinearLayout$LayoutParams(-2, -1);
                    v1_71.weight = 1065353216;
                    v1_71.gravity = 3;
                    v0_63.setLayoutParams(v1_71);
                    com.teamspeak.ts3client.h.l(this.a).addView(v0_63);
                }
                if (com.teamspeak.ts3client.h.z().y != null) {
                    v0_63.setImageBitmap(com.teamspeak.ts3client.h.z().y);
                }
            }
        } else {
            if (com.teamspeak.ts3client.h.z().w.k) {
                android.widget.ImageView v0_69 = new android.widget.TextView(com.teamspeak.ts3client.h.m(this.a));
                v0_69.setText(com.teamspeak.ts3client.data.e.a.a("contact.avatar.ignored"));
                com.teamspeak.ts3client.h.l(this.a).addView(v0_69);
            } else {
                if (com.teamspeak.ts3client.h.z().y != null) {
                    android.widget.ImageView v0_75 = ((android.widget.ImageView) com.teamspeak.ts3client.h.l(this.a).findViewById(100));
                    if (v0_75 == null) {
                        v0_75 = new android.widget.ImageView(this.a.i());
                        v0_75.setId(100);
                        android.graphics.Bitmap v1_89 = new android.widget.LinearLayout$LayoutParams(-2, -1);
                        v1_89.weight = 1065353216;
                        v1_89.gravity = 3;
                        v0_75.setLayoutParams(v1_89);
                        com.teamspeak.ts3client.h.l(this.a).addView(v0_75);
                    }
                    if (com.teamspeak.ts3client.h.z().y != null) {
                        v0_75.setImageBitmap(com.teamspeak.ts3client.h.z().y);
                    }
                }
            }
        }
        if (!com.teamspeak.ts3client.h.z().p.equals("0")) {
            android.widget.ImageView v0_81 = new android.widget.TextView(com.teamspeak.ts3client.h.m(this.a));
            int v2_37 = new Object[2];
            v2_37[0] = java.text.DateFormat.getDateTimeInstance().format(new java.util.Date((((long) Integer.parseInt(com.teamspeak.ts3client.h.z().p)) * 1000)));
            v2_37[1] = com.teamspeak.ts3client.h.z().q;
            v0_81.setText(com.teamspeak.ts3client.data.e.a.a("clientinfo.value.talkpower", v2_37));
            com.teamspeak.ts3client.h.l(this.a).addView(v0_81);
        }
        return;
    }
}
