package com.teamspeak.ts3client;
public class OpenBrowserIntent extends android.app.Activity {
    boolean a;

    public OpenBrowserIntent()
    {
        this.a = 0;
        return;
    }

    private void a(android.net.Uri p10)
    {
        String v2_1 = ((android.app.ActivityManager) this.getSystemService("activity")).getRunningServices(2147483647).iterator();
        while (v2_1.hasNext()) {
            if ("com.teamspeak.ts3client.ConnectionBackground".equals(((android.app.ActivityManager$RunningServiceInfo) v2_1.next()).service.getClassName())) {
                this.a = 1;
            }
        }
        String v2_3 = new com.teamspeak.ts3client.data.ab();
        if (p10 != null) {
            if (p10.getQuery() != null) {
                try {
                    String v0_7 = java.net.URLDecoder.decode(p10.getQuery(), "UTF-8");
                    Class v3_2 = new java.util.HashMap();
                } catch (String v0_53) {
                    v0_53.printStackTrace();
                }
                if (!v0_7.contains("&")) {
                    String v0_8 = v0_7.split("=");
                    if (v0_8.length <= 1) {
                        v3_2.put(v0_8[0], "");
                    } else {
                        v3_2.put(v0_8[0], v0_8[1]);
                    }
                } else {
                    int v4_4 = v0_7.split("&");
                    int v5 = v4_4.length;
                    String v0_11 = 0;
                    while (v0_11 < v5) {
                        StringBuilder v1_11 = v4_4[v0_11].split("=");
                        if (v1_11.length <= 1) {
                            v3_2.put(v1_11[0], "");
                        } else {
                            v3_2.put(v1_11[0], v1_11[1]);
                        }
                        v0_11++;
                    }
                }
                v2_3.a(p10.getHost());
                if (v3_2.containsKey("port")) {
                    v2_3.a(new StringBuilder().append(v2_3.c).append(":").append(((String) v3_2.get("port"))).toString());
                }
                if (v3_2.containsKey("nickname")) {
                    v2_3.f = ((String) v3_2.get("nickname"));
                }
                if (v3_2.containsKey("password")) {
                    v2_3.e = ((String) v3_2.get("password"));
                }
                if (v3_2.containsKey("channel")) {
                    v2_3.g = ((String) v3_2.get("channel"));
                }
                if (v3_2.containsKey("channelpassword")) {
                    v2_3.h = ((String) v3_2.get("channelpassword"));
                }
                if (v3_2.containsKey("addbookmark")) {
                    v2_3.a = ((String) v3_2.get("addbookmark"));
                }
                if (v3_2.containsKey("token")) {
                    v2_3.i = ((String) v3_2.get("token"));
                }
            } else {
                v2_3.a(p10.getHost());
            }
            String v0_56 = new android.content.Intent(this.getApplicationContext(), com.teamspeak.ts3client.StartGUIFragment);
            v0_56.putExtra("UrlStart", 1);
            v0_56.putExtra("uri", p10);
            v0_56.putExtra("address", v2_3.c);
            this.startActivity(v0_56);
            this.finish();
        }
        return;
    }

    protected void onCreate(android.os.Bundle p11)
    {
        super.onCreate(p11);
        String v2_0 = this.getIntent().getData();
        if (v2_0 != null) {
            com.teamspeak.ts3client.data.ab v3_1 = ((android.app.ActivityManager) this.getSystemService("activity")).getRunningServices(2147483647).iterator();
            while (v3_1.hasNext()) {
                if ("com.teamspeak.ts3client.ConnectionBackground".equals(((android.app.ActivityManager$RunningServiceInfo) v3_1.next()).service.getClassName())) {
                    this.a = 1;
                }
            }
            com.teamspeak.ts3client.data.ab v3_3 = new com.teamspeak.ts3client.data.ab();
            if (v2_0 != null) {
                if (v2_0.getQuery() != null) {
                    try {
                        String v0_8 = java.net.URLDecoder.decode(v2_0.getQuery(), "UTF-8");
                        Class v4_2 = new java.util.HashMap();
                    } catch (String v0_54) {
                        v0_54.printStackTrace();
                    }
                    if (!v0_8.contains("&")) {
                        String v0_9 = v0_8.split("=");
                        if (v0_9.length <= 1) {
                            v4_2.put(v0_9[0], "");
                        } else {
                            v4_2.put(v0_9[0], v0_9[1]);
                        }
                    } else {
                        int v5_4 = v0_8.split("&");
                        int v6 = v5_4.length;
                        String v0_12 = 0;
                        while (v0_12 < v6) {
                            StringBuilder v1_11 = v5_4[v0_12].split("=");
                            if (v1_11.length <= 1) {
                                v4_2.put(v1_11[0], "");
                            } else {
                                v4_2.put(v1_11[0], v1_11[1]);
                            }
                            v0_12++;
                        }
                    }
                    v3_3.a(v2_0.getHost());
                    if (v4_2.containsKey("port")) {
                        v3_3.a(new StringBuilder().append(v3_3.c).append(":").append(((String) v4_2.get("port"))).toString());
                    }
                    if (v4_2.containsKey("nickname")) {
                        v3_3.f = ((String) v4_2.get("nickname"));
                    }
                    if (v4_2.containsKey("password")) {
                        v3_3.e = ((String) v4_2.get("password"));
                    }
                    if (v4_2.containsKey("channel")) {
                        v3_3.g = ((String) v4_2.get("channel"));
                    }
                    if (v4_2.containsKey("channelpassword")) {
                        v3_3.h = ((String) v4_2.get("channelpassword"));
                    }
                    if (v4_2.containsKey("addbookmark")) {
                        v3_3.a = ((String) v4_2.get("addbookmark"));
                    }
                    if (v4_2.containsKey("token")) {
                        v3_3.i = ((String) v4_2.get("token"));
                    }
                } else {
                    v3_3.a(v2_0.getHost());
                }
                String v0_57 = new android.content.Intent(this.getApplicationContext(), com.teamspeak.ts3client.StartGUIFragment);
                v0_57.putExtra("UrlStart", 1);
                v0_57.putExtra("uri", v2_0);
                v0_57.putExtra("address", v3_3.c);
                this.startActivity(v0_57);
                this.finish();
            }
        }
        return;
    }
}
