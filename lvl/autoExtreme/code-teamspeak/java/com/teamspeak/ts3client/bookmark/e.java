package com.teamspeak.ts3client.bookmark;
public final class e extends android.support.v4.app.Fragment {
    public boolean a;
    public com.teamspeak.ts3client.data.b.a b;
    private com.teamspeak.ts3client.Ts3Application c;
    private android.widget.ListView d;
    private com.teamspeak.ts3client.bookmark.c e;

    public e()
    {
        this.a = 0;
        return;
    }

    static synthetic com.teamspeak.ts3client.bookmark.c a(com.teamspeak.ts3client.bookmark.e p1)
    {
        return p1.e;
    }

    private void a(int p4)
    {
        new com.teamspeak.ts3client.bookmark.a(this.b, ((com.teamspeak.ts3client.data.ab) this.e.getItem(p4))).a(this.M, "AddBookmark");
        return;
    }

    private void a(int p6, com.teamspeak.ts3client.data.ab p7)
    {
        android.app.AlertDialog v0_2 = new android.app.AlertDialog$Builder(this.i()).create();
        com.teamspeak.ts3client.data.d.q.a(v0_2);
        v0_2.setTitle(com.teamspeak.ts3client.data.e.a.a("bookmark.listbookmark.delete"));
        String v2_1 = new Object[1];
        v2_1[0] = p7.a;
        v0_2.setMessage(com.teamspeak.ts3client.data.e.a.a("bookmark.listbookmark.delete.text", v2_1));
        v0_2.setButton(-1, com.teamspeak.ts3client.data.e.a.a("button.delete"), new com.teamspeak.ts3client.bookmark.f(this, p6));
        v0_2.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.bookmark.g(this, v0_2));
        v0_2.setCancelable(0);
        v0_2.show();
        return;
    }

    static synthetic void a(com.teamspeak.ts3client.bookmark.e p3, int p4)
    {
        new com.teamspeak.ts3client.bookmark.a(p3.b, ((com.teamspeak.ts3client.data.ab) p3.e.getItem(p4))).a(p3.M, "AddBookmark");
        return;
    }

    static synthetic void a(com.teamspeak.ts3client.bookmark.e p5, int p6, com.teamspeak.ts3client.data.ab p7)
    {
        android.app.AlertDialog v0_2 = new android.app.AlertDialog$Builder(p5.i()).create();
        com.teamspeak.ts3client.data.d.q.a(v0_2);
        v0_2.setTitle(com.teamspeak.ts3client.data.e.a.a("bookmark.listbookmark.delete"));
        String v2_1 = new Object[1];
        v2_1[0] = p7.a;
        v0_2.setMessage(com.teamspeak.ts3client.data.e.a.a("bookmark.listbookmark.delete.text", v2_1));
        v0_2.setButton(-1, com.teamspeak.ts3client.data.e.a.a("button.delete"), new com.teamspeak.ts3client.bookmark.f(p5, p6));
        v0_2.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.bookmark.g(p5, v0_2));
        v0_2.setCancelable(0);
        v0_2.show();
        return;
    }

    private com.teamspeak.ts3client.data.b.a b()
    {
        return this.b;
    }

    static synthetic com.teamspeak.ts3client.data.b.a b(com.teamspeak.ts3client.bookmark.e p1)
    {
        return p1.b;
    }

    private void b(int p5, com.teamspeak.ts3client.data.ab p6)
    {
        android.app.AlertDialog v0_1 = new android.app.AlertDialog$Builder(this.i());
        String v1_2 = new CharSequence[2];
        v1_2[0] = com.teamspeak.ts3client.data.e.a.a("button.edit");
        v1_2[1] = com.teamspeak.ts3client.data.e.a.a("button.delete");
        v0_1.setItems(v1_2, new com.teamspeak.ts3client.bookmark.h(this, p5, p6));
        android.app.AlertDialog v0_2 = v0_1.create();
        com.teamspeak.ts3client.data.d.q.a(v0_2);
        v0_2.setTitle(com.teamspeak.ts3client.data.e.a.a("bookmark.listbookmark.options"));
        v0_2.show();
        return;
    }

    static synthetic void b(com.teamspeak.ts3client.bookmark.e p4, int p5, com.teamspeak.ts3client.data.ab p6)
    {
        android.app.AlertDialog v0_1 = new android.app.AlertDialog$Builder(p4.i());
        String v1_2 = new CharSequence[2];
        v1_2[0] = com.teamspeak.ts3client.data.e.a.a("button.edit");
        v1_2[1] = com.teamspeak.ts3client.data.e.a.a("button.delete");
        v0_1.setItems(v1_2, new com.teamspeak.ts3client.bookmark.h(p4, p5, p6));
        android.app.AlertDialog v0_2 = v0_1.create();
        com.teamspeak.ts3client.data.d.q.a(v0_2);
        v0_2.setTitle(com.teamspeak.ts3client.data.e.a.a("bookmark.listbookmark.options"));
        v0_2.show();
        return;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application c(com.teamspeak.ts3client.bookmark.e p1)
    {
        return p1.c;
    }

    public final android.view.View a(android.view.LayoutInflater p5, android.view.ViewGroup p6)
    {
        this.c = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        this.c.c = this;
        this.c.o.b(com.teamspeak.ts3client.data.e.a.a("bookmark.text"));
        android.view.View v1_3 = p5.inflate(2130903070, p6, 0);
        ((com.teamspeak.ts3client.customs.FloatingButton) v1_3.findViewById(2131493021)).setDrawable(this.j().getDrawable(2130837664));
        this.d = ((android.widget.ListView) v1_3.findViewById(2131493022));
        this.d.setContentDescription("Bookmark List");
        this.d.setOnItemClickListener(new com.teamspeak.ts3client.bookmark.i(this));
        this.d.setOnItemLongClickListener(new com.teamspeak.ts3client.bookmark.l(this));
        return v1_3;
    }

    public final void a()
    {
        this.e = new com.teamspeak.ts3client.bookmark.c(this.i().getApplicationContext());
        java.util.ArrayList v2 = this.b.a();
        int v1_2 = 0;
        while (v1_2 < v2.size()) {
            java.util.ArrayList v3_0 = this.e;
            int v0_7 = ((com.teamspeak.ts3client.data.ab) v2.get(v1_2));
            if (!v3_0.a.contains(v0_7)) {
                v3_0.a.add(v0_7);
            }
            v1_2++;
        }
        this.d.setAdapter(this.e);
        return;
    }

    public final void a(android.os.Bundle p3)
    {
        super.a(p3);
        this.b = new com.teamspeak.ts3client.data.b.a(this.i());
        return;
    }

    public final void a(com.teamspeak.ts3client.data.ab p8)
    {
        long v2_1 = this.c.e.getBoolean("use_proximity", 1);
        com.teamspeak.ts3client.Ts3Application v3_0 = this.c.f;
        com.teamspeak.ts3client.bookmark.o v0_7 = ((android.os.PowerManager) v3_0.n.getApplicationContext().getSystemService("power")).newWakeLock(1, "Ts3WakeLOCK");
        v0_7.acquire();
        v3_0.n.b = v0_7;
        com.teamspeak.ts3client.Ts3Application v3_2 = this.c.f;
        com.teamspeak.ts3client.bookmark.o v0_13 = ((android.net.wifi.WifiManager) v3_2.n.getApplicationContext().getSystemService("wifi")).createWifiLock(3, "Ts3WifiLOCK");
        v0_13.acquire();
        v3_2.n.n = v0_13;
        com.teamspeak.ts3client.Ts3Application v3_4 = this.c.f;
        com.teamspeak.ts3client.bookmark.o v0_19 = ((android.os.PowerManager) v3_4.n.getApplicationContext().getSystemService("power")).newWakeLock(32, "Ts3WakeLOCKSensor");
        v0_19.setReferenceCounted(0);
        v3_4.n.m = v0_19;
        this.c.g().a = 0;
        if ((v2_1 != 0) && (this.c.m != null)) {
            this.c.f.k();
        }
        try {
            com.teamspeak.ts3client.bookmark.o v0_28 = com.teamspeak.ts3client.data.b.f.a().b(((long) p8.d)).c;
        } catch (com.teamspeak.ts3client.bookmark.o v0) {
            v0_28 = 0;
        }
        try {
            if ((v0_28 == null) || (v0_28.equals(""))) {
                v0_28 = com.teamspeak.ts3client.data.b.f.a().d().c;
            }
        } catch (com.teamspeak.ts3client.bookmark.o v0) {
            com.teamspeak.ts3client.data.b.f.a().b();
            try {
                v0_28 = com.teamspeak.ts3client.data.b.f.a().d().c;
            } catch (com.teamspeak.ts3client.bookmark.o v0) {
                v0_28 = "";
            }
        }
        this.c.a = new com.teamspeak.ts3client.data.e(v0_28, this.i(), p8);
        long v2_7 = this.c.a;
        v2_7.m.d.log(java.util.logging.Level.INFO, "Loading lib");
        com.teamspeak.ts3client.bookmark.o v0_37 = com.teamspeak.ts3client.jni.Ts3Jni.a();
        if (v0_37 <= null) {
            v2_7.a.ts3client_prepareAudioDevice(v2_7.y, v2_7.x);
            v2_7.s = new com.teamspeak.ts3client.data.f.a(v2_7.m);
            v2_7.e = v2_7.a.ts3client_spawnNewServerConnectionHandler();
        }
        if (v0_37 <= null) {
            this.c.a.n();
            com.teamspeak.ts3client.bookmark.o v0_41 = new com.teamspeak.ts3client.t();
            v0_41.e(this.B);
            if (this.c.a.l == null) {
                this.c.k = new android.content.Intent(this.c, com.teamspeak.ts3client.ConnectionBackground);
                this.c.startService(this.c.k);
            }
            this.c.a.k = v0_41;
            com.teamspeak.ts3client.bookmark.o v0_43 = p8.c.replaceAll("(.*://)+", "");
            android.os.Handler v1_11 = new android.os.Handler();
            v1_11.postDelayed(new com.teamspeak.ts3client.bookmark.n(this, p8, v0_43), 500);
            v1_11.postDelayed(new com.teamspeak.ts3client.bookmark.o(this), 2000);
        } else {
            if (v0_37 == 5) {
                com.teamspeak.ts3client.bookmark.o v0_48 = new android.app.AlertDialog$Builder(this.i()).create();
                v0_48.setTitle(com.teamspeak.ts3client.data.e.a.a("critical.payment"));
                long v2_21 = new android.text.SpannableString("This beta version is expired, please visit our forum or \nhttp://play.google.com/store/apps/details?id=com.teamspeak.ts3client");
                android.text.util.Linkify.addLinks(v2_21, 15);
                v0_48.setMessage(v2_21);
                v0_48.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.exit"), new com.teamspeak.ts3client.bookmark.m(this, v0_48));
                v0_48.setCancelable(0);
                v0_48.show();
                ((android.widget.TextView) v0_48.findViewById(16908299)).setMovementMethod(android.text.method.LinkMovementMethod.getInstance());
                this.a = 0;
                this.c.a = 0;
            }
        }
        return;
    }

    public final void c(android.os.Bundle p1)
    {
        super.c(p1);
        this.a();
        return;
    }
}
