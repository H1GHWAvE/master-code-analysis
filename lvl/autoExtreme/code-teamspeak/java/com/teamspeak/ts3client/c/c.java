package com.teamspeak.ts3client.c;
public final class c extends android.widget.BaseAdapter {
    java.util.ArrayList a;
    private android.content.Context b;
    private android.view.LayoutInflater c;
    private android.support.v4.app.bi d;

    public c(android.content.Context p2, android.support.v4.app.bi p3)
    {
        this.b = p2;
        this.a = new java.util.ArrayList();
        this.c = android.view.LayoutInflater.from(p2);
        this.d = p3;
        return;
    }

    static synthetic android.content.Context a(com.teamspeak.ts3client.c.c p1)
    {
        return p1.b;
    }

    private void a(com.teamspeak.ts3client.c.a p2)
    {
        if (!this.a.contains(p2)) {
            this.a.add(p2);
        }
        return;
    }

    static synthetic java.util.ArrayList b(com.teamspeak.ts3client.c.c p1)
    {
        return p1.a;
    }

    private void b(com.teamspeak.ts3client.c.a p2)
    {
        if (!this.a.contains(p2)) {
            this.a.remove(p2);
        }
        return;
    }

    static synthetic android.support.v4.app.bi c(com.teamspeak.ts3client.c.c p1)
    {
        return p1.d;
    }

    public final int getCount()
    {
        return this.a.size();
    }

    public final Object getItem(int p2)
    {
        return this.a.get(p2);
    }

    public final long getItemId(int p3)
    {
        return 0;
    }

    public final android.view.View getView(int p9, android.view.View p10, android.view.ViewGroup p11)
    {
        android.view.View v4 = this.c.inflate(2130903091, 0);
        android.widget.TextView v0_3 = ((android.widget.TextView) v4.findViewById(2131493240));
        android.graphics.Bitmap v1_3 = ((android.widget.ImageView) v4.findViewById(2131493237));
        com.teamspeak.ts3client.c.g v2_3 = ((android.widget.ImageView) v4.findViewById(2131493239));
        v1_3.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837600, 1101004800, 1101004800));
        switch (((com.teamspeak.ts3client.c.a) this.a.get(p9)).e) {
            case 0:
                v2_3.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837672, 1101004800, 1101004800));
                break;
            case 1:
                v1_3.setColorFilter(-65536, android.graphics.PorterDuff$Mode.MULTIPLY);
                v2_3.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837671, 1101004800, 1101004800));
                break;
            case 2:
                v1_3.setColorFilter(-16711936, android.graphics.PorterDuff$Mode.MULTIPLY);
                v2_3.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837672, 1101004800, 1101004800));
                break;
        }
        android.graphics.Bitmap v1_10 = ((android.widget.ImageView) v4.findViewById(2131493238));
        v1_10.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837602, 1101004800, 1101004800));
        v2_3.setOnClickListener(new com.teamspeak.ts3client.c.d(this, p9));
        v1_10.setOnClickListener(new com.teamspeak.ts3client.c.g(this, p9));
        v0_3.setText(((com.teamspeak.ts3client.c.a) this.a.get(p9)).a);
        return v4;
    }
}
