package com.teamspeak.ts3client.c;
final class d implements android.view.View$OnClickListener {
    final synthetic int a;
    final synthetic com.teamspeak.ts3client.c.c b;

    d(com.teamspeak.ts3client.c.c p1, int p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onClick(android.view.View p8)
    {
        android.app.AlertDialog v1_2 = new android.app.AlertDialog$Builder(com.teamspeak.ts3client.c.c.a(this.b)).create();
        com.teamspeak.ts3client.data.d.q.a(v1_2);
        v1_2.setTitle(com.teamspeak.ts3client.data.e.a.a("contact.delete.info"));
        com.teamspeak.ts3client.c.f v3_0 = new Object[1];
        v3_0[0] = ((com.teamspeak.ts3client.c.a) com.teamspeak.ts3client.c.c.b(this.b).get(this.a)).a;
        v1_2.setMessage(com.teamspeak.ts3client.data.e.a.a("contact.delete.text", v3_0));
        v1_2.setButton(-1, com.teamspeak.ts3client.data.e.a.a("button.delete"), new com.teamspeak.ts3client.c.e(this));
        v1_2.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.c.f(this, v1_2));
        v1_2.setCancelable(1);
        v1_2.show();
        return;
    }
}
