package com.teamspeak.ts3client.data.e;
public final enum class c extends java.lang.Enum {
    public static final enum com.teamspeak.ts3client.data.e.c a;
    public static final enum com.teamspeak.ts3client.data.e.c b;
    public static final enum com.teamspeak.ts3client.data.e.c c;
    public static final enum com.teamspeak.ts3client.data.e.c d;
    public static final enum com.teamspeak.ts3client.data.e.c e;
    public static final enum com.teamspeak.ts3client.data.e.c f;
    public static final enum com.teamspeak.ts3client.data.e.c g;
    public static final enum com.teamspeak.ts3client.data.e.c h;
    private static final synthetic com.teamspeak.ts3client.data.e.c[] k;
    public String i;
    String j;

    static c()
    {
        com.teamspeak.ts3client.data.e.c.a = new com.teamspeak.ts3client.data.e.c("Espanol", 0, "es", "lang_ES.xml");
        com.teamspeak.ts3client.data.e.c.b = new com.teamspeak.ts3client.data.e.c("French", 1, "fr", "lang_FR.xml");
        com.teamspeak.ts3client.data.e.c.c = new com.teamspeak.ts3client.data.e.c("Polski", 2, "pl", "lang_PL.xml");
        com.teamspeak.ts3client.data.e.c.d = new com.teamspeak.ts3client.data.e.c("Portuguese", 3, "pt", "lang_PT-br.xml");
        com.teamspeak.ts3client.data.e.c.e = new com.teamspeak.ts3client.data.e.c("Russian", 4, "ru", "lang_RU.xml");
        com.teamspeak.ts3client.data.e.c.f = new com.teamspeak.ts3client.data.e.c("Turkish", 5, "tr", "lang_TR.xml");
        com.teamspeak.ts3client.data.e.c.g = new com.teamspeak.ts3client.data.e.c("Deutsch", 6, "de", "lang_de.xml");
        com.teamspeak.ts3client.data.e.c.h = new com.teamspeak.ts3client.data.e.c("English", 7, "en", "lang_eng.xml");
        com.teamspeak.ts3client.data.e.c[] v0_17 = new com.teamspeak.ts3client.data.e.c[8];
        v0_17[0] = com.teamspeak.ts3client.data.e.c.a;
        v0_17[1] = com.teamspeak.ts3client.data.e.c.b;
        v0_17[2] = com.teamspeak.ts3client.data.e.c.c;
        v0_17[3] = com.teamspeak.ts3client.data.e.c.d;
        v0_17[4] = com.teamspeak.ts3client.data.e.c.e;
        v0_17[5] = com.teamspeak.ts3client.data.e.c.f;
        v0_17[6] = com.teamspeak.ts3client.data.e.c.g;
        v0_17[7] = com.teamspeak.ts3client.data.e.c.h;
        com.teamspeak.ts3client.data.e.c.k = v0_17;
        return;
    }

    private c(String p1, int p2, String p3, String p4)
    {
        this(p1, p2);
        this.i = p3;
        this.j = p4;
        return;
    }

    private String a()
    {
        return this.i;
    }

    private String b()
    {
        return this.j;
    }

    public static com.teamspeak.ts3client.data.e.c valueOf(String p1)
    {
        return ((com.teamspeak.ts3client.data.e.c) Enum.valueOf(com.teamspeak.ts3client.data.e.c, p1));
    }

    public static com.teamspeak.ts3client.data.e.c[] values()
    {
        return ((com.teamspeak.ts3client.data.e.c[]) com.teamspeak.ts3client.data.e.c.k.clone());
    }
}
