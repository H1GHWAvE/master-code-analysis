package com.teamspeak.ts3client.data.f;
public final class a implements com.teamspeak.ts3client.data.w {
    public java.util.SortedMap a;
    public com.teamspeak.ts3client.Ts3Application b;
    public boolean c;

    public a(com.teamspeak.ts3client.Ts3Application p2)
    {
        this.c = 0;
        this.b = p2;
        p2.a.c.a(this);
        this.a = new java.util.TreeMap();
        return;
    }

    private boolean a()
    {
        if ((this.b.a.u == null) || (this.b.a.u.c)) {
            int v0_7 = this.c;
        } else {
            v0_7 = 0;
        }
        return v0_7;
    }

    private int b(String p2)
    {
        return this.b(this.b.a.u.a(p2));
    }

    private boolean c(int p3)
    {
        return this.a.containsKey(Integer.valueOf(p3));
    }

    public final void a(com.teamspeak.ts3client.jni.k p5)
    {
        if ((p5 instanceof com.teamspeak.ts3client.jni.events.rare.ClientNeededPermissions)) {
            if (this.c) {
                this.c = 0;
            }
            this.a.put(Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.ClientNeededPermissions) p5).a), Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.ClientNeededPermissions) p5).b));
        }
        if (((p5 instanceof com.teamspeak.ts3client.jni.events.rare.ClientNeededPermissionsFinished)) && (((com.teamspeak.ts3client.jni.events.rare.ClientNeededPermissionsFinished) p5).a == this.b.a.e)) {
            this.c = 1;
            this.b.a.k.y();
        }
        return;
    }

    public final boolean a(int p5)
    {
        int v0_2;
        if (!this.a.containsKey(Integer.valueOf(p5))) {
            v0_2 = 0;
        } else {
            if (((Integer) this.a.get(Integer.valueOf(p5))).intValue() != 1) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
        }
        return v0_2;
    }

    public final boolean a(com.teamspeak.ts3client.jni.g p2)
    {
        try {
            int v0_4 = this.a(this.b.a.u.a(p2));
        } catch (int v0) {
            v0_4 = 0;
        }
        return v0_4;
    }

    public final boolean a(String p2)
    {
        try {
            int v0_4 = this.a(this.b.a.u.a(p2));
        } catch (int v0) {
            v0_4 = 0;
        }
        return v0_4;
    }

    public final int b(int p3)
    {
        int v0_2;
        if (!this.a.containsKey(Integer.valueOf(p3))) {
            v0_2 = 0;
        } else {
            v0_2 = ((Integer) this.a.get(Integer.valueOf(p3))).intValue();
        }
        return v0_2;
    }
}
