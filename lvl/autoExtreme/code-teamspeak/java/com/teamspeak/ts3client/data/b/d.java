package com.teamspeak.ts3client.data.b;
final class d extends android.database.sqlite.SQLiteOpenHelper {
    final synthetic com.teamspeak.ts3client.data.b.c a;

    public d(com.teamspeak.ts3client.data.b.c p4, android.content.Context p5)
    {
        this.a = p4;
        this(p5, "Teamspeak-Contacts", 0, 2);
        return;
    }

    public final void onCreate(android.database.sqlite.SQLiteDatabase p2)
    {
        p2.execSQL("create table contacts (contact_id integer primary key autoincrement, u_identifier text not null, customname text not null , display integer not null, status integer not null, mute integer not null, ignorepublicchat integer not null, ignoreprivatechat integer not null, ignorepokes integer not null, hideaway integer not null,hideavatar integer not null, whisperallow integer not null, volumemodifier float not null);");
        p2.execSQL("Create Index u_identifier_idx ON contacts(u_identifier);");
        return;
    }

    public final void onUpgrade(android.database.sqlite.SQLiteDatabase p2, int p3, int p4)
    {
        if (p3 < p4) {
            while ((p3 - 1) < p4) {
                switch (p3) {
                    case 2:
                        p2.execSQL("ALTER TABLE contacts ADD volumemodifier float not null DEFAULT(\'0\')");
                        break;
                }
                p3++;
            }
        }
        return;
    }
}
