package com.teamspeak.ts3client.data;
public final class ab {
    public String a;
    String b;
    public String c;
    public int d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public boolean j;
    public java.util.Vector k;
    public long l;
    long m;
    public int n;
    public String o;
    public int p;
    public long q;
    public String r;
    public java.util.BitSet s;
    public String t;

    public ab()
    {
        this.a = "";
        this.b = "";
        this.c = "";
        this.d = 1;
        this.e = "";
        this.f = "Android";
        this.g = "";
        this.h = "";
        this.i = "";
        this.j = 0;
        this.k = new java.util.Vector();
        this.l = 0;
        this.m = 0;
        this.n = 0;
        this.o = "";
        this.p = 0;
        this.q = 0;
        this.r = "";
        this.s = new java.util.BitSet(3);
        this.t = "";
        return;
    }

    private ab(String p5, String p6, String p7, String p8, String p9, String p10, int p11)
    {
        this.a = "";
        this.b = "";
        this.c = "";
        this.d = 1;
        this.e = "";
        this.f = "Android";
        this.g = "";
        this.h = "";
        this.i = "";
        this.j = 0;
        this.k = new java.util.Vector();
        this.l = 0;
        this.m = 0;
        this.n = 0;
        this.o = "";
        this.p = 0;
        this.q = 0;
        this.r = "";
        this.s = new java.util.BitSet(3);
        this.t = "";
        this.a = p5;
        this.c = p6.trim();
        this.e = p7;
        this.f = p8;
        this.g = p9;
        this.h = p10;
        this.d = p11;
        return;
    }

    private void b(int p1)
    {
        this.d = p1;
        return;
    }

    private void c(long p2)
    {
        this.l = p2;
        return;
    }

    private void c(Long p2)
    {
        if (this.k.contains(p2)) {
            this.k.remove(p2);
        }
        return;
    }

    private void e(String p1)
    {
        this.g = p1;
        return;
    }

    private void f(String p1)
    {
        this.h = p1;
        return;
    }

    private void g(String p1)
    {
        this.a = p1;
        return;
    }

    private void h(String p1)
    {
        this.f = p1;
        return;
    }

    private void i(String p1)
    {
        this.e = p1;
        return;
    }

    private void j(String p1)
    {
        this.i = p1;
        return;
    }

    private void k(String p1)
    {
        this.b = p1;
        return;
    }

    private void l(String p2)
    {
        this.o = p2.trim();
        return;
    }

    private String m()
    {
        return this.c;
    }

    private void m(String p1)
    {
        this.t = p1;
        return;
    }

    private long n()
    {
        return this.l;
    }

    private int o()
    {
        return this.d;
    }

    private String p()
    {
        return this.a;
    }

    private java.util.Vector q()
    {
        return this.k;
    }

    private long r()
    {
        return this.m;
    }

    private String s()
    {
        return this.b;
    }

    private String t()
    {
        return this.o;
    }

    private int u()
    {
        return this.p;
    }

    private java.util.BitSet v()
    {
        return this.s;
    }

    private String w()
    {
        return this.t;
    }

    public final String a()
    {
        return this.g;
    }

    public final void a(int p1)
    {
        this.p = p1;
        return;
    }

    public final void a(long p4)
    {
        this.m = (2.1219957905e-314 & p4);
        return;
    }

    public final void a(Long p2)
    {
        if (!this.k.contains(p2)) {
            this.k.add(p2);
        }
        return;
    }

    public final void a(String p2)
    {
        this.c = p2.trim();
        return;
    }

    public final void a(boolean p1)
    {
        this.j = p1;
        return;
    }

    public final String b()
    {
        return this.h;
    }

    public final void b(long p2)
    {
        this.q = p2;
        return;
    }

    public final void b(String p9)
    {
        if (!p9.equals("")) {
            if (!p9.contains(",")) {
                this.k.add(Long.valueOf(Long.parseLong(p9)));
            } else {
                Long v1_1 = p9.split(",");
                long v2_1 = v1_1.length;
                int v0_6 = 0;
                while (v0_6 < v2_1) {
                    this.k.add(Long.valueOf(Long.parseLong(v1_1[v0_6])));
                    v0_6++;
                }
            }
        }
        return;
    }

    public final boolean b(Long p2)
    {
        return this.k.contains(p2);
    }

    public final String c()
    {
        return this.f;
    }

    public final void c(String p1)
    {
        this.o = p1;
        return;
    }

    public final String d()
    {
        return this.e;
    }

    public final void d(String p1)
    {
        this.r = p1;
        return;
    }

    public final long[] e()
    {
        long[] v0_7;
        if (this.k.size() > 0) {
            long[] v2 = new long[(this.k.size() + 1)];
            java.util.Iterator v3 = this.k.iterator();
            int v1_1 = 0;
            while (v3.hasNext()) {
                v2[v1_1] = ((Long) v3.next()).longValue();
                v1_1++;
            }
            v2[v1_1] = 0;
            v0_7 = v2;
        } else {
            v0_7 = 0;
        }
        return v0_7;
    }

    public final String f()
    {
        return this.i;
    }

    public final boolean g()
    {
        return this.j;
    }

    public final String h()
    {
        java.util.Iterator v2 = this.k.iterator();
        String v1_1 = "";
        while (v2.hasNext()) {
            String v0_3 = ((Long) v2.next());
            if (!v1_1.equals("")) {
                v1_1 = new StringBuilder().append(v1_1).append(",").append(v0_3).toString();
            } else {
                v1_1 = new StringBuilder().append(v1_1).append(v0_3).toString();
            }
        }
        return v1_1;
    }

    public final int i()
    {
        return this.n;
    }

    public final void j()
    {
        this.n = 0;
        return;
    }

    public final long k()
    {
        return this.q;
    }

    public final String l()
    {
        return this.r;
    }

    public final String toString()
    {
        return new StringBuilder("Server [Label=").append(this.a).append(", ServerName=").append(this.b).append(", Address=").append(this.c).append(", Ident=").append(this.d).append(", ServerPassword=").append(this.e).append(", Nickname=").append(this.f).append(", DefaultChannel=").append(this.g).append(", DefaultChannelPassword=").append(this.h).append(", Token=").append(this.i).append(", subscribeAll=").append(this.j).append(", subscriptionList=").append(this.k).append(", DB_ID=").append(this.l).append(", iconID=").append(this.m).append(", retry=").append(this.n).append(", ip=").append(this.o).append(", port=").append(this.p).append(", reconnectChannel=").append(this.q).append(", reconnectPW=").append(this.r).append(", reconnectSettings=").append(this.s).append(", reconnectAwayMsg=").append(this.t).append("]").toString();
    }
}
