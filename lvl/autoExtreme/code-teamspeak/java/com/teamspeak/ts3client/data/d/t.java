package com.teamspeak.ts3client.data.d;
public final class t {
    private static java.util.HashMap a;

    static t()
    {
        com.teamspeak.ts3client.data.d.t.a = new java.util.HashMap();
        return;
    }

    public t()
    {
        return;
    }

    private static int a(float p2)
    {
        return ((int) ((com.teamspeak.ts3client.Ts3Application.a().getResources().getDisplayMetrics().density * p2) + 1056964608));
    }

    public static android.graphics.Bitmap a(int p6, float p7, float p8)
    {
        android.graphics.Bitmap v0_9;
        if (!com.teamspeak.ts3client.data.d.t.a.containsKey(new StringBuilder().append(p6).append("_").append(p7).append("X").append(p8).toString())) {
            android.graphics.drawable.BitmapDrawable v1_9 = new android.graphics.drawable.BitmapDrawable(com.teamspeak.ts3client.Ts3Application.a().getResources(), android.graphics.Bitmap.createScaledBitmap(((android.graphics.drawable.BitmapDrawable) com.teamspeak.ts3client.Ts3Application.a().getResources().getDrawable(p6)).getBitmap(), com.teamspeak.ts3client.data.d.t.a(p7), com.teamspeak.ts3client.data.d.t.a(p8), 0));
            com.teamspeak.ts3client.data.d.t.a.put(new StringBuilder().append(p6).append("_").append(p7).append("X").append(p8).toString(), v1_9);
            v0_9 = v1_9.getBitmap();
        } else {
            v0_9 = ((android.graphics.drawable.BitmapDrawable) com.teamspeak.ts3client.data.d.t.a.get(new StringBuilder().append(p6).append("_").append(p7).append("X").append(p8).toString())).getBitmap();
        }
        return v0_9;
    }

    public static android.graphics.drawable.BitmapDrawable a(int p6)
    {
        android.graphics.drawable.BitmapDrawable v0_7;
        if (!com.teamspeak.ts3client.data.d.t.a.containsKey(new StringBuilder().append(p6).append("_20.0X20.0").toString())) {
            v0_7 = new android.graphics.drawable.BitmapDrawable(com.teamspeak.ts3client.Ts3Application.a().getResources(), android.graphics.Bitmap.createScaledBitmap(((android.graphics.drawable.BitmapDrawable) com.teamspeak.ts3client.Ts3Application.a().getResources().getDrawable(p6)).getBitmap(), com.teamspeak.ts3client.data.d.t.a(1101004800), com.teamspeak.ts3client.data.d.t.a(1101004800), 0));
            com.teamspeak.ts3client.data.d.t.a.put(new StringBuilder().append(p6).append("_20.0X20.0").toString(), v0_7);
        } else {
            v0_7 = ((android.graphics.drawable.BitmapDrawable) com.teamspeak.ts3client.data.d.t.a.get(new StringBuilder().append(p6).append("_20.0X20.0").toString()));
        }
        return v0_7;
    }
}
