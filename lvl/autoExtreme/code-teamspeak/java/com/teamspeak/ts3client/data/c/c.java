package com.teamspeak.ts3client.data.c;
public final class c implements com.teamspeak.ts3client.data.w {
    static final android.graphics.drawable.Drawable a;
    private java.io.File b;
    private long c;
    private android.graphics.drawable.Drawable d;
    private com.teamspeak.ts3client.Ts3Application e;
    private java.util.ArrayList f;

    static c()
    {
        com.teamspeak.ts3client.data.c.c.a = com.teamspeak.ts3client.Ts3Application.a().getResources().getDrawable(2130837609);
        return;
    }

    public c(long p16, java.io.File p18, com.teamspeak.ts3client.Ts3Application p19)
    {
        this.f = new java.util.ArrayList();
        this.c = p16;
        this.b = p18;
        this.e = p19;
        if ((p16 != 0) && ((p16 > 999) && (!new java.io.File(p18, new StringBuilder("icon_").append(p16).append(".gif").toString()).exists()))) {
            p19.a.c.a(this);
            p19.d.log(java.util.logging.Level.INFO, new StringBuilder("Loading Icon ").append(p16).append(" from Server").toString());
            this.f.add(Integer.valueOf(p19.a.a.ts3client_requestFile(p19.a.e, 0, "", new StringBuilder("/icon_").append(p16).toString(), 1, 0, this.b.getAbsolutePath(), new StringBuilder("icon_").append(p16).toString())));
        }
        return;
    }

    private long b()
    {
        return this.c;
    }

    private int c()
    {
        return ((int) ((this.e.getResources().getDisplayMetrics().density * 1099956224) + 1056964608));
    }

    public final android.graphics.drawable.Drawable a()
    {
        android.graphics.drawable.Drawable v0_12;
        if (this.c != 0) {
            if ((this.d == null) || (this.d == com.teamspeak.ts3client.data.c.c.a)) {
                if (this.c > 999) {
                    android.graphics.drawable.Drawable v0_5 = new java.io.File(this.b, new StringBuilder("icon_").append(this.c).append(".gif").toString());
                    if (!v0_5.exists()) {
                        this.d = com.teamspeak.ts3client.data.c.c.a;
                    } else {
                        android.graphics.drawable.Drawable v0_10 = ((android.graphics.drawable.BitmapDrawable) android.graphics.drawable.Drawable.createFromPath(v0_5.getAbsolutePath())).getBitmap();
                        if ((v0_10.getHeight() != 16) || (v0_10.getWidth() != 16)) {
                            android.content.res.Resources v2_8 = android.graphics.Bitmap.createBitmap(16, 16, android.graphics.Bitmap$Config.ARGB_8888);
                            int v3_6 = new android.graphics.Canvas(v2_8);
                            v3_6.drawBitmap(v0_10, ((float) (8 - (v0_10.getWidth() / 2))), ((float) (8 - (v0_10.getHeight() / 2))), 0);
                            v3_6.save();
                            v0_10 = v2_8;
                        }
                        this.d = new android.graphics.drawable.BitmapDrawable(this.e.getResources(), android.graphics.Bitmap.createScaledBitmap(v0_10, this.c(), this.c(), 1));
                    }
                    v0_12 = this.d;
                } else {
                    android.graphics.drawable.Drawable v0_14;
                    if (this.c != 100) {
                        v0_14 = 0;
                    } else {
                        v0_14 = this.e.getResources().getDrawable(2130837613);
                    }
                    if (this.c == 200) {
                        v0_14 = this.e.getResources().getDrawable(2130837614);
                    }
                    if (this.c == 300) {
                        v0_14 = this.e.getResources().getDrawable(2130837615);
                    }
                    if (this.c == 500) {
                        v0_14 = this.e.getResources().getDrawable(2130837616);
                    }
                    if (this.c == 600) {
                        v0_14 = this.e.getResources().getDrawable(2130837617);
                    }
                    if (v0_14 == null) {
                        this.d = this.e.getResources().getDrawable(2130837609);
                    }
                    this.d = new android.graphics.drawable.BitmapDrawable(this.e.getResources(), android.graphics.Bitmap.createScaledBitmap(((android.graphics.drawable.BitmapDrawable) v0_14).getBitmap(), this.c(), this.c(), 1));
                    v0_12 = this.d;
                }
            } else {
                v0_12 = this.d;
            }
        } else {
            v0_12 = com.teamspeak.ts3client.data.c.c.a;
        }
        return v0_12;
    }

    public final void a(com.teamspeak.ts3client.jni.k p7)
    {
        if (((p7 instanceof com.teamspeak.ts3client.jni.events.rare.FileTransferStatus)) && ((((com.teamspeak.ts3client.jni.events.rare.FileTransferStatus) p7).b == 2065) && (this.f.contains(Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.FileTransferStatus) p7).a))))) {
            new java.io.File(this.b, new StringBuilder("icon_").append(this.c).toString()).renameTo(new java.io.File(this.b, new StringBuilder("icon_").append(this.c).append(".gif").toString()));
            this.f.remove(this.f.indexOf(Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.FileTransferStatus) p7).a)));
            this.e.a.k.z();
            if (this.f.size() == 0) {
                this.e.a.c.b(this);
            }
        }
        return;
    }
}
