package com.teamspeak.ts3client.a;
public final class s extends android.os.AsyncTask {
    final synthetic com.teamspeak.ts3client.a.r a;

    public s(com.teamspeak.ts3client.a.r p1)
    {
        this.a = p1;
        return;
    }

    private varargs Void a()
    {
        try {
            int v0_2 = com.teamspeak.ts3client.Ts3Application.a().getAssets().open("sound/speech/settings.ini");
            java.util.logging.Logger v1_2 = new java.io.DataInputStream(v0_2);
            java.util.logging.Level v2_1 = new java.io.BufferedReader(new java.io.InputStreamReader(v1_2));
        } catch (int v0_3) {
            com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v0_3.getMessage().toString());
            return 0;
        } catch (int v0_6) {
            com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v0_6.getMessage().toString());
            return 0;
        }
        do {
            String v3_2 = v2_1.readLine();
            if (v3_2 == null) {
                v2_1.close();
                v1_2.close();
                v0_2.close();
                return 0;
            } else {
                if (v3_2.matches("(.*= say\\(\".*\"\\))")) {
                    String v3_3 = v3_2.split("=");
                    v3_3[0] = v3_3[0].trim();
                }
            }
        } while(v3_3[1].equals(""));
        v3_3[1] = v3_3[1].substring(6);
        v3_3[1] = v3_3[1].substring(0, (v3_3[1].length() - 2));
        com.teamspeak.ts3client.a.r.a(this.a).put(v3_3[0], v3_3[1].trim());
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return this.a();
    }
}
