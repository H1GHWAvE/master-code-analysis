package com.teamspeak.ts3client.a;
public final class r implements android.speech.tts.TextToSpeech$OnInitListener, com.teamspeak.ts3client.a.n {
    android.speech.tts.TextToSpeech a;
    private java.util.HashMap b;
    private java.util.HashMap c;

    public r()
    {
        this.c = new java.util.HashMap();
        return;
    }

    static synthetic java.util.HashMap a(com.teamspeak.ts3client.a.r p1)
    {
        return p1.c;
    }

    public final void a()
    {
        Void[] v1_1 = new Void[0];
        new com.teamspeak.ts3client.a.s(this).execute(v1_1);
        return;
    }

    public final void a(android.content.Context p5)
    {
        this.a = new android.speech.tts.TextToSpeech(p5, this);
        this.b = new java.util.HashMap();
        this.b.put("streamType", "0");
        this.b.put("volume", "0.4");
        if (android.os.Build$VERSION.SDK_INT < 11) {
            Void[] v1_3 = new Void[0];
            new com.teamspeak.ts3client.a.s(this).execute(v1_3);
        } else {
            Void[] v2_2 = new Void[0];
            new com.teamspeak.ts3client.a.s(this).executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR, v2_2);
        }
        return;
    }

    public final void a(com.teamspeak.ts3client.jni.h p8, com.teamspeak.ts3client.a.o p9)
    {
        String v0_2 = ((String) this.c.get(p8.toString()));
        if (v0_2 != null) {
            if (p9 != null) {
                try {
                } catch (Exception v1_3) {
                    com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, new StringBuilder("SOUND ERROR: ").append(p8.toString()).toString(), v1_3);
                }
                try {
                    Exception v1_4 = v0_2.replace("${clientname}", p9.a).replace("${channelname}", p9.c);
                    v0_2 = v1_4.replace("${servername}", p9.d);
                } catch (String v0_5) {
                    v0_2 = v1_4;
                    v1_3 = v0_5;
                }
            }
            this.a.speak(v0_2, 0, this.b);
        }
        return;
    }

    public final void a(String p4)
    {
        this.a.speak(p4, 0, this.b);
        return;
    }

    public final void b()
    {
        this.a.shutdown();
        return;
    }

    public final void onInit(int p3)
    {
        this.a.setLanguage(java.util.Locale.US);
        return;
    }
}
