package com.teamspeak.ts3client.a;
public final class u extends android.os.AsyncTask {
    final synthetic com.teamspeak.ts3client.a.t a;

    public u(com.teamspeak.ts3client.a.t p1)
    {
        this.a = p1;
        return;
    }

    private varargs Void a()
    {
        int v0_1 = com.teamspeak.ts3client.Ts3Application.a().getAssets();
        try {
            java.util.logging.Logger v1_1 = v0_1.open("sound/female/settings.ini");
            java.util.logging.Level v2_1 = new java.io.DataInputStream(v1_1);
            java.io.BufferedReader v3_1 = new java.io.BufferedReader(new java.io.InputStreamReader(v2_1));
        } catch (int v0_5) {
            com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v0_5.getMessage().toString());
            return 0;
        } catch (int v0_2) {
            com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v0_2.getMessage().toString());
            return 0;
        }
        do {
            String v4_2 = v3_1.readLine();
            if (v4_2 == null) {
                v3_1.close();
                v2_1.close();
                v1_1.close();
                return 0;
            } else {
                if (v4_2.matches("(.*= play\\(\".*\"\\))")) {
                    String v4_3 = v4_2.split("=");
                    v4_3[0] = v4_3[0].trim();
                }
            }
        } while(v4_3[1].equals(""));
        v4_3[1] = v4_3[1].substring(7);
        v4_3[1] = v4_3[1].substring(0, (v4_3[1].length() - 2));
        if (!v4_3[1].contains("${clientType}")) {
            android.content.res.AssetFileDescriptor v5_16 = v0_1.openFd(new StringBuilder("sound/female/").append(v4_3[1]).toString());
            this.a.a.put(v4_3[0], Integer.valueOf(this.a.c.load(v5_16, 1)));
            v5_16.close();
        } else {
            android.content.res.AssetFileDescriptor v5_22 = v0_1.openFd(new StringBuilder("sound/female/").append(v4_3[1].replace("${clientType}", "neutral")).toString());
            this.a.a.put(new StringBuilder("neutral_").append(v4_3[0]).toString(), Integer.valueOf(this.a.c.load(v5_22, 1)));
            v5_22.close();
            android.content.res.AssetFileDescriptor v5_28 = v0_1.openFd(new StringBuilder("sound/female/").append(v4_3[1].replace("${clientType}", "blocked_user")).toString());
            this.a.a.put(new StringBuilder("blocked_").append(v4_3[0]).toString(), Integer.valueOf(this.a.c.load(v5_28, 1)));
            v5_28.close();
            android.content.res.AssetFileDescriptor v5_34 = v0_1.openFd(new StringBuilder("sound/female/").append(v4_3[1].replace("${clientType}", "friend")).toString());
            this.a.a.put(new StringBuilder("friend_").append(v4_3[0]).toString(), Integer.valueOf(this.a.c.load(v5_34, 1)));
            v5_34.close();
        }
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return this.a();
    }
}
