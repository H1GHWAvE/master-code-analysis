package com.teamspeak.ts3client.customs;
public final class h extends android.widget.LinearLayout {
    private android.widget.ImageView a;
    private android.widget.TextView b;

    private h(android.content.Context p5)
    {
        this(p5);
        android.view.View v1_1 = android.view.LayoutInflater.from(p5).inflate(2130903104, 0, 0);
        this.a = ((android.widget.ImageView) v1_1.findViewById(2131493252));
        this.b = ((android.widget.TextView) v1_1.findViewById(2131493253));
        this.setBackgroundResource(2130837686);
        this.addView(v1_1);
        return;
    }

    private void a(int p3, String p4)
    {
        this.a.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(p3, 1098907648, 1098907648));
        this.b.setText(p4);
        return;
    }

    private void a(android.graphics.drawable.Drawable p2, String p3)
    {
        this.a.setImageDrawable(p2);
        this.b.setText(p3);
        return;
    }

    public final void setEnabled(boolean p4)
    {
        super.setEnabled(p4);
        if (!p4) {
            this.b.setTextColor(-12303292);
            this.a.setColorFilter(-12303292, android.graphics.PorterDuff$Mode.MULTIPLY);
        }
        return;
    }
}
