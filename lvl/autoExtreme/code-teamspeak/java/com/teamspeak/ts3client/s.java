package com.teamspeak.ts3client;
final class s implements android.content.SharedPreferences$OnSharedPreferenceChangeListener {
    private com.teamspeak.ts3client.Ts3Application a;

    public s(com.teamspeak.ts3client.Ts3Application p1)
    {
        this.a = p1;
        return;
    }

    public final void onSharedPreferenceChanged(android.content.SharedPreferences p9, String p10)
    {
        if (p10.equals("audio_ptt")) {
            com.a.b.d.bw v0_4 = this.a.e.getBoolean("audio_ptt", 0);
            this.a.d.log(java.util.logging.Level.INFO, new StringBuilder("Setting PTT: ").append(v0_4).toString());
            if (v0_4 == null) {
                this.a.a.b.d();
                this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.d.k, 0);
                this.a.a.a.ts3client_setPreProcessorConfigValue(this.a.a.e, "vad", "true");
                this.a.a.a.ts3client_setPreProcessorConfigValue(this.a.a.e, "voiceactivation_level", String.valueOf(this.a.e.getInt("voiceactivation_level", 10)));
            } else {
                this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.d.k, 1);
                this.a.a.a.ts3client_setPreProcessorConfigValue(this.a.a.e, "vad", "false");
                this.a.a.a.ts3client_setPreProcessorConfigValue(this.a.a.e, "voiceactivation_level", "-50");
                this.a.a.k.H();
            }
            com.teamspeak.ts3client.ConnectionBackground.f(this.a.a.l);
        }
        if (p10.equals("voiceactivation_level")) {
            com.a.b.d.bw v0_37 = this.a.e.getInt("voiceactivation_level", 10);
            this.a.d.log(java.util.logging.Level.INFO, new StringBuilder("Setting voiceactivation_level: ").append(v0_37).toString());
            this.a.a.a.ts3client_setPreProcessorConfigValue(this.a.a.e, "voiceactivation_level", String.valueOf(v0_37));
        }
        if (p10.equals("audio_bt")) {
            com.a.b.d.bw v0_43 = this.a.e.getBoolean("audio_bt", 0);
            this.a.d.log(java.util.logging.Level.INFO, new StringBuilder("Setting BT:").append(v0_43).toString());
            if (v0_43 == null) {
                this.a.a.b.b(0);
            } else {
                this.a.a.b.b(1);
            }
        }
        p10.equals("audio_use_tts");
        p10.equals("call_setaway");
        p10.equals("call_awaymessage");
        if (p10.equals("show_squeryclient")) {
            this.a.a.k.z();
        }
        if (p10.equals("audio_handfree")) {
            if (!this.a.e.getBoolean("audio_handfree", 0)) {
                com.teamspeak.ts3client.a.j.a().b(0);
            } else {
                com.teamspeak.ts3client.a.j.a().a(0);
            }
        }
        if (p10.equals("use_proximity")) {
            com.a.b.d.bw v0_69 = this.a.e.getBoolean("use_proximity", 1);
            if (this.a.m != null) {
                this.a.f.j();
                if (this.a.m.isHeld()) {
                    this.a.m.release();
                }
                if (v0_69 != null) {
                    this.a.f.k();
                }
            }
        }
        if (p10.equals("whisper")) {
            String v1_42 = this.a.a.w.keySet().iterator();
            while (v1_42.hasNext()) {
                this.a.a.a.ts3client_removeFromAllowedWhispersFrom(this.a.a.e, ((Integer) v1_42.next()).intValue());
            }
            this.a.a.w.clear();
        }
        if (p10.equals("talk_notification")) {
            com.teamspeak.ts3client.ConnectionBackground.b(this.a.e.getBoolean("talk_notification", 0));
            com.teamspeak.ts3client.ConnectionBackground.a(com.teamspeak.ts3client.ConnectionBackground.e());
        }
        return;
    }
}
