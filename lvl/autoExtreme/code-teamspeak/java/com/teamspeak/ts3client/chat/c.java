package com.teamspeak.ts3client.chat;
public final class c extends android.support.v4.app.Fragment {
    private com.teamspeak.ts3client.Ts3Application a;
    private android.widget.ListView b;
    private com.teamspeak.ts3client.chat.d c;

    public c()
    {
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p5, android.view.ViewGroup p6)
    {
        this.a = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        this.a.c = this;
        android.view.View v1 = p5.inflate(2130903077, p6, 0);
        this.b = ((android.widget.ListView) v1.findViewById(2131493098));
        this.c = com.teamspeak.ts3client.chat.d.a(this.a.getBaseContext());
        this.b.setAdapter(this.c);
        if (this.a.a != null) {
            this.a.a.k.e(0);
        }
        this.n();
        return v1;
    }

    public final void a(android.os.Bundle p1)
    {
        super.a(p1);
        return;
    }

    public final void a(android.view.Menu p1, android.view.MenuInflater p2)
    {
        p1.clear();
        super.a(p1, p2);
        return;
    }

    public final void c(android.os.Bundle p1)
    {
        super.c(p1);
        return;
    }

    public final void d(android.os.Bundle p1)
    {
        return;
    }

    public final void t()
    {
        super.t();
        if (this.a.a != null) {
            this.a.a.k.e(0);
        }
        return;
    }
}
