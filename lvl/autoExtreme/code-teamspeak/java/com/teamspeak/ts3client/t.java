package com.teamspeak.ts3client;
public final class t extends android.support.v4.app.Fragment {
    private static com.teamspeak.ts3client.customs.FloatingButton i;
    public com.teamspeak.ts3client.Ts3Application a;
    private boolean aA;
    private com.teamspeak.ts3client.bf aB;
    private boolean aC;
    private android.content.BroadcastReceiver aD;
    private com.teamspeak.ts3client.d.c.d aE;
    private boolean aF;
    private com.teamspeak.ts3client.customs.FloatingButton aG;
    private android.telephony.PhoneStateListener aH;
    private com.teamspeak.ts3client.customs.FloatingButton aI;
    private int at;
    private boolean au;
    private com.teamspeak.ts3client.chat.c av;
    private boolean aw;
    private boolean ax;
    private boolean ay;
    private boolean az;
    boolean b;
    boolean c;
    int d;
    boolean e;
    boolean f;
    com.teamspeak.ts3client.customs.FloatingButton g;
    com.teamspeak.ts3client.customs.FloatingButton h;
    private com.teamspeak.ts3client.data.customExpandableListView j;
    private com.teamspeak.ts3client.d.h k;
    private com.teamspeak.ts3client.data.g l;
    private android.telephony.TelephonyManager m;

    public t()
    {
        this.a = com.teamspeak.ts3client.Ts3Application.a();
        this.b = 0;
        this.c = 0;
        this.at = 2;
        this.aw = 0;
        this.ax = 0;
        this.d = 22;
        this.aD = new com.teamspeak.ts3client.u(this);
        this.aF = 0;
        this.aH = new com.teamspeak.ts3client.ah(this);
        return;
    }

    static synthetic com.teamspeak.ts3client.customs.FloatingButton J()
    {
        return com.teamspeak.ts3client.t.i;
    }

    private void K()
    {
        if (this.f) {
            this.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837656, ((float) this.d), ((float) this.d)));
        } else {
            this.c = 0;
            this.a.g().a = 0;
            this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.d.g, 0);
            this.a.a.a.ts3client_flushClientSelfUpdates(this.a.a.e, "");
            this.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837654, ((float) this.d), ((float) this.d)));
            this.a.a.v.s.set(1);
        }
        if (this.e) {
            this.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837636, ((float) this.d), ((float) this.d)));
        } else {
            this.b = 0;
            this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.d.f, 0);
            this.a.a.a.ts3client_flushClientSelfUpdates(this.a.a.e, "");
            this.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837633, ((float) this.d), ((float) this.d)));
            this.a.a.v.s.set(0);
        }
        new android.os.Handler().postDelayed(new com.teamspeak.ts3client.as(this), 1000);
        return;
    }

    private void L()
    {
        this.e = this.b;
        this.f = this.c;
        this.a.g().a = 1;
        this.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837636, ((float) this.d), ((float) this.d)));
        this.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837656, ((float) this.d), ((float) this.d)));
        this.c = 1;
        this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.d.g, 1);
        this.a.a.a.ts3client_flushClientSelfUpdates(this.a.a.e, "");
        this.b = 1;
        this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.d.f, 1);
        this.a.a.a.ts3client_flushClientSelfUpdates(this.a.a.e, "");
        return;
    }

    private void M()
    {
        android.app.AlertDialog v0_2 = new android.app.AlertDialog$Builder(this.i()).create();
        com.teamspeak.ts3client.data.d.q.a(v0_2);
        v0_2.setTitle(com.teamspeak.ts3client.data.e.a.a("disconnect.info"));
        v0_2.setMessage(com.teamspeak.ts3client.data.e.a.a("disconnect.text"));
        v0_2.setButton(-1, com.teamspeak.ts3client.data.e.a.a("disconnect.button"), new com.teamspeak.ts3client.az(this));
        v0_2.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.ba(this, v0_2));
        v0_2.setCancelable(0);
        v0_2.show();
        return;
    }

    private void N()
    {
        this.d(0);
        return;
    }

    private boolean O()
    {
        return this.au;
    }

    private void P()
    {
        this.i().runOnUiThread(new com.teamspeak.ts3client.ai(this));
        return;
    }

    private void Q()
    {
        com.teamspeak.ts3client.customs.FloatingButton v0 = this.aG;
        if (!v0.c) {
            v0.a(1);
            v0.invalidate();
        }
        return;
    }

    private void R()
    {
        this.i().runOnUiThread(new com.teamspeak.ts3client.ap(this));
        return;
    }

    private void S()
    {
        if (this.a.a.v.j) {
            this.a.a.l.h = 1;
            this.a.a.a.ts3client_requestChannelUnsubscribeAll(this.a.a.e, "UnsubscribeAll");
            if (this.a.a.v.e() != null) {
                this.a.a.a.ts3client_requestChannelSubscribe(this.a.a.e, this.a.a.v.e(), "Subscribe after UnsubscribeAll");
            }
        } else {
            this.a.a.l.h = 1;
            this.a.a.a.ts3client_requestChannelSubscribeAll(this.a.a.e, "SubscribeAll");
        }
        return;
    }

    private boolean T()
    {
        return this.ax;
    }

    private static int a(float p2)
    {
        return ((int) ((com.teamspeak.ts3client.Ts3Application.a().getResources().getDisplayMetrics().density * p2) + 1056964608));
    }

    private void a(android.text.Spanned p3, String p4, String p5)
    {
        this.i().runOnUiThread(new com.teamspeak.ts3client.ao(this, p3, p4, p5));
        return;
    }

    private void a(com.teamspeak.ts3client.data.a[] p3)
    {
        if (this.l()) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.v(this, p3));
        }
        return;
    }

    static synthetic boolean a(com.teamspeak.ts3client.t p1)
    {
        return p1.aC;
    }

    static synthetic boolean a(com.teamspeak.ts3client.t p0, boolean p1)
    {
        p0.aC = p1;
        return p1;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application b(com.teamspeak.ts3client.t p1)
    {
        return p1.a;
    }

    private void b(com.teamspeak.ts3client.data.a p5)
    {
        if ((this.l()) && (!(this.a.c instanceof com.teamspeak.ts3client.c))) {
            int v0_9;
            this.a.d.log(java.util.logging.Level.INFO, new StringBuilder("Channelinfo: ").append(p5).toString());
            if (com.teamspeak.ts3client.c.b != p5) {
                com.teamspeak.ts3client.c.a = new com.teamspeak.ts3client.c();
                com.teamspeak.ts3client.c.b = p5;
                v0_9 = com.teamspeak.ts3client.c.a;
            } else {
                v0_9 = com.teamspeak.ts3client.c.a;
            }
            android.support.v4.app.cd v1_2 = this.M.a();
            v1_2.b(v0_9, "ChannelInfo");
            v1_2.f();
            v1_2.a(4097);
            v1_2.i();
        }
        return;
    }

    static synthetic com.teamspeak.ts3client.d.h c(com.teamspeak.ts3client.t p1)
    {
        return p1.k;
    }

    static synthetic com.teamspeak.ts3client.data.g d(com.teamspeak.ts3client.t p1)
    {
        return p1.l;
    }

    static synthetic com.teamspeak.ts3client.data.customExpandableListView e(com.teamspeak.ts3client.t p1)
    {
        return p1.j;
    }

    static synthetic void f(com.teamspeak.ts3client.t p5)
    {
        if (p5.a.a.v.j) {
            p5.a.a.l.h = 1;
            p5.a.a.a.ts3client_requestChannelUnsubscribeAll(p5.a.a.e, "UnsubscribeAll");
            if (p5.a.a.v.e() != null) {
                p5.a.a.a.ts3client_requestChannelSubscribe(p5.a.a.e, p5.a.a.v.e(), "Subscribe after UnsubscribeAll");
            }
        } else {
            p5.a.a.l.h = 1;
            p5.a.a.a.ts3client_requestChannelSubscribeAll(p5.a.a.e, "SubscribeAll");
        }
        return;
    }

    static synthetic com.teamspeak.ts3client.chat.c g(com.teamspeak.ts3client.t p1)
    {
        return p1.av;
    }

    static synthetic void h(com.teamspeak.ts3client.t p2)
    {
        com.teamspeak.ts3client.customs.FloatingButton v0 = p2.aG;
        if (!v0.c) {
            v0.a(1);
            v0.invalidate();
        }
        return;
    }

    static synthetic boolean i(com.teamspeak.ts3client.t p1)
    {
        return p1.ay;
    }

    static synthetic boolean j(com.teamspeak.ts3client.t p1)
    {
        p1.b = 0;
        return 0;
    }

    static synthetic int k(com.teamspeak.ts3client.t p1)
    {
        return p1.d;
    }

    static synthetic com.teamspeak.ts3client.customs.FloatingButton l(com.teamspeak.ts3client.t p1)
    {
        return p1.aI;
    }

    public final void A()
    {
        if ((this.au) && (this.az)) {
            this.g(0);
        }
        return;
    }

    final void B()
    {
        if (!this.aw) {
            this.aI = ((com.teamspeak.ts3client.customs.FloatingButton) this.i().findViewById(2131493216));
            this.aI.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837645, ((float) this.d), ((float) this.d)));
            this.aI.setOnClickListener(new com.teamspeak.ts3client.y(this));
            this.aG = ((com.teamspeak.ts3client.customs.FloatingButton) this.i().findViewById(2131493213));
            this.aG.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837640, ((float) this.d), ((float) this.d)));
            this.aG.setOnClickListener(new com.teamspeak.ts3client.z(this));
            this.g = ((com.teamspeak.ts3client.customs.FloatingButton) this.i().findViewById(2131493214));
            this.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837633, ((float) this.d), ((float) this.d)));
            this.g.setOnClickListener(new com.teamspeak.ts3client.aa(this));
            if (this.ay) {
                this.i().runOnUiThread(new com.teamspeak.ts3client.ac(this));
            }
            this.h = ((com.teamspeak.ts3client.customs.FloatingButton) this.i().findViewById(2131493215));
            this.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837654, ((float) this.d), ((float) this.d)));
            this.h.setOnClickListener(new com.teamspeak.ts3client.ad(this));
            com.teamspeak.ts3client.ConnectionBackground v0_25 = ((com.teamspeak.ts3client.customs.FloatingButton) this.i().findViewById(2131493217));
            com.teamspeak.ts3client.t.i = v0_25;
            v0_25.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837644, ((float) this.d), ((float) this.d)));
            com.teamspeak.ts3client.t.i.setOnTouchListener(new com.teamspeak.ts3client.af(this));
            this.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837654, ((float) this.d), ((float) this.d)));
            this.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837633, ((float) this.d), ((float) this.d)));
            if (this.b) {
                this.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837636, ((float) this.d), ((float) this.d)));
            }
            if (this.c) {
                this.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837656, ((float) this.d), ((float) this.d)));
            }
            this.az = this.a.e.getBoolean("audio_ptt", 0);
            if (!this.az) {
                this.C();
            }
            this.j = ((com.teamspeak.ts3client.data.customExpandableListView) this.i().findViewById(2131493212));
            this.j.setOnScrollListener(new com.teamspeak.ts3client.ag(this));
            this.l.b = this.j;
            if (!this.a.e.getBoolean("trenn_linie", 0)) {
                this.j.setDividerHeight(0);
            }
            this.j.setAlwaysDrawnWithCacheEnabled(0);
            this.j.setAdapter(this.l);
            this.j.setGroupIndicator(0);
            this.a.a.l.b();
        }
        return;
    }

    public final void C()
    {
        if (!this.ax) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.aj(this));
        }
        return;
    }

    public final void D()
    {
        this.i().runOnUiThread(new com.teamspeak.ts3client.al(this));
        return;
    }

    public final void E()
    {
        try {
            this.i().runOnUiThread(new com.teamspeak.ts3client.aq(this));
        } catch (java.util.logging.Logger v0) {
            this.a.d.log(java.util.logging.Level.SEVERE, "Error: Dialog Exception");
        }
        return;
    }

    public final void F()
    {
        if (this.b) {
            this.b = 0;
            this.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837633, ((float) this.d), ((float) this.d)));
            this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.d.f, 0);
            this.a.a.a.ts3client_flushClientSelfUpdates(this.a.a.e, "InputUnMute");
        } else {
            this.b = 1;
            this.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837636, ((float) this.d), ((float) this.d)));
            this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.d.f, 1);
            this.a.a.a.ts3client_flushClientSelfUpdates(this.a.a.e, "InputMute");
        }
        return;
    }

    public final void G()
    {
        if (this.c) {
            this.c = 0;
            this.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837654, ((float) this.d), ((float) this.d)));
            this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.d.g, 0);
            this.a.a.a.ts3client_flushClientSelfUpdates(this.a.a.e, "");
            this.a.g().a = 0;
            this.a.g().a(com.teamspeak.ts3client.jni.h.aU, 0);
            this.a.a.v.s.clear(1);
        } else {
            this.c = 1;
            this.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837656, ((float) this.d), ((float) this.d)));
            this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.d.g, 1);
            this.a.a.a.ts3client_flushClientSelfUpdates(this.a.a.e, "");
            this.a.g().a(com.teamspeak.ts3client.jni.h.aT, 0);
            this.a.g().a = 1;
            this.a.a.v.s.set(1);
        }
        return;
    }

    public final void H()
    {
        this.aA = 0;
        this.ax = 0;
        return;
    }

    public final void I()
    {
        this.B();
        this.a.c = this;
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p10, android.view.ViewGroup p11)
    {
        if (this.aE == null) {
            this.aE = new com.teamspeak.ts3client.d.c.d();
        }
        android.support.v4.app.bb v0_37;
        if (!this.aw) {
            this.a.o.a(this.a.a.v.a);
            this.a.o.b(this.a.a.m());
            this.n();
            this.i().getWindow().addFlags(32768);
            this.i().getWindow().addFlags(524288);
            this.i().getWindow().addFlags(4194304);
            this.i().getWindow().addFlags(128);
            this.a.c = this;
            this.a.a.f = new com.teamspeak.ts3client.data.g(this.i(), this.M);
            this.l = this.a.a.f;
            android.view.View v7 = p10.inflate(2130903088, p11, 0);
            if (this.j == null) {
                if (this.a.a.n == 0) {
                    this.k = new com.teamspeak.ts3client.d.h(this.i(), com.teamspeak.ts3client.data.e.a.a("connectiondialog.step0"), com.teamspeak.ts3client.data.e.a.a("connectiondialog.info"), com.teamspeak.ts3client.data.e.a.a("button.cancel"), 0);
                    this.i().runOnUiThread(new com.teamspeak.ts3client.am(this));
                } else {
                    this.a.a.k.a(new StringBuilder().append(this.i().getString(2131034152)).append(this.a.a.n).toString(), this.i().getString(2131034151), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), this.i().getString(2131034142));
                    v0_37 = v7;
                    return v0_37;
                }
            }
            this.m = ((android.telephony.TelephonyManager) this.a.getSystemService("phone"));
            this.m.listen(this.aH, 32);
            this.aB = new com.teamspeak.ts3client.bf(this.a);
            this.a.e.registerOnSharedPreferenceChangeListener(this.aB);
            v0_37 = v7;
        } else {
            v0_37 = p10.inflate(2130903088, p11, 0);
        }
        return v0_37;
    }

    public final void a()
    {
        if (this.a.c != null) {
            this.a.c.i().runOnUiThread(new com.teamspeak.ts3client.ay(this));
        }
        return;
    }

    public final void a(android.os.Bundle p2)
    {
        super.a(p2);
        this.av = new com.teamspeak.ts3client.chat.c();
        return;
    }

    public final void a(android.view.Menu p3, android.view.MenuInflater p4)
    {
        p3.clear();
        this.aE.a(p3, this.a);
        super.a(p3, p4);
        return;
    }

    public final void a(com.teamspeak.ts3client.data.a p2)
    {
        this.a(p2, 0);
        return;
    }

    public final void a(com.teamspeak.ts3client.data.a p3, int p4)
    {
        this.i().runOnUiThread(new com.teamspeak.ts3client.at(this, p3, p4));
        return;
    }

    public final void a(com.teamspeak.ts3client.data.c p5)
    {
        if ((this.l()) && (!(this.a.c instanceof com.teamspeak.ts3client.h))) {
            this.a.d.log(java.util.logging.Level.INFO, new StringBuilder("Clientinfo: ").append(p5).toString());
            int v0_6 = com.teamspeak.ts3client.h.a(p5);
            android.support.v4.app.cd v1_2 = this.M.a();
            v1_2.b(v0_6, "ClientInfo");
            v1_2.f();
            v1_2.a(4097);
            v1_2.i();
        }
        return;
    }

    public final void a(String p3, Boolean p4)
    {
        if (this.l()) {
            this.a.c.i().runOnUiThread(new com.teamspeak.ts3client.w(this, p4, p3));
        }
        return;
    }

    public final void a(String p10, String p11, Boolean p12, Boolean p13, Boolean p14, String p15)
    {
        try {
            this.i().runOnUiThread(new com.teamspeak.ts3client.ar(this, p10, p11, p15, p14, p13, p12));
        } catch (java.util.logging.Logger v0) {
            this.a.d.log(java.util.logging.Level.SEVERE, "Error: Dialog Exception");
        }
        return;
    }

    public final void a(String p7, String p8, Boolean p9, Boolean p10, Boolean p11, String p12, com.teamspeak.ts3client.jni.h p13)
    {
        this.a(p7, p8, p9, p10, p11, p12);
        this.a.g().a(p13, new com.teamspeak.ts3client.a.o("", 0, "", this.a.a.m()));
        return;
    }

    public final void a(String p7, String p8, String p9)
    {
        android.app.Notification v0_1 = new android.content.Intent(this.a, com.teamspeak.ts3client.StartGUIFragment);
        v0_1.setAction("android.intent.action.MAIN");
        v0_1.addCategory("android.intent.category.LAUNCHER");
        android.app.Notification v0_2 = android.app.PendingIntent.getActivity(this.a, 0, v0_1, 0);
        android.app.NotificationManager v1_5 = new android.support.v4.app.dm(this.a.getApplicationContext());
        v1_5.a(2130837622).c(p7).a(System.currentTimeMillis()).a().a(p8).b(p9).d = v0_2;
        this.a.l.notify(0, 100, v1_5.c());
        return;
    }

    public final void a(boolean p3)
    {
        this.a.c.i().runOnUiThread(new com.teamspeak.ts3client.bb(this, p3));
        return;
    }

    public final boolean a(android.view.MenuItem p3)
    {
        return this.aE.a(p3, this.a);
    }

    public final boolean b()
    {
        android.text.Spanned v0_5;
        android.text.Spanned v0_3 = this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.i.x);
        String v1_6 = this.a.a.a.b(this.a.a.e, com.teamspeak.ts3client.jni.i.w);
        switch (v0_3) {
            case 1:
                com.teamspeak.ts3client.chat.d.b(v1_6);
                v0_5 = 0;
                break;
            case 2:
                if (v1_6.equals("")) {
                } else {
                    this.a(com.teamspeak.ts3client.data.d.a.a(v1_6), com.teamspeak.ts3client.data.e.a.a("messages.hostmessage"), com.teamspeak.ts3client.data.e.a.a("button.close"));
                }
                break;
            case 3:
                this.a(com.teamspeak.ts3client.data.d.a.a(v1_6), com.teamspeak.ts3client.data.e.a.a("messages.hostmessage"), com.teamspeak.ts3client.data.e.a.a("button.exit"));
                v0_5 = 1;
                break;
            default:
        }
        return v0_5;
    }

    public final void c(android.os.Bundle p1)
    {
        super.c(p1);
        this.B();
        return;
    }

    public final void d()
    {
        if ((this.a.c != null) && (this.l())) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.bd(this));
        }
        return;
    }

    public final void d(android.os.Bundle p1)
    {
        return;
    }

    public final void d(boolean p14)
    {
        this.aF = 1;
        try {
            if ((this.i() != null) && (this.aD != null)) {
                this.i().unregisterReceiver(this.aD);
            }
        } catch (android.app.Notification v0) {
            this.a.d.log(java.util.logging.Level.WARNING, "java.lang.IllegalArgumentException: Receiver not registered");
        }
        if (this.k.l()) {
            this.k.b();
        }
        this.aw = 1;
        if (!this.a.g) {
            if (!p14) {
                this.a.f.r = 1;
            }
        } else {
            if (this.M.f() > 0) {
                this.M.b(this.M.g().a());
                android.app.Notification v0_18 = this.M.a();
                v0_18.b(this.a.c);
                v0_18.a(8194);
                v0_18.i();
            }
            android.app.Notification v0_20 = this.M.a();
            try {
                v0_20.b(this.a.a.k);
            } catch (com.teamspeak.ts3client.StartGUIFragment v2) {
            }
            v0_20.f(this.M.a("Bookmark"));
            v0_20.a(8194);
            v0_20.i();
        }
        this.i().getWindow().clearFlags(32768);
        this.i().getWindow().clearFlags(524288);
        this.i().getWindow().clearFlags(4194304);
        this.i().getWindow().clearFlags(128);
        if (this.m != null) {
            this.m.listen(this.aH, 0);
        }
        this.a.e.unregisterOnSharedPreferenceChangeListener(this.aB);
        if (this.a.l != null) {
            this.a.l.cancel(1);
        }
        try {
            android.app.Notification v0_38 = this.a.a;
            v0_38.m.d.log(java.util.logging.Level.INFO, "Stop AUDIO");
            v0_38.b.f();
            v0_38.b.e();
            android.app.Notification v0_41 = this.a.a;
            com.teamspeak.ts3client.data.d.q.a();
            com.teamspeak.ts3client.data.v.a().b = 0;
            v0_41.m.f.o.b.a(v0_41.v.l, v0_41.v);
            v0_41.m.d.log(java.util.logging.Level.INFO, "Disconnecting from Server");
            v0_41.a.ts3client_stopConnection(v0_41.e, "....TS3 Android....");
            v0_41.a.ts3client_closeCaptureDevice(v0_41.e);
            v0_41.a.ts3client_closePlaybackDevice(v0_41.e);
            v0_41.a.ts3client_destroyServerConnectionHandler(v0_41.e);
            v0_41.m.stopService(v0_41.m.k);
            v0_41.b.b();
            new android.os.Handler().postDelayed(new com.teamspeak.ts3client.data.f(v0_41), 3000);
            v0_41.a.ts3client_unregisterCustomDevice("");
            v0_41.a = 0;
            com.teamspeak.ts3client.StartGUIFragment v2_31 = com.teamspeak.ts3client.chat.d.a(v0_41.m.getBaseContext());
            v2_31.c = 0;
            v2_31.d = 0;
            v2_31.a.clear();
            v2_31.b.clear();
            v2_31.a = new java.util.ArrayList();
            v2_31.b = new java.util.HashMap();
            v0_41.m.d.log(java.util.logging.Level.INFO, "ClientLib closed");
            this.a.d.log(java.util.logging.Level.INFO, "Connect status: STATUS_DISCONNECTED");
        } catch (android.app.Notification v0) {
        }
        com.teamspeak.ts3client.data.b.c.a.b = new java.util.Vector();
        try {
            android.app.Notification v0_49 = this.a.a.l;
            v0_49.e.a.c.b(v0_49);
            v0_49.e.e.unregisterOnSharedPreferenceChangeListener(v0_49.f);
        } catch (android.app.Notification v0) {
        }
        if ((this.a.b != null) && (this.a.b.isHeld())) {
            this.a.b.release();
        }
        if ((this.a.m != null) && (this.a.m.isHeld())) {
            this.a.m.release();
        }
        if ((this.a.n != null) && (this.a.n.isHeld())) {
            this.a.n.release();
        }
        android.app.Notification v0_74;
        if (this.a.a == null) {
            v0_74 = 0;
        } else {
            v0_74 = this.a.a.v;
        }
        this.a.f.j();
        if (this.a.a != null) {
            this.a.a.k = 0;
        }
        this.a.a = 0;
        this.a.c = this;
        if ((p14) && (v0_74 != null)) {
            com.teamspeak.ts3client.StartGUIFragment v2_50 = this.a.f;
            if (v0_74.n <= (Integer.parseInt(v2_50.n.e.getString("reconnect.limit", "15")) - 1)) {
                android.app.Notification v3_28 = new android.content.Intent(v2_50.n, com.teamspeak.ts3client.StartGUIFragment);
                v3_28.setAction("android.intent.action.MAIN");
                v3_28.addCategory("android.intent.category.LAUNCHER");
                android.app.Notification v3_29 = android.app.PendingIntent.getActivity(v2_50.n, 0, v3_28, 0);
                android.app.NotificationManager v4_21 = new android.support.v4.app.dm(v2_50.n.getApplicationContext());
                v4_21.a(2130837622).c(com.teamspeak.ts3client.data.e.a.a("connection.reconnect.retry")).a(System.currentTimeMillis()).a().a(com.teamspeak.ts3client.data.e.a.a("connection.reconnect.retry")).b(com.teamspeak.ts3client.data.e.a.a("connection.reconnect.retry")).d = v3_29;
                android.app.Notification v3_30 = v4_21.c();
                v3_30.flags = (v3_30.flags | 20);
                v2_50.n.l.notify(0, 101, v3_30);
                v0_74.n = (v0_74.n + 1);
                v2_50.o.a(v0_74);
            } else {
                android.app.Notification v0_78 = new android.content.Intent(v2_50.n, com.teamspeak.ts3client.StartGUIFragment);
                v0_78.setAction("android.intent.action.MAIN");
                v0_78.addCategory("android.intent.category.LAUNCHER");
                android.app.Notification v0_79 = android.app.PendingIntent.getActivity(v2_50.n, 0, v0_78, 0);
                android.app.Notification v3_36 = new android.support.v4.app.dm(v2_50.n.getApplicationContext());
                android.app.NotificationManager v4_34 = v3_36.a(2130837622).c(com.teamspeak.ts3client.data.e.a.a("connection.reconnect")).a(System.currentTimeMillis()).a().a(com.teamspeak.ts3client.data.e.a.a("connection.reconnect"));
                String v6_10 = new Object[1];
                v6_10[0] = Integer.valueOf(Integer.parseInt(v2_50.n.e.getString("reconnect.limit", "15")));
                v4_34.b(com.teamspeak.ts3client.data.e.a.a("connection.reconnect.text", v6_10)).d = v0_79;
                android.app.Notification v0_80 = v3_36.c();
                v0_80.flags = (v0_80.flags | 20);
                v2_50.n.l.notify(0, 101, v0_80);
                v2_50.p = 0;
                v2_50.s = 1;
            }
        }
        this.a.o.a("TeamSpeak");
        return;
    }

    public final void e()
    {
        super.e();
        android.content.IntentFilter v0_1 = new android.content.IntentFilter();
        v0_1.addAction("android.intent.action.HEADSET_PLUG");
        v0_1.addAction("android.intent.action.NEW_OUTGOING_CALL");
        this.i().registerReceiver(this.aD, v0_1);
        return;
    }

    public final void e(boolean p3)
    {
        if (!p3) {
            this.ay = 0;
        } else {
            this.i().runOnUiThread(new com.teamspeak.ts3client.ak(this));
            this.ay = 1;
        }
        return;
    }

    public final void f(boolean p3)
    {
        this.i().runOnUiThread(new com.teamspeak.ts3client.au(this, p3));
        return;
    }

    public final void g(boolean p7)
    {
        if (!p7) {
            if (!this.aA) {
                this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.d.k, 1);
                this.au = 0;
                this.a.a.b.f();
            } else {
                this.au = 0;
            }
        } else {
            this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.jni.d.k, 0);
            this.au = 1;
            this.a.a.b.d();
        }
        return;
    }

    public final void h(boolean p3)
    {
        this.i().runOnUiThread(new com.teamspeak.ts3client.ax(this, p3));
        return;
    }

    public final void i(boolean p2)
    {
        if ((!p2) && (this.au)) {
            this.aA = 1;
        }
        this.ax = p2;
        return;
    }

    public final void onConfigurationChanged(android.content.res.Configuration p1)
    {
        super.onConfigurationChanged(p1);
        this.A();
        return;
    }

    public final void s()
    {
        super.s();
        if ((this.j != null) && (this.aw != 1)) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.an(this));
        }
        return;
    }

    public final void t()
    {
        super.t();
        return;
    }

    public final void u()
    {
        super.u();
        if (!this.aF) {
            this.a.a.l.d();
            this.i().unregisterReceiver(this.aD);
            this.d(0);
        }
        return;
    }

    public final void y()
    {
        if (this.l()) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.be(this));
        }
        return;
    }

    public final void z()
    {
        if (this.l()) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.x(this));
        }
        return;
    }
}
