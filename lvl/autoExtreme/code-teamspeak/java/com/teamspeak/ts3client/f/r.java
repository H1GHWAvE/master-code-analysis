package com.teamspeak.ts3client.f;
final class r extends android.support.v4.app.ax implements com.teamspeak.ts3client.data.y {
    final java.util.BitSet at;
    android.view.KeyCharacterMap au;
    boolean av;
    final synthetic com.teamspeak.ts3client.f.p aw;
    private android.widget.TextView ax;
    private android.widget.CheckBox ay;

    private r(com.teamspeak.ts3client.f.p p2)
    {
        this.aw = p2;
        this.at = new java.util.BitSet();
        this.au = android.view.KeyCharacterMap.load(3);
        this.av = 1;
        return;
    }

    synthetic r(com.teamspeak.ts3client.f.p p1, byte p2)
    {
        this(p1);
        return;
    }

    static synthetic android.widget.CheckBox a(com.teamspeak.ts3client.f.r p1)
    {
        return p1.ay;
    }

    private void a(java.util.BitSet p6)
    {
        String v0_2 = "";
        int v1_1 = p6.nextSetBit(0);
        while (v1_1 >= 0) {
            String v2_1 = this.au.getDisplayLabel(v1_1);
            if (v2_1 != null) {
                v0_2 = new StringBuilder().append(v0_2).append(" ").append(v2_1).toString();
            } else {
                v0_2 = new StringBuilder().append(v0_2).append(" <").append(v1_1).append(">").toString();
            }
            v1_1 = p6.nextSetBit((v1_1 + 1));
        }
        this.ax.setText(v0_2);
        return;
    }

    static synthetic android.widget.TextView b(com.teamspeak.ts3client.f.r p1)
    {
        return p1.ax;
    }

    public final android.view.View a(android.view.LayoutInflater p12, android.view.ViewGroup p13)
    {
        android.app.Dialog v0_2 = ((com.teamspeak.ts3client.Ts3Application) p12.getContext().getApplicationContext());
        int v1_1 = v0_2.e.getString(com.teamspeak.ts3client.f.p.a(this.aw), "");
        String v3_3 = v0_2.e.getBoolean(new StringBuilder().append(com.teamspeak.ts3client.f.p.a(this.aw)).append("_intercept").toString(), 0);
        if (!v1_1.isEmpty()) {
            android.widget.LinearLayout$LayoutParams v4_8 = v1_1.split(",");
            android.widget.RelativeLayout$LayoutParams v5_3 = v4_8.length;
            int v1_2 = 0;
            while (v1_2 < v5_3) {
                try {
                    this.at.set(Integer.parseInt(v4_8[v1_2]));
                } catch (Exception v6) {
                }
                v1_2++;
            }
        }
        int v1_4 = new android.widget.RelativeLayout(p12.getContext());
        v1_4.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        android.widget.LinearLayout$LayoutParams v4_13 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v4_13.addRule(10);
        android.widget.RelativeLayout$LayoutParams v5_6 = new android.widget.TextView(this.h());
        v5_6.setId(3);
        v5_6.setText(com.teamspeak.ts3client.f.p.b(this.aw));
        v1_4.addView(v5_6, v4_13);
        android.widget.LinearLayout$LayoutParams v4_15 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v4_15.addRule(3, v5_6.getId());
        this.ax = new android.widget.TextView(p12.getContext());
        this.ax.setId(4);
        this.ax.setContentDescription("Key");
        this.ax.setTextColor(-65536);
        this.ax.setTypeface(0, 1);
        this.a(this.at);
        v1_4.addView(this.ax, v4_15);
        android.widget.LinearLayout$LayoutParams v4_17 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v4_17.addRule(3, this.ax.getId());
        this.ay = new android.widget.CheckBox(p12.getContext());
        this.ay.setText(com.teamspeak.ts3client.f.p.c(this.aw));
        this.ay.setId(5);
        this.ay.setChecked(v3_3);
        v1_4.addView(this.ay, v4_17);
        String v3_6 = new android.widget.LinearLayout(this.h());
        android.widget.LinearLayout$LayoutParams v4_20 = new android.widget.LinearLayout$LayoutParams(0, -2, 1056964608);
        android.widget.RelativeLayout$LayoutParams v5_25 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v5_25.addRule(3, this.ay.getId());
        Exception v6_15 = new android.widget.Button(p12.getContext());
        v6_15.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        v6_15.setOnClickListener(new com.teamspeak.ts3client.f.s(this, v0_2, v1_4));
        android.app.Dialog v0_4 = new android.widget.Button(p12.getContext());
        v0_4.setText(com.teamspeak.ts3client.data.e.a.a("button.clear"));
        v0_4.setOnClickListener(new com.teamspeak.ts3client.f.t(this));
        v0_4.setLayoutParams(v4_20);
        v6_15.setLayoutParams(v4_20);
        v3_6.addView(v0_4);
        v3_6.addView(v6_15);
        v1_4.addView(v3_6, v5_25);
        this.j.setTitle(com.teamspeak.ts3client.f.p.e(this.aw));
        this.a(0);
        com.teamspeak.ts3client.data.v.a().c = this;
        this.j.setOnKeyListener(new com.teamspeak.ts3client.f.u(this));
        return v1_4;
    }

    public final void a()
    {
        if (this.av) {
            this.at.clear();
            this.at.or(com.teamspeak.ts3client.data.v.a().a);
            this.a(this.at);
        }
        if (com.teamspeak.ts3client.data.v.a().a.isEmpty()) {
            this.av = 1;
        }
        return;
    }

    public final void p_()
    {
        this.av = 0;
        return;
    }
}
