package com.teamspeak.ts3client;
public class ConnectionBackground extends android.app.Service implements com.teamspeak.ts3client.data.w, com.teamspeak.ts3client.tsdns.i {
    private static android.app.Notification j;
    private static boolean k;
    private static android.support.v4.app.dm l;
    protected long a;
    protected boolean b;
    protected boolean c;
    protected int d;
    com.teamspeak.ts3client.Ts3Application e;
    android.content.SharedPreferences$OnSharedPreferenceChangeListener f;
    boolean g;
    boolean h;
    public android.content.BroadcastReceiver i;
    private boolean m;
    private int n;
    private boolean o;
    private boolean p;
    private java.util.ArrayList q;
    private boolean r;
    private android.view.WindowManager s;
    private android.widget.ImageView t;
    private android.view.WindowManager$LayoutParams u;
    private boolean v;
    private android.view.animation.Animation w;
    private android.content.IntentFilter x;

    public ConnectionBackground()
    {
        this.a = 0;
        this.g = 0;
        this.h = 1;
        this.m = 0;
        this.n = 0;
        this.o = 0;
        this.q = new java.util.ArrayList();
        this.r = 0;
        this.i = new com.teamspeak.ts3client.l(this);
        this.e = com.teamspeak.ts3client.Ts3Application.a();
        this.e.a.l = this;
        this.e.a.c.a(this);
        this.f = new com.teamspeak.ts3client.s(this.e);
        this.e.e.registerOnSharedPreferenceChangeListener(this.f);
        com.teamspeak.ts3client.ConnectionBackground.k = this.e.e.getBoolean("talk_notification", 0);
        return;
    }

    private int a(java.util.SortedMap p16, com.teamspeak.ts3client.data.a[] p17, long p18, int p20, int p21, java.util.concurrent.ConcurrentHashMap p22, Boolean p23)
    {
        com.teamspeak.ts3client.data.a v2_2 = ((java.util.SortedMap) p16.get(Long.valueOf(p18)));
        Boolean v7 = Boolean.valueOf(0);
        com.teamspeak.ts3client.data.a[] v13 = new com.teamspeak.ts3client.data.a[v2_2.size()];
        java.util.SortedMap v4_0 = 0;
        long v6_0 = 0;
        while (v6_0 < v2_2.size()) {
            int v3_21 = ((com.teamspeak.ts3client.data.a) v2_2.get(Long.valueOf(v4_0)));
            v13[v6_0] = v3_21;
            v6_0++;
            v4_0 = v3_21.b;
        }
        com.teamspeak.ts3client.data.a v2_4;
        if (!p23.booleanValue()) {
            v2_4 = v7;
        } else {
            v2_4 = Boolean.valueOf(1);
        }
        int v14 = v13.length;
        Boolean v11 = v2_4;
        int v12 = 0;
        while (v12 < v14) {
            com.teamspeak.ts3client.data.a v2_11;
            com.teamspeak.ts3client.data.a v2_6 = v13[v12];
            v2_6.q = 0;
            if (!p16.containsKey(Long.valueOf(v2_6.b))) {
                v2_6.f = p21;
                v2_6.p = v11.booleanValue();
                if (!v2_6.p) {
                    p17[p20] = v2_6;
                    if (p17[p20].j().size() == 0) {
                        p17[p20].q = 1;
                    }
                }
                v2_11 = (p20 + 1);
            } else {
                v2_6.f = p21;
                v2_6.p = v11.booleanValue();
                if (!v2_6.p) {
                    p17[p20] = v2_6;
                }
                if (!v2_6.o) {
                    v11 = Boolean.valueOf(1);
                }
                v2_11 = this.a(p16, p17, v2_6.b, (p20 + 1), (p21 + 1), p22, v11);
                if (!p23.booleanValue()) {
                    v11 = Boolean.valueOf(0);
                }
            }
            p20 = v2_11;
            v12++;
        }
        return p20;
    }

    public static android.app.Notification a()
    {
        return com.teamspeak.ts3client.ConnectionBackground.j;
    }

    static synthetic android.view.WindowManager$LayoutParams a(com.teamspeak.ts3client.ConnectionBackground p1)
    {
        return p1.u;
    }

    private void a(int p15)
    {
        if (this.e.a.i.containsKey(Integer.valueOf(p15))) {
            com.teamspeak.ts3client.a.p v0_9;
            this.b(p15).a = com.teamspeak.ts3client.data.d.x.a(this.e.a.a.b(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.b));
            com.teamspeak.ts3client.jni.h v3_6 = this.b(p15);
            if (this.e.a.a.c(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.f) != 0) {
                v0_9 = 1;
            } else {
                v0_9 = 0;
            }
            com.teamspeak.ts3client.a.p v0_14;
            v3_6.f = v0_9;
            com.teamspeak.ts3client.jni.h v3_7 = this.b(p15);
            if (this.e.a.a.c(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.g) != 0) {
                v0_14 = 1;
            } else {
                v0_14 = 0;
            }
            com.teamspeak.ts3client.a.p v0_19;
            v3_7.a(v0_14);
            com.teamspeak.ts3client.jni.h v3_8 = this.b(p15);
            if (this.e.a.a.c(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.j) != 1) {
                v0_19 = 1;
            } else {
                v0_19 = 0;
            }
            com.teamspeak.ts3client.a.p v0_24;
            v3_8.i = v0_19;
            com.teamspeak.ts3client.jni.h v3_9 = this.b(p15);
            if (this.e.a.a.c(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.i) != 1) {
                v0_24 = 1;
            } else {
                v0_24 = 0;
            }
            com.teamspeak.ts3client.a.p v0_32;
            v3_9.h = v0_24;
            this.b(p15).t = this.e.a.a.b(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.I);
            this.b(p15).u = this.e.a.a.b(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.H);
            this.b(p15).v = this.e.a.a.a(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.O);
            com.teamspeak.ts3client.jni.h v3_22 = this.b(p15);
            if (this.e.a.a.a(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.M) != 1) {
                v0_32 = 0;
            } else {
                v0_32 = 1;
            }
            com.teamspeak.ts3client.a.p v0_38;
            v3_22.j = v0_32;
            this.b(p15).m = this.e.a.a.b(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.N);
            com.teamspeak.ts3client.jni.h v3_27 = this.b(p15);
            if (this.e.a.a.a(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.af) != 1) {
                v0_38 = 0;
            } else {
                v0_38 = 1;
            }
            com.teamspeak.ts3client.a.p v0_44;
            v3_27.n = v0_38;
            this.b(p15).b(this.e.a.a.b(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.a));
            com.teamspeak.ts3client.jni.h v3_32 = this.b(p15);
            if (this.e.a.a.c(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.Z) != 0) {
                v0_44 = 1;
            } else {
                v0_44 = 0;
            }
            com.teamspeak.ts3client.jni.h v3_51;
            v3_32.k = v0_44;
            this.b(p15).o = this.e.a.a.a(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.Q);
            this.b(p15).p = this.e.a.a.b(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.R);
            this.b(p15).q = this.e.a.a.b(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.S);
            this.b(p15).a(this.e.a.a.c(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.ae));
            com.teamspeak.ts3client.a.o v4_57 = this.b(p15).s;
            if (this.e.a.a.c(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.r) != 0) {
                v3_51 = 1;
            } else {
                v3_51 = 0;
            }
            this.b(p15).s = v3_51;
            String v5_1 = this.e.a.c().a(((Long) this.e.a.i.get(Integer.valueOf(p15))));
            if ((v4_57 != null) && ((v3_51 == null) && (v5_1.b == this.e.a.g))) {
                String v6_21 = new Object[1];
                v6_21[0] = this.b(p15).a;
                com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.recording.stop", v6_21));
                this.e.g().a(com.teamspeak.ts3client.jni.h.av, new com.teamspeak.ts3client.a.o(this.b(p15).a, 0, v5_1.a, this.e.a.m()));
            }
            if ((v4_57 == null) && ((v3_51 != null) && (v5_1.b == this.e.a.g))) {
                com.teamspeak.ts3client.jni.h v3_52 = new Object[1];
                v3_52[0] = this.b(p15).a;
                com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.recording.started", v3_52));
                this.e.g().a(com.teamspeak.ts3client.jni.h.au, new com.teamspeak.ts3client.a.o(this.b(p15).a, 0, v5_1.a, this.e.a.m()));
            }
            this.b(p15).b();
            if (this.q.contains(Integer.valueOf(p15))) {
                this.b(p15).A = 1;
                this.b(p15).d = 0;
            }
            com.teamspeak.ts3client.a.p v0_86;
            com.teamspeak.ts3client.jni.h v3_55 = this.b(p15).r;
            com.teamspeak.ts3client.a.o v4_62 = this.b(p15);
            if (this.e.a.a.c(this.e.a.e, p15, com.teamspeak.ts3client.jni.d.U) != 0) {
                v0_86 = 1;
            } else {
                v0_86 = 0;
            }
            v4_62.r = v0_86;
            com.teamspeak.ts3client.a.p v0_88 = this.b(p15).r;
            com.teamspeak.ts3client.a.o v4_63 = v5_1.h;
            if ((p15 != this.e.a.h) && ((!this.b(p15).p.equals("0")) && ((v5_1.b == this.e.a.g) && (this.e.a.s.a(com.teamspeak.ts3client.jni.g.do))))) {
                this.e.g().a(com.teamspeak.ts3client.jni.h.ax, new com.teamspeak.ts3client.a.o(this.b(p15).a, 0, "", this.e.a.m()));
            }
            if ((this.e.a.h == p15) && ((v4_63 > null) && (v3_55 != v0_88))) {
                if (v0_88 == null) {
                    this.e.g().a(com.teamspeak.ts3client.jni.h.aE, new com.teamspeak.ts3client.a.o(this.b(p15).a, 0, "", this.e.a.m()));
                } else {
                    this.e.g().a(com.teamspeak.ts3client.jni.h.aD, new com.teamspeak.ts3client.a.o(this.b(p15).a, 0, "", this.e.a.m()));
                }
            }
            if (this.b(p15).v == 1) {
                this.b(p15).e = this.e.a.a.a(this.e.a.e, this.b(p15).c, com.teamspeak.ts3client.jni.d.ac);
            }
            this.e.a.c().a(((Long) this.e.a.i.get(Integer.valueOf(p15)))).d = 1;
        }
        return;
    }

    private void a(int p3, long p4, long p6)
    {
        this.e.a.c().a(Long.valueOf(p4)).e(p3);
        this.e.a.c().a(Long.valueOf(p6)).d(p3);
        return;
    }

    private void a(long p10)
    {
        java.util.Iterator v1_1 = this.e.a.c().a(Long.valueOf(p10)).j().iterator();
        while (v1_1.hasNext()) {
            String v0_9 = this.e.a.d.b(((Integer) v1_1.next()).intValue());
            if ((v0_9 != null) && (v0_9.s)) {
                com.teamspeak.ts3client.jni.h v3_1 = new Object[1];
                v3_1[0] = v0_9.a;
                com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.recording.isrecording", v3_1));
                this.e.g().a(com.teamspeak.ts3client.jni.h.aw, new com.teamspeak.ts3client.a.o(v0_9.a, 0, this.e.a.c().a(Long.valueOf(p10)).a, this.e.a.m()));
            }
        }
        return;
    }

    private void a(android.view.WindowManager$LayoutParams p4)
    {
        if (this.getResources().getConfiguration().orientation != 2) {
            if (this.getResources().getConfiguration().orientation != 1) {
                p4.x = this.e.e.getInt("overlay_last_x_u", 100);
                p4.y = this.e.e.getInt("overlay_last_y_u", 100);
            } else {
                p4.x = this.e.e.getInt("overlay_last_x_p", 100);
                p4.y = this.e.e.getInt("overlay_last_y_p", 100);
            }
        } else {
            p4.x = this.e.e.getInt("overlay_last_x_l", 100);
            p4.y = this.e.e.getInt("overlay_last_y_l", 100);
        }
        return;
    }

    static synthetic void a(com.teamspeak.ts3client.ConnectionBackground p0, android.view.WindowManager$LayoutParams p1)
    {
        p0.a(p1);
        return;
    }

    private void a(com.teamspeak.ts3client.data.a p3)
    {
        this.b(p3);
        return;
    }

    public static void a(boolean p6)
    {
        if (!p6) {
            com.teamspeak.ts3client.ConnectionBackground.l.a(2130837622);
            com.teamspeak.ts3client.ConnectionBackground.l.g = android.graphics.BitmapFactory.decodeResource(com.teamspeak.ts3client.Ts3Application.a().getResources(), 2130837622);
            com.teamspeak.ts3client.ConnectionBackground.j = com.teamspeak.ts3client.ConnectionBackground.l.c();
            com.teamspeak.ts3client.Ts3Application.a().l.notify(1, com.teamspeak.ts3client.ConnectionBackground.j);
        } else {
            if (com.teamspeak.ts3client.Ts3Application.a().a.d.b(com.teamspeak.ts3client.Ts3Application.a().a.h).d != 0) {
                com.teamspeak.ts3client.ConnectionBackground.l.a(2130837662);
                com.teamspeak.ts3client.ConnectionBackground.l.g = com.teamspeak.ts3client.data.d.t.a(2130837662, 1111490560, 1111490560);
                android.app.NotificationManager v0_14 = com.teamspeak.ts3client.ConnectionBackground.l.c();
                v0_14.ledARGB = -16776961;
                v0_14.flags = (v0_14.flags | 3);
                v0_14.ledOffMS = 0;
                v0_14.ledOnMS = 1;
                com.teamspeak.ts3client.ConnectionBackground.j = v0_14;
                com.teamspeak.ts3client.Ts3Application.a().l.notify(1, com.teamspeak.ts3client.ConnectionBackground.j);
            } else {
                com.teamspeak.ts3client.ConnectionBackground.l.a(2130837622);
                com.teamspeak.ts3client.ConnectionBackground.j = com.teamspeak.ts3client.ConnectionBackground.l.c();
                com.teamspeak.ts3client.Ts3Application.a().l.notify(1, com.teamspeak.ts3client.ConnectionBackground.j);
            }
        }
        return;
    }

    private boolean a(int p3, long p4)
    {
        return this.e.a.c().a(Long.valueOf(p4)).c.contains(Integer.valueOf(p3));
    }

    static synthetic boolean a(com.teamspeak.ts3client.ConnectionBackground p0, boolean p1)
    {
        p0.v = p1;
        return p1;
    }

    static synthetic android.widget.ImageView b(com.teamspeak.ts3client.ConnectionBackground p1)
    {
        return p1.t;
    }

    private com.teamspeak.ts3client.data.c b(int p2)
    {
        return this.e.a.d.b(p2);
    }

    private void b(long p4)
    {
        java.util.Iterator v1_1 = this.e.a.c().a(Long.valueOf(p4)).j().iterator();
        while (v1_1.hasNext()) {
            com.teamspeak.ts3client.data.c v0_9 = this.b(((Integer) v1_1.next()).intValue());
            if (v0_9 == null) {
                break;
            }
            v0_9.d = 0;
        }
        return;
    }

    private void b(com.teamspeak.ts3client.data.a p11)
    {
        int[] v2_1 = this.e.a.a.ts3client_getChannelClientList(this.e.a.e, p11.b);
        int v3 = v2_1.length;
        int v1_2 = 0;
        while (v1_2 < v3) {
            Integer v4_1 = v2_1[v1_2];
            if (!this.e.a.d.a(v4_1)) {
                com.teamspeak.ts3client.data.d v0_16;
                Long v5_4 = new com.teamspeak.ts3client.data.c(this.e.a.a.b(this.e.a.e, v4_1, com.teamspeak.ts3client.jni.d.b), v4_1, this.e);
                if (this.e.a.a.c(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.f) != 0) {
                    v0_16 = 1;
                } else {
                    v0_16 = 0;
                }
                com.teamspeak.ts3client.data.d v0_21;
                v5_4.f = v0_16;
                if (this.e.a.a.c(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.g) != 0) {
                    v0_21 = 1;
                } else {
                    v0_21 = 0;
                }
                com.teamspeak.ts3client.data.d v0_26;
                v5_4.a(v0_21);
                if (this.e.a.a.c(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.j) != 1) {
                    v0_26 = 1;
                } else {
                    v0_26 = 0;
                }
                com.teamspeak.ts3client.data.d v0_31;
                v5_4.i = v0_26;
                if (this.e.a.a.c(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.i) != 1) {
                    v0_31 = 1;
                } else {
                    v0_31 = 0;
                }
                com.teamspeak.ts3client.data.d v0_36;
                v5_4.h = v0_31;
                if (this.e.a.a.c(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.r) != 0) {
                    v0_36 = 1;
                } else {
                    v0_36 = 0;
                }
                v5_4.s = v0_36;
                if (p11.b != this.e.a.g) {
                    if (v5_4.c != this.e.a.h) {
                        if (!this.q.contains(Integer.valueOf(v5_4.c))) {
                            v5_4.A = 0;
                        } else {
                            v5_4.A = 1;
                        }
                    }
                } else {
                    if (v5_4.c != this.e.a.h) {
                        if (!this.q.contains(Integer.valueOf(v5_4.c))) {
                            v5_4.A = 0;
                        } else {
                            v5_4.A = 1;
                        }
                    }
                }
                v5_4.t = this.e.a.a.b(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.I);
                v5_4.u = this.e.a.a.b(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.H);
                v5_4.v = this.e.a.a.a(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.O);
                if (v5_4.v == 1) {
                    v5_4.e = this.e.a.a.a(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.ac);
                }
                com.teamspeak.ts3client.data.d v0_71;
                if (this.e.a.a.a(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.M) != 1) {
                    v0_71 = 0;
                } else {
                    v0_71 = 1;
                }
                com.teamspeak.ts3client.data.d v0_80;
                v5_4.j = v0_71;
                v5_4.m = this.e.a.a.b(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.N);
                if (this.e.a.a.a(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.af) != 1) {
                    v0_80 = 0;
                } else {
                    v0_80 = 1;
                }
                com.teamspeak.ts3client.data.d v0_89;
                v5_4.n = v0_80;
                v5_4.b(this.e.a.a.b(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.a));
                if (this.e.a.a.c(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.Z) != 0) {
                    v0_89 = 1;
                } else {
                    v0_89 = 0;
                }
                com.teamspeak.ts3client.data.d v0_106;
                v5_4.k = v0_89;
                v5_4.o = this.e.a.a.a(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.Q);
                v5_4.p = this.e.a.a.b(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.R);
                v5_4.q = this.e.a.a.b(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.S);
                if (this.e.a.a.c(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.U) != 0) {
                    v0_106 = 1;
                } else {
                    v0_106 = 0;
                }
                v5_4.r = v0_106;
                v5_4.a(this.e.a.a.c(this.e.a.e, v5_4.c, com.teamspeak.ts3client.jni.d.ae));
                this.e.a.d.a(v5_4);
                v5_4.a();
            }
            p11.d(v4_1);
            this.e.a.i.put(Integer.valueOf(v4_1), Long.valueOf(p11.b));
            v1_2++;
        }
        return;
    }

    static synthetic boolean b(boolean p0)
    {
        com.teamspeak.ts3client.ConnectionBackground.k = p0;
        return p0;
    }

    static synthetic android.view.WindowManager c(com.teamspeak.ts3client.ConnectionBackground p1)
    {
        return p1.s;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application d(com.teamspeak.ts3client.ConnectionBackground p1)
    {
        return p1.e;
    }

    static synthetic android.content.IntentFilter e(com.teamspeak.ts3client.ConnectionBackground p1)
    {
        return p1.x;
    }

    static synthetic boolean e()
    {
        return com.teamspeak.ts3client.ConnectionBackground.k;
    }

    private void f()
    {
        if (!this.e.e.getBoolean("audio_ptt", 0)) {
            if ((!this.e.a.s.a(com.teamspeak.ts3client.jni.g.cw)) || (this.r)) {
                if (this.r) {
                    this.e.a.b.d();
                    this.e.a.a.a(this.e.a.e, com.teamspeak.ts3client.jni.d.k, 0);
                    this.e.a.a.ts3client_setPreProcessorConfigValue(this.e.a.e, "voiceactivation_level", String.valueOf(this.e.e.getInt("voiceactivation_level", 10)));
                    this.e.a.k.i(0);
                    this.e.a.k.C();
                    this.e.a.k.a(com.teamspeak.ts3client.data.e.a.a("messages.forceptt.off"), com.teamspeak.ts3client.data.e.a.a("messages.forceptt.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                    this.d();
                    this.r = 0;
                }
            } else {
                this.e.a.k.H();
                this.e.a.a.a(this.e.a.e, com.teamspeak.ts3client.jni.d.k, 1);
                this.e.a.a.ts3client_setPreProcessorConfigValue(this.e.a.e, "voiceactivation_level", "-50");
                this.e.a.k.i(1);
                com.teamspeak.ts3client.t v0_42 = this.e.a.k;
                v0_42.i().runOnUiThread(new com.teamspeak.ts3client.ai(v0_42));
                this.e.a.k.a(com.teamspeak.ts3client.data.e.a.a("messages.forceptt.on"), com.teamspeak.ts3client.data.e.a.a("messages.forceptt.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                this.r = 1;
                this.c();
            }
        } else {
            this.r = 0;
        }
        return;
    }

    static synthetic void f(com.teamspeak.ts3client.ConnectionBackground p0)
    {
        p0.f();
        return;
    }

    private void g()
    {
        long[] v16 = this.e.a.a.ts3client_getChannelList(this.e.a.e);
        int v17 = v16.length;
        int v15 = 0;
        while (v15 < v17) {
            int v2_24;
            long v6 = v16[v15];
            int v2_8 = this.e.a.a.b(this.e.a.e, v6, com.teamspeak.ts3client.jni.c.a);
            long v12_0 = this.e.a.a.ts3client_getParentChannelOfChannel(this.e.a.e, v6);
            long v10_0 = this.e.a.a.c(this.e.a.e, v6, com.teamspeak.ts3client.jni.c.i);
            int v18 = this.e.a.a.a(this.e.a.e, v6, com.teamspeak.ts3client.jni.c.m);
            this.e.a.a.c(this.e.a.e, v6, com.teamspeak.ts3client.jni.c.i);
            com.teamspeak.ts3client.data.a v4_15 = new com.teamspeak.ts3client.data.a(v2_8, v6, v12_0, v10_0);
            v4_15.h = this.e.a.a.a(this.e.a.e, v6, com.teamspeak.ts3client.jni.c.C);
            v4_15.l = this.e.a.a.a(this.e.a.e, v6, com.teamspeak.ts3client.jni.c.e);
            if (this.e.a.a.a(this.e.a.e, v6, com.teamspeak.ts3client.jni.c.l) != 1) {
                v2_24 = 0;
            } else {
                v2_24 = 1;
            }
            v4_15.r = v2_24;
            if (v4_15.r) {
                this.e.a.c().b = v4_15;
            }
            int v2_39;
            v4_15.i = this.e.a.a.a(this.e.a.e, v6, com.teamspeak.ts3client.jni.c.g);
            if (this.e.a.a.a(this.e.a.e, v6, com.teamspeak.ts3client.jni.c.G) != 1) {
                v2_39 = 0;
            } else {
                v2_39 = 1;
            }
            v4_15.j = v2_39;
            v4_15.c(this.e.a.a.c(this.e.a.e, v6, com.teamspeak.ts3client.jni.c.F));
            if (v18 == 1) {
                v4_15.m = 1;
            }
            this.e.a.c().a(v4_15);
            v15++;
        }
        return;
    }

    private boolean h()
    {
        return this.g;
    }

    private boolean i()
    {
        return this.o;
    }

    private void j()
    {
        com.teamspeak.ts3client.data.ab v0_3 = this.e.a.a.ts3client_getClientID(this.e.a.e);
        Long v2_4 = this.e.a.a.c(this.e.a.e, v0_3, com.teamspeak.ts3client.jni.d.G);
        long v4_4 = this.e.a.a.ts3client_getChannelOfClient(this.e.a.e, v0_3);
        this.e.a.h = v0_3;
        this.e.a.z = v2_4;
        this.e.a.g = v4_4;
        this.e.a.c().a(Long.valueOf(v4_4)).k = 1;
        com.teamspeak.ts3client.data.ab v0_13 = new android.content.Intent(this.e, com.teamspeak.ts3client.StartGUIFragment);
        v0_13.setAction("android.intent.action.MAIN");
        v0_13.addCategory("android.intent.category.LAUNCHER");
        com.teamspeak.ts3client.data.ab v0_14 = android.app.PendingIntent.getActivity(this.e, 0, v0_13, 0);
        String v1_16 = new android.support.v4.app.dm(this.e.getApplicationContext());
        com.teamspeak.ts3client.ConnectionBackground.l = v1_16;
        String v1_19 = v1_16.a(2130837622).c(com.teamspeak.ts3client.data.e.a.a("messages.notification.info")).a(System.currentTimeMillis());
        v1_19.a(2, 1);
        v1_19.a().a(com.teamspeak.ts3client.data.e.a.a("messages.notification.text")).b(this.e.a.a.b(this.e.a.e, com.teamspeak.ts3client.jni.i.b)).d = v0_14;
        this.startForeground(1, com.teamspeak.ts3client.ConnectionBackground.l.c());
        this.e.a.v.q = v4_4;
        if (!com.teamspeak.ts3client.Ts3Application.a().a.j.containsKey(Long.valueOf(v4_4))) {
            this.e.a.v.r = "";
        } else {
            this.e.a.v.r = ((String) com.teamspeak.ts3client.Ts3Application.a().a.j.get(Long.valueOf(v4_4)));
        }
        return;
    }

    private void k()
    {
        this.b();
        return;
    }

    private void l()
    {
        this.h = 1;
        return;
    }

    private void m()
    {
        this.e.a.c.b(this);
        this.e.e.unregisterOnSharedPreferenceChangeListener(this.f);
        return;
    }

    private void n()
    {
        java.util.Iterator v1 = this.e.a.c().a.values().iterator();
        while (v1.hasNext()) {
            this.b(((com.teamspeak.ts3client.data.a) v1.next()));
        }
        this.b();
        return;
    }

    private boolean o()
    {
        return this.m;
    }

    public final void a(com.teamspeak.ts3client.jni.k p13)
    {
        if ((p13 instanceof com.teamspeak.ts3client.jni.events.ServerStop)) {
            this.e.g().a(com.teamspeak.ts3client.jni.h.b, 0);
            com.teamspeak.ts3client.jni.Ts3Jni v1_3 = new android.content.Intent(this.e, com.teamspeak.ts3client.StartGUIFragment);
            v1_3.setAction("android.intent.action.MAIN");
            v1_3.addCategory("android.intent.category.LAUNCHER");
            com.teamspeak.ts3client.jni.Ts3Jni v1_4 = android.app.PendingIntent.getActivity(this.e, 0, v1_3, 0);
            long v2_6 = new android.support.v4.app.dm(this.e.getApplicationContext());
            v2_6.a(2130837622).c(com.teamspeak.ts3client.data.e.a.a("messages.servershutdown.text")).a(System.currentTimeMillis()).a().a(com.teamspeak.ts3client.data.e.a.a("messages.servershutdown.text")).b(((com.teamspeak.ts3client.jni.events.ServerStop) p13).a()).a(v1_4);
            int v0_5 = v2_6.c();
            v0_5.flags = (v0_5.flags | 20);
            this.e.j().notify(0, 102, v0_5);
            this.e.c().e().d();
            int v0_11 = this.e.c().e();
            this.e.c().p();
            v0_11.E();
            this.m = 1;
        }
        if (!(p13 instanceof com.teamspeak.ts3client.jni.events.rare.ServerPermissionError)) {
            if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ServerEdited)) {
                if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ServerError)) {
                    if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ConnectStatusChange)) {
                        if (!(p13 instanceof com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer)) {
                            if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ChannelSubscribeFinished)) {
                                if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ChannelSubscribe)) {
                                    if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ChannelUnsubscribe)) {
                                        if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ChannelUnsubscribeFinished)) {
                                            if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ChannelMove)) {
                                                if (!(p13 instanceof com.teamspeak.ts3client.jni.events.UpdateChannelEdited)) {
                                                    if (!(p13 instanceof com.teamspeak.ts3client.jni.events.DelChannel)) {
                                                        if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ClientMoveMoved)) {
                                                            if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ClientMove)) {
                                                                if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ClientMoveTimeout)) {
                                                                    if (!(p13 instanceof com.teamspeak.ts3client.jni.events.TalkStatusChange)) {
                                                                        if (!(p13 instanceof com.teamspeak.ts3client.jni.events.NewChannelCreated)) {
                                                                            if (!(p13 instanceof com.teamspeak.ts3client.jni.events.UpdateClient)) {
                                                                                if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ClientKickFromChannel)) {
                                                                                    if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ClientKickFromServer)) {
                                                                                        if (!(p13 instanceof com.teamspeak.ts3client.jni.events.rare.ServerGroupList)) {
                                                                                            if (!(p13 instanceof com.teamspeak.ts3client.jni.events.rare.ChannelGroupList)) {
                                                                                                if (!(p13 instanceof com.teamspeak.ts3client.jni.events.rare.ClientChannelGroupChanged)) {
                                                                                                    if (!(p13 instanceof com.teamspeak.ts3client.jni.events.rare.ClientPoke)) {
                                                                                                        if (!(p13 instanceof com.teamspeak.ts3client.jni.events.TextMessage)) {
                                                                                                            if (!(p13 instanceof com.teamspeak.ts3client.jni.events.rare.ClientChatClosed)) {
                                                                                                                if (!(p13 instanceof com.teamspeak.ts3client.jni.events.rare.ClientNeededPermissions)) {
                                                                                                                    if (!(p13 instanceof com.teamspeak.ts3client.jni.events.rare.ServerGroupClientAdded)) {
                                                                                                                        if (!(p13 instanceof com.teamspeak.ts3client.jni.events.rare.ServerGroupClientDeleted)) {
                                                                                                                            if (!(p13 instanceof com.teamspeak.ts3client.jni.events.ChannelPasswordChanged)) {
                                                                                                                                if ((p13 instanceof com.teamspeak.ts3client.jni.events.IgnoredWhisper)) {
                                                                                                                                    if ((this.e.f().getInt("whisper", 0) == 2) || (this.e.c().a().containsKey(Integer.valueOf(((com.teamspeak.ts3client.jni.events.IgnoredWhisper) p13).a())))) {
                                                                                                                                        return;
                                                                                                                                    } else {
                                                                                                                                        this.e.c().h().ts3client_requestClientUIDfromClientID(((com.teamspeak.ts3client.jni.events.IgnoredWhisper) p13).b(), ((com.teamspeak.ts3client.jni.events.IgnoredWhisper) p13).a(), new StringBuilder("IgnoredWhisper: ").append(((com.teamspeak.ts3client.jni.events.IgnoredWhisper) p13).a()).toString());
                                                                                                                                        if (!this.e.c().a().containsKey(Integer.valueOf(((com.teamspeak.ts3client.jni.events.IgnoredWhisper) p13).a()))) {
                                                                                                                                            this.e.c().a().put(Integer.valueOf(((com.teamspeak.ts3client.jni.events.IgnoredWhisper) p13).a()), "");
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                if ((p13 instanceof com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID)) {
                                                                                                                                    if (this.e.f().getInt("whisper", 0) != 0) {
                                                                                                                                        if ((this.e.f().getInt("whisper", 0) == 1) && (this.e.c().a().containsKey(Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).b())))) {
                                                                                                                                            this.e.c().a().remove(Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).b()));
                                                                                                                                            this.e.c().a().put(Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).b()), ((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).c());
                                                                                                                                            com.teamspeak.ts3client.jni.Ts3Jni v1_50 = com.teamspeak.ts3client.data.b.c.a().a(((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).c());
                                                                                                                                            if (v1_50 == null) {
                                                                                                                                                return;
                                                                                                                                            } else {
                                                                                                                                                if ((v1_50 != null) && (v1_50.e())) {
                                                                                                                                                    this.e.c().h().ts3client_allowWhispersFrom(((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).a(), ((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).b());
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    } else {
                                                                                                                                        if (this.e.c().a().containsKey(Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).b()))) {
                                                                                                                                            this.e.c().a().remove(Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).b()));
                                                                                                                                            this.e.c().a().b().remove(((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).c());
                                                                                                                                            this.e.c().a().put(Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).b()), ((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).c());
                                                                                                                                            com.teamspeak.ts3client.jni.Ts3Jni v1_70 = com.teamspeak.ts3client.data.b.c.a().a(((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).c());
                                                                                                                                            if ((v1_70 == null) || ((v1_70 != null) && (v1_70.e()))) {
                                                                                                                                                this.e.c().h().ts3client_allowWhispersFrom(((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).a(), ((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p13).b());
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                if ((p13 instanceof com.teamspeak.ts3client.jni.events.special.TSDNSResolv)) {
                                                                                                                                    if (((com.teamspeak.ts3client.jni.events.special.TSDNSResolv) p13).a() != 0) {
                                                                                                                                        this.e.c().e().a();
                                                                                                                                        this.e.c().e().a(com.teamspeak.ts3client.data.e.a.a("tsdns.error.text"), com.teamspeak.ts3client.data.e.a.a("tsdns.error"), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.exit"));
                                                                                                                                    } else {
                                                                                                                                        String v7_0 = this.e.c().p();
                                                                                                                                        v7_0.c(((com.teamspeak.ts3client.jni.events.special.TSDNSResolv) p13).b());
                                                                                                                                        v7_0.a(((com.teamspeak.ts3client.jni.events.special.TSDNSResolv) p13).c());
                                                                                                                                        new com.teamspeak.ts3client.data.d.h(this.e, ((com.teamspeak.ts3client.jni.events.special.TSDNSResolv) p13).b());
                                                                                                                                        if (this.e.c().p().i() <= 0) {
                                                                                                                                            this.e.c().a(((com.teamspeak.ts3client.jni.events.special.TSDNSResolv) p13).b(), ((com.teamspeak.ts3client.jni.events.special.TSDNSResolv) p13).c(), v7_0.c(), v7_0.a(), v7_0.b(), v7_0.d(), v7_0.f());
                                                                                                                                        } else {
                                                                                                                                            this.e.c().a(((com.teamspeak.ts3client.jni.events.special.TSDNSResolv) p13).b(), ((com.teamspeak.ts3client.jni.events.special.TSDNSResolv) p13).c(), v7_0.c(), new StringBuilder("/").append(this.e.c().p().k()).toString(), this.e.c().p().l(), v7_0.d(), v7_0.f());
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            } else {
                                                                                                                                com.teamspeak.ts3client.Ts3Application.a().c().o().remove(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelPasswordChanged) p13).a()));
                                                                                                                                this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelPasswordChanged) p13).a())).e(0);
                                                                                                                            }
                                                                                                                        } else {
                                                                                                                            int v0_88 = this.b(((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientDeleted) p13).a());
                                                                                                                            if (v0_88 != 0) {
                                                                                                                                v0_88.c(this.e.c().h().b(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientDeleted) p13).a(), com.teamspeak.ts3client.jni.d.I));
                                                                                                                                com.teamspeak.ts3client.jni.Ts3Jni v1_88 = new Object[3];
                                                                                                                                v1_88[0] = ((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientDeleted) p13).b();
                                                                                                                                v1_88[1] = this.e.c().l().b(((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientDeleted) p13).c()).a();
                                                                                                                                v1_88[2] = ((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientDeleted) p13).d();
                                                                                                                                com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.group.server.deleted", v1_88));
                                                                                                                                if (((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientDeleted) p13).a() == this.e.c().i()) {
                                                                                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.aI, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientDeleted) p13).b(), 0, this.e.c().l().b(((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientDeleted) p13).c()).a(), this.e.c().m()));
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    } else {
                                                                                                                        int v0_95 = this.b(((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientAdded) p13).a());
                                                                                                                        if (v0_95 != 0) {
                                                                                                                            v0_95.c(this.e.c().h().b(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientAdded) p13).a(), com.teamspeak.ts3client.jni.d.I));
                                                                                                                            com.teamspeak.ts3client.jni.Ts3Jni v1_98 = new Object[3];
                                                                                                                            v1_98[0] = ((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientAdded) p13).b();
                                                                                                                            v1_98[1] = this.e.c().l().b(((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientAdded) p13).c()).a();
                                                                                                                            v1_98[2] = ((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientAdded) p13).d();
                                                                                                                            com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.group.server.assigned", v1_98));
                                                                                                                            if (((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientAdded) p13).a() == this.e.c().i()) {
                                                                                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.aG, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientAdded) p13).b(), 0, this.e.c().l().b(((com.teamspeak.ts3client.jni.events.rare.ServerGroupClientAdded) p13).c()).a(), this.e.c().m()));
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                } else {
                                                                                                                    try {
                                                                                                                        if (((com.teamspeak.ts3client.jni.events.rare.ClientNeededPermissions) p13).a() == this.e.c().j().a(com.teamspeak.ts3client.jni.g.cw)) {
                                                                                                                            this.f();
                                                                                                                        }
                                                                                                                    } catch (int v0_102) {
                                                                                                                        this.e.e().warning(v0_102.toString());
                                                                                                                    }
                                                                                                                    if (this.e.c() != null) {
                                                                                                                        this.e.c().e().D();
                                                                                                                    }
                                                                                                                }
                                                                                                            } else {
                                                                                                                int v0_110 = new com.teamspeak.ts3client.chat.y("", "", com.teamspeak.ts3client.data.e.a.a("event.chat.closed"), Boolean.valueOf(0), Boolean.valueOf(1));
                                                                                                                if (com.teamspeak.ts3client.chat.d.a().c(((com.teamspeak.ts3client.jni.events.rare.ClientChatClosed) p13).a()).booleanValue()) {
                                                                                                                    com.teamspeak.ts3client.chat.d.a().d(((com.teamspeak.ts3client.jni.events.rare.ClientChatClosed) p13).a()).a(v0_110);
                                                                                                                }
                                                                                                            }
                                                                                                        } else {
                                                                                                            int v0_112 = com.teamspeak.ts3client.data.b.c.a().a(((com.teamspeak.ts3client.jni.events.TextMessage) p13).c());
                                                                                                            if ((v0_112 == 0) || ((!v0_112.c()) && (!v0_112.d()))) {
                                                                                                                int v0_114 = 0;
                                                                                                                if (((com.teamspeak.ts3client.jni.events.TextMessage) p13).a() == this.e.c().i()) {
                                                                                                                    v0_114 = 1;
                                                                                                                }
                                                                                                                if (!((com.teamspeak.ts3client.jni.events.TextMessage) p13).d().equals("")) {
                                                                                                                    com.teamspeak.ts3client.jni.Ts3Jni v1_121 = new com.teamspeak.ts3client.chat.y(((com.teamspeak.ts3client.jni.events.TextMessage) p13).b(), ((com.teamspeak.ts3client.jni.events.TextMessage) p13).c(), ((com.teamspeak.ts3client.jni.events.TextMessage) p13).d(), Boolean.valueOf(v0_114));
                                                                                                                    switch (((com.teamspeak.ts3client.jni.events.TextMessage) p13).e()) {
                                                                                                                        case 1:
                                                                                                                            if (v0_114 != 0) {
                                                                                                                            } else {
                                                                                                                                if (!com.teamspeak.ts3client.chat.d.a().c(((com.teamspeak.ts3client.jni.events.TextMessage) p13).c()).booleanValue()) {
                                                                                                                                    if (!this.e.c().q().a(((com.teamspeak.ts3client.jni.events.TextMessage) p13).a())) {
                                                                                                                                        int v0_133 = new com.teamspeak.ts3client.data.c(((com.teamspeak.ts3client.jni.events.TextMessage) p13).b(), ((com.teamspeak.ts3client.jni.events.TextMessage) p13).a(), this.e, 1);
                                                                                                                                        v0_133.g();
                                                                                                                                        long v2_92 = new com.teamspeak.ts3client.chat.a(((com.teamspeak.ts3client.jni.events.TextMessage) p13).b(), ((com.teamspeak.ts3client.jni.events.TextMessage) p13).c(), v0_133, 1);
                                                                                                                                        v2_92.a(v1_121);
                                                                                                                                        com.teamspeak.ts3client.chat.d.a().a(v2_92);
                                                                                                                                    } else {
                                                                                                                                        long v2_94 = new com.teamspeak.ts3client.chat.a(((com.teamspeak.ts3client.jni.events.TextMessage) p13).b(), ((com.teamspeak.ts3client.jni.events.TextMessage) p13).c(), this.b(((com.teamspeak.ts3client.jni.events.TextMessage) p13).a()));
                                                                                                                                        v2_94.a(v1_121);
                                                                                                                                        com.teamspeak.ts3client.chat.d.a().a(v2_94);
                                                                                                                                    }
                                                                                                                                    if (!this.e.f().getBoolean("vibrate_chatnew", 0)) {
                                                                                                                                        if (this.e.f().getBoolean("vibrate_chat", 0)) {
                                                                                                                                            com.teamspeak.ts3client.data.ai.a();
                                                                                                                                            com.teamspeak.ts3client.data.ai.a(com.teamspeak.ts3client.data.aj.c);
                                                                                                                                        }
                                                                                                                                    } else {
                                                                                                                                        com.teamspeak.ts3client.data.ai.a();
                                                                                                                                        com.teamspeak.ts3client.data.ai.a(com.teamspeak.ts3client.data.aj.b);
                                                                                                                                    }
                                                                                                                                } else {
                                                                                                                                    com.teamspeak.ts3client.chat.d.a().d(((com.teamspeak.ts3client.jni.events.TextMessage) p13).c()).a(v1_121);
                                                                                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.aM, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.TextMessage) p13).b(), 0, "", this.e.c().m()));
                                                                                                                                    if (this.e.f().getBoolean("vibrate_chat", 0)) {
                                                                                                                                        com.teamspeak.ts3client.data.ai.a();
                                                                                                                                        com.teamspeak.ts3client.data.ai.a(com.teamspeak.ts3client.data.aj.c);
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.aM, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.TextMessage) p13).b(), 0, "", this.e.c().m()));
                                                                                                                            }
                                                                                                                        case 2:
                                                                                                                            com.teamspeak.ts3client.chat.d.a().b().a(v1_121);
                                                                                                                            if (((com.teamspeak.ts3client.jni.events.TextMessage) p13).a() == this.e.c().i()) {
                                                                                                                            } else {
                                                                                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.aO, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.TextMessage) p13).b(), 0, "", this.e.c().m()));
                                                                                                                            }
                                                                                                                            break;
                                                                                                                        case 3:
                                                                                                                            com.teamspeak.ts3client.chat.d.a().c().a(v1_121);
                                                                                                                            if (((com.teamspeak.ts3client.jni.events.TextMessage) p13).a() == this.e.c().i()) {
                                                                                                                            } else {
                                                                                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.aQ, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.TextMessage) p13).b(), 0, "", this.e.c().m()));
                                                                                                                            }
                                                                                                                            break;
                                                                                                                    }
                                                                                                                    if (!v1_121.a()) {
                                                                                                                        this.e.c().e().e(1);
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    } else {
                                                                                                        int v0_161 = com.teamspeak.ts3client.data.b.c.a().a(((com.teamspeak.ts3client.jni.events.rare.ClientPoke) p13).c());
                                                                                                        if ((v0_161 == 0) || (!v0_161.b())) {
                                                                                                            int v0_165 = this.e.c().e();
                                                                                                            com.teamspeak.ts3client.jni.Ts3Jni v1_124 = ((com.teamspeak.ts3client.jni.events.rare.ClientPoke) p13).a();
                                                                                                            String v3_61 = new Object[1];
                                                                                                            v3_61[0] = ((com.teamspeak.ts3client.jni.events.rare.ClientPoke) p13).b();
                                                                                                            v0_165.a(v1_124, com.teamspeak.ts3client.data.e.a.a("messages.poke.info", v3_61), Boolean.valueOf(0), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.close"));
                                                                                                            this.e.g().a(com.teamspeak.ts3client.jni.h.d, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.rare.ClientPoke) p13).b(), 0, "", this.e.c().m()));
                                                                                                            if (this.e.f().getBoolean("vibrate_poke", 0)) {
                                                                                                                com.teamspeak.ts3client.data.ai.a();
                                                                                                                com.teamspeak.ts3client.data.ai.a(com.teamspeak.ts3client.data.aj.a);
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                } else {
                                                                                                    int v0_173 = this.b(((com.teamspeak.ts3client.jni.events.rare.ClientChannelGroupChanged) p13).b());
                                                                                                    if (v0_173 != 0) {
                                                                                                        v0_173.a(this.e.c().h().b(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.rare.ClientChannelGroupChanged) p13).b(), com.teamspeak.ts3client.jni.d.H));
                                                                                                        com.teamspeak.ts3client.jni.Ts3Jni v1_132 = new Object[3];
                                                                                                        v1_132[0] = this.e.c().b().b(((com.teamspeak.ts3client.jni.events.rare.ClientChannelGroupChanged) p13).a()).a();
                                                                                                        v1_132[1] = this.b(((com.teamspeak.ts3client.jni.events.rare.ClientChannelGroupChanged) p13).b()).d();
                                                                                                        v1_132[2] = ((com.teamspeak.ts3client.jni.events.rare.ClientChannelGroupChanged) p13).d();
                                                                                                        com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.group.channel.assigned", v1_132));
                                                                                                        if ((((com.teamspeak.ts3client.jni.events.rare.ClientChannelGroupChanged) p13).b() == this.e.c().i()) && (((com.teamspeak.ts3client.jni.events.rare.ClientChannelGroupChanged) p13).c() != 0)) {
                                                                                                            this.e.g().a(com.teamspeak.ts3client.jni.h.aK, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.rare.ClientChannelGroupChanged) p13).d(), 0, this.e.c().b().b(((com.teamspeak.ts3client.jni.events.rare.ClientChannelGroupChanged) p13).a()).a(), this.e.c().m()));
                                                                                                        }
                                                                                                        this.e.c().e().y();
                                                                                                    }
                                                                                                }
                                                                                            } else {
                                                                                                this.e.c().b().a(((com.teamspeak.ts3client.jni.events.rare.ChannelGroupList) p13).a());
                                                                                                this.e.c().g().a(((com.teamspeak.ts3client.jni.events.rare.ChannelGroupList) p13).b());
                                                                                            }
                                                                                        } else {
                                                                                            this.e.c().l().a(((com.teamspeak.ts3client.jni.events.rare.ServerGroupList) p13).a());
                                                                                            this.e.c().g().a(((com.teamspeak.ts3client.jni.events.rare.ServerGroupList) p13).b());
                                                                                        }
                                                                                    } else {
                                                                                        com.teamspeak.ts3client.data.c v9_0 = this.b(((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).a());
                                                                                        if (v9_0 != null) {
                                                                                            long v8_1;
                                                                                            if (v9_0.e() == null) {
                                                                                                v8_1 = 0;
                                                                                            } else {
                                                                                                v8_1 = v9_0.e().a();
                                                                                            }
                                                                                            if (((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).a() != this.e.c().i()) {
                                                                                                if (((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).d() == this.e.c().f()) {
                                                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.al, new com.teamspeak.ts3client.a.o(v9_0.d(), v8_1, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).d())).e(), this.e.c().m()));
                                                                                                }
                                                                                                if (((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).d() != this.e.c().f()) {
                                                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.ak, new com.teamspeak.ts3client.a.o(v9_0.d(), v8_1, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).d())).e(), this.e.c().m()));
                                                                                                }
                                                                                            } else {
                                                                                                this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).c(), com.teamspeak.ts3client.data.e.a.a("messages.kicked.info"), Boolean.valueOf(0), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.exit"), com.teamspeak.ts3client.jni.h.aA);
                                                                                                this.m = 1;
                                                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.aA, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).b(), v8_1, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).d())).e(), this.e.c().m()));
                                                                                            }
                                                                                            com.teamspeak.ts3client.jni.Ts3Jni v1_149 = new Object[2];
                                                                                            v1_149[0] = this.b(((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).a()).d();
                                                                                            v1_149[1] = ((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).b();
                                                                                            com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.kick", v1_149));
                                                                                            this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).d())).e(v9_0.f());
                                                                                            this.e.c().d().remove(Integer.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromServer) p13).a()));
                                                                                            this.e.c().e().y();
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    if ((com.teamspeak.ts3client.jni.j.a != ((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).f()) && (com.teamspeak.ts3client.jni.j.b != ((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).f())) {
                                                                                        int v0_228 = this.b(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).a());
                                                                                        if (v0_228 == 0) {
                                                                                            return;
                                                                                        }
                                                                                    } else {
                                                                                        v0_228 = new com.teamspeak.ts3client.data.c(this.e.c().h().b(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).a(), com.teamspeak.ts3client.jni.d.b), ((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).a(), this.e);
                                                                                        this.e.c().q().a(v0_228);
                                                                                    }
                                                                                    this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).e())).e(v0_228.f());
                                                                                    this.e.c().d().remove(Integer.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).a()));
                                                                                    if (((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).d() != 0) {
                                                                                        if ((com.teamspeak.ts3client.jni.j.a != ((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).f()) && (com.teamspeak.ts3client.jni.j.b != ((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).f())) {
                                                                                            this.e.c().q().c(v0_228.f());
                                                                                        } else {
                                                                                            this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).d())).d(v0_228.f());
                                                                                            this.e.c().d().put(Integer.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).a()), Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).d()));
                                                                                        }
                                                                                    }
                                                                                    com.teamspeak.ts3client.jni.Ts3Jni v1_183 = 0;
                                                                                    if (v0_228.e() != null) {
                                                                                        v1_183 = v0_228.e().a();
                                                                                    }
                                                                                    if (((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).a() != this.e.c().i()) {
                                                                                        if (((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).e() != this.e.c().f()) {
                                                                                            if (((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).d() == this.e.c().f()) {
                                                                                                if (((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).f() == com.teamspeak.ts3client.jni.j.a) {
                                                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.ad, new com.teamspeak.ts3client.a.o(v0_228.d(), v1_183, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).d())).e(), this.e.c().m()));
                                                                                                }
                                                                                                if (((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).f() == com.teamspeak.ts3client.jni.j.b) {
                                                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.ae, new com.teamspeak.ts3client.a.o(v0_228.d(), v1_183, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).d())).e(), this.e.c().m()));
                                                                                                }
                                                                                            }
                                                                                        } else {
                                                                                            if (((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).f() == com.teamspeak.ts3client.jni.j.c) {
                                                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.af, new com.teamspeak.ts3client.a.o(v0_228.d(), v1_183, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).d())).e(), this.e.c().m()));
                                                                                            }
                                                                                            if (((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).f() == com.teamspeak.ts3client.jni.j.b) {
                                                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.ag, new com.teamspeak.ts3client.a.o(v0_228.d(), v1_183, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).d())).e(), this.e.c().m()));
                                                                                            }
                                                                                        }
                                                                                        if (!((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).c().equals("")) {
                                                                                            long v2_169 = new Object[4];
                                                                                            v2_169[0] = v0_228.d();
                                                                                            v2_169[1] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).e())).e();
                                                                                            v2_169[2] = ((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).b();
                                                                                            v2_169[3] = ((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).c();
                                                                                            com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.kicked.reason", v2_169));
                                                                                        } else {
                                                                                            long v2_171 = new Object[3];
                                                                                            v2_171[0] = v0_228.d();
                                                                                            v2_171[1] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).e())).e();
                                                                                            v2_171[2] = ((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).b();
                                                                                            com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.kicked", v2_171));
                                                                                        }
                                                                                    } else {
                                                                                        long v2_172 = this.e.c().f();
                                                                                        if ((!this.e.c().p().b(Long.valueOf(v2_172))) && (!this.e.c().p().g())) {
                                                                                            int v0_254 = this.e.c().h();
                                                                                            String v4_80 = this.e.c().k();
                                                                                            String v6_55 = new long[2];
                                                                                            v6_55[0] = v2_172;
                                                                                            v6_55[1] = 0;
                                                                                            v0_254.ts3client_requestChannelUnsubscribe(v4_80, v6_55, "Unsubscribe old Channel");
                                                                                        }
                                                                                        this.e.c().a(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).d());
                                                                                        try {
                                                                                            com.teamspeak.ts3client.chat.d.a(this.e.getBaseContext()).b().a(this.e.c().c().a(Long.valueOf(this.e.c().f())).e());
                                                                                        } catch (int v0) {
                                                                                        }
                                                                                        this.b(this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).d())));
                                                                                        this.e.c().c().a(Long.valueOf(v2_172)).a();
                                                                                        this.a(this.e.c().c().a(Long.valueOf(v2_172)));
                                                                                        this.a(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).d());
                                                                                        this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).d())).c(1);
                                                                                        this.e.g().a(com.teamspeak.ts3client.jni.h.aB, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).b(), v1_183, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).e())).e(), this.e.c().m()));
                                                                                        if (!((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).c().equals("")) {
                                                                                            com.teamspeak.ts3client.jni.Ts3Jni v1_191 = new Object[3];
                                                                                            v1_191[0] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).e())).e();
                                                                                            v1_191[1] = ((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).b();
                                                                                            v1_191[2] = ((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).c();
                                                                                            com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.kicked.self.reason", v1_191));
                                                                                        } else {
                                                                                            com.teamspeak.ts3client.jni.Ts3Jni v1_193 = new Object[2];
                                                                                            v1_193[0] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).e())).e();
                                                                                            v1_193[1] = ((com.teamspeak.ts3client.jni.events.ClientKickFromChannel) p13).b();
                                                                                            com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.kicked.self", v1_193));
                                                                                        }
                                                                                    }
                                                                                    this.b();
                                                                                }
                                                                            } else {
                                                                                this.a(((com.teamspeak.ts3client.jni.events.UpdateClient) p13).a());
                                                                                if ((this.e.c() != null) && (((com.teamspeak.ts3client.jni.events.UpdateClient) p13).a() == this.e.c().i())) {
                                                                                    this.e.c().e().D();
                                                                                }
                                                                                this.e.c().e().y();
                                                                            }
                                                                        } else {
                                                                            com.teamspeak.ts3client.jni.Ts3Jni v1_212;
                                                                            int v0_304 = new com.teamspeak.ts3client.data.a(this.e.c().h().b(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).a(), com.teamspeak.ts3client.jni.c.a), ((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).a(), ((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).b(), this.e.c().h().c(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).a(), com.teamspeak.ts3client.jni.c.i));
                                                                            v0_304.b(this.e.c().h().a(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).a(), com.teamspeak.ts3client.jni.c.C));
                                                                            v0_304.c(this.e.c().h().a(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).a(), com.teamspeak.ts3client.jni.c.e));
                                                                            if (this.e.c().h().a(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).a(), com.teamspeak.ts3client.jni.c.l) != 1) {
                                                                                v1_212 = 0;
                                                                            } else {
                                                                                v1_212 = 1;
                                                                            }
                                                                            if (v1_212 != null) {
                                                                                this.e.c().c().b().a(0);
                                                                                this.e.c().c().b(v0_304);
                                                                            }
                                                                            com.teamspeak.ts3client.jni.Ts3Jni v1_221;
                                                                            v0_304.a(v1_212);
                                                                            v0_304.a(this.e.c().h().a(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).a(), com.teamspeak.ts3client.jni.c.g));
                                                                            if (this.e.c().h().a(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).a(), com.teamspeak.ts3client.jni.c.G) != 1) {
                                                                                v1_221 = 0;
                                                                            } else {
                                                                                v1_221 = 1;
                                                                            }
                                                                            v0_304.b(v1_221);
                                                                            if (this.e.c().h().a(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).a(), com.teamspeak.ts3client.jni.c.m) == 1) {
                                                                                v0_304.d(1);
                                                                            }
                                                                            this.e.c().c().a(v0_304);
                                                                            if (this.e.c().p().g()) {
                                                                                int v0_310 = new long[1];
                                                                                v0_310[0] = ((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).a();
                                                                                this.e.c().h().ts3client_requestChannelSubscribe(this.e.c().k(), v0_310, new StringBuilder("Subscribe ").append(((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).a()).toString());
                                                                            }
                                                                            String v7_35 = this.e.c().c().a().values().iterator();
                                                                            while (v7_35.hasNext()) {
                                                                                int v0_321 = ((com.teamspeak.ts3client.data.a) v7_35.next());
                                                                                if (v0_321.g() == ((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).b()) {
                                                                                    this.e.c().c().a(Long.valueOf(v0_321.b())).a(this.e.c().h().c(this.e.c().k(), v0_321.b(), com.teamspeak.ts3client.jni.c.i));
                                                                                }
                                                                            }
                                                                            if (((com.teamspeak.ts3client.jni.events.NewChannelCreated) p13).c() == this.e.c().i()) {
                                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.r, 0);
                                                                            }
                                                                            this.b();
                                                                        }
                                                                    } else {
                                                                        if (this.g) {
                                                                            if (((com.teamspeak.ts3client.jni.events.TalkStatusChange) p13).b() != 0) {
                                                                                if (((com.teamspeak.ts3client.jni.events.TalkStatusChange) p13).c() != 1) {
                                                                                    this.q.remove(Integer.valueOf(((com.teamspeak.ts3client.jni.events.TalkStatusChange) p13).a()));
                                                                                } else {
                                                                                    this.q.add(Integer.valueOf(((com.teamspeak.ts3client.jni.events.TalkStatusChange) p13).a()));
                                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.j, new com.teamspeak.ts3client.a.o(new StringBuilder().append(((com.teamspeak.ts3client.jni.events.TalkStatusChange) p13).a()).toString(), 0, "", ""));
                                                                                }
                                                                                if (!this.e.c().q().a(((com.teamspeak.ts3client.jni.events.TalkStatusChange) p13).a())) {
                                                                                    return;
                                                                                } else {
                                                                                    int v0_335 = this.b(((com.teamspeak.ts3client.jni.events.TalkStatusChange) p13).a());
                                                                                    if (v0_335 != 0) {
                                                                                        v0_335.b(((com.teamspeak.ts3client.jni.events.TalkStatusChange) p13).c());
                                                                                    }
                                                                                }
                                                                            } else {
                                                                                int v0_337 = this.b(((com.teamspeak.ts3client.jni.events.TalkStatusChange) p13).a());
                                                                                if (v0_337 != 0) {
                                                                                    v0_337.a(((com.teamspeak.ts3client.jni.events.TalkStatusChange) p13).c());
                                                                                }
                                                                                if ((com.teamspeak.ts3client.ConnectionBackground.k) && (((com.teamspeak.ts3client.jni.events.TalkStatusChange) p13).a() == this.e.c().i())) {
                                                                                    if (((com.teamspeak.ts3client.jni.events.TalkStatusChange) p13).c() != 1) {
                                                                                        com.teamspeak.ts3client.ConnectionBackground.l.a(2130837622);
                                                                                        com.teamspeak.ts3client.ConnectionBackground.l.a(android.graphics.BitmapFactory.decodeResource(com.teamspeak.ts3client.Ts3Application.a().getResources(), 2130837622));
                                                                                        com.teamspeak.ts3client.ConnectionBackground.j = com.teamspeak.ts3client.ConnectionBackground.l.c();
                                                                                        this.e.j().notify(1, com.teamspeak.ts3client.ConnectionBackground.j);
                                                                                    } else {
                                                                                        com.teamspeak.ts3client.ConnectionBackground.l.a(2130837662);
                                                                                        com.teamspeak.ts3client.ConnectionBackground.l.a(com.teamspeak.ts3client.data.d.t.a(2130837662, 1111490560, 1111490560));
                                                                                        int v0_350 = com.teamspeak.ts3client.ConnectionBackground.l.c();
                                                                                        v0_350.ledARGB = -16776961;
                                                                                        v0_350.flags = (v0_350.flags | 3);
                                                                                        v0_350.ledOffMS = 0;
                                                                                        v0_350.ledOnMS = 1;
                                                                                        com.teamspeak.ts3client.ConnectionBackground.j = v0_350;
                                                                                        this.e.j().notify(1, com.teamspeak.ts3client.ConnectionBackground.j);
                                                                                    }
                                                                                }
                                                                            }
                                                                            if (this.e.c().e() != null) {
                                                                                this.e.c().e().y();
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    com.teamspeak.ts3client.jni.Ts3Jni v1_272 = this.b(((com.teamspeak.ts3client.jni.events.ClientMoveTimeout) p13).a());
                                                                    if (v1_272 != null) {
                                                                        try {
                                                                            if (v1_272.c() == 1) {
                                                                                this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveTimeout) p13).b())).e(v1_272.f());
                                                                                this.e.c().d().remove(Integer.valueOf(v1_272.f()));
                                                                                this.e.c().q().c(v1_272.f());
                                                                                this.e.c().e().y();
                                                                            } else {
                                                                                long v2_234 = new Object[1];
                                                                                v2_234[0] = v1_272.d();
                                                                                com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.dropped", v2_234));
                                                                            }
                                                                        } catch (int v0) {
                                                                        }
                                                                        if (((com.teamspeak.ts3client.jni.events.ClientMoveTimeout) p13).b() == this.e.c().f()) {
                                                                            int v0_379 = 0;
                                                                            if (v1_272.e() != null) {
                                                                                v0_379 = v1_272.e().a();
                                                                            }
                                                                            if (v1_272.c() != 1) {
                                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.M, new com.teamspeak.ts3client.a.o(v1_272.d(), v0_379, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveTimeout) p13).b())).e(), this.e.c().m()));
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                com.teamspeak.ts3client.jni.Ts3Jni v1_282;
                                                                if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).d() == 0) {
                                                                    int v0_387 = new com.teamspeak.ts3client.data.c(this.e.c().h().b(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.ClientMove) p13).a(), com.teamspeak.ts3client.jni.d.b), ((com.teamspeak.ts3client.jni.events.ClientMove) p13).a(), this.e);
                                                                    if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).c() == 0) {
                                                                        this.e.c().q().c(v0_387.f());
                                                                        v1_282 = v0_387;
                                                                    } else {
                                                                        this.e.c().q().a(v0_387);
                                                                        this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).d(v0_387.f());
                                                                        this.e.c().d().put(Integer.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).a()), Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c()));
                                                                        v1_282 = v0_387;
                                                                    }
                                                                } else {
                                                                    int v0_390;
                                                                    if (com.teamspeak.ts3client.jni.j.a != ((com.teamspeak.ts3client.jni.events.ClientMove) p13).f()) {
                                                                        try {
                                                                            v0_390 = this.b(((com.teamspeak.ts3client.jni.events.ClientMove) p13).a());
                                                                        } catch (int v0) {
                                                                            v0_390 = new com.teamspeak.ts3client.data.c(this.e.c().h().b(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.ClientMove) p13).a(), com.teamspeak.ts3client.jni.d.b), ((com.teamspeak.ts3client.jni.events.ClientMove) p13).a(), this.e);
                                                                            this.e.c().q().a(v0_390);
                                                                        }
                                                                        if (v0_390 == 0) {
                                                                            return;
                                                                        }
                                                                    } else {
                                                                        v0_390 = new com.teamspeak.ts3client.data.c(this.e.c().h().b(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.ClientMove) p13).a(), com.teamspeak.ts3client.jni.d.b), ((com.teamspeak.ts3client.jni.events.ClientMove) p13).a(), this.e);
                                                                        this.e.c().q().a(v0_390);
                                                                    }
                                                                    if (this.q.contains(Integer.valueOf(v0_390.f()))) {
                                                                        this.b(v0_390.f()).b(1);
                                                                        this.b(v0_390.f()).a(0);
                                                                    }
                                                                    if (this.a(((com.teamspeak.ts3client.jni.events.ClientMove) p13).a(), ((com.teamspeak.ts3client.jni.events.ClientMove) p13).d())) {
                                                                        this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).d())).e(v0_390.f());
                                                                        this.e.c().d().remove(Integer.valueOf(v0_390.f()));
                                                                    }
                                                                    if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).c() == 0) {
                                                                        this.e.c().q().c(v0_390.f());
                                                                        v1_282 = v0_390;
                                                                    } else {
                                                                        if ((com.teamspeak.ts3client.jni.j.a != ((com.teamspeak.ts3client.jni.events.ClientMove) p13).f()) && (com.teamspeak.ts3client.jni.j.b != ((com.teamspeak.ts3client.jni.events.ClientMove) p13).f())) {
                                                                            this.e.c().q().c(v0_390.f());
                                                                            v1_282 = v0_390;
                                                                        } else {
                                                                            this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).d(v0_390.f());
                                                                            this.e.c().d().put(Integer.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).a()), Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c()));
                                                                            v1_282 = v0_390;
                                                                        }
                                                                    }
                                                                }
                                                                if ((((com.teamspeak.ts3client.jni.events.ClientMove) p13).a() == this.e.c().i()) && (((com.teamspeak.ts3client.jni.events.ClientMove) p13).c() != 0)) {
                                                                    long v2_284 = this.e.c().f();
                                                                    if ((!this.e.c().p().b(Long.valueOf(v2_284))) && (!this.e.c().p().g())) {
                                                                        int v0_413 = this.e.c().h();
                                                                        String v4_127 = this.e.c().k();
                                                                        String v6_80 = new long[2];
                                                                        v6_80[0] = v2_284;
                                                                        v6_80[1] = 0;
                                                                        v0_413.ts3client_requestChannelUnsubscribe(v4_127, v6_80, "Unsubscribe old Channel");
                                                                    }
                                                                    this.e.c().a(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c());
                                                                    com.teamspeak.ts3client.chat.d.a(this.e.getBaseContext()).b().a(this.e.c().c().a(Long.valueOf(this.e.c().f())).e());
                                                                    String v4_135 = new Object[1];
                                                                    v4_135[0] = this.e.c().c().a(Long.valueOf(this.e.c().f())).e();
                                                                    com.teamspeak.ts3client.chat.d.a(com.teamspeak.ts3client.data.e.a.a("event.client.movemoved.self.info", v4_135));
                                                                    this.b(this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())));
                                                                    this.e.c().c().a(Long.valueOf(v2_284)).a();
                                                                    this.a(this.e.c().c().a(Long.valueOf(v2_284)));
                                                                    this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).c(1);
                                                                    this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).h();
                                                                    this.e.c().p().b(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c());
                                                                    if (!com.teamspeak.ts3client.Ts3Application.a().c().o().containsKey(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c()))) {
                                                                        this.e.c().p().d("");
                                                                    } else {
                                                                        this.e.c().p().d(((String) com.teamspeak.ts3client.Ts3Application.a().c().o().get(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c()))));
                                                                    }
                                                                    if (this.e.c().c().a(Long.valueOf(this.e.c().f())).i()) {
                                                                        this.e.c().c().a(Long.valueOf(this.e.c().f())).e(1);
                                                                    }
                                                                    this.b(v2_284);
                                                                    this.a(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c());
                                                                }
                                                                int v0_468 = 0;
                                                                if (v1_282.e() != null) {
                                                                    v0_468 = v1_282.e().a();
                                                                }
                                                                if ((((com.teamspeak.ts3client.jni.events.ClientMove) p13).a() == this.e.c().i()) && (((com.teamspeak.ts3client.jni.events.ClientMove) p13).c() != 0)) {
                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.ay, new com.teamspeak.ts3client.a.o(v1_282.d(), v0_468, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).e(), this.e.c().m()));
                                                                    String v3_146 = new Object[2];
                                                                    v3_146[0] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).d())).e();
                                                                    v3_146[1] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).e();
                                                                    com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.move.self", v3_146));
                                                                }
                                                                if ((((com.teamspeak.ts3client.jni.events.ClientMove) p13).a() != this.e.c().i()) && (this.e.c().h().a(((com.teamspeak.ts3client.jni.events.ClientMove) p13).e(), ((com.teamspeak.ts3client.jni.events.ClientMove) p13).a(), com.teamspeak.ts3client.jni.d.O) == 0)) {
                                                                    if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).c() != this.e.c().f()) {
                                                                        if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).d() != this.e.c().f()) {
                                                                            try {
                                                                                if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).f() == com.teamspeak.ts3client.jni.j.b) {
                                                                                    long v2_305 = new Object[3];
                                                                                    v2_305[0] = v1_282.d();
                                                                                    v2_305[1] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).d())).e();
                                                                                    v2_305[2] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).e();
                                                                                    com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.move.e", v2_305));
                                                                                }
                                                                            } catch (int v0) {
                                                                            }
                                                                            if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).f() == com.teamspeak.ts3client.jni.j.c) {
                                                                                long v2_308 = new Object[3];
                                                                                v2_308[0] = v1_282.d();
                                                                                v2_308[1] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).d())).e();
                                                                                v2_308[2] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).e();
                                                                                com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.move.f", v2_308));
                                                                            }
                                                                        } else {
                                                                            if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).f() == com.teamspeak.ts3client.jni.j.b) {
                                                                                try {
                                                                                    String v3_167 = new Object[3];
                                                                                    v3_167[0] = v1_282.d();
                                                                                    v3_167[1] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).d())).e();
                                                                                    v3_167[2] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).e();
                                                                                    com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.move.c", v3_167));
                                                                                } catch (long v2) {
                                                                                }
                                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.Q, new com.teamspeak.ts3client.a.o(v1_282.d(), v0_468, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).e(), this.e.c().m()));
                                                                            }
                                                                            if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).f() == com.teamspeak.ts3client.jni.j.c) {
                                                                                try {
                                                                                    String v3_171 = new Object[2];
                                                                                    v3_171[0] = v1_282.d();
                                                                                    v3_171[1] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).e();
                                                                                    com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.move.d", v3_171));
                                                                                } catch (long v2) {
                                                                                }
                                                                                if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).c() != 0) {
                                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.P, new com.teamspeak.ts3client.a.o(v1_282.d(), v0_468, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).d())).e(), this.e.c().m()));
                                                                                } else {
                                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.K, new com.teamspeak.ts3client.a.o(v1_282.d(), v0_468, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).d())).e(), this.e.c().m()));
                                                                                    long v2_324 = new Object[2];
                                                                                    v2_324[0] = v1_282.d();
                                                                                    v2_324[1] = ((com.teamspeak.ts3client.jni.events.ClientMove) p13).b();
                                                                                    com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.disconnected", v2_324));
                                                                                }
                                                                            }
                                                                        }
                                                                    } else {
                                                                        if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).f() == com.teamspeak.ts3client.jni.j.b) {
                                                                            this.e.g().a(com.teamspeak.ts3client.jni.h.O, new com.teamspeak.ts3client.a.o(v1_282.d(), v0_468, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).e(), this.e.c().m()));
                                                                        }
                                                                        if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).f() == com.teamspeak.ts3client.jni.j.a) {
                                                                            try {
                                                                                if (((com.teamspeak.ts3client.jni.events.ClientMove) p13).d() != 0) {
                                                                                    String v3_180 = new Object[2];
                                                                                    v3_180[0] = v1_282.d();
                                                                                    v3_180[1] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).d())).e();
                                                                                    com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.move.b", v3_180));
                                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.N, new com.teamspeak.ts3client.a.o(v1_282.d(), v0_468, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).e(), this.e.c().m()));
                                                                                } else {
                                                                                    String v3_183 = new Object[2];
                                                                                    v3_183[0] = v1_282.d();
                                                                                    v3_183[1] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).e();
                                                                                    com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.move.a", v3_183));
                                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.I, new com.teamspeak.ts3client.a.o(v1_282.d(), v0_468, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMove) p13).c())).e(), this.e.c().m()));
                                                                                }
                                                                            } catch (int v0) {
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                this.e.c().e().y();
                                                            }
                                                        } else {
                                                            int v0_487;
                                                            if (((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).d() == 0) {
                                                                v0_487 = new com.teamspeak.ts3client.data.c(this.e.c().h().b(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).a(), com.teamspeak.ts3client.jni.d.b), ((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).a(), this.e);
                                                                if (((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c() != 0) {
                                                                    this.e.c().q().a(v0_487);
                                                                    this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c())).d(v0_487.f());
                                                                    this.e.c().d().put(Integer.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).a()), Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c()));
                                                                }
                                                            } else {
                                                                if (com.teamspeak.ts3client.jni.j.a != ((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).e()) {
                                                                    v0_487 = this.b(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).a());
                                                                    if (v0_487 == 0) {
                                                                        return;
                                                                    }
                                                                } else {
                                                                    v0_487 = new com.teamspeak.ts3client.data.c(this.e.c().h().b(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).a(), com.teamspeak.ts3client.jni.d.b), ((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).a(), this.e);
                                                                    this.e.c().q().a(v0_487);
                                                                }
                                                                if (this.q.contains(Integer.valueOf(v0_487.f()))) {
                                                                    this.b(v0_487.f()).b(1);
                                                                    this.b(v0_487.f()).a(0);
                                                                }
                                                                if (this.a(v0_487.f(), ((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).d())) {
                                                                    this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).d())).e(v0_487.f());
                                                                    this.e.c().d().remove(Integer.valueOf(v0_487.f()));
                                                                }
                                                                if (((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c() == 0) {
                                                                    this.e.c().q().c(v0_487.f());
                                                                } else {
                                                                    if ((com.teamspeak.ts3client.jni.j.a != ((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).e()) && (com.teamspeak.ts3client.jni.j.b != ((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).e())) {
                                                                        this.e.c().q().c(v0_487.f());
                                                                    } else {
                                                                        this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c())).d(v0_487.f());
                                                                        this.e.c().d().put(Integer.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).a()), Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c()));
                                                                    }
                                                                }
                                                            }
                                                            com.teamspeak.ts3client.jni.Ts3Jni v1_401 = 0;
                                                            if (v0_487.e() != null) {
                                                                v1_401 = v0_487.e().a();
                                                            }
                                                            if ((((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c() == this.e.c().f()) || (((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).a() != this.e.c().i())) {
                                                                if ((((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c() != this.e.c().f()) || (((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).e() != com.teamspeak.ts3client.jni.j.a)) {
                                                                    if ((((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c() != this.e.c().f()) || (((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).e() != com.teamspeak.ts3client.jni.j.b)) {
                                                                        if ((((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).d() != this.e.c().f()) || (((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).e() != com.teamspeak.ts3client.jni.j.c)) {
                                                                            if ((((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).d() == this.e.c().f()) && (((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).e() == com.teamspeak.ts3client.jni.j.b)) {
                                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.X, new com.teamspeak.ts3client.a.o(v0_487.d(), v1_401, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c())).e(), this.e.c().m()));
                                                                            }
                                                                        } else {
                                                                            this.e.g().a(com.teamspeak.ts3client.jni.h.W, new com.teamspeak.ts3client.a.o(v0_487.d(), v1_401, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c())).e(), this.e.c().m()));
                                                                        }
                                                                    } else {
                                                                        this.e.g().a(com.teamspeak.ts3client.jni.h.V, new com.teamspeak.ts3client.a.o(v0_487.d(), v1_401, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c())).e(), this.e.c().m()));
                                                                    }
                                                                } else {
                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.U, new com.teamspeak.ts3client.a.o(v0_487.d(), v1_401, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c())).e(), this.e.c().m()));
                                                                }
                                                            } else {
                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.az, new com.teamspeak.ts3client.a.o(v0_487.d(), v1_401, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c())).e(), this.e.c().m()));
                                                            }
                                                            if (((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).a() != this.e.c().i()) {
                                                                long v2_400 = new Object[4];
                                                                v2_400[0] = v0_487.d();
                                                                v2_400[1] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).d())).e();
                                                                v2_400[2] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c())).e();
                                                                v2_400[3] = ((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).b();
                                                                com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.movemoved", v2_400));
                                                            } else {
                                                                long v2_401 = this.e.c().f();
                                                                if ((!this.e.c().p().b(Long.valueOf(v2_401))) && (!this.e.c().p().g())) {
                                                                    int v0_511 = this.e.c().h();
                                                                    String v4_244 = this.e.c().k();
                                                                    com.teamspeak.ts3client.jni.Ts3Jni v1_409 = new long[2];
                                                                    v1_409[0] = v2_401;
                                                                    v1_409[1] = 0;
                                                                    v0_511.ts3client_requestChannelUnsubscribe(v4_244, v1_409, "Unsubscribe old Channel");
                                                                }
                                                                this.e.c().a(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c());
                                                                com.teamspeak.ts3client.jni.Ts3Jni v1_411 = new Object[2];
                                                                v1_411[0] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c())).e();
                                                                v1_411[1] = ((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).b();
                                                                com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.movemoved.self", v1_411));
                                                                com.teamspeak.ts3client.jni.Ts3Jni v1_413 = new Object[1];
                                                                v1_413[0] = this.e.c().c().a(Long.valueOf(this.e.c().f())).e();
                                                                com.teamspeak.ts3client.chat.d.a(com.teamspeak.ts3client.data.e.a.a("event.client.movemoved.self.info", v1_413));
                                                                com.teamspeak.ts3client.chat.d.a(this.e.getBaseContext()).b().a(this.e.c().c().a(Long.valueOf(this.e.c().f())).e());
                                                                this.b(this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c())));
                                                                this.e.c().c().a(Long.valueOf(v2_401)).a();
                                                                this.a(this.e.c().c().a(Long.valueOf(v2_401)));
                                                                this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c())).c(1);
                                                                this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c())).h();
                                                                this.e.c().p().b(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c());
                                                                if (!com.teamspeak.ts3client.Ts3Application.a().c().o().containsKey(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c()))) {
                                                                    this.e.c().p().d("");
                                                                } else {
                                                                    this.e.c().p().d(((String) com.teamspeak.ts3client.Ts3Application.a().c().o().get(Long.valueOf(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c()))));
                                                                }
                                                                this.b(v2_401);
                                                                this.a(((com.teamspeak.ts3client.jni.events.ClientMoveMoved) p13).c());
                                                            }
                                                            this.e.c().e().y();
                                                        }
                                                    } else {
                                                        int v0_566 = this.e.c().c().b(Long.valueOf(((com.teamspeak.ts3client.jni.events.DelChannel) p13).a()));
                                                        if (((com.teamspeak.ts3client.jni.events.DelChannel) p13).b() != this.e.c().i()) {
                                                            if (((com.teamspeak.ts3client.jni.events.DelChannel) p13).b() != this.e.c().i()) {
                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.u, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.DelChannel) p13).c(), 0, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.DelChannel) p13).a())).e(), this.e.c().m()));
                                                                long v2_411 = new Object[2];
                                                                v2_411[0] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.DelChannel) p13).a())).e();
                                                                v2_411[1] = ((com.teamspeak.ts3client.jni.events.DelChannel) p13).c();
                                                                com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.channel.deleted", v2_411));
                                                            }
                                                        } else {
                                                            this.e.g().a(com.teamspeak.ts3client.jni.h.t, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.DelChannel) p13).c(), 0, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.DelChannel) p13).a())).e(), this.e.c().m()));
                                                            long v2_414 = new Object[1];
                                                            v2_414[0] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.DelChannel) p13).a())).e();
                                                            com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.channel.deleted.self", v2_414));
                                                        }
                                                        if (v0_566 != 0) {
                                                            this.e.c().c().a(v0_566).a(this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.DelChannel) p13).a())).f());
                                                        }
                                                        this.e.c().c().c(Long.valueOf(((com.teamspeak.ts3client.jni.events.DelChannel) p13).a()));
                                                        this.b();
                                                    }
                                                } else {
                                                    int v0_588;
                                                    this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).a(this.e.c().h().b(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a(), com.teamspeak.ts3client.jni.c.a));
                                                    this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).b(this.e.c().h().a(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a(), com.teamspeak.ts3client.jni.c.C));
                                                    this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).c(this.e.c().h().a(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a(), com.teamspeak.ts3client.jni.c.e));
                                                    if (this.e.c().h().a(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a(), com.teamspeak.ts3client.jni.c.l) != 1) {
                                                        v0_588 = 0;
                                                    } else {
                                                        v0_588 = 1;
                                                    }
                                                    if (v0_588 != 0) {
                                                        this.e.c().c().b().a(0);
                                                        this.e.c().c().b(this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())));
                                                    }
                                                    int v0_605;
                                                    this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).a(v0_588);
                                                    this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).a(this.e.c().h().a(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a(), com.teamspeak.ts3client.jni.c.g));
                                                    this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).c(this.e.c().h().c(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a(), com.teamspeak.ts3client.jni.c.F));
                                                    String v7_86 = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a()));
                                                    if (this.e.c().h().a(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a(), com.teamspeak.ts3client.jni.c.G) != 1) {
                                                        v0_605 = 0;
                                                    } else {
                                                        v0_605 = 1;
                                                    }
                                                    v7_86.b(v0_605);
                                                    if (this.e.c().h().a(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a(), com.teamspeak.ts3client.jni.c.m) != 1) {
                                                        this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).d(0);
                                                    } else {
                                                        this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).d(1);
                                                    }
                                                    int v0_623 = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).c();
                                                    com.teamspeak.ts3client.jni.Ts3Jni v1_497 = Long.valueOf(this.e.c().h().c(this.e.c().k(), ((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a(), com.teamspeak.ts3client.jni.c.i));
                                                    if (v1_497.longValue() != this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).f()) {
                                                        long v2_463 = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a()), v0_623);
                                                        if (v2_463 != 0) {
                                                            this.e.c().c().a(v2_463).a(this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).f());
                                                        }
                                                        int v0_624 = this.e.c().c().a(v1_497, v0_623);
                                                        if (v0_624 != 0) {
                                                            this.e.c().c().a(v0_624).a(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a());
                                                        }
                                                        this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).a(v1_497.longValue());
                                                    }
                                                    this.b();
                                                    if (((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a() == this.e.c().f()) {
                                                        com.teamspeak.ts3client.chat.d.a(this.e.getBaseContext()).b().a(this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).e());
                                                    }
                                                    if ((((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a() != this.e.c().f()) || (((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).b() != this.e.c().i())) {
                                                        if ((((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a() != this.e.c().f()) || (((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).b() == this.e.c().i())) {
                                                            if ((((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a() == this.e.c().f()) || (((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).b() != this.e.c().i())) {
                                                                if ((((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a() == this.e.c().f()) && (((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).b() != this.e.c().i())) {
                                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.z, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).c(), 0, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).e(), this.e.c().m()));
                                                                }
                                                            } else {
                                                                this.e.g().a(com.teamspeak.ts3client.jni.h.y, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).c(), 0, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).e(), this.e.c().m()));
                                                            }
                                                        } else {
                                                            this.e.g().a(com.teamspeak.ts3client.jni.h.x, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).c(), 0, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).e(), this.e.c().m()));
                                                        }
                                                    } else {
                                                        this.e.g().a(com.teamspeak.ts3client.jni.h.w, new com.teamspeak.ts3client.a.o(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).c(), 0, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).e(), this.e.c().m()));
                                                    }
                                                    if (((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).b() != this.e.c().i()) {
                                                        com.teamspeak.ts3client.jni.Ts3Jni v1_523 = new Object[2];
                                                        v1_523[0] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).e();
                                                        v1_523[1] = ((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).c();
                                                        com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.channel.edit", v1_523));
                                                    } else {
                                                        com.teamspeak.ts3client.jni.Ts3Jni v1_525 = new Object[1];
                                                        v1_525[0] = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.UpdateChannelEdited) p13).a())).e();
                                                        com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.channel.edit.self", v1_525));
                                                    }
                                                }
                                            } else {
                                                int v0_664 = this.e.c().c().b(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelMove) p13).a()));
                                                if (v0_664 != 0) {
                                                    this.e.c().c().a(v0_664).a(this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelMove) p13).a())).f());
                                                }
                                                this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelMove) p13).a())).b(((com.teamspeak.ts3client.jni.events.ChannelMove) p13).d());
                                                String v7_87 = this.e.c().c().a().values().iterator();
                                                while (v7_87.hasNext()) {
                                                    int v0_710 = ((com.teamspeak.ts3client.data.a) v7_87.next());
                                                    if (v0_710.g() == ((com.teamspeak.ts3client.jni.events.ChannelMove) p13).d()) {
                                                        this.e.c().c().a(Long.valueOf(v0_710.b())).a(this.e.c().h().c(this.e.c().k(), v0_710.b(), com.teamspeak.ts3client.jni.c.i));
                                                    }
                                                }
                                                this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelMove) p13).a())).d();
                                                this.b();
                                                if ((((com.teamspeak.ts3client.jni.events.ChannelMove) p13).a() != this.e.c().f()) || (((com.teamspeak.ts3client.jni.events.ChannelMove) p13).b() != this.e.c().i())) {
                                                    if ((((com.teamspeak.ts3client.jni.events.ChannelMove) p13).a() == this.e.c().f()) && (((com.teamspeak.ts3client.jni.events.ChannelMove) p13).b() != this.e.c().i())) {
                                                        this.e.g().a(com.teamspeak.ts3client.jni.h.C, new com.teamspeak.ts3client.a.o("", 0, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelMove) p13).a())).e(), this.e.c().m()));
                                                    }
                                                } else {
                                                    this.e.g().a(com.teamspeak.ts3client.jni.h.B, new com.teamspeak.ts3client.a.o("", 0, this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelMove) p13).a())).e(), this.e.c().m()));
                                                }
                                                int v0_693 = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelMove) p13).a()));
                                                if (((com.teamspeak.ts3client.jni.events.ChannelMove) p13).b() != this.e.c().i()) {
                                                    if (this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelMove) p13).a())).g() != 0) {
                                                        long v2_528 = new Object[3];
                                                        v2_528[0] = v0_693.e();
                                                        v2_528[1] = ((com.teamspeak.ts3client.jni.events.ChannelMove) p13).c();
                                                        v2_528[2] = this.e.c().c().a(Long.valueOf(v0_693.g())).e();
                                                        com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.channel.moved", v2_528));
                                                    } else {
                                                        long v2_530 = new Object[3];
                                                        v2_530[0] = v0_693.e();
                                                        v2_530[1] = ((com.teamspeak.ts3client.jni.events.ChannelMove) p13).c();
                                                        v2_530[2] = this.e.c().m();
                                                        com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.channel.moved", v2_530));
                                                    }
                                                } else {
                                                    if (this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelMove) p13).a())).g() != 0) {
                                                        long v2_535 = new Object[2];
                                                        v2_535[0] = v0_693.e();
                                                        v2_535[1] = this.e.c().c().a(Long.valueOf(v0_693.g())).e();
                                                        com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.channel.moved.self", v2_535));
                                                    } else {
                                                        long v2_537 = new Object[2];
                                                        v2_537[0] = v0_693.e();
                                                        v2_537[1] = this.e.c().m();
                                                        com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.channel.moved.self", v2_537));
                                                    }
                                                }
                                            }
                                        } else {
                                            if (((com.teamspeak.ts3client.jni.events.ChannelUnsubscribeFinished) p13).a() == this.e.c().k()) {
                                                this.n();
                                            }
                                            if ((this.h) && (this.e.c().p().e() == null)) {
                                                this.h = 0;
                                            }
                                        }
                                    } else {
                                        this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelUnsubscribe) p13).a())).c(0);
                                        com.teamspeak.ts3client.jni.Ts3Jni v1_570 = this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelUnsubscribe) p13).a())).j().iterator();
                                        while (v1_570.hasNext()) {
                                            this.e.c().q().c(((Integer) v1_570.next()).intValue());
                                        }
                                        this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelUnsubscribe) p13).a())).a();
                                    }
                                } else {
                                    this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.ChannelSubscribe) p13).a())).c(1);
                                }
                            } else {
                                if (((com.teamspeak.ts3client.jni.events.ChannelSubscribeFinished) p13).a() == this.e.c().k()) {
                                    this.n();
                                }
                                this.o = 1;
                                if (this.h) {
                                    this.h = 0;
                                }
                                this.e.c().e().y();
                            }
                        } else {
                            if (((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).a() != this.e.c().i()) {
                                com.teamspeak.ts3client.jni.Ts3Jni v1_578 = new Object[2];
                                v1_578[0] = this.e.c().q().b(((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).a()).d();
                                v1_578[1] = ((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).b();
                                com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("event.client.ban", v1_578));
                                if (((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).d() == this.e.c().f()) {
                                    this.e.g().a(com.teamspeak.ts3client.jni.h.an, new com.teamspeak.ts3client.a.o(this.e.c().q().b(((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).a()).d(), 0, "", this.e.c().m()));
                                }
                                this.e.c().c().a(Long.valueOf(((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).d())).e(((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).a());
                                this.e.c().q().c(((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).a());
                                this.e.c().d().remove(Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).a()));
                                this.b();
                            } else {
                                if (((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).e() <= 0) {
                                    int v0_770 = this.e.c().e();
                                    long v2_567 = new Object[1];
                                    v2_567[0] = ((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).c();
                                    v0_770.a(com.teamspeak.ts3client.data.e.a.a("messages.ban.text.permanent", v2_567), com.teamspeak.ts3client.data.e.a.a("messages.ban.info"), Boolean.valueOf(0), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.exit"), com.teamspeak.ts3client.jni.h.aC);
                                } else {
                                    long v2_570 = ((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).e();
                                    int v0_771 = "";
                                    String v4_322 = (v2_570 / 86400);
                                    long v2_571 = (v2_570 - (86400 * v4_322));
                                    if (v4_322 > 0) {
                                        v0_771 = new StringBuilder().append("").append(v4_322).append("d").toString();
                                    }
                                    String v4_324 = (v2_571 / 3600);
                                    long v2_572 = (v2_571 - (3600 * v4_324));
                                    if (v4_324 > 0) {
                                        v0_771 = new StringBuilder().append(v0_771).append(v4_324).append("h").toString();
                                    }
                                    String v4_326 = (v2_572 / 60);
                                    long v2_573 = (v2_572 - (60 * v4_326));
                                    if (v4_326 > 0) {
                                        v0_771 = new StringBuilder().append(v0_771).append(v4_326).append("m").toString();
                                    }
                                    com.teamspeak.ts3client.jni.Ts3Jni v1_600;
                                    if (v2_573 <= 0) {
                                        v1_600 = v0_771;
                                    } else {
                                        v1_600 = new StringBuilder().append(v0_771).append(v2_573).append("s").toString();
                                    }
                                    int v0_787 = this.e.c().e();
                                    String v3_275 = new Object[3];
                                    v3_275[0] = ((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).b();
                                    v3_275[1] = v1_600;
                                    v3_275[2] = ((com.teamspeak.ts3client.jni.events.rare.ClientBanFromServer) p13).c();
                                    v0_787.a(com.teamspeak.ts3client.data.e.a.a("messages.ban.text", v3_275), com.teamspeak.ts3client.data.e.a.a("messages.ban.info"), Boolean.valueOf(0), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.exit"), com.teamspeak.ts3client.jni.h.aC);
                                }
                                this.m = 1;
                            }
                        }
                    } else {
                        switch (((com.teamspeak.ts3client.jni.events.ConnectStatusChange) p13).b()) {
                            case 0:
                                this.e.e().log(java.util.logging.Level.INFO, "Connect status: STATUS_DISCONNECTED");
                            case 1:
                                this.e.e().log(java.util.logging.Level.INFO, "Connect status: STATUS_CONNECTING");
                                break;
                            case 2:
                                this.e.e().log(java.util.logging.Level.INFO, "Connect status: STATUS_CONNECTED");
                                break;
                            case 3:
                                this.e.e().log(java.util.logging.Level.INFO, "Connect status: STATUS_CONNECTION_ESTABLISHING");
                                break;
                            case 4:
                                this.e.e().log(java.util.logging.Level.INFO, "Connect status: STATUS_CONNECTION_ESTABLISHED");
                                break;
                            default:
                                if (com.teamspeak.ts3client.jni.e.a.a() != ((com.teamspeak.ts3client.jni.events.ConnectStatusChange) p13).b()) {
                                    if (com.teamspeak.ts3client.jni.e.b.a() == ((com.teamspeak.ts3client.jni.events.ConnectStatusChange) p13).b()) {
                                        if (this.e.c().e() != null) {
                                            this.e.c().e().a(com.teamspeak.ts3client.data.e.a.a("connectiondialog.step0"), Boolean.valueOf(0));
                                        }
                                        this.g = 0;
                                    }
                                    if (com.teamspeak.ts3client.jni.e.c.a() == ((com.teamspeak.ts3client.jni.events.ConnectStatusChange) p13).b()) {
                                        if (this.e.c().e() != null) {
                                            this.e.c().e().a(com.teamspeak.ts3client.data.e.a.a("connectiondialog.step1"), Boolean.valueOf(0));
                                        }
                                        this.e.c().b(this.e.c().h().b(this.e.c().k(), com.teamspeak.ts3client.jni.i.a));
                                        this.e.c().a(this.e.c().h().b(this.e.c().k(), com.teamspeak.ts3client.jni.i.b));
                                        this.e.c().p().a(this.e.c().h().c(this.e.c().k(), com.teamspeak.ts3client.jni.i.ap));
                                        com.teamspeak.ts3client.jni.Ts3Jni v1_630 = new Object[1];
                                        v1_630[0] = this.e.c().m();
                                        com.teamspeak.ts3client.chat.d.b(com.teamspeak.ts3client.data.e.a.a("chat.connected", v1_630));
                                        this.e.c().a(new com.teamspeak.ts3client.data.c.d(this.e));
                                        this.g = 0;
                                    }
                                    if (com.teamspeak.ts3client.jni.e.d.a() == ((com.teamspeak.ts3client.jni.events.ConnectStatusChange) p13).b()) {
                                        if (this.e.c().e() != null) {
                                            this.e.c().e().a(com.teamspeak.ts3client.data.e.a.a("connectiondialog.step2"), Boolean.valueOf(0));
                                        }
                                        this.g = 0;
                                    }
                                    if (com.teamspeak.ts3client.jni.e.e.a() == ((com.teamspeak.ts3client.jni.events.ConnectStatusChange) p13).b()) {
                                        this.g();
                                        if (this.e.c().e() != null) {
                                            this.e.c().e().a(com.teamspeak.ts3client.data.e.a.a("connectiondialog.step3"), Boolean.valueOf(0));
                                        }
                                        this.n();
                                        this.j();
                                        this.g = 1;
                                        com.teamspeak.ts3client.data.b.c.a().c();
                                        this.e.g().a(com.teamspeak.ts3client.jni.h.a, new com.teamspeak.ts3client.a.o("", 0, "", this.e.c().m()));
                                        if (!this.e.c().e().b()) {
                                            this.e.c().p().j();
                                            this.e.d().i().runOnUiThread(new com.teamspeak.ts3client.n(this));
                                            this.e.c().a(new com.teamspeak.ts3client.data.d.v(this.e));
                                            if (this.e.c().e() != null) {
                                                this.e.c().e().d();
                                            }
                                            if (this.e.c().h().a(this.e.c().k(), com.teamspeak.ts3client.jni.i.az) == 1) {
                                                this.e.c().e().a(1);
                                            }
                                            com.teamspeak.ts3client.chat.d.a(this.e.getBaseContext()).c().a(this.e.c().m());
                                            com.teamspeak.ts3client.chat.d.a(this.e.getBaseContext()).b().a(this.e.c().c().a(Long.valueOf(this.e.c().f())).e());
                                            if (!this.e.c().p().g()) {
                                                if (this.e.c().p().e() != null) {
                                                    this.e.c().h().ts3client_requestChannelSubscribe(this.e.c().k(), this.e.c().p().e(), "Subscribe last set");
                                                }
                                            } else {
                                                this.e.c().h().ts3client_requestChannelSubscribeAll(this.e.c().k(), "SubscribeAll");
                                            }
                                            this.e.c().e().D();
                                        } else {
                                            this.e.d().i().runOnUiThread(new com.teamspeak.ts3client.m(this));
                                        }
                                    }
                                } else {
                                    if (this.m) {
                                    } else {
                                        this.g = 0;
                                        if (this.e.c().e() != null) {
                                            if ((((com.teamspeak.ts3client.jni.events.ConnectStatusChange) p13).a() != 1793) && ((this.e.c().p().i() <= 0) || (((com.teamspeak.ts3client.jni.events.ConnectStatusChange) p13).a() != 1797))) {
                                                if (((com.teamspeak.ts3client.jni.events.ConnectStatusChange) p13).a() <= 0) {
                                                    if (this.p) {
                                                        return;
                                                    } else {
                                                        this.e.c().e().a(com.teamspeak.ts3client.data.e.a.a("messages.disconnected.text"), com.teamspeak.ts3client.data.e.a.a("messages.disconnected.info"), Boolean.valueOf(0), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.exit"), com.teamspeak.ts3client.jni.h.b);
                                                        return;
                                                    }
                                                } else {
                                                    this.e.c().e().d();
                                                    this.e.c().e().a(new StringBuilder().append(com.teamspeak.ts3client.data.e.a.a("messages.error.text")).append(com.teamspeak.ts3client.data.aa.a(((com.teamspeak.ts3client.jni.events.ConnectStatusChange) p13).a())).toString(), com.teamspeak.ts3client.data.e.a.a("messages.conerror.text"), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.exit"), com.teamspeak.ts3client.jni.h.c);
                                                    return;
                                                }
                                            } else {
                                                this.e.g().a(com.teamspeak.ts3client.jni.h.c, new com.teamspeak.ts3client.a.o("", 0, "", this.e.c().m()));
                                                this.e.c().e().d();
                                                int v0_934 = this.e.c().e();
                                                this.e.c().p();
                                                v0_934.E();
                                                return;
                                            }
                                        }
                                    }
                                }
                        }
                        if ((com.teamspeak.ts3client.jni.e.a.a() == ((com.teamspeak.ts3client.jni.events.ConnectStatusChange) p13).b()) && (!this.m)) {
                        }
                    }
                } else {
                    switch (((com.teamspeak.ts3client.jni.events.ServerError) p13).a()) {
                        case 0:
                            if (((com.teamspeak.ts3client.jni.events.ServerError) p13).d().equals("SubscribeAll")) {
                                this.e.c().p().a(1);
                                this.e.c().e().h(1);
                            }
                            if (((com.teamspeak.ts3client.jni.events.ServerError) p13).d().equals("UnsubscribeAll")) {
                                this.e.c().p().a(0);
                                this.e.c().e().h(0);
                            }
                            if ((((com.teamspeak.ts3client.jni.events.ServerError) p13).d().startsWith("Subscribe Channel:")) && (!this.e.c().p().g())) {
                                this.e.c().p().a(Long.valueOf(Long.parseLong(((com.teamspeak.ts3client.jni.events.ServerError) p13).d().split(":")[1])));
                            }
                            if (((com.teamspeak.ts3client.jni.events.ServerError) p13).d().equals("InputMute")) {
                                this.e.c().e().f(1);
                            }
                            if (!((com.teamspeak.ts3client.jni.events.ServerError) p13).d().equals("InputUnMute")) {
                                if (((com.teamspeak.ts3client.jni.events.ServerError) p13).a() != 0) {
                                    this.e.g().a(com.teamspeak.ts3client.jni.h.G, 0);
                                }
                            } else {
                                this.e.c().e().f(0);
                            }
                        case 512:
                            if ((!((com.teamspeak.ts3client.jni.events.ServerError) p13).d().equals("Composing")) && ((!((com.teamspeak.ts3client.jni.events.ServerError) p13).d().startsWith("Send Message to Client")) && ((!((com.teamspeak.ts3client.jni.events.ServerError) p13).d().equals("Chat closed")) && (!((com.teamspeak.ts3client.jni.events.ServerError) p13).d().startsWith("connectioninfo_"))))) {
                                this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.clienterror.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            }
                            break;
                        case 516:
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.clienterror.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 519:
                            this.p = 1;
                            this.e.c().e().a();
                            int v0_1042 = this.e.c().e();
                            long v2_679 = new Object[1];
                            v2_679[0] = ((com.teamspeak.ts3client.jni.events.ServerError) p13).c();
                            v0_1042.a(com.teamspeak.ts3client.data.e.a.a("messages.error.securitylevel", v2_679), com.teamspeak.ts3client.data.e.a.a("messages.disconnected.text"), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 521:
                            this.p = 1;
                            this.e.c().e().a();
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.disconnected.text"), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 522:
                            this.p = 1;
                            this.e.c().e().a();
                            this.e.c().e().a(com.teamspeak.ts3client.data.e.a.a("messages.clientoutdated.text2"), com.teamspeak.ts3client.data.e.a.a("messages.clientoutdated.text1"), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 524:
                            com.teamspeak.ts3client.chat.d.b("Action currently not possible due to spam protection. Please wait a few seconds and try again.");
                            break;
                        case 771:
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.channelerror.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 774:
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.channelerror.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 775:
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.channelerror.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 776:
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.channelerror.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 777:
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.channelerror.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 778:
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.channelerror.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 781:
                            if ((!((com.teamspeak.ts3client.jni.events.ServerError) p13).b().isEmpty()) && (((com.teamspeak.ts3client.jni.events.ServerError) p13).b().equals("invalid channel password"))) {
                                try {
                                    this.e.c().o().remove(Long.valueOf(Long.parseLong(((com.teamspeak.ts3client.jni.events.ServerError) p13).d())));
                                    this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.pwerror.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                                } catch (int v0) {
                                }
                            }
                            if (!((com.teamspeak.ts3client.jni.events.ServerError) p13).d().startsWith("skip")) {
                                if (!((com.teamspeak.ts3client.jni.events.ServerError) p13).d().startsWith("join")) {
                                    if ((!((com.teamspeak.ts3client.jni.events.ServerError) p13).d().equals("checkPW")) && (!((com.teamspeak.ts3client.jni.events.ServerError) p13).d().startsWith("Request Image"))) {
                                        this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.pwerror.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                                    }
                                } else {
                                    int v0_992 = Long.parseLong(((com.teamspeak.ts3client.jni.events.ServerError) p13).d().substring(5));
                                    this.e.c().o().remove(Long.valueOf(v0_992));
                                    this.e.c().e().a(this.e.c().c().a(Long.valueOf(v0_992)));
                                }
                            } else {
                                int v0_997 = Long.parseLong(((com.teamspeak.ts3client.jni.events.ServerError) p13).d().substring(5));
                                String v7_92 = this.e.c().c().a(Long.valueOf(v0_997));
                                if (!this.e.c().o().containsKey(Long.valueOf(v7_92.b()))) {
                                    this.e.c().e().a(this.e.c().c().a(Long.valueOf(v0_997)));
                                } else {
                                    this.e.c().h().ts3client_requestClientMove(this.e.c().k(), 0, v7_92.b(), ((String) this.e.c().o().get(Long.valueOf(v7_92.b()))), new StringBuilder("join ").append(v7_92.b()).toString());
                                }
                            }
                            break;
                        case 1027:
                            this.p = 1;
                            this.e.c().e().a();
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.disconnected.text"), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 1028:
                            this.p = 1;
                            this.e.c().e().a();
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.disconnected.text"), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 1541:
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.channelerror.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 2561:
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.error.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                        case 2570:
                            this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.permerror.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 3329:
                            this.p = 1;
                            this.e.c().e().a();
                            this.e.c().e().a(new StringBuilder().append(((com.teamspeak.ts3client.jni.events.ServerError) p13).b()).append("\n").append(((com.teamspeak.ts3client.jni.events.ServerError) p13).c()).toString(), com.teamspeak.ts3client.data.e.a.a("messages.ban.info"), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        case 3840:
                            this.e.c().e().a(com.teamspeak.ts3client.data.e.a.a("privilegekey.error.text"), com.teamspeak.ts3client.data.e.a.a("privilegekey.error"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            break;
                        default:
                            this.e.c().e().d();
                            if (((com.teamspeak.ts3client.jni.events.ServerError) p13).c() == null) {
                                this.e.c().e().a(((com.teamspeak.ts3client.jni.events.ServerError) p13).b(), com.teamspeak.ts3client.data.e.a.a("messages.error.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            } else {
                                this.e.c().e().a(new StringBuilder().append(((com.teamspeak.ts3client.jni.events.ServerError) p13).b()).append("\n").append(((com.teamspeak.ts3client.jni.events.ServerError) p13).c()).toString(), com.teamspeak.ts3client.data.e.a.a("messages.error.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.ok"));
                            }
                            this.m = 1;
                    }
                }
            } else {
                this.e.c().a(this.e.c().h().b(this.e.c().k(), com.teamspeak.ts3client.jni.i.b));
                this.e.c().p().a(this.e.c().h().c(this.e.c().k(), com.teamspeak.ts3client.jni.i.ap));
                com.teamspeak.ts3client.chat.d.a(this.e.getBaseContext()).c().a(this.e.c().m());
                this.b();
            }
        } else {
            if (((com.teamspeak.ts3client.jni.events.rare.ServerPermissionError) p13).c() != this.e.c().j().a(com.teamspeak.ts3client.jni.g.bK)) {
                this.e.g().a(com.teamspeak.ts3client.jni.h.G, 0);
                this.e.c().e().a(new StringBuilder().append(((com.teamspeak.ts3client.jni.events.rare.ServerPermissionError) p13).b()).append("\n").append(((com.teamspeak.ts3client.jni.events.rare.ServerPermissionError) p13).a()).toString(), com.teamspeak.ts3client.data.e.a.a("messages.serverpermission.text"), Boolean.valueOf(1), Boolean.valueOf(0), Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("button.close"));
            }
        }
        return;
    }

    public final void a(com.teamspeak.ts3client.tsdns.h p11)
    {
        if (this.e.a != null) {
            if (p11.d != 0) {
                this.e.a.k.a();
                this.e.a.k.a(com.teamspeak.ts3client.data.e.a.a("tsdns.error.text"), com.teamspeak.ts3client.data.e.a.a("tsdns.error"), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.exit"));
            } else {
                String v7_0 = this.e.a.v;
                v7_0.o = p11.a;
                v7_0.p = p11.c;
                try {
                    this.e.a.k.i().runOnUiThread(new com.teamspeak.ts3client.r(this, p11));
                } catch (com.teamspeak.ts3client.data.e v0) {
                    this.e.d.log(java.util.logging.Level.WARNING, "Request Error 1");
                }
                if (this.e.a.v.n <= 0) {
                    this.e.a.a(p11.a, p11.c, v7_0.f, v7_0.g, v7_0.h, v7_0.e, v7_0.i);
                } else {
                    this.e.a.a(p11.a, p11.c, v7_0.f, new StringBuilder("/").append(this.e.a.v.q).toString(), this.e.a.v.r, v7_0.e, v7_0.i);
                }
            }
        }
        return;
    }

    public final void b()
    {
        java.util.concurrent.ConcurrentHashMap v8 = this.e.a.c().a;
        java.util.SortedMap v12 = java.util.Collections.synchronizedSortedMap(new java.util.TreeMap());
        java.util.SortedMap v13 = java.util.Collections.synchronizedSortedMap(new java.util.TreeMap());
        java.util.SortedMap v2_0 = v8.values().iterator();
        while (v2_0.hasNext()) {
            long v0_33 = ((com.teamspeak.ts3client.data.a) v2_0.next());
            if (v0_33.e != 0) {
                if (!v13.containsKey(Long.valueOf(v0_33.e))) {
                    v13.put(Long.valueOf(v0_33.e), new java.util.TreeMap());
                    ((java.util.SortedMap) v13.get(Long.valueOf(v0_33.e))).put(Long.valueOf(v0_33.g), v0_33);
                } else {
                    ((java.util.SortedMap) v13.get(Long.valueOf(v0_33.e))).put(Long.valueOf(v0_33.g), v0_33);
                }
            } else {
                v12.put(Long.valueOf(v0_33.g), v0_33);
            }
        }
        com.teamspeak.ts3client.data.a[] v14 = new com.teamspeak.ts3client.data.a[(v8.size() + 1)];
        long v0_12 = new com.teamspeak.ts3client.data.a(this.e.a.m(), 0, 0, 0);
        v0_12.q = 1;
        v14[0] = v0_12;
        long v4_1 = 0;
        int v6_1 = 1;
        long v10 = 0;
        while (v6_1 < (v8.size() + 1)) {
            if (v4_1 != 0) {
                long v0_27;
                if (v14[(v6_1 - 1)].o) {
                    v0_27 = 0;
                } else {
                    v0_27 = 1;
                }
                v6_1 = this.a(v13, v14, v4_1, v6_1, 1, v8, Boolean.valueOf(v0_27));
                v4_1 = 0;
            } else {
                long v0_30 = ((com.teamspeak.ts3client.data.a) v12.get(Long.valueOf(v10)));
                v0_30.q = 1;
                v14[v6_1] = v0_30;
                v6_1++;
                try {
                    if (v13.containsKey(Long.valueOf(v0_30.b))) {
                        v4_1 = v0_30.b;
                        v0_30.q = 0;
                    }
                    if (v0_30.j().size() != 0) {
                        v0_30.q = 0;
                    }
                    v10 = v0_30.b;
                } catch (long v0) {
                }
            }
        }
        if (this.e.a.k != null) {
            long v0_21 = this.e.a.k;
            if (v0_21.l()) {
                v0_21.i().runOnUiThread(new com.teamspeak.ts3client.v(v0_21, v14));
            }
        }
        return;
    }

    public final void c()
    {
        if ((!this.e.g) && (((this.e.e.getBoolean("audio_ptt", 0)) && ((this.e.e.getBoolean("android_overlay", 0)) && (!this.r))) || ((!this.e.e.getBoolean("audio_ptt", 0)) && ((this.e.e.getBoolean("android_overlay", 0)) && (this.r))))) {
            this.e.a.k.i().runOnUiThread(new com.teamspeak.ts3client.p(this));
        }
        return;
    }

    public final void d()
    {
        if ((this.v) && (((this.e.e.getBoolean("audio_ptt", 0)) && ((this.e.e.getBoolean("android_overlay", 0)) && (!this.r))) || ((!this.e.e.getBoolean("audio_ptt", 0)) && ((this.e.e.getBoolean("android_overlay", 0)) && (this.r))))) {
            this.e.a.k.i().runOnUiThread(new com.teamspeak.ts3client.q(this));
        }
        return;
    }

    public android.os.IBinder onBind(android.content.Intent p2)
    {
        return 0;
    }

    public void onCreate()
    {
        super.onCreate();
        com.teamspeak.ts3client.Ts3Application.a().f.l();
        this.s = ((android.view.WindowManager) this.getSystemService("window"));
        this.t = new android.widget.ImageView(this);
        this.t.setImageResource(2130837622);
        this.x = new android.content.IntentFilter();
        this.x.addAction("android.intent.action.CONFIGURATION_CHANGED");
        this.u = new android.view.WindowManager$LayoutParams(-2, -2, 2002, 8, -3);
        this.u.gravity = 51;
        this.a(this.u);
        this.t.setOnTouchListener(new com.teamspeak.ts3client.o(this));
        return;
    }

    public void onDestroy()
    {
        super.onDestroy();
        if ((this.t != null) && (this.v)) {
            this.s.removeView(this.t);
        }
        return;
    }

    public int onStartCommand(android.content.Intent p2, int p3, int p4)
    {
        return 2;
    }
}
