package com.teamspeak.ts3client.jni.events;
public class ChannelUnsubscribeFinished implements com.teamspeak.ts3client.jni.k {
    private long a;

    public ChannelUnsubscribeFinished()
    {
        return;
    }

    private ChannelUnsubscribeFinished(long p2)
    {
        this.a = p2;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    public final long a()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ChannelUnsubscribeFinished [serverConnectionHandlerID=").append(this.a).append("]").toString();
    }
}
