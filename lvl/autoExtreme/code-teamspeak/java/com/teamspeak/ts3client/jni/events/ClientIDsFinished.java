package com.teamspeak.ts3client.jni.events;
public class ClientIDsFinished implements com.teamspeak.ts3client.jni.k {
    private long a;

    public ClientIDsFinished()
    {
        return;
    }

    private ClientIDsFinished(long p2)
    {
        this.a = p2;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ClientIDsFinished [serverConnectionHandlerID=").append(this.a).append("]").toString();
    }
}
