package com.teamspeak.ts3client.jni.events;
public class ClientKickFromChannel implements com.teamspeak.ts3client.jni.k {
    private long a;
    private int b;
    private long c;
    private long d;
    private com.teamspeak.ts3client.jni.j e;
    private int f;
    private String g;
    private String h;
    private String i;

    public ClientKickFromChannel()
    {
        return;
    }

    private ClientKickFromChannel(long p3, int p5, long p6, long p8, int p10, int p11, String p12, String p13, String p14)
    {
        this.a = p3;
        this.b = p5;
        this.c = p6;
        this.d = p8;
        if (p10 == 0) {
            this.e = com.teamspeak.ts3client.jni.j.a;
        }
        if (p10 == 1) {
            this.e = com.teamspeak.ts3client.jni.j.b;
        }
        if (p10 == 2) {
            this.e = com.teamspeak.ts3client.jni.j.c;
        }
        this.f = p11;
        this.g = p12;
        this.h = p13;
        this.i = p14;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private int g()
    {
        return this.f;
    }

    private String h()
    {
        return this.h;
    }

    private long i()
    {
        return this.a;
    }

    public final int a()
    {
        return this.b;
    }

    public final String b()
    {
        return this.g;
    }

    public final String c()
    {
        return this.i;
    }

    public final long d()
    {
        return this.d;
    }

    public final long e()
    {
        return this.c;
    }

    public final com.teamspeak.ts3client.jni.j f()
    {
        return this.e;
    }

    public String toString()
    {
        return new StringBuilder("ClientKickFromChannel [serverConnectionHandlerID=").append(this.a).append(", clientID=").append(this.b).append(", oldChannelID=").append(this.c).append(", newChannelID=").append(this.d).append(", visibility=").append(this.e).append(", kickerID=").append(this.f).append(", kickerName=").append(this.g).append(", kickerUniqueIdentifier=").append(this.h).append(", kickMessage=").append(this.i).append("]").toString();
    }
}
