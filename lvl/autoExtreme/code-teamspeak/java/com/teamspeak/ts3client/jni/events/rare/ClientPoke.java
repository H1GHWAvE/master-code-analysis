package com.teamspeak.ts3client.jni.events.rare;
public class ClientPoke implements com.teamspeak.ts3client.jni.k {
    private long a;
    private int b;
    private String c;
    private String d;
    private String e;

    public ClientPoke()
    {
        return;
    }

    private ClientPoke(long p2, int p4, String p5, String p6, String p7)
    {
        this.a = p2;
        this.b = p4;
        this.c = p5;
        this.d = p6;
        this.e = p7;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private int d()
    {
        return this.b;
    }

    private long e()
    {
        return this.a;
    }

    public final String a()
    {
        return this.e;
    }

    public final String b()
    {
        return this.c;
    }

    public final String c()
    {
        return this.d;
    }

    public String toString()
    {
        return new StringBuilder("ClientPoke [serverConnectionHandlerID=").append(this.a).append(", fromClientID=").append(this.b).append(", pokerName=").append(this.c).append(", pokerUniqueIdentity=").append(this.d).append(", message=").append(this.e).append("]").toString();
    }
}
