package com.teamspeak.ts3client.jni.events.rare;
public class ChannelGroupListFinished implements com.teamspeak.ts3client.jni.k {
    private long a;

    public ChannelGroupListFinished()
    {
        return;
    }

    private ChannelGroupListFinished(long p2)
    {
        this.a = p2;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ChannelGrouplistFinished [serverConnectionHandlerID=").append(this.a).append("]").toString();
    }
}
