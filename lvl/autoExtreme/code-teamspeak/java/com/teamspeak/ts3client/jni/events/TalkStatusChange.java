package com.teamspeak.ts3client.jni.events;
public class TalkStatusChange implements com.teamspeak.ts3client.jni.k {
    public long a;
    public int b;
    private int c;
    private int d;

    public TalkStatusChange()
    {
        return;
    }

    private TalkStatusChange(long p2, int p4, int p5, int p6)
    {
        this.a = p2;
        this.b = p4;
        this.c = p5;
        this.d = p6;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long d()
    {
        return this.a;
    }

    public final int a()
    {
        return this.d;
    }

    public final int b()
    {
        return this.c;
    }

    public final int c()
    {
        return this.b;
    }

    public String toString()
    {
        return new StringBuilder("TalkStatusChange [serverConnectionHandlerID=").append(this.a).append(", status=").append(this.b).append(", isReceivedWhisper=").append(this.c).append(", clientID=").append(this.d).append("]").toString();
    }
}
