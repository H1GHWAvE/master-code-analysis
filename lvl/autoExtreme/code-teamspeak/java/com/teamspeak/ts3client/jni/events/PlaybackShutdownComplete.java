package com.teamspeak.ts3client.jni.events;
public class PlaybackShutdownComplete implements com.teamspeak.ts3client.jni.k {
    private long a;

    public PlaybackShutdownComplete()
    {
        return;
    }

    private PlaybackShutdownComplete(long p2)
    {
        this.a = p2;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("PlaybackShutdownComplete [serverConnectionHandlerID=").append(this.a).append("]").toString();
    }
}
