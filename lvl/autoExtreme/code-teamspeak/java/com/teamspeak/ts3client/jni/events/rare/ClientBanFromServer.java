package com.teamspeak.ts3client.jni.events.rare;
public class ClientBanFromServer implements com.teamspeak.ts3client.jni.k {
    private long a;
    private int b;
    private long c;
    private long d;
    private com.teamspeak.ts3client.jni.j e;
    private int f;
    private String g;
    private String h;
    private long i;
    private String j;

    public ClientBanFromServer()
    {
        return;
    }

    private ClientBanFromServer(long p7, int p9, long p10, long p12, int p14, int p15, String p16, String p17, long p18, String p20)
    {
        this.a = p7;
        this.b = p9;
        this.c = p10;
        this.d = p12;
        if (p14 == 0) {
            this.e = com.teamspeak.ts3client.jni.j.a;
        }
        if (p14 == 1) {
            this.e = com.teamspeak.ts3client.jni.j.b;
        }
        if (p14 == 2) {
            this.e = com.teamspeak.ts3client.jni.j.c;
        }
        this.f = p15;
        this.g = p16;
        this.h = p17;
        this.i = p18;
        this.j = p20;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private int f()
    {
        return this.f;
    }

    private String g()
    {
        return this.h;
    }

    private long h()
    {
        return this.d;
    }

    private long i()
    {
        return this.a;
    }

    private com.teamspeak.ts3client.jni.j j()
    {
        return this.e;
    }

    public final int a()
    {
        return this.b;
    }

    public final String b()
    {
        return this.g;
    }

    public final String c()
    {
        return this.j;
    }

    public final long d()
    {
        return this.c;
    }

    public final long e()
    {
        return this.i;
    }

    public String toString()
    {
        return new StringBuilder("ClientBanFromServer [serverConnectionHandlerID=").append(this.a).append(", clientID=").append(this.b).append(", oldChannelID=").append(this.c).append(", newChannelID=").append(this.d).append(", visibility=").append(this.e).append(", kickerID=").append(this.f).append(", kickerName=").append(this.g).append(", kickerUniqueIdentifier=").append(this.h).append(", time=").append(this.i).append(", kickMessage=").append(this.j).append("]").toString();
    }
}
