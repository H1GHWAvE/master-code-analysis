package com.teamspeak.ts3client.jni.events.rare;
public class ServerTemporaryPasswordList implements com.teamspeak.ts3client.jni.k {
    public long a;
    public String b;
    public String c;
    public String d;
    public String e;
    public long f;
    public long g;
    public long h;
    public String i;

    public ServerTemporaryPasswordList()
    {
        return;
    }

    private ServerTemporaryPasswordList(long p2, String p4, String p5, String p6, String p7, long p8, long p10, long p12, String p14)
    {
        this.a = p2;
        this.b = p4;
        this.c = p5;
        this.d = p6;
        this.e = p7;
        this.f = p8;
        this.g = p10;
        this.h = p12;
        this.i = p14;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private String a()
    {
        return this.b;
    }

    private String b()
    {
        return this.d;
    }

    private String c()
    {
        return this.e;
    }

    private long d()
    {
        return this.a;
    }

    private long e()
    {
        return this.h;
    }

    private String f()
    {
        return this.i;
    }

    private long g()
    {
        return this.g;
    }

    private long h()
    {
        return this.f;
    }

    private String i()
    {
        return this.c;
    }

    public String toString()
    {
        return new StringBuilder("ServerTemporaryPasswordList [serverConnectionHandlerID=").append(this.a).append(", clientNickname=").append(this.b).append(", uniqueClientIdentifier=").append(this.c).append(", description=").append(this.d).append(", password=").append(this.e).append(", timestampStart=").append(this.f).append(", timestampEnd=").append(this.g).append(", targetChannelID=").append(this.h).append(", targetChannelPW=").append(this.i).append("]").toString();
    }
}
