package com.teamspeak.ts3client.jni.events;
public class UserLoggingMessage implements com.teamspeak.ts3client.jni.k {
    private String a;
    private int b;
    private String c;
    private long d;
    private String e;
    private String f;

    public UserLoggingMessage()
    {
        return;
    }

    private UserLoggingMessage(String p1, int p2, String p3, long p4, String p6, String p7)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        this.d = p4;
        this.e = p6;
        this.f = p7;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private String a()
    {
        return this.f;
    }

    private String b()
    {
        return this.c;
    }

    private long c()
    {
        return this.d;
    }

    private int d()
    {
        return this.b;
    }

    private String e()
    {
        return this.a;
    }

    private String f()
    {
        return this.e;
    }

    public String toString()
    {
        return new StringBuilder("UserLoggingMessage [logMessage=").append(this.a).append(", logLevel=").append(this.b).append(", logChannel=").append(this.c).append(", logID=").append(this.d).append(", logTime=").append(this.e).append(", completeLogString=").append(this.f).append("]").toString();
    }
}
