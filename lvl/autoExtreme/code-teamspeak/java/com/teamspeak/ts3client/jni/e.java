package com.teamspeak.ts3client.jni;
public final enum class e extends java.lang.Enum {
    public static final enum com.teamspeak.ts3client.jni.e a;
    public static final enum com.teamspeak.ts3client.jni.e b;
    public static final enum com.teamspeak.ts3client.jni.e c;
    public static final enum com.teamspeak.ts3client.jni.e d;
    public static final enum com.teamspeak.ts3client.jni.e e;
    private static final synthetic com.teamspeak.ts3client.jni.e[] g;
    private int f;

    static e()
    {
        com.teamspeak.ts3client.jni.e.a = new com.teamspeak.ts3client.jni.e("STATUS_DISCONNECTED", 0, 0);
        com.teamspeak.ts3client.jni.e.b = new com.teamspeak.ts3client.jni.e("STATUS_CONNECTING", 1, 1);
        com.teamspeak.ts3client.jni.e.c = new com.teamspeak.ts3client.jni.e("STATUS_CONNECTED", 2, 2);
        com.teamspeak.ts3client.jni.e.d = new com.teamspeak.ts3client.jni.e("STATUS_CONNECTION_ESTABLISHING", 3, 3);
        com.teamspeak.ts3client.jni.e.e = new com.teamspeak.ts3client.jni.e("STATUS_CONNECTION_ESTABLISHED", 4, 4);
        com.teamspeak.ts3client.jni.e[] v0_11 = new com.teamspeak.ts3client.jni.e[5];
        v0_11[0] = com.teamspeak.ts3client.jni.e.a;
        v0_11[1] = com.teamspeak.ts3client.jni.e.b;
        v0_11[2] = com.teamspeak.ts3client.jni.e.c;
        v0_11[3] = com.teamspeak.ts3client.jni.e.d;
        v0_11[4] = com.teamspeak.ts3client.jni.e.e;
        com.teamspeak.ts3client.jni.e.g = v0_11;
        return;
    }

    private e(String p1, int p2, int p3)
    {
        this(p1, p2);
        this.f = p3;
        return;
    }

    public static com.teamspeak.ts3client.jni.e valueOf(String p1)
    {
        return ((com.teamspeak.ts3client.jni.e) Enum.valueOf(com.teamspeak.ts3client.jni.e, p1));
    }

    public static com.teamspeak.ts3client.jni.e[] values()
    {
        return ((com.teamspeak.ts3client.jni.e[]) com.teamspeak.ts3client.jni.e.g.clone());
    }

    public final int a()
    {
        return this.f;
    }
}
