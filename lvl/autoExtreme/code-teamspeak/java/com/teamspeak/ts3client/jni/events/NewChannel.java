package com.teamspeak.ts3client.jni.events;
public class NewChannel implements com.teamspeak.ts3client.jni.k {
    long a;
    long b;
    long c;

    public NewChannel()
    {
        return;
    }

    private NewChannel(long p2, long p4, long p6)
    {
        this.a = p2;
        this.b = p4;
        this.c = p6;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.b;
    }

    private long b()
    {
        return this.c;
    }

    private long c()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("NewChannel [serverConnectionHandlerID=").append(this.a).append(", channelID=").append(this.b).append(", channelParentID=").append(this.c).append("]").toString();
    }
}
