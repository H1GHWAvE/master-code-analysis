package com.teamspeak.ts3client.jni.events.rare;
public class ClientChannelGroupChanged implements com.teamspeak.ts3client.jni.k {
    private long a;
    private long b;
    private long c;
    private int d;
    private int e;
    private String f;
    private String g;

    public ClientChannelGroupChanged()
    {
        return;
    }

    private ClientChannelGroupChanged(long p2, long p4, long p6, int p8, int p9, String p10, String p11)
    {
        this.a = p2;
        this.b = p4;
        this.c = p6;
        this.d = p8;
        this.e = p9;
        this.f = p10;
        this.g = p11;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long e()
    {
        return this.c;
    }

    private String f()
    {
        return this.g;
    }

    private long g()
    {
        return this.a;
    }

    public final long a()
    {
        return this.b;
    }

    public final int b()
    {
        return this.d;
    }

    public final int c()
    {
        return this.e;
    }

    public final String d()
    {
        return this.f;
    }

    public String toString()
    {
        return new StringBuilder("ClientChannelGroupChanged [serverConnectionHandlerID=").append(this.a).append(", channelGroupID=").append(this.b).append(", channelID=").append(this.c).append(", clientID=").append(this.d).append(", invokerClientID=").append(this.e).append(", invokerName=").append(this.f).append(", invokerUniqueIdentity=").append(this.g).append("]").toString();
    }
}
