package com.teamspeak.ts3client.tsdns;
public final class h {
    public String a;
    int b;
    public int c;
    public int d;
    int e;
    String f;

    public h(com.teamspeak.ts3client.tsdns.h p8)
    {
        this(p8.a, p8.c, p8.b, p8.d, p8.e, 1);
        return;
    }

    public h(String p3, int p4, int p5, int p6, int p7, boolean p8)
    {
        this.f = p3;
        this.e = p7;
        try {
            if (!p3.startsWith("_ts3._udp.")) {
                if (p3.startsWith("_tsdns._tcp.")) {
                    p3 = p3.replace("_tsdns._tcp.", "");
                }
                if (p3.endsWith(".")) {
                    p3 = p3.substring(0, (p3.length() - 1));
                }
                if (!p8) {
                    this.a = p3;
                } else {
                    this.a = org.xbill.DNS.Address.getByName(p3).getHostAddress();
                }
            } else {
                p3 = p3.replace("_ts3._udp.", "");
            }
        } catch (String v0) {
            this.a = p3;
            p6 = com.teamspeak.ts3client.tsdns.f.c;
        }
        this.c = p4;
        this.b = p5;
        this.d = p6;
        return;
    }

    private int a()
    {
        return this.e;
    }

    private void a(int p1)
    {
        this.e = p1;
        return;
    }

    private void a(String p1)
    {
        this.a = p1;
        return;
    }

    private int b()
    {
        return this.b;
    }

    private void b(int p1)
    {
        this.b = p1;
        return;
    }

    private int c()
    {
        return this.c;
    }

    private void c(int p1)
    {
        this.c = p1;
        return;
    }

    private String d()
    {
        return this.a;
    }

    private void d(int p1)
    {
        this.d = p1;
        return;
    }

    private int e()
    {
        return this.d;
    }

    private String f()
    {
        return this.f;
    }
}
