package com.teamspeak.ts3client.tsdns;
public final class j extends java.lang.Thread {
    public static boolean a;
    private String b;
    private int c;
    private int d;
    private int e;
    private com.teamspeak.ts3client.tsdns.k f;
    private String g;
    private int h;

    static j()
    {
        com.teamspeak.ts3client.tsdns.j.a = 1;
        return;
    }

    public j(String p2, String p3, int p4, int p5, int p6, com.teamspeak.ts3client.tsdns.k p7)
    {
        this.h = 3000;
        this.g = p3;
        this.b = p2;
        this.c = p4;
        this.d = p5;
        this.e = p6;
        this.f = p7;
        if (p7.b()) {
            this.h = 6000;
        }
        p7.a(this);
        return;
    }

    private String a(String p7, String p8, int p9)
    {
        try {
            if (!p8.endsWith(".")) {
                boolean v1_3 = new java.net.Socket();
                v1_3.connect(new java.net.InetSocketAddress(p7, p9), this.h);
                java.io.PrintStream v2_2 = new java.io.PrintStream(v1_3.getOutputStream());
                v2_2.print(p8);
                java.io.BufferedReader v3_1 = new java.io.BufferedReader(new java.io.InputStreamReader(v1_3.getInputStream(), "UTF-8"));
                String v0_8 = v3_1.readLine();
                v3_1.close();
                v2_2.close();
                v1_3.close();
                if (v0_8.equals("404")) {
                    v0_8 = "ERROR2";
                }
            } else {
                p8 = p8.substring(0, (p8.length() - 1));
            }
        } catch (String v0) {
            v0_8 = "ERROR1";
        }
        return v0_8;
    }

    public final void run()
    {
        if (this.d == com.teamspeak.ts3client.tsdns.f.h) {
            try {
                int v3_1 = org.xbill.DNS.Address.getByName(this.b);
                this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> TS_TYPE_DIRECT: Result for ").append(this.b).append(": ").append(v3_1.getHostAddress()).toString());
                this.f.a(new com.teamspeak.ts3client.tsdns.h(v3_1.getHostAddress(), this.c, this.e, com.teamspeak.ts3client.tsdns.f.a, this.d, 0));
            } catch (com.teamspeak.ts3client.tsdns.h v2) {
                this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> ERROR TS_TYPE_DIRECT: ").append(this.b).toString());
                this.f.a(new com.teamspeak.ts3client.tsdns.h(this.b, this.c, this.e, com.teamspeak.ts3client.tsdns.f.c, this.d, 0));
            }
        }
        if (this.d == com.teamspeak.ts3client.tsdns.f.g) {
            int v3_7 = this.a(this.g, this.b, 41144);
            if (!v3_7.equals("ERROR1")) {
                if (!v3_7.equals("ERROR2")) {
                    if (v3_7.contains(":")) {
                        com.teamspeak.ts3client.tsdns.h v2_19 = v3_7.split(":");
                        if ((v2_19[1] != null) && (!v2_19[1].contains("$PORT"))) {
                            this.c = Integer.parseInt(v2_19[1]);
                        }
                        v3_7 = v2_19[0];
                    }
                    this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> TS_TYPE_TSDNS: Result for ").append(this.b).append(": ").append(v3_7).toString());
                    this.f.a(new com.teamspeak.ts3client.tsdns.h(v3_7, this.c, this.e, com.teamspeak.ts3client.tsdns.f.a, this.d, 0));
                } else {
                    this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> ERROR 404 TS_TYPE_TSDNS: ").append(this.b).toString());
                    this.f.a(new com.teamspeak.ts3client.tsdns.h(this.b, this.c, this.e, com.teamspeak.ts3client.tsdns.f.b, this.d, 0));
                }
            } else {
                this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> ERROR TS_TYPE_TSDNS: ").append(this.b).toString());
                this.f.a(new com.teamspeak.ts3client.tsdns.h(this.b, this.c, this.e, com.teamspeak.ts3client.tsdns.f.c, this.d, 0));
            }
        }
        if (this.d != com.teamspeak.ts3client.tsdns.f.f) {
            if (this.d == com.teamspeak.ts3client.tsdns.f.e) {
                this.b = new StringBuilder("_ts3._udp.").append(this.b).toString();
                try {
                    com.teamspeak.ts3client.tsdns.k v9_11 = new org.xbill.DNS.Lookup(this.b, 33).run();
                } catch (com.teamspeak.ts3client.tsdns.h v2) {
                    this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> ERROR TS_TYPE_SRVTS3: ").append(this.b).toString());
                    this.f.a(new com.teamspeak.ts3client.tsdns.h(this.b, this.c, this.e, com.teamspeak.ts3client.tsdns.f.c, this.d, 0));
                }
                if ((v9_11 != null) && (v9_11.length != 0)) {
                    if (v9_11.length == 1) {
                        this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> TS_TYPE_SRVTS3: Result for ").append(this.b).append(": ").append(((org.xbill.DNS.SRVRecord) v9_11[0]).getTarget().toString()).append(":").append(((org.xbill.DNS.SRVRecord) v9_11[0]).getPort()).toString());
                        this.f.a(new com.teamspeak.ts3client.tsdns.h(((org.xbill.DNS.SRVRecord) v9_11[0]).getTarget().toString(), ((org.xbill.DNS.SRVRecord) v9_11[0]).getPort(), this.e, com.teamspeak.ts3client.tsdns.f.a, this.d, 0));
                    }
                    if (v9_11.length > 1) {
                        double v10_9 = new java.util.ArrayList();
                        int v5_82 = ((org.xbill.DNS.SRVRecord) v9_11[0]).getPriority();
                        String v4_87 = 0;
                        int v11_1 = v9_11.length;
                        int v8_16 = 0;
                        while (v8_16 < v11_1) {
                            com.teamspeak.ts3client.tsdns.h v2_171;
                            int v3_85;
                            int v3_84 = v9_11[v8_16];
                            if (((org.xbill.DNS.SRVRecord) v3_84).getPriority() > v5_82) {
                                v2_171 = v4_87;
                                v3_85 = v5_82;
                            } else {
                                if (((org.xbill.DNS.SRVRecord) v3_84).getPriority() != v5_82) {
                                    if (((org.xbill.DNS.SRVRecord) v3_84).getPriority() >= v5_82) {
                                    } else {
                                        v10_9.clear();
                                        v10_9.add(v3_84);
                                        v2_171 = ((org.xbill.DNS.SRVRecord) v3_84).getWeight();
                                        v3_85 = ((org.xbill.DNS.SRVRecord) v3_84).getPriority();
                                    }
                                } else {
                                    v10_9.add(v3_84);
                                    v2_171 = (((org.xbill.DNS.SRVRecord) v3_84).getWeight() + v4_87);
                                    v3_85 = v5_82;
                                }
                            }
                            v8_16++;
                            v5_82 = v3_85;
                            v4_87 = v2_171;
                        }
                        double v12_1 = Math.random();
                        int v5_83 = v10_9.iterator();
                        int v8_17 = 0;
                        while (v5_83.hasNext()) {
                            int v3_82 = ((org.xbill.DNS.Record) v5_83.next());
                            v8_17 += (((double) ((org.xbill.DNS.SRVRecord) v3_82).getWeight()) / ((double) v4_87));
                            if (v12_1 <= v8_17) {
                                com.teamspeak.ts3client.tsdns.h v2_160 = ((org.xbill.DNS.SRVRecord) v3_82).getTarget().toString();
                                String v4_88 = ((org.xbill.DNS.SRVRecord) v3_82).getPort();
                                int v3_80 = v2_160;
                            }
                            this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> TS_TYPE_SRVTS3: Result for ").append(this.b).append(": ").append(v3_80).append(":").append(v4_88).toString());
                            this.f.a(new com.teamspeak.ts3client.tsdns.h(v3_80, v4_88, this.e, com.teamspeak.ts3client.tsdns.f.a, this.d, 0));
                        }
                        v4_88 = 0;
                        v3_80 = "";
                    }
                } else {
                    this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> ERROR TS_TYPE_SRVTS3: ").append(this.b).toString());
                    this.f.a(new com.teamspeak.ts3client.tsdns.h(this.b, this.c, this.e, com.teamspeak.ts3client.tsdns.f.c, this.d, 0));
                }
            }
        } else {
            this.b = new StringBuilder("_tsdns._tcp.").append(this.b).toString();
            try {
                com.teamspeak.ts3client.tsdns.k v9_5 = new org.xbill.DNS.Lookup(this.b, 33).run();
            } catch (com.teamspeak.ts3client.tsdns.h v2) {
                this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> ERROR TS_TYPE_SRVTSDNS: ").append(this.b).toString());
                this.f.a(new com.teamspeak.ts3client.tsdns.h(this.b, this.c, this.e, com.teamspeak.ts3client.tsdns.f.c, this.d, 0));
            }
            if ((v9_5 != null) && (v9_5.length != 0)) {
                if (v9_5.length == 1) {
                    int v3_27 = this.a(((org.xbill.DNS.SRVRecord) v9_5[0]).getTarget().toString(), this.g, ((org.xbill.DNS.SRVRecord) v9_5[0]).getPort());
                    if (!v3_27.equals("ERROR1")) {
                        if (!v3_27.equals("ERROR2")) {
                            if (v3_27.contains(":")) {
                                com.teamspeak.ts3client.tsdns.h v2_56 = v3_27.split(":");
                                if ((v2_56[1] != null) && (!v2_56[1].contains("$PORT"))) {
                                    this.c = Integer.parseInt(v2_56[1]);
                                }
                                v3_27 = v2_56[0];
                            }
                            this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> TS_TYPE_SRVTSDNS: Result for ").append(this.b).append(": ").append(v3_27).toString());
                            this.f.a(new com.teamspeak.ts3client.tsdns.h(v3_27, this.c, this.e, com.teamspeak.ts3client.tsdns.f.a, this.d, 0));
                        } else {
                            this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> ERROR 404 TS_TYPE_SRVTSDNS: ").append(this.b).toString());
                            this.f.a(new com.teamspeak.ts3client.tsdns.h(this.b, this.c, this.e, com.teamspeak.ts3client.tsdns.f.b, this.d, 0));
                        }
                    } else {
                        this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> ERROR TS_TYPE_SRVTSDNS: ").append(this.b).toString());
                        this.f.a(new com.teamspeak.ts3client.tsdns.h(this.b, this.c, this.e, com.teamspeak.ts3client.tsdns.f.c, this.d, 0));
                    }
                }
                if (v9_5.length <= 1) {
                } else {
                    double v10_4 = new java.util.ArrayList();
                    int v5_47 = ((org.xbill.DNS.SRVRecord) v9_5[0]).getPriority();
                    String v4_46 = 0;
                    int v11_0 = v9_5.length;
                    int v8_8 = 0;
                    while (v8_8 < v11_0) {
                        int v3_61;
                        com.teamspeak.ts3client.tsdns.h v2_108;
                        int v3_60 = v9_5[v8_8];
                        if (((org.xbill.DNS.SRVRecord) v3_60).getPriority() > v5_47) {
                            v2_108 = v4_46;
                            v3_61 = v5_47;
                        } else {
                            if (((org.xbill.DNS.SRVRecord) v3_60).getPriority() != v5_47) {
                                if (((org.xbill.DNS.SRVRecord) v3_60).getPriority() >= v5_47) {
                                } else {
                                    v10_4.clear();
                                    v10_4.add(v3_60);
                                    v2_108 = ((org.xbill.DNS.SRVRecord) v3_60).getWeight();
                                    v3_61 = ((org.xbill.DNS.SRVRecord) v3_60).getPriority();
                                }
                            } else {
                                v10_4.add(v3_60);
                                v2_108 = (((org.xbill.DNS.SRVRecord) v3_60).getWeight() + v4_46);
                                v3_61 = v5_47;
                            }
                        }
                        v8_8++;
                        v5_47 = v3_61;
                        v4_46 = v2_108;
                    }
                    double v12_0 = Math.random();
                    int v5_48 = v10_4.iterator();
                    int v8_9 = 0;
                    while (v5_48.hasNext()) {
                        int v3_44 = ((org.xbill.DNS.Record) v5_48.next());
                        v8_9 += (((double) ((org.xbill.DNS.SRVRecord) v3_44).getWeight()) / ((double) v4_46));
                        if (v12_0 <= v8_9) {
                            String v4_47 = ((org.xbill.DNS.SRVRecord) v3_44).getTarget().toString();
                            com.teamspeak.ts3client.tsdns.h v2_76 = ((org.xbill.DNS.SRVRecord) v3_44).getPort();
                            int v3_42 = v4_47;
                        }
                        int v3_46 = this.a(v3_42, this.g, v2_76);
                        if (!v3_46.equals("ERROR1")) {
                            if (!v3_46.equals("ERROR2")) {
                                if (v3_46.contains(":")) {
                                    com.teamspeak.ts3client.tsdns.h v2_89 = v3_46.split(":");
                                    if ((v2_89[1] != null) && (!v2_89[1].contains("$PORT"))) {
                                        this.c = Integer.parseInt(v2_89[1]);
                                    }
                                    v3_46 = v2_89[0];
                                }
                                this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> TS_TYPE_SRVTSDNS: Result for ").append(this.b).append(": ").append(v3_46).toString());
                                this.f.a(new com.teamspeak.ts3client.tsdns.h(v3_46, this.c, this.e, com.teamspeak.ts3client.tsdns.f.a, this.d, 0));
                            } else {
                                this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> ERROR 404 TS_TYPE_SRVTSDNS: ").append(this.b).toString());
                                this.f.a(new com.teamspeak.ts3client.tsdns.h(this.b, this.c, this.e, com.teamspeak.ts3client.tsdns.f.b, this.d, 0));
                            }
                        } else {
                            this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> ERROR TS_TYPE_SRVTSDNS: ").append(this.b).toString());
                            this.f.a(new com.teamspeak.ts3client.tsdns.h(this.b, this.c, this.e, com.teamspeak.ts3client.tsdns.f.c, this.d, 0));
                        }
                    }
                    v2_76 = 0;
                    v3_42 = "";
                }
            } else {
                this.f.c().log(java.util.logging.Level.INFO, new StringBuilder().append(this.e).append(" -> ERROR TS_TYPE_SRVTSDNS: ").append(this.b).toString());
                this.f.a(new com.teamspeak.ts3client.tsdns.h(this.b, this.c, this.e, com.teamspeak.ts3client.tsdns.f.c, this.d, 0));
            }
        }
        return;
    }
}
