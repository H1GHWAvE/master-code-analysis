package com.a.c.c;
public final class a {
    public final Class a;
    public final reflect.Type b;
    final int c;

    protected a()
    {
        int v0_1 = this.getClass().getGenericSuperclass();
        if (!(v0_1 instanceof Class)) {
            this.b = com.a.c.b.b.a(((reflect.ParameterizedType) v0_1).getActualTypeArguments()[0]);
            this.a = com.a.c.b.b.b(this.b);
            this.c = this.b.hashCode();
            return;
        } else {
            throw new RuntimeException("Missing type parameter.");
        }
    }

    private a(reflect.Type p2)
    {
        this.b = com.a.c.b.b.a(((reflect.Type) com.a.c.b.a.a(p2)));
        this.a = com.a.c.b.b.b(this.b);
        this.c = this.b.hashCode();
        return;
    }

    public static com.a.c.c.a a(Class p1)
    {
        return new com.a.c.c.a(p1);
    }

    public static com.a.c.c.a a(reflect.Type p1)
    {
        return new com.a.c.c.a(p1);
    }

    private static varargs AssertionError a(reflect.Type p4, Class[] p5)
    {
        String v1_1 = new StringBuilder("Unexpected type. Expected one of: ");
        AssertionError v0_1 = 0;
        while (v0_1 < 3) {
            v1_1.append(p5[v0_1].getName()).append(", ");
            v0_1++;
        }
        v1_1.append("but got: ").append(p4.getClass().getName()).append(", for type token: ").append(p4.toString()).append(46);
        return new AssertionError(v1_1.toString());
    }

    private Class a()
    {
        return this.a;
    }

    private boolean a(com.a.c.c.a p2)
    {
        return this.b(p2.b);
    }

    private static boolean a(reflect.ParameterizedType p8, reflect.ParameterizedType p9, java.util.Map p10)
    {
        int v2 = 0;
        if (p8.getRawType().equals(p9.getRawType())) {
            reflect.Type[] v4 = p8.getActualTypeArguments();
            reflect.Type[] v5 = p9.getActualTypeArguments();
            int v1_1 = 0;
            while (v1_1 < v4.length) {
                int v0_8;
                int v0_3 = v4[v1_1];
                reflect.Type v6 = v5[v1_1];
                if ((!v6.equals(v0_3)) && ((!(v0_3 instanceof reflect.TypeVariable)) || (!v6.equals(p10.get(((reflect.TypeVariable) v0_3).getName()))))) {
                    v0_8 = 0;
                } else {
                    v0_8 = 1;
                }
                if (v0_8 != 0) {
                    v1_1++;
                }
            }
            v2 = 1;
        }
        return v2;
    }

    private static boolean a(reflect.Type p2, reflect.GenericArrayType p3)
    {
        boolean v0_1;
        boolean v0_0 = p3.getGenericComponentType();
        if (!(v0_0 instanceof reflect.ParameterizedType)) {
            v0_1 = 1;
        } else {
            if (!(p2 instanceof reflect.GenericArrayType)) {
                if ((p2 instanceof Class)) {
                    p2 = ((Class) p2);
                    while (p2.isArray()) {
                        p2 = p2.getComponentType();
                    }
                }
            } else {
                p2 = ((reflect.GenericArrayType) p2).getGenericComponentType();
            }
            v0_1 = com.a.c.c.a.a(p2, ((reflect.ParameterizedType) v0_0), new java.util.HashMap());
        }
        return v0_1;
    }

    private static boolean a(reflect.Type p10, reflect.ParameterizedType p11, java.util.Map p12)
    {
        int v2 = 0;
        int v0_0 = p10;
        while (v0_0 != 0) {
            if (!p11.equals(v0_0)) {
                reflect.Type[] v4_1;
                Class v5 = com.a.c.b.b.b(v0_0);
                if (!(v0_0 instanceof reflect.ParameterizedType)) {
                    v4_1 = 0;
                } else {
                    v4_1 = ((reflect.ParameterizedType) v0_0);
                }
                if (v4_1 != null) {
                    reflect.Type[] v6_0 = v4_1.getActualTypeArguments();
                    reflect.Type v7_0 = v5.getTypeParameters();
                    int v1_2 = 0;
                    while (v1_2 < v6_0.length) {
                        int v0_17 = v6_0[v1_2];
                        while ((v0_17 instanceof reflect.TypeVariable)) {
                            v0_17 = ((reflect.Type) p12.get(((reflect.TypeVariable) v0_17).getName()));
                        }
                        p12.put(v7_0[v1_2].getName(), v0_17);
                        v1_2++;
                    }
                    int v0_6;
                    if (!v4_1.getRawType().equals(p11.getRawType())) {
                        v0_6 = 0;
                    } else {
                        reflect.Type[] v4_2 = v4_1.getActualTypeArguments();
                        reflect.Type[] v6_1 = p11.getActualTypeArguments();
                        int v1_4 = 0;
                        while (v1_4 < v4_2.length) {
                            int v0_12;
                            int v0_7 = v4_2[v1_4];
                            reflect.Type v7_1 = v6_1[v1_4];
                            if ((!v7_1.equals(v0_7)) && ((!(v0_7 instanceof reflect.TypeVariable)) || (!v7_1.equals(p12.get(((reflect.TypeVariable) v0_7).getName()))))) {
                                v0_12 = 0;
                            } else {
                                v0_12 = 1;
                            }
                            if (v0_12 == 0) {
                            } else {
                                v1_4++;
                            }
                        }
                        v0_6 = 1;
                    }
                    if (v0_6 != 0) {
                        v2 = 1;
                        break;
                    }
                }
                int v1_5 = v5.getGenericInterfaces();
                int v0_13 = 0;
                while (v0_13 < v1_5.length) {
                    if (!com.a.c.c.a.a(v1_5[v0_13], p11, new java.util.HashMap(p12))) {
                        v0_13++;
                    } else {
                        v2 = 1;
                    }
                }
                p12 = new java.util.HashMap(p12);
                v0_0 = v5.getGenericSuperclass();
            } else {
                v2 = 1;
                break;
            }
        }
        return v2;
    }

    private static boolean a(reflect.Type p1, reflect.Type p2, java.util.Map p3)
    {
        if ((!p2.equals(p1)) && ((!(p1 instanceof reflect.TypeVariable)) || (!p2.equals(p3.get(((reflect.TypeVariable) p1).getName()))))) {
            int v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    private reflect.Type b()
    {
        return this.b;
    }

    private static reflect.Type b(Class p2)
    {
        reflect.Type v0_0 = p2.getGenericSuperclass();
        if (!(v0_0 instanceof Class)) {
            return com.a.c.b.b.a(((reflect.ParameterizedType) v0_0).getActualTypeArguments()[0]);
        } else {
            throw new RuntimeException("Missing type parameter.");
        }
    }

    private boolean b(reflect.Type p6)
    {
        Class v0_15;
        if (p6 != null) {
            if (!this.b.equals(p6)) {
                if (!(this.b instanceof Class)) {
                    if (!(this.b instanceof reflect.ParameterizedType)) {
                        if (!(this.b instanceof reflect.GenericArrayType)) {
                            Class v0_8 = this.b;
                            Class[] v3_1 = new Class[3];
                            v3_1[0] = Class;
                            v3_1[1] = reflect.ParameterizedType;
                            v3_1[2] = reflect.GenericArrayType;
                            throw com.a.c.c.a.a(v0_8, v3_1);
                        } else {
                            if ((!this.a.isAssignableFrom(com.a.c.b.b.b(p6))) || (!com.a.c.c.a.a(p6, ((reflect.GenericArrayType) this.b)))) {
                                v0_15 = 0;
                            } else {
                                v0_15 = 1;
                            }
                        }
                    } else {
                        v0_15 = com.a.c.c.a.a(p6, ((reflect.ParameterizedType) this.b), new java.util.HashMap());
                    }
                } else {
                    v0_15 = this.a.isAssignableFrom(com.a.c.b.b.b(p6));
                }
            } else {
                v0_15 = 1;
            }
        } else {
            v0_15 = 0;
        }
        return v0_15;
    }

    private boolean c(Class p2)
    {
        return this.b(p2);
    }

    public final boolean equals(Object p3)
    {
        if ((!(p3 instanceof com.a.c.c.a)) || (!com.a.c.b.b.a(this.b, ((com.a.c.c.a) p3).b))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final int hashCode()
    {
        return this.c;
    }

    public final String toString()
    {
        return com.a.c.b.b.c(this.b);
    }
}
