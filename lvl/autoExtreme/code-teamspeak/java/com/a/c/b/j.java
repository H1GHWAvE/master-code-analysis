package com.a.c.b;
final class j implements com.a.c.b.ao {
    final synthetic Class a;
    final synthetic reflect.Type b;
    final synthetic com.a.c.b.f c;
    private final com.a.c.b.au d;

    j(com.a.c.b.f p2, Class p3, reflect.Type p4)
    {
        this.c = p2;
        this.a = p3;
        this.b = p4;
        this.d = com.a.c.b.au.a();
        return;
    }

    public final Object a()
    {
        try {
            return this.d.a(this.a);
        } catch (Exception v0_2) {
            throw new RuntimeException(new StringBuilder("Unable to invoke no-args constructor for ").append(this.b).append(". Register an InstanceCreator with Gson for this type may fix this problem.").toString(), v0_2);
        }
    }
}
