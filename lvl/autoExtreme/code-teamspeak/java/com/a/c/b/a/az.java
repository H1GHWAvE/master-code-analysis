package com.a.c.b.a;
final class az implements com.a.c.ap {
    final synthetic Class a;
    final synthetic com.a.c.an b;

    az(Class p1, com.a.c.an p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final com.a.c.an a(com.a.c.k p3, com.a.c.c.a p4)
    {
        int v0_2;
        if (!this.a.isAssignableFrom(p4.a)) {
            v0_2 = 0;
        } else {
            v0_2 = this.b;
        }
        return v0_2;
    }

    public final String toString()
    {
        return new StringBuilder("Factory[typeHierarchy=").append(this.a.getName()).append(",adapter=").append(this.b).append("]").toString();
    }
}
