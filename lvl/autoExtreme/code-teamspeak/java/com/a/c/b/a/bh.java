package com.a.c.b.a;
final class bh extends com.a.c.an {
    private final java.util.Map a;
    private final java.util.Map b;

    public bh(Class p8)
    {
        this.a = new java.util.HashMap();
        this.b = new java.util.HashMap();
        try {
            AssertionError v0_5 = ((Enum[]) p8.getEnumConstants());
            int v4 = v0_5.length;
            int v3 = 0;
        } catch (AssertionError v0) {
            throw new AssertionError();
        }
        while (v3 < v4) {
            int v1_4;
            AssertionError v5 = v0_5[v3];
            java.util.Map v2_0 = v5.name();
            int v1_3 = ((com.a.c.a.c) p8.getField(v2_0).getAnnotation(com.a.c.a.c));
            if (v1_3 == 0) {
                v1_4 = v2_0;
            } else {
                v1_4 = v1_3.a();
            }
            this.a.put(v1_4, v5);
            this.b.put(v5, v1_4);
            v3++;
        }
        return;
    }

    private void a(com.a.c.d.e p2, Enum p3)
    {
        String v0_2;
        if (p3 != null) {
            v0_2 = ((String) this.b.get(p3));
        } else {
            v0_2 = 0;
        }
        p2.b(v0_2);
        return;
    }

    private Enum b(com.a.c.d.a p3)
    {
        Enum v0_3;
        if (p3.f() != com.a.c.d.d.i) {
            v0_3 = ((Enum) this.a.get(p3.i()));
        } else {
            p3.k();
            v0_3 = 0;
        }
        return v0_3;
    }

    public final synthetic Object a(com.a.c.d.a p3)
    {
        Enum v0_3;
        if (p3.f() != com.a.c.d.d.i) {
            v0_3 = ((Enum) this.a.get(p3.i()));
        } else {
            p3.k();
            v0_3 = 0;
        }
        return v0_3;
    }

    public final synthetic void a(com.a.c.d.e p2, Object p3)
    {
        String v0_2;
        if (((Enum) p3) != null) {
            v0_2 = ((String) this.b.get(((Enum) p3)));
        } else {
            v0_2 = 0;
        }
        p2.b(v0_2);
        return;
    }
}
