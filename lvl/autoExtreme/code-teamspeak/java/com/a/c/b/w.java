package com.a.c.b;
public final class w extends java.util.AbstractMap implements java.io.Serializable {
    static final synthetic boolean g;
    private static final java.util.Comparator h;
    java.util.Comparator a;
    com.a.c.b.af[] b;
    final com.a.c.b.af c;
    int d;
    int e;
    int f;
    private com.a.c.b.aa i;
    private com.a.c.b.ac j;

    static w()
    {
        com.a.c.b.x v0_2;
        if (com.a.c.b.w.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.a.c.b.w.g = v0_2;
        com.a.c.b.w.h = new com.a.c.b.x();
        return;
    }

    public w()
    {
        this(com.a.c.b.w.h);
        return;
    }

    private w(java.util.Comparator p3)
    {
        this.d = 0;
        this.e = 0;
        if (p3 == null) {
            p3 = com.a.c.b.w.h;
        }
        this.a = p3;
        this.c = new com.a.c.b.af();
        int v0_4 = new com.a.c.b.af[16];
        this.b = v0_4;
        this.f = ((this.b.length / 2) + (this.b.length / 4));
        return;
    }

    private static int a(int p2)
    {
        int v0_2 = (((p2 >> 20) ^ (p2 >> 12)) ^ p2);
        return ((v0_2 >> 4) ^ ((v0_2 >> 7) ^ v0_2));
    }

    private com.a.c.b.af a(Object p11, boolean p12)
    {
        int v6;
        ClassCastException v2_0 = 0;
        java.util.Comparator v7 = this.a;
        com.a.c.b.af[] v8 = this.b;
        ClassCastException v0_0 = p11.hashCode();
        ClassCastException v0_1 = (v0_0 ^ ((v0_0 >> 20) ^ (v0_0 >> 12)));
        int v3_1 = (((v0_1 >> 7) ^ v0_1) ^ (v0_1 >> 4));
        int v9 = (v3_1 & (v8.length - 1));
        com.a.c.b.af v1_4 = v8[v9];
        if (v1_4 == null) {
            v6 = 0;
            if (p12) {
                ClassCastException v0_10;
                int v4_3 = this.c;
                if (v1_4 != null) {
                    v0_10 = new com.a.c.b.af(v1_4, p11, v3_1, v4_3, v4_3.e);
                    if (v6 >= 0) {
                        v1_4.c = v0_10;
                    } else {
                        v1_4.b = v0_10;
                    }
                    this.b(v1_4, 1);
                } else {
                    if ((v7 != com.a.c.b.w.h) || ((p11 instanceof Comparable))) {
                        v0_10 = new com.a.c.b.af(v1_4, p11, v3_1, v4_3, v4_3.e);
                        v8[v9] = v0_10;
                    } else {
                        throw new ClassCastException(new StringBuilder().append(p11.getClass().getName()).append(" is not Comparable").toString());
                    }
                }
                com.a.c.b.af v1_5 = this.d;
                this.d = (v1_5 + 1);
                if (v1_5 > this.f) {
                    this.b = com.a.c.b.w.a(this.b);
                    this.f = ((this.b.length / 2) + (this.b.length / 4));
                }
                this.e = (this.e + 1);
                v2_0 = v0_10;
            }
        } else {
            ClassCastException v0_7;
            if (v7 != com.a.c.b.w.h) {
                v0_7 = 0;
            } else {
                v0_7 = ((Comparable) p11);
            }
            while(true) {
                int v4_1;
                if (v0_7 == null) {
                    v4_1 = v7.compare(p11, v1_4.f);
                } else {
                    v4_1 = v0_7.compareTo(v1_4.f);
                }
                if (v4_1 != 0) {
                    com.a.c.b.af v5_0;
                    if (v4_1 >= 0) {
                        v5_0 = v1_4.c;
                    } else {
                        v5_0 = v1_4.b;
                    }
                    if (v5_0 == null) {
                        break;
                    }
                    v1_4 = v5_0;
                } else {
                    v2_0 = v1_4;
                }
            }
            v6 = v4_1;
        }
        return v2_0;
    }

    private void a()
    {
        com.a.c.b.af[] v5 = this.b;
        int v6 = v5.length;
        com.a.c.b.af[] v7 = new com.a.c.b.af[(v6 * 2)];
        com.a.c.b.z v8_1 = new com.a.c.b.z();
        com.a.c.b.y v9_1 = new com.a.c.b.y();
        com.a.c.b.y v10_1 = new com.a.c.b.y();
        int v4 = 0;
        while (v4 < v6) {
            com.a.c.b.af v11_0 = v5[v4];
            if (v11_0 != null) {
                v8_1.a(v11_0);
                int v0_5 = 0;
                int v2_0 = 0;
                while(true) {
                    int v12_0 = v8_1.a();
                    if (v12_0 == 0) {
                        break;
                    }
                    if ((v12_0.g & v6) != 0) {
                        v0_5++;
                    } else {
                        v2_0++;
                    }
                }
                v9_1.a(v2_0);
                v10_1.a(v0_5);
                v8_1.a(v11_0);
                while(true) {
                    com.a.c.b.af v11_1 = v8_1.a();
                    if (v11_1 == null) {
                        break;
                    }
                    if ((v11_1.g & v6) != 0) {
                        v10_1.a(v11_1);
                    } else {
                        v9_1.a(v11_1);
                    }
                }
                int v2_1;
                if (v2_0 <= 0) {
                    v2_1 = 0;
                } else {
                    v2_1 = v9_1.a();
                }
                int v0_6;
                v7[v4] = v2_1;
                int v2_2 = (v4 + v6);
                if (v0_5 <= 0) {
                    v0_6 = 0;
                } else {
                    v0_6 = v10_1.a();
                }
                v7[v2_2] = v0_6;
            }
            v4++;
        }
        this.b = v7;
        this.f = ((this.b.length / 2) + (this.b.length / 4));
        return;
    }

    private void a(com.a.c.b.af p7)
    {
        int v1 = 0;
        int v0_0 = p7.b;
        com.a.c.b.af v3 = p7.c;
        com.a.c.b.af v4 = v3.b;
        com.a.c.b.af v5 = v3.c;
        p7.c = v4;
        if (v4 != null) {
            v4.a = p7;
        }
        int v2;
        this.a(p7, v3);
        v3.b = p7;
        p7.a = v3;
        if (v0_0 == 0) {
            v2 = 0;
        } else {
            v2 = v0_0.i;
        }
        int v0_2;
        if (v4 == null) {
            v0_2 = 0;
        } else {
            v0_2 = v4.i;
        }
        p7.i = (Math.max(v2, v0_2) + 1);
        if (v5 != null) {
            v1 = v5.i;
        }
        v3.i = (Math.max(p7.i, v1) + 1);
        return;
    }

    private void a(com.a.c.b.af p3, com.a.c.b.af p4)
    {
        AssertionError v0_0 = p3.a;
        p3.a = 0;
        if (p4 != null) {
            p4.a = v0_0;
        }
        if (v0_0 == null) {
            this.b[(p3.g & (this.b.length - 1))] = p4;
        } else {
            if (v0_0.b != p3) {
                if ((com.a.c.b.w.g) || (v0_0.c == p3)) {
                    v0_0.c = p4;
                } else {
                    throw new AssertionError();
                }
            } else {
                v0_0.b = p4;
            }
        }
        return;
    }

    private static boolean a(Object p1, Object p2)
    {
        if ((p1 != p2) && ((p1 == null) || (!p1.equals(p2)))) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static com.a.c.b.af[] a(com.a.c.b.af[] p12)
    {
        int v5 = p12.length;
        com.a.c.b.af[] v6 = new com.a.c.b.af[(v5 * 2)];
        com.a.c.b.z v7_1 = new com.a.c.b.z();
        com.a.c.b.y v8_1 = new com.a.c.b.y();
        com.a.c.b.y v9_1 = new com.a.c.b.y();
        int v4 = 0;
        while (v4 < v5) {
            com.a.c.b.af v10_0 = p12[v4];
            if (v10_0 != null) {
                v7_1.a(v10_0);
                int v0_1 = 0;
                int v2_0 = 0;
                while(true) {
                    int v11_0 = v7_1.a();
                    if (v11_0 == 0) {
                        break;
                    }
                    if ((v11_0.g & v5) != 0) {
                        v0_1++;
                    } else {
                        v2_0++;
                    }
                }
                v8_1.a(v2_0);
                v9_1.a(v0_1);
                v7_1.a(v10_0);
                while(true) {
                    com.a.c.b.af v10_1 = v7_1.a();
                    if (v10_1 == null) {
                        break;
                    }
                    if ((v10_1.g & v5) != 0) {
                        v9_1.a(v10_1);
                    } else {
                        v8_1.a(v10_1);
                    }
                }
                int v2_1;
                if (v2_0 <= 0) {
                    v2_1 = 0;
                } else {
                    v2_1 = v8_1.a();
                }
                int v0_2;
                v6[v4] = v2_1;
                int v2_2 = (v4 + v5);
                if (v0_1 <= 0) {
                    v0_2 = 0;
                } else {
                    v0_2 = v9_1.a();
                }
                v6[v2_2] = v0_2;
            }
            v4++;
        }
        return v6;
    }

    private com.a.c.b.af b(Object p3)
    {
        com.a.c.b.af v0 = 0;
        if (p3 != null) {
            try {
                v0 = this.a(p3, 0);
            } catch (ClassCastException v1) {
            }
        }
        return v0;
    }

    private Object b()
    {
        return new java.util.LinkedHashMap(this);
    }

    private void b(com.a.c.b.af p7)
    {
        int v1 = 0;
        com.a.c.b.af v3 = p7.b;
        int v0_0 = p7.c;
        com.a.c.b.af v4 = v3.b;
        com.a.c.b.af v5 = v3.c;
        p7.b = v5;
        if (v5 != null) {
            v5.a = p7;
        }
        int v2;
        this.a(p7, v3);
        v3.c = p7;
        p7.a = v3;
        if (v0_0 == 0) {
            v2 = 0;
        } else {
            v2 = v0_0.i;
        }
        int v0_2;
        if (v5 == null) {
            v0_2 = 0;
        } else {
            v0_2 = v5.i;
        }
        p7.i = (Math.max(v2, v0_2) + 1);
        if (v4 != null) {
            v1 = v4.i;
        }
        v3.i = (Math.max(p7.i, v1) + 1);
        return;
    }

    private void b(com.a.c.b.af p10, boolean p11)
    {
        while (p10 != null) {
            boolean v2_0;
            boolean v3_0 = p10.b;
            com.a.c.b.af v4_0 = p10.c;
            if (!v3_0) {
                v2_0 = 0;
            } else {
                v2_0 = v3_0.i;
            }
            AssertionError v0_1;
            if (v4_0 == null) {
                v0_1 = 0;
            } else {
                v0_1 = v4_0.i;
            }
            int v5 = (v2_0 - v0_1);
            if (v5 != -2) {
                if (v5 != 2) {
                    if (v5 != 0) {
                        if ((com.a.c.b.w.g) || ((v5 == -1) || (v5 == 1))) {
                            p10.i = (Math.max(v2_0, v0_1) + 1);
                            if (!p11) {
                                break;
                            }
                        } else {
                            throw new AssertionError();
                        }
                    } else {
                        p10.i = (v2_0 + 1);
                        if (p11) {
                            break;
                        }
                    }
                } else {
                    boolean v2_1;
                    com.a.c.b.af v4_2 = v3_0.b;
                    AssertionError v0_7 = v3_0.c;
                    if (v0_7 == null) {
                        v2_1 = 0;
                    } else {
                        v2_1 = v0_7.i;
                    }
                    AssertionError v0_9;
                    if (v4_2 == null) {
                        v0_9 = 0;
                    } else {
                        v0_9 = v4_2.i;
                    }
                    AssertionError v0_10 = (v0_9 - v2_1);
                    if ((v0_10 != 1) && ((v0_10 != null) || (p11))) {
                        if ((com.a.c.b.w.g) || (v0_10 == -1)) {
                            this.a(v3_0);
                            this.b(p10);
                        } else {
                            throw new AssertionError();
                        }
                    } else {
                        this.b(p10);
                    }
                    if (p11) {
                        break;
                    }
                }
            } else {
                boolean v2_3;
                boolean v3_2 = v4_0.b;
                AssertionError v0_13 = v4_0.c;
                if (v0_13 == null) {
                    v2_3 = 0;
                } else {
                    v2_3 = v0_13.i;
                }
                AssertionError v0_15;
                if (!v3_2) {
                    v0_15 = 0;
                } else {
                    v0_15 = v3_2.i;
                }
                AssertionError v0_16 = (v0_15 - v2_3);
                if ((v0_16 != -1) && ((v0_16 != null) || (p11))) {
                    if ((com.a.c.b.w.g) || (v0_16 == 1)) {
                        this.b(v4_0);
                        this.a(p10);
                    } else {
                        throw new AssertionError();
                    }
                } else {
                    this.a(p10);
                }
                if (p11) {
                    break;
                }
            }
            p10 = p10.a;
        }
        return;
    }

    final com.a.c.b.af a(Object p3)
    {
        com.a.c.b.af v0 = this.b(p3);
        if (v0 != null) {
            this.a(v0, 1);
        }
        return v0;
    }

    final com.a.c.b.af a(java.util.Map$Entry p6)
    {
        int v1 = 1;
        int v0_1 = this.b(p6.getKey());
        if (v0_1 == 0) {
            v1 = 0;
        } else {
            int v3_2;
            int v3_0 = v0_1.h;
            Object v4 = p6.getValue();
            if ((v3_0 != v4) && ((v3_0 == 0) || (!v3_0.equals(v4)))) {
                v3_2 = 0;
            } else {
                v3_2 = 1;
            }
            if (v3_2 == 0) {
            }
        }
        if (v1 == 0) {
            v0_1 = 0;
        }
        return v0_1;
    }

    final void a(com.a.c.b.af p8, boolean p9)
    {
        int v2 = 0;
        if (p9) {
            p8.e.d = p8.d;
            p8.d.e = p8.e;
            p8.e = 0;
            p8.d = 0;
        }
        int v1_2 = p8.b;
        com.a.c.b.af v0_2 = p8.c;
        com.a.c.b.af v3_0 = p8.a;
        if ((v1_2 == 0) || (v0_2 == null)) {
            if (v1_2 == 0) {
                if (v0_2 == null) {
                    this.a(p8, 0);
                } else {
                    this.a(p8, v0_2);
                    p8.c = 0;
                }
            } else {
                this.a(p8, v1_2);
                p8.b = 0;
            }
            this.b(v3_0, 0);
            this.d = (this.d - 1);
            this.e = (this.e + 1);
        } else {
            if (v1_2.i <= v0_2.i) {
                while(true) {
                    int v1_3 = v0_2.b;
                    if (v1_3 == 0) {
                        break;
                    }
                    v0_2 = v1_3;
                }
            } else {
                v0_2 = v1_2;
                int v1_4 = v1_2.c;
                while (v1_4 != 0) {
                    v0_2 = v1_4;
                    v1_4 = v1_4.c;
                }
            }
            int v1_5;
            this.a(v0_2, 0);
            com.a.c.b.af v3_2 = p8.b;
            if (v3_2 == null) {
                v1_5 = 0;
            } else {
                v1_5 = v3_2.i;
                v0_2.b = v3_2;
                v3_2.a = v0_2;
                p8.b = 0;
            }
            com.a.c.b.af v3_3 = p8.c;
            if (v3_3 != null) {
                v2 = v3_3.i;
                v0_2.c = v3_3;
                v3_3.a = v0_2;
                p8.c = 0;
            }
            v0_2.i = (Math.max(v1_5, v2) + 1);
            this.a(p8, v0_2);
        }
        return;
    }

    public final void clear()
    {
        java.util.Arrays.fill(this.b, 0);
        this.d = 0;
        this.e = (this.e + 1);
        com.a.c.b.af v2 = this.c;
        com.a.c.b.af v0_4 = v2.d;
        while (v0_4 != v2) {
            com.a.c.b.af v1 = v0_4.d;
            v0_4.e = 0;
            v0_4.d = 0;
            v0_4 = v1;
        }
        v2.e = v2;
        v2.d = v2;
        return;
    }

    public final boolean containsKey(Object p2)
    {
        int v0_1;
        if (this.b(p2) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final java.util.Set entrySet()
    {
        com.a.c.b.aa v0_0 = this.i;
        if (v0_0 == null) {
            v0_0 = new com.a.c.b.aa(this);
            this.i = v0_0;
        }
        return v0_0;
    }

    public final Object get(Object p2)
    {
        int v0_1;
        int v0_0 = this.b(p2);
        if (v0_0 == 0) {
            v0_1 = 0;
        } else {
            v0_1 = v0_0.h;
        }
        return v0_1;
    }

    public final java.util.Set keySet()
    {
        com.a.c.b.ac v0_0 = this.j;
        if (v0_0 == null) {
            v0_0 = new com.a.c.b.ac(this);
            this.j = v0_0;
        }
        return v0_0;
    }

    public final Object put(Object p3, Object p4)
    {
        if (p3 != null) {
            com.a.c.b.af v0_1 = this.a(p3, 1);
            Object v1_0 = v0_1.h;
            v0_1.h = p4;
            return v1_0;
        } else {
            throw new NullPointerException("key == null");
        }
    }

    public final Object remove(Object p2)
    {
        int v0_1;
        int v0_0 = this.a(p2);
        if (v0_0 == 0) {
            v0_1 = 0;
        } else {
            v0_1 = v0_0.h;
        }
        return v0_1;
    }

    public final int size()
    {
        return this.d;
    }
}
