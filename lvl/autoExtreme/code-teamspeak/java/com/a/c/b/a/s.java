package com.a.c.b.a;
public final class s extends com.a.c.an {
    private final com.a.c.b.ao a;
    private final java.util.Map b;

    private s(com.a.c.b.ao p1, java.util.Map p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    synthetic s(com.a.c.b.ao p1, java.util.Map p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public final Object a(com.a.c.d.a p4)
    {
        AssertionError v0_3;
        if (p4.f() != com.a.c.d.d.i) {
            AssertionError v1_1 = this.a.a();
            try {
                p4.c();
            } catch (AssertionError v0_7) {
                throw new AssertionError(v0_7);
            } catch (AssertionError v0_8) {
                throw new com.a.c.ag(v0_8);
            }
            while (p4.e()) {
                AssertionError v0_6 = ((com.a.c.b.a.t) this.b.get(p4.h()));
                if ((v0_6 != null) && (v0_6.i)) {
                    v0_6.a(p4, v1_1);
                } else {
                    p4.o();
                }
            }
            p4.d();
            v0_3 = v1_1;
        } else {
            p4.k();
            v0_3 = 0;
        }
        return v0_3;
    }

    public final void a(com.a.c.d.e p4, Object p5)
    {
        if (p5 != null) {
            p4.d();
            try {
                java.util.Iterator v1 = this.b.values().iterator();
            } catch (com.a.c.b.a.t v0) {
                throw new AssertionError();
            }
            while (v1.hasNext()) {
                com.a.c.b.a.t v0_4 = ((com.a.c.b.a.t) v1.next());
                if (v0_4.h) {
                    p4.a(v0_4.g);
                    v0_4.a(p4, p5);
                }
            }
            p4.e();
        } else {
            p4.f();
        }
        return;
    }
}
