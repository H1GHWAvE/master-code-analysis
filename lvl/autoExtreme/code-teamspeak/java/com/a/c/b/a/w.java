package com.a.c.b.a;
public final class w extends com.a.c.an {
    public static final com.a.c.ap a;
    private final java.text.DateFormat b;

    static w()
    {
        com.a.c.b.a.w.a = new com.a.c.b.a.x();
        return;
    }

    public w()
    {
        this.b = new java.text.SimpleDateFormat("hh:mm:ss a");
        return;
    }

    private declared_synchronized void a(com.a.c.d.e p2, java.sql.Time p3)
    {
        try {
            Throwable v0_1;
            if (p3 != null) {
                v0_1 = this.b.format(p3);
            } else {
                v0_1 = 0;
            }
        } catch (Throwable v0_2) {
            throw v0_2;
        }
        p2.b(v0_1);
        return;
    }

    private declared_synchronized java.sql.Time b(com.a.c.d.a p5)
    {
        try {
            java.text.ParseException v0_3;
            if (p5.f() != com.a.c.d.d.i) {
                try {
                    v0_3 = new java.sql.Time(this.b.parse(p5.i()).getTime());
                } catch (java.text.ParseException v0_4) {
                    throw new com.a.c.ag(v0_4);
                }
            } else {
                p5.k();
                v0_3 = 0;
            }
        } catch (java.text.ParseException v0_5) {
            throw v0_5;
        }
        return v0_3;
    }

    public final synthetic Object a(com.a.c.d.a p2)
    {
        return this.b(p2);
    }

    public final bridge synthetic void a(com.a.c.d.e p1, Object p2)
    {
        this.a(p1, ((java.sql.Time) p2));
        return;
    }
}
