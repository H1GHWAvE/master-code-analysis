package com.a.c.b;
final class an implements java.util.Map$Entry {
    com.a.c.b.an a;
    com.a.c.b.an b;
    com.a.c.b.an c;
    com.a.c.b.an d;
    com.a.c.b.an e;
    final Object f;
    Object g;
    int h;

    an()
    {
        this.f = 0;
        this.e = this;
        this.d = this;
        return;
    }

    an(com.a.c.b.an p2, Object p3, com.a.c.b.an p4, com.a.c.b.an p5)
    {
        this.a = p2;
        this.f = p3;
        this.h = 1;
        this.d = p4;
        this.e = p5;
        p5.d = this;
        p4.e = this;
        return;
    }

    private com.a.c.b.an a()
    {
        com.a.c.b.an v0 = this.b;
        while (v0 != null) {
            this = v0;
            v0 = v0.b;
        }
        return this;
    }

    private com.a.c.b.an b()
    {
        com.a.c.b.an v0 = this.c;
        while (v0 != null) {
            this = v0;
            v0 = v0.c;
        }
        return this;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if ((p4 instanceof java.util.Map$Entry)) {
            if (this.f != null) {
                if (!this.f.equals(((java.util.Map$Entry) p4).getKey())) {
                    return v0;
                }
            } else {
                if (((java.util.Map$Entry) p4).getKey() != null) {
                    return v0;
                }
            }
            if (this.g != null) {
                if (!this.g.equals(((java.util.Map$Entry) p4).getValue())) {
                    return v0;
                }
            } else {
                if (((java.util.Map$Entry) p4).getValue() != null) {
                    return v0;
                }
            }
            v0 = 1;
        }
        return v0;
    }

    public final Object getKey()
    {
        return this.f;
    }

    public final Object getValue()
    {
        return this.g;
    }

    public final int hashCode()
    {
        int v0_2;
        int v1_0 = 0;
        if (this.f != null) {
            v0_2 = this.f.hashCode();
        } else {
            v0_2 = 0;
        }
        if (this.g != null) {
            v1_0 = this.g.hashCode();
        }
        return (v0_2 ^ v1_0);
    }

    public final Object setValue(Object p2)
    {
        Object v0 = this.g;
        this.g = p2;
        return v0;
    }

    public final String toString()
    {
        return new StringBuilder().append(this.f).append("=").append(this.g).toString();
    }
}
