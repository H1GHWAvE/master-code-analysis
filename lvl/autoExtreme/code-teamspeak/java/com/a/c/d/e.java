package com.a.c.d;
public class e implements java.io.Closeable, java.io.Flushable {
    private static final String[] f;
    private static final String[] g;
    public String a;
    public String b;
    public boolean c;
    public boolean d;
    public boolean e;
    private final java.io.Writer h;
    private int[] i;
    private int j;
    private String k;

    static e()
    {
        String[] v0_1 = new String[128];
        com.a.c.d.e.f = v0_1;
        String[] v0_2 = 0;
        while (v0_2 <= 31) {
            Object[] v4_1 = new Object[1];
            v4_1[0] = Integer.valueOf(v0_2);
            com.a.c.d.e.f[v0_2] = String.format("\\u%04x", v4_1);
            v0_2++;
        }
        com.a.c.d.e.f[34] = "\\\"";
        com.a.c.d.e.f[92] = "\\\\";
        com.a.c.d.e.f[9] = "\\t";
        com.a.c.d.e.f[8] = "\\b";
        com.a.c.d.e.f[10] = "\\n";
        com.a.c.d.e.f[13] = "\\r";
        com.a.c.d.e.f[12] = "\\f";
        String[] v0_12 = ((String[]) com.a.c.d.e.f.clone());
        com.a.c.d.e.g = v0_12;
        v0_12[60] = "\\u003c";
        com.a.c.d.e.g[62] = "\\u003e";
        com.a.c.d.e.g[38] = "\\u0026";
        com.a.c.d.e.g[61] = "\\u003d";
        com.a.c.d.e.g[39] = "\\u0027";
        return;
    }

    public e(java.io.Writer p3)
    {
        NullPointerException v0_1 = new int[32];
        this.i = v0_1;
        this.j = 0;
        this.a(6);
        this.b = ":";
        this.e = 1;
        if (p3 != null) {
            this.h = p3;
            return;
        } else {
            throw new NullPointerException("out == null");
        }
    }

    private com.a.c.d.e a(int p4, int p5, String p6)
    {
        java.io.Writer v0_0 = this.i();
        if ((v0_0 == p5) || (v0_0 == p4)) {
            if (this.k == null) {
                this.j = (this.j - 1);
                if (v0_0 == p5) {
                    this.k();
                }
                this.h.write(p6);
                return this;
            } else {
                throw new IllegalStateException(new StringBuilder("Dangling name: ").append(this.k).toString());
            }
        } else {
            throw new IllegalStateException("Nesting problem.");
        }
    }

    private com.a.c.d.e a(int p2, String p3)
    {
        this.e(1);
        this.a(p2);
        this.h.write(p3);
        return this;
    }

    private void a(int p5)
    {
        if (this.j == this.i.length) {
            int[] v0_3 = new int[(this.j * 2)];
            System.arraycopy(this.i, 0, v0_3, 0, this.j);
            this.i = v0_3;
        }
        int[] v0_4 = this.i;
        int v1_3 = this.j;
        this.j = (v1_3 + 1);
        v0_4[v1_3] = p5;
        return;
    }

    private boolean a()
    {
        return this.c;
    }

    private void b(int p3)
    {
        this.i[(this.j - 1)] = p3;
        return;
    }

    private void b(boolean p1)
    {
        this.c = p1;
        return;
    }

    private void c(String p2)
    {
        if (p2.length() != 0) {
            this.a = p2;
            this.b = ": ";
        } else {
            this.a = 0;
            this.b = ":";
        }
        return;
    }

    private void c(boolean p1)
    {
        this.d = p1;
        return;
    }

    private void d(String p8)
    {
        java.io.Writer v0_1;
        int v1_0 = 0;
        if (!this.d) {
            v0_1 = com.a.c.d.e.f;
        } else {
            v0_1 = com.a.c.d.e.g;
        }
        this.h.write("\"");
        int v4 = p8.length();
        int v3_1 = 0;
        while (v3_1 < v4) {
            String v2_3;
            String v2_2 = p8.charAt(v3_1);
            if (v2_2 >= 128) {
                if (v2_2 != 8232) {
                    if (v2_2 == 8233) {
                        v2_3 = "\\u2029";
                        if (v1_0 < v3_1) {
                            this.h.write(p8, v1_0, (v3_1 - v1_0));
                        }
                        this.h.write(v2_3);
                        v1_0 = (v3_1 + 1);
                    }
                } else {
                    v2_3 = "\\u2028";
                }
            } else {
                v2_3 = v0_1[v2_2];
                if (v2_3 != null) {
                }
            }
            v3_1++;
        }
        if (v1_0 < v4) {
            this.h.write(p8, v1_0, (v4 - v1_0));
        }
        this.h.write("\"");
        return;
    }

    private void d(boolean p1)
    {
        this.e = p1;
        return;
    }

    private void e(boolean p3)
    {
        switch (this.i()) {
            case 1:
                this.b(2);
                this.k();
                break;
            case 2:
                this.h.append(44);
                this.k();
                break;
            case 3:
            case 5:
            default:
                throw new IllegalStateException("Nesting problem.");
                break;
            case 4:
                this.h.append(this.b);
                this.b(5);
                break;
            case 6:
                if ((this.c) || (p3)) {
                    this.b(7);
                } else {
                    throw new IllegalStateException("JSON must start with an array or an object.");
                }
                break;
            case 7:
                if (this.c) {
                } else {
                    throw new IllegalStateException("JSON must have only one top-level value.");
                }
                break;
        }
        return;
    }

    private boolean g()
    {
        return this.d;
    }

    private boolean h()
    {
        return this.e;
    }

    private int i()
    {
        if (this.j != 0) {
            return this.i[(this.j - 1)];
        } else {
            throw new IllegalStateException("JsonWriter is closed.");
        }
    }

    private void j()
    {
        if (this.k != null) {
            IllegalStateException v0_1 = this.i();
            if (v0_1 != 5) {
                if (v0_1 != 3) {
                    throw new IllegalStateException("Nesting problem.");
                }
            } else {
                this.h.write(44);
            }
            this.k();
            this.b(4);
            this.d(this.k);
            this.k = 0;
        }
        return;
    }

    private void k()
    {
        if (this.a != null) {
            this.h.write("\n");
            int v0_2 = 1;
            int v1_1 = this.j;
            while (v0_2 < v1_1) {
                this.h.write(this.a);
                v0_2++;
            }
        }
        return;
    }

    private void l()
    {
        IllegalStateException v0_0 = this.i();
        if (v0_0 != 5) {
            if (v0_0 != 3) {
                throw new IllegalStateException("Nesting problem.");
            }
        } else {
            this.h.write(44);
        }
        this.k();
        this.b(4);
        return;
    }

    public com.a.c.d.e a(double p4)
    {
        if ((!Double.isNaN(p4)) && (!Double.isInfinite(p4))) {
            this.j();
            this.e(0);
            this.h.append(Double.toString(p4));
            return this;
        } else {
            throw new IllegalArgumentException(new StringBuilder("Numeric values must be finite, but was ").append(p4).toString());
        }
    }

    public com.a.c.d.e a(long p4)
    {
        this.j();
        this.e(0);
        this.h.write(Long.toString(p4));
        return this;
    }

    public com.a.c.d.e a(Number p4)
    {
        if (p4 != null) {
            this.j();
            IllegalArgumentException v0_0 = p4.toString();
            if ((this.c) || ((!v0_0.equals("-Infinity")) && ((!v0_0.equals("Infinity")) && (!v0_0.equals("NaN"))))) {
                this.e(0);
                this.h.append(v0_0);
            } else {
                throw new IllegalArgumentException(new StringBuilder("Numeric values must be finite, but was ").append(p4).toString());
            }
        } else {
            this = this.f();
        }
        return this;
    }

    public com.a.c.d.e a(String p3)
    {
        if (p3 != null) {
            if (this.k == null) {
                if (this.j != 0) {
                    this.k = p3;
                    return this;
                } else {
                    throw new IllegalStateException("JsonWriter is closed.");
                }
            } else {
                throw new IllegalStateException();
            }
        } else {
            throw new NullPointerException("name == null");
        }
    }

    public com.a.c.d.e a(boolean p3)
    {
        String v0_1;
        this.j();
        this.e(0);
        if (!p3) {
            v0_1 = "false";
        } else {
            v0_1 = "true";
        }
        this.h.write(v0_1);
        return this;
    }

    public com.a.c.d.e b()
    {
        this.j();
        return this.a(1, "[");
    }

    public com.a.c.d.e b(String p2)
    {
        if (p2 != null) {
            this.j();
            this.e(0);
            this.d(p2);
        } else {
            this = this.f();
        }
        return this;
    }

    public com.a.c.d.e c()
    {
        return this.a(1, 2, "]");
    }

    public void close()
    {
        this.h.close();
        int v0_1 = this.j;
        if ((v0_1 <= 1) && ((v0_1 != 1) || (this.i[(v0_1 - 1)] == 7))) {
            this.j = 0;
            return;
        } else {
            throw new java.io.IOException("Incomplete document");
        }
    }

    public com.a.c.d.e d()
    {
        this.j();
        return this.a(3, "{");
    }

    public com.a.c.d.e e()
    {
        return this.a(3, 5, "}");
    }

    public com.a.c.d.e f()
    {
        if (this.k == null) {
            this.e(0);
            this.h.write("null");
        } else {
            if (!this.e) {
                this.k = 0;
            } else {
                this.j();
            }
        }
        return this;
    }

    public void flush()
    {
        if (this.j != 0) {
            this.h.flush();
            return;
        } else {
            throw new IllegalStateException("JsonWriter is closed.");
        }
    }
}
