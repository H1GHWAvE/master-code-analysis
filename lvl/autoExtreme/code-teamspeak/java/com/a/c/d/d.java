package com.a.c.d;
public final enum class d extends java.lang.Enum {
    public static final enum com.a.c.d.d a;
    public static final enum com.a.c.d.d b;
    public static final enum com.a.c.d.d c;
    public static final enum com.a.c.d.d d;
    public static final enum com.a.c.d.d e;
    public static final enum com.a.c.d.d f;
    public static final enum com.a.c.d.d g;
    public static final enum com.a.c.d.d h;
    public static final enum com.a.c.d.d i;
    public static final enum com.a.c.d.d j;
    private static final synthetic com.a.c.d.d[] k;

    static d()
    {
        com.a.c.d.d.a = new com.a.c.d.d("BEGIN_ARRAY", 0);
        com.a.c.d.d.b = new com.a.c.d.d("END_ARRAY", 1);
        com.a.c.d.d.c = new com.a.c.d.d("BEGIN_OBJECT", 2);
        com.a.c.d.d.d = new com.a.c.d.d("END_OBJECT", 3);
        com.a.c.d.d.e = new com.a.c.d.d("NAME", 4);
        com.a.c.d.d.f = new com.a.c.d.d("STRING", 5);
        com.a.c.d.d.g = new com.a.c.d.d("NUMBER", 6);
        com.a.c.d.d.h = new com.a.c.d.d("BOOLEAN", 7);
        com.a.c.d.d.i = new com.a.c.d.d("NULL", 8);
        com.a.c.d.d.j = new com.a.c.d.d("END_DOCUMENT", 9);
        com.a.c.d.d[] v0_21 = new com.a.c.d.d[10];
        v0_21[0] = com.a.c.d.d.a;
        v0_21[1] = com.a.c.d.d.b;
        v0_21[2] = com.a.c.d.d.c;
        v0_21[3] = com.a.c.d.d.d;
        v0_21[4] = com.a.c.d.d.e;
        v0_21[5] = com.a.c.d.d.f;
        v0_21[6] = com.a.c.d.d.g;
        v0_21[7] = com.a.c.d.d.h;
        v0_21[8] = com.a.c.d.d.i;
        v0_21[9] = com.a.c.d.d.j;
        com.a.c.d.d.k = v0_21;
        return;
    }

    private d(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    public static com.a.c.d.d valueOf(String p1)
    {
        return ((com.a.c.d.d) Enum.valueOf(com.a.c.d.d, p1));
    }

    public static com.a.c.d.d[] values()
    {
        return ((com.a.c.d.d[]) com.a.c.d.d.k.clone());
    }
}
