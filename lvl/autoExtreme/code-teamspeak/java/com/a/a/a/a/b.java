package com.a.a.a.a;
public final class b implements com.a.a.a.a.s {
    public static final int a = 0;
    public static final int b = 1;
    private static final String f = "APKExpansionPolicy";
    private static final String g = "com.android.vending.licensing.APKExpansionPolicy";
    private static final String h = "lastResponse";
    private static final String i = "validityTimestamp";
    private static final String j = "retryUntil";
    private static final String k = "maxRetries";
    private static final String l = "retryCount";
    private static final String m = "0";
    private static final String n = "0";
    private static final String o = "0";
    private static final String p = "0";
    private static final long q = 60000;
    private java.util.Vector A;
    private long r;
    private long s;
    private long t;
    private long u;
    private long v;
    private int w;
    private com.a.a.a.a.t x;
    private java.util.Vector y;
    private java.util.Vector z;

    private b(android.content.Context p4, com.a.a.a.a.r p5)
    {
        this.v = 0;
        this.y = new java.util.Vector();
        this.z = new java.util.Vector();
        this.A = new java.util.Vector();
        this.x = new com.a.a.a.a.t(p4.getSharedPreferences("com.android.vending.licensing.APKExpansionPolicy", 0), p5);
        this.w = Integer.parseInt(this.x.b("lastResponse", Integer.toString(291)));
        this.r = Long.parseLong(this.x.b("validityTimestamp", "0"));
        this.s = Long.parseLong(this.x.b("retryUntil", "0"));
        this.t = Long.parseLong(this.x.b("maxRetries", "0"));
        this.u = Long.parseLong(this.x.b("retryCount", "0"));
        return;
    }

    private void a(int p4)
    {
        this.v = System.currentTimeMillis();
        this.w = p4;
        this.x.a("lastResponse", Integer.toString(p4));
        return;
    }

    private void a(int p3, long p4)
    {
        if (p3 >= this.A.size()) {
            this.A.setSize((p3 + 1));
        }
        this.A.set(p3, Long.valueOf(p4));
        return;
    }

    private void a(int p3, String p4)
    {
        if (p3 >= this.y.size()) {
            this.y.setSize((p3 + 1));
        }
        this.y.set(p3, p4);
        return;
    }

    private void a(long p4)
    {
        this.u = p4;
        this.x.a("retryCount", Long.toString(p4));
        return;
    }

    private void a(String p5)
    {
        try {
            com.a.a.a.a.t v0_1 = Long.valueOf(Long.parseLong(p5));
        } catch (com.a.a.a.a.t v0) {
            android.util.Log.w("APKExpansionPolicy", "License validity timestamp (VT) missing, caching for a minute");
            v0_1 = Long.valueOf((System.currentTimeMillis() + 60000));
            p5 = Long.toString(v0_1.longValue());
        }
        this.r = v0_1.longValue();
        this.x.a("validityTimestamp", p5);
        return;
    }

    private String b(int p2)
    {
        int v0_2;
        if (p2 >= this.y.size()) {
            v0_2 = 0;
        } else {
            v0_2 = ((String) this.y.elementAt(p2));
        }
        return v0_2;
    }

    private void b(int p3, String p4)
    {
        if (p3 >= this.z.size()) {
            this.z.setSize((p3 + 1));
        }
        this.z.set(p3, p4);
        return;
    }

    private void b(String p3)
    {
        try {
            com.a.a.a.a.t v0_1 = Long.valueOf(Long.parseLong(p3));
        } catch (com.a.a.a.a.t v0) {
            android.util.Log.w("APKExpansionPolicy", "License retry timestamp (GT) missing, grace period disabled");
            p3 = "0";
            v0_1 = Long.valueOf(0);
        }
        this.s = v0_1.longValue();
        this.x.a("retryUntil", p3);
        return;
    }

    private String c(int p2)
    {
        int v0_2;
        if (p2 >= this.z.size()) {
            v0_2 = 0;
        } else {
            v0_2 = ((String) this.z.elementAt(p2));
        }
        return v0_2;
    }

    private void c()
    {
        this.x.a("lastResponse", Integer.toString(291));
        this.b("0");
        this.c("0");
        this.a(Long.parseLong("0"));
        this.a("0");
        this.x.a();
        return;
    }

    private void c(String p3)
    {
        try {
            com.a.a.a.a.t v0_1 = Long.valueOf(Long.parseLong(p3));
        } catch (com.a.a.a.a.t v0) {
            android.util.Log.w("APKExpansionPolicy", "Licence retry count (GR) missing, grace period disabled");
            p3 = "0";
            v0_1 = Long.valueOf(0);
        }
        this.t = v0_1.longValue();
        this.x.a("maxRetries", p3);
        return;
    }

    private long d()
    {
        return this.u;
    }

    private long d(int p3)
    {
        long v0_2;
        if (p3 >= this.A.size()) {
            v0_2 = -1;
        } else {
            v0_2 = ((Long) this.A.elementAt(p3)).longValue();
        }
        return v0_2;
    }

    private static java.util.Map d(String p9)
    {
        java.util.HashMap v4_1 = new java.util.HashMap();
        try {
            String[] v5 = p9.split("&");
            int v6 = v5.length;
            int v3 = 0;
        } catch (int v0) {
            android.util.Log.w("APKExpansionPolicy", "Invalid syntax error while decoding extras data from server.");
            return v4_1;
        }
        while (v3 < v6) {
            String[] v7 = v5[v3].split("=");
            String v2_1 = v7[0];
            int v0_4 = 0;
            while (v4_1.containsKey(v2_1)) {
                v0_4++;
                v2_1 = new StringBuilder().append(v7[0]).append(v0_4).toString();
            }
            v4_1.put(v2_1, v7[1]);
            v3++;
        }
        return v4_1;
    }

    private long e()
    {
        return this.r;
    }

    private long f()
    {
        return this.s;
    }

    private long g()
    {
        return this.t;
    }

    private int h()
    {
        return this.y.size();
    }

    public final void a(com.a.a.a.a.u p1, String p2, String p3)
    {
        return;
    }

    public final boolean a()
    {
        int v0 = 1;
        long v2_0 = System.currentTimeMillis();
        if (this.w != 256) {
            if ((this.w != 291) || (v2_0 >= (this.v + 60000))) {
                v0 = 0;
            } else {
                if ((v2_0 > this.s) && (this.u > this.t)) {
                    v0 = 0;
                }
            }
        } else {
            if (v2_0 > this.r) {
            }
        }
        return v0;
    }

    public final com.a.a.a.a.w b()
    {
        return 0;
    }
}
