package com.a.a.a.a;
public final class t {
    private static final String a = "PreferenceObfuscator";
    private final android.content.SharedPreferences b;
    private final com.a.a.a.a.r c;
    private android.content.SharedPreferences$Editor d;

    public t(android.content.SharedPreferences p2, com.a.a.a.a.r p3)
    {
        this.b = p2;
        this.c = p3;
        this.d = 0;
        return;
    }

    public final void a()
    {
        if (this.d != null) {
            this.d.commit();
            this.d = 0;
        }
        return;
    }

    public final void a(String p3, String p4)
    {
        if (this.d == null) {
            this.d = this.b.edit();
        }
        this.d.putString(p3, this.c.a(p4, p3));
        return;
    }

    public final String b(String p4, String p5)
    {
        String v0_1 = this.b.getString(p4, 0);
        if (v0_1 != null) {
            try {
                p5 = this.c.b(v0_1, p4);
            } catch (String v0) {
                android.util.Log.w("PreferenceObfuscator", new StringBuilder("Validation error while reading preference: ").append(p4).toString());
            }
        }
        return p5;
    }
}
