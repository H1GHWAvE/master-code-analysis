package com.a.a.a.a;
final class j implements com.a.a.a.a.h {
    private android.os.IBinder a;

    j(android.os.IBinder p1)
    {
        this.a = p1;
        return;
    }

    private static String a()
    {
        return "com.android.vending.licensing.ILicensingService";
    }

    public final void a(long p6, String p8, com.a.a.a.a.e p9)
    {
        android.os.IBinder v0_0 = 0;
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v1.writeInterfaceToken("com.android.vending.licensing.ILicensingService");
            v1.writeLong(p6);
            v1.writeString(p8);
        } catch (android.os.IBinder v0_1) {
            v1.recycle();
            throw v0_1;
        }
        if (p9 != null) {
            v0_0 = p9.asBinder();
        }
        v1.writeStrongBinder(v0_0);
        this.a.transact(1, v1, 0, 1);
        v1.recycle();
        return;
    }

    public final android.os.IBinder asBinder()
    {
        return this.a;
    }
}
