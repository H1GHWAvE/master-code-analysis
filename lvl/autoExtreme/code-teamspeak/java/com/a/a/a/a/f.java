package com.a.a.a.a;
public abstract class f extends android.os.Binder implements com.a.a.a.a.e {
    static final int a = 1;
    private static final String b = "com.android.vending.licensing.ILicenseResultListener";

    public f()
    {
        this.attachInterface(this, "com.android.vending.licensing.ILicenseResultListener");
        return;
    }

    private static com.a.a.a.a.e a(android.os.IBinder p2)
    {
        com.a.a.a.a.e v0_3;
        if (p2 != null) {
            com.a.a.a.a.e v0_1 = p2.queryLocalInterface("com.android.vending.licensing.ILicenseResultListener");
            if ((v0_1 == null) || (!(v0_1 instanceof com.a.a.a.a.e))) {
                v0_3 = new com.a.a.a.a.g(p2);
            } else {
                v0_3 = ((com.a.a.a.a.e) v0_1);
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p5, android.os.Parcel p6, android.os.Parcel p7, int p8)
    {
        boolean v0 = 1;
        switch (p5) {
            case 1:
                p6.enforceInterface("com.android.vending.licensing.ILicenseResultListener");
                this.a(p6.readInt(), p6.readString(), p6.readString());
                break;
            case 1598968902:
                p7.writeString("com.android.vending.licensing.ILicenseResultListener");
                break;
            default:
                v0 = super.onTransact(p5, p6, p7, p8);
        }
        return v0;
    }
}
