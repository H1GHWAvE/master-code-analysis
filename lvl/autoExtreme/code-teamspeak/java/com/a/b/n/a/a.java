package com.a.b.n.a;
public abstract class a extends com.a.b.n.a.ce implements com.a.b.n.a.bc {

    protected a(com.a.b.n.a.dp p1)
    {
        this(p1);
        return;
    }

    protected abstract Exception a();

    public final Object a()
    {
        try {
            return this.get();
        } catch (Exception v0_5) {
            Thread.currentThread().interrupt();
            throw this.a(v0_5);
        } catch (Exception v0_3) {
            throw this.a(v0_3);
        } catch (Exception v0_1) {
            throw this.a(v0_1);
        }
    }

    public final Object a(long p4, java.util.concurrent.TimeUnit p6)
    {
        try {
            return this.get(p4, p6);
        } catch (Exception v0_5) {
            Thread.currentThread().interrupt();
            throw this.a(v0_5);
        } catch (Exception v0_3) {
            throw this.a(v0_3);
        } catch (Exception v0_1) {
            throw this.a(v0_1);
        }
    }
}
