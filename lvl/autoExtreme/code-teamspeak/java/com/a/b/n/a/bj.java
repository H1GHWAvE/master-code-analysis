package com.a.b.n.a;
final class bj extends java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock {
    final com.a.b.n.a.bi a;
    final synthetic com.a.b.n.a.bd b;

    bj(com.a.b.n.a.bd p1, com.a.b.n.a.bi p2)
    {
        this.b = p1;
        this(p2);
        this.a = p2;
        return;
    }

    public final void lock()
    {
        com.a.b.n.a.bd.a(this.b, this.a);
        try {
            super.lock();
            com.a.b.n.a.bd.a(this.a);
            return;
        } catch (Throwable v0_2) {
            com.a.b.n.a.bd.a(this.a);
            throw v0_2;
        }
    }

    public final void lockInterruptibly()
    {
        com.a.b.n.a.bd.a(this.b, this.a);
        try {
            super.lockInterruptibly();
            com.a.b.n.a.bd.a(this.a);
            return;
        } catch (Throwable v0_2) {
            com.a.b.n.a.bd.a(this.a);
            throw v0_2;
        }
    }

    public final boolean tryLock()
    {
        com.a.b.n.a.bd.a(this.b, this.a);
        try {
            Throwable v0_1 = super.tryLock();
            com.a.b.n.a.bd.a(this.a);
            return v0_1;
        } catch (Throwable v0_2) {
            com.a.b.n.a.bd.a(this.a);
            throw v0_2;
        }
    }

    public final boolean tryLock(long p4, java.util.concurrent.TimeUnit p6)
    {
        com.a.b.n.a.bd.a(this.b, this.a);
        try {
            Throwable v0_1 = super.tryLock(p4, p6);
            com.a.b.n.a.bd.a(this.a);
            return v0_1;
        } catch (Throwable v0_2) {
            com.a.b.n.a.bd.a(this.a);
            throw v0_2;
        }
    }

    public final void unlock()
    {
        try {
            super.unlock();
            com.a.b.n.a.bd.a(this.a);
            return;
        } catch (Throwable v0_1) {
            com.a.b.n.a.bd.a(this.a);
            throw v0_1;
        }
    }
}
