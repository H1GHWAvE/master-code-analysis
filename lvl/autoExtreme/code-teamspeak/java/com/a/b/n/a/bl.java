package com.a.b.n.a;
 class bl {
    final java.util.Map a;
    final java.util.Map b;
    final String c;

    bl(String p3)
    {
        this.a = new com.a.b.d.ql().a(com.a.b.d.sh.c).e();
        this.b = new com.a.b.d.ql().a(com.a.b.d.sh.c).e();
        this.c = ((String) com.a.b.b.cn.a(p3));
        return;
    }

    private com.a.b.n.a.bk a(com.a.b.n.a.bl p6, java.util.Set p7)
    {
        com.a.b.n.a.bk v0_3;
        if (p7.add(this)) {
            v0_3 = ((com.a.b.n.a.bk) this.a.get(p6));
            if (v0_3 == null) {
                java.util.Iterator v3 = this.a.entrySet().iterator();
                while (v3.hasNext()) {
                    com.a.b.n.a.bk v0_8 = ((java.util.Map$Entry) v3.next());
                    com.a.b.n.a.bl v1_1 = ((com.a.b.n.a.bl) v0_8.getKey());
                    com.a.b.n.a.bk v4 = v1_1.a(p6, p7);
                    if (v4 != null) {
                        com.a.b.n.a.bk v2_2 = new com.a.b.n.a.bk(v1_1, this);
                        v2_2.setStackTrace(((com.a.b.n.a.bk) v0_8.getValue()).getStackTrace());
                        v2_2.initCause(v4);
                        v0_3 = v2_2;
                    }
                }
                v0_3 = 0;
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private String a()
    {
        return this.c;
    }

    private void a(com.a.b.n.a.bq p6, com.a.b.n.a.bl p7)
    {
        java.util.Map v0_0;
        if (this == p7) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.n.a.br v2_3;
        com.a.b.n.a.br v2_1 = String.valueOf(p7.c);
        if (v2_1.length() == 0) {
            v2_3 = new String("Attempted to acquire multiple locks with the same rank ");
        } else {
            v2_3 = "Attempted to acquire multiple locks with the same rank ".concat(v2_1);
        }
        com.a.b.b.cn.b(v0_0, v2_3);
        if (!this.a.containsKey(p7)) {
            java.util.Map v0_5 = ((com.a.b.n.a.br) this.b.get(p7));
            if (v0_5 == null) {
                java.util.Map v0_8 = p7.a(this, java.util.Collections.newSetFromMap(com.a.b.d.sz.f()));
                if (v0_8 != null) {
                    com.a.b.n.a.br v2_5 = new com.a.b.n.a.br(p7, this, v0_8, 0);
                    this.b.put(p7, v2_5);
                    p6.a(v2_5);
                } else {
                    this.a.put(p7, new com.a.b.n.a.bk(p7, this));
                }
            } else {
                p6.a(new com.a.b.n.a.br(p7, this, v0_5.c, 0));
            }
        }
        return;
    }

    final void a(com.a.b.n.a.bq p9, java.util.List p10)
    {
        int v5 = p10.size();
        int v4 = 0;
        while (v4 < v5) {
            java.util.Map v1_0;
            int v0_1 = ((com.a.b.n.a.bl) p10.get(v4));
            if (this == v0_1) {
                v1_0 = 0;
            } else {
                v1_0 = 1;
            }
            com.a.b.n.a.br v3_3;
            com.a.b.n.a.br v3_1 = String.valueOf(v0_1.c);
            if (v3_1.length() == 0) {
                v3_3 = new String("Attempted to acquire multiple locks with the same rank ");
            } else {
                v3_3 = "Attempted to acquire multiple locks with the same rank ".concat(v3_1);
            }
            com.a.b.b.cn.b(v1_0, v3_3);
            if (!this.a.containsKey(v0_1)) {
                java.util.Map v1_5 = ((com.a.b.n.a.br) this.b.get(v0_1));
                if (v1_5 == null) {
                    java.util.Map v1_8 = v0_1.a(this, java.util.Collections.newSetFromMap(com.a.b.d.sz.f()));
                    if (v1_8 != null) {
                        com.a.b.n.a.br v3_5 = new com.a.b.n.a.br(v0_1, this, v1_8, 0);
                        this.b.put(v0_1, v3_5);
                        p9.a(v3_5);
                    } else {
                        this.a.put(v0_1, new com.a.b.n.a.bk(v0_1, this));
                    }
                } else {
                    p9.a(new com.a.b.n.a.br(v0_1, this, v1_5.c, 0));
                }
            }
            v4++;
        }
        return;
    }
}
