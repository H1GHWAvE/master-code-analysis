package com.a.b.n.a;
final class ao {
    final com.a.b.n.a.ew a;
    final boolean b;
    final Throwable c;

    ao(com.a.b.n.a.ew p3)
    {
        this(p3, 0, 0);
        return;
    }

    ao(com.a.b.n.a.ew p6, boolean p7, Throwable p8)
    {
        int v0_1;
        if ((p7) && (p6 != com.a.b.n.a.ew.b)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v0_2;
        Object[] v4_0 = new Object[1];
        v4_0[0] = p6;
        com.a.b.b.cn.a(v0_1, "shudownWhenStartupFinishes can only be set if state is STARTING. Got %s instead.", v4_0);
        if (p8 == null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        String v3_2;
        if (p6 != com.a.b.n.a.ew.f) {
            v3_2 = 0;
        } else {
            v3_2 = 1;
        }
        int v0_4;
        if ((v0_2 ^ v3_2) != 0) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        Object[] v4_2 = new Object[2];
        v4_2[0] = p6;
        v4_2[1] = p8;
        com.a.b.b.cn.a(v0_4, "A failure cause should be set if and only if the state is failed.  Got %s and %s instead.", v4_2);
        this.a = p6;
        this.b = p7;
        this.c = p8;
        return;
    }

    private com.a.b.n.a.ew a()
    {
        if ((!this.b) || (this.a != com.a.b.n.a.ew.b)) {
            com.a.b.n.a.ew v0_2 = this.a;
        } else {
            v0_2 = com.a.b.n.a.ew.d;
        }
        return v0_2;
    }

    private Throwable b()
    {
        Throwable v0_1;
        if (this.a != com.a.b.n.a.ew.f) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = this.a;
        com.a.b.b.cn.b(v0_1, "failureCause() is only valid if the service has failed, service is %s", v1_1);
        return this.c;
    }
}
