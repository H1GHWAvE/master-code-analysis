package com.a.b.n.a;
final class cx extends com.a.b.n.a.dq {
    com.a.b.d.jl a;

    private cx(java.util.concurrent.Callable p1, com.a.b.d.jl p2)
    {
        this(p1);
        this.a = p2;
        return;
    }

    public final boolean cancel(boolean p3)
    {
        int v0_1;
        if (!super.cancel(p3)) {
            v0_1 = 0;
        } else {
            java.util.Iterator v1_1 = this.a.iterator();
            while (v1_1.hasNext()) {
                ((com.a.b.n.a.dp) v1_1.next()).cancel(p3);
            }
            v0_1 = 1;
        }
        return v0_1;
    }

    protected final void done()
    {
        super.done();
        this.a = 0;
        return;
    }

    protected final void setException(Throwable p1)
    {
        super.setException(p1);
        return;
    }
}
