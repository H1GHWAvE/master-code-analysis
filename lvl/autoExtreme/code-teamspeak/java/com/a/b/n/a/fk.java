package com.a.b.n.a;
final class fk {
    final com.a.b.n.a.dw a;
    final com.a.b.d.aac b;
    final com.a.b.d.xc c;
    final java.util.Map d;
    boolean e;
    boolean f;
    final int g;
    final com.a.b.n.a.dx h;
    final com.a.b.n.a.dx i;
    final java.util.List j;

    fk(com.a.b.d.iz p3)
    {
        this.a = new com.a.b.n.a.dw();
        this.b = com.a.b.d.we.b(new java.util.EnumMap(com.a.b.n.a.ew), new com.a.b.n.a.fl(this));
        this.c = this.b.q();
        this.d = com.a.b.d.sz.f();
        this.h = new com.a.b.n.a.fm(this, this.a);
        this.i = new com.a.b.n.a.fn(this, this.a);
        this.j = java.util.Collections.synchronizedList(new java.util.ArrayList());
        this.g = p3.size();
        this.b.c(com.a.b.n.a.ew.a, p3);
        return;
    }

    private void a(long p8, java.util.concurrent.TimeUnit p10)
    {
        this.a.a.lock();
        try {
            if (this.a.b(this.h, p8, p10)) {
                this.d();
                this.a.a();
                return;
            } else {
                String v1_3 = String.valueOf(String.valueOf("Timeout waiting for the services to become healthy. The following services have not started: "));
                String v2_3 = String.valueOf(String.valueOf(com.a.b.d.we.a(this.b, com.a.b.b.cp.a(com.a.b.d.lo.b(com.a.b.n.a.ew.a, com.a.b.n.a.ew.b)))));
                throw new java.util.concurrent.TimeoutException(new StringBuilder(((v1_3.length() + 0) + v2_3.length())).append(v1_3).append(v2_3).toString());
            }
        } catch (com.a.b.n.a.dw v0_7) {
            this.a.a();
            throw v0_7;
        }
    }

    private void a(com.a.b.n.a.et p3)
    {
        this.a.a.lock();
        try {
            if (((com.a.b.b.dw) this.d.get(p3)) == null) {
                this.d.put(p3, com.a.b.b.dw.a());
            }
        } catch (com.a.b.n.a.dw v0_6) {
            this.a.a();
            throw v0_6;
        }
        this.a.a();
        return;
    }

    private void b(long p8, java.util.concurrent.TimeUnit p10)
    {
        this.a.a.lock();
        try {
            if (this.a.b(this.i, p8, p10)) {
                this.a.a();
                return;
            } else {
                String v1_3 = String.valueOf(String.valueOf("Timeout waiting for the services to stop. The following services have not stopped: "));
                String v2_3 = String.valueOf(String.valueOf(com.a.b.d.we.a(this.b, com.a.b.b.cp.a(com.a.b.b.cp.a(com.a.b.d.lo.b(com.a.b.n.a.ew.e, com.a.b.n.a.ew.f))))));
                throw new java.util.concurrent.TimeoutException(new StringBuilder(((v1_3.length() + 0) + v2_3.length())).append(v1_3).append(v2_3).toString());
            }
        } catch (com.a.b.n.a.dw v0_7) {
            this.a.a();
            throw v0_7;
        }
    }

    private void b(com.a.b.n.a.et p5)
    {
        java.util.List v1_1 = String.valueOf(String.valueOf(p5));
        new com.a.b.n.a.fp(this, new StringBuilder((v1_1.length() + 18)).append("failed({service=").append(v1_1).append("})").toString(), p5).a(this.j);
        return;
    }

    private void e()
    {
        this.a.a(this.h);
        try {
            this.d();
            this.a.a();
            return;
        } catch (Throwable v0_2) {
            this.a.a();
            throw v0_2;
        }
    }

    private void f()
    {
        this.a.a(this.i);
        this.a.a();
        return;
    }

    private void g()
    {
        com.a.b.n.a.fd.b().a(this.j);
        return;
    }

    private void h()
    {
        com.a.b.n.a.fd.c().a(this.j);
        return;
    }

    private void i()
    {
        com.a.b.n.a.ds v0_3;
        int v1 = 0;
        if (this.a.a.isHeldByCurrentThread()) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        com.a.b.b.cn.b(v0_3, "It is incorrect to execute listeners with the monitor held.");
        while (v1 < this.j.size()) {
            ((com.a.b.n.a.ds) this.j.get(v1)).a();
            v1++;
        }
        return;
    }

    final void a()
    {
        this.a.a.lock();
        try {
            if (this.f) {
                String v1_1 = new java.util.ArrayList();
                StringBuilder v2_0 = this.b().x().iterator();
                while (v2_0.hasNext()) {
                    IllegalArgumentException v0_9 = ((com.a.b.n.a.et) v2_0.next());
                    if (v0_9.f() != com.a.b.n.a.ew.a) {
                        v1_1.add(v0_9);
                    }
                }
                StringBuilder v2_3 = String.valueOf(String.valueOf("Services started transitioning asynchronously before the ServiceManager was constructed: "));
                String v1_3 = String.valueOf(String.valueOf(v1_1));
                throw new IllegalArgumentException(new StringBuilder(((v2_3.length() + 0) + v1_3.length())).append(v2_3).append(v1_3).toString());
            } else {
                this.e = 1;
                this.a.a();
                return;
            }
        } catch (IllegalArgumentException v0_10) {
            this.a.a();
            throw v0_10;
        }
    }

    final void a(com.a.b.n.a.et p7, com.a.b.n.a.ew p8, com.a.b.n.a.ew p9)
    {
        com.a.b.n.a.dt v0_0 = 1;
        com.a.b.b.cn.a(p7);
        if (p8 == p9) {
            v0_0 = 0;
        }
        com.a.b.b.cn.a(v0_0);
        this.a.a.lock();
        try {
            this.f = 1;
        } catch (com.a.b.n.a.dt v0_22) {
            this.a.a();
            this.i();
            throw v0_22;
        }
        if (this.e) {
            com.a.b.n.a.dt v0_6 = this.b.c(p8, p7);
            com.a.b.n.a.ew v2_1 = new Object[2];
            v2_1[0] = p7;
            v2_1[1] = p8;
            com.a.b.b.cn.b(v0_6, "Service %s not at the expected location in the state map %s", v2_1);
            com.a.b.n.a.dt v0_8 = this.b.a(p9, p7);
            com.a.b.n.a.ew v2_3 = new Object[2];
            v2_3[0] = p7;
            v2_3[1] = p9;
            com.a.b.b.cn.b(v0_8, "Service %s in the state map unexpectedly at %s", v2_3);
            com.a.b.n.a.dt v0_11 = ((com.a.b.b.dw) this.d.get(p7));
            if (v0_11 == null) {
                v0_11 = com.a.b.b.dw.a();
                this.d.put(p7, v0_11);
            }
            if ((p9.compareTo(com.a.b.n.a.ew.c) >= 0) && (v0_11.a)) {
                v0_11.c();
                if (!(p7 instanceof com.a.b.n.a.fi)) {
                    java.util.List v1_8 = com.a.b.n.a.fd.a();
                    Object[] v4_1 = new Object[2];
                    v4_1[0] = p7;
                    v4_1[1] = v0_11;
                    v1_8.log(java.util.logging.Level.FINE, "Started {0} in {1}.", v4_1);
                }
            }
            if (p9 == com.a.b.n.a.ew.f) {
                java.util.List v1_10 = String.valueOf(String.valueOf(p7));
                new com.a.b.n.a.fp(this, new StringBuilder((v1_10.length() + 18)).append("failed({service=").append(v1_10).append("})").toString(), p7).a(this.j);
            }
            if (this.c.a(com.a.b.n.a.ew.c) != this.g) {
                if ((this.c.a(com.a.b.n.a.ew.e) + this.c.a(com.a.b.n.a.ew.f)) == this.g) {
                    com.a.b.n.a.fd.b().a(this.j);
                }
            } else {
                com.a.b.n.a.fd.c().a(this.j);
            }
            this.a.a();
            this.i();
        } else {
            this.a.a();
            this.i();
        }
        return;
    }

    final void a(com.a.b.n.a.fh p3, java.util.concurrent.Executor p4)
    {
        com.a.b.b.cn.a(p3, "listener");
        com.a.b.b.cn.a(p4, "executor");
        this.a.a.lock();
        try {
            if (!this.i.a()) {
                this.j.add(new com.a.b.n.a.ds(p3, p4));
            }
        } catch (com.a.b.n.a.dw v0_7) {
            this.a.a();
            throw v0_7;
        }
        this.a.a();
        return;
    }

    final com.a.b.d.kk b()
    {
        com.a.b.n.a.dw v1_0 = com.a.b.d.lr.c();
        this.a.a.lock();
        try {
            java.util.Iterator v2 = this.b.u().iterator();
        } catch (Object v0_9) {
            this.a.a();
            throw v0_9;
        }
        while (v2.hasNext()) {
            Object v0_8 = ((java.util.Map$Entry) v2.next());
            if (!(v0_8.getValue() instanceof com.a.b.n.a.fi)) {
                v1_0.a(v0_8.getKey(), v0_8.getValue());
            }
        }
        this.a.a();
        return v1_0.a();
    }

    final com.a.b.d.jt c()
    {
        this.a.a.lock();
        try {
            java.util.Iterator v2_0 = com.a.b.d.ov.a(this.d.size());
            java.util.Iterator v3 = this.d.entrySet().iterator();
        } catch (com.a.b.d.jt v0_18) {
            this.a.a();
            throw v0_18;
        }
        while (v3.hasNext()) {
            com.a.b.d.jt v0_15 = ((java.util.Map$Entry) v3.next());
            com.a.b.d.ju v1_4 = ((com.a.b.n.a.et) v0_15.getKey());
            com.a.b.d.jt v0_17 = ((com.a.b.b.dw) v0_15.getValue());
            if ((!v0_17.a) && (!(v1_4 instanceof com.a.b.n.a.fi))) {
                v2_0.add(com.a.b.d.sz.a(v1_4, Long.valueOf(v0_17.a(java.util.concurrent.TimeUnit.MILLISECONDS))));
            }
        }
        this.a.a();
        java.util.Collections.sort(v2_0, com.a.b.d.yd.d().a(new com.a.b.n.a.fo(this)));
        com.a.b.d.ju v1_2 = com.a.b.d.jt.l();
        java.util.Iterator v2_1 = v2_0.iterator();
        while (v2_1.hasNext()) {
            v1_2.a(((java.util.Map$Entry) v2_1.next()));
        }
        return v1_2.a();
    }

    final void d()
    {
        if (this.c.a(com.a.b.n.a.ew.c) == this.g) {
            return;
        } else {
            String v1_5 = String.valueOf(String.valueOf(com.a.b.d.we.a(this.b, com.a.b.b.cp.a(com.a.b.b.cp.a(com.a.b.n.a.ew.c)))));
            throw new IllegalStateException(new StringBuilder((v1_5.length() + 79)).append("Expected to be healthy after starting. The following services are not running: ").append(v1_5).toString());
        }
    }
}
