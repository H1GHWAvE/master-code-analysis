package com.a.b.n.a;
public class dq extends java.util.concurrent.FutureTask implements com.a.b.n.a.dp {
    private final com.a.b.n.a.bu a;

    private dq(Runnable p2, Object p3)
    {
        this(p2, p3);
        this.a = new com.a.b.n.a.bu();
        return;
    }

    dq(java.util.concurrent.Callable p2)
    {
        this(p2);
        this.a = new com.a.b.n.a.bu();
        return;
    }

    public static com.a.b.n.a.dq a(Runnable p1, Object p2)
    {
        return new com.a.b.n.a.dq(p1, p2);
    }

    public static com.a.b.n.a.dq a(java.util.concurrent.Callable p1)
    {
        return new com.a.b.n.a.dq(p1);
    }

    public final void a(Runnable p2, java.util.concurrent.Executor p3)
    {
        this.a.a(p2, p3);
        return;
    }

    protected void done()
    {
        this.a.a();
        return;
    }
}
