package com.a.b.n.a;
abstract class df implements com.a.b.n.a.dp {
    private static final java.util.logging.Logger a;

    static df()
    {
        com.a.b.n.a.df.a = java.util.logging.Logger.getLogger(com.a.b.n.a.df.getName());
        return;
    }

    private df()
    {
        return;
    }

    synthetic df(byte p1)
    {
        return;
    }

    public final void a(Runnable p9, java.util.concurrent.Executor p10)
    {
        com.a.b.b.cn.a(p9, "Runnable was null.");
        com.a.b.b.cn.a(p10, "Executor was null.");
        try {
            p10.execute(p9);
        } catch (RuntimeException v0_2) {
            String v3_1 = String.valueOf(String.valueOf(p9));
            String v4_1 = String.valueOf(String.valueOf(p10));
            com.a.b.n.a.df.a.log(java.util.logging.Level.SEVERE, new StringBuilder(((v3_1.length() + 57) + v4_1.length())).append("RuntimeException while executing runnable ").append(v3_1).append(" with executor ").append(v4_1).toString(), v0_2);
        }
        return;
    }

    public boolean cancel(boolean p2)
    {
        return 0;
    }

    public abstract Object get();

    public Object get(long p2, java.util.concurrent.TimeUnit p4)
    {
        com.a.b.b.cn.a(p4);
        return this.get();
    }

    public boolean isCancelled()
    {
        return 0;
    }

    public boolean isDone()
    {
        return 1;
    }
}
