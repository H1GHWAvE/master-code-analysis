package com.a.b.n.a;
public final class au implements java.io.Serializable {
    private static final long a;
    private transient java.util.concurrent.atomic.AtomicLongArray b;

    private au(int p2)
    {
        this.b = new java.util.concurrent.atomic.AtomicLongArray(p2);
        return;
    }

    private au(double[] p7)
    {
        int v1 = p7.length;
        long[] v2 = new long[v1];
        java.util.concurrent.atomic.AtomicLongArray v0_0 = 0;
        while (v0_0 < v1) {
            v2[v0_0] = Double.doubleToRawLongBits(p7[v0_0]);
            v0_0++;
        }
        this.b = new java.util.concurrent.atomic.AtomicLongArray(v2);
        return;
    }

    private double a(int p3)
    {
        return Double.longBitsToDouble(this.b.get(p3));
    }

    private int a()
    {
        return this.b.length();
    }

    private void a(int p5, double p6)
    {
        this.b.set(p5, Double.doubleToRawLongBits(p6));
        return;
    }

    private void a(java.io.ObjectInputStream p5)
    {
        p5.defaultReadObject();
        int v1 = p5.readInt();
        this.b = new java.util.concurrent.atomic.AtomicLongArray(v1);
        int v0_2 = 0;
        while (v0_2 < v1) {
            this.a(v0_2, p5.readDouble());
            v0_2++;
        }
        return;
    }

    private void a(java.io.ObjectOutputStream p5)
    {
        p5.defaultWriteObject();
        int v1 = this.b.length();
        p5.writeInt(v1);
        int v0_1 = 0;
        while (v0_1 < v1) {
            p5.writeDouble(Double.longBitsToDouble(this.b.get(v0_1)));
            v0_1++;
        }
        return;
    }

    private boolean a(int p7, double p8, double p10)
    {
        return this.b.compareAndSet(p7, Double.doubleToRawLongBits(p8), Double.doubleToRawLongBits(p10));
    }

    private void b(int p1, double p2)
    {
        this.a(p1, p2);
        return;
    }

    private boolean b(int p7, double p8, double p10)
    {
        return this.b.weakCompareAndSet(p7, Double.doubleToRawLongBits(p8), Double.doubleToRawLongBits(p10));
    }

    private double c(int p5, double p6)
    {
        return Double.longBitsToDouble(this.b.getAndSet(p5, Double.doubleToRawLongBits(p6)));
    }

    private double d(int p9, double p10)
    {
        do {
            long v2 = this.b.get(p9);
            double v6 = Double.longBitsToDouble(v2);
        } while(!this.b.compareAndSet(p9, v2, Double.doubleToRawLongBits((v6 + p10))));
        return v6;
    }

    private double e(int p9, double p10)
    {
        do {
            long v2 = this.b.get(p9);
            double v6 = (Double.longBitsToDouble(v2) + p10);
        } while(!this.b.compareAndSet(p9, v2, Double.doubleToRawLongBits(v6)));
        return v6;
    }

    public final String toString()
    {
        int v0_9;
        int v1 = (this.b.length() - 1);
        if (v1 != -1) {
            StringBuilder v2_1 = new StringBuilder(((v1 + 1) * 19));
            v2_1.append(91);
            int v0_6 = 0;
            while(true) {
                v2_1.append(Double.longBitsToDouble(this.b.get(v0_6)));
                if (v0_6 == v1) {
                    break;
                }
                v2_1.append(44).append(32);
                v0_6++;
            }
            v0_9 = v2_1.append(93).toString();
        } else {
            v0_9 = "[]";
        }
        return v0_9;
    }
}
