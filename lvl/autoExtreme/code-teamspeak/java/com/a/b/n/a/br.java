package com.a.b.n.a;
public final class br extends com.a.b.n.a.bk {
    final com.a.b.n.a.bk c;

    private br(com.a.b.n.a.bl p1, com.a.b.n.a.bl p2, com.a.b.n.a.bk p3)
    {
        this(p1, p2);
        this.c = p3;
        this.initCause(p3);
        return;
    }

    synthetic br(com.a.b.n.a.bl p1, com.a.b.n.a.bl p2, com.a.b.n.a.bk p3, byte p4)
    {
        this(p1, p2, p3);
        return;
    }

    private com.a.b.n.a.bk a()
    {
        return this.c;
    }

    public final String getMessage()
    {
        StringBuilder v1_1 = new StringBuilder(super.getMessage());
        String v0_1 = this.c;
        while (v0_1 != null) {
            v1_1.append(", ").append(v0_1.getMessage());
            v0_1 = v0_1.getCause();
        }
        return v1_1.toString();
    }
}
