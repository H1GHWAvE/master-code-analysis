package com.a.b.n.a;
final class dg extends com.a.b.n.a.df implements com.a.b.n.a.bc {
    private final Object a;

    dg(Object p2)
    {
        this(0);
        this.a = p2;
        return;
    }

    public final Object a()
    {
        return this.a;
    }

    public final Object a(long p2, java.util.concurrent.TimeUnit p4)
    {
        com.a.b.b.cn.a(p4);
        return this.a;
    }

    public final Object get()
    {
        return this.a;
    }
}
