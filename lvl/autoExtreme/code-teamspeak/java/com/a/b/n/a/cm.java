package com.a.b.n.a;
final class cm implements java.util.concurrent.Future {
    final synthetic java.util.concurrent.Future a;
    final synthetic com.a.b.b.bj b;

    cm(java.util.concurrent.Future p1, com.a.b.b.bj p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private Object a(Object p3)
    {
        try {
            return this.b.e(p3);
        } catch (Throwable v0_2) {
            throw new java.util.concurrent.ExecutionException(v0_2);
        }
    }

    public final boolean cancel(boolean p2)
    {
        return this.a.cancel(p2);
    }

    public final Object get()
    {
        return this.a(this.a.get());
    }

    public final Object get(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.a(this.a.get(p2, p4));
    }

    public final boolean isCancelled()
    {
        return this.a.isCancelled();
    }

    public final boolean isDone()
    {
        return this.a.isDone();
    }
}
