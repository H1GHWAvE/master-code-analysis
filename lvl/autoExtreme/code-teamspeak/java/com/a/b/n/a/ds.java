package com.a.b.n.a;
final class ds implements java.lang.Runnable {
    private static final java.util.logging.Logger a;
    private final Object b;
    private final java.util.concurrent.Executor c;
    private final java.util.Queue d;
    private boolean e;

    static ds()
    {
        com.a.b.n.a.ds.a = java.util.logging.Logger.getLogger(com.a.b.n.a.ds.getName());
        return;
    }

    ds(Object p2, java.util.concurrent.Executor p3)
    {
        this.d = new java.util.ArrayDeque();
        this.b = com.a.b.b.cn.a(p2);
        this.c = ((java.util.concurrent.Executor) com.a.b.b.cn.a(p3));
        return;
    }

    final void a()
    {
        Throwable v0_0 = 1;
        if (this.e) {
            v0_0 = 0;
        } else {
            this.e = 1;
        }
        if (v0_0 != null) {
            try {
                this.c.execute(this);
            } catch (Throwable v0_3) {
                this.e = 0;
                String v3_2 = String.valueOf(String.valueOf(this.b));
                String v4_2 = String.valueOf(String.valueOf(this.c));
                com.a.b.n.a.ds.a.log(java.util.logging.Level.SEVERE, new StringBuilder(((v3_2.length() + 42) + v4_2.length())).append("Exception while running callbacks for ").append(v3_2).append(" on ").append(v4_2).toString(), v0_3);
                throw v0_3;
            }
        }
        return;
    }

    final declared_synchronized void a(com.a.b.n.a.dt p2)
    {
        try {
            this.d.add(p2);
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final void run()
    {
        int v1_0 = 0;
        int v2 = 1;
        try {
            while(true) {
                com.a.b.b.cn.b(this.e);
                String v0_3 = ((com.a.b.n.a.dt) this.d.poll());
                if (v0_3 == null) {
                    break;
                }
                try {
                    v0_3.a(this.b);
                } catch (RuntimeException v3_1) {
                    StringBuilder v6_2 = String.valueOf(String.valueOf(this.b));
                    String v0_6 = String.valueOf(String.valueOf(v0_3.d));
                    com.a.b.n.a.ds.a.log(java.util.logging.Level.SEVERE, new StringBuilder(((v6_2.length() + 37) + v0_6.length())).append("Exception while executing callback: ").append(v6_2).append(".").append(v0_6).toString(), v3_1);
                }
            }
            this.e = 0;
            return;
        } catch (String v0_9) {
            if (v2 != 0) {
                this.e = 0;
            }
            throw v0_9;
        }
        try {
        } catch (String v0_11) {
            v1_0 = 1;
            try {
                throw v0_11;
            } catch (String v0_9) {
                v2 = v1_0;
            }
        } catch (String v0_11) {
        }
    }
}
