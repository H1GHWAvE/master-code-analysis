package com.a.b.n.a;
final class dd extends com.a.b.n.a.df implements com.a.b.n.a.bc {
    private final Exception a;

    dd(Exception p2)
    {
        this(0);
        this.a = p2;
        return;
    }

    public final Object a()
    {
        throw this.a;
    }

    public final Object a(long p2, java.util.concurrent.TimeUnit p4)
    {
        com.a.b.b.cn.a(p4);
        throw this.a;
    }

    public final Object get()
    {
        throw new java.util.concurrent.ExecutionException(this.a);
    }
}
