package com.a.b.n.a;
final class e implements java.lang.Runnable {
    final synthetic com.a.b.n.a.c a;

    e(com.a.b.n.a.c p1)
    {
        this.a = p1;
        return;
    }

    public final void run()
    {
        try {
            com.a.b.n.a.b.a();
            this.a.c();
        } catch (Throwable v0_9) {
            this.a.a(v0_9);
            throw com.a.b.b.ei.a(v0_9);
        }
        if (this.a.e()) {
            try {
                this.a.a.b();
            } catch (Throwable v0_6) {
                try {
                    com.a.b.n.a.b.c();
                } catch (Exception v1_1) {
                    com.a.b.n.a.b.l().log(java.util.logging.Level.WARNING, "Error while attempting to shut down the service after failure.", v1_1);
                }
                throw v0_6;
            }
        }
        com.a.b.n.a.b.c();
        this.a.d();
        return;
    }
}
