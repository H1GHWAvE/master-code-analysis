package com.a.b.n.a;
final class bi extends java.util.concurrent.locks.ReentrantReadWriteLock implements com.a.b.n.a.bf {
    final synthetic com.a.b.n.a.bd a;
    private final com.a.b.n.a.bh b;
    private final com.a.b.n.a.bj c;
    private final com.a.b.n.a.bl d;

    private bi(com.a.b.n.a.bd p2, com.a.b.n.a.bl p3)
    {
        this.a = p2;
        this(0);
        this.b = new com.a.b.n.a.bh(p2, this);
        this.c = new com.a.b.n.a.bj(p2, this);
        this.d = ((com.a.b.n.a.bl) com.a.b.b.cn.a(p3));
        return;
    }

    synthetic bi(com.a.b.n.a.bd p1, com.a.b.n.a.bl p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public final com.a.b.n.a.bl a()
    {
        return this.d;
    }

    public final boolean b()
    {
        if ((!this.isWriteLockedByCurrentThread()) && (this.getReadHoldCount() <= 0)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final bridge synthetic java.util.concurrent.locks.Lock readLock()
    {
        return this.readLock();
    }

    public final java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock readLock()
    {
        return this.b;
    }

    public final bridge synthetic java.util.concurrent.locks.Lock writeLock()
    {
        return this.writeLock();
    }

    public final java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock writeLock()
    {
        return this.c;
    }
}
