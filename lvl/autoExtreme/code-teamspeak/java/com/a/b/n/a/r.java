package com.a.b.n.a;
final class r implements java.lang.Runnable {
    final synthetic com.a.b.n.a.q a;

    r(com.a.b.n.a.q p1)
    {
        this.a = p1;
        return;
    }

    public final void run()
    {
        com.a.b.n.a.q.a(this.a).lock();
        try {
            this.a.a.a();
            com.a.b.n.a.q.a(this.a).unlock();
            return;
        } catch (RuntimeException v0_8) {
            com.a.b.n.a.q.a(this.a).unlock();
            throw v0_8;
        } catch (RuntimeException v0_6) {
            try {
                com.a.b.n.a.p.c();
            } catch (com.a.b.n.a.q v1_1) {
                com.a.b.n.a.p.m().log(java.util.logging.Level.WARNING, "Error while attempting to shut down the service after failure.", v1_1);
            }
            this.a.a(v0_6);
            throw com.a.b.b.ei.a(v0_6);
        }
    }
}
