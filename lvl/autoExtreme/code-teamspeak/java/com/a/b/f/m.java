package com.a.b.f;
final class m implements com.a.b.f.q {
    private final java.util.logging.Logger a;

    public m(String p6)
    {
        StringBuilder v1_0 = String.valueOf(String.valueOf(com.a.b.f.h.getName()));
        java.util.logging.Logger v0_6 = String.valueOf(String.valueOf(((String) com.a.b.b.cn.a(p6))));
        this.a = java.util.logging.Logger.getLogger(new StringBuilder(((v1_0.length() + 1) + v0_6.length())).append(v1_0).append(".").append(v0_6).toString());
        return;
    }

    public final void a(Throwable p8, com.a.b.f.p p9)
    {
        java.util.logging.Logger v0 = this.a;
        String v2_2 = String.valueOf(String.valueOf(p9.a));
        Throwable v3_2 = String.valueOf(String.valueOf(p9.b));
        v0.log(java.util.logging.Level.SEVERE, new StringBuilder(((v2_2.length() + 30) + v3_2.length())).append("Could not dispatch event: ").append(v2_2).append(" to ").append(v3_2).toString(), p8.getCause());
        return;
    }
}
