package com.a.b.f;
final class d {
    private final String a;
    private final java.util.List b;

    d(reflect.Method p2)
    {
        this.a = p2.getName();
        this.b = java.util.Arrays.asList(p2.getParameterTypes());
        return;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.f.d)) && ((this.a.equals(((com.a.b.f.d) p4).a)) && (this.b.equals(((com.a.b.f.d) p4).b)))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[2];
        v0_1[0] = this.a;
        v0_1[1] = this.b;
        return java.util.Arrays.hashCode(v0_1);
    }
}
