package com.a.b.f;
public final class e extends com.a.b.f.h {
    private final java.util.concurrent.Executor a;
    private final java.util.concurrent.ConcurrentLinkedQueue b;

    private e(String p2, java.util.concurrent.Executor p3)
    {
        this(p2);
        this.b = new java.util.concurrent.ConcurrentLinkedQueue();
        this.a = ((java.util.concurrent.Executor) com.a.b.b.cn.a(p3));
        return;
    }

    private e(java.util.concurrent.Executor p2)
    {
        this("default");
        this.b = new java.util.concurrent.ConcurrentLinkedQueue();
        this.a = ((java.util.concurrent.Executor) com.a.b.b.cn.a(p2));
        return;
    }

    private e(java.util.concurrent.Executor p2, com.a.b.f.q p3)
    {
        this(p3);
        this.b = new java.util.concurrent.ConcurrentLinkedQueue();
        this.a = ((java.util.concurrent.Executor) com.a.b.b.cn.a(p2));
        return;
    }

    static synthetic void a(com.a.b.f.e p0, Object p1, com.a.b.f.n p2)
    {
        super.b(p1, p2);
        return;
    }

    protected final void a()
    {
        while(true) {
            com.a.b.f.n v0_2 = ((com.a.b.f.l) this.b.poll());
            if (v0_2 == null) {
                break;
            }
            this.b(v0_2.a, v0_2.b);
        }
        return;
    }

    final void a(Object p3, com.a.b.f.n p4)
    {
        this.b.offer(new com.a.b.f.l(p3, p4));
        return;
    }

    final void b(Object p3, com.a.b.f.n p4)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p4);
        this.a.execute(new com.a.b.f.f(this, p3, p4));
        return;
    }
}
