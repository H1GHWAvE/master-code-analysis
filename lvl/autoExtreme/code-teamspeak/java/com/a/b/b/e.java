package com.a.b.b;
public final class e {
    public static final byte A = 0x17;
    public static final byte B = 0x18;
    public static final byte C = 0x19;
    public static final byte D = 0x1a;
    public static final byte E = 0x1b;
    public static final byte F = 0x1c;
    public static final byte G = 0x1d;
    public static final byte H = 0x1e;
    public static final byte I = 0x1f;
    public static final byte J = 0x20;
    public static final byte K = 0x20;
    public static final byte L = 0x7f;
    public static final char M = 0;
    public static final char N = 127;
    public static final byte a = 0x0;
    public static final byte b = 0x1;
    public static final byte c = 0x2;
    public static final byte d = 0x3;
    public static final byte e = 0x4;
    public static final byte f = 0x5;
    public static final byte g = 0x6;
    public static final byte h = 0x7;
    public static final byte i = 0x8;
    public static final byte j = 0x9;
    public static final byte k = 0xa;
    public static final byte l = 0xa;
    public static final byte m = 0xb;
    public static final byte n = 0xc;
    public static final byte o = 0xd;
    public static final byte p = 0xe;
    public static final byte q = 0xf;
    public static final byte r = 0x10;
    public static final byte s = 0x11;
    public static final byte t = 0x11;
    public static final byte u = 0x12;
    public static final byte v = 0x13;
    public static final byte w = 0x13;
    public static final byte x = 0x14;
    public static final byte y = 0x15;
    public static final byte z = 0x16;

    private e()
    {
        return;
    }

    public static char a(char p1)
    {
        if (com.a.b.b.e.d(p1)) {
            p1 = ((char) (p1 ^ 32));
        }
        return p1;
    }

    private static String a(CharSequence p4)
    {
        String v0_2;
        if (!(p4 instanceof String)) {
            int v1 = p4.length();
            StringBuilder v2_1 = new StringBuilder(v1);
            String v0_1 = 0;
            while (v0_1 < v1) {
                v2_1.append(com.a.b.b.e.a(p4.charAt(v0_1)));
                v0_1++;
            }
            v0_2 = v2_1.toString();
        } else {
            v0_2 = com.a.b.b.e.a(((String) p4));
        }
        return v0_2;
    }

    public static String a(CharSequence p8, String p9)
    {
        StringBuilder v0_1;
        com.a.b.b.cn.a(p8);
        int v3 = (30 - p9.length());
        if (v3 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v5_1 = new Object[2];
        v5_1[0] = Integer.valueOf(30);
        v5_1[1] = Integer.valueOf(p9.length());
        com.a.b.b.cn.a(v0_1, "maxLength (%s) must be >= length of the truncation indicator (%s)", v5_1);
        if (p8.length() > 30) {
            p8 = new StringBuilder(30).append(p8, 0, v3).append(p9).toString();
        } else {
            p8 = p8.toString();
            if (p8.length() > 30) {
            }
        }
        return p8;
    }

    public static String a(String p5)
    {
        int v1 = p5.length();
        int v0 = 0;
        while (v0 < v1) {
            if (!com.a.b.b.e.d(p5.charAt(v0))) {
                v0++;
            } else {
                char[] v2_2 = p5.toCharArray();
                while (v0 < v1) {
                    char v3_0 = v2_2[v0];
                    if (com.a.b.b.e.d(v3_0)) {
                        v2_2[v0] = ((char) (v3_0 ^ 32));
                    }
                    v0++;
                }
                p5 = String.valueOf(v2_2);
                break;
            }
        }
        return p5;
    }

    private static boolean a(CharSequence p7, CharSequence p8)
    {
        int v0 = 1;
        int v3 = p7.length();
        if (p7 != p8) {
            if (v3 == p8.length()) {
                int v2_1 = 0;
                while (v2_1 < v3) {
                    int v4_0 = p7.charAt(v2_1);
                    int v5_0 = p8.charAt(v2_1);
                    if (v4_0 != v5_0) {
                        int v4_1 = com.a.b.b.e.e(v4_0);
                        if ((v4_1 >= 26) || (v4_1 != com.a.b.b.e.e(v5_0))) {
                            v0 = 0;
                            break;
                        }
                    }
                    v2_1++;
                }
            } else {
                v0 = 0;
            }
        }
        return v0;
    }

    public static char b(char p1)
    {
        if (com.a.b.b.e.c(p1)) {
            p1 = ((char) (p1 & 95));
        }
        return p1;
    }

    private static String b(CharSequence p4)
    {
        String v0_2;
        if (!(p4 instanceof String)) {
            int v1 = p4.length();
            StringBuilder v2_1 = new StringBuilder(v1);
            String v0_1 = 0;
            while (v0_1 < v1) {
                v2_1.append(com.a.b.b.e.b(p4.charAt(v0_1)));
                v0_1++;
            }
            v0_2 = v2_1.toString();
        } else {
            v0_2 = com.a.b.b.e.b(((String) p4));
        }
        return v0_2;
    }

    public static String b(String p5)
    {
        int v1 = p5.length();
        int v0 = 0;
        while (v0 < v1) {
            if (!com.a.b.b.e.c(p5.charAt(v0))) {
                v0++;
            } else {
                char[] v2_2 = p5.toCharArray();
                while (v0 < v1) {
                    char v3_0 = v2_2[v0];
                    if (com.a.b.b.e.c(v3_0)) {
                        v2_2[v0] = ((char) (v3_0 & 95));
                    }
                    v0++;
                }
                p5 = String.valueOf(v2_2);
                break;
            }
        }
        return p5;
    }

    public static boolean c(char p1)
    {
        if ((p1 < 97) || (p1 > 122)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public static boolean d(char p1)
    {
        if ((p1 < 65) || (p1 > 90)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private static int e(char p1)
    {
        return ((char) ((p1 | 32) - 97));
    }
}
