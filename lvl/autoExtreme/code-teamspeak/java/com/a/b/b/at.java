package com.a.b.b;
final class at extends com.a.b.b.ak implements java.io.Serializable {
    private static final long b;
    private final Class a;

    at(Class p2)
    {
        this.a = ((Class) com.a.b.b.cn.a(p2));
        return;
    }

    private Enum a(String p2)
    {
        return Enum.valueOf(this.a, p2);
    }

    private static String a(Enum p1)
    {
        return p1.name();
    }

    protected final synthetic Object a(Object p2)
    {
        return ((Enum) p2).name();
    }

    protected final synthetic Object b(Object p2)
    {
        return Enum.valueOf(this.a, ((String) p2));
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.at)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.b.at) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.a.getName()));
        return new StringBuilder((v0_3.length() + 29)).append("Enums.stringConverter(").append(v0_3).append(".class)").toString();
    }
}
