package com.a.b.b.a;
public class a implements java.lang.Runnable {
    private static final java.util.logging.Logger a = None;
    private static final String b = "com.google.common.base.FinalizableReference";
    private static final reflect.Field f;
    private final ref.WeakReference c;
    private final ref.PhantomReference d;
    private final ref.ReferenceQueue e;

    static a()
    {
        com.a.b.b.a.a.a = java.util.logging.Logger.getLogger(com.a.b.b.a.a.getName());
        com.a.b.b.a.a.f = com.a.b.b.a.a.b();
        return;
    }

    private a(Class p2, ref.ReferenceQueue p3, ref.PhantomReference p4)
    {
        this.e = p3;
        this.c = new ref.WeakReference(p2);
        this.d = p4;
        return;
    }

    private reflect.Method a()
    {
        NoSuchMethodException v0_3;
        NoSuchMethodException v0_2 = ((Class) this.c.get());
        if (v0_2 != null) {
            try {
                Class[] v2_1 = new Class[0];
                v0_3 = v0_2.getMethod("finalizeReferent", v2_1);
            } catch (NoSuchMethodException v0_4) {
                throw new AssertionError(v0_4);
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private static void a(Class p5, ref.ReferenceQueue p6, ref.PhantomReference p7)
    {
        if (p5.getName().equals("com.google.common.base.FinalizableReference")) {
            Thread v1_2 = new Thread(new com.a.b.b.a.a(p5, p6, p7));
            v1_2.setName(com.a.b.b.a.a.getName());
            v1_2.setDaemon(1);
            try {
                if (com.a.b.b.a.a.f != null) {
                    com.a.b.b.a.a.f.set(v1_2, 0);
                }
            } catch (reflect.Field v0_9) {
                com.a.b.b.a.a.a.log(java.util.logging.Level.INFO, "Failed to clear thread local values inherited by reference finalizer thread.", v0_9);
            }
            v1_2.start();
            return;
        } else {
            throw new IllegalArgumentException("Expected com.google.common.base.FinalizableReference.");
        }
    }

    private boolean a(ref.Reference p7)
    {
        int v0 = 0;
        reflect.Method v2 = this.a();
        if (v2 != null) {
            do {
                p7.clear();
                if (p7 != this.d) {
                    try {
                        ref.ReferenceQueue v1_2 = new Object[0];
                        v2.invoke(p7, v1_2);
                    } catch (ref.ReferenceQueue v1_3) {
                        com.a.b.b.a.a.a.log(java.util.logging.Level.SEVERE, "Error cleaning up after reference.", v1_3);
                    }
                    p7 = this.e.poll();
                }
            } while(p7 != null);
            v0 = 1;
        }
        return v0;
    }

    private static reflect.Field b()
    {
        try {
            int v0_1 = Thread.getDeclaredField("inheritableThreadLocals");
            v0_1.setAccessible(1);
        } catch (int v0) {
            com.a.b.b.a.a.a.log(java.util.logging.Level.INFO, "Couldn\'t access Thread.inheritableThreadLocals. Reference finalizer threads will inherit thread local values.");
            v0_1 = 0;
        }
        return v0_1;
    }

    public void run()
    {
        try {
            while (this.a(this.e.remove())) {
            }
        } catch (InterruptedException v0) {
        }
        return;
    }
}
