package com.a.b.b;
final class cs implements com.a.b.b.co, java.io.Serializable {
    private static final long b;
    private final Class a;

    private cs(Class p2)
    {
        this.a = ((Class) com.a.b.b.cn.a(p2));
        return;
    }

    synthetic cs(Class p1, byte p2)
    {
        this(p1);
        return;
    }

    private boolean a(Class p2)
    {
        return this.a.isAssignableFrom(p2);
    }

    public final synthetic boolean a(Object p2)
    {
        return this.a.isAssignableFrom(((Class) p2));
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.b.cs)) && (this.a == ((com.a.b.b.cs) p4).a)) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.a.getName()));
        return new StringBuilder((v0_3.length() + 27)).append("Predicates.assignableFrom(").append(v0_3).append(")").toString();
    }
}
