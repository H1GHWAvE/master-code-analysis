package com.a.b.b;
final class cw implements com.a.b.b.co, java.io.Serializable {
    private static final long b;
    private final java.util.Collection a;

    private cw(java.util.Collection p2)
    {
        this.a = ((java.util.Collection) com.a.b.b.cn.a(p2));
        return;
    }

    synthetic cw(java.util.Collection p1, byte p2)
    {
        this(p1);
        return;
    }

    public final boolean a(Object p3)
    {
        try {
            boolean v0 = this.a.contains(p3);
        } catch (ClassCastException v1) {
        } catch (ClassCastException v1) {
        }
        return v0;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.cw)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.b.cw) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 15)).append("Predicates.in(").append(v0_2).append(")").toString();
    }
}
