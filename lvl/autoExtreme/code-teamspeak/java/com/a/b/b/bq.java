package com.a.b.b;
final class bq implements com.a.b.b.bj, java.io.Serializable {
    private static final long b;
    final java.util.Map a;

    bq(java.util.Map p2)
    {
        this.a = ((java.util.Map) com.a.b.b.cn.a(p2));
        return;
    }

    public final Object e(Object p6)
    {
        int v0_3;
        Object v3 = this.a.get(p6);
        if ((v3 == null) && (!this.a.containsKey(p6))) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        Object[] v2_1 = new Object[1];
        v2_1[0] = p6;
        com.a.b.b.cn.a(v0_3, "Key \'%s\' not present in map", v2_1);
        return v3;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.bq)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.b.bq) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 8)).append("forMap(").append(v0_2).append(")").toString();
    }
}
