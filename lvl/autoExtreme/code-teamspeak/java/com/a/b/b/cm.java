package com.a.b.b;
final class cm {

    private cm()
    {
        return;
    }

    private static long a()
    {
        return System.nanoTime();
    }

    private static com.a.b.b.ci a(Class p1, String p2)
    {
        com.a.b.b.ci v0_5;
        com.a.b.b.ci v0_2 = ((ref.WeakReference) com.a.b.b.as.a(p1).get(p2));
        if (v0_2 != null) {
            v0_5 = com.a.b.b.ci.b(p1.cast(v0_2.get()));
        } else {
            v0_5 = com.a.b.b.ci.f();
        }
        return v0_5;
    }

    private static com.a.b.b.m a(com.a.b.b.m p6)
    {
        String v0_8;
        java.util.BitSet v2_1 = new java.util.BitSet();
        p6.a(v2_1);
        String v0_0 = v2_1.cardinality();
        if ((v0_0 * 2) > 65536) {
            String v0_4;
            v2_1.flip(0, 65536);
            int v3_1 = (65536 - v0_0);
            if (!p6.n.endsWith(".negate()")) {
                com.a.b.b.af v1_4 = String.valueOf(p6.n);
                String v0_2 = String.valueOf(".negate()");
                if (v0_2.length() == 0) {
                    v0_4 = new String(v1_4);
                } else {
                    v0_4 = v1_4.concat(v0_2);
                }
            } else {
                v0_4 = p6.n.substring(0, (p6.n.length() - ".negate()".length()));
            }
            v0_8 = new com.a.b.b.af(p6.toString(), com.a.b.b.m.a(v3_1, v2_1, v0_4));
        } else {
            v0_8 = com.a.b.b.m.a(v0_0, v2_1, p6.n);
        }
        return v0_8;
    }
}
