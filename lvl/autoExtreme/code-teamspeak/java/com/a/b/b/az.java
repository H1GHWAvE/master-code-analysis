package com.a.b.b;
public final class az implements java.io.Serializable {
    private static final long c;
    private final com.a.b.b.au a;
    private final Object b;

    private az(com.a.b.b.au p2, Object p3)
    {
        this.a = ((com.a.b.b.au) com.a.b.b.cn.a(p2));
        this.b = p3;
        return;
    }

    synthetic az(com.a.b.b.au p1, Object p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    private Object a()
    {
        return this.b;
    }

    public final boolean equals(Object p4)
    {
        boolean v0_3;
        if (p4 != this) {
            if ((!(p4 instanceof com.a.b.b.az)) || (!this.a.equals(((com.a.b.b.az) p4).a))) {
                v0_3 = 0;
            } else {
                v0_3 = this.a.a(this.b, ((com.a.b.b.az) p4).b);
            }
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final int hashCode()
    {
        return this.a.a(this.b);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_2.length() + 7) + v1_2.length())).append(v0_2).append(".wrap(").append(v1_2).append(")").toString();
    }
}
