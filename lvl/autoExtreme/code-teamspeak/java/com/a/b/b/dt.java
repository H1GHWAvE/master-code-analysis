package com.a.b.b;
abstract class dt extends com.a.b.b.b {
    final CharSequence c;
    final com.a.b.b.m d;
    final boolean e;
    int f;
    int g;

    protected dt(com.a.b.b.di p2, CharSequence p3)
    {
        this.f = 0;
        this.d = p2.a;
        this.e = p2.b;
        this.g = p2.c;
        this.c = p3;
        return;
    }

    private String c()
    {
        int v0_0 = this.f;
        while (this.f != -1) {
            boolean v1_2 = this.a(this.f);
            if (v1_2 != -1) {
                this.f = this.b(v1_2);
            } else {
                v1_2 = this.c.length();
                this.f = -1;
            }
            if (this.f != v0_0) {
                int v2_2 = v0_0;
                while ((v2_2 < v1_2) && (this.d.c(this.c.charAt(v2_2)))) {
                    v2_2++;
                }
                int v0_4 = v1_2;
                while ((v0_4 > v2_2) && (this.d.c(this.c.charAt((v0_4 - 1))))) {
                    v0_4--;
                }
                if ((!this.e) || (v2_2 != v0_4)) {
                    if (this.g != 1) {
                        this.g = (this.g - 1);
                    } else {
                        v0_4 = this.c.length();
                        this.f = -1;
                        while ((v0_4 > v2_2) && (this.d.c(this.c.charAt((v0_4 - 1))))) {
                            v0_4--;
                        }
                    }
                    int v0_1 = this.c.subSequence(v2_2, v0_4).toString();
                } else {
                    v0_0 = this.f;
                }
            } else {
                this.f = (this.f + 1);
                if (this.f >= this.c.length()) {
                    this.f = -1;
                }
            }
            return v0_1;
        }
        this.b();
        v0_1 = 0;
        return v0_1;
    }

    abstract int a();

    protected final synthetic Object a()
    {
        int v0_0 = this.f;
        while (this.f != -1) {
            boolean v1_2 = this.a(this.f);
            if (v1_2 != -1) {
                this.f = this.b(v1_2);
            } else {
                v1_2 = this.c.length();
                this.f = -1;
            }
            if (this.f != v0_0) {
                int v2_2 = v0_0;
                while ((v2_2 < v1_2) && (this.d.c(this.c.charAt(v2_2)))) {
                    v2_2++;
                }
                int v0_4 = v1_2;
                while ((v0_4 > v2_2) && (this.d.c(this.c.charAt((v0_4 - 1))))) {
                    v0_4--;
                }
                if ((!this.e) || (v2_2 != v0_4)) {
                    if (this.g != 1) {
                        this.g = (this.g - 1);
                    } else {
                        v0_4 = this.c.length();
                        this.f = -1;
                        while ((v0_4 > v2_2) && (this.d.c(this.c.charAt((v0_4 - 1))))) {
                            v0_4--;
                        }
                    }
                    int v0_1 = this.c.subSequence(v2_2, v0_4).toString();
                } else {
                    v0_0 = this.f;
                }
            } else {
                this.f = (this.f + 1);
                if (this.f >= this.c.length()) {
                    this.f = -1;
                }
            }
            return v0_1;
        }
        this.b();
        v0_1 = 0;
        return v0_1;
    }

    abstract int b();
}
