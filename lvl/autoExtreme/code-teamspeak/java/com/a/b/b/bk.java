package com.a.b.b;
final class bk extends com.a.b.b.au implements java.io.Serializable {
    private static final long a;
    private final com.a.b.b.bj b;
    private final com.a.b.b.au c;

    bk(com.a.b.b.bj p2, com.a.b.b.au p3)
    {
        this.b = ((com.a.b.b.bj) com.a.b.b.cn.a(p2));
        this.c = ((com.a.b.b.au) com.a.b.b.cn.a(p3));
        return;
    }

    protected final int b(Object p3)
    {
        return this.c.a(this.b.e(p3));
    }

    protected final boolean b(Object p4, Object p5)
    {
        return this.c.a(this.b.e(p4), this.b.e(p5));
    }

    public final boolean equals(Object p5)
    {
        int v0 = 1;
        if (p5 != this) {
            if (!(p5 instanceof com.a.b.b.bk)) {
                v0 = 0;
            } else {
                if ((!this.b.equals(((com.a.b.b.bk) p5).b)) || (!this.c.equals(((com.a.b.b.bk) p5).c))) {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[2];
        v0_1[0] = this.b;
        v0_1[1] = this.c;
        return java.util.Arrays.hashCode(v0_1);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.c));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_2.length() + 13) + v1_2.length())).append(v0_2).append(".onResultOf(").append(v1_2).append(")").toString();
    }
}
