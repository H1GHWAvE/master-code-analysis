package com.a.b.b;
final class z extends com.a.b.b.ae {

    z(String p1)
    {
        this(p1);
        return;
    }

    public final int a(CharSequence p2, int p3)
    {
        int v0 = p2.length();
        com.a.b.b.cn.b(p3, v0);
        if (p3 == v0) {
            p3 = -1;
        }
        return p3;
    }

    public final com.a.b.b.m a()
    {
        return com.a.b.b.z.m;
    }

    public final com.a.b.b.m a(com.a.b.b.m p2)
    {
        return ((com.a.b.b.m) com.a.b.b.cn.a(p2));
    }

    public final String a(CharSequence p3, char p4)
    {
        char[] v0_1 = new char[p3.length()];
        java.util.Arrays.fill(v0_1, p4);
        return new String(v0_1);
    }

    public final String a(CharSequence p4, CharSequence p5)
    {
        StringBuilder v1_1 = new StringBuilder((p4.length() * p5.length()));
        String v0_2 = 0;
        while (v0_2 < p4.length()) {
            v1_1.append(p5);
            v0_2++;
        }
        return v1_1.toString();
    }

    public final com.a.b.b.m b(com.a.b.b.m p1)
    {
        com.a.b.b.cn.a(p1);
        return this;
    }

    public final String b(CharSequence p2, char p3)
    {
        String v0_1;
        if (p2.length() != 0) {
            v0_1 = String.valueOf(p3);
        } else {
            v0_1 = "";
        }
        return v0_1;
    }

    public final boolean c(char p2)
    {
        return 1;
    }

    public final boolean c(CharSequence p2)
    {
        com.a.b.b.cn.a(p2);
        return 1;
    }

    public final boolean d(CharSequence p2)
    {
        int v0_1;
        if (p2.length() != 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int e(CharSequence p2)
    {
        int v0_1;
        if (p2.length() != 0) {
            v0_1 = 0;
        } else {
            v0_1 = -1;
        }
        return v0_1;
    }

    public final int f(CharSequence p2)
    {
        return (p2.length() - 1);
    }

    public final int g(CharSequence p2)
    {
        return p2.length();
    }

    public final String h(CharSequence p2)
    {
        com.a.b.b.cn.a(p2);
        return "";
    }

    public final String i(CharSequence p2)
    {
        com.a.b.b.cn.a(p2);
        return "";
    }
}
