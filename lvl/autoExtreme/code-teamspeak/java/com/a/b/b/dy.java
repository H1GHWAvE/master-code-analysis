package com.a.b.b;
public final class dy {

    private dy()
    {
        return;
    }

    private static String a(CharSequence p5, CharSequence p6)
    {
        com.a.b.b.cn.a(p5);
        com.a.b.b.cn.a(p6);
        boolean v2_1 = Math.min(p5.length(), p6.length());
        String v0_1 = 0;
        while ((v0_1 < v2_1) && (p5.charAt(v0_1) == p6.charAt(v0_1))) {
            v0_1++;
        }
        if ((com.a.b.b.dy.a(p5, (v0_1 - 1))) || (com.a.b.b.dy.a(p6, (v0_1 - 1)))) {
            v0_1--;
        }
        return p5.subSequence(0, v0_1).toString();
    }

    private static String a(String p8, int p9)
    {
        com.a.b.b.cn.a(p8);
        if (p9 > 1) {
            int v0_0 = p8.length();
            int v4_1 = (((long) v0_0) * ((long) p9));
            int v1_1 = ((int) v4_1);
            if (((long) v1_1) == v4_1) {
                String v3_1 = new char[v1_1];
                p8.getChars(0, v0_0, v3_1, 0);
                while (v0_0 < (v1_1 - v0_0)) {
                    System.arraycopy(v3_1, 0, v3_1, v0_0, v0_0);
                    v0_0 <<= 1;
                }
                System.arraycopy(v3_1, 0, v3_1, v0_0, (v1_1 - v0_0));
                p8 = new String(v3_1);
            } else {
                throw new ArrayIndexOutOfBoundsException(new StringBuilder(51).append("Required array size too large: ").append(v4_1).toString());
            }
        } else {
            int v0_3;
            if (p9 < 0) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            int v1_8 = new Object[1];
            v1_8[0] = Integer.valueOf(p9);
            com.a.b.b.cn.a(v0_3, "invalid count: %s", v1_8);
            if (p9 == 0) {
                p8 = "";
            }
        }
        return p8;
    }

    private static String a(String p2, int p3, char p4)
    {
        com.a.b.b.cn.a(p2);
        if (p2.length() < p3) {
            StringBuilder v1_1 = new StringBuilder(p3);
            int v0_1 = p2.length();
            while (v0_1 < p3) {
                v1_1.append(p4);
                v0_1++;
            }
            v1_1.append(p2);
            p2 = v1_1.toString();
        }
        return p2;
    }

    private static boolean a(CharSequence p1, int p2)
    {
        if ((p2 < 0) || ((p2 > (p1.length() - 2)) || ((!Character.isHighSurrogate(p1.charAt(p2))) || (!Character.isLowSurrogate(p1.charAt((p2 + 1))))))) {
            int v0_7 = 0;
        } else {
            v0_7 = 1;
        }
        return v0_7;
    }

    public static boolean a(String p1)
    {
        if ((p1 != null) && (p1.length() != 0)) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static String b(CharSequence p4, CharSequence p5)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.b.cn.a(p5);
        int v1_1 = Math.min(p4.length(), p5.length());
        String v0_1 = 0;
        while ((v0_1 < v1_1) && (p4.charAt(((p4.length() - v0_1) - 1)) == p5.charAt(((p5.length() - v0_1) - 1)))) {
            v0_1++;
        }
        if ((com.a.b.b.dy.a(p4, ((p4.length() - v0_1) - 1))) || (com.a.b.b.dy.a(p5, ((p5.length() - v0_1) - 1)))) {
            v0_1--;
        }
        return p4.subSequence((p4.length() - v0_1), p4.length()).toString();
    }

    private static String b(String p0)
    {
        if (p0 == null) {
            p0 = "";
        }
        return p0;
    }

    private static String b(String p2, int p3, char p4)
    {
        com.a.b.b.cn.a(p2);
        if (p2.length() < p3) {
            StringBuilder v1_1 = new StringBuilder(p3);
            v1_1.append(p2);
            int v0_1 = p2.length();
            while (v0_1 < p3) {
                v1_1.append(p4);
                v0_1++;
            }
            p2 = v1_1.toString();
        }
        return p2;
    }

    private static String c(String p1)
    {
        if (com.a.b.b.dy.a(p1)) {
            p1 = 0;
        }
        return p1;
    }
}
