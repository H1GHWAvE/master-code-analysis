package com.a.b.b;
 class ag extends com.a.b.b.m {
    final com.a.b.b.m s;

    ag(com.a.b.b.m p4)
    {
        String v0_1 = String.valueOf(String.valueOf(p4));
        this(new StringBuilder((v0_1.length() + 9)).append(v0_1).append(".negate()").toString(), p4);
        return;
    }

    ag(String p1, com.a.b.b.m p2)
    {
        this(p1);
        this.s = p2;
        return;
    }

    public final com.a.b.b.m a()
    {
        return this.s;
    }

    com.a.b.b.m a(String p3)
    {
        return new com.a.b.b.ag(p3, this.s);
    }

    final void a(java.util.BitSet p4)
    {
        java.util.BitSet v0_1 = new java.util.BitSet();
        this.s.a(v0_1);
        v0_1.flip(0, 65536);
        p4.or(v0_1);
        return;
    }

    public final bridge synthetic boolean a(Object p2)
    {
        return super.a(((Character) p2));
    }

    public final boolean c(char p2)
    {
        int v0_2;
        if (this.s.c(p2)) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean c(CharSequence p2)
    {
        return this.s.d(p2);
    }

    public final boolean d(CharSequence p2)
    {
        return this.s.c(p2);
    }

    public final int g(CharSequence p3)
    {
        return (p3.length() - this.s.g(p3));
    }
}
