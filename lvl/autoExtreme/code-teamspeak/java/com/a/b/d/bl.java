package com.a.b.d;
public final class bl extends com.a.b.d.bf implements java.io.Serializable {
    private static final long h;
    private final com.a.b.d.jl a;
    private final com.a.b.d.jl b;
    private final com.a.b.d.jt c;
    private final com.a.b.d.jt d;
    private final Object[][] e;
    private transient com.a.b.d.bt f;
    private transient com.a.b.d.bv g;

    private bl(com.a.b.d.adv p3)
    {
        this(p3.a(), p3.b());
        super.a(p3);
        return;
    }

    private bl(com.a.b.d.bl p7)
    {
        this.a = p7.a;
        this.b = p7.b;
        this.c = p7.c;
        this.d = p7.d;
        Object[][] v0_9 = ((Object[][]) ((Object[][]) reflect.Array.newInstance(Object, new int[] {this.a.size(), this.b.size()}))));
        this.e = v0_9;
        this.p();
        int v1_3 = 0;
        while (v1_3 < this.a.size()) {
            System.arraycopy(p7.e[v1_3], 0, v0_9[v1_3], 0, p7.e[v1_3].length);
            v1_3++;
        }
        return;
    }

    private bl(Iterable p4, Iterable p5)
    {
        Object[][] v0_4;
        int v1_0 = 1;
        this.a = com.a.b.d.jl.a(p4);
        this.b = com.a.b.d.jl.a(p5);
        if (this.a.isEmpty()) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        com.a.b.b.cn.a(v0_4);
        if (this.b.isEmpty()) {
            v1_0 = 0;
        }
        com.a.b.b.cn.a(v1_0);
        this.c = com.a.b.d.bl.a(this.a);
        this.d = com.a.b.d.bl.a(this.b);
        this.e = ((Object[][]) ((Object[][]) reflect.Array.newInstance(Object, new int[] {this.a.size(), this.b.size()}))));
        this.p();
        return;
    }

    private static com.a.b.d.bl a(Iterable p1, Iterable p2)
    {
        return new com.a.b.d.bl(p1, p2);
    }

    static synthetic com.a.b.d.jl a(com.a.b.d.bl p1)
    {
        return p1.b;
    }

    private static com.a.b.d.jt a(java.util.List p4)
    {
        com.a.b.d.ju v1 = com.a.b.d.jt.l();
        com.a.b.d.jt v0_0 = 0;
        while (v0_0 < p4.size()) {
            v1.a(p4.get(v0_0), Integer.valueOf(v0_0));
            v0_0++;
        }
        return v1.a();
    }

    private Object[][] a(Class p7)
    {
        Object[][] v0_1 = new int[2];
        v0_1[0] = this.a.size();
        v0_1[1] = this.b.size();
        Object[][] v0_4 = ((Object[][]) ((Object[][]) reflect.Array.newInstance(p7, v0_1)));
        int v1_3 = 0;
        while (v1_3 < this.a.size()) {
            System.arraycopy(this.e[v1_3], 0, v0_4[v1_3], 0, this.e[v1_3].length);
            v1_3++;
        }
        return v0_4;
    }

    private static com.a.b.d.bl b(com.a.b.d.adv p1)
    {
        com.a.b.d.bl v0_2;
        if (!(p1 instanceof com.a.b.d.bl)) {
            v0_2 = new com.a.b.d.bl(p1);
        } else {
            v0_2 = new com.a.b.d.bl(((com.a.b.d.bl) p1));
        }
        return v0_2;
    }

    static synthetic com.a.b.d.jl b(com.a.b.d.bl p1)
    {
        return p1.a;
    }

    static synthetic com.a.b.d.jt c(com.a.b.d.bl p1)
    {
        return p1.c;
    }

    static synthetic com.a.b.d.jt d(com.a.b.d.bl p1)
    {
        return p1.d;
    }

    private Object d(Object p4, Object p5)
    {
        Object v0_4;
        Object v0_2 = ((Integer) this.c.get(p4));
        int v1_2 = ((Integer) this.d.get(p5));
        if ((v0_2 != null) && (v1_2 != 0)) {
            v0_4 = this.a(v0_2.intValue(), v1_2.intValue(), 0);
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    private com.a.b.d.jl n()
    {
        return this.a;
    }

    private com.a.b.d.jl o()
    {
        return this.b;
    }

    private void p()
    {
        Object[][] v1 = this.e;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            java.util.Arrays.fill(v1[v0], 0);
            v0++;
        }
        return;
    }

    private com.a.b.d.lo q()
    {
        return this.d.g();
    }

    private com.a.b.d.lo r()
    {
        return this.c.g();
    }

    public final Object a(int p2, int p3)
    {
        com.a.b.b.cn.a(p2, this.a.size());
        com.a.b.b.cn.a(p3, this.b.size());
        return this.e[p2][p3];
    }

    public final Object a(int p3, int p4, Object p5)
    {
        com.a.b.b.cn.a(p3, this.a.size());
        com.a.b.b.cn.a(p4, this.b.size());
        Object v0_6 = this.e[p3][p4];
        this.e[p3][p4] = p5;
        return v0_6;
    }

    public final Object a(Object p9, Object p10, Object p11)
    {
        int v1_0;
        com.a.b.b.cn.a(p9);
        com.a.b.b.cn.a(p10);
        Object v0_2 = ((Integer) this.c.get(p9));
        if (v0_2 == null) {
            v1_0 = 0;
        } else {
            v1_0 = 1;
        }
        int v4_1;
        String v5_0 = new Object[2];
        v5_0[0] = p9;
        v5_0[1] = this.a;
        com.a.b.b.cn.a(v1_0, "Row %s not in %s", v5_0);
        int v1_3 = ((Integer) this.d.get(p10));
        if (v1_3 == 0) {
            v4_1 = 0;
        } else {
            v4_1 = 1;
        }
        Object[] v6_1 = new Object[2];
        v6_1[0] = p10;
        v6_1[1] = this.b;
        com.a.b.b.cn.a(v4_1, "Column %s not in %s", v6_1);
        return this.a(v0_2.intValue(), v1_3.intValue(), p11);
    }

    public final synthetic java.util.Set a()
    {
        return this.c.g();
    }

    public final void a(com.a.b.d.adv p1)
    {
        super.a(p1);
        return;
    }

    public final boolean a(Object p2)
    {
        return this.c.containsKey(p2);
    }

    public final boolean a(Object p2, Object p3)
    {
        if ((!this.a(p2)) || (!this.b(p3))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final Object b(Object p3, Object p4)
    {
        Object v0_4;
        Object v0_2 = ((Integer) this.c.get(p3));
        int v1_2 = ((Integer) this.d.get(p4));
        if ((v0_2 != null) && (v1_2 != 0)) {
            v0_4 = this.a(v0_2.intValue(), v1_2.intValue());
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    public final synthetic java.util.Set b()
    {
        return this.d.g();
    }

    public final boolean b(Object p2)
    {
        return this.d.containsKey(p2);
    }

    public final Object c(Object p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean c()
    {
        return 0;
    }

    public final boolean c(Object p9)
    {
        int v0 = 0;
        Object[][] v3 = this.e;
        int v2 = 0;
        while (v2 < v3.length) {
            Object[] v5 = v3[v2];
            int v1_0 = 0;
            while (v1_0 < v5.length) {
                if (!com.a.b.b.ce.a(p9, v5[v1_0])) {
                    v1_0++;
                } else {
                    v0 = 1;
                }
                return v0;
            }
            v2++;
        }
        return v0;
    }

    public final java.util.Map d(Object p3)
    {
        com.a.b.d.bs v0_4;
        com.a.b.b.cn.a(p3);
        com.a.b.d.bs v0_2 = ((Integer) this.d.get(p3));
        if (v0_2 != null) {
            v0_4 = new com.a.b.d.bs(this, v0_2.intValue());
        } else {
            v0_4 = com.a.b.d.jt.k();
        }
        return v0_4;
    }

    public final void d()
    {
        throw new UnsupportedOperationException();
    }

    public final java.util.Map e(Object p3)
    {
        com.a.b.d.bu v0_4;
        com.a.b.b.cn.a(p3);
        com.a.b.d.bu v0_2 = ((Integer) this.c.get(p3));
        if (v0_2 != null) {
            v0_4 = new com.a.b.d.bu(this, v0_2.intValue());
        } else {
            v0_4 = com.a.b.d.jt.k();
        }
        return v0_4;
    }

    public final java.util.Set e()
    {
        return super.e();
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    final java.util.Iterator g()
    {
        return new com.a.b.d.bm(this, this.k());
    }

    public final java.util.Collection h()
    {
        return super.h();
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final int k()
    {
        return (this.a.size() * this.b.size());
    }

    public final java.util.Map l()
    {
        com.a.b.d.bt v0_0 = this.f;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.bt(this, 0);
            this.f = v0_0;
        }
        return v0_0;
    }

    public final java.util.Map m()
    {
        com.a.b.d.bv v0_0 = this.g;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.bv(this, 0);
            this.g = v0_0;
        }
        return v0_0;
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }
}
