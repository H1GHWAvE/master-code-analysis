package com.a.b.d;
abstract class xo implements com.a.b.d.xd {

    xo()
    {
        return;
    }

    public boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.d.xd)) && ((this.b() == ((com.a.b.d.xd) p4).b()) && (com.a.b.b.ce.a(this.a(), ((com.a.b.d.xd) p4).a())))) {
            v0 = 1;
        }
        return v0;
    }

    public int hashCode()
    {
        int v0_1;
        int v0_0 = this.a();
        if (v0_0 != 0) {
            v0_1 = v0_0.hashCode();
        } else {
            v0_1 = 0;
        }
        return (v0_1 ^ this.b());
    }

    public String toString()
    {
        String v0_1 = String.valueOf(this.a());
        int v1 = this.b();
        if (v1 != 1) {
            String v0_3 = String.valueOf(String.valueOf(v0_1));
            v0_1 = new StringBuilder((v0_3.length() + 14)).append(v0_3).append(" x ").append(v1).toString();
        }
        return v0_1;
    }
}
