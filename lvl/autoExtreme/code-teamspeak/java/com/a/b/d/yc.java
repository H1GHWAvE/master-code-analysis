package com.a.b.d;
public final class yc {
    static final Object[] a;

    static yc()
    {
        Object[] v0_1 = new Object[0];
        com.a.b.d.yc.a = v0_1;
        return;
    }

    private yc()
    {
        return;
    }

    static Object a(Object p3, int p4)
    {
        if (p3 != null) {
            return p3;
        } else {
            throw new NullPointerException(new StringBuilder(20).append("at index ").append(p4).toString());
        }
    }

    static void a(Object[] p2, int p3, int p4)
    {
        Object v0 = p2[p3];
        p2[p3] = p2[p4];
        p2[p4] = v0;
        return;
    }

    public static Object[] a(Class p1, int p2)
    {
        return ((Object[]) ((Object[]) reflect.Array.newInstance(p1, p2)));
    }

    private static Object[] a(Iterable p4, Object[] p5)
    {
        int v0 = 0;
        java.util.Iterator v2 = p4.iterator();
        while (v2.hasNext()) {
            int v1_1 = (v0 + 1);
            p5[v0] = v2.next();
            v0 = v1_1;
        }
        return p5;
    }

    private static Object[] a(Object p4, Object[] p5)
    {
        Object[] v0_2 = com.a.b.d.yc.a(p5, (p5.length + 1));
        v0_2[0] = p4;
        System.arraycopy(p5, 0, v0_2, 1, p5.length);
        return v0_2;
    }

    static Object[] a(java.util.Collection p1)
    {
        Object[] v0_1 = new Object[p1.size()];
        return com.a.b.d.yc.a(p1, v0_1);
    }

    public static Object[] a(java.util.Collection p2, Object[] p3)
    {
        int v0 = p2.size();
        if (p3.length < v0) {
            p3 = com.a.b.d.yc.a(p3, v0);
        }
        com.a.b.d.yc.a(p2, p3);
        if (p3.length > v0) {
            p3[v0] = 0;
        }
        return p3;
    }

    static varargs Object[] a(Object[] p1)
    {
        return com.a.b.d.yc.c(p1, p1.length);
    }

    public static Object[] a(Object[] p1, int p2)
    {
        return ((Object[]) ((Object[]) reflect.Array.newInstance(p1.getClass().getComponentType(), p2)));
    }

    private static Object[] a(Object[] p2, int p3, int p4, Object[] p5)
    {
        com.a.b.b.cn.a(p3, (p3 + p4), p2.length);
        if (p5.length >= p4) {
            if (p5.length > p4) {
                p5[p4] = 0;
            }
        } else {
            p5 = com.a.b.d.yc.a(p5, p4);
        }
        System.arraycopy(p2, p3, p5, 0, p4);
        return p5;
    }

    private static Object[] a(Object[] p2, Object p3)
    {
        Object[] v0_2 = com.a.b.d.yc.b(p2, (p2.length + 1));
        v0_2[p2.length] = p3;
        return v0_2;
    }

    public static Object[] a(Object[] p4, Object[] p5, Class p6)
    {
        Object[] v0_2 = com.a.b.d.yc.a(p6, (p4.length + p5.length));
        System.arraycopy(p4, 0, v0_2, 0, p4.length);
        System.arraycopy(p5, 0, v0_2, p4.length, p5.length);
        return v0_2;
    }

    static Object[] b(Object[] p3, int p4)
    {
        Object[] v0 = com.a.b.d.yc.a(p3, p4);
        System.arraycopy(p3, 0, v0, 0, Math.min(p3.length, p4));
        return v0;
    }

    private static Object[] b(Object[] p2, int p3, int p4)
    {
        Object[] v0_1;
        com.a.b.b.cn.a(p3, (p3 + p4), p2.length);
        if (p4 != 0) {
            v0_1 = new Object[p4];
            System.arraycopy(p2, p3, v0_1, 0, p4);
        } else {
            v0_1 = com.a.b.d.yc.a;
        }
        return v0_1;
    }

    static Object[] c(Object[] p2, int p3)
    {
        int v0 = 0;
        while (v0 < p3) {
            com.a.b.d.yc.a(p2[v0], v0);
            v0++;
        }
        return p2;
    }
}
