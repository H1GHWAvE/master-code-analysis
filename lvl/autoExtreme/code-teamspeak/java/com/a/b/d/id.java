package com.a.b.d;
final class id extends com.a.b.d.am {
    com.a.b.d.ia a;
    final synthetic com.a.b.d.ic b;

    id(com.a.b.d.ic p1, com.a.b.d.ia p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final Object getKey()
    {
        return this.a.e;
    }

    public final Object getValue()
    {
        return this.a.f;
    }

    public final Object setValue(Object p7)
    {
        Object v3 = this.a.f;
        int v4 = com.a.b.d.hy.b(p7);
        if ((v4 != this.a.b) || (!com.a.b.b.ce.a(p7, v3))) {
            com.a.b.d.ia v0_8;
            if (com.a.b.d.hy.b(this.b.a.a, p7, v4) != null) {
                v0_8 = 0;
            } else {
                v0_8 = 1;
            }
            com.a.b.d.ic v1_1 = new Object[1];
            v1_1[0] = p7;
            com.a.b.b.cn.a(v0_8, "value already present: %s", v1_1);
            com.a.b.d.hy.a(this.b.a.a, this.a);
            com.a.b.d.ia v0_13 = new com.a.b.d.ia(this.a.e, this.a.a, p7, v4);
            com.a.b.d.hy.b(this.b.a.a, v0_13);
            this.b.e = com.a.b.d.hy.a(this.b.a.a);
            if (this.b.d == this.a) {
                this.b.d = v0_13;
            }
            this.a = v0_13;
            p7 = v3;
        }
        return p7;
    }
}
