package com.a.b.d;
 class aam extends com.a.b.d.aal implements java.util.SortedSet {

    aam(java.util.SortedSet p1, com.a.b.b.co p2)
    {
        this(p1, p2);
        return;
    }

    public java.util.Comparator comparator()
    {
        return ((java.util.SortedSet) this.a).comparator();
    }

    public Object first()
    {
        return this.iterator().next();
    }

    public java.util.SortedSet headSet(Object p4)
    {
        return new com.a.b.d.aam(((java.util.SortedSet) this.a).headSet(p4), this.b);
    }

    public Object last()
    {
        java.util.SortedSet v0_1 = ((java.util.SortedSet) this.a);
        while(true) {
            Object v1 = v0_1.last();
            if (this.b.a(v1)) {
                break;
            }
            v0_1 = v0_1.headSet(v1);
        }
        return v1;
    }

    public java.util.SortedSet subSet(Object p4, Object p5)
    {
        return new com.a.b.d.aam(((java.util.SortedSet) this.a).subSet(p4, p5), this.b);
    }

    public java.util.SortedSet tailSet(Object p4)
    {
        return new com.a.b.d.aam(((java.util.SortedSet) this.a).tailSet(p4), this.b);
    }
}
