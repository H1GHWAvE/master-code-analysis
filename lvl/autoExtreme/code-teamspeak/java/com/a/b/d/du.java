package com.a.b.d;
public abstract class du extends com.a.b.d.me {
    final com.a.b.d.ep a;

    du(com.a.b.d.ep p2)
    {
        this(com.a.b.d.yd.d());
        this.a = p2;
        return;
    }

    public static com.a.b.d.du a(com.a.b.d.yl p3, com.a.b.d.ep p4)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p4);
        try {
            com.a.b.d.ys v0_1;
            if (p3.d()) {
                v0_1 = p3;
            } else {
                v0_1 = p3.c(com.a.b.d.yl.b(p4.a()));
            }
        } catch (com.a.b.d.ys v0_4) {
            throw new IllegalArgumentException(v0_4);
        }
        if (!p3.e()) {
            v0_1 = v0_1.c(com.a.b.d.yl.a(p4.b()));
        }
        if ((!v0_1.f()) && (com.a.b.d.yl.b(p3.b.a(p4), p3.c.b(p4)) <= 0)) {
            com.a.b.d.ys v1_9 = 0;
        } else {
            v1_9 = 1;
        }
        com.a.b.d.ys v0_5;
        if (v1_9 == null) {
            v0_5 = new com.a.b.d.ys(v0_1, p4);
        } else {
            v0_5 = new com.a.b.d.et(p4);
        }
        return v0_5;
    }

    private com.a.b.d.du a(Comparable p4, Comparable p5)
    {
        com.a.b.d.du v0_2;
        com.a.b.b.cn.a(p4);
        com.a.b.b.cn.a(p5);
        if (this.comparator().compare(p4, p5) > 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.a.b.b.cn.a(v0_2);
        return this.a(p4, 1, p5, 0);
    }

    private com.a.b.d.du b(Comparable p3)
    {
        return this.a(((Comparable) com.a.b.b.cn.a(p3)), 0);
    }

    private com.a.b.d.du b(Comparable p2, boolean p3, Comparable p4, boolean p5)
    {
        com.a.b.d.du v0_2;
        com.a.b.b.cn.a(p2);
        com.a.b.b.cn.a(p4);
        if (this.comparator().compare(p2, p4) > 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.a.b.b.cn.a(v0_2);
        return this.a(p2, p3, p4, p5);
    }

    private com.a.b.d.du c(Comparable p3)
    {
        return this.b(((Comparable) com.a.b.b.cn.a(p3)), 1);
    }

    private com.a.b.d.du c(Comparable p2, boolean p3)
    {
        return this.a(((Comparable) com.a.b.b.cn.a(p2)), p3);
    }

    private com.a.b.d.du d(Comparable p2, boolean p3)
    {
        return this.b(((Comparable) com.a.b.b.cn.a(p2)), p3);
    }

    private static com.a.b.d.mf k()
    {
        throw new UnsupportedOperationException();
    }

    public abstract com.a.b.d.du a();

    abstract com.a.b.d.du a();

    abstract com.a.b.d.du a();

    public final synthetic com.a.b.d.me a(Object p2)
    {
        return this.c(((Comparable) p2));
    }

    public final bridge synthetic com.a.b.d.me a(Object p2, Object p3)
    {
        return this.a(((Comparable) p2), ((Comparable) p3));
    }

    synthetic com.a.b.d.me a(Object p2, boolean p3)
    {
        return this.b(((Comparable) p2), p3);
    }

    bridge synthetic com.a.b.d.me a(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.a(((Comparable) p2), p3, ((Comparable) p4), p5);
    }

    public abstract com.a.b.d.yl a();

    abstract com.a.b.d.du b();

    public final bridge synthetic com.a.b.d.me b(Object p2)
    {
        return this.b(((Comparable) p2));
    }

    synthetic com.a.b.d.me b(Object p2, boolean p3)
    {
        return this.a(((Comparable) p2), p3);
    }

    public final bridge synthetic com.a.b.d.me b(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.b(((Comparable) p2), p3, ((Comparable) p4), p5);
    }

    public final synthetic com.a.b.d.me c(Object p2, boolean p3)
    {
        return this.d(((Comparable) p2), p3);
    }

    public final synthetic com.a.b.d.me d(Object p2, boolean p3)
    {
        return this.c(((Comparable) p2), p3);
    }

    public abstract com.a.b.d.yl f_();

    public synthetic java.util.NavigableSet headSet(Object p2, boolean p3)
    {
        return this.c(((Comparable) p2), p3);
    }

    public synthetic java.util.SortedSet headSet(Object p2)
    {
        return this.b(((Comparable) p2));
    }

    public synthetic java.util.NavigableSet subSet(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.b(((Comparable) p2), p3, ((Comparable) p4), p5);
    }

    public synthetic java.util.SortedSet subSet(Object p2, Object p3)
    {
        return this.a(((Comparable) p2), ((Comparable) p3));
    }

    public synthetic java.util.NavigableSet tailSet(Object p2, boolean p3)
    {
        return this.d(((Comparable) p2), p3);
    }

    public synthetic java.util.SortedSet tailSet(Object p2)
    {
        return this.c(((Comparable) p2));
    }

    public String toString()
    {
        return this.f_().toString();
    }
}
