package com.a.b.d;
final class age extends com.a.b.d.agi implements com.a.b.d.yi {
    final synthetic com.a.b.d.aga a;
    private final java.util.Queue b;

    age(com.a.b.d.aga p2, Object p3)
    {
        this.a = p2;
        this.b = new java.util.ArrayDeque();
        this.b.add(p3);
        return;
    }

    public final Object a()
    {
        return this.b.element();
    }

    public final boolean hasNext()
    {
        int v0_2;
        if (this.b.isEmpty()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final Object next()
    {
        Object v0_1 = this.b.remove();
        com.a.b.d.mq.a(this.b, this.a.a(v0_1));
        return v0_1;
    }
}
