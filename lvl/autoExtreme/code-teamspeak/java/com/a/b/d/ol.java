package com.a.b.d;
final class ol extends com.a.b.d.aan {
    final synthetic com.a.b.d.oj a;

    ol(com.a.b.d.oj p1)
    {
        this.a = p1;
        return;
    }

    public final boolean contains(Object p2)
    {
        return this.a.f(p2);
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.op(this.a, 0);
    }

    public final boolean remove(Object p2)
    {
        int v0_3;
        if (this.a.b(p2).isEmpty()) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final int size()
    {
        return com.a.b.d.oj.d(this.a).size();
    }
}
