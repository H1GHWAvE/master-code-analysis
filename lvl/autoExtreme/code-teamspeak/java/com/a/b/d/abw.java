package com.a.b.d;
final class abw extends com.a.b.d.acq implements java.util.SortedMap {
    final synthetic com.a.b.d.abu a;

    private abw(com.a.b.d.abu p1)
    {
        this.a = p1;
        this(p1);
        return;
    }

    synthetic abw(com.a.b.d.abu p1, byte p2)
    {
        this(p1);
        return;
    }

    private java.util.SortedSet c()
    {
        return ((java.util.SortedSet) super.keySet());
    }

    private java.util.SortedSet d()
    {
        return new com.a.b.d.up(this);
    }

    public final java.util.Comparator comparator()
    {
        return com.a.b.d.abu.a(this.a).comparator();
    }

    final synthetic java.util.Set e()
    {
        return new com.a.b.d.up(this);
    }

    public final Object firstKey()
    {
        return com.a.b.d.abu.a(this.a).firstKey();
    }

    public final java.util.SortedMap headMap(Object p4)
    {
        com.a.b.b.cn.a(p4);
        return new com.a.b.d.abu(com.a.b.d.abu.a(this.a).headMap(p4), this.a.b).j();
    }

    public final bridge synthetic java.util.Set keySet()
    {
        return ((java.util.SortedSet) super.keySet());
    }

    public final Object lastKey()
    {
        return com.a.b.d.abu.a(this.a).lastKey();
    }

    public final java.util.SortedMap subMap(Object p4, Object p5)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.b.cn.a(p5);
        return new com.a.b.d.abu(com.a.b.d.abu.a(this.a).subMap(p4, p5), this.a.b).j();
    }

    public final java.util.SortedMap tailMap(Object p4)
    {
        com.a.b.b.cn.a(p4);
        return new com.a.b.d.abu(com.a.b.d.abu.a(this.a).tailMap(p4), this.a.b).j();
    }
}
