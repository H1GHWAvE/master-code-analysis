package com.a.b.d;
public abstract class ku extends com.a.b.d.iz implements com.a.b.d.xc {
    private static final com.a.b.d.ku a;
    private transient com.a.b.d.lo b;

    static ku()
    {
        com.a.b.d.ku.a = new com.a.b.d.zj(com.a.b.d.jt.k(), 0);
        return;
    }

    ku()
    {
        return;
    }

    private static com.a.b.d.ku a(com.a.b.d.xc p1)
    {
        return com.a.b.d.ku.a(p1.a());
    }

    public static com.a.b.d.ku a(Iterable p2)
    {
        com.a.b.d.ku v0_2;
        if (!(p2 instanceof com.a.b.d.ku)) {
            com.a.b.d.ku v0_5;
            if (!(p2 instanceof com.a.b.d.xc)) {
                v0_5 = com.a.b.d.oi.a(com.a.b.d.xe.a(p2));
                com.a.b.d.mq.a(v0_5, p2);
            } else {
                v0_5 = com.a.b.d.xe.b(p2);
            }
            v0_2 = com.a.b.d.ku.a(v0_5);
        } else {
            v0_2 = ((com.a.b.d.ku) p2);
            if (((com.a.b.d.ku) p2).h_()) {
            }
        }
        return v0_2;
    }

    private static com.a.b.d.ku a(Object p2, Object p3)
    {
        com.a.b.d.ku v0_1 = new Object[2];
        v0_1[0] = p2;
        v0_1[1] = p3;
        return com.a.b.d.ku.a(java.util.Arrays.asList(v0_1));
    }

    private static com.a.b.d.ku a(Object p2, Object p3, Object p4)
    {
        com.a.b.d.ku v0_1 = new Object[3];
        v0_1[0] = p2;
        v0_1[1] = p3;
        v0_1[2] = p4;
        return com.a.b.d.ku.a(java.util.Arrays.asList(v0_1));
    }

    private static com.a.b.d.ku a(Object p2, Object p3, Object p4, Object p5)
    {
        com.a.b.d.ku v0_1 = new Object[4];
        v0_1[0] = p2;
        v0_1[1] = p3;
        v0_1[2] = p4;
        v0_1[3] = p5;
        return com.a.b.d.ku.a(java.util.Arrays.asList(v0_1));
    }

    private static com.a.b.d.ku a(Object p2, Object p3, Object p4, Object p5, Object p6)
    {
        com.a.b.d.ku v0_1 = new Object[5];
        v0_1[0] = p2;
        v0_1[1] = p3;
        v0_1[2] = p4;
        v0_1[3] = p5;
        v0_1[4] = p6;
        return com.a.b.d.ku.a(java.util.Arrays.asList(v0_1));
    }

    private static varargs com.a.b.d.ku a(Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object[] p7)
    {
        return new com.a.b.d.kw().a(p1).a(p2).a(p3).a(p4).a(p5).a(p6).b(p7).b();
    }

    static com.a.b.d.ku a(java.util.Collection p9)
    {
        com.a.b.d.ju v6 = com.a.b.d.jt.l();
        java.util.Iterator v7 = p9.iterator();
        com.a.b.d.zj v2_0 = 0;
        while (v7.hasNext()) {
            com.a.b.d.zj v0_6;
            com.a.b.d.zj v0_5 = ((com.a.b.d.xd) v7.next());
            com.a.b.d.jt v1_1 = v0_5.b();
            if (v1_1 <= null) {
                v0_6 = v2_0;
            } else {
                v6.a(v0_5.a(), Integer.valueOf(v1_1));
                v0_6 = (((long) v1_1) + v2_0);
            }
            v2_0 = v0_6;
        }
        com.a.b.d.zj v0_3;
        if (v2_0 != 0) {
            v0_3 = new com.a.b.d.zj(v6.a(), com.a.b.l.q.b(v2_0));
        } else {
            v0_3 = com.a.b.d.ku.a;
        }
        return v0_3;
    }

    private static com.a.b.d.ku a(java.util.Iterator p1)
    {
        com.a.b.d.ku v0_1 = new com.a.b.d.oi();
        com.a.b.d.nj.a(v0_1, p1);
        return com.a.b.d.ku.a(v0_1);
    }

    private static com.a.b.d.ku a(Object[] p1)
    {
        return com.a.b.d.ku.a(java.util.Arrays.asList(p1));
    }

    private static com.a.b.d.ku b()
    {
        return com.a.b.d.ku.a;
    }

    private static com.a.b.d.ku b(Object p2)
    {
        com.a.b.d.ku v0_1 = new Object[1];
        v0_1[0] = p2;
        return com.a.b.d.ku.a(java.util.Arrays.asList(v0_1));
    }

    private static varargs com.a.b.d.ku b(Object[] p1)
    {
        return com.a.b.d.ku.a(java.util.Arrays.asList(p1));
    }

    private final com.a.b.d.lo e()
    {
        com.a.b.d.kx v0_2;
        if (!this.isEmpty()) {
            v0_2 = new com.a.b.d.kx(this, 0);
        } else {
            v0_2 = com.a.b.d.lo.h();
        }
        return v0_2;
    }

    private static com.a.b.d.kw p()
    {
        return new com.a.b.d.kw();
    }

    public final int a(Object p2, int p3)
    {
        throw new UnsupportedOperationException();
    }

    int a(Object[] p5, int p6)
    {
        java.util.Iterator v1 = this.o().iterator();
        while (v1.hasNext()) {
            int v0_3 = ((com.a.b.d.xd) v1.next());
            java.util.Arrays.fill(p5, p6, (v0_3.b() + p6), v0_3.a());
            p6 += v0_3.b();
        }
        return p6;
    }

    abstract com.a.b.d.xd a();

    public final synthetic java.util.Set a()
    {
        return this.o();
    }

    public final boolean a(Object p2, int p3, int p4)
    {
        throw new UnsupportedOperationException();
    }

    public final int b(Object p2, int p3)
    {
        throw new UnsupportedOperationException();
    }

    public final int c(Object p2, int p3)
    {
        throw new UnsupportedOperationException();
    }

    public com.a.b.d.agi c()
    {
        return new com.a.b.d.kv(this, this.o().c());
    }

    public boolean contains(Object p2)
    {
        int v0_1;
        if (this.a(p2) <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean containsAll(java.util.Collection p2)
    {
        return this.n_().containsAll(p2);
    }

    public boolean equals(Object p2)
    {
        return com.a.b.d.xe.a(this, p2);
    }

    Object g()
    {
        return new com.a.b.d.la(this);
    }

    public int hashCode()
    {
        return com.a.b.d.aad.a(this.o());
    }

    public synthetic java.util.Iterator iterator()
    {
        return this.c();
    }

    public final com.a.b.d.lo o()
    {
        com.a.b.d.kx v0_0 = this.b;
        if (v0_0 == null) {
            if (!this.isEmpty()) {
                v0_0 = new com.a.b.d.kx(this, 0);
            } else {
                v0_0 = com.a.b.d.lo.h();
            }
            this.b = v0_0;
        }
        return v0_0;
    }

    public String toString()
    {
        return this.o().toString();
    }
}
