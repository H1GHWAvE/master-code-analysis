package com.a.b.d;
public abstract class gi extends com.a.b.d.gs implements java.util.concurrent.ConcurrentMap {

    protected gi()
    {
        return;
    }

    protected synthetic java.util.Map a()
    {
        return this.b();
    }

    protected abstract java.util.concurrent.ConcurrentMap b();

    protected synthetic Object k_()
    {
        return this.b();
    }

    public Object putIfAbsent(Object p2, Object p3)
    {
        return this.b().putIfAbsent(p2, p3);
    }

    public boolean remove(Object p2, Object p3)
    {
        return this.b().remove(p2, p3);
    }

    public Object replace(Object p2, Object p3)
    {
        return this.b().replace(p2, p3);
    }

    public boolean replace(Object p2, Object p3, Object p4)
    {
        return this.b().replace(p2, p3, p4);
    }
}
