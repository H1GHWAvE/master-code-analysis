package com.a.b.d;
 class ado extends com.a.b.d.add implements java.util.Queue {
    private static final long a;

    ado(java.util.Queue p3)
    {
        this(p3, 0, 0);
        return;
    }

    java.util.Queue a()
    {
        return ((java.util.Queue) super.b());
    }

    synthetic java.util.Collection b()
    {
        return this.a();
    }

    synthetic Object d()
    {
        return this.a();
    }

    public Object element()
    {
        try {
            return this.a().element();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public boolean offer(Object p3)
    {
        try {
            return this.a().offer(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public Object peek()
    {
        try {
            return this.a().peek();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public Object poll()
    {
        try {
            return this.a().poll();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public Object remove()
    {
        try {
            return this.a().remove();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }
}
