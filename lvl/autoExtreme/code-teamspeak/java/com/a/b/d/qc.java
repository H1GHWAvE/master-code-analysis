package com.a.b.d;
 class qc extends com.a.b.d.gx implements java.io.Serializable {
    final com.a.b.d.pn a;
    final com.a.b.d.vi b;
    transient java.util.Collection c;
    transient java.util.Map d;

    public qc(com.a.b.d.vi p2, com.a.b.d.pn p3)
    {
        this.b = ((com.a.b.d.vi) com.a.b.b.cn.a(p2));
        this.a = ((com.a.b.d.pn) com.a.b.b.cn.a(p3));
        return;
    }

    public final boolean a(com.a.b.d.vi p5)
    {
        java.util.Iterator v2 = p5.k().iterator();
        int v1_1 = 0;
        while (v2.hasNext()) {
            int v0_3 = ((java.util.Map$Entry) v2.next());
            v1_1 = (this.a(v0_3.getKey(), v0_3.getValue()) | v1_1);
        }
        return v1_1;
    }

    public final boolean a(Object p2, Object p3)
    {
        this.a.a(p2, p3);
        return this.b.a(p2, p3);
    }

    public java.util.Collection b(Object p3, Iterable p4)
    {
        return this.b.b(p3, com.a.b.d.po.a(p3, p4, this.a));
    }

    public final java.util.Map b()
    {
        com.a.b.d.qd v0_0 = this.d;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.qd(this, this.b.b());
            this.d = v0_0;
        }
        return v0_0;
    }

    protected final com.a.b.d.vi c()
    {
        return this.b;
    }

    public java.util.Collection c(Object p3)
    {
        return com.a.b.d.dn.a(this.b.c(p3), new com.a.b.d.qe(this, p3));
    }

    public final boolean c(Object p3, Iterable p4)
    {
        return this.b.c(p3, com.a.b.d.po.a(p3, p4, this.a));
    }

    public java.util.Collection k()
    {
        com.a.b.d.px v0_0 = this.c;
        if (v0_0 == null) {
            com.a.b.d.px v0_2 = this.b.k();
            com.a.b.d.pn v2 = this.a;
            if (!(v0_2 instanceof java.util.Set)) {
                v0_0 = new com.a.b.d.px(v0_2, v2);
            } else {
                v0_0 = com.a.b.d.po.a(((java.util.Set) v0_2), v2);
            }
            this.c = v0_0;
        }
        return v0_0;
    }

    protected final bridge synthetic Object k_()
    {
        return this.b;
    }
}
