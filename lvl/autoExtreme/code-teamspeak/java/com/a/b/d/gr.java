package com.a.b.d;
public abstract class gr extends com.a.b.d.gx implements com.a.b.d.ou {

    protected gr()
    {
        return;
    }

    protected abstract com.a.b.d.ou a();

    public final java.util.List a(Object p2)
    {
        return this.a().a(p2);
    }

    public final java.util.List a(Object p2, Iterable p3)
    {
        return this.a().a(p2, p3);
    }

    public final synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        return this.a(p2, p3);
    }

    public final java.util.List b(Object p2)
    {
        return this.a().b(p2);
    }

    protected final synthetic com.a.b.d.vi c()
    {
        return this.a();
    }

    public final synthetic java.util.Collection c(Object p2)
    {
        return this.a(p2);
    }

    public final synthetic java.util.Collection d(Object p2)
    {
        return this.b(p2);
    }

    protected final synthetic Object k_()
    {
        return this.a();
    }
}
