package com.a.b.d;
abstract class be extends com.a.b.d.ba implements com.a.b.d.abs {
    private static final long a = 430848587173315748;

    protected be(java.util.Map p1)
    {
        this(p1);
        return;
    }

    private java.util.SortedSet v()
    {
        com.a.b.d.me v0_2;
        if (this.d_() != null) {
            v0_2 = com.a.b.d.me.a(this.d_());
        } else {
            v0_2 = java.util.Collections.unmodifiableSortedSet(this.y());
        }
        return v0_2;
    }

    synthetic java.util.Set a()
    {
        return this.y();
    }

    public synthetic java.util.Set a(Object p2)
    {
        return this.h(p2);
    }

    public final synthetic java.util.Set a(Object p2, Iterable p3)
    {
        return this.d(p2, p3);
    }

    public final synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        return this.d(p2, p3);
    }

    public java.util.Map b()
    {
        return super.b();
    }

    public final synthetic java.util.Set b(Object p2)
    {
        return this.i(p2);
    }

    synthetic java.util.Collection c()
    {
        return this.y();
    }

    public synthetic java.util.Collection c(Object p2)
    {
        return this.h(p2);
    }

    final synthetic java.util.Collection d()
    {
        return this.v();
    }

    public final synthetic java.util.Collection d(Object p2)
    {
        return this.i(p2);
    }

    public java.util.SortedSet d(Object p2, Iterable p3)
    {
        return ((java.util.SortedSet) super.a(p2, p3));
    }

    public java.util.SortedSet h(Object p2)
    {
        return ((java.util.SortedSet) super.a(p2));
    }

    public java.util.Collection i()
    {
        return super.i();
    }

    public java.util.SortedSet i(Object p2)
    {
        return ((java.util.SortedSet) super.b(p2));
    }

    final synthetic java.util.Set t()
    {
        return this.v();
    }

    abstract java.util.SortedSet y();
}
