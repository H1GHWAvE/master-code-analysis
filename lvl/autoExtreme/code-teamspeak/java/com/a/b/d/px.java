package com.a.b.d;
 class px extends com.a.b.d.gh {
    final com.a.b.d.pn a;
    final java.util.Collection b;

    px(java.util.Collection p1, com.a.b.d.pn p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    protected final java.util.Collection b()
    {
        return this.b;
    }

    public boolean contains(Object p2)
    {
        return com.a.b.d.sz.a(this.b, p2);
    }

    public boolean containsAll(java.util.Collection p2)
    {
        return com.a.b.d.cm.a(this, p2);
    }

    public java.util.Iterator iterator()
    {
        return new com.a.b.d.py(this, this.b.iterator());
    }

    protected final bridge synthetic Object k_()
    {
        return this.b;
    }

    public boolean remove(Object p2)
    {
        return com.a.b.d.sz.b(this.b, p2);
    }

    public boolean removeAll(java.util.Collection p2)
    {
        return this.b(p2);
    }

    public boolean retainAll(java.util.Collection p2)
    {
        return this.c(p2);
    }

    public Object[] toArray()
    {
        return this.p();
    }

    public Object[] toArray(Object[] p2)
    {
        return com.a.b.d.yc.a(this, p2);
    }
}
