package com.a.b.d;
abstract class zr extends com.a.b.d.mi {

    zr()
    {
        return;
    }

    private static com.a.b.d.zr a(Iterable p1)
    {
        return com.a.b.d.zr.a(p1, 0, 0);
    }

    static final com.a.b.d.zr a(Iterable p10, java.util.Comparator p11, java.util.Comparator p12)
    {
        com.a.b.d.lo v1_0 = com.a.b.d.lo.i();
        com.a.b.d.abt v2_0 = com.a.b.d.lo.i();
        com.a.b.d.jl v3 = com.a.b.d.jl.a(p10);
        long v4_0 = v3.iterator();
        while (v4_0.hasNext()) {
            com.a.b.d.abt v0_5 = ((com.a.b.d.adw) v4_0.next());
            v1_0.c(v0_5.a());
            v2_0.c(v0_5.b());
        }
        com.a.b.d.abt v0_1 = v1_0.b();
        if (p11 != null) {
            com.a.b.d.abt v0_2 = com.a.b.d.ov.a(v0_1);
            java.util.Collections.sort(v0_2, p11);
            v0_1 = com.a.b.d.lo.a(v0_2);
        }
        com.a.b.d.lo v1_1 = v2_0.b();
        if (p12 != null) {
            com.a.b.d.lo v1_2 = com.a.b.d.ov.a(v1_1);
            java.util.Collections.sort(v1_2, p12);
            v1_1 = com.a.b.d.lo.a(v1_2);
        }
        com.a.b.d.abt v0_3;
        if (((long) v3.size()) <= ((((long) v0_1.size()) * ((long) v1_1.size())) / 2)) {
            v0_3 = new com.a.b.d.abt(v3, v0_1, v1_1);
        } else {
            v0_3 = new com.a.b.d.ec(v3, v0_1, v1_1);
        }
        return v0_3;
    }

    static com.a.b.d.zr a(java.util.List p1, java.util.Comparator p2, java.util.Comparator p3)
    {
        com.a.b.b.cn.a(p1);
        if ((p2 != null) || (p3 != null)) {
            java.util.Collections.sort(p1, new com.a.b.d.zs(p2, p3));
        }
        return com.a.b.d.zr.a(p1, p2, p3);
    }

    abstract com.a.b.d.adw a();

    abstract Object b();

    final synthetic java.util.Set f()
    {
        return this.q();
    }

    final synthetic java.util.Collection i()
    {
        return this.r();
    }

    final com.a.b.d.lo q()
    {
        com.a.b.d.zt v0_2;
        if (!this.c()) {
            v0_2 = new com.a.b.d.zt(this, 0);
        } else {
            v0_2 = com.a.b.d.lo.h();
        }
        return v0_2;
    }

    final com.a.b.d.iz r()
    {
        com.a.b.d.zv v0_2;
        if (!this.c()) {
            v0_2 = new com.a.b.d.zv(this, 0);
        } else {
            v0_2 = com.a.b.d.jl.d();
        }
        return v0_2;
    }
}
