package com.a.b.d;
final class fv extends com.a.b.d.gp {
    final Object a;

    fv(Object p1)
    {
        this.a = p1;
        return;
    }

    protected final java.util.List a()
    {
        return java.util.Collections.emptyList();
    }

    public final void add(int p5, Object p6)
    {
        com.a.b.b.cn.b(p5, 0);
        String v1_2 = String.valueOf(String.valueOf(this.a));
        throw new IllegalArgumentException(new StringBuilder((v1_2.length() + 32)).append("Key does not satisfy predicate: ").append(v1_2).toString());
    }

    public final boolean add(Object p2)
    {
        this.add(0, p2);
        return 1;
    }

    public final boolean addAll(int p5, java.util.Collection p6)
    {
        com.a.b.b.cn.a(p6);
        com.a.b.b.cn.b(p5, 0);
        String v1_2 = String.valueOf(String.valueOf(this.a));
        throw new IllegalArgumentException(new StringBuilder((v1_2.length() + 32)).append("Key does not satisfy predicate: ").append(v1_2).toString());
    }

    public final boolean addAll(java.util.Collection p2)
    {
        this.addAll(0, p2);
        return 1;
    }

    protected final synthetic java.util.Collection b()
    {
        return java.util.Collections.emptyList();
    }

    protected final synthetic Object k_()
    {
        return java.util.Collections.emptyList();
    }
}
