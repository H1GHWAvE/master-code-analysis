package com.a.b.d;
final class hc implements java.util.Iterator {
    final synthetic com.a.b.d.hb a;
    private java.util.Map$Entry b;
    private java.util.Map$Entry c;

    hc(com.a.b.d.hb p2)
    {
        this.a = p2;
        this.b = 0;
        this.c = this.a.a.lastEntry();
        return;
    }

    private java.util.Map$Entry a()
    {
        if (this.hasNext()) {
            try {
                Throwable v0_1 = this.c;
                this.b = this.c;
                this.c = this.a.a.lowerEntry(this.c.getKey());
                return v0_1;
            } catch (Throwable v0_2) {
                this.b = this.c;
                this.c = this.a.a.lowerEntry(this.c.getKey());
                throw v0_2;
            }
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final boolean hasNext()
    {
        int v0_1;
        if (this.c == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final synthetic Object next()
    {
        return this.a();
    }

    public final void remove()
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        this.a.a.remove(this.b.getKey());
        this.b = 0;
        return;
    }
}
