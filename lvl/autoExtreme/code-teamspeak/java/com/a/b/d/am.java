package com.a.b.d;
abstract class am implements java.util.Map$Entry {

    am()
    {
        return;
    }

    public boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof java.util.Map$Entry)) && ((com.a.b.b.ce.a(this.getKey(), ((java.util.Map$Entry) p4).getKey())) && (com.a.b.b.ce.a(this.getValue(), ((java.util.Map$Entry) p4).getValue())))) {
            v0 = 1;
        }
        return v0;
    }

    public abstract Object getKey();

    public abstract Object getValue();

    public int hashCode()
    {
        int v1_1;
        int v0_0 = 0;
        int v1_0 = this.getKey();
        Object v2 = this.getValue();
        if (v1_0 != 0) {
            v1_1 = v1_0.hashCode();
        } else {
            v1_1 = 0;
        }
        if (v2 != null) {
            v0_0 = v2.hashCode();
        }
        return (v0_0 ^ v1_1);
    }

    public Object setValue(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.getKey()));
        String v1_2 = String.valueOf(String.valueOf(this.getValue()));
        return new StringBuilder(((v0_2.length() + 1) + v1_2.length())).append(v0_2).append("=").append(v1_2).toString();
    }
}
