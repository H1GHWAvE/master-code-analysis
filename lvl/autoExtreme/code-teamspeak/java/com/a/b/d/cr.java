package com.a.b.d;
final class cr extends java.util.AbstractCollection {
    final com.a.b.d.jl a;

    cr(com.a.b.d.jl p1)
    {
        this.a = p1;
        return;
    }

    public final boolean contains(Object p2)
    {
        int v0_1;
        if (!(p2 instanceof java.util.List)) {
            v0_1 = 0;
        } else {
            v0_1 = com.a.b.d.cm.a(this.a, ((java.util.List) p2));
        }
        return v0_1;
    }

    public final boolean isEmpty()
    {
        return 0;
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.cs(this.a);
    }

    public final int size()
    {
        return com.a.b.j.g.a(this.a.size());
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 14)).append("permutations(").append(v0_2).append(")").toString();
    }
}
