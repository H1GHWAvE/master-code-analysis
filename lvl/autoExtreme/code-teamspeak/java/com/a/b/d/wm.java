package com.a.b.d;
abstract class wm extends java.util.AbstractCollection {

    wm()
    {
        return;
    }

    abstract com.a.b.d.vi a();

    public void clear()
    {
        this.a().g();
        return;
    }

    public boolean contains(Object p4)
    {
        int v0_1;
        if (!(p4 instanceof java.util.Map$Entry)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a().b(((java.util.Map$Entry) p4).getKey(), ((java.util.Map$Entry) p4).getValue());
        }
        return v0_1;
    }

    public boolean remove(Object p4)
    {
        int v0_1;
        if (!(p4 instanceof java.util.Map$Entry)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a().c(((java.util.Map$Entry) p4).getKey(), ((java.util.Map$Entry) p4).getValue());
        }
        return v0_1;
    }

    public int size()
    {
        return this.a().f();
    }
}
