package com.a.b.d;
final class acb extends com.a.b.d.aan {
    final synthetic com.a.b.d.aca a;

    private acb(com.a.b.d.aca p1)
    {
        this.a = p1;
        return;
    }

    synthetic acb(com.a.b.d.aca p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void clear()
    {
        this.a.a(com.a.b.b.cp.a());
        return;
    }

    public final boolean contains(Object p5)
    {
        int v0_1;
        if (!(p5 instanceof java.util.Map$Entry)) {
            v0_1 = 0;
        } else {
            v0_1 = com.a.b.d.abx.a(this.a.b, ((java.util.Map$Entry) p5).getKey(), this.a.a, ((java.util.Map$Entry) p5).getValue());
        }
        return v0_1;
    }

    public final boolean isEmpty()
    {
        int v0_3;
        if (this.a.b.b(this.a.a)) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.acc(this.a, 0);
    }

    public final boolean remove(Object p5)
    {
        int v0_1;
        if (!(p5 instanceof java.util.Map$Entry)) {
            v0_1 = 0;
        } else {
            v0_1 = com.a.b.d.abx.b(this.a.b, ((java.util.Map$Entry) p5).getKey(), this.a.a, ((java.util.Map$Entry) p5).getValue());
        }
        return v0_1;
    }

    public final boolean retainAll(java.util.Collection p3)
    {
        return this.a.a(com.a.b.b.cp.a(com.a.b.b.cp.a(p3)));
    }

    public final int size()
    {
        java.util.Iterator v2 = this.a.b.a.values().iterator();
        int v1_4 = 0;
        while (v2.hasNext()) {
            int v0_5;
            if (!((java.util.Map) v2.next()).containsKey(this.a.a)) {
                v0_5 = v1_4;
            } else {
                v0_5 = (v1_4 + 1);
            }
            v1_4 = v0_5;
        }
        return v1_4;
    }
}
