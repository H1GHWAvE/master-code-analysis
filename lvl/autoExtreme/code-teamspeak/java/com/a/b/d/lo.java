package com.a.b.d;
public abstract class lo extends com.a.b.d.iz implements java.util.Set {
    private static final double a = 4604480259023595110;
    static final int b = 1073741824;
    private static final int c = 751619276;

    lo()
    {
        return;
    }

    private static int a(int p6)
    {
        double v0_2;
        if (p6 >= 751619276) {
            double v0_1;
            if (p6 >= 1073741824) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
            }
            com.a.b.b.cn.a(v0_1, "collection too large");
            v0_2 = 1073741824;
        } else {
            v0_2 = (Integer.highestOneBit((p6 - 1)) << 1);
            while ((((double) v0_2) * 0.7) < ((double) p6)) {
                v0_2 <<= 1;
            }
        }
        return v0_2;
    }

    static synthetic com.a.b.d.lo a(int p1, Object[] p2)
    {
        return com.a.b.d.lo.b(p1, p2);
    }

    public static com.a.b.d.lo a(Iterable p3)
    {
        com.a.b.d.lo v0_3;
        if (!(p3 instanceof java.util.Collection)) {
            com.a.b.d.lo v0_1 = p3.iterator();
            if (v0_1.hasNext()) {
                com.a.b.d.lp v1_1 = v0_1.next();
                if (v0_1.hasNext()) {
                    v0_3 = new com.a.b.d.lp().c(v1_1).b(v0_1).b();
                } else {
                    v0_3 = com.a.b.d.lo.d(v1_1);
                }
            } else {
                v0_3 = com.a.b.d.ey.a;
            }
        } else {
            v0_3 = com.a.b.d.lo.a(((java.util.Collection) p3));
        }
        return v0_3;
    }

    public static com.a.b.d.lo a(Object p3, Object p4, Object p5)
    {
        com.a.b.d.lo v0_0 = new Object[3];
        v0_0[0] = p3;
        v0_0[1] = p4;
        v0_0[2] = p5;
        return com.a.b.d.lo.b(3, v0_0);
    }

    private static com.a.b.d.lo a(Object p3, Object p4, Object p5, Object p6)
    {
        com.a.b.d.lo v0_0 = new Object[4];
        v0_0[0] = p3;
        v0_0[1] = p4;
        v0_0[2] = p5;
        v0_0[3] = p6;
        return com.a.b.d.lo.b(4, v0_0);
    }

    private static com.a.b.d.lo a(Object p3, Object p4, Object p5, Object p6, Object p7)
    {
        com.a.b.d.lo v0_0 = new Object[5];
        v0_0[0] = p3;
        v0_0[1] = p4;
        v0_0[2] = p5;
        v0_0[3] = p6;
        v0_0[4] = p7;
        return com.a.b.d.lo.b(5, v0_0);
    }

    private static varargs com.a.b.d.lo a(Object p4, Object p5, Object p6, Object p7, Object p8, Object p9, Object[] p10)
    {
        com.a.b.d.lo v0_2 = new Object[(p10.length + 6)];
        v0_2[0] = p4;
        v0_2[1] = p5;
        v0_2[2] = p6;
        v0_2[3] = p7;
        v0_2[4] = p8;
        v0_2[5] = p9;
        System.arraycopy(p10, 0, v0_2, 6, p10.length);
        return com.a.b.d.lo.b(v0_2.length, v0_2);
    }

    public static com.a.b.d.lo a(java.util.Collection p2)
    {
        if ((!(p2 instanceof com.a.b.d.lo)) || ((p2 instanceof com.a.b.d.me))) {
            if (!(p2 instanceof java.util.EnumSet)) {
                com.a.b.d.lo v0_6 = p2.toArray();
                com.a.b.d.lo v0_4 = com.a.b.d.lo.b(v0_6.length, v0_6);
            } else {
                v0_4 = com.a.b.d.ji.a(java.util.EnumSet.copyOf(((java.util.EnumSet) p2)));
            }
        } else {
            v0_4 = ((com.a.b.d.lo) p2);
            if (((com.a.b.d.lo) p2).h_()) {
            }
        }
        return v0_4;
    }

    private static com.a.b.d.lo a(java.util.EnumSet p1)
    {
        return com.a.b.d.ji.a(java.util.EnumSet.copyOf(p1));
    }

    private static com.a.b.d.lo a(java.util.Iterator p2)
    {
        com.a.b.d.lo v0_4;
        if (p2.hasNext()) {
            com.a.b.d.lo v0_1 = p2.next();
            if (p2.hasNext()) {
                v0_4 = new com.a.b.d.lp().c(v0_1).b(p2).b();
            } else {
                v0_4 = com.a.b.d.lo.d(v0_1);
            }
        } else {
            v0_4 = com.a.b.d.ey.a;
        }
        return v0_4;
    }

    public static com.a.b.d.lo a(Object[] p2)
    {
        com.a.b.d.lo v0_3;
        switch (p2.length) {
            case 0:
                v0_3 = com.a.b.d.ey.a;
                break;
            case 1:
                v0_3 = com.a.b.d.lo.d(p2[0]);
                break;
            default:
                v0_3 = com.a.b.d.lo.b(p2.length, ((Object[]) p2.clone()));
        }
        return v0_3;
    }

    private static varargs com.a.b.d.lo b(int p12, Object[] p13)
    {
        while(true) {
            com.a.b.d.zk v0_1;
            switch (p12) {
                case 0:
                    v0_1 = com.a.b.d.ey.a;
                    break;
                case 1:
                    v0_1 = com.a.b.d.lo.d(p13[0]);
                    break;
                default:
                    int v5 = com.a.b.d.lo.a(p12);
                    Object[] v6 = new Object[v5];
                    int v7 = (v5 - 1);
                    int v3 = 0;
                    int v1_0 = 0;
                    int v2_0 = 0;
                    while (v3 < p12) {
                        Object v8 = com.a.b.d.yc.a(p13[v3], v3);
                        int v9 = v8.hashCode();
                        com.a.b.d.zk v0_9 = com.a.b.d.iq.a(v9);
                        while(true) {
                            int v1_2;
                            com.a.b.d.zk v0_10;
                            boolean v10_0 = (v0_9 & v7);
                            Object v11 = v6[v10_0];
                            if (v11 != null) {
                                if (v11.equals(v8)) {
                                    break;
                                }
                                v0_9++;
                            } else {
                                v0_10 = (v1_0 + 1);
                                p13[v1_0] = v8;
                                v6[v10_0] = v8;
                                v1_2 = (v2_0 + v9);
                            }
                            v3++;
                            v2_0 = v1_2;
                            v1_0 = v0_10;
                        }
                        v0_10 = v1_0;
                        v1_2 = v2_0;
                    }
                    java.util.Arrays.fill(p13, v1_0, p12, 0);
                    if (v1_0 != 1) {
                        if (v5 == com.a.b.d.lo.a(v1_0)) {
                            break;
                        }
                        p12 = v1_0;
                    } else {
                        v0_1 = new com.a.b.d.aaw(p13[0], v2_0);
                    }
            }
            return v0_1;
        }
        if (v1_0 < p13.length) {
            p13 = com.a.b.d.yc.b(p13, v1_0);
        }
        v0_1 = new com.a.b.d.zk(p13, v2_0, v6, v7);
        return v0_1;
    }

    public static com.a.b.d.lo b(Object p3, Object p4)
    {
        com.a.b.d.lo v0_0 = new Object[2];
        v0_0[0] = p3;
        v0_0[1] = p4;
        return com.a.b.d.lo.b(2, v0_0);
    }

    public static com.a.b.d.lo d(Object p1)
    {
        return new com.a.b.d.aaw(p1);
    }

    public static com.a.b.d.lo h()
    {
        return com.a.b.d.ey.a;
    }

    public static com.a.b.d.lp i()
    {
        return new com.a.b.d.lp();
    }

    public abstract com.a.b.d.agi c();

    public boolean equals(Object p3)
    {
        int v0_6;
        if (p3 != this) {
            if ((!(p3 instanceof com.a.b.d.lo)) || ((!this.g_()) || ((!((com.a.b.d.lo) p3).g_()) || (this.hashCode() == p3.hashCode())))) {
                v0_6 = com.a.b.d.aad.a(this, p3);
            } else {
                v0_6 = 0;
            }
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    Object g()
    {
        return new com.a.b.d.lq(this.toArray());
    }

    boolean g_()
    {
        return 0;
    }

    public int hashCode()
    {
        return com.a.b.d.aad.a(this);
    }

    public synthetic java.util.Iterator iterator()
    {
        return this.c();
    }
}
