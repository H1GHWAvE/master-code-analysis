package com.a.b.d;
final class dv implements java.io.Serializable {
    int a;

    dv(int p1)
    {
        this.a = p1;
        return;
    }

    private int a()
    {
        return this.a;
    }

    private void b()
    {
        this.a = 0;
        return;
    }

    private int c(int p3)
    {
        int v0 = this.a;
        this.a = (v0 + p3);
        return v0;
    }

    public final int a(int p2)
    {
        int v0_1 = (this.a + p2);
        this.a = v0_1;
        return v0_1;
    }

    public final int b(int p2)
    {
        int v0 = this.a;
        this.a = p2;
        return v0;
    }

    public final boolean equals(Object p3)
    {
        if ((!(p3 instanceof com.a.b.d.dv)) || (((com.a.b.d.dv) p3).a != this.a)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final int hashCode()
    {
        return this.a;
    }

    public final String toString()
    {
        return Integer.toString(this.a);
    }
}
