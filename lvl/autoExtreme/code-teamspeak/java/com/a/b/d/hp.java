package com.a.b.d;
public abstract class hp extends com.a.b.d.hi implements java.util.SortedSet {

    protected hp()
    {
        return;
    }

    private int b(Object p2, Object p3)
    {
        int v0_1;
        int v0_0 = this.comparator();
        if (v0_0 != 0) {
            v0_1 = v0_0.compare(p2, p3);
        } else {
            v0_1 = ((Comparable) p2).compareTo(p3);
        }
        return v0_1;
    }

    protected synthetic java.util.Set a()
    {
        return this.c();
    }

    protected java.util.SortedSet a(Object p2, Object p3)
    {
        return this.tailSet(p2).headSet(p3);
    }

    protected synthetic java.util.Collection b()
    {
        return this.c();
    }

    protected final boolean b(Object p3)
    {
        int v0 = 0;
        try {
            if (this.b(this.tailSet(p3).first(), p3) == 0) {
                v0 = 1;
            }
        } catch (NullPointerException v1) {
        } catch (NullPointerException v1) {
        } catch (NullPointerException v1) {
        }
        return v0;
    }

    protected abstract java.util.SortedSet c();

    protected final boolean c(Object p4)
    {
        int v0 = 0;
        try {
            NullPointerException v1_1 = this.tailSet(p4).iterator();
        } catch (NullPointerException v1) {
            return v0;
        } catch (NullPointerException v1) {
            return v0;
        }
        if ((!v1_1.hasNext()) || (this.b(v1_1.next(), p4) != 0)) {
            return v0;
        } else {
            v1_1.remove();
            v0 = 1;
            return v0;
        }
    }

    public java.util.Comparator comparator()
    {
        return this.c().comparator();
    }

    public Object first()
    {
        return this.c().first();
    }

    public java.util.SortedSet headSet(Object p2)
    {
        return this.c().headSet(p2);
    }

    protected synthetic Object k_()
    {
        return this.c();
    }

    public Object last()
    {
        return this.c().last();
    }

    public java.util.SortedSet subSet(Object p2, Object p3)
    {
        return this.c().subSet(p2, p3);
    }

    public java.util.SortedSet tailSet(Object p2)
    {
        return this.c().tailSet(p2);
    }
}
