package com.a.b.d;
abstract enum class qq extends java.lang.Enum {
    public static final enum com.a.b.d.qq a;
    public static final enum com.a.b.d.qq b;
    public static final enum com.a.b.d.qq c;
    public static final enum com.a.b.d.qq d;
    public static final enum com.a.b.d.qq e;
    private static final synthetic com.a.b.d.qq[] f;

    static qq()
    {
        com.a.b.d.qq.a = new com.a.b.d.qr("EXPLICIT");
        com.a.b.d.qq.b = new com.a.b.d.qs("REPLACED");
        com.a.b.d.qq.c = new com.a.b.d.qt("COLLECTED");
        com.a.b.d.qq.d = new com.a.b.d.qu("EXPIRED");
        com.a.b.d.qq.e = new com.a.b.d.qv("SIZE");
        com.a.b.d.qq[] v0_11 = new com.a.b.d.qq[5];
        v0_11[0] = com.a.b.d.qq.a;
        v0_11[1] = com.a.b.d.qq.b;
        v0_11[2] = com.a.b.d.qq.c;
        v0_11[3] = com.a.b.d.qq.d;
        v0_11[4] = com.a.b.d.qq.e;
        com.a.b.d.qq.f = v0_11;
        return;
    }

    private qq(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic qq(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.d.qq valueOf(String p1)
    {
        return ((com.a.b.d.qq) Enum.valueOf(com.a.b.d.qq, p1));
    }

    public static com.a.b.d.qq[] values()
    {
        return ((com.a.b.d.qq[]) com.a.b.d.qq.f.clone());
    }

    abstract boolean a();
}
