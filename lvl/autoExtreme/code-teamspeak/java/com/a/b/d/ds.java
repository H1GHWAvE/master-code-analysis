package com.a.b.d;
final class ds extends com.a.b.d.hi {
    private final java.util.Set a;
    private final com.a.b.d.dm b;

    public ds(java.util.Set p2, com.a.b.d.dm p3)
    {
        this.a = ((java.util.Set) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.d.dm) com.a.b.b.cn.a(p3));
        return;
    }

    protected final java.util.Set a()
    {
        return this.a;
    }

    public final boolean add(Object p2)
    {
        this.b.a(p2);
        return this.a.add(p2);
    }

    public final boolean addAll(java.util.Collection p3)
    {
        return this.a.addAll(com.a.b.d.dn.b(p3, this.b));
    }

    protected final bridge synthetic java.util.Collection b()
    {
        return this.a;
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }
}
