package com.a.b.d;
final class ct extends java.util.AbstractCollection {
    final java.util.Collection a;
    final com.a.b.b.bj b;

    ct(java.util.Collection p2, com.a.b.b.bj p3)
    {
        this.a = ((java.util.Collection) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.b.bj) com.a.b.b.cn.a(p3));
        return;
    }

    public final void clear()
    {
        this.a.clear();
        return;
    }

    public final boolean isEmpty()
    {
        return this.a.isEmpty();
    }

    public final java.util.Iterator iterator()
    {
        return com.a.b.d.nj.a(this.a.iterator(), this.b);
    }

    public final int size()
    {
        return this.a.size();
    }
}
