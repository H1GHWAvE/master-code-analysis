package com.a.b.d;
public abstract enum class sh extends java.lang.Enum {
    public static final enum com.a.b.d.sh a;
    public static final enum com.a.b.d.sh b;
    public static final enum com.a.b.d.sh c;
    private static final synthetic com.a.b.d.sh[] d;

    static sh()
    {
        com.a.b.d.sh.a = new com.a.b.d.si("STRONG");
        com.a.b.d.sh.b = new com.a.b.d.sj("SOFT");
        com.a.b.d.sh.c = new com.a.b.d.sk("WEAK");
        com.a.b.d.sh[] v0_7 = new com.a.b.d.sh[3];
        v0_7[0] = com.a.b.d.sh.a;
        v0_7[1] = com.a.b.d.sh.b;
        v0_7[2] = com.a.b.d.sh.c;
        com.a.b.d.sh.d = v0_7;
        return;
    }

    private sh(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic sh(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.d.sh valueOf(String p1)
    {
        return ((com.a.b.d.sh) Enum.valueOf(com.a.b.d.sh, p1));
    }

    public static com.a.b.d.sh[] values()
    {
        return ((com.a.b.d.sh[]) com.a.b.d.sh.d.clone());
    }

    abstract com.a.b.b.au a();

    abstract com.a.b.d.sr a();
}
