package com.a.b.d;
final class tb extends com.a.b.d.am {
    final synthetic java.util.Map$Entry a;
    final synthetic com.a.b.d.tv b;

    tb(java.util.Map$Entry p1, com.a.b.d.tv p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final Object getKey()
    {
        return this.a.getKey();
    }

    public final Object getValue()
    {
        return this.b.a(this.a.getKey(), this.a.getValue());
    }
}
