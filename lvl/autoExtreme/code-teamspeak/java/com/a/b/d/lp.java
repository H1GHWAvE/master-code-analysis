package com.a.b.d;
public class lp extends com.a.b.d.ja {

    public lp()
    {
        this(0);
        return;
    }

    private lp(byte p2)
    {
        this(4);
        return;
    }

    public synthetic com.a.b.d.iz a()
    {
        return this.b();
    }

    public synthetic com.a.b.d.ja a(Object p2)
    {
        return this.c(p2);
    }

    public synthetic com.a.b.d.jb a(Iterable p2)
    {
        return this.b(p2);
    }

    public synthetic com.a.b.d.jb a(java.util.Iterator p2)
    {
        return this.b(p2);
    }

    public synthetic com.a.b.d.jb a(Object[] p2)
    {
        return this.b(p2);
    }

    public synthetic com.a.b.d.jb b(Object p2)
    {
        return this.c(p2);
    }

    public com.a.b.d.lo b()
    {
        com.a.b.d.lo v0_1 = com.a.b.d.lo.a(this.b, this.a);
        this.b = v0_1.size();
        return v0_1;
    }

    public com.a.b.d.lp b(Iterable p1)
    {
        super.a(p1);
        return this;
    }

    public com.a.b.d.lp b(java.util.Iterator p1)
    {
        super.a(p1);
        return this;
    }

    public varargs com.a.b.d.lp b(Object[] p1)
    {
        super.a(p1);
        return this;
    }

    public com.a.b.d.lp c(Object p1)
    {
        super.a(p1);
        return this;
    }
}
