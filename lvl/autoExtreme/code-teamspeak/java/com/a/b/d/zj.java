package com.a.b.d;
final class zj extends com.a.b.d.ku {
    private final transient com.a.b.d.jt a;
    private final transient int b;

    zj(com.a.b.d.jt p1, int p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private com.a.b.d.lo b()
    {
        return this.a.g();
    }

    public final int a(Object p2)
    {
        int v0_3;
        int v0_2 = ((Integer) this.a.get(p2));
        if (v0_2 != 0) {
            v0_3 = v0_2.intValue();
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    final com.a.b.d.xd a(int p3)
    {
        com.a.b.d.xd v0_4 = ((java.util.Map$Entry) this.a.e().f().get(p3));
        return com.a.b.d.xe.a(v0_4.getKey(), ((Integer) v0_4.getValue()).intValue());
    }

    public final boolean contains(Object p2)
    {
        return this.a.containsKey(p2);
    }

    final boolean h_()
    {
        return this.a.i_();
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final synthetic java.util.Set n_()
    {
        return this.a.g();
    }

    public final int size()
    {
        return this.b;
    }
}
