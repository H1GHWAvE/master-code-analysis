package com.a.b.d;
final class nu extends com.a.b.d.agi {
    final synthetic java.util.Iterator a;
    final synthetic int b;
    final synthetic boolean c;

    nu(java.util.Iterator p1, int p2, boolean p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    private java.util.List a()
    {
        if (this.hasNext()) {
            int v3_0 = new Object[this.b];
            java.util.List v0_2 = 0;
            while ((v0_2 < this.b) && (this.a.hasNext())) {
                v3_0[v0_2] = this.a.next();
                v0_2++;
            }
            java.util.List v2_3 = v0_2;
            while (v2_3 < this.b) {
                v3_0[v2_3] = 0;
                v2_3++;
            }
            java.util.List v0_3;
            java.util.List v2_5 = java.util.Collections.unmodifiableList(java.util.Arrays.asList(v3_0));
            if ((!this.c) && (v0_2 != this.b)) {
                v0_3 = v2_5.subList(0, v0_2);
            } else {
                v0_3 = v2_5;
            }
            return v0_3;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final boolean hasNext()
    {
        return this.a.hasNext();
    }

    public final synthetic Object next()
    {
        if (this.hasNext()) {
            int v3_0 = new Object[this.b];
            java.util.List v0_2 = 0;
            while ((v0_2 < this.b) && (this.a.hasNext())) {
                v3_0[v0_2] = this.a.next();
                v0_2++;
            }
            java.util.List v2_3 = v0_2;
            while (v2_3 < this.b) {
                v3_0[v2_3] = 0;
                v2_3++;
            }
            java.util.List v0_3;
            java.util.List v2_5 = java.util.Collections.unmodifiableList(java.util.Arrays.asList(v3_0));
            if ((!this.c) && (v0_2 != this.b)) {
                v0_3 = v2_5.subList(0, v0_2);
            } else {
                v0_3 = v2_5;
            }
            return v0_3;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }
}
