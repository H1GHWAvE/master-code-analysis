package com.a.b.d;
 class up extends com.a.b.d.uk implements java.util.SortedSet {

    up(java.util.SortedMap p1)
    {
        this(p1);
        return;
    }

    java.util.SortedMap a()
    {
        return ((java.util.SortedMap) super.b());
    }

    synthetic java.util.Map b()
    {
        return this.a();
    }

    public java.util.Comparator comparator()
    {
        return this.a().comparator();
    }

    public Object first()
    {
        return this.a().firstKey();
    }

    public java.util.SortedSet headSet(Object p3)
    {
        return new com.a.b.d.up(this.a().headMap(p3));
    }

    public Object last()
    {
        return this.a().lastKey();
    }

    public java.util.SortedSet subSet(Object p3, Object p4)
    {
        return new com.a.b.d.up(this.a().subMap(p3, p4));
    }

    public java.util.SortedSet tailSet(Object p3)
    {
        return new com.a.b.d.up(this.a().tailMap(p3));
    }
}
