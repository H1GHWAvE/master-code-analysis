package com.a.b.d;
public abstract class gx extends com.a.b.d.hg implements com.a.b.d.vi {

    protected gx()
    {
        return;
    }

    public boolean a(com.a.b.d.vi p2)
    {
        return this.c().a(p2);
    }

    public boolean a(Object p2, Object p3)
    {
        return this.c().a(p2, p3);
    }

    public java.util.Collection b(Object p2, Iterable p3)
    {
        return this.c().b(p2, p3);
    }

    public java.util.Map b()
    {
        return this.c().b();
    }

    public final boolean b(Object p2, Object p3)
    {
        return this.c().b(p2, p3);
    }

    protected abstract com.a.b.d.vi c();

    public java.util.Collection c(Object p2)
    {
        return this.c().c(p2);
    }

    public boolean c(Object p2, Iterable p3)
    {
        return this.c().c(p2, p3);
    }

    public boolean c(Object p2, Object p3)
    {
        return this.c().c(p2, p3);
    }

    public java.util.Collection d(Object p2)
    {
        return this.c().d(p2);
    }

    public boolean equals(Object p2)
    {
        if ((p2 != this) && (!this.c().equals(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final int f()
    {
        return this.c().f();
    }

    public final boolean f(Object p2)
    {
        return this.c().f(p2);
    }

    public void g()
    {
        this.c().g();
        return;
    }

    public final boolean g(Object p2)
    {
        return this.c().g(p2);
    }

    public int hashCode()
    {
        return this.c().hashCode();
    }

    public java.util.Collection i()
    {
        return this.c().i();
    }

    public java.util.Collection k()
    {
        return this.c().k();
    }

    protected synthetic Object k_()
    {
        return this.c();
    }

    public final boolean n()
    {
        return this.c().n();
    }

    public java.util.Set p()
    {
        return this.c().p();
    }

    public com.a.b.d.xc q()
    {
        return this.c().q();
    }
}
