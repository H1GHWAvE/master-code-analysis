package com.a.b.d;
 class ur extends com.a.b.d.uj {
    final java.util.Map a;
    final com.a.b.d.tv b;

    ur(java.util.Map p2, com.a.b.d.tv p3)
    {
        this.a = ((java.util.Map) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.d.tv) com.a.b.b.cn.a(p3));
        return;
    }

    protected final java.util.Set a()
    {
        return new com.a.b.d.us(this);
    }

    public void clear()
    {
        this.a.clear();
        return;
    }

    public boolean containsKey(Object p2)
    {
        return this.a.containsKey(p2);
    }

    public Object get(Object p3)
    {
        int v0_2;
        int v0_1 = this.a.get(p3);
        if ((v0_1 == 0) && (!this.a.containsKey(p3))) {
            v0_2 = 0;
        } else {
            v0_2 = this.b.a(p3, v0_1);
        }
        return v0_2;
    }

    public java.util.Set keySet()
    {
        return this.a.keySet();
    }

    public Object remove(Object p3)
    {
        int v0_2;
        if (!this.a.containsKey(p3)) {
            v0_2 = 0;
        } else {
            v0_2 = this.b.a(p3, this.a.remove(p3));
        }
        return v0_2;
    }

    public int size()
    {
        return this.a.size();
    }
}
