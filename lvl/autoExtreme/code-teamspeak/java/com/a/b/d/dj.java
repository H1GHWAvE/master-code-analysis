package com.a.b.d;
final class dj extends com.a.b.d.go {
    final synthetic java.util.Iterator a;
    final synthetic com.a.b.d.dg b;
    private com.a.b.d.xd c;

    dj(com.a.b.d.dg p1, java.util.Iterator p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    private com.a.b.d.xd b()
    {
        this.c = ((com.a.b.d.xd) super.next());
        return this.c;
    }

    protected final java.util.Iterator a()
    {
        return this.a;
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public final bridge synthetic Object next()
    {
        this.c = ((com.a.b.d.xd) super.next());
        return this.c;
    }

    public final void remove()
    {
        int v0_1;
        if (this.c == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        this.b.c(this.c.a(), 0);
        this.c = 0;
        return;
    }
}
