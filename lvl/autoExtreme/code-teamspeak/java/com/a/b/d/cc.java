package com.a.b.d;
final class cc extends com.a.b.d.agi {
    final synthetic com.a.b.d.bx a;
    private final java.util.Deque b;
    private final java.util.BitSet c;

    cc(com.a.b.d.bx p2, Object p3)
    {
        this.a = p2;
        this.b = new java.util.ArrayDeque();
        this.b.addLast(p3);
        this.c = new java.util.BitSet();
        return;
    }

    public final boolean hasNext()
    {
        int v0_2;
        if (this.b.isEmpty()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final Object next()
    {
        while(true) {
            java.util.Deque v0_1 = this.b.getLast();
            if (this.c.get((this.b.size() - 1))) {
                break;
            }
            this.c.set((this.b.size() - 1));
            com.a.b.d.bx.a(this.b, this.a.b());
            com.a.b.d.bx.a(this.b, this.a.a());
        }
        this.b.removeLast();
        this.c.clear(this.b.size());
        return v0_1;
    }
}
