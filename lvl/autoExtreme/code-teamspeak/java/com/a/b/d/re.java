package com.a.b.d;
abstract enum class re extends java.lang.Enum {
    public static final enum com.a.b.d.re a = None;
    public static final enum com.a.b.d.re b = None;
    public static final enum com.a.b.d.re c = None;
    public static final enum com.a.b.d.re d = None;
    public static final enum com.a.b.d.re e = None;
    public static final enum com.a.b.d.re f = None;
    public static final enum com.a.b.d.re g = None;
    public static final enum com.a.b.d.re h = None;
    static final int i = 1;
    static final int j = 2;
    static final com.a.b.d.re[][] k;
    private static final synthetic com.a.b.d.re[] l;

    static re()
    {
        com.a.b.d.re.a = new com.a.b.d.rf("STRONG");
        com.a.b.d.re.b = new com.a.b.d.rg("STRONG_EXPIRABLE");
        com.a.b.d.re.c = new com.a.b.d.rh("STRONG_EVICTABLE");
        com.a.b.d.re.d = new com.a.b.d.ri("STRONG_EXPIRABLE_EVICTABLE");
        com.a.b.d.re.e = new com.a.b.d.rj("WEAK");
        com.a.b.d.re.f = new com.a.b.d.rk("WEAK_EXPIRABLE");
        com.a.b.d.re.g = new com.a.b.d.rl("WEAK_EVICTABLE");
        com.a.b.d.re.h = new com.a.b.d.rm("WEAK_EXPIRABLE_EVICTABLE");
        com.a.b.d.re[][] v0_17 = new com.a.b.d.re[8];
        v0_17[0] = com.a.b.d.re.a;
        v0_17[1] = com.a.b.d.re.b;
        v0_17[2] = com.a.b.d.re.c;
        v0_17[3] = com.a.b.d.re.d;
        v0_17[4] = com.a.b.d.re.e;
        v0_17[5] = com.a.b.d.re.f;
        v0_17[6] = com.a.b.d.re.g;
        v0_17[7] = com.a.b.d.re.h;
        com.a.b.d.re.l = v0_17;
        com.a.b.d.re[][] v0_18 = new com.a.b.d.re[][3];
        com.a.b.d.re[] v1_16 = new com.a.b.d.re[4];
        v1_16[0] = com.a.b.d.re.a;
        v1_16[1] = com.a.b.d.re.b;
        v1_16[2] = com.a.b.d.re.c;
        v1_16[3] = com.a.b.d.re.d;
        v0_18[0] = v1_16;
        com.a.b.d.re[] v1_17 = new com.a.b.d.re[0];
        v0_18[1] = v1_17;
        com.a.b.d.re[] v1_18 = new com.a.b.d.re[4];
        v1_18[0] = com.a.b.d.re.e;
        v1_18[1] = com.a.b.d.re.f;
        v1_18[2] = com.a.b.d.re.g;
        v1_18[3] = com.a.b.d.re.h;
        v0_18[2] = v1_18;
        com.a.b.d.re.k = v0_18;
        return;
    }

    private re(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic re(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    static com.a.b.d.re a(com.a.b.d.sh p3, boolean p4, boolean p5)
    {
        com.a.b.d.re[] v1_0;
        com.a.b.d.re v0_0 = 0;
        if (!p4) {
            v1_0 = 0;
        } else {
            v1_0 = 1;
        }
        if (p5) {
            v0_0 = 2;
        }
        return com.a.b.d.re.k[p3.ordinal()][(v0_0 | v1_0)];
    }

    static void a(com.a.b.d.rz p2, com.a.b.d.rz p3)
    {
        p3.a(p2.e());
        com.a.b.d.qy.a(p2.g(), p3);
        com.a.b.d.qy.a(p3, p2.f());
        com.a.b.d.qy.b(p2);
        return;
    }

    static void b(com.a.b.d.rz p1, com.a.b.d.rz p2)
    {
        com.a.b.d.qy.b(p1.i(), p2);
        com.a.b.d.qy.b(p2, p1.h());
        com.a.b.d.qy.c(p1);
        return;
    }

    public static com.a.b.d.re valueOf(String p1)
    {
        return ((com.a.b.d.re) Enum.valueOf(com.a.b.d.re, p1));
    }

    public static com.a.b.d.re[] values()
    {
        return ((com.a.b.d.re[]) com.a.b.d.re.l.clone());
    }

    com.a.b.d.rz a(com.a.b.d.sa p3, com.a.b.d.rz p4, com.a.b.d.rz p5)
    {
        return this.a(p3, p4.d(), p4.c(), p5);
    }

    abstract com.a.b.d.rz a();
}
