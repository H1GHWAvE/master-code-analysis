package com.a.b.d;
final class dz extends com.a.b.d.dw {
    private static final long b;

    dz(Comparable p2)
    {
        this(((Comparable) com.a.b.b.cn.a(p2)));
        return;
    }

    final com.a.b.d.ce a()
    {
        return com.a.b.d.ce.a;
    }

    final com.a.b.d.dw a(com.a.b.d.ce p3, com.a.b.d.ep p4)
    {
        switch (com.a.b.d.dx.a[p3.ordinal()]) {
            case 1:
                Comparable v0_3 = p4.a(this.a);
                if (v0_3 != null) {
                    this = com.a.b.d.dz.b(v0_3);
                } else {
                    this = com.a.b.d.ea.f();
                }
            case 2:
                break;
            default:
                throw new AssertionError();
        }
        return this;
    }

    final Comparable a(com.a.b.d.ep p2)
    {
        return p2.a(this.a);
    }

    final void a(StringBuilder p3)
    {
        p3.append(40).append(this.a);
        return;
    }

    final boolean a(Comparable p2)
    {
        int v0_2;
        if (com.a.b.d.yl.b(this.a, p2) >= 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    final com.a.b.d.ce b()
    {
        return com.a.b.d.ce.b;
    }

    final com.a.b.d.dw b(com.a.b.d.ce p3, com.a.b.d.ep p4)
    {
        switch (com.a.b.d.dx.a[p3.ordinal()]) {
            case 1:
                break;
            case 2:
                Comparable v0_3 = p4.a(this.a);
                if (v0_3 != null) {
                    this = com.a.b.d.dz.b(v0_3);
                } else {
                    this = com.a.b.d.dy.f();
                }
                break;
            default:
                throw new AssertionError();
        }
        return this;
    }

    final Comparable b(com.a.b.d.ep p2)
    {
        return this.a;
    }

    final void b(StringBuilder p3)
    {
        p3.append(this.a).append(93);
        return;
    }

    final com.a.b.d.dw c(com.a.b.d.ep p2)
    {
        com.a.b.d.dy v0_1;
        com.a.b.d.dy v0_0 = this.a(p2);
        if (v0_0 == null) {
            v0_1 = com.a.b.d.dy.f();
        } else {
            v0_1 = com.a.b.d.dz.b(v0_0);
        }
        return v0_1;
    }

    public final synthetic int compareTo(Object p2)
    {
        return super.a(((com.a.b.d.dw) p2));
    }

    public final int hashCode()
    {
        return (this.a.hashCode() ^ -1);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 2)).append("/").append(v0_2).append("\\").toString();
    }
}
