package com.a.b.d;
public abstract class gd implements java.lang.Iterable {
    public final Iterable c;

    protected gd()
    {
        this.c = this;
        return;
    }

    gd(Iterable p2)
    {
        this.c = ((Iterable) com.a.b.b.cn.a(p2));
        return;
    }

    private int a()
    {
        int v0_2;
        int v0_0 = this.c;
        if (!(v0_0 instanceof java.util.Collection)) {
            v0_2 = com.a.b.d.nj.b(v0_0.iterator());
        } else {
            v0_2 = ((java.util.Collection) v0_0).size();
        }
        return v0_2;
    }

    private com.a.b.d.gd a(int p4)
    {
        com.a.b.d.ms v1_0;
        com.a.b.d.gd v0_0 = this.c;
        com.a.b.b.cn.a(v0_0);
        if (p4 < 0) {
            v1_0 = 0;
        } else {
            v1_0 = 1;
        }
        com.a.b.d.gd v0_1;
        com.a.b.b.cn.a(v1_0, "number to skip cannot be negative");
        if (!(v0_0 instanceof java.util.List)) {
            v0_1 = new com.a.b.d.ms(v0_0, p4);
        } else {
            v0_1 = new com.a.b.d.ng(((java.util.List) v0_0), p4);
        }
        return com.a.b.d.gd.a(v0_1);
    }

    private com.a.b.d.gd a(com.a.b.b.bj p2)
    {
        return com.a.b.d.gd.a(com.a.b.d.mq.a(this.c, p2));
    }

    private static com.a.b.d.gd a(com.a.b.d.gd p1)
    {
        return ((com.a.b.d.gd) com.a.b.b.cn.a(p1));
    }

    public static com.a.b.d.gd a(Iterable p1)
    {
        com.a.b.d.ge v1_1;
        if (!(p1 instanceof com.a.b.d.gd)) {
            v1_1 = new com.a.b.d.ge(p1, p1);
        } else {
            v1_1 = ((com.a.b.d.gd) p1);
        }
        return v1_1;
    }

    private static com.a.b.d.gd a(Object[] p1)
    {
        return com.a.b.d.gd.a(com.a.b.d.ov.a(p1));
    }

    private com.a.b.d.jl a(java.util.Comparator p3)
    {
        return com.a.b.d.yd.a(p3).b(this.c);
    }

    private String a(com.a.b.b.bv p2)
    {
        return p2.a(this);
    }

    private java.util.Collection a(java.util.Collection p3)
    {
        com.a.b.b.cn.a(p3);
        if (!(this.c instanceof java.util.Collection)) {
            java.util.Iterator v0_3 = this.c.iterator();
            while (v0_3.hasNext()) {
                p3.add(v0_3.next());
            }
        } else {
            p3.addAll(com.a.b.d.cm.a(this.c));
        }
        return p3;
    }

    private boolean a(Object p3)
    {
        boolean v0_2;
        boolean v0_0 = this.c;
        if (!(v0_0 instanceof java.util.Collection)) {
            v0_2 = com.a.b.d.nj.a(v0_0.iterator(), p3);
        } else {
            v0_2 = com.a.b.d.cm.a(((java.util.Collection) v0_0), p3);
        }
        return v0_2;
    }

    private com.a.b.d.gd b()
    {
        return com.a.b.d.gd.a(com.a.b.d.mq.d(this.c));
    }

    private com.a.b.d.gd b(int p4)
    {
        com.a.b.d.gd v0_0;
        Iterable v1 = this.c;
        com.a.b.b.cn.a(v1);
        if (p4 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0, "limit is negative");
        return com.a.b.d.gd.a(new com.a.b.d.mu(v1, p4));
    }

    private com.a.b.d.gd b(com.a.b.b.bj p2)
    {
        return com.a.b.d.gd.a(com.a.b.d.mq.e(com.a.b.d.gd.a(com.a.b.d.mq.a(this.c, p2))));
    }

    private com.a.b.d.gd b(Iterable p2)
    {
        return com.a.b.d.gd.a(com.a.b.d.mq.a(this.c, p2));
    }

    private varargs com.a.b.d.gd b(Object[] p3)
    {
        return com.a.b.d.gd.a(com.a.b.d.mq.a(this.c, java.util.Arrays.asList(p3)));
    }

    private com.a.b.d.me b(java.util.Comparator p2)
    {
        return com.a.b.d.me.a(p2, this.c);
    }

    private boolean b(com.a.b.b.co p2)
    {
        return com.a.b.d.mq.d(this.c, p2);
    }

    private Object[] b(Class p2)
    {
        return com.a.b.d.mq.a(this.c, p2);
    }

    private com.a.b.b.ci c()
    {
        com.a.b.b.ci v0_2;
        com.a.b.b.ci v0_1 = this.c.iterator();
        if (!v0_1.hasNext()) {
            v0_2 = com.a.b.b.ci.f();
        } else {
            v0_2 = com.a.b.b.ci.b(v0_1.next());
        }
        return v0_2;
    }

    private com.a.b.d.jt c(com.a.b.b.bj p2)
    {
        return com.a.b.d.sz.a(this.c, p2);
    }

    private Object c(int p3)
    {
        Object v0_2;
        Object v0_0 = this.c;
        com.a.b.b.cn.a(v0_0);
        if (!(v0_0 instanceof java.util.List)) {
            v0_2 = com.a.b.d.nj.c(v0_0.iterator(), p3);
        } else {
            v0_2 = ((java.util.List) v0_0).get(p3);
        }
        return v0_2;
    }

    private boolean c(com.a.b.b.co p2)
    {
        return com.a.b.d.mq.e(this.c, p2);
    }

    private com.a.b.b.ci d()
    {
        com.a.b.b.ci v0_4;
        if (!(this.c instanceof java.util.List)) {
            com.a.b.b.ci v0_3 = this.c.iterator();
            if (v0_3.hasNext()) {
                if ((this.c instanceof java.util.SortedSet)) {
                    v0_4 = com.a.b.b.ci.b(((java.util.SortedSet) this.c).last());
                    return v0_4;
                }
                do {
                    Object v1_3 = v0_3.next();
                } while(v0_3.hasNext());
                v0_4 = com.a.b.b.ci.b(v1_3);
            } else {
                v0_4 = com.a.b.b.ci.f();
            }
        } else {
            com.a.b.b.ci v0_9 = ((java.util.List) this.c);
            if (!v0_9.isEmpty()) {
                v0_4 = com.a.b.b.ci.b(v0_9.get((v0_9.size() - 1)));
            } else {
                v0_4 = com.a.b.b.ci.f();
            }
        }
        return v0_4;
    }

    private com.a.b.b.ci d(com.a.b.b.co p2)
    {
        return com.a.b.d.nj.f(this.c.iterator(), p2);
    }

    private com.a.b.d.jr d(com.a.b.b.bj p5)
    {
        com.a.b.d.jr v0_1 = this.c.iterator();
        com.a.b.b.cn.a(p5);
        com.a.b.d.js v1 = com.a.b.d.jr.c();
        while (v0_1.hasNext()) {
            Object v2_1 = v0_1.next();
            com.a.b.b.cn.a(v2_1, v0_1);
            v1.a(p5.e(v2_1), v2_1);
        }
        return v1.a();
    }

    private com.a.b.d.jt e(com.a.b.b.bj p2)
    {
        return com.a.b.d.sz.b(this.c, p2);
    }

    private boolean e()
    {
        int v0_3;
        if (this.c.iterator().hasNext()) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private com.a.b.d.jl f()
    {
        return com.a.b.d.jl.a(this.c);
    }

    private com.a.b.d.lo g()
    {
        return com.a.b.d.lo.a(this.c);
    }

    public final com.a.b.d.gd a(com.a.b.b.co p2)
    {
        return com.a.b.d.gd.a(com.a.b.d.mq.c(this.c, p2));
    }

    public final com.a.b.d.gd a(Class p3)
    {
        com.a.b.d.gd v0_0 = this.c;
        com.a.b.b.cn.a(v0_0);
        com.a.b.b.cn.a(p3);
        return com.a.b.d.gd.a(new com.a.b.d.ne(v0_0, p3));
    }

    public String toString()
    {
        return com.a.b.d.mq.a(this.c);
    }
}
