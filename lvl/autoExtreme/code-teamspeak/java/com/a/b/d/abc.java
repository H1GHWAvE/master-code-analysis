package com.a.b.d;
public abstract enum class abc extends java.lang.Enum {
    public static final enum com.a.b.d.abc a;
    public static final enum com.a.b.d.abc b;
    public static final enum com.a.b.d.abc c;
    private static final synthetic com.a.b.d.abc[] d;

    static abc()
    {
        com.a.b.d.abc.a = new com.a.b.d.abd("NEXT_LOWER");
        com.a.b.d.abc.b = new com.a.b.d.abe("NEXT_HIGHER");
        com.a.b.d.abc.c = new com.a.b.d.abf("INVERTED_INSERTION_INDEX");
        com.a.b.d.abc[] v0_7 = new com.a.b.d.abc[3];
        v0_7[0] = com.a.b.d.abc.a;
        v0_7[1] = com.a.b.d.abc.b;
        v0_7[2] = com.a.b.d.abc.c;
        com.a.b.d.abc.d = v0_7;
        return;
    }

    private abc(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic abc(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.d.abc valueOf(String p1)
    {
        return ((com.a.b.d.abc) Enum.valueOf(com.a.b.d.abc, p1));
    }

    public static com.a.b.d.abc[] values()
    {
        return ((com.a.b.d.abc[]) com.a.b.d.abc.d.clone());
    }

    abstract int a();
}
