package com.a.b.d;
abstract class as extends java.util.AbstractCollection implements com.a.b.d.xc {
    private transient java.util.Set a;
    private transient java.util.Set b;

    as()
    {
        return;
    }

    public int a(Object p4)
    {
        java.util.Iterator v1 = this.a().iterator();
        while (v1.hasNext()) {
            int v0_4 = ((com.a.b.d.xd) v1.next());
            if (com.a.b.b.ce.a(v0_4.a(), p4)) {
                int v0_2 = v0_4.b();
            }
            return v0_2;
        }
        v0_2 = 0;
        return v0_2;
    }

    public int a(Object p2, int p3)
    {
        throw new UnsupportedOperationException();
    }

    public java.util.Set a()
    {
        java.util.Set v0 = this.b;
        if (v0 == null) {
            v0 = this.f();
            this.b = v0;
        }
        return v0;
    }

    public boolean a(Object p2, int p3, int p4)
    {
        return com.a.b.d.xe.a(this, p2, p3, p4);
    }

    public boolean add(Object p2)
    {
        this.a(p2, 1);
        return 1;
    }

    public boolean addAll(java.util.Collection p2)
    {
        return com.a.b.d.xe.a(this, p2);
    }

    public int b(Object p2, int p3)
    {
        throw new UnsupportedOperationException();
    }

    abstract java.util.Iterator b();

    abstract int c();

    public int c(Object p2, int p3)
    {
        return com.a.b.d.xe.a(this, p2, p3);
    }

    public void clear()
    {
        com.a.b.d.nj.i(this.b());
        return;
    }

    public boolean contains(Object p2)
    {
        int v0_1;
        if (this.a(p2) <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    java.util.Set e()
    {
        return new com.a.b.d.at(this);
    }

    public boolean equals(Object p2)
    {
        return com.a.b.d.xe.a(this, p2);
    }

    java.util.Set f()
    {
        return new com.a.b.d.au(this);
    }

    public int hashCode()
    {
        return this.a().hashCode();
    }

    public boolean isEmpty()
    {
        return this.a().isEmpty();
    }

    public java.util.Iterator iterator()
    {
        return com.a.b.d.xe.b(this);
    }

    public java.util.Set n_()
    {
        java.util.Set v0 = this.a;
        if (v0 == null) {
            v0 = this.e();
            this.a = v0;
        }
        return v0;
    }

    public boolean remove(Object p3)
    {
        int v0 = 1;
        if (this.b(p3, 1) <= 0) {
            v0 = 0;
        }
        return v0;
    }

    public boolean removeAll(java.util.Collection p2)
    {
        return com.a.b.d.xe.b(this, p2);
    }

    public boolean retainAll(java.util.Collection p2)
    {
        return com.a.b.d.xe.c(this, p2);
    }

    public int size()
    {
        return com.a.b.d.xe.c(this);
    }

    public String toString()
    {
        return this.a().toString();
    }
}
