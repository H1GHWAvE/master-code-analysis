package com.a.b.d;
final class acx extends com.a.b.d.adq {
    private static final long a;

    acx(java.util.Set p1, Object p2)
    {
        this(p1, p2);
        return;
    }

    public final boolean contains(Object p3)
    {
        try {
            return com.a.b.d.sz.a(this.c(), p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final boolean containsAll(java.util.Collection p3)
    {
        try {
            return com.a.b.d.cm.a(this.c(), p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final boolean equals(Object p3)
    {
        Throwable v0_1;
        if (p3 != this) {
            try {
                v0_1 = com.a.b.d.aad.a(this.c(), p3);
            } catch (Throwable v0_2) {
                throw v0_2;
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.acy(this, super.iterator());
    }

    public final boolean remove(Object p3)
    {
        try {
            return com.a.b.d.sz.b(this.c(), p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final boolean removeAll(java.util.Collection p3)
    {
        try {
            return com.a.b.d.nj.a(this.c().iterator(), p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final boolean retainAll(java.util.Collection p3)
    {
        try {
            return com.a.b.d.nj.b(this.c().iterator(), p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final Object[] toArray()
    {
        try {
            return com.a.b.d.yc.a(this.c());
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final Object[] toArray(Object[] p3)
    {
        try {
            return com.a.b.d.yc.a(this.c(), p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }
}
