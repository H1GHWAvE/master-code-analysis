package com.a.b.d;
public class kw extends com.a.b.d.jb {
    final com.a.b.d.xc a;

    public kw()
    {
        this(com.a.b.d.oi.g());
        return;
    }

    kw(com.a.b.d.xc p1)
    {
        this.a = p1;
        return;
    }

    public synthetic com.a.b.d.iz a()
    {
        return this.b();
    }

    public synthetic com.a.b.d.jb a(Iterable p2)
    {
        return this.b(p2);
    }

    public synthetic com.a.b.d.jb a(java.util.Iterator p2)
    {
        return this.b(p2);
    }

    public synthetic com.a.b.d.jb a(Object[] p2)
    {
        return this.b(p2);
    }

    public com.a.b.d.kw a(Object p3)
    {
        this.a.add(com.a.b.b.cn.a(p3));
        return this;
    }

    public com.a.b.d.kw a(Object p3, int p4)
    {
        this.a.a(com.a.b.b.cn.a(p3), p4);
        return this;
    }

    public synthetic com.a.b.d.jb b(Object p2)
    {
        return this.a(p2);
    }

    public com.a.b.d.ku b()
    {
        return com.a.b.d.ku.a(this.a);
    }

    public com.a.b.d.kw b(Iterable p4)
    {
        if (!(p4 instanceof com.a.b.d.xc)) {
            super.a(p4);
        } else {
            java.util.Iterator v1 = com.a.b.d.xe.b(p4).a().iterator();
            while (v1.hasNext()) {
                int v0_5 = ((com.a.b.d.xd) v1.next());
                this.a(v0_5.a(), v0_5.b());
            }
        }
        return this;
    }

    public com.a.b.d.kw b(Object p3, int p4)
    {
        this.a.c(com.a.b.b.cn.a(p3), p4);
        return this;
    }

    public com.a.b.d.kw b(java.util.Iterator p1)
    {
        super.a(p1);
        return this;
    }

    public varargs com.a.b.d.kw b(Object[] p1)
    {
        super.a(p1);
        return this;
    }
}
