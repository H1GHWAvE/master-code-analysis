package com.a.b.d;
final class cd extends com.a.b.d.agi implements com.a.b.d.yi {
    final synthetic com.a.b.d.bx a;
    private final java.util.Deque b;

    cd(com.a.b.d.bx p2, Object p3)
    {
        this.a = p2;
        this.b = new java.util.ArrayDeque();
        this.b.addLast(p3);
        return;
    }

    public final Object a()
    {
        return this.b.getLast();
    }

    public final boolean hasNext()
    {
        int v0_2;
        if (this.b.isEmpty()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final Object next()
    {
        Object v0_1 = this.b.removeLast();
        com.a.b.d.bx.a(this.b, this.a.b());
        com.a.b.d.bx.a(this.b, this.a.a());
        return v0_1;
    }
}
