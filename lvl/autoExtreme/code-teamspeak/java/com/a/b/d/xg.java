package com.a.b.d;
final class xg extends com.a.b.d.j {
    final synthetic java.util.Iterator a;
    final synthetic java.util.Iterator b;
    final synthetic com.a.b.d.xf c;

    xg(com.a.b.d.xf p1, java.util.Iterator p2, java.util.Iterator p3)
    {
        this.c = p1;
        this.a = p2;
        this.b = p3;
        return;
    }

    private com.a.b.d.xd c()
    {
        com.a.b.d.xd v0_4;
        if (!this.a.hasNext()) {
            while (this.b.hasNext()) {
                com.a.b.d.xd v0_7 = ((com.a.b.d.xd) this.b.next());
                Object v1_0 = v0_7.a();
                if (!this.c.a.contains(v1_0)) {
                    v0_4 = com.a.b.d.xe.a(v1_0, v0_7.b());
                }
            }
            this.b();
            v0_4 = 0;
        } else {
            com.a.b.d.xd v0_11 = ((com.a.b.d.xd) this.a.next());
            Object v1_1 = v0_11.a();
            v0_4 = com.a.b.d.xe.a(v1_1, Math.max(v0_11.b(), this.c.b.a(v1_1)));
        }
        return v0_4;
    }

    protected final synthetic Object a()
    {
        com.a.b.d.xd v0_4;
        if (!this.a.hasNext()) {
            while (this.b.hasNext()) {
                com.a.b.d.xd v0_7 = ((com.a.b.d.xd) this.b.next());
                Object v1_0 = v0_7.a();
                if (!this.c.a.contains(v1_0)) {
                    v0_4 = com.a.b.d.xe.a(v1_0, v0_7.b());
                }
            }
            this.b();
            v0_4 = 0;
        } else {
            com.a.b.d.xd v0_11 = ((com.a.b.d.xd) this.a.next());
            Object v1_1 = v0_11.a();
            v0_4 = com.a.b.d.xe.a(v1_1, Math.max(v0_11.b(), this.c.b.a(v1_1)));
        }
        return v0_4;
    }
}
