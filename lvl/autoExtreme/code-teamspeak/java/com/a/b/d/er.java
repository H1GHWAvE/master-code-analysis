package com.a.b.d;
final class er extends com.a.b.d.ep implements java.io.Serializable {
    private static final com.a.b.d.er a;
    private static final long b;

    static er()
    {
        com.a.b.d.er.a = new com.a.b.d.er();
        return;
    }

    private er()
    {
        return;
    }

    private static long a(Integer p4, Integer p5)
    {
        return (((long) p5.intValue()) - ((long) p4.intValue()));
    }

    private static Integer a(Integer p2)
    {
        Integer v0_2;
        Integer v0_0 = p2.intValue();
        if (v0_0 != 2147483647) {
            v0_2 = Integer.valueOf((v0_0 + 1));
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private static Integer b(Integer p2)
    {
        Integer v0_2;
        Integer v0_0 = p2.intValue();
        if (v0_0 != -2147483648) {
            v0_2 = Integer.valueOf((v0_0 - 1));
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    static synthetic com.a.b.d.er c()
    {
        return com.a.b.d.er.a;
    }

    private static Integer d()
    {
        return Integer.valueOf(-2147483648);
    }

    private static Integer e()
    {
        return Integer.valueOf(2147483647);
    }

    private static Object f()
    {
        return com.a.b.d.er.a;
    }

    public final synthetic long a(Comparable p5, Comparable p6)
    {
        return (((long) ((Integer) p6).intValue()) - ((long) ((Integer) p5).intValue()));
    }

    public final synthetic Comparable a()
    {
        return Integer.valueOf(-2147483648);
    }

    public final synthetic Comparable a(Comparable p3)
    {
        Integer v0_2;
        Integer v0_0 = ((Integer) p3).intValue();
        if (v0_0 != 2147483647) {
            v0_2 = Integer.valueOf((v0_0 + 1));
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final synthetic Comparable b()
    {
        return Integer.valueOf(2147483647);
    }

    public final synthetic Comparable b(Comparable p3)
    {
        Integer v0_2;
        Integer v0_0 = ((Integer) p3).intValue();
        if (v0_0 != -2147483648) {
            v0_2 = Integer.valueOf((v0_0 - 1));
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final String toString()
    {
        return "DiscreteDomain.integers()";
    }
}
