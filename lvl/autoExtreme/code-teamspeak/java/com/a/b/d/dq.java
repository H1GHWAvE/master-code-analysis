package com.a.b.d;
final class dq extends com.a.b.d.gq {
    private final java.util.ListIterator a;
    private final com.a.b.d.dm b;

    public dq(java.util.ListIterator p1, com.a.b.d.dm p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    protected final bridge synthetic java.util.Iterator a()
    {
        return this.a;
    }

    public final void add(Object p2)
    {
        this.b.a(p2);
        this.a.add(p2);
        return;
    }

    protected final java.util.ListIterator b()
    {
        return this.a;
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public final void set(Object p2)
    {
        this.b.a(p2);
        this.a.set(p2);
        return;
    }
}
