package com.a.b.d;
 class uu extends com.a.b.d.ur implements java.util.SortedMap {

    uu(java.util.SortedMap p1, com.a.b.d.tv p2)
    {
        this(p1, p2);
        return;
    }

    protected java.util.SortedMap c()
    {
        return ((java.util.SortedMap) this.a);
    }

    public java.util.Comparator comparator()
    {
        return this.c().comparator();
    }

    public Object firstKey()
    {
        return this.c().firstKey();
    }

    public java.util.SortedMap headMap(Object p3)
    {
        return com.a.b.d.sz.a(this.c().headMap(p3), this.b);
    }

    public Object lastKey()
    {
        return this.c().lastKey();
    }

    public java.util.SortedMap subMap(Object p3, Object p4)
    {
        return com.a.b.d.sz.a(this.c().subMap(p3, p4), this.b);
    }

    public java.util.SortedMap tailMap(Object p3)
    {
        return com.a.b.d.sz.a(this.c().tailMap(p3), this.b);
    }
}
