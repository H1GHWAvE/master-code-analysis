package com.a.b.d;
abstract class bc extends com.a.b.d.as implements com.a.b.d.abn {
    final java.util.Comparator a;
    private transient com.a.b.d.abn b;

    bc()
    {
        this(com.a.b.d.yd.d());
        return;
    }

    bc(java.util.Comparator p2)
    {
        this.a = ((java.util.Comparator) com.a.b.b.cn.a(p2));
        return;
    }

    private java.util.NavigableSet o()
    {
        return new com.a.b.d.abr(this);
    }

    private java.util.Iterator p()
    {
        return com.a.b.d.xe.b(this.m());
    }

    private com.a.b.d.abn q()
    {
        return new com.a.b.d.bd(this);
    }

    public com.a.b.d.abn a(Object p2, com.a.b.d.ce p3, Object p4, com.a.b.d.ce p5)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p5);
        return this.c(p2, p3).d(p4, p5);
    }

    public java.util.Comparator comparator()
    {
        return this.a;
    }

    final synthetic java.util.Set e()
    {
        return new com.a.b.d.abr(this);
    }

    public java.util.NavigableSet e_()
    {
        return ((java.util.NavigableSet) super.n_());
    }

    public com.a.b.d.xd h()
    {
        int v0_1;
        int v0_0 = this.b();
        if (!v0_0.hasNext()) {
            v0_1 = 0;
        } else {
            v0_1 = ((com.a.b.d.xd) v0_0.next());
        }
        return v0_1;
    }

    public com.a.b.d.xd i()
    {
        int v0_1;
        int v0_0 = this.l();
        if (!v0_0.hasNext()) {
            v0_1 = 0;
        } else {
            v0_1 = ((com.a.b.d.xd) v0_0.next());
        }
        return v0_1;
    }

    public com.a.b.d.xd j()
    {
        int v0_1;
        java.util.Iterator v1 = this.b();
        if (!v1.hasNext()) {
            v0_1 = 0;
        } else {
            int v0_3 = ((com.a.b.d.xd) v1.next());
            v0_1 = com.a.b.d.xe.a(v0_3.a(), v0_3.b());
            v1.remove();
        }
        return v0_1;
    }

    public com.a.b.d.xd k()
    {
        int v0_1;
        java.util.Iterator v1 = this.l();
        if (!v1.hasNext()) {
            v0_1 = 0;
        } else {
            int v0_3 = ((com.a.b.d.xd) v1.next());
            v0_1 = com.a.b.d.xe.a(v0_3.a(), v0_3.b());
            v1.remove();
        }
        return v0_1;
    }

    abstract java.util.Iterator l();

    public com.a.b.d.abn m()
    {
        com.a.b.d.bd v0_0 = this.b;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.bd(this);
            this.b = v0_0;
        }
        return v0_0;
    }

    public final synthetic java.util.SortedSet n()
    {
        return this.e_();
    }

    public final synthetic java.util.Set n_()
    {
        return this.e_();
    }
}
