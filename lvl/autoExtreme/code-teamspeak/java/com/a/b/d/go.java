package com.a.b.d;
public abstract class go extends com.a.b.d.hg implements java.util.Iterator {

    protected go()
    {
        return;
    }

    protected abstract java.util.Iterator a();

    public boolean hasNext()
    {
        return this.a().hasNext();
    }

    protected synthetic Object k_()
    {
        return this.a();
    }

    public Object next()
    {
        return this.a().next();
    }

    public void remove()
    {
        this.a().remove();
        return;
    }
}
