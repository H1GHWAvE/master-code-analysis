package com.a.b.d;
abstract enum class tr extends java.lang.Enum implements com.a.b.b.bj {
    public static final enum com.a.b.d.tr a;
    public static final enum com.a.b.d.tr b;
    private static final synthetic com.a.b.d.tr[] c;

    static tr()
    {
        com.a.b.d.tr.a = new com.a.b.d.ts("KEY");
        com.a.b.d.tr.b = new com.a.b.d.tt("VALUE");
        com.a.b.d.tr[] v0_5 = new com.a.b.d.tr[2];
        v0_5[0] = com.a.b.d.tr.a;
        v0_5[1] = com.a.b.d.tr.b;
        com.a.b.d.tr.c = v0_5;
        return;
    }

    private tr(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic tr(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.d.tr valueOf(String p1)
    {
        return ((com.a.b.d.tr) Enum.valueOf(com.a.b.d.tr, p1));
    }

    public static com.a.b.d.tr[] values()
    {
        return ((com.a.b.d.tr[]) com.a.b.d.tr.c.clone());
    }
}
