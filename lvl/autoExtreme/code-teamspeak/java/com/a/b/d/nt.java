package com.a.b.d;
final class nt implements java.util.Iterator {
    java.util.Iterator a;
    java.util.Iterator b;
    final synthetic java.util.Iterator c;

    nt(java.util.Iterator p2)
    {
        this.c = p2;
        this.a = com.a.b.d.nj.a();
        return;
    }

    public final boolean hasNext()
    {
        while(true) {
            java.util.Iterator v0_3 = ((java.util.Iterator) com.a.b.b.cn.a(this.a)).hasNext();
            if ((v0_3 != null) || (!this.c.hasNext())) {
                break;
            }
            this.a = ((java.util.Iterator) this.c.next());
        }
        return v0_3;
    }

    public final Object next()
    {
        if (this.hasNext()) {
            this.b = this.a;
            return this.a.next();
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final void remove()
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        this.b.remove();
        this.b = 0;
        return;
    }
}
