package com.a.b.d;
final class aav extends com.a.b.d.jl {
    final transient Object a;

    aav(Object p2)
    {
        this.a = com.a.b.b.cn.a(p2);
        return;
    }

    final int a(Object[] p2, int p3)
    {
        p2[p3] = this.a;
        return (p3 + 1);
    }

    public final com.a.b.d.jl a(int p2, int p3)
    {
        com.a.b.b.cn.a(p2, p3, 1);
        if (p2 == p3) {
            this = com.a.b.d.jl.c;
        }
        return this;
    }

    public final com.a.b.d.agi c()
    {
        return com.a.b.d.nj.a(this.a);
    }

    public final boolean contains(Object p2)
    {
        return this.a.equals(p2);
    }

    public final com.a.b.d.jl e()
    {
        return this;
    }

    public final boolean equals(Object p5)
    {
        int v0 = 1;
        if (p5 != this) {
            if (!(p5 instanceof java.util.List)) {
                v0 = 0;
            } else {
                if ((((java.util.List) p5).size() != 1) || (!this.a.equals(((java.util.List) p5).get(0)))) {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public final Object get(int p2)
    {
        com.a.b.b.cn.a(p2, 1);
        return this.a;
    }

    final boolean h_()
    {
        return 0;
    }

    public final int hashCode()
    {
        return (this.a.hashCode() + 31);
    }

    public final int indexOf(Object p2)
    {
        int v0_2;
        if (!this.a.equals(p2)) {
            v0_2 = -1;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final boolean isEmpty()
    {
        return 0;
    }

    public final synthetic java.util.Iterator iterator()
    {
        return com.a.b.d.nj.a(this.a);
    }

    public final int lastIndexOf(Object p2)
    {
        return this.indexOf(p2);
    }

    public final int size()
    {
        return 1;
    }

    public final synthetic java.util.List subList(int p2, int p3)
    {
        return this.a(p2, p3);
    }

    public final String toString()
    {
        String v0_1 = this.a.toString();
        return new StringBuilder((v0_1.length() + 2)).append(91).append(v0_1).append(93).toString();
    }
}
