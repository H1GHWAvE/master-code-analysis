package com.a.b.d;
final class mt implements java.util.Iterator {
    boolean a;
    final synthetic java.util.Iterator b;
    final synthetic com.a.b.d.ms c;

    mt(com.a.b.d.ms p2, java.util.Iterator p3)
    {
        this.c = p2;
        this.b = p3;
        this.a = 1;
        return;
    }

    public final boolean hasNext()
    {
        return this.b.hasNext();
    }

    public final Object next()
    {
        Object v0_1 = this.b.next();
        this.a = 0;
        return v0_1;
    }

    public final void remove()
    {
        java.util.Iterator v0_1;
        if (this.a) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        this.b.remove();
        return;
    }
}
