package com.a.b.d;
final class af extends com.a.b.d.ah implements java.util.NavigableSet {
    final synthetic com.a.b.d.n a;

    af(com.a.b.d.n p1, Object p2, java.util.NavigableSet p3, com.a.b.d.ab p4)
    {
        this.a = p1;
        this(p1, p2, p3, p4);
        return;
    }

    private java.util.NavigableSet a(java.util.NavigableSet p5)
    {
        com.a.b.d.n v1 = this.a;
        Object v2 = this.b;
        if (this.d != null) {
            this = this.d;
        }
        return new com.a.b.d.af(v1, v2, p5, this);
    }

    private java.util.NavigableSet e()
    {
        return ((java.util.NavigableSet) super.d());
    }

    public final Object ceiling(Object p2)
    {
        return ((java.util.NavigableSet) super.d()).ceiling(p2);
    }

    final bridge synthetic java.util.SortedSet d()
    {
        return ((java.util.NavigableSet) super.d());
    }

    public final java.util.Iterator descendingIterator()
    {
        return new com.a.b.d.ac(this, ((java.util.NavigableSet) super.d()).descendingIterator());
    }

    public final java.util.NavigableSet descendingSet()
    {
        return this.a(((java.util.NavigableSet) super.d()).descendingSet());
    }

    public final Object floor(Object p2)
    {
        return ((java.util.NavigableSet) super.d()).floor(p2);
    }

    public final java.util.NavigableSet headSet(Object p2, boolean p3)
    {
        return this.a(((java.util.NavigableSet) super.d()).headSet(p2, p3));
    }

    public final Object higher(Object p2)
    {
        return ((java.util.NavigableSet) super.d()).higher(p2);
    }

    public final Object lower(Object p2)
    {
        return ((java.util.NavigableSet) super.d()).lower(p2);
    }

    public final Object pollFirst()
    {
        return com.a.b.d.nj.h(this.iterator());
    }

    public final Object pollLast()
    {
        return com.a.b.d.nj.h(this.descendingIterator());
    }

    public final java.util.NavigableSet subSet(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.a(((java.util.NavigableSet) super.d()).subSet(p2, p3, p4, p5));
    }

    public final java.util.NavigableSet tailSet(Object p2, boolean p3)
    {
        return this.a(((java.util.NavigableSet) super.d()).tailSet(p2, p3));
    }
}
