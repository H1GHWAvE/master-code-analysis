package com.a.b.d;
public abstract class jl extends com.a.b.d.iz implements java.util.List, java.util.RandomAccess {
    static final com.a.b.d.jl c;

    static jl()
    {
        com.a.b.d.jl.c = new com.a.b.d.ze(com.a.b.d.yc.a);
        return;
    }

    jl()
    {
        return;
    }

    public static com.a.b.d.jl a(Iterable p1)
    {
        com.a.b.d.jl v0_2;
        com.a.b.b.cn.a(p1);
        if (!(p1 instanceof java.util.Collection)) {
            v0_2 = com.a.b.d.jl.a(p1.iterator());
        } else {
            v0_2 = com.a.b.d.jl.a(((java.util.Collection) p1));
        }
        return v0_2;
    }

    public static com.a.b.d.jl a(Object p1)
    {
        return new com.a.b.d.aav(p1);
    }

    public static com.a.b.d.jl a(Object p2, Object p3)
    {
        com.a.b.d.jl v0_1 = new Object[2];
        v0_1[0] = p2;
        v0_1[1] = p3;
        com.a.b.d.jl v0_2 = com.a.b.d.yc.a(v0_1);
        return com.a.b.d.jl.b(v0_2, v0_2.length);
    }

    public static com.a.b.d.jl a(Object p2, Object p3, Object p4)
    {
        com.a.b.d.jl v0_1 = new Object[3];
        v0_1[0] = p2;
        v0_1[1] = p3;
        v0_1[2] = p4;
        com.a.b.d.jl v0_2 = com.a.b.d.yc.a(v0_1);
        return com.a.b.d.jl.b(v0_2, v0_2.length);
    }

    public static com.a.b.d.jl a(Object p2, Object p3, Object p4, Object p5)
    {
        com.a.b.d.jl v0_1 = new Object[4];
        v0_1[0] = p2;
        v0_1[1] = p3;
        v0_1[2] = p4;
        v0_1[3] = p5;
        com.a.b.d.jl v0_2 = com.a.b.d.yc.a(v0_1);
        return com.a.b.d.jl.b(v0_2, v0_2.length);
    }

    private static com.a.b.d.jl a(Object p2, Object p3, Object p4, Object p5, Object p6)
    {
        com.a.b.d.jl v0_1 = new Object[5];
        v0_1[0] = p2;
        v0_1[1] = p3;
        v0_1[2] = p4;
        v0_1[3] = p5;
        v0_1[4] = p6;
        com.a.b.d.jl v0_2 = com.a.b.d.yc.a(v0_1);
        return com.a.b.d.jl.b(v0_2, v0_2.length);
    }

    private static com.a.b.d.jl a(Object p2, Object p3, Object p4, Object p5, Object p6, Object p7)
    {
        com.a.b.d.jl v0_1 = new Object[6];
        v0_1[0] = p2;
        v0_1[1] = p3;
        v0_1[2] = p4;
        v0_1[3] = p5;
        v0_1[4] = p6;
        v0_1[5] = p7;
        com.a.b.d.jl v0_2 = com.a.b.d.yc.a(v0_1);
        return com.a.b.d.jl.b(v0_2, v0_2.length);
    }

    private static com.a.b.d.jl a(Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8)
    {
        com.a.b.d.jl v0_1 = new Object[7];
        v0_1[0] = p2;
        v0_1[1] = p3;
        v0_1[2] = p4;
        v0_1[3] = p5;
        v0_1[4] = p6;
        v0_1[5] = p7;
        v0_1[6] = p8;
        com.a.b.d.jl v0_2 = com.a.b.d.yc.a(v0_1);
        return com.a.b.d.jl.b(v0_2, v0_2.length);
    }

    private static com.a.b.d.jl a(Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8, Object p9)
    {
        com.a.b.d.jl v0_1 = new Object[8];
        v0_1[0] = p2;
        v0_1[1] = p3;
        v0_1[2] = p4;
        v0_1[3] = p5;
        v0_1[4] = p6;
        v0_1[5] = p7;
        v0_1[6] = p8;
        v0_1[7] = p9;
        com.a.b.d.jl v0_2 = com.a.b.d.yc.a(v0_1);
        return com.a.b.d.jl.b(v0_2, v0_2.length);
    }

    private static com.a.b.d.jl a(Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8, Object p9, Object p10)
    {
        com.a.b.d.jl v0_1 = new Object[9];
        v0_1[0] = p2;
        v0_1[1] = p3;
        v0_1[2] = p4;
        v0_1[3] = p5;
        v0_1[4] = p6;
        v0_1[5] = p7;
        v0_1[6] = p8;
        v0_1[7] = p9;
        v0_1[8] = p10;
        com.a.b.d.jl v0_2 = com.a.b.d.yc.a(v0_1);
        return com.a.b.d.jl.b(v0_2, v0_2.length);
    }

    private static com.a.b.d.jl a(Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8, Object p9, Object p10, Object p11)
    {
        com.a.b.d.jl v0_1 = new Object[10];
        v0_1[0] = p2;
        v0_1[1] = p3;
        v0_1[2] = p4;
        v0_1[3] = p5;
        v0_1[4] = p6;
        v0_1[5] = p7;
        v0_1[6] = p8;
        v0_1[7] = p9;
        v0_1[8] = p10;
        v0_1[9] = p11;
        com.a.b.d.jl v0_2 = com.a.b.d.yc.a(v0_1);
        return com.a.b.d.jl.b(v0_2, v0_2.length);
    }

    private static com.a.b.d.jl a(Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8, Object p9, Object p10, Object p11, Object p12)
    {
        com.a.b.d.jl v0_1 = new Object[11];
        v0_1[0] = p2;
        v0_1[1] = p3;
        v0_1[2] = p4;
        v0_1[3] = p5;
        v0_1[4] = p6;
        v0_1[5] = p7;
        v0_1[6] = p8;
        v0_1[7] = p9;
        v0_1[8] = p10;
        v0_1[9] = p11;
        v0_1[10] = p12;
        com.a.b.d.jl v0_2 = com.a.b.d.yc.a(v0_1);
        return com.a.b.d.jl.b(v0_2, v0_2.length);
    }

    private static varargs com.a.b.d.jl a(Object p5, Object p6, Object p7, Object p8, Object p9, Object p10, Object p11, Object p12, Object p13, Object p14, Object p15, Object p16, Object[] p17)
    {
        com.a.b.d.jl v1_2 = new Object[(p17.length + 12)];
        v1_2[0] = p5;
        v1_2[1] = p6;
        v1_2[2] = p7;
        v1_2[3] = p8;
        v1_2[4] = p9;
        v1_2[5] = p10;
        v1_2[6] = p11;
        v1_2[7] = p12;
        v1_2[8] = p13;
        v1_2[9] = p14;
        v1_2[10] = p15;
        v1_2[11] = p16;
        System.arraycopy(p17, 0, v1_2, 12, p17.length);
        com.a.b.d.jl v1_3 = com.a.b.d.yc.a(v1_2);
        return com.a.b.d.jl.b(v1_3, v1_3.length);
    }

    public static com.a.b.d.jl a(java.util.Collection p2)
    {
        com.a.b.d.jl v0_3;
        if (!(p2 instanceof com.a.b.d.iz)) {
            com.a.b.d.jl v0_2 = com.a.b.d.yc.a(p2.toArray());
            v0_3 = com.a.b.d.jl.b(v0_2, v0_2.length);
        } else {
            v0_3 = ((com.a.b.d.iz) p2).f();
            if (v0_3.h_()) {
                com.a.b.d.jl v0_4 = v0_3.toArray();
                v0_3 = com.a.b.d.jl.b(v0_4, v0_4.length);
            }
        }
        return v0_3;
    }

    public static com.a.b.d.jl a(java.util.Iterator p2)
    {
        com.a.b.d.jl v0_4;
        if (p2.hasNext()) {
            com.a.b.d.jl v0_1 = p2.next();
            if (p2.hasNext()) {
                v0_4 = new com.a.b.d.jn().c(v0_1).b(p2).b();
            } else {
                v0_4 = com.a.b.d.jl.a(v0_1);
            }
        } else {
            v0_4 = com.a.b.d.jl.c;
        }
        return v0_4;
    }

    public static com.a.b.d.jl a(Object[] p2)
    {
        com.a.b.d.aav v0_2;
        switch (p2.length) {
            case 0:
                v0_2 = com.a.b.d.jl.c;
                break;
            case 1:
                v0_2 = new com.a.b.d.aav(p2[0]);
                break;
            default:
                v0_2 = new com.a.b.d.ze(com.a.b.d.yc.a(((Object[]) p2.clone())));
        }
        return v0_2;
    }

    private com.a.b.d.agj b()
    {
        return this.a(0);
    }

    static com.a.b.d.jl b(Object[] p1)
    {
        return com.a.b.d.jl.b(p1, p1.length);
    }

    static com.a.b.d.jl b(Object[] p2, int p3)
    {
        com.a.b.d.ze v0_1;
        switch (p3) {
            case 0:
                v0_1 = com.a.b.d.jl.c;
                break;
            case 1:
                v0_1 = new com.a.b.d.aav(p2[0]);
                break;
            default:
                if (p3 < p2.length) {
                    p2 = com.a.b.d.yc.b(p2, p3);
                }
                v0_1 = new com.a.b.d.ze(p2);
        }
        return v0_1;
    }

    private static varargs com.a.b.d.jl c(Object[] p2)
    {
        com.a.b.d.jl v0_0 = com.a.b.d.yc.a(p2);
        return com.a.b.d.jl.b(v0_0, v0_0.length);
    }

    public static com.a.b.d.jl d()
    {
        return com.a.b.d.jl.c;
    }

    public static com.a.b.d.jn h()
    {
        return new com.a.b.d.jn();
    }

    private static void i()
    {
        throw new java.io.InvalidObjectException("Use SerializedForm");
    }

    int a(Object[] p5, int p6)
    {
        int v1 = this.size();
        int v0_0 = 0;
        while (v0_0 < v1) {
            p5[(p6 + v0_0)] = this.get(v0_0);
            v0_0++;
        }
        return (p6 + v1);
    }

    public com.a.b.d.agj a(int p3)
    {
        return new com.a.b.d.jm(this, this.size(), p3);
    }

    public com.a.b.d.jl a(int p2, int p3)
    {
        com.a.b.d.jl v0_3;
        com.a.b.b.cn.a(p2, p3, this.size());
        switch ((p3 - p2)) {
            case 0:
                v0_3 = com.a.b.d.jl.c;
                break;
            case 1:
                v0_3 = com.a.b.d.jl.a(this.get(p2));
                break;
            default:
                v0_3 = this.b(p2, p3);
        }
        return v0_3;
    }

    public final void add(int p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean addAll(int p2, java.util.Collection p3)
    {
        throw new UnsupportedOperationException();
    }

    com.a.b.d.jl b(int p3, int p4)
    {
        return new com.a.b.d.jq(this, p3, (p4 - p3));
    }

    public com.a.b.d.agi c()
    {
        return this.a(0);
    }

    public boolean contains(Object p2)
    {
        int v0_1;
        if (this.indexOf(p2) < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public com.a.b.d.jl e()
    {
        return new com.a.b.d.jo(this);
    }

    public boolean equals(Object p2)
    {
        return com.a.b.d.ov.a(this, p2);
    }

    public final com.a.b.d.jl f()
    {
        return this;
    }

    Object g()
    {
        return new com.a.b.d.jp(this.toArray());
    }

    public int hashCode()
    {
        int v1_0 = 1;
        int v2 = this.size();
        int v0 = 0;
        while (v0 < v2) {
            v1_0 = ((((v1_0 * 31) + this.get(v0).hashCode()) ^ -1) ^ -1);
            v0++;
        }
        return v1_0;
    }

    public int indexOf(Object p2)
    {
        int v0;
        if (p2 != null) {
            v0 = com.a.b.d.ov.b(this, p2);
        } else {
            v0 = -1;
        }
        return v0;
    }

    public synthetic java.util.Iterator iterator()
    {
        return this.c();
    }

    public int lastIndexOf(Object p2)
    {
        int v0;
        if (p2 != null) {
            v0 = com.a.b.d.ov.c(this, p2);
        } else {
            v0 = -1;
        }
        return v0;
    }

    public synthetic java.util.ListIterator listIterator()
    {
        return this.a(0);
    }

    public synthetic java.util.ListIterator listIterator(int p2)
    {
        return this.a(p2);
    }

    public final Object remove(int p2)
    {
        throw new UnsupportedOperationException();
    }

    public final Object set(int p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public synthetic java.util.List subList(int p2, int p3)
    {
        return this.a(p2, p3);
    }
}
