package com.a.b.d;
final class wj extends com.a.b.d.n {
    private static final long b;
    transient com.a.b.b.dz a;

    wj(java.util.Map p2, com.a.b.b.dz p3)
    {
        this(p2);
        this.a = ((com.a.b.b.dz) com.a.b.b.cn.a(p3));
        return;
    }

    private void a(java.io.ObjectInputStream p2)
    {
        p2.defaultReadObject();
        this.a = ((com.a.b.b.dz) p2.readObject());
        this.a(((java.util.Map) p2.readObject()));
        return;
    }

    private void a(java.io.ObjectOutputStream p2)
    {
        p2.defaultWriteObject();
        p2.writeObject(this.a);
        p2.writeObject(this.e());
        return;
    }

    protected final java.util.Collection c()
    {
        return ((java.util.Collection) this.a.a());
    }
}
