package com.a.b.d;
public final class aer extends com.a.b.d.bc implements java.io.Serializable {
    private static final long e = 1;
    private final transient com.a.b.d.afa b;
    private final transient com.a.b.d.hs c;
    private final transient com.a.b.d.aez d;

    private aer(com.a.b.d.afa p2, com.a.b.d.hs p3, com.a.b.d.aez p4)
    {
        this(p3.a);
        this.b = p2;
        this.c = p3;
        this.d = p4;
        return;
    }

    private aer(java.util.Comparator p4)
    {
        this(p4);
        this.c = com.a.b.d.hs.a(p4);
        this.d = new com.a.b.d.aez(0, 1);
        com.a.b.d.aer.b(this.d, this.d);
        this.b = new com.a.b.d.afa(0);
        return;
    }

    static int a(com.a.b.d.aez p1)
    {
        int v0;
        if (p1 != null) {
            v0 = p1.c;
        } else {
            v0 = 0;
        }
        return v0;
    }

    private long a(com.a.b.d.aew p7)
    {
        long v0_2 = ((com.a.b.d.aez) this.b.a);
        long v2 = p7.b(v0_2);
        if (this.c.b) {
            v2 -= this.a(p7, v0_2);
        }
        if (this.c.e) {
            v2 -= this.b(p7, v0_2);
        }
        return v2;
    }

    private long a(com.a.b.d.aew p5, com.a.b.d.aez p6)
    {
        while (p6 != null) {
            long v0_1 = this.comparator().compare(this.c.c, p6.a);
            if (v0_1 >= 0) {
                if (v0_1 != 0) {
                    long v0_5 = ((p5.b(p6.e) + ((long) p5.a(p6))) + this.a(p5, p6.f));
                } else {
                    switch (com.a.b.d.aev.a[this.c.d.ordinal()]) {
                        case 1:
                            v0_5 = (((long) p5.a(p6)) + p5.b(p6.e));
                            break;
                        case 2:
                            v0_5 = p5.b(p6.e);
                            break;
                        default:
                            throw new AssertionError();
                    }
                }
            } else {
                p6 = p6.e;
            }
            return v0_5;
        }
        v0_5 = 0;
        return v0_5;
    }

    private static com.a.b.d.aer a(Iterable p2)
    {
        com.a.b.d.aer v0_1 = new com.a.b.d.aer(com.a.b.d.yd.d());
        com.a.b.d.mq.a(v0_1, p2);
        return v0_1;
    }

    public static com.a.b.d.aer a(java.util.Comparator p2)
    {
        com.a.b.d.aer v0_1;
        if (p2 != null) {
            v0_1 = new com.a.b.d.aer(p2);
        } else {
            v0_1 = new com.a.b.d.aer(com.a.b.d.yd.d());
        }
        return v0_1;
    }

    static synthetic com.a.b.d.aez a(com.a.b.d.aer p5)
    {
        com.a.b.d.aez v0_6;
        if (((com.a.b.d.aez) p5.b.a) != null) {
            if (!p5.c.b) {
                v0_6 = p5.d.h;
            } else {
                boolean v2_0 = p5.c.c;
                v0_6 = ((com.a.b.d.aez) p5.b.a).a(p5.comparator(), v2_0);
                if (v0_6 != null) {
                    if ((p5.c.d == com.a.b.d.ce.a) && (p5.comparator().compare(v2_0, v0_6.a) == 0)) {
                        v0_6 = v0_6.h;
                    }
                } else {
                    v0_6 = 0;
                    return v0_6;
                }
            }
            if ((v0_6 == p5.d) || (!p5.c.c(v0_6.a))) {
                v0_6 = 0;
            }
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    static synthetic com.a.b.d.xd a(com.a.b.d.aer p1, com.a.b.d.aez p2)
    {
        return new com.a.b.d.aes(p1, p2);
    }

    static synthetic void a(com.a.b.d.aez p0, com.a.b.d.aez p1)
    {
        com.a.b.d.aer.b(p0, p1);
        return;
    }

    static synthetic void a(com.a.b.d.aez p0, com.a.b.d.aez p1, com.a.b.d.aez p2)
    {
        com.a.b.d.aer.b(p0, p1, p2);
        return;
    }

    private void a(java.io.ObjectInputStream p4)
    {
        p4.defaultReadObject();
        com.a.b.d.aez v0_1 = ((java.util.Comparator) p4.readObject());
        com.a.b.d.zz.a(com.a.b.d.bc, "comparator").a(this, v0_1);
        com.a.b.d.zz.a(com.a.b.d.aer, "range").a(this, com.a.b.d.hs.a(v0_1));
        com.a.b.d.zz.a(com.a.b.d.aer, "rootReference").a(this, new com.a.b.d.afa(0));
        com.a.b.d.aez v0_6 = new com.a.b.d.aez(0, 1);
        com.a.b.d.zz.a(com.a.b.d.aer, "header").a(this, v0_6);
        com.a.b.d.aer.b(v0_6, v0_6);
        com.a.b.d.zz.a(this, p4);
        return;
    }

    private void a(java.io.ObjectOutputStream p2)
    {
        p2.defaultWriteObject();
        p2.writeObject(super.e_().comparator());
        com.a.b.d.zz.a(this, p2);
        return;
    }

    private long b(com.a.b.d.aew p5, com.a.b.d.aez p6)
    {
        while (p6 != null) {
            long v0_1 = this.comparator().compare(this.c.f, p6.a);
            if (v0_1 <= 0) {
                if (v0_1 != 0) {
                    long v0_5 = ((p5.b(p6.f) + ((long) p5.a(p6))) + this.b(p5, p6.e));
                } else {
                    switch (com.a.b.d.aev.a[this.c.g.ordinal()]) {
                        case 1:
                            v0_5 = (((long) p5.a(p6)) + p5.b(p6.f));
                            break;
                        case 2:
                            v0_5 = p5.b(p6.f);
                            break;
                        default:
                            throw new AssertionError();
                    }
                }
            } else {
                p6 = p6.f;
            }
            return v0_5;
        }
        v0_5 = 0;
        return v0_5;
    }

    static synthetic com.a.b.d.hs b(com.a.b.d.aer p1)
    {
        return p1.c;
    }

    private com.a.b.d.xd b(com.a.b.d.aez p2)
    {
        return new com.a.b.d.aes(this, p2);
    }

    private static void b(com.a.b.d.aez p0, com.a.b.d.aez p1)
    {
        p0.h = p1;
        p1.g = p0;
        return;
    }

    private static void b(com.a.b.d.aez p0, com.a.b.d.aez p1, com.a.b.d.aez p2)
    {
        com.a.b.d.aer.b(p0, p1);
        com.a.b.d.aer.b(p1, p2);
        return;
    }

    static synthetic com.a.b.d.aez c(com.a.b.d.aer p1)
    {
        return p1.d;
    }

    static synthetic com.a.b.d.aez d(com.a.b.d.aer p5)
    {
        com.a.b.d.aez v0_6;
        if (((com.a.b.d.aez) p5.b.a) != null) {
            if (!p5.c.e) {
                v0_6 = p5.d.g;
            } else {
                boolean v2_0 = p5.c.f;
                v0_6 = ((com.a.b.d.aez) p5.b.a).b(p5.comparator(), v2_0);
                if (v0_6 != null) {
                    if ((p5.c.g == com.a.b.d.ce.a) && (p5.comparator().compare(v2_0, v0_6.a) == 0)) {
                        v0_6 = v0_6.g;
                    }
                } else {
                    v0_6 = 0;
                    return v0_6;
                }
            }
            if ((v0_6 == p5.d) || (!p5.c.c(v0_6.a))) {
                v0_6 = 0;
            }
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    private static com.a.b.d.aer o()
    {
        return new com.a.b.d.aer(com.a.b.d.yd.d());
    }

    private com.a.b.d.aez p()
    {
        com.a.b.d.aez v0_6;
        if (((com.a.b.d.aez) this.b.a) != null) {
            if (!this.c.b) {
                v0_6 = this.d.h;
            } else {
                boolean v2_0 = this.c.c;
                v0_6 = ((com.a.b.d.aez) this.b.a).a(this.comparator(), v2_0);
                if (v0_6 != null) {
                    if ((this.c.d == com.a.b.d.ce.a) && (this.comparator().compare(v2_0, v0_6.a) == 0)) {
                        v0_6 = v0_6.h;
                    }
                } else {
                    v0_6 = 0;
                    return v0_6;
                }
            }
            if ((v0_6 == this.d) || (!this.c.c(v0_6.a))) {
                v0_6 = 0;
            }
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    private com.a.b.d.aez q()
    {
        com.a.b.d.aez v0_6;
        if (((com.a.b.d.aez) this.b.a) != null) {
            if (!this.c.e) {
                v0_6 = this.d.g;
            } else {
                boolean v2_0 = this.c.f;
                v0_6 = ((com.a.b.d.aez) this.b.a).b(this.comparator(), v2_0);
                if (v0_6 != null) {
                    if ((this.c.g == com.a.b.d.ce.a) && (this.comparator().compare(v2_0, v0_6.a) == 0)) {
                        v0_6 = v0_6.g;
                    }
                } else {
                    v0_6 = 0;
                    return v0_6;
                }
            }
            if ((v0_6 == this.d) || (!this.c.c(v0_6.a))) {
                v0_6 = 0;
            }
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    public final int a(Object p5)
    {
        try {
            com.a.b.d.aez v0_3;
            com.a.b.d.aez v0_2 = ((com.a.b.d.aez) this.b.a);
        } catch (com.a.b.d.aez v0) {
            v0_3 = 0;
            return v0_3;
        } catch (com.a.b.d.aez v0) {
            v0_3 = 0;
            return v0_3;
        }
        if ((this.c.c(p5)) && (v0_2 != null)) {
            java.util.Comparator v2_2 = this.comparator();
            while(true) {
                com.a.b.d.aez v3_1 = v2_2.compare(p5, v0_2.a);
                if (v3_1 >= null) {
                    if (v3_1 <= null) {
                        break;
                    }
                    if (v0_2.f != null) {
                        v0_2 = v0_2.f;
                    } else {
                        v0_3 = 0;
                        return v0_3;
                    }
                } else {
                    if (v0_2.e != null) {
                        v0_2 = v0_2.e;
                    } else {
                        v0_3 = 0;
                        return v0_3;
                    }
                }
            }
            v0_3 = v0_2.b;
            return v0_3;
        } else {
            v0_3 = 0;
            return v0_3;
        }
    }

    public final int a(Object p6, int p7)
    {
        int v0_6;
        com.a.b.d.cl.a(p7, "occurrences");
        if (p7 != 0) {
            com.a.b.b.cn.a(this.c.c(p6));
            int v0_5 = ((com.a.b.d.aez) this.b.a);
            if (v0_5 != 0) {
                int[] v2_1 = new int[1];
                this.b.a(v0_5, v0_5.a(this.comparator(), p6, p7, v2_1));
                v0_6 = v2_1[0];
            } else {
                this.comparator().compare(p6, p6);
                int[] v2_4 = new com.a.b.d.aez(p6, p7);
                com.a.b.d.aer.b(this.d, v2_4, this.d);
                this.b.a(v0_5, v2_4);
                v0_6 = 0;
            }
        } else {
            v0_6 = this.a(p6);
        }
        return v0_6;
    }

    public final bridge synthetic com.a.b.d.abn a(Object p2, com.a.b.d.ce p3, Object p4, com.a.b.d.ce p5)
    {
        return super.a(p2, p3, p4, p5);
    }

    public final bridge synthetic java.util.Set a()
    {
        return super.a();
    }

    public final boolean a(Object p9, int p10, int p11)
    {
        int v0_8;
        com.a.b.d.cl.a(p11, "newCount");
        com.a.b.d.cl.a(p10, "oldCount");
        com.a.b.b.cn.a(this.c.c(p9));
        int v0_6 = ((com.a.b.d.aez) this.b.a);
        if (v0_6 != 0) {
            int[] v5 = new int[1];
            this.b.a(v0_6, v0_6.a(this.comparator(), p9, p10, p11, v5));
            if (v5[0] != p10) {
                v0_8 = 0;
            } else {
                v0_8 = 1;
            }
        } else {
            if (p10 != 0) {
                v0_8 = 0;
            } else {
                if (p11 > 0) {
                    this.a(p9, p11);
                }
                v0_8 = 1;
            }
        }
        return v0_8;
    }

    public final bridge synthetic boolean add(Object p2)
    {
        return super.add(p2);
    }

    public final bridge synthetic boolean addAll(java.util.Collection p2)
    {
        return super.addAll(p2);
    }

    public final int b(Object p6, int p7)
    {
        int v0_4;
        com.a.b.d.cl.a(p7, "occurrences");
        if (p7 != 0) {
            int v0_3 = ((com.a.b.d.aez) this.b.a);
            int[] v2_1 = new int[1];
            try {
                if ((this.c.c(p6)) && (v0_3 != 0)) {
                    this.b.a(v0_3, v0_3.b(this.comparator(), p6, p7, v2_1));
                    v0_4 = v2_1[0];
                } else {
                    v0_4 = 0;
                }
            } catch (int v0) {
                v0_4 = 0;
            } catch (int v0) {
                v0_4 = 0;
            }
        } else {
            v0_4 = this.a(p6);
        }
        return v0_4;
    }

    final java.util.Iterator b()
    {
        return new com.a.b.d.aet(this);
    }

    final int c()
    {
        return com.a.b.l.q.b(this.a(com.a.b.d.aew.b));
    }

    public final int c(Object p6, int p7)
    {
        int v2 = 0;
        com.a.b.d.cl.a(p7, "count");
        if (this.c.c(p6)) {
            int v0_5 = ((com.a.b.d.aez) this.b.a);
            if (v0_5 != 0) {
                int[] v1_1 = new int[1];
                this.b.a(v0_5, v0_5.c(this.comparator(), p6, p7, v1_1));
                v2 = v1_1[0];
            } else {
                if (p7 > 0) {
                    this.a(p6, p7);
                }
            }
        } else {
            int v0_6;
            if (p7 != 0) {
                v0_6 = 0;
            } else {
                v0_6 = 1;
            }
            com.a.b.b.cn.a(v0_6);
        }
        return v2;
    }

    public final com.a.b.d.abn c(Object p12, com.a.b.d.ce p13)
    {
        return new com.a.b.d.aer(this.b, this.c.a(new com.a.b.d.hs(this.comparator(), 1, p12, p13, 0, 0, com.a.b.d.ce.a)), this.d);
    }

    public final bridge synthetic void clear()
    {
        super.clear();
        return;
    }

    public final bridge synthetic java.util.Comparator comparator()
    {
        return super.comparator();
    }

    public final bridge synthetic boolean contains(Object p2)
    {
        return super.contains(p2);
    }

    public final com.a.b.d.abn d(Object p12, com.a.b.d.ce p13)
    {
        return new com.a.b.d.aer(this.b, this.c.a(new com.a.b.d.hs(this.comparator(), 0, 0, com.a.b.d.ce.a, 1, p12, p13)), this.d);
    }

    public final bridge synthetic java.util.NavigableSet e_()
    {
        return super.e_();
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final bridge synthetic com.a.b.d.xd h()
    {
        return super.h();
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final bridge synthetic com.a.b.d.xd i()
    {
        return super.i();
    }

    public final bridge synthetic boolean isEmpty()
    {
        return super.isEmpty();
    }

    public final bridge synthetic java.util.Iterator iterator()
    {
        return super.iterator();
    }

    public final bridge synthetic com.a.b.d.xd j()
    {
        return super.j();
    }

    public final bridge synthetic com.a.b.d.xd k()
    {
        return super.k();
    }

    final java.util.Iterator l()
    {
        return new com.a.b.d.aeu(this);
    }

    public final bridge synthetic com.a.b.d.abn m()
    {
        return super.m();
    }

    public final bridge synthetic boolean remove(Object p2)
    {
        return super.remove(p2);
    }

    public final bridge synthetic boolean removeAll(java.util.Collection p2)
    {
        return super.removeAll(p2);
    }

    public final bridge synthetic boolean retainAll(java.util.Collection p2)
    {
        return super.retainAll(p2);
    }

    public final int size()
    {
        return com.a.b.l.q.b(this.a(com.a.b.d.aew.a));
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }
}
