package com.a.b.d;
final enum class qi extends java.lang.Enum implements com.a.b.d.pn {
    public static final enum com.a.b.d.qi a;
    private static final synthetic com.a.b.d.qi[] b;

    static qi()
    {
        com.a.b.d.qi.a = new com.a.b.d.qi("INSTANCE");
        com.a.b.d.qi[] v0_3 = new com.a.b.d.qi[1];
        v0_3[0] = com.a.b.d.qi.a;
        com.a.b.d.qi.b = v0_3;
        return;
    }

    private qi(String p2)
    {
        this(p2, 0);
        return;
    }

    public static com.a.b.d.qi valueOf(String p1)
    {
        return ((com.a.b.d.qi) Enum.valueOf(com.a.b.d.qi, p1));
    }

    public static com.a.b.d.qi[] values()
    {
        return ((com.a.b.d.qi[]) com.a.b.d.qi.b.clone());
    }

    public final void a(Object p1, Object p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return;
    }

    public final String toString()
    {
        return "Not null";
    }
}
