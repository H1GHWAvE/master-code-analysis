package com.a.b.d;
final enum class abi extends com.a.b.d.abg {

    abi(String p3)
    {
        this(p3, 1, 0);
        return;
    }

    final int a(java.util.Comparator p4, Object p5, java.util.List p6, int p7)
    {
        int v0_1 = (p6.size() - 1);
        while (p7 < v0_1) {
            int v1_2 = (((p7 + v0_1) + 1) >> 1);
            if (p4.compare(p6.get(v1_2), p5) <= 0) {
                p7 = v1_2;
            } else {
                v0_1 = (v1_2 - 1);
            }
        }
        return p7;
    }
}
