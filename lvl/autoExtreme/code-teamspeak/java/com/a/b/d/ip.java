package com.a.b.d;
public final class ip extends com.a.b.d.ai {
    private static final long b;

    private ip()
    {
        this(new java.util.HashMap());
        return;
    }

    private ip(int p2)
    {
        this(com.a.b.d.sz.a(p2));
        return;
    }

    private static com.a.b.d.ip a(int p1)
    {
        return new com.a.b.d.ip(p1);
    }

    public static com.a.b.d.ip a(Iterable p2)
    {
        com.a.b.d.ip v1_1 = new com.a.b.d.ip(com.a.b.d.xe.a(p2));
        com.a.b.d.mq.a(v1_1, p2);
        return v1_1;
    }

    private void a(java.io.ObjectInputStream p3)
    {
        p3.defaultReadObject();
        int v0 = p3.readInt();
        this.a = com.a.b.d.sz.a(v0);
        com.a.b.d.zz.a(this, p3, v0);
        return;
    }

    private void a(java.io.ObjectOutputStream p1)
    {
        p1.defaultWriteObject();
        com.a.b.d.zz.a(this, p1);
        return;
    }

    private static com.a.b.d.ip g()
    {
        return new com.a.b.d.ip();
    }

    public final bridge synthetic int a(Object p2)
    {
        return super.a(p2);
    }

    public final bridge synthetic int a(Object p2, int p3)
    {
        return super.a(p2, p3);
    }

    public final bridge synthetic java.util.Set a()
    {
        return super.a();
    }

    public final bridge synthetic boolean a(Object p2, int p3, int p4)
    {
        return super.a(p2, p3, p4);
    }

    public final bridge synthetic boolean add(Object p2)
    {
        return super.add(p2);
    }

    public final bridge synthetic boolean addAll(java.util.Collection p2)
    {
        return super.addAll(p2);
    }

    public final bridge synthetic int b(Object p2, int p3)
    {
        return super.b(p2, p3);
    }

    public final bridge synthetic int c(Object p2, int p3)
    {
        return super.c(p2, p3);
    }

    public final bridge synthetic void clear()
    {
        super.clear();
        return;
    }

    public final bridge synthetic boolean contains(Object p2)
    {
        return super.contains(p2);
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final bridge synthetic boolean isEmpty()
    {
        return super.isEmpty();
    }

    public final bridge synthetic java.util.Iterator iterator()
    {
        return super.iterator();
    }

    public final bridge synthetic java.util.Set n_()
    {
        return super.n_();
    }

    public final bridge synthetic boolean remove(Object p2)
    {
        return super.remove(p2);
    }

    public final bridge synthetic boolean removeAll(java.util.Collection p2)
    {
        return super.removeAll(p2);
    }

    public final bridge synthetic boolean retainAll(java.util.Collection p2)
    {
        return super.retainAll(p2);
    }

    public final bridge synthetic int size()
    {
        return super.size();
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }
}
