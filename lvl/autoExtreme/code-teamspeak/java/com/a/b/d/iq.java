package com.a.b.d;
final class iq {
    private static final int a = 3432918353;
    private static final int b = 461845907;
    private static int c;

    static iq()
    {
        com.a.b.d.iq.c = 1073741824;
        return;
    }

    private iq()
    {
        return;
    }

    static int a(int p3)
    {
        return (461845907 * Integer.rotateLeft((-862048943 * p3), 15));
    }

    static int a(int p5, double p6)
    {
        int v1 = Math.max(p5, 2);
        int v0_1 = Integer.highestOneBit(v1);
        if (v1 > ((int) (((double) v0_1) * p6))) {
            v0_1 <<= 1;
            if (v0_1 <= 0) {
                v0_1 = com.a.b.d.iq.c;
            }
        }
        return v0_1;
    }

    static int a(Object p1)
    {
        int v0_0;
        if (p1 != null) {
            v0_0 = p1.hashCode();
        } else {
            v0_0 = 0;
        }
        return com.a.b.d.iq.a(v0_0);
    }

    static boolean a(int p6, int p7)
    {
        if ((((double) p6) <= (1.0 * ((double) p7))) || (p7 >= com.a.b.d.iq.c)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }
}
