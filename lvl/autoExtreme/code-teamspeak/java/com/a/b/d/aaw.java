package com.a.b.d;
final class aaw extends com.a.b.d.lo {
    final transient Object a;
    private transient int c;

    aaw(Object p2)
    {
        this.a = com.a.b.b.cn.a(p2);
        return;
    }

    aaw(Object p1, int p2)
    {
        this.a = p1;
        this.c = p2;
        return;
    }

    final int a(Object[] p2, int p3)
    {
        p2[p3] = this.a;
        return (p3 + 1);
    }

    public final com.a.b.d.agi c()
    {
        return com.a.b.d.nj.a(this.a);
    }

    public final boolean contains(Object p2)
    {
        return this.a.equals(p2);
    }

    public final boolean equals(Object p5)
    {
        int v0 = 1;
        if (p5 != this) {
            if (!(p5 instanceof java.util.Set)) {
                v0 = 0;
            } else {
                if ((((java.util.Set) p5).size() != 1) || (!this.a.equals(((java.util.Set) p5).iterator().next()))) {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    final boolean g_()
    {
        int v0_1;
        if (this.c == 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    final boolean h_()
    {
        return 0;
    }

    public final int hashCode()
    {
        int v0_0 = this.c;
        if (v0_0 == 0) {
            v0_0 = this.a.hashCode();
            this.c = v0_0;
        }
        return v0_0;
    }

    public final boolean isEmpty()
    {
        return 0;
    }

    public final synthetic java.util.Iterator iterator()
    {
        return com.a.b.d.nj.a(this.a);
    }

    public final int size()
    {
        return 1;
    }

    public final String toString()
    {
        String v0_1 = this.a.toString();
        return new StringBuilder((v0_1.length() + 2)).append(91).append(v0_1).append(93).toString();
    }
}
