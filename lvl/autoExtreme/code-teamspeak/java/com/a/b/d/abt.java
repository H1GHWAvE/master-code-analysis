package com.a.b.d;
final class abt extends com.a.b.d.zr {
    private final com.a.b.d.jt a;
    private final com.a.b.d.jt b;
    private final int[] c;
    private final int[] d;

    abt(com.a.b.d.jl p12, com.a.b.d.lo p13, com.a.b.d.lo p14)
    {
        java.util.Iterator v2_0 = com.a.b.d.sz.c();
        Object v3_0 = com.a.b.d.sz.d();
        com.a.b.d.jt v0_0 = p13.iterator();
        while (v0_0.hasNext()) {
            com.a.b.d.ju v1_8 = v0_0.next();
            v2_0.put(v1_8, Integer.valueOf(v3_0.size()));
            v3_0.put(v1_8, new java.util.LinkedHashMap());
        }
        String v4_0 = com.a.b.d.sz.d();
        com.a.b.d.jt v0_1 = p14.iterator();
        while (v0_1.hasNext()) {
            v4_0.put(v0_1.next(), new java.util.LinkedHashMap());
        }
        String v5_0 = new int[p12.size()];
        String v6_0 = new int[p12.size()];
        com.a.b.d.ju v1_2 = 0;
        while (v1_2 < p12.size()) {
            com.a.b.d.jt v0_23 = ((com.a.b.d.adw) p12.get(v1_2));
            int v7_0 = v0_23.a();
            Object v8 = v0_23.b();
            Object v9 = v0_23.c();
            v5_0[v1_2] = ((Integer) v2_0.get(v7_0)).intValue();
            com.a.b.d.jt v0_28 = ((java.util.Map) v3_0.get(v7_0));
            v6_0[v1_2] = v0_28.size();
            com.a.b.d.jt v0_29 = v0_28.put(v8, v9);
            if (v0_29 == null) {
                ((java.util.Map) v4_0.get(v8)).put(v7_0, v9);
                v1_2++;
            } else {
                java.util.Iterator v2_4 = String.valueOf(String.valueOf(v7_0));
                Object v3_4 = String.valueOf(String.valueOf(v8));
                String v4_2 = String.valueOf(String.valueOf(v9));
                com.a.b.d.jt v0_34 = String.valueOf(String.valueOf(v0_29));
                throw new IllegalArgumentException(new StringBuilder(((((v2_4.length() + 37) + v3_4.length()) + v4_2.length()) + v0_34.length())).append("Duplicate value for row=").append(v2_4).append(", column=").append(v3_4).append(": ").append(v4_2).append(", ").append(v0_34).toString());
            }
        }
        this.c = v5_0;
        this.d = v6_0;
        com.a.b.d.ju v1_3 = com.a.b.d.jt.l();
        java.util.Iterator v2_1 = v3_0.entrySet().iterator();
        while (v2_1.hasNext()) {
            com.a.b.d.jt v0_18 = ((java.util.Map$Entry) v2_1.next());
            v1_3.a(v0_18.getKey(), com.a.b.d.jt.a(((java.util.Map) v0_18.getValue())));
        }
        this.a = v1_3.a();
        com.a.b.d.ju v1_4 = com.a.b.d.jt.l();
        java.util.Iterator v2_2 = v4_0.entrySet().iterator();
        while (v2_2.hasNext()) {
            com.a.b.d.jt v0_13 = ((java.util.Map$Entry) v2_2.next());
            v1_4.a(v0_13.getKey(), com.a.b.d.jt.a(((java.util.Map) v0_13.getValue())));
        }
        this.b = v1_4.a();
        return;
    }

    final com.a.b.d.adw a(int p4)
    {
        com.a.b.d.adw v0_3 = ((java.util.Map$Entry) this.a.e().f().get(this.c[p4]));
        Object v1_8 = ((java.util.Map$Entry) ((com.a.b.d.jt) v0_3.getValue()).e().f().get(this.d[p4]));
        return com.a.b.d.abt.b(v0_3.getKey(), v1_8.getKey(), v1_8.getValue());
    }

    final Object b(int p3)
    {
        return ((com.a.b.d.jt) this.a.h().f().get(this.c[p3])).h().f().get(this.d[p3]);
    }

    public final int k()
    {
        return this.c.length;
    }

    public final bridge synthetic java.util.Map l()
    {
        return this.b;
    }

    public final bridge synthetic java.util.Map m()
    {
        return this.a;
    }

    public final com.a.b.d.jt n()
    {
        return this.b;
    }

    public final com.a.b.d.jt o()
    {
        return this.a;
    }
}
