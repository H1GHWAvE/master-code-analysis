package com.a.b.d;
 class fu extends com.a.b.d.an implements com.a.b.d.ga {
    final com.a.b.d.vi a;
    final com.a.b.b.co b;

    fu(com.a.b.d.vi p2, com.a.b.b.co p3)
    {
        this.a = ((com.a.b.d.vi) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.b.co) com.a.b.b.cn.a(p3));
        return;
    }

    private java.util.Collection d()
    {
        com.a.b.d.jl v0_2;
        if (!(this.a instanceof com.a.b.d.aac)) {
            v0_2 = com.a.b.d.jl.d();
        } else {
            v0_2 = com.a.b.d.lo.h();
        }
        return v0_2;
    }

    public com.a.b.d.vi a()
    {
        return this.a;
    }

    public final com.a.b.b.co c()
    {
        return com.a.b.d.sz.a(this.b);
    }

    public java.util.Collection c(Object p2)
    {
        com.a.b.d.fv v0_5;
        if (!this.b.a(p2)) {
            if (!(this.a instanceof com.a.b.d.aac)) {
                v0_5 = new com.a.b.d.fv(p2);
            } else {
                v0_5 = new com.a.b.d.fw(p2);
            }
        } else {
            v0_5 = this.a.c(p2);
        }
        return v0_5;
    }

    public java.util.Collection d(Object p2)
    {
        com.a.b.d.jl v0_3;
        if (!this.f(p2)) {
            if (!(this.a instanceof com.a.b.d.aac)) {
                v0_3 = com.a.b.d.jl.d();
            } else {
                v0_3 = com.a.b.d.lo.h();
            }
        } else {
            v0_3 = this.a.d(p2);
        }
        return v0_3;
    }

    public final int f()
    {
        java.util.Iterator v2 = this.b().values().iterator();
        int v1_2 = 0;
        while (v2.hasNext()) {
            v1_2 = (((java.util.Collection) v2.next()).size() + v1_2);
        }
        return v1_2;
    }

    public final boolean f(Object p2)
    {
        int v0_2;
        if (!this.a.f(p2)) {
            v0_2 = 0;
        } else {
            v0_2 = this.b.a(p2);
        }
        return v0_2;
    }

    public final void g()
    {
        this.p().clear();
        return;
    }

    final java.util.Set h()
    {
        return com.a.b.d.aad.a(this.a.p(), this.b);
    }

    final java.util.Iterator l()
    {
        throw new AssertionError("should never be called");
    }

    final java.util.Map m()
    {
        return com.a.b.d.sz.a(this.a.b(), this.b);
    }

    java.util.Collection o()
    {
        return new com.a.b.d.fx(this);
    }

    final com.a.b.d.xc r()
    {
        return com.a.b.d.xe.a(this.a.q(), this.b);
    }

    final java.util.Collection s()
    {
        return new com.a.b.d.gb(this);
    }
}
