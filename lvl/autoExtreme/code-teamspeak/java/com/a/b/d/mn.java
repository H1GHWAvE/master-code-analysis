package com.a.b.d;
final class mn implements com.a.b.b.bj {
    private final com.a.b.d.mk a;

    public mn(com.a.b.d.mk p1)
    {
        this.a = p1;
        return;
    }

    public final Object e(Object p2)
    {
        return this.a.a(p2);
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.d.mn)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.d.mn) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }
}
