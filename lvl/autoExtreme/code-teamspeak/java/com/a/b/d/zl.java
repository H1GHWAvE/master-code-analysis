package com.a.b.d;
final class zl extends com.a.b.d.lw {
    final transient com.a.b.d.zq a;
    private final transient com.a.b.d.jl b;

    zl(com.a.b.d.zq p1, com.a.b.d.jl p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private zl(com.a.b.d.zq p1, com.a.b.d.jl p2, com.a.b.d.lw p3)
    {
        this(p3);
        this.a = p1;
        this.b = p2;
        return;
    }

    static synthetic com.a.b.d.jl a(com.a.b.d.zl p1)
    {
        return p1.b;
    }

    private com.a.b.d.lw a(int p3, int p4)
    {
        if ((p3 != 0) || (p4 != this.size())) {
            if (p3 != p4) {
                this = com.a.b.d.zl.a(this.a.a(p3, p4), this.b.a(p3, p4));
            } else {
                this = com.a.b.d.zl.a(this.comparator());
            }
        }
        return this;
    }

    public final com.a.b.d.lw a(Object p4, boolean p5)
    {
        return this.a(0, this.a.e(com.a.b.b.cn.a(p4), p5));
    }

    public final com.a.b.d.me a()
    {
        return this.a;
    }

    public final com.a.b.d.lw b(Object p3, boolean p4)
    {
        return this.a(this.a.f(com.a.b.b.cn.a(p3), p4), this.size());
    }

    final com.a.b.d.lo d()
    {
        return new com.a.b.d.zn(this, 0);
    }

    public final bridge synthetic com.a.b.d.lo g()
    {
        return this.a;
    }

    public final Object get(Object p3)
    {
        Object v0_2;
        Object v0_1 = this.a.c(p3);
        if (v0_1 != -1) {
            v0_2 = this.b.get(v0_1);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final com.a.b.d.iz h()
    {
        return this.b;
    }

    public final synthetic java.util.NavigableMap headMap(Object p2, boolean p3)
    {
        return this.a(p2, p3);
    }

    final com.a.b.d.lw i()
    {
        return new com.a.b.d.zl(((com.a.b.d.zq) this.a.b()), this.b.e(), this);
    }

    public final bridge synthetic java.util.Set keySet()
    {
        return this.a;
    }

    public final synthetic java.util.NavigableMap tailMap(Object p2, boolean p3)
    {
        return this.b(p2, p3);
    }

    public final bridge synthetic java.util.Collection values()
    {
        return this.b;
    }
}
