package com.a.b.d;
public final class hw extends com.a.b.d.abx {
    private static final long c;

    private hw(java.util.Map p1, com.a.b.d.hx p2)
    {
        this(p1, p2);
        return;
    }

    private static com.a.b.d.hw a(int p3, int p4)
    {
        com.a.b.d.cl.a(p4, "expectedCellsPerRow");
        return new com.a.b.d.hw(com.a.b.d.sz.a(p3), new com.a.b.d.hx(p4));
    }

    private static com.a.b.d.hw b(com.a.b.d.adv p4)
    {
        com.a.b.d.hw v0_1 = new com.a.b.d.hw(new java.util.HashMap(), new com.a.b.d.hx(0));
        v0_1.a(p4);
        return v0_1;
    }

    private static com.a.b.d.hw p()
    {
        return new com.a.b.d.hw(new java.util.HashMap(), new com.a.b.d.hx(0));
    }

    public final bridge synthetic Object a(Object p2, Object p3, Object p4)
    {
        return super.a(p2, p3, p4);
    }

    public final bridge synthetic java.util.Set a()
    {
        return super.a();
    }

    public final bridge synthetic void a(com.a.b.d.adv p1)
    {
        super.a(p1);
        return;
    }

    public final boolean a(Object p2)
    {
        return super.a(p2);
    }

    public final boolean a(Object p2, Object p3)
    {
        return super.a(p2, p3);
    }

    public final Object b(Object p2, Object p3)
    {
        return super.b(p2, p3);
    }

    public final bridge synthetic java.util.Set b()
    {
        return super.b();
    }

    public final boolean b(Object p2)
    {
        return super.b(p2);
    }

    public final Object c(Object p2, Object p3)
    {
        return super.c(p2, p3);
    }

    public final bridge synthetic boolean c()
    {
        return super.c();
    }

    public final boolean c(Object p2)
    {
        return super.c(p2);
    }

    public final bridge synthetic java.util.Map d(Object p2)
    {
        return super.d(p2);
    }

    public final bridge synthetic void d()
    {
        super.d();
        return;
    }

    public final bridge synthetic java.util.Map e(Object p2)
    {
        return super.e(p2);
    }

    public final bridge synthetic java.util.Set e()
    {
        return super.e();
    }

    public final boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final bridge synthetic java.util.Collection h()
    {
        return super.h();
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final bridge synthetic int k()
    {
        return super.k();
    }

    public final bridge synthetic java.util.Map l()
    {
        return super.l();
    }

    public final bridge synthetic java.util.Map m()
    {
        return super.m();
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }
}
