package com.a.b.d;
final class vf {
    final com.a.b.d.yd a;
    com.a.b.d.vf b;
    final synthetic com.a.b.d.vc c;

    vf(com.a.b.d.vc p1, com.a.b.d.yd p2)
    {
        this.c = p1;
        this.a = p2;
        return;
    }

    private int a(int p3)
    {
        return this.b(((p3 * 2) + 1), 2);
    }

    private int a(Object p5)
    {
        int v0_6;
        Object v1_0 = ((com.a.b.d.vc.b(this.c) - 1) / 2);
        if (v1_0 == null) {
            v0_6 = com.a.b.d.vc.b(this.c);
        } else {
            v0_6 = ((((v1_0 - 1) / 2) * 2) + 2);
            if ((v0_6 == v1_0) || (((v0_6 * 2) + 1) < com.a.b.d.vc.b(this.c))) {
            } else {
                Object v1_5 = this.c.b[v0_6];
                if (this.a.compare(v1_5, p5) >= 0) {
                } else {
                    com.a.b.d.vc.a(this.c)[v0_6] = p5;
                    com.a.b.d.vc.a(this.c)[com.a.b.d.vc.b(this.c)] = v1_5;
                }
            }
        }
        return v0_6;
    }

    private com.a.b.d.vg a(int p5, int p6, Object p7)
    {
        int v2_3;
        com.a.b.d.vg v0_2 = this.b(((p6 * 2) + 1), 2);
        if ((v0_2 <= null) || (this.a.compare(this.c.b[v0_2], p7) >= 0)) {
            v2_3 = this.b(p6, p7);
        } else {
            com.a.b.d.vc.a(this.c)[p6] = this.c.b[v0_2];
            com.a.b.d.vc.a(this.c)[v0_2] = p7;
            v2_3 = v0_2;
        }
        com.a.b.d.vg v0_9;
        if (v2_3 != p6) {
            com.a.b.d.vg v0_6;
            if (v2_3 >= p5) {
                v0_6 = this.c.b[((p5 - 1) / 2)];
            } else {
                v0_6 = this.c.b[p5];
            }
            if (this.b.a(v2_3, p7) >= p5) {
                v0_9 = 0;
            } else {
                v0_9 = new com.a.b.d.vg(p7, v0_6);
            }
        } else {
            v0_9 = 0;
        }
        return v0_9;
    }

    private static synthetic boolean a(com.a.b.d.vf p3, int p4)
    {
        int v0 = 0;
        if (((((p4 * 2) + 1) >= com.a.b.d.vc.b(p3.c)) || (p3.a(p4, ((p4 * 2) + 1)) <= 0)) && (((((p4 * 2) + 2) >= com.a.b.d.vc.b(p3.c)) || (p3.a(p4, ((p4 * 2) + 2)) <= 0)) && (((p4 <= 0) || (p3.a(p4, ((p4 - 1) / 2)) <= 0)) && ((p4 <= 2) || (p3.a(((((p4 - 1) / 2) - 1) / 2), p4) <= 0))))) {
            v0 = 1;
        }
        return v0;
    }

    private int b(int p3)
    {
        int v0_4;
        int v0_1 = ((p3 * 2) + 1);
        if (v0_1 >= 0) {
            v0_4 = this.b(((v0_1 * 2) + 1), 4);
        } else {
            v0_4 = -1;
        }
        return v0_4;
    }

    private int c(int p4)
    {
        while(true) {
            int v0_4;
            int v0_1 = ((p4 * 2) + 1);
            if (v0_1 >= 0) {
                v0_4 = this.b(((v0_1 * 2) + 1), 4);
            } else {
                v0_4 = -1;
            }
            if (v0_4 <= 0) {
                break;
            }
            com.a.b.d.vc.a(this.c)[p4] = this.c.b[v0_4];
            p4 = v0_4;
        }
        return p4;
    }

    private void c(int p2, Object p3)
    {
        int v0 = this.b(p2, p3);
        if (v0 != p2) {
            this = this.b;
            p2 = v0;
        }
        this.a(p2, p3);
        return;
    }

    private int d(int p4, Object p5)
    {
        int v0_2 = this.b(((p4 * 2) + 1), 2);
        if ((v0_2 <= 0) || (this.a.compare(this.c.b[v0_2], p5) >= 0)) {
            v0_2 = this.b(p4, p5);
        } else {
            com.a.b.d.vc.a(this.c)[p4] = this.c.b[v0_2];
            com.a.b.d.vc.a(this.c)[v0_2] = p5;
        }
        return v0_2;
    }

    private boolean d(int p4)
    {
        int v0 = 0;
        if (((((p4 * 2) + 1) >= com.a.b.d.vc.b(this.c)) || (this.a(p4, ((p4 * 2) + 1)) <= 0)) && (((((p4 * 2) + 2) >= com.a.b.d.vc.b(this.c)) || (this.a(p4, ((p4 * 2) + 2)) <= 0)) && (((p4 <= 0) || (this.a(p4, ((p4 - 1) / 2)) <= 0)) && ((p4 <= 2) || (this.a(((((p4 - 1) / 2) - 1) / 2), p4) <= 0))))) {
            v0 = 1;
        }
        return v0;
    }

    private static int e(int p1)
    {
        return ((p1 * 2) + 1);
    }

    private static int f(int p1)
    {
        return ((p1 * 2) + 2);
    }

    private static int g(int p1)
    {
        return ((p1 - 1) / 2);
    }

    private static int h(int p1)
    {
        return ((((p1 - 1) / 2) - 1) / 2);
    }

    final int a(int p4, int p5)
    {
        return this.a.compare(this.c.b[p4], this.c.b[p5]);
    }

    final int a(int p4, Object p5)
    {
        while (p4 > 2) {
            Object[] v0_4 = ((((p4 - 1) / 2) - 1) / 2);
            Object v1_2 = this.c.b[v0_4];
            if (this.a.compare(v1_2, p5) <= 0) {
                break;
            }
            com.a.b.d.vc.a(this.c)[p4] = v1_2;
            p4 = v0_4;
        }
        com.a.b.d.vc.a(this.c)[p4] = p5;
        return p4;
    }

    final int b(int p5, int p6)
    {
        int v0_7;
        if (p5 < com.a.b.d.vc.b(this.c)) {
            int v0_2;
            if (p5 <= 0) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
            com.a.b.b.cn.b(v0_2);
            int v2 = (Math.min(p5, (com.a.b.d.vc.b(this.c) - p6)) + p6);
            int v1 = (p5 + 1);
            v0_7 = p5;
            while (v1 < v2) {
                if (this.a(v1, v0_7) < 0) {
                    v0_7 = v1;
                }
                v1++;
            }
        } else {
            v0_7 = -1;
        }
        return v0_7;
    }

    final int b(int p6, Object p7)
    {
        Object[] v1_0 = 0;
        if (p6 != 0) {
            Object[] v0_10;
            int v3 = ((p6 - 1) / 2);
            Object[] v1_1 = this.c.b[v3];
            if (v3 == 0) {
                v0_10 = v1_1;
                v1_0 = v3;
            } else {
                Object[] v2_0 = ((((v3 - 1) / 2) * 2) + 2);
                if ((v2_0 == v3) || (((v2_0 * 2) + 1) < com.a.b.d.vc.b(this.c))) {
                } else {
                    v0_10 = this.c.b[v2_0];
                    if (this.a.compare(v0_10, v1_1) >= 0) {
                    } else {
                        v1_0 = v2_0;
                    }
                }
            }
            if (this.a.compare(v0_10, p7) >= 0) {
                com.a.b.d.vc.a(this.c)[p6] = p7;
                v1_0 = p6;
            } else {
                com.a.b.d.vc.a(this.c)[p6] = v0_10;
                com.a.b.d.vc.a(this.c)[v1_0] = p7;
            }
        } else {
            com.a.b.d.vc.a(this.c)[0] = p7;
        }
        return v1_0;
    }
}
