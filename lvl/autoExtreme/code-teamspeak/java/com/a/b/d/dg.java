package com.a.b.d;
public final class dg extends com.a.b.d.as implements java.io.Serializable {
    private static final long b = 1;
    private final transient java.util.concurrent.ConcurrentMap a;

    private dg(java.util.concurrent.ConcurrentMap p2)
    {
        com.a.b.b.cn.a(p2.isEmpty());
        this.a = p2;
        return;
    }

    private static com.a.b.d.dg a(com.a.b.d.ql p2)
    {
        return new com.a.b.d.dg(p2.e());
    }

    private static com.a.b.d.dg a(Iterable p2)
    {
        com.a.b.d.dg v0_1 = new com.a.b.d.dg(new java.util.concurrent.ConcurrentHashMap());
        com.a.b.d.mq.a(v0_1, p2);
        return v0_1;
    }

    static synthetic java.util.concurrent.ConcurrentMap a(com.a.b.d.dg p1)
    {
        return p1.a;
    }

    private void a(java.io.ObjectInputStream p3)
    {
        p3.defaultReadObject();
        com.a.b.d.dl.a.a(this, ((java.util.concurrent.ConcurrentMap) p3.readObject()));
        return;
    }

    private void a(java.io.ObjectOutputStream p2)
    {
        p2.defaultWriteObject();
        p2.writeObject(this.a);
        return;
    }

    private boolean d(Object p7, int p8)
    {
        int v1 = 1;
        if (p8 != 0) {
            java.util.concurrent.atomic.AtomicInteger v0_0;
            if (p8 <= 0) {
                v0_0 = 0;
            } else {
                v0_0 = 1;
            }
            int v4_0 = new Object[1];
            v4_0[0] = Integer.valueOf(p8);
            com.a.b.b.cn.a(v0_0, "Invalid occurrences: %s", v4_0);
            java.util.concurrent.atomic.AtomicInteger v0_3 = ((java.util.concurrent.atomic.AtomicInteger) com.a.b.d.sz.a(this.a, p7));
            if (v0_3 == null) {
                v1 = 0;
                return v1;
            }
            do {
                boolean v3_1 = v0_3.get();
                if (v3_1 >= p8) {
                    int v4_1 = (v3_1 - p8);
                } else {
                    v1 = 0;
                }
            } while(!v0_3.compareAndSet(v3_1, v4_1));
            if (v4_1 == 0) {
                this.a.remove(p7, v0_3);
            }
        }
        return v1;
    }

    private static com.a.b.d.dg g()
    {
        return new com.a.b.d.dg(new java.util.concurrent.ConcurrentHashMap());
    }

    private java.util.List h()
    {
        java.util.ArrayList v1 = com.a.b.d.ov.b(this.size());
        java.util.Iterator v2 = this.a().iterator();
        while (v2.hasNext()) {
            int v0_4 = ((com.a.b.d.xd) v2.next());
            Object v3 = v0_4.a();
            int v0_5 = v0_4.b();
            while (v0_5 > 0) {
                v1.add(v3);
                v0_5--;
            }
        }
        return v1;
    }

    public final int a(Object p2)
    {
        int v0_3;
        int v0_2 = ((java.util.concurrent.atomic.AtomicInteger) com.a.b.d.sz.a(this.a, p2));
        if (v0_2 != 0) {
            v0_3 = v0_2.get();
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final int a(Object p6, int p7)
    {
        String v2_0 = 0;
        com.a.b.b.cn.a(p6);
        if (p7 != 0) {
            boolean v0_0;
            if (p7 <= 0) {
                v0_0 = 0;
            } else {
                v0_0 = 1;
            }
            String v1_1 = new Object[1];
            v1_1[0] = Integer.valueOf(p7);
            com.a.b.b.cn.a(v0_0, "Invalid occurrences: %s", v1_1);
            do {
                boolean v0_3 = ((java.util.concurrent.atomic.AtomicInteger) com.a.b.d.sz.a(this.a, p6));
                if (!v0_3) {
                    v0_3 = ((java.util.concurrent.atomic.AtomicInteger) this.a.putIfAbsent(p6, new java.util.concurrent.atomic.AtomicInteger(p7)));
                    if (!v0_3) {
                        break;
                    }
                }
                do {
                    String v1_4 = v0_3.get();
                    if (v1_4 == null) {
                        String v1_6 = new java.util.concurrent.atomic.AtomicInteger(p7);
                        if ((this.a.putIfAbsent(p6, v1_6) == null) || (this.a.replace(p6, v0_3, v1_6))) {
                        }
                    } else {
                    }
                } while(
    public final bridge synthetic java.util.Set a()
    {
        return super.a();
    }

    public final boolean a(Object p6, int p7, int p8)
    {
        java.util.concurrent.ConcurrentMap v0_5;
        com.a.b.b.cn.a(p6);
        com.a.b.d.cl.a(p7, "oldCount");
        com.a.b.d.cl.a(p8, "newCount");
        java.util.concurrent.ConcurrentMap v0_4 = ((java.util.concurrent.atomic.AtomicInteger) com.a.b.d.sz.a(this.a, p6));
        if (v0_4 != null) {
            java.util.concurrent.atomic.AtomicInteger v3_0 = v0_4.get();
            if (v3_0 == p7) {
                if (v3_0 != null) {
                    if (v0_4.compareAndSet(v3_0, p8)) {
                        if (p8 == 0) {
                            this.a.remove(p6, v0_4);
                        }
                        v0_5 = 1;
                        return v0_5;
                    }
                } else {
                    if (p8 != 0) {
                        java.util.concurrent.atomic.AtomicInteger v3_3 = new java.util.concurrent.atomic.AtomicInteger(p8);
                        if ((this.a.putIfAbsent(p6, v3_3) != null) && (!this.a.replace(p6, v0_4, v3_3))) {
                            v0_5 = 0;
                        } else {
                            v0_5 = 1;
                        }
                    } else {
                        this.a.remove(p6, v0_4);
                        v0_5 = 1;
                    }
                    return v0_5;
                }
            }
            v0_5 = 0;
        } else {
            if (p7 == 0) {
                if (p8 != 0) {
                    if (this.a.putIfAbsent(p6, new java.util.concurrent.atomic.AtomicInteger(p8)) != null) {
                        v0_5 = 0;
                    } else {
                        v0_5 = 1;
                    }
                } else {
                    v0_5 = 1;
                }
            } else {
                v0_5 = 0;
            }
        }
        return v0_5;
    }

    public final bridge synthetic boolean add(Object p2)
    {
        return super.add(p2);
    }

    public final bridge synthetic boolean addAll(java.util.Collection p2)
    {
        return super.addAll(p2);
    }

    public final int b(Object p6, int p7)
    {
        int v2_0 = 0;
        if (p7 != 0) {
            java.util.concurrent.atomic.AtomicInteger v0_0;
            if (p7 <= 0) {
                v0_0 = 0;
            } else {
                v0_0 = 1;
            }
            int v1_1 = new Object[1];
            v1_1[0] = Integer.valueOf(p7);
            com.a.b.b.cn.a(v0_0, "Invalid occurrences: %s", v1_1);
            java.util.concurrent.atomic.AtomicInteger v0_3 = ((java.util.concurrent.atomic.AtomicInteger) com.a.b.d.sz.a(this.a, p6));
            if (v0_3 == null) {
                return v2_0;
            }
            do {
                int v1_2 = v0_3.get();
                if (v1_2 != 0) {
                    int v3_2 = Math.max(0, (v1_2 - p7));
                }
            } while(!v0_3.compareAndSet(v1_2, v3_2));
            if (v3_2 == 0) {
                this.a.remove(p6, v0_3);
            }
            v2_0 = v1_2;
        } else {
            v2_0 = this.a(p6);
        }
        return v2_0;
    }

    final java.util.Iterator b()
    {
        return new com.a.b.d.dj(this, new com.a.b.d.di(this));
    }

    final int c()
    {
        return this.a.size();
    }

    public final int c(Object p5, int p6)
    {
        com.a.b.b.cn.a(p5);
        com.a.b.d.cl.a(p6, "count");
        while(true) {
            java.util.concurrent.atomic.AtomicInteger v0_6;
            java.util.concurrent.atomic.AtomicInteger v0_3 = ((java.util.concurrent.atomic.AtomicInteger) com.a.b.d.sz.a(this.a, p5));
            if (v0_3 != null) {
                do {
                    java.util.concurrent.atomic.AtomicInteger v2_2 = v0_3.get();
                    if (v2_2 != null) {
                    } else {
                        if (p6 != 0) {
                            java.util.concurrent.atomic.AtomicInteger v2_4 = new java.util.concurrent.atomic.AtomicInteger(p6);
                            if ((this.a.putIfAbsent(p5, v2_4) == null) || (this.a.replace(p5, v0_3, v2_4))) {
                                v0_6 = 0;
                            }
                        } else {
                            v0_6 = 0;
                        }
                    }
                } while(!v0_3.compareAndSet(v2_2, p6));
            } else {
                if (p6 != 0) {
                    v0_3 = ((java.util.concurrent.atomic.AtomicInteger) this.a.putIfAbsent(p5, new java.util.concurrent.atomic.AtomicInteger(p6)));
                    if (v0_3 != null) {
                    } else {
                        v0_6 = 0;
                    }
                } else {
                    v0_6 = 0;
                }
            }
            return v0_6;
        }
        if (p6 == 0) {
            this.a.remove(p5, v0_3);
        }
        v0_6 = v2_2;
        return v0_6;
    }

    public final void clear()
    {
        this.a.clear();
        return;
    }

    public final bridge synthetic boolean contains(Object p2)
    {
        return super.contains(p2);
    }

    final java.util.Set e()
    {
        return new com.a.b.d.dh(this, this.a.keySet());
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final java.util.Set f()
    {
        return new com.a.b.d.dk(this, 0);
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final boolean isEmpty()
    {
        return this.a.isEmpty();
    }

    public final bridge synthetic java.util.Iterator iterator()
    {
        return super.iterator();
    }

    public final bridge synthetic java.util.Set n_()
    {
        return super.n_();
    }

    public final bridge synthetic boolean remove(Object p2)
    {
        return super.remove(p2);
    }

    public final bridge synthetic boolean removeAll(java.util.Collection p2)
    {
        return super.removeAll(p2);
    }

    public final bridge synthetic boolean retainAll(java.util.Collection p2)
    {
        return super.retainAll(p2);
    }

    public final int size()
    {
        java.util.Iterator v4 = this.a.values().iterator();
        long v2_2 = 0;
        while (v4.hasNext()) {
            v2_2 = (((long) ((java.util.concurrent.atomic.AtomicInteger) v4.next()).get()) + v2_2);
        }
        return com.a.b.l.q.b(v2_2);
    }

    public final Object[] toArray()
    {
        return this.h().toArray();
    }

    public final Object[] toArray(Object[] p2)
    {
        return this.h().toArray(p2);
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }
}
