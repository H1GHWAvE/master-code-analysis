package com.a.b.d;
final enum class mp extends java.lang.Enum {
    public static final enum com.a.b.d.mp a;
    private static final synthetic com.a.b.d.mp[] b;

    static mp()
    {
        com.a.b.d.mp.a = new com.a.b.d.mp("VALUE");
        com.a.b.d.mp[] v0_3 = new com.a.b.d.mp[1];
        v0_3[0] = com.a.b.d.mp.a;
        com.a.b.d.mp.b = v0_3;
        return;
    }

    private mp(String p2)
    {
        this(p2, 0);
        return;
    }

    public static com.a.b.d.mp valueOf(String p1)
    {
        return ((com.a.b.d.mp) Enum.valueOf(com.a.b.d.mp, p1));
    }

    public static com.a.b.d.mp[] values()
    {
        return ((com.a.b.d.mp[]) com.a.b.d.mp.b.clone());
    }
}
