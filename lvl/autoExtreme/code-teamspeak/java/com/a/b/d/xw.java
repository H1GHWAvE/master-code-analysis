package com.a.b.d;
 class xw extends com.a.b.d.gy implements java.io.Serializable {
    private static final long d;
    final com.a.b.d.xc a;
    transient java.util.Set b;
    transient java.util.Set c;

    xw(com.a.b.d.xc p1)
    {
        this.a = p1;
        return;
    }

    public final int a(Object p2, int p3)
    {
        throw new UnsupportedOperationException();
    }

    public final java.util.Set a()
    {
        java.util.Set v0_0 = this.c;
        if (v0_0 == null) {
            v0_0 = java.util.Collections.unmodifiableSet(this.a.a());
            this.c = v0_0;
        }
        return v0_0;
    }

    public final boolean a(Object p2, int p3, int p4)
    {
        throw new UnsupportedOperationException();
    }

    public boolean add(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(java.util.Collection p2)
    {
        throw new UnsupportedOperationException();
    }

    public final int b(Object p2, int p3)
    {
        throw new UnsupportedOperationException();
    }

    protected synthetic java.util.Collection b()
    {
        return this.f();
    }

    public final int c(Object p2, int p3)
    {
        throw new UnsupportedOperationException();
    }

    java.util.Set c()
    {
        return java.util.Collections.unmodifiableSet(this.a.n_());
    }

    public void clear()
    {
        throw new UnsupportedOperationException();
    }

    protected com.a.b.d.xc f()
    {
        return this.a;
    }

    public java.util.Iterator iterator()
    {
        return com.a.b.d.nj.a(this.a.iterator());
    }

    protected synthetic Object k_()
    {
        return this.f();
    }

    public java.util.Set n_()
    {
        java.util.Set v0 = this.b;
        if (v0 == null) {
            v0 = this.c();
            this.b = v0;
        }
        return v0;
    }

    public boolean remove(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(java.util.Collection p2)
    {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(java.util.Collection p2)
    {
        throw new UnsupportedOperationException();
    }
}
