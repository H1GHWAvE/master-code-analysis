package com.a.b.d;
final class qd extends com.a.b.d.gs {
    java.util.Set a;
    java.util.Collection b;
    final synthetic java.util.Map c;
    final synthetic com.a.b.d.qc d;

    qd(com.a.b.d.qc p1, java.util.Map p2)
    {
        this.d = p1;
        this.c = p2;
        return;
    }

    private java.util.Collection a(Object p4)
    {
        ClassCastException v0 = 0;
        try {
            ClassCastException v1_1 = this.d.c(p4);
        } catch (ClassCastException v1) {
            return v0;
        }
        if (!v1_1.isEmpty()) {
            v0 = v1_1;
            return v0;
        } else {
            return v0;
        }
    }

    protected final java.util.Map a()
    {
        return this.c;
    }

    public final boolean containsValue(Object p2)
    {
        return this.values().contains(p2);
    }

    public final java.util.Set entrySet()
    {
        com.a.b.d.ps v0_0 = this.a;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.ps(this.c.entrySet(), this.d.a);
            this.a = v0_0;
        }
        return v0_0;
    }

    public final synthetic Object get(Object p2)
    {
        return this.a(p2);
    }

    protected final bridge synthetic Object k_()
    {
        return this.c;
    }

    public final java.util.Collection values()
    {
        com.a.b.d.pu v0_0 = this.b;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.pu(this.c.values(), this.entrySet());
            this.b = v0_0;
        }
        return v0_0;
    }
}
