package com.a.b.l;
public final class ao extends java.lang.Number implements java.io.Serializable, java.lang.Comparable {
    public static final com.a.b.l.ao a = None;
    public static final com.a.b.l.ao b = None;
    public static final com.a.b.l.ao c = None;
    private static final long d = 9223372036854775807;
    private final long e;

    static ao()
    {
        com.a.b.l.ao.a = new com.a.b.l.ao(0);
        com.a.b.l.ao.b = new com.a.b.l.ao(1);
        com.a.b.l.ao.c = new com.a.b.l.ao(-1);
        return;
    }

    private ao(long p2)
    {
        this.e = p2;
        return;
    }

    private static com.a.b.l.ao a(long p2)
    {
        return new com.a.b.l.ao(p2);
    }

    private com.a.b.l.ao a(com.a.b.l.ao p5)
    {
        return com.a.b.l.ao.a((((com.a.b.l.ao) com.a.b.b.cn.a(p5)).e + this.e));
    }

    private static com.a.b.l.ao a(String p2)
    {
        return com.a.b.l.ao.a(com.a.b.l.ap.a(p2, 10));
    }

    private static com.a.b.l.ao a(java.math.BigInteger p4)
    {
        com.a.b.l.ao v0_2;
        com.a.b.b.cn.a(p4);
        if ((p4.signum() < 0) || (p4.bitLength() > 64)) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = p4;
        com.a.b.b.cn.a(v0_2, "value (%s) is outside the range for an unsigned long value", v1_1);
        return com.a.b.l.ao.a(p4.longValue());
    }

    private String a(int p3)
    {
        return com.a.b.l.ap.a(this.e, p3);
    }

    private java.math.BigInteger a()
    {
        java.math.BigInteger v0_2 = java.math.BigInteger.valueOf((this.e & nan));
        if (this.e < 0) {
            v0_2 = v0_2.setBit(63);
        }
        return v0_2;
    }

    private static com.a.b.l.ao b(long p6)
    {
        com.a.b.l.ao v0_1;
        if (p6 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = Long.valueOf(p6);
        com.a.b.b.cn.a(v0_1, "value (%s) is outside the range for an unsigned long value", v1_1);
        return com.a.b.l.ao.a(p6);
    }

    private com.a.b.l.ao b(com.a.b.l.ao p5)
    {
        return com.a.b.l.ao.a((this.e - ((com.a.b.l.ao) com.a.b.b.cn.a(p5)).e));
    }

    private static com.a.b.l.ao b(String p2)
    {
        return com.a.b.l.ao.a(com.a.b.l.ap.a(p2, 10));
    }

    private com.a.b.l.ao c(com.a.b.l.ao p5)
    {
        return com.a.b.l.ao.a((((com.a.b.l.ao) com.a.b.b.cn.a(p5)).e * this.e));
    }

    private com.a.b.l.ao d(com.a.b.l.ao p5)
    {
        return com.a.b.l.ao.a(com.a.b.l.ap.b(this.e, ((com.a.b.l.ao) com.a.b.b.cn.a(p5)).e));
    }

    private com.a.b.l.ao e(com.a.b.l.ao p5)
    {
        return com.a.b.l.ao.a(com.a.b.l.ap.c(this.e, ((com.a.b.l.ao) com.a.b.b.cn.a(p5)).e));
    }

    private int f(com.a.b.l.ao p5)
    {
        com.a.b.b.cn.a(p5);
        return com.a.b.l.ap.a(this.e, p5.e);
    }

    public final synthetic int compareTo(Object p5)
    {
        com.a.b.b.cn.a(((com.a.b.l.ao) p5));
        return com.a.b.l.ap.a(this.e, ((com.a.b.l.ao) p5).e);
    }

    public final double doubleValue()
    {
        double v0_2 = ((double) (this.e & nan));
        if (this.e < 0) {
            v0_2 += 9.223372036854776e+18;
        }
        return v0_2;
    }

    public final boolean equals(Object p7)
    {
        int v0 = 0;
        if (((p7 instanceof com.a.b.l.ao)) && (this.e == ((com.a.b.l.ao) p7).e)) {
            v0 = 1;
        }
        return v0;
    }

    public final float floatValue()
    {
        float v0_2 = ((float) (this.e & nan));
        if (this.e < 0) {
            v0_2 += 1593835520;
        }
        return v0_2;
    }

    public final int hashCode()
    {
        int v0_0 = this.e;
        return ((int) (v0_0 ^ (v0_0 >> 32)));
    }

    public final int intValue()
    {
        return ((int) this.e);
    }

    public final long longValue()
    {
        return this.e;
    }

    public final String toString()
    {
        return com.a.b.l.ap.a(this.e);
    }
}
