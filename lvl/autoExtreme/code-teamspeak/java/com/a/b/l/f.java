package com.a.b.l;
public final class f {
    public static final int a = 2;

    private f()
    {
        return;
    }

    private static char a(byte p2, byte p3)
    {
        return ((char) ((p2 << 8) | (p3 & 255)));
    }

    private static char a(long p4)
    {
        if (((long) ((char) ((int) p4))) == p4) {
            return ((char) ((int) p4));
        } else {
            throw new IllegalArgumentException(new StringBuilder(34).append("Out of range: ").append(p4).toString());
        }
    }

    private static char a(byte[] p7)
    {
        char v0_1;
        if (p7.length < 2) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4 = new Object[2];
        v4[0] = Integer.valueOf(p7.length);
        v4[1] = Integer.valueOf(2);
        com.a.b.b.cn.a(v0_1, "array too small: %s < %s", v4);
        return ((char) ((p7[0] << 8) | (p7[1] & 255)));
    }

    private static varargs char a(char[] p3)
    {
        char v0_1;
        int v1 = 1;
        if (p3.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        char v0_2 = p3[0];
        while (v1 < p3.length) {
            if (p3[v1] < v0_2) {
                v0_2 = p3[v1];
            }
            v1++;
        }
        return v0_2;
    }

    private static int a(char p0)
    {
        return p0;
    }

    private static int a(char p1, char p2)
    {
        return (p1 - p2);
    }

    static int a(char[] p2, char p3, int p4, int p5)
    {
        int v0 = p4;
        while (v0 < p5) {
            if (p2[v0] != p3) {
                v0++;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static int a(char[] p5, char[] p6)
    {
        int v1 = 0;
        com.a.b.b.cn.a(p5, "array");
        com.a.b.b.cn.a(p6, "target");
        if (p6.length != 0) {
            int v0_3 = 0;
            while (v0_3 < ((p5.length - p6.length) + 1)) {
                int v2_3 = 0;
                while (v2_3 < p6.length) {
                    if (p5[(v0_3 + v2_3)] != p6[v2_3]) {
                        v0_3++;
                    } else {
                        v2_3++;
                    }
                }
                v1 = v0_3;
            }
            v1 = -1;
        }
        return v1;
    }

    private static varargs String a(String p5, char[] p6)
    {
        String v0_6;
        com.a.b.b.cn.a(p5);
        int v1 = p6.length;
        if (v1 != 0) {
            StringBuilder v2_1 = new StringBuilder(((p5.length() * (v1 - 1)) + v1));
            v2_1.append(p6[0]);
            String v0_5 = 1;
            while (v0_5 < v1) {
                v2_1.append(p5).append(p6[v0_5]);
                v0_5++;
            }
            v0_6 = v2_1.toString();
        } else {
            v0_6 = "";
        }
        return v0_6;
    }

    private static java.util.Comparator a()
    {
        return com.a.b.l.h.a;
    }

    private static boolean a(char[] p4, char p5)
    {
        int v0 = 0;
        int v1 = 0;
        while (v1 < p4.length) {
            if (p4[v1] != p5) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    private static char[] a(java.util.Collection p5)
    {
        char[] v0_1;
        int v2 = 0;
        if (!(p5 instanceof com.a.b.l.g)) {
            Object[] v3_0 = p5.toArray();
            int v4_0 = v3_0.length;
            char[] v1_0 = new char[v4_0];
            while (v2 < v4_0) {
                v1_0[v2] = ((Character) com.a.b.b.cn.a(v3_0[v2])).charValue();
                v2++;
            }
            v0_1 = v1_0;
        } else {
            char[] v1_1 = ((com.a.b.l.g) p5).size();
            v0_1 = new char[v1_1];
            System.arraycopy(((com.a.b.l.g) p5).a, ((com.a.b.l.g) p5).b, v0_1, 0, v1_1);
        }
        return v0_1;
    }

    private static char[] a(char[] p3, int p4)
    {
        char[] v0 = new char[p4];
        System.arraycopy(p3, 0, v0, 0, Math.min(p3.length, p4));
        return v0;
    }

    private static char[] a(char[] p6, int p7, int p8)
    {
        char[] v0_0;
        if (p7 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        char[] v0_1;
        Integer v4_0 = new Object[1];
        v4_0[0] = Integer.valueOf(p7);
        com.a.b.b.cn.a(v0_0, "Invalid minLength: %s", v4_0);
        if (p8 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p8);
        com.a.b.b.cn.a(v0_1, "Invalid padding: %s", v1_1);
        if (p6.length < p7) {
            int v1_2 = (p7 + p8);
            char[] v0_3 = new char[v1_2];
            System.arraycopy(p6, 0, v0_3, 0, Math.min(p6.length, v1_2));
            p6 = v0_3;
        }
        return p6;
    }

    private static varargs char[] a(char[][] p7)
    {
        int v0_0 = 0;
        int v2_0 = 0;
        while (v0_0 < p7.length) {
            v2_0 += p7[v0_0].length;
            v0_0++;
        }
        char[] v3_1 = new char[v2_0];
        int v4_0 = p7.length;
        int v0_1 = 0;
        int v2_1 = 0;
        while (v2_1 < v4_0) {
            int v5_0 = p7[v2_1];
            System.arraycopy(v5_0, 0, v3_1, v0_1, v5_0.length);
            v0_1 += v5_0.length;
            v2_1++;
        }
        return v3_1;
    }

    private static char b(long p2)
    {
        char v0_5;
        if (p2 <= 65535) {
            if (p2 >= 0) {
                v0_5 = ((char) ((int) p2));
            } else {
                v0_5 = 0;
            }
        } else {
            v0_5 = 65535;
        }
        return v0_5;
    }

    private static varargs char b(char[] p3)
    {
        char v0_1;
        int v1 = 1;
        if (p3.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        char v0_2 = p3[0];
        while (v1 < p3.length) {
            if (p3[v1] > v0_2) {
                v0_2 = p3[v1];
            }
            v1++;
        }
        return v0_2;
    }

    private static int b(char[] p2, char p3)
    {
        return com.a.b.l.f.a(p2, p3, 0, p2.length);
    }

    static int b(char[] p2, char p3, int p4, int p5)
    {
        int v0 = (p5 - 1);
        while (v0 >= p4) {
            if (p2[v0] != p3) {
                v0--;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static byte[] b(char p3)
    {
        byte[] v0_1 = new byte[2];
        v0_1[0] = ((byte) (p3 >> 8));
        v0_1[1] = ((byte) p3);
        return v0_1;
    }

    private static int c(char[] p2, char p3)
    {
        return com.a.b.l.f.b(p2, p3, 0, p2.length);
    }

    private static synthetic int c(char[] p1, char p2, int p3, int p4)
    {
        return com.a.b.l.f.a(p1, p2, p3, p4);
    }

    private static varargs java.util.List c(char[] p1)
    {
        com.a.b.l.g v0_2;
        if (p1.length != 0) {
            v0_2 = new com.a.b.l.g(p1);
        } else {
            v0_2 = java.util.Collections.emptyList();
        }
        return v0_2;
    }

    private static synthetic int d(char[] p1, char p2, int p3, int p4)
    {
        return com.a.b.l.f.b(p1, p2, p3, p4);
    }
}
