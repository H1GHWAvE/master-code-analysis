package com.a.b.l;
final class r extends java.util.AbstractList implements java.io.Serializable, java.util.RandomAccess {
    private static final long d;
    final int[] a;
    final int b;
    final int c;

    r(int[] p3)
    {
        this(p3, 0, p3.length);
        return;
    }

    private r(int[] p1, int p2, int p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    private Integer a(int p3)
    {
        com.a.b.b.cn.a(p3, this.size());
        return Integer.valueOf(this.a[(this.b + p3)]);
    }

    private Integer a(int p5, Integer p6)
    {
        com.a.b.b.cn.a(p5, this.size());
        int v1_2 = this.a[(this.b + p5)];
        this.a[(this.b + p5)] = ((Integer) com.a.b.b.cn.a(p6)).intValue();
        return Integer.valueOf(v1_2);
    }

    private int[] a()
    {
        int v0 = this.size();
        int[] v1 = new int[v0];
        System.arraycopy(this.a, this.b, v1, 0, v0);
        return v1;
    }

    public final boolean contains(Object p5)
    {
        if ((!(p5 instanceof Integer)) || (com.a.b.l.q.a(this.a, ((Integer) p5).intValue(), this.b, this.c) == -1)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean equals(Object p8)
    {
        int v0 = 1;
        if (p8 != this) {
            if (!(p8 instanceof com.a.b.l.r)) {
                v0 = super.equals(p8);
            } else {
                int v3 = this.size();
                if (((com.a.b.l.r) p8).size() == v3) {
                    int v2_2 = 0;
                    while (v2_2 < v3) {
                        if (this.a[(this.b + v2_2)] == ((com.a.b.l.r) p8).a[(((com.a.b.l.r) p8).b + v2_2)]) {
                            v2_2++;
                        } else {
                            v0 = 0;
                            break;
                        }
                    }
                } else {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public final synthetic Object get(int p3)
    {
        com.a.b.b.cn.a(p3, this.size());
        return Integer.valueOf(this.a[(this.b + p3)]);
    }

    public final int hashCode()
    {
        int v1_0 = 1;
        int v0 = this.b;
        while (v0 < this.c) {
            v1_0 = ((v1_0 * 31) + com.a.b.l.q.a(this.a[v0]));
            v0++;
        }
        return v1_0;
    }

    public final int indexOf(Object p5)
    {
        int v0_3;
        if (!(p5 instanceof Integer)) {
            v0_3 = -1;
        } else {
            int v0_2 = com.a.b.l.q.a(this.a, ((Integer) p5).intValue(), this.b, this.c);
            if (v0_2 < 0) {
            } else {
                v0_3 = (v0_2 - this.b);
            }
        }
        return v0_3;
    }

    public final boolean isEmpty()
    {
        return 0;
    }

    public final int lastIndexOf(Object p5)
    {
        int v0_3;
        if (!(p5 instanceof Integer)) {
            v0_3 = -1;
        } else {
            int v0_2 = com.a.b.l.q.b(this.a, ((Integer) p5).intValue(), this.b, this.c);
            if (v0_2 < 0) {
            } else {
                v0_3 = (v0_2 - this.b);
            }
        }
        return v0_3;
    }

    public final synthetic Object set(int p5, Object p6)
    {
        com.a.b.b.cn.a(p5, this.size());
        int v1_2 = this.a[(this.b + p5)];
        this.a[(this.b + p5)] = ((Integer) com.a.b.b.cn.a(((Integer) p6))).intValue();
        return Integer.valueOf(v1_2);
    }

    public final int size()
    {
        return (this.c - this.b);
    }

    public final java.util.List subList(int p5, int p6)
    {
        com.a.b.l.r v0_2;
        com.a.b.b.cn.a(p5, p6, this.size());
        if (p5 != p6) {
            v0_2 = new com.a.b.l.r(this.a, (this.b + p5), (this.b + p6));
        } else {
            v0_2 = java.util.Collections.emptyList();
        }
        return v0_2;
    }

    public final String toString()
    {
        StringBuilder v1_1 = new StringBuilder((this.size() * 5));
        v1_1.append(91).append(this.a[this.b]);
        String v0_5 = (this.b + 1);
        while (v0_5 < this.c) {
            v1_1.append(", ").append(this.a[v0_5]);
            v0_5++;
        }
        return v1_1.append(93).toString();
    }
}
