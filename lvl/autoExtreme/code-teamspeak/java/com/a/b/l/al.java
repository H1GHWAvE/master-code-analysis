package com.a.b.l;
public final class al extends java.lang.Number implements java.lang.Comparable {
    public static final com.a.b.l.al a;
    public static final com.a.b.l.al b;
    public static final com.a.b.l.al c;
    private final int d;

    static al()
    {
        com.a.b.l.al.a = com.a.b.l.al.a(0);
        com.a.b.l.al.b = com.a.b.l.al.a(1);
        com.a.b.l.al.c = com.a.b.l.al.a(-1);
        return;
    }

    private al(int p2)
    {
        this.d = (p2 & -1);
        return;
    }

    private static com.a.b.l.al a(int p1)
    {
        return new com.a.b.l.al(p1);
    }

    private static com.a.b.l.al a(long p6)
    {
        com.a.b.l.al v0_1;
        if ((2.1219957905e-314 & p6) != p6) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = Long.valueOf(p6);
        com.a.b.b.cn.a(v0_1, "value (%s) is outside the range for an unsigned integer value", v1_1);
        return com.a.b.l.al.a(((int) p6));
    }

    private com.a.b.l.al a(com.a.b.l.al p3)
    {
        return com.a.b.l.al.a((((com.a.b.l.al) com.a.b.b.cn.a(p3)).d + this.d));
    }

    private static com.a.b.l.al a(String p1)
    {
        return com.a.b.l.al.a(com.a.b.l.am.a(p1, 10));
    }

    private static com.a.b.l.al a(java.math.BigInteger p4)
    {
        com.a.b.l.al v0_2;
        com.a.b.b.cn.a(p4);
        if ((p4.signum() < 0) || (p4.bitLength() > 32)) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = p4;
        com.a.b.b.cn.a(v0_2, "value (%s) is outside the range for an unsigned integer value", v1_1);
        return com.a.b.l.al.a(p4.intValue());
    }

    private java.math.BigInteger a()
    {
        return java.math.BigInteger.valueOf(this.longValue());
    }

    private com.a.b.l.al b(com.a.b.l.al p3)
    {
        return com.a.b.l.al.a((this.d - ((com.a.b.l.al) com.a.b.b.cn.a(p3)).d));
    }

    private static com.a.b.l.al b(String p1)
    {
        return com.a.b.l.al.a(com.a.b.l.am.a(p1, 10));
    }

    private String b()
    {
        return com.a.b.l.am.a(this.d);
    }

    private com.a.b.l.al c(com.a.b.l.al p3)
    {
        return com.a.b.l.al.a((((com.a.b.l.al) com.a.b.b.cn.a(p3)).d * this.d));
    }

    private com.a.b.l.al d(com.a.b.l.al p7)
    {
        return com.a.b.l.al.a(((int) ((((long) this.d) & 2.1219957905e-314) / (((long) ((com.a.b.l.al) com.a.b.b.cn.a(p7)).d) & 2.1219957905e-314))));
    }

    private com.a.b.l.al e(com.a.b.l.al p7)
    {
        return com.a.b.l.al.a(((int) ((((long) this.d) & 2.1219957905e-314) % (((long) ((com.a.b.l.al) com.a.b.b.cn.a(p7)).d) & 2.1219957905e-314))));
    }

    private int f(com.a.b.l.al p3)
    {
        com.a.b.b.cn.a(p3);
        return com.a.b.l.am.a(this.d, p3.d);
    }

    public final synthetic int compareTo(Object p3)
    {
        com.a.b.b.cn.a(((com.a.b.l.al) p3));
        return com.a.b.l.am.a(this.d, ((com.a.b.l.al) p3).d);
    }

    public final double doubleValue()
    {
        return ((double) this.longValue());
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.l.al)) && (this.d == ((com.a.b.l.al) p4).d)) {
            v0 = 1;
        }
        return v0;
    }

    public final float floatValue()
    {
        return ((float) this.longValue());
    }

    public final int hashCode()
    {
        return this.d;
    }

    public final int intValue()
    {
        return this.d;
    }

    public final long longValue()
    {
        return (((long) this.d) & 2.1219957905e-314);
    }

    public final String toString()
    {
        return com.a.b.l.am.a(this.d);
    }
}
