package com.a.b.l;
final enum class c extends java.lang.Enum implements java.util.Comparator {
    public static final enum com.a.b.l.c a;
    private static final synthetic com.a.b.l.c[] b;

    static c()
    {
        com.a.b.l.c.a = new com.a.b.l.c("INSTANCE");
        com.a.b.l.c[] v0_3 = new com.a.b.l.c[1];
        v0_3[0] = com.a.b.l.c.a;
        com.a.b.l.c.b = v0_3;
        return;
    }

    private c(String p2)
    {
        this(p2, 0);
        return;
    }

    private static int a(boolean[] p4, boolean[] p5)
    {
        int v2 = Math.min(p4.length, p5.length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = com.a.b.l.a.a(p4[v1_1], p5[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (p4.length - p5.length);
        return v0_3;
    }

    public static com.a.b.l.c valueOf(String p1)
    {
        return ((com.a.b.l.c) Enum.valueOf(com.a.b.l.c, p1));
    }

    public static com.a.b.l.c[] values()
    {
        return ((com.a.b.l.c[]) com.a.b.l.c.b.clone());
    }

    public final synthetic int compare(Object p5, Object p6)
    {
        int v2 = Math.min(((boolean[]) p5).length, ((boolean[]) p6).length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = com.a.b.l.a.a(((boolean[]) p5)[v1_1], ((boolean[]) p6)[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (((boolean[]) p5).length - ((boolean[]) p6).length);
        return v0_3;
    }
}
