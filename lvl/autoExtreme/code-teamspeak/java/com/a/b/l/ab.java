package com.a.b.l;
final enum class ab extends java.lang.Enum implements java.util.Comparator {
    public static final enum com.a.b.l.ab a;
    private static final synthetic com.a.b.l.ab[] b;

    static ab()
    {
        com.a.b.l.ab.a = new com.a.b.l.ab("INSTANCE");
        com.a.b.l.ab[] v0_3 = new com.a.b.l.ab[1];
        v0_3[0] = com.a.b.l.ab.a;
        com.a.b.l.ab.b = v0_3;
        return;
    }

    private ab(String p2)
    {
        this(p2, 0);
        return;
    }

    private static int a(short[] p4, short[] p5)
    {
        int v2 = Math.min(p4.length, p5.length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = (p4[v1_1] - p5[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (p4.length - p5.length);
        return v0_3;
    }

    public static com.a.b.l.ab valueOf(String p1)
    {
        return ((com.a.b.l.ab) Enum.valueOf(com.a.b.l.ab, p1));
    }

    public static com.a.b.l.ab[] values()
    {
        return ((com.a.b.l.ab[]) com.a.b.l.ab.b.clone());
    }

    public final synthetic int compare(Object p5, Object p6)
    {
        int v2 = Math.min(((short[]) p5).length, ((short[]) p6).length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = (((short[]) p5)[v1_1] - ((short[]) p6)[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (((short[]) p5).length - ((short[]) p6).length);
        return v0_3;
    }
}
