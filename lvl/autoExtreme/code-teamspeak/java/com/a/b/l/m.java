package com.a.b.l;
public final class m {
    public static final int a = 4;

    private m()
    {
        return;
    }

    private static varargs float a(float[] p3)
    {
        float v0_1;
        int v1 = 1;
        if (p3.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        float v0_2 = p3[0];
        while (v1 < p3.length) {
            v0_2 = Math.min(v0_2, p3[v1]);
            v1++;
        }
        return v0_2;
    }

    private static int a(float p1)
    {
        return Float.valueOf(p1).hashCode();
    }

    private static int a(float p1, float p2)
    {
        return Float.compare(p1, p2);
    }

    static int a(float[] p2, float p3, int p4, int p5)
    {
        int v0 = p4;
        while (v0 < p5) {
            if (p2[v0] != p3) {
                v0++;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static int a(float[] p5, float[] p6)
    {
        int v1 = 0;
        com.a.b.b.cn.a(p5, "array");
        com.a.b.b.cn.a(p6, "target");
        if (p6.length != 0) {
            int v0_3 = 0;
            while (v0_3 < ((p5.length - p6.length) + 1)) {
                int v2_3 = 0;
                while (v2_3 < p6.length) {
                    if (p5[(v0_3 + v2_3)] != p6[v2_3]) {
                        v0_3++;
                    } else {
                        v2_3++;
                    }
                }
                v1 = v0_3;
            }
            v1 = -1;
        }
        return v1;
    }

    private static com.a.b.b.ak a()
    {
        return com.a.b.l.o.a;
    }

    private static Float a(String p1)
    {
        NumberFormatException v0_4;
        if (!com.a.b.l.i.b.matcher(p1).matches()) {
            v0_4 = 0;
        } else {
            try {
                v0_4 = Float.valueOf(Float.parseFloat(p1));
            } catch (NumberFormatException v0) {
            }
        }
        return v0_4;
    }

    private static varargs String a(String p4, float[] p5)
    {
        String v0_6;
        com.a.b.b.cn.a(p4);
        if (p5.length != 0) {
            StringBuilder v1_1 = new StringBuilder((p5.length * 12));
            v1_1.append(p5[0]);
            String v0_5 = 1;
            while (v0_5 < p5.length) {
                v1_1.append(p4).append(p5[v0_5]);
                v0_5++;
            }
            v0_6 = v1_1.toString();
        } else {
            v0_6 = "";
        }
        return v0_6;
    }

    private static boolean a(float[] p4, float p5)
    {
        int v0 = 0;
        int v1 = 0;
        while (v1 < p4.length) {
            if (p4[v1] != p5) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    private static float[] a(java.util.Collection p5)
    {
        float[] v0_1;
        int v2 = 0;
        if (!(p5 instanceof com.a.b.l.n)) {
            Object[] v3_0 = p5.toArray();
            int v4_0 = v3_0.length;
            float[] v1_0 = new float[v4_0];
            while (v2 < v4_0) {
                v1_0[v2] = ((Number) com.a.b.b.cn.a(v3_0[v2])).floatValue();
                v2++;
            }
            v0_1 = v1_0;
        } else {
            float[] v1_1 = ((com.a.b.l.n) p5).size();
            v0_1 = new float[v1_1];
            System.arraycopy(((com.a.b.l.n) p5).a, ((com.a.b.l.n) p5).b, v0_1, 0, v1_1);
        }
        return v0_1;
    }

    private static float[] a(float[] p3, int p4)
    {
        float[] v0 = new float[p4];
        System.arraycopy(p3, 0, v0, 0, Math.min(p3.length, p4));
        return v0;
    }

    private static float[] a(float[] p6, int p7, int p8)
    {
        float[] v0_0;
        if (p7 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        float[] v0_1;
        Integer v4_0 = new Object[1];
        v4_0[0] = Integer.valueOf(p7);
        com.a.b.b.cn.a(v0_0, "Invalid minLength: %s", v4_0);
        if (p8 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p8);
        com.a.b.b.cn.a(v0_1, "Invalid padding: %s", v1_1);
        if (p6.length < p7) {
            int v1_2 = (p7 + p8);
            float[] v0_3 = new float[v1_2];
            System.arraycopy(p6, 0, v0_3, 0, Math.min(p6.length, v1_2));
            p6 = v0_3;
        }
        return p6;
    }

    private static varargs float[] a(float[][] p7)
    {
        int v0_0 = 0;
        int v2_0 = 0;
        while (v0_0 < p7.length) {
            v2_0 += p7[v0_0].length;
            v0_0++;
        }
        float[] v3_1 = new float[v2_0];
        int v4_0 = p7.length;
        int v0_1 = 0;
        int v2_1 = 0;
        while (v2_1 < v4_0) {
            int v5_0 = p7[v2_1];
            System.arraycopy(v5_0, 0, v3_1, v0_1, v5_0.length);
            v0_1 += v5_0.length;
            v2_1++;
        }
        return v3_1;
    }

    private static varargs float b(float[] p3)
    {
        float v0_1;
        int v1 = 1;
        if (p3.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        float v0_2 = p3[0];
        while (v1 < p3.length) {
            v0_2 = Math.max(v0_2, p3[v1]);
            v1++;
        }
        return v0_2;
    }

    private static int b(float[] p2, float p3)
    {
        return com.a.b.l.m.a(p2, p3, 0, p2.length);
    }

    static int b(float[] p2, float p3, int p4, int p5)
    {
        int v0 = (p5 - 1);
        while (v0 >= p4) {
            if (p2[v0] != p3) {
                v0--;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static java.util.Comparator b()
    {
        return com.a.b.l.p.a;
    }

    private static boolean b(float p4)
    {
        int v2_2;
        int v0_0 = 1;
        if (-8388608 >= p4) {
            v2_2 = 0;
        } else {
            v2_2 = 1;
        }
        if (p4 >= 2139095040) {
            v0_0 = 0;
        }
        return (v0_0 & v2_2);
    }

    private static int c(float[] p2, float p3)
    {
        return com.a.b.l.m.b(p2, p3, 0, p2.length);
    }

    private static synthetic int c(float[] p1, float p2, int p3, int p4)
    {
        return com.a.b.l.m.a(p1, p2, p3, p4);
    }

    private static varargs java.util.List c(float[] p1)
    {
        com.a.b.l.n v0_2;
        if (p1.length != 0) {
            v0_2 = new com.a.b.l.n(p1);
        } else {
            v0_2 = java.util.Collections.emptyList();
        }
        return v0_2;
    }

    private static synthetic int d(float[] p1, float p2, int p3, int p4)
    {
        return com.a.b.l.m.b(p1, p2, p3, p4);
    }
}
