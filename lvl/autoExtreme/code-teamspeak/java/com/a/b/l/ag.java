package com.a.b.l;
public final class ag {
    public static final byte a = 0x-80;
    public static final byte b = 0x-1;
    private static final int c = 255;

    private ag()
    {
        return;
    }

    private static byte a(long p4)
    {
        if ((p4 >> 8) == 0) {
            return ((byte) ((int) p4));
        } else {
            throw new IllegalArgumentException(new StringBuilder(34).append("Out of range: ").append(p4).toString());
        }
    }

    private static byte a(String p4)
    {
        String v0_2 = Integer.parseInt(((String) com.a.b.b.cn.a(p4)), 10);
        if ((v0_2 >> 8) != 0) {
            throw new NumberFormatException(new StringBuilder(25).append("out of range: ").append(v0_2).toString());
        } else {
            return ((byte) v0_2);
        }
    }

    private static varargs byte a(byte[] p3)
    {
        int v0_1;
        int v1 = 1;
        if (p3.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        int v0_3 = (p3[0] & 255);
        while (v1 < p3.length) {
            int v2_3 = (p3[v1] & 255);
            if (v2_3 < v0_3) {
                v0_3 = v2_3;
            }
            v1++;
        }
        return ((byte) v0_3);
    }

    private static int a(byte p1)
    {
        return (p1 & 255);
    }

    public static int a(byte p2, byte p3)
    {
        return ((p2 & 255) - (p3 & 255));
    }

    private static varargs String a(String p10, byte[] p11)
    {
        String v0_6;
        com.a.b.b.cn.a(p10);
        if (p11.length != 0) {
            StringBuilder v2_1 = new StringBuilder((p11.length * (p10.length() + 3)));
            v2_1.append((p11[0] & 255));
            String v0_5 = 1;
            while (v0_5 < p11.length) {
                StringBuilder v3_3 = v2_1.append(p10);
                String v4_0 = p11[v0_5];
                Object[] v6 = new Object[1];
                v6[0] = Integer.valueOf(10);
                com.a.b.b.cn.a(1, "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX", v6);
                v3_3.append(Integer.toString((v4_0 & 255), 10));
                v0_5++;
            }
            v0_6 = v2_1.toString();
        } else {
            v0_6 = "";
        }
        return v0_6;
    }

    private static java.util.Comparator a()
    {
        return com.a.b.l.ah.b;
    }

    private static byte b(long p2)
    {
        byte v0_5;
        if (p2 <= 255) {
            if (p2 >= 0) {
                v0_5 = ((byte) ((int) p2));
            } else {
                v0_5 = 0;
            }
        } else {
            v0_5 = -1;
        }
        return v0_5;
    }

    private static byte b(String p4)
    {
        String v0_2 = Integer.parseInt(((String) com.a.b.b.cn.a(p4)), 10);
        if ((v0_2 >> 8) != 0) {
            throw new NumberFormatException(new StringBuilder(25).append("out of range: ").append(v0_2).toString());
        } else {
            return ((byte) v0_2);
        }
    }

    private static varargs byte b(byte[] p3)
    {
        int v0_1;
        int v1 = 1;
        if (p3.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        int v0_3 = (p3[0] & 255);
        while (v1 < p3.length) {
            int v2_3 = (p3[v1] & 255);
            if (v2_3 > v0_3) {
                v0_3 = v2_3;
            }
            v1++;
        }
        return ((byte) v0_3);
    }

    private static String b(byte p6)
    {
        Object[] v1 = new Object[1];
        v1[0] = Integer.valueOf(10);
        com.a.b.b.cn.a(1, "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX", v1);
        return Integer.toString((p6 & 255), 10);
    }

    private static java.util.Comparator b()
    {
        return com.a.b.l.ai.a;
    }

    private static String c(byte p6)
    {
        Object[] v1 = new Object[1];
        v1[0] = Integer.valueOf(10);
        com.a.b.b.cn.a(1, "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX", v1);
        return Integer.toString((p6 & 255), 10);
    }
}
