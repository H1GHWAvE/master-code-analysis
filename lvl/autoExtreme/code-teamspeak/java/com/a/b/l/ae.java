package com.a.b.l;
public final class ae {
    public static final byte a = 0x40;

    private ae()
    {
        return;
    }

    private static byte a(long p4)
    {
        if (((long) ((byte) ((int) p4))) == p4) {
            return ((byte) ((int) p4));
        } else {
            throw new IllegalArgumentException(new StringBuilder(34).append("Out of range: ").append(p4).toString());
        }
    }

    private static varargs byte a(byte[] p3)
    {
        byte v0_1;
        int v1 = 1;
        if (p3.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        byte v0_2 = p3[0];
        while (v1 < p3.length) {
            if (p3[v1] < v0_2) {
                v0_2 = p3[v1];
            }
            v1++;
        }
        return v0_2;
    }

    private static int a(byte p1, byte p2)
    {
        return (p1 - p2);
    }

    private static varargs String a(String p4, byte[] p5)
    {
        String v0_6;
        com.a.b.b.cn.a(p4);
        if (p5.length != 0) {
            StringBuilder v1_1 = new StringBuilder((p5.length * 5));
            v1_1.append(p5[0]);
            String v0_5 = 1;
            while (v0_5 < p5.length) {
                v1_1.append(p4).append(p5[v0_5]);
                v0_5++;
            }
            v0_6 = v1_1.toString();
        } else {
            v0_6 = "";
        }
        return v0_6;
    }

    private static java.util.Comparator a()
    {
        return com.a.b.l.af.a;
    }

    private static byte b(long p2)
    {
        byte v0_5;
        if (p2 <= 127) {
            if (p2 >= -128) {
                v0_5 = ((byte) ((int) p2));
            } else {
                v0_5 = -128;
            }
        } else {
            v0_5 = 127;
        }
        return v0_5;
    }

    private static varargs byte b(byte[] p3)
    {
        byte v0_1;
        int v1 = 1;
        if (p3.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        byte v0_2 = p3[0];
        while (v1 < p3.length) {
            if (p3[v1] > v0_2) {
                v0_2 = p3[v1];
            }
            v1++;
        }
        return v0_2;
    }
}
