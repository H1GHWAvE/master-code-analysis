package com.a.b.c;
 class bq extends com.a.b.c.aj implements java.io.Serializable {
    private static final long a = 1;
    final com.a.b.c.bw b;
    final com.a.b.c.bw c;
    final com.a.b.b.au d;
    final com.a.b.b.au e;
    final long f;
    final long g;
    final long h;
    final com.a.b.c.do i;
    final int j;
    final com.a.b.c.dg k;
    final com.a.b.b.ej l;
    final com.a.b.c.ab m;
    transient com.a.b.c.e n;

    bq(com.a.b.c.ao p20)
    {
        this(p20.m, p20.n, p20.k, p20.l, p20.r, p20.q, p20.o, p20.p, p20.j, p20.u, p20.v, p20.y);
        return;
    }

    private bq(com.a.b.c.bw p4, com.a.b.c.bw p5, com.a.b.b.au p6, com.a.b.b.au p7, long p8, long p10, long p12, com.a.b.c.do p14, int p15, com.a.b.c.dg p16, com.a.b.b.ej p17, com.a.b.c.ab p18)
    {
        this.b = p4;
        this.c = p5;
        this.d = p6;
        this.e = p7;
        this.f = p8;
        this.g = p10;
        this.h = p12;
        this.i = p14;
        this.j = p15;
        this.k = p16;
        if ((p17 == com.a.b.b.ej.b()) || (p17 == com.a.b.c.f.d)) {
            p17 = 0;
        }
        this.l = p17;
        this.m = p18;
        return;
    }

    private void a(java.io.ObjectInputStream p7)
    {
        com.a.b.c.bo v0_1;
        p7.defaultReadObject();
        com.a.b.c.f v1 = this.h();
        v1.d();
        if (v1.p != -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "refreshAfterWrite requires a LoadingCache");
        this.n = new com.a.b.c.bo(v1);
        return;
    }

    private Object i()
    {
        return this.n;
    }

    protected final com.a.b.c.e f()
    {
        return this.n;
    }

    final com.a.b.c.f h()
    {
        int v0_3;
        int v1 = 1;
        com.a.b.c.f v3_2 = com.a.b.c.f.a().a(this.b).b(this.c);
        long v4_0 = this.d;
        if (v3_2.q != null) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        int v0_7;
        Object[] v6_0 = new Object[1];
        v6_0[0] = v3_2.q;
        com.a.b.b.cn.b(v0_3, "key equivalence was already set to %s", v6_0);
        v3_2.q = ((com.a.b.b.au) com.a.b.b.cn.a(v4_0));
        long v4_1 = this.e;
        if (v3_2.r != null) {
            v0_7 = 0;
        } else {
            v0_7 = 1;
        }
        int v0_12;
        Object[] v6_1 = new Object[1];
        v6_1[0] = v3_2.r;
        com.a.b.b.cn.b(v0_7, "value equivalence was already set to %s", v6_1);
        v3_2.r = ((com.a.b.b.au) com.a.b.b.cn.a(v4_1));
        com.a.b.c.f v3_3 = v3_2.a(this.j);
        long v4_2 = this.k;
        if (v3_3.s != null) {
            v0_12 = 0;
        } else {
            v0_12 = 1;
        }
        com.a.b.b.cn.b(v0_12);
        v3_3.s = ((com.a.b.c.dg) com.a.b.b.cn.a(v4_2));
        v3_3.f = 0;
        if (this.f > 0) {
            v3_3.a(this.f, java.util.concurrent.TimeUnit.NANOSECONDS);
        }
        if (this.g > 0) {
            v3_3.b(this.g, java.util.concurrent.TimeUnit.NANOSECONDS);
        }
        if (this.i == com.a.b.c.k.a) {
            if (this.h != -1) {
                v3_3.a(this.h);
            }
        } else {
            int v0_22;
            long v4_10 = this.i;
            if (v3_3.k != null) {
                v0_22 = 0;
            } else {
                v0_22 = 1;
            }
            com.a.b.b.cn.b(v0_22);
            if (v3_3.f) {
                int v0_25;
                if (v3_3.i != -1) {
                    v0_25 = 0;
                } else {
                    v0_25 = 1;
                }
                Object[] v6_3 = new Object[1];
                v6_3[0] = Long.valueOf(v3_3.i);
                com.a.b.b.cn.b(v0_25, "weigher can not be combined with maximum size", v6_3);
            }
            v3_3.k = ((com.a.b.c.do) com.a.b.b.cn.a(v4_10));
            if (this.h != -1) {
                v3_3.b(this.h);
            }
        }
        if (this.l != null) {
            int v0_30 = this.l;
            if (v3_3.t != null) {
                v1 = 0;
            }
            com.a.b.b.cn.b(v1);
            v3_3.t = ((com.a.b.b.ej) com.a.b.b.cn.a(v0_30));
        }
        return v3_3;
    }

    protected final bridge synthetic Object k_()
    {
        return this.n;
    }
}
