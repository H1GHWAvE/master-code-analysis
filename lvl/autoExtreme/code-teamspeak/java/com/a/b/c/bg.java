package com.a.b.c;
final class bg extends com.a.b.c.ar {
    final synthetic com.a.b.c.ao c;

    bg(com.a.b.c.ao p1, java.util.concurrent.ConcurrentMap p2)
    {
        this.c = p1;
        this(p1, p2);
        return;
    }

    public final boolean contains(Object p5)
    {
        int v0 = 0;
        if ((p5 instanceof java.util.Map$Entry)) {
            boolean v1_1 = ((java.util.Map$Entry) p5).getKey();
            if (v1_1) {
                boolean v1_2 = this.c.get(v1_1);
                if ((v1_2) && (this.c.l.a(((java.util.Map$Entry) p5).getValue(), v1_2))) {
                    v0 = 1;
                }
            }
        }
        return v0;
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.c.bf(this.c);
    }

    public final boolean remove(Object p5)
    {
        int v0 = 0;
        if ((p5 instanceof java.util.Map$Entry)) {
            boolean v1_1 = ((java.util.Map$Entry) p5).getKey();
            if ((v1_1) && (this.c.remove(v1_1, ((java.util.Map$Entry) p5).getValue()))) {
                v0 = 1;
            }
        }
        return v0;
    }
}
