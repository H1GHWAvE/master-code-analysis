package com.a.b.c;
 class ao extends java.util.AbstractMap implements java.util.concurrent.ConcurrentMap {
    static final java.util.Queue A = None;
    static final int a = 1073741824;
    static final int b = 65536;
    static final int c = 3;
    static final int d = 63;
    static final int e = 16;
    static final java.util.logging.Logger f;
    static final com.a.b.c.cg z;
    java.util.Set B;
    java.util.Collection C;
    java.util.Set D;
    final int g;
    final int h;
    final com.a.b.c.bt[] i;
    final int j;
    final com.a.b.b.au k;
    final com.a.b.b.au l;
    final com.a.b.c.bw m;
    final com.a.b.c.bw n;
    final long o;
    final com.a.b.c.do p;
    final long q;
    final long r;
    final long s;
    final java.util.Queue t;
    final com.a.b.c.dg u;
    final com.a.b.b.ej v;
    final com.a.b.c.aw w;
    final com.a.b.c.c x;
    final com.a.b.c.ab y;

    static ao()
    {
        com.a.b.c.ao.f = java.util.logging.Logger.getLogger(com.a.b.c.ao.getName());
        com.a.b.c.ao.z = new com.a.b.c.ap();
        com.a.b.c.ao.A = new com.a.b.c.aq();
        return;
    }

    ao(com.a.b.c.f p13, com.a.b.c.ab p14)
    {
        com.a.b.c.bt v0_1;
        int v5 = 1;
        com.a.b.c.bt v2_0 = 0;
        int v4 = 0;
        if (p13.h != -1) {
            v0_1 = p13.h;
        } else {
            v0_1 = 4;
        }
        com.a.b.c.bt v0_16;
        this.j = Math.min(v0_1, 65536);
        this.m = p13.b();
        this.n = p13.c();
        this.k = ((com.a.b.b.au) com.a.b.b.ca.a(p13.q, p13.b().a()));
        this.l = ((com.a.b.b.au) com.a.b.b.ca.a(p13.r, p13.c().a()));
        if ((p13.n != 0) && (p13.o != 0)) {
            if (p13.k != null) {
                v0_16 = p13.j;
            } else {
                v0_16 = p13.i;
            }
        } else {
            v0_16 = 0;
        }
        com.a.b.c.bt v0_22;
        this.o = v0_16;
        this.p = ((com.a.b.c.do) com.a.b.b.ca.a(p13.k, com.a.b.c.k.a));
        if (p13.o != -1) {
            v0_22 = p13.o;
        } else {
            v0_22 = 0;
        }
        com.a.b.c.bt v0_25;
        this.q = v0_22;
        if (p13.n != -1) {
            v0_25 = p13.n;
        } else {
            v0_25 = 0;
        }
        this.r = v0_25;
        if (p13.p != -1) {
            v2_0 = p13.p;
        }
        com.a.b.c.bt v0_33;
        this.s = v2_0;
        this.u = ((com.a.b.c.dg) com.a.b.b.ca.a(p13.s, com.a.b.c.j.a));
        if (this.u != com.a.b.c.j.a) {
            v0_33 = new java.util.concurrent.ConcurrentLinkedQueue();
        } else {
            v0_33 = com.a.b.c.ao.A;
        }
        com.a.b.c.bt v0_36;
        this.t = v0_33;
        if ((!this.g()) && (!this.d())) {
            v0_36 = 0;
        } else {
            v0_36 = 1;
        }
        com.a.b.c.bt v0_37;
        if (p13.t == null) {
            if (v0_36 == null) {
                v0_37 = com.a.b.c.f.d;
            } else {
                v0_37 = com.a.b.b.ej.b();
            }
        } else {
            v0_37 = p13.t;
        }
        com.a.b.c.bt v0_40;
        this.v = v0_37;
        if ((!this.f()) && (!this.d())) {
            v0_40 = 0;
        } else {
            v0_40 = 1;
        }
        if ((!this.c()) && (!this.g())) {
            com.a.b.c.bt[] v1_11 = 0;
        } else {
            v1_11 = 1;
        }
        com.a.b.c.bt v0_46;
        this.w = com.a.b.c.aw.a(this.m, v0_40, v1_11);
        this.x = ((com.a.b.c.c) p13.u.a());
        this.y = p14;
        if (p13.g != -1) {
            v0_46 = p13.g;
        } else {
            v0_46 = 16;
        }
        com.a.b.c.bt v0_47 = Math.min(v0_46, 1073741824);
        if ((this.a()) && (!this.b())) {
            v0_47 = Math.min(v0_47, ((int) this.o));
        }
        com.a.b.c.bt[] v1_16 = 1;
        com.a.b.c.bt v2_3 = 0;
        while ((v1_16 < this.j) && ((!this.a()) || (((long) (v1_16 * 20)) <= this.o))) {
            v2_3++;
            v1_16 <<= 1;
        }
        com.a.b.c.bt v0_48;
        this.h = (32 - v2_3);
        this.g = (v1_16 - 1);
        com.a.b.c.bt v2_6 = new com.a.b.c.bt[v1_16];
        this.i = v2_6;
        com.a.b.c.bt v2_7 = (v0_47 / v1_16);
        if ((v2_7 * v1_16) >= v0_47) {
            v0_48 = v2_7;
        } else {
            v0_48 = (v2_7 + 1);
        }
        while (v5 < v0_48) {
            v5 <<= 1;
        }
        if (!this.a()) {
            while (v4 < this.i.length) {
                this.i[v4] = this.a(v5, -1, ((com.a.b.c.c) p13.u.a()));
                v4++;
            }
        } else {
            long v6_5 = (this.o % ((long) v1_16));
            com.a.b.c.bt v0_57 = ((this.o / ((long) v1_16)) + 1);
            while (v4 < this.i.length) {
                com.a.b.c.bt v2_15;
                if (((long) v4) != v6_5) {
                    v2_15 = v0_57;
                } else {
                    v2_15 = (v0_57 - 1);
                }
                this.i[v4] = this.a(v5, v2_15, ((com.a.b.c.c) p13.u.a()));
                v4++;
                v0_57 = v2_15;
            }
        }
        return;
    }

    private com.a.b.c.bs a(Object p3, int p4, com.a.b.c.bs p5)
    {
        com.a.b.c.bt v0 = this.a(p4);
        v0.lock();
        try {
            Throwable v1_0 = v0.a(p3, p4, p5);
            v0.unlock();
            return v1_0;
        } catch (Throwable v1_1) {
            v0.unlock();
            throw v1_1;
        }
    }

    private com.a.b.c.bt a(int p9, long p10, com.a.b.c.c p12)
    {
        return new com.a.b.c.bt(this, p9, p10, p12);
    }

    private com.a.b.c.cg a(com.a.b.c.bs p4, Object p5, int p6)
    {
        return this.n.a(this.a(p4.c()), p4, com.a.b.b.cn.a(p5), p6);
    }

    private java.util.Map a(java.util.Set p8, com.a.b.c.ab p9)
    {
        long v2_0 = 1;
        com.a.b.c.c v0_0 = 0;
        com.a.b.b.cn.a(p9);
        com.a.b.b.cn.a(p8);
        int v3_0 = com.a.b.b.dw.a();
        try {
            java.util.Map v4 = p9.a(p8);
        } catch (java.util.concurrent.TimeUnit v1_3) {
            Thread.currentThread().interrupt();
            throw new java.util.concurrent.ExecutionException(v1_3);
        } catch (com.a.b.c.c v0_1) {
            try {
                throw v0_1;
            } catch (com.a.b.c.c v0_0) {
                if (v2_0 == 0) {
                    this.x.b(v3_0.a(java.util.concurrent.TimeUnit.NANOSECONDS));
                }
                throw v0_0;
            }
        }
        if (v4 != null) {
            v3_0.c();
            java.util.Iterator v5 = v4.entrySet().iterator();
            java.util.concurrent.TimeUnit v1_7 = 0;
            while (v5.hasNext()) {
                com.a.b.c.c v0_8 = ((java.util.Map$Entry) v5.next());
                Object v6 = v0_8.getKey();
                com.a.b.c.c v0_9 = v0_8.getValue();
                if ((v6 != null) && (v0_9 != null)) {
                    this.put(v6, v0_9);
                } else {
                    v1_7 = 1;
                }
            }
            if (v1_7 == null) {
                this.x.a(v3_0.a(java.util.concurrent.TimeUnit.NANOSECONDS));
                return v4;
            } else {
                this.x.b(v3_0.a(java.util.concurrent.TimeUnit.NANOSECONDS));
                java.util.concurrent.TimeUnit v1_11 = String.valueOf(String.valueOf(p9));
                throw new com.a.b.c.af(new StringBuilder((v1_11.length() + 42)).append(v1_11).append(" returned null keys or values from loadAll").toString());
            }
        } else {
            this.x.b(v3_0.a(java.util.concurrent.TimeUnit.NANOSECONDS));
            java.util.concurrent.TimeUnit v1_17 = String.valueOf(String.valueOf(p9));
            throw new com.a.b.c.af(new StringBuilder((v1_17.length() + 31)).append(v1_17).append(" returned null map from loadAll").toString());
        }
    }

    static void a(com.a.b.c.bs p1)
    {
        com.a.b.c.br v0 = com.a.b.c.br.a;
        p1.a(v0);
        p1.b(v0);
        return;
    }

    static void a(com.a.b.c.bs p0, com.a.b.c.bs p1)
    {
        p0.a(p1);
        p1.b(p0);
        return;
    }

    private void a(com.a.b.c.cg p4)
    {
        Object v0_0 = p4.b();
        int v1 = v0_0.c();
        this.a(v1).a(v0_0.d(), v1, p4);
        return;
    }

    private static int b(int p3)
    {
        int v0_2 = (((p3 << 15) ^ -12931) + p3);
        int v0_3 = (v0_2 ^ (v0_2 >> 10));
        int v0_4 = (v0_3 + (v0_3 << 3));
        int v0_5 = (v0_4 ^ (v0_4 >> 6));
        int v0_6 = (v0_5 + ((v0_5 << 2) + (v0_5 << 14)));
        return (v0_6 ^ (v0_6 >> 16));
    }

    private com.a.b.d.jt b(Iterable p7)
    {
        int v0_0 = 0;
        java.util.LinkedHashMap v2 = com.a.b.d.sz.d();
        com.a.b.c.c v3_0 = p7.iterator();
        int v1_0 = 0;
        while (v3_0.hasNext()) {
            Object v4_1 = v3_0.next();
            Object v5 = this.get(v4_1);
            if (v5 != null) {
                v2.put(v4_1, v5);
                v1_0++;
            } else {
                v0_0++;
            }
        }
        this.x.a(v1_0);
        this.x.b(v0_0);
        return com.a.b.d.jt.a(v2);
    }

    private Object b(Object p4)
    {
        Object v0_1 = this.a(com.a.b.b.cn.a(p4));
        Object v0_2 = this.a(v0_1).b(p4, v0_1);
        if (v0_2 != null) {
            this.x.a(1);
        } else {
            this.x.b(1);
        }
        return v0_2;
    }

    static void b(com.a.b.c.bs p1)
    {
        com.a.b.c.br v0 = com.a.b.c.br.a;
        p1.c(v0);
        p1.d(v0);
        return;
    }

    static void b(com.a.b.c.bs p0, com.a.b.c.bs p1)
    {
        p0.c(p1);
        p1.d(p0);
        return;
    }

    private boolean b(com.a.b.c.bs p3, long p4)
    {
        int v0_3;
        if (this.a(p3.c()).a(p3, p4) == null) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private com.a.b.c.bs c(com.a.b.c.bs p2, com.a.b.c.bs p3)
    {
        return this.a(p2.c()).a(p2, p3);
    }

    private Object c(com.a.b.c.bs p5, long p6)
    {
        Object v0 = 0;
        if (p5.d() != null) {
            Object v1_2 = p5.a().get();
            if ((v1_2 != null) && (!this.a(p5, p6))) {
                v0 = v1_2;
            }
        }
        return v0;
    }

    private Object c(Object p2)
    {
        return this.a(p2, this.y);
    }

    private void c(com.a.b.c.bs p3)
    {
        int v0 = p3.c();
        this.a(v0).a(p3, v0);
        return;
    }

    private void c(Iterable p3)
    {
        java.util.Iterator v0 = p3.iterator();
        while (v0.hasNext()) {
            this.remove(v0.next());
        }
        return;
    }

    private static com.a.b.c.bt[] c(int p1)
    {
        com.a.b.c.bt[] v0 = new com.a.b.c.bt[p1];
        return v0;
    }

    private com.a.b.c.bs d(Object p3)
    {
        com.a.b.c.bs v0_1;
        if (p3 != null) {
            com.a.b.c.bs v0_0 = this.a(p3);
            v0_1 = this.a(v0_0).a(p3, v0_0);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private void e(Object p5)
    {
        int v0_1 = this.a(com.a.b.b.cn.a(p5));
        this.a(v0_1).a(p5, v0_1, this.y, 0);
        return;
    }

    static com.a.b.c.cg j()
    {
        return com.a.b.c.ao.z;
    }

    static com.a.b.c.bs k()
    {
        return com.a.b.c.br.a;
    }

    static java.util.Queue l()
    {
        return com.a.b.c.ao.A;
    }

    private boolean n()
    {
        if ((!this.c()) && (!this.d())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private boolean o()
    {
        return this.c();
    }

    private boolean p()
    {
        return this.d();
    }

    private boolean q()
    {
        if ((!this.g()) && (!this.d())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private boolean r()
    {
        if ((!this.c()) && (!this.g())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private boolean s()
    {
        if ((!this.f()) && (!this.d())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private void t()
    {
        while(true) {
            Throwable v0_2 = ((com.a.b.c.dk) this.t.poll());
            if (v0_2 == null) {
                break;
            }
            try {
                this.u.a(v0_2);
            } catch (Throwable v0_3) {
                com.a.b.c.ao.f.log(java.util.logging.Level.WARNING, "Exception thrown by removal listener", v0_3);
            }
        }
        return;
    }

    private void u()
    {
        com.a.b.c.bt[] v1 = this.i;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].b();
            v0++;
        }
        return;
    }

    final int a(Object p4)
    {
        int v0_1 = this.k.a(p4);
        int v0_2 = (v0_1 + ((v0_1 << 15) ^ -12931));
        int v0_3 = (v0_2 ^ (v0_2 >> 10));
        int v0_4 = (v0_3 + (v0_3 << 3));
        int v0_5 = (v0_4 ^ (v0_4 >> 6));
        int v0_6 = (v0_5 + ((v0_5 << 2) + (v0_5 << 14)));
        return (v0_6 ^ (v0_6 >> 16));
    }

    final com.a.b.c.bt a(int p4)
    {
        return this.i[((p4 >> this.h) & this.g)];
    }

    final com.a.b.d.jt a(Iterable p10)
    {
        Object v0_0 = 0;
        com.a.b.c.c v3_0 = com.a.b.d.sz.d();
        int v2_1 = new java.util.LinkedHashSet();
        com.a.b.c.af v4_0 = p10.iterator();
        com.a.b.c.c v1_0 = 0;
        while (v4_0.hasNext()) {
            Object v5_8 = v4_0.next();
            StringBuilder v6_5 = this.get(v5_8);
            if (!v3_0.containsKey(v5_8)) {
                v3_0.put(v5_8, v6_5);
                if (v6_5 != null) {
                    v1_0++;
                } else {
                    v0_0++;
                    v2_1.add(v5_8);
                }
            }
        }
        try {
            int v2_2;
            if (v2_1.isEmpty()) {
                v2_2 = v0_0;
                Object v0_2 = com.a.b.d.jt.a(v3_0);
                this.x.a(v1_0);
                v1_0 = this.x;
                v1_0.b(v2_2);
                return v0_2;
            } else {
                try {
                    com.a.b.c.af v4_3 = this.a(v2_1, this.y);
                    Object v5_1 = v2_1.iterator();
                } catch (com.a.b.c.af v4) {
                    com.a.b.c.af v4_6 = v2_1.iterator();
                    v2_2 = v0_0;
                }
                while (v5_1.hasNext()) {
                    StringBuilder v6_1 = v5_1.next();
                    String v7_0 = v4_3.get(v6_1);
                    if (v7_0 != null) {
                        v3_0.put(v6_1, v7_0);
                    } else {
                        Object v5_3 = String.valueOf(String.valueOf(v6_1));
                        throw new com.a.b.c.af(new StringBuilder((v5_3.length() + 37)).append("loadAll failed to return a value for ").append(v5_3).toString());
                    }
                }
                v2_2 = v0_0;
                v0_2 = com.a.b.d.jt.a(v3_0);
                this.x.a(v1_0);
                v1_0 = this.x;
                v1_0.b(v2_2);
                return v0_2;
            }
        } catch (int v2_3) {
            v2_2 = v0_0;
            Object v0_3 = v2_3;
        } catch (Object v0_3) {
        }
        this.x.a(v1_0);
        this.x.b(v2_2);
        throw v0_3;
    }

    final Object a(Object p3, com.a.b.c.ab p4)
    {
        Object v0_1 = this.a(com.a.b.b.cn.a(p3));
        return this.a(v0_1).a(p3, v0_1, p4);
    }

    final boolean a()
    {
        int v0_2;
        if (this.o < 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    final boolean a(com.a.b.c.bs p7, long p8)
    {
        int v0 = 1;
        com.a.b.b.cn.a(p7);
        if (((!this.d()) || ((p8 - p7.e()) < this.q)) && ((!this.c()) || ((p8 - p7.h()) < this.r))) {
            v0 = 0;
        }
        return v0;
    }

    final boolean b()
    {
        int v0_1;
        if (this.p == com.a.b.c.k.a) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    final boolean c()
    {
        int v0_2;
        if (this.r <= 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public void clear()
    {
        com.a.b.c.bt[] v4 = this.i;
        int v5 = v4.length;
        int v3 = 0;
        while (v3 < v5) {
            com.a.b.c.bt v6 = v4[v3];
            if (v6.b != 0) {
                v6.lock();
                try {
                    java.util.concurrent.atomic.AtomicReferenceArray v7 = v6.f;
                    int v2_0 = 0;
                } catch (int v0_21) {
                    v6.unlock();
                    v6.c();
                    throw v0_21;
                }
                while (v2_0 < v7.length()) {
                    int v0_19 = ((com.a.b.c.bs) v7.get(v2_0));
                    while (v0_19 != 0) {
                        if (v0_19.a().d()) {
                            v6.a(v0_19, com.a.b.c.da.a);
                        }
                        v0_19 = v0_19.b();
                    }
                    v2_0++;
                }
                int v0_2 = 0;
                while (v0_2 < v7.length()) {
                    v7.set(v0_2, 0);
                    v0_2++;
                }
                if (v6.a.h()) {
                    while (v6.h.poll() != null) {
                    }
                }
                if (v6.a.i()) {
                    while (v6.i.poll() != null) {
                    }
                }
                v6.l.clear();
                v6.m.clear();
                v6.k.set(0);
                v6.d = (v6.d + 1);
                v6.b = 0;
                v6.unlock();
                v6.c();
            }
            v3++;
        }
        return;
    }

    public boolean containsKey(Object p3)
    {
        boolean v0_1;
        if (p3 != null) {
            boolean v0_0 = this.a(p3);
            v0_1 = this.a(v0_0).c(p3, v0_0);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public boolean containsValue(Object p21)
    {
        int v4_5;
        if (p21 != null) {
            long v14 = this.v.a();
            com.a.b.c.bt[] v11 = this.i;
            int v10 = 0;
            long v12 = -1;
            while (v10 < 3) {
                int v16 = v11.length;
                long v8_1 = 0;
                int v6_1 = 0;
                while (v6_1 < v16) {
                    com.a.b.c.bt v7 = v11[v6_1];
                    java.util.concurrent.atomic.AtomicReferenceArray v17 = v7.f;
                    int v5 = 0;
                    while (v5 < v17.length()) {
                        int v4_13 = ((com.a.b.c.bs) v17.get(v5));
                        while (v4_13 != 0) {
                            boolean v18_0 = v7.a(v4_13, v14);
                            if ((!v18_0) || (!this.l.a(p21, v18_0))) {
                                v4_13 = v4_13.b();
                            } else {
                                v4_5 = 1;
                            }
                        }
                        v5++;
                    }
                    v8_1 += ((long) v7.d);
                    v6_1++;
                }
                if (v8_1 == v12) {
                    break;
                }
                v10++;
                v12 = v8_1;
            }
            v4_5 = 0;
        } else {
            v4_5 = 0;
        }
        return v4_5;
    }

    final boolean d()
    {
        int v0_2;
        if (this.q <= 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    final boolean e()
    {
        int v0_2;
        if (this.s <= 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public java.util.Set entrySet()
    {
        com.a.b.c.bg v0_0 = this.D;
        if (v0_0 == null) {
            v0_0 = new com.a.b.c.bg(this, this);
            this.D = v0_0;
        }
        return v0_0;
    }

    final boolean f()
    {
        if ((!this.d()) && (!this.a())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    final boolean g()
    {
        if ((!this.c()) && (!this.e())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public Object get(Object p3)
    {
        Object v0_1;
        if (p3 != null) {
            Object v0_0 = this.a(p3);
            v0_1 = this.a(v0_0).b(p3, v0_0);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    final boolean h()
    {
        int v0_1;
        if (this.m == com.a.b.c.bw.a) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    final boolean i()
    {
        int v0_1;
        if (this.n == com.a.b.c.bw.a) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean isEmpty()
    {
        int v1 = 0;
        com.a.b.c.bt[] v6 = this.i;
        int v0_0 = 0;
        long v2 = 0;
        while (v0_0 < v6.length) {
            if (v6[v0_0].b == 0) {
                v2 += ((long) v6[v0_0].d);
                v0_0++;
            }
            return v1;
        }
        if (v2 != 0) {
            int v0_2 = 0;
            while (v0_2 < v6.length) {
                if (v6[v0_2].b == 0) {
                    v2 -= ((long) v6[v0_2].d);
                    v0_2++;
                }
                return v1;
            }
            if (v2 != 0) {
                return v1;
            }
        }
        v1 = 1;
        return v1;
    }

    public java.util.Set keySet()
    {
        com.a.b.c.bj v0_0 = this.B;
        if (v0_0 == null) {
            v0_0 = new com.a.b.c.bj(this, this);
            this.B = v0_0;
        }
        return v0_0;
    }

    final long m()
    {
        com.a.b.c.bt[] v1 = this.i;
        long v2 = 0;
        int v0 = 0;
        while (v0 < v1.length) {
            v2 += ((long) v1[v0].b);
            v0++;
        }
        return v2;
    }

    public Object put(Object p4, Object p5)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.b.cn.a(p5);
        Object v0_0 = this.a(p4);
        return this.a(v0_0).a(p4, v0_0, p5, 0);
    }

    public void putAll(java.util.Map p4)
    {
        java.util.Iterator v1 = p4.entrySet().iterator();
        while (v1.hasNext()) {
            Object v0_3 = ((java.util.Map$Entry) v1.next());
            this.put(v0_3.getKey(), v0_3.getValue());
        }
        return;
    }

    public Object putIfAbsent(Object p4, Object p5)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.b.cn.a(p5);
        Object v0_0 = this.a(p4);
        return this.a(v0_0).a(p4, v0_0, p5, 1);
    }

    public Object remove(Object p3)
    {
        Object v0_1;
        if (p3 != null) {
            Object v0_0 = this.a(p3);
            v0_1 = this.a(v0_0).d(p3, v0_0);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public boolean remove(Object p3, Object p4)
    {
        if ((p3 != null) && (p4 != null)) {
            boolean v0_0 = this.a(p3);
            boolean v0_1 = this.a(v0_0).b(p3, v0_0, p4);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public Object replace(Object p3, Object p4)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p4);
        Object v0_0 = this.a(p3);
        return this.a(v0_0).a(p3, v0_0, p4);
    }

    public boolean replace(Object p3, Object p4, Object p5)
    {
        boolean v0_1;
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p5);
        if (p4 != null) {
            boolean v0_0 = this.a(p3);
            v0_1 = this.a(v0_0).a(p3, v0_0, p4, p5);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public int size()
    {
        return com.a.b.l.q.b(this.m());
    }

    public java.util.Collection values()
    {
        com.a.b.c.ch v0_0 = this.C;
        if (v0_0 == null) {
            v0_0 = new com.a.b.c.ch(this, this);
            this.C = v0_0;
        }
        return v0_0;
    }
}
