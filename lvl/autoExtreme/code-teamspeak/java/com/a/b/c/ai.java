package com.a.b.c;
public final class ai {
    final long a;
    final long b;
    final long c;
    final long d;
    final long e;
    final long f;

    public ai(long p4, long p6, long p8, long p10, long p12, long p14)
    {
        int v0_2;
        if (p4 < 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        int v0_5;
        com.a.b.b.cn.a(v0_2);
        if (p6 < 0) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        int v0_8;
        com.a.b.b.cn.a(v0_5);
        if (p8 < 0) {
            v0_8 = 0;
        } else {
            v0_8 = 1;
        }
        int v0_11;
        com.a.b.b.cn.a(v0_8);
        if (p10 < 0) {
            v0_11 = 0;
        } else {
            v0_11 = 1;
        }
        int v0_14;
        com.a.b.b.cn.a(v0_11);
        if (p12 < 0) {
            v0_14 = 0;
        } else {
            v0_14 = 1;
        }
        int v0_17;
        com.a.b.b.cn.a(v0_14);
        if (p14 < 0) {
            v0_17 = 0;
        } else {
            v0_17 = 1;
        }
        com.a.b.b.cn.a(v0_17);
        this.a = p4;
        this.b = p6;
        this.c = p8;
        this.d = p10;
        this.e = p12;
        this.f = p14;
        return;
    }

    private long a()
    {
        return (this.a + this.b);
    }

    private com.a.b.c.ai a(com.a.b.c.ai p21)
    {
        return new com.a.b.c.ai(Math.max(0, (this.a - p21.a)), Math.max(0, (this.b - p21.b)), Math.max(0, (this.c - p21.c)), Math.max(0, (this.d - p21.d)), Math.max(0, (this.e - p21.e)), Math.max(0, (this.f - p21.f)));
    }

    private long b()
    {
        return this.a;
    }

    private com.a.b.c.ai b(com.a.b.c.ai p19)
    {
        return new com.a.b.c.ai((this.a + p19.a), (this.b + p19.b), (this.c + p19.c), (this.d + p19.d), (this.e + p19.e), (this.f + p19.f));
    }

    private double c()
    {
        double v0_2;
        double v0_0 = this.a();
        if (v0_0 != 0) {
            v0_2 = (((double) this.a) / ((double) v0_0));
        } else {
            v0_2 = 1.0;
        }
        return v0_2;
    }

    private long d()
    {
        return this.b;
    }

    private double e()
    {
        double v0_2;
        double v0_0 = this.a();
        if (v0_0 != 0) {
            v0_2 = (((double) this.b) / ((double) v0_0));
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private long f()
    {
        return (this.c + this.d);
    }

    private long g()
    {
        return this.c;
    }

    private long h()
    {
        return this.d;
    }

    private double i()
    {
        double v0_3;
        double v0_1 = (this.c + this.d);
        if (v0_1 != 0) {
            v0_3 = (((double) this.d) / ((double) v0_1));
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private long j()
    {
        return this.e;
    }

    private double k()
    {
        double v0_3;
        double v0_1 = (this.c + this.d);
        if (v0_1 != 0) {
            v0_3 = (((double) this.e) / ((double) v0_1));
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private long l()
    {
        return this.f;
    }

    public final boolean equals(Object p7)
    {
        int v0 = 0;
        if (((p7 instanceof com.a.b.c.ai)) && ((this.a == ((com.a.b.c.ai) p7).a) && ((this.b == ((com.a.b.c.ai) p7).b) && ((this.c == ((com.a.b.c.ai) p7).c) && ((this.d == ((com.a.b.c.ai) p7).d) && ((this.e == ((com.a.b.c.ai) p7).e) && (this.f == ((com.a.b.c.ai) p7).f))))))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[6];
        v0_1[0] = Long.valueOf(this.a);
        v0_1[1] = Long.valueOf(this.b);
        v0_1[2] = Long.valueOf(this.c);
        v0_1[3] = Long.valueOf(this.d);
        v0_1[4] = Long.valueOf(this.e);
        v0_1[5] = Long.valueOf(this.f);
        return java.util.Arrays.hashCode(v0_1);
    }

    public final String toString()
    {
        return com.a.b.b.ca.a(this).a("hitCount", this.a).a("missCount", this.b).a("loadSuccessCount", this.c).a("loadExceptionCount", this.d).a("totalLoadTime", this.e).a("evictionCount", this.f).toString();
    }
}
