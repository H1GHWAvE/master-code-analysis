package com.a.b.c;
public abstract class d extends com.a.b.c.a implements com.a.b.c.an {

    protected d()
    {
        return;
    }

    public final Object b(Object p3)
    {
        try {
            return this.f(p3);
        } catch (Throwable v0_1) {
            throw new com.a.b.n.a.gq(v0_1.getCause());
        }
    }

    public final com.a.b.d.jt c(Iterable p5)
    {
        com.a.b.d.jt v0_0 = com.a.b.d.sz.d();
        java.util.Iterator v1 = p5.iterator();
        while (v1.hasNext()) {
            Object v2_1 = v1.next();
            if (!v0_0.containsKey(v2_1)) {
                v0_0.put(v2_1, this.f(v2_1));
            }
        }
        return com.a.b.d.jt.a(v0_0);
    }

    public final void c(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final Object e(Object p2)
    {
        return this.b(p2);
    }
}
