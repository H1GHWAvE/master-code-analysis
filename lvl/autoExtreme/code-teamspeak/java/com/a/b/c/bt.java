package com.a.b.c;
final class bt extends java.util.concurrent.locks.ReentrantLock {
    final com.a.b.c.ao a;
    volatile int b;
    long c;
    int d;
    int e;
    volatile java.util.concurrent.atomic.AtomicReferenceArray f;
    final long g;
    final ref.ReferenceQueue h;
    final ref.ReferenceQueue i;
    final java.util.Queue j;
    final java.util.concurrent.atomic.AtomicInteger k;
    final java.util.Queue l;
    final java.util.Queue m;
    final com.a.b.c.c n;

    bt(com.a.b.c.ao p8, int p9, long p10, com.a.b.c.c p12)
    {
        ref.ReferenceQueue v1_0 = 0;
        this.k = new java.util.concurrent.atomic.AtomicInteger();
        this.a = p8;
        this.g = p10;
        this.n = ((com.a.b.c.c) com.a.b.b.cn.a(p12));
        java.util.Queue v0_4 = com.a.b.c.bt.a(p9);
        this.e = ((v0_4.length() * 3) / 4);
        if ((!this.a.b()) && (((long) this.e) == this.g)) {
            this.e = (this.e + 1);
        }
        java.util.Queue v0_6;
        this.f = v0_4;
        if (!p8.h()) {
            v0_6 = 0;
        } else {
            v0_6 = new ref.ReferenceQueue();
        }
        this.h = v0_6;
        if (p8.i()) {
            v1_0 = new ref.ReferenceQueue();
        }
        java.util.Queue v0_10;
        this.i = v1_0;
        if (!p8.f()) {
            v0_10 = com.a.b.c.ao.l();
        } else {
            v0_10 = new java.util.concurrent.ConcurrentLinkedQueue();
        }
        java.util.Queue v0_13;
        this.j = v0_10;
        if (!p8.c()) {
            v0_13 = com.a.b.c.ao.l();
        } else {
            v0_13 = new com.a.b.c.cq();
        }
        java.util.Queue v0_16;
        this.l = v0_13;
        if (!p8.f()) {
            v0_16 = com.a.b.c.ao.l();
        } else {
            v0_16 = new com.a.b.c.at();
        }
        this.m = v0_16;
        return;
    }

    private com.a.b.c.bl a(Object p9, int p10, boolean p11)
    {
        this.lock();
        try {
            long v2_0 = this.a.v.a();
            this.d(v2_0);
            com.a.b.c.cg v4_0 = this.f;
            int v5 = (p10 & (v4_0.length() - 1));
            com.a.b.c.bl v0_5 = ((com.a.b.c.bs) v4_0.get(v5));
            com.a.b.c.bs v1_0 = v0_5;
        } catch (com.a.b.c.bl v0_14) {
            this.unlock();
            this.c();
            throw v0_14;
        }
        while (v1_0 != null) {
            long v6_0 = v1_0.d();
            if ((v1_0.c() != p10) || ((v6_0 == 0) || (!this.a.k.a(p9, v6_0)))) {
                v1_0 = v1_0.b();
            } else {
                com.a.b.c.cg v4_1 = v1_0.a();
                if ((!v4_1.c()) && ((!p11) || ((v2_0 - v1_0.h()) >= this.a.s))) {
                    this.d = (this.d + 1);
                    com.a.b.c.bl v0_7 = new com.a.b.c.bl(v4_1);
                    v1_0.a(v0_7);
                    this.unlock();
                    this.c();
                } else {
                    this.unlock();
                    this.c();
                    v0_7 = 0;
                }
            }
            return v0_7;
        }
        this.d = (this.d + 1);
        com.a.b.c.bs v1_4 = new com.a.b.c.bl();
        com.a.b.c.bl v0_6 = this.a(p9, p10, v0_5);
        v0_6.a(v1_4);
        v4_0.set(v5, v0_6);
        this.unlock();
        this.c();
        v0_7 = v1_4;
        return v0_7;
    }

    private com.a.b.c.bs a(com.a.b.c.bs p2, com.a.b.c.bs p3, Object p4, com.a.b.c.cg p5, com.a.b.c.da p6)
    {
        this.a(p4, p5, p6);
        this.l.remove(p3);
        this.m.remove(p3);
        if (!p5.c()) {
            p2 = this.b(p2, p3);
        } else {
            p5.a(0);
        }
        return p2;
    }

    private com.a.b.c.bs a(Object p4, int p5, long p6)
    {
        com.a.b.c.bs v0 = 0;
        com.a.b.c.bs v1 = this.a(p4, p5);
        if (v1 != null) {
            if (!this.a.a(v1, p6)) {
                v0 = v1;
            } else {
                this.a(p6);
            }
        }
        return v0;
    }

    private Object a(com.a.b.c.bs p6, Object p7, int p8, Object p9, long p10, com.a.b.c.ab p12)
    {
        if ((this.a.e()) && (((p10 - p6.h()) > this.a.s) && (!p6.a().c()))) {
            Object v0_8 = this.a(p7, p8, p12, 1);
            if (v0_8 != null) {
                p9 = v0_8;
            }
        }
        return p9;
    }

    private Object a(com.a.b.c.bs p6, Object p7, com.a.b.c.cg p8)
    {
        if (p8.c()) {
            com.a.b.c.af v0_2;
            if (Thread.holdsLock(p6)) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
            String v4_0 = new Object[1];
            v4_0[0] = p7;
            com.a.b.b.cn.b(v0_2, "Recursive load of: %s", v4_0);
            try {
                com.a.b.c.af v0_3 = p8.e();
            } catch (com.a.b.c.af v0_6) {
                this.n.b(1);
                throw v0_6;
            }
            if (v0_3 != null) {
                this.b(p6, this.a.v.a());
                this.n.b(1);
                return v0_3;
            } else {
                com.a.b.c.c v2_6 = String.valueOf(String.valueOf(p7));
                throw new com.a.b.c.af(new StringBuilder((v2_6.length() + 35)).append("CacheLoader returned null for key ").append(v2_6).append(".").toString());
            }
        } else {
            throw new AssertionError();
        }
    }

    private Object a(Object p2, int p3, com.a.b.c.bl p4, com.a.b.c.ab p5)
    {
        return this.a(p2, p3, p4, p4.a(p2, p5));
    }

    private static java.util.concurrent.atomic.AtomicReferenceArray a(int p1)
    {
        return new java.util.concurrent.atomic.AtomicReferenceArray(p1);
    }

    private void a(long p2)
    {
        if (this.tryLock()) {
            try {
                this.b(p2);
                this.unlock();
            } catch (Throwable v0_1) {
                this.unlock();
                throw v0_1;
            }
        }
        return;
    }

    private void a(com.a.b.c.bs p2)
    {
        this.a(p2, com.a.b.c.da.c);
        this.l.remove(p2);
        this.m.remove(p2);
        return;
    }

    private void a(com.a.b.c.bs p6, int p7, long p8)
    {
        this.k();
        this.c = (this.c + ((long) p7));
        if (this.a.d()) {
            p6.a(p8);
        }
        if (this.a.g()) {
            p6.b(p8);
        }
        this.m.add(p6);
        this.l.add(p6);
        return;
    }

    private void a(com.a.b.c.bs p8, Object p9, long p10)
    {
        com.a.b.c.cg v0 = p8.a();
        com.a.b.b.cn.b(1, "Weights must be non-negative");
        p8.a(this.a.n.a(this, p8, p9, 1));
        this.k();
        this.c = (this.c + 1);
        if (this.a.d()) {
            p8.a(p10);
        }
        if (this.a.g()) {
            p8.b(p10);
        }
        this.m.add(p8);
        this.l.add(p8);
        v0.a(p9);
        return;
    }

    private void a(Object p5, com.a.b.c.cg p6, com.a.b.c.da p7)
    {
        this.c = (this.c - ((long) p6.a()));
        if (p7.a()) {
            this.n.a();
        }
        if (this.a.t != com.a.b.c.ao.A) {
            this.a.t.offer(new com.a.b.c.dk(p5, p6.get(), p7));
        }
        return;
    }

    private void a(java.util.concurrent.atomic.AtomicReferenceArray p5)
    {
        this.e = ((p5.length() * 3) / 4);
        if ((!this.a.b()) && (((long) this.e) == this.g)) {
            this.e = (this.e + 1);
        }
        this.f = p5;
        return;
    }

    private boolean a(com.a.b.c.bs p9, int p10, com.a.b.c.da p11)
    {
        java.util.concurrent.atomic.AtomicReferenceArray v6 = this.f;
        int v7 = (p10 & (v6.length() - 1));
        int v1_1 = ((com.a.b.c.bs) v6.get(v7));
        com.a.b.c.bs v2 = v1_1;
        while (v2 != null) {
            if (v2 != p9) {
                v2 = v2.b();
            } else {
                this.d = (this.d + 1);
                int v1_3 = (this.b - 1);
                v6.set(v7, this.a(v1_1, v2, v2.d(), v2.a(), p11));
                this.b = v1_3;
                int v0_2 = 1;
            }
            return v0_2;
        }
        v0_2 = 0;
        return v0_2;
    }

    private boolean a(Object p10)
    {
        try {
            int v0_3;
            if (this.b == 0) {
                this.a();
                v0_3 = 0;
            } else {
                long v4 = this.a.v.a();
                java.util.concurrent.atomic.AtomicReferenceArray v3 = this.f;
                int v6 = v3.length();
                int v2 = 0;
                while (v2 < v6) {
                    int v0_5 = ((com.a.b.c.bs) v3.get(v2));
                    while (v0_5 != 0) {
                        boolean v7_0 = this.a(v0_5, v4);
                        if ((!v7_0) || (!this.a.l.a(p10, v7_0))) {
                            v0_5 = v0_5.b();
                        } else {
                            this.a();
                            v0_3 = 1;
                        }
                    }
                    v2++;
                }
            }
        } catch (int v0_7) {
            this.a();
            throw v0_7;
        }
        return v0_3;
    }

    private boolean a(Object p8, int p9, com.a.b.c.bl p10)
    {
        this.lock();
        try {
            java.util.concurrent.atomic.AtomicReferenceArray v3 = this.f;
            int v4 = (p9 & (v3.length() - 1));
            int v0_3 = ((com.a.b.c.bs) v3.get(v4));
            com.a.b.c.bs v2 = v0_3;
        } catch (int v0_7) {
            this.unlock();
            this.c();
            throw v0_7;
        }
        while (v2 != null) {
            com.a.b.c.cg v5_0 = v2.d();
            if ((v2.c() != p9) || ((v5_0 == null) || (!this.a.k.a(p8, v5_0)))) {
                v2 = v2.b();
            } else {
                if (v2.a() != p10) {
                    this.unlock();
                    this.c();
                    int v0_4 = 0;
                } else {
                    if (!p10.d()) {
                        v3.set(v4, this.b(v0_3, v2));
                    } else {
                        v2.a(p10.a);
                    }
                    this.unlock();
                    this.c();
                    v0_4 = 1;
                }
            }
            return v0_4;
        }
        this.unlock();
        this.c();
        v0_4 = 0;
        return v0_4;
    }

    private boolean a(Object p12, int p13, com.a.b.c.bl p14, Object p15)
    {
        this.lock();
        try {
            long v6 = this.a.v.a();
            this.d(v6);
            int v3 = (this.b + 1);
        } catch (int v0_19) {
            this.unlock();
            this.c();
            throw v0_19;
        }
        if (v3 > this.e) {
            this.n();
            v3 = (this.b + 1);
        }
        Object v5_0 = this.f;
        com.a.b.c.cg v8_0 = (p13 & (v5_0.length() - 1));
        int v0_8 = ((com.a.b.c.bs) v5_0.get(v8_0));
        com.a.b.c.bs v4 = v0_8;
        while (v4 != null) {
            boolean v9_0 = v4.d();
            if ((v4.c() != p13) || ((!v9_0) || (!this.a.k.a(p12, v9_0)))) {
                v4 = v4.b();
            } else {
                int v0_11 = v4.a();
                Object v5_1 = v0_11.get();
                if ((p14 != v0_11) && ((v5_1 != null) || (v0_11 == com.a.b.c.ao.z))) {
                    this.a(p12, new com.a.b.c.co(p15, 0), com.a.b.c.da.b);
                    this.unlock();
                    this.c();
                    int v0_10 = 0;
                } else {
                    int v0_17;
                    this.d = (this.d + 1);
                    if (!p14.d()) {
                        v0_17 = v3;
                    } else {
                        int v0_18;
                        if (v5_1 != null) {
                            v0_18 = com.a.b.c.da.b;
                        } else {
                            v0_18 = com.a.b.c.da.c;
                        }
                        this.a(p12, p14, v0_18);
                        v0_17 = (v3 - 1);
                    }
                    this.a(v4, p15, v6);
                    this.b = v0_17;
                    this.l();
                    this.unlock();
                    this.c();
                    v0_10 = 1;
                }
            }
            return v0_10;
        }
        this.d = (this.d + 1);
        int v0_9 = this.a(p12, p13, v0_8);
        this.a(v0_9, p15, v6);
        v5_0.set(v8_0, v0_9);
        this.b = v3;
        this.l();
        this.unlock();
        this.c();
        v0_10 = 1;
        return v0_10;
    }

    private com.a.b.c.bs b(int p3)
    {
        com.a.b.c.bs v0_0 = this.f;
        return ((com.a.b.c.bs) v0_0.get(((v0_0.length() - 1) & p3)));
    }

    private com.a.b.c.bs b(com.a.b.c.bs p5, com.a.b.c.bs p6)
    {
        int v2 = this.b;
        int v1_0 = p6.b();
        while (p5 != p6) {
            int v1_1;
            int v0_0 = this.a(p5, v1_0);
            if (v0_0 == 0) {
                this.a(p5);
                v1_1 = (v2 - 1);
                v0_0 = v1_0;
            } else {
                v1_1 = v2;
            }
            p5 = p5.b();
            v2 = v1_1;
            v1_0 = v0_0;
        }
        this.b = v2;
        return v1_0;
    }

    private com.a.b.n.a.dp b(Object p7, int p8, com.a.b.c.bl p9, com.a.b.c.ab p10)
    {
        com.a.b.n.a.dp v5 = p9.a(p7, p10);
        v5.a(new com.a.b.c.bu(this, p7, p8, p9, v5), com.a.b.n.a.ef.a);
        return v5;
    }

    private Object b(Object p19, int p20, com.a.b.c.ab p21)
    {
        this.lock();
        try {
            long v10 = this.a.v.a();
            this.d(v10);
            int v12 = (this.b - 1);
            java.util.concurrent.atomic.AtomicReferenceArray v13 = this.f;
            int v14 = (p20 & (v13.length() - 1));
            Throwable v4_6 = ((com.a.b.c.bs) v13.get(v14));
            int v6_0 = v4_6;
        } catch (Throwable v4_10) {
            this.unlock();
            this.c();
            throw v4_10;
        }
        while (v6_0 != 0) {
            Object v15 = v6_0.d();
            if ((v6_0.c() != p20) || ((v15 == null) || (!this.a.k.a(p19, v15)))) {
                v6_0 = v6_0.b();
            } else {
                com.a.b.c.cg v9_4 = v6_0.a();
                if (!v9_4.c()) {
                    com.a.b.c.da v7_3 = v9_4.get();
                    if (v7_3 != null) {
                        if (!this.a.a(v6_0, v10)) {
                            this.c(v6_0, v10);
                            this.n.a(1);
                            this.unlock();
                            this.c();
                            Throwable v4_8 = v7_3;
                            return v4_8;
                        } else {
                            this.a(v15, v9_4, com.a.b.c.da.d);
                        }
                    } else {
                        this.a(v15, v9_4, com.a.b.c.da.c);
                    }
                    this.l.remove(v6_0);
                    this.m.remove(v6_0);
                    this.b = v12;
                    com.a.b.c.da v7_1 = 1;
                    com.a.b.c.cg v8_1 = v9_4;
                    Throwable v4_9;
                    int v5_2;
                    if (v7_1 == null) {
                        v4_9 = 0;
                        v5_2 = v6_0;
                    } else {
                        int v5_4 = new com.a.b.c.bl();
                        if (v6_0 != 0) {
                            v6_0.a(v5_4);
                            v4_9 = v5_4;
                            v5_2 = v6_0;
                        } else {
                            Throwable v4_11 = this.a(p19, p20, v4_6);
                            v4_11.a(v5_4);
                            v13.set(v14, v4_11);
                            v5_2 = v4_11;
                            v4_9 = v5_4;
                        }
                    }
                    this.unlock();
                    this.c();
                    if (v7_1 == null) {
                        v4_8 = this.a(v5_2, p19, v8_1);
                    } else {
                        try {
                        } catch (Throwable v4_13) {
                            this.n.b(1);
                            throw v4_13;
                        }
                        v4_8 = this.a(p19, p20, v4_9, v4_9.a(p19, p21));
                        this.n.b(1);
                    }
                } else {
                    v7_1 = 0;
                    v8_1 = v9_4;
                }
            }
            return v4_8;
        }
        v8_1 = 0;
        v7_1 = 1;
    }

    private void b(long p4)
    {
        this.k();
        do {
            AssertionError v0_2 = ((com.a.b.c.bs) this.l.peek());
            if ((v0_2 != null) && (this.a.a(v0_2, p4))) {
            }
            do {
                AssertionError v0_5 = ((com.a.b.c.bs) this.m.peek());
                if ((v0_5 == null) || (!this.a.a(v0_5, p4))) {
                    return;
                } else {
                }
            } while(this.a(v0_5, v0_5.c(), com.a.b.c.da.d));
            throw new AssertionError();
        } while(this.a(v0_2, v0_2.c(), com.a.b.c.da.d));
        throw new AssertionError();
    }

    private void b(com.a.b.c.bs p3, long p4)
    {
        if (this.a.d()) {
            p3.a(p4);
        }
        this.j.add(p3);
        return;
    }

    private void c(long p2)
    {
        this.d(p2);
        return;
    }

    private void c(com.a.b.c.bs p3, long p4)
    {
        if (this.a.d()) {
            p3.a(p4);
        }
        this.m.add(p3);
        return;
    }

    private void d()
    {
        if (this.tryLock()) {
            try {
                this.e();
                this.unlock();
            } catch (Throwable v0_1) {
                this.unlock();
                throw v0_1;
            }
        }
        return;
    }

    private void d(long p4)
    {
        if (this.tryLock()) {
            try {
                this.e();
                this.b(p4);
                this.k.set(0);
                this.unlock();
            } catch (Throwable v0_2) {
                this.unlock();
                throw v0_2;
            }
        }
        return;
    }

    private void e()
    {
        int v2 = 0;
        if (this.a.h()) {
            com.a.b.c.cg v1_0 = 0;
            while(true) {
                com.a.b.c.cg v0_3 = this.h.poll();
                if (v0_3 == null) {
                    break;
                }
                com.a.b.c.cg v0_4 = ((com.a.b.c.bs) v0_3);
                Object v3_0 = this.a;
                int v4_0 = v0_4.c();
                v3_0.a(v4_0).a(v0_4, v4_0);
                com.a.b.c.cg v0_5 = (v1_0 + 1);
                if (v0_5 == 16) {
                    break;
                }
                v1_0 = v0_5;
            }
        }
        if (this.a.i()) {
            do {
                com.a.b.c.cg v0_9 = this.i.poll();
                if (v0_9 == null) {
                    break;
                }
                com.a.b.c.cg v0_10 = ((com.a.b.c.cg) v0_9);
                com.a.b.c.cg v1_1 = this.a;
                Object v3_2 = v0_10.b();
                int v4_1 = v3_2.c();
                v1_1.a(v4_1).a(v3_2.d(), v4_1, v0_10);
                v2++;
            } while(v2 != 16);
        }
        return;
    }

    private void f()
    {
        int v1_0 = 0;
        while(true) {
            int v0_2 = this.h.poll();
            if (v0_2 == 0) {
                break;
            }
            int v0_3 = ((com.a.b.c.bs) v0_2);
            com.a.b.c.bt v2_0 = this.a;
            int v3 = v0_3.c();
            v2_0.a(v3).a(v0_3, v3);
            int v0_4 = (v1_0 + 1);
            if (v0_4 == 16) {
                break;
            }
            v1_0 = v0_4;
        }
        return;
    }

    private void g()
    {
        int v1_0 = 0;
        while(true) {
            int v0_2 = this.i.poll();
            if (v0_2 == 0) {
                break;
            }
            int v0_3 = ((com.a.b.c.cg) v0_2);
            com.a.b.c.bt v2_0 = this.a;
            Object v3_0 = v0_3.b();
            int v4 = v3_0.c();
            v2_0.a(v4).a(v3_0.d(), v4, v0_3);
            int v0_4 = (v1_0 + 1);
            if (v0_4 == 16) {
                break;
            }
            v1_0 = v0_4;
        }
        return;
    }

    private void h()
    {
        if (this.a.h()) {
            while (this.h.poll() != null) {
            }
        }
        if (this.a.i()) {
            while (this.i.poll() != null) {
            }
        }
        return;
    }

    private void i()
    {
        while (this.h.poll() != null) {
        }
        return;
    }

    private void j()
    {
        while (this.i.poll() != null) {
        }
        return;
    }

    private void k()
    {
        while(true) {
            com.a.b.c.bs v0_2 = ((com.a.b.c.bs) this.j.poll());
            if (v0_2 == null) {
                break;
            }
            if (this.m.contains(v0_2)) {
                this.m.add(v0_2);
            }
        }
        return;
    }

    private void l()
    {
        if (this.a.a()) {
            this.k();
            while (this.c > this.g) {
                int v1_0 = this.m.iterator();
                while (v1_0.hasNext()) {
                    AssertionError v0_9 = ((com.a.b.c.bs) v1_0.next());
                    if (v0_9.a().a() > 0) {
                        if (!this.a(v0_9, v0_9.c(), com.a.b.c.da.e)) {
                            throw new AssertionError();
                        }
                    }
                }
                throw new AssertionError();
            }
        }
        return;
    }

    private com.a.b.c.bs m()
    {
        java.util.Iterator v1 = this.m.iterator();
        while (v1.hasNext()) {
            AssertionError v0_5 = ((com.a.b.c.bs) v1.next());
            if (v0_5.a().a() > 0) {
                return v0_5;
            }
        }
        throw new AssertionError();
    }

    private void n()
    {
        java.util.concurrent.atomic.AtomicReferenceArray v7 = this.f;
        int v8 = v7.length();
        if (v8 < 1073741824) {
            int v5 = this.b;
            java.util.concurrent.atomic.AtomicReferenceArray v9 = com.a.b.c.bt.a((v8 << 1));
            this.e = ((v9.length() * 3) / 4);
            int v10 = (v9.length() - 1);
            int v6 = 0;
            while (v6 < v8) {
                int v2_0;
                int v0_8 = ((com.a.b.c.bs) v7.get(v6));
                if (v0_8 == 0) {
                    v2_0 = v5;
                } else {
                    int v2_1 = v0_8.b();
                    int v4_0 = (v0_8.c() & v10);
                    if (v2_1 != 0) {
                        int v1_1 = v0_8;
                        while (v2_1 != 0) {
                            com.a.b.c.bs v3_2 = (v2_1.c() & v10);
                            if (v3_2 == v4_0) {
                                v3_2 = v4_0;
                            } else {
                                v1_1 = v2_1;
                            }
                            v2_1 = v2_1.b();
                            v4_0 = v3_2;
                        }
                        v9.set(v4_0, v1_1);
                        com.a.b.c.bs v3_0 = v0_8;
                        v2_0 = v5;
                        while (v3_0 != v1_1) {
                            int v0_13;
                            int v4_1 = (v3_0.c() & v10);
                            int v0_12 = this.a(v3_0, ((com.a.b.c.bs) v9.get(v4_1)));
                            if (v0_12 == 0) {
                                this.a(v3_0);
                                v0_13 = (v2_0 - 1);
                            } else {
                                v9.set(v4_1, v0_12);
                                v0_13 = v2_0;
                            }
                            v3_0 = v3_0.b();
                            v2_0 = v0_13;
                        }
                    } else {
                        v9.set(v4_0, v0_8);
                        v2_0 = v5;
                    }
                }
                v6++;
                v5 = v2_0;
            }
            this.f = v9;
            this.b = v5;
        }
        return;
    }

    private void o()
    {
        if (this.b != 0) {
            this.lock();
            try {
                java.util.concurrent.atomic.AtomicReferenceArray v3 = this.f;
                int v2 = 0;
            } catch (int v0_20) {
                this.unlock();
                this.c();
                throw v0_20;
            }
            while (v2 < v3.length()) {
                int v0_18 = ((com.a.b.c.bs) v3.get(v2));
                while (v0_18 != 0) {
                    if (v0_18.a().d()) {
                        this.a(v0_18, com.a.b.c.da.a);
                    }
                    v0_18 = v0_18.b();
                }
                v2++;
            }
            int v0_2 = 0;
            while (v0_2 < v3.length()) {
                v3.set(v0_2, 0);
                v0_2++;
            }
            if (this.a.h()) {
                while (this.h.poll() != null) {
                }
            }
            if (this.a.i()) {
                while (this.i.poll() != null) {
                }
            }
            this.l.clear();
            this.m.clear();
            this.k.set(0);
            this.d = (this.d + 1);
            this.b = 0;
            this.unlock();
            this.c();
        }
        return;
    }

    private void p()
    {
        this.c();
        return;
    }

    final com.a.b.c.bs a(com.a.b.c.bs p5, com.a.b.c.bs p6)
    {
        com.a.b.c.bs v0_0 = 0;
        if (p5.d() != null) {
            com.a.b.c.cg v1_1 = p5.a();
            Object v2 = v1_1.get();
            if ((v2 != null) || (!v1_1.d())) {
                v0_0 = this.a.w.a(this, p5, p6);
                v0_0.a(v1_1.a(this.i, v2, v0_0));
            }
        }
        return v0_0;
    }

    final com.a.b.c.bs a(Object p4, int p5)
    {
        com.a.b.c.bs v0_0 = this.f;
        com.a.b.c.bs v0_2 = ((com.a.b.c.bs) v0_0.get(((v0_0.length() - 1) & p5)));
        while (v0_2 != null) {
            if (v0_2.c() == p5) {
                boolean v1_4 = v0_2.d();
                if (v1_4) {
                    if (this.a.k.a(p4, v1_4)) {
                        return v0_2;
                    }
                } else {
                    this.d();
                }
            }
            v0_2 = v0_2.b();
        }
        v0_2 = 0;
        return v0_2;
    }

    final com.a.b.c.bs a(Object p3, int p4, com.a.b.c.bs p5)
    {
        return this.a.w.a(this, com.a.b.b.cn.a(p3), p4, p5);
    }

    final Object a(com.a.b.c.bs p5, long p6)
    {
        Object v0 = 0;
        if (p5.d() != null) {
            Object v1_2 = p5.a().get();
            if (v1_2 != null) {
                if (!this.a.a(p5, p6)) {
                    v0 = v1_2;
                } else {
                    this.a(p6);
                }
            } else {
                this.d();
            }
        } else {
            this.d();
        }
        return v0;
    }

    final Object a(Object p10, int p11, com.a.b.c.ab p12)
    {
        com.a.b.b.cn.a(p10);
        com.a.b.b.cn.a(p12);
        try {
            Object v0_4;
            if (this.b == 0) {
                v0_4 = this.b(p10, p11, p12);
                this.a();
            } else {
                boolean v2_0 = this.a(p10, p11);
                if (!v2_0) {
                } else {
                    long v6 = this.a.v.a();
                    Object v5 = this.a(v2_0, v6);
                    if (v5 == null) {
                        Object v0_3 = v2_0.a();
                        if (!v0_3.c()) {
                        } else {
                            v0_4 = this.a(v2_0, p10, v0_3);
                            this.a();
                        }
                    } else {
                        this.b(v2_0, v6);
                        this.n.a(1);
                        v0_4 = this.a(v2_0, p10, p11, v5, v6, p12);
                        this.a();
                    }
                }
            }
        } catch (Object v0_8) {
            this.a();
            throw v0_8;
        } catch (Object v0_5) {
            com.a.b.n.a.gq v1_1 = v0_5;
            Object v0_6 = v1_1.getCause();
            if (!(v0_6 instanceof Error)) {
                if (!(v0_6 instanceof RuntimeException)) {
                    throw v1_1;
                } else {
                    throw new com.a.b.n.a.gq(v0_6);
                }
            } else {
                throw new com.a.b.n.a.bt(((Error) v0_6));
            }
        }
        return v0_4;
    }

    final Object a(Object p8, int p9, com.a.b.c.ab p10, boolean p11)
    {
        Throwable v0_3;
        com.a.b.c.bl v4 = this.a(p8, p9, p11);
        if (v4 != null) {
            com.a.b.n.a.dp v5 = v4.a(p8, p10);
            v5.a(new com.a.b.c.bu(this, p8, p9, v4, v5), com.a.b.n.a.ef.a);
            if (v5.isDone()) {
                try {
                    v0_3 = com.a.b.n.a.gs.a(v5);
                } catch (Throwable v0) {
                }
                return v0_3;
            }
            v0_3 = 0;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    final Object a(Object p6, int p7, com.a.b.c.bl p8, com.a.b.n.a.dp p9)
    {
        try {
            com.a.b.c.c v1_0 = com.a.b.n.a.gs.a(p9);
        } catch (com.a.b.c.c v0_4) {
            if (v1_0 == null) {
                this.n.b(p8.f());
                this.a(p6, p7, p8);
            }
            throw v0_4;
        }
        if (v1_0 != null) {
            this.n.a(p8.f());
            this.a(p6, p7, p8, v1_0);
            if (v1_0 == null) {
                this.n.b(p8.f());
                this.a(p6, p7, p8);
            }
            return v1_0;
        } else {
            long v2_3 = String.valueOf(String.valueOf(p6));
            throw new com.a.b.c.af(new StringBuilder((v2_3.length() + 35)).append("CacheLoader returned null for key ").append(v2_3).append(".").toString());
        }
    }

    final Object a(Object p12, int p13, Object p14)
    {
        this.lock();
        try {
            long v8 = this.a.v.a();
            this.d(v8);
            java.util.concurrent.atomic.AtomicReferenceArray v7 = this.f;
            int v10 = (p13 & (v7.length() - 1));
            int v1_1 = ((com.a.b.c.bs) v7.get(v10));
            com.a.b.c.bs v2 = v1_1;
        } catch (int v0_14) {
            this.unlock();
            this.c();
            throw v0_14;
        }
        while (v2 != null) {
            Object v3 = v2.d();
            if ((v2.c() != p13) || ((v3 == null) || (!this.a.k.a(p12, v3)))) {
                v2 = v2.b();
            } else {
                com.a.b.c.cg v4 = v2.a();
                int v0_4 = v4.get();
                if (v0_4 != 0) {
                    this.d = (this.d + 1);
                    this.a(p12, v4, com.a.b.c.da.b);
                    this.a(v2, p14, v8);
                    this.l();
                    this.unlock();
                    this.c();
                } else {
                    if (v4.d()) {
                        this.d = (this.d + 1);
                        int v1_6 = (this.b - 1);
                        v7.set(v10, this.a(v1_1, v2, v3, v4, com.a.b.c.da.c));
                        this.b = v1_6;
                    }
                    this.unlock();
                    this.c();
                    v0_4 = 0;
                }
            }
            return v0_4;
        }
        this.unlock();
        this.c();
        v0_4 = 0;
        return v0_4;
    }

    final Object a(Object p10, int p11, Object p12, boolean p13)
    {
        this.lock();
        try {
            long v4 = this.a.v.a();
            this.d(v4);
        } catch (com.a.b.c.da v0_18) {
            this.unlock();
            this.c();
            throw v0_18;
        }
        if ((this.b + 1) > this.e) {
            this.n();
        }
        com.a.b.c.cg v3_0 = this.f;
        int v6 = (p11 & (v3_0.length() - 1));
        com.a.b.c.da v0_7 = ((com.a.b.c.bs) v3_0.get(v6));
        com.a.b.c.bs v2_1 = v0_7;
        while (v2_1 != null) {
            boolean v7_0 = v2_1.d();
            if ((v2_1.c() != p11) || ((!v7_0) || (!this.a.k.a(p10, v7_0)))) {
                v2_1 = v2_1.b();
            } else {
                com.a.b.c.cg v3_1 = v2_1.a();
                com.a.b.c.da v0_11 = v3_1.get();
                if (v0_11 != null) {
                    if (!p13) {
                        this.d = (this.d + 1);
                        this.a(p10, v3_1, com.a.b.c.da.b);
                        this.a(v2_1, p12, v4);
                        this.l();
                        this.unlock();
                        this.c();
                    } else {
                        this.c(v2_1, v4);
                        this.unlock();
                        this.c();
                    }
                } else {
                    com.a.b.c.da v0_16;
                    this.d = (this.d + 1);
                    if (!v3_1.d()) {
                        this.a(v2_1, p12, v4);
                        v0_16 = (this.b + 1);
                    } else {
                        this.a(p10, v3_1, com.a.b.c.da.c);
                        this.a(v2_1, p12, v4);
                        v0_16 = this.b;
                    }
                    this.b = v0_16;
                    this.l();
                    this.unlock();
                    this.c();
                    v0_11 = 0;
                }
            }
            return v0_11;
        }
        this.d = (this.d + 1);
        com.a.b.c.da v0_8 = this.a(p10, p11, v0_7);
        this.a(v0_8, p12, v4);
        v3_0.set(v6, v0_8);
        this.b = (this.b + 1);
        this.l();
        this.unlock();
        this.c();
        v0_11 = 0;
        return v0_11;
    }

    final void a()
    {
        if ((this.k.incrementAndGet() & 63) == 0) {
            this.b();
        }
        return;
    }

    final void a(com.a.b.c.bs p3, com.a.b.c.da p4)
    {
        Object v0 = p3.d();
        p3.c();
        this.a(v0, p3.a(), p4);
        return;
    }

    final boolean a(com.a.b.c.bs p9, int p10)
    {
        this.lock();
        try {
            java.util.concurrent.atomic.AtomicReferenceArray v6 = this.f;
            int v7 = (p10 & (v6.length() - 1));
            int v1_1 = ((com.a.b.c.bs) v6.get(v7));
            com.a.b.c.bs v2 = v1_1;
        } catch (int v0_3) {
            this.unlock();
            this.c();
            throw v0_3;
        }
        while (v2 != null) {
            if (v2 != p9) {
                v2 = v2.b();
            } else {
                this.d = (this.d + 1);
                int v1_3 = (this.b - 1);
                v6.set(v7, this.a(v1_1, v2, v2.d(), v2.a(), com.a.b.c.da.c));
                this.b = v1_3;
                this.unlock();
                this.c();
                int v0_2 = 1;
            }
            return v0_2;
        }
        this.unlock();
        this.c();
        v0_2 = 0;
        return v0_2;
    }

    final boolean a(Object p9, int p10, com.a.b.c.cg p11)
    {
        int v0_0 = 0;
        this.lock();
        try {
            java.util.concurrent.atomic.AtomicReferenceArray v6 = this.f;
            int v7 = (p10 & (v6.length() - 1));
            boolean v1_3 = ((com.a.b.c.bs) v6.get(v7));
            com.a.b.c.bs v2 = v1_3;
        } catch (int v0_1) {
            this.unlock();
            if (!this.isHeldByCurrentThread()) {
                this.c();
            }
            throw v0_1;
        }
        while (v2 != null) {
            Object v3 = v2.d();
            if ((v2.c() != p10) || ((v3 == null) || (!this.a.k.a(p9, v3)))) {
                v2 = v2.b();
            } else {
                if (v2.a() != p11) {
                    this.unlock();
                    if (!this.isHeldByCurrentThread()) {
                        this.c();
                    }
                } else {
                    this.d = (this.d + 1);
                    boolean v1_8 = (this.b - 1);
                    v6.set(v7, this.a(v1_3, v2, v3, p11, com.a.b.c.da.c));
                    this.b = v1_8;
                    this.unlock();
                    if (!this.isHeldByCurrentThread()) {
                        this.c();
                    }
                    v0_0 = 1;
                }
            }
            return v0_0;
        }
        this.unlock();
        if (!this.isHeldByCurrentThread()) {
            this.c();
        }
        return v0_0;
    }

    final boolean a(Object p12, int p13, Object p14, Object p15)
    {
        this.lock();
        try {
            long v8 = this.a.v.a();
            this.d(v8);
            java.util.concurrent.atomic.AtomicReferenceArray v7 = this.f;
            int v10 = (p13 & (v7.length() - 1));
            int v1_1 = ((com.a.b.c.bs) v7.get(v10));
            com.a.b.c.bs v2 = v1_1;
        } catch (int v0_19) {
            this.unlock();
            this.c();
            throw v0_19;
        }
        while (v2 != null) {
            Object v3 = v2.d();
            if ((v2.c() != p13) || ((v3 == null) || (!this.a.k.a(p12, v3)))) {
                v2 = v2.b();
            } else {
                com.a.b.c.cg v4 = v2.a();
                int v0_9 = v4.get();
                if (v0_9 != 0) {
                    if (!this.a.l.a(p14, v0_9)) {
                        this.c(v2, v8);
                        this.unlock();
                        this.c();
                        int v0_4 = 0;
                    } else {
                        this.d = (this.d + 1);
                        this.a(p12, v4, com.a.b.c.da.b);
                        this.a(v2, p15, v8);
                        this.l();
                        this.unlock();
                        this.c();
                        v0_4 = 1;
                    }
                } else {
                    if (v4.d()) {
                        this.d = (this.d + 1);
                        int v1_5 = (this.b - 1);
                        v7.set(v10, this.a(v1_1, v2, v3, v4, com.a.b.c.da.c));
                        this.b = v1_5;
                    }
                    this.unlock();
                    this.c();
                    v0_4 = 0;
                }
            }
            return v0_4;
        }
        this.unlock();
        this.c();
        v0_4 = 0;
        return v0_4;
    }

    final Object b(Object p10, int p11)
    {
        Object v0_0 = 0;
        try {
            if (this.b == 0) {
                this.a();
            } else {
                long v6 = this.a.v.a();
                com.a.b.c.bs v2 = this.a(p10, p11, v6);
                if (v2 != null) {
                    Object v5 = v2.a().get();
                    if (v5 == null) {
                        this.d();
                    } else {
                        this.b(v2, v6);
                        v0_0 = this.a(v2, v2.d(), p11, v5, v6, this.a.y);
                        this.a();
                    }
                } else {
                    this.a();
                }
            }
        } catch (Object v0_1) {
            this.a();
            throw v0_1;
        }
        return v0_0;
    }

    final void b()
    {
        this.d(this.a.v.a());
        this.c();
        return;
    }

    final boolean b(Object p10, int p11, Object p12)
    {
        int v6 = 0;
        this.lock();
        try {
            this.d(this.a.v.a());
            java.util.concurrent.atomic.AtomicReferenceArray v7 = this.f;
            int v8 = (p11 & (v7.length() - 1));
            int v1_1 = ((com.a.b.c.bs) v7.get(v8));
            com.a.b.c.bs v2 = v1_1;
        } catch (int v0_11) {
            this.unlock();
            this.c();
            throw v0_11;
        }
        while (v2 != null) {
            Object v3 = v2.d();
            if ((v2.c() != p11) || ((v3 == null) || (!this.a.k.a(p10, v3)))) {
                v2 = v2.b();
            } else {
                com.a.b.c.da v5_3;
                com.a.b.c.cg v4 = v2.a();
                int v0_9 = v4.get();
                if (!this.a.l.a(p12, v0_9)) {
                    if ((v0_9 != 0) || (!v4.d())) {
                        this.unlock();
                        this.c();
                        return v6;
                    } else {
                        v5_3 = com.a.b.c.da.c;
                    }
                } else {
                    v5_3 = com.a.b.c.da.a;
                }
                int v0_17;
                this.d = (this.d + 1);
                int v1_3 = (this.b - 1);
                v7.set(v8, this.a(v1_1, v2, v3, v4, v5_3));
                this.b = v1_3;
                if (v5_3 != com.a.b.c.da.a) {
                    v0_17 = 0;
                } else {
                    v0_17 = 1;
                }
                this.unlock();
                this.c();
                v6 = v0_17;
            }
            return v6;
        }
        this.unlock();
        this.c();
        return v6;
    }

    final void c()
    {
        if (!this.isHeldByCurrentThread()) {
            com.a.b.c.ao v1 = this.a;
            while(true) {
                Throwable v0_3 = ((com.a.b.c.dk) v1.t.poll());
                if (v0_3 == null) {
                    break;
                }
                try {
                    v1.u.a(v0_3);
                } catch (Throwable v0_4) {
                    com.a.b.c.ao.f.log(java.util.logging.Level.WARNING, "Exception thrown by removal listener", v0_4);
                }
            }
        }
        return;
    }

    final boolean c(Object p5, int p6)
    {
        int v0_0 = 0;
        try {
            if (this.b == 0) {
                this.a();
            } else {
                Object v1_3 = this.a(p5, p6, this.a.v.a());
                if (v1_3 != null) {
                    if (v1_3.a().get() != null) {
                        v0_0 = 1;
                    }
                    this.a();
                } else {
                    this.a();
                }
            }
        } catch (int v0_1) {
            this.a();
            throw v0_1;
        }
        return v0_0;
    }

    final Object d(Object p10, int p11)
    {
        Object v0_0 = 0;
        this.lock();
        try {
            this.d(this.a.v.a());
            java.util.concurrent.atomic.AtomicReferenceArray v7 = this.f;
            int v8 = (p11 & (v7.length() - 1));
            int v1_5 = ((com.a.b.c.bs) v7.get(v8));
            com.a.b.c.bs v2_1 = v1_5;
        } catch (Object v0_1) {
            this.unlock();
            this.c();
            throw v0_1;
        }
        while (v2_1 != null) {
            Object v3 = v2_1.d();
            if ((v2_1.c() != p11) || ((v3 == null) || (!this.a.k.a(p10, v3)))) {
                v2_1 = v2_1.b();
            } else {
                com.a.b.c.da v5_1;
                com.a.b.c.cg v4_4 = v2_1.a();
                Object v6 = v4_4.get();
                if (v6 == null) {
                    if (!v4_4.d()) {
                        this.unlock();
                        this.c();
                        return v0_0;
                    } else {
                        v5_1 = com.a.b.c.da.c;
                    }
                } else {
                    v5_1 = com.a.b.c.da.a;
                }
                this.d = (this.d + 1);
                int v1_7 = (this.b - 1);
                v7.set(v8, this.a(v1_5, v2_1, v3, v4_4, v5_1));
                this.b = v1_7;
                this.unlock();
                this.c();
                v0_0 = v6;
            }
            return v0_0;
        }
        this.unlock();
        this.c();
        return v0_0;
    }
}
