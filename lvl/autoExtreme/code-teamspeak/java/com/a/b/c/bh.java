package com.a.b.c;
abstract class bh implements java.util.Iterator {
    int b;
    int c;
    com.a.b.c.bt d;
    java.util.concurrent.atomic.AtomicReferenceArray e;
    com.a.b.c.bs f;
    com.a.b.c.ct g;
    com.a.b.c.ct h;
    final synthetic com.a.b.c.ao i;

    bh(com.a.b.c.ao p2)
    {
        this.i = p2;
        this.b = (p2.i.length - 1);
        this.c = -1;
        this.b();
        return;
    }

    private boolean a(com.a.b.c.bs p7)
    {
        com.a.b.c.ct v0_0 = 0;
        try {
            boolean v2_0 = this.i.v.a();
            Object v4 = p7.d();
            com.a.b.c.ao v5 = this.i;
        } catch (com.a.b.c.ct v0_1) {
            this.d.a();
            throw v0_1;
        }
        if (p7.d() != null) {
            com.a.b.c.ct v1_4 = p7.a().get();
            if ((v1_4 != null) && (!v5.a(p7, v2_0))) {
                v0_0 = v1_4;
            }
        }
        com.a.b.c.ct v0_3;
        if (v0_0 == null) {
            this.d.a();
            v0_3 = 0;
        } else {
            this.g = new com.a.b.c.ct(this.i, v4, v0_0);
            this.d.a();
            v0_3 = 1;
        }
        return v0_3;
    }

    private void b()
    {
        this.g = 0;
        if ((!this.c()) && (!this.d())) {
            while (this.b >= 0) {
                boolean v0_5 = this.i.i;
                int v1 = this.b;
                this.b = (v1 - 1);
                this.d = v0_5[v1];
                if (this.d.b != 0) {
                    this.e = this.d.f;
                    this.c = (this.e.length() - 1);
                    if (this.d()) {
                        break;
                    }
                }
            }
        }
        return;
    }

    private boolean c()
    {
        com.a.b.c.bs v0_4;
        if (this.f == null) {
            v0_4 = 0;
        } else {
            this.f = this.f.b();
            while (this.f != null) {
                if (!this.a(this.f)) {
                    this.f = this.f.b();
                } else {
                    v0_4 = 1;
                }
            }
        }
        return v0_4;
    }

    private boolean d()
    {
        while (this.c >= 0) {
            int v0_2 = this.e;
            int v1 = this.c;
            this.c = (v1 - 1);
            int v0_4 = ((com.a.b.c.bs) v0_2.get(v1));
            this.f = v0_4;
            if ((v0_4 != 0) && ((this.a(this.f)) || (this.c()))) {
                int v0_1 = 1;
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    final com.a.b.c.ct a()
    {
        if (this.g != null) {
            this.h = this.g;
            this.b();
            return this.h;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public boolean hasNext()
    {
        int v0_1;
        if (this.g == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public abstract Object next();

    public void remove()
    {
        int v0_1;
        if (this.h == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1);
        this.i.remove(this.h.getKey());
        this.h = 0;
        return;
    }
}
