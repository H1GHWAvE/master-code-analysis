package com.a.b.c;
final class ct implements java.util.Map$Entry {
    final Object a;
    Object b;
    final synthetic com.a.b.c.ao c;

    ct(com.a.b.c.ao p1, Object p2, Object p3)
    {
        this.c = p1;
        this.a = p2;
        this.b = p3;
        return;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof java.util.Map$Entry)) && ((this.a.equals(((java.util.Map$Entry) p4).getKey())) && (this.b.equals(((java.util.Map$Entry) p4).getValue())))) {
            v0 = 1;
        }
        return v0;
    }

    public final Object getKey()
    {
        return this.a;
    }

    public final Object getValue()
    {
        return this.b;
    }

    public final int hashCode()
    {
        return (this.a.hashCode() ^ this.b.hashCode());
    }

    public final Object setValue(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.getKey()));
        String v1_2 = String.valueOf(String.valueOf(this.getValue()));
        return new StringBuilder(((v0_2.length() + 1) + v1_2.length())).append(v0_2).append("=").append(v1_2).toString();
    }
}
