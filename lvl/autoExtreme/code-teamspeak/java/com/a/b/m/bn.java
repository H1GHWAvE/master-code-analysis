package com.a.b.m;
final class bn implements java.io.Serializable, java.lang.reflect.ParameterizedType {
    private static final long d;
    private final reflect.Type a;
    private final com.a.b.d.jl b;
    private final Class c;

    bn(reflect.Type p3, Class p4, reflect.Type[] p5)
    {
        com.a.b.d.jl v0_1;
        com.a.b.b.cn.a(p4);
        if (p5.length != p4.getTypeParameters().length) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        com.a.b.m.ay.a(p5, "type parameter");
        this.a = p3;
        this.c = p4;
        this.b = com.a.b.m.bh.d.a(p5);
        return;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof reflect.ParameterizedType)) && ((this.getRawType().equals(((reflect.ParameterizedType) p4).getRawType())) && ((com.a.b.b.ce.a(this.getOwnerType(), ((reflect.ParameterizedType) p4).getOwnerType())) && (java.util.Arrays.equals(this.getActualTypeArguments(), ((reflect.ParameterizedType) p4).getActualTypeArguments()))))) {
            v0 = 1;
        }
        return v0;
    }

    public final reflect.Type[] getActualTypeArguments()
    {
        return com.a.b.m.ay.a(this.b);
    }

    public final reflect.Type getOwnerType()
    {
        return this.a;
    }

    public final reflect.Type getRawType()
    {
        return this.c;
    }

    public final int hashCode()
    {
        int v0_2;
        if (this.a != null) {
            v0_2 = this.a.hashCode();
        } else {
            v0_2 = 0;
        }
        return ((v0_2 ^ this.b.hashCode()) ^ this.c.hashCode());
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder();
        if (this.a != null) {
            v0_1.append(com.a.b.m.bh.d.c(this.a)).append(46);
        }
        v0_1.append(this.c.getName()).append(60).append(com.a.b.m.ay.b().a(com.a.b.d.mq.a(this.b, com.a.b.m.ay.a()))).append(62);
        return v0_1.toString();
    }
}
