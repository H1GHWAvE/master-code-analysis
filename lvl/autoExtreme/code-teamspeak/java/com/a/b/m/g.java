package com.a.b.m;
 class g extends java.lang.reflect.AccessibleObject implements java.lang.reflect.Member {
    private final reflect.AccessibleObject a;
    private final reflect.Member b;

    g(reflect.AccessibleObject p1)
    {
        com.a.b.b.cn.a(p1);
        this.a = p1;
        this.b = ((reflect.Member) p1);
        return;
    }

    private boolean b()
    {
        return reflect.Modifier.isPublic(this.getModifiers());
    }

    private boolean c()
    {
        return reflect.Modifier.isProtected(this.getModifiers());
    }

    private boolean d()
    {
        if ((reflect.Modifier.isPrivate(this.getModifiers())) || ((reflect.Modifier.isPublic(this.getModifiers())) || (reflect.Modifier.isProtected(this.getModifiers())))) {
            int v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    private boolean e()
    {
        return reflect.Modifier.isPrivate(this.getModifiers());
    }

    private boolean f()
    {
        return reflect.Modifier.isStatic(this.getModifiers());
    }

    private boolean g()
    {
        return reflect.Modifier.isFinal(this.getModifiers());
    }

    private boolean h()
    {
        return reflect.Modifier.isAbstract(this.getModifiers());
    }

    private boolean i()
    {
        return reflect.Modifier.isNative(this.getModifiers());
    }

    private boolean j()
    {
        return reflect.Modifier.isSynchronized(this.getModifiers());
    }

    private boolean k()
    {
        return reflect.Modifier.isVolatile(this.getModifiers());
    }

    private boolean l()
    {
        return reflect.Modifier.isTransient(this.getModifiers());
    }

    public com.a.b.m.ae a()
    {
        return com.a.b.m.ae.a(this.getDeclaringClass());
    }

    public boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.m.g)) && ((this.a().equals(((com.a.b.m.g) p4).a())) && (this.b.equals(((com.a.b.m.g) p4).b)))) {
            v0 = 1;
        }
        return v0;
    }

    public final otation.Annotation getAnnotation(Class p2)
    {
        return this.a.getAnnotation(p2);
    }

    public final otation.Annotation[] getAnnotations()
    {
        return this.a.getAnnotations();
    }

    public final otation.Annotation[] getDeclaredAnnotations()
    {
        return this.a.getDeclaredAnnotations();
    }

    public Class getDeclaringClass()
    {
        return this.b.getDeclaringClass();
    }

    public final int getModifiers()
    {
        return this.b.getModifiers();
    }

    public final String getName()
    {
        return this.b.getName();
    }

    public int hashCode()
    {
        return this.b.hashCode();
    }

    public final boolean isAccessible()
    {
        return this.a.isAccessible();
    }

    public final boolean isAnnotationPresent(Class p2)
    {
        return this.a.isAnnotationPresent(p2);
    }

    public final boolean isSynthetic()
    {
        return this.b.isSynthetic();
    }

    public final void setAccessible(boolean p2)
    {
        this.a.setAccessible(p2);
        return;
    }

    public String toString()
    {
        return this.b.toString();
    }
}
