package com.a.b.m;
public final class w {
    private final com.a.b.m.z a;

    public w()
    {
        this.a = new com.a.b.m.z();
        return;
    }

    private w(com.a.b.m.z p1)
    {
        this.a = p1;
        return;
    }

    synthetic w(com.a.b.m.z p1, byte p2)
    {
        this(p1);
        return;
    }

    private com.a.b.m.w a(reflect.Type p4, reflect.Type p5)
    {
        java.util.HashMap v2 = com.a.b.d.sz.c();
        com.a.b.m.w.a(v2, ((reflect.Type) com.a.b.b.cn.a(p4)), ((reflect.Type) com.a.b.b.cn.a(p5)));
        return this.a(v2);
    }

    static Object a(Class p6, Object p7)
    {
        try {
            return p6.cast(p7);
        } catch (IllegalArgumentException v0) {
            String v1_1 = String.valueOf(String.valueOf(p7));
            String v2_2 = String.valueOf(String.valueOf(p6.getSimpleName()));
            throw new IllegalArgumentException(new StringBuilder(((v1_1.length() + 10) + v2_2.length())).append(v1_1).append(" is not a ").append(v2_2).toString());
        }
    }

    private reflect.ParameterizedType a(reflect.ParameterizedType p4)
    {
        reflect.Type v1;
        reflect.ParameterizedType v0_0 = p4.getOwnerType();
        if (v0_0 != null) {
            v1 = this.a(v0_0);
        } else {
            v1 = 0;
        }
        return com.a.b.m.ay.a(v1, ((Class) this.a(p4.getRawType())), this.a(p4.getActualTypeArguments()));
    }

    private reflect.Type a(reflect.GenericArrayType p2)
    {
        return com.a.b.m.ay.a(this.a(p2.getGenericComponentType()));
    }

    private reflect.WildcardType a(reflect.WildcardType p4)
    {
        return new com.a.b.m.bp(this.a(p4.getLowerBounds()), this.a(p4.getUpperBounds()));
    }

    static void a(java.util.Map p3, reflect.Type p4, reflect.Type p5)
    {
        if (!p4.equals(p5)) {
            com.a.b.m.x v0_2 = new com.a.b.m.x(p3, p5);
            reflect.Type[] v1_1 = new reflect.Type[1];
            v1_1[0] = p4;
            v0_2.a(v1_1);
        }
        return;
    }

    private static synthetic reflect.Type[] a(com.a.b.m.w p1, reflect.Type[] p2)
    {
        return p1.a(p2);
    }

    private static com.a.b.m.w b(reflect.Type p2)
    {
        return new com.a.b.m.w().a(com.a.b.m.y.a(p2));
    }

    private static synthetic Object b(Class p1, Object p2)
    {
        return com.a.b.m.w.a(p1, p2);
    }

    private static synthetic void b(java.util.Map p0, reflect.Type p1, reflect.Type p2)
    {
        com.a.b.m.w.a(p0, p1, p2);
        return;
    }

    final com.a.b.m.w a(java.util.Map p3)
    {
        return new com.a.b.m.w(this.a.a(p3));
    }

    public final reflect.Type a(reflect.Type p4)
    {
        com.a.b.b.cn.a(p4);
        if (!(p4 instanceof reflect.TypeVariable)) {
            if (!(p4 instanceof reflect.ParameterizedType)) {
                if (!(p4 instanceof reflect.GenericArrayType)) {
                    if ((p4 instanceof reflect.WildcardType)) {
                        p4 = new com.a.b.m.bp(this.a(((reflect.WildcardType) p4).getLowerBounds()), this.a(((reflect.WildcardType) p4).getUpperBounds()));
                    }
                } else {
                    p4 = com.a.b.m.ay.a(this.a(((reflect.GenericArrayType) p4).getGenericComponentType()));
                }
            } else {
                reflect.Type[] v1_2;
                reflect.Type[] v0_8 = ((reflect.ParameterizedType) p4).getOwnerType();
                if (v0_8 != null) {
                    v1_2 = this.a(v0_8);
                } else {
                    v1_2 = 0;
                }
                p4 = com.a.b.m.ay.a(v1_2, ((Class) this.a(((reflect.ParameterizedType) p4).getRawType())), this.a(((reflect.ParameterizedType) p4).getActualTypeArguments()));
            }
        } else {
            reflect.Type[] v0_14 = this.a;
            p4 = v0_14.a(((reflect.TypeVariable) p4), new com.a.b.m.aa(v0_14, ((reflect.TypeVariable) p4), v0_14));
        }
        return p4;
    }

    final reflect.Type[] a(reflect.Type[] p4)
    {
        reflect.Type[] v1 = new reflect.Type[p4.length];
        int v0_1 = 0;
        while (v0_1 < p4.length) {
            v1[v0_1] = this.a(p4[v0_1]);
            v0_1++;
        }
        return v1;
    }
}
