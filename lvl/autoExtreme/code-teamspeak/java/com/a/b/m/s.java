package com.a.b.m;
public final class s implements java.lang.reflect.AnnotatedElement {
    private final com.a.b.m.k a;
    private final int b;
    private final com.a.b.m.ae c;
    private final com.a.b.d.jl d;

    s(com.a.b.m.k p2, int p3, com.a.b.m.ae p4, otation.Annotation[] p5)
    {
        this.a = p2;
        this.b = p3;
        this.c = p4;
        this.d = com.a.b.d.jl.a(p5);
        return;
    }

    private com.a.b.m.ae a()
    {
        return this.c;
    }

    private otation.Annotation[] a(Class p2)
    {
        return ((otation.Annotation[]) com.a.b.d.mq.a(com.a.b.d.gd.a(this.d).a(p2).c, p2));
    }

    private com.a.b.m.k b()
    {
        return this.a;
    }

    private otation.Annotation b(Class p3)
    {
        otation.Annotation v0_5;
        com.a.b.b.cn.a(p3);
        otation.Annotation v0_4 = com.a.b.d.gd.a(this.d).a(p3).c.iterator();
        if (!v0_4.hasNext()) {
            v0_5 = com.a.b.b.ci.f();
        } else {
            v0_5 = com.a.b.b.ci.b(v0_4.next());
        }
        return ((otation.Annotation) v0_5.d());
    }

    private otation.Annotation[] c(Class p2)
    {
        return ((otation.Annotation[]) com.a.b.d.mq.a(com.a.b.d.gd.a(this.d).a(p2).c, p2));
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.m.s)) && ((this.b == ((com.a.b.m.s) p4).b) && (this.a.equals(((com.a.b.m.s) p4).a)))) {
            v0 = 1;
        }
        return v0;
    }

    public final otation.Annotation getAnnotation(Class p4)
    {
        com.a.b.b.cn.a(p4);
        java.util.Iterator v1 = this.d.iterator();
        while (v1.hasNext()) {
            otation.Annotation v0_4 = ((otation.Annotation) v1.next());
            if (p4.isInstance(v0_4)) {
                otation.Annotation v0_2 = ((otation.Annotation) p4.cast(v0_4));
            }
            return v0_2;
        }
        v0_2 = 0;
        return v0_2;
    }

    public final otation.Annotation[] getAnnotations()
    {
        return this.getDeclaredAnnotations();
    }

    public final otation.Annotation[] getDeclaredAnnotations()
    {
        otation.Annotation[] v1_2 = new otation.Annotation[this.d.size()];
        return ((otation.Annotation[]) this.d.toArray(v1_2));
    }

    public final int hashCode()
    {
        return this.b;
    }

    public final boolean isAnnotationPresent(Class p2)
    {
        int v0_1;
        if (this.getAnnotation(p2) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.c));
        return new StringBuilder((v0_2.length() + 15)).append(v0_2).append(" arg").append(this.b).toString();
    }
}
