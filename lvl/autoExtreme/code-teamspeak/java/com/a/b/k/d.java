package com.a.b.k;
public final class d {
    private static final int a = 4;
    private static final int b = 8;
    private static final java.net.Inet4Address c;
    private static final java.net.Inet4Address d;

    static d()
    {
        com.a.b.k.d.c = ((java.net.Inet4Address) com.a.b.k.d.a("127.0.0.1"));
        com.a.b.k.d.d = ((java.net.Inet4Address) com.a.b.k.d.a("0.0.0.0"));
        return;
    }

    private d()
    {
        return;
    }

    public static String a(java.net.InetAddress p3)
    {
        String v0_1;
        if (!(p3 instanceof java.net.Inet6Address)) {
            v0_1 = com.a.b.k.d.b(p3);
        } else {
            String v0_4 = String.valueOf(String.valueOf(com.a.b.k.d.b(p3)));
            v0_1 = new StringBuilder((v0_4.length() + 2)).append("[").append(v0_4).append("]").toString();
        }
        return v0_1;
    }

    static synthetic java.net.Inet4Address a()
    {
        return com.a.b.k.d.d;
    }

    private static java.net.Inet4Address a(int p1)
    {
        return com.a.b.k.d.a(com.a.b.l.q.b(p1));
    }

    private static java.net.Inet4Address a(byte[] p5)
    {
        java.net.Inet4Address v0_1;
        if (p5.length != 4) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p5.length);
        com.a.b.b.cn.a(v0_1, "Byte array has invalid length for an IPv4 address: %s != 4.", v1_1);
        return ((java.net.Inet4Address) com.a.b.k.d.b(p5));
    }

    public static java.net.InetAddress a(String p4)
    {
        java.net.InetAddress v0_0 = com.a.b.k.d.c(p4);
        if (v0_0 != null) {
            return com.a.b.k.d.b(v0_0);
        } else {
            Object[] v2_1 = new Object[1];
            v2_1[0] = p4;
            throw new IllegalArgumentException(String.format("\'%s\' is not an IP string literal.", v2_1));
        }
    }

    private static void a(int[] p6)
    {
        int v4 = 0;
        int v2 = -1;
        int v1_0 = -1;
        int v3 = -1;
        while (v4 < 9) {
            if ((v4 >= 8) || (p6[v4] != 0)) {
                if (v2 >= 0) {
                    int v1_1;
                    int v0_5 = (v4 - v2);
                    if (v0_5 <= v1_0) {
                        v0_5 = v1_0;
                        v1_1 = v3;
                    } else {
                        v1_1 = v2;
                    }
                    v2 = -1;
                    v3 = v1_1;
                    v1_0 = v0_5;
                }
            } else {
                if (v2 < 0) {
                    v2 = v4;
                }
            }
            v4++;
        }
        if (v1_0 >= 2) {
            java.util.Arrays.fill(p6, v3, (v3 + v1_0), -1);
        }
        return;
    }

    private static boolean a(java.net.Inet6Address p5)
    {
        int v0 = 0;
        if (p5.isIPv4CompatibleAddress()) {
            byte v2_1 = p5.getAddress();
            if ((v2_1[12] != 0) || ((v2_1[13] != 0) || ((v2_1[14] != 0) || ((v2_1[15] != 0) && (v2_1[15] != 1))))) {
                v0 = 1;
            }
        }
        return v0;
    }

    private static String b(java.net.InetAddress p9)
    {
        int v0_7;
        com.a.b.b.cn.a(p9);
        if (!(p9 instanceof java.net.Inet4Address)) {
            com.a.b.b.cn.a((p9 instanceof java.net.Inet6Address));
            int v1_0 = p9.getAddress();
            int[] v7 = new int[8];
            int v0_2 = 0;
            while (v0_2 < 8) {
                v7[v0_2] = com.a.b.l.q.a(0, 0, v1_0[(v0_2 * 2)], v1_0[((v0_2 * 2) + 1)]);
                v0_2++;
            }
            int v4 = 0;
            int v2_0 = -1;
            int v1_1 = -1;
            StringBuilder v3_0 = -1;
            while (v4 < 9) {
                if ((v4 >= 8) || (v7[v4] != 0)) {
                    if (v2_0 >= 0) {
                        int v1_3;
                        int v0_11 = (v4 - v2_0);
                        if (v0_11 <= v1_1) {
                            v0_11 = v1_1;
                            v1_3 = v3_0;
                        } else {
                            v1_3 = v2_0;
                        }
                        v2_0 = -1;
                        v3_0 = v1_3;
                        v1_1 = v0_11;
                    }
                } else {
                    if (v2_0 < 0) {
                        v2_0 = v4;
                    }
                }
                v4++;
            }
            if (v1_1 >= 2) {
                java.util.Arrays.fill(v7, v3_0, (v3_0 + v1_1), -1);
            }
            StringBuilder v3_2 = new StringBuilder(39);
            int v1_2 = 0;
            int v2_1 = 0;
            while (v1_2 < 8) {
                int v0_9;
                if (v7[v1_2] < 0) {
                    v0_9 = 0;
                } else {
                    v0_9 = 1;
                }
                if (v0_9 == 0) {
                    if ((v1_2 == 0) || (v2_1 != 0)) {
                        v3_2.append("::");
                    }
                } else {
                    if (v2_1 != 0) {
                        v3_2.append(58);
                    }
                    v3_2.append(Integer.toHexString(v7[v1_2]));
                }
                v1_2++;
                v2_1 = v0_9;
            }
            v0_7 = v3_2.toString();
        } else {
            v0_7 = p9.getHostAddress();
        }
        return v0_7;
    }

    private static String b(int[] p5)
    {
        StringBuilder v4_1 = new StringBuilder(39);
        int v2 = 0;
        int v3_0 = 0;
        while (v2 < 8) {
            int v0_4;
            if (p5[v2] < 0) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            if (v0_4 == 0) {
                if ((v2 == 0) || (v3_0 != 0)) {
                    v4_1.append("::");
                }
            } else {
                if (v3_0 != 0) {
                    v4_1.append(58);
                }
                v4_1.append(Integer.toHexString(p5[v2]));
            }
            v2++;
            v3_0 = v0_4;
        }
        return v4_1.toString();
    }

    private static java.net.Inet4Address b(java.net.Inet6Address p5)
    {
        java.net.Inet4Address v0_0 = com.a.b.k.d.a(p5);
        int v2_1 = new Object[1];
        v2_1[0] = com.a.b.k.d.b(p5);
        com.a.b.b.cn.a(v0_0, "Address \'%s\' is not IPv4-compatible.", v2_1);
        return com.a.b.k.d.a(java.util.Arrays.copyOfRange(p5.getAddress(), 12, 16));
    }

    private static java.net.InetAddress b(byte[] p2)
    {
        try {
            return java.net.InetAddress.getByAddress(p2);
        } catch (java.net.UnknownHostException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    private static boolean b(String p1)
    {
        int v0_1;
        if (com.a.b.k.d.c(p1) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static java.net.Inet4Address c(java.net.InetAddress p8)
    {
        java.net.Inet4Address v8_2;
        java.net.Inet4Address v0_0 = 0;
        if (!(p8 instanceof java.net.Inet4Address)) {
            String v3_0 = p8.getAddress();
            com.a.b.k.e v1_1 = 0;
            while (v1_1 < 15) {
                if (v3_0[v1_1] == null) {
                    v1_1++;
                } else {
                    com.a.b.k.e v1_2 = 0;
                }
                if ((v1_2 == null) || (v3_0[15] != 1)) {
                    if ((v1_2 == null) || (v3_0[15] != null)) {
                        if ((!com.a.b.k.d.a(((java.net.Inet6Address) p8))) && ((!com.a.b.k.d.c(((java.net.Inet6Address) p8))) && (!com.a.b.k.d.e(((java.net.Inet6Address) p8))))) {
                            com.a.b.k.e v1_7 = 0;
                        } else {
                            v1_7 = 1;
                        }
                        java.net.Inet4Address v0_2;
                        if (v1_7 == null) {
                            v0_2 = java.nio.ByteBuffer.wrap(((java.net.Inet6Address) p8).getAddress(), 0, 8).getLong();
                        } else {
                            java.net.Inet4Address v0_5;
                            if (!com.a.b.k.d.a(((java.net.Inet6Address) p8))) {
                                if (!com.a.b.k.d.c(((java.net.Inet6Address) p8))) {
                                    if (!com.a.b.k.d.e(((java.net.Inet6Address) p8))) {
                                        Object[] v2_1 = new Object[1];
                                        v2_1[0] = com.a.b.k.d.b(((java.net.Inet6Address) p8));
                                        throw new IllegalArgumentException(String.format("\'%s\' has no embedded IPv4 address.", v2_1));
                                    } else {
                                        com.a.b.k.e v1_14 = com.a.b.k.d.e(((java.net.Inet6Address) p8));
                                        Object[] v2_2 = new Object[1];
                                        v2_2[0] = com.a.b.k.d.b(((java.net.Inet6Address) p8));
                                        com.a.b.b.cn.a(v1_14, "Address \'%s\' is not a Teredo address.", v2_2);
                                        com.a.b.k.e v1_15 = ((java.net.Inet6Address) p8).getAddress();
                                        Object[] v2_5 = com.a.b.k.d.a(java.util.Arrays.copyOfRange(v1_15, 4, 8));
                                        String v3_5 = (com.a.b.i.z.a(v1_15, 8).readShort() & 65535);
                                        String v4_9 = ((com.a.b.i.z.a(v1_15, 10).readShort() ^ -1) & 65535);
                                        com.a.b.k.e v1_16 = java.util.Arrays.copyOfRange(v1_15, 12, 16);
                                        while (v0_0 < v1_16.length) {
                                            v1_16[v0_0] = ((byte) (v1_16[v0_0] ^ -1));
                                            v0_0++;
                                        }
                                        v0_5 = new com.a.b.k.e(v2_5, com.a.b.k.d.a(v1_16), v4_9, v3_5).a;
                                    }
                                } else {
                                    com.a.b.k.e v1_19 = com.a.b.k.d.c(((java.net.Inet6Address) p8));
                                    Object[] v2_6 = new Object[1];
                                    v2_6[0] = com.a.b.k.d.b(((java.net.Inet6Address) p8));
                                    com.a.b.b.cn.a(v1_19, "Address \'%s\' is not a 6to4 address.", v2_6);
                                    v0_5 = com.a.b.k.d.a(java.util.Arrays.copyOfRange(((java.net.Inet6Address) p8).getAddress(), 2, 6));
                                }
                            } else {
                                com.a.b.k.e v1_21 = com.a.b.k.d.a(((java.net.Inet6Address) p8));
                                Object[] v2_8 = new Object[1];
                                v2_8[0] = com.a.b.k.d.b(((java.net.Inet6Address) p8));
                                com.a.b.b.cn.a(v1_21, "Address \'%s\' is not IPv4-compatible.", v2_8);
                                v0_5 = com.a.b.k.d.a(java.util.Arrays.copyOfRange(((java.net.Inet6Address) p8).getAddress(), 12, 16));
                            }
                            v0_2 = ((long) v0_5.hashCode());
                        }
                        java.net.Inet4Address v0_13 = (com.a.b.g.am.a().a(v0_2).b() | -536870912);
                        if (v0_13 == -1) {
                            v0_13 = -2;
                        }
                        v8_2 = com.a.b.k.d.a(com.a.b.l.q.b(v0_13));
                    } else {
                        v8_2 = com.a.b.k.d.d;
                    }
                } else {
                    v8_2 = com.a.b.k.d.c;
                }
            }
            v1_2 = 1;
        } else {
            v8_2 = ((java.net.Inet4Address) p8);
        }
        return v8_2;
    }

    private static java.net.InetAddress c(byte[] p3)
    {
        byte[] v1 = new byte[p3.length];
        java.net.InetAddress v0_1 = 0;
        while (v0_1 < p3.length) {
            v1[v0_1] = p3[((p3.length - v0_1) - 1)];
            v0_1++;
        }
        return java.net.InetAddress.getByAddress(v1);
    }

    private static boolean c(java.net.Inet6Address p5)
    {
        int v0 = 1;
        byte v2_0 = p5.getAddress();
        if ((v2_0[0] != 32) || (v2_0[1] != 2)) {
            v0 = 0;
        }
        return v0;
    }

    private static byte[] c(String p9)
    {
        int v0_0 = 0;
        String v2_0 = 0;
        StringBuilder v3_0 = 0;
        while (v0_0 < p9.length()) {
            int v6_3 = p9.charAt(v0_0);
            if (v6_3 != 46) {
                if (v6_3 != 58) {
                    if (Character.digit(v6_3, 16) == -1) {
                        int v0_1 = 0;
                        return v0_1;
                    }
                } else {
                    if (v2_0 == null) {
                        v3_0 = 1;
                    } else {
                        v0_1 = 0;
                        return v0_1;
                    }
                }
            } else {
                v2_0 = 1;
            }
            v0_0++;
        }
        if (v3_0 == null) {
            if (v2_0 == null) {
                v0_1 = 0;
            } else {
                v0_1 = com.a.b.k.d.d(p9);
            }
        } else {
            if (v2_0 != null) {
                int v0_2 = p9.lastIndexOf(58);
                String v2_2 = p9.substring(0, (v0_2 + 1));
                int v0_5 = com.a.b.k.d.d(p9.substring((v0_2 + 1)));
                if (v0_5 != 0) {
                    StringBuilder v1_5 = Integer.toHexString((((v0_5[0] & 255) << 8) | (v0_5[1] & 255)));
                    int v0_9 = Integer.toHexString(((v0_5[3] & 255) | ((v0_5[2] & 255) << 8)));
                    String v2_4 = String.valueOf(String.valueOf(v2_2));
                    StringBuilder v1_7 = String.valueOf(String.valueOf(v1_5));
                    int v0_11 = String.valueOf(String.valueOf(v0_9));
                    p9 = new StringBuilder((((v2_4.length() + 1) + v1_7.length()) + v0_11.length())).append(v2_4).append(v1_7).append(":").append(v0_11).toString();
                } else {
                    p9 = 0;
                }
                if (p9 == null) {
                    v0_1 = 0;
                    return v0_1;
                }
            }
            v0_1 = com.a.b.k.d.e(p9);
        }
        return v0_1;
    }

    private static int d(java.net.InetAddress p8)
    {
        java.net.Inet4Address v8_2;
        java.net.Inet4Address v0_0 = 0;
        if (!(p8 instanceof java.net.Inet4Address)) {
            String v3_0 = p8.getAddress();
            com.a.b.k.e v1_1 = 0;
            while (v1_1 < 15) {
                if (v3_0[v1_1] == null) {
                    v1_1++;
                } else {
                    com.a.b.k.e v1_2 = 0;
                }
                if ((v1_2 == null) || (v3_0[15] != 1)) {
                    if ((v1_2 == null) || (v3_0[15] != null)) {
                        if ((!com.a.b.k.d.a(((java.net.Inet6Address) p8))) && ((!com.a.b.k.d.c(((java.net.Inet6Address) p8))) && (!com.a.b.k.d.e(((java.net.Inet6Address) p8))))) {
                            com.a.b.k.e v1_7 = 0;
                        } else {
                            v1_7 = 1;
                        }
                        java.net.Inet4Address v0_2;
                        if (v1_7 == null) {
                            v0_2 = java.nio.ByteBuffer.wrap(((java.net.Inet6Address) p8).getAddress(), 0, 8).getLong();
                        } else {
                            java.net.Inet4Address v0_5;
                            if (!com.a.b.k.d.a(((java.net.Inet6Address) p8))) {
                                if (!com.a.b.k.d.c(((java.net.Inet6Address) p8))) {
                                    if (!com.a.b.k.d.e(((java.net.Inet6Address) p8))) {
                                        Object[] v2_1 = new Object[1];
                                        v2_1[0] = com.a.b.k.d.b(((java.net.Inet6Address) p8));
                                        throw new IllegalArgumentException(String.format("\'%s\' has no embedded IPv4 address.", v2_1));
                                    } else {
                                        com.a.b.k.e v1_14 = com.a.b.k.d.e(((java.net.Inet6Address) p8));
                                        Object[] v2_2 = new Object[1];
                                        v2_2[0] = com.a.b.k.d.b(((java.net.Inet6Address) p8));
                                        com.a.b.b.cn.a(v1_14, "Address \'%s\' is not a Teredo address.", v2_2);
                                        com.a.b.k.e v1_15 = ((java.net.Inet6Address) p8).getAddress();
                                        Object[] v2_5 = com.a.b.k.d.a(java.util.Arrays.copyOfRange(v1_15, 4, 8));
                                        String v3_5 = (com.a.b.i.z.a(v1_15, 8).readShort() & 65535);
                                        String v4_9 = ((com.a.b.i.z.a(v1_15, 10).readShort() ^ -1) & 65535);
                                        com.a.b.k.e v1_16 = java.util.Arrays.copyOfRange(v1_15, 12, 16);
                                        while (v0_0 < v1_16.length) {
                                            v1_16[v0_0] = ((byte) (v1_16[v0_0] ^ -1));
                                            v0_0++;
                                        }
                                        v0_5 = new com.a.b.k.e(v2_5, com.a.b.k.d.a(v1_16), v4_9, v3_5).a;
                                    }
                                } else {
                                    com.a.b.k.e v1_19 = com.a.b.k.d.c(((java.net.Inet6Address) p8));
                                    Object[] v2_6 = new Object[1];
                                    v2_6[0] = com.a.b.k.d.b(((java.net.Inet6Address) p8));
                                    com.a.b.b.cn.a(v1_19, "Address \'%s\' is not a 6to4 address.", v2_6);
                                    v0_5 = com.a.b.k.d.a(java.util.Arrays.copyOfRange(((java.net.Inet6Address) p8).getAddress(), 2, 6));
                                }
                            } else {
                                com.a.b.k.e v1_21 = com.a.b.k.d.a(((java.net.Inet6Address) p8));
                                Object[] v2_8 = new Object[1];
                                v2_8[0] = com.a.b.k.d.b(((java.net.Inet6Address) p8));
                                com.a.b.b.cn.a(v1_21, "Address \'%s\' is not IPv4-compatible.", v2_8);
                                v0_5 = com.a.b.k.d.a(java.util.Arrays.copyOfRange(((java.net.Inet6Address) p8).getAddress(), 12, 16));
                            }
                            v0_2 = ((long) v0_5.hashCode());
                        }
                        java.net.Inet4Address v0_13 = (com.a.b.g.am.a().a(v0_2).b() | -536870912);
                        if (v0_13 == -1) {
                            v0_13 = -2;
                        }
                        v8_2 = com.a.b.k.d.a(com.a.b.l.q.b(v0_13));
                    } else {
                        v8_2 = com.a.b.k.d.d;
                    }
                } else {
                    v8_2 = com.a.b.k.d.c;
                }
            }
            v1_2 = 1;
        } else {
            v8_2 = ((java.net.Inet4Address) p8);
        }
        return com.a.b.i.z.a(v8_2.getAddress()).readInt();
    }

    private static java.net.Inet4Address d(java.net.Inet6Address p5)
    {
        java.net.Inet4Address v0_0 = com.a.b.k.d.c(p5);
        int v2_1 = new Object[1];
        v2_1[0] = com.a.b.k.d.b(p5);
        com.a.b.b.cn.a(v0_0, "Address \'%s\' is not a 6to4 address.", v2_1);
        return com.a.b.k.d.a(java.util.Arrays.copyOfRange(p5.getAddress(), 2, 6));
    }

    private static byte[] d(String p8)
    {
        byte[] v0 = 0;
        String[] v3 = p8.split("\\.", 5);
        if (v3.length == 4) {
            NumberFormatException v1_2 = new byte[4];
            int v2_1 = 0;
            while (v2_1 < 4) {
                byte v4_0 = v3[v2_1];
                int v5 = Integer.parseInt(v4_0);
                if ((v5 <= 255) && ((!v4_0.startsWith("0")) || (v4_0.length() <= 1))) {
                    v1_2[v2_1] = ((byte) v5);
                    v2_1++;
                } else {
                    throw new NumberFormatException();
                }
            }
            v0 = v1_2;
        }
        return v0;
    }

    private static java.net.InetAddress e(java.net.InetAddress p6)
    {
        byte[] v4 = p6.getAddress();
        int v3 = (v4.length - 1);
        while ((v3 >= 0) && (v4[v3] == 0)) {
            v4[v3] = -1;
            v3--;
        }
        java.net.InetAddress v0_3;
        if (v3 < 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = p6;
        com.a.b.b.cn.a(v0_3, "Decrementing %s would wrap.", v1_1);
        v4[v3] = ((byte) (v4[v3] - 1));
        return com.a.b.k.d.b(v4);
    }

    private static boolean e(java.net.Inet6Address p5)
    {
        int v0 = 1;
        byte v2_0 = p5.getAddress();
        if ((v2_0[0] != 32) || ((v2_0[1] != 1) || ((v2_0[2] != 0) || (v2_0[3] != 0)))) {
            v0 = 0;
        }
        return v0;
    }

    private static byte[] e(String p10)
    {
        byte[] v0_11;
        String[] v5 = p10.split(":", 10);
        if ((v5.length >= 3) && (v5.length <= 9)) {
            short v1_3 = -1;
            byte[] v0_3 = 1;
            while (v0_3 < (v5.length - 1)) {
                if (v5[v0_3].length() == 0) {
                    if (v1_3 < 0) {
                        v1_3 = v0_3;
                    } else {
                        v0_11 = 0;
                        return v0_11;
                    }
                }
                v0_3++;
            }
            byte[] v0_5;
            int v4_2;
            if (v1_3 < 0) {
                v4_2 = v5.length;
                v0_5 = 0;
            } else {
                byte[] v0_10;
                int v4_3 = ((v5.length - v1_3) - 1);
                if (v5[0].length() != 0) {
                    v0_10 = v1_3;
                } else {
                    v0_10 = (v1_3 - 1);
                    if (v0_10 != null) {
                        v0_11 = 0;
                        return v0_11;
                    }
                }
                if (v5[(v5.length - 1)].length() == 0) {
                    v4_3--;
                    if (v4_3 != 0) {
                        v0_11 = 0;
                        return v0_11;
                    }
                }
                v4_2 = v0_10;
                v0_5 = v4_3;
            }
            int v6_5 = (8 - (v4_2 + v0_5));
            try {
                if (v1_3 < 0) {
                    if (v6_5 != 0) {
                        v0_11 = 0;
                        return v0_11;
                    }
                } else {
                    if (v6_5 <= 0) {
                    }
                }
            } catch (byte[] v0) {
                v0_11 = 0;
            }
            java.nio.ByteBuffer v7 = java.nio.ByteBuffer.allocate(16);
            short v1_5 = 0;
            while (v1_5 < v4_2) {
                v7.putShort(com.a.b.k.d.h(v5[v1_5]));
                v1_5++;
            }
            short v1_6 = 0;
            while (v1_6 < v6_5) {
                v7.putShort(0);
                v1_6++;
            }
            while (v0_5 > null) {
                v7.putShort(com.a.b.k.d.h(v5[(v5.length - v0_5)]));
                v0_5--;
            }
            v0_11 = v7.array();
        } else {
            v0_11 = 0;
        }
        return v0_11;
    }

    private static com.a.b.k.e f(java.net.Inet6Address p7)
    {
        java.net.Inet4Address v0_0 = 0;
        com.a.b.k.e v1_0 = com.a.b.k.d.e(p7);
        int v3_1 = new Object[1];
        v3_1[0] = com.a.b.k.d.b(p7);
        com.a.b.b.cn.a(v1_0, "Address \'%s\' is not a Teredo address.", v3_1);
        com.a.b.k.e v1_1 = p7.getAddress();
        java.net.Inet4Address v2_3 = com.a.b.k.d.a(java.util.Arrays.copyOfRange(v1_1, 4, 8));
        int v3_4 = (com.a.b.i.z.a(v1_1, 8).readShort() & 65535);
        int v4_5 = ((com.a.b.i.z.a(v1_1, 10).readShort() ^ -1) & 65535);
        com.a.b.k.e v1_2 = java.util.Arrays.copyOfRange(v1_1, 12, 16);
        while (v0_0 < v1_2.length) {
            v1_2[v0_0] = ((byte) (v1_2[v0_0] ^ -1));
            v0_0++;
        }
        return new com.a.b.k.e(v2_3, com.a.b.k.d.a(v1_2), v4_5, v3_4);
    }

    private static String f(String p6)
    {
        String v0_12;
        String v0_1 = p6.lastIndexOf(58);
        StringBuilder v1_1 = p6.substring(0, (v0_1 + 1));
        String v0_4 = com.a.b.k.d.d(p6.substring((v0_1 + 1)));
        if (v0_4 != null) {
            String v2_5 = Integer.toHexString((((v0_4[0] & 255) << 8) | (v0_4[1] & 255)));
            String v0_8 = Integer.toHexString(((v0_4[3] & 255) | ((v0_4[2] & 255) << 8)));
            StringBuilder v1_3 = String.valueOf(String.valueOf(v1_1));
            String v2_7 = String.valueOf(String.valueOf(v2_5));
            String v0_10 = String.valueOf(String.valueOf(v0_8));
            v0_12 = new StringBuilder((((v1_3.length() + 1) + v2_7.length()) + v0_10.length())).append(v1_3).append(v2_7).append(":").append(v0_10).toString();
        } else {
            v0_12 = 0;
        }
        return v0_12;
    }

    private static java.net.InetAddress f(java.net.InetAddress p6)
    {
        byte[] v4 = p6.getAddress();
        int v3 = (v4.length - 1);
        while ((v3 >= 0) && (v4[v3] == -1)) {
            v4[v3] = 0;
            v3--;
        }
        java.net.InetAddress v0_3;
        if (v3 < 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = p6;
        com.a.b.b.cn.a(v0_3, "Incrementing %s would wrap.", v1_1);
        v4[v3] = ((byte) (v4[v3] + 1));
        return com.a.b.k.d.b(v4);
    }

    private static byte g(String p3)
    {
        byte v0_0 = Integer.parseInt(p3);
        if ((v0_0 <= 255) && ((!p3.startsWith("0")) || (p3.length() <= 1))) {
            return ((byte) v0_0);
        } else {
            throw new NumberFormatException();
        }
    }

    private static boolean g(java.net.Inet6Address p4)
    {
        int v0 = 0;
        if (!com.a.b.k.d.e(p4)) {
            byte v1_1 = p4.getAddress();
            if (((v1_1[8] | 3) == 3) && ((v1_1[9] == 0) && ((v1_1[10] == 94) && (v1_1[11] == -2)))) {
                v0 = 1;
            }
        }
        return v0;
    }

    private static boolean g(java.net.InetAddress p5)
    {
        int v1 = 0;
        byte[] v2 = p5.getAddress();
        int v0 = 0;
        while (v0 < v2.length) {
            if (v2[v0] == -1) {
                v0++;
            }
            return v1;
        }
        v1 = 1;
        return v1;
    }

    private static java.net.Inet4Address h(java.net.Inet6Address p5)
    {
        int v0_3;
        if (com.a.b.k.d.e(p5)) {
            v0_3 = 0;
        } else {
            int v0_1 = p5.getAddress();
            if (((v0_1[8] | 3) != 3) || ((v0_1[9] != 0) || ((v0_1[10] != 94) || (v0_1[11] != -2)))) {
            } else {
                v0_3 = 1;
            }
        }
        int v1_1 = new Object[1];
        v1_1[0] = com.a.b.k.d.b(p5);
        com.a.b.b.cn.a(v0_3, "Address \'%s\' is not an ISATAP address.", v1_1);
        return com.a.b.k.d.a(java.util.Arrays.copyOfRange(p5.getAddress(), 12, 16));
    }

    private static short h(String p2)
    {
        short v0_1 = Integer.parseInt(p2, 16);
        if (v0_1 <= 65535) {
            return ((short) v0_1);
        } else {
            throw new NumberFormatException();
        }
    }

    private static java.net.InetAddress i(String p4)
    {
        String v1_0;
        java.net.InetAddress v0_4;
        com.a.b.b.cn.a(p4);
        if ((!p4.startsWith("[")) || (!p4.endsWith("]"))) {
            v0_4 = 4;
            v1_0 = p4;
        } else {
            v1_0 = p4.substring(1, (p4.length() - 1));
            v0_4 = 16;
        }
        String v1_1 = com.a.b.k.d.c(v1_0);
        if ((v1_1 != null) && (v1_1.length == v0_4)) {
            return com.a.b.k.d.b(v1_1);
        } else {
            Object[] v2_1 = new Object[1];
            v2_1[0] = p4;
            throw new IllegalArgumentException(String.format("Not a valid URI IP literal: \'%s\'", v2_1));
        }
    }

    private static boolean i(java.net.Inet6Address p1)
    {
        if ((!com.a.b.k.d.a(p1)) && ((!com.a.b.k.d.c(p1)) && (!com.a.b.k.d.e(p1)))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private static java.net.Inet4Address j(java.net.Inet6Address p8)
    {
        java.net.Inet4Address v0_3;
        java.net.Inet4Address v0_0 = 0;
        if (!com.a.b.k.d.a(p8)) {
            if (!com.a.b.k.d.c(p8)) {
                if (!com.a.b.k.d.e(p8)) {
                    Object[] v3_1 = new Object[1];
                    v3_1[0] = com.a.b.k.d.b(p8);
                    throw new IllegalArgumentException(String.format("\'%s\' has no embedded IPv4 address.", v3_1));
                } else {
                    com.a.b.k.e v1_5 = com.a.b.k.d.e(p8);
                    Object[] v3_2 = new Object[1];
                    v3_2[0] = com.a.b.k.d.b(p8);
                    com.a.b.b.cn.a(v1_5, "Address \'%s\' is not a Teredo address.", v3_2);
                    com.a.b.k.e v1_6 = p8.getAddress();
                    String v2_4 = com.a.b.k.d.a(java.util.Arrays.copyOfRange(v1_6, 4, 8));
                    Object[] v3_5 = (com.a.b.i.z.a(v1_6, 8).readShort() & 65535);
                    String v4_7 = ((com.a.b.i.z.a(v1_6, 10).readShort() ^ -1) & 65535);
                    com.a.b.k.e v1_7 = java.util.Arrays.copyOfRange(v1_6, 12, 16);
                    while (v0_0 < v1_7.length) {
                        v1_7[v0_0] = ((byte) (v1_7[v0_0] ^ -1));
                        v0_0++;
                    }
                    v0_3 = new com.a.b.k.e(v2_4, com.a.b.k.d.a(v1_7), v4_7, v3_5).a;
                }
            } else {
                com.a.b.k.e v1_10 = com.a.b.k.d.c(p8);
                Object[] v3_6 = new Object[1];
                v3_6[0] = com.a.b.k.d.b(p8);
                com.a.b.b.cn.a(v1_10, "Address \'%s\' is not a 6to4 address.", v3_6);
                v0_3 = com.a.b.k.d.a(java.util.Arrays.copyOfRange(p8.getAddress(), 2, 6));
            }
        } else {
            com.a.b.k.e v1_12 = com.a.b.k.d.a(p8);
            Object[] v3_7 = new Object[1];
            v3_7[0] = com.a.b.k.d.b(p8);
            com.a.b.b.cn.a(v1_12, "Address \'%s\' is not IPv4-compatible.", v3_7);
            v0_3 = com.a.b.k.d.a(java.util.Arrays.copyOfRange(p8.getAddress(), 12, 16));
        }
        return v0_3;
    }

    private static boolean j(String p5)
    {
        IllegalArgumentException v0_0 = 1;
        String v2_4;
        Object[] v3_0;
        com.a.b.b.cn.a(p5);
        if ((!p5.startsWith("[")) || (!p5.endsWith("]"))) {
            v2_4 = 4;
            v3_0 = p5;
        } else {
            v3_0 = p5.substring(1, (p5.length() - 1));
            v2_4 = 16;
        }
        Object[] v3_3 = com.a.b.k.d.c(v3_0);
        if ((v3_3 != null) && (v3_3.length == v2_4)) {
            com.a.b.k.d.b(v3_3);
            return v0_0;
        } else {
            Object[] v3_5 = new Object[1];
            v3_5[0] = p5;
            throw new IllegalArgumentException(String.format("Not a valid URI IP literal: \'%s\'", v3_5));
        }
    }

    private static boolean k(String p5)
    {
        int v1 = 10;
        int v0 = 0;
        byte[] v3 = com.a.b.k.d.c(p5);
        if ((v3 != null) && (v3.length == 16)) {
            byte v2_1 = 0;
            while (v2_1 < 10) {
                if (v3[v2_1] == 0) {
                    v2_1++;
                }
            }
            while (v1 < 12) {
                if (v3[v1] == -1) {
                    v1++;
                }
            }
            v0 = 1;
        }
        return v0;
    }
}
