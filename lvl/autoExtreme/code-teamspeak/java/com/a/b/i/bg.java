package com.a.b.i;
final class bg extends com.a.b.i.s {
    private final java.io.File a;

    private bg(java.io.File p2)
    {
        this.a = ((java.io.File) com.a.b.b.cn.a(p2));
        return;
    }

    synthetic bg(java.io.File p1, byte p2)
    {
        this(p1);
        return;
    }

    private java.io.FileInputStream f()
    {
        return new java.io.FileInputStream(this.a);
    }

    public final synthetic java.io.InputStream a()
    {
        return this.f();
    }

    public final long d()
    {
        if (this.a.isFile()) {
            return this.a.length();
        } else {
            throw new java.io.FileNotFoundException(this.a.toString());
        }
    }

    public final byte[] e()
    {
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_2 = ((java.io.FileInputStream) v1.a(this.f()));
            RuntimeException v0_3 = com.a.b.i.bc.a(v0_2, v0_2.getChannel().size());
            v1.close();
            return v0_3;
        } catch (RuntimeException v0_6) {
            v1.close();
            throw v0_6;
        } catch (RuntimeException v0_4) {
            throw v1.a(v0_4);
        }
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 20)).append("Files.asByteSource(").append(v0_2).append(")").toString();
    }
}
