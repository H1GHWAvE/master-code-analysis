package com.a.b.i;
final class g extends com.a.b.b.m {
    final String s;
    final char[] t;
    final int u;
    final int v;
    final int w;
    final int x;
    final byte[] y;
    private final boolean[] z;

    g(String p11, char[] p12)
    {
        int v1_0 = 0;
        this.s = ((String) com.a.b.b.cn.a(p11));
        this.t = ((char[]) com.a.b.b.cn.a(p12));
        try {
            this.v = com.a.b.j.g.a(p12.length, java.math.RoundingMode.UNNECESSARY);
            int v0_8 = Math.min(8, Integer.lowestOneBit(this.v));
            this.w = (8 / v0_8);
            this.x = (this.v / v0_8);
            this.u = (p12.length - 1);
            int v4_1 = new byte[128];
            java.util.Arrays.fill(v4_1, -1);
            int v0_13 = 0;
        } catch (int v0_14) {
            throw new IllegalArgumentException(new StringBuilder(35).append("Illegal alphabet length ").append(p12.length).toString(), v0_14);
        }
        while (v0_13 < p12.length) {
            int v2_13;
            java.math.RoundingMode v5_1 = p12[v0_13];
            int v2_11 = com.a.b.b.m.b.c(v5_1);
            Object[] v7_0 = new Object[1];
            v7_0[0] = Character.valueOf(v5_1);
            com.a.b.b.cn.a(v2_11, "Non-ASCII character: %s", v7_0);
            if (v4_1[v5_1] != -1) {
                v2_13 = 0;
            } else {
                v2_13 = 1;
            }
            Object[] v7_1 = new Object[1];
            v7_1[0] = Character.valueOf(v5_1);
            com.a.b.b.cn.a(v2_13, "Duplicate character: %s", v7_1);
            v4_1[v5_1] = ((byte) v0_13);
            v0_13++;
        }
        this.y = v4_1;
        int v0_16 = new boolean[this.w];
        while (v1_0 < this.x) {
            v0_16[com.a.b.j.g.a((v1_0 * 8), this.v, java.math.RoundingMode.CEILING)] = 1;
            v1_0++;
        }
        this.z = v0_16;
        return;
    }

    private char b(int p2)
    {
        return this.t[p2];
    }

    private int d(char p4)
    {
        if ((p4 <= 127) && (this.y[p4] != -1)) {
            return this.y[p4];
        } else {
            throw new com.a.b.i.h(new StringBuilder(25).append("Unrecognized character: ").append(p4).toString());
        }
    }

    private com.a.b.i.g e()
    {
        String v1_0 = 0;
        if (this.c()) {
            com.a.b.i.g v0_2;
            if (this.d()) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
            com.a.b.b.cn.b(v0_2, "Cannot call upperCase() on a mixed-case alphabet");
            char[] v2_1 = new char[this.t.length];
            while (v1_0 < this.t.length) {
                v2_1[v1_0] = com.a.b.b.e.b(this.t[v1_0]);
                v1_0++;
            }
            this = new com.a.b.i.g(String.valueOf(this.s).concat(".upperCase()"), v2_1);
        }
        return this;
    }

    private com.a.b.i.g f()
    {
        String v1_0 = 0;
        if (this.d()) {
            com.a.b.i.g v0_2;
            if (this.c()) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
            com.a.b.b.cn.b(v0_2, "Cannot call lowerCase() on a mixed-case alphabet");
            char[] v2_1 = new char[this.t.length];
            while (v1_0 < this.t.length) {
                v2_1[v1_0] = com.a.b.b.e.a(this.t[v1_0]);
                v1_0++;
            }
            this = new com.a.b.i.g(String.valueOf(this.s).concat(".lowerCase()"), v2_1);
        }
        return this;
    }

    final boolean a(int p3)
    {
        return this.z[(p3 % this.w)];
    }

    final boolean c()
    {
        int v0 = 0;
        char[] v2 = this.t;
        int v1 = 0;
        while (v1 < v2.length) {
            if (!com.a.b.b.e.c(v2[v1])) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    public final boolean c(char p3)
    {
        if ((!com.a.b.b.m.b.c(p3)) || (this.y[p3] == -1)) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    final boolean d()
    {
        int v0 = 0;
        char[] v2 = this.t;
        int v1 = 0;
        while (v1 < v2.length) {
            if (!com.a.b.b.e.d(v2[v1])) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    public final String toString()
    {
        return this.s;
    }
}
