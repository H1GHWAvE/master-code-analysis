package com.a.b.i;
final class a extends java.io.Writer {
    private final Appendable a;
    private boolean b;

    a(Appendable p2)
    {
        this.a = ((Appendable) com.a.b.b.cn.a(p2));
        return;
    }

    private void a()
    {
        if (!this.b) {
            return;
        } else {
            throw new java.io.IOException("Cannot write to a closed writer.");
        }
    }

    public final java.io.Writer append(char p2)
    {
        this.a();
        this.a.append(p2);
        return this;
    }

    public final java.io.Writer append(CharSequence p2)
    {
        this.a();
        this.a.append(p2);
        return this;
    }

    public final java.io.Writer append(CharSequence p2, int p3, int p4)
    {
        this.a();
        this.a.append(p2, p3, p4);
        return this;
    }

    public final bridge synthetic Appendable append(char p2)
    {
        return this.append(p2);
    }

    public final bridge synthetic Appendable append(CharSequence p2)
    {
        return this.append(p2);
    }

    public final bridge synthetic Appendable append(CharSequence p2, int p3, int p4)
    {
        return this.append(p2, p3, p4);
    }

    public final void close()
    {
        this.b = 1;
        if ((this.a instanceof java.io.Closeable)) {
            ((java.io.Closeable) this.a).close();
        }
        return;
    }

    public final void flush()
    {
        this.a();
        if ((this.a instanceof java.io.Flushable)) {
            ((java.io.Flushable) this.a).flush();
        }
        return;
    }

    public final void write(int p3)
    {
        this.a();
        this.a.append(((char) p3));
        return;
    }

    public final void write(String p2)
    {
        this.a();
        this.a.append(p2);
        return;
    }

    public final void write(String p3, int p4, int p5)
    {
        this.a();
        this.a.append(p3, p4, (p4 + p5));
        return;
    }

    public final void write(char[] p3, int p4, int p5)
    {
        this.a();
        this.a.append(new String(p3, p4, p5));
        return;
    }
}
