package com.a.b.i;
public interface m implements java.io.DataInput {

    public abstract boolean readBoolean();

    public abstract byte readByte();

    public abstract char readChar();

    public abstract double readDouble();

    public abstract float readFloat();

    public abstract void readFully();

    public abstract void readFully();

    public abstract int readInt();

    public abstract String readLine();

    public abstract long readLong();

    public abstract short readShort();

    public abstract String readUTF();

    public abstract int readUnsignedByte();

    public abstract int readUnsignedShort();

    public abstract int skipBytes();
}
