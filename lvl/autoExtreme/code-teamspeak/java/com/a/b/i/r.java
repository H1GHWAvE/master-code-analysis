package com.a.b.i;
final class r extends com.a.b.i.ag {
    final synthetic com.a.b.i.p a;
    private final java.nio.charset.Charset b;

    private r(com.a.b.i.p p2, java.nio.charset.Charset p3)
    {
        this.a = p2;
        this.b = ((java.nio.charset.Charset) com.a.b.b.cn.a(p3));
        return;
    }

    synthetic r(com.a.b.i.p p1, java.nio.charset.Charset p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public final java.io.Writer a()
    {
        return new java.io.OutputStreamWriter(this.a.a(), this.b);
    }

    public final String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.a.toString()));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_3.length() + 13) + v1_2.length())).append(v0_3).append(".asCharSink(").append(v1_2).append(")").toString();
    }
}
