package com.a.b.i;
public final class bc {
    private static final int a = 10000;
    private static final com.a.b.d.aga b;

    static bc()
    {
        com.a.b.i.bc.b = new com.a.b.i.be();
        return;
    }

    private bc()
    {
        return;
    }

    private static com.a.b.g.ag a(java.io.File p1, com.a.b.g.ak p2)
    {
        return com.a.b.i.bc.a(p1).a(p2);
    }

    private static varargs com.a.b.i.ag a(java.io.File p3, java.nio.charset.Charset p4, com.a.b.i.bb[] p5)
    {
        return new com.a.b.i.r(com.a.b.i.bc.a(p3, p5), p4, 0);
    }

    private static varargs com.a.b.i.p a(java.io.File p2, com.a.b.i.bb[] p3)
    {
        return new com.a.b.i.bf(p2, p3, 0);
    }

    private static com.a.b.i.s a(java.io.File p2)
    {
        return new com.a.b.i.bg(p2, 0);
    }

    private static java.io.BufferedReader a(java.io.File p3, java.nio.charset.Charset p4)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p4);
        return new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(p3), p4));
    }

    private static java.io.File a()
    {
        String v1_1 = new java.io.File(System.getProperty("java.io.tmpdir"));
        String v2_2 = new StringBuilder(21).append(System.currentTimeMillis()).append("-").toString();
        int v0_6 = 0;
        while (v0_6 < 10000) {
            StringBuilder v4_4 = String.valueOf(String.valueOf(v2_2));
            String v3_5 = new java.io.File(v1_1, new StringBuilder((v4_4.length() + 11)).append(v4_4).append(v0_6).toString());
            if (!v3_5.mkdir()) {
                v0_6++;
            } else {
                return v3_5;
            }
        }
        String v1_4 = String.valueOf(String.valueOf("Failed to create directory within 10000 attempts (tried "));
        String v3_2 = String.valueOf(String.valueOf(v2_2));
        String v2_4 = String.valueOf(String.valueOf(v2_2));
        throw new IllegalStateException(new StringBuilder((((v1_4.length() + 17) + v3_2.length()) + v2_4.length())).append(v1_4).append(v3_2).append("0 to ").append(v2_4).append(9999).append(")").toString());
    }

    private static Object a(java.io.File p1, com.a.b.i.o p2)
    {
        return com.a.b.i.bc.a(p1).a(p2);
    }

    private static Object a(java.io.File p1, java.nio.charset.Charset p2, com.a.b.i.by p3)
    {
        return com.a.b.i.bc.c(p1, p2).a(p3);
    }

    private static String a(String p5)
    {
        String v0_6;
        com.a.b.b.cn.a(p5);
        if (p5.length() != 0) {
            String v0_3 = com.a.b.b.di.a(47).a().a(p5);
            boolean v1_1 = new java.util.ArrayList();
            int v2_0 = v0_3.iterator();
            while (v2_0.hasNext()) {
                String v0_10 = ((String) v2_0.next());
                if (!v0_10.equals(".")) {
                    if (!v0_10.equals("..")) {
                        v1_1.add(v0_10);
                    } else {
                        if ((v1_1.size() <= 0) || (((String) v1_1.get((v1_1.size() - 1))).equals(".."))) {
                            v1_1.add("..");
                        } else {
                            v1_1.remove((v1_1.size() - 1));
                        }
                    }
                }
            }
            v0_6 = com.a.b.b.bv.a(47).a(v1_1);
            if (p5.charAt(0) == 47) {
                String v0_7 = String.valueOf(v0_6);
                if (v0_7.length() == 0) {
                    v0_6 = new String("/");
                } else {
                    v0_6 = "/".concat(v0_7);
                }
            }
            while (v0_6.startsWith("/../")) {
                v0_6 = v0_6.substring(3);
            }
            if (!v0_6.equals("/..")) {
                if ("".equals(v0_6)) {
                    v0_6 = ".";
                }
            } else {
                v0_6 = "/";
            }
        } else {
            v0_6 = ".";
        }
        return v0_6;
    }

    private static java.nio.MappedByteBuffer a(java.io.File p2, java.nio.channels.FileChannel$MapMode p3)
    {
        com.a.b.b.cn.a(p2);
        com.a.b.b.cn.a(p3);
        if (p2.exists()) {
            return com.a.b.i.bc.a(p2, p3, p2.length());
        } else {
            throw new java.io.FileNotFoundException(p2.toString());
        }
    }

    private static java.nio.MappedByteBuffer a(java.io.File p4, java.nio.channels.FileChannel$MapMode p5, long p6)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.b.cn.a(p5);
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            java.nio.MappedByteBuffer v0_1;
            if (p5 != java.nio.channels.FileChannel$MapMode.READ_ONLY) {
                v0_1 = "rw";
            } else {
                v0_1 = "r";
            }
        } catch (java.nio.MappedByteBuffer v0_2) {
            throw v1.a(v0_2);
        } catch (java.nio.MappedByteBuffer v0_4) {
            v1.close();
            throw v0_4;
        }
        java.nio.MappedByteBuffer v0_7 = com.a.b.i.bc.a(((java.io.RandomAccessFile) v1.a(new java.io.RandomAccessFile(p4, v0_1))), p5, p6);
        v1.close();
        return v0_7;
    }

    private static java.nio.MappedByteBuffer a(java.io.RandomAccessFile p8, java.nio.channels.FileChannel$MapMode p9, long p10)
    {
        com.a.b.i.ar v6 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_3 = ((java.nio.channels.FileChannel) v6.a(p8.getChannel())).map(p9, 0, p10);
            v6.close();
            return v0_3;
        } catch (RuntimeException v0_6) {
            v6.close();
            throw v0_6;
        } catch (RuntimeException v0_4) {
            throw v6.a(v0_4);
        }
    }

    private static void a(java.io.File p5, java.io.File p6)
    {
        com.a.b.i.s v0_1;
        if (p5.equals(p6)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4_1 = new Object[2];
        v4_1[0] = p5;
        v4_1[1] = p6;
        com.a.b.b.cn.a(v0_1, "Source %s and destination %s must be different", v4_1);
        com.a.b.i.p v1_1 = new com.a.b.i.bb[0];
        com.a.b.i.bc.a(p5).a(com.a.b.i.bc.a(p6, v1_1));
        return;
    }

    private static void a(java.io.File p1, java.io.OutputStream p2)
    {
        com.a.b.i.bc.a(p1).a(p2);
        return;
    }

    private static void a(java.io.File p1, java.nio.charset.Charset p2, Appendable p3)
    {
        com.a.b.i.bc.c(p1, p2).a(p3);
        return;
    }

    private static void a(CharSequence p1, java.io.File p2, java.nio.charset.Charset p3)
    {
        com.a.b.i.ag v0_1 = new com.a.b.i.bb[0];
        com.a.b.i.bc.a(p2, p3, v0_1).a(p1);
        return;
    }

    private static void a(byte[] p2, java.io.File p3)
    {
        RuntimeException v0_1 = new com.a.b.i.bb[0];
        RuntimeException v0_2 = com.a.b.i.bc.a(p3, v0_1);
        com.a.b.b.cn.a(p2);
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_5 = ((java.io.OutputStream) v1.a(v0_2.a()));
            v0_5.write(p2);
            v0_5.flush();
            v1.close();
            return;
        } catch (RuntimeException v0_8) {
            v1.close();
            throw v0_8;
        } catch (RuntimeException v0_6) {
            throw v1.a(v0_6);
        }
    }

    static byte[] a(java.io.InputStream p3, long p4)
    {
        if (p4 <= 2147483647) {
            byte[] v0_5;
            if (p4 != 0) {
                v0_5 = com.a.b.i.z.a(p3, ((int) p4));
            } else {
                v0_5 = com.a.b.i.z.a(p3);
            }
            return v0_5;
        } else {
            throw new OutOfMemoryError(new StringBuilder(68).append("file is too large to fit in a byte array: ").append(p4).append(" bytes").toString());
        }
    }

    private static com.a.b.i.bb[] a(boolean p3)
    {
        com.a.b.i.bb[] v0_0;
        if (!p3) {
            v0_0 = new com.a.b.i.bb[0];
        } else {
            v0_0 = new com.a.b.i.bb[1];
            v0_0[0] = com.a.b.i.bb.a;
        }
        return v0_0;
    }

    private static com.a.b.d.aga b()
    {
        return com.a.b.i.bc.b;
    }

    private static java.io.BufferedWriter b(java.io.File p3, java.nio.charset.Charset p4)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p4);
        return new java.io.BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(p3), p4));
    }

    private static String b(String p3)
    {
        String v0_3;
        com.a.b.b.cn.a(p3);
        String v0_2 = new java.io.File(p3).getName();
        int v1_1 = v0_2.lastIndexOf(46);
        if (v1_1 != -1) {
            v0_3 = v0_2.substring((v1_1 + 1));
        } else {
            v0_3 = "";
        }
        return v0_3;
    }

    private static void b(CharSequence p3, java.io.File p4, java.nio.charset.Charset p5)
    {
        com.a.b.i.ag v0_1 = new com.a.b.i.bb[1];
        v0_1[0] = com.a.b.i.bb.a;
        com.a.b.i.bc.a(p4, p5, v0_1).a(p3);
        return;
    }

    private static boolean b(java.io.File p8, java.io.File p9)
    {
        int v0_4;
        com.a.b.b.cn.a(p8);
        com.a.b.b.cn.a(p9);
        if ((p8 != p9) && (!p8.equals(p9))) {
            int v0_1 = p8.length();
            long v2 = p9.length();
            if ((v0_1 == 0) || ((v2 == 0) || (v0_1 == v2))) {
                v0_4 = com.a.b.i.bc.a(p8).a(com.a.b.i.bc.a(p9));
            } else {
                v0_4 = 0;
            }
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    private static byte[] b(java.io.File p1)
    {
        return com.a.b.i.bc.a(p1).e();
    }

    private static com.a.b.b.co c()
    {
        return com.a.b.i.bh.a;
    }

    private static com.a.b.i.ah c(java.io.File p1, java.nio.charset.Charset p2)
    {
        return com.a.b.i.bc.a(p1).a(p2);
    }

    private static String c(String p3)
    {
        com.a.b.b.cn.a(p3);
        String v0_2 = new java.io.File(p3).getName();
        int v1_1 = v0_2.lastIndexOf(46);
        if (v1_1 != -1) {
            v0_2 = v0_2.substring(0, v1_1);
        }
        return v0_2;
    }

    private static void c(java.io.File p4)
    {
        com.a.b.b.cn.a(p4);
        if ((p4.createNewFile()) || (p4.setLastModified(System.currentTimeMillis()))) {
            return;
        } else {
            String v1_1 = String.valueOf(String.valueOf(p4));
            throw new java.io.IOException(new StringBuilder((v1_1.length() + 38)).append("Unable to update modification time of ").append(v1_1).toString());
        }
    }

    private static void c(java.io.File p6, java.io.File p7)
    {
        java.io.IOException v0_1;
        com.a.b.b.cn.a(p6);
        com.a.b.b.cn.a(p7);
        if (p6.equals(p7)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4_0 = new Object[2];
        v4_0[0] = p6;
        v4_0[1] = p7;
        com.a.b.b.cn.a(v0_1, "Source %s and destination %s must be different", v4_0);
        if (!p6.renameTo(p7)) {
            java.io.IOException v0_4;
            if (p6.equals(p7)) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            Object[] v4_1 = new Object[2];
            v4_1[0] = p6;
            v4_1[1] = p7;
            com.a.b.b.cn.a(v0_4, "Source %s and destination %s must be different", v4_1);
            String v1_1 = new com.a.b.i.bb[0];
            com.a.b.i.bc.a(p6).a(com.a.b.i.bc.a(p7, v1_1));
            if (!p6.delete()) {
                if (p7.delete()) {
                    String v1_4 = String.valueOf(String.valueOf(p6));
                    throw new java.io.IOException(new StringBuilder((v1_4.length() + 17)).append("Unable to delete ").append(v1_4).toString());
                } else {
                    String v1_8 = String.valueOf(String.valueOf(p7));
                    throw new java.io.IOException(new StringBuilder((v1_8.length() + 17)).append("Unable to delete ").append(v1_8).toString());
                }
            }
        }
        return;
    }

    private static void c(CharSequence p3, java.io.File p4, java.nio.charset.Charset p5)
    {
        com.a.b.i.ag v0_1 = new com.a.b.i.bb[1];
        v0_1[0] = com.a.b.i.bb.a;
        com.a.b.i.bc.a(p4, p5, v0_1).a(p3);
        return;
    }

    private static com.a.b.b.co d()
    {
        return com.a.b.i.bh.b;
    }

    private static String d(java.io.File p1, java.nio.charset.Charset p2)
    {
        return com.a.b.i.bc.c(p1, p2).b();
    }

    private static void d(java.io.File p4)
    {
        com.a.b.b.cn.a(p4);
        java.io.IOException v0_1 = p4.getCanonicalFile().getParentFile();
        if (v0_1 != null) {
            v0_1.mkdirs();
            if (!v0_1.isDirectory()) {
                String v1_1 = String.valueOf(String.valueOf(p4));
                throw new java.io.IOException(new StringBuilder((v1_1.length() + 39)).append("Unable to create parent directories of ").append(v1_1).toString());
            }
        }
        return;
    }

    private static String e(java.io.File p1, java.nio.charset.Charset p2)
    {
        return com.a.b.i.bc.c(p1, p2).c();
    }

    private static java.nio.MappedByteBuffer e(java.io.File p4)
    {
        com.a.b.b.cn.a(p4);
        java.nio.MappedByteBuffer v0_0 = java.nio.channels.FileChannel$MapMode.READ_ONLY;
        com.a.b.b.cn.a(p4);
        com.a.b.b.cn.a(v0_0);
        if (p4.exists()) {
            return com.a.b.i.bc.a(p4, v0_0, p4.length());
        } else {
            throw new java.io.FileNotFoundException(p4.toString());
        }
    }

    private static java.util.List f(java.io.File p2, java.nio.charset.Charset p3)
    {
        return ((java.util.List) com.a.b.i.bc.c(p2, p3).a(new com.a.b.i.bd()));
    }
}
