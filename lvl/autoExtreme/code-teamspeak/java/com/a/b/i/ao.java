package com.a.b.i;
final class ao extends java.io.Reader {
    final synthetic Readable a;

    ao(Readable p1)
    {
        this.a = p1;
        return;
    }

    public final void close()
    {
        if ((this.a instanceof java.io.Closeable)) {
            ((java.io.Closeable) this.a).close();
        }
        return;
    }

    public final int read(java.nio.CharBuffer p2)
    {
        return this.a.read(p2);
    }

    public final int read(char[] p2, int p3, int p4)
    {
        return this.read(java.nio.CharBuffer.wrap(p2, p3, p4));
    }
}
