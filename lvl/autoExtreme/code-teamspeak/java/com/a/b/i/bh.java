package com.a.b.i;
abstract enum class bh extends java.lang.Enum implements com.a.b.b.co {
    public static final enum com.a.b.i.bh a;
    public static final enum com.a.b.i.bh b;
    private static final synthetic com.a.b.i.bh[] c;

    static bh()
    {
        com.a.b.i.bh.a = new com.a.b.i.bi("IS_DIRECTORY");
        com.a.b.i.bh.b = new com.a.b.i.bj("IS_FILE");
        com.a.b.i.bh[] v0_5 = new com.a.b.i.bh[2];
        v0_5[0] = com.a.b.i.bh.a;
        v0_5[1] = com.a.b.i.bh.b;
        com.a.b.i.bh.c = v0_5;
        return;
    }

    private bh(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic bh(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.i.bh valueOf(String p1)
    {
        return ((com.a.b.i.bh) Enum.valueOf(com.a.b.i.bh, p1));
    }

    public static com.a.b.i.bh[] values()
    {
        return ((com.a.b.i.bh[]) com.a.b.i.bh.c.clone());
    }
}
