package com.a.b.i;
public final class bz {
    final java.util.Queue a;
    private final Readable b;
    private final java.io.Reader c;
    private final char[] d;
    private final java.nio.CharBuffer e;
    private final com.a.b.i.bx f;

    public bz(Readable p2)
    {
        int v2_1;
        boolean v0_1 = new char[4096];
        this.d = v0_1;
        this.e = java.nio.CharBuffer.wrap(this.d);
        this.a = new java.util.LinkedList();
        this.f = new com.a.b.i.ca(this);
        this.b = ((Readable) com.a.b.b.cn.a(p2));
        if (!(p2 instanceof java.io.Reader)) {
            v2_1 = 0;
        } else {
            v2_1 = ((java.io.Reader) p2);
        }
        this.c = v2_1;
        return;
    }

    private static synthetic java.util.Queue a(com.a.b.i.bz p1)
    {
        return p1.a;
    }

    public final String a()
    {
        while (this.a.peek() == null) {
            com.a.b.i.bx v0_5;
            this.e.clear();
            if (this.c == null) {
                v0_5 = this.b.read(this.e);
            } else {
                v0_5 = this.c.read(this.d, 0, this.d.length);
            }
            if (v0_5 != -1) {
                this.f.a(this.d, v0_5);
            } else {
                this.f.a();
                break;
            }
        }
        return ((String) this.a.poll());
    }
}
