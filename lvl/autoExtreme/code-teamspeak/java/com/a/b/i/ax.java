package com.a.b.i;
public final class ax extends java.io.OutputStream {
    private final int a;
    private final boolean b;
    private final com.a.b.i.s c;
    private java.io.OutputStream d;
    private com.a.b.i.ba e;
    private java.io.File f;

    private ax(int p2)
    {
        this(p2, 0);
        return;
    }

    private ax(int p3, byte p4)
    {
        this.a = p3;
        this.b = 0;
        this.e = new com.a.b.i.ba(0);
        this.d = this.e;
        this.c = new com.a.b.i.az(this);
        return;
    }

    static synthetic java.io.InputStream a(com.a.b.i.ax p1)
    {
        return p1.d();
    }

    private void a(int p7)
    {
        if ((this.f == null) && ((this.e.b() + p7) > this.a)) {
            java.io.File v0_5 = java.io.File.createTempFile("FileBackedOutputStream", 0);
            if (this.b) {
                v0_5.deleteOnExit();
            }
            java.io.FileOutputStream v1_3 = new java.io.FileOutputStream(v0_5);
            v1_3.write(this.e.a(), 0, this.e.b());
            v1_3.flush();
            this.d = v1_3;
            this.f = v0_5;
            this.e = 0;
        }
        return;
    }

    private declared_synchronized java.io.File b()
    {
        try {
            return this.f;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    private com.a.b.i.s c()
    {
        return this.c;
    }

    private declared_synchronized java.io.InputStream d()
    {
        try {
            java.io.ByteArrayInputStream v0_2;
            if (this.f == null) {
                v0_2 = new java.io.ByteArrayInputStream(this.e.a(), 0, this.e.b());
            } else {
                v0_2 = new java.io.FileInputStream(this.f);
            }
        } catch (java.io.ByteArrayInputStream v0_4) {
            throw v0_4;
        }
        return v0_2;
    }

    public final declared_synchronized void a()
    {
        try {
            this.close();
        } catch (String v0_14) {
            throw v0_14;
        }
        if (this.e != null) {
            this.e.reset();
        } else {
            this.e = new com.a.b.i.ba(0);
        }
        this.d = this.e;
        if (this.f != null) {
            String v0_9 = this.f;
            this.f = 0;
            if (!v0_9.delete()) {
                String v0_11 = String.valueOf(String.valueOf(v0_9));
                throw new java.io.IOException(new StringBuilder((v0_11.length() + 18)).append("Could not delete: ").append(v0_11).toString());
            }
        }
        return;
    }

    public final declared_synchronized void close()
    {
        try {
            this.d.close();
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final declared_synchronized void flush()
    {
        try {
            this.d.flush();
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final declared_synchronized void write(int p2)
    {
        try {
            this.a(1);
            this.d.write(p2);
            return;
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final declared_synchronized void write(byte[] p3)
    {
        try {
            this.write(p3, 0, p3.length);
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final declared_synchronized void write(byte[] p2, int p3, int p4)
    {
        try {
            this.a(p4);
            this.d.write(p2, p3, p4);
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }
}
