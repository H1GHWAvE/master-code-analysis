package com.a.b.i;
final class bf extends com.a.b.i.p {
    private final java.io.File a;
    private final com.a.b.d.lo b;

    private varargs bf(java.io.File p2, com.a.b.i.bb[] p3)
    {
        this.a = ((java.io.File) com.a.b.b.cn.a(p2));
        this.b = com.a.b.d.lo.a(p3);
        return;
    }

    synthetic bf(java.io.File p1, com.a.b.i.bb[] p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    private java.io.FileOutputStream b()
    {
        return new java.io.FileOutputStream(this.a, this.b.contains(com.a.b.i.bb.a));
    }

    public final synthetic java.io.OutputStream a()
    {
        return new java.io.FileOutputStream(this.a, this.b.contains(com.a.b.i.bb.a));
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_2.length() + 20) + v1_2.length())).append("Files.asByteSink(").append(v0_2).append(", ").append(v1_2).append(")").toString();
    }
}
