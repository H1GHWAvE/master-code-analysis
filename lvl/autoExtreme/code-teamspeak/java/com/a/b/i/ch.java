package com.a.b.i;
public final class ch {

    private ch()
    {
        return;
    }

    private static com.a.b.i.ah a(java.net.URL p1, java.nio.charset.Charset p2)
    {
        return com.a.b.i.ch.a(p1).a(p2);
    }

    private static com.a.b.i.s a(java.net.URL p2)
    {
        return new com.a.b.i.cj(p2, 0);
    }

    private static Object a(java.net.URL p1, java.nio.charset.Charset p2, com.a.b.i.by p3)
    {
        return com.a.b.i.ch.a(p1, p2).a(p3);
    }

    private static java.net.URL a(Class p6, String p7)
    {
        int v0;
        java.net.URL v3 = p6.getResource(p7);
        if (v3 == null) {
            v0 = 0;
        } else {
            v0 = 1;
        }
        Object[] v5_1 = new Object[2];
        v5_1[0] = p7;
        v5_1[1] = p6.getName();
        com.a.b.b.cn.a(v0, "resource %s relative to %s not found.", v5_1);
        return v3;
    }

    private static java.net.URL a(String p5)
    {
        int v0_4;
        java.net.URL v3_2 = ((ClassLoader) com.a.b.b.ca.a(Thread.currentThread().getContextClassLoader(), com.a.b.i.ch.getClassLoader())).getResource(p5);
        if (v3_2 == null) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = p5;
        com.a.b.b.cn.a(v0_4, "resource %s not found.", v1_1);
        return v3_2;
    }

    private static void a(java.net.URL p1, java.io.OutputStream p2)
    {
        com.a.b.i.ch.a(p1).a(p2);
        return;
    }

    private static String b(java.net.URL p1, java.nio.charset.Charset p2)
    {
        return com.a.b.i.ch.a(p1, p2).b();
    }

    private static byte[] b(java.net.URL p1)
    {
        return com.a.b.i.ch.a(p1).e();
    }

    private static java.util.List c(java.net.URL p2, java.nio.charset.Charset p3)
    {
        return ((java.util.List) com.a.b.i.ch.a(p2, p3).a(new com.a.b.i.ci()));
    }
}
