package com.a.b.i;
final class ac implements com.a.b.i.n {
    final java.io.DataOutput a;
    final java.io.ByteArrayOutputStream b;

    ac(java.io.ByteArrayOutputStream p2)
    {
        this.b = p2;
        this.a = new java.io.DataOutputStream(p2);
        return;
    }

    public final byte[] a()
    {
        return this.b.toByteArray();
    }

    public final void write(int p3)
    {
        try {
            this.a.write(p3);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void write(byte[] p3)
    {
        try {
            this.a.write(p3);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void write(byte[] p3, int p4, int p5)
    {
        try {
            this.a.write(p3, p4, p5);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void writeBoolean(boolean p3)
    {
        try {
            this.a.writeBoolean(p3);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void writeByte(int p3)
    {
        try {
            this.a.writeByte(p3);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void writeBytes(String p3)
    {
        try {
            this.a.writeBytes(p3);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void writeChar(int p3)
    {
        try {
            this.a.writeChar(p3);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void writeChars(String p3)
    {
        try {
            this.a.writeChars(p3);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void writeDouble(double p4)
    {
        try {
            this.a.writeDouble(p4);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void writeFloat(float p3)
    {
        try {
            this.a.writeFloat(p3);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void writeInt(int p3)
    {
        try {
            this.a.writeInt(p3);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void writeLong(long p4)
    {
        try {
            this.a.writeLong(p4);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void writeShort(int p3)
    {
        try {
            this.a.writeShort(p3);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    public final void writeUTF(String p3)
    {
        try {
            this.a.writeUTF(p3);
            return;
        } catch (java.io.IOException v0_1) {
            throw new AssertionError(v0_1);
        }
    }
}
