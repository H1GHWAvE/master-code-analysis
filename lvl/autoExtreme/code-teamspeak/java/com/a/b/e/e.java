package com.a.b.e;
public final class e {
    private final java.util.Map a;
    private int b;

    public e()
    {
        this.b = -1;
        this.a = new java.util.HashMap();
        return;
    }

    private com.a.b.e.e a(char p4, String p5)
    {
        this.a.put(Character.valueOf(p4), com.a.b.b.cn.a(p5));
        if (p4 > this.b) {
            this.b = p4;
        }
        return this;
    }

    private com.a.b.e.e a(char[] p7, String p8)
    {
        com.a.b.b.cn.a(p8);
        int v1 = p7.length;
        int v0 = 0;
        while (v0 < v1) {
            char v2 = p7[v0];
            this.a.put(Character.valueOf(v2), com.a.b.b.cn.a(p8));
            if (v2 > this.b) {
                this.b = v2;
            }
            v0++;
        }
        return this;
    }

    private char[][] a()
    {
        char[][] v2 = new char[][(this.b + 1)];
        java.util.Iterator v3 = this.a.entrySet().iterator();
        while (v3.hasNext()) {
            char[] v0_6 = ((java.util.Map$Entry) v3.next());
            v2[((Character) v0_6.getKey()).charValue()] = ((String) v0_6.getValue()).toCharArray();
        }
        return v2;
    }

    private com.a.b.e.g b()
    {
        return new com.a.b.e.f(this.a());
    }
}
