package com.a.b.g;
public final class am {
    private static final int a;

    static am()
    {
        com.a.b.g.am.a = ((int) System.currentTimeMillis());
        return;
    }

    private am()
    {
        return;
    }

    private static int a(long p10, int p12)
    {
        int v0_0;
        int v2 = 0;
        if (p12 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.g.av v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p12);
        com.a.b.b.cn.a(v0_0, "buckets must be positive: %s", v1_1);
        com.a.b.g.av v1_3 = new com.a.b.g.av(p10);
        while(true) {
            double v4_1 = ((double) (v2 + 1));
            v1_3.a = ((2.694898184339827e-117 * v1_3.a) + 1);
            int v0_5 = ((int) (v4_1 / (((double) (((int) (v1_3.a >> 33)) + 1)) / 2147483648.0)));
            if ((v0_5 < 0) || (v0_5 >= p12)) {
                break;
            }
            v2 = v0_5;
        }
        return v2;
    }

    private static int a(com.a.b.g.ag p10, int p11)
    {
        int v0_0;
        int v2 = 0;
        double v4_0 = p10.d();
        if (p11 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.g.av v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p11);
        com.a.b.b.cn.a(v0_0, "buckets must be positive: %s", v1_1);
        com.a.b.g.av v1_3 = new com.a.b.g.av(v4_0);
        while(true) {
            double v4_1 = ((double) (v2 + 1));
            v1_3.a = ((2.694898184339827e-117 * v1_3.a) + 1);
            int v0_5 = ((int) (v4_1 / (((double) (((int) (v1_3.a >> 33)) + 1)) / 2147483648.0)));
            if ((v0_5 < 0) || (v0_5 >= p11)) {
                break;
            }
            v2 = v0_5;
        }
        return v2;
    }

    private static com.a.b.g.ag a(Iterable p7)
    {
        int v0_0 = p7.iterator();
        com.a.b.b.cn.a(v0_0.hasNext(), "Must be at least 1 hash code to combine.");
        byte[] v2_1 = new byte[(((com.a.b.g.ag) v0_0.next()).a() / 8)];
        java.util.Iterator v3_1 = p7.iterator();
        while (v3_1.hasNext()) {
            int v0_10;
            byte[] v4 = ((com.a.b.g.ag) v3_1.next()).e();
            if (v4.length != v2_1.length) {
                v0_10 = 0;
            } else {
                v0_10 = 1;
            }
            com.a.b.b.cn.a(v0_10, "All hashcodes must have the same bit length.");
            int v0_11 = 0;
            while (v0_11 < v4.length) {
                v2_1[v0_11] = ((byte) ((v2_1[v0_11] * 37) ^ v4[v0_11]));
                v0_11++;
            }
        }
        return com.a.b.g.ag.a(v2_1);
    }

    public static com.a.b.g.ak a()
    {
        return com.a.b.g.ay.a;
    }

    public static com.a.b.g.ak a(int p1)
    {
        return new com.a.b.g.bl(p1);
    }

    private static com.a.b.g.ak a(long p2, long p4)
    {
        return new com.a.b.g.bo(p2, p4);
    }

    static synthetic com.a.b.g.ak a(com.a.b.g.ap p2, String p3)
    {
        return new com.a.b.g.r(p2, com.a.b.g.ap.a(p2), p3);
    }

    private static com.a.b.g.ag b(Iterable p7)
    {
        int v0_0 = p7.iterator();
        com.a.b.b.cn.a(v0_0.hasNext(), "Must be at least 1 hash code to combine.");
        byte[] v2_1 = new byte[(((com.a.b.g.ag) v0_0.next()).a() / 8)];
        java.util.Iterator v3_1 = p7.iterator();
        while (v3_1.hasNext()) {
            int v0_10;
            byte[] v4 = ((com.a.b.g.ag) v3_1.next()).e();
            if (v4.length != v2_1.length) {
                v0_10 = 0;
            } else {
                v0_10 = 1;
            }
            com.a.b.b.cn.a(v0_10, "All hashcodes must have the same bit length.");
            int v0_11 = 0;
            while (v0_11 < v4.length) {
                v2_1[v0_11] = ((byte) (v2_1[v0_11] + v4[v0_11]));
                v0_11++;
            }
        }
        return com.a.b.g.ag.a(v2_1);
    }

    public static com.a.b.g.ak b()
    {
        return com.a.b.g.ax.a;
    }

    public static com.a.b.g.ak b(int p1)
    {
        return new com.a.b.g.bj(p1);
    }

    private static com.a.b.g.ak b(com.a.b.g.ap p2, String p3)
    {
        return new com.a.b.g.r(p2, com.a.b.g.ap.a(p2), p3);
    }

    static synthetic int c()
    {
        return com.a.b.g.am.a;
    }

    private static com.a.b.g.ak c(int p5)
    {
        com.a.b.g.as v0_0;
        int v1 = 1;
        if (p5 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.g.as v0_7;
        com.a.b.b.cn.a(v0_0, "Number of bits must be positive");
        com.a.b.g.as v0_2 = ((p5 + 31) & -32);
        if (v0_2 != 32) {
            if (v0_2 > 128) {
                int v3_3 = ((v0_2 + 127) / 128);
                com.a.b.g.ak[] v4 = new com.a.b.g.ak[v3_3];
                v4[0] = com.a.b.g.ax.b;
                com.a.b.g.as v0_5 = com.a.b.g.am.a;
                while (v1 < v3_3) {
                    v0_5 += 1500450271;
                    v4[v1] = com.a.b.g.am.b(v0_5);
                    v1++;
                }
                v0_7 = new com.a.b.g.as(v4);
            } else {
                v0_7 = com.a.b.g.ax.b;
            }
        } else {
            v0_7 = com.a.b.g.ay.b;
        }
        return v0_7;
    }

    private static int d(int p2)
    {
        int v0_0;
        if (p2 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0, "Number of bits must be positive");
        return ((p2 + 31) & -32);
    }

    private static com.a.b.g.ak d()
    {
        return com.a.b.g.bc.a;
    }

    private static com.a.b.g.ak e()
    {
        return com.a.b.g.aw.a;
    }

    private static com.a.b.g.ak f()
    {
        return com.a.b.g.az.a;
    }

    private static com.a.b.g.ak g()
    {
        return com.a.b.g.ba.a;
    }

    private static com.a.b.g.ak h()
    {
        return com.a.b.g.bb.a;
    }

    private static com.a.b.g.ak i()
    {
        return com.a.b.g.au.a;
    }

    private static com.a.b.g.ak j()
    {
        return com.a.b.g.at.a;
    }

    private static com.a.b.g.ak k()
    {
        return com.a.b.g.ao.a;
    }
}
