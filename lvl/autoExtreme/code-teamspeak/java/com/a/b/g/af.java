package com.a.b.g;
final enum class af extends java.lang.Enum implements com.a.b.g.w {
    public static final enum com.a.b.g.af a;
    private static final synthetic com.a.b.g.af[] b;

    static af()
    {
        com.a.b.g.af.a = new com.a.b.g.af("INSTANCE");
        com.a.b.g.af[] v0_3 = new com.a.b.g.af[1];
        v0_3[0] = com.a.b.g.af.a;
        com.a.b.g.af.b = v0_3;
        return;
    }

    private af(String p2)
    {
        this(p2, 0);
        return;
    }

    private static void a(CharSequence p0, com.a.b.g.bn p1)
    {
        p1.b(p0);
        return;
    }

    public static com.a.b.g.af valueOf(String p1)
    {
        return ((com.a.b.g.af) Enum.valueOf(com.a.b.g.af, p1));
    }

    public static com.a.b.g.af[] values()
    {
        return ((com.a.b.g.af[]) com.a.b.g.af.b.clone());
    }

    public final synthetic void a(Object p1, com.a.b.g.bn p2)
    {
        p2.b(((CharSequence) p1));
        return;
    }

    public final String toString()
    {
        return "Funnels.unencodedCharsFunnel()";
    }
}
