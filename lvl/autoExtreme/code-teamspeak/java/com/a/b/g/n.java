package com.a.b.g;
abstract enum class n extends java.lang.Enum implements com.a.b.g.m {
    public static final enum com.a.b.g.n a;
    public static final enum com.a.b.g.n b;
    private static final synthetic com.a.b.g.n[] c;

    static n()
    {
        com.a.b.g.n.a = new com.a.b.g.o("MURMUR128_MITZ_32");
        com.a.b.g.n.b = new com.a.b.g.p("MURMUR128_MITZ_64");
        com.a.b.g.n[] v0_5 = new com.a.b.g.n[2];
        v0_5[0] = com.a.b.g.n.a;
        v0_5[1] = com.a.b.g.n.b;
        com.a.b.g.n.c = v0_5;
        return;
    }

    private n(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic n(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.g.n valueOf(String p1)
    {
        return ((com.a.b.g.n) Enum.valueOf(com.a.b.g.n, p1));
    }

    public static com.a.b.g.n[] values()
    {
        return ((com.a.b.g.n[]) com.a.b.g.n.c.clone());
    }
}
