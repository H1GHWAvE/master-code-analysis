package com.a.b.g;
final class bl extends com.a.b.g.h implements java.io.Serializable {
    private static final int a = 3432918353;
    private static final int b = 461845907;
    private static final long d;
    private final int c;

    bl(int p1)
    {
        this.c = p1;
        return;
    }

    static synthetic int a(int p1, int p2)
    {
        return com.a.b.g.bl.c(p1, p2);
    }

    static synthetic com.a.b.g.ag b(int p1, int p2)
    {
        return com.a.b.g.bl.d(p1, p2);
    }

    static synthetic int c(int p1)
    {
        return com.a.b.g.bl.d(p1);
    }

    private static int c(int p2, int p3)
    {
        return ((Integer.rotateLeft((p2 ^ p3), 13) * 5) + -430675100);
    }

    private static int d(int p2)
    {
        return (Integer.rotateLeft((-862048943 * p2), 15) * 461845907);
    }

    private static com.a.b.g.ag d(int p2, int p3)
    {
        com.a.b.g.ag v0_0 = (p2 ^ p3);
        com.a.b.g.ag v0_2 = ((v0_0 ^ (v0_0 >> 16)) * -2048144789);
        com.a.b.g.ag v0_4 = ((v0_2 ^ (v0_2 >> 13)) * -1028477387);
        return com.a.b.g.ag.a((v0_4 ^ (v0_4 >> 16)));
    }

    public final com.a.b.g.ag a(long p6)
    {
        return com.a.b.g.bl.d(com.a.b.g.bl.c(com.a.b.g.bl.c(this.c, com.a.b.g.bl.d(((int) p6))), com.a.b.g.bl.d(((int) (p6 >> 32)))), 8);
    }

    public final com.a.b.g.ag a(CharSequence p6)
    {
        int v2 = this.c;
        com.a.b.g.ag v0_1 = 1;
        while (v0_1 < p6.length()) {
            v2 = com.a.b.g.bl.c(v2, com.a.b.g.bl.d((p6.charAt((v0_1 - 1)) | (p6.charAt(v0_1) << 16))));
            v0_1 += 2;
        }
        if ((p6.length() & 1) == 1) {
            v2 ^= com.a.b.g.bl.d(p6.charAt((p6.length() - 1)));
        }
        return com.a.b.g.bl.d(v2, (p6.length() * 2));
    }

    public final com.a.b.g.al a()
    {
        return new com.a.b.g.bm(this.c);
    }

    public final int b()
    {
        return 32;
    }

    public final com.a.b.g.ag b(int p3)
    {
        return com.a.b.g.bl.d(com.a.b.g.bl.c(this.c, com.a.b.g.bl.d(p3)), 4);
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.g.bl)) && (this.c == ((com.a.b.g.bl) p4).c)) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        return (this.getClass().hashCode() ^ this.c);
    }

    public final String toString()
    {
        return new StringBuilder(31).append("Hashing.murmur3_32(").append(this.c).append(")").toString();
    }
}
