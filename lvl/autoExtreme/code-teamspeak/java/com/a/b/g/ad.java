package com.a.b.g;
 class ad implements com.a.b.g.w, java.io.Serializable {
    private final java.nio.charset.Charset a;

    ad(java.nio.charset.Charset p2)
    {
        this.a = ((java.nio.charset.Charset) com.a.b.b.cn.a(p2));
        return;
    }

    private Object a()
    {
        return new com.a.b.g.ae(this.a);
    }

    private void a(CharSequence p2, com.a.b.g.bn p3)
    {
        p3.b(p2, this.a);
        return;
    }

    public final synthetic void a(Object p2, com.a.b.g.bn p3)
    {
        p3.b(((CharSequence) p2), this.a);
        return;
    }

    public boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.g.ad)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.g.ad) p3).a);
        }
        return v0_1;
    }

    public int hashCode()
    {
        return (com.a.b.g.ad.hashCode() ^ this.a.hashCode());
    }

    public String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.a.name()));
        return new StringBuilder((v0_3.length() + 22)).append("Funnels.stringFunnel(").append(v0_3).append(")").toString();
    }
}
