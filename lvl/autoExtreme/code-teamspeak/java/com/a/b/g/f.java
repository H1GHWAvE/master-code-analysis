package com.a.b.g;
final class f extends com.a.b.g.d {
    static final int b = 255;
    final com.a.b.g.g a;
    final synthetic com.a.b.g.e c;

    f(com.a.b.g.e p2, int p3)
    {
        this.c = p2;
        this.a = new com.a.b.g.g(p3);
        return;
    }

    public final com.a.b.g.ag a()
    {
        return this.c.a(this.a.a(), this.a.b());
    }

    public final com.a.b.g.al a(char p3)
    {
        this.a.write((p3 & 255));
        this.a.write(((p3 >> 8) & 255));
        return this;
    }

    public final com.a.b.g.al a(int p3)
    {
        this.a.write((p3 & 255));
        this.a.write(((p3 >> 8) & 255));
        this.a.write(((p3 >> 16) & 255));
        this.a.write(((p3 >> 24) & 255));
        return this;
    }

    public final com.a.b.g.al a(long p8)
    {
        int v0 = 0;
        while (v0 < 64) {
            this.a.write(((byte) ((int) ((p8 >> v0) & 255))));
            v0 += 8;
        }
        return this;
    }

    public final com.a.b.g.al a(Object p1, com.a.b.g.w p2)
    {
        p2.a(p1, this);
        return this;
    }

    public final com.a.b.g.al a(short p3)
    {
        this.a.write((p3 & 255));
        this.a.write(((p3 >> 8) & 255));
        return this;
    }

    public final com.a.b.g.al b(byte p2)
    {
        this.a.write(p2);
        return this;
    }

    public final com.a.b.g.al b(byte[] p3)
    {
        try {
            this.a.write(p3);
            return this;
        } catch (java.io.IOException v0_1) {
            throw new RuntimeException(v0_1);
        }
    }

    public final com.a.b.g.al b(byte[] p2, int p3, int p4)
    {
        this.a.write(p2, p3, p4);
        return this;
    }

    public final synthetic com.a.b.g.bn b(char p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(int p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(long p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(short p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn c(byte p2)
    {
        return this.b(p2);
    }

    public final synthetic com.a.b.g.bn c(byte[] p2)
    {
        return this.b(p2);
    }

    public final synthetic com.a.b.g.bn c(byte[] p2, int p3, int p4)
    {
        return this.b(p2, p3, p4);
    }
}
