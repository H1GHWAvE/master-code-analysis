package com.a.b.g;
final class ah extends com.a.b.g.ag implements java.io.Serializable {
    private static final long b;
    final byte[] a;

    ah(byte[] p2)
    {
        this.a = ((byte[]) com.a.b.b.cn.a(p2));
        return;
    }

    public final int a()
    {
        return (this.a.length * 8);
    }

    final void a(byte[] p3, int p4, int p5)
    {
        System.arraycopy(this.a, 0, p3, p4, p5);
        return;
    }

    final boolean a(com.a.b.g.ag p3)
    {
        return java.security.MessageDigest.isEqual(this.a, p3.f());
    }

    public final int b()
    {
        int v0_2;
        if (this.a.length < 4) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        Object[] v4 = new Object[1];
        v4[0] = Integer.valueOf(this.a.length);
        com.a.b.b.cn.b(v0_2, "HashCode#asInt() requires >= 4 bytes (it only has %s bytes).", v4);
        return ((((this.a[0] & 255) | ((this.a[1] & 255) << 8)) | ((this.a[2] & 255) << 16)) | ((this.a[3] & 255) << 24));
    }

    public final long c()
    {
        long v0_2;
        if (this.a.length < 8) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(this.a.length);
        com.a.b.b.cn.b(v0_2, "HashCode#asLong() requires >= 8 bytes (it only has %s bytes).", v1_1);
        return this.d();
    }

    public final long d()
    {
        long v2 = ((long) (this.a[0] & 255));
        int v0_3 = 1;
        while (v0_3 < Math.min(this.a.length, 8)) {
            v2 |= ((((long) this.a[v0_3]) & 255) << (v0_3 * 8));
            v0_3++;
        }
        return v2;
    }

    public final byte[] e()
    {
        return ((byte[]) this.a.clone());
    }

    final byte[] f()
    {
        return this.a;
    }
}
