package com.a.b.g;
final class c implements com.a.b.g.al {
    final synthetic com.a.b.g.al[] a;
    final synthetic com.a.b.g.b b;

    c(com.a.b.g.b p1, com.a.b.g.al[] p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final com.a.b.g.ag a()
    {
        return this.b.a(this.a);
    }

    public final com.a.b.g.al a(char p5)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].a(p5);
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al a(double p6)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].a(p6);
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al a(float p5)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].a(p5);
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al a(int p5)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].a(p5);
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al a(long p6)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].a(p6);
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al a(CharSequence p5)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].a(p5);
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al a(CharSequence p5, java.nio.charset.Charset p6)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].a(p5, p6);
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al a(Object p5, com.a.b.g.w p6)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].a(p5, p6);
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al a(short p5)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].a(p5);
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al a(boolean p5)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].a(p5);
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al b(byte p5)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].b(p5);
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al b(byte[] p5)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].b(p5);
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al b(byte[] p5, int p6, int p7)
    {
        com.a.b.g.al[] v1 = this.a;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            v1[v0].b(p5, p6, p7);
            v0++;
        }
        return this;
    }

    public final synthetic com.a.b.g.bn b(char p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(double p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(float p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(int p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(long p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(CharSequence p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(CharSequence p2, java.nio.charset.Charset p3)
    {
        return this.a(p2, p3);
    }

    public final synthetic com.a.b.g.bn b(short p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(boolean p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn c(byte p2)
    {
        return this.b(p2);
    }

    public final synthetic com.a.b.g.bn c(byte[] p2)
    {
        return this.b(p2);
    }

    public final synthetic com.a.b.g.bn c(byte[] p2, int p3, int p4)
    {
        return this.b(p2, p3, p4);
    }
}
