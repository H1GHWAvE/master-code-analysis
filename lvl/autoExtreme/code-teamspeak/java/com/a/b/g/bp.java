package com.a.b.g;
final class bp extends com.a.b.g.i {
    private static final int a = 8;
    private final int b;
    private final int c;
    private long d;
    private long e;
    private long f;
    private long g;
    private long h;
    private long i;

    bp(int p6, int p7, long p8, long p10)
    {
        this(8);
        this.d = 1.0986868386607877e+248;
        this.e = 6.222199573468475e+175;
        this.f = 3.4208747916531402e+214;
        this.g = 4.901176695720602e+252;
        this.h = 0;
        this.i = 0;
        this.b = p6;
        this.c = p7;
        this.d = (this.d ^ p8);
        this.e = (this.e ^ p10);
        this.f = (this.f ^ p8);
        this.g = (this.g ^ p10);
        return;
    }

    private void c(int p8)
    {
        int v0 = 0;
        while (v0 < p8) {
            this.d = (this.d + this.e);
            this.f = (this.f + this.g);
            this.e = Long.rotateLeft(this.e, 13);
            this.g = Long.rotateLeft(this.g, 16);
            this.e = (this.e ^ this.d);
            this.g = (this.g ^ this.f);
            this.d = Long.rotateLeft(this.d, 32);
            this.f = (this.f + this.e);
            this.d = (this.d + this.g);
            this.e = Long.rotateLeft(this.e, 17);
            this.g = Long.rotateLeft(this.g, 21);
            this.e = (this.e ^ this.f);
            this.g = (this.g ^ this.d);
            this.f = Long.rotateLeft(this.f, 32);
            v0++;
        }
        return;
    }

    private void c(long p4)
    {
        this.g = (this.g ^ p4);
        this.c(this.b);
        this.d = (this.d ^ p4);
        return;
    }

    protected final void a(java.nio.ByteBuffer p5)
    {
        this.h = (this.h + 8);
        this.c(p5.getLong());
        return;
    }

    public final com.a.b.g.ag b()
    {
        this.i = (this.i ^ (this.h << 56));
        this.c(this.i);
        this.f = (this.f ^ 255);
        this.c(this.c);
        return com.a.b.g.ag.a((((this.d ^ this.e) ^ this.f) ^ this.g));
    }

    protected final void b(java.nio.ByteBuffer p9)
    {
        this.h = (this.h + ((long) p9.remaining()));
        int v0_2 = 0;
        while (p9.hasRemaining()) {
            this.i = (this.i ^ ((((long) p9.get()) & 255) << v0_2));
            v0_2 += 8;
        }
        return;
    }
}
