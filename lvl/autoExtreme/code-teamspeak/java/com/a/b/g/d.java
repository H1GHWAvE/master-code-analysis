package com.a.b.g;
abstract class d implements com.a.b.g.al {

    d()
    {
        return;
    }

    public final com.a.b.g.al a(double p4)
    {
        return this.a(Double.doubleToRawLongBits(p4));
    }

    public final com.a.b.g.al a(float p2)
    {
        return this.a(Float.floatToRawIntBits(p2));
    }

    public com.a.b.g.al a(CharSequence p4)
    {
        int v0 = 0;
        int v1 = p4.length();
        while (v0 < v1) {
            this.a(p4.charAt(v0));
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al a(CharSequence p2, java.nio.charset.Charset p3)
    {
        return this.b(p2.toString().getBytes(p3));
    }

    public final com.a.b.g.al a(boolean p2)
    {
        com.a.b.g.al v0_0;
        if (!p2) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        return this.b(v0_0);
    }

    public final synthetic com.a.b.g.bn b(double p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(float p2)
    {
        return this.a(p2);
    }

    public synthetic com.a.b.g.bn b(CharSequence p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(CharSequence p2, java.nio.charset.Charset p3)
    {
        return this.a(p2, p3);
    }

    public final synthetic com.a.b.g.bn b(boolean p2)
    {
        return this.a(p2);
    }
}
