package com.a.b.g;
 class ab implements com.a.b.g.w, java.io.Serializable {
    private final com.a.b.g.w a;

    ab(com.a.b.g.w p2)
    {
        this.a = ((com.a.b.g.w) com.a.b.b.cn.a(p2));
        return;
    }

    private void a(Iterable p4, com.a.b.g.bn p5)
    {
        java.util.Iterator v0 = p4.iterator();
        while (v0.hasNext()) {
            this.a.a(v0.next(), p5);
        }
        return;
    }

    public final synthetic void a(Object p4, com.a.b.g.bn p5)
    {
        java.util.Iterator v0 = ((Iterable) p4).iterator();
        while (v0.hasNext()) {
            this.a.a(v0.next(), p5);
        }
        return;
    }

    public boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.g.ab)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.g.ab) p3).a);
        }
        return v0_1;
    }

    public int hashCode()
    {
        return (com.a.b.g.ab.hashCode() ^ this.a.hashCode());
    }

    public String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 26)).append("Funnels.sequentialFunnel(").append(v0_2).append(")").toString();
    }
}
