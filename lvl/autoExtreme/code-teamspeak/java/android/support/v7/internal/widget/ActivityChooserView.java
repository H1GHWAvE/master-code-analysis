package android.support.v7.internal.widget;
public final class ActivityChooserView extends android.view.ViewGroup implements android.support.v7.internal.widget.n {
    private static final String b = "ActivityChooserView";
    android.support.v4.view.n a;
    private final android.support.v7.internal.widget.y c;
    private final android.support.v7.internal.widget.z d;
    private final android.support.v7.widget.aj e;
    private final android.graphics.drawable.Drawable f;
    private final android.widget.FrameLayout g;
    private final android.widget.ImageView h;
    private final android.widget.FrameLayout i;
    private final android.widget.ImageView j;
    private final int k;
    private final android.database.DataSetObserver l;
    private final android.view.ViewTreeObserver$OnGlobalLayoutListener m;
    private android.support.v7.widget.an n;
    private android.widget.PopupWindow$OnDismissListener o;
    private boolean p;
    private int q;
    private boolean r;
    private int s;

    public ActivityChooserView(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private ActivityChooserView(android.content.Context p2, byte p3)
    {
        this(p2, 0);
        return;
    }

    private ActivityChooserView(android.content.Context p6, char p7)
    {
        this(p6, 0, 0);
        this.l = new android.support.v7.internal.widget.u(this);
        this.m = new android.support.v7.internal.widget.v(this);
        this.q = 4;
        int v0_5 = p6.obtainStyledAttributes(0, android.support.v7.a.n.ActivityChooserView, 0, 0);
        this.q = v0_5.getInt(android.support.v7.a.n.ActivityChooserView_initialActivityCount, 4);
        int v1_4 = v0_5.getDrawable(android.support.v7.a.n.ActivityChooserView_expandActivityOverflowButtonDrawable);
        v0_5.recycle();
        android.view.LayoutInflater.from(this.getContext()).inflate(android.support.v7.a.k.abc_activity_chooser_view, this, 1);
        this.d = new android.support.v7.internal.widget.z(this, 0);
        this.e = ((android.support.v7.widget.aj) this.findViewById(android.support.v7.a.i.activity_chooser_view_content));
        this.f = this.e.getBackground();
        this.i = ((android.widget.FrameLayout) this.findViewById(android.support.v7.a.i.default_activity_button));
        this.i.setOnClickListener(this.d);
        this.i.setOnLongClickListener(this.d);
        this.j = ((android.widget.ImageView) this.i.findViewById(android.support.v7.a.i.image));
        int v0_25 = ((android.widget.FrameLayout) this.findViewById(android.support.v7.a.i.expand_activities_button));
        v0_25.setOnClickListener(this.d);
        v0_25.setOnTouchListener(new android.support.v7.internal.widget.w(this, v0_25));
        this.g = v0_25;
        this.h = ((android.widget.ImageView) v0_25.findViewById(android.support.v7.a.i.image));
        this.h.setImageDrawable(v1_4);
        this.c = new android.support.v7.internal.widget.y(this, 0);
        this.c.registerDataSetObserver(new android.support.v7.internal.widget.x(this));
        int v0_32 = p6.getResources();
        this.k = Math.max((v0_32.getDisplayMetrics().widthPixels / 2), v0_32.getDimensionPixelSize(android.support.v7.a.g.abc_config_prefDialogWidth));
        return;
    }

    static synthetic android.support.v7.internal.widget.y a(android.support.v7.internal.widget.ActivityChooserView p1)
    {
        return p1.c;
    }

    private void a(int p7)
    {
        if (this.c.c != null) {
            android.support.v7.widget.ar v0_5;
            this.getViewTreeObserver().addOnGlobalLayoutListener(this.m);
            if (this.i.getVisibility() != 0) {
                v0_5 = 0;
            } else {
                v0_5 = 1;
            }
            android.support.v7.widget.an v3_3;
            boolean v4_0 = this.c.c.a();
            if (v0_5 == null) {
                v3_3 = 0;
            } else {
                v3_3 = 1;
            }
            if ((p7 == 2147483647) || (v4_0 <= (v3_3 + p7))) {
                this.c.a(0);
                this.c.a(p7);
            } else {
                this.c.a(1);
                this.c.a((p7 - 1));
            }
            android.support.v7.widget.an v3_9 = this.getListPopupWindow();
            if (!v3_9.c.isShowing()) {
                if ((!this.p) && (v0_5 != null)) {
                    this.c.a(0, 0);
                } else {
                    this.c.a(1, v0_5);
                }
                v3_9.a(Math.min(this.c.a(), this.k));
                v3_9.b();
                if (this.a != null) {
                    this.a.a(1);
                }
                v3_9.d.setContentDescription(this.getContext().getString(android.support.v7.a.l.abc_activitychooserview_choose_application));
            }
            return;
        } else {
            throw new IllegalStateException("No data model. Did you call #setDataModel?");
        }
    }

    static synthetic void a(android.support.v7.internal.widget.ActivityChooserView p0, int p1)
    {
        p0.a(p1);
        return;
    }

    static synthetic boolean a(android.support.v7.internal.widget.ActivityChooserView p0, boolean p1)
    {
        p0.p = p1;
        return p1;
    }

    static synthetic android.support.v7.widget.an b(android.support.v7.internal.widget.ActivityChooserView p1)
    {
        return p1.getListPopupWindow();
    }

    static synthetic void c(android.support.v7.internal.widget.ActivityChooserView p6)
    {
        if (p6.c.getCount() <= 0) {
            p6.g.setEnabled(0);
        } else {
            p6.g.setEnabled(1);
        }
        android.support.v7.widget.aj v0_6 = p6.c.c.a();
        int v1_2 = p6.c.c.c();
        if ((v0_6 != 1) && ((v0_6 <= 1) || (v1_2 <= 0))) {
            p6.i.setVisibility(8);
        } else {
            p6.i.setVisibility(0);
            android.support.v7.widget.aj v0_11 = p6.c.c.b();
            int v1_5 = p6.getContext().getPackageManager();
            p6.j.setImageDrawable(v0_11.loadIcon(v1_5));
            if (p6.s != 0) {
                android.support.v7.widget.aj v0_12 = v0_11.loadLabel(v1_5);
                int v1_6 = p6.getContext();
                int v2_2 = p6.s;
                Object[] v3_1 = new Object[1];
                v3_1[0] = v0_12;
                p6.i.setContentDescription(v1_6.getString(v2_2, v3_1));
            }
        }
        if (p6.i.getVisibility() != 0) {
            p6.e.setBackgroundDrawable(0);
        } else {
            p6.e.setBackgroundDrawable(p6.f);
        }
        return;
    }

    private void d()
    {
        if (this.c.getCount() <= 0) {
            this.g.setEnabled(0);
        } else {
            this.g.setEnabled(1);
        }
        android.support.v7.widget.aj v0_6 = this.c.c.a();
        int v1_2 = this.c.c.c();
        if ((v0_6 != 1) && ((v0_6 <= 1) || (v1_2 <= 0))) {
            this.i.setVisibility(8);
        } else {
            this.i.setVisibility(0);
            android.support.v7.widget.aj v0_11 = this.c.c.b();
            int v1_5 = this.getContext().getPackageManager();
            this.j.setImageDrawable(v0_11.loadIcon(v1_5));
            if (this.s != 0) {
                android.support.v7.widget.aj v0_12 = v0_11.loadLabel(v1_5);
                int v1_6 = this.getContext();
                int v2_2 = this.s;
                Object[] v3_1 = new Object[1];
                v3_1[0] = v0_12;
                this.i.setContentDescription(v1_6.getString(v2_2, v3_1));
            }
        }
        if (this.i.getVisibility() != 0) {
            this.e.setBackgroundDrawable(0);
        } else {
            this.e.setBackgroundDrawable(this.f);
        }
        return;
    }

    static synthetic boolean d(android.support.v7.internal.widget.ActivityChooserView p1)
    {
        return p1.p;
    }

    static synthetic android.widget.FrameLayout e(android.support.v7.internal.widget.ActivityChooserView p1)
    {
        return p1.i;
    }

    static synthetic android.widget.FrameLayout f(android.support.v7.internal.widget.ActivityChooserView p1)
    {
        return p1.g;
    }

    static synthetic int g(android.support.v7.internal.widget.ActivityChooserView p1)
    {
        return p1.q;
    }

    private android.support.v7.widget.an getListPopupWindow()
    {
        if (this.n == null) {
            this.n = new android.support.v7.widget.an(this.getContext());
            this.n.a(this.c);
            this.n.l = this;
            this.n.c();
            this.n.m = this.d;
            this.n.a(this.d);
        }
        return this.n;
    }

    static synthetic android.widget.PopupWindow$OnDismissListener h(android.support.v7.internal.widget.ActivityChooserView p1)
    {
        return p1.o;
    }

    static synthetic android.database.DataSetObserver i(android.support.v7.internal.widget.ActivityChooserView p1)
    {
        return p1.l;
    }

    public final boolean a()
    {
        int v0_0 = 0;
        if ((!this.getListPopupWindow().c.isShowing()) && (this.r)) {
            this.p = 0;
            this.a(this.q);
            v0_0 = 1;
        }
        return v0_0;
    }

    public final boolean b()
    {
        if (this.getListPopupWindow().c.isShowing()) {
            this.getListPopupWindow().d();
            int v0_4 = this.getViewTreeObserver();
            if (v0_4.isAlive()) {
                v0_4.removeGlobalOnLayoutListener(this.m);
            }
        }
        return 1;
    }

    public final boolean c()
    {
        return this.getListPopupWindow().c.isShowing();
    }

    public final android.support.v7.internal.widget.l getDataModel()
    {
        return this.c.c;
    }

    protected final void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        int v0_1 = this.c.c;
        if (v0_1 != 0) {
            v0_1.registerObserver(this.l);
        }
        this.r = 1;
        return;
    }

    protected final void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        int v0_1 = this.c.c;
        if (v0_1 != 0) {
            v0_1.unregisterObserver(this.l);
        }
        int v0_2 = this.getViewTreeObserver();
        if (v0_2.isAlive()) {
            v0_2.removeGlobalOnLayoutListener(this.m);
        }
        if (this.c()) {
            this.b();
        }
        this.r = 0;
        return;
    }

    protected final void onLayout(boolean p5, int p6, int p7, int p8, int p9)
    {
        this.e.layout(0, 0, (p8 - p6), (p9 - p7));
        if (!this.c()) {
            this.b();
        }
        return;
    }

    protected final void onMeasure(int p4, int p5)
    {
        int v0_0 = this.e;
        if (this.i.getVisibility() != 0) {
            p5 = android.view.View$MeasureSpec.makeMeasureSpec(android.view.View$MeasureSpec.getSize(p5), 1073741824);
        }
        this.measureChild(v0_0, p4, p5);
        this.setMeasuredDimension(v0_0.getMeasuredWidth(), v0_0.getMeasuredHeight());
        return;
    }

    public final void setActivityChooserModel(android.support.v7.internal.widget.l p4)
    {
        boolean v0_0 = this.c;
        android.database.DataSetObserver v1_2 = v0_0.e.c.c;
        if ((v1_2 != null) && (v0_0.e.isShown())) {
            v1_2.unregisterObserver(v0_0.e.l);
        }
        v0_0.c = p4;
        if ((p4 != null) && (v0_0.e.isShown())) {
            p4.registerObserver(v0_0.e.l);
        }
        v0_0.notifyDataSetChanged();
        if (this.getListPopupWindow().c.isShowing()) {
            this.b();
            this.a();
        }
        return;
    }

    public final void setDefaultActionButtonContentDescription(int p1)
    {
        this.s = p1;
        return;
    }

    public final void setExpandActivityOverflowButtonContentDescription(int p3)
    {
        this.h.setContentDescription(this.getContext().getString(p3));
        return;
    }

    public final void setExpandActivityOverflowButtonDrawable(android.graphics.drawable.Drawable p2)
    {
        this.h.setImageDrawable(p2);
        return;
    }

    public final void setInitialActivityCount(int p1)
    {
        this.q = p1;
        return;
    }

    public final void setOnDismissListener(android.widget.PopupWindow$OnDismissListener p1)
    {
        this.o = p1;
        return;
    }

    public final void setProvider(android.support.v4.view.n p1)
    {
        this.a = p1;
        return;
    }
}
