package android.support.v7.internal.a;
public final class e extends android.support.v7.app.a {
    android.support.v7.internal.widget.ad i;
    boolean j;
    public android.view.Window$Callback k;
    android.support.v7.internal.view.menu.g l;
    private boolean m;
    private boolean n;
    private java.util.ArrayList o;
    private final Runnable p;
    private final android.support.v7.widget.cl q;

    public e(android.support.v7.widget.Toolbar p3, CharSequence p4, android.view.Window$Callback p5)
    {
        this.o = new java.util.ArrayList();
        this.p = new android.support.v7.internal.a.f(this);
        this.q = new android.support.v7.internal.a.g(this);
        this.i = new android.support.v7.internal.widget.ay(p3, 0);
        this.k = new android.support.v7.internal.a.k(this, p5);
        this.i.a(this.k);
        p3.setOnMenuItemClickListener(this.q);
        this.i.a(p4);
        return;
    }

    private android.view.Window$Callback B()
    {
        return this.k;
    }

    private void C()
    {
        android.support.v7.internal.view.menu.i v2_1;
        android.view.Menu v1 = this.A();
        if (!(v1 instanceof android.support.v7.internal.view.menu.i)) {
            v2_1 = 0;
        } else {
            v2_1 = ((android.support.v7.internal.view.menu.i) v1);
        }
        if (v2_1 != null) {
            v2_1.d();
        }
        try {
            v1.clear();
        } catch (boolean v0_7) {
            if (v2_1 != null) {
                v2_1.e();
            }
            throw v0_7;
        }
        if ((!this.k.onCreatePanelMenu(0, v1)) || (!this.k.onPreparePanel(0, 0, v1))) {
            v1.clear();
        }
        if (v2_1 != null) {
            v2_1.e();
        }
        return;
    }

    private static synthetic android.view.View a(android.support.v7.internal.a.e p8, android.view.Menu p9)
    {
        if ((p8.l == null) && ((p9 instanceof android.support.v7.internal.view.menu.i))) {
            android.support.v7.internal.view.menu.g v2_1 = p8.i.b();
            android.support.v7.internal.a.j v3_1 = new android.util.TypedValue();
            int v4_1 = v2_1.getResources().newTheme();
            v4_1.setTo(v2_1.getTheme());
            v4_1.resolveAttribute(android.support.v7.a.d.actionBarPopupTheme, v3_1, 1);
            if (v3_1.resourceId != 0) {
                v4_1.applyStyle(v3_1.resourceId, 1);
            }
            v4_1.resolveAttribute(android.support.v7.a.d.panelMenuListTheme, v3_1, 1);
            if (v3_1.resourceId == 0) {
                v4_1.applyStyle(android.support.v7.a.m.Theme_AppCompat_CompactMenu, 1);
            } else {
                v4_1.applyStyle(v3_1.resourceId, 1);
            }
            android.support.v7.internal.a.j v3_5 = new android.view.ContextThemeWrapper(v2_1, 0);
            v3_5.getTheme().setTo(v4_1);
            p8.l = new android.support.v7.internal.view.menu.g(v3_5, android.support.v7.a.k.abc_list_menu_item_layout);
            p8.l.g = new android.support.v7.internal.a.j(p8, 0);
            ((android.support.v7.internal.view.menu.i) p9).a(p8.l);
        }
        if ((p9 != null) && (p8.l != null)) {
            if (p8.l.d().getCount() <= 0) {
                android.view.ViewGroup v0_8 = 0;
            } else {
                v0_8 = ((android.view.View) p8.l.a(p8.i.a()));
            }
        } else {
            v0_8 = 0;
        }
        return v0_8;
    }

    private android.view.View a(android.view.Menu p9)
    {
        if ((this.l == null) && ((p9 instanceof android.support.v7.internal.view.menu.i))) {
            android.support.v7.internal.view.menu.g v2_1 = this.i.b();
            android.support.v7.internal.a.j v3_1 = new android.util.TypedValue();
            int v4_1 = v2_1.getResources().newTheme();
            v4_1.setTo(v2_1.getTheme());
            v4_1.resolveAttribute(android.support.v7.a.d.actionBarPopupTheme, v3_1, 1);
            if (v3_1.resourceId != 0) {
                v4_1.applyStyle(v3_1.resourceId, 1);
            }
            v4_1.resolveAttribute(android.support.v7.a.d.panelMenuListTheme, v3_1, 1);
            if (v3_1.resourceId == 0) {
                v4_1.applyStyle(android.support.v7.a.m.Theme_AppCompat_CompactMenu, 1);
            } else {
                v4_1.applyStyle(v3_1.resourceId, 1);
            }
            android.support.v7.internal.a.j v3_5 = new android.view.ContextThemeWrapper(v2_1, 0);
            v3_5.getTheme().setTo(v4_1);
            this.l = new android.support.v7.internal.view.menu.g(v3_5, android.support.v7.a.k.abc_list_menu_item_layout);
            this.l.g = new android.support.v7.internal.a.j(this, 0);
            ((android.support.v7.internal.view.menu.i) p9).a(this.l);
        }
        if ((p9 != null) && (this.l != null)) {
            if (this.l.d().getCount() <= 0) {
                android.view.ViewGroup v0_8 = 0;
            } else {
                v0_8 = ((android.view.View) this.l.a(this.i.a()));
            }
        } else {
            v0_8 = 0;
        }
        return v0_8;
    }

    private static synthetic android.view.Window$Callback a(android.support.v7.internal.a.e p1)
    {
        return p1.k;
    }

    private void b(android.view.Menu p7)
    {
        if ((this.l == null) && ((p7 instanceof android.support.v7.internal.view.menu.i))) {
            android.support.v7.internal.view.menu.g v0_3 = this.i.b();
            android.support.v7.internal.a.j v1_1 = new android.util.TypedValue();
            int v2_1 = v0_3.getResources().newTheme();
            v2_1.setTo(v0_3.getTheme());
            v2_1.resolveAttribute(android.support.v7.a.d.actionBarPopupTheme, v1_1, 1);
            if (v1_1.resourceId != 0) {
                v2_1.applyStyle(v1_1.resourceId, 1);
            }
            v2_1.resolveAttribute(android.support.v7.a.d.panelMenuListTheme, v1_1, 1);
            if (v1_1.resourceId == 0) {
                v2_1.applyStyle(android.support.v7.a.m.Theme_AppCompat_CompactMenu, 1);
            } else {
                v2_1.applyStyle(v1_1.resourceId, 1);
            }
            android.support.v7.internal.a.j v1_5 = new android.view.ContextThemeWrapper(v0_3, 0);
            v1_5.getTheme().setTo(v2_1);
            this.l = new android.support.v7.internal.view.menu.g(v1_5, android.support.v7.a.k.abc_list_menu_item_layout);
            this.l.g = new android.support.v7.internal.a.j(this, 0);
            ((android.support.v7.internal.view.menu.i) p7).a(this.l);
        }
        return;
    }

    private static synthetic boolean b(android.support.v7.internal.a.e p1)
    {
        return p1.j;
    }

    private static synthetic android.support.v7.internal.widget.ad c(android.support.v7.internal.a.e p1)
    {
        return p1.i;
    }

    private static synthetic boolean d(android.support.v7.internal.a.e p1)
    {
        p1.j = 1;
        return 1;
    }

    final android.view.Menu A()
    {
        if (!this.m) {
            this.i.a(new android.support.v7.internal.a.h(this, 0), new android.support.v7.internal.a.i(this, 0));
            this.m = 1;
        }
        return this.i.B();
    }

    public final int a()
    {
        return -1;
    }

    public final void a(float p2)
    {
        android.support.v4.view.cx.f(this.i.a(), p2);
        return;
    }

    public final void a(int p4)
    {
        this.a(android.view.LayoutInflater.from(this.i.b()).inflate(p4, this.i.a(), 0));
        return;
    }

    public final void a(int p5, int p6)
    {
        this.i.c(((this.i.r() & (p6 ^ -1)) | (p5 & p6)));
        return;
    }

    public final void a(android.content.res.Configuration p1)
    {
        super.a(p1);
        return;
    }

    public final void a(android.graphics.drawable.Drawable p2)
    {
        this.i.a(p2);
        return;
    }

    public final void a(android.support.v7.app.e p2)
    {
        this.o.add(p2);
        return;
    }

    public final void a(android.support.v7.app.g p3)
    {
        throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
    }

    public final void a(android.support.v7.app.g p3, int p4)
    {
        throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
    }

    public final void a(android.support.v7.app.g p3, int p4, boolean p5)
    {
        throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
    }

    public final void a(android.support.v7.app.g p3, boolean p4)
    {
        throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
    }

    public final void a(android.view.View p3)
    {
        android.support.v7.internal.widget.ad v0_1 = new android.support.v7.app.c(-2);
        if (p3 != null) {
            p3.setLayoutParams(v0_1);
        }
        this.i.a(p3);
        return;
    }

    public final void a(android.view.View p2, android.support.v7.app.c p3)
    {
        if (p2 != null) {
            p2.setLayoutParams(p3);
        }
        this.i.a(p2);
        return;
    }

    public final void a(android.widget.SpinnerAdapter p3, android.support.v7.app.f p4)
    {
        this.i.a(p3, new android.support.v7.internal.a.b(p4));
        return;
    }

    public final void a(CharSequence p2)
    {
        this.i.b(p2);
        return;
    }

    public final void a(boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = 0;
        } else {
            v0 = 1;
        }
        this.a(v0, 1);
        return;
    }

    public final boolean a(int p5, android.view.KeyEvent p6)
    {
        android.view.Menu v3 = this.A();
        if (v3 != null) {
            int v0_0;
            if (p6 == null) {
                v0_0 = -1;
            } else {
                v0_0 = p6.getDeviceId();
            }
            int v0_3;
            if (android.view.KeyCharacterMap.load(v0_0).getKeyboardType() == 1) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            v3.setQwertyMode(v0_3);
            v3.performShortcut(p5, p6, 0);
        }
        return 1;
    }

    public final boolean a(android.view.KeyEvent p3)
    {
        if (p3.getAction() == 1) {
            this.i.n();
        }
        return 1;
    }

    public final int b()
    {
        return 0;
    }

    public final void b(int p2)
    {
        this.i.a(p2);
        return;
    }

    public final void b(android.graphics.drawable.Drawable p2)
    {
        this.i.b(p2);
        return;
    }

    public final void b(android.support.v7.app.e p2)
    {
        this.o.remove(p2);
        return;
    }

    public final void b(android.support.v7.app.g p3)
    {
        throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
    }

    public final void b(CharSequence p2)
    {
        this.i.c(p2);
        return;
    }

    public final void b(boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = 0;
        } else {
            v0 = 2;
        }
        this.a(v0, 2);
        return;
    }

    public final void c()
    {
        return;
    }

    public final void c(int p2)
    {
        this.i.b(p2);
        return;
    }

    public final void c(android.graphics.drawable.Drawable p2)
    {
        this.i.e(p2);
        return;
    }

    public final void c(android.support.v7.app.g p3)
    {
        throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
    }

    public final void c(CharSequence p2)
    {
        this.i.d(p2);
        return;
    }

    public final void c(boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = 0;
        } else {
            v0 = 4;
        }
        this.a(v0, 4);
        return;
    }

    public final android.view.View d()
    {
        return this.i.y();
    }

    public final void d(int p3)
    {
        switch (this.i.v()) {
            case 1:
                this.i.e(p3);
                return;
            default:
                throw new IllegalStateException("setSelectedNavigationIndex not valid for current navigation mode");
        }
    }

    public final void d(android.graphics.drawable.Drawable p1)
    {
        return;
    }

    public final void d(CharSequence p2)
    {
        this.i.a(p2);
        return;
    }

    public final void d(boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = 0;
        } else {
            v0 = 8;
        }
        this.a(v0, 8);
        return;
    }

    public final CharSequence e()
    {
        return this.i.e();
    }

    public final void e(int p3)
    {
        int v0_0;
        android.support.v7.internal.widget.ad v1 = this.i;
        if (p3 == 0) {
            v0_0 = 0;
        } else {
            v0_0 = this.i.b().getText(p3);
        }
        v1.b(v0_0);
        return;
    }

    public final void e(android.graphics.drawable.Drawable p2)
    {
        this.i.c(p2);
        return;
    }

    public final void e(boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = 0;
        } else {
            v0 = 16;
        }
        this.a(v0, 16);
        return;
    }

    public final CharSequence f()
    {
        return this.i.f();
    }

    public final void f(int p3)
    {
        int v0_0;
        android.support.v7.internal.widget.ad v1 = this.i;
        if (p3 == 0) {
            v0_0 = 0;
        } else {
            v0_0 = this.i.b().getText(p3);
        }
        v1.c(v0_0);
        return;
    }

    public final void f(boolean p1)
    {
        return;
    }

    public final int g()
    {
        return 0;
    }

    public final void g(int p2)
    {
        this.a(p2, -1);
        return;
    }

    public final void g(boolean p1)
    {
        return;
    }

    public final int h()
    {
        return this.i.r();
    }

    public final void h(int p3)
    {
        if (p3 != 2) {
            this.i.d(p3);
            return;
        } else {
            throw new IllegalArgumentException("Tabs not supported in this configuration");
        }
    }

    public final void h(boolean p4)
    {
        if (p4 != this.n) {
            this.n = p4;
            int v1 = this.o.size();
            int v0_2 = 0;
            while (v0_2 < v1) {
                this.o.get(v0_2);
                v0_2++;
            }
        }
        return;
    }

    public final android.support.v7.app.g i()
    {
        throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
    }

    public final void i(int p3)
    {
        throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
    }

    public final android.support.v7.app.g j(int p3)
    {
        throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
    }

    public final void j()
    {
        throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
    }

    public final android.support.v7.app.g k()
    {
        throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
    }

    public final void k(int p2)
    {
        this.i.g(p2);
        return;
    }

    public final int l()
    {
        return 0;
    }

    public final void l(int p2)
    {
        this.i.h(p2);
        return;
    }

    public final int m()
    {
        return this.i.z();
    }

    public final void n()
    {
        this.i.j(0);
        return;
    }

    public final void o()
    {
        this.i.j(8);
        return;
    }

    public final boolean p()
    {
        int v0_2;
        if (this.i.A() != 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final void q()
    {
        return;
    }

    public final android.content.Context r()
    {
        return this.i.b();
    }

    public final boolean s()
    {
        return super.s();
    }

    public final float w()
    {
        return android.support.v4.view.cx.r(this.i.a());
    }

    public final boolean x()
    {
        return this.i.n();
    }

    public final boolean y()
    {
        this.i.a().removeCallbacks(this.p);
        android.support.v4.view.cx.a(this.i.a(), this.p);
        return 1;
    }

    public final boolean z()
    {
        int v0_2;
        if (!this.i.c()) {
            v0_2 = 0;
        } else {
            this.i.d();
            v0_2 = 1;
        }
        return v0_2;
    }
}
