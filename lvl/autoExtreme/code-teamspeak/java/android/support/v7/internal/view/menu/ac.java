package android.support.v7.internal.view.menu;
 class ac extends android.support.v7.internal.view.menu.e implements android.view.Menu {

    ac(android.content.Context p1, android.support.v4.g.a.a p2)
    {
        this(p1, p2);
        return;
    }

    public android.view.MenuItem add(int p2)
    {
        return this.a(((android.support.v4.g.a.a) this.d).add(p2));
    }

    public android.view.MenuItem add(int p2, int p3, int p4, int p5)
    {
        return this.a(((android.support.v4.g.a.a) this.d).add(p2, p3, p4, p5));
    }

    public android.view.MenuItem add(int p2, int p3, int p4, CharSequence p5)
    {
        return this.a(((android.support.v4.g.a.a) this.d).add(p2, p3, p4, p5));
    }

    public android.view.MenuItem add(CharSequence p2)
    {
        return this.a(((android.support.v4.g.a.a) this.d).add(p2));
    }

    public int addIntentOptions(int p11, int p12, int p13, android.content.ComponentName p14, android.content.Intent[] p15, android.content.Intent p16, int p17, android.view.MenuItem[] p18)
    {
        android.view.MenuItem[] v9 = 0;
        if (p18 != null) {
            v9 = new android.view.MenuItem[p18.length];
        }
        int v2_1 = ((android.support.v4.g.a.a) this.d).addIntentOptions(p11, p12, p13, p14, p15, p16, p17, v9);
        if (v9 != null) {
            int v1_3 = 0;
            int v3_1 = v9.length;
            while (v1_3 < v3_1) {
                p18[v1_3] = this.a(v9[v1_3]);
                v1_3++;
            }
        }
        return v2_1;
    }

    public android.view.SubMenu addSubMenu(int p2)
    {
        return this.a(((android.support.v4.g.a.a) this.d).addSubMenu(p2));
    }

    public android.view.SubMenu addSubMenu(int p2, int p3, int p4, int p5)
    {
        return this.a(((android.support.v4.g.a.a) this.d).addSubMenu(p2, p3, p4, p5));
    }

    public android.view.SubMenu addSubMenu(int p2, int p3, int p4, CharSequence p5)
    {
        return this.a(((android.support.v4.g.a.a) this.d).addSubMenu(p2, p3, p4, p5));
    }

    public android.view.SubMenu addSubMenu(CharSequence p2)
    {
        return this.a(((android.support.v4.g.a.a) this.d).addSubMenu(p2));
    }

    public void clear()
    {
        if (this.b != null) {
            this.b.clear();
        }
        if (this.c != null) {
            this.c.clear();
        }
        ((android.support.v4.g.a.a) this.d).clear();
        return;
    }

    public void close()
    {
        ((android.support.v4.g.a.a) this.d).close();
        return;
    }

    public android.view.MenuItem findItem(int p2)
    {
        return this.a(((android.support.v4.g.a.a) this.d).findItem(p2));
    }

    public android.view.MenuItem getItem(int p2)
    {
        return this.a(((android.support.v4.g.a.a) this.d).getItem(p2));
    }

    public boolean hasVisibleItems()
    {
        return ((android.support.v4.g.a.a) this.d).hasVisibleItems();
    }

    public boolean isShortcutKey(int p2, android.view.KeyEvent p3)
    {
        return ((android.support.v4.g.a.a) this.d).isShortcutKey(p2, p3);
    }

    public boolean performIdentifierAction(int p2, int p3)
    {
        return ((android.support.v4.g.a.a) this.d).performIdentifierAction(p2, p3);
    }

    public boolean performShortcut(int p2, android.view.KeyEvent p3, int p4)
    {
        return ((android.support.v4.g.a.a) this.d).performShortcut(p2, p3, p4);
    }

    public void removeGroup(int p3)
    {
        if (this.b != null) {
            java.util.Iterator v1 = this.b.keySet().iterator();
            while (v1.hasNext()) {
                if (p3 == ((android.view.MenuItem) v1.next()).getGroupId()) {
                    v1.remove();
                }
            }
        }
        ((android.support.v4.g.a.a) this.d).removeGroup(p3);
        return;
    }

    public void removeItem(int p3)
    {
        if (this.b != null) {
            java.util.Iterator v1 = this.b.keySet().iterator();
            while (v1.hasNext()) {
                if (p3 == ((android.view.MenuItem) v1.next()).getItemId()) {
                    v1.remove();
                    break;
                }
            }
        }
        ((android.support.v4.g.a.a) this.d).removeItem(p3);
        return;
    }

    public void setGroupCheckable(int p2, boolean p3, boolean p4)
    {
        ((android.support.v4.g.a.a) this.d).setGroupCheckable(p2, p3, p4);
        return;
    }

    public void setGroupEnabled(int p2, boolean p3)
    {
        ((android.support.v4.g.a.a) this.d).setGroupEnabled(p2, p3);
        return;
    }

    public void setGroupVisible(int p2, boolean p3)
    {
        ((android.support.v4.g.a.a) this.d).setGroupVisible(p2, p3);
        return;
    }

    public void setQwertyMode(boolean p2)
    {
        ((android.support.v4.g.a.a) this.d).setQwertyMode(p2);
        return;
    }

    public int size()
    {
        return ((android.support.v4.g.a.a) this.d).size();
    }
}
