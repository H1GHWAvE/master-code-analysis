package android.support.v7.internal.a;
final class f implements java.lang.Runnable {
    final synthetic android.support.v7.internal.a.e a;

    f(android.support.v7.internal.a.e p1)
    {
        this.a = p1;
        return;
    }

    public final void run()
    {
        android.support.v7.internal.view.menu.i v2_1;
        int v3_0 = this.a;
        android.view.Menu v1 = v3_0.A();
        if (!(v1 instanceof android.support.v7.internal.view.menu.i)) {
            v2_1 = 0;
        } else {
            v2_1 = ((android.support.v7.internal.view.menu.i) v1);
        }
        if (v2_1 != null) {
            v2_1.d();
        }
        try {
            v1.clear();
        } catch (boolean v0_7) {
            if (v2_1 != null) {
                v2_1.e();
            }
            throw v0_7;
        }
        if ((!v3_0.k.onCreatePanelMenu(0, v1)) || (!v3_0.k.onPreparePanel(0, 0, v1))) {
            v1.clear();
        }
        if (v2_1 != null) {
            v2_1.e();
        }
        return;
    }
}
