package android.support.v7.internal.widget;
abstract class a extends android.view.ViewGroup {
    private static final int g = 200;
    protected final android.support.v7.internal.widget.c a;
    protected final android.content.Context b;
    protected android.support.v7.widget.ActionMenuView c;
    protected android.support.v7.widget.ActionMenuPresenter d;
    protected int e;
    protected android.support.v4.view.fk f;
    private boolean h;
    private boolean i;

    a(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    a(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, 0);
        return;
    }

    a(android.content.Context p5, android.util.AttributeSet p6, int p7)
    {
        this(p5, p6, p7);
        this.a = new android.support.v7.internal.widget.c(this);
        int v0_3 = new android.util.TypedValue();
        if ((!p5.getTheme().resolveAttribute(android.support.v7.a.d.actionBarPopupTheme, v0_3, 1)) || (v0_3.resourceId == 0)) {
            this.b = p5;
        } else {
            this.b = new android.view.ContextThemeWrapper(p5, v0_3.resourceId);
        }
        return;
    }

    protected static int a(int p1, int p2, boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = (p1 + p2);
        } else {
            v0 = (p1 - p2);
        }
        return v0;
    }

    protected static int a(android.view.View p2, int p3, int p4)
    {
        p2.measure(android.view.View$MeasureSpec.makeMeasureSpec(p3, -2147483648), p4);
        return Math.max(0, ((p3 - p2.getMeasuredWidth()) + 0));
    }

    protected static int a(android.view.View p4, int p5, int p6, int p7, boolean p8)
    {
        int v0 = p4.getMeasuredWidth();
        int v1_0 = p4.getMeasuredHeight();
        int v2_2 = (((p7 - v1_0) / 2) + p6);
        if (!p8) {
            p4.layout(p5, v2_2, (p5 + v0), (v1_0 + v2_2));
        } else {
            p4.layout((p5 - v0), v2_2, p5, (v1_0 + v2_2));
        }
        if (p8) {
            v0 = (- v0);
        }
        return v0;
    }

    static synthetic void a(android.support.v7.internal.widget.a p1)
    {
        super.setVisibility(0);
        return;
    }

    static synthetic void a(android.support.v7.internal.widget.a p0, int p1)
    {
        super.setVisibility(p1);
        return;
    }

    public android.support.v4.view.fk a(int p3, long p4)
    {
        if (this.f != null) {
            this.f.a();
        }
        android.support.v4.view.fk v0_3;
        if (p3 != 0) {
            v0_3 = android.support.v4.view.cx.p(this).a(0);
            v0_3.a(p4);
            v0_3.a(this.a.a(v0_3, p3));
        } else {
            if (this.getVisibility() != 0) {
                android.support.v4.view.cx.c(this, 0);
            }
            v0_3 = android.support.v4.view.cx.p(this).a(1065353216);
            v0_3.a(p4);
            v0_3.a(this.a.a(v0_3, p3));
        }
        return v0_3;
    }

    public void a(int p3)
    {
        this.a(p3, 200).b();
        return;
    }

    public boolean a()
    {
        int v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.d.e();
        }
        return v0_1;
    }

    public void b()
    {
        this.post(new android.support.v7.internal.widget.b(this));
        return;
    }

    public boolean c()
    {
        int v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.d.f();
        }
        return v0_1;
    }

    public boolean d()
    {
        int v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.d.i();
        }
        return v0_1;
    }

    public boolean e()
    {
        int v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.d.j();
        }
        return v0_1;
    }

    public boolean f()
    {
        if ((this.d == null) || (!this.d.l)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public boolean g()
    {
        if ((!this.f()) || (this.getVisibility() != 0)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public int getAnimatedVisibility()
    {
        int v0_1;
        if (this.f == null) {
            v0_1 = this.getVisibility();
        } else {
            v0_1 = this.a.a;
        }
        return v0_1;
    }

    public int getContentHeight()
    {
        return this.e;
    }

    public void h()
    {
        if (this.d != null) {
            this.d.g();
        }
        return;
    }

    protected void onConfigurationChanged(android.content.res.Configuration p6)
    {
        if (android.os.Build$VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(p6);
        }
        android.support.v7.internal.view.menu.i v0_2 = this.getContext().obtainStyledAttributes(0, android.support.v7.a.n.ActionBar, android.support.v7.a.d.actionBarStyle, 0);
        this.setContentHeight(v0_2.getLayoutDimension(android.support.v7.a.n.ActionBar_height, 0));
        v0_2.recycle();
        if (this.d != null) {
            android.support.v7.internal.view.menu.i v0_4 = this.d;
            if (!v0_4.n) {
                v0_4.m = v0_4.b.getResources().getInteger(android.support.v7.a.j.abc_max_action_buttons);
            }
            if (v0_4.c != null) {
                v0_4.c.c(1);
            }
        }
        return;
    }

    public boolean onHoverEvent(android.view.MotionEvent p6)
    {
        int v0 = android.support.v4.view.bk.a(p6);
        if (v0 == 9) {
            this.i = 0;
        }
        if (!this.i) {
            int v1_1 = super.onHoverEvent(p6);
            if ((v0 == 9) && (v1_1 == 0)) {
                this.i = 1;
            }
        }
        if ((v0 == 10) || (v0 == 3)) {
            this.i = 0;
        }
        return 1;
    }

    public boolean onTouchEvent(android.view.MotionEvent p5)
    {
        int v0 = android.support.v4.view.bk.a(p5);
        if (v0 == 0) {
            this.h = 0;
        }
        if (!this.h) {
            int v1_1 = super.onTouchEvent(p5);
            if ((v0 == 0) && (v1_1 == 0)) {
                this.h = 1;
            }
        }
        if ((v0 == 1) || (v0 == 3)) {
            this.h = 0;
        }
        return 1;
    }

    public void setContentHeight(int p1)
    {
        this.e = p1;
        this.requestLayout();
        return;
    }

    public void setVisibility(int p2)
    {
        if (p2 != this.getVisibility()) {
            if (this.f != null) {
                this.f.a();
            }
            super.setVisibility(p2);
        }
        return;
    }
}
