package android.support.v7.internal.view.menu;
final class h extends android.widget.BaseAdapter {
    final synthetic android.support.v7.internal.view.menu.g a;
    private int b;

    public h(android.support.v7.internal.view.menu.g p2)
    {
        this.a = p2;
        this.b = -1;
        this.a();
        return;
    }

    private void a()
    {
        android.support.v7.internal.view.menu.m v2 = this.a.c.n;
        if (v2 == null) {
            this.b = -1;
        } else {
            java.util.ArrayList v3 = this.a.c.j();
            int v4 = v3.size();
            int v1 = 0;
            while (v1 < v4) {
                if (((android.support.v7.internal.view.menu.m) v3.get(v1)) != v2) {
                    v1++;
                } else {
                    this.b = v1;
                }
            }
        }
        return;
    }

    public final android.support.v7.internal.view.menu.m a(int p4)
    {
        java.util.ArrayList v1 = this.a.c.j();
        int v0_4 = (android.support.v7.internal.view.menu.g.a(this.a) + p4);
        if ((this.b >= 0) && (v0_4 >= this.b)) {
            v0_4++;
        }
        return ((android.support.v7.internal.view.menu.m) v1.get(v0_4));
    }

    public final int getCount()
    {
        int v0_4 = (this.a.c.j().size() - android.support.v7.internal.view.menu.g.a(this.a));
        if (this.b >= 0) {
            v0_4--;
        }
        return v0_4;
    }

    public final synthetic Object getItem(int p2)
    {
        return this.a(p2);
    }

    public final long getItemId(int p3)
    {
        return ((long) p3);
    }

    public final android.view.View getView(int p4, android.view.View p5, android.view.ViewGroup p6)
    {
        android.view.View v1_0;
        if (p5 != null) {
            v1_0 = p5;
        } else {
            v1_0 = this.a.b.inflate(this.a.f, p6, 0);
        }
        ((android.support.v7.internal.view.menu.aa) v1_0).a(this.a(p4));
        return v1_0;
    }

    public final void notifyDataSetChanged()
    {
        this.a();
        super.notifyDataSetChanged();
        return;
    }
}
