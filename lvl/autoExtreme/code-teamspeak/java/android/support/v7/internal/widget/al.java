package android.support.v7.internal.widget;
public final class al extends android.widget.HorizontalScrollView implements android.widget.AdapterView$OnItemSelectedListener {
    private static final String i = "ScrollingTabContainerView";
    private static final android.view.animation.Interpolator m = None;
    private static final int n = 200;
    Runnable a;
    public android.support.v7.widget.aj b;
    public android.widget.Spinner c;
    public boolean d;
    int e;
    int f;
    protected android.support.v4.view.fk g;
    protected final android.support.v7.internal.widget.aq h;
    private android.support.v7.internal.widget.ao j;
    private int k;
    private int l;

    static al()
    {
        android.support.v7.internal.widget.al.m = new android.view.animation.DecelerateInterpolator();
        return;
    }

    public al(android.content.Context p7)
    {
        this(p7);
        this.h = new android.support.v7.internal.widget.aq(this);
        this.setHorizontalScrollBarEnabled(0);
        android.support.v7.widget.aj v0_3 = android.support.v7.internal.view.a.a(p7);
        this.setContentHeight(v0_3.b());
        this.f = v0_3.c();
        android.support.v7.widget.aj v0_6 = new android.support.v7.widget.aj(this.getContext(), 0, android.support.v7.a.d.actionBarTabBarStyle);
        v0_6.setMeasureWithLargestChildEnabled(1);
        v0_6.setGravity(17);
        v0_6.setLayoutParams(new android.support.v7.widget.al(-2, -1));
        this.b = v0_6;
        this.addView(this.b, new android.view.ViewGroup$LayoutParams(-2, -1));
        return;
    }

    static synthetic android.support.v7.internal.widget.ap a(android.support.v7.internal.widget.al p1, android.support.v7.app.g p2)
    {
        return p1.a(p2, 1);
    }

    static synthetic android.support.v7.widget.aj a(android.support.v7.internal.widget.al p1)
    {
        return p1.b;
    }

    private void a(android.support.v7.app.g p4, int p5, boolean p6)
    {
        android.support.v7.internal.widget.ap v1 = this.a(p4, 0);
        this.b.addView(v1, p5, new android.support.v7.widget.al());
        if (this.c != null) {
            ((android.support.v7.internal.widget.an) this.c.getAdapter()).notifyDataSetChanged();
        }
        if (p6) {
            v1.setSelected(1);
        }
        if (this.d) {
            this.requestLayout();
        }
        return;
    }

    private boolean a()
    {
        if ((this.c == null) || (this.c.getParent() != this)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private void b()
    {
        if (!this.a()) {
            if (this.c == null) {
                android.widget.Spinner v0_3 = new android.support.v7.widget.aa(this.getContext(), 0, android.support.v7.a.d.actionDropDownStyle);
                v0_3.setLayoutParams(new android.support.v7.widget.al(-2, -1));
                v0_3.setOnItemSelectedListener(this);
                this.c = v0_3;
            }
            this.removeView(this.b);
            this.addView(this.c, new android.view.ViewGroup$LayoutParams(-2, -1));
            if (this.c.getAdapter() == null) {
                this.c.setAdapter(new android.support.v7.internal.widget.an(this, 0));
            }
            if (this.a != null) {
                this.removeCallbacks(this.a);
                this.a = 0;
            }
            this.c.setSelection(this.l);
        }
        return;
    }

    private void b(android.support.v7.app.g p4, boolean p5)
    {
        android.support.v7.internal.widget.ap v1 = this.a(p4, 0);
        this.b.addView(v1, new android.support.v7.widget.al());
        if (this.c != null) {
            ((android.support.v7.internal.widget.an) this.c.getAdapter()).notifyDataSetChanged();
        }
        if (p5) {
            v1.setSelected(1);
        }
        if (this.d) {
            this.requestLayout();
        }
        return;
    }

    private void c(int p5)
    {
        if (this.g != null) {
            this.g.a();
        }
        if (p5 != 0) {
            android.support.v4.view.fk v0_3 = android.support.v4.view.cx.p(this).a(0);
            v0_3.a(200);
            v0_3.a(android.support.v7.internal.widget.al.m);
            v0_3.a(this.h.a(v0_3, p5));
            v0_3.b();
        } else {
            if (this.getVisibility() != 0) {
                android.support.v4.view.cx.c(this, 0);
            }
            android.support.v4.view.fk v0_6 = android.support.v4.view.cx.p(this).a(1065353216);
            v0_6.a(200);
            v0_6.a(android.support.v7.internal.widget.al.m);
            v0_6.a(this.h.a(v0_6, p5));
            v0_6.b();
        }
        return;
    }

    private boolean c()
    {
        if (this.a()) {
            this.removeView(this.c);
            this.addView(this.b, new android.view.ViewGroup$LayoutParams(-2, -1));
            this.setTabSelected(this.c.getSelectedItemPosition());
        }
        return 0;
    }

    private android.support.v7.widget.aj d()
    {
        android.support.v7.widget.aj v0_1 = new android.support.v7.widget.aj(this.getContext(), 0, android.support.v7.a.d.actionBarTabBarStyle);
        v0_1.setMeasureWithLargestChildEnabled(1);
        v0_1.setGravity(17);
        v0_1.setLayoutParams(new android.support.v7.widget.al(-2, -1));
        return v0_1;
    }

    private void d(int p2)
    {
        this.b.removeViewAt(p2);
        if (this.c != null) {
            ((android.support.v7.internal.widget.an) this.c.getAdapter()).notifyDataSetChanged();
        }
        if (this.d) {
            this.requestLayout();
        }
        return;
    }

    private android.widget.Spinner e()
    {
        android.support.v7.widget.aa v0_1 = new android.support.v7.widget.aa(this.getContext(), 0, android.support.v7.a.d.actionDropDownStyle);
        v0_1.setLayoutParams(new android.support.v7.widget.al(-2, -1));
        v0_1.setOnItemSelectedListener(this);
        return v0_1;
    }

    private void f()
    {
        this.b.removeAllViews();
        if (this.c != null) {
            ((android.support.v7.internal.widget.an) this.c.getAdapter()).notifyDataSetChanged();
        }
        if (this.d) {
            this.requestLayout();
        }
        return;
    }

    public final android.support.v7.internal.widget.ap a(android.support.v7.app.g p5, boolean p6)
    {
        android.support.v7.internal.widget.ap v0_1 = new android.support.v7.internal.widget.ap(this, this.getContext(), p5, p6);
        if (!p6) {
            v0_1.setFocusable(1);
            if (this.j == null) {
                this.j = new android.support.v7.internal.widget.ao(this, 0);
            }
            v0_1.setOnClickListener(this.j);
        } else {
            v0_1.setBackgroundDrawable(0);
            v0_1.setLayoutParams(new android.widget.AbsListView$LayoutParams(-1, this.k));
        }
        return v0_1;
    }

    public final void a(int p3)
    {
        Runnable v0_1 = this.b.getChildAt(p3);
        if (this.a != null) {
            this.removeCallbacks(this.a);
        }
        this.a = new android.support.v7.internal.widget.am(this, v0_1);
        this.post(this.a);
        return;
    }

    public final void b(int p2)
    {
        ((android.support.v7.internal.widget.ap) this.b.getChildAt(p2)).a();
        if (this.c != null) {
            ((android.support.v7.internal.widget.an) this.c.getAdapter()).notifyDataSetChanged();
        }
        if (this.d) {
            this.requestLayout();
        }
        return;
    }

    public final void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if (this.a != null) {
            this.post(this.a);
        }
        return;
    }

    protected final void onConfigurationChanged(android.content.res.Configuration p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(p3);
        }
        int v0_2 = android.support.v7.internal.view.a.a(this.getContext());
        this.setContentHeight(v0_2.b());
        this.f = v0_2.c();
        return;
    }

    public final void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if (this.a != null) {
            this.removeCallbacks(this.a);
        }
        return;
    }

    public final void onItemSelected(android.widget.AdapterView p2, android.view.View p3, int p4, long p5)
    {
        ((android.support.v7.internal.widget.ap) p3).a.f();
        return;
    }

    public final void onMeasure(int p10, int p11)
    {
        int v0_0;
        android.widget.Spinner v1_0 = 1;
        int v3_0 = android.view.View$MeasureSpec.getMode(p10);
        if (v3_0 != 1073741824) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        this.setFillViewport(v0_0);
        android.support.v7.internal.widget.an v4_1 = this.b.getChildCount();
        if ((v4_1 <= 1) || ((v3_0 != 1073741824) && (v3_0 != -2147483648))) {
            this.e = -1;
        } else {
            if (v4_1 <= 2) {
                this.e = (android.view.View$MeasureSpec.getSize(p10) / 2);
            } else {
                this.e = ((int) (((float) android.view.View$MeasureSpec.getSize(p10)) * 1053609165));
            }
            this.e = Math.min(this.e, this.f);
        }
        int v3_11 = android.view.View$MeasureSpec.makeMeasureSpec(this.k, 1073741824);
        if ((v0_0 != 0) || (!this.d)) {
            v1_0 = 0;
        }
        if (v1_0 == null) {
            this.c();
        } else {
            this.b.measure(0, v3_11);
            if (this.b.getMeasuredWidth() <= android.view.View$MeasureSpec.getSize(p10)) {
                this.c();
            } else {
                if (!this.a()) {
                    if (this.c == null) {
                        android.widget.Spinner v1_7 = new android.support.v7.widget.aa(this.getContext(), 0, android.support.v7.a.d.actionDropDownStyle);
                        v1_7.setLayoutParams(new android.support.v7.widget.al(-2, -1));
                        v1_7.setOnItemSelectedListener(this);
                        this.c = v1_7;
                    }
                    this.removeView(this.b);
                    this.addView(this.c, new android.view.ViewGroup$LayoutParams(-2, -1));
                    if (this.c.getAdapter() == null) {
                        this.c.setAdapter(new android.support.v7.internal.widget.an(this, 0));
                    }
                    if (this.a != null) {
                        this.removeCallbacks(this.a);
                        this.a = 0;
                    }
                    this.c.setSelection(this.l);
                }
            }
        }
        android.widget.Spinner v1_17 = this.getMeasuredWidth();
        super.onMeasure(p10, v3_11);
        int v2_2 = this.getMeasuredWidth();
        if ((v0_0 != 0) && (v1_17 != v2_2)) {
            this.setTabSelected(this.l);
        }
        return;
    }

    public final void onNothingSelected(android.widget.AdapterView p1)
    {
        return;
    }

    public final void setAllowCollapse(boolean p1)
    {
        this.d = p1;
        return;
    }

    public final void setContentHeight(int p1)
    {
        this.k = p1;
        this.requestLayout();
        return;
    }

    public final void setTabSelected(int p6)
    {
        this.l = p6;
        int v3 = this.b.getChildCount();
        int v2 = 0;
        while (v2 < v3) {
            int v0_4;
            android.view.View v4 = this.b.getChildAt(v2);
            if (v2 != p6) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            v4.setSelected(v0_4);
            if (v0_4 != 0) {
                this.a(p6);
            }
            v2++;
        }
        if ((this.c != null) && (p6 >= 0)) {
            this.c.setSelection(p6);
        }
        return;
    }
}
