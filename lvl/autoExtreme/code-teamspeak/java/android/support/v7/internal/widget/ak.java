package android.support.v7.internal.widget;
public final class ak {
    public static final int a = 2147483648;
    public int b;
    public int c;
    public int d;
    public int e;
    public int f;
    public int g;
    public boolean h;
    public boolean i;

    public ak()
    {
        this.b = 0;
        this.c = 0;
        this.d = -2147483648;
        this.e = -2147483648;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        return;
    }

    private int a()
    {
        return this.b;
    }

    private void a(boolean p3)
    {
        if (p3 != this.h) {
            this.h = p3;
            if (!this.i) {
                this.b = this.f;
                this.c = this.g;
            } else {
                if (!p3) {
                    int v0_5;
                    if (this.d == -2147483648) {
                        v0_5 = this.f;
                    } else {
                        v0_5 = this.d;
                    }
                    int v0_7;
                    this.b = v0_5;
                    if (this.e == -2147483648) {
                        v0_7 = this.g;
                    } else {
                        v0_7 = this.e;
                    }
                    this.c = v0_7;
                } else {
                    int v0_9;
                    if (this.e == -2147483648) {
                        v0_9 = this.f;
                    } else {
                        v0_9 = this.e;
                    }
                    int v0_11;
                    this.b = v0_9;
                    if (this.d == -2147483648) {
                        v0_11 = this.g;
                    } else {
                        v0_11 = this.d;
                    }
                    this.c = v0_11;
                }
            }
        }
        return;
    }

    private int b()
    {
        return this.c;
    }

    private int c()
    {
        int v0_1;
        if (!this.h) {
            v0_1 = this.b;
        } else {
            v0_1 = this.c;
        }
        return v0_1;
    }

    private int d()
    {
        int v0_1;
        if (!this.h) {
            v0_1 = this.c;
        } else {
            v0_1 = this.b;
        }
        return v0_1;
    }

    public final void a(int p3, int p4)
    {
        this.d = p3;
        this.e = p4;
        this.i = 1;
        if (!this.h) {
            if (p3 != -2147483648) {
                this.b = p3;
            }
            if (p4 != -2147483648) {
                this.c = p4;
            }
        } else {
            if (p4 != -2147483648) {
                this.b = p4;
            }
            if (p3 != -2147483648) {
                this.c = p3;
            }
        }
        return;
    }

    public final void b(int p3, int p4)
    {
        this.i = 0;
        if (p3 != -2147483648) {
            this.f = p3;
            this.b = p3;
        }
        if (p4 != -2147483648) {
            this.g = p4;
            this.c = p4;
        }
        return;
    }
}
