package android.support.v7.internal.a;
public class l extends android.support.v7.app.a implements android.support.v7.internal.widget.j {
    private static final int L = 255;
    private static final long M = 100;
    private static final long N = 200;
    static final synthetic boolean r = False;
    private static final String s = "WindowDecorActionBar";
    private static final android.view.animation.Interpolator t;
    private static final android.view.animation.Interpolator u;
    private static final boolean v;
    private android.support.v7.internal.widget.ActionBarContainer A;
    private android.support.v7.internal.widget.ad B;
    private android.support.v7.internal.widget.ActionBarContextView C;
    private android.view.View D;
    private android.support.v7.internal.widget.al E;
    private java.util.ArrayList F;
    private android.support.v7.internal.a.q G;
    private int H;
    private boolean I;
    private boolean J;
    private java.util.ArrayList K;
    private boolean O;
    private int P;
    private boolean Q;
    private boolean R;
    private boolean S;
    private boolean T;
    private boolean U;
    private android.support.v7.internal.view.i V;
    private boolean W;
    android.content.Context i;
    android.support.v7.internal.a.p j;
    android.support.v7.c.a k;
    android.support.v7.c.b l;
    boolean m;
    android.support.v7.internal.widget.av n;
    final android.support.v4.view.gd o;
    final android.support.v4.view.gd p;
    final android.support.v4.view.gf q;
    private android.content.Context w;
    private android.app.Activity x;
    private android.app.Dialog y;
    private android.support.v7.internal.widget.ActionBarOverlayLayout z;

    static l()
    {
        int v0_2;
        int v1 = 1;
        if (android.support.v7.internal.a.l.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        android.support.v7.internal.a.l.r = v0_2;
        android.support.v7.internal.a.l.t = new android.view.animation.AccelerateInterpolator();
        android.support.v7.internal.a.l.u = new android.view.animation.DecelerateInterpolator();
        if (android.os.Build$VERSION.SDK_INT < 14) {
            v1 = 0;
        }
        android.support.v7.internal.a.l.v = v1;
        return;
    }

    public l(android.app.Activity p3, boolean p4)
    {
        this.F = new java.util.ArrayList();
        this.H = -1;
        this.K = new java.util.ArrayList();
        this.P = 0;
        this.Q = 1;
        this.U = 1;
        this.o = new android.support.v7.internal.a.m(this);
        this.p = new android.support.v7.internal.a.n(this);
        this.q = new android.support.v7.internal.a.o(this);
        this.x = p3;
        android.view.View v0_13 = p3.getWindow().getDecorView();
        this.b(v0_13);
        if (!p4) {
            this.D = v0_13.findViewById(16908290);
        }
        return;
    }

    public l(android.app.Dialog p3)
    {
        this.F = new java.util.ArrayList();
        this.H = -1;
        this.K = new java.util.ArrayList();
        this.P = 0;
        this.Q = 1;
        this.U = 1;
        this.o = new android.support.v7.internal.a.m(this);
        this.p = new android.support.v7.internal.a.n(this);
        this.q = new android.support.v7.internal.a.o(this);
        this.y = p3;
        this.b(p3.getWindow().getDecorView());
        return;
    }

    private l(android.view.View p3)
    {
        this.F = new java.util.ArrayList();
        this.H = -1;
        this.K = new java.util.ArrayList();
        this.P = 0;
        this.Q = 1;
        this.U = 1;
        this.o = new android.support.v7.internal.a.m(this);
        this.p = new android.support.v7.internal.a.n(this);
        this.q = new android.support.v7.internal.a.o(this);
        if ((android.support.v7.internal.a.l.r) || (p3.isInEditMode())) {
            this.b(p3);
            return;
        } else {
            throw new AssertionError();
        }
    }

    private void E()
    {
        if (this.E == null) {
            android.support.v7.internal.widget.al v0_2 = new android.support.v7.internal.widget.al(this.i);
            if (!this.O) {
                if (this.g() != 2) {
                    v0_2.setVisibility(8);
                } else {
                    v0_2.setVisibility(0);
                    if (this.z != null) {
                        android.support.v4.view.cx.t(this.z);
                    }
                }
                this.A.setTabContainer(v0_2);
            } else {
                v0_2.setVisibility(0);
                this.B.a(v0_2);
            }
            this.E = v0_2;
        }
        return;
    }

    private void F()
    {
        if (this.l != null) {
            this.l.a(this.k);
            this.k = 0;
            this.l = 0;
        }
        return;
    }

    private void G()
    {
        if (this.G != null) {
            this.c(0);
        }
        this.F.clear();
        if (this.E != null) {
            android.support.v7.internal.widget.al v1 = this.E;
            v1.b.removeAllViews();
            if (v1.c != null) {
                ((android.support.v7.internal.widget.an) v1.c.getAdapter()).notifyDataSetChanged();
            }
            if (v1.d) {
                v1.requestLayout();
            }
        }
        this.H = -1;
        return;
    }

    private void H()
    {
        if (!this.T) {
            this.T = 1;
            if (this.z != null) {
                this.z.setShowingForActionMode(1);
            }
            this.l(0);
        }
        return;
    }

    private void I()
    {
        if (this.T) {
            this.T = 0;
            if (this.z != null) {
                this.z.setShowingForActionMode(0);
            }
            this.l(0);
        }
        return;
    }

    private boolean J()
    {
        return this.B.i();
    }

    private boolean K()
    {
        return this.B.j();
    }

    private android.support.v7.internal.widget.av L()
    {
        if (this.n == null) {
            this.n = android.support.v7.internal.widget.av.a(this.i);
        }
        return this.n;
    }

    static synthetic boolean a(android.support.v7.internal.a.l p1)
    {
        return p1.Q;
    }

    static synthetic boolean a(boolean p1, boolean p2)
    {
        return android.support.v7.internal.a.l.a(p1, p2, 0);
    }

    private static boolean a(boolean p1, boolean p2, boolean p3)
    {
        int v0 = 1;
        if ((!p3) && ((p1) || (p2))) {
            v0 = 0;
        }
        return v0;
    }

    static synthetic android.view.View b(android.support.v7.internal.a.l p1)
    {
        return p1.D;
    }

    private void b(android.support.v7.app.g p4, int p5)
    {
        if (((android.support.v7.internal.a.q) p4).b != null) {
            ((android.support.v7.internal.a.q) p4).c = p5;
            this.F.add(p5, ((android.support.v7.internal.a.q) p4));
            int v2 = this.F.size();
            int v1_0 = (p5 + 1);
            while (v1_0 < v2) {
                ((android.support.v7.internal.a.q) this.F.get(v1_0)).c = v1_0;
                v1_0++;
            }
            return;
        } else {
            throw new IllegalStateException("Action Bar Tab must have a Callback");
        }
    }

    private void b(android.view.View p7)
    {
        this.z = ((android.support.v7.internal.widget.ActionBarOverlayLayout) p7.findViewById(android.support.v7.a.i.decor_content_parent));
        if (this.z != null) {
            this.z.setActionBarVisibilityCallback(this);
        }
        IllegalStateException v0_10;
        IllegalStateException v0_6 = p7.findViewById(android.support.v7.a.i.action_bar);
        if (!(v0_6 instanceof android.support.v7.internal.widget.ad)) {
            if (!(v0_6 instanceof android.support.v7.widget.Toolbar)) {
                IllegalStateException v0_7;
                if (new StringBuilder("Can\'t make a decor toolbar out of ").append(v0_6).toString() == null) {
                    v0_7 = "null";
                } else {
                    v0_7 = v0_6.getClass().getSimpleName();
                }
                throw new IllegalStateException(v0_7);
            } else {
                v0_10 = ((android.support.v7.widget.Toolbar) v0_6).getWrapper();
            }
        } else {
            v0_10 = ((android.support.v7.internal.widget.ad) v0_6);
        }
        this.B = v0_10;
        this.C = ((android.support.v7.internal.widget.ActionBarContextView) p7.findViewById(android.support.v7.a.i.action_context_bar));
        this.A = ((android.support.v7.internal.widget.ActionBarContainer) p7.findViewById(android.support.v7.a.i.action_bar_container));
        if ((this.B != null) && ((this.C != null) && (this.A != null))) {
            IllegalStateException v0_25;
            this.i = this.B.b();
            if ((this.B.r() & 4) == 0) {
                v0_25 = 0;
            } else {
                v0_25 = 1;
            }
            if (v0_25 != null) {
                this.I = 1;
            }
            IllegalStateException v0_27 = android.support.v7.internal.view.a.a(this.i);
            v0_27.a.getApplicationInfo();
            this.k(v0_27.a());
            IllegalStateException v0_30 = this.i.obtainStyledAttributes(0, android.support.v7.a.n.ActionBar, android.support.v7.a.d.actionBarStyle, 0);
            if (v0_30.getBoolean(android.support.v7.a.n.ActionBar_hideOnContentScroll, 0)) {
                if (this.z.a) {
                    this.m = 1;
                    this.z.setHideOnContentScrollEnabled(1);
                } else {
                    throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
                }
            }
            float v1_4 = v0_30.getDimensionPixelSize(android.support.v7.a.n.ActionBar_elevation, 0);
            if (v1_4 != 0) {
                android.support.v4.view.cx.f(this.A, ((float) v1_4));
            }
            v0_30.recycle();
            return;
        } else {
            throw new IllegalStateException(new StringBuilder().append(this.getClass().getSimpleName()).append(" can only be used with a compatible window decor layout").toString());
        }
    }

    static synthetic android.support.v7.internal.widget.ActionBarContainer c(android.support.v7.internal.a.l p1)
    {
        return p1.A;
    }

    private static android.support.v7.internal.widget.ad c(android.view.View p3)
    {
        android.support.v7.internal.widget.ad v3_2;
        if (!(p3 instanceof android.support.v7.internal.widget.ad)) {
            if (!(p3 instanceof android.support.v7.widget.Toolbar)) {
                String v0_6;
                if (new StringBuilder("Can\'t make a decor toolbar out of ").append(p3).toString() == null) {
                    v0_6 = "null";
                } else {
                    v0_6 = p3.getClass().getSimpleName();
                }
                throw new IllegalStateException(v0_6);
            } else {
                v3_2 = ((android.support.v7.widget.Toolbar) p3).getWrapper();
            }
        } else {
            v3_2 = ((android.support.v7.internal.widget.ad) p3);
        }
        return v3_2;
    }

    static synthetic android.support.v7.internal.view.i d(android.support.v7.internal.a.l p1)
    {
        p1.V = 0;
        return 0;
    }

    static synthetic android.support.v7.internal.widget.ActionBarOverlayLayout e(android.support.v7.internal.a.l p1)
    {
        return p1.z;
    }

    static synthetic boolean f(android.support.v7.internal.a.l p1)
    {
        return p1.R;
    }

    static synthetic boolean g(android.support.v7.internal.a.l p1)
    {
        return p1.S;
    }

    static synthetic android.support.v7.internal.widget.ActionBarContextView h(android.support.v7.internal.a.l p1)
    {
        return p1.C;
    }

    static synthetic android.support.v7.internal.widget.ad i(android.support.v7.internal.a.l p1)
    {
        return p1.B;
    }

    static synthetic android.content.Context j(android.support.v7.internal.a.l p1)
    {
        return p1.i;
    }

    static synthetic android.support.v7.internal.widget.al k(android.support.v7.internal.a.l p1)
    {
        return p1.E;
    }

    private void k(boolean p6)
    {
        int v1 = 1;
        this.O = p6;
        if (this.O) {
            this.A.setTabContainer(0);
            this.B.a(this.E);
        } else {
            this.B.a(0);
            this.A.setTabContainer(this.E);
        }
        int v0_6;
        if (this.g() != 2) {
            v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        if (this.E != null) {
            if (v0_6 == 0) {
                this.E.setVisibility(8);
            } else {
                this.E.setVisibility(0);
                if (this.z != null) {
                    android.support.v4.view.cx.t(this.z);
                }
            }
        }
        if ((this.O) || (v0_6 == 0)) {
            android.support.v7.internal.widget.ActionBarOverlayLayout v3_10 = 0;
        } else {
            v3_10 = 1;
        }
        this.B.a(v3_10);
        if ((this.O) || (v0_6 == 0)) {
            v1 = 0;
        }
        this.z.setHasNonEmbeddedTabs(v1);
        return;
    }

    private void l(boolean p9)
    {
        if (!android.support.v7.internal.a.l.a(this.R, this.S, this.T)) {
            if (this.U) {
                this.U = 0;
                if (this.V != null) {
                    this.V.b();
                }
                if ((this.P != 0) || ((!android.support.v7.internal.a.l.v) || ((!this.W) && (!p9)))) {
                    this.o.b(0);
                } else {
                    android.support.v4.view.cx.c(this.A, 1065353216);
                    this.A.setTransitioning(1);
                    android.support.v7.internal.view.i v1_3 = new android.support.v7.internal.view.i();
                    android.support.v4.view.fk v0_14 = ((float) (- this.A.getHeight()));
                    if (p9) {
                        android.support.v4.view.fk v2_1 = new int[2];
                        v2_1 = {0, 0};
                        this.A.getLocationInWindow(v2_1);
                        v0_14 -= ((float) v2_1[1]);
                    }
                    android.support.v4.view.fk v2_6 = android.support.v4.view.cx.p(this.A).b(v0_14);
                    v2_6.a(this.q);
                    v1_3.a(v2_6);
                    if ((this.Q) && (this.D != null)) {
                        v1_3.a(android.support.v4.view.cx.p(this.D).b(v0_14));
                    }
                    v1_3.a(android.support.v7.internal.a.l.t);
                    v1_3.c();
                    v1_3.a(this.o);
                    this.V = v1_3;
                    v1_3.a();
                }
            }
        } else {
            if (!this.U) {
                this.U = 1;
                if (this.V != null) {
                    this.V.b();
                }
                this.A.setVisibility(0);
                if ((this.P != 0) || ((!android.support.v7.internal.a.l.v) || ((!this.W) && (!p9)))) {
                    android.support.v4.view.cx.c(this.A, 1065353216);
                    android.support.v4.view.cx.b(this.A, 0);
                    if ((this.Q) && (this.D != null)) {
                        android.support.v4.view.cx.b(this.D, 0);
                    }
                    this.p.b(0);
                } else {
                    android.support.v4.view.cx.b(this.A, 0);
                    android.support.v4.view.fk v0_35 = ((float) (- this.A.getHeight()));
                    if (p9) {
                        android.support.v7.internal.view.i v1_5 = new int[2];
                        v1_5 = {0, 0};
                        this.A.getLocationInWindow(v1_5);
                        v0_35 -= ((float) v1_5[1]);
                    }
                    android.support.v4.view.cx.b(this.A, v0_35);
                    android.support.v7.internal.view.i v1_10 = new android.support.v7.internal.view.i();
                    android.support.v4.view.fk v2_14 = android.support.v4.view.cx.p(this.A).b(0);
                    v2_14.a(this.q);
                    v1_10.a(v2_14);
                    if ((this.Q) && (this.D != null)) {
                        android.support.v4.view.cx.b(this.D, v0_35);
                        v1_10.a(android.support.v4.view.cx.p(this.D).b(0));
                    }
                    v1_10.a(android.support.v7.internal.a.l.u);
                    v1_10.c();
                    v1_10.a(this.p);
                    this.V = v1_10;
                    v1_10.a();
                }
                if (this.z != null) {
                    android.support.v4.view.cx.t(this.z);
                }
            }
        }
        return;
    }

    private void m(boolean p6)
    {
        if (this.V != null) {
            this.V.b();
        }
        this.A.setVisibility(0);
        if ((this.P != 0) || ((!android.support.v7.internal.a.l.v) || ((!this.W) && (!p6)))) {
            android.support.v4.view.cx.c(this.A, 1065353216);
            android.support.v4.view.cx.b(this.A, 0);
            if ((this.Q) && (this.D != null)) {
                android.support.v4.view.cx.b(this.D, 0);
            }
            this.p.b(0);
        } else {
            android.support.v4.view.cx.b(this.A, 0);
            android.support.v4.view.fk v0_16 = ((float) (- this.A.getHeight()));
            if (p6) {
                android.support.v7.internal.view.i v1_4 = new int[2];
                v1_4 = {0, 0};
                this.A.getLocationInWindow(v1_4);
                v0_16 -= ((float) v1_4[1]);
            }
            android.support.v4.view.cx.b(this.A, v0_16);
            android.support.v7.internal.view.i v1_9 = new android.support.v7.internal.view.i();
            android.view.View v2_4 = android.support.v4.view.cx.p(this.A).b(0);
            v2_4.a(this.q);
            v1_9.a(v2_4);
            if ((this.Q) && (this.D != null)) {
                android.support.v4.view.cx.b(this.D, v0_16);
                v1_9.a(android.support.v4.view.cx.p(this.D).b(0));
            }
            v1_9.a(android.support.v7.internal.a.l.u);
            v1_9.c();
            v1_9.a(this.p);
            this.V = v1_9;
            v1_9.a();
        }
        if (this.z != null) {
            android.support.v4.view.cx.t(this.z);
        }
        return;
    }

    private void n(boolean p6)
    {
        if (this.V != null) {
            this.V.b();
        }
        if ((this.P != 0) || ((!android.support.v7.internal.a.l.v) || ((!this.W) && (!p6)))) {
            this.o.b(0);
        } else {
            android.support.v4.view.cx.c(this.A, 1065353216);
            this.A.setTransitioning(1);
            android.support.v7.internal.view.i v1_3 = new android.support.v7.internal.view.i();
            android.support.v4.view.fk v0_11 = ((float) (- this.A.getHeight()));
            if (p6) {
                android.support.v4.view.fk v2_1 = new int[2];
                v2_1 = {0, 0};
                this.A.getLocationInWindow(v2_1);
                v0_11 -= ((float) v2_1[1]);
            }
            android.support.v4.view.fk v2_6 = android.support.v4.view.cx.p(this.A).b(v0_11);
            v2_6.a(this.q);
            v1_3.a(v2_6);
            if ((this.Q) && (this.D != null)) {
                v1_3.a(android.support.v4.view.cx.p(this.D).b(v0_11));
            }
            v1_3.a(android.support.v7.internal.a.l.t);
            v1_3.c();
            v1_3.a(this.o);
            this.V = v1_3;
            v1_3.a();
        }
        return;
    }

    public final void A()
    {
        if (this.S) {
            this.S = 0;
            this.l(1);
        }
        return;
    }

    public final void B()
    {
        if (!this.S) {
            this.S = 1;
            this.l(1);
        }
        return;
    }

    public final void C()
    {
        if (this.V != null) {
            this.V.b();
            this.V = 0;
        }
        return;
    }

    public final void D()
    {
        return;
    }

    public final int a()
    {
        int v0_0 = -1;
        switch (this.B.v()) {
            case 1:
                v0_0 = this.B.w();
                break;
            case 2:
                if (this.G == null) {
                } else {
                    v0_0 = this.G.c;
                }
                break;
        }
        return v0_0;
    }

    public final android.support.v7.c.a a(android.support.v7.c.b p4)
    {
        if (this.j != null) {
            this.j.c();
        }
        this.z.setHideOnContentScrollEnabled(0);
        this.C.i();
        int v0_5 = new android.support.v7.internal.a.p(this, this.C.getContext(), p4);
        if (!v0_5.e()) {
            v0_5 = 0;
        } else {
            v0_5.d();
            this.C.a(v0_5);
            this.j(1);
            this.C.sendAccessibilityEvent(32);
            this.j = v0_5;
        }
        return v0_5;
    }

    public final void a(float p2)
    {
        android.support.v4.view.cx.f(this.A, p2);
        return;
    }

    public final void a(int p4)
    {
        this.a(android.view.LayoutInflater.from(this.r()).inflate(p4, this.B.a(), 0));
        return;
    }

    public final void a(int p5, int p6)
    {
        int v0_1 = this.B.r();
        if ((p6 & 4) != 0) {
            this.I = 1;
        }
        this.B.c(((v0_1 & (p6 ^ -1)) | (p5 & p6)));
        return;
    }

    public final void a(android.content.res.Configuration p2)
    {
        this.k(android.support.v7.internal.view.a.a(this.i).a());
        return;
    }

    public final void a(android.graphics.drawable.Drawable p2)
    {
        this.B.a(p2);
        return;
    }

    public final void a(android.support.v7.app.e p2)
    {
        this.K.add(p2);
        return;
    }

    public final void a(android.support.v7.app.g p6)
    {
        boolean v1 = this.F.isEmpty();
        this.E();
        android.support.v7.internal.widget.al v2 = this.E;
        android.support.v7.internal.widget.ap v3 = v2.a(p6, 0);
        v2.b.addView(v3, new android.support.v7.widget.al());
        if (v2.c != null) {
            ((android.support.v7.internal.widget.an) v2.c.getAdapter()).notifyDataSetChanged();
        }
        if (v1) {
            v3.setSelected(1);
        }
        if (v2.d) {
            v2.requestLayout();
        }
        this.b(p6, this.F.size());
        if (v1) {
            this.c(p6);
        }
        return;
    }

    public final void a(android.support.v7.app.g p6, int p7)
    {
        boolean v1 = this.F.isEmpty();
        this.E();
        android.support.v7.internal.widget.al v2 = this.E;
        android.support.v7.internal.widget.ap v3 = v2.a(p6, 0);
        v2.b.addView(v3, p7, new android.support.v7.widget.al());
        if (v2.c != null) {
            ((android.support.v7.internal.widget.an) v2.c.getAdapter()).notifyDataSetChanged();
        }
        if (v1) {
            v3.setSelected(1);
        }
        if (v2.d) {
            v2.requestLayout();
        }
        this.b(p6, p7);
        if (v1) {
            this.c(p6);
        }
        return;
    }

    public final void a(android.support.v7.app.g p5, int p6, boolean p7)
    {
        this.E();
        android.support.v7.internal.widget.al v1 = this.E;
        android.support.v7.internal.widget.ap v2 = v1.a(p5, 0);
        v1.b.addView(v2, p6, new android.support.v7.widget.al());
        if (v1.c != null) {
            ((android.support.v7.internal.widget.an) v1.c.getAdapter()).notifyDataSetChanged();
        }
        if (p7) {
            v2.setSelected(1);
        }
        if (v1.d) {
            v1.requestLayout();
        }
        this.b(p5, p6);
        if (p7) {
            this.c(p5);
        }
        return;
    }

    public final void a(android.support.v7.app.g p5, boolean p6)
    {
        this.E();
        android.support.v7.internal.widget.al v1 = this.E;
        android.support.v7.internal.widget.ap v2 = v1.a(p5, 0);
        v1.b.addView(v2, new android.support.v7.widget.al());
        if (v1.c != null) {
            ((android.support.v7.internal.widget.an) v1.c.getAdapter()).notifyDataSetChanged();
        }
        if (p6) {
            v2.setSelected(1);
        }
        if (v1.d) {
            v1.requestLayout();
        }
        this.b(p5, this.F.size());
        if (p6) {
            this.c(p5);
        }
        return;
    }

    public final void a(android.view.View p2)
    {
        this.B.a(p2);
        return;
    }

    public final void a(android.view.View p2, android.support.v7.app.c p3)
    {
        p2.setLayoutParams(p3);
        this.B.a(p2);
        return;
    }

    public final void a(android.widget.SpinnerAdapter p3, android.support.v7.app.f p4)
    {
        this.B.a(p3, new android.support.v7.internal.a.b(p4));
        return;
    }

    public final void a(CharSequence p2)
    {
        this.B.b(p2);
        return;
    }

    public final void a(boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = 0;
        } else {
            v0 = 1;
        }
        this.a(v0, 1);
        return;
    }

    public final int b()
    {
        int v0_3;
        switch (this.B.v()) {
            case 1:
                v0_3 = this.B.x();
                break;
            case 2:
                v0_3 = this.F.size();
                break;
            default:
                v0_3 = 0;
        }
        return v0_3;
    }

    public final void b(int p2)
    {
        this.B.a(p2);
        return;
    }

    public final void b(android.graphics.drawable.Drawable p2)
    {
        this.B.b(p2);
        return;
    }

    public final void b(android.support.v7.app.e p2)
    {
        this.K.remove(p2);
        return;
    }

    public final void b(android.support.v7.app.g p6)
    {
        int v3 = p6.a();
        if (this.E != null) {
            int v1_0;
            if (this.G == null) {
                v1_0 = this.H;
            } else {
                v1_0 = this.G.c;
            }
            int v2_0 = this.E;
            v2_0.b.removeViewAt(v3);
            if (v2_0.c != null) {
                ((android.support.v7.internal.widget.an) v2_0.c.getAdapter()).notifyDataSetChanged();
            }
            if (v2_0.d) {
                v2_0.requestLayout();
            }
            android.support.v7.internal.a.q v0_13 = ((android.support.v7.internal.a.q) this.F.remove(v3));
            if (v0_13 != null) {
                v0_13.c = -1;
            }
            int v4 = this.F.size();
            int v2_2 = v3;
            while (v2_2 < v4) {
                ((android.support.v7.internal.a.q) this.F.get(v2_2)).c = v2_2;
                v2_2++;
            }
            if (v1_0 == v3) {
                android.support.v7.internal.a.q v0_19;
                if (!this.F.isEmpty()) {
                    v0_19 = ((android.support.v7.internal.a.q) this.F.get(Math.max(0, (v3 - 1))));
                } else {
                    v0_19 = 0;
                }
                this.c(v0_19);
            }
        }
        return;
    }

    public final void b(CharSequence p2)
    {
        this.B.c(p2);
        return;
    }

    public final void b(boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = 0;
        } else {
            v0 = 2;
        }
        this.a(v0, 2);
        return;
    }

    public final void c()
    {
        return;
    }

    public final void c(int p2)
    {
        this.B.b(p2);
        return;
    }

    public final void c(android.graphics.drawable.Drawable p2)
    {
        this.A.setPrimaryBackground(p2);
        return;
    }

    public final void c(android.support.v7.app.g p4)
    {
        boolean v1_0 = -1;
        if (this.g() == 2) {
            if ((!(this.x instanceof android.support.v4.app.bb)) || (this.B.a().isInEditMode())) {
                android.support.v4.app.cd v0_6 = 0;
            } else {
                v0_6 = ((android.support.v4.app.bb) this.x).c().a().h();
            }
            if (this.G != p4) {
                int v2_2 = this.E;
                if (p4 != null) {
                    v1_0 = p4.a();
                }
                v2_2.setTabSelected(v1_0);
                this.G = ((android.support.v7.internal.a.q) p4);
            } else {
                if (this.G != null) {
                    this.E.a(p4.a());
                }
            }
            if ((v0_6 != null) && (!v0_6.l())) {
                v0_6.i();
            }
        } else {
            android.support.v4.app.cd v0_11;
            if (p4 == null) {
                v0_11 = -1;
            } else {
                v0_11 = p4.a();
            }
            this.H = v0_11;
        }
        return;
    }

    public final void c(CharSequence p2)
    {
        this.B.d(p2);
        return;
    }

    public final void c(boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = 0;
        } else {
            v0 = 4;
        }
        this.a(v0, 4);
        return;
    }

    public final android.view.View d()
    {
        return this.B.y();
    }

    public final void d(int p3)
    {
        switch (this.B.v()) {
            case 1:
                this.B.e(p3);
                break;
            case 2:
                this.c(((android.support.v7.app.g) this.F.get(p3)));
                break;
            default:
                throw new IllegalStateException("setSelectedNavigationIndex not valid for current navigation mode");
        }
        return;
    }

    public final void d(android.graphics.drawable.Drawable p2)
    {
        this.A.setStackedBackground(p2);
        return;
    }

    public final void d(CharSequence p2)
    {
        this.B.a(p2);
        return;
    }

    public final void d(boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = 0;
        } else {
            v0 = 8;
        }
        this.a(v0, 8);
        return;
    }

    public final CharSequence e()
    {
        return this.B.e();
    }

    public final void e(int p2)
    {
        this.a(this.i.getString(p2));
        return;
    }

    public final void e(android.graphics.drawable.Drawable p2)
    {
        this.B.c(p2);
        return;
    }

    public final void e(boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = 0;
        } else {
            v0 = 16;
        }
        this.a(v0, 16);
        return;
    }

    public final CharSequence f()
    {
        return this.B.f();
    }

    public final void f(int p2)
    {
        this.b(this.i.getString(p2));
        return;
    }

    public final void f(boolean p3)
    {
        if (!this.I) {
            int v0_1;
            if (!p3) {
                v0_1 = 0;
            } else {
                v0_1 = 4;
            }
            this.a(v0_1, 4);
        }
        return;
    }

    public final int g()
    {
        return this.B.v();
    }

    public final void g(int p2)
    {
        if ((p2 & 4) != 0) {
            this.I = 1;
        }
        this.B.c(p2);
        return;
    }

    public final void g(boolean p2)
    {
        this.W = p2;
        if ((!p2) && (this.V != null)) {
            this.V.b();
        }
        return;
    }

    public final int h()
    {
        return this.B.r();
    }

    public final void h(int p8)
    {
        int v2 = 1;
        java.util.ArrayList v4_0 = this.B.v();
        switch (v4_0) {
            case 2:
                android.support.v7.app.g v0_4;
                switch (this.B.v()) {
                    case 1:
                        v0_4 = this.B.w();
                        break;
                    case 2:
                        if (this.G == null) {
                            v0_4 = -1;
                        } else {
                            v0_4 = this.G.c;
                        }
                        break;
                    default:
                        v0_4 = -1;
                }
                this.H = v0_4;
                this.c(0);
                this.E.setVisibility(8);
                break;
            default:
                if (v4_0 == p8) {
                    this.B.d(p8);
                    switch (p8) {
                        case 2:
                            this.E();
                            this.E.setVisibility(0);
                            if (this.H == -1) {
                            } else {
                                android.support.v7.app.g v0_15 = this.H;
                                switch (this.B.v()) {
                                    case 1:
                                        this.B.e(v0_15);
                                        break;
                                    case 2:
                                        this.c(((android.support.v7.app.g) this.F.get(v0_15)));
                                        break;
                                    default:
                                        throw new IllegalStateException("setSelectedNavigationIndex not valid for current navigation mode");
                                }
                                this.H = -1;
                            }
                            break;
                        default:
                            android.support.v7.app.g v0_21;
                            if (p8 != 2) {
                                v0_21 = 0;
                                this.B.a(v0_21);
                                if ((p8 != 2) || (this.O)) {
                                    v2 = 0;
                                }
                                this.z.setHasNonEmbeddedTabs(v2);
                                return;
                            } else {
                                if (this.O) {
                                } else {
                                    v0_21 = 1;
                                }
                            }
                    }
                    if ((p8 != 2) || (this.O)) {
                    }
                } else {
                    if ((this.O) || (this.z == null)) {
                    } else {
                        android.support.v4.view.cx.t(this.z);
                    }
                }
        }
        if ((v4_0 != p8) && ((!this.O) && (this.z != null))) {
        }
    }

    public final void h(boolean p4)
    {
        if (p4 != this.J) {
            this.J = p4;
            int v1 = this.K.size();
            int v0_2 = 0;
            while (v0_2 < v1) {
                this.K.get(v0_2);
                v0_2++;
            }
        }
        return;
    }

    public final android.support.v7.app.g i()
    {
        return new android.support.v7.internal.a.q(this);
    }

    public final void i(int p5)
    {
        if (this.E != null) {
            int v1_0;
            if (this.G == null) {
                v1_0 = this.H;
            } else {
                v1_0 = this.G.c;
            }
            int v2_0 = this.E;
            v2_0.b.removeViewAt(p5);
            if (v2_0.c != null) {
                ((android.support.v7.internal.widget.an) v2_0.c.getAdapter()).notifyDataSetChanged();
            }
            if (v2_0.d) {
                v2_0.requestLayout();
            }
            android.support.v7.internal.a.q v0_13 = ((android.support.v7.internal.a.q) this.F.remove(p5));
            if (v0_13 != null) {
                v0_13.c = -1;
            }
            int v3 = this.F.size();
            int v2_2 = p5;
            while (v2_2 < v3) {
                ((android.support.v7.internal.a.q) this.F.get(v2_2)).c = v2_2;
                v2_2++;
            }
            if (v1_0 == p5) {
                android.support.v7.internal.a.q v0_19;
                if (!this.F.isEmpty()) {
                    v0_19 = ((android.support.v7.internal.a.q) this.F.get(Math.max(0, (p5 - 1))));
                } else {
                    v0_19 = 0;
                }
                this.c(v0_19);
            }
        }
        return;
    }

    public final void i(boolean p1)
    {
        this.Q = p1;
        return;
    }

    public final android.support.v7.app.g j(int p2)
    {
        return ((android.support.v7.app.g) this.F.get(p2));
    }

    public final void j()
    {
        if (this.G != null) {
            this.c(0);
        }
        this.F.clear();
        if (this.E != null) {
            android.support.v7.internal.widget.al v1 = this.E;
            v1.b.removeAllViews();
            if (v1.c != null) {
                ((android.support.v7.internal.widget.an) v1.c.getAdapter()).notifyDataSetChanged();
            }
            if (v1.d) {
                v1.requestLayout();
            }
        }
        this.H = -1;
        return;
    }

    public final void j(boolean p9)
    {
        if (!p9) {
            if (this.T) {
                this.T = 0;
                if (this.z != null) {
                    this.z.setShowingForActionMode(0);
                }
                this.l(0);
            }
        } else {
            if (!this.T) {
                this.T = 1;
                if (this.z != null) {
                    this.z.setShowingForActionMode(1);
                }
                this.l(0);
            }
        }
        android.support.v4.view.fk v1_1;
        java.util.ArrayList v0_8;
        if (!p9) {
            v1_1 = this.B.a(0, 200);
            v0_8 = this.C.a(8, 100);
        } else {
            v0_8 = this.B.a(8, 100);
            v1_1 = this.C.a(0, 200);
        }
        long v2_2;
        android.support.v7.internal.view.i v4_2 = new android.support.v7.internal.view.i();
        v4_2.a.add(v0_8);
        java.util.ArrayList v0_12 = ((android.view.View) v0_8.a.get());
        if (v0_12 == null) {
            v2_2 = 0;
        } else {
            v2_2 = android.support.v4.view.fk.c.a(v0_12);
        }
        v1_1.b(v2_2);
        v4_2.a.add(v1_1);
        v4_2.a();
        return;
    }

    public final android.support.v7.app.g k()
    {
        return this.G;
    }

    public final void k(int p2)
    {
        this.B.g(p2);
        return;
    }

    public final int l()
    {
        return this.F.size();
    }

    public final void l(int p2)
    {
        this.B.h(p2);
        return;
    }

    public final int m()
    {
        return this.A.getHeight();
    }

    public final void m(int p3)
    {
        if ((p3 == 0) || (this.z.a)) {
            this.z.setActionBarHideOffset(p3);
            return;
        } else {
            throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to set a non-zero hide offset");
        }
    }

    public final void n()
    {
        if (this.R) {
            this.R = 0;
            this.l(0);
        }
        return;
    }

    public final void n(int p1)
    {
        this.P = p1;
        return;
    }

    public final void o()
    {
        if (!this.R) {
            this.R = 1;
            this.l(0);
        }
        return;
    }

    public final boolean p()
    {
        int v0_2;
        int v0_1 = this.A.getHeight();
        if ((!this.U) || ((v0_1 != 0) && (this.z.getActionBarHideOffset() >= v0_1))) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final void q()
    {
        return;
    }

    public final android.content.Context r()
    {
        if (this.w == null) {
            android.content.Context v0_2 = new android.util.TypedValue();
            this.i.getTheme().resolveAttribute(android.support.v7.a.d.actionBarWidgetTheme, v0_2, 1);
            android.content.Context v0_3 = v0_2.resourceId;
            if (v0_3 == null) {
                this.w = this.i;
            } else {
                this.w = new android.view.ContextThemeWrapper(this.i, v0_3);
            }
        }
        return this.w;
    }

    public final boolean s()
    {
        if ((this.B == null) || (!this.B.t())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final void t()
    {
        if (this.z.a) {
            this.m = 1;
            this.z.setHideOnContentScrollEnabled(1);
            return;
        } else {
            throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
        }
    }

    public final boolean u()
    {
        return this.z.b;
    }

    public final int v()
    {
        return this.z.getActionBarHideOffset();
    }

    public final float w()
    {
        return android.support.v4.view.cx.r(this.A);
    }

    public final boolean z()
    {
        if ((this.B == null) || (!this.B.c())) {
            int v0_3 = 0;
        } else {
            this.B.d();
            v0_3 = 1;
        }
        return v0_3;
    }
}
