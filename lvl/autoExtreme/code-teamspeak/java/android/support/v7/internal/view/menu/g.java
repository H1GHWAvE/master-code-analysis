package android.support.v7.internal.view.menu;
public final class g implements android.support.v7.internal.view.menu.x, android.widget.AdapterView$OnItemClickListener {
    public static final String i = "android:menu:list";
    private static final String j = "ListMenuPresenter";
    android.content.Context a;
    android.view.LayoutInflater b;
    android.support.v7.internal.view.menu.i c;
    android.support.v7.internal.view.menu.ExpandedMenuView d;
    int e;
    int f;
    public android.support.v7.internal.view.menu.y g;
    android.support.v7.internal.view.menu.h h;
    private int k;
    private int l;

    private g(int p2)
    {
        this.f = p2;
        this.e = 0;
        return;
    }

    public g(android.content.Context p2, int p3)
    {
        this(p3);
        this.a = p2;
        this.b = android.view.LayoutInflater.from(this.a);
        return;
    }

    static synthetic int a(android.support.v7.internal.view.menu.g p1)
    {
        return p1.k;
    }

    private void a(int p2)
    {
        this.k = p2;
        if (this.d != null) {
            this.a(0);
        }
        return;
    }

    private void a(android.os.Bundle p3)
    {
        android.util.SparseArray v0_1 = new android.util.SparseArray();
        if (this.d != null) {
            this.d.saveHierarchyState(v0_1);
        }
        p3.putSparseParcelableArray("android:menu:list", v0_1);
        return;
    }

    private void b(int p1)
    {
        this.l = p1;
        return;
    }

    private void b(android.os.Bundle p3)
    {
        android.util.SparseArray v0_1 = p3.getSparseParcelableArray("android:menu:list");
        if (v0_1 != null) {
            this.d.restoreHierarchyState(v0_1);
        }
        return;
    }

    private int e()
    {
        return this.k;
    }

    public final android.support.v7.internal.view.menu.z a(android.view.ViewGroup p4)
    {
        if (this.d == null) {
            this.d = ((android.support.v7.internal.view.menu.ExpandedMenuView) this.b.inflate(android.support.v7.a.k.abc_expanded_menu_layout, p4, 0));
            if (this.h == null) {
                this.h = new android.support.v7.internal.view.menu.h(this);
            }
            this.d.setAdapter(this.h);
            this.d.setOnItemClickListener(this);
        }
        return this.d;
    }

    public final void a(android.content.Context p3, android.support.v7.internal.view.menu.i p4)
    {
        if (this.e == 0) {
            if (this.a != null) {
                this.a = p3;
                if (this.b == null) {
                    this.b = android.view.LayoutInflater.from(this.a);
                }
            }
        } else {
            this.a = new android.view.ContextThemeWrapper(p3, this.e);
            this.b = android.view.LayoutInflater.from(this.a);
        }
        this.c = p4;
        if (this.h != null) {
            this.h.notifyDataSetChanged();
        }
        return;
    }

    public final void a(android.os.Parcelable p3)
    {
        android.util.SparseArray v0_1 = ((android.os.Bundle) p3).getSparseParcelableArray("android:menu:list");
        if (v0_1 != null) {
            this.d.restoreHierarchyState(v0_1);
        }
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p2, boolean p3)
    {
        if (this.g != null) {
            this.g.a(p2, p3);
        }
        return;
    }

    public final void a(android.support.v7.internal.view.menu.y p1)
    {
        this.g = p1;
        return;
    }

    public final void a(boolean p2)
    {
        if (this.h != null) {
            this.h.notifyDataSetChanged();
        }
        return;
    }

    public final boolean a()
    {
        return 0;
    }

    public final boolean a(android.support.v7.internal.view.menu.ad p7)
    {
        int v0_6;
        if (p7.hasVisibleItems()) {
            int v0_2 = new android.support.v7.internal.view.menu.l(p7);
            android.view.WindowManager$LayoutParams v1_0 = v0_2.a;
            int v2_1 = new android.support.v7.app.ag(v1_0.e);
            v0_2.c = new android.support.v7.internal.view.menu.g(v2_1.a.a, android.support.v7.a.k.abc_list_menu_item_layout);
            v0_2.c.g = v0_2;
            v0_2.a.a(v0_2.c);
            v2_1.a.t = v0_2.c.d();
            v2_1.a.u = v0_2;
            int v3_8 = v1_0.l;
            if (v3_8 == 0) {
                v2_1.a.d = v1_0.k;
                v2_1.a.f = v1_0.j;
            } else {
                v2_1.a.g = v3_8;
            }
            v2_1.a.r = v0_2;
            v0_2.b = v2_1.a();
            v0_2.b.setOnDismissListener(v0_2);
            android.view.WindowManager$LayoutParams v1_8 = v0_2.b.getWindow().getAttributes();
            v1_8.type = 1003;
            v1_8.flags = (v1_8.flags | 131072);
            v0_2.b.show();
            if (this.g != null) {
                this.g.a_(p7);
            }
            v0_6 = 1;
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    public final boolean a(android.support.v7.internal.view.menu.m p2)
    {
        return 0;
    }

    public final int b()
    {
        return this.l;
    }

    public final boolean b(android.support.v7.internal.view.menu.m p2)
    {
        return 0;
    }

    public final android.os.Parcelable c()
    {
        android.os.Bundle v0_2;
        if (this.d != null) {
            v0_2 = new android.os.Bundle();
            android.util.SparseArray v1_1 = new android.util.SparseArray();
            if (this.d != null) {
                this.d.saveHierarchyState(v1_1);
            }
            v0_2.putSparseParcelableArray("android:menu:list", v1_1);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final android.widget.ListAdapter d()
    {
        if (this.h == null) {
            this.h = new android.support.v7.internal.view.menu.h(this);
        }
        return this.h;
    }

    public final void onItemClick(android.widget.AdapterView p4, android.view.View p5, int p6, long p7)
    {
        this.c.a(this.h.a(p6), this, 0);
        return;
    }
}
