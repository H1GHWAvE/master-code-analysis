package android.support.v7.internal.view.menu;
public class v implements android.support.v7.internal.view.menu.x, android.view.View$OnKeyListener, android.view.ViewTreeObserver$OnGlobalLayoutListener, android.widget.AdapterView$OnItemClickListener, android.widget.PopupWindow$OnDismissListener {
    static final int a = 0;
    private static final String g = "MenuPopupHelper";
    public android.view.View b;
    public android.support.v7.widget.an c;
    public android.support.v7.internal.view.menu.y d;
    public boolean e;
    public int f;
    private final android.content.Context h;
    private final android.view.LayoutInflater i;
    private final android.support.v7.internal.view.menu.i j;
    private final android.support.v7.internal.view.menu.w k;
    private final boolean l;
    private final int m;
    private final int n;
    private final int o;
    private android.view.ViewTreeObserver p;
    private android.view.ViewGroup q;
    private boolean r;
    private int s;

    static v()
    {
        android.support.v7.internal.view.menu.v.a = android.support.v7.a.k.abc_popup_menu_item_layout;
        return;
    }

    public v(android.content.Context p7, android.support.v7.internal.view.menu.i p8)
    {
        this(p7, p8, 0, 0, android.support.v7.a.d.popupMenuStyle);
        return;
    }

    public v(android.content.Context p7, android.support.v7.internal.view.menu.i p8, android.view.View p9)
    {
        this(p7, p8, p9, 0, android.support.v7.a.d.popupMenuStyle);
        return;
    }

    public v(android.content.Context p8, android.support.v7.internal.view.menu.i p9, android.view.View p10, boolean p11, int p12)
    {
        this(p8, p9, p10, p11, p12, 0);
        return;
    }

    public v(android.content.Context p4, android.support.v7.internal.view.menu.i p5, android.view.View p6, boolean p7, int p8, byte p9)
    {
        this.f = 0;
        this.h = p4;
        this.i = android.view.LayoutInflater.from(p4);
        this.j = p5;
        this.k = new android.support.v7.internal.view.menu.w(this, this.j);
        this.l = p7;
        this.n = p8;
        this.o = 0;
        int v0_3 = p4.getResources();
        this.m = Math.max((v0_3.getDisplayMetrics().widthPixels / 2), v0_3.getDimensionPixelSize(android.support.v7.a.g.abc_config_prefDialogWidth));
        this.b = p6;
        p5.a(this, p4);
        return;
    }

    private void a(int p1)
    {
        this.f = p1;
        return;
    }

    private void a(android.view.View p1)
    {
        this.b = p1;
        return;
    }

    static synthetic boolean a(android.support.v7.internal.view.menu.v p1)
    {
        return p1.l;
    }

    static synthetic android.view.LayoutInflater b(android.support.v7.internal.view.menu.v p1)
    {
        return p1.i;
    }

    private void b(boolean p1)
    {
        this.e = p1;
        return;
    }

    static synthetic android.support.v7.internal.view.menu.i c(android.support.v7.internal.view.menu.v p1)
    {
        return p1.j;
    }

    private int h()
    {
        return this.f;
    }

    private android.support.v7.widget.an i()
    {
        return this.c;
    }

    private int j()
    {
        android.support.v7.internal.view.menu.w v6 = this.k;
        int v7 = android.view.View$MeasureSpec.makeMeasureSpec(0, 0);
        int v8 = android.view.View$MeasureSpec.makeMeasureSpec(0, 0);
        int v9 = v6.getCount();
        int v5 = 0;
        int v2 = 0;
        android.view.View v4_0 = 0;
        int v1_0 = 0;
        while (v5 < v9) {
            int v0_2;
            int v0_1 = v6.getItemViewType(v5);
            if (v0_1 == v2) {
                v0_2 = v4_0;
            } else {
                v2 = v0_1;
                v0_2 = 0;
            }
            if (this.q == null) {
                this.q = new android.widget.FrameLayout(this.h);
            }
            v4_0 = v6.getView(v5, v0_2, this.q);
            v4_0.measure(v7, v8);
            int v0_3 = v4_0.getMeasuredWidth();
            if (v0_3 < this.m) {
                if (v0_3 <= v1_0) {
                    v0_3 = v1_0;
                }
                v5++;
                v1_0 = v0_3;
            } else {
                v1_0 = this.m;
                break;
            }
        }
        return v1_0;
    }

    public final android.support.v7.internal.view.menu.z a(android.view.ViewGroup p3)
    {
        throw new UnsupportedOperationException("MenuPopupHelpers manage their own views");
    }

    public final void a(android.content.Context p1, android.support.v7.internal.view.menu.i p2)
    {
        return;
    }

    public final void a(android.os.Parcelable p1)
    {
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p2, boolean p3)
    {
        if (p2 == this.j) {
            this.f();
            if (this.d != null) {
                this.d.a(p2, p3);
            }
        }
        return;
    }

    public final void a(android.support.v7.internal.view.menu.y p1)
    {
        this.d = p1;
        return;
    }

    public final void a(boolean p2)
    {
        this.r = 0;
        if (this.k != null) {
            this.k.notifyDataSetChanged();
        }
        return;
    }

    public final boolean a()
    {
        return 0;
    }

    public final boolean a(android.support.v7.internal.view.menu.ad p8)
    {
        int v1 = 1;
        if (!p8.hasVisibleItems()) {
            v1 = 0;
        } else {
            android.support.v7.internal.view.menu.v v3_1 = new android.support.v7.internal.view.menu.v(this.h, p8, this.b);
            v3_1.d = this.d;
            int v4_1 = p8.size();
            android.support.v7.internal.view.menu.y v0_3 = 0;
            while (v0_3 < v4_1) {
                android.graphics.drawable.Drawable v5_0 = p8.getItem(v0_3);
                if ((!v5_0.isVisible()) || (v5_0.getIcon() == null)) {
                    v0_3++;
                } else {
                    android.support.v7.internal.view.menu.y v0_4 = 1;
                }
                v3_1.e = v0_4;
                if (!v3_1.e()) {
                } else {
                    if (this.d != null) {
                        this.d.a_(p8);
                    }
                }
            }
            v0_4 = 0;
        }
        return v1;
    }

    public final boolean a(android.support.v7.internal.view.menu.m p2)
    {
        return 0;
    }

    public final int b()
    {
        return 0;
    }

    public final boolean b(android.support.v7.internal.view.menu.m p2)
    {
        return 0;
    }

    public final android.os.Parcelable c()
    {
        return 0;
    }

    public final void d()
    {
        if (this.e()) {
            return;
        } else {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    public final boolean e()
    {
        int v1 = 1;
        int v2_0 = 0;
        this.c = new android.support.v7.widget.an(this.h, 0, this.n, this.o);
        this.c.a(this);
        this.c.m = this;
        this.c.a(this.k);
        this.c.c();
        int v3_2 = this.b;
        if (v3_2 == 0) {
            v1 = 0;
        } else {
            int v0_7;
            if (this.p != null) {
                v0_7 = 0;
            } else {
                v0_7 = 1;
            }
            this.p = v3_2.getViewTreeObserver();
            if (v0_7 != 0) {
                this.p.addOnGlobalLayoutListener(this);
            }
            this.c.l = v3_2;
            this.c.i = this.f;
            if (!this.r) {
                android.support.v7.internal.view.menu.w v7 = this.k;
                int v8 = android.view.View$MeasureSpec.makeMeasureSpec(0, 0);
                int v9 = android.view.View$MeasureSpec.makeMeasureSpec(0, 0);
                int v10 = v7.getCount();
                int v6_1 = 0;
                int v3_4 = 0;
                android.view.View v5_2 = 0;
                while (v6_1 < v10) {
                    int v0_13;
                    int v0_12 = v7.getItemViewType(v6_1);
                    if (v0_12 == v3_4) {
                        v0_13 = v5_2;
                    } else {
                        v3_4 = v0_12;
                        v0_13 = 0;
                    }
                    if (this.q == null) {
                        this.q = new android.widget.FrameLayout(this.h);
                    }
                    v5_2 = v7.getView(v6_1, v0_13, this.q);
                    v5_2.measure(v8, v9);
                    int v0_14 = v5_2.getMeasuredWidth();
                    if (v0_14 < this.m) {
                        if (v0_14 <= v2_0) {
                            v0_14 = v2_0;
                        }
                        v6_1++;
                        v2_0 = v0_14;
                    } else {
                        v2_0 = this.m;
                        break;
                    }
                }
                this.s = v2_0;
                this.r = 1;
            }
            this.c.a(this.s);
            this.c.e();
            this.c.b();
            this.c.d.setOnKeyListener(this);
        }
        return v1;
    }

    public final void f()
    {
        if (this.g()) {
            this.c.d();
        }
        return;
    }

    public final boolean g()
    {
        if ((this.c == null) || (!this.c.c.isShowing())) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public void onDismiss()
    {
        this.c = 0;
        this.j.close();
        if (this.p != null) {
            if (!this.p.isAlive()) {
                this.p = this.b.getViewTreeObserver();
            }
            this.p.removeGlobalOnLayoutListener(this);
            this.p = 0;
        }
        return;
    }

    public void onGlobalLayout()
    {
        if (this.g()) {
            android.support.v7.widget.an v0_1 = this.b;
            if ((v0_1 != null) && (v0_1.isShown())) {
                if (this.g()) {
                    this.c.b();
                }
            } else {
                this.f();
            }
        }
        return;
    }

    public void onItemClick(android.widget.AdapterView p5, android.view.View p6, int p7, long p8)
    {
        android.support.v7.internal.view.menu.m v0_0 = this.k;
        android.support.v7.internal.view.menu.w.a(v0_0).a(v0_0.a(p7), 0, 0);
        return;
    }

    public boolean onKey(android.view.View p3, int p4, android.view.KeyEvent p5)
    {
        int v0 = 1;
        if ((p5.getAction() != 1) || (p4 != 82)) {
            v0 = 0;
        } else {
            this.f();
        }
        return v0;
    }
}
