package android.support.v7.internal.view.menu;
final class w extends android.widget.BaseAdapter {
    final synthetic android.support.v7.internal.view.menu.v a;
    private android.support.v7.internal.view.menu.i b;
    private int c;

    public w(android.support.v7.internal.view.menu.v p2, android.support.v7.internal.view.menu.i p3)
    {
        this.a = p2;
        this.c = -1;
        this.b = p3;
        this.a();
        return;
    }

    static synthetic android.support.v7.internal.view.menu.i a(android.support.v7.internal.view.menu.w p1)
    {
        return p1.b;
    }

    private void a()
    {
        android.support.v7.internal.view.menu.m v2 = android.support.v7.internal.view.menu.v.c(this.a).n;
        if (v2 == null) {
            this.c = -1;
        } else {
            java.util.ArrayList v3 = android.support.v7.internal.view.menu.v.c(this.a).j();
            int v4 = v3.size();
            int v1 = 0;
            while (v1 < v4) {
                if (((android.support.v7.internal.view.menu.m) v3.get(v1)) != v2) {
                    v1++;
                } else {
                    this.c = v1;
                }
            }
        }
        return;
    }

    public final android.support.v7.internal.view.menu.m a(int p3)
    {
        android.support.v7.internal.view.menu.m v0_3;
        if (!android.support.v7.internal.view.menu.v.a(this.a)) {
            v0_3 = this.b.h();
        } else {
            v0_3 = this.b.j();
        }
        if ((this.c >= 0) && (p3 >= this.c)) {
            p3++;
        }
        return ((android.support.v7.internal.view.menu.m) v0_3.get(p3));
    }

    public final int getCount()
    {
        int v0_3;
        if (!android.support.v7.internal.view.menu.v.a(this.a)) {
            v0_3 = this.b.h();
        } else {
            v0_3 = this.b.j();
        }
        int v0_6;
        if (this.c >= 0) {
            v0_6 = (v0_3.size() - 1);
        } else {
            v0_6 = v0_3.size();
        }
        return v0_6;
    }

    public final synthetic Object getItem(int p2)
    {
        return this.a(p2);
    }

    public final long getItemId(int p3)
    {
        return ((long) p3);
    }

    public final android.view.View getView(int p5, android.view.View p6, android.view.ViewGroup p7)
    {
        android.view.View v1_0;
        if (p6 != null) {
            v1_0 = p6;
        } else {
            v1_0 = android.support.v7.internal.view.menu.v.b(this.a).inflate(android.support.v7.internal.view.menu.v.a, p7, 0);
        }
        android.support.v7.internal.view.menu.aa v0_3 = ((android.support.v7.internal.view.menu.aa) v1_0);
        if (this.a.e) {
            ((android.support.v7.internal.view.menu.ListMenuItemView) v1_0).setForceShowIcon(1);
        }
        v0_3.a(this.a(p5));
        return v1_0;
    }

    public final void notifyDataSetChanged()
    {
        this.a();
        super.notifyDataSetChanged();
        return;
    }
}
