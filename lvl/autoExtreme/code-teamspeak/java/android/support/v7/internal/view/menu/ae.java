package android.support.v7.internal.view.menu;
final class ae extends android.support.v7.internal.view.menu.ac implements android.view.SubMenu {

    ae(android.content.Context p1, android.support.v4.g.a.c p2)
    {
        this(p1, p2);
        return;
    }

    private android.support.v4.g.a.c b()
    {
        return ((android.support.v4.g.a.c) this.d);
    }

    public final bridge synthetic Object a()
    {
        return ((android.support.v4.g.a.c) this.d);
    }

    public final void clearHeader()
    {
        ((android.support.v4.g.a.c) this.d).clearHeader();
        return;
    }

    public final android.view.MenuItem getItem()
    {
        return this.a(((android.support.v4.g.a.c) this.d).getItem());
    }

    public final android.view.SubMenu setHeaderIcon(int p2)
    {
        ((android.support.v4.g.a.c) this.d).setHeaderIcon(p2);
        return this;
    }

    public final android.view.SubMenu setHeaderIcon(android.graphics.drawable.Drawable p2)
    {
        ((android.support.v4.g.a.c) this.d).setHeaderIcon(p2);
        return this;
    }

    public final android.view.SubMenu setHeaderTitle(int p2)
    {
        ((android.support.v4.g.a.c) this.d).setHeaderTitle(p2);
        return this;
    }

    public final android.view.SubMenu setHeaderTitle(CharSequence p2)
    {
        ((android.support.v4.g.a.c) this.d).setHeaderTitle(p2);
        return this;
    }

    public final android.view.SubMenu setHeaderView(android.view.View p2)
    {
        ((android.support.v4.g.a.c) this.d).setHeaderView(p2);
        return this;
    }

    public final android.view.SubMenu setIcon(int p2)
    {
        ((android.support.v4.g.a.c) this.d).setIcon(p2);
        return this;
    }

    public final android.view.SubMenu setIcon(android.graphics.drawable.Drawable p2)
    {
        ((android.support.v4.g.a.c) this.d).setIcon(p2);
        return this;
    }
}
