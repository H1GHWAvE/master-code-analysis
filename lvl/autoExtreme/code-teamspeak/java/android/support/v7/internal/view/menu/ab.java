package android.support.v7.internal.view.menu;
public final class ab {

    private ab()
    {
        return;
    }

    public static android.view.Menu a(android.content.Context p2, android.support.v4.g.a.a p3)
    {
        if (android.os.Build$VERSION.SDK_INT < 14) {
            throw new UnsupportedOperationException();
        } else {
            return new android.support.v7.internal.view.menu.ac(p2, p3);
        }
    }

    public static android.view.MenuItem a(android.content.Context p2, android.support.v4.g.a.b p3)
    {
        UnsupportedOperationException v0_5;
        if (android.os.Build$VERSION.SDK_INT < 16) {
            if (android.os.Build$VERSION.SDK_INT < 14) {
                throw new UnsupportedOperationException();
            } else {
                v0_5 = new android.support.v7.internal.view.menu.o(p2, p3);
            }
        } else {
            v0_5 = new android.support.v7.internal.view.menu.t(p2, p3);
        }
        return v0_5;
    }

    private static android.view.SubMenu a(android.content.Context p2, android.support.v4.g.a.c p3)
    {
        if (android.os.Build$VERSION.SDK_INT < 14) {
            throw new UnsupportedOperationException();
        } else {
            return new android.support.v7.internal.view.menu.ae(p2, p3);
        }
    }
}
