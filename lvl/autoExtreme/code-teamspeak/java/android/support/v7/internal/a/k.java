package android.support.v7.internal.a;
final class k extends android.support.v7.internal.view.k {
    final synthetic android.support.v7.internal.a.e a;

    public k(android.support.v7.internal.a.e p1, android.view.Window$Callback p2)
    {
        this.a = p1;
        this(p2);
        return;
    }

    public final android.view.View onCreatePanelView(int p11)
    {
        int v0_12;
        switch (p11) {
            case 0:
                android.view.ViewGroup v1_0 = this.a.i.B();
                if ((!this.onPreparePanel(p11, 0, v1_0)) || (!this.onMenuOpened(p11, v1_0))) {
                    v0_12 = super.onCreatePanelView(p11);
                } else {
                    android.support.v7.internal.a.e v3 = this.a;
                    if ((v3.l == null) && ((v1_0 instanceof android.support.v7.internal.view.menu.i))) {
                        int v0_7 = ((android.support.v7.internal.view.menu.i) v1_0);
                        android.support.v7.internal.view.menu.g v4_1 = v3.i.b();
                        android.support.v7.internal.a.j v5_1 = new android.util.TypedValue();
                        int v6_1 = v4_1.getResources().newTheme();
                        v6_1.setTo(v4_1.getTheme());
                        v6_1.resolveAttribute(android.support.v7.a.d.actionBarPopupTheme, v5_1, 1);
                        if (v5_1.resourceId != 0) {
                            v6_1.applyStyle(v5_1.resourceId, 1);
                        }
                        v6_1.resolveAttribute(android.support.v7.a.d.panelMenuListTheme, v5_1, 1);
                        if (v5_1.resourceId == 0) {
                            v6_1.applyStyle(android.support.v7.a.m.Theme_AppCompat_CompactMenu, 1);
                        } else {
                            v6_1.applyStyle(v5_1.resourceId, 1);
                        }
                        android.support.v7.internal.a.j v5_5 = new android.view.ContextThemeWrapper(v4_1, 0);
                        v5_5.getTheme().setTo(v6_1);
                        v3.l = new android.support.v7.internal.view.menu.g(v5_5, android.support.v7.a.k.abc_list_menu_item_layout);
                        v3.l.g = new android.support.v7.internal.a.j(v3, 0);
                        v0_7.a(v3.l);
                    }
                    if ((v1_0 != null) && (v3.l != null)) {
                        if (v3.l.d().getCount() <= 0) {
                            v0_12 = 0;
                        } else {
                            v0_12 = ((android.view.View) v3.l.a(v3.i.a()));
                        }
                    } else {
                        v0_12 = 0;
                    }
                }
                break;
            default:
        }
        return v0_12;
    }

    public final boolean onPreparePanel(int p4, android.view.View p5, android.view.Menu p6)
    {
        boolean v0 = super.onPreparePanel(p4, p5, p6);
        if ((v0) && (!this.a.j)) {
            this.a.i.p();
            this.a.j = 1;
        }
        return v0;
    }
}
