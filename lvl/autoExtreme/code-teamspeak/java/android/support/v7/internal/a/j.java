package android.support.v7.internal.a;
final class j implements android.support.v7.internal.view.menu.y {
    final synthetic android.support.v7.internal.a.e a;

    private j(android.support.v7.internal.a.e p1)
    {
        this.a = p1;
        return;
    }

    synthetic j(android.support.v7.internal.a.e p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p3, boolean p4)
    {
        if (this.a.k != null) {
            this.a.k.onPanelClosed(0, p3);
        }
        return;
    }

    public final boolean a_(android.support.v7.internal.view.menu.i p3)
    {
        if ((p3 == null) && (this.a.k != null)) {
            this.a.k.onMenuOpened(0, p3);
        }
        return 1;
    }
}
