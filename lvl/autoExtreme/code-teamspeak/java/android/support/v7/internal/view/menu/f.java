package android.support.v7.internal.view.menu;
public class f {
    public final Object d;

    f(Object p3)
    {
        if (p3 != null) {
            this.d = p3;
            return;
        } else {
            throw new IllegalArgumentException("Wrapped Object can not be null.");
        }
    }

    public Object a()
    {
        return this.d;
    }
}
