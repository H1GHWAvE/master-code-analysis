package android.support.v7.app;
abstract class ak extends android.support.v7.app.aj {
    final android.content.Context e;
    final android.view.Window f;
    final android.view.Window$Callback g;
    final android.view.Window$Callback h;
    final android.support.v7.app.ai i;
    android.support.v7.app.a j;
    android.view.MenuInflater k;
    boolean l;
    boolean m;
    boolean n;
    boolean o;
    boolean p;
    CharSequence q;
    boolean r;

    ak(android.content.Context p3, android.view.Window p4, android.support.v7.app.ai p5)
    {
        this.e = p3;
        this.f = p4;
        this.i = p5;
        this.g = this.f.getCallback();
        if (!(this.g instanceof android.support.v7.app.an)) {
            this.h = this.a(this.g);
            this.f.setCallback(this.h);
            return;
        } else {
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
    }

    private android.support.v7.app.a n()
    {
        return this.j;
    }

    private boolean o()
    {
        return this.r;
    }

    private android.view.Window$Callback p()
    {
        return this.f.getCallback();
    }

    private CharSequence q()
    {
        CharSequence v0_2;
        if (!(this.g instanceof android.app.Activity)) {
            v0_2 = this.q;
        } else {
            v0_2 = ((android.app.Activity) this.g).getTitle();
        }
        return v0_2;
    }

    public final android.support.v7.app.a a()
    {
        this.l();
        return this.j;
    }

    android.view.Window$Callback a(android.view.Window$Callback p2)
    {
        return new android.support.v7.app.an(this, p2);
    }

    public final void a(CharSequence p1)
    {
        this.q = p1;
        this.b(p1);
        return;
    }

    public void a(boolean p1)
    {
        return;
    }

    abstract boolean a();

    abstract boolean a();

    abstract android.support.v7.c.a b();

    public final android.view.MenuInflater b()
    {
        if (this.k == null) {
            android.content.Context v0_2;
            this.l();
            if (this.j == null) {
                v0_2 = this.e;
            } else {
                v0_2 = this.j.r();
            }
            this.k = new android.support.v7.internal.view.f(v0_2);
        }
        return this.k;
    }

    abstract void b();

    abstract void d();

    abstract boolean e();

    public final void h()
    {
        this.r = 1;
        return;
    }

    public final android.support.v7.app.l i()
    {
        return new android.support.v7.app.am(this, 0);
    }

    public boolean k()
    {
        return 0;
    }

    abstract void l();

    final android.content.Context m()
    {
        android.content.Context v0 = 0;
        android.support.v7.app.a v1 = this.a();
        if (v1 != null) {
            v0 = v1.r();
        }
        if (v0 == null) {
            v0 = this.e;
        }
        return v0;
    }
}
