package android.support.v7.app;
final class be implements android.support.v7.internal.view.menu.y {
    final synthetic android.support.v7.app.AppCompatDelegateImplV7 a;

    private be(android.support.v7.app.AppCompatDelegateImplV7 p1)
    {
        this.a = p1;
        return;
    }

    synthetic be(android.support.v7.app.AppCompatDelegateImplV7 p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p6, boolean p7)
    {
        android.support.v7.app.AppCompatDelegateImplV7 v0_0;
        android.support.v7.internal.view.menu.i v2 = p6.k();
        if (v2 == p6) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        if (v0_0 != null) {
            p6 = v2;
        }
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v3_1 = this.a.a(p6);
        if (v3_1 != null) {
            if (v0_0 == null) {
                this.a.a(v3_1, p7);
            } else {
                this.a.a(v3_1.a, v3_1, v2);
                this.a.a(v3_1, 1);
            }
        }
        return;
    }

    public final boolean a_(android.support.v7.internal.view.menu.i p3)
    {
        if ((p3 == null) && (this.a.l)) {
            android.view.Window$Callback v0_4 = this.a.f.getCallback();
            if ((v0_4 != null) && (!this.a.r)) {
                v0_4.onMenuOpened(108, p3);
            }
        }
        return 1;
    }
}
