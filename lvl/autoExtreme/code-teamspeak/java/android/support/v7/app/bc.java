package android.support.v7.app;
final class bc extends android.widget.FrameLayout {
    final synthetic android.support.v7.app.AppCompatDelegateImplV7 a;

    public bc(android.support.v7.app.AppCompatDelegateImplV7 p1, android.content.Context p2)
    {
        this.a = p1;
        this(p2);
        return;
    }

    private boolean a(int p2, int p3)
    {
        if ((p2 >= -5) && ((p3 >= -5) && ((p2 <= (this.getWidth() + 5)) && (p3 <= (this.getHeight() + 5))))) {
            int v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public final boolean dispatchKeyEvent(android.view.KeyEvent p2)
    {
        if ((!this.a.a(p2)) && (!super.dispatchKeyEvent(p2))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean onInterceptTouchEvent(android.view.MotionEvent p6)
    {
        boolean v0 = 1;
        if (p6.getAction() != 0) {
            v0 = super.onInterceptTouchEvent(p6);
        } else {
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v1_5;
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v1_2 = ((int) p6.getX());
            int v3_1 = ((int) p6.getY());
            if ((v1_2 >= -5) && ((v3_1 >= -5) && ((v1_2 <= (this.getWidth() + 5)) && (v3_1 <= (this.getHeight() + 5))))) {
                v1_5 = 0;
            } else {
                v1_5 = 1;
            }
            if (v1_5 == null) {
            } else {
                android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v1_6 = this.a;
                v1_6.a(v1_6.f(0), 1);
            }
        }
        return v0;
    }

    public final void setBackgroundResource(int p2)
    {
        this.setBackgroundDrawable(android.support.v7.internal.widget.av.a(this.getContext(), p2));
        return;
    }
}
