package android.support.v7.app;
final class n extends android.support.v7.b.a.c implements android.support.v7.app.o {
    private final android.app.Activity f;

    public n(android.app.Activity p1, android.content.Context p2)
    {
        this(p2);
        this.f = p1;
        return;
    }

    public final float a()
    {
        return this.e;
    }

    public final void a(float p2)
    {
        if (p2 != 1065353216) {
            if (p2 == 0) {
                this.a(0);
            }
        } else {
            this.a(1);
        }
        if (this.e != p2) {
            this.e = p2;
            this.invalidateSelf();
        }
        return;
    }
}
