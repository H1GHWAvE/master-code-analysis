package android.support.v7.widget;
final class cm implements android.os.Parcelable$Creator {

    cm()
    {
        return;
    }

    private static android.support.v7.widget.Toolbar$SavedState a(android.os.Parcel p1)
    {
        return new android.support.v7.widget.Toolbar$SavedState(p1);
    }

    private static android.support.v7.widget.Toolbar$SavedState[] a(int p1)
    {
        android.support.v7.widget.Toolbar$SavedState[] v0 = new android.support.v7.widget.Toolbar$SavedState[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return new android.support.v7.widget.Toolbar$SavedState(p2);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v7.widget.Toolbar$SavedState[] v0 = new android.support.v7.widget.Toolbar$SavedState[p2];
        return v0;
    }
}
