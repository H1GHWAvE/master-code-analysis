package android.support.v7.widget;
final class h implements android.support.v7.internal.view.menu.y {
    final synthetic android.support.v7.widget.ActionMenuPresenter a;

    private h(android.support.v7.widget.ActionMenuPresenter p1)
    {
        this.a = p1;
        return;
    }

    synthetic h(android.support.v7.widget.ActionMenuPresenter p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p3, boolean p4)
    {
        if ((p3 instanceof android.support.v7.internal.view.menu.ad)) {
            ((android.support.v7.internal.view.menu.ad) p3).p.b(0);
        }
        android.support.v7.internal.view.menu.y v0_5 = this.a.f;
        if (v0_5 != null) {
            v0_5.a(p3, p4);
        }
        return;
    }

    public final boolean a_(android.support.v7.internal.view.menu.i p4)
    {
        int v0_6;
        if (p4 != null) {
            this.a.t = ((android.support.v7.internal.view.menu.ad) p4).getItem().getItemId();
            int v0_5 = this.a.f;
            if (v0_5 == 0) {
                v0_6 = 0;
            } else {
                v0_6 = v0_5.a_(p4);
            }
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }
}
