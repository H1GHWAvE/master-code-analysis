package android.support.v7.widget;
public final class w extends android.widget.EditText implements android.support.v4.view.cr {
    private android.support.v7.internal.widget.av a;
    private android.support.v7.widget.q b;
    private android.support.v7.widget.ah c;

    private w(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public w(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, android.support.v7.a.d.editTextStyle);
        return;
    }

    private w(android.content.Context p3, android.util.AttributeSet p4, int p5)
    {
        this(android.support.v7.internal.widget.as.a(p3), p4, p5);
        this.a = android.support.v7.internal.widget.av.a(this.getContext());
        this.b = new android.support.v7.widget.q(this, this.a);
        this.b.a(p4, p5);
        this.c = new android.support.v7.widget.ah(this);
        this.c.a(p4, p5);
        return;
    }

    protected final void drawableStateChanged()
    {
        super.drawableStateChanged();
        if (this.b != null) {
            this.b.c();
        }
        return;
    }

    public final android.content.res.ColorStateList getSupportBackgroundTintList()
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.b.a();
        }
        return v0_1;
    }

    public final android.graphics.PorterDuff$Mode getSupportBackgroundTintMode()
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.b.b();
        }
        return v0_1;
    }

    public final void setBackgroundDrawable(android.graphics.drawable.Drawable p3)
    {
        super.setBackgroundDrawable(p3);
        if (this.b != null) {
            this.b.b(0);
        }
        return;
    }

    public final void setBackgroundResource(int p2)
    {
        super.setBackgroundResource(p2);
        if (this.b != null) {
            this.b.a(p2);
        }
        return;
    }

    public final void setSupportBackgroundTintList(android.content.res.ColorStateList p2)
    {
        if (this.b != null) {
            this.b.a(p2);
        }
        return;
    }

    public final void setSupportBackgroundTintMode(android.graphics.PorterDuff$Mode p2)
    {
        if (this.b != null) {
            this.b.a(p2);
        }
        return;
    }

    public final void setTextAppearance(android.content.Context p2, int p3)
    {
        super.setTextAppearance(p2, p3);
        if (this.c != null) {
            this.c.a(p2, p3);
        }
        return;
    }
}
