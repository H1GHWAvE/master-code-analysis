package android.support.v7.widget;
public final class y extends android.widget.RadioButton implements android.support.v4.widget.ef {
    private android.support.v7.internal.widget.av a;
    private android.support.v7.widget.u b;

    private y(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public y(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, android.support.v7.a.d.radioButtonStyle);
        return;
    }

    private y(android.content.Context p3, android.util.AttributeSet p4, int p5)
    {
        this(p3, p4, p5);
        this.a = android.support.v7.internal.widget.av.a(p3);
        this.b = new android.support.v7.widget.u(this, this.a);
        this.b.a(p4, p5);
        return;
    }

    public final int getCompoundPaddingLeft()
    {
        int v0 = super.getCompoundPaddingLeft();
        if (this.b != null) {
            v0 = this.b.a(v0);
        }
        return v0;
    }

    public final android.content.res.ColorStateList getSupportButtonTintList()
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.b.a;
        }
        return v0_1;
    }

    public final android.graphics.PorterDuff$Mode getSupportButtonTintMode()
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.b.b;
        }
        return v0_1;
    }

    public final void setButtonDrawable(int p3)
    {
        android.graphics.drawable.Drawable v0_2;
        if (this.a == null) {
            v0_2 = android.support.v4.c.h.a(this.getContext(), p3);
        } else {
            v0_2 = this.a.a(p3, 0);
        }
        this.setButtonDrawable(v0_2);
        return;
    }

    public final void setButtonDrawable(android.graphics.drawable.Drawable p2)
    {
        super.setButtonDrawable(p2);
        if (this.b != null) {
            this.b.a();
        }
        return;
    }

    public final void setSupportButtonTintList(android.content.res.ColorStateList p2)
    {
        if (this.b != null) {
            this.b.a(p2);
        }
        return;
    }

    public final void setSupportButtonTintMode(android.graphics.PorterDuff$Mode p2)
    {
        if (this.b != null) {
            this.b.a(p2);
        }
        return;
    }
}
