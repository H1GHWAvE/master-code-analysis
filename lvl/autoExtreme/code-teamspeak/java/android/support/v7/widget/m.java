package android.support.v7.widget;
public final class m extends android.support.v7.widget.al {
    public boolean a;
    public int b;
    public int c;
    public boolean d;
    public boolean e;
    boolean f;

    public m()
    {
        this(-2, -2);
        this.a = 0;
        return;
    }

    private m(int p1, int p2, boolean p3)
    {
        this(p1, p2);
        this.a = p3;
        return;
    }

    public m(android.content.Context p1, android.util.AttributeSet p2)
    {
        this(p1, p2);
        return;
    }

    public m(android.support.v7.widget.m p2)
    {
        this(p2);
        this.a = p2.a;
        return;
    }

    public m(android.view.ViewGroup$LayoutParams p1)
    {
        this(p1);
        return;
    }
}
