package android.support.v7.widget;
public class ai extends android.widget.TextView {
    private android.support.v7.widget.ah a;

    public ai(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public ai(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, 16842884);
        return;
    }

    public ai(android.content.Context p2, android.util.AttributeSet p3, int p4)
    {
        this(p2, p3, p4);
        this.a = new android.support.v7.widget.ah(this);
        this.a.a(p3, p4);
        return;
    }

    public void setTextAppearance(android.content.Context p2, int p3)
    {
        super.setTextAppearance(p2, p3);
        if (this.a != null) {
            this.a.a(p2, p3);
        }
        return;
    }
}
