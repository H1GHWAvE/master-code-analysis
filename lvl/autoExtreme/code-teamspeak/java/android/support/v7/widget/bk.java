package android.support.v7.widget;
final class bk implements android.view.View$OnFocusChangeListener {
    final synthetic android.support.v7.widget.SearchView a;

    bk(android.support.v7.widget.SearchView p1)
    {
        this.a = p1;
        return;
    }

    public final void onFocusChange(android.view.View p3, boolean p4)
    {
        if (android.support.v7.widget.SearchView.c(this.a) != null) {
            android.support.v7.widget.SearchView.c(this.a).onFocusChange(this.a, p4);
        }
        return;
    }
}
