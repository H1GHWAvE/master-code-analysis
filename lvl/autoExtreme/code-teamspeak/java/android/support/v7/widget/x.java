package android.support.v7.widget;
public final class x extends android.widget.MultiAutoCompleteTextView implements android.support.v4.view.cr {
    private static final int[] a;
    private android.support.v7.internal.widget.av b;
    private android.support.v7.widget.q c;
    private android.support.v7.widget.ah d;

    static x()
    {
        int[] v0_1 = new int[1];
        v0_1[0] = 16843126;
        android.support.v7.widget.x.a = v0_1;
        return;
    }

    private x(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public x(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, android.support.v7.a.d.autoCompleteTextViewStyle);
        return;
    }

    private x(android.content.Context p4, android.util.AttributeSet p5, int p6)
    {
        this(android.support.v7.internal.widget.as.a(p4), p5, p6);
        android.support.v7.widget.ah v0_2 = android.support.v7.internal.widget.ax.a(this.getContext(), p5, android.support.v7.widget.x.a, p6);
        this.b = v0_2.a();
        if (v0_2.e(0)) {
            this.setDropDownBackgroundDrawable(v0_2.a(0));
        }
        v0_2.a.recycle();
        this.c = new android.support.v7.widget.q(this, this.b);
        this.c.a(p5, p6);
        this.d = new android.support.v7.widget.ah(this);
        this.d.a(p5, p6);
        return;
    }

    protected final void drawableStateChanged()
    {
        super.drawableStateChanged();
        if (this.c != null) {
            this.c.c();
        }
        return;
    }

    public final android.content.res.ColorStateList getSupportBackgroundTintList()
    {
        int v0_1;
        if (this.c == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.c.a();
        }
        return v0_1;
    }

    public final android.graphics.PorterDuff$Mode getSupportBackgroundTintMode()
    {
        int v0_1;
        if (this.c == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.c.b();
        }
        return v0_1;
    }

    public final void setBackgroundDrawable(android.graphics.drawable.Drawable p3)
    {
        super.setBackgroundDrawable(p3);
        if (this.c != null) {
            this.c.b(0);
        }
        return;
    }

    public final void setBackgroundResource(int p2)
    {
        super.setBackgroundResource(p2);
        if (this.c != null) {
            this.c.a(p2);
        }
        return;
    }

    public final void setDropDownBackgroundResource(int p3)
    {
        if (this.b == null) {
            super.setDropDownBackgroundResource(p3);
        } else {
            this.setDropDownBackgroundDrawable(this.b.a(p3, 0));
        }
        return;
    }

    public final void setSupportBackgroundTintList(android.content.res.ColorStateList p2)
    {
        if (this.c != null) {
            this.c.a(p2);
        }
        return;
    }

    public final void setSupportBackgroundTintMode(android.graphics.PorterDuff$Mode p2)
    {
        if (this.c != null) {
            this.c.a(p2);
        }
        return;
    }

    public final void setTextAppearance(android.content.Context p2, int p3)
    {
        super.setTextAppearance(p2, p3);
        if (this.d != null) {
            this.d.a(p2, p3);
        }
        return;
    }
}
