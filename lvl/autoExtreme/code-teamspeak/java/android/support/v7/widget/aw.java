package android.support.v7.widget;
final class aw extends android.database.DataSetObserver {
    final synthetic android.support.v7.widget.an a;

    private aw(android.support.v7.widget.an p1)
    {
        this.a = p1;
        return;
    }

    synthetic aw(android.support.v7.widget.an p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void onChanged()
    {
        if (this.a.c.isShowing()) {
            this.a.b();
        }
        return;
    }

    public final void onInvalidated()
    {
        this.a.d();
        return;
    }
}
