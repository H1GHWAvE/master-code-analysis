package android.support.v7.widget;
final class u {
    android.content.res.ColorStateList a;
    android.graphics.PorterDuff$Mode b;
    private final android.widget.CompoundButton c;
    private final android.support.v7.internal.widget.av d;
    private boolean e;
    private boolean f;
    private boolean g;

    u(android.widget.CompoundButton p3, android.support.v7.internal.widget.av p4)
    {
        this.a = 0;
        this.b = 0;
        this.e = 0;
        this.f = 0;
        this.c = p3;
        this.d = p4;
        return;
    }

    private android.content.res.ColorStateList b()
    {
        return this.a;
    }

    private android.graphics.PorterDuff$Mode c()
    {
        return this.b;
    }

    private void d()
    {
        android.graphics.drawable.Drawable v0_1 = android.support.v4.widget.g.a(this.c);
        if ((v0_1 != null) && ((this.e) || (this.f))) {
            android.graphics.drawable.Drawable v0_3 = android.support.v4.e.a.a.c(v0_1).mutate();
            if (this.e) {
                android.support.v4.e.a.a.a(v0_3, this.a);
            }
            if (this.f) {
                android.support.v4.e.a.a.a(v0_3, this.b);
            }
            if (v0_3.isStateful()) {
                v0_3.setState(this.c.getDrawableState());
            }
            this.c.setButtonDrawable(v0_3);
        }
        return;
    }

    final int a(int p3)
    {
        if (android.os.Build$VERSION.SDK_INT < 17) {
            int v0_2 = android.support.v4.widget.g.a(this.c);
            if (v0_2 != 0) {
                p3 += v0_2.getIntrinsicWidth();
            }
        }
        return p3;
    }

    final void a()
    {
        if (!this.g) {
            this.g = 1;
            this.d();
        } else {
            this.g = 0;
        }
        return;
    }

    final void a(android.content.res.ColorStateList p2)
    {
        this.a = p2;
        this.e = 1;
        this.d();
        return;
    }

    final void a(android.graphics.PorterDuff$Mode p2)
    {
        this.b = p2;
        this.f = 1;
        this.d();
        return;
    }

    final void a(android.util.AttributeSet p6, int p7)
    {
        android.content.res.TypedArray v1_1 = this.c.getContext().obtainStyledAttributes(p6, android.support.v7.a.n.CompoundButton, p7, 0);
        try {
            if (v1_1.hasValue(android.support.v7.a.n.CompoundButton_android_button)) {
                android.widget.CompoundButton v0_5 = v1_1.getResourceId(android.support.v7.a.n.CompoundButton_android_button, 0);
                if (v0_5 != null) {
                    this.c.setButtonDrawable(this.d.a(v0_5, 0));
                }
            }
        } catch (android.widget.CompoundButton v0_13) {
            v1_1.recycle();
            throw v0_13;
        }
        if (v1_1.hasValue(android.support.v7.a.n.CompoundButton_buttonTint)) {
            android.support.v4.widget.g.a(this.c, v1_1.getColorStateList(android.support.v7.a.n.CompoundButton_buttonTint));
        }
        if (v1_1.hasValue(android.support.v7.a.n.CompoundButton_buttonTintMode)) {
            android.support.v4.widget.g.a(this.c, android.support.v7.b.a.a.a(v1_1.getInt(android.support.v7.a.n.CompoundButton_buttonTintMode, -1)));
        }
        v1_1.recycle();
        return;
    }
}
