package android.support.v7.widget;
public class an {
    private static final String a = "ListPopupWindow";
    private static final boolean b = False;
    public static final int n = 0;
    public static final int o = 1;
    public static final int p = 255;
    public static final int q = 254;
    public static final int r = 0;
    public static final int s = 1;
    public static final int t = 2;
    private static final int u = 250;
    private static reflect.Method v;
    private boolean A;
    private android.view.View B;
    private android.database.DataSetObserver C;
    private android.graphics.drawable.Drawable D;
    private android.widget.AdapterView$OnItemSelectedListener E;
    private final android.support.v7.widget.az F;
    private final android.support.v7.widget.ay G;
    private final android.support.v7.widget.ax H;
    private final android.support.v7.widget.av I;
    private Runnable J;
    private android.os.Handler K;
    private android.graphics.Rect L;
    private boolean M;
    private int N;
    public android.widget.PopupWindow c;
    public android.support.v7.widget.ar d;
    int e;
    int f;
    int g;
    boolean h;
    public int i;
    int j;
    int k;
    public android.view.View l;
    public android.widget.AdapterView$OnItemClickListener m;
    private android.content.Context w;
    private android.widget.ListAdapter x;
    private int y;
    private boolean z;

    static an()
    {
        try {
            Class[] v2_1 = new Class[1];
            v2_1[0] = Boolean.TYPE;
            android.support.v7.widget.an.v = android.widget.PopupWindow.getDeclaredMethod("setClipToScreenEnabled", v2_1);
        } catch (String v0) {
            android.util.Log.i("ListPopupWindow", "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
        }
        return;
    }

    public an(android.content.Context p3)
    {
        this(p3, 0, android.support.v7.a.d.listPopupWindowStyle);
        return;
    }

    private an(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, android.support.v7.a.d.listPopupWindowStyle);
        return;
    }

    public an(android.content.Context p2, android.util.AttributeSet p3, int p4)
    {
        this(p2, p3, p4, 0);
        return;
    }

    public an(android.content.Context p5, android.util.AttributeSet p6, int p7, int p8)
    {
        this.y = -2;
        this.e = -2;
        this.i = 0;
        this.z = 0;
        this.A = 0;
        this.j = 2147483647;
        this.k = 0;
        this.F = new android.support.v7.widget.az(this, 0);
        this.G = new android.support.v7.widget.ay(this, 0);
        this.H = new android.support.v7.widget.ax(this, 0);
        this.I = new android.support.v7.widget.av(this, 0);
        this.K = new android.os.Handler();
        this.L = new android.graphics.Rect();
        this.w = p5;
        int v0_15 = p5.obtainStyledAttributes(p6, android.support.v7.a.n.ListPopupWindow, p7, p8);
        this.f = v0_15.getDimensionPixelOffset(android.support.v7.a.n.ListPopupWindow_android_dropDownHorizontalOffset, 0);
        this.g = v0_15.getDimensionPixelOffset(android.support.v7.a.n.ListPopupWindow_android_dropDownVerticalOffset, 0);
        if (this.g != 0) {
            this.h = 1;
        }
        v0_15.recycle();
        this.c = new android.support.v7.internal.widget.aa(p5, p6, p7);
        this.c.setInputMethodMode(1);
        this.N = android.support.v4.m.u.a(this.w.getResources().getConfiguration().locale);
        return;
    }

    private android.widget.ListView A()
    {
        return this.d;
    }

    private int B()
    {
        int v0_2;
        int v2_0 = 0;
        if (this.d != null) {
            this.c.getContentView();
            int v1_1 = this.B;
            if (v1_1 == 0) {
                v0_2 = 0;
            } else {
                int v0_4 = ((android.widget.LinearLayout$LayoutParams) v1_1.getLayoutParams());
                v0_2 = (v0_4.bottomMargin + (v1_1.getMeasuredHeight() + v0_4.topMargin));
            }
        } else {
            int v0_9;
            android.support.v7.widget.ar v4_0 = this.w;
            this.J = new android.support.v7.widget.ap(this);
            if (this.M) {
                v0_9 = 0;
            } else {
                v0_9 = 1;
            }
            this.d = new android.support.v7.widget.ar(v4_0, v0_9);
            if (this.D != null) {
                this.d.setSelector(this.D);
            }
            this.d.setAdapter(this.x);
            this.d.setOnItemClickListener(this.m);
            this.d.setFocusable(1);
            this.d.setFocusableInTouchMode(1);
            this.d.setOnItemSelectedListener(new android.support.v7.widget.aq(this));
            this.d.setOnScrollListener(this.H);
            if (this.E != null) {
                this.d.setOnItemSelectedListener(this.E);
            }
            int v1_4;
            int v0_20 = this.d;
            int v5_0 = this.B;
            if (v5_0 == 0) {
                v1_4 = v0_20;
                v0_2 = 0;
            } else {
                int v3_11 = new android.widget.LinearLayout(v4_0);
                v3_11.setOrientation(1);
                int v1_6 = new android.widget.LinearLayout$LayoutParams(-1, 0, 1065353216);
                switch (this.k) {
                    case 0:
                        v3_11.addView(v5_0);
                        v3_11.addView(v0_20, v1_6);
                        break;
                    case 1:
                        v3_11.addView(v0_20, v1_6);
                        v3_11.addView(v5_0);
                        break;
                    default:
                        android.util.Log.e("ListPopupWindow", new StringBuilder("Invalid hint position ").append(this.k).toString());
                }
                v5_0.measure(android.view.View$MeasureSpec.makeMeasureSpec(this.e, -2147483648), 0);
                int v0_25 = ((android.widget.LinearLayout$LayoutParams) v5_0.getLayoutParams());
                v0_2 = (v0_25.bottomMargin + (v5_0.getMeasuredHeight() + v0_25.topMargin));
                v1_4 = v3_11;
            }
            this.c.setContentView(v1_4);
        }
        int v1_14 = this.c.getBackground();
        if (v1_14 == 0) {
            this.L.setEmpty();
        } else {
            v1_14.getPadding(this.L);
            v2_0 = (this.L.bottom + this.L.top);
            if (!this.h) {
                this.g = (- this.L.top);
            }
        }
        int v0_27;
        this.c.getInputMethodMode();
        int v3_14 = this.c.getMaxAvailableHeight(this.l, this.g);
        if ((!this.z) && (this.y != -1)) {
            int v1_32;
            switch (this.e) {
                case -2:
                    v1_32 = android.view.View$MeasureSpec.makeMeasureSpec((this.w.getResources().getDisplayMetrics().widthPixels - (this.L.left + this.L.right)), -2147483648);
                    break;
                case -1:
                    v1_32 = android.view.View$MeasureSpec.makeMeasureSpec((this.w.getResources().getDisplayMetrics().widthPixels - (this.L.left + this.L.right)), 1073741824);
                    break;
                default:
                    v1_32 = android.view.View$MeasureSpec.makeMeasureSpec(this.e, 1073741824);
            }
            int v1_39 = this.d.a(v1_32, (v3_14 - v0_2));
            if (v1_39 > 0) {
                v0_2 += v2_0;
            }
            v0_27 = (v0_2 + v1_39);
        } else {
            v0_27 = (v3_14 + v2_0);
        }
        return v0_27;
    }

    private void C()
    {
        if (android.support.v7.widget.an.v != null) {
            try {
                String v1_0 = this.c;
                Object[] v2_1 = new Object[1];
                v2_1[0] = Boolean.valueOf(1);
                android.support.v7.widget.an.v.invoke(v1_0, v2_1);
            } catch (String v0) {
                android.util.Log.i("ListPopupWindow", "Could not call setClipToScreenEnabled() on PopupWindow. Oh well.");
            }
        }
        return;
    }

    static synthetic android.support.v7.widget.ar a(android.support.v7.widget.an p1)
    {
        return p1.d;
    }

    private void a()
    {
        this.k = 0;
        return;
    }

    private void a(android.view.View p1)
    {
        this.l = p1;
        return;
    }

    private void a(android.widget.AdapterView$OnItemClickListener p1)
    {
        this.m = p1;
        return;
    }

    private void a(android.widget.AdapterView$OnItemSelectedListener p1)
    {
        this.E = p1;
        return;
    }

    private void a(boolean p1)
    {
        this.A = p1;
        return;
    }

    private boolean a(int p11, android.view.KeyEvent p12)
    {
        int v1 = 1;
        if ((!this.c.isShowing()) || ((p11 == 62) || ((this.d.getSelectedItemPosition() < 0) && (android.support.v7.widget.an.l(p11))))) {
            v1 = 0;
        } else {
            android.support.v7.widget.ar v0_9;
            int v5 = this.d.getSelectedItemPosition();
            if (this.c.isAboveAnchor()) {
                v0_9 = 0;
            } else {
                v0_9 = 1;
            }
            int v6_0 = this.x;
            int v3_0 = 2147483647;
            int v4_0 = -2147483648;
            if (v6_0 != 0) {
                int v4_2;
                int v3_1 = v6_0.areAllItemsEnabled();
                if (v3_1 == 0) {
                    v4_2 = this.d.a(0, 1);
                } else {
                    v4_2 = 0;
                }
                int v3_3;
                if (v3_1 == 0) {
                    v3_3 = this.d.a((v6_0.getCount() - 1), 0);
                } else {
                    v3_3 = (v6_0.getCount() - 1);
                }
                v4_0 = v3_3;
                v3_0 = v4_2;
            }
            if (((v0_9 == null) || ((p11 != 19) || (v5 > v3_0))) && ((v0_9 != null) || ((p11 != 20) || (v5 < v4_0)))) {
                android.support.v7.widget.ar.a(this.d, 0);
                if (!this.d.onKeyDown(p11, p12)) {
                    if ((v0_9 == null) || (p11 != 20)) {
                        if ((v0_9 != null) || ((p11 != 19) || (v5 != v3_0))) {
                        }
                    } else {
                        if (v5 != v4_0) {
                        }
                    }
                } else {
                    this.c.setInputMethodMode(2);
                    this.d.requestFocusFromTouch();
                    this.b();
                    switch (p11) {
                        case 19:
                        case 20:
                        case 23:
                        case 66:
                            break;
                        case 19:
                        case 20:
                        case 23:
                        case 66:
                            break;
                        case 19:
                        case 20:
                        case 23:
                        case 66:
                            break;
                        case 19:
                        case 20:
                        case 23:
                        case 66:
                            break;
                        default:
                    }
                }
            } else {
                this.f();
                this.c.setInputMethodMode(1);
                this.b();
            }
        }
        return v1;
    }

    static synthetic android.widget.PopupWindow b(android.support.v7.widget.an p1)
    {
        return p1.c;
    }

    private void b(int p2)
    {
        this.c.setSoftInputMode(p2);
        return;
    }

    private void b(android.graphics.drawable.Drawable p1)
    {
        this.D = p1;
        return;
    }

    private void b(android.view.View p2)
    {
        boolean v0_1 = this.c.isShowing();
        if (v0_1) {
            this.t();
        }
        this.B = p2;
        if (v0_1) {
            this.b();
        }
        return;
    }

    private void b(boolean p1)
    {
        this.z = p1;
        return;
    }

    private boolean b(int p3, android.view.KeyEvent p4)
    {
        if ((!this.c.isShowing()) || (this.d.getSelectedItemPosition() < 0)) {
            boolean v0_4 = 0;
        } else {
            v0_4 = this.d.onKeyUp(p3, p4);
            if ((v0_4) && (android.support.v7.widget.an.l(p3))) {
                this.d();
            }
        }
        return v0_4;
    }

    static synthetic android.support.v7.widget.az c(android.support.v7.widget.an p1)
    {
        return p1.F;
    }

    private android.view.View$OnTouchListener c(android.view.View p2)
    {
        return new android.support.v7.widget.ao(this, p2);
    }

    private void c(int p2)
    {
        this.c.setAnimationStyle(p2);
        return;
    }

    private boolean c(int p4, android.view.KeyEvent p5)
    {
        int v0 = 1;
        if ((p4 != 4) || (!this.c.isShowing())) {
            v0 = 0;
        } else {
            boolean v1_3 = this.l;
            if ((p5.getAction() != 0) || (p5.getRepeatCount() != 0)) {
                if (p5.getAction() != 1) {
                } else {
                    boolean v1_4 = v1_3.getKeyDispatcherState();
                    if (v1_4) {
                        v1_4.handleUpEvent(p5);
                    }
                    if ((!p5.isTracking()) || (p5.isCanceled())) {
                    } else {
                        this.d();
                    }
                }
            } else {
                boolean v1_7 = v1_3.getKeyDispatcherState();
                if (v1_7) {
                    v1_7.startTracking(p5, this);
                }
            }
        }
        return v0;
    }

    static synthetic android.os.Handler d(android.support.v7.widget.an p1)
    {
        return p1.K;
    }

    private void d(int p1)
    {
        this.f = p1;
        return;
    }

    private void e(int p2)
    {
        this.g = p2;
        this.h = 1;
        return;
    }

    private void f(int p1)
    {
        this.i = p1;
        return;
    }

    private void g(int p1)
    {
        this.e = p1;
        return;
    }

    private int h()
    {
        return this.k;
    }

    private void h(int p1)
    {
        this.y = p1;
        return;
    }

    private void i(int p4)
    {
        android.support.v7.widget.ar v0 = this.d;
        if ((this.c.isShowing()) && (v0 != null)) {
            android.support.v7.widget.ar.a(v0, 0);
            v0.setSelection(p4);
            if ((android.os.Build$VERSION.SDK_INT >= 11) && (v0.getChoiceMode() != 0)) {
                v0.setItemChecked(p4, 1);
            }
        }
        return;
    }

    private boolean i()
    {
        return this.M;
    }

    private boolean j()
    {
        return this.z;
    }

    private boolean j(int p7)
    {
        int v0_2;
        if (!this.c.isShowing()) {
            v0_2 = 0;
        } else {
            if (this.m != null) {
                android.support.v7.widget.ar v1 = this.d;
                this.m.onItemClick(v1, v1.getChildAt((p7 - v1.getFirstVisiblePosition())), p7, v1.getAdapter().getItemId(p7));
            }
            v0_2 = 1;
        }
        return v0_2;
    }

    private int k()
    {
        return this.c.getSoftInputMode();
    }

    private void k(int p1)
    {
        this.j = p1;
        return;
    }

    private android.graphics.drawable.Drawable l()
    {
        return this.c.getBackground();
    }

    private static boolean l(int p1)
    {
        if ((p1 != 66) && (p1 != 23)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private int m()
    {
        return this.c.getAnimationStyle();
    }

    private android.view.View n()
    {
        return this.l;
    }

    private int o()
    {
        return this.f;
    }

    private int p()
    {
        int v0_1;
        if (this.h) {
            v0_1 = this.g;
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private int q()
    {
        return this.e;
    }

    private int r()
    {
        return this.y;
    }

    private void s()
    {
        this.K.post(this.J);
        return;
    }

    private void t()
    {
        if (this.B != null) {
            android.view.ViewGroup v0_2 = this.B.getParent();
            if ((v0_2 instanceof android.view.ViewGroup)) {
                ((android.view.ViewGroup) v0_2).removeView(this.B);
            }
        }
        return;
    }

    private int u()
    {
        return this.c.getInputMethodMode();
    }

    private boolean v()
    {
        return this.c.isShowing();
    }

    private Object w()
    {
        Object v0_3;
        if (this.c.isShowing()) {
            v0_3 = this.d.getSelectedItem();
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private int x()
    {
        int v0_3;
        if (this.c.isShowing()) {
            v0_3 = this.d.getSelectedItemPosition();
        } else {
            v0_3 = -1;
        }
        return v0_3;
    }

    private long y()
    {
        long v0_3;
        if (this.c.isShowing()) {
            v0_3 = this.d.getSelectedItemId();
        } else {
            v0_3 = -0.0;
        }
        return v0_3;
    }

    private android.view.View z()
    {
        android.view.View v0_3;
        if (this.c.isShowing()) {
            v0_3 = this.d.getSelectedView();
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final void a(int p3)
    {
        int v0_1 = this.c.getBackground();
        if (v0_1 == 0) {
            this.e = p3;
        } else {
            v0_1.getPadding(this.L);
            this.e = ((this.L.left + this.L.right) + p3);
        }
        return;
    }

    public final void a(android.graphics.drawable.Drawable p2)
    {
        this.c.setBackgroundDrawable(p2);
        return;
    }

    public void a(android.widget.ListAdapter p3)
    {
        if (this.C != null) {
            if (this.x != null) {
                this.x.unregisterDataSetObserver(this.C);
            }
        } else {
            this.C = new android.support.v7.widget.aw(this, 0);
        }
        this.x = p3;
        if (this.x != null) {
            p3.registerDataSetObserver(this.C);
        }
        if (this.d != null) {
            this.d.setAdapter(this.x);
        }
        return;
    }

    public final void a(android.widget.PopupWindow$OnDismissListener p2)
    {
        this.c.setOnDismissListener(p2);
        return;
    }

    public void b()
    {
        android.os.Handler v0_2;
        android.support.v7.widget.av v1_0 = 1;
        int v5_0 = -1;
        if (this.d != null) {
            this.c.getContentView();
            int v3_0 = this.B;
            if (v3_0 == 0) {
                v0_2 = 0;
            } else {
                android.os.Handler v0_4 = ((android.widget.LinearLayout$LayoutParams) v3_0.getLayoutParams());
                v0_2 = (v0_4.bottomMargin + (v3_0.getMeasuredHeight() + v0_4.topMargin));
            }
        } else {
            android.os.Handler v0_9;
            int v4_1 = this.w;
            this.J = new android.support.v7.widget.ap(this);
            if (this.M) {
                v0_9 = 0;
            } else {
                v0_9 = 1;
            }
            this.d = new android.support.v7.widget.ar(v4_1, v0_9);
            if (this.D != null) {
                this.d.setSelector(this.D);
            }
            this.d.setAdapter(this.x);
            this.d.setOnItemClickListener(this.m);
            this.d.setFocusable(1);
            this.d.setFocusableInTouchMode(1);
            this.d.setOnItemSelectedListener(new android.support.v7.widget.aq(this));
            this.d.setOnScrollListener(this.H);
            if (this.E != null) {
                this.d.setOnItemSelectedListener(this.E);
            }
            int v3_12;
            android.os.Handler v0_20 = this.d;
            android.widget.PopupWindow v6_0 = this.B;
            if (v6_0 == null) {
                v3_12 = v0_20;
                v0_2 = 0;
            } else {
                v3_12 = new android.widget.LinearLayout(v4_1);
                v3_12.setOrientation(1);
                int v4_3 = new android.widget.LinearLayout$LayoutParams(-1, 0, 1065353216);
                switch (this.k) {
                    case 0:
                        v3_12.addView(v6_0);
                        v3_12.addView(v0_20, v4_3);
                        break;
                    case 1:
                        v3_12.addView(v0_20, v4_3);
                        v3_12.addView(v6_0);
                        break;
                    default:
                        android.util.Log.e("ListPopupWindow", new StringBuilder("Invalid hint position ").append(this.k).toString());
                }
                v6_0.measure(android.view.View$MeasureSpec.makeMeasureSpec(this.e, -2147483648), 0);
                android.os.Handler v0_25 = ((android.widget.LinearLayout$LayoutParams) v6_0.getLayoutParams());
                v0_2 = (v0_25.bottomMargin + (v6_0.getMeasuredHeight() + v0_25.topMargin));
            }
            this.c.setContentView(v3_12);
        }
        int v3_17;
        int v3_15 = this.c.getBackground();
        if (v3_15 == 0) {
            this.L.setEmpty();
            v3_17 = 0;
        } else {
            v3_15.getPadding(this.L);
            v3_17 = (this.L.top + this.L.bottom);
            if (!this.h) {
                this.g = (- this.L.top);
            }
        }
        android.os.Handler v0_27;
        this.c.getInputMethodMode();
        android.widget.PopupWindow v6_3 = this.c.getMaxAvailableHeight(this.l, this.g);
        if ((!this.z) && (this.y != -1)) {
            int v4_28;
            switch (this.e) {
                case -2:
                    v4_28 = android.view.View$MeasureSpec.makeMeasureSpec((this.w.getResources().getDisplayMetrics().widthPixels - (this.L.left + this.L.right)), -2147483648);
                    break;
                case -1:
                    v4_28 = android.view.View$MeasureSpec.makeMeasureSpec((this.w.getResources().getDisplayMetrics().widthPixels - (this.L.left + this.L.right)), 1073741824);
                    break;
                default:
                    v4_28 = android.view.View$MeasureSpec.makeMeasureSpec(this.e, 1073741824);
            }
            int v4_35 = this.d.a(v4_28, (v6_3 - v0_2));
            if (v4_35 > 0) {
                v0_2 += v3_17;
            }
            v0_27 = (v0_2 + v4_35);
        } else {
            v0_27 = (v6_3 + v3_17);
        }
        android.widget.PopupWindow v6_5 = this.g();
        if (!this.c.isShowing()) {
            int v3_25;
            if (this.e != -1) {
                if (this.e != -2) {
                    this.c.setWidth(this.e);
                    v3_25 = 0;
                } else {
                    this.c.setWidth(this.l.getWidth());
                    v3_25 = 0;
                }
            } else {
                v3_25 = -1;
            }
            android.os.Handler v0_29;
            if (this.y != -1) {
                if (this.y != -2) {
                    this.c.setHeight(this.y);
                    v0_29 = 0;
                } else {
                    this.c.setHeight(v0_27);
                    v0_29 = 0;
                }
            } else {
                v0_29 = -1;
            }
            this.c.setWindowLayoutMode(v3_25, v0_29);
            if (android.support.v7.widget.an.v != null) {
                try {
                    int v3_27 = this.c;
                    int v4_45 = new Object[1];
                    v4_45[0] = Boolean.valueOf(1);
                    android.support.v7.widget.an.v.invoke(v3_27, v4_45);
                } catch (android.os.Handler v0) {
                    android.util.Log.i("ListPopupWindow", "Could not call setClipToScreenEnabled() on PopupWindow. Oh well.");
                }
            }
            if ((this.A) || (this.z)) {
                v1_0 = 0;
            }
            this.c.setOutsideTouchable(v1_0);
            this.c.setTouchInterceptor(this.G);
            android.support.v4.widget.bo.a(this.c, this.l, this.f, this.g, this.i);
            this.d.setSelection(-1);
            if ((!this.M) || (this.d.isInTouchMode())) {
                this.f();
            }
            if (!this.M) {
                this.K.post(this.I);
            }
        } else {
            int v4_47;
            if (this.e != -1) {
                if (this.e != -2) {
                    v4_47 = this.e;
                } else {
                    v4_47 = this.l.getWidth();
                }
            } else {
                v4_47 = -1;
            }
            int v5_1;
            if (this.y != -1) {
                if (this.y != -2) {
                    v5_1 = this.y;
                } else {
                    v5_1 = v0_27;
                }
            } else {
                int v3_37;
                if (v6_5 == null) {
                    v3_37 = -1;
                } else {
                    v3_37 = v0_27;
                }
                if (v6_5 == null) {
                    android.os.Handler v0_43;
                    if (this.e != -1) {
                        v0_43 = 0;
                    } else {
                        v0_43 = -1;
                    }
                    this.c.setWindowLayoutMode(v0_43, -1);
                    v5_1 = v3_37;
                } else {
                    if (this.e != -1) {
                        v5_0 = 0;
                    }
                    this.c.setWindowLayoutMode(v5_0, 0);
                    v5_1 = v3_37;
                }
            }
            if ((this.A) || (this.z)) {
                v1_0 = 0;
            }
            this.c.setOutsideTouchable(v1_0);
            this.c.update(this.l, this.f, this.g, v4_47, v5_1);
        }
        return;
    }

    public final void c()
    {
        this.M = 1;
        this.c.setFocusable(1);
        return;
    }

    public final void d()
    {
        this.c.dismiss();
        this.t();
        this.c.setContentView(0);
        this.d = 0;
        this.K.removeCallbacks(this.F);
        return;
    }

    public final void e()
    {
        this.c.setInputMethodMode(2);
        return;
    }

    public final void f()
    {
        android.support.v7.widget.ar v0 = this.d;
        if (v0 != null) {
            android.support.v7.widget.ar.a(v0, 1);
            v0.requestLayout();
        }
        return;
    }

    public final boolean g()
    {
        int v0_2;
        if (this.c.getInputMethodMode() != 2) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }
}
