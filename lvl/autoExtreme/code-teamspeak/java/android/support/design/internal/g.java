package android.support.design.internal;
final class g implements android.support.v4.view.bx {
    final synthetic android.support.design.internal.f a;

    g(android.support.design.internal.f p1)
    {
        this.a = p1;
        return;
    }

    public final android.support.v4.view.gh a(android.view.View p6, android.support.v4.view.gh p7)
    {
        if (android.support.design.internal.f.a(this.a) == null) {
            android.support.design.internal.f.a(this.a, new android.graphics.Rect());
        }
        android.support.v4.view.gh v0_10;
        android.support.design.internal.f.a(this.a).set(p7.a(), p7.b(), p7.c(), p7.d());
        if ((!android.support.design.internal.f.a(this.a).isEmpty()) && (android.support.design.internal.f.b(this.a) != null)) {
            v0_10 = 0;
        } else {
            v0_10 = 1;
        }
        this.a.setWillNotDraw(v0_10);
        android.support.v4.view.cx.b(this.a);
        return p7.i();
    }
}
