package android.support.design.widget;
public class ce extends android.widget.LinearLayout {
    private static final int a = 200;
    private android.widget.EditText b;
    private CharSequence c;
    private android.graphics.Paint d;
    private boolean e;
    private android.widget.TextView f;
    private int g;
    private android.content.res.ColorStateList h;
    private android.content.res.ColorStateList i;
    private final android.support.design.widget.l j;
    private boolean k;
    private android.support.design.widget.ck l;

    private ce(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private ce(android.content.Context p2, byte p3)
    {
        this(p2, 0);
        return;
    }

    private ce(android.content.Context p7, char p8)
    {
        this(p7, 0);
        this.j = new android.support.design.widget.l(this);
        this.setOrientation(1);
        this.setWillNotDraw(0);
        this.setAddStatesFromChildren(1);
        this.j.a(android.support.design.widget.a.b);
        android.support.design.widget.cj v0_3 = this.j;
        v0_3.i = new android.view.animation.AccelerateInterpolator();
        v0_3.a();
        this.j.d(8388659);
        android.support.design.widget.cj v0_6 = p7.obtainStyledAttributes(0, android.support.design.n.TextInputLayout, 0, android.support.design.m.Widget_Design_TextInputLayout);
        this.c = v0_6.getText(android.support.design.n.TextInputLayout_android_hint);
        this.k = v0_6.getBoolean(android.support.design.n.TextInputLayout_hintAnimationEnabled, 1);
        if (v0_6.hasValue(android.support.design.n.TextInputLayout_android_textColorHint)) {
            boolean v1_12 = v0_6.getColorStateList(android.support.design.n.TextInputLayout_android_textColorHint);
            this.i = v1_12;
            this.h = v1_12;
        }
        if (v0_6.getResourceId(android.support.design.n.TextInputLayout_hintTextAppearance, -1) != -1) {
            this.setHintTextAppearance(v0_6.getResourceId(android.support.design.n.TextInputLayout_hintTextAppearance, 0));
        }
        this.g = v0_6.getResourceId(android.support.design.n.TextInputLayout_errorTextAppearance, 0);
        boolean v1_20 = v0_6.getBoolean(android.support.design.n.TextInputLayout_errorEnabled, 0);
        v0_6.recycle();
        this.setErrorEnabled(v1_20);
        if (android.support.v4.view.cx.c(this) == 0) {
            android.support.v4.view.cx.c(this, 1);
        }
        android.support.v4.view.cx.a(this, new android.support.design.widget.cj(this, 0));
        return;
    }

    private int a(int p4)
    {
        int v0_2;
        int v0_1 = new android.util.TypedValue();
        if (!this.getContext().getTheme().resolveAttribute(p4, v0_1, 1)) {
            v0_2 = -65281;
        } else {
            v0_2 = v0_1.data;
        }
        return v0_2;
    }

    private android.widget.LinearLayout$LayoutParams a(android.view.ViewGroup$LayoutParams p3)
    {
        android.widget.LinearLayout$LayoutParams v3_1;
        if (!(p3 instanceof android.widget.LinearLayout$LayoutParams)) {
            v3_1 = new android.widget.LinearLayout$LayoutParams(p3);
        } else {
            v3_1 = ((android.widget.LinearLayout$LayoutParams) p3);
        }
        if (this.d == null) {
            this.d = new android.graphics.Paint();
        }
        this.d.setTypeface(this.j.h.getTypeface());
        this.d.setTextSize(this.j.e);
        v3_1.topMargin = ((int) (- this.d.ascent()));
        return v3_1;
    }

    private void a(float p3)
    {
        if (this.j.a != p3) {
            if (this.l == null) {
                this.l = android.support.design.widget.dh.a();
                this.l.a(android.support.design.widget.a.a);
                this.l.a(200);
                this.l.a(new android.support.design.widget.ci(this));
            }
            this.l.a(this.j.a, p3);
            this.l.a.a();
        }
        return;
    }

    static synthetic void a(android.support.design.widget.ce p1)
    {
        p1.a(1);
        return;
    }

    private void a(boolean p11)
    {
        android.support.design.widget.l v0_4;
        int v1 = 1;
        if ((this.b == null) || (android.text.TextUtils.isEmpty(this.b.getText()))) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        int[] v4 = this.getDrawableState();
        android.support.design.widget.l v3_0 = 0;
        while (v3_0 < v4.length) {
            if (v4[v3_0] != 16842908) {
                v3_0++;
            }
            if ((this.h != null) && (this.i != null)) {
                int v2_5;
                this.j.b(this.h.getDefaultColor());
                android.support.design.widget.l v3_3 = this.j;
                if (v1 == 0) {
                    v2_5 = this.h.getDefaultColor();
                } else {
                    v2_5 = this.i.getDefaultColor();
                }
                v3_3.a(v2_5);
            }
            if ((v0_4 == null) && (v1 == 0)) {
                if ((this.l != null) && (this.l.a.b())) {
                    this.l.a.e();
                }
                if ((!p11) || (!this.k)) {
                    this.j.a(0);
                } else {
                    this.a(0);
                }
            } else {
                if ((this.l != null) && (this.l.a.b())) {
                    this.l.a.e();
                }
                if ((!p11) || (!this.k)) {
                    this.j.a(1065353216);
                } else {
                    this.a(1065353216);
                }
            }
            return;
        }
        v1 = 0;
    }

    private boolean a()
    {
        return this.e;
    }

    private static boolean a(int[] p5)
    {
        int v0 = 0;
        int v1 = 0;
        while (v1 < p5.length) {
            if (p5[v1] != 16842908) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    static synthetic android.support.design.widget.l b(android.support.design.widget.ce p1)
    {
        return p1.j;
    }

    private void b(boolean p3)
    {
        if ((this.l != null) && (this.l.a.b())) {
            this.l.a.e();
        }
        if ((!p3) || (!this.k)) {
            this.j.a(1065353216);
        } else {
            this.a(1065353216);
        }
        return;
    }

    private boolean b()
    {
        return this.k;
    }

    static synthetic android.widget.EditText c(android.support.design.widget.ce p1)
    {
        return p1.b;
    }

    private void c(boolean p3)
    {
        if ((this.l != null) && (this.l.a.b())) {
            this.l.a.e();
        }
        if ((!p3) || (!this.k)) {
            this.j.a(0);
        } else {
            this.a(0);
        }
        return;
    }

    static synthetic android.widget.TextView d(android.support.design.widget.ce p1)
    {
        return p1.f;
    }

    private void setEditText(android.widget.EditText p6)
    {
        if (this.b == null) {
            this.b = p6;
            this.j.a(this.b.getTypeface());
            android.widget.TextView v0_2 = this.j;
            int v1_3 = this.b.getTextSize();
            if (v0_2.d != v1_3) {
                v0_2.d = v1_3;
                v0_2.a();
            }
            this.j.c(this.b.getGravity());
            this.b.addTextChangedListener(new android.support.design.widget.cf(this));
            if (this.h == null) {
                this.h = this.b.getHintTextColors();
            }
            if (android.text.TextUtils.isEmpty(this.c)) {
                this.setHint(this.b.getHint());
                this.b.setHint(0);
            }
            if (this.f != null) {
                android.support.v4.view.cx.b(this.f, android.support.v4.view.cx.k(this.b), 0, android.support.v4.view.cx.l(this.b), this.b.getPaddingBottom());
            }
            this.a(0);
            return;
        } else {
            throw new IllegalArgumentException("We already have an EditText, can only have one");
        }
    }

    public void addView(android.view.View p3, int p4, android.view.ViewGroup$LayoutParams p5)
    {
        if (!(p3 instanceof android.widget.EditText)) {
            super.addView(p3, p4, p5);
        } else {
            this.setEditText(((android.widget.EditText) p3));
            super.addView(p3, 0, this.a(p5));
        }
        return;
    }

    public void draw(android.graphics.Canvas p2)
    {
        super.draw(p2);
        this.j.a(p2);
        return;
    }

    public android.widget.EditText getEditText()
    {
        return this.b;
    }

    public CharSequence getError()
    {
        if ((!this.e) || ((this.f == null) || (this.f.getVisibility() != 0))) {
            CharSequence v0_4 = 0;
        } else {
            v0_4 = this.f.getText();
        }
        return v0_4;
    }

    public CharSequence getHint()
    {
        return this.c;
    }

    protected void onLayout(boolean p7, int p8, int p9, int p10, int p11)
    {
        this = super.onLayout(p7, p8, p9, p10, p11);
        if (this.b != null) {
            android.support.design.widget.l v0_3 = (this.b.getLeft() + this.b.getCompoundPaddingLeft());
            int v1_4 = (this.b.getRight() - this.b.getCompoundPaddingRight());
            this.j.a(v0_3, (this.b.getTop() + this.b.getCompoundPaddingTop()), v1_4, (this.b.getBottom() - this.b.getCompoundPaddingBottom()));
            this.j.b(v0_3, this.getPaddingTop(), v1_4, ((p11 - p9) - this.getPaddingBottom()));
            this.j.a();
        }
        return;
    }

    public void refreshDrawableState()
    {
        super.refreshDrawableState();
        this.a(android.support.v4.view.cx.B(this));
        return;
    }

    public void setError(CharSequence p5)
    {
        if (this.e) {
            if (android.text.TextUtils.isEmpty(p5)) {
                if (this.f.getVisibility() == 0) {
                    android.support.v4.view.cx.p(this.f).a(0).a(200).a(android.support.design.widget.a.b).a(new android.support.design.widget.ch(this)).b();
                    android.support.v4.view.cx.a(this.b, android.support.v7.internal.widget.av.a(this.getContext()).a(android.support.design.h.abc_edit_text_material));
                }
            } else {
                android.support.v4.view.cx.c(this.f, 0);
                this.f.setText(p5);
                android.support.v4.view.cx.p(this.f).a(1065353216).a(200).a(android.support.design.widget.a.b).a(new android.support.design.widget.cg(this)).b();
                android.support.v4.view.cx.a(this.b, android.content.res.ColorStateList.valueOf(this.f.getCurrentTextColor()));
            }
            this.sendAccessibilityEvent(2048);
        } else {
            if (!android.text.TextUtils.isEmpty(p5)) {
                this.setErrorEnabled(1);
            }
        }
        return;
    }

    public void setErrorEnabled(boolean p6)
    {
        if (this.e != p6) {
            if (this.f != null) {
                android.support.v4.view.cx.p(this.f).a();
            }
            if (!p6) {
                this.removeView(this.f);
                this.f = 0;
            } else {
                this.f = new android.widget.TextView(this.getContext());
                this.f.setTextAppearance(this.getContext(), this.g);
                this.f.setVisibility(4);
                this.addView(this.f);
                if (this.b != null) {
                    android.support.v4.view.cx.b(this.f, android.support.v4.view.cx.k(this.b), 0, android.support.v4.view.cx.l(this.b), this.b.getPaddingBottom());
                }
            }
            this.e = p6;
        }
        return;
    }

    public void setHint(CharSequence p2)
    {
        this.c = p2;
        this.j.a(p2);
        this.sendAccessibilityEvent(2048);
        return;
    }

    public void setHintAnimationEnabled(boolean p1)
    {
        this.k = p1;
        return;
    }

    public void setHintTextAppearance(int p3)
    {
        this.j.e(p3);
        this.i = android.content.res.ColorStateList.valueOf(this.j.f);
        if (this.b != null) {
            this.a(0);
            this.b.setLayoutParams(this.a(this.b.getLayoutParams()));
            this.b.requestLayout();
        }
        return;
    }

    public void setTypeface(android.graphics.Typeface p2)
    {
        this.j.a(p2);
        return;
    }
}
