package android.support.design.widget;
final class bl {
    final java.util.ArrayList a;
    android.support.design.widget.bn b;
    android.view.animation.Animation c;
    ref.WeakReference d;
    private android.view.animation.Animation$AnimationListener e;

    bl()
    {
        this.a = new java.util.ArrayList();
        this.b = 0;
        this.c = 0;
        this.e = new android.support.design.widget.bm(this);
        return;
    }

    private static synthetic android.view.animation.Animation a(android.support.design.widget.bl p1)
    {
        return p1.c;
    }

    private void a(android.support.design.widget.bn p3)
    {
        this.c = p3.b;
        android.view.View v0_1 = this.a();
        if (v0_1 != null) {
            v0_1.startAnimation(this.c);
        }
        return;
    }

    private void a(android.view.View p7)
    {
        int v0_0 = this.a();
        if (v0_0 != p7) {
            if (v0_0 != 0) {
                android.view.View v2 = this.a();
                int v3 = this.a.size();
                int v1 = 0;
                while (v1 < v3) {
                    if (v2.getAnimation() == ((android.support.design.widget.bn) this.a.get(v1)).b) {
                        v2.clearAnimation();
                    }
                    v1++;
                }
                this.d = 0;
                this.b = 0;
                this.c = 0;
            }
            if (p7 != null) {
                this.d = new ref.WeakReference(p7);
            }
        }
        return;
    }

    private void a(int[] p6)
    {
        android.view.animation.Animation v3_0 = this.a.size();
        android.view.View v2_0 = 0;
        while (v2_0 < v3_0) {
            android.view.View v0_2 = ((android.support.design.widget.bn) this.a.get(v2_0));
            if (!android.util.StateSet.stateSetMatches(v0_2.a, p6)) {
                v2_0++;
            }
            if (v0_2 != this.b) {
                if ((this.b != null) && (this.c != null)) {
                    android.view.View v2_4 = this.a();
                    if ((v2_4 != null) && (v2_4.getAnimation() == this.c)) {
                        v2_4.clearAnimation();
                    }
                    this.c = 0;
                }
                this.b = v0_2;
                if (v0_2 != null) {
                    this.c = v0_2.b;
                    android.view.View v0_7 = this.a();
                    if (v0_7 != null) {
                        v0_7.startAnimation(this.c);
                    }
                }
            }
            return;
        }
        v0_2 = 0;
    }

    private android.view.animation.Animation b()
    {
        return this.c;
    }

    private static synthetic android.view.animation.Animation b(android.support.design.widget.bl p1)
    {
        p1.c = 0;
        return 0;
    }

    private void c()
    {
        android.view.View v2 = this.a();
        int v3 = this.a.size();
        int v1 = 0;
        while (v1 < v3) {
            if (v2.getAnimation() == ((android.support.design.widget.bn) this.a.get(v1)).b) {
                v2.clearAnimation();
            }
            v1++;
        }
        this.d = 0;
        this.b = 0;
        this.c = 0;
        return;
    }

    private void d()
    {
        if (this.c != null) {
            int v0_1 = this.a();
            if ((v0_1 != 0) && (v0_1.getAnimation() == this.c)) {
                v0_1.clearAnimation();
            }
            this.c = 0;
        }
        return;
    }

    private java.util.ArrayList e()
    {
        return this.a;
    }

    private void f()
    {
        if (this.c != null) {
            android.view.View v0_1 = this.a();
            if ((v0_1 != null) && (v0_1.getAnimation() == this.c)) {
                v0_1.clearAnimation();
            }
        }
        return;
    }

    final android.view.View a()
    {
        android.view.View v0_3;
        if (this.d != null) {
            v0_3 = ((android.view.View) this.d.get());
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final void a(int[] p3, android.view.animation.Animation p4)
    {
        android.support.design.widget.bn v0_1 = new android.support.design.widget.bn(p3, p4, 0);
        p4.setAnimationListener(this.e);
        this.a.add(v0_1);
        return;
    }
}
