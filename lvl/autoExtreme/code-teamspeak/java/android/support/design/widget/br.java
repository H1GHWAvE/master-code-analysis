package android.support.design.widget;
public final class br extends android.widget.HorizontalScrollView {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 0;
    public static final int d = 1;
    private static final int e = 48;
    private static final int f = 56;
    private static final int g = 16;
    private static final int h = 24;
    private static final int i = 300;
    private android.view.View$OnClickListener A;
    private android.support.design.widget.ck B;
    private android.support.design.widget.ck C;
    private final java.util.ArrayList j;
    private android.support.design.widget.bz k;
    private final android.support.design.widget.bw l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private android.content.res.ColorStateList r;
    private final int s;
    private final int t;
    private int u;
    private final int v;
    private int w;
    private int x;
    private int y;
    private android.support.design.widget.bv z;

    private br(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private br(android.content.Context p2, byte p3)
    {
        this(p2, 0);
        return;
    }

    private br(android.content.Context p7, char p8)
    {
        this(p7, 0, 0);
        this.j = new java.util.ArrayList();
        this.u = 2147483647;
        this.setHorizontalScrollBarEnabled(0);
        this.setFillViewport(1);
        this.l = new android.support.design.widget.bw(this, p7);
        this.addView(this.l, -2, -1);
        android.content.res.TypedArray v0_7 = p7.obtainStyledAttributes(0, android.support.design.n.TabLayout, 0, android.support.design.m.Widget_Design_TabLayout);
        this.l.b(v0_7.getDimensionPixelSize(android.support.design.n.TabLayout_tabIndicatorHeight, 0));
        this.l.a(v0_7.getColor(android.support.design.n.TabLayout_tabIndicatorColor, 0));
        this.q = v0_7.getResourceId(android.support.design.n.TabLayout_tabTextAppearance, android.support.design.m.TextAppearance_Design_Tab);
        int v1_7 = v0_7.getDimensionPixelSize(android.support.design.n.TabLayout_tabPadding, 0);
        this.p = v1_7;
        this.o = v1_7;
        this.n = v1_7;
        this.m = v1_7;
        this.m = v0_7.getDimensionPixelSize(android.support.design.n.TabLayout_tabPaddingStart, this.m);
        this.n = v0_7.getDimensionPixelSize(android.support.design.n.TabLayout_tabPaddingTop, this.n);
        this.o = v0_7.getDimensionPixelSize(android.support.design.n.TabLayout_tabPaddingEnd, this.o);
        this.p = v0_7.getDimensionPixelSize(android.support.design.n.TabLayout_tabPaddingBottom, this.p);
        this.r = this.g(this.q);
        if (v0_7.hasValue(android.support.design.n.TabLayout_tabTextColor)) {
            this.r = v0_7.getColorStateList(android.support.design.n.TabLayout_tabTextColor);
        }
        if (v0_7.hasValue(android.support.design.n.TabLayout_tabSelectedTextColor)) {
            this.r = android.support.design.widget.br.b(this.r.getDefaultColor(), v0_7.getColor(android.support.design.n.TabLayout_tabSelectedTextColor, 0));
        }
        this.t = v0_7.getDimensionPixelSize(android.support.design.n.TabLayout_tabMinWidth, 0);
        this.v = v0_7.getDimensionPixelSize(android.support.design.n.TabLayout_tabMaxWidth, 0);
        this.s = v0_7.getResourceId(android.support.design.n.TabLayout_tabBackground, 0);
        this.w = v0_7.getDimensionPixelSize(android.support.design.n.TabLayout_tabContentStart, 0);
        this.y = v0_7.getInt(android.support.design.n.TabLayout_tabMode, 1);
        this.x = v0_7.getInt(android.support.design.n.TabLayout_tabGravity, 0);
        v0_7.recycle();
        this.e();
        return;
    }

    private int a(int p5, float p6)
    {
        int v0_0 = 0;
        if (this.y == 0) {
            int v2_2;
            android.view.View v3 = this.l.getChildAt(p5);
            if ((p5 + 1) >= this.l.getChildCount()) {
                v2_2 = 0;
            } else {
                v2_2 = this.l.getChildAt((p5 + 1));
            }
            int v1_6;
            if (v3 == null) {
                v1_6 = 0;
            } else {
                v1_6 = v3.getWidth();
            }
            if (v2_2 != 0) {
                v0_0 = v2_2.getWidth();
            }
            v0_0 = (((((int) ((((float) (v0_0 + v1_6)) * p6) * 1056964608)) + v3.getLeft()) + (v3.getWidth() / 2)) - (this.getWidth() / 2));
        }
        return v0_0;
    }

    static synthetic int a(android.support.design.widget.br p1)
    {
        return p1.s;
    }

    private android.support.design.widget.bz a()
    {
        return new android.support.design.widget.bz(this);
    }

    static synthetic android.support.design.widget.ck a(android.support.design.widget.br p0, android.support.design.widget.ck p1)
    {
        p0.C = p1;
        return p1;
    }

    private void a(int p2, int p3)
    {
        this.setTabTextColors(android.support.design.widget.br.b(p2, p3));
        return;
    }

    static synthetic void a(android.support.design.widget.br p0, int p1)
    {
        p0.c(p1);
        return;
    }

    private void a(android.support.design.widget.bz p5)
    {
        IllegalArgumentException v0_1 = this.j.isEmpty();
        if (p5.g == this) {
            int v1_1 = this.c(p5);
            this.l.addView(v1_1, this.d());
            if (v0_1 != null) {
                v1_1.setSelected(1);
            }
            this.b(p5, this.j.size());
            if (v0_1 != null) {
                p5.a();
            }
            return;
        } else {
            throw new IllegalArgumentException("Tab belongs to a different TabLayout.");
        }
    }

    private void a(android.support.design.widget.bz p5, int p6)
    {
        IllegalArgumentException v0_1 = this.j.isEmpty();
        if (p5.g == this) {
            android.support.design.widget.cc v1_1 = this.c(p5);
            this.l.addView(v1_1, p6, this.d());
            if (v0_1 != null) {
                v1_1.setSelected(1);
            }
            this.b(p5, p6);
            if (v0_1 != null) {
                p5.a();
            }
            return;
        } else {
            throw new IllegalArgumentException("Tab belongs to a different TabLayout.");
        }
    }

    private void a(android.support.design.widget.bz p4, int p5, boolean p6)
    {
        if (p4.g == this) {
            android.support.design.widget.cc v0_1 = this.c(p4);
            this.l.addView(v0_1, p5, this.d());
            if (p6) {
                v0_1.setSelected(1);
            }
            this.b(p4, p5);
            if (p6) {
                p4.a();
            }
            return;
        } else {
            throw new IllegalArgumentException("Tab belongs to a different TabLayout.");
        }
    }

    private void a(android.widget.LinearLayout$LayoutParams p3)
    {
        if ((this.y != 1) || (this.x != 0)) {
            p3.width = -2;
            p3.weight = 0;
        } else {
            p3.width = 0;
            p3.weight = 1065353216;
        }
        return;
    }

    static synthetic int b(android.support.design.widget.br p1)
    {
        return p1.m;
    }

    static synthetic int b(android.support.design.widget.br p1, int p2)
    {
        return p1.d(p2);
    }

    private static android.content.res.ColorStateList b(int p5, int p6)
    {
        int[][] v0 = new int[][2];
        int[] v1_1 = new int[2];
        v0[0] = android.support.design.widget.br.SELECTED_STATE_SET;
        v1_1[0] = p6;
        v0[1] = android.support.design.widget.br.EMPTY_STATE_SET;
        v1_1[1] = p5;
        return new android.content.res.ColorStateList(v0, v1_1);
    }

    private void b()
    {
        this.l.removeAllViews();
        java.util.Iterator v1 = this.j.iterator();
        while (v1.hasNext()) {
            ((android.support.design.widget.bz) v1.next()).e = -1;
            v1.remove();
        }
        this.k = 0;
        return;
    }

    private void b(int p6)
    {
        int v1_0;
        if (this.k == null) {
            v1_0 = 0;
        } else {
            v1_0 = this.k.e;
        }
        this.l.removeViewAt(p6);
        this.requestLayout();
        android.support.design.widget.bz v0_6 = ((android.support.design.widget.bz) this.j.remove(p6));
        if (v0_6 != null) {
            v0_6.e = -1;
        }
        int v4 = this.j.size();
        int v3_1 = p6;
        while (v3_1 < v4) {
            ((android.support.design.widget.bz) this.j.get(v3_1)).e = v3_1;
            v3_1++;
        }
        if (v1_0 == p6) {
            android.support.design.widget.bz v0_12;
            if (!this.j.isEmpty()) {
                v0_12 = ((android.support.design.widget.bz) this.j.get(Math.max(0, (p6 - 1))));
            } else {
                v0_12 = 0;
            }
            this.a(v0_12, 1);
        }
        return;
    }

    private void b(android.support.design.widget.bz p3)
    {
        if (p3.g == this) {
            this.b(p3.e);
            return;
        } else {
            throw new IllegalArgumentException("Tab does not belong to this TabLayout.");
        }
    }

    private void b(android.support.design.widget.bz p4, int p5)
    {
        p4.e = p5;
        this.j.add(p5, p4);
        int v2 = this.j.size();
        int v1 = (p5 + 1);
        while (v1 < v2) {
            ((android.support.design.widget.bz) this.j.get(v1)).e = v1;
            v1++;
        }
        return;
    }

    private void b(android.support.design.widget.bz p4, int p5, boolean p6)
    {
        android.support.design.widget.cc v0 = this.c(p4);
        this.l.addView(v0, p5, this.d());
        if (p6) {
            v0.setSelected(1);
        }
        return;
    }

    private void b(android.support.design.widget.bz p4, boolean p5)
    {
        if (p4.g == this) {
            int v0_1 = this.c(p4);
            this.l.addView(v0_1, this.d());
            if (p5) {
                v0_1.setSelected(1);
            }
            this.b(p4, this.j.size());
            if (p5) {
                p4.a();
            }
            return;
        } else {
            throw new IllegalArgumentException("Tab belongs to a different TabLayout.");
        }
    }

    static synthetic int c(android.support.design.widget.br p1)
    {
        return p1.n;
    }

    private android.support.design.widget.cc c(android.support.design.widget.bz p3)
    {
        android.support.design.widget.cc v0_1 = new android.support.design.widget.cc(this, this.getContext(), p3);
        v0_1.setFocusable(1);
        if (this.A == null) {
            this.A = new android.support.design.widget.bs(this);
        }
        v0_1.setOnClickListener(this.A);
        return v0_1;
    }

    private void c()
    {
        int v0 = 0;
        int v1_1 = this.l.getChildCount();
        while (v0 < v1_1) {
            this.c(v0);
            v0++;
        }
        return;
    }

    private void c(int p2)
    {
        android.support.design.widget.cc v0_2 = ((android.support.design.widget.cc) this.l.getChildAt(p2));
        if (v0_2 != null) {
            v0_2.a();
        }
        return;
    }

    private void c(android.support.design.widget.bz p4, boolean p5)
    {
        android.support.design.widget.cc v0 = this.c(p4);
        this.l.addView(v0, this.d());
        if (p5) {
            v0.setSelected(1);
        }
        return;
    }

    private int d(int p3)
    {
        return Math.round((this.getResources().getDisplayMetrics().density * ((float) p3)));
    }

    static synthetic int d(android.support.design.widget.br p1)
    {
        return p1.o;
    }

    private android.widget.LinearLayout$LayoutParams d()
    {
        android.widget.LinearLayout$LayoutParams v0_1 = new android.widget.LinearLayout$LayoutParams(-2, -1);
        this.a(v0_1);
        return v0_1;
    }

    private void d(android.support.design.widget.bz p2)
    {
        this.a(p2, 1);
        return;
    }

    static synthetic int e(android.support.design.widget.br p1)
    {
        return p1.p;
    }

    private void e()
    {
        android.support.design.widget.bw v0_1;
        if (this.y != 0) {
            v0_1 = 0;
        } else {
            v0_1 = Math.max(0, (this.w - this.m));
        }
        android.support.v4.view.cx.b(this.l, v0_1, 0, 0, 0);
        switch (this.y) {
            case 0:
                this.l.setGravity(8388611);
                break;
            case 1:
                this.l.setGravity(1);
                break;
        }
        this.f();
        return;
    }

    private void e(int p2)
    {
        this.l.removeViewAt(p2);
        this.requestLayout();
        return;
    }

    static synthetic int f(android.support.design.widget.br p1)
    {
        return p1.t;
    }

    private void f()
    {
        int v1 = 0;
        while (v1 < this.l.getChildCount()) {
            android.view.View v2 = this.l.getChildAt(v1);
            this.a(((android.widget.LinearLayout$LayoutParams) v2.getLayoutParams()));
            v2.requestLayout();
            v1++;
        }
        return;
    }

    private void f(int p10)
    {
        android.support.design.widget.cr v0_0 = 0;
        if (p10 != -1) {
            if ((this.getWindowToken() != null) && (android.support.v4.view.cx.B(this))) {
                int v3_0 = this.l;
                int v4_0 = v3_0.getChildCount();
                android.support.design.widget.cr v1_3 = 0;
                while (v1_3 < v4_0) {
                    if (v3_0.getChildAt(v1_3).getWidth() > 0) {
                        v1_3++;
                    } else {
                        android.support.design.widget.cr v1_4 = 1;
                    }
                    if (v1_4 == null) {
                        android.support.design.widget.cr v1_5 = this.getScrollX();
                        int v3_1 = this.a(p10, 0);
                        if (v1_5 != v3_1) {
                            if (this.B == null) {
                                this.B = android.support.design.widget.dh.a();
                                this.B.a(android.support.design.widget.a.b);
                                this.B.a(300);
                                this.B.a(new android.support.design.widget.bt(this));
                            }
                            this.B.a(v1_5, v3_1);
                            this.B.a.a();
                        }
                        android.support.design.widget.cr v1_8 = this.l;
                        if (android.support.v4.view.cx.f(v1_8) == 1) {
                            v0_0 = 1;
                        }
                        int v2_3;
                        int v4_13;
                        int v4_7 = v1_8.getChildAt(p10);
                        int v3_3 = v4_7.getLeft();
                        int v5_5 = v4_7.getRight();
                        if (Math.abs((p10 - v1_8.a)) > 1) {
                            int v2_2 = v1_8.e.d(24);
                            if (p10 >= v1_8.a) {
                                if (v0_0 != null) {
                                    v4_13 = (v5_5 + v2_2);
                                    v2_3 = v4_13;
                                    if ((v2_3 == v3_3) && (v4_13 == v5_5)) {
                                        return;
                                    } else {
                                        android.support.design.widget.cr v0_1 = v1_8.e;
                                        android.support.design.widget.ck v6 = android.support.design.widget.dh.a();
                                        v0_1.C = v6;
                                        v6.a(android.support.design.widget.a.b);
                                        v6.a(300);
                                        v6.a(0, 1065353216);
                                        v6.a(new android.support.design.widget.bx(v1_8, v2_3, v3_3, v4_13, v5_5));
                                        v6.a.a(new android.support.design.widget.cm(v6, new android.support.design.widget.by(v1_8, p10)));
                                        v6.a.a();
                                        return;
                                    }
                                    this.a(p10, 0, 1);
                                    return;
                                }
                            } else {
                                if (v0_0 == null) {
                                    v4_13 = (v5_5 + v2_2);
                                    v2_3 = v4_13;
                                }
                            }
                            v4_13 = (v3_3 - v2_2);
                            v2_3 = v4_13;
                        } else {
                            v2_3 = v1_8.c;
                            v4_13 = v1_8.d;
                        }
                    }
                }
                v1_4 = 0;
            }
        }
        return;
    }

    static synthetic int g(android.support.design.widget.br p1)
    {
        return p1.u;
    }

    private android.content.res.ColorStateList g(int p3)
    {
        android.content.res.TypedArray v1_1 = this.getContext().obtainStyledAttributes(p3, android.support.design.n.TextAppearance);
        try {
            Throwable v0_2 = v1_1.getColorStateList(android.support.design.n.TextAppearance_android_textColor);
            v1_1.recycle();
            return v0_2;
        } catch (Throwable v0_3) {
            v1_1.recycle();
            throw v0_3;
        }
    }

    private float getScrollPosition()
    {
        float v0_0 = this.l;
        return (v0_0.b + ((float) v0_0.a));
    }

    static synthetic int h(android.support.design.widget.br p1)
    {
        return p1.q;
    }

    static synthetic android.content.res.ColorStateList i(android.support.design.widget.br p1)
    {
        return p1.r;
    }

    static synthetic int j(android.support.design.widget.br p1)
    {
        return p1.y;
    }

    static synthetic int k(android.support.design.widget.br p1)
    {
        return p1.x;
    }

    static synthetic int l(android.support.design.widget.br p1)
    {
        p1.x = 0;
        return 0;
    }

    static synthetic void m(android.support.design.widget.br p0)
    {
        p0.f();
        return;
    }

    private void setSelectedTabView(int p6)
    {
        int v3 = this.l.getChildCount();
        if ((p6 < v3) && (!this.l.getChildAt(p6).isSelected())) {
            int v2 = 0;
            while (v2 < v3) {
                int v0_5;
                android.view.View v4 = this.l.getChildAt(v2);
                if (v2 != p6) {
                    v0_5 = 0;
                } else {
                    v0_5 = 1;
                }
                v4.setSelected(v0_5);
                v2++;
            }
        }
        return;
    }

    public final android.support.design.widget.bz a(int p2)
    {
        return ((android.support.design.widget.bz) this.j.get(p2));
    }

    public final void a(int p3, float p4, boolean p5)
    {
        if (((this.C == null) || (!this.C.a.b())) && ((p3 >= 0) && (p3 < this.l.getChildCount()))) {
            int v0_6 = this.l;
            v0_6.a = p3;
            v0_6.b = p4;
            v0_6.a();
            this.scrollTo(this.a(p3, p4), 0);
            if (p5) {
                this.setSelectedTabView(Math.round((((float) p3) + p4)));
            }
        }
        return;
    }

    final void a(android.support.design.widget.bz p4, boolean p5)
    {
        if (this.k != p4) {
            android.support.design.widget.bv v0_1;
            if (p4 == null) {
                v0_1 = -1;
            } else {
                v0_1 = p4.e;
            }
            this.setSelectedTabView(v0_1);
            if (p5) {
                if (((this.k != null) && (this.k.e != -1)) || (v0_1 == -1)) {
                    this.f(v0_1);
                } else {
                    this.a(v0_1, 0, 1);
                }
            }
            this.k = p4;
            if ((this.k != null) && (this.z != null)) {
                this.z.a(this.k);
            }
        } else {
            if (this.k != null) {
                this.f(p4.e);
            }
        }
        return;
    }

    public final int getSelectedTabPosition()
    {
        int v0_1;
        if (this.k == null) {
            v0_1 = -1;
        } else {
            v0_1 = this.k.e;
        }
        return v0_1;
    }

    public final int getTabCount()
    {
        return this.j.size();
    }

    public final int getTabGravity()
    {
        return this.x;
    }

    public final int getTabMode()
    {
        return this.y;
    }

    public final android.content.res.ColorStateList getTabTextColors()
    {
        return this.r;
    }

    protected final void onMeasure(int p6, int p7)
    {
        int v0_3 = ((this.d(48) + this.getPaddingTop()) + this.getPaddingBottom());
        switch (android.view.View$MeasureSpec.getMode(p7)) {
            case -2147483648:
                p7 = android.view.View$MeasureSpec.makeMeasureSpec(Math.min(v0_3, android.view.View$MeasureSpec.getSize(p7)), 1073741824);
                break;
            case 0:
                p7 = android.view.View$MeasureSpec.makeMeasureSpec(v0_3, 1073741824);
                break;
        }
        super.onMeasure(p6, p7);
        if ((this.y == 1) && (this.getChildCount() == 1)) {
            int v0_8 = this.getChildAt(0);
            int v1_4 = this.getMeasuredWidth();
            if (v0_8.getMeasuredWidth() > v1_4) {
                v0_8.measure(android.view.View$MeasureSpec.makeMeasureSpec(v1_4, 1073741824), android.support.design.widget.br.getChildMeasureSpec(p7, (this.getPaddingTop() + this.getPaddingBottom()), v0_8.getLayoutParams().height));
            }
        }
        int v0_9 = this.v;
        int v1_7 = (this.getMeasuredWidth() - this.d(56));
        if ((v0_9 == 0) || (v0_9 > v1_7)) {
            v0_9 = v1_7;
        }
        if (this.u != v0_9) {
            this.u = v0_9;
            super.onMeasure(p6, p7);
        }
        return;
    }

    public final void setOnTabSelectedListener(android.support.design.widget.bv p1)
    {
        this.z = p1;
        return;
    }

    public final void setSelectedTabIndicatorColor(int p2)
    {
        this.l.a(p2);
        return;
    }

    public final void setSelectedTabIndicatorHeight(int p2)
    {
        this.l.b(p2);
        return;
    }

    public final void setTabGravity(int p2)
    {
        if (this.x != p2) {
            this.x = p2;
            this.e();
        }
        return;
    }

    public final void setTabMode(int p2)
    {
        if (p2 != this.y) {
            this.y = p2;
            this.e();
        }
        return;
    }

    public final void setTabTextColors(android.content.res.ColorStateList p3)
    {
        if (this.r != p3) {
            this.r = p3;
            int v0_1 = 0;
            int v1_1 = this.l.getChildCount();
            while (v0_1 < v1_1) {
                this.c(v0_1);
                v0_1++;
            }
        }
        return;
    }

    public final void setTabsFromPagerAdapter(android.support.v4.view.by p9)
    {
        this.l.removeAllViews();
        String v1_0 = this.j.iterator();
        while (v1_0.hasNext()) {
            ((android.support.design.widget.bz) v1_0.next()).e = -1;
            v1_0.remove();
        }
        this.k = 0;
        int v0_3 = 0;
        String v1_1 = p9.e();
        while (v0_3 < v1_1) {
            android.support.design.widget.bz v2_2 = new android.support.design.widget.bz(this).a(0);
            boolean v3_1 = this.j.isEmpty();
            if (v2_2.g == this) {
                int v4_1 = this.c(v2_2);
                this.l.addView(v4_1, this.d());
                if (v3_1) {
                    v4_1.setSelected(1);
                }
                this.b(v2_2, this.j.size());
                if (v3_1) {
                    v2_2.a();
                }
                v0_3++;
            } else {
                throw new IllegalArgumentException("Tab belongs to a different TabLayout.");
            }
        }
        return;
    }

    public final void setupWithViewPager(android.support.v4.view.ViewPager p4)
    {
        android.support.design.widget.bz v0_0 = p4.getAdapter();
        if (v0_0 != null) {
            this.setTabsFromPagerAdapter(v0_0);
            int v1_1 = new android.support.design.widget.cb(this);
            if (p4.a == null) {
                p4.a = new java.util.ArrayList();
            }
            p4.a.add(v1_1);
            this.setOnTabSelectedListener(new android.support.design.widget.cd(p4));
            if (v0_0.e() > 0) {
                android.support.design.widget.bz v0_2 = p4.getCurrentItem();
                if (this.getSelectedTabPosition() != v0_2) {
                    this.a(this.a(v0_2), 1);
                }
            }
            return;
        } else {
            throw new IllegalArgumentException("ViewPager does not have a PagerAdapter set");
        }
    }
}
