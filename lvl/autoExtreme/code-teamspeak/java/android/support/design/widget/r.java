package android.support.design.widget;
final class r implements java.util.Comparator {
    final synthetic android.support.design.widget.CoordinatorLayout a;

    r(android.support.design.widget.CoordinatorLayout p1)
    {
        this.a = p1;
        return;
    }

    private static int a(android.view.View p2, android.view.View p3)
    {
        int v0_6;
        if (p2 != p3) {
            if (!((android.support.design.widget.w) p2.getLayoutParams()).a(p3)) {
                if (!((android.support.design.widget.w) p3.getLayoutParams()).a(p2)) {
                    v0_6 = 0;
                } else {
                    v0_6 = -1;
                }
            } else {
                v0_6 = 1;
            }
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    public final synthetic int compare(Object p2, Object p3)
    {
        int v0_6;
        if (((android.view.View) p2) == ((android.view.View) p3)) {
            v0_6 = 0;
        } else {
            if (!((android.support.design.widget.w) ((android.view.View) p2).getLayoutParams()).a(((android.view.View) p3))) {
                if (!((android.support.design.widget.w) ((android.view.View) p3).getLayoutParams()).a(((android.view.View) p2))) {
                } else {
                    v0_6 = -1;
                }
            } else {
                v0_6 = 1;
            }
        }
        return v0_6;
    }
}
