package android.support.design.widget;
public final class p extends android.widget.FrameLayout$LayoutParams {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    private static final float f = 63;
    int d;
    float e;

    private p(int p2, int p3)
    {
        this(p2, p3);
        this.d = 0;
        this.e = 1056964608;
        return;
    }

    private p(int p2, int p3, int p4)
    {
        this(p2, p3, p4);
        this.d = 0;
        this.e = 1056964608;
        return;
    }

    public p(android.content.Context p5, android.util.AttributeSet p6)
    {
        this(p5, p6);
        this.d = 0;
        this.e = 1056964608;
        android.content.res.TypedArray v0_1 = p5.obtainStyledAttributes(p6, android.support.design.n.CollapsingAppBarLayout_LayoutParams);
        this.d = v0_1.getInt(android.support.design.n.CollapsingAppBarLayout_LayoutParams_layout_collapseMode, 0);
        this.e = v0_1.getFloat(android.support.design.n.CollapsingAppBarLayout_LayoutParams_layout_collapseParallaxMultiplier, 1056964608);
        v0_1.recycle();
        return;
    }

    public p(android.view.ViewGroup$LayoutParams p2)
    {
        this(p2);
        this.d = 0;
        this.e = 1056964608;
        return;
    }

    private p(android.view.ViewGroup$MarginLayoutParams p2)
    {
        this(p2);
        this.d = 0;
        this.e = 1056964608;
        return;
    }

    public p(android.widget.FrameLayout$LayoutParams p2)
    {
        this(p2);
        this.d = 0;
        this.e = 1056964608;
        return;
    }

    private int a()
    {
        return this.d;
    }

    private void a(float p1)
    {
        this.e = p1;
        return;
    }

    private void a(int p1)
    {
        this.d = p1;
        return;
    }

    private float b()
    {
        return this.e;
    }
}
