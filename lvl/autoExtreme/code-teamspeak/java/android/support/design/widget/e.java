package android.support.design.widget;
final class e implements java.lang.Runnable {
    final synthetic android.support.design.widget.AppBarLayout$Behavior a;
    private final android.support.design.widget.CoordinatorLayout b;
    private final android.support.design.widget.AppBarLayout c;

    e(android.support.design.widget.AppBarLayout$Behavior p1, android.support.design.widget.CoordinatorLayout p2, android.support.design.widget.AppBarLayout p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    public final void run()
    {
        if ((this.c != null) && ((android.support.design.widget.AppBarLayout$Behavior.a(this.a) != null) && (android.support.design.widget.AppBarLayout$Behavior.a(this.a).f()))) {
            this.a.a(this.b, this.c, android.support.design.widget.AppBarLayout$Behavior.a(this.a).c());
            android.support.v4.view.cx.a(this.c, this);
        }
        return;
    }
}
