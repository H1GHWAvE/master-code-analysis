package android.support.design.widget;
public final class NavigationView extends android.support.design.internal.f {
    private static final int[] a = None;
    private static final int[] b = None;
    private static final int c = 1;
    private final android.support.design.internal.a d;
    private final android.support.design.internal.b e;
    private android.support.design.widget.ap f;
    private int g;
    private android.view.MenuInflater h;

    static NavigationView()
    {
        int[] v0_0 = new int[1];
        v0_0[0] = 16842912;
        android.support.design.widget.NavigationView.a = v0_0;
        int[] v0_1 = new int[1];
        v0_1[0] = -16842910;
        android.support.design.widget.NavigationView.b = v0_1;
        return;
    }

    private NavigationView(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private NavigationView(android.content.Context p2, byte p3)
    {
        this(p2, 0);
        return;
    }

    private NavigationView(android.content.Context p11, char p12)
    {
        android.content.res.ColorStateList v5_0 = 0;
        this(p11, 0);
        this.e = new android.support.design.internal.b();
        this.d = new android.support.design.internal.a(p11);
        android.content.res.TypedArray v6 = p11.obtainStyledAttributes(0, android.support.design.n.NavigationView, 0, android.support.design.m.Widget_Design_NavigationView);
        this.setBackgroundDrawable(v6.getDrawable(android.support.design.n.NavigationView_android_background));
        if (v6.hasValue(android.support.design.n.NavigationView_elevation)) {
            android.support.v4.view.cx.f(this, ((float) v6.getDimensionPixelSize(android.support.design.n.NavigationView_elevation, 0)));
        }
        android.view.View v0_19;
        android.support.v4.view.cx.a(this, v6.getBoolean(android.support.design.n.NavigationView_android_fitsSystemWindows, 0));
        this.g = v6.getDimensionPixelSize(android.support.design.n.NavigationView_android_maxWidth, 0);
        if (!v6.hasValue(android.support.design.n.NavigationView_itemIconTint)) {
            v0_19 = this.c(16842808);
        } else {
            v0_19 = v6.getColorStateList(android.support.design.n.NavigationView_itemIconTint);
        }
        android.support.design.internal.b v1_3;
        android.view.LayoutInflater v3_0;
        if (!v6.hasValue(android.support.design.n.NavigationView_itemTextAppearance)) {
            v1_3 = 0;
            v3_0 = 0;
        } else {
            v1_3 = v6.getResourceId(android.support.design.n.NavigationView_itemTextAppearance, 0);
            v3_0 = 1;
        }
        if (v6.hasValue(android.support.design.n.NavigationView_itemTextColor)) {
            v5_0 = v6.getColorStateList(android.support.design.n.NavigationView_itemTextColor);
        }
        if ((v3_0 == null) && (v5_0 == null)) {
            v5_0 = this.c(16842806);
        }
        android.graphics.drawable.Drawable v7_3 = v6.getDrawable(android.support.design.n.NavigationView_itemBackground);
        this.d.a(new android.support.design.widget.ao(this));
        this.e.c = 1;
        this.e.a(p11, this.d);
        this.e.a(v0_19);
        if (v3_0 != null) {
            this.e.a(v1_3);
        }
        this.e.b(v5_0);
        this.e.h = v7_3;
        this.d.a(this.e);
        this.addView(((android.view.View) this.e.a(this)));
        if (v6.hasValue(android.support.design.n.NavigationView_menu)) {
            android.view.View v0_31 = v6.getResourceId(android.support.design.n.NavigationView_menu, 0);
            this.e.b(1);
            this.getMenuInflater().inflate(v0_31, this.d);
            this.e.b(0);
            this.e.a(0);
        }
        if (v6.hasValue(android.support.design.n.NavigationView_headerLayout)) {
            android.support.design.internal.b v1_8 = this.e;
            v1_8.a(v1_8.e.inflate(v6.getResourceId(android.support.design.n.NavigationView_headerLayout, 0), v1_8.b, 0));
        }
        v6.recycle();
        return;
    }

    static synthetic android.support.design.widget.ap a(android.support.design.widget.NavigationView p1)
    {
        return p1.f;
    }

    private void a(int p4)
    {
        this.e.b(1);
        this.getMenuInflater().inflate(p4, this.d);
        this.e.b(0);
        this.e.a(0);
        return;
    }

    private void a(android.view.View p2)
    {
        this.e.a(p2);
        return;
    }

    private android.view.View b(int p5)
    {
        android.support.design.internal.b v0 = this.e;
        android.view.View v1_1 = v0.e.inflate(p5, v0.b, 0);
        v0.a(v1_1);
        return v1_1;
    }

    private void b(android.view.View p5)
    {
        int v0_0 = this.e;
        v0_0.b.removeView(p5);
        if (v0_0.b.getChildCount() == 0) {
            v0_0.a.setPadding(0, v0_0.i, 0, v0_0.a.getPaddingBottom());
        }
        return;
    }

    private android.content.res.ColorStateList c(int p11)
    {
        android.content.res.ColorStateList v0_0 = 0;
        int v1_1 = new android.util.TypedValue();
        if (this.getContext().getTheme().resolveAttribute(p11, v1_1, 1)) {
            int v2_4 = this.getResources().getColorStateList(v1_1.resourceId);
            if (this.getContext().getTheme().resolveAttribute(android.support.design.d.colorPrimary, v1_1, 1)) {
                int v1_2 = v1_1.data;
                int v3_4 = v2_4.getDefaultColor();
                int[][] v4_1 = new int[][3];
                v4_1[0] = android.support.design.widget.NavigationView.b;
                v4_1[1] = android.support.design.widget.NavigationView.a;
                v4_1[2] = android.support.design.widget.NavigationView.EMPTY_STATE_SET;
                int[] v5_3 = new int[3];
                v5_3[0] = v2_4.getColorForState(android.support.design.widget.NavigationView.b, v3_4);
                v5_3[1] = v1_2;
                v5_3[2] = v3_4;
                v0_0 = new android.content.res.ColorStateList(v4_1, v5_3);
            }
        }
        return v0_0;
    }

    private android.view.MenuInflater getMenuInflater()
    {
        if (this.h == null) {
            this.h = new android.support.v7.internal.view.f(this.getContext());
        }
        return this.h;
    }

    public final android.graphics.drawable.Drawable getItemBackground()
    {
        return this.e.h;
    }

    public final android.content.res.ColorStateList getItemIconTintList()
    {
        return this.e.g;
    }

    public final android.content.res.ColorStateList getItemTextColor()
    {
        return this.e.f;
    }

    public final android.view.Menu getMenu()
    {
        return this.d;
    }

    protected final void onMeasure(int p4, int p5)
    {
        switch (android.view.View$MeasureSpec.getMode(p4)) {
            case -2147483648:
                p4 = android.view.View$MeasureSpec.makeMeasureSpec(Math.min(android.view.View$MeasureSpec.getSize(p4), this.g), 1073741824);
                break;
            case 0:
                p4 = android.view.View$MeasureSpec.makeMeasureSpec(this.g, 1073741824);
            case 1073741824:
            default:
                break;
        }
        super.onMeasure(p4, p5);
        return;
    }

    protected final void onRestoreInstanceState(android.os.Parcelable p3)
    {
        super.onRestoreInstanceState(((android.support.design.widget.NavigationView$SavedState) p3).getSuperState());
        this.d.b(((android.support.design.widget.NavigationView$SavedState) p3).a);
        return;
    }

    protected final android.os.Parcelable onSaveInstanceState()
    {
        android.support.design.widget.NavigationView$SavedState v1_1 = new android.support.design.widget.NavigationView$SavedState(super.onSaveInstanceState());
        v1_1.a = new android.os.Bundle();
        this.d.a(v1_1.a);
        return v1_1;
    }

    public final void setCheckedItem(int p3)
    {
        android.support.v7.internal.view.menu.m v0_1 = this.d.findItem(p3);
        if (v0_1 != null) {
            this.e.d.a(((android.support.v7.internal.view.menu.m) v0_1));
        }
        return;
    }

    public final void setItemBackground(android.graphics.drawable.Drawable p2)
    {
        this.e.h = p2;
        return;
    }

    public final void setItemBackgroundResource(int p2)
    {
        this.setItemBackground(android.support.v4.c.h.a(this.getContext(), p2));
        return;
    }

    public final void setItemIconTintList(android.content.res.ColorStateList p2)
    {
        this.e.a(p2);
        return;
    }

    public final void setItemTextAppearance(int p2)
    {
        this.e.a(p2);
        return;
    }

    public final void setItemTextColor(android.content.res.ColorStateList p2)
    {
        this.e.b(p2);
        return;
    }

    public final void setNavigationItemSelectedListener(android.support.design.widget.ap p1)
    {
        this.f = p1;
        return;
    }
}
