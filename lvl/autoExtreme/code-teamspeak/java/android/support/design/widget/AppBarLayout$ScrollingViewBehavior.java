package android.support.design.widget;
public class AppBarLayout$ScrollingViewBehavior extends android.support.design.widget.df {
    private int a;

    public AppBarLayout$ScrollingViewBehavior()
    {
        return;
    }

    public AppBarLayout$ScrollingViewBehavior(android.content.Context p4, android.util.AttributeSet p5)
    {
        this(p4, p5);
        android.content.res.TypedArray v0_1 = p4.obtainStyledAttributes(p5, android.support.design.n.ScrollingViewBehavior_Params);
        this.a = v0_1.getDimensionPixelSize(android.support.design.n.ScrollingViewBehavior_Params_behavior_overlapTop, 0);
        v0_1.recycle();
        return;
    }

    private int a()
    {
        return this.a;
    }

    private static android.support.design.widget.AppBarLayout a(java.util.List p4)
    {
        int v2 = p4.size();
        int v1 = 0;
        while (v1 < v2) {
            int v0_3 = ((android.view.View) p4.get(v1));
            if (!(v0_3 instanceof android.support.design.widget.AppBarLayout)) {
                v1++;
            } else {
                int v0_1 = ((android.support.design.widget.AppBarLayout) v0_3);
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    private void c(int p1)
    {
        this.a = p1;
        return;
    }

    public final bridge synthetic boolean a(int p2)
    {
        return super.a(p2);
    }

    public final bridge synthetic boolean a(android.support.design.widget.CoordinatorLayout p2, android.view.View p3, int p4)
    {
        return super.a(p2, p3, p4);
    }

    public final boolean a(android.support.design.widget.CoordinatorLayout p10, android.view.View p11, int p12, int p13, int p14)
    {
        int v0_9;
        int v4 = p11.getLayoutParams().height;
        if ((v4 != -1) && (v4 != -2)) {
            v0_9 = 0;
        } else {
            java.util.List v5 = p10.a(p11);
            if (!v5.isEmpty()) {
                int v6 = v5.size();
                int v3_0 = 0;
                while (v3_0 < v6) {
                    int v0_5 = ((android.view.View) v5.get(v3_0));
                    if (!(v0_5 instanceof android.support.design.widget.AppBarLayout)) {
                        v3_0++;
                    } else {
                        int v3_1 = ((android.support.design.widget.AppBarLayout) v0_5);
                    }
                    if ((v3_1 == 0) || (!android.support.v4.view.cx.B(v3_1))) {
                    } else {
                        if (android.support.v4.view.cx.u(v3_1)) {
                            android.support.v4.view.cx.a(p11, 1);
                        }
                        int v0_11 = android.view.View$MeasureSpec.getSize(p14);
                        if (v0_11 == 0) {
                            v0_11 = p10.getHeight();
                        }
                        int v0_13;
                        int v1_3 = (v3_1.getTotalScrollRange() + (v0_11 - v3_1.getMeasuredHeight()));
                        if (v4 != -1) {
                            v0_13 = -2147483648;
                        } else {
                            v0_13 = 1073741824;
                        }
                        p10.a(p11, p12, p13, android.view.View$MeasureSpec.makeMeasureSpec(v1_3, v0_13));
                        v0_9 = 1;
                    }
                }
                v3_1 = 0;
            } else {
                v0_9 = 0;
            }
        }
        return v0_9;
    }

    public final boolean a(android.support.design.widget.CoordinatorLayout p5, android.view.View p6, android.view.View p7)
    {
        int v0_2 = ((android.support.design.widget.w) p7.getLayoutParams()).a;
        if ((v0_2 instanceof android.support.design.widget.AppBarLayout$Behavior)) {
            int v0_4 = ((android.support.design.widget.AppBarLayout$Behavior) v0_2).a();
            int v1_2 = (p7.getHeight() - this.a);
            int v2_2 = (p5.getHeight() - p6.getHeight());
            if ((this.a == 0) || (!(p7 instanceof android.support.design.widget.AppBarLayout))) {
                super.b((v0_4 + (p7.getHeight() - this.a)));
            } else {
                super.b(android.support.design.widget.a.a(v1_2, v2_2, (((float) Math.abs(v0_4)) / ((float) ((android.support.design.widget.AppBarLayout) p7).getTotalScrollRange()))));
            }
        }
        return 0;
    }

    public final bridge synthetic int b()
    {
        return super.b();
    }

    public final bridge synthetic boolean b(int p2)
    {
        return super.b(p2);
    }

    public final boolean b(android.view.View p2)
    {
        return (p2 instanceof android.support.design.widget.AppBarLayout);
    }

    public final bridge synthetic int c()
    {
        return super.c();
    }
}
