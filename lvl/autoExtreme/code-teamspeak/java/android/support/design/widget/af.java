package android.support.design.widget;
abstract class af extends android.view.animation.Animation {
    final synthetic android.support.design.widget.ad a;
    private float b;
    private float c;

    private af(android.support.design.widget.ad p1)
    {
        this.a = p1;
        return;
    }

    synthetic af(android.support.design.widget.ad p1, byte p2)
    {
        this(p1);
        return;
    }

    protected abstract float a();

    protected void applyTransformation(float p4, android.view.animation.Transformation p5)
    {
        android.support.design.widget.ar v0_1 = this.a.a;
        v0_1.a((this.b + (this.c * p4)), v0_1.l);
        return;
    }

    public void reset()
    {
        super.reset();
        this.b = this.a.a.n;
        this.c = (this.a() - this.b);
        return;
    }
}
