package android.support.design.widget;
final class bw extends android.widget.LinearLayout {
    int a;
    float b;
    int c;
    int d;
    final synthetic android.support.design.widget.br e;
    private int f;
    private final android.graphics.Paint g;

    bw(android.support.design.widget.br p2, android.content.Context p3)
    {
        this.e = p2;
        this(p3);
        this.a = -1;
        this.c = -1;
        this.d = -1;
        this.setWillNotDraw(0);
        this.g = new android.graphics.Paint();
        return;
    }

    static synthetic float a(android.support.design.widget.bw p1)
    {
        p1.b = 0;
        return 0;
    }

    static synthetic int a(android.support.design.widget.bw p0, int p1)
    {
        p0.a = p1;
        return p1;
    }

    private void a(int p1, float p2)
    {
        this.a = p1;
        this.b = p2;
        this.a();
        return;
    }

    private void a(int p2, int p3)
    {
        if ((p2 != this.c) || (p3 != this.d)) {
            this.c = p2;
            this.d = p3;
            android.support.v4.view.cx.b(this);
        }
        return;
    }

    static synthetic void a(android.support.design.widget.bw p0, int p1, int p2)
    {
        p0.a(p1, p2);
        return;
    }

    private boolean b()
    {
        int v0 = 0;
        int v2 = this.getChildCount();
        int v1 = 0;
        while (v1 < v2) {
            if (this.getChildAt(v1).getWidth() > 0) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    private float c()
    {
        return (((float) this.a) + this.b);
    }

    private void c(int p8)
    {
        android.support.design.widget.cr v0_1;
        if (android.support.v4.view.cx.f(this) != 1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v2_6;
        int v4;
        int v2_0 = this.getChildAt(p8);
        int v3 = v2_0.getLeft();
        int v5 = v2_0.getRight();
        if (Math.abs((p8 - this.a)) > 1) {
            android.support.design.widget.cr v1_2 = android.support.design.widget.br.b(this.e, 24);
            if (p8 >= this.a) {
                if (v0_1 != null) {
                    v4 = (v5 + v1_2);
                    v2_6 = v4;
                    if ((v2_6 != v3) || (v4 != v5)) {
                        android.support.design.widget.ck v6 = android.support.design.widget.br.a(this.e, android.support.design.widget.dh.a());
                        v6.a(android.support.design.widget.a.b);
                        v6.a(300);
                        v6.a(0, 1065353216);
                        v6.a(new android.support.design.widget.bx(this, v2_6, v3, v4, v5));
                        v6.a.a(new android.support.design.widget.cm(v6, new android.support.design.widget.by(this, p8)));
                        v6.a.a();
                    }
                    return;
                }
            } else {
                if (v0_1 == null) {
                    v4 = (v5 + v1_2);
                    v2_6 = v4;
                }
            }
            v4 = (v3 - v1_2);
            v2_6 = v4;
        } else {
            v2_6 = this.c;
            v4 = this.d;
        }
    }

    final void a()
    {
        int v1_1;
        int v0_2;
        int v1_0 = this.getChildAt(this.a);
        if ((v1_0 == 0) || (v1_0.getWidth() <= 0)) {
            v0_2 = -1;
            v1_1 = -1;
        } else {
            v0_2 = v1_0.getLeft();
            v1_1 = v1_0.getRight();
            if ((this.b > 0) && (this.a < (this.getChildCount() - 1))) {
                float v2_5 = this.getChildAt((this.a + 1));
                v0_2 = ((int) ((((float) v0_2) * (1065353216 - this.b)) + (this.b * ((float) v2_5.getLeft()))));
                v1_1 = ((int) ((((float) v1_1) * (1065353216 - this.b)) + (((float) v2_5.getRight()) * this.b)));
            }
        }
        this.a(v0_2, v1_1);
        return;
    }

    final void a(int p2)
    {
        if (this.g.getColor() != p2) {
            this.g.setColor(p2);
            android.support.v4.view.cx.b(this);
        }
        return;
    }

    final void b(int p2)
    {
        if (this.f != p2) {
            this.f = p2;
            android.support.v4.view.cx.b(this);
        }
        return;
    }

    public final void draw(android.graphics.Canvas p7)
    {
        super.draw(p7);
        if ((this.c >= 0) && (this.d > this.c)) {
            p7.drawRect(((float) this.c), ((float) (this.getHeight() - this.f)), ((float) this.d), ((float) this.getHeight()), this.g);
        }
        return;
    }

    protected final void onLayout(boolean p1, int p2, int p3, int p4, int p5)
    {
        super.onLayout(p1, p2, p3, p4, p5).a();
        return;
    }

    protected final void onMeasure(int p7, int p8)
    {
        super.onMeasure(p7, p8);
        if ((android.view.View$MeasureSpec.getMode(p7) == 1073741824) && ((android.support.design.widget.br.j(this.e) == 1) && (android.support.design.widget.br.k(this.e) == 1))) {
            int v3_1 = this.getChildCount();
            int v4_0 = android.view.View$MeasureSpec.makeMeasureSpec(0, 0);
            int v1_5 = 0;
            int v2_1 = 0;
            while (v1_5 < v3_1) {
                int v5_1 = this.getChildAt(v1_5);
                v5_1.measure(v4_0, p8);
                v2_1 = Math.max(v2_1, v5_1.getMeasuredWidth());
                v1_5++;
            }
            if (v2_1 > 0) {
                if ((v2_1 * v3_1) > (this.getMeasuredWidth() - (android.support.design.widget.br.b(this.e, 16) * 2))) {
                    android.support.design.widget.br.l(this.e);
                    android.support.design.widget.br.m(this.e);
                } else {
                    int v1_10 = 0;
                    while (v1_10 < v3_1) {
                        int v0_5 = ((android.widget.LinearLayout$LayoutParams) this.getChildAt(v1_10).getLayoutParams());
                        v0_5.width = v2_1;
                        v0_5.weight = 0;
                        v1_10++;
                    }
                }
                super.onMeasure(p7, p8);
            }
        }
        return;
    }
}
