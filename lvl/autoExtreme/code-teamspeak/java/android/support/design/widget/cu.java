package android.support.design.widget;
final class cu extends android.support.design.widget.cr {
    private static final int a = 10;
    private static final int b = 200;
    private static final android.os.Handler c;
    private long d;
    private boolean e;
    private final int[] f;
    private final float[] g;
    private int h;
    private android.view.animation.Interpolator i;
    private android.support.design.widget.cs j;
    private android.support.design.widget.ct k;
    private float l;
    private final Runnable m;

    static cu()
    {
        android.support.design.widget.cu.c = new android.os.Handler(android.os.Looper.getMainLooper());
        return;
    }

    cu()
    {
        android.support.design.widget.cv v0_0 = new int[2];
        this.f = v0_0;
        android.support.design.widget.cv v0_1 = new float[2];
        this.g = v0_1;
        this.h = 200;
        this.m = new android.support.design.widget.cv(this);
        return;
    }

    static synthetic void a(android.support.design.widget.cu p6)
    {
        if (p6.e) {
            android.support.design.widget.cs v0_4 = (((float) (android.os.SystemClock.uptimeMillis() - p6.d)) / ((float) p6.h));
            if (p6.i != null) {
                v0_4 = p6.i.getInterpolation(v0_4);
            }
            p6.l = v0_4;
            if (p6.k != null) {
                p6.k.a();
            }
            if (android.os.SystemClock.uptimeMillis() >= (p6.d + ((long) p6.h))) {
                p6.e = 0;
                if (p6.j != null) {
                    p6.j.b();
                }
            }
        }
        if (p6.e) {
            android.support.design.widget.cu.c.postDelayed(p6.m, 10);
        }
        return;
    }

    private void h()
    {
        if (this.e) {
            android.support.design.widget.cs v0_4 = (((float) (android.os.SystemClock.uptimeMillis() - this.d)) / ((float) this.h));
            if (this.i != null) {
                v0_4 = this.i.getInterpolation(v0_4);
            }
            this.l = v0_4;
            if (this.k != null) {
                this.k.a();
            }
            if (android.os.SystemClock.uptimeMillis() >= (this.d + ((long) this.h))) {
                this.e = 0;
                if (this.j != null) {
                    this.j.b();
                }
            }
        }
        if (this.e) {
            android.support.design.widget.cu.c.postDelayed(this.m, 10);
        }
        return;
    }

    public final void a()
    {
        if (!this.e) {
            if (this.i == null) {
                this.i = new android.view.animation.AccelerateDecelerateInterpolator();
            }
            this.d = android.os.SystemClock.uptimeMillis();
            this.e = 1;
            android.support.design.widget.cu.c.postDelayed(this.m, 10);
        }
        return;
    }

    public final void a(float p3, float p4)
    {
        this.g[0] = p3;
        this.g[1] = p4;
        return;
    }

    public final void a(int p1)
    {
        this.h = p1;
        return;
    }

    public final void a(int p3, int p4)
    {
        this.f[0] = p3;
        this.f[1] = p4;
        return;
    }

    public final void a(android.support.design.widget.cs p1)
    {
        this.j = p1;
        return;
    }

    public final void a(android.support.design.widget.ct p1)
    {
        this.k = p1;
        return;
    }

    public final void a(android.view.animation.Interpolator p1)
    {
        this.i = p1;
        return;
    }

    public final boolean b()
    {
        return this.e;
    }

    public final int c()
    {
        return android.support.design.widget.a.a(this.f[0], this.f[1], this.l);
    }

    public final float d()
    {
        return android.support.design.widget.a.a(this.g[0], this.g[1], this.l);
    }

    public final void e()
    {
        this.e = 0;
        android.support.design.widget.cu.c.removeCallbacks(this.m);
        if (this.j != null) {
            this.j.c();
        }
        return;
    }

    public final float f()
    {
        return this.l;
    }

    public final void g()
    {
        if (this.e) {
            this.e = 0;
            android.support.design.widget.cu.c.removeCallbacks(this.m);
            this.l = 1065353216;
            if (this.k != null) {
                this.k.a();
            }
            if (this.j != null) {
                this.j.b();
            }
        }
        return;
    }
}
