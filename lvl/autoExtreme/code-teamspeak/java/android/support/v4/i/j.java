package android.support.v4.i;
public final class j implements android.os.Parcelable$Creator {
    final android.support.v4.i.k a;

    public j(android.support.v4.i.k p1)
    {
        this.a = p1;
        return;
    }

    public final Object createFromParcel(android.os.Parcel p3)
    {
        return this.a.a(p3, 0);
    }

    public final Object[] newArray(int p2)
    {
        return this.a.a(p2);
    }
}
