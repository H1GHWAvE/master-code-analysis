package android.support.v4.j;
final class j extends android.print.PrintDocumentAdapter {
    final synthetic String a;
    final synthetic android.graphics.Bitmap b;
    final synthetic int c;
    final synthetic android.support.v4.j.n d;
    final synthetic android.support.v4.j.i e;
    private android.print.PrintAttributes f;

    j(android.support.v4.j.i p1, String p2, android.graphics.Bitmap p3, int p4, android.support.v4.j.n p5)
    {
        this.e = p1;
        this.a = p2;
        this.b = p3;
        this.c = p4;
        this.d = p5;
        return;
    }

    public final void onFinish()
    {
        return;
    }

    public final void onLayout(android.print.PrintAttributes p4, android.print.PrintAttributes p5, android.os.CancellationSignal p6, android.print.PrintDocumentAdapter$LayoutResultCallback p7, android.os.Bundle p8)
    {
        int v0 = 1;
        this.f = p5;
        android.print.PrintDocumentInfo v1_4 = new android.print.PrintDocumentInfo$Builder(this.a).setContentType(1).setPageCount(1).build();
        if (p5.equals(p4)) {
            v0 = 0;
        }
        p7.onLayoutFinished(v1_4, v0);
        return;
    }

    public final void onWrite(android.print.PageRange[] p8, android.os.ParcelFileDescriptor p9, android.os.CancellationSignal p10, android.print.PrintDocumentAdapter$WriteResultCallback p11)
    {
        java.io.IOException v1_1 = new android.print.pdf.PrintedPdfDocument(this.e.a, this.f);
        android.graphics.Bitmap v2_3 = android.support.v4.j.i.a(this.b, this.f.getColorMode());
        try {
            java.io.IOException v0_4 = v1_1.startPage(1);
            v0_4.getCanvas().drawBitmap(v2_3, android.support.v4.j.i.a(v2_3.getWidth(), v2_3.getHeight(), new android.graphics.RectF(v0_4.getInfo().getContentRect()), this.c), 0);
            v1_1.finishPage(v0_4);
            try {
                v1_1.writeTo(new java.io.FileOutputStream(p9.getFileDescriptor()));
                java.io.IOException v0_8 = new android.print.PageRange[1];
                v0_8[0] = android.print.PageRange.ALL_PAGES;
                p11.onWriteFinished(v0_8);
            } catch (java.io.IOException v0_9) {
                android.util.Log.e("PrintHelperKitkat", "Error writing printed content", v0_9);
                p11.onWriteFailed(0);
            }
            v1_1.close();
            if (p9 != null) {
                try {
                    p9.close();
                } catch (java.io.IOException v0) {
                }
            }
            if (v2_3 != this.b) {
                v2_3.recycle();
            }
            return;
        } catch (java.io.IOException v0_11) {
            v1_1.close();
            if (p9 != null) {
                try {
                    p9.close();
                } catch (java.io.IOException v1) {
                }
            }
            if (v2_3 != this.b) {
                v2_3.recycle();
            }
            throw v0_11;
        }
    }
}
