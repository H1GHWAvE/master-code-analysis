package android.support.v4.media.a;
public final class g {

    public g()
    {
        return;
    }

    private static CharSequence a(Object p1)
    {
        return ((android.media.MediaRouter$RouteInfo) p1).getStatus();
    }

    private static CharSequence a(Object p1, android.content.Context p2)
    {
        return ((android.media.MediaRouter$RouteInfo) p1).getName(p2);
    }

    private static void a(Object p0, int p1)
    {
        ((android.media.MediaRouter$RouteInfo) p0).requestSetVolume(p1);
        return;
    }

    private static void a(Object p0, Object p1)
    {
        ((android.media.MediaRouter$RouteInfo) p0).setTag(p1);
        return;
    }

    private static int b(Object p1)
    {
        return ((android.media.MediaRouter$RouteInfo) p1).getSupportedTypes();
    }

    private static void b(Object p0, int p1)
    {
        ((android.media.MediaRouter$RouteInfo) p0).requestUpdateVolume(p1);
        return;
    }

    private static Object c(Object p1)
    {
        return ((android.media.MediaRouter$RouteInfo) p1).getCategory();
    }

    private static android.graphics.drawable.Drawable d(Object p1)
    {
        return ((android.media.MediaRouter$RouteInfo) p1).getIconDrawable();
    }

    private static int e(Object p1)
    {
        return ((android.media.MediaRouter$RouteInfo) p1).getPlaybackType();
    }

    private static int f(Object p1)
    {
        return ((android.media.MediaRouter$RouteInfo) p1).getPlaybackStream();
    }

    private static int g(Object p1)
    {
        return ((android.media.MediaRouter$RouteInfo) p1).getVolume();
    }

    private static int h(Object p1)
    {
        return ((android.media.MediaRouter$RouteInfo) p1).getVolumeMax();
    }

    private static int i(Object p1)
    {
        return ((android.media.MediaRouter$RouteInfo) p1).getVolumeHandling();
    }

    private static Object j(Object p1)
    {
        return ((android.media.MediaRouter$RouteInfo) p1).getTag();
    }

    private static Object k(Object p1)
    {
        return ((android.media.MediaRouter$RouteInfo) p1).getGroup();
    }

    private static boolean l(Object p1)
    {
        return (p1 instanceof android.media.MediaRouter$RouteGroup);
    }
}
