package android.support.v4.media.session;
public abstract class e extends android.os.Binder implements android.support.v4.media.session.d {
    static final int A = 27;
    static final int B = 28;
    static final int C = 29;
    static final int D = 30;
    static final int E = 31;
    static final int F = 32;
    private static final String G = "android.support.v4.media.session.IMediaSession";
    static final int a = 1;
    static final int b = 2;
    static final int c = 3;
    static final int d = 4;
    static final int e = 5;
    static final int f = 6;
    static final int g = 7;
    static final int h = 8;
    static final int i = 9;
    static final int j = 10;
    static final int k = 11;
    static final int l = 12;
    static final int m = 13;
    static final int n = 14;
    static final int o = 15;
    static final int p = 16;
    static final int q = 17;
    static final int r = 18;
    static final int s = 19;
    static final int t = 20;
    static final int u = 21;
    static final int v = 22;
    static final int w = 23;
    static final int x = 24;
    static final int y = 25;
    static final int z = 26;

    public e()
    {
        this.attachInterface(this, "android.support.v4.media.session.IMediaSession");
        return;
    }

    public static android.support.v4.media.session.d a(android.os.IBinder p2)
    {
        android.support.v4.media.session.d v0_3;
        if (p2 != null) {
            android.support.v4.media.session.d v0_1 = p2.queryLocalInterface("android.support.v4.media.session.IMediaSession");
            if ((v0_1 == null) || (!(v0_1 instanceof android.support.v4.media.session.d))) {
                v0_3 = new android.support.v4.media.session.f(p2);
            } else {
                v0_3 = ((android.support.v4.media.session.d) v0_1);
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p6, android.os.Parcel p7, android.os.Parcel p8, int p9)
    {
        String v1_0 = 0;
        boolean v3 = 1;
        switch (p6) {
            case 1:
                String v1_7;
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v4 = p7.readString();
                if (p7.readInt() == 0) {
                    v1_7 = 0;
                } else {
                    v1_7 = ((android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(p7));
                }
                String v0_88;
                if (p7.readInt() == 0) {
                    v0_88 = 0;
                } else {
                    v0_88 = ((android.support.v4.media.session.MediaSessionCompat$ResultReceiverWrapper) android.support.v4.media.session.MediaSessionCompat$ResultReceiverWrapper.CREATOR.createFromParcel(p7));
                }
                this.a(v4, v1_7, v0_88);
                p8.writeNoException();
                break;
            case 2:
                String v0_77;
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                if (p7.readInt() == 0) {
                    v0_77 = 0;
                } else {
                    v0_77 = ((android.view.KeyEvent) android.view.KeyEvent.CREATOR.createFromParcel(p7));
                }
                String v0_81;
                String v0_80 = this.a(v0_77);
                p8.writeNoException();
                if (v0_80 == null) {
                    v0_81 = 0;
                } else {
                    v0_81 = 1;
                }
                p8.writeInt(v0_81);
                break;
            case 3:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.a(android.support.v4.media.session.b.a(p7.readStrongBinder()));
                p8.writeNoException();
                break;
            case 4:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.b(android.support.v4.media.session.b.a(p7.readStrongBinder()));
                p8.writeNoException();
                break;
            case 5:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v0_68 = this.a();
                p8.writeNoException();
                if (v0_68 != null) {
                    v1_0 = 1;
                }
                p8.writeInt(v1_0);
                break;
            case 6:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v0_66 = this.b();
                p8.writeNoException();
                p8.writeString(v0_66);
                break;
            case 7:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v0_64 = this.c();
                p8.writeNoException();
                p8.writeString(v0_64);
                break;
            case 8:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v0_62 = this.d();
                p8.writeNoException();
                if (v0_62 == null) {
                    p8.writeInt(0);
                } else {
                    p8.writeInt(1);
                    v0_62.writeToParcel(p8, 1);
                }
                break;
            case 9:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v0_60 = this.e();
                p8.writeNoException();
                p8.writeLong(v0_60);
                break;
            case 10:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v0_58 = this.f();
                p8.writeNoException();
                if (v0_58 == null) {
                    p8.writeInt(0);
                } else {
                    p8.writeInt(1);
                    v0_58.writeToParcel(p8, 1);
                }
                break;
            case 11:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.a(p7.readInt(), p7.readInt(), p7.readString());
                p8.writeNoException();
                break;
            case 12:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.b(p7.readInt(), p7.readInt(), p7.readString());
                p8.writeNoException();
                break;
            case 13:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.g();
                p8.writeNoException();
                break;
            case 14:
                String v0_49;
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v1_4 = p7.readString();
                if (p7.readInt() == 0) {
                    v0_49 = 0;
                } else {
                    v0_49 = ((android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(p7));
                }
                this.a(v1_4, v0_49);
                p8.writeNoException();
                break;
            case 15:
                String v0_44;
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v1_3 = p7.readString();
                if (p7.readInt() == 0) {
                    v0_44 = 0;
                } else {
                    v0_44 = ((android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(p7));
                }
                this.b(v1_3, v0_44);
                p8.writeNoException();
                break;
            case 16:
                String v1_2;
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                if (p7.readInt() == 0) {
                    v1_2 = 0;
                } else {
                    v1_2 = ((android.net.Uri) android.net.Uri.CREATOR.createFromParcel(p7));
                }
                String v0_39;
                if (p7.readInt() == 0) {
                    v0_39 = 0;
                } else {
                    v0_39 = ((android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(p7));
                }
                this.a(v1_2, v0_39);
                p8.writeNoException();
                break;
            case 17:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.a(p7.readLong());
                p8.writeNoException();
                break;
            case 18:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.h();
                p8.writeNoException();
                break;
            case 19:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.i();
                p8.writeNoException();
                break;
            case 20:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.j();
                p8.writeNoException();
                break;
            case 21:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.k();
                p8.writeNoException();
                break;
            case 22:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.l();
                p8.writeNoException();
                break;
            case 23:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.m();
                p8.writeNoException();
                break;
            case 24:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                this.b(p7.readLong());
                p8.writeNoException();
                break;
            case 25:
                String v0_20;
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                if (p7.readInt() == 0) {
                    v0_20 = 0;
                } else {
                    v0_20 = ((android.support.v4.media.RatingCompat) android.support.v4.media.RatingCompat.CREATOR.createFromParcel(p7));
                }
                this.a(v0_20);
                p8.writeNoException();
                break;
            case 26:
                String v0_15;
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v1_1 = p7.readString();
                if (p7.readInt() == 0) {
                    v0_15 = 0;
                } else {
                    v0_15 = ((android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(p7));
                }
                this.c(v1_1, v0_15);
                p8.writeNoException();
                break;
            case 27:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v0_12 = this.n();
                p8.writeNoException();
                if (v0_12 == null) {
                    p8.writeInt(0);
                } else {
                    p8.writeInt(1);
                    v0_12.writeToParcel(p8, 1);
                }
                break;
            case 28:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v0_10 = this.o();
                p8.writeNoException();
                if (v0_10 == null) {
                    p8.writeInt(0);
                } else {
                    p8.writeInt(1);
                    v0_10.writeToParcel(p8, 1);
                }
                break;
            case 29:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v0_8 = this.p();
                p8.writeNoException();
                p8.writeTypedList(v0_8);
                break;
            case 30:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v0_6 = this.q();
                p8.writeNoException();
                if (v0_6 == null) {
                    p8.writeInt(0);
                } else {
                    p8.writeInt(1);
                    android.text.TextUtils.writeToParcel(v0_6, p8, 1);
                }
                break;
            case 31:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v0_4 = this.r();
                p8.writeNoException();
                if (v0_4 == null) {
                    p8.writeInt(0);
                } else {
                    p8.writeInt(1);
                    v0_4.writeToParcel(p8, 1);
                }
                break;
            case 32:
                p7.enforceInterface("android.support.v4.media.session.IMediaSession");
                String v0_2 = this.s();
                p8.writeNoException();
                p8.writeInt(v0_2);
                break;
            case 1598968902:
                p8.writeString("android.support.v4.media.session.IMediaSession");
                break;
            default:
                v3 = super.onTransact(p6, p7, p8, p9);
        }
        return v3;
    }
}
