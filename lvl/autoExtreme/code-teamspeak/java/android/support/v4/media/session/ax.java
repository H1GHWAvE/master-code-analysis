package android.support.v4.media.session;
public final class ax {
    private static final long a = 128;
    private static final String b = "android.media.metadata.USER_RATING";
    private static final String c = "android.media.metadata.RATING";
    private static final String d = "android.media.metadata.YEAR";

    public ax()
    {
        return;
    }

    private static int a(long p6)
    {
        int v0 = android.support.v4.media.session.av.a(p6);
        if ((128 & p6) != 0) {
            v0 |= 512;
        }
        return v0;
    }

    private static Object a(android.support.v4.media.session.au p1)
    {
        return new android.support.v4.media.session.ay(p1);
    }

    private static void a(android.os.Bundle p4, android.media.RemoteControlClient$MetadataEditor p5)
    {
        if (p4 != null) {
            if (p4.containsKey("android.media.metadata.YEAR")) {
                p5.putLong(8, p4.getLong("android.media.metadata.YEAR"));
            }
            if (p4.containsKey("android.media.metadata.RATING")) {
                p5.putObject(101, p4.getParcelable("android.media.metadata.RATING"));
            }
            if (p4.containsKey("android.media.metadata.USER_RATING")) {
                p5.putObject(268435457, p4.getParcelable("android.media.metadata.USER_RATING"));
            }
        }
        return;
    }

    private static void a(Object p7, long p8)
    {
        int v0 = android.support.v4.media.session.av.a(p8);
        if ((128 & p8) != 0) {
            v0 |= 512;
        }
        ((android.media.RemoteControlClient) p7).setTransportControlFlags(v0);
        return;
    }

    private static void a(Object p8, android.os.Bundle p9, long p10)
    {
        android.media.RemoteControlClient$MetadataEditor v0_1 = ((android.media.RemoteControlClient) p8).editMetadata(1);
        android.support.v4.media.session.at.a(p9, v0_1);
        if (p9 != null) {
            if (p9.containsKey("android.media.metadata.YEAR")) {
                v0_1.putLong(8, p9.getLong("android.media.metadata.YEAR"));
            }
            if (p9.containsKey("android.media.metadata.RATING")) {
                v0_1.putObject(101, p9.getParcelable("android.media.metadata.RATING"));
            }
            if (p9.containsKey("android.media.metadata.USER_RATING")) {
                v0_1.putObject(268435457, p9.getParcelable("android.media.metadata.USER_RATING"));
            }
        }
        if ((128 & p10) != 0) {
            v0_1.addEditableKey(268435457);
        }
        v0_1.apply();
        return;
    }

    public static void a(Object p0, Object p1)
    {
        ((android.media.RemoteControlClient) p0).setMetadataUpdateListener(((android.media.RemoteControlClient$OnMetadataUpdateListener) p1));
        return;
    }
}
