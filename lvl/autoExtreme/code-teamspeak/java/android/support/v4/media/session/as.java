package android.support.v4.media.session;
final class as implements android.os.Parcelable$Creator {

    as()
    {
        return;
    }

    private static android.support.v4.media.session.MediaSessionCompat$Token a(android.os.Parcel p2)
    {
        android.os.IBinder v0_1;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_1 = p2.readStrongBinder();
        } else {
            v0_1 = p2.readParcelable(0);
        }
        return new android.support.v4.media.session.MediaSessionCompat$Token(v0_1);
    }

    private static android.support.v4.media.session.MediaSessionCompat$Token[] a(int p1)
    {
        android.support.v4.media.session.MediaSessionCompat$Token[] v0 = new android.support.v4.media.session.MediaSessionCompat$Token[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p3)
    {
        android.os.IBinder v0_1;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_1 = p3.readStrongBinder();
        } else {
            v0_1 = p3.readParcelable(0);
        }
        return new android.support.v4.media.session.MediaSessionCompat$Token(v0_1);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.media.session.MediaSessionCompat$Token[] v0 = new android.support.v4.media.session.MediaSessionCompat$Token[p2];
        return v0;
    }
}
