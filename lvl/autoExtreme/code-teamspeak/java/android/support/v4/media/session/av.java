package android.support.v4.media.session;
public final class av {
    private static final long a = 256;

    public av()
    {
        return;
    }

    static int a(long p6)
    {
        int v0 = android.support.v4.media.session.at.a(p6);
        if ((256 & p6) != 0) {
            v0 |= 256;
        }
        return v0;
    }

    private static Object a(android.support.v4.media.session.au p1)
    {
        return new android.support.v4.media.session.aw(p1);
    }

    public static void a(android.content.Context p1, android.app.PendingIntent p2)
    {
        ((android.media.AudioManager) p1.getSystemService("audio")).unregisterMediaButtonEventReceiver(p2);
        return;
    }

    private static void a(Object p6, int p7, long p8, float p10, long p11)
    {
        long v0_0 = 0;
        float v2_0 = android.os.SystemClock.elapsedRealtime();
        if ((p7 == 3) && (p8 > 0)) {
            if (p11 > 0) {
                v0_0 = (v2_0 - p11);
                if ((p10 > 0) && (p10 != 1065353216)) {
                    v0_0 = ((long) (((float) v0_0) * p10));
                }
            }
            p8 += v0_0;
        }
        ((android.media.RemoteControlClient) p6).setPlaybackState(android.support.v4.media.session.at.a(p7), p8, p10);
        return;
    }

    private static void a(Object p1, long p2)
    {
        ((android.media.RemoteControlClient) p1).setTransportControlFlags(android.support.v4.media.session.av.a(p2));
        return;
    }

    public static void a(Object p0, Object p1)
    {
        ((android.media.RemoteControlClient) p0).setPlaybackPositionUpdateListener(((android.media.RemoteControlClient$OnPlaybackPositionUpdateListener) p1));
        return;
    }

    private static void b(android.content.Context p1, android.app.PendingIntent p2)
    {
        ((android.media.AudioManager) p1.getSystemService("audio")).registerMediaButtonEventReceiver(p2);
        return;
    }
}
