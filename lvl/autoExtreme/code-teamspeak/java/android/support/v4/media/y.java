package android.support.v4.media;
final class y implements android.view.ViewTreeObserver$OnWindowAttachListener {
    final synthetic android.support.v4.media.x a;

    y(android.support.v4.media.x p1)
    {
        this.a = p1;
        return;
    }

    public final void onWindowAttached()
    {
        android.support.v4.media.x v0 = this.a;
        v0.a.registerReceiver(v0.j, v0.f);
        v0.l = android.app.PendingIntent.getBroadcast(v0.a, 0, v0.g, 268435456);
        v0.m = new android.media.RemoteControlClient(v0.l);
        v0.m.setOnGetPlaybackPositionListener(v0);
        v0.m.setPlaybackPositionUpdateListener(v0);
        return;
    }

    public final void onWindowDetached()
    {
        this.a.d();
        return;
    }
}
