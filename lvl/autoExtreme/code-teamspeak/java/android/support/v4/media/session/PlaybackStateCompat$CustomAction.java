package android.support.v4.media.session;
public final class PlaybackStateCompat$CustomAction implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR;
    final String a;
    final CharSequence b;
    final int c;
    final android.os.Bundle d;
    Object e;

    static PlaybackStateCompat$CustomAction()
    {
        android.support.v4.media.session.PlaybackStateCompat$CustomAction.CREATOR = new android.support.v4.media.session.bm();
        return;
    }

    private PlaybackStateCompat$CustomAction(android.os.Parcel p2)
    {
        this.a = p2.readString();
        this.b = ((CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(p2));
        this.c = p2.readInt();
        this.d = p2.readBundle();
        return;
    }

    synthetic PlaybackStateCompat$CustomAction(android.os.Parcel p1, byte p2)
    {
        this(p1);
        return;
    }

    private PlaybackStateCompat$CustomAction(String p1, CharSequence p2, int p3, android.os.Bundle p4)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        this.d = p4;
        return;
    }

    synthetic PlaybackStateCompat$CustomAction(String p1, CharSequence p2, int p3, android.os.Bundle p4, byte p5)
    {
        this(p1, p2, p3, p4);
        return;
    }

    public static android.support.v4.media.session.PlaybackStateCompat$CustomAction a(Object p5)
    {
        if ((p5 != null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            android.support.v4.media.session.PlaybackStateCompat$CustomAction v1_2 = new android.support.v4.media.session.PlaybackStateCompat$CustomAction(((android.media.session.PlaybackState$CustomAction) p5).getAction(), ((android.media.session.PlaybackState$CustomAction) p5).getName(), ((android.media.session.PlaybackState$CustomAction) p5).getIcon(), ((android.media.session.PlaybackState$CustomAction) p5).getExtras());
            v1_2.e = p5;
            android.support.v4.media.session.PlaybackStateCompat$CustomAction v0_10 = v1_2;
        } else {
            v0_10 = 0;
        }
        return v0_10;
    }

    private Object a()
    {
        if ((this.e == null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            android.os.Bundle v3 = this.d;
            android.media.session.PlaybackState$CustomAction$Builder v4_1 = new android.media.session.PlaybackState$CustomAction$Builder(this.a, this.b, this.c);
            v4_1.setExtras(v3);
            this.e = v4_1.build();
            Object v0_4 = this.e;
        } else {
            v0_4 = this.e;
        }
        return v0_4;
    }

    private String b()
    {
        return this.a;
    }

    private CharSequence c()
    {
        return this.b;
    }

    private int d()
    {
        return this.c;
    }

    private android.os.Bundle e()
    {
        return this.d;
    }

    public final int describeContents()
    {
        return 0;
    }

    public final String toString()
    {
        return new StringBuilder("Action:mName=\'").append(this.b).append(", mIcon=").append(this.c).append(", mExtras=").append(this.d).toString();
    }

    public final void writeToParcel(android.os.Parcel p2, int p3)
    {
        p2.writeString(this.a);
        android.text.TextUtils.writeToParcel(this.b, p2, p3);
        p2.writeInt(this.c);
        p2.writeBundle(this.d);
        return;
    }
}
