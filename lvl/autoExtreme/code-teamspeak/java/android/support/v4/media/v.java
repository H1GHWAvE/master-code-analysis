package android.support.v4.media;
final class v implements android.view.KeyEvent$Callback {
    final synthetic android.support.v4.media.t a;

    v(android.support.v4.media.t p1)
    {
        this.a = p1;
        return;
    }

    public final boolean onKeyDown(int p2, android.view.KeyEvent p3)
    {
        int v0_1;
        if (!android.support.v4.media.t.a(p2)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean onKeyLongPress(int p2, android.view.KeyEvent p3)
    {
        return 0;
    }

    public final boolean onKeyMultiple(int p2, int p3, android.view.KeyEvent p4)
    {
        return 0;
    }

    public final boolean onKeyUp(int p2, android.view.KeyEvent p3)
    {
        int v0_1;
        if (!android.support.v4.media.t.a(p2)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }
}
