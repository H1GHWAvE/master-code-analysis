package android.support.v4.media.session;
public final class MediaSessionCompat$QueueItem implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR = None;
    public static final int a = 255;
    final android.support.v4.media.MediaDescriptionCompat b;
    final long c;
    Object d;

    static MediaSessionCompat$QueueItem()
    {
        android.support.v4.media.session.MediaSessionCompat$QueueItem.CREATOR = new android.support.v4.media.session.ap();
        return;
    }

    private MediaSessionCompat$QueueItem(android.os.Parcel p3)
    {
        this.b = ((android.support.v4.media.MediaDescriptionCompat) android.support.v4.media.MediaDescriptionCompat.CREATOR.createFromParcel(p3));
        this.c = p3.readLong();
        return;
    }

    synthetic MediaSessionCompat$QueueItem(android.os.Parcel p1, byte p2)
    {
        this(p1);
        return;
    }

    private MediaSessionCompat$QueueItem(android.support.v4.media.MediaDescriptionCompat p3, long p4)
    {
        this(0, p3, p4);
        return;
    }

    private MediaSessionCompat$QueueItem(Object p4, android.support.v4.media.MediaDescriptionCompat p5, long p6)
    {
        if (p5 != null) {
            if (p6 != -1) {
                this.b = p5;
                this.c = p6;
                this.d = p4;
                return;
            } else {
                throw new IllegalArgumentException("Id cannot be QueueItem.UNKNOWN_ID");
            }
        } else {
            throw new IllegalArgumentException("Description cannot be null.");
        }
    }

    private android.support.v4.media.MediaDescriptionCompat a()
    {
        return this.b;
    }

    public static android.support.v4.media.session.MediaSessionCompat$QueueItem a(Object p4)
    {
        return new android.support.v4.media.session.MediaSessionCompat$QueueItem(p4, android.support.v4.media.MediaDescriptionCompat.a(((android.media.session.MediaSession$QueueItem) p4).getDescription()), ((android.media.session.MediaSession$QueueItem) p4).getQueueId());
    }

    private long b()
    {
        return this.c;
    }

    private Object c()
    {
        if ((this.d == null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            this.d = new android.media.session.MediaSession$QueueItem(((android.media.MediaDescription) this.b.a()), this.c);
            Object v0_5 = this.d;
        } else {
            v0_5 = this.d;
        }
        return v0_5;
    }

    public final int describeContents()
    {
        return 0;
    }

    public final String toString()
    {
        return new StringBuilder("MediaSession.QueueItem {Description=").append(this.b).append(", Id=").append(this.c).append(" }").toString();
    }

    public final void writeToParcel(android.os.Parcel p3, int p4)
    {
        this.b.writeToParcel(p3, p4);
        p3.writeLong(this.c);
        return;
    }
}
