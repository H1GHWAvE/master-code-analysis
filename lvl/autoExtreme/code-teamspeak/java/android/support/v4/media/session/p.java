package android.support.v4.media.session;
final class p implements android.support.v4.media.session.m {
    private android.support.v4.media.session.MediaSessionCompat$Token a;
    private android.support.v4.media.session.d b;
    private android.support.v4.media.session.r c;

    public p(android.support.v4.media.session.MediaSessionCompat$Token p2)
    {
        this.a = p2;
        this.b = android.support.v4.media.session.e.a(((android.os.IBinder) p2.a));
        return;
    }

    public final android.support.v4.media.session.r a()
    {
        if (this.c == null) {
            this.c = new android.support.v4.media.session.u(this.b);
        }
        return this.c;
    }

    public final void a(int p5, int p6)
    {
        try {
            this.b.b(p5, p6, 0);
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in setVolumeTo. ").append(v0_1).toString());
        }
        return;
    }

    public final void a(android.support.v4.media.session.i p5)
    {
        if (p5 != null) {
            try {
                this.b.b(((android.support.v4.media.session.a) android.support.v4.media.session.i.c(p5)));
                this.b.asBinder().unlinkToDeath(p5, 0);
                android.support.v4.media.session.i.a(p5, 0);
            } catch (String v0_5) {
                android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in unregisterCallback. ").append(v0_5).toString());
            }
            return;
        } else {
            throw new IllegalArgumentException("callback may not be null.");
        }
    }

    public final void a(android.support.v4.media.session.i p5, android.os.Handler p6)
    {
        if (p5 != null) {
            try {
                this.b.asBinder().linkToDeath(p5, 0);
                this.b.a(((android.support.v4.media.session.a) android.support.v4.media.session.i.c(p5)));
                android.support.v4.media.session.i.a(p5, p6);
                android.support.v4.media.session.i.a(p5, 1);
            } catch (String v0_5) {
                android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in registerCallback. ").append(v0_5).toString());
            }
            return;
        } else {
            throw new IllegalArgumentException("callback may not be null.");
        }
    }

    public final void a(String p5, android.os.Bundle p6, android.os.ResultReceiver p7)
    {
        try {
            this.b.a(p5, p6, new android.support.v4.media.session.MediaSessionCompat$ResultReceiverWrapper(p7));
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in sendCommand. ").append(v0_1).toString());
        }
        return;
    }

    public final boolean a(android.view.KeyEvent p5)
    {
        if (p5 != null) {
            try {
                this.b.a(p5);
            } catch (int v0_1) {
                android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in dispatchMediaButtonEvent. ").append(v0_1).toString());
            }
            return 0;
        } else {
            throw new IllegalArgumentException("event may not be null.");
        }
    }

    public final android.support.v4.media.session.PlaybackStateCompat b()
    {
        try {
            int v0_1 = this.b.o();
        } catch (int v0_2) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in getPlaybackState. ").append(v0_2).toString());
            v0_1 = 0;
        }
        return v0_1;
    }

    public final void b(int p5, int p6)
    {
        try {
            this.b.a(p5, p6, 0);
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in adjustVolume. ").append(v0_1).toString());
        }
        return;
    }

    public final android.support.v4.media.MediaMetadataCompat c()
    {
        try {
            int v0_1 = this.b.n();
        } catch (int v0_2) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in getMetadata. ").append(v0_2).toString());
            v0_1 = 0;
        }
        return v0_1;
    }

    public final java.util.List d()
    {
        try {
            int v0_1 = this.b.p();
        } catch (int v0_2) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in getQueue. ").append(v0_2).toString());
            v0_1 = 0;
        }
        return v0_1;
    }

    public final CharSequence e()
    {
        try {
            int v0_1 = this.b.q();
        } catch (int v0_2) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in getQueueTitle. ").append(v0_2).toString());
            v0_1 = 0;
        }
        return v0_1;
    }

    public final android.os.Bundle f()
    {
        try {
            int v0_1 = this.b.r();
        } catch (int v0_2) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in getExtras. ").append(v0_2).toString());
            v0_1 = 0;
        }
        return v0_1;
    }

    public final int g()
    {
        try {
            int v0_1 = this.b.s();
        } catch (int v0_2) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in getRatingType. ").append(v0_2).toString());
            v0_1 = 0;
        }
        return v0_1;
    }

    public final long h()
    {
        try {
            long v0_1 = this.b.e();
        } catch (long v0_2) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in getFlags. ").append(v0_2).toString());
            v0_1 = 0;
        }
        return v0_1;
    }

    public final android.support.v4.media.session.q i()
    {
        try {
            int v5_0 = this.b.f();
            int v0_2 = new android.support.v4.media.session.q(v5_0.a, v5_0.b, v5_0.c, v5_0.d, v5_0.e);
        } catch (int v0_3) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in getPlaybackInfo. ").append(v0_3).toString());
            v0_2 = 0;
        }
        return v0_2;
    }

    public final android.app.PendingIntent j()
    {
        try {
            int v0_1 = this.b.d();
        } catch (int v0_2) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in getSessionActivity. ").append(v0_2).toString());
            v0_1 = 0;
        }
        return v0_1;
    }

    public final String k()
    {
        try {
            int v0_1 = this.b.b();
        } catch (int v0_2) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in getPackageName. ").append(v0_2).toString());
            v0_1 = 0;
        }
        return v0_1;
    }

    public final Object l()
    {
        return 0;
    }
}
