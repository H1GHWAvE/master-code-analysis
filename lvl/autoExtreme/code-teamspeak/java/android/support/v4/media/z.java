package android.support.v4.media;
final class z implements android.view.ViewTreeObserver$OnWindowFocusChangeListener {
    final synthetic android.support.v4.media.x a;

    z(android.support.v4.media.x p1)
    {
        this.a = p1;
        return;
    }

    public final void onWindowFocusChanged(boolean p4)
    {
        if (!p4) {
            this.a.c();
        } else {
            android.support.v4.media.x v0_1 = this.a;
            if (!v0_1.n) {
                v0_1.n = 1;
                v0_1.b.registerMediaButtonEventReceiver(v0_1.l);
                v0_1.b.registerRemoteControlClient(v0_1.m);
                if (v0_1.o == 3) {
                    v0_1.a();
                }
            }
        }
        return;
    }
}
