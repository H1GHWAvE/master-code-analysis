package android.support.v4.media;
public final class MediaMetadataCompat implements android.os.Parcelable {
    public static final String A = "android.media.metadata.MEDIA_ID";
    public static final android.support.v4.n.a B = None;
    public static final android.os.Parcelable$Creator CREATOR = None;
    private static final String E = "MediaMetadata";
    private static final int F = 0;
    private static final int G = 1;
    private static final int H = 2;
    private static final int I = 3;
    private static final String[] J = None;
    private static final String[] K = None;
    private static final String[] L = None;
    public static final String a = "android.media.metadata.TITLE";
    public static final String b = "android.media.metadata.ARTIST";
    public static final String c = "android.media.metadata.DURATION";
    public static final String d = "android.media.metadata.ALBUM";
    public static final String e = "android.media.metadata.AUTHOR";
    public static final String f = "android.media.metadata.WRITER";
    public static final String g = "android.media.metadata.COMPOSER";
    public static final String h = "android.media.metadata.COMPILATION";
    public static final String i = "android.media.metadata.DATE";
    public static final String j = "android.media.metadata.YEAR";
    public static final String k = "android.media.metadata.GENRE";
    public static final String l = "android.media.metadata.TRACK_NUMBER";
    public static final String m = "android.media.metadata.NUM_TRACKS";
    public static final String n = "android.media.metadata.DISC_NUMBER";
    public static final String o = "android.media.metadata.ALBUM_ARTIST";
    public static final String p = "android.media.metadata.ART";
    public static final String q = "android.media.metadata.ART_URI";
    public static final String r = "android.media.metadata.ALBUM_ART";
    public static final String s = "android.media.metadata.ALBUM_ART_URI";
    public static final String t = "android.media.metadata.USER_RATING";
    public static final String u = "android.media.metadata.RATING";
    public static final String v = "android.media.metadata.DISPLAY_TITLE";
    public static final String w = "android.media.metadata.DISPLAY_SUBTITLE";
    public static final String x = "android.media.metadata.DISPLAY_DESCRIPTION";
    public static final String y = "android.media.metadata.DISPLAY_ICON";
    public static final String z = "android.media.metadata.DISPLAY_ICON_URI";
    public final android.os.Bundle C;
    public Object D;
    private android.support.v4.media.MediaDescriptionCompat M;

    static MediaMetadataCompat()
    {
        android.support.v4.media.g v0_1 = new android.support.v4.n.a();
        android.support.v4.media.MediaMetadataCompat.B = v0_1;
        v0_1.put("android.media.metadata.TITLE", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.ARTIST", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.DURATION", Integer.valueOf(0));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.ALBUM", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.AUTHOR", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.WRITER", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.COMPOSER", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.COMPILATION", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.DATE", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.YEAR", Integer.valueOf(0));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.GENRE", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.TRACK_NUMBER", Integer.valueOf(0));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.NUM_TRACKS", Integer.valueOf(0));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.DISC_NUMBER", Integer.valueOf(0));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.ALBUM_ARTIST", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.ART", Integer.valueOf(2));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.ART_URI", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.ALBUM_ART", Integer.valueOf(2));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.ALBUM_ART_URI", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.USER_RATING", Integer.valueOf(3));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.RATING", Integer.valueOf(3));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.DISPLAY_TITLE", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.DISPLAY_SUBTITLE", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.DISPLAY_DESCRIPTION", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.DISPLAY_ICON", Integer.valueOf(2));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.DISPLAY_ICON_URI", Integer.valueOf(1));
        android.support.v4.media.MediaMetadataCompat.B.put("android.media.metadata.MEDIA_ID", Integer.valueOf(1));
        android.support.v4.media.g v0_29 = new String[7];
        v0_29[0] = "android.media.metadata.TITLE";
        v0_29[1] = "android.media.metadata.ARTIST";
        v0_29[2] = "android.media.metadata.ALBUM";
        v0_29[3] = "android.media.metadata.ALBUM_ARTIST";
        v0_29[4] = "android.media.metadata.WRITER";
        v0_29[5] = "android.media.metadata.AUTHOR";
        v0_29[6] = "android.media.metadata.COMPOSER";
        android.support.v4.media.MediaMetadataCompat.J = v0_29;
        android.support.v4.media.g v0_30 = new String[3];
        v0_30[0] = "android.media.metadata.DISPLAY_ICON";
        v0_30[1] = "android.media.metadata.ART";
        v0_30[2] = "android.media.metadata.ALBUM_ART";
        android.support.v4.media.MediaMetadataCompat.K = v0_30;
        android.support.v4.media.g v0_31 = new String[3];
        v0_31[0] = "android.media.metadata.DISPLAY_ICON_URI";
        v0_31[1] = "android.media.metadata.ART_URI";
        v0_31[2] = "android.media.metadata.ALBUM_ART_URI";
        android.support.v4.media.MediaMetadataCompat.L = v0_31;
        android.support.v4.media.MediaMetadataCompat.CREATOR = new android.support.v4.media.g();
        return;
    }

    private MediaMetadataCompat(android.os.Bundle p2)
    {
        this.C = new android.os.Bundle(p2);
        return;
    }

    synthetic MediaMetadataCompat(android.os.Bundle p1, byte p2)
    {
        this(p1);
        return;
    }

    private MediaMetadataCompat(android.os.Parcel p2)
    {
        this.C = p2.readBundle();
        return;
    }

    synthetic MediaMetadataCompat(android.os.Parcel p1, byte p2)
    {
        this(p1);
        return;
    }

    static synthetic android.os.Bundle a(android.support.v4.media.MediaMetadataCompat p1)
    {
        return p1.C;
    }

    public static android.support.v4.media.MediaMetadataCompat a(Object p6)
    {
        if ((p6 != null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            String v2_1 = new android.support.v4.media.i();
            String v3_0 = ((android.media.MediaMetadata) p6).keySet().iterator();
            while (v3_0.hasNext()) {
                String v0_8 = ((String) v3_0.next());
                IllegalArgumentException v1_4 = ((Integer) android.support.v4.media.MediaMetadataCompat.B.get(v0_8));
                if (v1_4 != null) {
                    switch (v1_4.intValue()) {
                        case 0:
                            android.support.v4.media.RatingCompat v4_3 = ((android.media.MediaMetadata) p6).getLong(v0_8);
                            if ((!android.support.v4.media.MediaMetadataCompat.B.containsKey(v0_8)) || (((Integer) android.support.v4.media.MediaMetadataCompat.B.get(v0_8)).intValue() == 0)) {
                                v2_1.a.putLong(v0_8, v4_3);
                            } else {
                                throw new IllegalArgumentException(new StringBuilder("The ").append(v0_8).append(" key cannot be used to put a long").toString());
                            }
                            break;
                        case 1:
                            android.support.v4.media.RatingCompat v4_2 = ((android.media.MediaMetadata) p6).getText(v0_8);
                            if ((!android.support.v4.media.MediaMetadataCompat.B.containsKey(v0_8)) || (((Integer) android.support.v4.media.MediaMetadataCompat.B.get(v0_8)).intValue() == 1)) {
                                v2_1.a.putCharSequence(v0_8, v4_2);
                            } else {
                                throw new IllegalArgumentException(new StringBuilder("The ").append(v0_8).append(" key cannot be used to put a CharSequence").toString());
                            }
                            break;
                        case 2:
                            android.support.v4.media.RatingCompat v4_1 = ((android.media.MediaMetadata) p6).getBitmap(v0_8);
                            if ((!android.support.v4.media.MediaMetadataCompat.B.containsKey(v0_8)) || (((Integer) android.support.v4.media.MediaMetadataCompat.B.get(v0_8)).intValue() == 2)) {
                                v2_1.a.putParcelable(v0_8, v4_1);
                            } else {
                                throw new IllegalArgumentException(new StringBuilder("The ").append(v0_8).append(" key cannot be used to put a Bitmap").toString());
                            }
                            break;
                        case 3:
                            android.support.v4.media.RatingCompat v4_0 = android.support.v4.media.RatingCompat.a(((android.media.MediaMetadata) p6).getRating(v0_8));
                            if ((!android.support.v4.media.MediaMetadataCompat.B.containsKey(v0_8)) || (((Integer) android.support.v4.media.MediaMetadataCompat.B.get(v0_8)).intValue() == 3)) {
                                v2_1.a.putParcelable(v0_8, v4_0);
                            } else {
                                throw new IllegalArgumentException(new StringBuilder("The ").append(v0_8).append(" key cannot be used to put a Rating").toString());
                            }
                            break;
                        default:
                    }
                }
            }
            String v0_6 = new android.support.v4.media.MediaMetadataCompat(v2_1.a, 0);
            v0_6.D = p6;
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    static synthetic android.support.v4.n.a a()
    {
        return android.support.v4.media.MediaMetadataCompat.B;
    }

    private android.support.v4.media.MediaDescriptionCompat b()
    {
        android.support.v4.media.MediaDescriptionCompat v0_15;
        android.net.Uri v3 = 0;
        if (this.M == null) {
            String v5 = this.f("android.media.metadata.MEDIA_ID");
            CharSequence[] v6 = new CharSequence[3];
            android.support.v4.media.MediaDescriptionCompat v0_3 = this.a("android.media.metadata.DISPLAY_TITLE");
            if (android.text.TextUtils.isEmpty(v0_3)) {
                android.support.v4.media.MediaDescriptionCompat v0_4 = 0;
                int v1_1 = 0;
                while ((v1_1 < 3) && (v0_4 < android.support.v4.media.MediaMetadataCompat.J.length)) {
                    android.support.v4.media.MediaDescriptionCompat v0_7;
                    String v4_2 = (v0_4 + 1);
                    boolean v7_1 = this.a(android.support.v4.media.MediaMetadataCompat.J[v0_4]);
                    if (android.text.TextUtils.isEmpty(v7_1)) {
                        v0_7 = v1_1;
                    } else {
                        v0_7 = (v1_1 + 1);
                        v6[v1_1] = v7_1;
                    }
                    v1_1 = v0_7;
                    v0_4 = v4_2;
                }
            } else {
                v6[0] = v0_3;
                v6[1] = this.a("android.media.metadata.DISPLAY_SUBTITLE");
                v6[2] = this.a("android.media.metadata.DISPLAY_DESCRIPTION");
            }
            android.support.v4.media.MediaDescriptionCompat v0_12 = 0;
            while (v0_12 < android.support.v4.media.MediaMetadataCompat.K.length) {
                int v1_6 = this.d(android.support.v4.media.MediaMetadataCompat.K[v0_12]);
                if (v1_6 == 0) {
                    v0_12++;
                } else {
                    android.support.v4.media.MediaDescriptionCompat v0_13 = v1_6;
                }
                int v1_7 = 0;
                while (v1_7 < android.support.v4.media.MediaMetadataCompat.L.length) {
                    String v4_7 = this.f(android.support.v4.media.MediaMetadataCompat.L[v1_7]);
                    if (android.text.TextUtils.isEmpty(v4_7)) {
                        v1_7++;
                    } else {
                        v3 = android.net.Uri.parse(v4_7);
                        break;
                    }
                }
                int v1_9 = new android.support.v4.media.b();
                v1_9.a = v5;
                v1_9.b = v6[0];
                v1_9.c = v6[1];
                v1_9.d = v6[2];
                v1_9.e = v0_13;
                v1_9.f = v3;
                this.M = v1_9.a();
                v0_15 = this.M;
            }
            v0_13 = 0;
        } else {
            v0_15 = this.M;
        }
        return v0_15;
    }

    private int c()
    {
        return this.C.size();
    }

    private java.util.Set d()
    {
        return this.C.keySet();
    }

    private android.os.Bundle e()
    {
        return this.C;
    }

    private boolean e(String p2)
    {
        return this.C.containsKey(p2);
    }

    private Object f()
    {
        if ((this.D == null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            android.media.MediaMetadata$Builder v2_1 = new android.media.MediaMetadata$Builder();
            java.util.Iterator v4 = this.C.keySet().iterator();
            while (v4.hasNext()) {
                Object v0_8 = ((String) v4.next());
                android.media.MediaMetadata$Builder v1_3 = ((Integer) android.support.v4.media.MediaMetadataCompat.B.get(v0_8));
                if (v1_3 != null) {
                    switch (v1_3.intValue()) {
                        case 0:
                            ((android.media.MediaMetadata$Builder) v2_1).putLong(v0_8, this.b(v0_8));
                            break;
                        case 1:
                            ((android.media.MediaMetadata$Builder) v2_1).putText(v0_8, this.a(v0_8));
                            break;
                        case 2:
                            ((android.media.MediaMetadata$Builder) v2_1).putBitmap(v0_8, this.d(v0_8));
                            break;
                        case 3:
                            ((android.media.MediaMetadata$Builder) v2_1).putRating(v0_8, ((android.media.Rating) this.c(v0_8).a()));
                            break;
                        default:
                    }
                }
            }
            this.D = ((android.media.MediaMetadata$Builder) v2_1).build();
            Object v0_6 = this.D;
        } else {
            v0_6 = this.D;
        }
        return v0_6;
    }

    private String f(String p2)
    {
        int v0_2;
        int v0_1 = this.C.getCharSequence(p2);
        if (v0_1 == 0) {
            v0_2 = 0;
        } else {
            v0_2 = v0_1.toString();
        }
        return v0_2;
    }

    public final CharSequence a(String p2)
    {
        return this.C.getCharSequence(p2);
    }

    public final long b(String p5)
    {
        return this.C.getLong(p5, 0);
    }

    public final android.support.v4.media.RatingCompat c(String p5)
    {
        try {
            int v0_2 = ((android.support.v4.media.RatingCompat) this.C.getParcelable(p5));
        } catch (int v0_3) {
            android.util.Log.w("MediaMetadata", "Failed to retrieve a key as Rating.", v0_3);
            v0_2 = 0;
        }
        return v0_2;
    }

    public final android.graphics.Bitmap d(String p5)
    {
        try {
            int v0_2 = ((android.graphics.Bitmap) this.C.getParcelable(p5));
        } catch (int v0_3) {
            android.util.Log.w("MediaMetadata", "Failed to retrieve a key as Bitmap.", v0_3);
            v0_2 = 0;
        }
        return v0_2;
    }

    public final int describeContents()
    {
        return 0;
    }

    public final void writeToParcel(android.os.Parcel p2, int p3)
    {
        p2.writeBundle(this.C);
        return;
    }
}
