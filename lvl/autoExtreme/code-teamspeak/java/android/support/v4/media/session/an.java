package android.support.v4.media.session;
final class an extends android.os.Handler {
    private static final int b = 1;
    private static final int c = 2;
    private static final int d = 3;
    private static final int e = 4;
    private static final int f = 5;
    private static final int g = 6;
    private static final int h = 7;
    private static final int i = 8;
    private static final int j = 9;
    private static final int k = 10;
    private static final int l = 11;
    private static final int m = 12;
    private static final int n = 13;
    private static final int o = 14;
    private static final int p = 15;
    private static final int q = 16;
    private static final int r = 17;
    private static final int s = 18;
    private static final int t = 127;
    private static final int u = 126;
    final synthetic android.support.v4.media.session.ai a;

    public an(android.support.v4.media.session.ai p1, android.os.Looper p2)
    {
        this.a = p1;
        this(p2);
        return;
    }

    private void a(int p2)
    {
        this.a(p2, 0);
        return;
    }

    private void a(int p2, Object p3, int p4)
    {
        this.obtainMessage(p2, p4, 0, p3).sendToTarget();
        return;
    }

    private void a(android.view.KeyEvent p7)
    {
        if ((p7 != null) && (p7.getAction() == 0)) {
            int v0_5;
            if (this.a.k != null) {
                v0_5 = this.a.k.F;
            } else {
                v0_5 = 0;
            }
            switch (p7.getKeyCode()) {
                case 79:
                case 85:
                    // Both branches of the condition point to the same code.
                    // if (this.a.k == null) {
                    // }
                    break;
                case 86:
                    // Both branches of the condition point to the same code.
                    // if ((v0_5 & 1) == 0) {
                    // }
                    break;
                case 87:
                    // Both branches of the condition point to the same code.
                    // if ((v0_5 & 32) == 0) {
                    // }
                    break;
                case 88:
                    // Both branches of the condition point to the same code.
                    // if ((v0_5 & 16) == 0) {
                    // }
                    break;
                case 89:
                    // Both branches of the condition point to the same code.
                    // if ((v0_5 & 8) == 0) {
                    // }
                    break;
                case 90:
                    // Both branches of the condition point to the same code.
                    // if ((v0_5 & 64) == 0) {
                    // }
                    break;
                case 126:
                    // Both branches of the condition point to the same code.
                    // if ((v0_5 & 4) == 0) {
                    // }
                    break;
                case 127:
                    // Both branches of the condition point to the same code.
                    // if ((v0_5 & 2) == 0) {
                    // }
                    break;
                default:
            }
        }
        return;
    }

    public final void a(int p2, Object p3)
    {
        this.obtainMessage(p2, p3).sendToTarget();
        return;
    }

    public final void a(int p2, Object p3, android.os.Bundle p4)
    {
        android.os.Message v0 = this.obtainMessage(p2, p3);
        v0.setData(p4);
        v0.sendToTarget();
        return;
    }

    public final void handleMessage(android.os.Message p7)
    {
        if (this.a.h != null) {
            switch (p7.what) {
                case 1:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    break;
                case 2:
                    p7.getData();
                    break;
                case 3:
                    p7.getData();
                    break;
                case 4:
                    ((Long) p7.obj).longValue();
                case 1:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    break;
                case 1:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    break;
                case 1:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    break;
                case 1:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    break;
                case 1:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    break;
                case 1:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    break;
                case 11:
                    ((Long) p7.obj).longValue();
                    break;
                case 12:
                    break;
                case 13:
                    p7.getData();
                    break;
                case 14:
                    int v0_10 = ((android.view.KeyEvent) p7.obj);
                    new android.content.Intent("android.intent.action.MEDIA_BUTTON").putExtra("android.intent.extra.KEY_EVENT", v0_10);
                    if ((v0_10 != 0) && (v0_10.getAction() == 0)) {
                        long v2_3;
                        if (this.a.k != null) {
                            v2_3 = this.a.k.F;
                        } else {
                            v2_3 = 0;
                        }
                        switch (v0_10.getKeyCode()) {
                            case 79:
                            case 85:
                                // Both branches of the condition point to the same code.
                                // if (this.a.k == null) {
                                // }
                                break;
                            case 86:
                                // Both branches of the condition point to the same code.
                                // if ((1 & v2_3) == 0) {
                                // }
                                break;
                            case 87:
                                // Both branches of the condition point to the same code.
                                // if ((32 & v2_3) == 0) {
                                // }
                                break;
                            case 88:
                                // Both branches of the condition point to the same code.
                                // if ((16 & v2_3) == 0) {
                                // }
                                break;
                            case 89:
                                // Both branches of the condition point to the same code.
                                // if ((8 & v2_3) == 0) {
                                // }
                                break;
                            case 90:
                                // Both branches of the condition point to the same code.
                                // if ((64 & v2_3) == 0) {
                                // }
                                break;
                            case 126:
                                // Both branches of the condition point to the same code.
                                // if ((4 & v2_3) == 0) {
                                // }
                                break;
                            case 127:
                                // Both branches of the condition point to the same code.
                                // if ((2 & v2_3) == 0) {
                                // }
                                break;
                            default:
                        }
                    }
                    break;
                case 15:
                    break;
                case 16:
                    android.support.v4.media.session.ai.a(this.a, ((Integer) p7.obj).intValue(), 0);
                    break;
                case 17:
                    android.support.v4.media.session.ai.b(this.a, ((Integer) p7.obj).intValue(), 0);
                    break;
                case 18:
                    p7.getData();
                    break;
                default:
            }
        }
        return;
    }
}
