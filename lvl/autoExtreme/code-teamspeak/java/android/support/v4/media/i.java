package android.support.v4.media;
public final class i {
    final android.os.Bundle a;

    public i()
    {
        this.a = new android.os.Bundle();
        return;
    }

    private i(android.support.v4.media.MediaMetadataCompat p3)
    {
        this.a = new android.os.Bundle(android.support.v4.media.MediaMetadataCompat.a(p3));
        return;
    }

    private android.support.v4.media.MediaMetadataCompat a()
    {
        return new android.support.v4.media.MediaMetadataCompat(this.a, 0);
    }

    private android.support.v4.media.i a(String p5, long p6)
    {
        if ((!android.support.v4.media.MediaMetadataCompat.a().containsKey(p5)) || (((Integer) android.support.v4.media.MediaMetadataCompat.a().get(p5)).intValue() == 0)) {
            this.a.putLong(p5, p6);
            return this;
        } else {
            throw new IllegalArgumentException(new StringBuilder("The ").append(p5).append(" key cannot be used to put a long").toString());
        }
    }

    private android.support.v4.media.i a(String p4, android.graphics.Bitmap p5)
    {
        if ((!android.support.v4.media.MediaMetadataCompat.a().containsKey(p4)) || (((Integer) android.support.v4.media.MediaMetadataCompat.a().get(p4)).intValue() == 2)) {
            this.a.putParcelable(p4, p5);
            return this;
        } else {
            throw new IllegalArgumentException(new StringBuilder("The ").append(p4).append(" key cannot be used to put a Bitmap").toString());
        }
    }

    private android.support.v4.media.i a(String p4, android.support.v4.media.RatingCompat p5)
    {
        if ((!android.support.v4.media.MediaMetadataCompat.a().containsKey(p4)) || (((Integer) android.support.v4.media.MediaMetadataCompat.a().get(p4)).intValue() == 3)) {
            this.a.putParcelable(p4, p5);
            return this;
        } else {
            throw new IllegalArgumentException(new StringBuilder("The ").append(p4).append(" key cannot be used to put a Rating").toString());
        }
    }

    private android.support.v4.media.i a(String p4, CharSequence p5)
    {
        if ((!android.support.v4.media.MediaMetadataCompat.a().containsKey(p4)) || (((Integer) android.support.v4.media.MediaMetadataCompat.a().get(p4)).intValue() == 1)) {
            this.a.putCharSequence(p4, p5);
            return this;
        } else {
            throw new IllegalArgumentException(new StringBuilder("The ").append(p4).append(" key cannot be used to put a CharSequence").toString());
        }
    }

    private android.support.v4.media.i a(String p4, String p5)
    {
        if ((!android.support.v4.media.MediaMetadataCompat.a().containsKey(p4)) || (((Integer) android.support.v4.media.MediaMetadataCompat.a().get(p4)).intValue() == 1)) {
            this.a.putCharSequence(p4, p5);
            return this;
        } else {
            throw new IllegalArgumentException(new StringBuilder("The ").append(p4).append(" key cannot be used to put a String").toString());
        }
    }
}
