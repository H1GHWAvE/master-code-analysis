package android.support.v4.media;
final class m {

    m()
    {
        return;
    }

    private static android.graphics.Bitmap a(Object p1, String p2)
    {
        return ((android.media.MediaMetadata) p1).getBitmap(p2);
    }

    private static java.util.Set a(Object p1)
    {
        return ((android.media.MediaMetadata) p1).keySet();
    }

    private static long b(Object p2, String p3)
    {
        return ((android.media.MediaMetadata) p2).getLong(p3);
    }

    private static Object c(Object p1, String p2)
    {
        return ((android.media.MediaMetadata) p1).getRating(p2);
    }

    private static CharSequence d(Object p1, String p2)
    {
        return ((android.media.MediaMetadata) p1).getText(p2);
    }
}
