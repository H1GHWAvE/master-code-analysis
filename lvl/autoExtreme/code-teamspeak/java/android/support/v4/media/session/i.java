package android.support.v4.media.session;
public abstract class i implements android.os.IBinder$DeathRecipient {
    private final Object a;
    private android.support.v4.media.session.j b;
    private boolean c;

    public i()
    {
        this.c = 0;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            this.a = new android.support.v4.media.session.l(this, 0);
        } else {
            this.a = new android.support.v4.media.session.x(new android.support.v4.media.session.k(this, 0));
        }
        return;
    }

    static synthetic android.support.v4.media.session.j a(android.support.v4.media.session.i p1)
    {
        return p1.b;
    }

    private static void a()
    {
        return;
    }

    private void a(android.os.Handler p3)
    {
        this.b = new android.support.v4.media.session.j(this, p3.getLooper());
        return;
    }

    static synthetic void a(android.support.v4.media.session.i p2, android.os.Handler p3)
    {
        p2.b = new android.support.v4.media.session.j(p2, p3.getLooper());
        return;
    }

    static synthetic boolean a(android.support.v4.media.session.i p0, boolean p1)
    {
        p0.c = p1;
        return p1;
    }

    private static void b()
    {
        return;
    }

    static synthetic boolean b(android.support.v4.media.session.i p1)
    {
        return p1.c;
    }

    static synthetic Object c(android.support.v4.media.session.i p1)
    {
        return p1.a;
    }

    private static void c()
    {
        return;
    }

    private static void d()
    {
        return;
    }

    private static void e()
    {
        return;
    }

    private static void f()
    {
        return;
    }

    private static void g()
    {
        return;
    }

    private static void h()
    {
        return;
    }

    public void binderDied()
    {
        return;
    }
}
