package android.support.v4.media.session;
final class v {

    v()
    {
        return;
    }

    public static Object a(android.content.Context p1, Object p2)
    {
        return new android.media.session.MediaController(p1, ((android.media.session.MediaSession$Token) p2));
    }

    private static Object a(android.support.v4.media.session.w p1)
    {
        return new android.support.v4.media.session.x(p1);
    }

    private static Object a(Object p1)
    {
        return ((android.media.session.MediaController) p1).getTransportControls();
    }

    private static void a(Object p0, int p1, int p2)
    {
        ((android.media.session.MediaController) p0).setVolumeTo(p1, p2);
        return;
    }

    private static void a(Object p0, Object p1)
    {
        ((android.media.session.MediaController) p0).unregisterCallback(((android.media.session.MediaController$Callback) p1));
        return;
    }

    private static void a(Object p0, Object p1, android.os.Handler p2)
    {
        ((android.media.session.MediaController) p0).registerCallback(((android.media.session.MediaController$Callback) p1), p2);
        return;
    }

    private static void a(Object p0, String p1, android.os.Bundle p2, android.os.ResultReceiver p3)
    {
        ((android.media.session.MediaController) p0).sendCommand(p1, p2, p3);
        return;
    }

    private static boolean a(Object p1, android.view.KeyEvent p2)
    {
        return ((android.media.session.MediaController) p1).dispatchMediaButtonEvent(p2);
    }

    private static Object b(Object p1)
    {
        return ((android.media.session.MediaController) p1).getPlaybackState();
    }

    private static void b(Object p0, int p1, int p2)
    {
        ((android.media.session.MediaController) p0).adjustVolume(p1, p2);
        return;
    }

    private static Object c(Object p1)
    {
        return ((android.media.session.MediaController) p1).getMetadata();
    }

    private static java.util.List d(Object p2)
    {
        java.util.ArrayList v0_1;
        java.util.List v1 = ((android.media.session.MediaController) p2).getQueue();
        if (v1 != null) {
            v0_1 = new java.util.ArrayList(v1);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private static CharSequence e(Object p1)
    {
        return ((android.media.session.MediaController) p1).getQueueTitle();
    }

    private static android.os.Bundle f(Object p1)
    {
        return ((android.media.session.MediaController) p1).getExtras();
    }

    private static int g(Object p1)
    {
        return ((android.media.session.MediaController) p1).getRatingType();
    }

    private static long h(Object p2)
    {
        return ((android.media.session.MediaController) p2).getFlags();
    }

    private static Object i(Object p1)
    {
        return ((android.media.session.MediaController) p1).getPlaybackInfo();
    }

    private static android.app.PendingIntent j(Object p1)
    {
        return ((android.media.session.MediaController) p1).getSessionActivity();
    }

    private static String k(Object p1)
    {
        return ((android.media.session.MediaController) p1).getPackageName();
    }
}
