package android.support.v4.media;
public final class MediaDescriptionCompat implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR;
    private final String a;
    private final CharSequence b;
    private final CharSequence c;
    private final CharSequence d;
    private final android.graphics.Bitmap e;
    private final android.net.Uri f;
    private final android.os.Bundle g;
    private final android.net.Uri h;
    private Object i;

    static MediaDescriptionCompat()
    {
        android.support.v4.media.MediaDescriptionCompat.CREATOR = new android.support.v4.media.a();
        return;
    }

    private MediaDescriptionCompat(android.os.Parcel p3)
    {
        this.a = p3.readString();
        this.b = ((CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(p3));
        this.c = ((CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(p3));
        this.d = ((CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(p3));
        this.e = ((android.graphics.Bitmap) p3.readParcelable(0));
        this.f = ((android.net.Uri) p3.readParcelable(0));
        this.g = p3.readBundle();
        this.h = ((android.net.Uri) p3.readParcelable(0));
        return;
    }

    synthetic MediaDescriptionCompat(android.os.Parcel p1, byte p2)
    {
        this(p1);
        return;
    }

    private MediaDescriptionCompat(String p1, CharSequence p2, CharSequence p3, CharSequence p4, android.graphics.Bitmap p5, android.net.Uri p6, android.os.Bundle p7, android.net.Uri p8)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        this.d = p4;
        this.e = p5;
        this.f = p6;
        this.g = p7;
        this.h = p8;
        return;
    }

    synthetic MediaDescriptionCompat(String p1, CharSequence p2, CharSequence p3, CharSequence p4, android.graphics.Bitmap p5, android.net.Uri p6, android.os.Bundle p7, android.net.Uri p8, byte p9)
    {
        this(p1, p2, p3, p4, p5, p6, p7, p8);
        return;
    }

    public static android.support.v4.media.MediaDescriptionCompat a(Object p3)
    {
        if ((p3 != null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            android.support.v4.media.b v1_2 = new android.support.v4.media.b();
            v1_2.a = ((android.media.MediaDescription) p3).getMediaId();
            v1_2.b = ((android.media.MediaDescription) p3).getTitle();
            v1_2.c = ((android.media.MediaDescription) p3).getSubtitle();
            v1_2.d = ((android.media.MediaDescription) p3).getDescription();
            v1_2.e = ((android.media.MediaDescription) p3).getIconBitmap();
            v1_2.f = ((android.media.MediaDescription) p3).getIconUri();
            v1_2.g = ((android.media.MediaDescription) p3).getExtras();
            if (android.os.Build$VERSION.SDK_INT >= 23) {
                v1_2.h = ((android.media.MediaDescription) p3).getMediaUri();
            }
            android.support.v4.media.MediaDescriptionCompat v0_26 = v1_2.a();
            v0_26.i = p3;
        } else {
            v0_26 = 0;
        }
        return v0_26;
    }

    private String b()
    {
        return this.a;
    }

    private CharSequence c()
    {
        return this.b;
    }

    private CharSequence d()
    {
        return this.c;
    }

    private CharSequence e()
    {
        return this.d;
    }

    private android.graphics.Bitmap f()
    {
        return this.e;
    }

    private android.net.Uri g()
    {
        return this.f;
    }

    private android.os.Bundle h()
    {
        return this.g;
    }

    private android.net.Uri i()
    {
        return this.h;
    }

    public final Object a()
    {
        if ((this.i == null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            android.media.MediaDescription$Builder v1_2 = new android.media.MediaDescription$Builder();
            ((android.media.MediaDescription$Builder) v1_2).setMediaId(this.a);
            ((android.media.MediaDescription$Builder) v1_2).setTitle(this.b);
            ((android.media.MediaDescription$Builder) v1_2).setSubtitle(this.c);
            ((android.media.MediaDescription$Builder) v1_2).setDescription(this.d);
            ((android.media.MediaDescription$Builder) v1_2).setIconBitmap(this.e);
            ((android.media.MediaDescription$Builder) v1_2).setIconUri(this.f);
            ((android.media.MediaDescription$Builder) v1_2).setExtras(this.g);
            if (android.os.Build$VERSION.SDK_INT >= 23) {
                ((android.media.MediaDescription$Builder) v1_2).setMediaUri(this.h);
            }
            this.i = ((android.media.MediaDescription$Builder) v1_2).build();
            Object v0_20 = this.i;
        } else {
            v0_20 = this.i;
        }
        return v0_20;
    }

    public final int describeContents()
    {
        return 0;
    }

    public final String toString()
    {
        return new StringBuilder().append(this.b).append(", ").append(this.c).append(", ").append(this.d).toString();
    }

    public final void writeToParcel(android.os.Parcel p3, int p4)
    {
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            ((android.media.MediaDescription) this.a()).writeToParcel(p3, p4);
        } else {
            p3.writeString(this.a);
            android.text.TextUtils.writeToParcel(this.b, p3, p4);
            android.text.TextUtils.writeToParcel(this.c, p3, p4);
            android.text.TextUtils.writeToParcel(this.d, p3, p4);
            p3.writeParcelable(this.e, p4);
            p3.writeParcelable(this.f, p4);
            p3.writeBundle(this.g);
        }
        return;
    }
}
