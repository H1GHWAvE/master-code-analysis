package android.support.v4.media.session;
public final class q {
    public static final int a = 1;
    public static final int b = 2;
    private final int c;
    private final int d;
    private final int e;
    private final int f;
    private final int g;

    q(int p1, int p2, int p3, int p4, int p5)
    {
        this.c = p1;
        this.d = p2;
        this.e = p3;
        this.f = p4;
        this.g = p5;
        return;
    }

    private int a()
    {
        return this.c;
    }

    private int b()
    {
        return this.d;
    }

    private int c()
    {
        return this.e;
    }

    private int d()
    {
        return this.f;
    }

    private int e()
    {
        return this.g;
    }
}
