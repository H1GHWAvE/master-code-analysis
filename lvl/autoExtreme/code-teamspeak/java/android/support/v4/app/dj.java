package android.support.v4.app;
public final class dj implements android.support.v4.app.di {
    private static final String a = "android.wearable.EXTENSIONS";
    private static final String b = "flags";
    private static final String c = "inProgressLabel";
    private static final String d = "confirmLabel";
    private static final String e = "cancelLabel";
    private static final int f = 1;
    private static final int g = 1;
    private int h;
    private CharSequence i;
    private CharSequence j;
    private CharSequence k;

    public dj()
    {
        this.h = 1;
        return;
    }

    private dj(android.support.v4.app.df p4)
    {
        this.h = 1;
        CharSequence v0_1 = p4.a.getBundle("android.wearable.EXTENSIONS");
        if (v0_1 != null) {
            this.h = v0_1.getInt("flags", 1);
            this.i = v0_1.getCharSequence("inProgressLabel");
            this.j = v0_1.getCharSequence("confirmLabel");
            this.k = v0_1.getCharSequence("cancelLabel");
        }
        return;
    }

    private android.support.v4.app.dj a()
    {
        android.support.v4.app.dj v0_1 = new android.support.v4.app.dj();
        v0_1.h = this.h;
        v0_1.i = this.i;
        v0_1.j = this.j;
        v0_1.k = this.k;
        return v0_1;
    }

    private android.support.v4.app.dj a(CharSequence p1)
    {
        this.i = p1;
        return this;
    }

    private android.support.v4.app.dj a(boolean p2)
    {
        if (!p2) {
            this.h = (this.h & -2);
        } else {
            this.h = (this.h | 1);
        }
        return this;
    }

    private android.support.v4.app.dj b(CharSequence p1)
    {
        this.j = p1;
        return this;
    }

    private void b(boolean p2)
    {
        if (!p2) {
            this.h = (this.h & -2);
        } else {
            this.h = (this.h | 1);
        }
        return;
    }

    private boolean b()
    {
        int v0_2;
        if ((this.h & 1) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private android.support.v4.app.dj c(CharSequence p1)
    {
        this.k = p1;
        return this;
    }

    private CharSequence c()
    {
        return this.i;
    }

    private CharSequence d()
    {
        return this.j;
    }

    private CharSequence e()
    {
        return this.k;
    }

    public final android.support.v4.app.dh a(android.support.v4.app.dh p4)
    {
        android.os.Bundle v0_1 = new android.os.Bundle();
        if (this.h != 1) {
            v0_1.putInt("flags", this.h);
        }
        if (this.i != null) {
            v0_1.putCharSequence("inProgressLabel", this.i);
        }
        if (this.j != null) {
            v0_1.putCharSequence("confirmLabel", this.j);
        }
        if (this.k != null) {
            v0_1.putCharSequence("cancelLabel", this.k);
        }
        p4.a.putBundle("android.wearable.EXTENSIONS", v0_1);
        return p4;
    }

    public final synthetic Object clone()
    {
        android.support.v4.app.dj v0_1 = new android.support.v4.app.dj();
        v0_1.h = this.h;
        v0_1.i = this.i;
        v0_1.j = this.j;
        v0_1.k = this.k;
        return v0_1;
    }
}
