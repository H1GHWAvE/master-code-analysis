package android.support.v4.app;
final class aq implements android.os.Parcelable$Creator {

    aq()
    {
        return;
    }

    private static android.support.v4.app.BackStackState a(android.os.Parcel p1)
    {
        return new android.support.v4.app.BackStackState(p1);
    }

    private static android.support.v4.app.BackStackState[] a(int p1)
    {
        android.support.v4.app.BackStackState[] v0 = new android.support.v4.app.BackStackState[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return new android.support.v4.app.BackStackState(p2);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.app.BackStackState[] v0 = new android.support.v4.app.BackStackState[p2];
        return v0;
    }
}
