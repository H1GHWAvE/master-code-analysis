package android.support.v4.app;
final class bv implements android.os.Parcelable$Creator {

    bv()
    {
        return;
    }

    private static android.support.v4.app.FragmentManagerState a(android.os.Parcel p1)
    {
        return new android.support.v4.app.FragmentManagerState(p1);
    }

    private static android.support.v4.app.FragmentManagerState[] a(int p1)
    {
        android.support.v4.app.FragmentManagerState[] v0 = new android.support.v4.app.FragmentManagerState[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return new android.support.v4.app.FragmentManagerState(p2);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.app.FragmentManagerState[] v0 = new android.support.v4.app.FragmentManagerState[p2];
        return v0;
    }
}
