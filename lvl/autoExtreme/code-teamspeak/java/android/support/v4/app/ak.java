package android.support.v4.app;
final class ak extends android.support.v4.app.cd implements android.support.v4.app.bj, java.lang.Runnable {
    static final String a = "FragmentManager";
    static final boolean b = False;
    static final int d = 0;
    static final int e = 1;
    static final int f = 2;
    static final int g = 3;
    static final int h = 4;
    static final int i = 5;
    static final int j = 6;
    static final int k = 7;
    CharSequence A;
    int B;
    CharSequence C;
    java.util.ArrayList D;
    java.util.ArrayList E;
    final android.support.v4.app.bl c;
    android.support.v4.app.ao l;
    android.support.v4.app.ao m;
    int n;
    int o;
    int p;
    int q;
    int r;
    int s;
    int t;
    boolean u;
    boolean v;
    String w;
    boolean x;
    int y;
    int z;

    static ak()
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        android.support.v4.app.ak.b = v0_1;
        return;
    }

    public ak(android.support.v4.app.bl p2)
    {
        this.v = 1;
        this.y = -1;
        this.c = p2;
        return;
    }

    private int a(boolean p4)
    {
        if (!this.x) {
            if (android.support.v4.app.bl.b) {
                android.util.Log.v("FragmentManager", new StringBuilder("Commit: ").append(this).toString());
                this.a("  ", new java.io.PrintWriter(new android.support.v4.n.h("FragmentManager")));
            }
            this.x = 1;
            if (!this.u) {
                this.y = -1;
            } else {
                this.y = this.c.a(this);
            }
            this.c.a(this, p4);
            return this.y;
        } else {
            throw new IllegalStateException("commit already called");
        }
    }

    private android.support.v4.app.ap a(android.util.SparseArray p11, android.util.SparseArray p12, boolean p13)
    {
        int v7 = 0;
        int v2_1 = new android.support.v4.app.ap(this);
        v2_1.d = new android.view.View(this.c.u.c);
        int v6 = 0;
        int v8 = 0;
        while (v6 < p11.size()) {
            int v1_5;
            if (!this.a(p11.keyAt(v6), v2_1, p13, p11, p12)) {
                v1_5 = v8;
            } else {
                v1_5 = 1;
            }
            v6++;
            v8 = v1_5;
        }
        while (v7 < p12.size()) {
            int v1_3 = p12.keyAt(v7);
            if ((p11.get(v1_3) == null) && (this.a(v1_3, v2_1, p13, p11, p12))) {
                v8 = 1;
            }
            v7++;
        }
        if (v8 == 0) {
            v2_1 = 0;
        }
        return v2_1;
    }

    static synthetic android.support.v4.n.a a(android.support.v4.app.ak p4, android.support.v4.app.ap p5, boolean p6, android.support.v4.app.Fragment p7)
    {
        android.support.v4.n.a v0_1 = new android.support.v4.n.a();
        android.support.v4.app.gj v1_0 = p7.ac;
        if ((v1_0 != null) && (p4.D != null)) {
            android.support.v4.app.ce.a(v0_1, v1_0);
            if (!p6) {
                android.support.v4.n.k.c(v0_1, p4.E);
            } else {
                v0_1 = android.support.v4.app.ak.a(p4.D, p4.E, v0_1);
            }
        }
        if (!p6) {
            // Both branches of the condition point to the same code.
            // if (p7.ar == null) {
                android.support.v4.app.ak.b(p5, v0_1, 1);
            // }
        } else {
            // Both branches of the condition point to the same code.
            // if (p7.as == null) {
                p4.a(p5, v0_1, 1);
            // }
        }
        return v0_1;
    }

    private android.support.v4.n.a a(android.support.v4.app.ap p5, android.support.v4.app.Fragment p6, boolean p7)
    {
        android.support.v4.n.a v0_1 = new android.support.v4.n.a();
        if (this.D != null) {
            android.support.v4.app.ce.a(v0_1, p6.ac);
            if (!p7) {
                v0_1 = android.support.v4.app.ak.a(this.D, this.E, v0_1);
            } else {
                android.support.v4.n.k.c(v0_1, this.E);
            }
        }
        if (!p7) {
            // Both branches of the condition point to the same code.
            // if (p6.as == null) {
                android.support.v4.app.ak.b(p5, v0_1, 0);
            // }
        } else {
            // Both branches of the condition point to the same code.
            // if (p6.ar == null) {
                this.a(p5, v0_1, 0);
            // }
        }
        return v0_1;
    }

    private android.support.v4.n.a a(android.support.v4.app.ap p5, boolean p6, android.support.v4.app.Fragment p7)
    {
        android.support.v4.n.a v0_1 = new android.support.v4.n.a();
        android.support.v4.app.gj v1_0 = p7.ac;
        if ((v1_0 != null) && (this.D != null)) {
            android.support.v4.app.ce.a(v0_1, v1_0);
            if (!p6) {
                android.support.v4.n.k.c(v0_1, this.E);
            } else {
                v0_1 = android.support.v4.app.ak.a(this.D, this.E, v0_1);
            }
        }
        if (!p6) {
            // Both branches of the condition point to the same code.
            // if (p7.ar == null) {
                android.support.v4.app.ak.b(p5, v0_1, 1);
            // }
        } else {
            // Both branches of the condition point to the same code.
            // if (p7.as == null) {
                this.a(p5, v0_1, 1);
            // }
        }
        return v0_1;
    }

    private static android.support.v4.n.a a(java.util.ArrayList p5, java.util.ArrayList p6, android.support.v4.n.a p7)
    {
        if (!p7.isEmpty()) {
            android.support.v4.n.a v1_1 = new android.support.v4.n.a();
            int v3 = p5.size();
            int v2 = 0;
            while (v2 < v3) {
                int v0_4 = ((android.view.View) p7.get(p5.get(v2)));
                if (v0_4 != 0) {
                    v1_1.put(p6.get(v2), v0_4);
                }
                v2++;
            }
            p7 = v1_1;
        }
        return p7;
    }

    private static Object a(android.support.v4.app.Fragment p3, android.support.v4.app.Fragment p4, boolean p5)
    {
        if ((p3 != null) && (p4 != null)) {
            android.transition.TransitionSet v0_1;
            if (!p5) {
                p4 = p3;
                v0_1 = p4.an;
            } else {
                if (p4.ao == android.support.v4.app.Fragment.n) {
                } else {
                    v0_1 = p4.ao;
                }
            }
            if (v0_1 != null) {
                android.transition.TransitionSet v0_2 = ((android.transition.Transition) v0_1);
                if (v0_2 != null) {
                    android.transition.TransitionSet v1_2 = new android.transition.TransitionSet();
                    v1_2.addTransition(v0_2);
                    android.transition.TransitionSet v0_3 = v1_2;
                } else {
                    v0_3 = 0;
                }
            } else {
                v0_3 = 0;
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private static Object a(android.support.v4.app.Fragment p2, boolean p3)
    {
        Object v0_2;
        if (p2 != null) {
            Object v0_0;
            if (!p3) {
                v0_0 = p2.aj;
            } else {
                if (p2.am != android.support.v4.app.Fragment.n) {
                    v0_0 = p2.am;
                } else {
                    v0_0 = p2.al;
                }
            }
            v0_2 = android.support.v4.app.ce.a(v0_0);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private static Object a(Object p1, android.support.v4.app.Fragment p2, java.util.ArrayList p3, android.support.v4.n.a p4, android.view.View p5)
    {
        if ((p1 != 0) && (p1 != 0)) {
            android.support.v4.app.ce.a(p3, p2.ac);
            if (p4 != null) {
                p3.removeAll(p4.values());
            }
            if (!p3.isEmpty()) {
                p3.add(p5);
                android.support.v4.app.ce.b(((android.transition.Transition) p1), p3);
            } else {
                p1 = 0;
            }
        }
        return p1;
    }

    private void a(int p4, android.support.v4.app.Fragment p5, String p6, int p7)
    {
        p5.M = this.c;
        if (p6 != null) {
            if ((p5.S == null) || (p6.equals(p5.S))) {
                p5.S = p6;
            } else {
                throw new IllegalStateException(new StringBuilder("Can\'t change tag of fragment ").append(p5).append(": was ").append(p5.S).append(" now ").append(p6).toString());
            }
        }
        if (p4 != 0) {
            if ((p5.Q == 0) || (p5.Q == p4)) {
                p5.Q = p4;
                p5.R = p4;
            } else {
                throw new IllegalStateException(new StringBuilder("Can\'t change container ID of fragment ").append(p5).append(": was ").append(p5.Q).append(" now ").append(p4).toString());
            }
        }
        IllegalStateException v0_7 = new android.support.v4.app.ao();
        v0_7.c = p7;
        v0_7.d = p5;
        this.a(v0_7);
        return;
    }

    static synthetic void a(android.support.v4.app.Fragment p2, android.support.v4.app.Fragment p3, boolean p4, android.support.v4.n.a p5)
    {
        java.util.ArrayList v0_0;
        if (!p4) {
            v0_0 = p2.ar;
        } else {
            v0_0 = p3.ar;
        }
        if (v0_0 != null) {
            new java.util.ArrayList(p5.keySet());
            new java.util.ArrayList(p5.values());
        }
        return;
    }

    static synthetic void a(android.support.v4.app.ak p0, android.support.v4.app.ap p1, int p2, Object p3)
    {
        p0.a(p1, p2, p3);
        return;
    }

    static synthetic void a(android.support.v4.app.ak p2, android.support.v4.n.a p3, android.support.v4.app.ap p4)
    {
        if ((p2.E != null) && (!p3.isEmpty())) {
            android.view.View v0_5 = ((android.view.View) p3.get(p2.E.get(0)));
            if (v0_5 != null) {
                p4.c.a = v0_5;
            }
        }
        return;
    }

    private void a(android.support.v4.app.ap p6, int p7, Object p8)
    {
        if (this.c.m != null) {
            int v1 = 0;
            while (v1 < this.c.m.size()) {
                android.view.View v0_8 = ((android.support.v4.app.Fragment) this.c.m.get(v1));
                if ((v0_8.ac != null) && ((v0_8.ab != null) && (v0_8.R == p7))) {
                    if (!v0_8.T) {
                        android.support.v4.app.ce.a(p8, v0_8.ac, 0);
                        p6.b.remove(v0_8.ac);
                    } else {
                        if (!p6.b.contains(v0_8.ac)) {
                            android.support.v4.app.ce.a(p8, v0_8.ac, 1);
                            p6.b.add(v0_8.ac);
                        }
                    }
                }
                v1++;
            }
        }
        return;
    }

    private void a(android.support.v4.app.ap p6, android.support.v4.n.a p7, boolean p8)
    {
        int v2;
        if (this.E != null) {
            v2 = this.E.size();
        } else {
            v2 = 0;
        }
        int v3 = 0;
        while (v3 < v2) {
            int v0_3 = ((String) this.D.get(v3));
            String v1_7 = ((android.view.View) p7.get(((String) this.E.get(v3))));
            if (v1_7 != null) {
                String v1_8 = v1_7.getTransitionName();
                if (!p8) {
                    android.support.v4.app.ak.a(p6.a, v1_8, v0_3);
                } else {
                    android.support.v4.app.ak.a(p6.a, v0_3, v1_8);
                }
            }
            v3++;
        }
        return;
    }

    private void a(android.support.v4.app.ap p11, android.view.View p12, Object p13, android.support.v4.app.Fragment p14, android.support.v4.app.Fragment p15, boolean p16, java.util.ArrayList p17)
    {
        p12.getViewTreeObserver().addOnPreDrawListener(new android.support.v4.app.am(this, p12, p13, p17, p11, p16, p14, p15));
        return;
    }

    private static void a(android.support.v4.app.ap p4, java.util.ArrayList p5, java.util.ArrayList p6)
    {
        if (p5 != null) {
            int v2 = 0;
            while (v2 < p5.size()) {
                android.support.v4.app.ak.a(p4.a, ((String) p5.get(v2)), ((String) p6.get(v2)));
                v2++;
            }
        }
        return;
    }

    private void a(android.support.v4.n.a p3, android.support.v4.app.ap p4)
    {
        if ((this.E != null) && (!p3.isEmpty())) {
            android.view.View v0_5 = ((android.view.View) p3.get(this.E.get(0)));
            if (v0_5 != null) {
                p4.c.a = v0_5;
            }
        }
        return;
    }

    private static void a(android.support.v4.n.a p2, String p3, String p4)
    {
        if ((p3 != null) && (p4 != null)) {
            int v0 = 0;
            while (v0 < p2.size()) {
                if (!p3.equals(p2.c(v0))) {
                    v0++;
                } else {
                    p2.a(v0, p4);
                }
            }
            p2.put(p3, p4);
        }
        return;
    }

    private static void a(android.util.SparseArray p2, android.support.v4.app.Fragment p3)
    {
        if (p3 != null) {
            int v0 = p3.R;
            if ((v0 != 0) && ((!p3.T) && ((p3.k()) && ((p3.ac != null) && (p2.get(v0) == null))))) {
                p2.put(v0, p3);
            }
        }
        return;
    }

    private void a(android.view.View p8, android.support.v4.app.ap p9, int p10, Object p11)
    {
        p8.getViewTreeObserver().addOnPreDrawListener(new android.support.v4.app.an(this, p8, p9, p10, p11));
        return;
    }

    private boolean a(int p29, android.support.v4.app.ap p30, boolean p31, android.util.SparseArray p32, android.util.SparseArray p33)
    {
        android.view.ViewTreeObserver v3_40;
        android.view.ViewGroup v5_1 = ((android.view.ViewGroup) this.c.w.a(p29));
        if (v5_1 != null) {
            android.support.v4.app.ci v16_0;
            android.view.ViewGroup v10_1 = ((android.support.v4.app.Fragment) p33.get(p29));
            android.support.v4.app.ap v11_1 = ((android.support.v4.app.Fragment) p32.get(p29));
            if (v10_1 != null) {
                android.view.ViewTreeObserver v3_2;
                if (!p31) {
                    v3_2 = v10_1.aj;
                } else {
                    if (v10_1.am != android.support.v4.app.Fragment.n) {
                        v3_2 = v10_1.am;
                    } else {
                        v3_2 = v10_1.al;
                    }
                }
                v16_0 = android.support.v4.app.ce.a(v3_2);
            } else {
                v16_0 = 0;
            }
            if ((v10_1 != null) && (v11_1 != null)) {
                android.view.ViewTreeObserver v3_4;
                if (!p31) {
                    v3_4 = v10_1.an;
                } else {
                    if (v11_1.ao != android.support.v4.app.Fragment.n) {
                        v3_4 = v11_1.ao;
                    } else {
                        v3_4 = v11_1.an;
                    }
                }
                if (v3_4 != null) {
                    android.view.ViewTreeObserver v3_6 = ((android.transition.Transition) v3_4);
                    if (v3_6 != null) {
                        Object v6_1 = new android.transition.TransitionSet();
                        v6_1.addTransition(v3_6);
                    } else {
                        v6_1 = 0;
                    }
                } else {
                    v6_1 = 0;
                }
            } else {
                v6_1 = 0;
            }
            int v12_0;
            if (v11_1 != null) {
                android.view.ViewTreeObserver v3_7;
                if (!p31) {
                    v3_7 = v11_1.al;
                } else {
                    if (v11_1.ak != android.support.v4.app.Fragment.n) {
                        v3_7 = v11_1.ak;
                    } else {
                        v3_7 = v11_1.aj;
                    }
                }
                v12_0 = android.support.v4.app.ce.a(v3_7);
            } else {
                v12_0 = 0;
            }
            android.view.ViewGroup v17_0;
            android.view.ViewTreeObserver v18_0;
            java.util.ArrayList v7_1 = new java.util.ArrayList();
            if (v6_1 == null) {
                v18_0 = 0;
                v17_0 = v6_1;
            } else {
                android.transition.TransitionSet v13_0 = this.a(p30, v11_1, p31);
                if (!v13_0.isEmpty()) {
                    android.view.ViewTreeObserver v3_13;
                    if (!p31) {
                        v3_13 = v10_1.ar;
                    } else {
                        v3_13 = v11_1.ar;
                    }
                    if (v3_13 != null) {
                        new java.util.ArrayList(v13_0.keySet());
                        new java.util.ArrayList(v13_0.values());
                    }
                    v5_1.getViewTreeObserver().addOnPreDrawListener(new android.support.v4.app.am(this, v5_1, v6_1, v7_1, p30, p31, v10_1, v11_1));
                    v18_0 = v13_0;
                    v17_0 = v6_1;
                } else {
                    v18_0 = 0;
                    v17_0 = 0;
                }
            }
            if ((v16_0 != null) || ((v17_0 != null) || (v12_0 != 0))) {
                java.util.ArrayList v21_1 = new java.util.ArrayList();
                Object v6_3 = android.support.v4.app.ak.a(v12_0, v11_1, v21_1, v18_0, p30.d);
                if ((this.E != null) && (v18_0 != null)) {
                    android.view.ViewTreeObserver v3_24 = ((android.view.View) v18_0.get(this.E.get(0)));
                    if (v3_24 != null) {
                        if (v6_3 != null) {
                            android.support.v4.app.ce.a(v6_3, v3_24);
                        }
                        if (v17_0 != null) {
                            android.support.v4.app.ce.a(v17_0, v3_24);
                        }
                    }
                }
                android.view.ViewGroup v9_1;
                int v12_2 = new android.support.v4.app.al(this, v10_1);
                java.util.ArrayList v15_1 = new java.util.ArrayList();
                android.view.ViewTreeObserver v14_2 = new android.support.v4.n.a();
                if (v10_1 == null) {
                    v9_1 = 1;
                } else {
                    android.view.ViewTreeObserver v3_28;
                    if (!p31) {
                        if (v10_1.aq != null) {
                            v3_28 = v10_1.aq.booleanValue();
                        } else {
                            v3_28 = 1;
                        }
                    } else {
                        if (v10_1.ap != null) {
                            v3_28 = v10_1.ap.booleanValue();
                        } else {
                            v3_28 = 1;
                        }
                    }
                    v9_1 = v3_28;
                }
                android.view.ViewTreeObserver v3_32 = ((android.transition.Transition) v16_0);
                android.transition.TransitionSet v4_8 = ((android.transition.Transition) v6_3);
                android.support.v4.app.ch v8_2 = ((android.transition.Transition) v17_0);
                if ((v3_32 == null) || (v4_8 == null)) {
                    v9_1 = 1;
                }
                android.transition.TransitionSet v4_11;
                if (v9_1 == null) {
                    if ((v4_8 == null) || (v3_32 == null)) {
                        if (v4_8 == null) {
                            if (v3_32 == null) {
                                v3_32 = 0;
                            }
                        } else {
                            v3_32 = v4_8;
                        }
                    } else {
                        v3_32 = new android.transition.TransitionSet().addTransition(v4_8).addTransition(v3_32).setOrdering(1);
                    }
                    if (v8_2 == null) {
                        v4_11 = v3_32;
                    } else {
                        v4_11 = new android.transition.TransitionSet();
                        if (v3_32 != null) {
                            v4_11.addTransition(v3_32);
                        }
                        v4_11.addTransition(v8_2);
                    }
                } else {
                    android.view.ViewGroup v9_6 = new android.transition.TransitionSet();
                    if (v3_32 != null) {
                        v9_6.addTransition(v3_32);
                    }
                    if (v4_8 != null) {
                        v9_6.addTransition(v4_8);
                    }
                    if (v8_2 != null) {
                        v9_6.addTransition(v8_2);
                    }
                    v4_11 = v9_6;
                }
                if (v4_11 != null) {
                    android.support.v4.app.ap v11_2 = p30.d;
                    android.view.ViewTreeObserver v3_34 = p30.c;
                    android.transition.TransitionSet v13_1 = p30.a;
                    if ((v16_0 != null) || (v17_0 != null)) {
                        android.view.ViewGroup v10_4 = ((android.transition.Transition) v16_0);
                        if (v10_4 != null) {
                            v10_4.addTarget(v11_2);
                        }
                        if (v17_0 != null) {
                            android.support.v4.app.ce.a(v17_0, v11_2, v18_0, v7_1);
                        }
                        v5_1.getViewTreeObserver().addOnPreDrawListener(new android.support.v4.app.cg(v5_1, v10_4, v11_2, v12_2, v13_1, v14_2, v15_1));
                        if (v10_4 != null) {
                            v10_4.setEpicenterCallback(new android.support.v4.app.ch(v3_34));
                        }
                    }
                    v5_1.getViewTreeObserver().addOnPreDrawListener(new android.support.v4.app.an(this, v5_1, p30, p29, v4_11));
                    android.support.v4.app.ce.a(v4_11, p30.d, 1);
                    this.a(p30, p29, v4_11);
                    android.transition.TransitionManager.beginDelayedTransition(v5_1, ((android.transition.Transition) v4_11));
                    android.transition.Transition v26_1 = ((android.transition.Transition) v4_11);
                    if (v26_1 != null) {
                        v5_1.getViewTreeObserver().addOnPreDrawListener(new android.support.v4.app.ci(v5_1, ((android.transition.Transition) v16_0), v15_1, ((android.transition.Transition) v6_3), v21_1, ((android.transition.Transition) v17_0), v7_1, v14_2, p30.b, v26_1, p30.d));
                    }
                }
                if (v4_11 == null) {
                    v3_40 = 0;
                } else {
                    v3_40 = 1;
                }
            } else {
                v3_40 = 0;
            }
        } else {
            v3_40 = 0;
        }
        return v3_40;
    }

    private static Object b(android.support.v4.app.Fragment p2, boolean p3)
    {
        Object v0_2;
        if (p2 != null) {
            Object v0_0;
            if (!p3) {
                v0_0 = p2.al;
            } else {
                if (p2.ak != android.support.v4.app.Fragment.n) {
                    v0_0 = p2.ak;
                } else {
                    v0_0 = p2.aj;
                }
            }
            v0_2 = android.support.v4.app.ce.a(v0_0);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private static void b(android.support.v4.app.Fragment p2, android.support.v4.app.Fragment p3, boolean p4, android.support.v4.n.a p5)
    {
        java.util.ArrayList v0_0;
        if (!p4) {
            v0_0 = p2.ar;
        } else {
            v0_0 = p3.ar;
        }
        if (v0_0 != null) {
            new java.util.ArrayList(p5.keySet());
            new java.util.ArrayList(p5.values());
        }
        return;
    }

    private static void b(android.support.v4.app.ap p5, android.support.v4.n.a p6, boolean p7)
    {
        int v3 = p6.size();
        int v2 = 0;
        while (v2 < v3) {
            int v0_2 = ((String) p6.b(v2));
            String v1_2 = ((android.view.View) p6.c(v2)).getTransitionName();
            if (!p7) {
                android.support.v4.app.ak.a(p5.a, v1_2, v0_2);
            } else {
                android.support.v4.app.ak.a(p5.a, v0_2, v1_2);
            }
            v2++;
        }
        return;
    }

    private static void b(android.util.SparseArray p1, android.support.v4.app.Fragment p2)
    {
        if (p2 != null) {
            int v0 = p2.R;
            if (v0 != 0) {
                p1.put(v0, p2);
            }
        }
        return;
    }

    private void b(android.util.SparseArray p7, android.util.SparseArray p8)
    {
        if (this.c.w.a()) {
            android.support.v4.app.ao v3 = this.l;
            while (v3 != null) {
                switch (v3.c) {
                    case 1:
                        android.support.v4.app.ak.b(p8, v3.d);
                        break;
                    case 2:
                        int v2;
                        int v1_0 = v3.d;
                        if (this.c.m == null) {
                            v2 = v1_0;
                        } else {
                            v2 = v1_0;
                            int v1_1 = 0;
                            while (v1_1 < this.c.m.size()) {
                                int v0_19 = ((android.support.v4.app.Fragment) this.c.m.get(v1_1));
                                if ((v2 == 0) || (v0_19.R == v2.R)) {
                                    if (v0_19 != v2) {
                                        android.support.v4.app.ak.a(p7, v0_19);
                                    } else {
                                        v2 = 0;
                                    }
                                }
                                v1_1++;
                            }
                        }
                        android.support.v4.app.ak.b(p8, v2);
                        break;
                    case 3:
                        android.support.v4.app.ak.a(p7, v3.d);
                        break;
                    case 4:
                        android.support.v4.app.ak.a(p7, v3.d);
                        break;
                    case 5:
                        android.support.v4.app.ak.b(p8, v3.d);
                        break;
                    case 6:
                        android.support.v4.app.ak.a(p7, v3.d);
                        break;
                    case 7:
                        android.support.v4.app.ak.b(p8, v3.d);
                        break;
                }
                v3 = v3.a;
            }
        }
        return;
    }

    private android.support.v4.n.a c(android.support.v4.app.Fragment p4, boolean p5)
    {
        android.support.v4.n.a v0_1 = new android.support.v4.n.a();
        java.util.ArrayList v1_0 = p4.ac;
        if ((v1_0 != null) && (this.D != null)) {
            android.support.v4.app.ce.a(v0_1, v1_0);
            if (!p5) {
                android.support.v4.n.k.c(v0_1, this.E);
            } else {
                v0_1 = android.support.v4.app.ak.a(this.D, this.E, v0_1);
            }
        }
        return v0_1;
    }

    private int m()
    {
        return this.s;
    }

    private int n()
    {
        return this.t;
    }

    public final int a()
    {
        return this.y;
    }

    public final android.support.v4.app.ap a(boolean p12, android.support.v4.app.ap p13, android.util.SparseArray p14, android.util.SparseArray p15)
    {
        if (android.support.v4.app.bl.b) {
            android.util.Log.v("FragmentManager", new StringBuilder("popFromBackStack: ").append(this).toString());
            this.a("  ", new java.io.PrintWriter(new android.support.v4.n.h("FragmentManager")));
        }
        if (android.support.v4.app.ak.b) {
            if (p13 != 0) {
                if (!p12) {
                    android.support.v4.app.ak.a(p13, this.E, this.D);
                }
            } else {
                if ((p14.size() != 0) || (p15.size() != 0)) {
                    p13 = this.a(p14, p15, 1);
                }
            }
        }
        int v7;
        this.e(-1);
        if (p13 == 0) {
            v7 = this.t;
        } else {
            v7 = 0;
        }
        String v1_8;
        if (p13 == 0) {
            v1_8 = this.s;
        } else {
            v1_8 = 0;
        }
        android.support.v4.app.ao v6 = this.m;
        while (v6 != null) {
            int v5_0;
            if (p13 == 0) {
                v5_0 = v6.g;
            } else {
                v5_0 = 0;
            }
            int v0_18;
            if (p13 == 0) {
                v0_18 = v6.h;
            } else {
                v0_18 = 0;
            }
            switch (v6.c) {
                case 1:
                    int v3_14 = v6.d;
                    v3_14.aa = v0_18;
                    this.c.a(v3_14, android.support.v4.app.bl.d(v1_8), v7);
                    break;
                case 2:
                    int v3_12 = v6.d;
                    if (v3_12 != 0) {
                        v3_12.aa = v0_18;
                        this.c.a(v3_12, android.support.v4.app.bl.d(v1_8), v7);
                    }
                    if (v6.i == null) {
                    } else {
                        int v3_13 = 0;
                        while (v3_13 < v6.i.size()) {
                            int v0_30 = ((android.support.v4.app.Fragment) v6.i.get(v3_13));
                            v0_30.aa = v5_0;
                            this.c.a(v0_30, 0);
                            v3_13++;
                        }
                    }
                    break;
                case 3:
                    int v0_23 = v6.d;
                    v0_23.aa = v5_0;
                    this.c.a(v0_23, 0);
                    break;
                case 4:
                    int v0_22 = v6.d;
                    v0_22.aa = v5_0;
                    this.c.c(v0_22, android.support.v4.app.bl.d(v1_8), v7);
                    break;
                case 5:
                    int v3_9 = v6.d;
                    v3_9.aa = v0_18;
                    this.c.b(v3_9, android.support.v4.app.bl.d(v1_8), v7);
                    break;
                case 6:
                    int v0_20 = v6.d;
                    v0_20.aa = v5_0;
                    this.c.e(v0_20, android.support.v4.app.bl.d(v1_8), v7);
                    break;
                case 7:
                    int v0_19 = v6.d;
                    v0_19.aa = v5_0;
                    this.c.d(v0_19, android.support.v4.app.bl.d(v1_8), v7);
                    break;
                default:
                    throw new IllegalArgumentException(new StringBuilder("Unknown cmd: ").append(v6.c).toString());
            }
            v6 = v6.b;
        }
        if (p12) {
            this.c.a(this.c.t, android.support.v4.app.bl.d(v1_8), v7, 1);
            p13 = 0;
        }
        if (this.y >= 0) {
            String v1_10 = this.c;
            int v0_14 = this.y;
            v1_10.q.set(v0_14, 0);
            if (v1_10.r == null) {
                v1_10.r = new java.util.ArrayList();
            }
            if (android.support.v4.app.bl.b) {
                android.util.Log.v("FragmentManager", new StringBuilder("Freeing back stack index ").append(v0_14).toString());
            }
            v1_10.r.add(Integer.valueOf(v0_14));
            this.y = -1;
        }
        return p13;
    }

    public final android.support.v4.app.cd a(int p1)
    {
        this.s = p1;
        return this;
    }

    public final android.support.v4.app.cd a(int p2, int p3)
    {
        this.o = p2;
        this.p = p3;
        this.q = 0;
        this.r = 0;
        return this;
    }

    public final android.support.v4.app.cd a(int p3, android.support.v4.app.Fragment p4)
    {
        this.a(p3, p4, 0, 1);
        return this;
    }

    public final android.support.v4.app.cd a(int p2, android.support.v4.app.Fragment p3, String p4)
    {
        this.a(p2, p3, p4, 1);
        return this;
    }

    public final android.support.v4.app.cd a(android.support.v4.app.Fragment p2)
    {
        return this.b(p2, 0);
    }

    public final android.support.v4.app.cd a(android.support.v4.app.Fragment p3, String p4)
    {
        this.a(0, p3, p4, 1);
        return this;
    }

    public final android.support.v4.app.cd a(android.view.View p3, String p4)
    {
        if (android.support.v4.app.ak.b) {
            java.util.ArrayList v0_1 = p3.getTransitionName();
            if (v0_1 != null) {
                if (this.D == null) {
                    this.D = new java.util.ArrayList();
                    this.E = new java.util.ArrayList();
                }
                this.D.add(v0_1);
                this.E.add(p4);
            } else {
                throw new IllegalArgumentException("Unique transitionNames are required for all sharedElements");
            }
        }
        return this;
    }

    public final android.support.v4.app.cd a(CharSequence p2)
    {
        this.z = 0;
        this.A = p2;
        return this;
    }

    final void a(android.support.v4.app.ao p2)
    {
        if (this.l != null) {
            p2.b = this.m;
            this.m.a = p2;
            this.m = p2;
        } else {
            this.m = p2;
            this.l = p2;
        }
        p2.e = this.o;
        p2.f = this.p;
        p2.g = this.q;
        p2.h = this.r;
        this.n = (this.n + 1);
        return;
    }

    public final void a(android.util.SparseArray p4, android.util.SparseArray p5)
    {
        if (this.c.w.a()) {
            android.support.v4.app.ao v2 = this.l;
            while (v2 != null) {
                switch (v2.c) {
                    case 1:
                        android.support.v4.app.ak.a(p4, v2.d);
                        break;
                    case 2:
                        if (v2.i != null) {
                            int v1 = (v2.i.size() - 1);
                            while (v1 >= 0) {
                                android.support.v4.app.ak.b(p5, ((android.support.v4.app.Fragment) v2.i.get(v1)));
                                v1--;
                            }
                        }
                        android.support.v4.app.ak.a(p4, v2.d);
                        break;
                    case 3:
                        android.support.v4.app.ak.b(p5, v2.d);
                        break;
                    case 4:
                        android.support.v4.app.ak.b(p5, v2.d);
                        break;
                    case 5:
                        android.support.v4.app.ak.a(p4, v2.d);
                        break;
                    case 6:
                        android.support.v4.app.ak.b(p5, v2.d);
                        break;
                    case 7:
                        android.support.v4.app.ak.a(p4, v2.d);
                        break;
                }
                v2 = v2.a;
            }
        }
        return;
    }

    public final void a(String p2, java.io.PrintWriter p3)
    {
        this.a(p2, p3, 1);
        return;
    }

    public final void a(String p8, java.io.PrintWriter p9, boolean p10)
    {
        if (p10) {
            p9.print(p8);
            p9.print("mName=");
            p9.print(this.w);
            p9.print(" mIndex=");
            p9.print(this.y);
            p9.print(" mCommitted=");
            p9.println(this.x);
            if (this.s != 0) {
                p9.print(p8);
                p9.print("mTransition=#");
                p9.print(Integer.toHexString(this.s));
                p9.print(" mTransitionStyle=#");
                p9.println(Integer.toHexString(this.t));
            }
            if ((this.o != 0) || (this.p != 0)) {
                p9.print(p8);
                p9.print("mEnterAnim=#");
                p9.print(Integer.toHexString(this.o));
                p9.print(" mExitAnim=#");
                p9.println(Integer.toHexString(this.p));
            }
            if ((this.q != 0) || (this.r != 0)) {
                p9.print(p8);
                p9.print("mPopEnterAnim=#");
                p9.print(Integer.toHexString(this.q));
                p9.print(" mPopExitAnim=#");
                p9.println(Integer.toHexString(this.r));
            }
            if ((this.z != 0) || (this.A != null)) {
                p9.print(p8);
                p9.print("mBreadCrumbTitleRes=#");
                p9.print(Integer.toHexString(this.z));
                p9.print(" mBreadCrumbTitleText=");
                p9.println(this.A);
            }
            if ((this.B != 0) || (this.C != null)) {
                p9.print(p8);
                p9.print("mBreadCrumbShortTitleRes=#");
                p9.print(Integer.toHexString(this.B));
                p9.print(" mBreadCrumbShortTitleText=");
                p9.println(this.C);
            }
        }
        if (this.l != null) {
            p9.print(p8);
            p9.println("Operations:");
            String v4 = new StringBuilder().append(p8).append("    ").toString();
            int v2_1 = 0;
            android.support.v4.app.ao v3 = this.l;
            while (v3 != null) {
                int v0_51;
                switch (v3.c) {
                    case 0:
                        v0_51 = "NULL";
                        break;
                    case 1:
                        v0_51 = "ADD";
                        break;
                    case 2:
                        v0_51 = "REPLACE";
                        break;
                    case 3:
                        v0_51 = "REMOVE";
                        break;
                    case 4:
                        v0_51 = "HIDE";
                        break;
                    case 5:
                        v0_51 = "SHOW";
                        break;
                    case 6:
                        v0_51 = "DETACH";
                        break;
                    case 7:
                        v0_51 = "ATTACH";
                        break;
                    default:
                        v0_51 = new StringBuilder("cmd=").append(v3.c).toString();
                }
                p9.print(p8);
                p9.print("  Op #");
                p9.print(v2_1);
                p9.print(": ");
                p9.print(v0_51);
                p9.print(" ");
                p9.println(v3.d);
                if (p10) {
                    if ((v3.e != 0) || (v3.f != 0)) {
                        p9.print(p8);
                        p9.print("enterAnim=#");
                        p9.print(Integer.toHexString(v3.e));
                        p9.print(" exitAnim=#");
                        p9.println(Integer.toHexString(v3.f));
                    }
                    if ((v3.g != 0) || (v3.h != 0)) {
                        p9.print(p8);
                        p9.print("popEnterAnim=#");
                        p9.print(Integer.toHexString(v3.g));
                        p9.print(" popExitAnim=#");
                        p9.println(Integer.toHexString(v3.h));
                    }
                }
                if ((v3.i != null) && (v3.i.size() > 0)) {
                    int v0_76 = 0;
                    while (v0_76 < v3.i.size()) {
                        p9.print(v4);
                        if (v3.i.size() != 1) {
                            if (v0_76 == 0) {
                                p9.println("Removed:");
                            }
                            p9.print(v4);
                            p9.print("  #");
                            p9.print(v0_76);
                            p9.print(": ");
                        } else {
                            p9.print("Removed: ");
                        }
                        p9.println(v3.i.get(v0_76));
                        v0_76++;
                    }
                }
                v3 = v3.a;
                v2_1++;
            }
        }
        return;
    }

    public final int b()
    {
        return this.z;
    }

    public final android.support.v4.app.cd b(int p1)
    {
        this.t = p1;
        return this;
    }

    public final android.support.v4.app.cd b(int p2, int p3)
    {
        this.o = p2;
        this.p = p3;
        this.q = 0;
        this.r = 0;
        return this;
    }

    public final android.support.v4.app.cd b(android.support.v4.app.Fragment p3)
    {
        android.support.v4.app.ao v0_1 = new android.support.v4.app.ao();
        v0_1.c = 3;
        v0_1.d = p3;
        this.a(v0_1);
        return this;
    }

    public final android.support.v4.app.cd b(android.support.v4.app.Fragment p3, String p4)
    {
        this.a(2131493319, p3, p4, 2);
        return this;
    }

    public final android.support.v4.app.cd b(CharSequence p2)
    {
        this.B = 0;
        this.C = p2;
        return this;
    }

    public final int c()
    {
        return this.B;
    }

    public final android.support.v4.app.cd c(int p2)
    {
        this.z = p2;
        this.A = 0;
        return this;
    }

    public final android.support.v4.app.cd c(android.support.v4.app.Fragment p3)
    {
        android.support.v4.app.ao v0_1 = new android.support.v4.app.ao();
        v0_1.c = 4;
        v0_1.d = p3;
        this.a(v0_1);
        return this;
    }

    public final android.support.v4.app.cd d(int p2)
    {
        this.B = p2;
        this.C = 0;
        return this;
    }

    public final android.support.v4.app.cd d(android.support.v4.app.Fragment p3)
    {
        android.support.v4.app.ao v0_1 = new android.support.v4.app.ao();
        v0_1.c = 5;
        v0_1.d = p3;
        this.a(v0_1);
        return this;
    }

    public final CharSequence d()
    {
        CharSequence v0_1;
        if (this.z == 0) {
            v0_1 = this.A;
        } else {
            v0_1 = this.c.u.c.getText(this.z);
        }
        return v0_1;
    }

    public final android.support.v4.app.cd e(android.support.v4.app.Fragment p3)
    {
        android.support.v4.app.ao v0_1 = new android.support.v4.app.ao();
        v0_1.c = 6;
        v0_1.d = p3;
        this.a(v0_1);
        return this;
    }

    public final CharSequence e()
    {
        CharSequence v0_1;
        if (this.B == 0) {
            v0_1 = this.C;
        } else {
            v0_1 = this.c.u.c.getText(this.B);
        }
        return v0_1;
    }

    final void e(int p7)
    {
        if (this.u) {
            if (android.support.v4.app.bl.b) {
                android.util.Log.v("FragmentManager", new StringBuilder("Bump nesting in ").append(this).append(" by ").append(p7).toString());
            }
            android.support.v4.app.ao v2_2 = this.l;
            while (v2_2 != null) {
                if (v2_2.d != null) {
                    int v0_5 = v2_2.d;
                    v0_5.L = (v0_5.L + p7);
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("Bump nesting of ").append(v2_2.d).append(" to ").append(v2_2.d.L).toString());
                    }
                }
                if (v2_2.i != null) {
                    int v1_14 = (v2_2.i.size() - 1);
                    while (v1_14 >= 0) {
                        int v0_15 = ((android.support.v4.app.Fragment) v2_2.i.get(v1_14));
                        v0_15.L = (v0_15.L + p7);
                        if (android.support.v4.app.bl.b) {
                            android.util.Log.v("FragmentManager", new StringBuilder("Bump nesting of ").append(v0_15).append(" to ").append(v0_15.L).toString());
                        }
                        v1_14--;
                    }
                }
                v2_2 = v2_2.a;
            }
        }
        return;
    }

    public final android.support.v4.app.cd f()
    {
        if (this.v) {
            this.u = 1;
            this.w = 0;
            return this;
        } else {
            throw new IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
        }
    }

    public final android.support.v4.app.cd f(android.support.v4.app.Fragment p3)
    {
        android.support.v4.app.ao v0_1 = new android.support.v4.app.ao();
        v0_1.c = 7;
        v0_1.d = p3;
        this.a(v0_1);
        return this;
    }

    public final boolean g()
    {
        return this.v;
    }

    public final android.support.v4.app.cd h()
    {
        if (!this.u) {
            this.v = 0;
            return this;
        } else {
            throw new IllegalStateException("This transaction is already being added to the back stack");
        }
    }

    public final int i()
    {
        return this.a(0);
    }

    public final int j()
    {
        return this.a(1);
    }

    public final String k()
    {
        return this.w;
    }

    public final boolean l()
    {
        int v0_1;
        if (this.n != 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final void run()
    {
        if (android.support.v4.app.bl.b) {
            android.util.Log.v("FragmentManager", new StringBuilder("Run: ").append(this).toString());
        }
        if ((!this.u) || (this.y >= 0)) {
            android.support.v4.app.ap v8;
            this.e(1);
            if (!android.support.v4.app.ak.b) {
                v8 = 0;
            } else {
                int v0_7 = new android.util.SparseArray();
                String v1_6 = new android.util.SparseArray();
                this.b(v0_7, v1_6);
                v8 = this.a(v0_7, v1_6, 0);
            }
            int v7;
            if (v8 == null) {
                v7 = this.t;
            } else {
                v7 = 0;
            }
            String v1_7;
            if (v8 == null) {
                v1_7 = this.s;
            } else {
                v1_7 = 0;
            }
            android.support.v4.app.ao v6 = this.l;
            while (v6 != null) {
                int v5;
                if (v8 == null) {
                    v5 = v6.e;
                } else {
                    v5 = 0;
                }
                int v2_4;
                if (v8 == null) {
                    v2_4 = v6.f;
                } else {
                    v2_4 = 0;
                }
                switch (v6.c) {
                    case 1:
                        int v0_39 = v6.d;
                        v0_39.aa = v5;
                        this.c.a(v0_39, 0);
                        break;
                    case 2:
                        int v4;
                        int v3_1 = v6.d;
                        int v9 = v3_1.R;
                        if (this.c.m == null) {
                            v4 = v3_1;
                        } else {
                            v4 = v3_1;
                            int v3_2 = 0;
                            while (v3_2 < this.c.m.size()) {
                                int v0_37 = ((android.support.v4.app.Fragment) this.c.m.get(v3_2));
                                if (android.support.v4.app.bl.b) {
                                    android.util.Log.v("FragmentManager", new StringBuilder("OP_REPLACE: adding=").append(v4).append(" old=").append(v0_37).toString());
                                }
                                if (v0_37.R == v9) {
                                    if (v0_37 != v4) {
                                        if (v6.i == null) {
                                            v6.i = new java.util.ArrayList();
                                        }
                                        v6.i.add(v0_37);
                                        v0_37.aa = v2_4;
                                        if (this.u) {
                                            v0_37.L = (v0_37.L + 1);
                                            if (android.support.v4.app.bl.b) {
                                                android.util.Log.v("FragmentManager", new StringBuilder("Bump nesting of ").append(v0_37).append(" to ").append(v0_37.L).toString());
                                            }
                                        }
                                        this.c.a(v0_37, v1_7, v7);
                                    } else {
                                        v4 = 0;
                                        v6.d = 0;
                                    }
                                }
                                v3_2++;
                            }
                        }
                        if (v4 == 0) {
                        } else {
                            v4.aa = v5;
                            this.c.a(v4, 0);
                        }
                        break;
                    case 3:
                        int v0_26 = v6.d;
                        v0_26.aa = v2_4;
                        this.c.a(v0_26, v1_7, v7);
                        break;
                    case 4:
                        int v0_25 = v6.d;
                        v0_25.aa = v2_4;
                        this.c.b(v0_25, v1_7, v7);
                        break;
                    case 5:
                        int v0_24 = v6.d;
                        v0_24.aa = v5;
                        this.c.c(v0_24, v1_7, v7);
                        break;
                    case 6:
                        int v0_23 = v6.d;
                        v0_23.aa = v2_4;
                        this.c.d(v0_23, v1_7, v7);
                        break;
                    case 7:
                        int v0_22 = v6.d;
                        v0_22.aa = v5;
                        this.c.e(v0_22, v1_7, v7);
                        break;
                    default:
                        throw new IllegalArgumentException(new StringBuilder("Unknown cmd: ").append(v6.c).toString());
                }
                v6 = v6.a;
            }
            this.c.a(this.c.t, v1_7, v7, 1);
            if (this.u) {
                int v0_16 = this.c;
                if (v0_16.o == null) {
                    v0_16.o = new java.util.ArrayList();
                }
                v0_16.o.add(this);
                v0_16.l();
            }
            return;
        } else {
            throw new IllegalStateException("addToBackStack() called after commit()");
        }
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder(128);
        v0_1.append("BackStackEntry{");
        v0_1.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.y >= 0) {
            v0_1.append(" #");
            v0_1.append(this.y);
        }
        if (this.w != null) {
            v0_1.append(" ");
            v0_1.append(this.w);
        }
        v0_1.append("}");
        return v0_1.toString();
    }
}
