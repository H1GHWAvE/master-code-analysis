package android.support.v4.app;
final class ba implements android.os.Parcelable$Creator {

    ba()
    {
        return;
    }

    private static android.support.v4.app.Fragment$SavedState a(android.os.Parcel p1)
    {
        return new android.support.v4.app.Fragment$SavedState(p1);
    }

    private static android.support.v4.app.Fragment$SavedState[] a(int p1)
    {
        android.support.v4.app.Fragment$SavedState[] v0 = new android.support.v4.app.Fragment$SavedState[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return new android.support.v4.app.Fragment$SavedState(p2);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.app.Fragment$SavedState[] v0 = new android.support.v4.app.Fragment$SavedState[p2];
        return v0;
    }
}
