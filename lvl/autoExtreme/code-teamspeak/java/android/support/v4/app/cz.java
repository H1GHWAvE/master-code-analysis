package android.support.v4.app;
final class cz {

    cz()
    {
        return;
    }

    private static android.content.Intent a(android.app.Activity p1)
    {
        return p1.getParentActivityIntent();
    }

    private static String a(android.content.pm.ActivityInfo p1)
    {
        return p1.parentActivityName;
    }

    private static boolean a(android.app.Activity p1, android.content.Intent p2)
    {
        return p1.shouldUpRecreateTask(p2);
    }

    private static void b(android.app.Activity p0, android.content.Intent p1)
    {
        p0.navigateUpTo(p1);
        return;
    }
}
