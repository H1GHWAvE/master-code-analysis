package android.support.v4.app;
final class ca implements android.widget.TabHost$TabContentFactory {
    private final android.content.Context a;

    public ca(android.content.Context p1)
    {
        this.a = p1;
        return;
    }

    public final android.view.View createTabContent(String p4)
    {
        android.view.View v0_1 = new android.view.View(this.a);
        v0_1.setMinimumWidth(0);
        v0_1.setMinimumHeight(0);
        return v0_1;
    }
}
