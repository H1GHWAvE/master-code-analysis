package android.support.v4.app;
final class fb implements android.support.v4.app.fk {
    final String a;
    final int b;
    final String c;
    final boolean d;

    public fb(String p2)
    {
        this.a = p2;
        this.b = 0;
        this.c = 0;
        this.d = 1;
        return;
    }

    public fb(String p2, int p3)
    {
        this.a = p2;
        this.b = p3;
        this.c = 0;
        this.d = 0;
        return;
    }

    public final void a(android.support.v4.app.cl p4)
    {
        if (!this.d) {
            p4.a(this.a, this.b, this.c);
        } else {
            p4.a(this.a);
        }
        return;
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder("CancelTask[");
        v0_1.append("packageName:").append(this.a);
        v0_1.append(", id:").append(this.b);
        v0_1.append(", tag:").append(this.c);
        v0_1.append(", all:").append(this.d);
        v0_1.append("]");
        return v0_1.toString();
    }
}
