package android.support.v4.app;
public final class es implements android.support.v4.app.dc {
    private android.app.Notification$Builder a;

    public es(android.content.Context p9, android.app.Notification p10, CharSequence p11, CharSequence p12, CharSequence p13, android.widget.RemoteViews p14, int p15, android.app.PendingIntent p16, android.app.PendingIntent p17, android.graphics.Bitmap p18, int p19, int p20, boolean p21)
    {
        android.app.Notification$Builder v4_10;
        android.app.Notification$Builder v5_6 = new android.app.Notification$Builder(p9).setWhen(p10.when).setSmallIcon(p10.icon, p10.iconLevel).setContent(p10.contentView).setTicker(p10.tickerText, p14).setSound(p10.sound, p10.audioStreamType).setVibrate(p10.vibrate).setLights(p10.ledARGB, p10.ledOnMS, p10.ledOffMS);
        if ((p10.flags & 2) == 0) {
            v4_10 = 0;
        } else {
            v4_10 = 1;
        }
        android.app.Notification$Builder v4_13;
        android.app.Notification$Builder v5_7 = v5_6.setOngoing(v4_10);
        if ((p10.flags & 8) == 0) {
            v4_13 = 0;
        } else {
            v4_13 = 1;
        }
        android.app.Notification$Builder v4_16;
        android.app.Notification$Builder v5_8 = v5_7.setOnlyAlertOnce(v4_13);
        if ((p10.flags & 16) == 0) {
            v4_16 = 0;
        } else {
            v4_16 = 1;
        }
        android.app.Notification$Builder v4_25;
        android.app.Notification$Builder v5_11 = v5_8.setAutoCancel(v4_16).setDefaults(p10.defaults).setContentTitle(p11).setContentText(p12).setContentInfo(p13).setContentIntent(p16).setDeleteIntent(p10.deleteIntent);
        if ((p10.flags & 128) == 0) {
            v4_25 = 0;
        } else {
            v4_25 = 1;
        }
        this.a = v5_11.setFullScreenIntent(p17, v4_25).setLargeIcon(p18).setNumber(p15).setProgress(p19, p20, p21);
        return;
    }

    public final android.app.Notification$Builder a()
    {
        return this.a;
    }

    public final android.app.Notification b()
    {
        return this.a.getNotification();
    }
}
