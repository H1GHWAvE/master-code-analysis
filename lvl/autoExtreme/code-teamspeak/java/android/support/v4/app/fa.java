package android.support.v4.app;
public final class fa {
    public static final String a = "android.support.useSideChannel";
    public static final String b = "android.support.BIND_NOTIFICATION_SIDE_CHANNEL";
    static final int c = 19;
    private static final String d = "NotifManCompat";
    private static final int e = 1000;
    private static final int f = 6;
    private static final String g = "enabled_notification_listeners";
    private static final int h;
    private static final Object i;
    private static String j;
    private static java.util.Set k;
    private static final Object n;
    private static android.support.v4.app.fi o;
    private static final android.support.v4.app.fc p;
    private final android.content.Context l;
    private final android.app.NotificationManager m;

    static fa()
    {
        android.support.v4.app.fa.i = new Object();
        android.support.v4.app.fa.k = new java.util.HashSet();
        android.support.v4.app.fa.n = new Object();
        if (android.os.Build$VERSION.SDK_INT < 14) {
            if (android.os.Build$VERSION.SDK_INT < 5) {
                android.support.v4.app.fa.p = new android.support.v4.app.fd();
            } else {
                android.support.v4.app.fa.p = new android.support.v4.app.fe();
            }
        } else {
            android.support.v4.app.fa.p = new android.support.v4.app.ff();
        }
        android.support.v4.app.fa.h = android.support.v4.app.fa.p.a();
        return;
    }

    private fa(android.content.Context p3)
    {
        this.l = p3;
        this.m = ((android.app.NotificationManager) this.l.getSystemService("notification"));
        return;
    }

    static synthetic int a()
    {
        return android.support.v4.app.fa.h;
    }

    public static java.util.Set a(android.content.Context p6)
    {
        String v1_1 = android.provider.Settings$Secure.getString(p6.getContentResolver(), "enabled_notification_listeners");
        if ((v1_1 != null) && (!v1_1.equals(android.support.v4.app.fa.j))) {
            Object v2 = v1_1.split(":");
            java.util.HashSet v3_1 = new java.util.HashSet(v2.length);
            int v4 = v2.length;
            Throwable v0_5 = 0;
            while (v0_5 < v4) {
                String v5_1 = android.content.ComponentName.unflattenFromString(v2[v0_5]);
                if (v5_1 != null) {
                    v3_1.add(v5_1.getPackageName());
                }
                v0_5++;
            }
            android.support.v4.app.fa.k = v3_1;
            android.support.v4.app.fa.j = v1_1;
        }
        return android.support.v4.app.fa.k;
    }

    private void a(int p3)
    {
        android.support.v4.app.fa.p.a(this.m, p3);
        if (android.os.Build$VERSION.SDK_INT <= 19) {
            this.a(new android.support.v4.app.fb(this.l.getPackageName(), p3));
        }
        return;
    }

    private void a(int p3, android.app.Notification p4)
    {
        android.support.v4.app.fc v0_2;
        android.support.v4.app.fc v0_0 = android.support.v4.app.dd.a(p4);
        if ((v0_0 == null) || (!v0_0.getBoolean("android.support.useSideChannel"))) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        if (v0_2 == null) {
            android.support.v4.app.fa.p.a(this.m, p3, p4);
        } else {
            this.a(new android.support.v4.app.fg(this.l.getPackageName(), p3, p4));
            android.support.v4.app.fa.p.a(this.m, p3);
        }
        return;
    }

    private void a(android.support.v4.app.fk p4)
    {
        if (android.support.v4.app.fa.o == null) {
            android.support.v4.app.fa.o = new android.support.v4.app.fi(this.l.getApplicationContext());
        }
        android.support.v4.app.fa.o.a.obtainMessage(0, p4).sendToTarget();
        return;
    }

    private static boolean a(android.app.Notification p2)
    {
        int v0_2;
        int v0_0 = android.support.v4.app.dd.a(p2);
        if ((v0_0 == 0) || (!v0_0.getBoolean("android.support.useSideChannel"))) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private static android.support.v4.app.fa b(android.content.Context p1)
    {
        return new android.support.v4.app.fa(p1);
    }

    private void b()
    {
        this.m.cancelAll();
        if (android.os.Build$VERSION.SDK_INT <= 19) {
            this.a(new android.support.v4.app.fb(this.l.getPackageName()));
        }
        return;
    }

    private void b(int p3)
    {
        android.support.v4.app.fa.p.a(this.m, p3);
        if (android.os.Build$VERSION.SDK_INT <= 19) {
            this.a(new android.support.v4.app.fb(this.l.getPackageName(), p3));
        }
        return;
    }

    private void b(int p3, android.app.Notification p4)
    {
        android.support.v4.app.fc v0_2;
        android.support.v4.app.fc v0_0 = android.support.v4.app.dd.a(p4);
        if ((v0_0 == null) || (!v0_0.getBoolean("android.support.useSideChannel"))) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        if (v0_2 == null) {
            android.support.v4.app.fa.p.a(this.m, p3, p4);
        } else {
            this.a(new android.support.v4.app.fg(this.l.getPackageName(), p3, p4));
            android.support.v4.app.fa.p.a(this.m, p3);
        }
        return;
    }
}
