package android.support.v4.app;
final class ah extends android.support.v4.app.ai {

    private ah()
    {
        this(0);
        return;
    }

    synthetic ah(byte p1)
    {
        return;
    }

    public final int a(android.content.Context p2, String p3, int p4, String p5)
    {
        return ((android.app.AppOpsManager) p2.getSystemService(android.app.AppOpsManager)).noteOp(p3, p4, p5);
    }

    public final int a(android.content.Context p2, String p3, String p4)
    {
        return ((android.app.AppOpsManager) p2.getSystemService(android.app.AppOpsManager)).noteProxyOp(p3, p4);
    }

    public final String a(String p2)
    {
        return android.app.AppOpsManager.permissionToOp(p2);
    }
}
