package android.support.v4.app;
public final class dr {
    private final java.util.List a;
    private final String b;
    private android.support.v4.app.fn c;
    private android.app.PendingIntent d;
    private android.app.PendingIntent e;
    private long f;

    private dr(String p2)
    {
        this.a = new java.util.ArrayList();
        this.b = p2;
        return;
    }

    private android.support.v4.app.dp a()
    {
        String[] v1_2 = new String[this.a.size()];
        String[] v1_4 = ((String[]) this.a.toArray(v1_2));
        String[] v5 = new String[1];
        v5[0] = this.b;
        return new android.support.v4.app.dp(v1_4, this.c, this.e, this.d, v5, this.f);
    }

    private android.support.v4.app.dr a(long p2)
    {
        this.f = p2;
        return this;
    }

    private android.support.v4.app.dr a(android.app.PendingIntent p1)
    {
        this.d = p1;
        return this;
    }

    private android.support.v4.app.dr a(android.app.PendingIntent p1, android.support.v4.app.fn p2)
    {
        this.c = p2;
        this.e = p1;
        return this;
    }

    private android.support.v4.app.dr a(String p2)
    {
        this.a.add(p2);
        return this;
    }
}
