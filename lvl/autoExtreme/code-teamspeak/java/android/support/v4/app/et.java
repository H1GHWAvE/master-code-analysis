package android.support.v4.app;
final class et {
    public static final String a = "NotificationCompat";
    static final String b = "android.support.localOnly";
    static final String c = "android.support.actionExtras";
    static final String d = "android.support.remoteInputs";
    static final String e = "android.support.groupKey";
    static final String f = "android.support.isGroupSummary";
    static final String g = "android.support.sortKey";
    static final String h = "android.support.useSideChannel";
    private static final String i = "icon";
    private static final String j = "title";
    private static final String k = "actionIntent";
    private static final String l = "extras";
    private static final String m = "remoteInputs";
    private static final Object n;
    private static reflect.Field o;
    private static boolean p;
    private static final Object q;
    private static Class r;
    private static reflect.Field s;
    private static reflect.Field t;
    private static reflect.Field u;
    private static reflect.Field v;
    private static boolean w;

    static et()
    {
        android.support.v4.app.et.n = new Object();
        android.support.v4.app.et.q = new Object();
        return;
    }

    et()
    {
        return;
    }

    public static android.os.Bundle a(android.app.Notification$Builder p3, android.support.v4.app.ek p4)
    {
        p4.a();
        p3.addAction(0, p4.b(), p4.c());
        android.os.Bundle v0_2 = new android.os.Bundle(p4.d());
        if (p4.e() != null) {
            v0_2.putParcelableArray("android.support.remoteInputs", android.support.v4.app.fy.a(p4.e()));
        }
        return v0_2;
    }

    public static android.os.Bundle a(android.app.Notification p5)
    {
        try {
            int v0_6;
            if (!android.support.v4.app.et.p) {
                try {
                    if (android.support.v4.app.et.o != null) {
                        v0_6 = ((android.os.Bundle) android.support.v4.app.et.o.get(p5));
                        if (v0_6 == 0) {
                            v0_6 = new android.os.Bundle();
                            android.support.v4.app.et.o.set(p5, v0_6);
                        }
                        return v0_6;
                    } else {
                        int v0_3 = android.app.Notification.getDeclaredField("extras");
                        if (android.os.Bundle.isAssignableFrom(v0_3.getType())) {
                            v0_3.setAccessible(1);
                            android.support.v4.app.et.o = v0_3;
                        } else {
                            android.util.Log.e("NotificationCompat", "Notification.extras field is not of type Bundle");
                            android.support.v4.app.et.p = 1;
                            v0_6 = 0;
                            return v0_6;
                        }
                    }
                } catch (int v0_10) {
                    android.util.Log.e("NotificationCompat", "Unable to access notification extras", v0_10);
                } catch (int v0_11) {
                    android.util.Log.e("NotificationCompat", "Unable to access notification extras", v0_11);
                }
                android.support.v4.app.et.p = 1;
                v0_6 = 0;
            } else {
                v0_6 = 0;
            }
        } catch (int v0_13) {
            throw v0_13;
        }
        return v0_6;
    }

    private static android.os.Bundle a(android.support.v4.app.ek p3)
    {
        android.os.Bundle v0_1 = new android.os.Bundle();
        v0_1.putInt("icon", p3.a());
        v0_1.putCharSequence("title", p3.b());
        v0_1.putParcelable("actionIntent", p3.c());
        v0_1.putBundle("extras", p3.d());
        v0_1.putParcelableArray("remoteInputs", android.support.v4.app.fy.a(p3.e()));
        return v0_1;
    }

    public static android.support.v4.app.ek a(android.app.Notification p8, int p9, android.support.v4.app.el p10, android.support.v4.app.fx p11)
    {
        try {
            android.os.Bundle v5;
            android.support.v4.app.fx v1_0 = android.support.v4.app.et.g(p8)[p9];
            android.support.v4.app.ek v0_1 = android.support.v4.app.et.a(p8);
        } catch (android.support.v4.app.ek v0_12) {
            throw v0_12;
        }
        if (v0_1 == null) {
            v5 = 0;
        } else {
            android.support.v4.app.ek v0_2 = v0_1.getSparseParcelableArray("android.support.actionExtras");
            if (v0_2 == null) {
            } else {
                v5 = ((android.os.Bundle) v0_2.get(p9));
            }
        }
        android.support.v4.app.ek v0_7 = android.support.v4.app.et.a(p10, p11, android.support.v4.app.et.t.getInt(v1_0), ((CharSequence) android.support.v4.app.et.u.get(v1_0)), ((android.app.PendingIntent) android.support.v4.app.et.v.get(v1_0)), v5);
        return v0_7;
    }

    private static android.support.v4.app.ek a(android.os.Bundle p6, android.support.v4.app.el p7, android.support.v4.app.fx p8)
    {
        return p7.a(p6.getInt("icon"), p6.getCharSequence("title"), ((android.app.PendingIntent) p6.getParcelable("actionIntent")), p6.getBundle("extras"), android.support.v4.app.fy.a(android.support.v4.app.aw.a(p6, "remoteInputs"), p8));
    }

    public static android.support.v4.app.ek a(android.support.v4.app.el p6, android.support.v4.app.fx p7, int p8, CharSequence p9, android.app.PendingIntent p10, android.os.Bundle p11)
    {
        android.support.v4.app.fw[] v5 = 0;
        if (p11 != null) {
            v5 = android.support.v4.app.fy.a(android.support.v4.app.aw.a(p11, "android.support.remoteInputs"), p7);
        }
        return p6.a(p8, p9, p10, p11, v5);
    }

    public static android.util.SparseArray a(java.util.List p4)
    {
        android.util.SparseArray v1_0 = 0;
        int v3 = p4.size();
        int v2 = 0;
        while (v2 < v3) {
            int v0_2 = ((android.os.Bundle) p4.get(v2));
            if (v0_2 != 0) {
                if (v1_0 == null) {
                    v1_0 = new android.util.SparseArray();
                }
                v1_0.put(v2, v0_2);
            }
            v2++;
        }
        return v1_0;
    }

    public static java.util.ArrayList a(android.support.v4.app.ek[] p7)
    {
        java.util.ArrayList v0_1;
        if (p7 != null) {
            v0_1 = new java.util.ArrayList(p7.length);
            int v2 = p7.length;
            int v1_1 = 0;
            while (v1_1 < v2) {
                android.os.Bundle[] v3_0 = p7[v1_1];
                android.os.Bundle v4_1 = new android.os.Bundle();
                v4_1.putInt("icon", v3_0.a());
                v4_1.putCharSequence("title", v3_0.b());
                v4_1.putParcelable("actionIntent", v3_0.c());
                v4_1.putBundle("extras", v3_0.d());
                v4_1.putParcelableArray("remoteInputs", android.support.v4.app.fy.a(v3_0.e()));
                v0_1.add(v4_1);
                v1_1++;
            }
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public static void a(android.support.v4.app.dc p2, CharSequence p3, boolean p4, CharSequence p5, android.graphics.Bitmap p6, android.graphics.Bitmap p7, boolean p8)
    {
        android.app.Notification$BigPictureStyle v0_3 = new android.app.Notification$BigPictureStyle(p2.a()).setBigContentTitle(p3).bigPicture(p6);
        if (p8) {
            v0_3.bigLargeIcon(p7);
        }
        if (p4) {
            v0_3.setSummaryText(p5);
        }
        return;
    }

    public static void a(android.support.v4.app.dc p2, CharSequence p3, boolean p4, CharSequence p5, CharSequence p6)
    {
        android.app.Notification$BigTextStyle v0_3 = new android.app.Notification$BigTextStyle(p2.a()).setBigContentTitle(p3).bigText(p6);
        if (p4) {
            v0_3.setSummaryText(p5);
        }
        return;
    }

    public static void a(android.support.v4.app.dc p3, CharSequence p4, boolean p5, CharSequence p6, java.util.ArrayList p7)
    {
        android.app.Notification$InboxStyle v1_1 = new android.app.Notification$InboxStyle(p3.a()).setBigContentTitle(p4);
        if (p5) {
            v1_1.setSummaryText(p6);
        }
        java.util.Iterator v2 = p7.iterator();
        while (v2.hasNext()) {
            v1_1.addLine(((CharSequence) v2.next()));
        }
        return;
    }

    private static boolean a()
    {
        int v0 = 0;
        if (!android.support.v4.app.et.w) {
            try {
                if (android.support.v4.app.et.s == null) {
                    boolean v2_3 = Class.forName("android.app.Notification$Action");
                    android.support.v4.app.et.r = v2_3;
                    android.support.v4.app.et.t = v2_3.getDeclaredField("icon");
                    android.support.v4.app.et.u = android.support.v4.app.et.r.getDeclaredField("title");
                    android.support.v4.app.et.v = android.support.v4.app.et.r.getDeclaredField("actionIntent");
                    boolean v2_10 = android.app.Notification.getDeclaredField("actions");
                    android.support.v4.app.et.s = v2_10;
                    v2_10.setAccessible(1);
                }
            } catch (boolean v2_12) {
                android.util.Log.e("NotificationCompat", "Unable to access notification actions", v2_12);
                android.support.v4.app.et.w = 1;
            } catch (boolean v2_11) {
                android.util.Log.e("NotificationCompat", "Unable to access notification actions", v2_11);
                android.support.v4.app.et.w = 1;
            }
            if (!android.support.v4.app.et.w) {
                v0 = 1;
            }
        }
        return v0;
    }

    public static android.support.v4.app.ek[] a(java.util.ArrayList p8, android.support.v4.app.el p9, android.support.v4.app.fx p10)
    {
        android.support.v4.app.ek[] v0_3;
        if (p8 != null) {
            android.support.v4.app.ek[] v7 = p9.a(p8.size());
            int v6 = 0;
            while (v6 < v7.length) {
                android.support.v4.app.ek[] v0_5 = ((android.os.Bundle) p8.get(v6));
                v7[v6] = p9.a(v0_5.getInt("icon"), v0_5.getCharSequence("title"), ((android.app.PendingIntent) v0_5.getParcelable("actionIntent")), v0_5.getBundle("extras"), android.support.v4.app.fy.a(android.support.v4.app.aw.a(v0_5, "remoteInputs"), p10));
                v6++;
            }
            v0_3 = v7;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public static int b(android.app.Notification p2)
    {
        try {
            int v0_1;
            int v0_0 = android.support.v4.app.et.g(p2);
        } catch (int v0_2) {
            throw v0_2;
        }
        if (v0_0 == 0) {
            v0_1 = 0;
        } else {
            v0_1 = v0_0.length;
        }
        return v0_1;
    }

    public static boolean c(android.app.Notification p2)
    {
        return android.support.v4.app.et.a(p2).getBoolean("android.support.localOnly");
    }

    public static String d(android.app.Notification p2)
    {
        return android.support.v4.app.et.a(p2).getString("android.support.groupKey");
    }

    public static boolean e(android.app.Notification p2)
    {
        return android.support.v4.app.et.a(p2).getBoolean("android.support.isGroupSummary");
    }

    public static String f(android.app.Notification p2)
    {
        return android.support.v4.app.et.a(p2).getString("android.support.sortKey");
    }

    private static Object[] g(android.app.Notification p5)
    {
        try {
            int v0_4;
            if (android.support.v4.app.et.a()) {
                try {
                    v0_4 = ((Object[]) ((Object[]) android.support.v4.app.et.s.get(p5)));
                } catch (int v0_5) {
                    android.util.Log.e("NotificationCompat", "Unable to access notification actions", v0_5);
                    android.support.v4.app.et.w = 1;
                    v0_4 = 0;
                }
            } else {
                v0_4 = 0;
            }
        } catch (int v0_7) {
            throw v0_7;
        }
        return v0_4;
    }
}
