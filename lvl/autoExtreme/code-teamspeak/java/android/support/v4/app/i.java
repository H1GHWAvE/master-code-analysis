package android.support.v4.app;
final class i extends android.graphics.drawable.InsetDrawable implements android.graphics.drawable.Drawable$Callback {
    float a;
    float b;
    final synthetic android.support.v4.app.a c;
    private final boolean d;
    private final android.graphics.Rect e;

    private i(android.support.v4.app.a p4, android.graphics.drawable.Drawable p5)
    {
        android.graphics.Rect v0_0 = 0;
        this.c = p4;
        this(p5, 0);
        if (android.os.Build$VERSION.SDK_INT > 18) {
            v0_0 = 1;
        }
        this.d = v0_0;
        this.e = new android.graphics.Rect();
        return;
    }

    synthetic i(android.support.v4.app.a p1, android.graphics.drawable.Drawable p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    private float a()
    {
        return this.a;
    }

    private void b(float p1)
    {
        this.b = p1;
        this.invalidateSelf();
        return;
    }

    public final void a(float p1)
    {
        this.a = p1;
        this.invalidateSelf();
        return;
    }

    public final void draw(android.graphics.Canvas p7)
    {
        int v1_6;
        int v0_0 = 1;
        this.copyBounds(this.e);
        p7.save();
        if (android.support.v4.view.cx.f(android.support.v4.app.a.a(this.c).getWindow().getDecorView()) != 1) {
            v1_6 = 0;
        } else {
            v1_6 = 1;
        }
        if (v1_6 != 0) {
            v0_0 = -1;
        }
        int v2_1 = this.e.width();
        p7.translate((((float) v0_0) * (((- this.b) * ((float) v2_1)) * this.a)), 0);
        if ((v1_6 != 0) && (!this.d)) {
            p7.translate(((float) v2_1), 0);
            p7.scale(-1082130432, 1065353216);
        }
        super.draw(p7);
        p7.restore();
        return;
    }
}
