package android.support.v4.app;
final class ev {

    ev()
    {
        return;
    }

    private static android.os.Bundle a(android.app.Notification p1)
    {
        return p1.extras;
    }

    private static android.support.v4.app.ek a(android.app.Notification p6, int p7, android.support.v4.app.el p8, android.support.v4.app.fx p9)
    {
        android.support.v4.app.fx v1_0 = p6.actions[p7];
        android.os.Bundle v5 = 0;
        android.support.v4.app.ek v0_2 = p6.extras.getSparseParcelableArray("android.support.actionExtras");
        if (v0_2 != null) {
            v5 = ((android.os.Bundle) v0_2.get(p7));
        }
        return android.support.v4.app.et.a(p8, p9, v1_0.icon, v1_0.title, v1_0.actionIntent, v5);
    }

    private static int b(android.app.Notification p1)
    {
        int v0_1;
        if (p1.actions == null) {
            v0_1 = 0;
        } else {
            v0_1 = p1.actions.length;
        }
        return v0_1;
    }

    private static boolean c(android.app.Notification p2)
    {
        return p2.extras.getBoolean("android.support.localOnly");
    }

    private static String d(android.app.Notification p2)
    {
        return p2.extras.getString("android.support.groupKey");
    }

    private static boolean e(android.app.Notification p2)
    {
        return p2.extras.getBoolean("android.support.isGroupSummary");
    }

    private static String f(android.app.Notification p2)
    {
        return p2.extras.getString("android.support.sortKey");
    }
}
