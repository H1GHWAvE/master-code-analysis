package android.support.v4.app;
final class ae {
    final android.app.ActivityOptions a;

    ae(android.app.ActivityOptions p1)
    {
        this.a = p1;
        return;
    }

    private android.os.Bundle a()
    {
        return this.a.toBundle();
    }

    private static android.support.v4.app.ae a(android.content.Context p2, int p3, int p4)
    {
        return new android.support.v4.app.ae(android.app.ActivityOptions.makeCustomAnimation(p2, p3, p4));
    }

    private static android.support.v4.app.ae a(android.view.View p2, int p3, int p4, int p5, int p6)
    {
        return new android.support.v4.app.ae(android.app.ActivityOptions.makeScaleUpAnimation(p2, p3, p4, p5, p6));
    }

    private static android.support.v4.app.ae a(android.view.View p2, android.graphics.Bitmap p3, int p4, int p5)
    {
        return new android.support.v4.app.ae(android.app.ActivityOptions.makeThumbnailScaleUpAnimation(p2, p3, p4, p5));
    }

    private void a(android.support.v4.app.ae p3)
    {
        this.a.update(p3.a);
        return;
    }
}
