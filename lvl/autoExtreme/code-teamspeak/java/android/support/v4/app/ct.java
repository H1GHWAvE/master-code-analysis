package android.support.v4.app;
final class ct extends android.support.v4.app.cr {
    static final String a = "LoaderManager";
    static boolean b;
    final android.support.v4.n.w c;
    final android.support.v4.n.w d;
    final String e;
    boolean f;
    boolean g;
    boolean h;
    boolean i;
    android.support.v4.app.bh j;

    static ct()
    {
        android.support.v4.app.ct.b = 0;
        return;
    }

    ct(String p2, android.support.v4.app.bh p3, boolean p4)
    {
        this.c = new android.support.v4.n.w();
        this.d = new android.support.v4.n.w();
        this.e = p2;
        this.j = p3;
        this.f = p4;
        return;
    }

    static synthetic android.support.v4.app.bh a(android.support.v4.app.ct p1)
    {
        return p1.j;
    }

    private void a(android.support.v4.app.bh p1)
    {
        this.j = p1;
        return;
    }

    private android.support.v4.app.cu c(int p3, android.os.Bundle p4, android.support.v4.app.cs p5)
    {
        android.support.v4.app.cu v0_1 = new android.support.v4.app.cu(this, p3, p4, p5);
        v0_1.d = p5.a();
        return v0_1;
    }

    private android.support.v4.app.cu d(int p3, android.os.Bundle p4, android.support.v4.app.cs p5)
    {
        try {
            this.i = 1;
            Throwable v0_1 = this.c(p3, p4, p5);
            this.a(v0_1);
            this.i = 0;
            return v0_1;
        } catch (Throwable v0_2) {
            this.i = 0;
            throw v0_2;
        }
    }

    private void h()
    {
        if (this.g) {
            if (android.support.v4.app.ct.b) {
                android.util.Log.v("LoaderManager", new StringBuilder("Finished Retaining in ").append(this).toString());
            }
            this.g = 0;
            int v1_4 = (this.c.a() - 1);
            while (v1_4 >= 0) {
                int v0_8 = ((android.support.v4.app.cu) this.c.d(v1_4));
                if (v0_8.i) {
                    if (android.support.v4.app.ct.b) {
                        android.util.Log.v("LoaderManager", new StringBuilder("  Finished Retaining: ").append(v0_8).toString());
                    }
                    v0_8.i = 0;
                    if ((v0_8.h != v0_8.j) && (!v0_8.h)) {
                        v0_8.b();
                    }
                }
                if ((v0_8.h) && ((v0_8.e) && (!v0_8.k))) {
                    v0_8.b(v0_8.d, v0_8.g);
                }
                v1_4--;
            }
        }
        return;
    }

    public final android.support.v4.c.aa a(int p5, android.os.Bundle p6, android.support.v4.app.cs p7)
    {
        if (!this.i) {
            android.support.v4.c.aa v0_3 = ((android.support.v4.app.cu) this.c.a(p5));
            if (android.support.v4.app.ct.b) {
                android.util.Log.v("LoaderManager", new StringBuilder("initLoader in ").append(this).append(": args=").append(p6).toString());
            }
            if (v0_3 != null) {
                if (android.support.v4.app.ct.b) {
                    android.util.Log.v("LoaderManager", new StringBuilder("  Re-using existing loader ").append(v0_3).toString());
                }
                v0_3.c = p7;
            } else {
                v0_3 = this.d(p5, p6, p7);
                if (android.support.v4.app.ct.b) {
                    android.util.Log.v("LoaderManager", new StringBuilder("  Created new loader ").append(v0_3).toString());
                }
            }
            if ((v0_3.e) && (this.f)) {
                v0_3.b(v0_3.d, v0_3.g);
            }
            return v0_3.d;
        } else {
            throw new IllegalStateException("Called while creating a loader");
        }
    }

    public final void a(int p4)
    {
        if (!this.i) {
            if (android.support.v4.app.ct.b) {
                android.util.Log.v("LoaderManager", new StringBuilder("destroyLoader in ").append(this).append(" of ").append(p4).toString());
            }
            int v1_6 = this.c.e(p4);
            if (v1_6 >= 0) {
                android.support.v4.app.bl v0_6 = ((android.support.v4.app.cu) this.c.d(v1_6));
                this.c.b(v1_6);
                v0_6.c();
            }
            int v1_7 = this.d.e(p4);
            if (v1_7 >= 0) {
                android.support.v4.app.bl v0_10 = ((android.support.v4.app.cu) this.d.d(v1_7));
                this.d.b(v1_7);
                v0_10.c();
            }
            if ((this.j != null) && (!this.a())) {
                this.j.f.j();
            }
            return;
        } else {
            throw new IllegalStateException("Called while creating a loader");
        }
    }

    final void a(android.support.v4.app.cu p3)
    {
        this.c.a(p3.a, p3);
        if (this.f) {
            p3.a();
        }
        return;
    }

    public final void a(String p6, java.io.FileDescriptor p7, java.io.PrintWriter p8, String[] p9)
    {
        int v2 = 0;
        if (this.c.a() > 0) {
            p8.print(p6);
            p8.println("Active Loaders:");
            String v3_0 = new StringBuilder().append(p6).append("    ").toString();
            int v1_1 = 0;
            while (v1_1 < this.c.a()) {
                android.support.v4.app.cu v0_23 = ((android.support.v4.app.cu) this.c.d(v1_1));
                p8.print(p6);
                p8.print("  #");
                p8.print(this.c.c(v1_1));
                p8.print(": ");
                p8.println(v0_23.toString());
                v0_23.a(v3_0, p7, p8, p9);
                v1_1++;
            }
        }
        if (this.d.a() > 0) {
            p8.print(p6);
            p8.println("Inactive Loaders:");
            int v1_3 = new StringBuilder().append(p6).append("    ").toString();
            while (v2 < this.d.a()) {
                android.support.v4.app.cu v0_20 = ((android.support.v4.app.cu) this.d.d(v2));
                p8.print(p6);
                p8.print("  #");
                p8.print(this.d.c(v2));
                p8.print(": ");
                p8.println(v0_20.toString());
                v0_20.a(v1_3, p7, p8, p9);
                v2++;
            }
        }
        return;
    }

    public final boolean a()
    {
        int v4 = this.c.a();
        int v2 = 0;
        int v3 = 0;
        while (v2 < v4) {
            int v0_5;
            int v0_3 = ((android.support.v4.app.cu) this.c.d(v2));
            if ((!v0_3.h) || (v0_3.f)) {
                v0_5 = 0;
            } else {
                v0_5 = 1;
            }
            v3 |= v0_5;
            v2++;
        }
        return v3;
    }

    public final android.support.v4.c.aa b(int p3)
    {
        if (!this.i) {
            android.support.v4.c.aa v0_4;
            android.support.v4.c.aa v0_3 = ((android.support.v4.app.cu) this.c.a(p3));
            if (v0_3 == null) {
                v0_4 = 0;
            } else {
                if (v0_3.n == null) {
                    v0_4 = v0_3.d;
                } else {
                    v0_4 = v0_3.n.d;
                }
            }
            return v0_4;
        } else {
            throw new IllegalStateException("Called while creating a loader");
        }
    }

    public final android.support.v4.c.aa b(int p6, android.os.Bundle p7, android.support.v4.app.cs p8)
    {
        if (!this.i) {
            android.support.v4.c.aa v0_3 = ((android.support.v4.app.cu) this.c.a(p6));
            if (android.support.v4.app.ct.b) {
                android.util.Log.v("LoaderManager", new StringBuilder("restartLoader in ").append(this).append(": args=").append(p7).toString());
            }
            android.support.v4.c.aa v0_5;
            if (v0_3 == null) {
                v0_5 = this.d(p6, p7, p8).d;
            } else {
                android.support.v4.app.cu v1_4 = ((android.support.v4.app.cu) this.d.a(p6));
                if (v1_4 == null) {
                    if (android.support.v4.app.ct.b) {
                        android.util.Log.v("LoaderManager", new StringBuilder("  Making last loader inactive: ").append(v0_3).toString());
                    }
                } else {
                    if (!v0_3.e) {
                        if (v0_3.h) {
                            if (android.support.v4.app.ct.b) {
                                android.util.Log.v("LoaderManager", "  Current loader is running; attempting to cancel");
                            }
                            if (android.support.v4.app.ct.b) {
                                android.util.Log.v("LoaderManager", new StringBuilder("  Canceling: ").append(v0_3).toString());
                            }
                            if ((v0_3.h) && ((v0_3.d != null) && ((v0_3.m) && (!v0_3.d.j())))) {
                                v0_3.d();
                            }
                            if (v0_3.n != null) {
                                if (android.support.v4.app.ct.b) {
                                    android.util.Log.v("LoaderManager", new StringBuilder("  Removing pending loader: ").append(v0_3.n).toString());
                                }
                                v0_3.n.c();
                                v0_3.n = 0;
                            }
                            if (android.support.v4.app.ct.b) {
                                android.util.Log.v("LoaderManager", "  Enqueuing as new pending loader");
                            }
                            v0_3.n = this.c(p6, p7, p8);
                            v0_5 = v0_3.n.d;
                            return v0_5;
                        } else {
                            if (android.support.v4.app.ct.b) {
                                android.util.Log.v("LoaderManager", "  Current loader is stopped; replacing");
                            }
                            this.c.a(p6, 0);
                            v0_3.c();
                        }
                    } else {
                        if (android.support.v4.app.ct.b) {
                            android.util.Log.v("LoaderManager", new StringBuilder("  Removing last inactive loader: ").append(v0_3).toString());
                        }
                        v1_4.f = 0;
                        v1_4.c();
                    }
                }
                v0_3.d.u = 1;
                this.d.a(p6, v0_3);
            }
            return v0_5;
        } else {
            throw new IllegalStateException("Called while creating a loader");
        }
    }

    final void b()
    {
        if (android.support.v4.app.ct.b) {
            android.util.Log.v("LoaderManager", new StringBuilder("Starting in ").append(this).toString());
        }
        if (!this.f) {
            this.f = 1;
            int v1_4 = (this.c.a() - 1);
            while (v1_4 >= 0) {
                ((android.support.v4.app.cu) this.c.d(v1_4)).a();
                v1_4--;
            }
        } else {
            int v0_12 = new RuntimeException("here");
            v0_12.fillInStackTrace();
            android.util.Log.w("LoaderManager", new StringBuilder("Called doStart when already started: ").append(this).toString(), v0_12);
        }
        return;
    }

    final void c()
    {
        if (android.support.v4.app.ct.b) {
            android.util.Log.v("LoaderManager", new StringBuilder("Stopping in ").append(this).toString());
        }
        if (this.f) {
            int v1_4 = (this.c.a() - 1);
            while (v1_4 >= 0) {
                ((android.support.v4.app.cu) this.c.d(v1_4)).b();
                v1_4--;
            }
            this.f = 0;
        } else {
            int v0_12 = new RuntimeException("here");
            v0_12.fillInStackTrace();
            android.util.Log.w("LoaderManager", new StringBuilder("Called doStop when not started: ").append(this).toString(), v0_12);
        }
        return;
    }

    final void d()
    {
        if (android.support.v4.app.ct.b) {
            android.util.Log.v("LoaderManager", new StringBuilder("Retaining in ").append(this).toString());
        }
        if (this.f) {
            this.g = 1;
            this.f = 0;
            int v1_4 = (this.c.a() - 1);
            while (v1_4 >= 0) {
                int v0_8 = ((android.support.v4.app.cu) this.c.d(v1_4));
                if (android.support.v4.app.ct.b) {
                    android.util.Log.v("LoaderManager", new StringBuilder("  Retaining: ").append(v0_8).toString());
                }
                v0_8.i = 1;
                v0_8.j = v0_8.h;
                v0_8.h = 0;
                v0_8.c = 0;
                v1_4--;
            }
        } else {
            int v0_11 = new RuntimeException("here");
            v0_11.fillInStackTrace();
            android.util.Log.w("LoaderManager", new StringBuilder("Called doRetain when not started: ").append(this).toString(), v0_11);
        }
        return;
    }

    final void e()
    {
        int v1 = (this.c.a() - 1);
        while (v1 >= 0) {
            ((android.support.v4.app.cu) this.c.d(v1)).k = 1;
            v1--;
        }
        return;
    }

    final void f()
    {
        int v1 = (this.c.a() - 1);
        while (v1 >= 0) {
            int v0_5 = ((android.support.v4.app.cu) this.c.d(v1));
            if ((v0_5.h) && (v0_5.k)) {
                v0_5.k = 0;
                if (v0_5.e) {
                    v0_5.b(v0_5.d, v0_5.g);
                }
            }
            v1--;
        }
        return;
    }

    final void g()
    {
        if (!this.g) {
            if (android.support.v4.app.ct.b) {
                android.util.Log.v("LoaderManager", new StringBuilder("Destroying Active in ").append(this).toString());
            }
            int v1_4 = (this.c.a() - 1);
            while (v1_4 >= 0) {
                ((android.support.v4.app.cu) this.c.d(v1_4)).c();
                v1_4--;
            }
            this.c.b();
        }
        if (android.support.v4.app.ct.b) {
            android.util.Log.v("LoaderManager", new StringBuilder("Destroying Inactive in ").append(this).toString());
        }
        int v1_9 = (this.d.a() - 1);
        while (v1_9 >= 0) {
            ((android.support.v4.app.cu) this.d.d(v1_9)).c();
            v1_9--;
        }
        this.d.b();
        return;
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder(128);
        v0_1.append("LoaderManager{");
        v0_1.append(Integer.toHexString(System.identityHashCode(this)));
        v0_1.append(" in ");
        android.support.v4.n.g.a(this.j, v0_1);
        v0_1.append("}}");
        return v0_1.toString();
    }
}
