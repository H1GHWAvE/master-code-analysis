package android.support.v4.app;
public final class bg {
    final android.support.v4.app.bh a;

    bg(android.support.v4.app.bh p1)
    {
        this.a = p1;
        return;
    }

    private static android.support.v4.app.bg a(android.support.v4.app.bh p1)
    {
        return new android.support.v4.app.bg(p1);
    }

    private android.view.View a(android.view.View p2, String p3, android.content.Context p4, android.util.AttributeSet p5)
    {
        return this.a.f.a(p2, p3, p4, p5);
    }

    private void a(android.content.res.Configuration p2)
    {
        this.a.f.a(p2);
        return;
    }

    private void a(android.os.Parcelable p2, java.util.List p3)
    {
        this.a.f.a(p2, p3);
        return;
    }

    private void a(android.support.v4.n.v p2)
    {
        this.a.g = p2;
        return;
    }

    private void a(String p4, java.io.FileDescriptor p5, java.io.PrintWriter p6, String[] p7)
    {
        android.support.v4.app.ct v0_0 = this.a;
        p6.print(p4);
        p6.print("mLoadersStarted=");
        p6.println(v0_0.j);
        if (v0_0.h != null) {
            p6.print(p4);
            p6.print("Loader Manager ");
            p6.print(Integer.toHexString(System.identityHashCode(v0_0.h)));
            p6.println(":");
            v0_0.h.a(new StringBuilder().append(p4).append("  ").toString(), p5, p6, p7);
        }
        return;
    }

    private void a(boolean p3)
    {
        android.support.v4.app.ct v0_0 = this.a;
        if ((v0_0.h != null) && (v0_0.j)) {
            v0_0.j = 0;
            if (!p3) {
                v0_0.h.c();
            } else {
                v0_0.h.d();
            }
        }
        return;
    }

    private boolean a(android.view.Menu p2)
    {
        return this.a.f.a(p2);
    }

    private boolean a(android.view.Menu p2, android.view.MenuInflater p3)
    {
        return this.a.f.a(p2, p3);
    }

    private boolean a(android.view.MenuItem p2)
    {
        return this.a.f.a(p2);
    }

    private void b(android.view.Menu p2)
    {
        this.a.f.b(p2);
        return;
    }

    private boolean b(android.view.MenuItem p2)
    {
        return this.a.f.b(p2);
    }

    private android.support.v4.app.bi d()
    {
        return this.a.f;
    }

    private android.support.v4.app.cr e()
    {
        android.support.v4.app.ct v0_1;
        android.support.v4.app.ct v0_0 = this.a;
        if (v0_0.h == null) {
            v0_0.i = 1;
            v0_0.h = v0_0.a("(root)", v0_0.j, 1);
            v0_1 = v0_0.h;
        } else {
            v0_1 = v0_0.h;
        }
        return v0_1;
    }

    private void f()
    {
        this.a.f.a(this.a, this.a, 0);
        return;
    }

    private android.os.Parcelable g()
    {
        return this.a.f.m();
    }

    private java.util.List h()
    {
        android.support.v4.app.bl v4 = this.a.f;
        java.util.ArrayList v1_0 = 0;
        if (v4.l != null) {
            int v3 = 0;
            while (v3 < v4.l.size()) {
                String v0_7 = ((android.support.v4.app.Fragment) v4.l.get(v3));
                if ((v0_7 != null) && (v0_7.V)) {
                    if (v1_0 == null) {
                        v1_0 = new java.util.ArrayList();
                    }
                    String v2_3;
                    v1_0.add(v0_7);
                    v0_7.W = 1;
                    if (v0_7.C == null) {
                        v2_3 = -1;
                    } else {
                        v2_3 = v0_7.C.z;
                    }
                    v0_7.D = v2_3;
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("retainNonConfig: keeping retained ").append(v0_7).toString());
                    }
                }
                v3++;
            }
        }
        return v1_0;
    }

    private void i()
    {
        this.a.f.n();
        return;
    }

    private void j()
    {
        this.a.f.o();
        return;
    }

    private void k()
    {
        this.a.f.p();
        return;
    }

    private void l()
    {
        this.a.f.q();
        return;
    }

    private void m()
    {
        this.a.f.c(4);
        return;
    }

    private void n()
    {
        this.a.f.r();
        return;
    }

    private void o()
    {
        this.a.f.c(2);
        return;
    }

    private void p()
    {
        this.a.f.c(1);
        return;
    }

    private void q()
    {
        this.a.f.s();
        return;
    }

    private void r()
    {
        this.a.f.t();
        return;
    }

    private void s()
    {
        android.support.v4.app.bh v0 = this.a;
        if (!v0.j) {
            v0.j = 1;
            if (v0.h == null) {
                if (!v0.i) {
                    v0.h = v0.a("(root)", v0.j, 0);
                    if ((v0.h != null) && (!v0.h.f)) {
                        v0.h.b();
                    }
                }
            } else {
                v0.h.b();
            }
            v0.i = 1;
        }
        return;
    }

    private void t()
    {
        android.support.v4.app.ct v0_0 = this.a;
        if (v0_0.h != null) {
            v0_0.h.d();
        }
        return;
    }

    private void u()
    {
        android.support.v4.app.ct v0_0 = this.a;
        if (v0_0.h != null) {
            v0_0.h.g();
        }
        return;
    }

    private void v()
    {
        int v3_0 = this.a;
        if (v3_0.g != null) {
            int v4 = v3_0.g.size();
            android.support.v4.app.ct[] v5 = new android.support.v4.app.ct[v4];
            int v1_0 = (v4 - 1);
            while (v1_0 >= 0) {
                v5[v1_0] = ((android.support.v4.app.ct) v3_0.g.c(v1_0));
                v1_0--;
            }
            int v1_1 = 0;
            while (v1_1 < v4) {
                android.support.v4.app.ct v6 = v5[v1_1];
                if (v6.g) {
                    if (android.support.v4.app.ct.b) {
                        android.util.Log.v("LoaderManager", new StringBuilder("Finished Retaining in ").append(v6).toString());
                    }
                    v6.g = 0;
                    int v3_5 = (v6.c.a() - 1);
                    while (v3_5 >= 0) {
                        int v0_12 = ((android.support.v4.app.cu) v6.c.d(v3_5));
                        if (v0_12.i) {
                            if (android.support.v4.app.ct.b) {
                                android.util.Log.v("LoaderManager", new StringBuilder("  Finished Retaining: ").append(v0_12).toString());
                            }
                            v0_12.i = 0;
                            if ((v0_12.h != v0_12.j) && (!v0_12.h)) {
                                v0_12.b();
                            }
                        }
                        if ((v0_12.h) && ((v0_12.e) && (!v0_12.k))) {
                            v0_12.b(v0_12.d, v0_12.g);
                        }
                        v3_5--;
                    }
                }
                v6.f();
                v1_1++;
            }
        }
        return;
    }

    private android.support.v4.n.v w()
    {
        int v0_1;
        int v1 = 0;
        android.support.v4.app.bh v3 = this.a;
        if (v3.g == null) {
            v0_1 = 0;
        } else {
            int v4 = v3.g.size();
            android.support.v4.app.ct[] v5 = new android.support.v4.app.ct[v4];
            String v2_0 = (v4 - 1);
            while (v2_0 >= null) {
                v5[v2_0] = ((android.support.v4.app.ct) v3.g.c(v2_0));
                v2_0--;
            }
            v0_1 = 0;
            while (v1 < v4) {
                String v2_1 = v5[v1];
                if (!v2_1.g) {
                    v2_1.g();
                    v3.g.remove(v2_1.e);
                } else {
                    v0_1 = 1;
                }
                v1++;
            }
        }
        int v0_4;
        if (v0_1 == 0) {
            v0_4 = 0;
        } else {
            v0_4 = v3.g;
        }
        return v0_4;
    }

    public final int a()
    {
        int v0_3;
        int v0_2 = this.a.f.l;
        if (v0_2 != 0) {
            v0_3 = v0_2.size();
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final java.util.List a(java.util.List p2)
    {
        if (this.a.f.l != null) {
            p2.addAll(this.a.f.l);
        } else {
            p2 = 0;
        }
        return p2;
    }

    public final void b()
    {
        this.a.f.z = 0;
        return;
    }

    public final boolean c()
    {
        return this.a.f.k();
    }
}
