package android.support.v4.app;
final class bl extends android.support.v4.app.bi implements android.support.v4.view.as {
    static final android.view.animation.Interpolator G = None;
    static final android.view.animation.Interpolator H = None;
    static final android.view.animation.Interpolator I = None;
    static final android.view.animation.Interpolator J = None;
    static final int K = 220;
    public static final int L = 1;
    public static final int M = 2;
    public static final int N = 3;
    public static final int O = 4;
    public static final int P = 5;
    public static final int Q = 6;
    static boolean b = False;
    static final String c = "FragmentManager";
    static final boolean d = False;
    static final String e = "android:target_req_state";
    static final String f = "android:target_state";
    static final String g = "android:view_state";
    static final String h = "android:user_visible_hint";
    boolean A;
    String B;
    boolean C;
    android.os.Bundle D;
    android.util.SparseArray E;
    Runnable F;
    java.util.ArrayList i;
    Runnable[] j;
    boolean k;
    java.util.ArrayList l;
    java.util.ArrayList m;
    java.util.ArrayList n;
    java.util.ArrayList o;
    java.util.ArrayList p;
    java.util.ArrayList q;
    java.util.ArrayList r;
    java.util.ArrayList s;
    int t;
    android.support.v4.app.bh u;
    android.support.v4.app.bg v;
    android.support.v4.app.bf w;
    android.support.v4.app.Fragment x;
    boolean y;
    boolean z;

    static bl()
    {
        android.view.animation.AccelerateInterpolator v0_0 = 0;
        android.support.v4.app.bl.b = 0;
        if (android.os.Build$VERSION.SDK_INT >= 11) {
            v0_0 = 1;
        }
        android.support.v4.app.bl.d = v0_0;
        android.support.v4.app.bl.G = new android.view.animation.DecelerateInterpolator(1075838976);
        android.support.v4.app.bl.H = new android.view.animation.DecelerateInterpolator(1069547520);
        android.support.v4.app.bl.I = new android.view.animation.AccelerateInterpolator(1075838976);
        android.support.v4.app.bl.J = new android.view.animation.AccelerateInterpolator(1069547520);
        return;
    }

    bl()
    {
        this.t = 0;
        this.D = 0;
        this.E = 0;
        this.F = new android.support.v4.app.bm(this);
        return;
    }

    private android.support.v4.view.as A()
    {
        return this;
    }

    private static int a(int p1, boolean p2)
    {
        int v0 = -1;
        switch (p1) {
            case 4097:
                if (!p2) {
                    v0 = 2;
                } else {
                    v0 = 1;
                }
                break;
            case 4099:
                if (!p2) {
                    v0 = 6;
                } else {
                    v0 = 5;
                }
                break;
            case 8194:
                if (!p2) {
                    v0 = 4;
                } else {
                    v0 = 3;
                }
                break;
        }
        return v0;
    }

    private static android.view.animation.Animation a(float p4, float p5)
    {
        android.view.animation.AlphaAnimation v0_1 = new android.view.animation.AlphaAnimation(p4, p5);
        v0_1.setInterpolator(android.support.v4.app.bl.H);
        v0_1.setDuration(220);
        return v0_1;
    }

    private static android.view.animation.Animation a(float p12, float p13, float p14, float p15)
    {
        android.view.animation.AnimationSet v9_1 = new android.view.animation.AnimationSet(0);
        android.view.animation.AlphaAnimation v0_2 = new android.view.animation.ScaleAnimation(p12, p13, p12, p13, 1, 1056964608, 1, 1056964608);
        v0_2.setInterpolator(android.support.v4.app.bl.G);
        v0_2.setDuration(220);
        v9_1.addAnimation(v0_2);
        android.view.animation.AlphaAnimation v0_4 = new android.view.animation.AlphaAnimation(p14, p15);
        v0_4.setInterpolator(android.support.v4.app.bl.H);
        v0_4.setDuration(220);
        v9_1.addAnimation(v0_4);
        return v9_1;
    }

    private android.view.animation.Animation a(android.support.v4.app.Fragment p7, int p8, boolean p9, int p10)
    {
        int v0_3;
        android.support.v4.app.Fragment.r();
        if (p7.aa == 0) {
            if (p8 != 0) {
                int v0_4 = -1;
                switch (p8) {
                    case 4097:
                        if (!p9) {
                            v0_4 = 2;
                        } else {
                            v0_4 = 1;
                        }
                        break;
                    case 4099:
                        if (!p9) {
                            v0_4 = 6;
                        } else {
                            v0_4 = 5;
                        }
                        break;
                    case 8194:
                        if (!p9) {
                            v0_4 = 4;
                        } else {
                            v0_4 = 3;
                        }
                        break;
                }
                if (v0_4 >= 0) {
                    switch (v0_4) {
                        case 1:
                            v0_3 = android.support.v4.app.bl.a(1066401792, 1065353216, 0, 1065353216);
                            break;
                        case 2:
                            v0_3 = android.support.v4.app.bl.a(1065353216, 1064933786, 1065353216, 0);
                            break;
                        case 3:
                            v0_3 = android.support.v4.app.bl.a(1064933786, 1065353216, 0, 1065353216);
                            break;
                        case 4:
                            v0_3 = android.support.v4.app.bl.a(1065353216, 1065982362, 1065353216, 0);
                            break;
                        case 5:
                            v0_3 = android.support.v4.app.bl.a(0, 1065353216);
                            break;
                        case 6:
                            v0_3 = android.support.v4.app.bl.a(1065353216, 0);
                            break;
                        default:
                            if (p10 != 0) {
                                if (p10 != 0) {
                                    v0_3 = 0;
                                } else {
                                    v0_3 = 0;
                                }
                            } else {
                                if (!this.u.e()) {
                                } else {
                                    p10 = this.u.f();
                                }
                            }
                    }
                } else {
                    v0_3 = 0;
                }
            } else {
                v0_3 = 0;
            }
        } else {
            v0_3 = android.view.animation.AnimationUtils.loadAnimation(this.u.c, p7.aa);
            if (v0_3 == 0) {
            }
        }
        return v0_3;
    }

    private void a(int p5, android.support.v4.app.ak p6)
    {
        try {
            if (this.q == null) {
                this.q = new java.util.ArrayList();
            }
        } catch (int v0_11) {
            throw v0_11;
        }
        int v0_4 = this.q.size();
        if (p5 >= v0_4) {
            while (v0_4 < p5) {
                this.q.add(0);
                if (this.r == null) {
                    this.r = new java.util.ArrayList();
                }
                if (android.support.v4.app.bl.b) {
                    android.util.Log.v("FragmentManager", new StringBuilder("Adding available back stack index ").append(v0_4).toString());
                }
                this.r.add(Integer.valueOf(v0_4));
                v0_4++;
            }
            if (android.support.v4.app.bl.b) {
                android.util.Log.v("FragmentManager", new StringBuilder("Adding back stack index ").append(p5).append(" with ").append(p6).toString());
            }
            this.q.add(p6);
        } else {
            if (android.support.v4.app.bl.b) {
                android.util.Log.v("FragmentManager", new StringBuilder("Setting back stack index ").append(p5).append(" to ").append(p6).toString());
            }
            this.q.set(p5, p6);
        }
        return;
    }

    private void a(RuntimeException p5)
    {
        android.util.Log.e("FragmentManager", p5.getMessage());
        android.util.Log.e("FragmentManager", "Activity state:");
        String v1_4 = new java.io.PrintWriter(new android.support.v4.n.h("FragmentManager"));
        if (this.u == null) {
            try {
                String[] v3_1 = new String[0];
                this.a("  ", 0, v1_4, v3_1);
            } catch (Exception v0_6) {
                android.util.Log.e("FragmentManager", "Failed dumping state", v0_6);
            }
        } else {
            try {
                String[] v3_3 = new String[0];
                this.u.a("  ", v1_4, v3_3);
            } catch (Exception v0_8) {
                android.util.Log.e("FragmentManager", "Failed dumping state", v0_8);
            }
        }
        throw p5;
    }

    static boolean a(android.view.View p5, android.view.animation.Animation p6)
    {
        int v1 = 0;
        if ((android.support.v4.view.cx.e(p5) == 0) && (android.support.v4.view.cx.x(p5))) {
            int v0_5;
            if (!(p6 instanceof android.view.animation.AlphaAnimation)) {
                if ((p6 instanceof android.view.animation.AnimationSet)) {
                    java.util.List v3 = ((android.view.animation.AnimationSet) p6).getAnimations();
                    int v0_4 = 0;
                    while (v0_4 < v3.size()) {
                        if (!(v3.get(v0_4) instanceof android.view.animation.AlphaAnimation)) {
                            v0_4++;
                        } else {
                            v0_5 = 1;
                        }
                        if (v0_5 != 0) {
                            v1 = 1;
                        }
                        return v1;
                    }
                }
                v0_5 = 0;
            } else {
                v0_5 = 1;
            }
        }
        return v1;
    }

    private static boolean a(android.view.animation.Animation p5)
    {
        int v1 = 0;
        if (!(p5 instanceof android.view.animation.AlphaAnimation)) {
            if ((p5 instanceof android.view.animation.AnimationSet)) {
                java.util.List v3 = ((android.view.animation.AnimationSet) p5).getAnimations();
                int v0_2 = 0;
                while (v0_2 < v3.size()) {
                    if (!(v3.get(v0_2) instanceof android.view.animation.AlphaAnimation)) {
                        v0_2++;
                    } else {
                        v1 = 1;
                        break;
                    }
                }
            }
        } else {
            v1 = 1;
        }
        return v1;
    }

    private android.support.v4.app.Fragment b(String p6)
    {
        if ((this.l == null) || (p6 == null)) {
            int v0_4 = 0;
        } else {
            int v3 = (this.l.size() - 1);
            while (v3 >= 0) {
                v0_4 = ((android.support.v4.app.Fragment) this.l.get(v3));
                if (v0_4 != 0) {
                    if (!p6.equals(v0_4.A)) {
                        if (v0_4.O == null) {
                            v0_4 = 0;
                        } else {
                            android.support.v4.app.bl v4 = v0_4.O;
                            if ((v4.l != null) && (p6 != null)) {
                                int v2_3 = (v4.l.size() - 1);
                                while (v2_3 >= 0) {
                                    int v0_13 = ((android.support.v4.app.Fragment) v4.l.get(v2_3));
                                    if (v0_13 != 0) {
                                        v0_4 = v0_13.a(p6);
                                        if (v0_4 != 0) {
                                            if (v0_4 != 0) {
                                                return v0_4;
                                            }
                                            v3--;
                                        }
                                    }
                                    v2_3--;
                                }
                            }
                            v0_4 = 0;
                        }
                    }
                }
            }
        }
        return v0_4;
    }

    private void b(android.support.v4.app.ak p2)
    {
        if (this.o == null) {
            this.o = new java.util.ArrayList();
        }
        this.o.add(p2);
        this.l();
        return;
    }

    private static void b(android.view.View p1, android.view.animation.Animation p2)
    {
        if ((p1 != null) && ((p2 != null) && (android.support.v4.app.bl.a(p1, p2)))) {
            p2.setAnimationListener(new android.support.v4.app.br(p1, p2));
        }
        return;
    }

    private void c(android.support.v4.app.Fragment p7)
    {
        this.a(p7, this.t, 0, 0, 0);
        return;
    }

    public static int d(int p1)
    {
        int v0 = 0;
        switch (p1) {
            case 4097:
                v0 = 8194;
                break;
            case 4099:
                v0 = 4099;
                break;
            case 8194:
                v0 = 4097;
                break;
        }
        return v0;
    }

    private void d(android.support.v4.app.Fragment p4)
    {
        if (p4.z < 0) {
            if ((this.n != null) && (this.n.size() > 0)) {
                p4.a(((Integer) this.n.remove((this.n.size() - 1))).intValue(), this.x);
                this.l.set(p4.z, p4);
            } else {
                if (this.l == null) {
                    this.l = new java.util.ArrayList();
                }
                p4.a(this.l.size(), this.x);
                this.l.add(p4);
            }
            if (android.support.v4.app.bl.b) {
                android.util.Log.v("FragmentManager", new StringBuilder("Allocated fragment index ").append(p4).toString());
            }
        }
        return;
    }

    private void e(int p4)
    {
        try {
            this.q.set(p4, 0);
        } catch (java.util.ArrayList v0_7) {
            throw v0_7;
        }
        if (this.r == null) {
            this.r = new java.util.ArrayList();
        }
        if (android.support.v4.app.bl.b) {
            android.util.Log.v("FragmentManager", new StringBuilder("Freeing back stack index ").append(p4).toString());
        }
        this.r.add(Integer.valueOf(p4));
        return;
    }

    private void e(android.support.v4.app.Fragment p6)
    {
        if (p6.z >= 0) {
            if (android.support.v4.app.bl.b) {
                android.util.Log.v("FragmentManager", new StringBuilder("Freeing fragment index ").append(p6).toString());
            }
            this.l.set(p6.z, 0);
            if (this.n == null) {
                this.n = new java.util.ArrayList();
            }
            this.n.add(Integer.valueOf(p6.z));
            this.u.b(p6.A);
            p6.z = -1;
            p6.A = 0;
            p6.F = 0;
            p6.G = 0;
            p6.H = 0;
            p6.I = 0;
            p6.J = 0;
            p6.K = 0;
            p6.L = 0;
            p6.M = 0;
            p6.O = 0;
            p6.N = 0;
            p6.Q = 0;
            p6.R = 0;
            p6.S = 0;
            p6.T = 0;
            p6.U = 0;
            p6.W = 0;
            p6.ag = 0;
            p6.ah = 0;
            p6.ai = 0;
        }
        return;
    }

    private void f(android.support.v4.app.Fragment p3)
    {
        if (p3.ad != null) {
            if (this.E != null) {
                this.E.clear();
            } else {
                this.E = new android.util.SparseArray();
            }
            p3.ad.saveHierarchyState(this.E);
            if (this.E.size() > 0) {
                p3.y = this.E;
                this.E = 0;
            }
        }
        return;
    }

    private android.os.Bundle g(android.support.v4.app.Fragment p4)
    {
        if (this.D == null) {
            this.D = new android.os.Bundle();
        }
        android.os.Bundle v0_6;
        p4.f(this.D);
        if (this.D.isEmpty()) {
            v0_6 = 0;
        } else {
            v0_6 = this.D;
            this.D = 0;
        }
        if (p4.ac != null) {
            this.f(p4);
        }
        if (p4.y != null) {
            if (v0_6 == null) {
                v0_6 = new android.os.Bundle();
            }
            v0_6.putSparseParcelableArray("android:view_state", p4.y);
        }
        if (!p4.af) {
            if (v0_6 == null) {
                v0_6 = new android.os.Bundle();
            }
            v0_6.putBoolean("android:user_visible_hint", p4.af);
        }
        return v0_6;
    }

    private void u()
    {
        if (!this.z) {
            if (this.B == null) {
                return;
            } else {
                throw new IllegalStateException(new StringBuilder("Can not perform this action inside of ").append(this.B).toString());
            }
        } else {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        }
    }

    private java.util.ArrayList v()
    {
        java.util.ArrayList v1_0 = 0;
        if (this.l != null) {
            int v3 = 0;
            while (v3 < this.l.size()) {
                String v0_6 = ((android.support.v4.app.Fragment) this.l.get(v3));
                if ((v0_6 != null) && (v0_6.V)) {
                    if (v1_0 == null) {
                        v1_0 = new java.util.ArrayList();
                    }
                    String v2_3;
                    v1_0.add(v0_6);
                    v0_6.W = 1;
                    if (v0_6.C == null) {
                        v2_3 = -1;
                    } else {
                        v2_3 = v0_6.C.z;
                    }
                    v0_6.D = v2_3;
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("retainNonConfig: keeping retained ").append(v0_6).toString());
                    }
                }
                v3++;
            }
        }
        return v1_0;
    }

    private void w()
    {
        this.z = 0;
        return;
    }

    private void x()
    {
        this.c(4);
        return;
    }

    private void y()
    {
        this.c(2);
        return;
    }

    private void z()
    {
        this.c(1);
        return;
    }

    public final int a(android.support.v4.app.ak p5)
    {
        try {
            if ((this.r != null) && (this.r.size() > 0)) {
                int v0_6 = ((Integer) this.r.remove((this.r.size() - 1))).intValue();
                if (android.support.v4.app.bl.b) {
                    android.util.Log.v("FragmentManager", new StringBuilder("Adding back stack index ").append(v0_6).append(" with ").append(p5).toString());
                }
                this.q.set(v0_6, p5);
            } else {
                if (this.q == null) {
                    this.q = new java.util.ArrayList();
                }
                v0_6 = this.q.size();
                if (android.support.v4.app.bl.b) {
                    android.util.Log.v("FragmentManager", new StringBuilder("Setting back stack index ").append(v0_6).append(" to ").append(p5).toString());
                }
                this.q.add(p5);
            }
        } catch (int v0_11) {
            throw v0_11;
        }
        return v0_6;
    }

    public final android.support.v4.app.Fragment$SavedState a(android.support.v4.app.Fragment p5)
    {
        android.support.v4.app.Fragment$SavedState v0_0 = 0;
        if (p5.z < 0) {
            this.a(new IllegalStateException(new StringBuilder("Fragment ").append(p5).append(" is not currently in the FragmentManager").toString()));
        }
        if (p5.u > 0) {
            android.os.Bundle v1_4 = this.g(p5);
            if (v1_4 != null) {
                v0_0 = new android.support.v4.app.Fragment$SavedState(v1_4);
            }
        }
        return v0_0;
    }

    public final android.support.v4.app.Fragment a(int p4)
    {
        int v0_8;
        if (this.m == null) {
            if (this.l != null) {
                int v1_1 = (this.l.size() - 1);
                while (v1_1 >= 0) {
                    v0_8 = ((android.support.v4.app.Fragment) this.l.get(v1_1));
                    if ((v0_8 != 0) && (v0_8.Q == p4)) {
                        return v0_8;
                    } else {
                        v1_1--;
                    }
                }
            }
            v0_8 = 0;
        } else {
            int v1_0 = (this.m.size() - 1);
            while (v1_0 >= 0) {
                v0_8 = ((android.support.v4.app.Fragment) this.m.get(v1_0));
                if ((v0_8 == 0) || (v0_8.Q != p4)) {
                    v1_0--;
                }
            }
        }
        return v0_8;
    }

    public final android.support.v4.app.Fragment a(android.os.Bundle p6, String p7)
    {
        android.support.v4.app.Fragment v0_7;
        String v1_0 = p6.getInt(p7, -1);
        if (v1_0 != -1) {
            if (v1_0 >= this.l.size()) {
                this.a(new IllegalStateException(new StringBuilder("Fragment no longer exists for key ").append(p7).append(": index ").append(v1_0).toString()));
            }
            v0_7 = ((android.support.v4.app.Fragment) this.l.get(v1_0));
            if (v0_7 == null) {
                this.a(new IllegalStateException(new StringBuilder("Fragment no longer exists for key ").append(p7).append(": index ").append(v1_0).toString()));
            }
        } else {
            v0_7 = 0;
        }
        return v0_7;
    }

    public final android.support.v4.app.Fragment a(String p4)
    {
        if ((this.m == null) || (p4 == null)) {
            if ((this.l != null) && (p4 != null)) {
                int v1_1 = (this.l.size() - 1);
                while (v1_1 >= 0) {
                    int v0_8 = ((android.support.v4.app.Fragment) this.l.get(v1_1));
                    if ((v0_8 != 0) && (p4.equals(v0_8.S))) {
                        return v0_8;
                    } else {
                        v1_1--;
                    }
                }
            }
            v0_8 = 0;
        } else {
            int v1_0 = (this.m.size() - 1);
            while (v1_0 >= 0) {
                v0_8 = ((android.support.v4.app.Fragment) this.m.get(v1_0));
                if ((v0_8 == 0) || (!p4.equals(v0_8.S))) {
                    v1_0--;
                }
            }
        }
        return v0_8;
    }

    public final android.support.v4.app.cd a()
    {
        return new android.support.v4.app.ak(this);
    }

    public final android.view.View a(android.view.View p11, String p12, android.content.Context p13, android.util.AttributeSet p14)
    {
        android.view.View v0_22;
        if ("fragment".equals(p12)) {
            String v6;
            android.view.View v0_3 = p14.getAttributeValue(0, "class");
            String v1_1 = p13.obtainStyledAttributes(p14, android.support.v4.app.bu.a);
            if (v0_3 != null) {
                v6 = v0_3;
            } else {
                v6 = v1_1.getString(0);
            }
            int v7 = v1_1.getResourceId(1, -1);
            String v8 = v1_1.getString(2);
            v1_1.recycle();
            if (android.support.v4.app.Fragment.b(this.u.c, v6)) {
                String v1_2;
                if (p11 == null) {
                    v1_2 = 0;
                } else {
                    v1_2 = p11.getId();
                }
                if ((v1_2 != -1) || ((v7 != -1) || (v8 != null))) {
                    android.view.View v0_9;
                    if (v7 == -1) {
                        v0_9 = 0;
                    } else {
                        v0_9 = this.a(v7);
                    }
                    if ((v0_9 == null) && (v8 != null)) {
                        v0_9 = this.a(v8);
                    }
                    if ((v0_9 == null) && (v1_2 != -1)) {
                        v0_9 = this.a(v1_2);
                    }
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("onCreateView: id=0x").append(Integer.toHexString(v7)).append(" fname=").append(v6).append(" existing=").append(v0_9).toString());
                    }
                    String v1_4;
                    if (v0_9 != null) {
                        if (!v0_9.J) {
                            v0_9.J = 1;
                            if (!v0_9.W) {
                                v0_9.q();
                            }
                            v1_4 = v0_9;
                        } else {
                            throw new IllegalArgumentException(new StringBuilder().append(p14.getPositionDescription()).append(": Duplicate id 0x").append(Integer.toHexString(v7)).append(", tag ").append(v8).append(", or parent id 0x").append(Integer.toHexString(v1_2)).append(" with another fragment for ").append(v6).toString());
                        }
                    } else {
                        android.view.View v0_12;
                        String v4_4 = android.support.v4.app.Fragment.a(p13, v6);
                        v4_4.I = 1;
                        if (v7 == 0) {
                            v0_12 = v1_2;
                        } else {
                            v0_12 = v7;
                        }
                        v4_4.Q = v0_12;
                        v4_4.R = v1_2;
                        v4_4.S = v8;
                        v4_4.J = 1;
                        v4_4.M = this;
                        v4_4.N = this.u;
                        v4_4.q();
                        this.a(v4_4, 1);
                        v1_4 = v4_4;
                    }
                    if ((this.t > 0) || (!v1_4.I)) {
                        this.c(v1_4);
                    } else {
                        this.a(v1_4, 1, 0, 0, 0);
                    }
                    if (v1_4.ac != null) {
                        if (v7 != 0) {
                            v1_4.ac.setId(v7);
                        }
                        if (v1_4.ac.getTag() == null) {
                            v1_4.ac.setTag(v8);
                        }
                        v0_22 = v1_4.ac;
                    } else {
                        throw new IllegalStateException(new StringBuilder("Fragment ").append(v6).append(" did not create a view.").toString());
                    }
                } else {
                    throw new IllegalArgumentException(new StringBuilder().append(p14.getPositionDescription()).append(": Must specify unique android:id, android:tag, or have a parent with an id for ").append(v6).toString());
                }
            } else {
                v0_22 = 0;
            }
        } else {
            v0_22 = 0;
        }
        return v0_22;
    }

    final void a(int p9, int p10, int p11, boolean p12)
    {
        if ((this.u != null) || (p9 == 0)) {
            if ((p12) || (this.t != p9)) {
                this.t = p9;
                if (this.l != null) {
                    int v6 = 0;
                    int v7_0 = 0;
                    while (v6 < this.l.size()) {
                        int v1_3;
                        int v1_2 = ((android.support.v4.app.Fragment) this.l.get(v6));
                        if (v1_2 == 0) {
                            v1_3 = v7_0;
                        } else {
                            this.a(v1_2, p9, p10, p11, 0);
                            if (v1_2.ag == null) {
                            } else {
                                v1_3 = (v7_0 | v1_2.ag.a());
                            }
                        }
                        v6++;
                        v7_0 = v1_3;
                    }
                    if (v7_0 == 0) {
                        this.j();
                    }
                    if ((this.y) && ((this.u != null) && (this.t == 5))) {
                        this.u.d();
                        this.y = 0;
                    }
                }
            }
            return;
        } else {
            throw new IllegalStateException("No host");
        }
    }

    public final void a(android.content.res.Configuration p4)
    {
        if (this.m != null) {
            int v1 = 0;
            while (v1 < this.m.size()) {
                android.support.v4.app.bl v0_6 = ((android.support.v4.app.Fragment) this.m.get(v1));
                if (v0_6 != null) {
                    v0_6.onConfigurationChanged(p4);
                    if (v0_6.O != null) {
                        v0_6.O.a(p4);
                    }
                }
                v1++;
            }
        }
        return;
    }

    public final void a(android.os.Bundle p4, String p5, android.support.v4.app.Fragment p6)
    {
        if (p6.z < 0) {
            this.a(new IllegalStateException(new StringBuilder("Fragment ").append(p6).append(" is not currently in the FragmentManager").toString()));
        }
        p4.putInt(p5, p6.z);
        return;
    }

    final void a(android.os.Parcelable p11, java.util.List p12)
    {
        if ((p11 != null) && (((android.support.v4.app.FragmentManagerState) p11).a != null)) {
            if (p12 != null) {
                int v1_0 = 0;
                while (v1_0 < p12.size()) {
                    int v0_27 = ((android.support.v4.app.Fragment) p12.get(v1_0));
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("restoreAllState: re-attaching retained ").append(v0_27).toString());
                    }
                    int v3_31 = ((android.support.v4.app.FragmentManagerState) p11).a[v0_27.z];
                    v3_31.k = v0_27;
                    v0_27.y = 0;
                    v0_27.L = 0;
                    v0_27.J = 0;
                    v0_27.F = 0;
                    v0_27.C = 0;
                    if (v3_31.j != null) {
                        v3_31.j.setClassLoader(this.u.c.getClassLoader());
                        v0_27.y = v3_31.j.getSparseParcelableArray("android:view_state");
                        v0_27.x = v3_31.j;
                    }
                    v1_0++;
                }
            }
            this.l = new java.util.ArrayList(((android.support.v4.app.FragmentManagerState) p11).a.length);
            if (this.n != null) {
                this.n.clear();
            }
            int v0_6 = 0;
            while (v0_6 < ((android.support.v4.app.FragmentManagerState) p11).a.length) {
                int v1_23 = ((android.support.v4.app.FragmentManagerState) p11).a[v0_6];
                if (v1_23 == 0) {
                    this.l.add(0);
                    if (this.n == null) {
                        this.n = new java.util.ArrayList();
                    }
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("restoreAllState: avail #").append(v0_6).toString());
                    }
                    this.n.add(Integer.valueOf(v0_6));
                } else {
                    int v3_23 = this.u;
                    java.io.PrintWriter v4_31 = this.x;
                    if (v1_23.k == null) {
                        String v5_13 = v3_23.c;
                        if (v1_23.i != null) {
                            v1_23.i.setClassLoader(v5_13.getClassLoader());
                        }
                        v1_23.k = android.support.v4.app.Fragment.a(v5_13, v1_23.a, v1_23.i);
                        if (v1_23.j != null) {
                            v1_23.j.setClassLoader(v5_13.getClassLoader());
                            v1_23.k.x = v1_23.j;
                        }
                        v1_23.k.a(v1_23.b, v4_31);
                        v1_23.k.I = v1_23.c;
                        v1_23.k.K = 1;
                        v1_23.k.Q = v1_23.d;
                        v1_23.k.R = v1_23.e;
                        v1_23.k.S = v1_23.f;
                        v1_23.k.V = v1_23.g;
                        v1_23.k.U = v1_23.h;
                        v1_23.k.M = v3_23.f;
                        if (android.support.v4.app.bl.b) {
                            android.util.Log.v("FragmentManager", new StringBuilder("Instantiated fragment ").append(v1_23.k).toString());
                        }
                    }
                    int v3_27 = v1_23.k;
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("restoreAllState: active #").append(v0_6).append(": ").append(v3_27).toString());
                    }
                    this.l.add(v3_27);
                    v1_23.k = 0;
                }
                v0_6++;
            }
            if (p12 != null) {
                int v3_0 = 0;
                while (v3_0 < p12.size()) {
                    int v0_24 = ((android.support.v4.app.Fragment) p12.get(v3_0));
                    if (v0_24.D >= 0) {
                        if (v0_24.D >= this.l.size()) {
                            android.util.Log.w("FragmentManager", new StringBuilder("Re-attaching retained fragment ").append(v0_24).append(" target no longer exists: ").append(v0_24.D).toString());
                            v0_24.C = 0;
                        } else {
                            v0_24.C = ((android.support.v4.app.Fragment) this.l.get(v0_24.D));
                        }
                    }
                    v3_0++;
                }
            }
            if (((android.support.v4.app.FragmentManagerState) p11).b == null) {
                this.m = 0;
            } else {
                this.m = new java.util.ArrayList(((android.support.v4.app.FragmentManagerState) p11).b.length);
                int v1_7 = 0;
                while (v1_7 < ((android.support.v4.app.FragmentManagerState) p11).b.length) {
                    int v0_19 = ((android.support.v4.app.Fragment) this.l.get(((android.support.v4.app.FragmentManagerState) p11).b[v1_7]));
                    if (v0_19 == 0) {
                        this.a(new IllegalStateException(new StringBuilder("No instantiated fragment for index #").append(((android.support.v4.app.FragmentManagerState) p11).b[v1_7]).toString()));
                    }
                    v0_19.F = 1;
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("restoreAllState: added #").append(v1_7).append(": ").append(v0_19).toString());
                    }
                    if (!this.m.contains(v0_19)) {
                        this.m.add(v0_19);
                        v1_7++;
                    } else {
                        throw new IllegalStateException("Already added!");
                    }
                }
            }
            if (((android.support.v4.app.FragmentManagerState) p11).c == null) {
                this.o = 0;
            } else {
                this.o = new java.util.ArrayList(((android.support.v4.app.FragmentManagerState) p11).c.length);
                int v0_16 = 0;
                while (v0_16 < ((android.support.v4.app.FragmentManagerState) p11).c.length) {
                    int v1_14 = ((android.support.v4.app.FragmentManagerState) p11).c[v0_16].a(this);
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("restoreAllState: back stack #").append(v0_16).append(" (index ").append(v1_14.y).append("): ").append(v1_14).toString());
                        v1_14.a("  ", new java.io.PrintWriter(new android.support.v4.n.h("FragmentManager")), 0);
                    }
                    this.o.add(v1_14);
                    if (v1_14.y >= 0) {
                        this.a(v1_14.y, v1_14);
                    }
                    v0_16++;
                }
            }
        }
        return;
    }

    public final void a(android.support.v4.app.Fragment p7, int p8, int p9)
    {
        if (android.support.v4.app.bl.b) {
            android.util.Log.v("FragmentManager", new StringBuilder("remove: ").append(p7).append(" nesting=").append(p7.L).toString());
        }
        android.support.v4.app.bl v0_3;
        if (p7.L <= 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        android.support.v4.app.bl v0_4;
        if (v0_3 != null) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        if ((!p7.U) || (v0_4 != null)) {
            if (this.m != null) {
                this.m.remove(p7);
            }
            if ((p7.X) && (p7.Y)) {
                this.y = 1;
            }
            int v2_11;
            p7.F = 0;
            p7.G = 1;
            if (v0_4 == null) {
                v2_11 = 1;
            } else {
                v2_11 = 0;
            }
            this.a(p7, v2_11, p8, p9, 0);
        }
        return;
    }

    final void a(android.support.v4.app.Fragment p11, int p12, int p13, int p14, boolean p15)
    {
        if (((!p11.F) || (p11.U)) && (p12 > 1)) {
            p12 = 1;
        }
        if ((p11.G) && (p12 > p11.u)) {
            p12 = p11.u;
        }
        if ((p11.ae) && ((p11.u < 4) && (p12 > 3))) {
            p12 = 3;
        }
        if (p11.u >= p12) {
            if (p11.u <= p12) {
                p11.u = p12;
            } else {
                switch (p11.u) {
                    case 1:
                        break;
                    case 2:
                        if (p12 >= 2) {
                        } else {
                            if (android.support.v4.app.bl.b) {
                                android.util.Log.v("FragmentManager", new StringBuilder("movefrom ACTIVITY_CREATED: ").append(p11).toString());
                            }
                            if ((p11.ac != null) && ((this.u.b()) && (p11.y == null))) {
                                this.f(p11);
                            }
                            if (p11.O != null) {
                                p11.O.c(1);
                            }
                            p11.Z = 0;
                            p11.g();
                            if (p11.Z) {
                                if (p11.ag != null) {
                                    p11.ag.e();
                                }
                                if ((p11.ac != null) && (p11.ab != null)) {
                                    if ((this.t <= 0) || (this.A)) {
                                        android.view.View v0_42 = 0;
                                    } else {
                                        v0_42 = this.a(p11, p13, 0, p14);
                                    }
                                    if (v0_42 != null) {
                                        p11.v = p11.ac;
                                        p11.w = p12;
                                        v0_42.setAnimationListener(new android.support.v4.app.bq(this, p11.ac, v0_42, p11));
                                        p11.ac.startAnimation(v0_42);
                                    }
                                    p11.ab.removeView(p11.ac);
                                }
                                p11.ab = 0;
                                p11.ac = 0;
                                p11.ad = 0;
                            } else {
                                throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(p11).append(" did not call through to super.onDestroyView()").toString());
                            }
                        }
                        break;
                    case 3:
                        if (p12 >= 3) {
                        } else {
                            if (android.support.v4.app.bl.b) {
                                android.util.Log.v("FragmentManager", new StringBuilder("movefrom STOPPED: ").append(p11).toString());
                            }
                            p11.x();
                        }
                        break;
                    case 4:
                        if (p12 >= 4) {
                        } else {
                            if (android.support.v4.app.bl.b) {
                                android.util.Log.v("FragmentManager", new StringBuilder("movefrom STARTED: ").append(p11).toString());
                            }
                            if (p11.O != null) {
                                p11.O.r();
                            }
                            p11.Z = 0;
                            p11.f();
                            if (p11.Z) {
                            } else {
                                throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(p11).append(" did not call through to super.onStop()").toString());
                            }
                        }
                        break;
                    case 5:
                        if (p12 >= 5) {
                        } else {
                            if (android.support.v4.app.bl.b) {
                                android.util.Log.v("FragmentManager", new StringBuilder("movefrom RESUMED: ").append(p11).toString());
                            }
                            if (p11.O != null) {
                                p11.O.c(4);
                            }
                            p11.Z = 0;
                            p11.t();
                            if (p11.Z) {
                                p11.H = 0;
                            } else {
                                throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(p11).append(" did not call through to super.onPause()").toString());
                            }
                        }
                        break;
                    default:
                }
                if (p12 > 0) {
                } else {
                    if ((this.A) && (p11.v != null)) {
                        android.view.View v0_48 = p11.v;
                        p11.v = 0;
                        v0_48.clearAnimation();
                    }
                    if (p11.v == null) {
                        if (android.support.v4.app.bl.b) {
                            android.util.Log.v("FragmentManager", new StringBuilder("movefrom CREATED: ").append(p11).toString());
                        }
                        if (!p11.W) {
                            if (p11.O != null) {
                                p11.O.s();
                            }
                            p11.Z = 0;
                            p11.u();
                            if (!p11.Z) {
                                throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(p11).append(" did not call through to super.onDestroy()").toString());
                            }
                        }
                        p11.Z = 0;
                        p11.c();
                        if (p11.Z) {
                            if (p15) {
                            } else {
                                if (p11.W) {
                                    p11.N = 0;
                                    p11.P = 0;
                                    p11.M = 0;
                                    p11.O = 0;
                                } else {
                                    if (p11.z < 0) {
                                    } else {
                                        if (android.support.v4.app.bl.b) {
                                            android.util.Log.v("FragmentManager", new StringBuilder("Freeing fragment index ").append(p11).toString());
                                        }
                                        this.l.set(p11.z, 0);
                                        if (this.n == null) {
                                            this.n = new java.util.ArrayList();
                                        }
                                        this.n.add(Integer.valueOf(p11.z));
                                        this.u.b(p11.A);
                                        p11.z = -1;
                                        p11.A = 0;
                                        p11.F = 0;
                                        p11.G = 0;
                                        p11.H = 0;
                                        p11.I = 0;
                                        p11.J = 0;
                                        p11.K = 0;
                                        p11.L = 0;
                                        p11.M = 0;
                                        p11.O = 0;
                                        p11.N = 0;
                                        p11.Q = 0;
                                        p11.R = 0;
                                        p11.S = 0;
                                        p11.T = 0;
                                        p11.U = 0;
                                        p11.W = 0;
                                        p11.ag = 0;
                                        p11.ah = 0;
                                        p11.ai = 0;
                                    }
                                }
                            }
                        } else {
                            throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(p11).append(" did not call through to super.onDetach()").toString());
                        }
                    } else {
                        p11.w = p12;
                        p12 = 1;
                    }
                }
            }
        } else {
            if ((!p11.I) || (p11.J)) {
                if (p11.v != null) {
                    p11.v = 0;
                    this.a(p11, p11.w, 0, 0, 1);
                }
                switch (p11.u) {
                    case 0:
                        if (android.support.v4.app.bl.b) {
                            android.util.Log.v("FragmentManager", new StringBuilder("moveto CREATED: ").append(p11).toString());
                        }
                        if (p11.x != null) {
                            p11.x.setClassLoader(this.u.c.getClassLoader());
                            p11.y = p11.x.getSparseParcelableArray("android:view_state");
                            p11.C = this.a(p11.x, "android:target_state");
                            if (p11.C != null) {
                                p11.E = p11.x.getInt("android:target_req_state", 0);
                            }
                            p11.af = p11.x.getBoolean("android:user_visible_hint", 1);
                            if (!p11.af) {
                                p11.ae = 1;
                                if (p12 > 3) {
                                    p12 = 3;
                                }
                            }
                        }
                        android.view.View v0_95;
                        p11.N = this.u;
                        p11.P = this.x;
                        if (this.x == null) {
                            v0_95 = this.u.f;
                        } else {
                            v0_95 = this.x.O;
                        }
                        android.view.View v0_99;
                        p11.M = v0_95;
                        p11.Z = 0;
                        p11.Z = 1;
                        if (p11.N != null) {
                            v0_99 = p11.N.b;
                        } else {
                            v0_99 = 0;
                        }
                        if (v0_99 != null) {
                            p11.Z = 0;
                            p11.a(v0_99);
                        }
                        if (p11.Z) {
                            if (!p11.W) {
                                android.view.View v0_102 = p11.x;
                                if (p11.O != null) {
                                    p11.O.z = 0;
                                }
                                p11.Z = 0;
                                p11.a(v0_102);
                                if (p11.Z) {
                                    if (v0_102 != null) {
                                        android.view.View v0_103 = v0_102.getParcelable("android:support:fragments");
                                        if (v0_103 != null) {
                                            if (p11.O == null) {
                                                p11.w();
                                            }
                                            p11.O.a(v0_103, 0);
                                            p11.O.n();
                                        }
                                    }
                                } else {
                                    throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(p11).append(" did not call through to super.onCreate()").toString());
                                }
                            }
                            p11.W = 0;
                            if (p11.I) {
                                p11.ac = p11.b(p11.b(p11.x), 0);
                                if (p11.ac == null) {
                                    p11.ad = 0;
                                } else {
                                    p11.ad = p11.ac;
                                    if (android.os.Build$VERSION.SDK_INT < 11) {
                                        p11.ac = android.support.v4.app.da.a(p11.ac);
                                    } else {
                                        android.support.v4.view.cx.w(p11.ac);
                                    }
                                    if (p11.T) {
                                        p11.ac.setVisibility(8);
                                    }
                                    p11.a(p11.ac, p11.x);
                                }
                            } else {
                                if (p12 <= 1) {
                                    if (p12 <= 3) {
                                        if (p12 <= 4) {
                                        } else {
                                            if (android.support.v4.app.bl.b) {
                                                android.util.Log.v("FragmentManager", new StringBuilder("moveto RESUMED: ").append(p11).toString());
                                            }
                                            p11.H = 1;
                                            if (p11.O != null) {
                                                p11.O.z = 0;
                                                p11.O.k();
                                            }
                                            p11.Z = 0;
                                            p11.s();
                                            if (p11.Z) {
                                                if (p11.O != null) {
                                                    p11.O.q();
                                                    p11.O.k();
                                                }
                                                p11.x = 0;
                                                p11.y = 0;
                                            } else {
                                                throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(p11).append(" did not call through to super.onResume()").toString());
                                            }
                                        }
                                    } else {
                                        if (android.support.v4.app.bl.b) {
                                            android.util.Log.v("FragmentManager", new StringBuilder("moveto STARTED: ").append(p11).toString());
                                        }
                                        if (p11.O != null) {
                                            p11.O.z = 0;
                                            p11.O.k();
                                        }
                                        p11.Z = 0;
                                        p11.e();
                                        if (p11.Z) {
                                            if (p11.O != null) {
                                                p11.O.p();
                                            }
                                            if (p11.ag == null) {
                                            } else {
                                                p11.ag.f();
                                            }
                                        } else {
                                            throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(p11).append(" did not call through to super.onStart()").toString());
                                        }
                                    }
                                } else {
                                    if (android.support.v4.app.bl.b) {
                                        android.util.Log.v("FragmentManager", new StringBuilder("moveto ACTIVITY_CREATED: ").append(p11).toString());
                                    }
                                    if (!p11.I) {
                                        android.view.View v0_122;
                                        if (p11.R == 0) {
                                            v0_122 = 0;
                                        } else {
                                            v0_122 = ((android.view.ViewGroup) this.w.a(p11.R));
                                            if ((v0_122 == null) && (!p11.K)) {
                                                this.a(new IllegalArgumentException(new StringBuilder("No view found for id 0x").append(Integer.toHexString(p11.R)).append(" (").append(p11.j().getResourceName(p11.R)).append(") for fragment ").append(p11).toString()));
                                            }
                                        }
                                        p11.ab = v0_122;
                                        p11.ac = p11.b(p11.b(p11.x), v0_122);
                                        if (p11.ac == null) {
                                            p11.ad = 0;
                                        } else {
                                            p11.ad = p11.ac;
                                            if (android.os.Build$VERSION.SDK_INT < 11) {
                                                p11.ac = android.support.v4.app.da.a(p11.ac);
                                            } else {
                                                android.support.v4.view.cx.w(p11.ac);
                                            }
                                            if (v0_122 != null) {
                                                android.os.Bundle v1_95 = this.a(p11, p13, 1, p14);
                                                if (v1_95 != null) {
                                                    android.support.v4.app.bl.b(p11.ac, v1_95);
                                                    p11.ac.startAnimation(v1_95);
                                                }
                                                v0_122.addView(p11.ac);
                                            }
                                            if (p11.T) {
                                                p11.ac.setVisibility(8);
                                            }
                                            p11.a(p11.ac, p11.x);
                                        }
                                    }
                                    android.view.View v0_128 = p11.x;
                                    if (p11.O != null) {
                                        p11.O.z = 0;
                                    }
                                    p11.Z = 0;
                                    p11.c(v0_128);
                                    if (p11.Z) {
                                        if (p11.O != null) {
                                            p11.O.o();
                                        }
                                        if (p11.ac != null) {
                                            if (p11.y != null) {
                                                p11.ad.restoreHierarchyState(p11.y);
                                                p11.y = 0;
                                            }
                                            p11.Z = 0;
                                            p11.Z = 1;
                                            if (!p11.Z) {
                                                throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(p11).append(" did not call through to super.onViewStateRestored()").toString());
                                            }
                                        }
                                        p11.x = 0;
                                    } else {
                                        throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(p11).append(" did not call through to super.onActivityCreated()").toString());
                                    }
                                }
                            }
                        } else {
                            throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(p11).append(" did not call through to super.onAttach()").toString());
                        }
                    default:
                }
            }
        }
        return;
    }

    public final void a(android.support.v4.app.Fragment p5, boolean p6)
    {
        if (this.m == null) {
            this.m = new java.util.ArrayList();
        }
        if (android.support.v4.app.bl.b) {
            android.util.Log.v("FragmentManager", new StringBuilder("add: ").append(p5).toString());
        }
        if (p5.z < 0) {
            if ((this.n != null) && (this.n.size() > 0)) {
                p5.a(((Integer) this.n.remove((this.n.size() - 1))).intValue(), this.x);
                this.l.set(p5.z, p5);
            } else {
                if (this.l == null) {
                    this.l = new java.util.ArrayList();
                }
                p5.a(this.l.size(), this.x);
                this.l.add(p5);
            }
            if (android.support.v4.app.bl.b) {
                android.util.Log.v("FragmentManager", new StringBuilder("Allocated fragment index ").append(p5).toString());
            }
        }
        if (!p5.U) {
            if (!this.m.contains(p5)) {
                this.m.add(p5);
                p5.F = 1;
                p5.G = 0;
                if ((p5.X) && (p5.Y)) {
                    this.y = 1;
                }
                if (p6) {
                    this.c(p5);
                }
            } else {
                throw new IllegalStateException(new StringBuilder("Fragment already added: ").append(p5).toString());
            }
        }
        return;
    }

    public final void a(android.support.v4.app.bh p3, android.support.v4.app.bf p4, android.support.v4.app.Fragment p5)
    {
        if (this.u == null) {
            this.u = p3;
            this.w = p4;
            this.x = p5;
            return;
        } else {
            throw new IllegalStateException("Already attached");
        }
    }

    public final void a(android.support.v4.app.bk p2)
    {
        if (this.s == null) {
            this.s = new java.util.ArrayList();
        }
        this.s.add(p2);
        return;
    }

    public final void a(Runnable p3, boolean p4)
    {
        if (!p4) {
            this.u();
        }
        try {
            if ((!this.A) && (this.u != null)) {
                if (this.i == null) {
                    this.i = new java.util.ArrayList();
                }
                this.i.add(p3);
                if (this.i.size() == 1) {
                    this.u.d.removeCallbacks(this.F);
                    this.u.d.post(this.F);
                }
                return;
            } else {
                throw new IllegalStateException("Activity has been destroyed");
            }
        } catch (android.os.Handler v0_14) {
            throw v0_14;
        }
    }

    public final void a(String p9, java.io.FileDescriptor p10, java.io.PrintWriter p11, String[] p12)
    {
        int v1 = 0;
        String v3_0 = new StringBuilder().append(p9).append("    ").toString();
        if (this.l != null) {
            String v4_0 = this.l.size();
            if (v4_0 > null) {
                p11.print(p9);
                p11.print("Active Fragments in ");
                p11.print(Integer.toHexString(System.identityHashCode(this)));
                p11.println(":");
                int v2_1 = 0;
                while (v2_1 < v4_0) {
                    android.support.v4.app.bl v0_84 = ((android.support.v4.app.Fragment) this.l.get(v2_1));
                    p11.print(p9);
                    p11.print("  #");
                    p11.print(v2_1);
                    p11.print(": ");
                    p11.println(v0_84);
                    if (v0_84 != null) {
                        p11.print(v3_0);
                        p11.print("mFragmentId=#");
                        p11.print(Integer.toHexString(v0_84.Q));
                        p11.print(" mContainerId=#");
                        p11.print(Integer.toHexString(v0_84.R));
                        p11.print(" mTag=");
                        p11.println(v0_84.S);
                        p11.print(v3_0);
                        p11.print("mState=");
                        p11.print(v0_84.u);
                        p11.print(" mIndex=");
                        p11.print(v0_84.z);
                        p11.print(" mWho=");
                        p11.print(v0_84.A);
                        p11.print(" mBackStackNesting=");
                        p11.println(v0_84.L);
                        p11.print(v3_0);
                        p11.print("mAdded=");
                        p11.print(v0_84.F);
                        p11.print(" mRemoving=");
                        p11.print(v0_84.G);
                        p11.print(" mResumed=");
                        p11.print(v0_84.H);
                        p11.print(" mFromLayout=");
                        p11.print(v0_84.I);
                        p11.print(" mInLayout=");
                        p11.println(v0_84.J);
                        p11.print(v3_0);
                        p11.print("mHidden=");
                        p11.print(v0_84.T);
                        p11.print(" mDetached=");
                        p11.print(v0_84.U);
                        p11.print(" mMenuVisible=");
                        p11.print(v0_84.Y);
                        p11.print(" mHasMenu=");
                        p11.println(v0_84.X);
                        p11.print(v3_0);
                        p11.print("mRetainInstance=");
                        p11.print(v0_84.V);
                        p11.print(" mRetaining=");
                        p11.print(v0_84.W);
                        p11.print(" mUserVisibleHint=");
                        p11.println(v0_84.af);
                        if (v0_84.M != null) {
                            p11.print(v3_0);
                            p11.print("mFragmentManager=");
                            p11.println(v0_84.M);
                        }
                        if (v0_84.N != null) {
                            p11.print(v3_0);
                            p11.print("mHost=");
                            p11.println(v0_84.N);
                        }
                        if (v0_84.P != null) {
                            p11.print(v3_0);
                            p11.print("mParentFragment=");
                            p11.println(v0_84.P);
                        }
                        if (v0_84.B != null) {
                            p11.print(v3_0);
                            p11.print("mArguments=");
                            p11.println(v0_84.B);
                        }
                        if (v0_84.x != null) {
                            p11.print(v3_0);
                            p11.print("mSavedFragmentState=");
                            p11.println(v0_84.x);
                        }
                        if (v0_84.y != null) {
                            p11.print(v3_0);
                            p11.print("mSavedViewState=");
                            p11.println(v0_84.y);
                        }
                        if (v0_84.C != null) {
                            p11.print(v3_0);
                            p11.print("mTarget=");
                            p11.print(v0_84.C);
                            p11.print(" mTargetRequestCode=");
                            p11.println(v0_84.E);
                        }
                        if (v0_84.aa != 0) {
                            p11.print(v3_0);
                            p11.print("mNextAnim=");
                            p11.println(v0_84.aa);
                        }
                        if (v0_84.ab != null) {
                            p11.print(v3_0);
                            p11.print("mContainer=");
                            p11.println(v0_84.ab);
                        }
                        if (v0_84.ac != null) {
                            p11.print(v3_0);
                            p11.print("mView=");
                            p11.println(v0_84.ac);
                        }
                        if (v0_84.ad != null) {
                            p11.print(v3_0);
                            p11.print("mInnerView=");
                            p11.println(v0_84.ac);
                        }
                        if (v0_84.v != null) {
                            p11.print(v3_0);
                            p11.print("mAnimatingAway=");
                            p11.println(v0_84.v);
                            p11.print(v3_0);
                            p11.print("mStateAfterAnimating=");
                            p11.println(v0_84.w);
                        }
                        if (v0_84.ag != null) {
                            p11.print(v3_0);
                            p11.println("Loader Manager:");
                            v0_84.ag.a(new StringBuilder().append(v3_0).append("  ").toString(), p10, p11, p12);
                        }
                        if (v0_84.O != null) {
                            p11.print(v3_0);
                            p11.println(new StringBuilder("Child ").append(v0_84.O).append(":").toString());
                            v0_84.O.a(new StringBuilder().append(v3_0).append("  ").toString(), p10, p11, p12);
                        }
                    }
                    v2_1++;
                }
            }
        }
        if (this.m != null) {
            String v4_1 = this.m.size();
            if (v4_1 > null) {
                p11.print(p9);
                p11.println("Added Fragments:");
                int v2_2 = 0;
                while (v2_2 < v4_1) {
                    android.support.v4.app.bl v0_79 = ((android.support.v4.app.Fragment) this.m.get(v2_2));
                    p11.print(p9);
                    p11.print("  #");
                    p11.print(v2_2);
                    p11.print(": ");
                    p11.println(v0_79.toString());
                    v2_2++;
                }
            }
        }
        if (this.p != null) {
            String v4_2 = this.p.size();
            if (v4_2 > null) {
                p11.print(p9);
                p11.println("Fragments Created Menus:");
                int v2_3 = 0;
                while (v2_3 < v4_2) {
                    android.support.v4.app.bl v0_74 = ((android.support.v4.app.Fragment) this.p.get(v2_3));
                    p11.print(p9);
                    p11.print("  #");
                    p11.print(v2_3);
                    p11.print(": ");
                    p11.println(v0_74.toString());
                    v2_3++;
                }
            }
        }
        if (this.o != null) {
            String v4_3 = this.o.size();
            if (v4_3 > null) {
                p11.print(p9);
                p11.println("Back Stack:");
                int v2_4 = 0;
                while (v2_4 < v4_3) {
                    android.support.v4.app.bl v0_70 = ((android.support.v4.app.ak) this.o.get(v2_4));
                    p11.print(p9);
                    p11.print("  #");
                    p11.print(v2_4);
                    p11.print(": ");
                    p11.println(v0_70.toString());
                    v0_70.a(v3_0, p11);
                    v2_4++;
                }
            }
        }
        if (this.q != null) {
            String v3_1 = this.q.size();
            if (v3_1 > null) {
                p11.print(p9);
                p11.println("Back Stack Indices:");
                int v2_5 = 0;
                while (v2_5 < v3_1) {
                    android.support.v4.app.bl v0_65 = ((android.support.v4.app.ak) this.q.get(v2_5));
                    p11.print(p9);
                    p11.print("  #");
                    p11.print(v2_5);
                    p11.print(": ");
                    p11.println(v0_65);
                    v2_5++;
                }
            }
        }
        if ((this.r != null) && (this.r.size() > 0)) {
            p11.print(p9);
            p11.print("mAvailBackStackIndices: ");
            p11.println(java.util.Arrays.toString(this.r.toArray()));
        }
        if (this.i != null) {
            int v2_6 = this.i.size();
            if (v2_6 > 0) {
                p11.print(p9);
                p11.println("Pending Actions:");
                while (v1 < v2_6) {
                    android.support.v4.app.bl v0_61 = ((Runnable) this.i.get(v1));
                    p11.print(p9);
                    p11.print("  #");
                    p11.print(v1);
                    p11.print(": ");
                    p11.println(v0_61);
                    v1++;
                }
            }
        }
        p11.print(p9);
        p11.println("FragmentManager misc state:");
        p11.print(p9);
        p11.print("  mHost=");
        p11.println(this.u);
        p11.print(p9);
        p11.print("  mContainer=");
        p11.println(this.w);
        if (this.x != null) {
            p11.print(p9);
            p11.print("  mParent=");
            p11.println(this.x);
        }
        p11.print(p9);
        p11.print("  mCurState=");
        p11.print(this.t);
        p11.print(" mStateSaved=");
        p11.print(this.z);
        p11.print(" mDestroyed=");
        p11.println(this.A);
        if (this.y) {
            p11.print(p9);
            p11.print("  mNeedMenuInvalidate=");
            p11.println(this.y);
        }
        if (this.B != null) {
            p11.print(p9);
            p11.print("  mNoTransactionsBecause=");
            p11.println(this.B);
        }
        if ((this.n != null) && (this.n.size() > 0)) {
            p11.print(p9);
            p11.print("  mAvailIndices: ");
            p11.println(java.util.Arrays.toString(this.n.toArray()));
        }
        return;
    }

    public final boolean a(int p4, int p5)
    {
        this.u();
        this.k();
        if (p4 >= 0) {
            return this.a(0, p4, p5);
        } else {
            throw new IllegalArgumentException(new StringBuilder("Bad id: ").append(p4).toString());
        }
    }

    public final boolean a(android.view.Menu p8)
    {
        int v3;
        if (this.m == null) {
            v3 = 0;
        } else {
            int v1 = 0;
            v3 = 0;
            while (v1 < this.m.size()) {
                boolean v0_5 = ((android.support.v4.app.Fragment) this.m.get(v1));
                if (v0_5) {
                    int v4_1;
                    if (v0_5.T) {
                        v4_1 = 0;
                    } else {
                        if ((!v0_5.X) || (!v0_5.Y)) {
                            v4_1 = 0;
                        } else {
                            v4_1 = 1;
                        }
                        if (v0_5.O != null) {
                            v4_1 |= v0_5.O.a(p8);
                        }
                    }
                    if (v4_1 != 0) {
                        v3 = 1;
                    }
                }
                v1++;
            }
        }
        return v3;
    }

    public final boolean a(android.view.Menu p9, android.view.MenuInflater p10)
    {
        android.support.v4.app.Fragment v3_0;
        int v5 = 0;
        java.util.ArrayList v1_0 = 0;
        if (this.m == null) {
            v3_0 = 0;
        } else {
            int v4 = 0;
            v3_0 = 0;
            while (v4 < this.m.size()) {
                int v0_13;
                int v0_12 = ((android.support.v4.app.Fragment) this.m.get(v4));
                if (v0_12 == 0) {
                    v0_13 = v3_0;
                } else {
                    int v6_1;
                    if (v0_12.T) {
                        v6_1 = 0;
                    } else {
                        if ((!v0_12.X) || (!v0_12.Y)) {
                            v6_1 = 0;
                        } else {
                            v0_12.a(p9, p10);
                            v6_1 = 1;
                        }
                        if (v0_12.O != null) {
                            v6_1 |= v0_12.O.a(p9, p10);
                        }
                    }
                    if (v6_1 == 0) {
                    } else {
                        if (v1_0 == null) {
                            v1_0 = new java.util.ArrayList();
                        }
                        v1_0.add(v0_12);
                        v0_13 = 1;
                    }
                }
                v4++;
                v3_0 = v0_13;
            }
        }
        if (this.p != null) {
            while (v5 < this.p.size()) {
                if ((v1_0 == null) || (!v1_0.contains(((android.support.v4.app.Fragment) this.p.get(v5))))) {
                    android.support.v4.app.Fragment.v();
                }
                v5++;
            }
        }
        this.p = v1_0;
        return v3_0;
    }

    public final boolean a(android.view.MenuItem p6)
    {
        int v2 = 0;
        if (this.m != null) {
            int v1 = 0;
            while (v1 < this.m.size()) {
                int v0_5 = ((android.support.v4.app.Fragment) this.m.get(v1));
                if (v0_5 != 0) {
                    int v0_8;
                    if (v0_5.T) {
                        v0_8 = 0;
                    } else {
                        if ((!v0_5.X) || ((!v0_5.Y) || (!v0_5.a(p6)))) {
                            if ((v0_5.O == null) || (!v0_5.O.a(p6))) {
                            } else {
                                v0_8 = 1;
                            }
                        } else {
                            v0_8 = 1;
                        }
                    }
                    if (v0_8 != 0) {
                        v2 = 1;
                        break;
                    }
                }
                v1++;
            }
        }
        return v2;
    }

    public final boolean a(String p2, int p3)
    {
        this.u();
        this.k();
        return this.a(p2, -1, p3);
    }

    final boolean a(String p12, int p13, int p14)
    {
        int v3_0 = 0;
        if (this.o != null) {
            if ((p12 != null) || ((p13 >= 0) || ((p14 & 1) != 0))) {
                int v0_2 = -1;
                if ((p12 != null) || (p13 >= 0)) {
                    android.support.v4.app.ap v1_0 = (this.o.size() - 1);
                    while (v1_0 >= null) {
                        int v0_7 = ((android.support.v4.app.ak) this.o.get(v1_0));
                        if (((p12 != null) && (p12.equals(v0_7.w))) || ((p13 >= 0) && (p13 == v0_7.y))) {
                            break;
                        }
                        v1_0--;
                    }
                    if (v1_0 < null) {
                        return v3_0;
                    } else {
                        if ((p14 & 1) != 0) {
                            v1_0--;
                            while (v1_0 >= null) {
                                int v0_12 = ((android.support.v4.app.ak) this.o.get(v1_0));
                                if (((p12 == null) || (!p12.equals(v0_12.w))) && ((p13 < 0) || (p13 != v0_12.y))) {
                                    break;
                                }
                                v1_0--;
                            }
                        }
                        v0_2 = v1_0;
                    }
                }
                if (v0_2 == (this.o.size() - 1)) {
                    return v3_0;
                } else {
                    java.util.ArrayList v6_1 = new java.util.ArrayList();
                    android.support.v4.app.ap v1_6 = (this.o.size() - 1);
                    while (v1_6 > v0_2) {
                        v6_1.add(this.o.remove(v1_6));
                        v1_6--;
                    }
                    int v7 = (v6_1.size() - 1);
                    android.util.SparseArray v8_1 = new android.util.SparseArray();
                    android.util.SparseArray v9_1 = new android.util.SparseArray();
                    android.support.v4.app.ap v1_7 = 0;
                    while (v1_7 <= v7) {
                        ((android.support.v4.app.ak) v6_1.get(v1_7)).a(v8_1, v9_1);
                        v1_7++;
                    }
                    android.support.v4.app.ap v5_4 = 0;
                    int v4_1 = 0;
                    while (v4_1 <= v7) {
                        if (android.support.v4.app.bl.b) {
                            android.util.Log.v("FragmentManager", new StringBuilder("Popping back stack state: ").append(v6_1.get(v4_1)).toString());
                        }
                        android.support.v4.app.ap v1_12;
                        if (v4_1 != v7) {
                            v1_12 = 0;
                        } else {
                            v1_12 = 1;
                        }
                        android.support.v4.app.ap v1_13 = ((android.support.v4.app.ak) v6_1.get(v4_1)).a(v1_12, v5_4, v8_1, v9_1);
                        v4_1++;
                        v5_4 = v1_13;
                    }
                }
            } else {
                int v0_25 = (this.o.size() - 1);
                if (v0_25 < 0) {
                    return v3_0;
                } else {
                    int v0_27 = ((android.support.v4.app.ak) this.o.remove(v0_25));
                    android.support.v4.app.ap v1_16 = new android.util.SparseArray();
                    int v3_2 = new android.util.SparseArray();
                    v0_27.a(v1_16, v3_2);
                    v0_27.a(1, 0, v1_16, v3_2);
                }
            }
            this.l();
            v3_0 = 1;
        }
        return v3_0;
    }

    public final void b(int p4)
    {
        if (p4 >= 0) {
            this.a(new android.support.v4.app.bp(this, p4), 0);
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder("Bad id: ").append(p4).toString());
        }
    }

    public final void b(android.support.v4.app.Fragment p7)
    {
        if (p7.ae) {
            if (!this.k) {
                p7.ae = 0;
                this.a(p7, this.t, 0, 0, 0);
            } else {
                this.C = 1;
            }
        }
        return;
    }

    public final void b(android.support.v4.app.Fragment p5, int p6, int p7)
    {
        if (android.support.v4.app.bl.b) {
            android.util.Log.v("FragmentManager", new StringBuilder("hide: ").append(p5).toString());
        }
        if (!p5.T) {
            p5.T = 1;
            if (p5.ac != null) {
                boolean v0_5 = this.a(p5, p6, 0, p7);
                if (v0_5) {
                    android.support.v4.app.bl.b(p5.ac, v0_5);
                    p5.ac.startAnimation(v0_5);
                }
                p5.ac.setVisibility(8);
            }
            if ((p5.F) && ((p5.X) && (p5.Y))) {
                this.y = 1;
            }
            android.support.v4.app.Fragment.m();
        }
        return;
    }

    public final void b(android.support.v4.app.bk p2)
    {
        if (this.s != null) {
            this.s.remove(p2);
        }
        return;
    }

    public final void b(android.view.Menu p4)
    {
        if (this.m != null) {
            int v1 = 0;
            while (v1 < this.m.size()) {
                android.support.v4.app.bl v0_6 = ((android.support.v4.app.Fragment) this.m.get(v1));
                if ((v0_6 != null) && ((!v0_6.T) && (v0_6.O != null))) {
                    v0_6.O.b(p4);
                }
                v1++;
            }
        }
        return;
    }

    public final boolean b()
    {
        return this.k();
    }

    public final boolean b(android.view.MenuItem p6)
    {
        int v2 = 0;
        if (this.m != null) {
            int v1 = 0;
            while (v1 < this.m.size()) {
                int v0_5 = ((android.support.v4.app.Fragment) this.m.get(v1));
                if (v0_5 != 0) {
                    if ((v0_5.T) || ((v0_5.O == null) || (!v0_5.O.b(p6)))) {
                        int v0_8 = 0;
                    } else {
                        v0_8 = 1;
                    }
                    if (v0_8 != 0) {
                        v2 = 1;
                        break;
                    }
                }
                v1++;
            }
        }
        return v2;
    }

    public final void c()
    {
        this.a(new android.support.v4.app.bn(this), 0);
        return;
    }

    final void c(int p2)
    {
        this.a(p2, 0, 0, 0);
        return;
    }

    public final void c(android.support.v4.app.Fragment p6, int p7, int p8)
    {
        if (android.support.v4.app.bl.b) {
            android.util.Log.v("FragmentManager", new StringBuilder("show: ").append(p6).toString());
        }
        if (p6.T) {
            p6.T = 0;
            if (p6.ac != null) {
                boolean v0_4 = this.a(p6, p7, 1, p8);
                if (v0_4) {
                    android.support.v4.app.bl.b(p6.ac, v0_4);
                    p6.ac.startAnimation(v0_4);
                }
                p6.ac.setVisibility(0);
            }
            if ((p6.F) && ((p6.X) && (p6.Y))) {
                this.y = 1;
            }
            android.support.v4.app.Fragment.m();
        }
        return;
    }

    public final void d(android.support.v4.app.Fragment p7, int p8, int p9)
    {
        if (android.support.v4.app.bl.b) {
            android.util.Log.v("FragmentManager", new StringBuilder("detach: ").append(p7).toString());
        }
        if (!p7.U) {
            p7.U = 1;
            if (p7.F) {
                if (this.m != null) {
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("remove from detach: ").append(p7).toString());
                    }
                    this.m.remove(p7);
                }
                if ((p7.X) && (p7.Y)) {
                    this.y = 1;
                }
                p7.F = 0;
                this.a(p7, 1, p8, p9, 0);
            }
        }
        return;
    }

    public final boolean d()
    {
        this.u();
        this.k();
        return this.a(0, -1, 0);
    }

    public final void e()
    {
        this.a(new android.support.v4.app.bo(this), 0);
        return;
    }

    public final void e(android.support.v4.app.Fragment p7, int p8, int p9)
    {
        if (android.support.v4.app.bl.b) {
            android.util.Log.v("FragmentManager", new StringBuilder("attach: ").append(p7).toString());
        }
        if (p7.U) {
            p7.U = 0;
            if (!p7.F) {
                if (this.m == null) {
                    this.m = new java.util.ArrayList();
                }
                if (!this.m.contains(p7)) {
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("add from attach: ").append(p7).toString());
                    }
                    this.m.add(p7);
                    p7.F = 1;
                    if ((p7.X) && (p7.Y)) {
                        this.y = 1;
                    }
                    this.a(p7, this.t, p8, p9, 0);
                } else {
                    throw new IllegalStateException(new StringBuilder("Fragment already added: ").append(p7).toString());
                }
            }
        }
        return;
    }

    public final int f()
    {
        int v0_1;
        if (this.o == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.o.size();
        }
        return v0_1;
    }

    public final android.support.v4.app.bj g()
    {
        return ((android.support.v4.app.bj) this.o.get(0));
    }

    public final java.util.List h()
    {
        return this.l;
    }

    public final boolean i()
    {
        return this.A;
    }

    final void j()
    {
        if (this.l != null) {
            int v1 = 0;
            while (v1 < this.l.size()) {
                int v0_6 = ((android.support.v4.app.Fragment) this.l.get(v1));
                if (v0_6 != 0) {
                    this.b(v0_6);
                }
                v1++;
            }
        }
        return;
    }

    public final boolean k()
    {
        if (!this.k) {
            if (android.os.Looper.myLooper() == this.u.d.getLooper()) {
                int v1_2 = 0;
                while ((this.i != null) && (this.i.size() != 0)) {
                    int v3_6 = this.i.size();
                    if ((this.j == null) || (this.j.length < v3_6)) {
                        int v1_7 = new Runnable[v3_6];
                        this.j = v1_7;
                    }
                    this.i.toArray(this.j);
                    this.i.clear();
                    this.u.d.removeCallbacks(this.F);
                    this.k = 1;
                    int v1_12 = 0;
                    while (v1_12 < v3_6) {
                        this.j[v1_12].run();
                        this.j[v1_12] = 0;
                        v1_12++;
                    }
                    this.k = 0;
                    v1_2 = 1;
                }
                if (this.C) {
                    int v3_7 = 0;
                    int v4_5 = 0;
                    while (v3_7 < this.l.size()) {
                        boolean v0_7 = ((android.support.v4.app.Fragment) this.l.get(v3_7));
                        if ((v0_7) && (v0_7.ag != null)) {
                            v4_5 |= v0_7.ag.a();
                        }
                        v3_7++;
                    }
                    if (v4_5 == 0) {
                        this.C = 0;
                        this.j();
                    }
                }
                return v1_2;
            } else {
                throw new IllegalStateException("Must be called from main thread of process");
            }
        } else {
            throw new IllegalStateException("Recursive entry to executePendingTransactions");
        }
    }

    final void l()
    {
        if (this.s != null) {
            int v1 = 0;
            while (v1 < this.s.size()) {
                ((android.support.v4.app.bk) this.s.get(v1)).a();
                v1++;
            }
        }
        return;
    }

    final android.os.Parcelable m()
    {
        android.support.v4.app.BackStackState[] v3 = 0;
        this.k();
        if (android.support.v4.app.bl.d) {
            this.z = 1;
        }
        if ((this.l != null) && (this.l.size() > 0)) {
            Object v6_0 = this.l.size();
            android.support.v4.app.FragmentState[] v7 = new android.support.v4.app.FragmentState[v6_0];
            int v5_0 = 0;
            int v2_0 = 0;
            while (v5_0 < v6_0) {
                int v0_32;
                int v0_31 = ((android.support.v4.app.Fragment) this.l.get(v5_0));
                if (v0_31 == 0) {
                    v0_32 = v2_0;
                } else {
                    if (v0_31.z < 0) {
                        this.a(new IllegalStateException(new StringBuilder("Failure saving state: active ").append(v0_31).append(" has cleared index: ").append(v0_31.z).toString()));
                    }
                    int v2_7 = new android.support.v4.app.FragmentState(v0_31);
                    v7[v5_0] = v2_7;
                    if ((v0_31.u <= 0) || (v2_7.j != null)) {
                        v2_7.j = v0_31.x;
                    } else {
                        v2_7.j = this.g(v0_31);
                        if (v0_31.C != null) {
                            if (v0_31.C.z < 0) {
                                this.a(new IllegalStateException(new StringBuilder("Failure saving state: ").append(v0_31).append(" has target not in fragment manager: ").append(v0_31.C).toString()));
                            }
                            if (v2_7.j == null) {
                                v2_7.j = new android.os.Bundle();
                            }
                            this.a(v2_7.j, "android:target_state", v0_31.C);
                            if (v0_31.E != 0) {
                                v2_7.j.putInt("android:target_req_state", v0_31.E);
                            }
                        }
                    }
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("Saved state of ").append(v0_31).append(": ").append(v2_7.j).toString());
                    }
                    v0_32 = 1;
                }
                v5_0++;
                v2_0 = v0_32;
            }
            if (v2_0 != 0) {
                int[] v1_1;
                if (this.m == null) {
                    v1_1 = 0;
                } else {
                    int v5_1 = this.m.size();
                    if (v5_1 <= 0) {
                    } else {
                        v1_1 = new int[v5_1];
                        int v2_1 = 0;
                        while (v2_1 < v5_1) {
                            v1_1[v2_1] = ((android.support.v4.app.Fragment) this.m.get(v2_1)).z;
                            if (v1_1[v2_1] < 0) {
                                this.a(new IllegalStateException(new StringBuilder("Failure saving state: active ").append(this.m.get(v2_1)).append(" has cleared index: ").append(v1_1[v2_1]).toString()));
                            }
                            if (android.support.v4.app.bl.b) {
                                android.util.Log.v("FragmentManager", new StringBuilder("saveAllState: adding fragment #").append(v2_1).append(": ").append(this.m.get(v2_1)).toString());
                            }
                            v2_1++;
                        }
                    }
                }
                if (this.o != null) {
                    int v5_2 = this.o.size();
                    if (v5_2 > 0) {
                        v3 = new android.support.v4.app.BackStackState[v5_2];
                        int v2_2 = 0;
                        while (v2_2 < v5_2) {
                            v3[v2_2] = new android.support.v4.app.BackStackState(((android.support.v4.app.ak) this.o.get(v2_2)));
                            if (android.support.v4.app.bl.b) {
                                android.util.Log.v("FragmentManager", new StringBuilder("saveAllState: adding back stack #").append(v2_2).append(": ").append(this.o.get(v2_2)).toString());
                            }
                            v2_2++;
                        }
                    }
                }
                int v0_10 = new android.support.v4.app.FragmentManagerState();
                v0_10.a = v7;
                v0_10.b = v1_1;
                v0_10.c = v3;
                v3 = v0_10;
            } else {
                if (android.support.v4.app.bl.b) {
                    android.util.Log.v("FragmentManager", "saveAllState: no fragments!");
                }
            }
        }
        return v3;
    }

    public final void n()
    {
        this.z = 0;
        this.c(1);
        return;
    }

    public final void o()
    {
        this.z = 0;
        this.c(2);
        return;
    }

    public final void p()
    {
        this.z = 0;
        this.c(4);
        return;
    }

    public final void q()
    {
        this.z = 0;
        this.c(5);
        return;
    }

    public final void r()
    {
        this.z = 1;
        this.c(3);
        return;
    }

    public final void s()
    {
        this.A = 1;
        this.k();
        this.c(0);
        this.u = 0;
        this.w = 0;
        this.x = 0;
        return;
    }

    public final void t()
    {
        if (this.m != null) {
            int v1 = 0;
            while (v1 < this.m.size()) {
                android.support.v4.app.bl v0_6 = ((android.support.v4.app.Fragment) this.m.get(v1));
                if (v0_6 != null) {
                    v0_6.onLowMemory();
                    if (v0_6.O != null) {
                        v0_6.O.t();
                    }
                }
                v1++;
            }
        }
        return;
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder(128);
        v0_1.append("FragmentManager{");
        v0_1.append(Integer.toHexString(System.identityHashCode(this)));
        v0_1.append(" in ");
        if (this.x == null) {
            android.support.v4.n.g.a(this.u, v0_1);
        } else {
            android.support.v4.n.g.a(this.x, v0_1);
        }
        v0_1.append("}}");
        return v0_1.toString();
    }
}
