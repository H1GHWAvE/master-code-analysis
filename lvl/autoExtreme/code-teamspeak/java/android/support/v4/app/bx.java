package android.support.v4.app;
final class bx implements android.os.Parcelable$Creator {

    bx()
    {
        return;
    }

    private static android.support.v4.app.FragmentState a(android.os.Parcel p1)
    {
        return new android.support.v4.app.FragmentState(p1);
    }

    private static android.support.v4.app.FragmentState[] a(int p1)
    {
        android.support.v4.app.FragmentState[] v0 = new android.support.v4.app.FragmentState[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return new android.support.v4.app.FragmentState(p2);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.app.FragmentState[] v0 = new android.support.v4.app.FragmentState[p2];
        return v0;
    }
}
