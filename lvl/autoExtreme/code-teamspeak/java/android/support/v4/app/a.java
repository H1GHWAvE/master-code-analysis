package android.support.v4.app;
public final class a implements android.support.v4.widget.ac {
    private static final android.support.v4.app.c a = None;
    private static final float b = 1051372203;
    private static final int c = 16908332;
    private final android.app.Activity d;
    private final android.support.v4.app.g e;
    private final android.support.v4.widget.DrawerLayout f;
    private boolean g;
    private boolean h;
    private android.graphics.drawable.Drawable i;
    private android.graphics.drawable.Drawable j;
    private android.support.v4.app.i k;
    private final int l;
    private final int m;
    private final int n;
    private Object o;

    static a()
    {
        android.support.v4.app.d v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 18) {
            if (v0_0 < 11) {
                android.support.v4.app.a.a = new android.support.v4.app.d(0);
            } else {
                android.support.v4.app.a.a = new android.support.v4.app.e(0);
            }
        } else {
            android.support.v4.app.a.a = new android.support.v4.app.f(0);
        }
        return;
    }

    private a(android.app.Activity p8, android.support.v4.widget.DrawerLayout p9, int p10, int p11, int p12)
    {
        android.app.Activity v1_3;
        android.support.v4.app.a v3 = 1;
        if ((p8.getApplicationInfo().targetSdkVersion < 21) || (android.os.Build$VERSION.SDK_INT < 21)) {
            v1_3 = 0;
        } else {
            v1_3 = 1;
        }
        if (v1_3 != null) {
            v3 = 0;
        }
        this(p8, p9, v3, p10, p11, p12);
        return;
    }

    private a(android.app.Activity p4, android.support.v4.widget.DrawerLayout p5, boolean p6, int p7, int p8, int p9)
    {
        this.g = 1;
        this.d = p4;
        if (!(p4 instanceof android.support.v4.app.h)) {
            this.e = 0;
        } else {
            this.e = ((android.support.v4.app.h) p4).a();
        }
        int v0_10;
        this.f = p5;
        this.l = p7;
        this.m = p8;
        this.n = p9;
        this.i = this.g();
        this.j = android.support.v4.c.h.a(p4, p7);
        this.k = new android.support.v4.app.i(this, this.j, 0);
        android.support.v4.app.i v1_1 = this.k;
        if (!p6) {
            v0_10 = 0;
        } else {
            v0_10 = 1051372203;
        }
        v1_1.b = v0_10;
        v1_1.invalidateSelf();
        return;
    }

    static synthetic android.app.Activity a(android.support.v4.app.a p1)
    {
        return p1.d;
    }

    private void a(int p3)
    {
        android.graphics.drawable.Drawable v0_0 = 0;
        if (p3 != 0) {
            v0_0 = android.support.v4.c.h.a(this.d, p3);
        }
        if (v0_0 != null) {
            this.i = v0_0;
            this.h = 1;
        } else {
            this.i = this.g();
            this.h = 0;
        }
        if (!this.g) {
            this.a(this.i, 0);
        }
        return;
    }

    private void a(android.graphics.drawable.Drawable p3)
    {
        if (p3 != null) {
            this.i = p3;
            this.h = 1;
        } else {
            this.i = this.g();
            this.h = 0;
        }
        if (!this.g) {
            this.a(this.i, 0);
        }
        return;
    }

    private void a(android.graphics.drawable.Drawable p4, int p5)
    {
        if (this.e == null) {
            this.o = android.support.v4.app.a.a.a(this.o, this.d, p4, p5);
        }
        return;
    }

    private void a(boolean p3)
    {
        if (p3 != this.g) {
            if (!p3) {
                this.a(this.i, 0);
            } else {
                int v0_4;
                if (!this.f.c()) {
                    v0_4 = this.m;
                } else {
                    v0_4 = this.n;
                }
                this.a(this.k, v0_4);
            }
            this.g = p3;
        }
        return;
    }

    private static boolean a(android.content.Context p2)
    {
        if ((p2.getApplicationInfo().targetSdkVersion < 21) || (android.os.Build$VERSION.SDK_INT < 21)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private boolean a(android.view.MenuItem p3)
    {
        if ((p3 == null) || ((p3.getItemId() != 16908332) || (!this.g))) {
            int v0_2 = 0;
        } else {
            if (!this.f.d()) {
                this.f.a();
            } else {
                this.f.b();
            }
            v0_2 = 1;
        }
        return v0_2;
    }

    private void b(int p4)
    {
        if (this.e == null) {
            this.o = android.support.v4.app.a.a.a(this.o, this.d, p4);
        }
        return;
    }

    private void d()
    {
        if (!this.f.c()) {
            this.k.a(0);
        } else {
            this.k.a(1065353216);
        }
        if (this.g) {
            int v0_7;
            if (!this.f.c()) {
                v0_7 = this.m;
            } else {
                v0_7 = this.n;
            }
            this.a(this.k, v0_7);
        }
        return;
    }

    private boolean e()
    {
        return this.g;
    }

    private void f()
    {
        if (!this.h) {
            this.i = this.g();
        }
        this.j = android.support.v4.c.h.a(this.d, this.l);
        if (!this.f.c()) {
            this.k.a(0);
        } else {
            this.k.a(1065353216);
        }
        if (this.g) {
            int v0_11;
            if (!this.f.c()) {
                v0_11 = this.m;
            } else {
                v0_11 = this.n;
            }
            this.a(this.k, v0_11);
        }
        return;
    }

    private android.graphics.drawable.Drawable g()
    {
        android.graphics.drawable.Drawable v0_2;
        if (this.e == null) {
            v0_2 = android.support.v4.app.a.a.a(this.d);
        } else {
            v0_2 = this.e.a();
        }
        return v0_2;
    }

    public final void a()
    {
        this.k.a(1065353216);
        if (this.g) {
            this.b(this.n);
        }
        return;
    }

    public final void a(float p5)
    {
        float v0_2;
        float v0_1 = this.k.a;
        if (p5 <= 1056964608) {
            v0_2 = Math.min(v0_1, (p5 * 1073741824));
        } else {
            v0_2 = Math.max(v0_1, (Math.max(0, (p5 - 1056964608)) * 1073741824));
        }
        this.k.a(v0_2);
        return;
    }

    public final void b()
    {
        this.k.a(0);
        if (this.g) {
            this.b(this.m);
        }
        return;
    }

    public final void c()
    {
        return;
    }
}
