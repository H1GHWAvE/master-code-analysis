package android.support.v4.app;
public final class ew implements android.support.v4.app.db, android.support.v4.app.dc {
    private android.app.Notification$Builder a;
    private android.os.Bundle b;
    private java.util.List c;

    public ew(android.content.Context p9, android.app.Notification p10, CharSequence p11, CharSequence p12, CharSequence p13, android.widget.RemoteViews p14, int p15, android.app.PendingIntent p16, android.app.PendingIntent p17, android.graphics.Bitmap p18, int p19, int p20, boolean p21, boolean p22, boolean p23, int p24, CharSequence p25, boolean p26, java.util.ArrayList p27, android.os.Bundle p28, String p29, boolean p30, String p31)
    {
        android.os.Bundle v4_13;
        this.c = new java.util.ArrayList();
        String v5_6 = new android.app.Notification$Builder(p9).setWhen(p10.when).setShowWhen(p22).setSmallIcon(p10.icon, p10.iconLevel).setContent(p10.contentView).setTicker(p10.tickerText, p14).setSound(p10.sound, p10.audioStreamType).setVibrate(p10.vibrate).setLights(p10.ledARGB, p10.ledOnMS, p10.ledOffMS);
        if ((p10.flags & 2) == 0) {
            v4_13 = 0;
        } else {
            v4_13 = 1;
        }
        android.os.Bundle v4_16;
        String v5_7 = v5_6.setOngoing(v4_13);
        if ((p10.flags & 8) == 0) {
            v4_16 = 0;
        } else {
            v4_16 = 1;
        }
        android.os.Bundle v4_19;
        String v5_8 = v5_7.setOnlyAlertOnce(v4_16);
        if ((p10.flags & 16) == 0) {
            v4_19 = 0;
        } else {
            v4_19 = 1;
        }
        android.os.Bundle v4_29;
        String v5_11 = v5_8.setAutoCancel(v4_19).setDefaults(p10.defaults).setContentTitle(p11).setContentText(p12).setSubText(p25).setContentInfo(p13).setContentIntent(p16).setDeleteIntent(p10.deleteIntent);
        if ((p10.flags & 128) == 0) {
            v4_29 = 0;
        } else {
            v4_29 = 1;
        }
        this.a = v5_11.setFullScreenIntent(p17, v4_29).setLargeIcon(p18).setNumber(p15).setUsesChronometer(p23).setPriority(p24).setProgress(p19, p20, p21);
        this.b = new android.os.Bundle();
        if (p28 != null) {
            this.b.putAll(p28);
        }
        if ((p27 != null) && (!p27.isEmpty())) {
            android.os.Bundle v4_41 = new String[p27.size()];
            this.b.putStringArray("android.people", ((String[]) p27.toArray(v4_41)));
        }
        if (p26) {
            this.b.putBoolean("android.support.localOnly", 1);
        }
        if (p29 != null) {
            this.b.putString("android.support.groupKey", p29);
            if (!p30) {
                this.b.putBoolean("android.support.useSideChannel", 1);
            } else {
                this.b.putBoolean("android.support.isGroupSummary", 1);
            }
        }
        if (p31 != null) {
            this.b.putString("android.support.sortKey", p31);
        }
        return;
    }

    public final android.app.Notification$Builder a()
    {
        return this.a;
    }

    public final void a(android.support.v4.app.ek p3)
    {
        this.c.add(android.support.v4.app.et.a(this.a, p3));
        return;
    }

    public final android.app.Notification b()
    {
        android.app.Notification v0_1 = android.support.v4.app.et.a(this.c);
        if (v0_1 != null) {
            this.b.putSparseParcelableArray("android.support.actionExtras", v0_1);
        }
        this.a.setExtras(this.b);
        return this.a.build();
    }
}
