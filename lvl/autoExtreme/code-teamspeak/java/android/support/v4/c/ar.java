package android.support.v4.c;
public final class ar {
    public static final int a = 0;
    public static final int b = 255;
    public static final int c = 254;

    private ar()
    {
        return;
    }

    public static int a(android.content.Context p3, String p4)
    {
        return android.support.v4.c.ar.a(p3, p4, android.os.Process.myPid(), android.os.Process.myUid(), p3.getPackageName());
    }

    private static int a(android.content.Context p5, String p6, int p7, int p8, String p9)
    {
        int v0_0 = -1;
        if (p5.checkPermission(p6, p7, p8) != -1) {
            String v2_1 = android.support.v4.app.af.a(p6);
            if (v2_1 != null) {
                if (p9 == null) {
                    String[] v3_1 = p5.getPackageManager().getPackagesForUid(p8);
                    if ((v3_1 == null) || (v3_1.length <= 0)) {
                        return v0_0;
                    } else {
                        p9 = v3_1[0];
                    }
                }
                if (android.support.v4.app.af.a(p5, v2_1, p9) == 0) {
                    v0_0 = 0;
                } else {
                    v0_0 = -2;
                }
            } else {
                v0_0 = 0;
            }
        }
        return v0_0;
    }

    private static int a(android.content.Context p2, String p3, String p4)
    {
        int v0_2;
        if (android.os.Binder.getCallingPid() != android.os.Process.myPid()) {
            v0_2 = android.support.v4.c.ar.a(p2, p3, android.os.Binder.getCallingPid(), android.os.Binder.getCallingUid(), p4);
        } else {
            v0_2 = -1;
        }
        return v0_2;
    }

    private static int b(android.content.Context p3, String p4)
    {
        int v0_1;
        if (android.os.Binder.getCallingPid() != android.os.Process.myPid()) {
            v0_1 = 0;
        } else {
            v0_1 = p3.getPackageName();
        }
        return android.support.v4.c.ar.a(p3, p4, android.os.Binder.getCallingPid(), android.os.Binder.getCallingUid(), v0_1);
    }
}
