package android.support.v4.c;
final class f extends android.support.v4.c.e {

    f()
    {
        return;
    }

    public final android.database.Cursor a(android.content.ContentResolver p8, android.net.Uri p9, String[] p10, String p11, String[] p12, String p13, android.support.v4.i.c p14)
    {
        android.os.CancellationSignal v6_0;
        if (p14 == null) {
            v6_0 = 0;
        } else {
            try {
                v6_0 = p14.b();
            } catch (android.support.v4.i.h v0_0) {
                if (!(v0_0 instanceof android.os.OperationCanceledException)) {
                    throw v0_0;
                } else {
                    throw new android.support.v4.i.h();
                }
            }
        }
        return p8.query(p9, p10, p11, p12, p13, ((android.os.CancellationSignal) v6_0));
    }
}
