package android.support.v4.m;
public final class a {
    private static android.support.v4.m.l a = None;
    private static final char b = 8234;
    private static final char c = 8235;
    private static final char d = 8236;
    private static final char e = 8206;
    private static final char f = 8207;
    private static final String g = "None";
    private static final String h = "None";
    private static final String i = "";
    private static final int j = 2;
    private static final int k = 2;
    private static final android.support.v4.m.a l = None;
    private static final android.support.v4.m.a m = None;
    private static final int q = 255;
    private static final int r = 0;
    private static final int s = 1;
    private final boolean n;
    private final int o;
    private final android.support.v4.m.l p;

    static a()
    {
        android.support.v4.m.a.a = android.support.v4.m.m.c;
        android.support.v4.m.a.g = Character.toString(8206);
        android.support.v4.m.a.h = Character.toString(8207);
        android.support.v4.m.a.l = new android.support.v4.m.a(0, 2, android.support.v4.m.a.a);
        android.support.v4.m.a.m = new android.support.v4.m.a(1, 2, android.support.v4.m.a.a);
        return;
    }

    private a(boolean p1, int p2, android.support.v4.m.l p3)
    {
        this.n = p1;
        this.o = p2;
        this.p = p3;
        return;
    }

    synthetic a(boolean p1, int p2, android.support.v4.m.l p3, byte p4)
    {
        this(p1, p2, p3);
        return;
    }

    private static android.support.v4.m.a a(boolean p1)
    {
        return new android.support.v4.m.c(p1).a();
    }

    static synthetic android.support.v4.m.l a()
    {
        return android.support.v4.m.a.a;
    }

    private String a(String p4, android.support.v4.m.l p5)
    {
        String v0_3;
        String v0_1 = p5.a(p4, 0, p4.length());
        if ((this.n) || ((v0_1 == null) && (android.support.v4.m.a.c(p4) != 1))) {
            if ((!this.n) || ((v0_1 != null) && (android.support.v4.m.a.c(p4) != -1))) {
                v0_3 = "";
            } else {
                v0_3 = android.support.v4.m.a.h;
            }
        } else {
            v0_3 = android.support.v4.m.a.g;
        }
        return v0_3;
    }

    private String a(String p8, android.support.v4.m.l p9, boolean p10)
    {
        String v0_15;
        if (p8 != null) {
            String v0_3;
            int v3_0 = p9.a(p8, 0, p8.length());
            StringBuilder v4_1 = new StringBuilder();
            if ((this.o & 2) == 0) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            if ((v0_3 != null) && (p10)) {
                String v0_4;
                if (v3_0 == 0) {
                    v0_4 = android.support.v4.m.m.a;
                } else {
                    v0_4 = android.support.v4.m.m.b;
                }
                String v0_7;
                String v0_5 = v0_4.a(p8, 0, p8.length());
                if ((this.n) || ((v0_5 == null) && (android.support.v4.m.a.d(p8) != 1))) {
                    if ((!this.n) || ((v0_5 != null) && (android.support.v4.m.a.d(p8) != -1))) {
                        v0_7 = "";
                    } else {
                        v0_7 = android.support.v4.m.a.h;
                    }
                } else {
                    v0_7 = android.support.v4.m.a.g;
                }
                v4_1.append(v0_7);
            }
            if (v3_0 == this.n) {
                v4_1.append(p8);
            } else {
                String v0_9;
                if (v3_0 == 0) {
                    v0_9 = 8234;
                } else {
                    v0_9 = 8235;
                }
                v4_1.append(v0_9);
                v4_1.append(p8);
                v4_1.append(8236);
            }
            if (p10) {
                String v0_11;
                if (v3_0 == 0) {
                    v0_11 = android.support.v4.m.m.a;
                } else {
                    v0_11 = android.support.v4.m.m.b;
                }
                String v0_14;
                String v0_12 = v0_11.a(p8, 0, p8.length());
                if ((this.n) || ((v0_12 == null) && (android.support.v4.m.a.c(p8) != 1))) {
                    if ((!this.n) || ((v0_12 != null) && (android.support.v4.m.a.c(p8) != -1))) {
                        v0_14 = "";
                    } else {
                        v0_14 = android.support.v4.m.a.h;
                    }
                } else {
                    v0_14 = android.support.v4.m.a.g;
                }
                v4_1.append(v0_14);
            }
            v0_15 = v4_1.toString();
        } else {
            v0_15 = 0;
        }
        return v0_15;
    }

    private String a(String p2, boolean p3)
    {
        return this.a(p2, this.p, p3);
    }

    private boolean a(String p4)
    {
        return this.p.a(p4, 0, p4.length());
    }

    static synthetic boolean a(java.util.Locale p2)
    {
        int v0 = 1;
        if (android.support.v4.m.u.a(p2) != 1) {
            v0 = 0;
        }
        return v0;
    }

    static synthetic android.support.v4.m.a b()
    {
        return android.support.v4.m.a.m;
    }

    private static android.support.v4.m.a b(java.util.Locale p1)
    {
        return new android.support.v4.m.c(p1).a();
    }

    private String b(String p3)
    {
        return this.a(p3, this.p, 1);
    }

    private String b(String p4, android.support.v4.m.l p5)
    {
        String v0_3;
        String v0_1 = p5.a(p4, 0, p4.length());
        if ((this.n) || ((v0_1 == null) && (android.support.v4.m.a.d(p4) != 1))) {
            if ((!this.n) || ((v0_1 != null) && (android.support.v4.m.a.d(p4) != -1))) {
                v0_3 = "";
            } else {
                v0_3 = android.support.v4.m.a.h;
            }
        } else {
            v0_3 = android.support.v4.m.a.g;
        }
        return v0_3;
    }

    private static int c(String p7)
    {
        int v1 = 0;
        android.support.v4.m.d v5_1 = new android.support.v4.m.d(p7);
        v5_1.d = v5_1.c;
        int v0_1 = 0;
        int v2 = 0;
        while (v5_1.d > 0) {
            switch (v5_1.a()) {
                case 0:
                    if (v2 != 0) {
                        if (v0_1 == 0) {
                            v0_1 = v2;
                        }
                    } else {
                        v1 = -1;
                        break;
                    }
                    break;
                case 1:
                case 2:
                    if (v2 != 0) {
                        if (v0_1 == 0) {
                            v0_1 = v2;
                        }
                    } else {
                        v1 = 1;
                        break;
                    }
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 10:
                case 11:
                case 12:
                case 13:
                default:
                    if (v0_1 == 0) {
                        v0_1 = v2;
                    }
                    break;
                case 14:
                case 15:
                    if (v0_1 != v2) {
                        v2--;
                    } else {
                        v1 = -1;
                        break;
                    }
                    break;
                case 16:
                case 17:
                    if (v0_1 != v2) {
                        v2--;
                    } else {
                        v1 = 1;
                        break;
                    }
                    break;
                case 18:
                    v2++;
                    break;
            }
        }
        return v1;
    }

    static synthetic android.support.v4.m.a c()
    {
        return android.support.v4.m.a.l;
    }

    private String c(String p2, android.support.v4.m.l p3)
    {
        return this.a(p2, p3, 1);
    }

    private static boolean c(java.util.Locale p2)
    {
        int v0 = 1;
        if (android.support.v4.m.u.a(p2) != 1) {
            v0 = 0;
        }
        return v0;
    }

    private static int d(String p14)
    {
        int v4 = -1;
        android.support.v4.m.d v8_1 = new android.support.v4.m.d(p14);
        v8_1.d = 0;
        int v0 = 0;
        int v3_0 = 0;
        int v2 = 0;
        while ((v8_1.d < v8_1.c) && (v0 == 0)) {
            int v6_8;
            v8_1.e = v8_1.a.charAt(v8_1.d);
            if (!Character.isHighSurrogate(v8_1.e)) {
                v8_1.d = (v8_1.d + 1);
                v6_8 = android.support.v4.m.d.a(v8_1.e);
                if (v8_1.b) {
                    if (v8_1.e != 60) {
                        if (v8_1.e != 38) {
                            switch (v6_8) {
                                case 0:
                                    if (v2 != 0) {
                                        v0 = v2;
                                    }
                                    break;
                                case 1:
                                case 2:
                                    if (v2 != 0) {
                                        v0 = v2;
                                    } else {
                                        v4 = 1;
                                    }
                                    break;
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                default:
                                    v0 = v2;
                                    break;
                                case 14:
                                case 15:
                                    v2++;
                                    v3_0 = -1;
                                    break;
                                case 16:
                                case 17:
                                    v2++;
                                    v3_0 = 1;
                                    break;
                                case 18:
                                    v2--;
                                    v3_0 = 0;
                                    break;
                            }
                            return v4;
                        }
                        while (v8_1.d < v8_1.c) {
                            int v6_10 = v8_1.a;
                            char v9_6 = v8_1.d;
                            v8_1.d = (v9_6 + 1);
                            int v6_11 = v6_10.charAt(v9_6);
                            v8_1.e = v6_11;
                            if (v6_11 == 59) {
                                break;
                            }
                        }
                        v6_8 = 12;
                    } else {
                        int v6_12 = v8_1.d;
                        while (v8_1.d < v8_1.c) {
                            char v9_9 = v8_1.a;
                            char v10_3 = v8_1.d;
                            v8_1.d = (v10_3 + 1);
                            v8_1.e = v9_9.charAt(v10_3);
                            if (v8_1.e != 62) {
                                if ((v8_1.e == 34) || (v8_1.e == 39)) {
                                    char v9_14 = v8_1.e;
                                    while (v8_1.d < v8_1.c) {
                                        char v10_8 = v8_1.a;
                                        int v11_2 = v8_1.d;
                                        v8_1.d = (v11_2 + 1);
                                        char v10_9 = v10_8.charAt(v11_2);
                                        v8_1.e = v10_9;
                                        if (v10_9 == v9_14) {
                                            break;
                                        }
                                    }
                                }
                            } else {
                                v6_8 = 12;
                            }
                        }
                        v8_1.d = v6_12;
                        v8_1.e = 60;
                        v6_8 = 13;
                    }
                }
            } else {
                int v6_14 = Character.codePointAt(v8_1.a, v8_1.d);
                v8_1.d = (v8_1.d + Character.charCount(v6_14));
                v6_8 = Character.getDirectionality(v6_14);
            }
        }
        if (v0 != 0) {
            if (v3_0 != 0) {
                v4 = v3_0;
                return v4;
            }
            while (v8_1.d > 0) {
                switch (v8_1.a()) {
                    case 14:
                    case 15:
                        if (v0 != v2) {
                            v2--;
                        }
                    case 16:
                    case 17:
                        if (v0 != v2) {
                            v2--;
                        } else {
                            v4 = 1;
                        }
                        break;
                    case 18:
                        v2++;
                        break;
                    default:
                }
                return v4;
            }
        }
        v4 = 0;
        return v4;
    }

    private static android.support.v4.m.a d()
    {
        return new android.support.v4.m.c().a();
    }

    private boolean e()
    {
        return this.n;
    }

    private boolean f()
    {
        int v0_2;
        if ((this.o & 2) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }
}
