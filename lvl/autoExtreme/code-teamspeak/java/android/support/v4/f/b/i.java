package android.support.v4.f.b;
public final class i {

    public i()
    {
        return;
    }

    private static android.hardware.fingerprint.FingerprintManager$AuthenticationCallback a(android.support.v4.f.b.k p1)
    {
        return new android.support.v4.f.b.j(p1);
    }

    private static android.hardware.fingerprint.FingerprintManager$CryptoObject a(android.support.v4.f.b.m p2)
    {
        android.hardware.fingerprint.FingerprintManager$CryptoObject v0_0 = 0;
        if (p2 != null) {
            if (p2.b == null) {
                if (p2.a == null) {
                    if (p2.c != null) {
                        v0_0 = new android.hardware.fingerprint.FingerprintManager$CryptoObject(p2.c);
                    }
                } else {
                    v0_0 = new android.hardware.fingerprint.FingerprintManager$CryptoObject(p2.a);
                }
            } else {
                v0_0 = new android.hardware.fingerprint.FingerprintManager$CryptoObject(p2.b);
            }
        }
        return v0_0;
    }

    static android.hardware.fingerprint.FingerprintManager a(android.content.Context p1)
    {
        return ((android.hardware.fingerprint.FingerprintManager) p1.getSystemService(android.hardware.fingerprint.FingerprintManager));
    }

    private static android.support.v4.f.b.m a(android.hardware.fingerprint.FingerprintManager$CryptoObject p2)
    {
        android.support.v4.f.b.m v0_0 = 0;
        if (p2 != null) {
            if (p2.getCipher() == null) {
                if (p2.getSignature() == null) {
                    if (p2.getMac() != null) {
                        v0_0 = new android.support.v4.f.b.m(p2.getMac());
                    }
                } else {
                    v0_0 = new android.support.v4.f.b.m(p2.getSignature());
                }
            } else {
                v0_0 = new android.support.v4.f.b.m(p2.getCipher());
            }
        }
        return v0_0;
    }

    private static void a(android.content.Context p6, android.support.v4.f.b.m p7, int p8, Object p9, android.support.v4.f.b.k p10, android.os.Handler p11)
    {
        android.hardware.fingerprint.FingerprintManager$CryptoObject v1_3;
        android.hardware.fingerprint.FingerprintManager v0 = android.support.v4.f.b.i.a(p6);
        if (p7 == null) {
            v1_3 = 0;
        } else {
            if (p7.b == null) {
                if (p7.a == null) {
                    if (p7.c == null) {
                    } else {
                        v1_3 = new android.hardware.fingerprint.FingerprintManager$CryptoObject(p7.c);
                    }
                } else {
                    v1_3 = new android.hardware.fingerprint.FingerprintManager$CryptoObject(p7.a);
                }
            } else {
                v1_3 = new android.hardware.fingerprint.FingerprintManager$CryptoObject(p7.b);
            }
        }
        v0.authenticate(v1_3, ((android.os.CancellationSignal) p9), p8, new android.support.v4.f.b.j(p10), p11);
        return;
    }

    private static synthetic android.support.v4.f.b.m b(android.hardware.fingerprint.FingerprintManager$CryptoObject p2)
    {
        android.support.v4.f.b.m v0_3;
        if (p2 == null) {
            v0_3 = 0;
        } else {
            if (p2.getCipher() == null) {
                if (p2.getSignature() == null) {
                    if (p2.getMac() == null) {
                    } else {
                        v0_3 = new android.support.v4.f.b.m(p2.getMac());
                    }
                } else {
                    v0_3 = new android.support.v4.f.b.m(p2.getSignature());
                }
            } else {
                v0_3 = new android.support.v4.f.b.m(p2.getCipher());
            }
        }
        return v0_3;
    }

    private static boolean b(android.content.Context p1)
    {
        return android.support.v4.f.b.i.a(p1).hasEnrolledFingerprints();
    }

    private static boolean c(android.content.Context p1)
    {
        return android.support.v4.f.b.i.a(p1).isHardwareDetected();
    }
}
