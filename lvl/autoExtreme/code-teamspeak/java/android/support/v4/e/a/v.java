package android.support.v4.e.a;
public abstract class v extends android.graphics.drawable.Drawable {
    private static final int d = 3;
    final android.graphics.Bitmap a;
    float b;
    final android.graphics.Rect c;
    private int e;
    private int f;
    private final android.graphics.Paint g;
    private final android.graphics.BitmapShader h;
    private final android.graphics.Matrix i;
    private final android.graphics.RectF j;
    private boolean k;
    private boolean l;
    private int m;
    private int n;

    v(android.content.res.Resources p5, android.graphics.Bitmap p6)
    {
        this.e = 160;
        this.f = 119;
        this.g = new android.graphics.Paint(3);
        this.i = new android.graphics.Matrix();
        this.c = new android.graphics.Rect();
        this.j = new android.graphics.RectF();
        this.k = 1;
        if (p5 != null) {
            this.e = p5.getDisplayMetrics().densityDpi;
        }
        this.a = p6;
        if (this.a == null) {
            this.n = -1;
            this.m = -1;
            this.h = 0;
        } else {
            this.e();
            this.h = new android.graphics.BitmapShader(this.a, android.graphics.Shader$TileMode.CLAMP, android.graphics.Shader$TileMode.CLAMP);
        }
        return;
    }

    private void a(int p2)
    {
        if (this.e != p2) {
            if (p2 == 0) {
                p2 = 160;
            }
            this.e = p2;
            if (this.a != null) {
                this.e();
            }
            this.invalidateSelf();
        }
        return;
    }

    private void a(android.graphics.Canvas p2)
    {
        this.a(p2.getDensity());
        return;
    }

    private void a(android.util.DisplayMetrics p2)
    {
        this.a(p2.densityDpi);
        return;
    }

    private static boolean a(float p1)
    {
        int v0_2;
        if (p1 <= 1028443341) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private void b(int p2)
    {
        if (this.f != p2) {
            this.f = p2;
            this.k = 1;
            this.invalidateSelf();
        }
        return;
    }

    private void b(boolean p2)
    {
        this.g.setAntiAlias(p2);
        this.invalidateSelf();
        return;
    }

    private android.graphics.Paint c()
    {
        return this.g;
    }

    private void c(boolean p4)
    {
        this.l = p4;
        this.k = 1;
        if (!p4) {
            if (this.b != 0) {
                this.l = 0;
                if (!android.support.v4.e.a.v.a(0)) {
                    this.g.setShader(0);
                } else {
                    this.g.setShader(this.h);
                }
                this.b = 0;
                this.invalidateSelf();
            }
        } else {
            this.h();
            this.g.setShader(this.h);
            this.invalidateSelf();
        }
        return;
    }

    private android.graphics.Bitmap d()
    {
        return this.a;
    }

    private void e()
    {
        this.m = this.a.getScaledWidth(this.e);
        this.n = this.a.getScaledHeight(this.e);
        return;
    }

    private int f()
    {
        return this.f;
    }

    private boolean g()
    {
        return this.g.isAntiAlias();
    }

    private void h()
    {
        this.b = ((float) (Math.min(this.n, this.m) / 2));
        return;
    }

    private boolean i()
    {
        return this.l;
    }

    private void j()
    {
        if (this.b != 0) {
            this.l = 0;
            if (!android.support.v4.e.a.v.a(0)) {
                this.g.setShader(0);
            } else {
                this.g.setShader(this.h);
            }
            this.b = 0;
            this.invalidateSelf();
        }
        return;
    }

    private float k()
    {
        return this.b;
    }

    void a(int p2, int p3, int p4, android.graphics.Rect p5, android.graphics.Rect p6)
    {
        throw new UnsupportedOperationException();
    }

    public void a(boolean p2)
    {
        throw new UnsupportedOperationException();
    }

    public boolean a()
    {
        throw new UnsupportedOperationException();
    }

    final void b()
    {
        if (this.k) {
            if (!this.l) {
                this.a(this.f, this.m, this.n, this.getBounds(), this.c);
            } else {
                float v2_1 = Math.min(this.m, this.n);
                this.a(this.f, v2_1, v2_1, this.getBounds(), this.c);
                android.graphics.Paint v0_7 = Math.min(this.c.width(), this.c.height());
                this.c.inset(Math.max(0, ((this.c.width() - v0_7) / 2)), Math.max(0, ((this.c.height() - v0_7) / 2)));
                this.b = (((float) v0_7) * 1056964608);
            }
            this.j.set(this.c);
            if (this.h != null) {
                this.i.setTranslate(this.j.left, this.j.top);
                this.i.preScale((this.j.width() / ((float) this.a.getWidth())), (this.j.height() / ((float) this.a.getHeight())));
                this.h.setLocalMatrix(this.i);
                this.g.setShader(this.h);
            }
            this.k = 0;
        }
        return;
    }

    public void draw(android.graphics.Canvas p5)
    {
        android.graphics.RectF v0_0 = this.a;
        if (v0_0 != null) {
            this.b();
            if (this.g.getShader() != null) {
                p5.drawRoundRect(this.j, this.b, this.b, this.g);
            } else {
                p5.drawBitmap(v0_0, 0, this.c, this.g);
            }
        }
        return;
    }

    public int getAlpha()
    {
        return this.g.getAlpha();
    }

    public android.graphics.ColorFilter getColorFilter()
    {
        return this.g.getColorFilter();
    }

    public int getIntrinsicHeight()
    {
        return this.n;
    }

    public int getIntrinsicWidth()
    {
        return this.m;
    }

    public int getOpacity()
    {
        int v0 = -3;
        if ((this.f == 119) && (!this.l)) {
            boolean v1_2 = this.a;
            if ((v1_2) && ((!v1_2.hasAlpha()) && ((this.g.getAlpha() >= 255) && (!android.support.v4.e.a.v.a(this.b))))) {
                v0 = -1;
            }
        }
        return v0;
    }

    protected void onBoundsChange(android.graphics.Rect p2)
    {
        super.onBoundsChange(p2);
        if (this.l) {
            this.h();
        }
        this.k = 1;
        return;
    }

    public void setAlpha(int p2)
    {
        if (p2 != this.g.getAlpha()) {
            this.g.setAlpha(p2);
            this.invalidateSelf();
        }
        return;
    }

    public void setColorFilter(android.graphics.ColorFilter p2)
    {
        this.g.setColorFilter(p2);
        this.invalidateSelf();
        return;
    }

    public void setDither(boolean p2)
    {
        this.g.setDither(p2);
        this.invalidateSelf();
        return;
    }

    public void setFilterBitmap(boolean p2)
    {
        this.g.setFilterBitmap(p2);
        this.invalidateSelf();
        return;
    }
}
