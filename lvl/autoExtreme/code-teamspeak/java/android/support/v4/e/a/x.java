package android.support.v4.e.a;
public final class x {
    private static final String a = "RoundedBitmapDrawableFactory";

    public x()
    {
        return;
    }

    private static android.support.v4.e.a.v a(android.content.res.Resources p2, android.graphics.Bitmap p3)
    {
        android.support.v4.e.a.y v0_2;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_2 = new android.support.v4.e.a.y(p2, p3);
        } else {
            v0_2 = new android.support.v4.e.a.w(p2, p3);
        }
        return v0_2;
    }

    private static android.support.v4.e.a.v a(android.content.res.Resources p4, java.io.InputStream p5)
    {
        android.support.v4.e.a.v v0_1 = android.support.v4.e.a.x.a(p4, android.graphics.BitmapFactory.decodeStream(p5));
        if (v0_1.a == null) {
            android.util.Log.w("RoundedBitmapDrawableFactory", new StringBuilder("RoundedBitmapDrawable cannot decode ").append(p5).toString());
        }
        return v0_1;
    }

    private static android.support.v4.e.a.v a(android.content.res.Resources p4, String p5)
    {
        android.support.v4.e.a.v v0_1 = android.support.v4.e.a.x.a(p4, android.graphics.BitmapFactory.decodeFile(p5));
        if (v0_1.a == null) {
            android.util.Log.w("RoundedBitmapDrawableFactory", new StringBuilder("RoundedBitmapDrawable cannot decode ").append(p5).toString());
        }
        return v0_1;
    }
}
