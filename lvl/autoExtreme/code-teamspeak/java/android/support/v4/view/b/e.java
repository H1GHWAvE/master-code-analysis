package android.support.v4.view.b;
public final class e {

    private e()
    {
        return;
    }

    private static android.view.animation.Interpolator a(float p2, float p3)
    {
        android.support.v4.view.b.h v0_2;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_2 = new android.support.v4.view.b.h(p2, p3);
        } else {
            v0_2 = new android.view.animation.PathInterpolator(p2, p3);
        }
        return v0_2;
    }

    private static android.view.animation.Interpolator a(float p2, float p3, float p4, float p5)
    {
        android.support.v4.view.b.h v0_2;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_2 = new android.support.v4.view.b.h(p2, p3, p4, p5);
        } else {
            v0_2 = new android.view.animation.PathInterpolator(p2, p3, p4, p5);
        }
        return v0_2;
    }

    private static android.view.animation.Interpolator a(android.graphics.Path p2)
    {
        android.support.v4.view.b.h v0_2;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_2 = new android.support.v4.view.b.h(p2);
        } else {
            v0_2 = new android.view.animation.PathInterpolator(p2);
        }
        return v0_2;
    }
}
