package android.support.v4.view.a;
final class al {

    al()
    {
        return;
    }

    private static String a(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getViewIdResourceName();
    }

    private static void a(Object p0, int p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setTextSelection(p1, p2);
        return;
    }

    private static void a(Object p0, String p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setViewIdResourceName(p1);
        return;
    }

    private static void a(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setEditable(p1);
        return;
    }

    private static int b(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getTextSelectionStart();
    }

    private static java.util.List b(Object p1, String p2)
    {
        return ((java.util.List) ((android.view.accessibility.AccessibilityNodeInfo) p1).findAccessibilityNodeInfosByViewId(p2));
    }

    private static int c(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getTextSelectionEnd();
    }

    private static boolean d(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isEditable();
    }

    private static boolean e(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).refresh();
    }
}
