package android.support.v4.view.a;
public final class ae {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    final Object d;

    private ae(Object p1)
    {
        this.d = p1;
        return;
    }

    synthetic ae(Object p1, byte p2)
    {
        this(p1);
        return;
    }

    private float a()
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$RangeInfo) this.d).getCurrent();
    }

    private static synthetic Object a(android.support.v4.view.a.ae p1)
    {
        return p1.d;
    }

    private float b()
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$RangeInfo) this.d).getMax();
    }

    private float c()
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$RangeInfo) this.d).getMin();
    }

    private int d()
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$RangeInfo) this.d).getType();
    }
}
