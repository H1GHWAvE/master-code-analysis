package android.support.v4.view;
public interface cr {

    public abstract android.content.res.ColorStateList getSupportBackgroundTintList();

    public abstract android.graphics.PorterDuff$Mode getSupportBackgroundTintMode();

    public abstract void setSupportBackgroundTintList();

    public abstract void setSupportBackgroundTintMode();
}
