package android.support.v4.view.a;
public final class ac {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    final Object d;

    private ac(Object p1)
    {
        this.d = p1;
        return;
    }

    synthetic ac(Object p1, byte p2)
    {
        this(p1);
        return;
    }

    private int a()
    {
        return android.support.v4.view.a.q.p().L(this.d);
    }

    private static android.support.v4.view.a.ac a(int p2, int p3, boolean p4, int p5)
    {
        return new android.support.v4.view.a.ac(android.support.v4.view.a.q.p().a(p2, p3, p4, p5));
    }

    private int b()
    {
        return android.support.v4.view.a.q.p().M(this.d);
    }

    private boolean c()
    {
        return android.support.v4.view.a.q.p().N(this.d);
    }
}
