package android.support.v4.view;
final class cp {

    private cp()
    {
        return;
    }

    private static void a(Object p0, boolean p1)
    {
        ((android.view.ScaleGestureDetector) p0).setQuickScaleEnabled(p1);
        return;
    }

    private static boolean a(Object p1)
    {
        return ((android.view.ScaleGestureDetector) p1).isQuickScaleEnabled();
    }
}
