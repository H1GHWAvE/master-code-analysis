package android.support.v4.view;
 class de extends android.support.v4.view.dd {

    de()
    {
        return;
    }

    public final int L(android.view.View p2)
    {
        return p2.getWindowSystemUiVisibility();
    }

    public final boolean T(android.view.View p2)
    {
        return p2.isPaddingRelative();
    }

    public final void a(android.view.View p1, android.graphics.Paint p2)
    {
        p1.setLayerPaint(p2);
        return;
    }

    public final void b(android.view.View p1, int p2, int p3, int p4, int p5)
    {
        p1.setPaddingRelative(p2, p3, p4, p5);
        return;
    }

    public final void e(android.view.View p1, int p2)
    {
        p1.setLabelFor(p2);
        return;
    }

    public final void f(android.view.View p1, int p2)
    {
        p1.setLayoutDirection(p2);
        return;
    }

    public final int j(android.view.View p2)
    {
        return p2.getLabelFor();
    }

    public final int k(android.view.View p2)
    {
        return p2.getLayoutDirection();
    }

    public final int r(android.view.View p2)
    {
        return p2.getPaddingStart();
    }

    public final int s(android.view.View p2)
    {
        return p2.getPaddingEnd();
    }
}
