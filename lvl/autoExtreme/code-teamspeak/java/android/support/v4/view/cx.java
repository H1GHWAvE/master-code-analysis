package android.support.v4.view;
public final class cx {
    private static final long A = 10;
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = 0;
    public static final int e = 1;
    public static final int f = 2;
    public static final int g = 4;
    public static final int h = 0;
    public static final int i = 1;
    public static final int j = 2;
    public static final int k = 0;
    public static final int l = 1;
    public static final int m = 2;
    public static final int n = 0;
    public static final int o = 1;
    public static final int p = 2;
    public static final int q = 3;
    public static final int r = 16777215;
    public static final int s = 4278190080;
    public static final int t = 16;
    public static final int u = 16777216;
    public static final int v = 0;
    public static final int w = 1;
    public static final int x = 2;
    static final android.support.v4.view.di y = None;
    private static final String z = "ViewCompat";

    static cx()
    {
        android.support.v4.view.cy v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 21) {
            if (v0_0 < 19) {
                if (v0_0 < 17) {
                    if (v0_0 < 16) {
                        if (v0_0 < 14) {
                            if (v0_0 < 11) {
                                if (v0_0 < 9) {
                                    if (v0_0 < 7) {
                                        android.support.v4.view.cx.y = new android.support.v4.view.cy();
                                    } else {
                                        android.support.v4.view.cx.y = new android.support.v4.view.cz();
                                    }
                                } else {
                                    android.support.v4.view.cx.y = new android.support.v4.view.da();
                                }
                            } else {
                                android.support.v4.view.cx.y = new android.support.v4.view.db();
                            }
                        } else {
                            android.support.v4.view.cx.y = new android.support.v4.view.dc();
                        }
                    } else {
                        android.support.v4.view.cx.y = new android.support.v4.view.dd();
                    }
                } else {
                    android.support.v4.view.cx.y = new android.support.v4.view.de();
                }
            } else {
                android.support.v4.view.cx.y = new android.support.v4.view.dg();
            }
        } else {
            android.support.v4.view.cx.y = new android.support.v4.view.dh();
        }
        return;
    }

    public cx()
    {
        return;
    }

    public static void A(android.view.View p1)
    {
        android.support.v4.view.cx.y.X(p1);
        return;
    }

    public static boolean B(android.view.View p1)
    {
        return android.support.v4.view.cx.y.Z(p1);
    }

    public static float C(android.view.View p1)
    {
        return android.support.v4.view.cx.y.aa(p1);
    }

    public static boolean D(android.view.View p1)
    {
        return android.support.v4.view.cx.y.ab(p1);
    }

    private static boolean E(android.view.View p1)
    {
        return android.support.v4.view.cx.y.b(p1);
    }

    private static boolean F(android.view.View p1)
    {
        return android.support.v4.view.cx.y.c(p1);
    }

    private static android.support.v4.view.a.aq G(android.view.View p1)
    {
        return android.support.v4.view.cx.y.g(p1);
    }

    private static int H(android.view.View p1)
    {
        return android.support.v4.view.cx.y.j(p1);
    }

    private static int I(android.view.View p1)
    {
        return android.support.v4.view.cx.y.o(p1);
    }

    private static int J(android.view.View p1)
    {
        return android.support.v4.view.cx.y.q(p1);
    }

    private static void K(android.view.View p1)
    {
        android.support.v4.view.cx.y.t(p1);
        return;
    }

    private static void L(android.view.View p1)
    {
        android.support.v4.view.cx.y.u(p1);
        return;
    }

    private static int M(android.view.View p1)
    {
        return android.support.v4.view.cx.y.F(p1);
    }

    private static float N(android.view.View p1)
    {
        return android.support.v4.view.cx.y.I(p1);
    }

    private static float O(android.view.View p1)
    {
        return android.support.v4.view.cx.y.J(p1);
    }

    private static float P(android.view.View p1)
    {
        return android.support.v4.view.cx.y.A(p1);
    }

    private static float Q(android.view.View p1)
    {
        return android.support.v4.view.cx.y.B(p1);
    }

    private static float R(android.view.View p1)
    {
        return android.support.v4.view.cx.y.C(p1);
    }

    private static float S(android.view.View p1)
    {
        return android.support.v4.view.cx.y.E(p1);
    }

    private static float T(android.view.View p1)
    {
        return android.support.v4.view.cx.y.y(p1);
    }

    private static float U(android.view.View p1)
    {
        return android.support.v4.view.cx.y.z(p1);
    }

    private static float V(android.view.View p1)
    {
        return android.support.v4.view.cx.y.O(p1);
    }

    private static String W(android.view.View p1)
    {
        return android.support.v4.view.cx.y.K(p1);
    }

    private static android.content.res.ColorStateList X(android.view.View p1)
    {
        return android.support.v4.view.cx.y.V(p1);
    }

    private static android.graphics.PorterDuff$Mode Y(android.view.View p1)
    {
        return android.support.v4.view.cx.y.W(p1);
    }

    private static boolean Z(android.view.View p1)
    {
        return android.support.v4.view.cx.y.Y(p1);
    }

    public static int a(int p1, int p2)
    {
        return android.support.v4.view.cx.y.a(p1, p2);
    }

    public static int a(int p1, int p2, int p3)
    {
        return android.support.v4.view.cx.y.a(p1, p2, p3);
    }

    public static int a(android.view.View p1)
    {
        return android.support.v4.view.cx.y.a(p1);
    }

    public static android.support.v4.view.gh a(android.view.View p1, android.support.v4.view.gh p2)
    {
        return android.support.v4.view.cx.y.a(p1, p2);
    }

    public static void a(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.b(p1, p2);
        return;
    }

    public static void a(android.view.View p6, int p7, int p8, int p9, int p10)
    {
        android.support.v4.view.cx.y.a(p6, p7, p8, p9, p10);
        return;
    }

    public static void a(android.view.View p1, int p2, android.graphics.Paint p3)
    {
        android.support.v4.view.cx.y.a(p1, p2, p3);
        return;
    }

    public static void a(android.view.View p1, android.content.res.ColorStateList p2)
    {
        android.support.v4.view.cx.y.a(p1, p2);
        return;
    }

    public static void a(android.view.View p1, android.graphics.Paint p2)
    {
        android.support.v4.view.cx.y.a(p1, p2);
        return;
    }

    public static void a(android.view.View p1, android.graphics.PorterDuff$Mode p2)
    {
        android.support.v4.view.cx.y.a(p1, p2);
        return;
    }

    private static void a(android.view.View p1, android.graphics.Rect p2)
    {
        android.support.v4.view.cx.y.a(p1, p2);
        return;
    }

    public static void a(android.view.View p1, android.support.v4.view.a.q p2)
    {
        android.support.v4.view.cx.y.a(p1, p2);
        return;
    }

    public static void a(android.view.View p1, android.support.v4.view.a p2)
    {
        android.support.v4.view.cx.y.a(p1, p2);
        return;
    }

    public static void a(android.view.View p1, android.support.v4.view.bx p2)
    {
        android.support.v4.view.cx.y.a(p1, p2);
        return;
    }

    public static void a(android.view.View p1, android.view.accessibility.AccessibilityEvent p2)
    {
        android.support.v4.view.cx.y.b(p1, p2);
        return;
    }

    public static void a(android.view.View p1, Runnable p2)
    {
        android.support.v4.view.cx.y.a(p1, p2);
        return;
    }

    public static void a(android.view.View p2, Runnable p3, long p4)
    {
        android.support.v4.view.cx.y.a(p2, p3, p4);
        return;
    }

    private static void a(android.view.View p1, String p2)
    {
        android.support.v4.view.cx.y.a(p1, p2);
        return;
    }

    public static void a(android.view.View p1, boolean p2)
    {
        android.support.v4.view.cx.y.b(p1, p2);
        return;
    }

    public static void a(android.view.ViewGroup p1)
    {
        android.support.v4.view.cx.y.a(p1);
        return;
    }

    private static boolean a(android.view.View p1, float p2, float p3)
    {
        return android.support.v4.view.cx.y.a(p1, p2, p3);
    }

    private static boolean a(android.view.View p1, float p2, float p3, boolean p4)
    {
        return android.support.v4.view.cx.y.a(p1, p2, p3, p4);
    }

    public static boolean a(android.view.View p1, int p2)
    {
        return android.support.v4.view.cx.y.a(p1, p2);
    }

    private static boolean a(android.view.View p7, int p8, int p9, int p10, int p11, int[] p12)
    {
        return android.support.v4.view.cx.y.a(p7, p8, p9, p10, p11, p12);
    }

    private static boolean a(android.view.View p6, int p7, int p8, int[] p9, int[] p10)
    {
        return android.support.v4.view.cx.y.a(p6, p7, p8, p9, p10);
    }

    public static boolean a(android.view.View p1, int p2, android.os.Bundle p3)
    {
        return android.support.v4.view.cx.y.a(p1, p2, p3);
    }

    private static android.graphics.Rect aa(android.view.View p1)
    {
        return android.support.v4.view.cx.y.P(p1);
    }

    public static android.support.v4.view.gh b(android.view.View p1, android.support.v4.view.gh p2)
    {
        return android.support.v4.view.cx.y.b(p1, p2);
    }

    public static void b(android.view.View p1)
    {
        android.support.v4.view.cx.y.d(p1);
        return;
    }

    public static void b(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.c(p1, p2);
        return;
    }

    public static void b(android.view.View p6, int p7, int p8, int p9, int p10)
    {
        android.support.v4.view.cx.y.b(p6, p7, p8, p9, p10);
        return;
    }

    private static void b(android.view.View p1, android.view.accessibility.AccessibilityEvent p2)
    {
        android.support.v4.view.cx.y.a(p1, p2);
        return;
    }

    public static void b(android.view.View p1, boolean p2)
    {
        android.support.v4.view.cx.y.c(p1, p2);
        return;
    }

    public static boolean b(android.view.View p1, int p2)
    {
        return android.support.v4.view.cx.y.b(p1, p2);
    }

    public static int c(android.view.View p1)
    {
        return android.support.v4.view.cx.y.e(p1);
    }

    public static void c(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.d(p1, p2);
        return;
    }

    public static void c(android.view.View p1, int p2)
    {
        android.support.v4.view.cx.y.d(p1, p2);
        return;
    }

    private static void c(android.view.View p1, boolean p2)
    {
        android.support.v4.view.cx.y.a(p1, p2);
        return;
    }

    public static float d(android.view.View p1)
    {
        return android.support.v4.view.cx.y.h(p1);
    }

    public static void d(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.g(p1, p2);
        return;
    }

    public static void d(android.view.View p2, int p3)
    {
        p2.offsetTopAndBottom(p3);
        if ((p3 != 0) && (android.os.Build$VERSION.SDK_INT < 11)) {
            p2.invalidate();
        }
        return;
    }

    private static void d(android.view.View p1, boolean p2)
    {
        android.support.v4.view.cx.y.d(p1, p2);
        return;
    }

    public static int e(android.view.View p1)
    {
        return android.support.v4.view.cx.y.i(p1);
    }

    public static void e(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.h(p1, p2);
        return;
    }

    public static void e(android.view.View p2, int p3)
    {
        p2.offsetLeftAndRight(p3);
        if ((p3 != 0) && (android.os.Build$VERSION.SDK_INT < 11)) {
            p2.invalidate();
        }
        return;
    }

    public static int f(android.view.View p1)
    {
        return android.support.v4.view.cx.y.k(p1);
    }

    public static void f(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.m(p1, p2);
        return;
    }

    private static void f(android.view.View p1, int p2)
    {
        android.support.v4.view.cx.y.c(p1, p2);
        return;
    }

    public static android.view.ViewParent g(android.view.View p1)
    {
        return android.support.v4.view.cx.y.l(p1);
    }

    private static void g(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.i(p1, p2);
        return;
    }

    private static void g(android.view.View p1, int p2)
    {
        android.support.v4.view.cx.y.e(p1, p2);
        return;
    }

    private static void h(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.j(p1, p2);
        return;
    }

    private static void h(android.view.View p1, int p2)
    {
        android.support.v4.view.cx.y.f(p1, p2);
        return;
    }

    public static boolean h(android.view.View p1)
    {
        return android.support.v4.view.cx.y.m(p1);
    }

    public static int i(android.view.View p1)
    {
        return android.support.v4.view.cx.y.n(p1);
    }

    private static void i(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.a(p1, p2);
        return;
    }

    private static void i(android.view.View p1, int p2)
    {
        android.support.v4.view.cx.y.g(p1, p2);
        return;
    }

    public static int j(android.view.View p1)
    {
        return android.support.v4.view.cx.y.p(p1);
    }

    private static void j(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.e(p1, p2);
        return;
    }

    private static boolean j(android.view.View p1, int p2)
    {
        return android.support.v4.view.cx.y.h(p1, p2);
    }

    public static int k(android.view.View p1)
    {
        return android.support.v4.view.cx.y.r(p1);
    }

    private static void k(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.f(p1, p2);
        return;
    }

    public static int l(android.view.View p1)
    {
        return android.support.v4.view.cx.y.s(p1);
    }

    private static void l(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.k(p1, p2);
        return;
    }

    public static float m(android.view.View p1)
    {
        return android.support.v4.view.cx.y.w(p1);
    }

    private static void m(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.k(p1, p2);
        return;
    }

    public static float n(android.view.View p1)
    {
        return android.support.v4.view.cx.y.x(p1);
    }

    private static void n(android.view.View p1, float p2)
    {
        android.support.v4.view.cx.y.n(p1, p2);
        return;
    }

    public static int o(android.view.View p1)
    {
        return android.support.v4.view.cx.y.G(p1);
    }

    public static android.support.v4.view.fk p(android.view.View p1)
    {
        return android.support.v4.view.cx.y.H(p1);
    }

    public static float q(android.view.View p1)
    {
        return android.support.v4.view.cx.y.D(p1);
    }

    public static float r(android.view.View p1)
    {
        return android.support.v4.view.cx.y.N(p1);
    }

    public static int s(android.view.View p1)
    {
        return android.support.v4.view.cx.y.L(p1);
    }

    public static void t(android.view.View p1)
    {
        android.support.v4.view.cx.y.M(p1);
        return;
    }

    public static boolean u(android.view.View p1)
    {
        return android.support.v4.view.cx.y.Q(p1);
    }

    public static void v(android.view.View p1)
    {
        android.support.v4.view.cx.y.R(p1);
        return;
    }

    public static void w(android.view.View p1)
    {
        android.support.v4.view.cx.y.S(p1);
        return;
    }

    public static boolean x(android.view.View p1)
    {
        return android.support.v4.view.cx.y.v(p1);
    }

    public static boolean y(android.view.View p1)
    {
        return android.support.v4.view.cx.y.T(p1);
    }

    public static boolean z(android.view.View p1)
    {
        return android.support.v4.view.cx.y.U(p1);
    }
}
