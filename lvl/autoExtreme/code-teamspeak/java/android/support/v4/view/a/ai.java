package android.support.v4.view.a;
final class ai {

    ai()
    {
        return;
    }

    private static Object a()
    {
        return android.view.accessibility.AccessibilityNodeInfo.obtain();
    }

    private static Object a(android.view.View p1)
    {
        return android.view.accessibility.AccessibilityNodeInfo.obtain(p1);
    }

    private static Object a(Object p1)
    {
        return android.view.accessibility.AccessibilityNodeInfo.obtain(((android.view.accessibility.AccessibilityNodeInfo) p1));
    }

    private static java.util.List a(Object p1, String p2)
    {
        return ((java.util.List) ((android.view.accessibility.AccessibilityNodeInfo) p1).findAccessibilityNodeInfosByText(p2));
    }

    private static void a(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).addAction(p1);
        return;
    }

    private static void a(Object p0, android.graphics.Rect p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).getBoundsInParent(p1);
        return;
    }

    private static void a(Object p0, android.view.View p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).addChild(p1);
        return;
    }

    private static void a(Object p0, CharSequence p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setClassName(p1);
        return;
    }

    private static void a(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setCheckable(p1);
        return;
    }

    private static int b(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getActions();
    }

    private static Object b(Object p1, int p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getChild(p2);
    }

    private static void b(Object p0, android.graphics.Rect p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).getBoundsInScreen(p1);
        return;
    }

    private static void b(Object p0, android.view.View p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setParent(p1);
        return;
    }

    private static void b(Object p0, CharSequence p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setContentDescription(p1);
        return;
    }

    private static void b(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setChecked(p1);
        return;
    }

    private static int c(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getChildCount();
    }

    private static void c(Object p0, android.graphics.Rect p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setBoundsInParent(p1);
        return;
    }

    private static void c(Object p0, android.view.View p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setSource(p1);
        return;
    }

    private static void c(Object p0, CharSequence p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setPackageName(p1);
        return;
    }

    private static void c(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setClickable(p1);
        return;
    }

    private static boolean c(Object p1, int p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).performAction(p2);
    }

    private static CharSequence d(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getClassName();
    }

    private static void d(Object p0, android.graphics.Rect p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setBoundsInScreen(p1);
        return;
    }

    private static void d(Object p0, CharSequence p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setText(p1);
        return;
    }

    private static void d(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setEnabled(p1);
        return;
    }

    private static CharSequence e(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getContentDescription();
    }

    private static void e(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setFocusable(p1);
        return;
    }

    private static CharSequence f(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getPackageName();
    }

    private static void f(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setFocused(p1);
        return;
    }

    private static Object g(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getParent();
    }

    private static void g(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setLongClickable(p1);
        return;
    }

    private static CharSequence h(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getText();
    }

    private static void h(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setPassword(p1);
        return;
    }

    private static int i(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getWindowId();
    }

    private static void i(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setScrollable(p1);
        return;
    }

    private static void j(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setSelected(p1);
        return;
    }

    private static boolean j(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isCheckable();
    }

    private static boolean k(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isChecked();
    }

    private static boolean l(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isClickable();
    }

    private static boolean m(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isEnabled();
    }

    private static boolean n(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isFocusable();
    }

    private static boolean o(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isFocused();
    }

    private static boolean p(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isLongClickable();
    }

    private static boolean q(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isPassword();
    }

    private static boolean r(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isScrollable();
    }

    private static boolean s(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isSelected();
    }

    private static void t(Object p0)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).recycle();
        return;
    }
}
