package android.support.v4.view.a;
final class bb extends android.view.accessibility.AccessibilityNodeProvider {
    final synthetic android.support.v4.view.a.bc a;

    bb(android.support.v4.view.a.bc p1)
    {
        this.a = p1;
        return;
    }

    public final android.view.accessibility.AccessibilityNodeInfo createAccessibilityNodeInfo(int p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) this.a.a(p2));
    }

    public final java.util.List findAccessibilityNodeInfosByText(String p2, int p3)
    {
        return this.a.a();
    }

    public final android.view.accessibility.AccessibilityNodeInfo findFocus(int p2)
    {
        this.a.b();
        return 0;
    }

    public final boolean performAction(int p2, int p3, android.os.Bundle p4)
    {
        return this.a.a(p2, p3, p4);
    }
}
