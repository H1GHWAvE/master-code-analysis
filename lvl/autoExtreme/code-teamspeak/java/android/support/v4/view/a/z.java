package android.support.v4.view.a;
 class z extends android.support.v4.view.a.y {

    z()
    {
        return;
    }

    public final String G(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getViewIdResourceName();
    }

    public final void a(Object p1, int p2, int p3)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setTextSelection(p2, p3);
        return;
    }

    public final int aa(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getTextSelectionStart();
    }

    public final int ab(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getTextSelectionEnd();
    }

    public final boolean ad(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isEditable();
    }

    public final boolean af(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).refresh();
    }

    public final void b(Object p1, String p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setViewIdResourceName(p2);
        return;
    }

    public final java.util.List c(Object p2, String p3)
    {
        return ((java.util.List) ((android.view.accessibility.AccessibilityNodeInfo) p2).findAccessibilityNodeInfosByViewId(p3));
    }

    public final void o(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setEditable(p2);
        return;
    }
}
