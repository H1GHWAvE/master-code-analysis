package android.support.v4.view.a;
final class af {

    af()
    {
        return;
    }

    private static Object a(int p1, int p2, int p3, int p4, boolean p5, boolean p6)
    {
        return android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo.obtain(p1, p2, p3, p4, p5, p6);
    }

    private static Object a(int p1, int p2, boolean p3, int p4)
    {
        return android.view.accessibility.AccessibilityNodeInfo$CollectionInfo.obtain(p1, p2, p3, p4);
    }

    private static Object a(int p1, CharSequence p2)
    {
        return new android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction(p1, p2);
    }

    private static java.util.List a(Object p1)
    {
        return ((java.util.List) ((android.view.accessibility.AccessibilityNodeInfo) p1).getActionList());
    }

    private static void a(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setMaxTextLength(p1);
        return;
    }

    private static void a(Object p0, CharSequence p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setError(p1);
        return;
    }

    private static void a(Object p0, Object p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).addAction(((android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction) p1));
        return;
    }

    private static boolean a(Object p1, android.view.View p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).removeChild(p2);
    }

    private static boolean a(Object p1, android.view.View p2, int p3)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).removeChild(p2, p3);
    }

    private static CharSequence b(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getError();
    }

    private static boolean b(Object p1, Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).removeAction(((android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction) p2));
    }

    private static int c(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getMaxTextLength();
    }

    private static Object d(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getWindow();
    }

    private static int e(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction) p1).getId();
    }

    private static CharSequence f(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction) p1).getLabel();
    }
}
