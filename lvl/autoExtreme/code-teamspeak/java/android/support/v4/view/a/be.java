package android.support.v4.view.a;
 class be extends android.support.v4.view.a.bi {

    be()
    {
        return;
    }

    public final Object a()
    {
        return android.view.accessibility.AccessibilityRecord.obtain();
    }

    public final Object a(Object p2)
    {
        return android.view.accessibility.AccessibilityRecord.obtain(((android.view.accessibility.AccessibilityRecord) p2));
    }

    public final void a(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setAddedCount(p2);
        return;
    }

    public final void a(Object p1, android.os.Parcelable p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setParcelableData(p2);
        return;
    }

    public final void a(Object p1, android.view.View p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setSource(p2);
        return;
    }

    public final void a(Object p1, CharSequence p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setBeforeText(p2);
        return;
    }

    public final void a(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setChecked(p2);
        return;
    }

    public final int b(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getAddedCount();
    }

    public final void b(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setCurrentItemIndex(p2);
        return;
    }

    public final void b(Object p1, CharSequence p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setClassName(p2);
        return;
    }

    public final void b(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setEnabled(p2);
        return;
    }

    public final CharSequence c(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getBeforeText();
    }

    public final void c(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setFromIndex(p2);
        return;
    }

    public final void c(Object p1, CharSequence p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setContentDescription(p2);
        return;
    }

    public final void c(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setFullScreen(p2);
        return;
    }

    public final CharSequence d(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getClassName();
    }

    public final void d(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setItemCount(p2);
        return;
    }

    public final void d(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setPassword(p2);
        return;
    }

    public final CharSequence e(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getContentDescription();
    }

    public final void e(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setRemovedCount(p2);
        return;
    }

    public final void e(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setScrollable(p2);
        return;
    }

    public final int f(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getCurrentItemIndex();
    }

    public final void f(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setScrollX(p2);
        return;
    }

    public final int g(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getFromIndex();
    }

    public final void g(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setScrollY(p2);
        return;
    }

    public final int h(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getItemCount();
    }

    public final void h(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).setToIndex(p2);
        return;
    }

    public final android.os.Parcelable i(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getParcelableData();
    }

    public final int j(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getRemovedCount();
    }

    public final int k(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getScrollX();
    }

    public final int l(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getScrollY();
    }

    public final android.support.v4.view.a.q m(Object p2)
    {
        return android.support.v4.view.a.q.a(((android.view.accessibility.AccessibilityRecord) p2).getSource());
    }

    public final java.util.List n(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getText();
    }

    public final int o(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getToIndex();
    }

    public final int p(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).getWindowId();
    }

    public final boolean q(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).isChecked();
    }

    public final boolean r(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).isEnabled();
    }

    public final boolean s(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).isFullScreen();
    }

    public final boolean t(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).isPassword();
    }

    public final boolean u(Object p2)
    {
        return ((android.view.accessibility.AccessibilityRecord) p2).isScrollable();
    }

    public final void v(Object p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p1).recycle();
        return;
    }
}
