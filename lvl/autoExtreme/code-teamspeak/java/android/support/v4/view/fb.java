package android.support.v4.view;
public final class fb {
    static final android.support.v4.view.fd a;

    static fb()
    {
        android.support.v4.view.fg v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 21) {
            if (v0_0 < 19) {
                if (v0_0 < 14) {
                    android.support.v4.view.fb.a = new android.support.v4.view.fg();
                } else {
                    android.support.v4.view.fb.a = new android.support.v4.view.fc();
                }
            } else {
                android.support.v4.view.fb.a = new android.support.v4.view.fe();
            }
        } else {
            android.support.v4.view.fb.a = new android.support.v4.view.ff();
        }
        return;
    }

    private fb()
    {
        return;
    }

    public static void a(android.view.ViewParent p1, android.view.View p2)
    {
        android.support.v4.view.fb.a.a(p1, p2);
        return;
    }

    public static void a(android.view.ViewParent p7, android.view.View p8, int p9, int p10, int p11, int p12)
    {
        android.support.v4.view.fb.a.a(p7, p8, p9, p10, p11, p12);
        return;
    }

    public static void a(android.view.ViewParent p6, android.view.View p7, int p8, int p9, int[] p10)
    {
        android.support.v4.view.fb.a.a(p6, p7, p8, p9, p10);
        return;
    }

    public static boolean a(android.view.ViewParent p1, android.view.View p2, float p3, float p4)
    {
        return android.support.v4.view.fb.a.a(p1, p2, p3, p4);
    }

    public static boolean a(android.view.ViewParent p6, android.view.View p7, float p8, float p9, boolean p10)
    {
        return android.support.v4.view.fb.a.a(p6, p7, p8, p9, p10);
    }

    public static boolean a(android.view.ViewParent p1, android.view.View p2, android.view.View p3, int p4)
    {
        return android.support.v4.view.fb.a.a(p1, p2, p3, p4);
    }

    public static boolean a(android.view.ViewParent p1, android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        return android.support.v4.view.fb.a.a(p1, p2, p3);
    }

    public static void b(android.view.ViewParent p1, android.view.View p2, android.view.View p3, int p4)
    {
        android.support.v4.view.fb.a.b(p1, p2, p3, p4);
        return;
    }

    private static void c(android.view.ViewParent p1, android.view.View p2, android.view.View p3, int p4)
    {
        android.support.v4.view.fb.a.c(p1, p2, p3, p4);
        return;
    }
}
