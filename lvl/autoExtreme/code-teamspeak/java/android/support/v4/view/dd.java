package android.support.v4.view;
 class dd extends android.support.v4.view.dc {

    dd()
    {
        return;
    }

    public final int F(android.view.View p2)
    {
        return p2.getMinimumWidth();
    }

    public final int G(android.view.View p2)
    {
        return p2.getMinimumHeight();
    }

    public void M(android.view.View p1)
    {
        p1.requestFitSystemWindows();
        return;
    }

    public final boolean Q(android.view.View p2)
    {
        return p2.getFitsSystemWindows();
    }

    public final void a(android.view.View p1, int p2, int p3, int p4, int p5)
    {
        p1.postInvalidate(p2, p3, p4, p5);
        return;
    }

    public final void a(android.view.View p1, Runnable p2)
    {
        p1.postOnAnimation(p2);
        return;
    }

    public final void a(android.view.View p2, Runnable p3, long p4)
    {
        p2.postOnAnimationDelayed(p3, p4);
        return;
    }

    public final void a(android.view.View p1, boolean p2)
    {
        p1.setHasTransientState(p2);
        return;
    }

    public final boolean a(android.view.View p2, int p3, android.os.Bundle p4)
    {
        return p2.performAccessibilityAction(p3, p4);
    }

    public final boolean c(android.view.View p2)
    {
        return p2.hasTransientState();
    }

    public final void d(android.view.View p1)
    {
        p1.postInvalidateOnAnimation();
        return;
    }

    public void d(android.view.View p2, int p3)
    {
        if (p3 == 4) {
            p3 = 2;
        }
        p2.setImportantForAccessibility(p3);
        return;
    }

    public final int e(android.view.View p2)
    {
        return p2.getImportantForAccessibility();
    }

    public final android.support.v4.view.a.aq g(android.view.View p3)
    {
        int v0_0;
        android.view.accessibility.AccessibilityNodeProvider v1 = p3.getAccessibilityNodeProvider();
        if (v1 == null) {
            v0_0 = 0;
        } else {
            v0_0 = new android.support.v4.view.a.aq(v1);
        }
        return v0_0;
    }

    public final android.view.ViewParent l(android.view.View p2)
    {
        return p2.getParentForAccessibility();
    }

    public final boolean v(android.view.View p2)
    {
        return p2.hasOverlappingRendering();
    }
}
