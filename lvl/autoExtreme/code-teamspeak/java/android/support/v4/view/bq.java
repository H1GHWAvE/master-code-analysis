package android.support.v4.view;
final class bq {

    bq()
    {
        return;
    }

    private static int a(android.view.MotionEvent p1)
    {
        return p1.getPointerCount();
    }

    private static int a(android.view.MotionEvent p1, int p2)
    {
        return p1.findPointerIndex(p2);
    }

    private static int b(android.view.MotionEvent p1, int p2)
    {
        return p1.getPointerId(p2);
    }

    private static float c(android.view.MotionEvent p1, int p2)
    {
        return p1.getX(p2);
    }

    private static float d(android.view.MotionEvent p1, int p2)
    {
        return p1.getY(p2);
    }
}
