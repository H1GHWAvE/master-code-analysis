package android.support.v4.n;
public final class i implements java.lang.Cloneable {
    private static final Object a;
    private boolean b;
    private long[] c;
    private Object[] d;
    private int e;

    static i()
    {
        android.support.v4.n.i.a = new Object();
        return;
    }

    public i()
    {
        this(0);
        return;
    }

    private i(byte p4)
    {
        this.b = 0;
        Object[] v0_1 = android.support.v4.n.f.b(10);
        long[] v1 = new long[v0_1];
        this.c = v1;
        Object[] v0_2 = new Object[v0_1];
        this.d = v0_2;
        this.e = 0;
        return;
    }

    private int a(Object p3)
    {
        if (this.b) {
            this.b();
        }
        int v0_1 = 0;
        while (v0_1 < this.e) {
            if (this.d[v0_1] != p3) {
                v0_1++;
            }
            return v0_1;
        }
        v0_1 = -1;
        return v0_1;
    }

    private android.support.v4.n.i a()
    {
        try {
            int v0_1 = ((android.support.v4.n.i) super.clone());
            try {
                v0_1.c = ((long[]) this.c.clone());
                v0_1.d = ((Object[]) this.d.clone());
            } catch (CloneNotSupportedException v1) {
            }
            return v0_1;
        } catch (int v0) {
            v0_1 = 0;
            return v0_1;
        }
    }

    private Object a(long p4)
    {
        Object v0_2;
        Object v0_1 = android.support.v4.n.f.a(this.c, this.e, p4);
        if ((v0_1 >= null) && (this.d[v0_1] != android.support.v4.n.i.a)) {
            v0_2 = this.d[v0_1];
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private void a(int p3)
    {
        if (this.d[p3] != android.support.v4.n.i.a) {
            this.d[p3] = android.support.v4.n.i.a;
            this.b = 1;
        }
        return;
    }

    private void a(int p2, Object p3)
    {
        if (this.b) {
            this.b();
        }
        this.d[p2] = p3;
        return;
    }

    private void a(long p8, Object p10)
    {
        int v0_1 = android.support.v4.n.f.a(this.c, this.e, p8);
        if (v0_1 < 0) {
            int v0_2 = (v0_1 ^ -1);
            if ((v0_2 >= this.e) || (this.d[v0_2] != android.support.v4.n.i.a)) {
                if ((this.b) && (this.e >= this.c.length)) {
                    this.b();
                    v0_2 = (android.support.v4.n.f.a(this.c, this.e, p8) ^ -1);
                }
                if (this.e >= this.c.length) {
                    Object[] v1_10 = android.support.v4.n.f.b((this.e + 1));
                    Object[] v2_5 = new long[v1_10];
                    Object[] v1_11 = new Object[v1_10];
                    System.arraycopy(this.c, 0, v2_5, 0, this.c.length);
                    System.arraycopy(this.d, 0, v1_11, 0, this.d.length);
                    this.c = v2_5;
                    this.d = v1_11;
                }
                if ((this.e - v0_2) != 0) {
                    System.arraycopy(this.c, v0_2, this.c, (v0_2 + 1), (this.e - v0_2));
                    System.arraycopy(this.d, v0_2, this.d, (v0_2 + 1), (this.e - v0_2));
                }
                this.c[v0_2] = p8;
                this.d[v0_2] = p10;
                this.e = (this.e + 1);
            } else {
                this.c[v0_2] = p8;
                this.d[v0_2] = p10;
            }
        } else {
            this.d[v0_1] = p10;
        }
        return;
    }

    private long b(int p3)
    {
        if (this.b) {
            this.b();
        }
        return this.c[p3];
    }

    private Object b(long p4)
    {
        Object v0_2;
        Object v0_1 = android.support.v4.n.f.a(this.c, this.e, p4);
        if ((v0_1 >= null) && (this.d[v0_1] != android.support.v4.n.i.a)) {
            v0_2 = this.d[v0_1];
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private void b()
    {
        int v3 = this.e;
        long[] v4 = this.c;
        Object[] v5 = this.d;
        int v1 = 0;
        int v0 = 0;
        while (v1 < v3) {
            int v6_0 = v5[v1];
            if (v6_0 != android.support.v4.n.i.a) {
                if (v1 != v0) {
                    v4[v0] = v4[v1];
                    v5[v0] = v6_0;
                    v5[v1] = 0;
                }
                v0++;
            }
            v1++;
        }
        this.b = 0;
        this.e = v0;
        return;
    }

    private void b(long p8, Object p10)
    {
        if ((this.e == 0) || (p8 > this.c[(this.e - 1)])) {
            if ((this.b) && (this.e >= this.c.length)) {
                this.b();
            }
            int v0_6 = this.e;
            if (v0_6 >= this.c.length) {
                Object[] v1_7 = android.support.v4.n.f.b((v0_6 + 1));
                Object[] v2_0 = new long[v1_7];
                Object[] v1_8 = new Object[v1_7];
                System.arraycopy(this.c, 0, v2_0, 0, this.c.length);
                System.arraycopy(this.d, 0, v1_8, 0, this.d.length);
                this.c = v2_0;
                this.d = v1_8;
            }
            this.c[v0_6] = p8;
            this.d[v0_6] = p10;
            this.e = (v0_6 + 1);
        } else {
            int v0_9 = android.support.v4.n.f.a(this.c, this.e, p8);
            if (v0_9 < 0) {
                int v0_10 = (v0_9 ^ -1);
                if ((v0_10 >= this.e) || (this.d[v0_10] != android.support.v4.n.i.a)) {
                    if ((this.b) && (this.e >= this.c.length)) {
                        this.b();
                        v0_10 = (android.support.v4.n.f.a(this.c, this.e, p8) ^ -1);
                    }
                    if (this.e >= this.c.length) {
                        Object[] v1_21 = android.support.v4.n.f.b((this.e + 1));
                        Object[] v2_6 = new long[v1_21];
                        Object[] v1_22 = new Object[v1_21];
                        System.arraycopy(this.c, 0, v2_6, 0, this.c.length);
                        System.arraycopy(this.d, 0, v1_22, 0, this.d.length);
                        this.c = v2_6;
                        this.d = v1_22;
                    }
                    if ((this.e - v0_10) != 0) {
                        System.arraycopy(this.c, v0_10, this.c, (v0_10 + 1), (this.e - v0_10));
                        System.arraycopy(this.d, v0_10, this.d, (v0_10 + 1), (this.e - v0_10));
                    }
                    this.c[v0_10] = p8;
                    this.d[v0_10] = p10;
                    this.e = (this.e + 1);
                } else {
                    this.c[v0_10] = p8;
                    this.d[v0_10] = p10;
                }
            } else {
                this.d[v0_9] = p10;
            }
        }
        return;
    }

    private int c()
    {
        if (this.b) {
            this.b();
        }
        return this.e;
    }

    private Object c(int p2)
    {
        if (this.b) {
            this.b();
        }
        return this.d[p2];
    }

    private void c(long p4)
    {
        int v0_1 = android.support.v4.n.f.a(this.c, this.e, p4);
        if ((v0_1 >= 0) && (this.d[v0_1] != android.support.v4.n.i.a)) {
            this.d[v0_1] = android.support.v4.n.i.a;
            this.b = 1;
        }
        return;
    }

    private void d()
    {
        int v2 = this.e;
        int v0 = 0;
        while (v0 < v2) {
            this.d[v0] = 0;
            v0++;
        }
        this.e = 0;
        this.b = 0;
        return;
    }

    private void d(long p4)
    {
        int v0_1 = android.support.v4.n.f.a(this.c, this.e, p4);
        if ((v0_1 >= 0) && (this.d[v0_1] != android.support.v4.n.i.a)) {
            this.d[v0_1] = android.support.v4.n.i.a;
            this.b = 1;
        }
        return;
    }

    private int e(long p4)
    {
        if (this.b) {
            this.b();
        }
        return android.support.v4.n.f.a(this.c, this.e, p4);
    }

    public final synthetic Object clone()
    {
        return this.a();
    }

    public final String toString()
    {
        if (this.b) {
            this.b();
        }
        int v0_7;
        if (this.e > 0) {
            StringBuilder v1_1 = new StringBuilder((this.e * 28));
            v1_1.append(123);
            int v0_5 = 0;
            while (v0_5 < this.e) {
                if (v0_5 > 0) {
                    v1_1.append(", ");
                }
                v1_1.append(this.b(v0_5));
                v1_1.append(61);
                String v2_4 = this.c(v0_5);
                if (v2_4 == this) {
                    v1_1.append("(this Map)");
                } else {
                    v1_1.append(v2_4);
                }
                v0_5++;
            }
            v1_1.append(125);
            v0_7 = v1_1.toString();
        } else {
            v0_7 = "{}";
        }
        return v0_7;
    }
}
