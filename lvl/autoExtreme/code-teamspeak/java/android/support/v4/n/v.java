package android.support.v4.n;
public class v {
    private static final boolean a = False;
    static Object[] b = None;
    static int c = 0;
    static Object[] d = None;
    static int e = 0;
    private static final String i = "ArrayMap";
    private static final int j = 4;
    private static final int k = 10;
    int[] f;
    Object[] g;
    int h;

    public v()
    {
        this.f = android.support.v4.n.f.a;
        this.g = android.support.v4.n.f.c;
        this.h = 0;
        return;
    }

    public v(int p2)
    {
        if (p2 != 0) {
            this.e(p2);
        } else {
            this.f = android.support.v4.n.f.a;
            this.g = android.support.v4.n.f.c;
        }
        this.h = 0;
        return;
    }

    public v(android.support.v4.n.v p6)
    {
        int v0 = 0;
        if (p6 != null) {
            int v1 = p6.h;
            this.a((this.h + v1));
            if (this.h == 0) {
                if (v1 > 0) {
                    System.arraycopy(p6.f, 0, this.f, 0, v1);
                    System.arraycopy(p6.g, 0, this.g, 0, (v1 << 1));
                    this.h = v1;
                }
                return;
            }
            while (v0 < v1) {
                this.put(p6.b(v0), p6.c(v0));
                v0++;
            }
        }
        return;
    }

    private int a()
    {
        int v0_1;
        Object v2_0 = this.h;
        if (v2_0 != null) {
            v0_1 = android.support.v4.n.f.a(this.f, v2_0, 0);
            if ((v0_1 >= 0) && (this.g[(v0_1 << 1)] != null)) {
                int v1_3 = (v0_1 + 1);
                while ((v1_3 < v2_0) && (this.f[v1_3] == 0)) {
                    if (this.g[(v1_3 << 1)] != null) {
                        v1_3++;
                    } else {
                        v0_1 = v1_3;
                    }
                }
                v0_1--;
                while ((v0_1 >= 0) && (this.f[v0_1] == 0)) {
                    if (this.g[(v0_1 << 1)] != null) {
                        v0_1--;
                    }
                }
                v0_1 = (v1_3 ^ -1);
            }
        } else {
            v0_1 = -1;
        }
        return v0_1;
    }

    private int a(Object p6, int p7)
    {
        int v0_1;
        boolean v2_0 = this.h;
        if (v2_0) {
            v0_1 = android.support.v4.n.f.a(this.f, v2_0, p7);
            if ((v0_1 >= 0) && (!p6.equals(this.g[(v0_1 << 1)]))) {
                int v1_3 = (v0_1 + 1);
                while ((v1_3 < v2_0) && (this.f[v1_3] == p7)) {
                    if (!p6.equals(this.g[(v1_3 << 1)])) {
                        v1_3++;
                    } else {
                        v0_1 = v1_3;
                    }
                }
                v0_1--;
                while ((v0_1 >= 0) && (this.f[v0_1] == p7)) {
                    if (!p6.equals(this.g[(v0_1 << 1)])) {
                        v0_1--;
                    }
                }
                v0_1 = (v1_3 ^ -1);
            }
        } else {
            v0_1 = -1;
        }
        return v0_1;
    }

    private void a(android.support.v4.n.v p6)
    {
        int v0 = 0;
        int v1 = p6.h;
        this.a((this.h + v1));
        if (this.h != 0) {
            while (v0 < v1) {
                this.put(p6.b(v0), p6.c(v0));
                v0++;
            }
        } else {
            if (v1 > 0) {
                System.arraycopy(p6.f, 0, this.f, 0, v1);
                System.arraycopy(p6.g, 0, this.g, 0, (v1 << 1));
                this.h = v1;
            }
        }
        return;
    }

    private static void a(int[] p4, Object[] p5, int p6)
    {
        try {
            if (p4.length != 8) {
                if (p4.length == 4) {
                    try {
                        if (android.support.v4.n.v.c < 10) {
                            p5[0] = android.support.v4.n.v.b;
                            p5[1] = p4;
                            int v0_6 = ((p6 << 1) - 1);
                            while (v0_6 >= 2) {
                                p5[v0_6] = 0;
                                v0_6--;
                            }
                            android.support.v4.n.v.b = p5;
                            android.support.v4.n.v.c = (android.support.v4.n.v.c + 1);
                        }
                    } catch (int v0_9) {
                        throw v0_9;
                    }
                }
            } else {
                if (android.support.v4.n.v.e < 10) {
                    p5[0] = android.support.v4.n.v.d;
                    p5[1] = p4;
                    int v0_14 = ((p6 << 1) - 1);
                    while (v0_14 >= 2) {
                        p5[v0_14] = 0;
                        v0_14--;
                    }
                    android.support.v4.n.v.d = p5;
                    android.support.v4.n.v.e = (android.support.v4.n.v.e + 1);
                }
            }
        } catch (int v0_17) {
            throw v0_17;
        }
        return;
    }

    private void e(int p6)
    {
        if (p6 != 8) {
            if (p6 != 4) {
                int v0_16 = new int[p6];
                this.f = v0_16;
                int v0_18 = new Object[(p6 << 1)];
                this.g = v0_18;
            } else {
                try {
                    if (android.support.v4.n.v.b == null) {
                    } else {
                        Object[] v2_0 = android.support.v4.n.v.b;
                        this.g = v2_0;
                        android.support.v4.n.v.b = ((Object[]) ((Object[]) v2_0[0]));
                        this.f = ((int[]) ((int[]) v2_0[1]));
                        v2_0[1] = 0;
                        v2_0[0] = 0;
                        android.support.v4.n.v.c = (android.support.v4.n.v.c - 1);
                    }
                } catch (int v0_14) {
                    throw v0_14;
                }
            }
        } else {
            if (android.support.v4.n.v.d == null) {
            } else {
                Object[] v2_1 = android.support.v4.n.v.d;
                this.g = v2_1;
                android.support.v4.n.v.d = ((Object[]) ((Object[]) v2_1[0]));
                this.f = ((int[]) ((int[]) v2_1[1]));
                v2_1[1] = 0;
                v2_1[0] = 0;
                android.support.v4.n.v.e = (android.support.v4.n.v.e - 1);
            }
        }
        return;
    }

    public final int a(Object p2)
    {
        int v0_1;
        if (p2 != null) {
            v0_1 = this.a(p2, p2.hashCode());
        } else {
            v0_1 = this.a();
        }
        return v0_1;
    }

    public final Object a(int p4, Object p5)
    {
        int v0_1 = ((p4 << 1) + 1);
        Object v1_1 = this.g[v0_1];
        this.g[v0_1] = p5;
        return v1_1;
    }

    public final void a(int p6)
    {
        if (this.f.length < p6) {
            int[] v0_2 = this.f;
            Object[] v1 = this.g;
            this.e(p6);
            if (this.h > 0) {
                System.arraycopy(v0_2, 0, this.f, 0, this.h);
                System.arraycopy(v1, 0, this.g, 0, (this.h << 1));
            }
            android.support.v4.n.v.a(v0_2, v1, this.h);
        }
        return;
    }

    final int b(Object p5)
    {
        int v0_1;
        int v0_0 = 1;
        int v1_1 = (this.h * 2);
        Object[] v2 = this.g;
        if (p5 != null) {
            while (v0_0 < v1_1) {
                if (!p5.equals(v2[v0_0])) {
                    v0_0 += 2;
                } else {
                    v0_1 = (v0_0 >> 1);
                }
            }
            v0_1 = -1;
        } else {
            while (v0_0 < v1_1) {
                if (v2[v0_0] != null) {
                    v0_0 += 2;
                } else {
                    v0_1 = (v0_0 >> 1);
                }
            }
        }
        return v0_1;
    }

    public final Object b(int p3)
    {
        return this.g[(p3 << 1)];
    }

    public final Object c(int p3)
    {
        return this.g[((p3 << 1) + 1)];
    }

    public void clear()
    {
        if (this.h != 0) {
            android.support.v4.n.v.a(this.f, this.g, this.h);
            this.f = android.support.v4.n.f.a;
            this.g = android.support.v4.n.f.c;
            this.h = 0;
        }
        return;
    }

    public boolean containsKey(Object p2)
    {
        int v0_1;
        if (this.a(p2) < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean containsValue(Object p2)
    {
        int v0_1;
        if (this.b(p2) < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final Object d(int p8)
    {
        int v0_0 = 8;
        Object v1_1 = this.g[((p8 << 1) + 1)];
        if (this.h > 1) {
            if ((this.f.length <= 8) || (this.h >= (this.f.length / 3))) {
                this.h = (this.h - 1);
                if (p8 < this.h) {
                    System.arraycopy(this.f, (p8 + 1), this.f, p8, (this.h - p8));
                    System.arraycopy(this.g, ((p8 + 1) << 1), this.g, (p8 << 1), ((this.h - p8) << 1));
                }
                this.g[(this.h << 1)] = 0;
                this.g[((this.h << 1) + 1)] = 0;
            } else {
                if (this.h > 8) {
                    v0_0 = (this.h + (this.h >> 1));
                }
                Object[] v2_17 = this.f;
                Object[] v3_6 = this.g;
                this.e(v0_0);
                this.h = (this.h - 1);
                if (p8 > 0) {
                    System.arraycopy(v2_17, 0, this.f, 0, p8);
                    System.arraycopy(v3_6, 0, this.g, 0, (p8 << 1));
                }
                if (p8 < this.h) {
                    System.arraycopy(v2_17, (p8 + 1), this.f, p8, (this.h - p8));
                    System.arraycopy(v3_6, ((p8 + 1) << 1), this.g, (p8 << 1), ((this.h - p8) << 1));
                }
            }
        } else {
            android.support.v4.n.v.a(this.f, this.g, this.h);
            this.f = android.support.v4.n.f.a;
            this.g = android.support.v4.n.f.c;
            this.h = 0;
        }
        return v1_1;
    }

    public boolean equals(Object p7)
    {
        int v0 = 1;
        if (this != p7) {
            if (!(p7 instanceof java.util.Map)) {
                v0 = 0;
            } else {
                if (this.size() == ((java.util.Map) p7).size()) {
                    int v2_2 = 0;
                    try {
                        while (v2_2 < this.h) {
                            boolean v3_2 = this.b(v2_2);
                            Object v4 = this.c(v2_2);
                            Object v5 = ((java.util.Map) p7).get(v3_2);
                            if (v4 != null) {
                                if (!v4.equals(v5)) {
                                    v0 = 0;
                                    break;
                                }
                            } else {
                                if ((v5 != null) || (!((java.util.Map) p7).containsKey(v3_2))) {
                                    v0 = 0;
                                    break;
                                }
                            }
                            v2_2++;
                        }
                    } catch (int v0) {
                        v0 = 0;
                    } catch (int v0) {
                        v0 = 0;
                    }
                } else {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public Object get(Object p3)
    {
        int v0_1;
        int v0_0 = this.a(p3);
        if (v0_0 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = this.g[((v0_0 << 1) + 1)];
        }
        return v0_1;
    }

    public int hashCode()
    {
        int[] v5 = this.f;
        Object[] v6 = this.g;
        int v7 = this.h;
        int v2 = 1;
        int v3 = 0;
        int v4 = 0;
        while (v3 < v7) {
            int v0_2;
            int v0_1 = v6[v2];
            int v8 = v5[v3];
            if (v0_1 != 0) {
                v0_2 = v0_1.hashCode();
            } else {
                v0_2 = 0;
            }
            v4 += (v0_2 ^ v8);
            v3++;
            v2 += 2;
        }
        return v4;
    }

    public boolean isEmpty()
    {
        int v0_1;
        if (this.h > 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public Object put(Object p8, Object p9)
    {
        int v2_0;
        int v3;
        int v0_0 = 8;
        if (p8 != null) {
            v3 = p8.hashCode();
            v2_0 = this.a(p8, v3);
        } else {
            v2_0 = this.a();
            v3 = 0;
        }
        int v0_15;
        if (v2_0 < 0) {
            int v2_1 = (v2_0 ^ -1);
            if (this.h >= this.f.length) {
                if (this.h < 8) {
                    if (this.h < 4) {
                        v0_0 = 4;
                    }
                } else {
                    v0_0 = (this.h + (this.h >> 1));
                }
                int[] v1_3 = this.f;
                Object[] v5_3 = this.g;
                this.e(v0_0);
                if (this.f.length > 0) {
                    System.arraycopy(v1_3, 0, this.f, 0, v1_3.length);
                    System.arraycopy(v5_3, 0, this.g, 0, v5_3.length);
                }
                android.support.v4.n.v.a(v1_3, v5_3, this.h);
            }
            if (v2_1 < this.h) {
                System.arraycopy(this.f, v2_1, this.f, (v2_1 + 1), (this.h - v2_1));
                System.arraycopy(this.g, (v2_1 << 1), this.g, ((v2_1 + 1) << 1), ((this.h - v2_1) << 1));
            }
            this.f[v2_1] = v3;
            this.g[(v2_1 << 1)] = p8;
            this.g[((v2_1 << 1) + 1)] = p9;
            this.h = (this.h + 1);
            v0_15 = 0;
        } else {
            int[] v1_9 = ((v2_0 << 1) + 1);
            v0_15 = this.g[v1_9];
            this.g[v1_9] = p9;
        }
        return v0_15;
    }

    public Object remove(Object p2)
    {
        int v0_1;
        int v0_0 = this.a(p2);
        if (v0_0 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = this.d(v0_0);
        }
        return v0_1;
    }

    public int size()
    {
        return this.h;
    }

    public String toString()
    {
        int v0_6;
        if (!this.isEmpty()) {
            StringBuilder v1_1 = new StringBuilder((this.h * 28));
            v1_1.append(123);
            int v0_4 = 0;
            while (v0_4 < this.h) {
                if (v0_4 > 0) {
                    v1_1.append(", ");
                }
                String v2_2 = this.b(v0_4);
                if (v2_2 == this) {
                    v1_1.append("(this Map)");
                } else {
                    v1_1.append(v2_2);
                }
                v1_1.append(61);
                String v2_5 = this.c(v0_4);
                if (v2_5 == this) {
                    v1_1.append("(this Map)");
                } else {
                    v1_1.append(v2_5);
                }
                v0_4++;
            }
            v1_1.append(125);
            v0_6 = v1_1.toString();
        } else {
            v0_6 = "{}";
        }
        return v0_6;
    }
}
