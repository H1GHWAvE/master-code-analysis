package android.support.v4.n;
final class n implements java.util.Set {
    final synthetic android.support.v4.n.k a;

    n(android.support.v4.n.k p1)
    {
        this.a = p1;
        return;
    }

    public final boolean add(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean addAll(java.util.Collection p2)
    {
        throw new UnsupportedOperationException();
    }

    public final void clear()
    {
        this.a.c();
        return;
    }

    public final boolean contains(Object p2)
    {
        int v0_2;
        if (this.a.a(p2) < 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean containsAll(java.util.Collection p2)
    {
        return android.support.v4.n.k.a(this.a.b(), p2);
    }

    public final boolean equals(Object p2)
    {
        return android.support.v4.n.k.a(this, p2);
    }

    public final int hashCode()
    {
        int v2 = (this.a.a() - 1);
        int v3 = 0;
        while (v2 >= 0) {
            int v0_5;
            int v0_4 = this.a.a(v2, 0);
            if (v0_4 != 0) {
                v0_5 = v0_4.hashCode();
            } else {
                v0_5 = 0;
            }
            v3 += v0_5;
            v2--;
        }
        return v3;
    }

    public final boolean isEmpty()
    {
        int v0_2;
        if (this.a.a() != 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final java.util.Iterator iterator()
    {
        return new android.support.v4.n.l(this.a, 0);
    }

    public final boolean remove(Object p3)
    {
        int v0_2;
        int v0_1 = this.a.a(p3);
        if (v0_1 < 0) {
            v0_2 = 0;
        } else {
            this.a.a(v0_1);
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean removeAll(java.util.Collection p2)
    {
        return android.support.v4.n.k.b(this.a.b(), p2);
    }

    public final boolean retainAll(java.util.Collection p2)
    {
        return android.support.v4.n.k.c(this.a.b(), p2);
    }

    public final int size()
    {
        return this.a.a();
    }

    public final Object[] toArray()
    {
        return this.a.b(0);
    }

    public final Object[] toArray(Object[] p3)
    {
        return this.a.a(p3, 0);
    }
}
