package android.support.v4.n;
public class t implements android.support.v4.n.s {
    private final Object[] a;
    private int b;

    public t(int p3)
    {
        if (p3 > 0) {
            Object[] v0_0 = new Object[p3];
            this.a = v0_0;
            return;
        } else {
            throw new IllegalArgumentException("The max pool size must be > 0");
        }
    }

    private boolean b(Object p4)
    {
        int v1 = 0;
        int v0 = 0;
        while (v0 < this.b) {
            if (this.a[v0] != p4) {
                v0++;
            } else {
                v1 = 1;
                break;
            }
        }
        return v1;
    }

    public Object a()
    {
        int v0_1;
        if (this.b <= 0) {
            v0_1 = 0;
        } else {
            int v2 = (this.b - 1);
            v0_1 = this.a[v2];
            this.a[v2] = 0;
            this.b = (this.b - 1);
        }
        return v0_1;
    }

    public boolean a(Object p5)
    {
        int v1_0 = 0;
        int v0_0 = 0;
        while (v0_0 < this.b) {
            if (this.a[v0_0] != p5) {
                v0_0++;
            } else {
                int v0_1 = 1;
            }
            if (v0_1 == 0) {
                if (this.b < this.a.length) {
                    this.a[this.b] = p5;
                    this.b = (this.b + 1);
                    v1_0 = 1;
                }
                return v1_0;
            } else {
                throw new IllegalStateException("Already in the pool!");
            }
        }
        v0_1 = 0;
    }
}
