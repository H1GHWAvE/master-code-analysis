package android.support.v4.widget;
final class c {
    int a;
    int b;
    float c;
    float d;
    long e;
    long f;
    int g;
    int h;
    long i;
    float j;
    int k;

    public c()
    {
        this.e = -0.0;
        this.i = -1;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        return;
    }

    private static float a(float p2)
    {
        return (((-1065353216 * p2) * p2) + (1082130432 * p2));
    }

    private void a()
    {
        this.e = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
        this.i = -1;
        this.f = this.e;
        this.j = 1056964608;
        this.g = 0;
        this.h = 0;
        return;
    }

    private void a(float p1, float p2)
    {
        this.c = p1;
        this.d = p2;
        return;
    }

    private void a(int p1)
    {
        this.a = p1;
        return;
    }

    private void b()
    {
        long v0 = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
        this.k = android.support.v4.widget.a.a(((int) (v0 - this.e)), this.b);
        this.j = this.a(v0);
        this.i = v0;
        return;
    }

    private void b(int p1)
    {
        this.b = p1;
        return;
    }

    private boolean c()
    {
        if ((this.i <= 0) || (android.view.animation.AnimationUtils.currentAnimationTimeMillis() <= (this.i + ((long) this.k)))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    private void d()
    {
        if (this.f != 0) {
            int v0_2 = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
            float v2_1 = this.a(v0_2);
            float v2_3 = ((v2_1 * 1082130432) + ((-1065353216 * v2_1) * v2_1));
            long v4_2 = (v0_2 - this.f);
            this.f = v0_2;
            this.g = ((int) ((((float) v4_2) * v2_3) * this.c));
            this.h = ((int) ((((float) v4_2) * v2_3) * this.d));
            return;
        } else {
            throw new RuntimeException("Cannot compute scroll delta before calling start()");
        }
    }

    private int e()
    {
        return ((int) (this.c / Math.abs(this.c)));
    }

    private int f()
    {
        return ((int) (this.d / Math.abs(this.d)));
    }

    private int g()
    {
        return this.g;
    }

    private int h()
    {
        return this.h;
    }

    final float a(long p6)
    {
        float v0_12;
        if (p6 >= this.e) {
            if ((this.i >= 0) && (p6 >= this.i)) {
                v0_12 = ((android.support.v4.widget.a.a((((float) (p6 - this.i)) / ((float) this.k))) * this.j) + (1065353216 - this.j));
            } else {
                v0_12 = (android.support.v4.widget.a.a((((float) (p6 - this.e)) / ((float) this.a))) * 1056964608);
            }
        } else {
            v0_12 = 0;
        }
        return v0_12;
    }
}
