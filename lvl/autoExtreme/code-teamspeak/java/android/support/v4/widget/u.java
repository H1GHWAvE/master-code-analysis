package android.support.v4.widget;
final class u extends android.database.DataSetObserver {
    final synthetic android.support.v4.widget.r a;

    private u(android.support.v4.widget.r p1)
    {
        this.a = p1;
        return;
    }

    synthetic u(android.support.v4.widget.r p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void onChanged()
    {
        this.a.a = 1;
        this.a.notifyDataSetChanged();
        return;
    }

    public final void onInvalidated()
    {
        this.a.a = 0;
        this.a.notifyDataSetInvalidated();
        return;
    }
}
