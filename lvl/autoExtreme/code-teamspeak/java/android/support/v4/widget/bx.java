package android.support.v4.widget;
final class bx {
    static reflect.Method a;
    static boolean b;
    private static reflect.Method c;
    private static boolean d;

    bx()
    {
        return;
    }

    static int a(android.widget.PopupWindow p5)
    {
        if (!android.support.v4.widget.bx.d) {
            try {
                Class[] v3_1 = new Class[0];
                Exception v0_2 = android.widget.PopupWindow.getDeclaredMethod("getWindowLayoutType", v3_1);
                android.support.v4.widget.bx.c = v0_2;
                v0_2.setAccessible(1);
            } catch (Exception v0) {
            }
            android.support.v4.widget.bx.d = 1;
        }
        Exception v0_7;
        if (android.support.v4.widget.bx.c == null) {
            v0_7 = 0;
        } else {
            try {
                Object[] v2_3 = new Object[0];
                v0_7 = ((Integer) android.support.v4.widget.bx.c.invoke(p5, v2_3)).intValue();
            } catch (Exception v0) {
            }
        }
        return v0_7;
    }

    private static void a(android.widget.PopupWindow p6, int p7)
    {
        if (!android.support.v4.widget.bx.b) {
            try {
                int v2_1 = new Class[1];
                v2_1[0] = Integer.TYPE;
                Exception v0_2 = android.widget.PopupWindow.getDeclaredMethod("setWindowLayoutType", v2_1);
                android.support.v4.widget.bx.a = v0_2;
                v0_2.setAccessible(1);
            } catch (Exception v0) {
            }
            android.support.v4.widget.bx.b = 1;
        }
        if (android.support.v4.widget.bx.a != null) {
            try {
                Object[] v1_3 = new Object[1];
                v1_3[0] = Integer.valueOf(p7);
                android.support.v4.widget.bx.a.invoke(p6, v1_3);
            } catch (Exception v0) {
            }
        }
        return;
    }
}
