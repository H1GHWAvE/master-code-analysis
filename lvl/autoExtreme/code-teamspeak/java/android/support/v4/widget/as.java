package android.support.v4.widget;
public abstract class as extends android.support.v4.view.a {
    public static final int a = 2147483648;
    public static final int c = 255;
    private static final String d;
    private final android.graphics.Rect e;
    private final android.graphics.Rect f;
    private final android.graphics.Rect g;
    private final int[] h;
    private final android.view.accessibility.AccessibilityManager i;
    private final android.view.View j;
    private android.support.v4.widget.au k;
    private int l;
    private int m;

    static as()
    {
        android.support.v4.widget.as.d = android.view.View.getName();
        return;
    }

    private as(android.view.View p3)
    {
        this.e = new android.graphics.Rect();
        this.f = new android.graphics.Rect();
        this.g = new android.graphics.Rect();
        android.view.accessibility.AccessibilityManager v0_7 = new int[2];
        this.h = v0_7;
        this.l = -2147483648;
        this.m = -2147483648;
        if (p3 != null) {
            this.j = p3;
            this.i = ((android.view.accessibility.AccessibilityManager) p3.getContext().getSystemService("accessibility"));
            return;
        } else {
            throw new IllegalArgumentException("View may not be null");
        }
    }

    static synthetic android.support.v4.view.a.q a(android.support.v4.widget.as p6, int p7)
    {
        RuntimeException v0_5;
        switch (p7) {
            case -1:
                android.graphics.Rect v1_0 = android.support.v4.view.a.q.a(p6.j);
                android.support.v4.view.cx.a(p6.j, v1_0);
                int v2_0 = new java.util.LinkedList().iterator();
                while (v2_0.hasNext()) {
                    android.support.v4.view.a.q.a.e(v1_0.b, p6.j, ((Integer) v2_0.next()).intValue());
                }
                v0_5 = v1_0;
                break;
            default:
                v0_5 = android.support.v4.view.a.q.a();
                v0_5.h(1);
                v0_5.b(android.support.v4.widget.as.d);
                if ((v0_5.m() != null) || (v0_5.n() != null)) {
                    v0_5.a(p6.f);
                    if (!p6.f.isEmpty()) {
                        android.graphics.Rect v1_7 = v0_5.b();
                        if ((v1_7 & 64) == 0) {
                            if ((v1_7 & 128) == 0) {
                                v0_5.a(p6.j.getContext().getPackageName());
                                android.support.v4.view.a.q.a.d(v0_5.b, p6.j, p7);
                                v0_5.d(p6.j);
                                if (p6.l != p7) {
                                    v0_5.d(0);
                                    v0_5.a(64);
                                } else {
                                    v0_5.d(1);
                                    v0_5.a(128);
                                }
                                if (p6.a(p6.f)) {
                                    v0_5.c(1);
                                    v0_5.b(p6.f);
                                }
                                p6.j.getLocationOnScreen(p6.h);
                                android.graphics.Rect v1_22 = p6.h[0];
                                int v2_5 = p6.h[1];
                                p6.e.set(p6.f);
                                p6.e.offset(v1_22, v2_5);
                                v0_5.d(p6.e);
                            } else {
                                throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
                            }
                        } else {
                            throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
                        }
                    } else {
                        throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
                    }
                } else {
                    throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
                }
        }
        return v0_5;
    }

    private void a(int p3)
    {
        if (this.m != p3) {
            int v0_1 = this.m;
            this.m = p3;
            this.a(p3, 128);
            this.a(v0_1, 256);
        }
        return;
    }

    private boolean a(int p6, int p7)
    {
        RuntimeException v0_0 = 0;
        if ((p6 != -2147483648) && (this.i.isEnabled())) {
            String v1_4 = this.j.getParent();
            if (v1_4 != null) {
                RuntimeException v0_1;
                switch (p6) {
                    case -1:
                        v0_1 = android.view.accessibility.AccessibilityEvent.obtain(p7);
                        android.support.v4.view.cx.a(this.j, v0_1);
                        break;
                    default:
                        v0_1 = android.view.accessibility.AccessibilityEvent.obtain(p7);
                        v0_1.setEnabled(1);
                        v0_1.setClassName(android.support.v4.widget.as.d);
                        if ((!v0_1.getText().isEmpty()) || (v0_1.getContentDescription() != null)) {
                            v0_1.setPackageName(this.j.getContext().getPackageName());
                            android.support.v4.view.a.bd.a.a(android.support.v4.view.a.a.a(v0_1).b, this.j, p6);
                        } else {
                            throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
                        }
                }
                v0_0 = android.support.v4.view.fb.a(v1_4, this.j, v0_1);
            }
        }
        return v0_0;
    }

    private boolean a(int p6, int p7, android.os.Bundle p8)
    {
        int v0_0 = 1;
        switch (p6) {
            case -1:
                v0_0 = android.support.v4.view.cx.a(this.j, p7, p8);
                break;
            default:
                switch (p7) {
                    case 64:
                    case 128:
                        switch (p7) {
                            case 64:
                                if ((this.i.isEnabled()) && (android.support.v4.view.a.h.a(this.i))) {
                                    if (this.e(p6)) {
                                        v0_0 = 0;
                                    } else {
                                        if (this.l != -2147483648) {
                                            this.a(this.l, 65536);
                                        }
                                        this.l = p6;
                                        this.j.invalidate();
                                        this.a(p6, 32768);
                                    }
                                } else {
                                    v0_0 = 0;
                                }
                                break;
                            case 128:
                                if (!this.e(p6)) {
                                    v0_0 = 0;
                                } else {
                                    this.l = -2147483648;
                                    this.j.invalidate();
                                    this.a(p6, 65536);
                                }
                                break;
                            default:
                                v0_0 = 0;
                        }
                        break;
                    default:
                        v0_0 = this.e();
                }
        }
        return v0_0;
    }

    private boolean a(int p2, android.os.Bundle p3)
    {
        return android.support.v4.view.cx.a(this.j, p2, p3);
    }

    private boolean a(android.graphics.Rect p5)
    {
        if ((p5 != null) && (!p5.isEmpty())) {
            if (this.j.getWindowVisibility() == 0) {
                boolean v0_4 = this.j.getParent();
                while ((v0_4 instanceof android.view.View)) {
                    boolean v0_9 = ((android.view.View) v0_4);
                    if ((android.support.v4.view.cx.d(v0_9) > 0) && (v0_9.getVisibility() == 0)) {
                        v0_4 = v0_9.getParent();
                    } else {
                        boolean v0_8 = 0;
                    }
                }
                if (v0_4) {
                    if (this.j.getLocalVisibleRect(this.g)) {
                        v0_8 = p5.intersect(this.g);
                    } else {
                        v0_8 = 0;
                    }
                } else {
                    v0_8 = 0;
                }
            } else {
                v0_8 = 0;
            }
        } else {
            v0_8 = 0;
        }
        return v0_8;
    }

    static synthetic boolean a(android.support.v4.widget.as p5, int p6, int p7, android.os.Bundle p8)
    {
        int v0_0 = 1;
        switch (p6) {
            case -1:
                v0_0 = android.support.v4.view.cx.a(p5.j, p7, p8);
                break;
            default:
                switch (p7) {
                    case 64:
                    case 128:
                        switch (p7) {
                            case 64:
                                if ((p5.i.isEnabled()) && (android.support.v4.view.a.h.a(p5.i))) {
                                    if (p5.e(p6)) {
                                        v0_0 = 0;
                                    } else {
                                        if (p5.l != -2147483648) {
                                            p5.a(p5.l, 65536);
                                        }
                                        p5.l = p6;
                                        p5.j.invalidate();
                                        p5.a(p6, 32768);
                                    }
                                } else {
                                    v0_0 = 0;
                                }
                                break;
                            case 128:
                                if (!p5.e(p6)) {
                                    v0_0 = 0;
                                } else {
                                    p5.l = -2147483648;
                                    p5.j.invalidate();
                                    p5.a(p6, 65536);
                                }
                                break;
                            default:
                                v0_0 = 0;
                        }
                        break;
                    default:
                        v0_0 = p5.e();
                }
        }
        return v0_0;
    }

    private boolean a(android.view.MotionEvent p5)
    {
        int v0 = 1;
        if ((this.i.isEnabled()) && (android.support.v4.view.a.h.a(this.i))) {
            switch (p5.getAction()) {
                case 7:
                case 9:
                    p5.getX();
                    p5.getY();
                    int v2_6 = this.a();
                    this.a(v2_6);
                    if (v2_6 == -2147483648) {
                        v0 = 0;
                    }
                    break;
                case 8:
                default:
                    v0 = 0;
                    break;
                case 10:
                    if (this.l == -2147483648) {
                        v0 = 0;
                    } else {
                        this.a(-2147483648);
                    }
                    break;
            }
        } else {
            v0 = 0;
        }
        return v0;
    }

    private android.view.accessibility.AccessibilityEvent b(int p3)
    {
        android.view.accessibility.AccessibilityEvent v0 = android.view.accessibility.AccessibilityEvent.obtain(p3);
        android.support.v4.view.cx.a(this.j, v0);
        return v0;
    }

    private android.view.accessibility.AccessibilityEvent b(int p5, int p6)
    {
        RuntimeException v0_0;
        switch (p5) {
            case -1:
                v0_0 = android.view.accessibility.AccessibilityEvent.obtain(p6);
                android.support.v4.view.cx.a(this.j, v0_0);
                break;
            default:
                v0_0 = android.view.accessibility.AccessibilityEvent.obtain(p6);
                v0_0.setEnabled(1);
                v0_0.setClassName(android.support.v4.widget.as.d);
                if ((!v0_0.getText().isEmpty()) || (v0_0.getContentDescription() != null)) {
                    v0_0.setPackageName(this.j.getContext().getPackageName());
                    android.support.v4.view.a.bd.a.a(android.support.v4.view.a.a.a(v0_0).b, this.j, p5);
                } else {
                    throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
                }
        }
        return v0_0;
    }

    private android.support.v4.view.a.q c(int p7)
    {
        RuntimeException v0_5;
        switch (p7) {
            case -1:
                android.graphics.Rect v1_0 = android.support.v4.view.a.q.a(this.j);
                android.support.v4.view.cx.a(this.j, v1_0);
                int v2_0 = new java.util.LinkedList().iterator();
                while (v2_0.hasNext()) {
                    android.support.v4.view.a.q.a.e(v1_0.b, this.j, ((Integer) v2_0.next()).intValue());
                }
                v0_5 = v1_0;
                break;
            default:
                v0_5 = android.support.v4.view.a.q.a();
                v0_5.h(1);
                v0_5.b(android.support.v4.widget.as.d);
                if ((v0_5.m() != null) || (v0_5.n() != null)) {
                    v0_5.a(this.f);
                    if (!this.f.isEmpty()) {
                        android.graphics.Rect v1_7 = v0_5.b();
                        if ((v1_7 & 64) == 0) {
                            if ((v1_7 & 128) == 0) {
                                v0_5.a(this.j.getContext().getPackageName());
                                android.support.v4.view.a.q.a.d(v0_5.b, this.j, p7);
                                v0_5.d(this.j);
                                if (this.l != p7) {
                                    v0_5.d(0);
                                    v0_5.a(64);
                                } else {
                                    v0_5.d(1);
                                    v0_5.a(128);
                                }
                                if (this.a(this.f)) {
                                    v0_5.c(1);
                                    v0_5.b(this.f);
                                }
                                this.j.getLocationOnScreen(this.h);
                                android.graphics.Rect v1_22 = this.h[0];
                                int v2_5 = this.h[1];
                                this.e.set(this.f);
                                this.e.offset(v1_22, v2_5);
                                v0_5.d(this.e);
                            } else {
                                throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
                            }
                        } else {
                            throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
                        }
                    } else {
                        throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
                    }
                } else {
                    throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
                }
        }
        return v0_5;
    }

    private android.view.accessibility.AccessibilityEvent c(int p5, int p6)
    {
        RuntimeException v0_0 = android.view.accessibility.AccessibilityEvent.obtain(p6);
        v0_0.setEnabled(1);
        v0_0.setClassName(android.support.v4.widget.as.d);
        if ((!v0_0.getText().isEmpty()) || (v0_0.getContentDescription() != null)) {
            v0_0.setPackageName(this.j.getContext().getPackageName());
            android.support.v4.view.a.bd.a.a(android.support.v4.view.a.a.a(v0_0).b, this.j, p5);
            return v0_0;
        } else {
            throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
        }
    }

    private android.support.v4.view.a.q d(int p7)
    {
        RuntimeException v0_0 = android.support.v4.view.a.q.a();
        v0_0.h(1);
        v0_0.b(android.support.v4.widget.as.d);
        if ((v0_0.m() != null) || (v0_0.n() != null)) {
            v0_0.a(this.f);
            if (!this.f.isEmpty()) {
                android.graphics.Rect v1_6 = v0_0.b();
                if ((v1_6 & 64) == 0) {
                    if ((v1_6 & 128) == 0) {
                        v0_0.a(this.j.getContext().getPackageName());
                        android.support.v4.view.a.q.a.d(v0_0.b, this.j, p7);
                        v0_0.d(this.j);
                        if (this.l != p7) {
                            v0_0.d(0);
                            v0_0.a(64);
                        } else {
                            v0_0.d(1);
                            v0_0.a(128);
                        }
                        if (this.a(this.f)) {
                            v0_0.c(1);
                            v0_0.b(this.f);
                        }
                        this.j.getLocationOnScreen(this.h);
                        android.graphics.Rect v1_21 = this.h[0];
                        int v2_4 = this.h[1];
                        this.e.set(this.f);
                        this.e.offset(v1_21, v2_4);
                        v0_0.d(this.e);
                        return v0_0;
                    } else {
                        throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
                    }
                } else {
                    throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
                }
            } else {
                throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
            }
        } else {
            throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
        }
    }

    private boolean d(int p6, int p7)
    {
        int v0 = 1;
        switch (p7) {
            case 64:
            case 128:
                switch (p7) {
                    case 64:
                        if ((this.i.isEnabled()) && (android.support.v4.view.a.h.a(this.i))) {
                            if (this.e(p6)) {
                                v0 = 0;
                            } else {
                                if (this.l != -2147483648) {
                                    this.a(this.l, 65536);
                                }
                                this.l = p6;
                                this.j.invalidate();
                                this.a(p6, 32768);
                            }
                        } else {
                            v0 = 0;
                        }
                        break;
                    case 128:
                        if (!this.e(p6)) {
                            v0 = 0;
                        } else {
                            this.l = -2147483648;
                            this.j.invalidate();
                            this.a(p6, 65536);
                        }
                        break;
                    default:
                        v0 = 0;
                }
                break;
            default:
                v0 = this.e();
        }
        return v0;
    }

    private boolean e(int p2)
    {
        int v0_1;
        if (this.l != p2) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private boolean e(int p6, int p7)
    {
        int v0 = 1;
        switch (p7) {
            case 64:
                if ((this.i.isEnabled()) && (android.support.v4.view.a.h.a(this.i))) {
                    if (this.e(p6)) {
                        v0 = 0;
                    } else {
                        if (this.l != -2147483648) {
                            this.a(this.l, 65536);
                        }
                        this.l = p6;
                        this.j.invalidate();
                        this.a(p6, 32768);
                    }
                } else {
                    v0 = 0;
                }
                break;
            case 128:
                if (!this.e(p6)) {
                    v0 = 0;
                } else {
                    this.l = -2147483648;
                    this.j.invalidate();
                    this.a(p6, 65536);
                }
                break;
            default:
                v0 = 0;
        }
        return v0;
    }

    private void f()
    {
        this.a(-1, 2048);
        return;
    }

    private boolean f(int p3)
    {
        int v0_0 = 0;
        if ((this.i.isEnabled()) && ((android.support.v4.view.a.h.a(this.i)) && (!this.e(p3)))) {
            if (this.l != -2147483648) {
                this.a(this.l, 65536);
            }
            this.l = p3;
            this.j.invalidate();
            this.a(p3, 32768);
            v0_0 = 1;
        }
        return v0_0;
    }

    private void g()
    {
        this.a(-1, 2048);
        return;
    }

    private boolean g(int p2)
    {
        int v0_1;
        if (!this.e(p2)) {
            v0_1 = 0;
        } else {
            this.l = -2147483648;
            this.j.invalidate();
            this.a(p2, 65536);
            v0_1 = 1;
        }
        return v0_1;
    }

    private int h()
    {
        return this.l;
    }

    private android.support.v4.view.a.q i()
    {
        android.support.v4.view.a.q v1 = android.support.v4.view.a.q.a(this.j);
        android.support.v4.view.cx.a(this.j, v1);
        java.util.Iterator v2 = new java.util.LinkedList().iterator();
        while (v2.hasNext()) {
            android.support.v4.view.a.q.a.e(v1.b, this.j, ((Integer) v2.next()).intValue());
        }
        return v1;
    }

    private static void j()
    {
        return;
    }

    protected abstract int a();

    public final android.support.v4.view.a.aq a(android.view.View p3)
    {
        if (this.k == null) {
            this.k = new android.support.v4.widget.au(this, 0);
        }
        return this.k;
    }

    protected abstract void b();

    protected abstract void c();

    protected abstract void d();

    protected abstract boolean e();
}
