package android.support.v4.widget;
final class d implements java.lang.Runnable {
    final synthetic android.support.v4.widget.a a;

    private d(android.support.v4.widget.a p1)
    {
        this.a = p1;
        return;
    }

    synthetic d(android.support.v4.widget.a p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void run()
    {
        android.view.View v0_0 = 0;
        if (android.support.v4.widget.a.a(this.a)) {
            if (android.support.v4.widget.a.b(this.a)) {
                android.support.v4.widget.a.c(this.a);
                android.support.v4.widget.a v1_6 = android.support.v4.widget.a.d(this.a);
                v1_6.e = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
                v1_6.i = -1;
                v1_6.f = v1_6.e;
                v1_6.j = 1056964608;
                v1_6.g = 0;
                v1_6.h = 0;
            }
            android.support.v4.widget.a v1_8 = android.support.v4.widget.a.d(this.a);
            if ((v1_8.i > 0) && (android.view.animation.AnimationUtils.currentAnimationTimeMillis() > (v1_8.i + ((long) v1_8.k)))) {
                v0_0 = 1;
            }
            if ((v0_0 == null) && (android.support.v4.widget.a.e(this.a))) {
                if (android.support.v4.widget.a.g(this.a)) {
                    android.support.v4.widget.a.h(this.a);
                    android.support.v4.widget.a.i(this.a);
                }
                if (v1_8.f != 0) {
                    float v2_9 = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
                    android.view.View v0_8 = v1_8.a(v2_9);
                    android.view.View v0_10 = ((v0_8 * 1082130432) + ((-1065353216 * v0_8) * v0_8));
                    long v4_6 = (v2_9 - v1_8.f);
                    v1_8.f = v2_9;
                    v1_8.g = ((int) ((((float) v4_6) * v0_10) * v1_8.c));
                    v1_8.h = ((int) ((v0_10 * ((float) v4_6)) * v1_8.d));
                    this.a.a(v1_8.h);
                    android.support.v4.view.cx.a(android.support.v4.widget.a.j(this.a), this);
                } else {
                    throw new RuntimeException("Cannot compute scroll delta before calling start()");
                }
            } else {
                android.support.v4.widget.a.f(this.a);
            }
        }
        return;
    }
}
