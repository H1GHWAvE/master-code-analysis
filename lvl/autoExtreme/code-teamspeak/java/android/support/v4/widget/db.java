package android.support.v4.widget;
final class db extends android.support.v4.view.a {
    final synthetic android.support.v4.widget.SlidingPaneLayout a;
    private final android.graphics.Rect c;

    db(android.support.v4.widget.SlidingPaneLayout p2)
    {
        this.a = p2;
        this.c = new android.graphics.Rect();
        return;
    }

    private void a(android.support.v4.view.a.q p4, android.support.v4.view.a.q p5)
    {
        int v0_0 = this.c;
        p5.a(v0_0);
        p4.b(v0_0);
        p5.c(v0_0);
        p4.d(v0_0);
        p4.c(p5.e());
        p4.a(p5.k());
        p4.b(p5.l());
        p4.c(p5.n());
        p4.h(p5.j());
        p4.f(p5.h());
        p4.a(p5.c());
        p4.b(p5.d());
        p4.d(p5.f());
        p4.e(p5.g());
        p4.g(p5.i());
        p4.a(p5.b());
        android.support.v4.view.a.q.a.g(p4.b, android.support.v4.view.a.q.a.D(p5.b));
        return;
    }

    private boolean b(android.view.View p2)
    {
        return this.a.b(p2);
    }

    public final void a(android.view.View p5, android.support.v4.view.a.q p6)
    {
        int v0_0 = android.support.v4.view.a.q.a(p6);
        super.a(p5, v0_0);
        int v1_0 = this.c;
        v0_0.a(v1_0);
        p6.b(v1_0);
        v0_0.c(v1_0);
        p6.d(v1_0);
        p6.c(v0_0.e());
        p6.a(v0_0.k());
        p6.b(v0_0.l());
        p6.c(v0_0.n());
        p6.h(v0_0.j());
        p6.f(v0_0.h());
        p6.a(v0_0.c());
        p6.b(v0_0.d());
        p6.d(v0_0.f());
        p6.e(v0_0.g());
        p6.g(v0_0.i());
        p6.a(v0_0.b());
        android.support.v4.view.a.q.a.g(p6.b, android.support.v4.view.a.q.a.D(v0_0.b));
        v0_0.o();
        p6.b(android.support.v4.widget.SlidingPaneLayout.getName());
        p6.b(p5);
        int v0_3 = android.support.v4.view.cx.g(p5);
        if ((v0_3 instanceof android.view.View)) {
            p6.d(((android.view.View) v0_3));
        }
        int v1_16 = this.a.getChildCount();
        int v0_6 = 0;
        while (v0_6 < v1_16) {
            android.view.View v2_3 = this.a.getChildAt(v0_6);
            if ((!this.b(v2_3)) && (v2_3.getVisibility() == 0)) {
                android.support.v4.view.cx.c(v2_3, 1);
                p6.c(v2_3);
            }
            v0_6++;
        }
        return;
    }

    public final void a(android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        super.a(p2, p3);
        p3.setClassName(android.support.v4.widget.SlidingPaneLayout.getName());
        return;
    }

    public final boolean a(android.view.ViewGroup p2, android.view.View p3, android.view.accessibility.AccessibilityEvent p4)
    {
        int v0_1;
        if (this.b(p3)) {
            v0_1 = 0;
        } else {
            v0_1 = super.a(p2, p3, p4);
        }
        return v0_1;
    }
}
