package android.support.v4.widget;
final class do implements android.view.animation.Animation$AnimationListener {
    final synthetic android.support.v4.widget.dn a;

    do(android.support.v4.widget.dn p1)
    {
        this.a = p1;
        return;
    }

    public final void onAnimationEnd(android.view.animation.Animation p4)
    {
        if (!android.support.v4.widget.dn.a(this.a)) {
            android.support.v4.widget.dn.b(this.a).stop();
            android.support.v4.widget.dn.e(this.a).setVisibility(8);
            android.support.v4.widget.dn.f(this.a);
            if (!android.support.v4.widget.dn.g(this.a)) {
                android.support.v4.widget.dn.a(this.a, (this.a.d - android.support.v4.widget.dn.h(this.a)), 1);
            } else {
                android.support.v4.widget.dn.a(this.a, 0);
            }
        } else {
            android.support.v4.widget.dn.b(this.a).setAlpha(255);
            android.support.v4.widget.dn.b(this.a).start();
            if ((android.support.v4.widget.dn.c(this.a)) && (android.support.v4.widget.dn.d(this.a) != null)) {
                android.support.v4.widget.dn.d(this.a);
            }
        }
        android.support.v4.widget.dn.a(this.a, android.support.v4.widget.dn.e(this.a).getTop());
        return;
    }

    public final void onAnimationRepeat(android.view.animation.Animation p1)
    {
        return;
    }

    public final void onAnimationStart(android.view.animation.Animation p1)
    {
        return;
    }
}
