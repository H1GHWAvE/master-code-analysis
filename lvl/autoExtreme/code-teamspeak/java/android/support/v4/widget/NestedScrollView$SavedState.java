package android.support.v4.widget;
 class NestedScrollView$SavedState extends android.view.View$BaseSavedState {
    public static final android.os.Parcelable$Creator CREATOR;
    public int a;

    static NestedScrollView$SavedState()
    {
        android.support.v4.widget.NestedScrollView$SavedState.CREATOR = new android.support.v4.widget.bi();
        return;
    }

    public NestedScrollView$SavedState(android.os.Parcel p2)
    {
        this(p2);
        this.a = p2.readInt();
        return;
    }

    NestedScrollView$SavedState(android.os.Parcelable p1)
    {
        this(p1);
        return;
    }

    public String toString()
    {
        return new StringBuilder("HorizontalScrollView.SavedState{").append(Integer.toHexString(System.identityHashCode(this))).append(" scrollPosition=").append(this.a).append("}").toString();
    }

    public void writeToParcel(android.os.Parcel p2, int p3)
    {
        super.writeToParcel(p2, p3);
        p2.writeInt(this.a);
        return;
    }
}
