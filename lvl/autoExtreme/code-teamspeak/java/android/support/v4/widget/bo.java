package android.support.v4.widget;
public final class bo {
    static final android.support.v4.widget.bu a;

    static bo()
    {
        android.support.v4.widget.br v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 23) {
            if (v0_0 < 21) {
                if (v0_0 < 19) {
                    if (v0_0 < 9) {
                        android.support.v4.widget.bo.a = new android.support.v4.widget.br();
                    } else {
                        android.support.v4.widget.bo.a = new android.support.v4.widget.bs();
                    }
                } else {
                    android.support.v4.widget.bo.a = new android.support.v4.widget.bt();
                }
            } else {
                android.support.v4.widget.bo.a = new android.support.v4.widget.bp();
            }
        } else {
            android.support.v4.widget.bo.a = new android.support.v4.widget.bq();
        }
        return;
    }

    private bo()
    {
        return;
    }

    public static void a(android.widget.PopupWindow p6, android.view.View p7, int p8, int p9, int p10)
    {
        android.support.v4.widget.bo.a.a(p6, p7, p8, p9, p10);
        return;
    }

    public static void a(android.widget.PopupWindow p1, boolean p2)
    {
        android.support.v4.widget.bo.a.a(p1, p2);
        return;
    }

    public static boolean a(android.widget.PopupWindow p1)
    {
        return android.support.v4.widget.bo.a.a(p1);
    }

    public static void b(android.widget.PopupWindow p1)
    {
        android.support.v4.widget.bo.a.b(p1);
        return;
    }

    private static int c(android.widget.PopupWindow p1)
    {
        return android.support.v4.widget.bo.a.c(p1);
    }
}
