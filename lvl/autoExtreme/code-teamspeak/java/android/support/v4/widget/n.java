package android.support.v4.widget;
final class n {

    n()
    {
        return;
    }

    private static android.content.res.ColorStateList a(android.widget.CompoundButton p1)
    {
        return p1.getButtonTintList();
    }

    private static void a(android.widget.CompoundButton p0, android.content.res.ColorStateList p1)
    {
        p0.setButtonTintList(p1);
        return;
    }

    private static void a(android.widget.CompoundButton p0, android.graphics.PorterDuff$Mode p1)
    {
        p0.setButtonTintMode(p1);
        return;
    }

    private static android.graphics.PorterDuff$Mode b(android.widget.CompoundButton p1)
    {
        return p1.getButtonTintMode();
    }
}
