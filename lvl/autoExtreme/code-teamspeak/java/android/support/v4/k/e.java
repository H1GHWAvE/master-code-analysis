package android.support.v4.k;
final class e extends android.support.v4.k.a {
    private android.content.Context b;
    private android.net.Uri c;

    e(android.content.Context p2, android.net.Uri p3)
    {
        this(0);
        this.b = p2;
        this.c = p3;
        return;
    }

    public final android.net.Uri a()
    {
        return this.c;
    }

    public final android.support.v4.k.a a(String p2)
    {
        throw new UnsupportedOperationException();
    }

    public final android.support.v4.k.a a(String p2, String p3)
    {
        throw new UnsupportedOperationException();
    }

    public final String b()
    {
        return android.support.v4.k.b.a(this.b, this.c, "_display_name");
    }

    public final boolean b(String p2)
    {
        throw new UnsupportedOperationException();
    }

    public final String c()
    {
        return android.support.v4.k.b.a(this.b, this.c);
    }

    public final boolean d()
    {
        return android.support.v4.k.b.b(this.b, this.c);
    }

    public final boolean e()
    {
        return android.support.v4.k.b.c(this.b, this.c);
    }

    public final long f()
    {
        return android.support.v4.k.b.b(this.b, this.c, "last_modified");
    }

    public final long g()
    {
        return android.support.v4.k.b.b(this.b, this.c, "_size");
    }

    public final boolean h()
    {
        return android.support.v4.k.b.d(this.b, this.c);
    }

    public final boolean i()
    {
        return android.support.v4.k.b.e(this.b, this.c);
    }

    public final boolean j()
    {
        return android.support.v4.k.b.f(this.b, this.c);
    }

    public final boolean k()
    {
        return android.support.v4.k.b.g(this.b, this.c);
    }

    public final android.support.v4.k.a[] l()
    {
        throw new UnsupportedOperationException();
    }
}
