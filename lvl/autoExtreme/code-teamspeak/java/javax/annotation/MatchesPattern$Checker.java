package javax.annotation;
public final class MatchesPattern$Checker implements javax.annotation.meta.TypeQualifierValidator {

    public MatchesPattern$Checker()
    {
        return;
    }

    private static javax.annotation.meta.When forConstantValue(javax.annotation.MatchesPattern p2, Object p3)
    {
        javax.annotation.meta.When v0_4;
        if (!java.util.regex.Pattern.compile(p2.value(), p2.flags()).matcher(((String) p3)).matches()) {
            v0_4 = javax.annotation.meta.When.NEVER;
        } else {
            v0_4 = javax.annotation.meta.When.ALWAYS;
        }
        return v0_4;
    }

    public final synthetic javax.annotation.meta.When forConstantValue(otation.Annotation p3, Object p4)
    {
        javax.annotation.meta.When v0_4;
        if (!java.util.regex.Pattern.compile(((javax.annotation.MatchesPattern) p3).value(), ((javax.annotation.MatchesPattern) p3).flags()).matcher(((String) p4)).matches()) {
            v0_4 = javax.annotation.meta.When.NEVER;
        } else {
            v0_4 = javax.annotation.meta.When.ALWAYS;
        }
        return v0_4;
    }
}
