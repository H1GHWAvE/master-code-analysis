package javax.inject;
public interface Provider {

    public abstract Object get();
}
