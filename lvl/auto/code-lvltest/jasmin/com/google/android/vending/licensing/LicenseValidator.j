.bytecode 50.0
.class synchronized com/google/android/vending/licensing/LicenseValidator
.super java/lang/Object

.field private static final 'ERROR_CONTACTING_SERVER' I = 257


.field private static final 'ERROR_INVALID_PACKAGE_NAME' I = 258


.field private static final 'ERROR_NON_MATCHING_UID' I = 259


.field private static final 'ERROR_NOT_MARKET_MANAGED' I = 3


.field private static final 'ERROR_OVER_QUOTA' I = 5


.field private static final 'ERROR_SERVER_FAILURE' I = 4


.field private static final 'LICENSED' I = 0


.field private static final 'LICENSED_OLD_KEY' I = 2


.field private static final 'NOT_LICENSED' I = 1


.field private static final 'SIGNATURE_ALGORITHM' Ljava/lang/String; = "SHA1withRSA"

.field private static final 'TAG' Ljava/lang/String; = "LicenseValidator"

.field private final 'mCallback' Lcom/google/android/vending/licensing/LicenseCheckerCallback;

.field private final 'mDeviceLimiter' Lcom/google/android/vending/licensing/DeviceLimiter;

.field private final 'mNonce' I

.field private final 'mPackageName' Ljava/lang/String;

.field private final 'mPolicy' Lcom/google/android/vending/licensing/Policy;

.field private final 'mVersionCode' Ljava/lang/String;

.method <init>(Lcom/google/android/vending/licensing/Policy;Lcom/google/android/vending/licensing/DeviceLimiter;Lcom/google/android/vending/licensing/LicenseCheckerCallback;ILjava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/google/android/vending/licensing/LicenseValidator/mPolicy Lcom/google/android/vending/licensing/Policy;
aload 0
aload 2
putfield com/google/android/vending/licensing/LicenseValidator/mDeviceLimiter Lcom/google/android/vending/licensing/DeviceLimiter;
aload 0
aload 3
putfield com/google/android/vending/licensing/LicenseValidator/mCallback Lcom/google/android/vending/licensing/LicenseCheckerCallback;
aload 0
iload 4
putfield com/google/android/vending/licensing/LicenseValidator/mNonce I
aload 0
aload 5
putfield com/google/android/vending/licensing/LicenseValidator/mPackageName Ljava/lang/String;
aload 0
aload 6
putfield com/google/android/vending/licensing/LicenseValidator/mVersionCode Ljava/lang/String;
return
.limit locals 7
.limit stack 2
.end method

.method private handleApplicationError(I)V
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mCallback Lcom/google/android/vending/licensing/LicenseCheckerCallback;
iload 1
invokeinterface com/google/android/vending/licensing/LicenseCheckerCallback/applicationError(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private handleInvalidResponse()V
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mCallback Lcom/google/android/vending/licensing/LicenseCheckerCallback;
sipush 561
invokeinterface com/google/android/vending/licensing/LicenseCheckerCallback/dontAllow(I)V 1
return
.limit locals 1
.limit stack 2
.end method

.method private handleResponse(ILcom/google/android/vending/licensing/ResponseData;)V
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mPolicy Lcom/google/android/vending/licensing/Policy;
iload 1
aload 2
invokeinterface com/google/android/vending/licensing/Policy/processServerResponse(ILcom/google/android/vending/licensing/ResponseData;)V 2
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mPolicy Lcom/google/android/vending/licensing/Policy;
invokeinterface com/google/android/vending/licensing/Policy/allowAccess()Z 0
ifeq L0
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mCallback Lcom/google/android/vending/licensing/LicenseCheckerCallback;
iload 1
invokeinterface com/google/android/vending/licensing/LicenseCheckerCallback/allow(I)V 1
return
L0:
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mCallback Lcom/google/android/vending/licensing/LicenseCheckerCallback;
iload 1
invokeinterface com/google/android/vending/licensing/LicenseCheckerCallback/dontAllow(I)V 1
return
.limit locals 3
.limit stack 3
.end method

.method public getCallback()Lcom/google/android/vending/licensing/LicenseCheckerCallback;
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mCallback Lcom/google/android/vending/licensing/LicenseCheckerCallback;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getNonce()I
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mNonce I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getPackageName()Ljava/lang/String;
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mPackageName Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public verify(Ljava/security/PublicKey;ILjava/lang/String;Ljava/lang/String;)V
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/InvalidKeyException from L0 to L1 using L3
.catch java/security/SignatureException from L0 to L1 using L4
.catch com/google/android/vending/licensing/util/Base64DecoderException from L0 to L1 using L5
.catch java/security/NoSuchAlgorithmException from L6 to L7 using L2
.catch java/security/InvalidKeyException from L6 to L7 using L3
.catch java/security/SignatureException from L6 to L7 using L4
.catch com/google/android/vending/licensing/util/Base64DecoderException from L6 to L7 using L5
.catch java/lang/IllegalArgumentException from L8 to L9 using L10
aconst_null
astore 6
aconst_null
astore 5
iload 2
ifeq L0
iload 2
iconst_1
if_icmpeq L0
iload 2
iconst_2
if_icmpne L11
L0:
ldc "SHA1withRSA"
invokestatic java/security/Signature/getInstance(Ljava/lang/String;)Ljava/security/Signature;
astore 5
aload 5
aload 1
invokevirtual java/security/Signature/initVerify(Ljava/security/PublicKey;)V
aload 5
aload 3
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/security/Signature/update([B)V
aload 5
aload 4
invokestatic com/google/android/vending/licensing/util/Base64/decode(Ljava/lang/String;)[B
invokevirtual java/security/Signature/verify([B)Z
pop
aload 5
aload 4
invokestatic com/google/android/vending/licensing/util/Base64/decode(Ljava/lang/String;)[B
invokevirtual java/security/Signature/verify([B)Z
pop
L1:
iconst_1
ifne L8
L6:
ldc "LicenseValidator"
ldc "Signature verification failed."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokespecial com/google/android/vending/licensing/LicenseValidator/handleInvalidResponse()V
L7:
return
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 1
aload 0
iconst_5
invokespecial com/google/android/vending/licensing/LicenseValidator/handleApplicationError(I)V
return
L4:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L5:
astore 1
ldc "LicenseValidator"
ldc "Could not Base64-decode signature."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokespecial com/google/android/vending/licensing/LicenseValidator/handleInvalidResponse()V
return
L8:
aload 3
invokestatic com/google/android/vending/licensing/ResponseData/parse(Ljava/lang/String;)Lcom/google/android/vending/licensing/ResponseData;
astore 5
L9:
aload 5
getfield com/google/android/vending/licensing/ResponseData/responseCode I
iload 2
if_icmpeq L12
ldc "LicenseValidator"
ldc "Response codes don't match."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokespecial com/google/android/vending/licensing/LicenseValidator/handleInvalidResponse()V
return
L10:
astore 1
ldc "LicenseValidator"
ldc "Could not parse response."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokespecial com/google/android/vending/licensing/LicenseValidator/handleInvalidResponse()V
return
L12:
aload 5
getfield com/google/android/vending/licensing/ResponseData/nonce I
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mNonce I
if_icmpeq L13
ldc "LicenseValidator"
ldc "Nonce doesn't match."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokespecial com/google/android/vending/licensing/LicenseValidator/handleInvalidResponse()V
return
L13:
aload 5
getfield com/google/android/vending/licensing/ResponseData/packageName Ljava/lang/String;
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mPackageName Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L14
ldc "LicenseValidator"
ldc "Package name doesn't match."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokespecial com/google/android/vending/licensing/LicenseValidator/handleInvalidResponse()V
return
L14:
aload 5
getfield com/google/android/vending/licensing/ResponseData/versionCode Ljava/lang/String;
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mVersionCode Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L15
ldc "LicenseValidator"
ldc "Version codes don't match."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokespecial com/google/android/vending/licensing/LicenseValidator/handleInvalidResponse()V
return
L15:
aload 5
getfield com/google/android/vending/licensing/ResponseData/userId Ljava/lang/String;
astore 1
aload 1
astore 6
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L11
ldc "LicenseValidator"
ldc "User identifier is empty."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokespecial com/google/android/vending/licensing/LicenseValidator/handleInvalidResponse()V
return
L11:
iload 2
lookupswitch
0 : L16
1 : L16
2 : L17
3 : L18
4 : L19
5 : L20
257 : L21
258 : L22
259 : L23
default : L24
L24:
ldc "LicenseValidator"
ldc "Unknown response code for license check."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokespecial com/google/android/vending/licensing/LicenseValidator/handleInvalidResponse()V
return
L16:
aload 0
aload 0
getfield com/google/android/vending/licensing/LicenseValidator/mDeviceLimiter Lcom/google/android/vending/licensing/DeviceLimiter;
aload 6
invokeinterface com/google/android/vending/licensing/DeviceLimiter/isDeviceAllowed(Ljava/lang/String;)I 1
aload 5
invokespecial com/google/android/vending/licensing/LicenseValidator/handleResponse(ILcom/google/android/vending/licensing/ResponseData;)V
return
L17:
aload 0
sipush 561
aload 5
invokespecial com/google/android/vending/licensing/LicenseValidator/handleResponse(ILcom/google/android/vending/licensing/ResponseData;)V
return
L21:
ldc "LicenseValidator"
ldc "Error contacting licensing server."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
sipush 291
aload 5
invokespecial com/google/android/vending/licensing/LicenseValidator/handleResponse(ILcom/google/android/vending/licensing/ResponseData;)V
return
L19:
ldc "LicenseValidator"
ldc "An error has occurred on the licensing server."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
sipush 291
aload 5
invokespecial com/google/android/vending/licensing/LicenseValidator/handleResponse(ILcom/google/android/vending/licensing/ResponseData;)V
return
L20:
ldc "LicenseValidator"
ldc "Licensing server is refusing to talk to this device, over quota."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
sipush 291
aload 5
invokespecial com/google/android/vending/licensing/LicenseValidator/handleResponse(ILcom/google/android/vending/licensing/ResponseData;)V
return
L22:
aload 0
iconst_1
invokespecial com/google/android/vending/licensing/LicenseValidator/handleApplicationError(I)V
return
L23:
aload 0
iconst_2
invokespecial com/google/android/vending/licensing/LicenseValidator/handleApplicationError(I)V
return
L18:
aload 0
iconst_3
invokespecial com/google/android/vending/licensing/LicenseValidator/handleApplicationError(I)V
return
.limit locals 7
.limit stack 3
.end method
