package com.google.android.vending.licensing;
 class LicenseValidator {
    private static final int ERROR_CONTACTING_SERVER = 257;
    private static final int ERROR_INVALID_PACKAGE_NAME = 258;
    private static final int ERROR_NON_MATCHING_UID = 259;
    private static final int ERROR_NOT_MARKET_MANAGED = 3;
    private static final int ERROR_OVER_QUOTA = 5;
    private static final int ERROR_SERVER_FAILURE = 4;
    private static final int LICENSED = 0;
    private static final int LICENSED_OLD_KEY = 2;
    private static final int NOT_LICENSED = 1;
    private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";
    private static final String TAG = "LicenseValidator";
    private final com.google.android.vending.licensing.LicenseCheckerCallback mCallback;
    private final com.google.android.vending.licensing.DeviceLimiter mDeviceLimiter;
    private final int mNonce;
    private final String mPackageName;
    private final com.google.android.vending.licensing.Policy mPolicy;
    private final String mVersionCode;

    LicenseValidator(com.google.android.vending.licensing.Policy p1, com.google.android.vending.licensing.DeviceLimiter p2, com.google.android.vending.licensing.LicenseCheckerCallback p3, int p4, String p5, String p6)
    {
        this.mPolicy = p1;
        this.mDeviceLimiter = p2;
        this.mCallback = p3;
        this.mNonce = p4;
        this.mPackageName = p5;
        this.mVersionCode = p6;
        return;
    }

    private void handleApplicationError(int p2)
    {
        this.mCallback.applicationError(p2);
        return;
    }

    private void handleInvalidResponse()
    {
        this.mCallback.dontAllow(561);
        return;
    }

    private void handleResponse(int p2, com.google.android.vending.licensing.ResponseData p3)
    {
        this.mPolicy.processServerResponse(p2, p3);
        if (!this.mPolicy.allowAccess()) {
            this.mCallback.dontAllow(p2);
        } else {
            this.mCallback.allow(p2);
        }
        return;
    }

    public com.google.android.vending.licensing.LicenseCheckerCallback getCallback()
    {
        return this.mCallback;
    }

    public int getNonce()
    {
        return this.mNonce;
    }

    public String getPackageName()
    {
        return this.mPackageName;
    }

    public void verify(java.security.PublicKey p11, int p12, String p13, String p14)
    {
        String v4 = 0;
        com.google.android.vending.licensing.ResponseData v0 = 0;
        if ((p12 != 0) && ((p12 != 1) && (p12 != 2))) {
            switch (p12) {
                case 0:
                case 1:
                    this.handleResponse(this.mDeviceLimiter.isDeviceAllowed(v4), v0);
                    break;
                case 2:
                    this.handleResponse(561, v0);
                    break;
                case 3:
                    this.handleApplicationError(3);
                    break;
                case 4:
                    android.util.Log.w("LicenseValidator", "An error has occurred on the licensing server.");
                    this.handleResponse(291, v0);
                    break;
                case 5:
                    android.util.Log.w("LicenseValidator", "Licensing server is refusing to talk to this device, over quota.");
                    this.handleResponse(291, v0);
                    break;
                case 257:
                    android.util.Log.w("LicenseValidator", "Error contacting licensing server.");
                    this.handleResponse(291, v0);
                    break;
                case 258:
                    this.handleApplicationError(1);
                    break;
                case 259:
                    this.handleApplicationError(2);
                    break;
                default:
                    android.util.Log.e("LicenseValidator", "Unknown response code for license check.");
                    this.handleInvalidResponse();
            }
        } else {
            try {
                java.security.Signature v3 = java.security.Signature.getInstance("SHA1withRSA");
                v3.initVerify(p11);
                v3.update(p13.getBytes());
                v3.verify(com.google.android.vending.licensing.util.Base64.decode(p14));
                v3.verify(com.google.android.vending.licensing.util.Base64.decode(p14));
            } catch (IllegalArgumentException v1_1) {
                throw new RuntimeException(v1_1);
            } catch (IllegalArgumentException v1) {
                this.handleApplicationError(5);
            } catch (IllegalArgumentException v1) {
                android.util.Log.e("LicenseValidator", "Could not Base64-decode signature.");
                this.handleInvalidResponse();
            } catch (IllegalArgumentException v1_0) {
                throw new RuntimeException(v1_0);
            }
            if (1 != 0) {
                try {
                    v0 = com.google.android.vending.licensing.ResponseData.parse(p13);
                } catch (IllegalArgumentException v1) {
                    android.util.Log.e("LicenseValidator", "Could not parse response.");
                    this.handleInvalidResponse();
                }
                if (v0.responseCode == p12) {
                    if (v0.nonce == this.mNonce) {
                        if (v0.packageName.equals(this.mPackageName)) {
                            if (v0.versionCode.equals(this.mVersionCode)) {
                                v4 = v0.userId;
                                if (!android.text.TextUtils.isEmpty(v4)) {
                                } else {
                                    android.util.Log.e("LicenseValidator", "User identifier is empty.");
                                    this.handleInvalidResponse();
                                }
                            } else {
                                android.util.Log.e("LicenseValidator", "Version codes don\'t match.");
                                this.handleInvalidResponse();
                            }
                        } else {
                            android.util.Log.e("LicenseValidator", "Package name doesn\'t match.");
                            this.handleInvalidResponse();
                        }
                    } else {
                        android.util.Log.e("LicenseValidator", "Nonce doesn\'t match.");
                        this.handleInvalidResponse();
                    }
                } else {
                    android.util.Log.e("LicenseValidator", "Response codes don\'t match.");
                    this.handleInvalidResponse();
                }
            } else {
                android.util.Log.e("LicenseValidator", "Signature verification failed.");
                this.handleInvalidResponse();
            }
        }
        return;
    }
}
