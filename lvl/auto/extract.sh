#!/bin/bash

#remove old files
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-lvltest/java/
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-teamspeak/java/
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-lvltest/smali/
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-teamspeak/smali/
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-lvltest/jasmin
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-teamspeak/jasmin
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-lvltest/jar/lvltest.jar
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-teamspeak/jar/teamspeak.jar

#androguard
python ~/Desktop/master_thesis/sourcefiles/_tools/androdd.py -i ~/Desktop/master_thesis/sourcefiles/lvl/auto/apk-lvltest/lvltest.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-lvltest/java/
python ~/Desktop/master_thesis/sourcefiles/_tools/androdd.py -i ~/Desktop/master_thesis/sourcefiles/lvl/auto/apk-teamspeak/teamspeak.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-teamspeak/java/

#baksmali
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/baksmali.jar -x ~/Desktop/master_thesis/sourcefiles/lvl/auto/apk-lvltest/lvltest.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-lvltest/smali/
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/baksmali.jar -x ~/Desktop/master_thesis/sourcefiles/lvl/auto/apk-teamspeak/teamspeak.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-teamspeak/smali/

#jasmin
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-dex2jar.sh -f -o ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-lvltest/jar/lvltest.jar ~/Desktop/master_thesis/sourcefiles/lvl/auto/apk-lvltest/lvltest.apk
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-dex2jar.sh -f -o ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-teamspeak/jar/teamspeak.jar ~/Desktop/master_thesis/sourcefiles/lvl/auto/apk-teamspeak/teamspeak.apk

#jar
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-jar2jasmin.sh -f -o ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-lvltest/jasmin ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-lvltest/jar/lvltest.jar
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-jar2jasmin.sh -f -o ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-teamspeak/jasmin ~/Desktop/master_thesis/sourcefiles/lvl/auto/code-teamspeak/jar/teamspeak.jar
