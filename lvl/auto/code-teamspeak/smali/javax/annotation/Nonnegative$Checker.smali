.class public final Ljavax/annotation/Nonnegative$Checker;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljavax/annotation/meta/TypeQualifierValidator;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static forConstantValue$4ec69d5d(Ljava/lang/Object;)Ljavax/annotation/meta/When;
    .registers 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 21
    instance-of v2, p0, Ljava/lang/Number;

    if-nez v2, :cond_9

    .line 22
    sget-object v0, Ljavax/annotation/meta/When;->NEVER:Ljavax/annotation/meta/When;

    .line 37
    :goto_8
    return-object v0

    .line 24
    :cond_9
    check-cast p0, Ljava/lang/Number;

    .line 25
    instance-of v2, p0, Ljava/lang/Long;

    if-eqz v2, :cond_20

    .line 26
    invoke-virtual {p0}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_1e

    .line 34
    :cond_19
    :goto_19
    if-eqz v0, :cond_47

    .line 35
    sget-object v0, Ljavax/annotation/meta/When;->NEVER:Ljavax/annotation/meta/When;

    goto :goto_8

    :cond_1e
    move v0, v1

    .line 26
    goto :goto_19

    .line 27
    :cond_20
    instance-of v2, p0, Ljava/lang/Double;

    if-eqz v2, :cond_30

    .line 28
    invoke-virtual {p0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpg-double v2, v2, v4

    if-ltz v2, :cond_19

    move v0, v1

    goto :goto_19

    .line 29
    :cond_30
    instance-of v2, p0, Ljava/lang/Float;

    if-eqz v2, :cond_3f

    .line 30
    invoke-virtual {p0}, Ljava/lang/Number;->floatValue()F

    move-result v2

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_19

    move v0, v1

    goto :goto_19

    .line 32
    :cond_3f
    invoke-virtual {p0}, Ljava/lang/Number;->intValue()I

    move-result v2

    if-ltz v2, :cond_19

    move v0, v1

    goto :goto_19

    .line 37
    :cond_47
    sget-object v0, Ljavax/annotation/meta/When;->ALWAYS:Ljavax/annotation/meta/When;

    goto :goto_8
.end method


# virtual methods
.method public final synthetic forConstantValue(Ljava/lang/annotation/Annotation;Ljava/lang/Object;)Ljavax/annotation/meta/When;
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 18
    .line 1021
    instance-of v2, p2, Ljava/lang/Number;

    if-nez v2, :cond_9

    .line 1022
    sget-object v0, Ljavax/annotation/meta/When;->NEVER:Ljavax/annotation/meta/When;

    .line 1035
    :goto_8
    return-object v0

    .line 1024
    :cond_9
    check-cast p2, Ljava/lang/Number;

    .line 1025
    instance-of v2, p2, Ljava/lang/Long;

    if-eqz v2, :cond_20

    .line 1026
    invoke-virtual {p2}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_1e

    .line 1034
    :cond_19
    :goto_19
    if-eqz v0, :cond_47

    .line 1035
    sget-object v0, Ljavax/annotation/meta/When;->NEVER:Ljavax/annotation/meta/When;

    goto :goto_8

    :cond_1e
    move v0, v1

    .line 1026
    goto :goto_19

    .line 1027
    :cond_20
    instance-of v2, p2, Ljava/lang/Double;

    if-eqz v2, :cond_30

    .line 1028
    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpg-double v2, v2, v4

    if-ltz v2, :cond_19

    move v0, v1

    goto :goto_19

    .line 1029
    :cond_30
    instance-of v2, p2, Ljava/lang/Float;

    if-eqz v2, :cond_3f

    .line 1030
    invoke-virtual {p2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_19

    move v0, v1

    goto :goto_19

    .line 1032
    :cond_3f
    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v2

    if-ltz v2, :cond_19

    move v0, v1

    goto :goto_19

    .line 1037
    :cond_47
    sget-object v0, Ljavax/annotation/meta/When;->ALWAYS:Ljavax/annotation/meta/When;

    goto :goto_8
.end method
