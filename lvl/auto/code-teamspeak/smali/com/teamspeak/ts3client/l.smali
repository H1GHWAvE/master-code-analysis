.class final Lcom/teamspeak/ts3client/l;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/ConnectionBackground;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V
    .registers 2

    .prologue
    .line 122
    iput-object p1, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8

    .prologue
    .line 126
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 127
    iget-object v0, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    iget-object v1, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;Landroid/view/WindowManager$LayoutParams;)V

    .line 128
    iget-object v0, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->c(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v2}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 129
    iget-object v0, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    iget-boolean v0, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->c:Z

    if-eqz v0, :cond_70

    iget-object v0, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    iget v0, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->d:I

    iget-object v1, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_70

    .line 130
    iget-object v0, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 130
    iget-object v1, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 2061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 130
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 131
    iget-object v0, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3157
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 131
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->f()V

    .line 132
    iget-object v0, p0, Lcom/teamspeak/ts3client/l;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->c:Z

    .line 135
    :cond_70
    return-void
.end method
