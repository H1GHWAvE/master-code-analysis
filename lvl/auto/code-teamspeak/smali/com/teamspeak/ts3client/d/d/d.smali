.class public final Lcom/teamspeak/ts3client/d/d/d;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field private aA:Landroid/widget/Button;

.field private at:Lcom/teamspeak/ts3client/Ts3Application;

.field private au:Landroid/widget/EditText;

.field private av:Landroid/widget/EditText;

.field private aw:Landroid/widget/EditText;

.field private ax:Landroid/widget/Spinner;

.field private ay:Landroid/widget/Spinner;

.field private az:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/d/d;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/d;->aw:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/d/d;)Landroid/widget/Button;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/d;->aA:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/d/d;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/d;->au:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/d/d;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/d;->az:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/d/d;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/d;->av:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/d/d;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/d;->ax:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/d/d/d;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/d;->ay:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/d/d/d;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/d;->at:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 9

    .prologue
    const/4 v5, 0x0

    .line 41
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/d/d;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 42
    const v0, 0x7f030056

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 1207
    iget-object v1, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 43
    const-string v2, "temppass.title"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 44
    const-string v1, "temppass.create.password"

    const v2, 0x7f0c01c9

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 45
    const-string v1, "temppass.create.description"

    const v2, 0x7f0c01cb

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 46
    const-string v1, "temppass.create.duration"

    const v2, 0x7f0c01cd

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 47
    const-string v1, "temppass.create.channel"

    const v2, 0x7f0c01d0

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 48
    const-string v1, "temppass.create.channelpw"

    const v2, 0x7f0c01d2

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 49
    const v1, 0x7f0c01d4

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->aA:Landroid/widget/Button;

    .line 50
    const v1, 0x7f0c01ca

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->au:Landroid/widget/EditText;

    .line 51
    const v1, 0x7f0c01cc

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->av:Landroid/widget/EditText;

    .line 52
    const v1, 0x7f0c01ce

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->aw:Landroid/widget/EditText;

    .line 53
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->au:Landroid/widget/EditText;

    new-instance v2, Lcom/teamspeak/ts3client/d/d/e;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/d/e;-><init>(Lcom/teamspeak/ts3client/d/d/d;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 76
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->aw:Landroid/widget/EditText;

    new-instance v2, Lcom/teamspeak/ts3client/d/d/f;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/d/f;-><init>(Lcom/teamspeak/ts3client/d/d/d;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 98
    const v1, 0x7f0c01cf

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->ax:Landroid/widget/Spinner;

    .line 99
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->ax:Landroid/widget/Spinner;

    const-string v2, "temppass.create.duration.array"

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/d/d;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    const/4 v4, 0x3

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 100
    const v1, 0x7f0c01d1

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->ay:Landroid/widget/Spinner;

    .line 101
    const v1, 0x7f0c01d3

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->az:Landroid/widget/EditText;

    .line 103
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2165
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->f:Lcom/teamspeak/ts3client/data/g;

    .line 3080
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    .line 104
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 105
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_cb
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_db

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 106
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_cb

    .line 108
    :cond_db
    new-instance v1, Lcom/teamspeak/ts3client/data/a;

    invoke-direct {v1}, Lcom/teamspeak/ts3client/data/a;-><init>()V

    .line 109
    const-string v3, "temppass.create.notarget"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/teamspeak/ts3client/data/a;->a(Ljava/lang/String;)V

    .line 110
    invoke-interface {v2, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 111
    invoke-interface {v2, v5, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 112
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x1090009

    invoke-direct {v1, v3, v4, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 113
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/d/d;->ay:Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 114
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->ay:Landroid/widget/Spinner;

    new-instance v2, Lcom/teamspeak/ts3client/d/d/g;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/d/g;-><init>(Lcom/teamspeak/ts3client/d/d/d;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 132
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->aA:Landroid/widget/Button;

    const-string v2, "button.save"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/d/d;->aA:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/d/h;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/d/h;-><init>(Lcom/teamspeak/ts3client/d/d/d;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    return-object v0
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 166
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 167
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 168
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 172
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 173
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 175
    return-void
.end method
