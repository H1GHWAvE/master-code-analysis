.class public final Lcom/teamspeak/ts3client/d/a/t;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field private at:Lcom/teamspeak/ts3client/d/a/m;

.field private au:Landroid/widget/EditText;

.field private av:Landroid/widget/Button;

.field private aw:Landroid/view/View;

.field private ax:Landroid/view/View;

.field private ay:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/d/a/m;)V
    .registers 2

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/t;->at:Lcom/teamspeak/ts3client/d/a/m;

    .line 29
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/a/t;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 17
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/t;->au:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/a/t;)Lcom/teamspeak/ts3client/d/a/m;
    .registers 2

    .prologue
    .line 17
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/t;->at:Lcom/teamspeak/ts3client/d/a/m;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 6

    .prologue
    .line 53
    const v0, 0x7f030022

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1207
    iget-object v1, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 54
    const-string v2, "channeldialog.editdesc.dialog"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 55
    const v1, 0x7f0c00c8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/t;->ay:Landroid/view/View;

    .line 56
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/t;->ay:Landroid/view/View;

    new-instance v2, Lcom/teamspeak/ts3client/d/a/u;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/a/u;-><init>(Lcom/teamspeak/ts3client/d/a/t;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    const v1, 0x7f0c00c7

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/t;->ax:Landroid/view/View;

    .line 65
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/t;->ax:Landroid/view/View;

    new-instance v2, Lcom/teamspeak/ts3client/d/a/v;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/a/v;-><init>(Lcom/teamspeak/ts3client/d/a/t;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    const v1, 0x7f0c00c9

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/t;->aw:Landroid/view/View;

    .line 74
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/t;->aw:Landroid/view/View;

    new-instance v2, Lcom/teamspeak/ts3client/d/a/w;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/a/w;-><init>(Lcom/teamspeak/ts3client/d/a/t;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    const v1, 0x7f0c00ca

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/t;->au:Landroid/widget/EditText;

    .line 82
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/t;->au:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/t;->at:Lcom/teamspeak/ts3client/d/a/m;

    .line 2118
    iget-object v2, v2, Lcom/teamspeak/ts3client/d/a/m;->at:Ljava/lang/String;

    .line 82
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 83
    const v1, 0x7f0c00cb

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/t;->av:Landroid/widget/Button;

    .line 84
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/t;->av:Landroid/widget/Button;

    const-string v2, "button.save"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/t;->av:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/a/x;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/a/x;-><init>(Lcom/teamspeak/ts3client/d/a/t;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    return-object v0
.end method

.method protected final b(Ljava/lang/String;)V
    .registers 9

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/t;->au:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/t;->au:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    .line 34
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/t;->au:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    .line 35
    if-eq v1, v2, :cond_60

    .line 36
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x7

    invoke-virtual {p1, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 37
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/a/t;->au:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 38
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/t;->au:Landroid/widget/EditText;

    add-int/lit8 v3, v1, 0x3

    add-int/2addr v2, v3

    sub-int v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 48
    :goto_5f
    return-void

    .line 40
    :cond_60
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/t;->au:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v2, v3, :cond_8a

    .line 41
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 44
    :goto_7d
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/t;->au:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/t;->au:Landroid/widget/EditText;

    add-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_5f

    .line 43
    :cond_8a
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_7d
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 99
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 100
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 101
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 105
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 106
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 108
    return-void
.end method
