.class final Lcom/teamspeak/ts3client/d/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/Ts3Application;

.field final synthetic b:Lcom/teamspeak/ts3client/d/e;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/e;Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 3

    .prologue
    .line 147
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 6

    .prologue
    .line 151
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->l(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 196
    :goto_16
    return-void

    .line 153
    :cond_17
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    if-eqz v0, :cond_161

    .line 154
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->l(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1034
    iput-object v1, v0, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 155
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->c(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    .line 1050
    iput v1, v0, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 156
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->n(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    .line 1058
    iput v1, v0, Lcom/teamspeak/ts3client/c/a;->e:I

    .line 157
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->d(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 1106
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->f:Z

    .line 158
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->e(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 2098
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->g:Z

    .line 159
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->f(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 3090
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->h:Z

    .line 160
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->g(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 4082
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->i:Z

    .line 161
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->i(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 5066
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->k:Z

    .line 162
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->h(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 5074
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->j:Z

    .line 163
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->j(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/RadioButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    .line 5114
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->l:Z

    .line 6042
    sget-object v0, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 164
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v1

    .line 7038
    iget v1, v1, Lcom/teamspeak/ts3client/c/a;->b:I

    .line 164
    int-to-long v2, v1

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/data/b/c;->a(JLcom/teamspeak/ts3client/c/a;)Z

    .line 182
    :goto_eb
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 14061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 182
    if-eqz v0, :cond_14b

    .line 184
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 15061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15092
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->w:Lcom/a/b/d/bw;

    .line 184
    invoke-interface {v0}, Lcom/a/b/d/bw;->b()Lcom/a/b/d/bw;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->o(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 15188
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 184
    invoke-interface {v0, v1}, Lcom/a/b/d/bw;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14b

    .line 185
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 16092
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->w:Lcom/a/b/d/bw;

    .line 185
    invoke-interface {v0}, Lcom/a/b/d/bw;->b()Lcom/a/b/d/bw;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->o(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 16188
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 185
    invoke-interface {v0, v1}, Lcom/a/b/d/bw;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 186
    if-eqz v0, :cond_136

    .line 187
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 187
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 18061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 18267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 187
    invoke-virtual {v1, v2, v3, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_removeFromAllowedWhispersFrom(JI)I

    .line 189
    :cond_136
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19092
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->w:Lcom/a/b/d/bw;

    .line 189
    invoke-interface {v0}, Lcom/a/b/d/bw;->b()Lcom/a/b/d/bw;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->o(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 19188
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 189
    invoke-interface {v0, v1}, Lcom/a/b/d/bw;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    :cond_14b
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 20061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 193
    if-eqz v0, :cond_15a

    .line 194
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 21206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 194
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->y()V

    .line 195
    :cond_15a
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/e;->b()V

    goto/16 :goto_16

    .line 166
    :cond_161
    new-instance v0, Lcom/teamspeak/ts3client/c/a;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/c/a;-><init>()V

    .line 167
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->o(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 7188
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 167
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/c/a;->a(Ljava/lang/String;)V

    .line 168
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->l(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 8034
    iput-object v1, v0, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 169
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->c(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    .line 8050
    iput v1, v0, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 170
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->n(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    .line 8058
    iput v1, v0, Lcom/teamspeak/ts3client/c/a;->e:I

    .line 171
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->d(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 8106
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->f:Z

    .line 172
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->e(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 9098
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->g:Z

    .line 173
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->f(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 10090
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->h:Z

    .line 174
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->g(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 11082
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->i:Z

    .line 175
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->i(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 12066
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->k:Z

    .line 176
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->h(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 12074
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->j:Z

    .line 177
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/g;->b:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/e;->j(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/RadioButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    .line 12114
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/c/a;->l:Z

    .line 13042
    sget-object v1, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 178
    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/b/c;->a(Lcom/teamspeak/ts3client/c/a;)J

    .line 14042
    sget-object v0, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 179
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/c;->c()V

    goto/16 :goto_eb
.end method
