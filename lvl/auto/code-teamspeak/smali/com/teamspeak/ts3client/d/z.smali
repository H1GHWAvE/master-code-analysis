.class public final Lcom/teamspeak/ts3client/d/z;
.super Landroid/support/v4/app/ax;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# static fields
.field private static at:Ljava/util/regex/Pattern;


# instance fields
.field private aA:Landroid/widget/TextView;

.field private aB:Landroid/widget/TextView;

.field private aC:Landroid/widget/TextView;

.field private aD:Lcom/teamspeak/ts3client/Ts3Application;

.field private aE:Ljava/lang/Thread;

.field private aF:Z

.field private aG:Landroid/widget/TextView;

.field private aH:Landroid/widget/TextView;

.field private aI:Landroid/widget/TextView;

.field private aJ:Landroid/widget/TextView;

.field private aK:Landroid/widget/TextView;

.field private aL:Landroid/widget/TextView;

.field private aM:Landroid/widget/TextView;

.field private aN:Landroid/widget/TextView;

.field private aO:Landroid/widget/TextView;

.field private aP:Landroid/widget/TextView;

.field private aQ:Landroid/widget/TextView;

.field private aR:Landroid/widget/TextView;

.field private au:Landroid/widget/TextView;

.field private av:Landroid/widget/TextView;

.field private aw:Landroid/widget/TextView;

.field private ax:Landroid/widget/TextView;

.field private ay:Landroid/widget/TextView;

.field private az:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 33
    const-string v0, ".*(\\[Build: (\\d+)\\]).*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/teamspeak/ts3client/d/z;->at:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/z;->aF:Z

    return-void
.end method

.method private A()V
    .registers 4

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/z;->l()Z

    move-result v0

    if-nez v0, :cond_7

    .line 227
    :goto_6
    return-void

    .line 198
    :cond_7
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00%"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 199
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/z;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    new-instance v2, Lcom/teamspeak/ts3client/d/ac;

    invoke-direct {v2, p0, v0}, Lcom/teamspeak/ts3client/d/ac;-><init>(Lcom/teamspeak/ts3client/d/z;Ljava/text/DecimalFormat;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_6
.end method

.method private B()Ljava/lang/String;
    .registers 5

    .prologue
    .line 230
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 31061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 31234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 230
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 32061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 32267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 230
    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getServerLicenseType(J)I

    move-result v0

    .line 231
    packed-switch v0, :pswitch_data_32

    .line 241
    const-string v0, "Error"

    :goto_15
    return-object v0

    .line 233
    :pswitch_16
    const-string v0, "license.no"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_15

    .line 235
    :pswitch_1d
    const-string v0, "license.licensed"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_15

    .line 237
    :pswitch_24
    const-string v0, "license.offline"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_15

    .line 239
    :pswitch_2b
    const-string v0, "license.npl"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_15

    .line 231
    :pswitch_data_32
    .packed-switch 0x0
        :pswitch_16
        :pswitch_1d
        :pswitch_24
        :pswitch_2b
    .end packed-switch
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/z;)Z
    .registers 2

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/z;->aF:Z

    return v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->ay:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->au:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aB:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aC:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aG:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aH:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aJ:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aI:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aK:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aL:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aM:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic n(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aN:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic o(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aO:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic p(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aP:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic q(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aQ:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic r(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aR:Landroid/widget/TextView;

    return-object v0
.end method

.method private y()V
    .registers 11

    .prologue
    const/4 v6, 0x2

    .line 165
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 165
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 20061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 165
    sget-object v1, Lcom/teamspeak/ts3client/jni/i;->e:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v0

    .line 166
    sget-object v1, Lcom/teamspeak/ts3client/d/z;->at:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 167
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    .line 168
    if-eqz v2, :cond_63

    .line 171
    invoke-virtual {v1, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x4e20

    cmp-long v2, v2, v4

    if-lez v2, :cond_63

    .line 172
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v6}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-virtual {v1, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 174
    :cond_63
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->av:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aw:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 21234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 176
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 22267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 176
    sget-object v4, Lcom/teamspeak/ts3client/jni/i;->d:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->ax:Landroid/widget/TextView;

    .line 23230
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 24061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 24234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 23230
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 25061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 25267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 23230
    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getServerLicenseType(J)I

    move-result v0

    .line 23231
    packed-switch v0, :pswitch_data_f0

    .line 23241
    const-string v0, "Error"

    .line 177
    :goto_96
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aA:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 26061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 26360
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 27211
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/ab;->o:Ljava/lang/String;

    .line 178
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 28061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 28360
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 29219
    iget v2, v2, Lcom/teamspeak/ts3client/data/ab;->p:I

    .line 178
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->az:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 30360
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 31055
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 179
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    return-void

    .line 23233
    :pswitch_d3
    const-string v0, "license.no"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_96

    .line 23235
    :pswitch_da
    const-string v0, "license.licensed"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_96

    .line 23237
    :pswitch_e1
    const-string v0, "license.offline"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_96

    .line 23239
    :pswitch_e8
    const-string v0, "license.npl"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_96

    .line 23231
    nop

    :pswitch_data_f0
    .packed-switch 0x0
        :pswitch_d3
        :pswitch_da
        :pswitch_e1
        :pswitch_e8
    .end packed-switch
.end method

.method private z()V
    .registers 3

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/z;->l()Z

    move-result v0

    if-nez v0, :cond_7

    .line 193
    :goto_6
    return-void

    .line 185
    :cond_7
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/z;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/d/ab;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/d/ab;-><init>(Lcom/teamspeak/ts3client/d/z;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_6
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 6

    .prologue
    .line 75
    const v0, 0x7f030052

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    .line 1207
    iget-object v1, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 76
    const-string v2, "connectioninfo.title"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 77
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 78
    const v1, 0x7f0c019f

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->au:Landroid/widget/TextView;

    .line 79
    const v1, 0x7f0c01a1

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->av:Landroid/widget/TextView;

    .line 80
    const v1, 0x7f0c01a3

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aw:Landroid/widget/TextView;

    .line 81
    const v1, 0x7f0c01a5

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->ax:Landroid/widget/TextView;

    .line 82
    const v1, 0x7f0c01a7

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->ay:Landroid/widget/TextView;

    .line 83
    const v1, 0x7f0c01a9

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->az:Landroid/widget/TextView;

    .line 84
    const v1, 0x7f0c01ab

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aA:Landroid/widget/TextView;

    .line 85
    const v1, 0x7f0c01ad

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aB:Landroid/widget/TextView;

    .line 86
    const v1, 0x7f0c01af

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aC:Landroid/widget/TextView;

    .line 88
    const v1, 0x7f0c01b3

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aG:Landroid/widget/TextView;

    .line 89
    const v1, 0x7f0c01b4

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aH:Landroid/widget/TextView;

    .line 91
    const v1, 0x7f0c01b6

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aJ:Landroid/widget/TextView;

    .line 92
    const v1, 0x7f0c01b7

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aI:Landroid/widget/TextView;

    .line 94
    const v1, 0x7f0c01b9

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aK:Landroid/widget/TextView;

    .line 95
    const v1, 0x7f0c01ba

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aL:Landroid/widget/TextView;

    .line 97
    const v1, 0x7f0c01bc

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aM:Landroid/widget/TextView;

    .line 98
    const v1, 0x7f0c01bd

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aN:Landroid/widget/TextView;

    .line 100
    const v1, 0x7f0c01bf

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aO:Landroid/widget/TextView;

    .line 101
    const v1, 0x7f0c01c0

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aP:Landroid/widget/TextView;

    .line 103
    const v1, 0x7f0c01c2

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aQ:Landroid/widget/TextView;

    .line 104
    const v1, 0x7f0c01c3

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aR:Landroid/widget/TextView;

    .line 106
    const-string v1, "connectioninfo.name"

    const v2, 0x7f0c019e

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 107
    const-string v1, "connectioninfo.version"

    const v2, 0x7f0c01a0

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 108
    const-string v1, "connectioninfo.platform"

    const v2, 0x7f0c01a2

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 109
    const-string v1, "connectioninfo.license"

    const v2, 0x7f0c01a4

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 110
    const-string v1, "connectioninfo.uptime"

    const v2, 0x7f0c01a6

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 111
    const-string v1, "connectioninfo.address"

    const v2, 0x7f0c01a8

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 112
    const-string v1, "connectioninfo.ip"

    const v2, 0x7f0c01aa

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 113
    const-string v1, "connectioninfo.ping"

    const v2, 0x7f0c01ac

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 114
    const-string v1, "connectioninfo.packetloss"

    const v2, 0x7f0c01ae

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 115
    const-string v1, "connectioninfo.packettrans"

    const v2, 0x7f0c01b2

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 116
    const-string v1, "connectioninfo.bytetrans"

    const v2, 0x7f0c01b5

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 117
    const-string v1, "connectioninfo.bandwidthsec"

    const v2, 0x7f0c01b8

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 118
    const-string v1, "connectioninfo.bandwidthmin"

    const v2, 0x7f0c01bb

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 119
    const-string v1, "connectioninfo.filebandwidth"

    const v2, 0x7f0c01be

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 120
    const-string v1, "connectioninfo.filebyte"

    const v2, 0x7f0c01c1

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 121
    const-string v1, "connectioninfo.tablein"

    const v2, 0x7f0c01b0

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 122
    const-string v1, "connectioninfo.tableout"

    const v2, 0x7f0c01b1

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 124
    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/support/v4/app/ax;->a(Landroid/os/Bundle;)V

    .line 69
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 5

    .prologue
    .line 247
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerUpdated;

    if-eqz v0, :cond_16

    .line 33183
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/z;->l()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 33185
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/z;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/d/ab;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/d/ab;-><init>(Lcom/teamspeak/ts3client/d/z;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 250
    :cond_16
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerConnectionInfo;

    if-eqz v0, :cond_33

    .line 33196
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/z;->l()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 33198
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00%"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 33199
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/z;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    new-instance v2, Lcom/teamspeak/ts3client/d/ac;

    invoke-direct {v2, p0, v0}, Lcom/teamspeak/ts3client/d/ac;-><init>(Lcom/teamspeak/ts3client/d/z;Ljava/text/DecimalFormat;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 253
    :cond_33
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;

    if-eqz v0, :cond_55

    .line 254
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;

    .line 34033
    iget v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->a:I

    .line 255
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 34061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 34250
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 255
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->x:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    if-ne v0, v1, :cond_55

    .line 256
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/z;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/d/ad;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/d/ad;-><init>(Lcom/teamspeak/ts3client/d/z;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 264
    :cond_55
    return-void
.end method

.method public final b()V
    .registers 1

    .prologue
    .line 62
    invoke-super {p0}, Landroid/support/v4/app/ax;->b()V

    .line 63
    return-void
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 269
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 270
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 271
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 276
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 277
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 278
    return-void
.end method

.method public final s()V
    .registers 12

    .prologue
    const/4 v10, 0x1

    const/4 v6, 0x2

    .line 130
    invoke-super {p0}, Landroid/support/v4/app/ax;->s()V

    .line 2165
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 2165
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 2165
    sget-object v1, Lcom/teamspeak/ts3client/jni/i;->e:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v0

    .line 2166
    sget-object v1, Lcom/teamspeak/ts3client/d/z;->at:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 2167
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    .line 2168
    if-eqz v2, :cond_66

    .line 2171
    invoke-virtual {v1, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x4e20

    cmp-long v2, v2, v4

    if-lez v2, :cond_66

    .line 2172
    invoke-virtual {v1, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v6}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-virtual {v1, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2174
    :cond_66
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->av:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2176
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aw:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 2176
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 2176
    sget-object v4, Lcom/teamspeak/ts3client/jni/i;->d:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2177
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->ax:Landroid/widget/TextView;

    .line 7230
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 7230
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 7230
    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getServerLicenseType(J)I

    move-result v0

    .line 7231
    packed-switch v0, :pswitch_data_11e

    .line 7241
    const-string v0, "Error"

    .line 2177
    :goto_99
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2178
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aA:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10360
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 11211
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/ab;->o:Ljava/lang/String;

    .line 2178
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12360
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 13219
    iget v2, v2, Lcom/teamspeak/ts3client/data/ab;->p:I

    .line 2178
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2179
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->az:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 14061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 14360
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 15055
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 2179
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 15061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 132
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 133
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 16234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 133
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 133
    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestServerVariables(J)I

    .line 134
    iput-boolean v10, p0, Lcom/teamspeak/ts3client/d/z;->aF:Z

    .line 135
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/teamspeak/ts3client/d/aa;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/d/aa;-><init>(Lcom/teamspeak/ts3client/d/z;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aE:Ljava/lang/Thread;

    .line 151
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aE:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 152
    return-void

    .line 7233
    :pswitch_101
    const-string v0, "license.no"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_99

    .line 7235
    :pswitch_108
    const-string v0, "license.licensed"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_99

    .line 7237
    :pswitch_10f
    const-string v0, "license.offline"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_99

    .line 7239
    :pswitch_116
    const-string v0, "license.npl"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_99

    .line 7231
    :pswitch_data_11e
    .packed-switch 0x0
        :pswitch_101
        :pswitch_108
        :pswitch_10f
        :pswitch_116
    .end packed-switch
.end method

.method public final t()V
    .registers 2

    .prologue
    .line 157
    invoke-super {p0}, Landroid/support/v4/app/ax;->t()V

    .line 158
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aD:Lcom/teamspeak/ts3client/Ts3Application;

    .line 18061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 18238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 158
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/z;->aF:Z

    .line 160
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/z;->aE:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 161
    return-void
.end method
