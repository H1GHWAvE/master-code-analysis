.class final Lcom/teamspeak/ts3client/d/d/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/d/d/d;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/d/d;)V
    .registers 2

    .prologue
    .line 53
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/d/e;->a:Lcom/teamspeak/ts3client/d/d/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .registers 4

    .prologue
    .line 57
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_21

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/e;->a:Lcom/teamspeak/ts3client/d/d/d;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/d/d;->a(Lcom/teamspeak/ts3client/d/d/d;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_21

    .line 58
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/e;->a:Lcom/teamspeak/ts3client/d/d/d;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/d/d;->b(Lcom/teamspeak/ts3client/d/d/d;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 61
    :goto_20
    return-void

    .line 60
    :cond_21
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/e;->a:Lcom/teamspeak/ts3client/d/d/d;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/d/d;->b(Lcom/teamspeak/ts3client/d/d/d;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_20
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    .prologue
    .line 67
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    .prologue
    .line 73
    return-void
.end method
