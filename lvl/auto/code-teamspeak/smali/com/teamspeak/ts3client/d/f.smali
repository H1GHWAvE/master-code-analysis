.class final Lcom/teamspeak/ts3client/d/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/d/e;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/e;)V
    .registers 2

    .prologue
    .line 86
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 10

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 91
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->a(Lcom/teamspeak/ts3client/d/e;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 92
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->b(Lcom/teamspeak/ts3client/d/e;)Z

    .line 126
    :cond_10
    :goto_10
    return-void

    .line 96
    :cond_11
    if-nez p3, :cond_5b

    .line 97
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->c(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 98
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->d(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 99
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->e(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 100
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->f(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 101
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->g(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 102
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->h(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 103
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->i(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 104
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->j(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 106
    :cond_5b
    if-ne p3, v2, :cond_a5

    .line 107
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->c(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 108
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->d(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 109
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->e(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 110
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->f(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 111
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->g(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 112
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->h(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 113
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->i(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 114
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->k(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 116
    :cond_a5
    if-ne p3, v3, :cond_10

    .line 117
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->c(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 118
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->d(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 119
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->e(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 120
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->f(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 121
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->g(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 122
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->h(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 123
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->i(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 124
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/f;->a:Lcom/teamspeak/ts3client/d/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/e;->j(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_10
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 2

    .prologue
    .line 132
    return-void
.end method
