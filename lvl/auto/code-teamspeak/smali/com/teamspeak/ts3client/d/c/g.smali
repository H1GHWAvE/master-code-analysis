.class final Lcom/teamspeak/ts3client/d/c/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/Ts3Application;

.field final synthetic b:Landroid/widget/EditText;

.field final synthetic c:Landroid/app/Dialog;

.field final synthetic d:Lcom/teamspeak/ts3client/d/c/d;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/c/d;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/EditText;Landroid/app/Dialog;)V
    .registers 5

    .prologue
    .line 244
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/c/g;->d:Lcom/teamspeak/ts3client/d/c/d;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/c/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    iput-object p3, p0, Lcom/teamspeak/ts3client/d/c/g;->b:Landroid/widget/EditText;

    iput-object p4, p0, Lcom/teamspeak/ts3client/d/c/g;->c:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 7

    .prologue
    .line 248
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 248
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 248
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->M:Lcom/teamspeak/ts3client/jni/d;

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 249
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 249
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 249
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->N:Lcom/teamspeak/ts3client/jni/d;

    iget-object v4, p0, Lcom/teamspeak/ts3client/d/c/g;->b:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;Ljava/lang/String;)I

    .line 250
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 250
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 250
    const-string v1, "SetAway"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 251
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 8243
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 251
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 252
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 252
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c/g;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 10251
    iput-object v1, v0, Lcom/teamspeak/ts3client/data/ab;->t:Ljava/lang/String;

    .line 253
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c/g;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aV:Lcom/teamspeak/ts3client/jni/h;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 254
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c/g;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 255
    return-void
.end method
