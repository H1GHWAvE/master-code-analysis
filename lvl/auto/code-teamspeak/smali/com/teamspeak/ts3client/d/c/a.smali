.class public final Lcom/teamspeak/ts3client/d/c/a;
.super Lcom/teamspeak/ts3client/d/c/i;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/teamspeak/ts3client/d/c/i;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 1

    .prologue
    .line 28
    return-void
.end method

.method public final a(Landroid/view/Menu;Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 10

    .prologue
    const v6, 0x7f020080

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 33
    const v0, 0x7f0200af

    const-string v1, "menu.settings"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1, v3, v3}, Lcom/teamspeak/ts3client/d/c/a;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 34
    const v0, 0x7f02007f

    const-string v1, "menu.identity"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1, v4, v3}, Lcom/teamspeak/ts3client/d/c/a;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 35
    const v0, 0x7f020060

    const-string v1, "menu.contact"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1, v5, v3}, Lcom/teamspeak/ts3client/d/c/a;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 36
    const v0, 0x7f020064

    const-string v1, "menu.copyright"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {p1, v0, v1, v2, v4}, Lcom/teamspeak/ts3client/d/c/a;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 37
    const v0, 0x7f020061

    const-string v1, "menu.contactbug"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {p1, v0, v1, v2, v4}, Lcom/teamspeak/ts3client/d/c/a;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 38
    const-string v0, "menu.changelog"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {p1, v6, v0, v1, v3}, Lcom/teamspeak/ts3client/d/c/a;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 39
    const-string v0, "menu.licenseagreement"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x7

    invoke-static {p1, v6, v0, v1, v5}, Lcom/teamspeak/ts3client/d/c/a;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 41
    return-void
.end method

.method public final a(Landroid/view/MenuItem;Lcom/teamspeak/ts3client/Ts3Application;)Z
    .registers 9

    .prologue
    const/16 v4, 0x1001

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 45
    .line 1107
    iget-object v2, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 45
    invoke-virtual {v2}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v2

    .line 46
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_16c

    move v0, v1

    .line 133
    :goto_16
    return v0

    .line 2107
    :pswitch_17
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 49
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v3, "Settings"

    invoke-virtual {v1, v3}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_39

    .line 50
    new-instance v1, Lcom/teamspeak/ts3client/f/as;

    invoke-direct {v1}, Lcom/teamspeak/ts3client/f/as;-><init>()V

    const-string v3, "Settings"

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 53
    :goto_2f
    invoke-virtual {v2}, Landroid/support/v4/app/cd;->f()Landroid/support/v4/app/cd;

    .line 54
    invoke-virtual {v2, v4}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 55
    invoke-virtual {v2}, Landroid/support/v4/app/cd;->i()I

    goto :goto_16

    .line 3107
    :cond_39
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 52
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v3, "Settings"

    invoke-virtual {v1, v3}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/support/v4/app/cd;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    goto :goto_2f

    .line 4107
    :pswitch_49
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 59
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v3, "IdentManager"

    invoke-virtual {v1, v3}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_6b

    .line 60
    new-instance v1, Lcom/teamspeak/ts3client/e/b;

    invoke-direct {v1}, Lcom/teamspeak/ts3client/e/b;-><init>()V

    const-string v3, "IdentManager"

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 63
    :goto_61
    invoke-virtual {v2}, Landroid/support/v4/app/cd;->f()Landroid/support/v4/app/cd;

    .line 64
    invoke-virtual {v2, v4}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 65
    invoke-virtual {v2}, Landroid/support/v4/app/cd;->i()I

    goto :goto_16

    .line 5107
    :cond_6b
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 62
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v3, "IdentManager"

    invoke-virtual {v1, v3}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/support/v4/app/cd;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    goto :goto_61

    .line 6107
    :pswitch_7b
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 69
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v3, "Contact"

    invoke-virtual {v1, v3}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_9e

    .line 70
    new-instance v1, Lcom/teamspeak/ts3client/c/b;

    invoke-direct {v1}, Lcom/teamspeak/ts3client/c/b;-><init>()V

    const-string v3, "Contact"

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 73
    :goto_93
    invoke-virtual {v2}, Landroid/support/v4/app/cd;->f()Landroid/support/v4/app/cd;

    .line 74
    invoke-virtual {v2, v4}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 75
    invoke-virtual {v2}, Landroid/support/v4/app/cd;->i()I

    goto/16 :goto_16

    .line 7107
    :cond_9e
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 72
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v3, "Contact"

    invoke-virtual {v1, v3}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/support/v4/app/cd;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    goto :goto_93

    .line 80
    :pswitch_ae
    new-instance v1, Landroid/app/Dialog;

    .line 8107
    iget-object v2, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 80
    const v3, 0x7f070080

    invoke-direct {v1, v2, v3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 81
    invoke-static {v1}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 82
    new-instance v2, Landroid/widget/LinearLayout;

    .line 9107
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 82
    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 83
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 84
    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 85
    new-instance v3, Landroid/webkit/WebView;

    .line 10107
    iget-object v4, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 85
    invoke-direct {v3, v4}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 86
    const-string v4, "file:///android_asset/copyright/copyright.html"

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 87
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 88
    const-string v2, "menu.copyright.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 89
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_16

    .line 93
    :pswitch_e5
    new-instance v2, Landroid/app/AlertDialog$Builder;

    .line 11107
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 93
    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 94
    const-string v3, "contactbug.info"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 95
    const-string v3, "contactbug.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 96
    const/4 v3, -0x2

    const-string v4, "button.cancel"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/teamspeak/ts3client/d/c/b;

    invoke-direct {v5, p0, v2}, Lcom/teamspeak/ts3client/d/c/b;-><init>(Lcom/teamspeak/ts3client/d/c/a;Landroid/app/AlertDialog;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 103
    const/4 v3, -0x1

    const-string v4, "button.open"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/teamspeak/ts3client/d/c/c;

    invoke-direct {v5, p0, p2, v2}, Lcom/teamspeak/ts3client/d/c/c;-><init>(Lcom/teamspeak/ts3client/d/c/a;Lcom/teamspeak/ts3client/Ts3Application;Landroid/app/AlertDialog;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 114
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 115
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_16

    .line 12107
    :pswitch_128
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 13070
    iget-object v1, v1, Lcom/teamspeak/ts3client/StartGUIFragment;->q:Lcom/teamspeak/ts3client/b;

    .line 119
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/b;->show()V

    goto/16 :goto_16

    .line 122
    :pswitch_131
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 13093
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 122
    const-string v3, "la_url"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 123
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_156

    .line 14093
    iget-object v2, p2, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 124
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "LicenseAgreement"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_16

    .line 127
    :cond_156
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 128
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 14107
    iget-object v2, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 129
    invoke-virtual {v2, v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_16

    .line 46
    nop

    :pswitch_data_16c
    .packed-switch 0x1
        :pswitch_17
        :pswitch_49
        :pswitch_7b
        :pswitch_ae
        :pswitch_e5
        :pswitch_128
        :pswitch_131
    .end packed-switch
.end method
