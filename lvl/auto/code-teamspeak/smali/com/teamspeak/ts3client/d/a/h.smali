.class final Lcom/teamspeak/ts3client/d/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/d/a/a;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/a/a;)V
    .registers 2

    .prologue
    .line 171
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/h;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 12

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 177
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 1061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1165
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->f:Lcom/teamspeak/ts3client/data/g;

    .line 2080
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    .line 177
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 178
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 179
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1d
    :goto_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 2087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 180
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/a/h;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    .line 3087
    iget-wide v6, v3, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 180
    cmp-long v3, v4, v6

    if-eqz v3, :cond_1d

    .line 181
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1d

    .line 183
    :cond_3b
    new-instance v2, Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 184
    invoke-static {v2}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 185
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 186
    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 188
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f030046

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 189
    const v0, 0x7f0c0190

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 190
    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 192
    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x1090009

    invoke-direct {v5, v6, v7, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 193
    new-instance v1, Landroid/widget/Button;

    iget-object v6, p0, Lcom/teamspeak/ts3client/d/a/h;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/d/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 194
    invoke-virtual {v1, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 195
    new-instance v6, Landroid/widget/Button;

    iget-object v7, p0, Lcom/teamspeak/ts3client/d/a/h;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/d/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 196
    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 197
    new-instance v7, Lcom/teamspeak/ts3client/d/a/i;

    invoke-direct {v7, p0, v1}, Lcom/teamspeak/ts3client/d/a/i;-><init>(Lcom/teamspeak/ts3client/d/a/h;Landroid/widget/Button;)V

    invoke-virtual {v0, v7}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 214
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 215
    const-string v4, "dialog.channel.move.info1"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 216
    new-instance v4, Lcom/teamspeak/ts3client/d/a/j;

    invoke-direct {v4, p0, v5, v0, v2}, Lcom/teamspeak/ts3client/d/a/j;-><init>(Lcom/teamspeak/ts3client/d/a/h;Landroid/widget/ArrayAdapter;Landroid/widget/Spinner;Landroid/app/Dialog;)V

    invoke-virtual {v6, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 232
    const-string v4, "dialog.channel.move.info2"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 233
    new-instance v4, Lcom/teamspeak/ts3client/d/a/k;

    invoke-direct {v4, p0, v5, v0, v2}, Lcom/teamspeak/ts3client/d/a/k;-><init>(Lcom/teamspeak/ts3client/d/a/h;Landroid/widget/ArrayAdapter;Landroid/widget/Spinner;Landroid/app/Dialog;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 246
    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 247
    const-string v0, "dialog.channel.move.info"

    new-array v1, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/teamspeak/ts3client/d/a/h;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    .line 3103
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 247
    aput-object v3, v1, v8

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 248
    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 249
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/h;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/a/a;->b()V

    .line 250
    return-void
.end method
