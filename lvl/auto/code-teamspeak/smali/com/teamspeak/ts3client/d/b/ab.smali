.class public final Lcom/teamspeak/ts3client/d/b/ab;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field private at:Lcom/teamspeak/ts3client/data/c;

.field private au:Landroid/widget/Button;

.field private av:Landroid/widget/TextView;

.field private aw:Landroid/widget/TextView;

.field private ax:Landroid/widget/TextView;

.field private ay:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/c;)V
    .registers 2

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/ab;->at:Lcom/teamspeak/ts3client/data/c;

    .line 30
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ab;->ax:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ab;->ay:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/b/ab;)Lcom/teamspeak/ts3client/data/c;
    .registers 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ab;->at:Lcom/teamspeak/ts3client/data/c;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ab;->aw:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10

    .prologue
    const v6, 0x7f0c0149

    .line 34
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    .line 35
    const v1, 0x7f03002d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1207
    iget-object v2, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 36
    const-string v3, "clientdialog.ban.info"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 37
    const-string v2, "clientdialog.ban.name"

    const v3, 0x7f0c0142

    invoke-static {v2, v1, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 38
    const-string v2, "clientdialog.ban.reason"

    const v3, 0x7f0c0144

    invoke-static {v2, v1, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 39
    const-string v2, "clientdialog.ban.duration"

    const v3, 0x7f0c0146

    invoke-static {v2, v1, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 40
    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    .line 41
    const-string v3, "clientdialog.ban.duration.array"

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/b/ab;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    const/4 v5, 0x4

    invoke-static {v3, v4, v5}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 43
    const v2, 0x7f0c014a

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/ab;->au:Landroid/widget/Button;

    .line 44
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/ab;->au:Landroid/widget/Button;

    const-string v3, "clientdialog.ban.button"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 45
    const v2, 0x7f0c0143

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/ab;->av:Landroid/widget/TextView;

    .line 46
    const v2, 0x7f0c0145

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/ab;->aw:Landroid/widget/TextView;

    .line 47
    const v2, 0x7f0c0148

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/ab;->ax:Landroid/widget/TextView;

    .line 48
    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/ab;->ay:Landroid/widget/Spinner;

    .line 50
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/ab;->av:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/teamspeak/ts3client/d/b/ab;->at:Lcom/teamspeak/ts3client/data/c;

    .line 2203
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/teamspeak/ts3client/d/b/ab;->at:Lcom/teamspeak/ts3client/data/c;

    .line 2235
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/ab;->au:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/b/ac;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/b/ac;-><init>(Lcom/teamspeak/ts3client/d/b/ab;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    return-object v1
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 81
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 82
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 83
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 87
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 88
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 90
    return-void
.end method
