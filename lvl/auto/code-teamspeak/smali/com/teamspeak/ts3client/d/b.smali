.class final Lcom/teamspeak/ts3client/d/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/d/a;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/a;)V
    .registers 2

    .prologue
    .line 149
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 7

    .prologue
    .line 151
    :goto_0
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->a(Lcom/teamspeak/ts3client/d/a;)Z

    move-result v0

    if-eqz v0, :cond_96

    .line 152
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 1235
    iget v0, v0, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 152
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 2061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2242
    iget v1, v1, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 152
    if-eq v0, v1, :cond_5a

    .line 153
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 153
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 4061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 153
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 5235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 153
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "connectioninfo_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v5}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v5

    .line 6235
    iget v5, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 153
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestConnectionInfo(JILjava/lang/String;)I

    .line 159
    :goto_52
    const-wide/16 v0, 0x7d0

    :try_start_54
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_57
    .catch Ljava/lang/InterruptedException; {:try_start_54 .. :try_end_57} :catch_58

    goto :goto_0

    .line 162
    :catch_58
    move-exception v0

    goto :goto_0

    .line 155
    :cond_5a
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 155
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 8061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 155
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 9235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 155
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "connectioninfo_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v5}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v5

    .line 10235
    iget v5, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 155
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestConnectionInfo(JILjava/lang/String;)I

    .line 156
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b;->a:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->d(Lcom/teamspeak/ts3client/d/a;)V

    goto :goto_52

    .line 164
    :cond_96
    return-void
.end method
