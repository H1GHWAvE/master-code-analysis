.class final Lcom/teamspeak/ts3client/d/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/text/DecimalFormat;

.field final synthetic b:Ljava/text/DecimalFormat;

.field final synthetic c:Lcom/teamspeak/ts3client/d/a;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/a;Ljava/text/DecimalFormat;Ljava/text/DecimalFormat;)V
    .registers 4

    .prologue
    .line 190
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/c;->a:Ljava/text/DecimalFormat;

    iput-object p3, p0, Lcom/teamspeak/ts3client/d/c;->b:Ljava/text/DecimalFormat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 12

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 194
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->e(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 1203
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 194
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->f(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 2061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 195
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 3061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 195
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v4

    .line 4235
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 195
    sget-object v5, Lcom/teamspeak/ts3client/jni/f;->c:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/d/w;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->g(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 5061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 196
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 6061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 196
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v4

    .line 7235
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 196
    sget-object v5, Lcom/teamspeak/ts3client/jni/f;->d:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/d/w;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->h(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 8061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 197
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 9061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 197
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 10235
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 197
    sget-object v6, Lcom/teamspeak/ts3client/jni/f;->a:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms \u00b1 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->a:Ljava/text/DecimalFormat;

    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 11061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11234
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 197
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v4

    .line 12061
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12267
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 197
    iget-object v6, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v6}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v6

    .line 13235
    iget v6, v6, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 197
    sget-object v7, Lcom/teamspeak/ts3client/jni/f;->b:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/f;)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 14235
    iget v0, v0, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 198
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 15061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15242
    iget v1, v1, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 198
    if-ne v0, v1, :cond_15f

    .line 199
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 16061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 16234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 199
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 17061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 199
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 18235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 199
    sget-object v4, Lcom/teamspeak/ts3client/jni/f;->h:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v0

    .line 200
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->i(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v4

    .line 19061
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19234
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 200
    iget-object v5, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v5}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v5

    .line 20061
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20267
    iget-wide v6, v5, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 200
    iget-object v5, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v5}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v5

    .line 21235
    iget v5, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 200
    sget-object v8, Lcom/teamspeak/ts3client/jni/f;->g:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v4, v6, v7, v5, v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/f;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_470

    const-string v0, "9987"

    :goto_154
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    :cond_15f
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 22061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 22234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 202
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 23061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 202
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 24235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 202
    sget-object v4, Lcom/teamspeak/ts3client/jni/f;->e:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/f;)Ljava/lang/String;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_476

    .line 204
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->j(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 25061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 25234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 204
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 26061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 26267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 204
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 27235
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 204
    sget-object v6, Lcom/teamspeak/ts3client/jni/f;->f:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    :goto_1c5
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->k(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->b:Ljava/text/DecimalFormat;

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 28061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 28234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 207
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 29061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 29267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 207
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 30235
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 207
    sget-object v6, Lcom/teamspeak/ts3client/jni/f;->F:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/f;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->l(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->b:Ljava/text/DecimalFormat;

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 31061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 31234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 208
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 32061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 32267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 208
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 33235
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 208
    sget-object v6, Lcom/teamspeak/ts3client/jni/f;->J:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/f;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->m(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 34061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 34234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 209
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 35061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 35267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 209
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v4

    .line 36235
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 209
    sget-object v5, Lcom/teamspeak/ts3client/jni/f;->t:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v10}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->n(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 37061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 37234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 210
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 38061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 38267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 210
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v4

    .line 39235
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 210
    sget-object v5, Lcom/teamspeak/ts3client/jni/f;->l:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v10}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->o(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 40061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 40234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 211
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 41061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 41267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 211
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v4

    .line 42235
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 211
    sget-object v5, Lcom/teamspeak/ts3client/jni/f;->x:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v9}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->p(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 43061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 43234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 212
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 44061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 44267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 212
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v4

    .line 45235
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 212
    sget-object v5, Lcom/teamspeak/ts3client/jni/f;->p:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v9}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->q(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 46061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 46234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 213
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 47061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 47267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 213
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 48235
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 213
    sget-object v6, Lcom/teamspeak/ts3client/jni/f;->V:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v9}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->r(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 49061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 49234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 214
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 50061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50062
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 214
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 50063
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 214
    sget-object v6, Lcom/teamspeak/ts3client/jni/f;->N:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v9}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->s(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 50064
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50065
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 215
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 50066
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50067
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 215
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 50068
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 215
    sget-object v6, Lcom/teamspeak/ts3client/jni/f;->Z:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v9}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->t(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 50069
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50070
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 216
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 50071
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50072
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 216
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 50073
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 216
    sget-object v6, Lcom/teamspeak/ts3client/jni/f;->R:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v9}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->u(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 50074
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50075
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 217
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 50076
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50077
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 217
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 50078
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 217
    sget-object v6, Lcom/teamspeak/ts3client/jni/f;->al:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v9}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->v(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 50079
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50080
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 218
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 50081
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50082
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 218
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/a;->b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 50083
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 218
    sget-object v6, Lcom/teamspeak/ts3client/jni/f;->ak:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v9}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    return-void

    .line 200
    :cond_470
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_154

    .line 206
    :cond_476
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/c;->c:Lcom/teamspeak/ts3client/d/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a;->j(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "connectioninfoclient.iphidden"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1c5
.end method
