.class public final Lcom/teamspeak/ts3client/d/t;
.super Landroid/app/Dialog;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/customs/f;
.implements Lcom/teamspeak/ts3client/d/j;


# instance fields
.field public a:Lcom/teamspeak/ts3client/d/k;

.field public b:Lcom/teamspeak/ts3client/d/k;

.field public c:I

.field public d:Lcom/teamspeak/ts3client/d/k;

.field public e:Lcom/teamspeak/ts3client/d/l;

.field public f:Ljava/lang/String;

.field private g:Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;

.field private h:Ljava/lang/String;

.field private i:Landroid/widget/Button;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 50
    const v0, 0x7f0700b4

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 51
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/d/t;->a(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 3

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 56
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/d/t;->a(Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .registers 4

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 46
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/d/t;->a(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/t;)Lcom/teamspeak/ts3client/d/k;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->b:Lcom/teamspeak/ts3client/d/k;

    return-object v0
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 60
    iput p1, p0, Lcom/teamspeak/ts3client/d/t;->c:I

    .line 61
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 64
    invoke-virtual {p0, v4}, Lcom/teamspeak/ts3client/d/t;->setCancelable(Z)V

    .line 65
    const-string v0, "License Agreement"

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/d/t;->setTitle(Ljava/lang/CharSequence;)V

    .line 66
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 67
    const v1, 0x7f030043

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 68
    const v0, 0x7f0c018e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/t;->g:Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;

    .line 69
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->g:Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;

    new-instance v2, Lcom/teamspeak/ts3client/d/u;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/u;-><init>(Lcom/teamspeak/ts3client/d/t;)V

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 79
    const v0, 0x7f0c018b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 80
    const-string v2, "button.reject"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 81
    new-instance v2, Lcom/teamspeak/ts3client/d/v;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/v;-><init>(Lcom/teamspeak/ts3client/d/t;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    const v0, 0x7f0c018c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 92
    const-string v2, "button.browser"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 93
    new-instance v2, Lcom/teamspeak/ts3client/d/w;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/w;-><init>(Lcom/teamspeak/ts3client/d/t;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    const v0, 0x7f0c018d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/t;->i:Landroid/widget/Button;

    .line 103
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->i:Landroid/widget/Button;

    const-string v2, "button.accept"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->i:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/x;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/x;-><init>(Lcom/teamspeak/ts3client/d/t;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->g:Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;

    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;->setOnBottomReachedListener(Lcom/teamspeak/ts3client/customs/f;)V

    .line 115
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v0}, Lcom/teamspeak/ts3client/d/t;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/t;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 31
    .line 1119
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/t;->h:Ljava/lang/String;

    .line 1120
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->g:Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;

    const-string v1, "text/html; charset=UTF-8"

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1121
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->e:Lcom/teamspeak/ts3client/d/l;

    invoke-interface {v0}, Lcom/teamspeak/ts3client/d/l;->n()V

    .line 31
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 119
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/t;->h:Ljava/lang/String;

    .line 120
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->g:Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;

    const-string v1, "text/html; charset=UTF-8"

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->e:Lcom/teamspeak/ts3client/d/l;

    invoke-interface {v0}, Lcom/teamspeak/ts3client/d/l;->n()V

    .line 122
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/t;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/t;Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 31
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/t;->f:Ljava/lang/String;

    return-object p1
.end method

.method private b(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 156
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/t;->j:Ljava/lang/String;

    .line 157
    return-void
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/t;)Lcom/teamspeak/ts3client/d/k;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->a:Lcom/teamspeak/ts3client/d/k;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/t;)I
    .registers 2

    .prologue
    .line 31
    iget v0, p0, Lcom/teamspeak/ts3client/d/t;->c:I

    return v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/t;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/t;)Lcom/teamspeak/ts3client/d/k;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->d:Lcom/teamspeak/ts3client/d/k;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/d/t;)V
    .registers 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/t;->b()V

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->i:Landroid/widget/Button;

    if-eqz v0, :cond_a

    .line 162
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/t;->i:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 163
    :cond_a
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/d/k;)V
    .registers 2

    .prologue
    .line 126
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/t;->a:Lcom/teamspeak/ts3client/d/k;

    .line 127
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/d/l;)V
    .registers 5

    .prologue
    .line 141
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 141
    const-string v1, "lang_tag"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/t;->f:Ljava/lang/String;

    .line 142
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/t;->e:Lcom/teamspeak/ts3client/d/l;

    .line 143
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/t;->b()V

    .line 144
    return-void
.end method

.method public final b()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://la.teamspeak.com/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/teamspeak/ts3client/d/t;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/t;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/la.html"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/t;->j:Ljava/lang/String;

    .line 148
    new-instance v0, Lcom/teamspeak/ts3client/d/y;

    invoke-direct {v0, p0, v3}, Lcom/teamspeak/ts3client/d/y;-><init>(Lcom/teamspeak/ts3client/d/t;B)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/t;->j:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/d/y;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 149
    return-void
.end method

.method public final b(Lcom/teamspeak/ts3client/d/k;)V
    .registers 2

    .prologue
    .line 131
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/t;->b:Lcom/teamspeak/ts3client/d/k;

    .line 132
    return-void
.end method

.method public final c(Lcom/teamspeak/ts3client/d/k;)V
    .registers 2

    .prologue
    .line 136
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/t;->d:Lcom/teamspeak/ts3client/d/k;

    .line 137
    return-void
.end method
