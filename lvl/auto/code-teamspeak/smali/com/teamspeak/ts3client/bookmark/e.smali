.class public final Lcom/teamspeak/ts3client/bookmark/e;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Lcom/teamspeak/ts3client/data/b/a;

.field private c:Lcom/teamspeak/ts3client/Ts3Application;

.field private d:Landroid/widget/ListView;

.field private e:Lcom/teamspeak/ts3client/bookmark/c;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->a:Z

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/bookmark/c;
    .registers 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->e:Lcom/teamspeak/ts3client/bookmark/c;

    return-object v0
.end method

.method private a(I)V
    .registers 5

    .prologue
    .line 75
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->e:Lcom/teamspeak/ts3client/bookmark/c;

    invoke-virtual {v0, p1}, Lcom/teamspeak/ts3client/bookmark/c;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/ab;

    .line 81
    new-instance v1, Lcom/teamspeak/ts3client/bookmark/a;

    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/e;->b:Lcom/teamspeak/ts3client/data/b/a;

    invoke-direct {v1, v2, v0}, Lcom/teamspeak/ts3client/bookmark/a;-><init>(Lcom/teamspeak/ts3client/data/b/a;Lcom/teamspeak/ts3client/data/ab;)V

    .line 1688
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 82
    const-string v2, "AddBookmark"

    invoke-virtual {v1, v0, v2}, Lcom/teamspeak/ts3client/bookmark/a;->a(Landroid/support/v4/app/bi;Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method private a(ILcom/teamspeak/ts3client/data/ab;)V
    .registers 8

    .prologue
    const/4 v4, 0x0

    .line 49
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 50
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 51
    const-string v1, "bookmark.listbookmark.delete"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 52
    const-string v1, "bookmark.listbookmark.delete.text"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 1095
    iget-object v3, p2, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 52
    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 53
    const/4 v1, -0x1

    const-string v2, "button.delete"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/bookmark/f;

    invoke-direct {v3, p0, p1}, Lcom/teamspeak/ts3client/bookmark/f;-><init>(Lcom/teamspeak/ts3client/bookmark/e;I)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 63
    const/4 v1, -0x2

    const-string v2, "button.cancel"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/bookmark/g;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/bookmark/g;-><init>(Lcom/teamspeak/ts3client/bookmark/e;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 70
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 71
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/bookmark/e;I)V
    .registers 5

    .prologue
    .line 37
    .line 23075
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->e:Lcom/teamspeak/ts3client/bookmark/c;

    invoke-virtual {v0, p1}, Lcom/teamspeak/ts3client/bookmark/c;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/ab;

    .line 23081
    new-instance v1, Lcom/teamspeak/ts3client/bookmark/a;

    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/e;->b:Lcom/teamspeak/ts3client/data/b/a;

    invoke-direct {v1, v2, v0}, Lcom/teamspeak/ts3client/bookmark/a;-><init>(Lcom/teamspeak/ts3client/data/b/a;Lcom/teamspeak/ts3client/data/ab;)V

    .line 23688
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 23082
    const-string v2, "AddBookmark"

    invoke-virtual {v1, v0, v2}, Lcom/teamspeak/ts3client/bookmark/a;->a(Landroid/support/v4/app/bi;Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/bookmark/e;ILcom/teamspeak/ts3client/data/ab;)V
    .registers 8

    .prologue
    const/4 v4, 0x0

    .line 24049
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 24050
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 24051
    const-string v1, "bookmark.listbookmark.delete"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 24052
    const-string v1, "bookmark.listbookmark.delete.text"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 24095
    iget-object v3, p2, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 24052
    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 24053
    const/4 v1, -0x1

    const-string v2, "button.delete"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/bookmark/f;

    invoke-direct {v3, p0, p1}, Lcom/teamspeak/ts3client/bookmark/f;-><init>(Lcom/teamspeak/ts3client/bookmark/e;I)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 24063
    const/4 v1, -0x2

    const-string v2, "button.cancel"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/bookmark/g;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/bookmark/g;-><init>(Lcom/teamspeak/ts3client/bookmark/e;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 24070
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 24071
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 37
    return-void
.end method

.method private b()Lcom/teamspeak/ts3client/data/b/a;
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->b:Lcom/teamspeak/ts3client/data/b/a;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/data/b/a;
    .registers 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->b:Lcom/teamspeak/ts3client/data/b/a;

    return-object v0
.end method

.method private b(ILcom/teamspeak/ts3client/data/ab;)V
    .registers 7

    .prologue
    .line 90
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 92
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const-string v3, "button.edit"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "button.delete"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 93
    new-instance v2, Lcom/teamspeak/ts3client/bookmark/h;

    invoke-direct {v2, p0, p1, p2}, Lcom/teamspeak/ts3client/bookmark/h;-><init>(Lcom/teamspeak/ts3client/bookmark/e;ILcom/teamspeak/ts3client/data/ab;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 102
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 103
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 104
    const-string v1, "bookmark.listbookmark.options"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 105
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 107
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/bookmark/e;ILcom/teamspeak/ts3client/data/ab;)V
    .registers 7

    .prologue
    .line 25090
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 25092
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const-string v3, "button.edit"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "button.delete"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 25093
    new-instance v2, Lcom/teamspeak/ts3client/bookmark/h;

    invoke-direct {v2, p0, p1, p2}, Lcom/teamspeak/ts3client/bookmark/h;-><init>(Lcom/teamspeak/ts3client/bookmark/e;ILcom/teamspeak/ts3client/data/ab;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 25102
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 25103
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 25104
    const-string v1, "bookmark.listbookmark.options"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 25105
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 37
    return-void
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 125
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2081
    iput-object p0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 126
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2204
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 126
    const-string v1, "bookmark.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 127
    const v0, 0x7f03001e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 129
    const v0, 0x7f0c009d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/customs/FloatingButton;

    .line 130
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/e;->j()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200a0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    const v0, 0x7f0c009e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->d:Landroid/widget/ListView;

    .line 133
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->d:Landroid/widget/ListView;

    const-string v2, "Bookmark List"

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->d:Landroid/widget/ListView;

    new-instance v2, Lcom/teamspeak/ts3client/bookmark/i;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/bookmark/i;-><init>(Lcom/teamspeak/ts3client/bookmark/e;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 175
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->d:Landroid/widget/ListView;

    new-instance v2, Lcom/teamspeak/ts3client/bookmark/l;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/bookmark/l;-><init>(Lcom/teamspeak/ts3client/bookmark/e;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 183
    return-object v1
.end method

.method public final a()V
    .registers 6

    .prologue
    .line 187
    new-instance v0, Lcom/teamspeak/ts3client/bookmark/c;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/teamspeak/ts3client/bookmark/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->e:Lcom/teamspeak/ts3client/bookmark/c;

    .line 188
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->b:Lcom/teamspeak/ts3client/data/b/a;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/a;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 189
    const/4 v0, 0x0

    move v1, v0

    :goto_17
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_36

    .line 190
    iget-object v3, p0, Lcom/teamspeak/ts3client/bookmark/e;->e:Lcom/teamspeak/ts3client/bookmark/c;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/ab;

    .line 3032
    iget-object v4, v3, Lcom/teamspeak/ts3client/bookmark/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_32

    .line 3033
    iget-object v3, v3, Lcom/teamspeak/ts3client/bookmark/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_32
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_17

    .line 192
    :cond_36
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/bookmark/e;->e:Lcom/teamspeak/ts3client/bookmark/c;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 193
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 118
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 119
    new-instance v0, Lcom/teamspeak/ts3client/data/b/a;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/teamspeak/ts3client/data/b/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->b:Lcom/teamspeak/ts3client/data/b/a;

    .line 120
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/data/ab;)V
    .registers 9

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 196
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 196
    const-string v2, "use_proximity"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 197
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3107
    iget-object v3, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 3772
    iget-object v0, v3, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v4, "power"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 3774
    const-string v4, "Ts3WakeLOCK"

    invoke-virtual {v0, v5, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 3775
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 3776
    iget-object v3, v3, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4119
    iput-object v0, v3, Lcom/teamspeak/ts3client/Ts3Application;->b:Landroid/os/PowerManager$WakeLock;

    .line 198
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5107
    iget-object v3, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 5798
    iget-object v0, v3, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v4, "wifi"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 5799
    const/4 v4, 0x3

    const-string v5, "Ts3WifiLOCK"

    invoke-virtual {v0, v4, v5}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    .line 5800
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 5801
    iget-object v3, v3, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6135
    iput-object v0, v3, Lcom/teamspeak/ts3client/Ts3Application;->n:Landroid/net/wifi/WifiManager$WifiLock;

    .line 199
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7107
    iget-object v3, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 7780
    iget-object v0, v3, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v4, "power"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 7783
    const/16 v4, 0x20

    const-string v5, "Ts3WakeLOCKSensor"

    invoke-virtual {v0, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 7784
    invoke-virtual {v0, v6}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 7786
    iget-object v3, v3, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8127
    iput-object v0, v3, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    .line 200
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    .line 9121
    iput-boolean v6, v0, Lcom/teamspeak/ts3client/a/p;->a:Z

    .line 201
    if-eqz v2, :cond_84

    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9123
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    .line 201
    if-eqz v0, :cond_84

    .line 202
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10107
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 202
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->k()V

    .line 205
    :cond_84
    :try_start_84
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v0

    .line 11087
    iget v2, p1, Lcom/teamspeak/ts3client/data/ab;->d:I

    .line 205
    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/data/b/f;->b(J)Lcom/teamspeak/ts3client/e/a;

    move-result-object v0

    .line 12033
    iget-object v0, v0, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;
    :try_end_91
    .catch Ljava/lang/Exception; {:try_start_84 .. :try_end_91} :catch_11d

    .line 209
    :goto_91
    if-eqz v0, :cond_9b

    :try_start_93
    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a5

    .line 210
    :cond_9b
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/f;->d()Lcom/teamspeak/ts3client/e/a;

    move-result-object v0

    .line 13033
    iget-object v0, v0, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;
    :try_end_a5
    .catch Ljava/lang/Exception; {:try_start_93 .. :try_end_a5} :catch_121

    .line 219
    :cond_a5
    :goto_a5
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    new-instance v3, Lcom/teamspeak/ts3client/data/e;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    invoke-direct {v3, v0, v4, p1}, Lcom/teamspeak/ts3client/data/e;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/teamspeak/ts3client/data/ab;)V

    .line 14065
    iput-object v3, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 220
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 15061
    iget-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15295
    iget-object v0, v2, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 15295
    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v4, "Loading lib"

    invoke-virtual {v0, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 15296
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a()I

    move-result v0

    .line 15297
    if-lez v0, :cond_13a

    .line 221
    :goto_c7
    if-lez v0, :cond_156

    .line 222
    const/4 v2, 0x5

    if-ne v0, v2, :cond_11c

    .line 224
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 225
    const-string v2, "critical.payment"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 226
    new-instance v2, Landroid/text/SpannableString;

    const-string v3, "This beta version is expired, please visit our forum or \nhttp://play.google.com/store/apps/details?id=com.teamspeak.ts3client"

    invoke-direct {v2, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 227
    const/16 v3, 0xf

    invoke-static {v2, v3}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    .line 229
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 230
    const/4 v2, -0x2

    const-string v3, "button.exit"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/teamspeak/ts3client/bookmark/m;

    invoke-direct {v4, p0, v0}, Lcom/teamspeak/ts3client/bookmark/m;-><init>(Lcom/teamspeak/ts3client/bookmark/e;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 238
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 239
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 240
    const v2, 0x102000b

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 241
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 242
    iput-boolean v6, p0, Lcom/teamspeak/ts3client/bookmark/e;->a:Z

    .line 243
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17065
    iput-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 303
    :cond_11c
    :goto_11c
    return-void

    :catch_11d
    move-exception v0

    move-object v0, v1

    goto/16 :goto_91

    .line 212
    :catch_121
    move-exception v0

    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/f;->b()V

    .line 214
    :try_start_129
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/f;->d()Lcom/teamspeak/ts3client/e/a;

    move-result-object v0

    .line 14033
    iget-object v0, v0, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;
    :try_end_133
    .catch Ljava/lang/Exception; {:try_start_129 .. :try_end_133} :catch_135

    goto/16 :goto_a5

    .line 216
    :catch_135
    move-exception v0

    const-string v0, ""

    goto/16 :goto_a5

    .line 15299
    :cond_13a
    iget-object v3, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget v4, v2, Lcom/teamspeak/ts3client/data/e;->y:I

    iget v5, v2, Lcom/teamspeak/ts3client/data/e;->x:I

    invoke-virtual {v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_prepareAudioDevice(II)I

    .line 15300
    new-instance v3, Lcom/teamspeak/ts3client/data/f/a;

    iget-object v4, v2, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;-><init>(Lcom/teamspeak/ts3client/Ts3Application;)V

    iput-object v3, v2, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 15301
    iget-object v3, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_spawnNewServerConnectionHandler()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    goto/16 :goto_c7

    .line 247
    :cond_156
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 18061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 247
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->n()V

    .line 249
    new-instance v0, Lcom/teamspeak/ts3client/t;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/t;-><init>()V

    .line 18561
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->B:Landroid/os/Bundle;

    .line 250
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/t;->e(Landroid/os/Bundle;)V

    .line 252
    iget-object v1, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19198
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 252
    if-nez v1, :cond_185

    .line 253
    iget-object v1, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    const-class v4, Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 20192
    iput-object v2, v1, Lcom/teamspeak/ts3client/Ts3Application;->k:Landroid/content/Intent;

    .line 254
    iget-object v1, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21188
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->k:Landroid/content/Intent;

    .line 254
    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/Ts3Application;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 257
    :cond_185
    iget-object v1, p0, Lcom/teamspeak/ts3client/bookmark/e;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 22210
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 23055
    iget-object v0, p1, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 258
    const-string v1, "(.*://)+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 261
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 262
    new-instance v2, Lcom/teamspeak/ts3client/bookmark/n;

    invoke-direct {v2, p0, p1, v0}, Lcom/teamspeak/ts3client/bookmark/n;-><init>(Lcom/teamspeak/ts3client/bookmark/e;Lcom/teamspeak/ts3client/data/ab;Ljava/lang/String;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 297
    new-instance v0, Lcom/teamspeak/ts3client/bookmark/o;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/bookmark/o;-><init>(Lcom/teamspeak/ts3client/bookmark/e;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_11c
.end method

.method public final c(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 111
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    .line 113
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/e;->a()V

    .line 114
    return-void
.end method
