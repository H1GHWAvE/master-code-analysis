.class public Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field public a:I

.field public b:I

.field private c:J


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method private constructor <init>(JII)V
    .registers 6

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;->c:J

    .line 17
    iput p3, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;->a:I

    .line 18
    iput p4, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;->b:I

    .line 19
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 20
    return-void
.end method

.method private b()I
    .registers 2

    .prologue
    .line 27
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;->b:I

    return v0
.end method

.method private c()J
    .registers 3

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;->c:J

    return-wide v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 23
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;->a:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClientNeededPermissions [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", permissionID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", permissionValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
