.class public Lcom/teamspeak/ts3client/jni/events/ChannelMove;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method private constructor <init>(JJJILjava/lang/String;Ljava/lang/String;)V
    .registers 11

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a:J

    .line 22
    iput-wide p3, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->b:J

    .line 23
    iput-wide p5, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->c:J

    .line 24
    iput p7, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->d:I

    .line 25
    iput-object p8, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->e:Ljava/lang/String;

    .line 26
    iput-object p9, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->f:Ljava/lang/String;

    .line 27
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 28
    return-void
.end method

.method private e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->f:Ljava/lang/String;

    return-object v0
.end method

.method private f()J
    .registers 3

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a:J

    return-wide v0
.end method


# virtual methods
.method public final a()J
    .registers 3

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->b:J

    return-wide v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 35
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->d:I

    return v0
.end method

.method public final c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()J
    .registers 3

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->c:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ChannelMove [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", channelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newChannelParentID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invokerID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invokerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invokerUniqueIdentifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
