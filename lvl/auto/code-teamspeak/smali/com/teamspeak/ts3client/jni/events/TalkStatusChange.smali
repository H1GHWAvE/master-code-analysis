.class public Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field public a:J

.field public b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method private constructor <init>(JIII)V
    .registers 7

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->a:J

    .line 21
    iput p3, p0, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->b:I

    .line 22
    iput p4, p0, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->c:I

    .line 23
    iput p5, p0, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->d:I

    .line 24
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 25
    return-void
.end method

.method private d()J
    .registers 3

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->a:J

    return-wide v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 28
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->d:I

    return v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 32
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->c:I

    return v0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 40
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->b:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TalkStatusChange [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isReceivedWhisper="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
