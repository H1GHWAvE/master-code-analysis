.class public Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field public a:I

.field public b:J

.field private c:Ljava/lang/String;

.field private d:J

.field private e:J


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method private constructor <init>(IJLjava/lang/String;JJ)V
    .registers 10

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->a:I

    .line 25
    iput-wide p2, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->b:J

    .line 26
    iput-object p4, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->c:Ljava/lang/String;

    .line 27
    iput-wide p5, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->d:J

    .line 28
    iput-wide p7, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->e:J

    .line 29
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 30
    return-void
.end method

.method private a()J
    .registers 3

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->d:J

    return-wide v0
.end method

.method private b()J
    .registers 3

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->e:J

    return-wide v0
.end method

.method private c()J
    .registers 3

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->b:J

    return-wide v0
.end method

.method private d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->c:Ljava/lang/String;

    return-object v0
.end method

.method private e()I
    .registers 2

    .prologue
    .line 49
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->a:I

    return v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FileTransferStatus [transferID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", remotefileSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", serverConnectionHandlerID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
