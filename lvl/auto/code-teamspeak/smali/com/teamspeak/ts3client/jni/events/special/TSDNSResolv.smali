.class public Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;ZI)V
    .registers 6

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->a:Ljava/lang/String;

    .line 22
    iput p2, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->b:I

    .line 23
    iput-object p3, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->c:Ljava/lang/String;

    .line 24
    iput-boolean p4, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->d:Z

    .line 25
    iput p5, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->e:I

    .line 26
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 27
    return-void
.end method

.method private d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->c:Ljava/lang/String;

    return-object v0
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->d:Z

    return v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 30
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->e:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 38
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->b:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TSDNSResolv [ip="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", port="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", wasTSDNS="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
