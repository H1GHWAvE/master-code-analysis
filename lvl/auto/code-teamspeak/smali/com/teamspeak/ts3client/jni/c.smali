.class public final enum Lcom/teamspeak/ts3client/jni/c;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum A:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum B:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum C:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum D:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum E:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum F:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum G:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum H:Lcom/teamspeak/ts3client/jni/c;

.field private static final synthetic J:[Lcom/teamspeak/ts3client/jni/c;

.field public static final enum a:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum b:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum c:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum d:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum e:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum f:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum g:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum h:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum i:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum j:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum k:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum l:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum m:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum n:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum o:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum p:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum q:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum r:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum s:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum t:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum u:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum v:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum w:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum x:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum y:Lcom/teamspeak/ts3client/jni/c;

.field public static final enum z:Lcom/teamspeak/ts3client/jni/c;


# instance fields
.field I:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 6
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_NAME"

    invoke-direct {v0, v1, v4, v4}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->a:Lcom/teamspeak/ts3client/jni/c;

    .line 8
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_TOPIC"

    invoke-direct {v0, v1, v5, v5}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->b:Lcom/teamspeak/ts3client/jni/c;

    .line 10
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_DESCRIPTION"

    invoke-direct {v0, v1, v6, v6}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->c:Lcom/teamspeak/ts3client/jni/c;

    .line 12
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_PASSWORD"

    invoke-direct {v0, v1, v7, v7}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->d:Lcom/teamspeak/ts3client/jni/c;

    .line 13
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_CODEC"

    invoke-direct {v0, v1, v8, v8}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->e:Lcom/teamspeak/ts3client/jni/c;

    .line 15
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_CODEC_QUALITY"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->f:Lcom/teamspeak/ts3client/jni/c;

    .line 17
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_MAXCLIENTS"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->g:Lcom/teamspeak/ts3client/jni/c;

    .line 19
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_MAXFAMILYCLIENTS"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->h:Lcom/teamspeak/ts3client/jni/c;

    .line 21
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_ORDER"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->i:Lcom/teamspeak/ts3client/jni/c;

    .line 23
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_FLAG_PERMANENT"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->j:Lcom/teamspeak/ts3client/jni/c;

    .line 25
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_FLAG_SEMI_PERMANENT"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->k:Lcom/teamspeak/ts3client/jni/c;

    .line 27
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_FLAG_DEFAULT"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->l:Lcom/teamspeak/ts3client/jni/c;

    .line 29
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_FLAG_PASSWORD"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->m:Lcom/teamspeak/ts3client/jni/c;

    .line 31
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_CODEC_LATENCY_FACTOR"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->n:Lcom/teamspeak/ts3client/jni/c;

    .line 33
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_CODEC_IS_UNENCRYPTED"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->o:Lcom/teamspeak/ts3client/jni/c;

    .line 35
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_SECURITY_SALT"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->p:Lcom/teamspeak/ts3client/jni/c;

    .line 37
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_DELETE_DELAY"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->q:Lcom/teamspeak/ts3client/jni/c;

    .line 39
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_DUMMY_2"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->r:Lcom/teamspeak/ts3client/jni/c;

    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_DUMMY_3"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->s:Lcom/teamspeak/ts3client/jni/c;

    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_DUMMY_4"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->t:Lcom/teamspeak/ts3client/jni/c;

    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_DUMMY_5"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->u:Lcom/teamspeak/ts3client/jni/c;

    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_DUMMY_6"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->v:Lcom/teamspeak/ts3client/jni/c;

    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_DUMMY_7"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->w:Lcom/teamspeak/ts3client/jni/c;

    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_FLAG_MAXCLIENTS_UNLIMITED"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->x:Lcom/teamspeak/ts3client/jni/c;

    .line 44
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_FLAG_MAXFAMILYCLIENTS_UNLIMITED"

    const/16 v2, 0x18

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->y:Lcom/teamspeak/ts3client/jni/c;

    .line 48
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_FLAG_MAXFAMILYCLIENTS_INHERITED"

    const/16 v2, 0x19

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->z:Lcom/teamspeak/ts3client/jni/c;

    .line 52
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_FLAG_ARE_SUBSCRIBED"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->A:Lcom/teamspeak/ts3client/jni/c;

    .line 55
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_FILEPATH"

    const/16 v2, 0x1b

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->B:Lcom/teamspeak/ts3client/jni/c;

    .line 57
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_NEEDED_TALK_POWER"

    const/16 v2, 0x1c

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->C:Lcom/teamspeak/ts3client/jni/c;

    .line 59
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_FORCED_SILENCE"

    const/16 v2, 0x1d

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->D:Lcom/teamspeak/ts3client/jni/c;

    .line 61
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_NAME_PHONETIC"

    const/16 v2, 0x1e

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->E:Lcom/teamspeak/ts3client/jni/c;

    .line 63
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_ICON_ID"

    const/16 v2, 0x1f

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->F:Lcom/teamspeak/ts3client/jni/c;

    .line 65
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_FLAG_PRIVATE"

    const/16 v2, 0x20

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->G:Lcom/teamspeak/ts3client/jni/c;

    .line 67
    new-instance v0, Lcom/teamspeak/ts3client/jni/c;

    const-string v1, "CHANNEL_ENDMARKER_RARE"

    const/16 v2, 0x21

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->H:Lcom/teamspeak/ts3client/jni/c;

    .line 5
    const/16 v0, 0x22

    new-array v0, v0, [Lcom/teamspeak/ts3client/jni/c;

    sget-object v1, Lcom/teamspeak/ts3client/jni/c;->a:Lcom/teamspeak/ts3client/jni/c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/teamspeak/ts3client/jni/c;->b:Lcom/teamspeak/ts3client/jni/c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/teamspeak/ts3client/jni/c;->c:Lcom/teamspeak/ts3client/jni/c;

    aput-object v1, v0, v6

    sget-object v1, Lcom/teamspeak/ts3client/jni/c;->d:Lcom/teamspeak/ts3client/jni/c;

    aput-object v1, v0, v7

    sget-object v1, Lcom/teamspeak/ts3client/jni/c;->e:Lcom/teamspeak/ts3client/jni/c;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->f:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->g:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->h:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->i:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->j:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->k:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->l:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->m:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->n:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->o:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->p:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->q:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->r:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->s:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->t:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->u:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->v:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->w:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->x:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->y:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->z:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->A:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->B:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->C:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->D:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->E:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->F:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->G:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/teamspeak/ts3client/jni/c;->H:Lcom/teamspeak/ts3client/jni/c;

    aput-object v2, v0, v1

    sput-object v0, Lcom/teamspeak/ts3client/jni/c;->J:[Lcom/teamspeak/ts3client/jni/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    iput p3, p0, Lcom/teamspeak/ts3client/jni/c;->I:I

    .line 73
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 76
    iget v0, p0, Lcom/teamspeak/ts3client/jni/c;->I:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/teamspeak/ts3client/jni/c;
    .registers 2

    .prologue
    .line 5
    const-class v0, Lcom/teamspeak/ts3client/jni/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/jni/c;

    return-object v0
.end method

.method public static values()[Lcom/teamspeak/ts3client/jni/c;
    .registers 1

    .prologue
    .line 5
    sget-object v0, Lcom/teamspeak/ts3client/jni/c;->J:[Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual {v0}, [Lcom/teamspeak/ts3client/jni/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/teamspeak/ts3client/jni/c;

    return-object v0
.end method
