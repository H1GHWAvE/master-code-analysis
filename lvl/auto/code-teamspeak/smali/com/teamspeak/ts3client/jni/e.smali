.class public final enum Lcom/teamspeak/ts3client/jni/e;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/teamspeak/ts3client/jni/e;

.field public static final enum b:Lcom/teamspeak/ts3client/jni/e;

.field public static final enum c:Lcom/teamspeak/ts3client/jni/e;

.field public static final enum d:Lcom/teamspeak/ts3client/jni/e;

.field public static final enum e:Lcom/teamspeak/ts3client/jni/e;

.field private static final synthetic g:[Lcom/teamspeak/ts3client/jni/e;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 236
    new-instance v0, Lcom/teamspeak/ts3client/jni/e;

    const-string v1, "STATUS_DISCONNECTED"

    invoke-direct {v0, v1, v2, v2}, Lcom/teamspeak/ts3client/jni/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/e;->a:Lcom/teamspeak/ts3client/jni/e;

    .line 238
    new-instance v0, Lcom/teamspeak/ts3client/jni/e;

    const-string v1, "STATUS_CONNECTING"

    invoke-direct {v0, v1, v3, v3}, Lcom/teamspeak/ts3client/jni/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/e;->b:Lcom/teamspeak/ts3client/jni/e;

    .line 241
    new-instance v0, Lcom/teamspeak/ts3client/jni/e;

    const-string v1, "STATUS_CONNECTED"

    invoke-direct {v0, v1, v4, v4}, Lcom/teamspeak/ts3client/jni/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/e;->c:Lcom/teamspeak/ts3client/jni/e;

    .line 245
    new-instance v0, Lcom/teamspeak/ts3client/jni/e;

    const-string v1, "STATUS_CONNECTION_ESTABLISHING"

    invoke-direct {v0, v1, v5, v5}, Lcom/teamspeak/ts3client/jni/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/e;->d:Lcom/teamspeak/ts3client/jni/e;

    .line 247
    new-instance v0, Lcom/teamspeak/ts3client/jni/e;

    const-string v1, "STATUS_CONNECTION_ESTABLISHED"

    invoke-direct {v0, v1, v6, v6}, Lcom/teamspeak/ts3client/jni/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/e;->e:Lcom/teamspeak/ts3client/jni/e;

    .line 235
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/teamspeak/ts3client/jni/e;

    sget-object v1, Lcom/teamspeak/ts3client/jni/e;->a:Lcom/teamspeak/ts3client/jni/e;

    aput-object v1, v0, v2

    sget-object v1, Lcom/teamspeak/ts3client/jni/e;->b:Lcom/teamspeak/ts3client/jni/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/teamspeak/ts3client/jni/e;->c:Lcom/teamspeak/ts3client/jni/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/teamspeak/ts3client/jni/e;->d:Lcom/teamspeak/ts3client/jni/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/teamspeak/ts3client/jni/e;->e:Lcom/teamspeak/ts3client/jni/e;

    aput-object v1, v0, v6

    sput-object v0, Lcom/teamspeak/ts3client/jni/e;->g:[Lcom/teamspeak/ts3client/jni/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4

    .prologue
    .line 252
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 253
    iput p3, p0, Lcom/teamspeak/ts3client/jni/e;->f:I

    .line 254
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/teamspeak/ts3client/jni/e;
    .registers 2

    .prologue
    .line 235
    const-class v0, Lcom/teamspeak/ts3client/jni/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/jni/e;

    return-object v0
.end method

.method public static values()[Lcom/teamspeak/ts3client/jni/e;
    .registers 1

    .prologue
    .line 235
    sget-object v0, Lcom/teamspeak/ts3client/jni/e;->g:[Lcom/teamspeak/ts3client/jni/e;

    invoke-virtual {v0}, [Lcom/teamspeak/ts3client/jni/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/teamspeak/ts3client/jni/e;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 257
    iget v0, p0, Lcom/teamspeak/ts3client/jni/e;->f:I

    return v0
.end method
