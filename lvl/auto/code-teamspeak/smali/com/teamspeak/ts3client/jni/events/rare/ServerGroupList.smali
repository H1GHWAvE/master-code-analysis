.class public Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:J

.field private c:Ljava/lang/String;

.field private d:I

.field private e:J

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method private constructor <init>(JJLjava/lang/String;IJIIIIII)V
    .registers 20

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->a:J

    .line 29
    iput-wide p3, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->b:J

    .line 30
    iput-object p5, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->c:Ljava/lang/String;

    .line 31
    iput p6, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->d:I

    .line 32
    const-wide v2, 0xffffffffL

    and-long/2addr v2, p7

    iput-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->e:J

    .line 33
    iput p9, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->f:I

    .line 34
    iput p10, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->g:I

    .line 35
    move/from16 v0, p11

    iput v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->h:I

    .line 36
    move/from16 v0, p12

    iput v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->i:I

    .line 37
    move/from16 v0, p13

    iput v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->j:I

    .line 38
    move/from16 v0, p14

    iput v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->k:I

    .line 39
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 40
    return-void
.end method

.method private c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->c:Ljava/lang/String;

    return-object v0
.end method

.method private d()I
    .registers 2

    .prologue
    .line 55
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->h:I

    return v0
.end method

.method private e()I
    .registers 2

    .prologue
    .line 59
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->j:I

    return v0
.end method

.method private f()I
    .registers 2

    .prologue
    .line 63
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->k:I

    return v0
.end method

.method private g()I
    .registers 2

    .prologue
    .line 67
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->i:I

    return v0
.end method

.method private h()I
    .registers 2

    .prologue
    .line 71
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->f:I

    return v0
.end method

.method private i()J
    .registers 3

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->a:J

    return-wide v0
.end method

.method private j()J
    .registers 3

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->b:J

    return-wide v0
.end method

.method private k()I
    .registers 2

    .prologue
    .line 83
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->g:I

    return v0
.end method

.method private l()I
    .registers 2

    .prologue
    .line 87
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->d:I

    return v0
.end method


# virtual methods
.method public final a()Lcom/teamspeak/ts3client/data/g/a;
    .registers 19

    .prologue
    .line 43
    new-instance v3, Lcom/teamspeak/ts3client/data/g/a;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->a:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->b:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->d:I

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->e:J

    move-object/from16 v0, p0

    iget v12, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->f:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->g:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->h:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->i:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->j:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->k:I

    move/from16 v17, v0

    invoke-direct/range {v3 .. v17}, Lcom/teamspeak/ts3client/data/g/a;-><init>(JJLjava/lang/String;IJIIIIII)V

    return-object v3
.end method

.method public final b()J
    .registers 3

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->e:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServerGroupList [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", serverGroupID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", iconID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", saveDB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sortID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nameMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", neededModifyPower="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", neededMemberAddPower="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", neededMemberRemovePower="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
