.class public final Lcom/teamspeak/ts3client/jni/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/teamspeak/ts3client/jni/l;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 9
    const/4 v0, 0x0

    sput-object v0, Lcom/teamspeak/ts3client/jni/l;->a:Lcom/teamspeak/ts3client/jni/l;

    .line 11
    const-string v0, "Ts3Callback"

    sput-object v0, Lcom/teamspeak/ts3client/jni/l;->b:Ljava/lang/String;

    .line 12
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/jni/l;->c:Ljava/util/Vector;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public static a()Lcom/teamspeak/ts3client/jni/l;
    .registers 1

    .prologue
    .line 19
    sget-object v0, Lcom/teamspeak/ts3client/jni/l;->a:Lcom/teamspeak/ts3client/jni/l;

    if-nez v0, :cond_b

    .line 20
    new-instance v0, Lcom/teamspeak/ts3client/jni/l;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/jni/l;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/jni/l;->a:Lcom/teamspeak/ts3client/jni/l;

    .line 22
    :cond_b
    sget-object v0, Lcom/teamspeak/ts3client/jni/l;->a:Lcom/teamspeak/ts3client/jni/l;

    return-object v0
.end method

.method private declared-synchronized a(Lcom/teamspeak/ts3client/data/w;Lcom/teamspeak/ts3client/jni/k;)V
    .registers 4

    .prologue
    .line 40
    monitor-enter p0

    :try_start_1
    invoke-interface {p1, p2}, Lcom/teamspeak/ts3client/data/w;->a(Lcom/teamspeak/ts3client/jni/k;)V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_6

    .line 41
    monitor-exit p0

    return-void

    .line 40
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 5

    .prologue
    .line 30
    const-class v1, Lcom/teamspeak/ts3client/jni/l;

    monitor-enter v1

    :try_start_3
    sget-object v2, Lcom/teamspeak/ts3client/jni/l;->c:Ljava/util/Vector;

    monitor-enter v2
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_25

    .line 31
    :try_start_6
    sget-object v0, Lcom/teamspeak/ts3client/jni/l;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 32
    monitor-exit v2
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_28

    .line 33
    :try_start_f
    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/w;

    .line 35
    sget-object v3, Lcom/teamspeak/ts3client/jni/l;->a:Lcom/teamspeak/ts3client/jni/l;

    invoke-direct {v3, v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;Lcom/teamspeak/ts3client/jni/k;)V
    :try_end_24
    .catchall {:try_start_f .. :try_end_24} :catchall_25

    goto :goto_13

    .line 30
    :catchall_25
    move-exception v0

    monitor-exit v1

    throw v0

    .line 32
    :catchall_28
    move-exception v0

    :try_start_29
    monitor-exit v2
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_28

    :try_start_2a
    throw v0
    :try_end_2b
    .catchall {:try_start_2a .. :try_end_2b} :catchall_25

    .line 37
    :cond_2b
    monitor-exit v1

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/teamspeak/ts3client/data/w;)V
    .registers 3

    .prologue
    .line 44
    monitor-enter p0

    :try_start_1
    sget-object v0, Lcom/teamspeak/ts3client/jni/l;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 45
    sget-object v0, Lcom/teamspeak/ts3client/jni/l;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    .line 46
    :cond_e
    monitor-exit p0

    return-void

    .line 44
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/teamspeak/ts3client/data/w;)V
    .registers 3

    .prologue
    .line 49
    monitor-enter p0

    :try_start_1
    sget-object v0, Lcom/teamspeak/ts3client/jni/l;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 50
    monitor-exit p0

    return-void

    .line 49
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method
