.class public Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:I

.field private c:J

.field private d:J

.field private e:Lcom/teamspeak/ts3client/jni/j;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method private constructor <init>(JIJJI)V
    .registers 11

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->a:J

    .line 22
    iput p3, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->b:I

    .line 23
    iput-wide p4, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->c:J

    .line 24
    iput-wide p6, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->d:J

    .line 25
    if-nez p8, :cond_11

    .line 26
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 27
    :cond_11
    const/4 v0, 0x1

    if-ne p8, v0, :cond_18

    .line 28
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 29
    :cond_18
    const/4 v0, 0x2

    if-ne p8, v0, :cond_1f

    .line 30
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->c:Lcom/teamspeak/ts3client/jni/j;

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 31
    :cond_1f
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 32
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 35
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->b:I

    return v0
.end method

.method private b()J
    .registers 3

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->d:J

    return-wide v0
.end method

.method private c()J
    .registers 3

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->c:J

    return-wide v0
.end method

.method private d()J
    .registers 3

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->a:J

    return-wide v0
.end method

.method private e()Lcom/teamspeak/ts3client/jni/j;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->e:Lcom/teamspeak/ts3client/jni/j;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClientMoveSubscription [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", oldChannelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newChannelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveSubscription;->e:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
