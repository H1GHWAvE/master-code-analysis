.class public final Lcom/teamspeak/ts3client/chat/j;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/d/s;
.implements Lcom/teamspeak/ts3client/data/w;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation


# instance fields
.field a:Ljava/util/regex/Pattern;

.field private at:Ljava/lang/String;

.field private au:Z

.field private av:Z

.field private aw:I

.field b:Ljava/util/regex/Pattern;

.field c:Landroid/database/DataSetObserver;

.field private d:Lcom/teamspeak/ts3client/Ts3Application;

.field private e:Lcom/teamspeak/ts3client/chat/MListView;

.field private f:Lcom/teamspeak/ts3client/chat/h;

.field private g:Lcom/teamspeak/ts3client/chat/a;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/EditText;

.field private j:Landroid/widget/ImageButton;

.field private k:Landroid/widget/ImageButton;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/chat/a;)V
    .registers 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ValidFragment"
        }
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 80
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 48
    const-string v0, "(ts3server://[^\\s\"]{3,}|(?:(?:(?:http://|https://|ftp://)(?:\\w+:\\w+@)?)|www\\.|ftp\\.)[^ !\"#$%&\'()*+,/:;<=>?@\\\\\\[\\\\\\]^_`{|}~\u0008\t\n\u000c\r]{3,}(?::[0-9]+)?(?:/[^\\s\"]*)?)"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->a:Ljava/util/regex/Pattern;

    .line 49
    const-string v0, "((^|\\s)([A-Z0-9._%+-]+@[A-Z0-9\u00c4\u00d6\u00dc.-]+\\.[A-Z]{2,4})(\\s|$)?)"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->b:Ljava/util/regex/Pattern;

    .line 55
    new-instance v0, Lcom/teamspeak/ts3client/chat/k;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/chat/k;-><init>(Lcom/teamspeak/ts3client/chat/j;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->c:Landroid/database/DataSetObserver;

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/teamspeak/ts3client/chat/j;->aw:I

    .line 81
    iput-object p1, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 82
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/chat/j;I)I
    .registers 2

    .prologue
    .line 46
    iput p1, p0, Lcom/teamspeak/ts3client/chat/j;->aw:I

    return p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/chat/a;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    return-object v0
.end method

.method private a()V
    .registers 9

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 249
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 282
    :goto_14
    return-void

    .line 252
    :cond_15
    new-instance v0, Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 253
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->a:Ljava/util/regex/Pattern;

    iget-object v2, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 254
    :goto_38
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_5f

    .line 255
    invoke-virtual {v1, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 256
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[URL]"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "[/URL]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 257
    invoke-static {v2}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_38

    .line 259
    :cond_5f
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 261
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 262
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 263
    :goto_7d
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_ae

    .line 264
    invoke-virtual {v0, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 265
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "[URL=mailto:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[/URL]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 266
    invoke-static {v1}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_7d

    .line 268
    :cond_ae
    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 270
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 3123
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    .line 270
    const-string v1, "CHANNEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10a

    .line 271
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 271
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5267
    iget-wide v1, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 271
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6214
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 271
    const-string v6, "Send Message to Channel"

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestSendChannelTextMsg(JLjava/lang/String;JLjava/lang/String;)I

    .line 272
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aN:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    const-string v3, ""

    const-string v4, ""

    iget-object v5, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 272
    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v7, v4, v5}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 280
    :goto_f8
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 10088
    iget-object v1, v1, Lcom/teamspeak/ts3client/chat/a;->a:Ljava/lang/String;

    .line 280
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_14

    .line 273
    :cond_10a
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 7123
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    .line 273
    const-string v1, "SERVER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14c

    .line 274
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 274
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9267
    iget-wide v4, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 274
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Send Message to Server"

    invoke-virtual {v0, v4, v5, v1, v2}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestSendServerTextMsg(JLjava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aP:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    const-string v3, ""

    const-string v4, ""

    iget-object v5, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10061
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 275
    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v7, v4, v5}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto :goto_f8

    .line 277
    :cond_14c
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->at:Ljava/lang/String;

    .line 278
    invoke-direct {p0, v5}, Lcom/teamspeak/ts3client/chat/j;->a(Z)V

    goto :goto_f8
.end method

.method private a(Z)V
    .registers 9

    .prologue
    .line 285
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 11061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 285
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 286
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 286
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 13061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 286
    iget-object v4, p0, Lcom/teamspeak/ts3client/chat/j;->at:Ljava/lang/String;

    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 14097
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 14235
    iget v5, v0, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "Send Message to Client "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 15097
    iget-object v6, v6, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 15235
    iget v6, v6, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 286
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestSendPrivateTextMsg(JLjava/lang/String;ILjava/lang/String;)I

    .line 287
    if-eqz p1, :cond_56

    .line 288
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aL:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    iget-object v3, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 16088
    iget-object v3, v3, Lcom/teamspeak/ts3client/chat/a;->a:Ljava/lang/String;

    .line 288
    const/4 v4, 0x0

    const-string v5, ""

    iget-object v6, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17061
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 288
    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 289
    :cond_56
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/chat/j;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/chat/MListView;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->e:Lcom/teamspeak/ts3client/chat/MListView;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/chat/j;)V
    .registers 9

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 46
    .line 30249
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_107

    .line 30252
    new-instance v0, Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 30253
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->a:Ljava/util/regex/Pattern;

    iget-object v2, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 30254
    :goto_37
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_5e

    .line 30255
    invoke-virtual {v1, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 30256
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[URL]"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "[/URL]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 30257
    invoke-static {v2}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_37

    .line 30259
    :cond_5e
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 30261
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 30262
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 30263
    :goto_7c
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_ad

    .line 30264
    invoke-virtual {v0, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 30265
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "[URL=mailto:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[/URL]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 30266
    invoke-static {v1}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_7c

    .line 30268
    :cond_ad
    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 30270
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 31123
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    .line 30270
    const-string v1, "CHANNEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_108

    .line 30271
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 32061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 32234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 30271
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 33061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 33267
    iget-wide v1, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 30271
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 34061
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 34214
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 30271
    const-string v6, "Send Message to Channel"

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestSendChannelTextMsg(JLjava/lang/String;JLjava/lang/String;)I

    .line 30272
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aN:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    const-string v3, ""

    const-string v4, ""

    iget-object v5, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 35061
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 30272
    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v7, v4, v5}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 30280
    :goto_f7
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 38088
    iget-object v1, v1, Lcom/teamspeak/ts3client/chat/a;->a:Ljava/lang/String;

    .line 30280
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 30281
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 46
    :cond_107
    return-void

    .line 30273
    :cond_108
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 35123
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    .line 30273
    const-string v1, "SERVER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14a

    .line 30274
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 36061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 36234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 30274
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 37061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 37267
    iget-wide v4, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 30274
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Send Message to Server"

    invoke-virtual {v0, v4, v5, v1, v2}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestSendServerTextMsg(JLjava/lang/String;Ljava/lang/String;)I

    .line 30275
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aP:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    const-string v3, ""

    const-string v4, ""

    iget-object v5, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 38061
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 30275
    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v7, v4, v5}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto :goto_f7

    .line 30277
    :cond_14a
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->at:Ljava/lang/String;

    .line 30278
    invoke-direct {p0, v5}, Lcom/teamspeak/ts3client/chat/j;->a(Z)V

    goto :goto_f7
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/chat/j;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/chat/j;)I
    .registers 2

    .prologue
    .line 46
    iget v0, p0, Lcom/teamspeak/ts3client/chat/j;->aw:I

    return v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/chat/h;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->f:Lcom/teamspeak/ts3client/chat/h;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    .prologue
    const/4 v3, 0x1

    .line 120
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/chat/j;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 121
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1081
    iput-object p0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 122
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/chat/j;->n()V

    .line 123
    const v0, 0x7f030027

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 124
    const v0, 0x7f0c00f0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->h:Landroid/widget/TextView;

    .line 125
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->h:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 1088
    iget-object v2, v2, Lcom/teamspeak/ts3client/chat/a;->a:Ljava/lang/String;

    .line 125
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    const v0, 0x7f0c00f3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    .line 127
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    new-instance v2, Lcom/teamspeak/ts3client/chat/n;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/chat/n;-><init>(Lcom/teamspeak/ts3client/chat/j;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 153
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    new-instance v2, Lcom/teamspeak/ts3client/chat/o;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/chat/o;-><init>(Lcom/teamspeak/ts3client/chat/j;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 173
    const v0, 0x7f0c00f5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->j:Landroid/widget/ImageButton;

    .line 174
    const v0, 0x7f0c00f4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->k:Landroid/widget/ImageButton;

    .line 175
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->k:Landroid/widget/ImageButton;

    new-instance v2, Lcom/teamspeak/ts3client/chat/q;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/chat/q;-><init>(Lcom/teamspeak/ts3client/chat/j;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    const v0, 0x7f0c00f6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->m:Landroid/view/View;

    .line 184
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->m:Landroid/view/View;

    new-instance v2, Lcom/teamspeak/ts3client/chat/r;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/chat/r;-><init>(Lcom/teamspeak/ts3client/chat/j;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    const v0, 0x7f0c00f7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->l:Landroid/view/View;

    .line 193
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->l:Landroid/view/View;

    new-instance v2, Lcom/teamspeak/ts3client/chat/s;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/chat/s;-><init>(Lcom/teamspeak/ts3client/chat/j;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    const v0, 0x7f0c00f1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/MListView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->e:Lcom/teamspeak/ts3client/chat/MListView;

    .line 202
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->f:Lcom/teamspeak/ts3client/chat/h;

    if-nez v0, :cond_af

    .line 203
    new-instance v0, Lcom/teamspeak/ts3client/chat/h;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/chat/j;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/teamspeak/ts3client/chat/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->f:Lcom/teamspeak/ts3client/chat/h;

    .line 204
    :cond_af
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->f:Lcom/teamspeak/ts3client/chat/h;

    iget-object v2, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 1109
    iput-object v2, v0, Lcom/teamspeak/ts3client/chat/h;->b:Lcom/teamspeak/ts3client/chat/a;

    .line 2109
    iput-object v0, v2, Lcom/teamspeak/ts3client/chat/a;->f:Lcom/teamspeak/ts3client/chat/h;

    .line 3080
    iget-object v2, v2, Lcom/teamspeak/ts3client/chat/a;->c:Ljava/util/ArrayList;

    .line 1111
    iput-object v2, v0, Lcom/teamspeak/ts3client/chat/h;->a:Ljava/util/ArrayList;

    .line 1112
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/h;->a()V

    .line 205
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->e:Lcom/teamspeak/ts3client/chat/MListView;

    iget-object v2, p0, Lcom/teamspeak/ts3client/chat/j;->f:Lcom/teamspeak/ts3client/chat/h;

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/chat/MListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 206
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->e:Lcom/teamspeak/ts3client/chat/MListView;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/chat/MListView;->setItemsCanFocus(Z)V

    .line 207
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->e:Lcom/teamspeak/ts3client/chat/MListView;

    new-instance v2, Lcom/teamspeak/ts3client/chat/t;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/chat/t;-><init>(Lcom/teamspeak/ts3client/chat/j;)V

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/chat/MListView;->setOnSizeChangedListener(Lcom/teamspeak/ts3client/chat/x;)V

    .line 221
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->e:Lcom/teamspeak/ts3client/chat/MListView;

    new-instance v2, Lcom/teamspeak/ts3client/chat/v;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/chat/v;-><init>(Lcom/teamspeak/ts3client/chat/j;)V

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/chat/MListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 235
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->e:Lcom/teamspeak/ts3client/chat/MListView;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/chat/MListView;->setTranscriptMode(I)V

    .line 236
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->j:Landroid/widget/ImageButton;

    new-instance v2, Lcom/teamspeak/ts3client/chat/w;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/chat/w;-><init>(Lcom/teamspeak/ts3client/chat/j;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/a;->b()V

    .line 245
    return-object v1
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 116
    return-void
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 3

    .prologue
    .line 371
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 372
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 373
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 11

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 325
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/TextMessage;

    if-eqz v0, :cond_47

    move-object v0, p1

    .line 326
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/TextMessage;

    move-object v2, p1

    .line 327
    check-cast v2, Lcom/teamspeak/ts3client/jni/events/TextMessage;

    .line 18053
    iget v2, v2, Lcom/teamspeak/ts3client/jni/events/TextMessage;->a:I

    .line 327
    if-ne v2, v7, :cond_47

    .line 19033
    iget v2, v0, Lcom/teamspeak/ts3client/jni/events/TextMessage;->c:I

    .line 327
    iget-object v3, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19242
    iget v3, v3, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 327
    if-ne v2, v3, :cond_47

    move-object v2, p1

    .line 328
    check-cast v2, Lcom/teamspeak/ts3client/jni/events/TextMessage;

    .line 20057
    iget v2, v2, Lcom/teamspeak/ts3client/jni/events/TextMessage;->b:I

    .line 328
    iget-object v3, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 20097
    iget-object v3, v3, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 20235
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 328
    if-ne v2, v3, :cond_47

    .line 329
    new-instance v2, Lcom/teamspeak/ts3client/chat/y;

    .line 21037
    iget-object v3, v0, Lcom/teamspeak/ts3client/jni/events/TextMessage;->d:Ljava/lang/String;

    .line 21041
    iget-object v4, v0, Lcom/teamspeak/ts3client/jni/events/TextMessage;->e:Ljava/lang/String;

    .line 21045
    iget-object v0, v0, Lcom/teamspeak/ts3client/jni/events/TextMessage;->f:Ljava/lang/String;

    .line 329
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/teamspeak/ts3client/chat/y;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 330
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/chat/a;->a(Lcom/teamspeak/ts3client/chat/y;)V

    .line 331
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 21238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 331
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 332
    iput-boolean v8, p0, Lcom/teamspeak/ts3client/chat/j;->au:Z

    .line 335
    :cond_47
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    if-eqz v0, :cond_f1

    move-object v0, p1

    .line 336
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 22033
    iget-object v2, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;->a:Ljava/lang/String;

    .line 337
    const-string v3, "invalid clientID"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b8

    .line 22041
    iget-object v2, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 337
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Send Message to Client "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 22097
    iget-object v4, v4, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 22235
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 337
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b8

    .line 338
    iget-boolean v2, p0, Lcom/teamspeak/ts3client/chat/j;->au:Z

    if-eqz v2, :cond_9f

    iget-boolean v2, p0, Lcom/teamspeak/ts3client/chat/j;->av:Z

    if-nez v2, :cond_9f

    .line 339
    iput-boolean v7, p0, Lcom/teamspeak/ts3client/chat/j;->av:Z

    .line 340
    new-instance v0, Lcom/teamspeak/ts3client/chat/y;

    const-string v2, ""

    const-string v3, "Chat partner disconnected out of view"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/chat/y;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 341
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/chat/a;->a(Lcom/teamspeak/ts3client/chat/y;)V

    .line 342
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 23061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 342
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 367
    :cond_9e
    :goto_9e
    return-void

    .line 345
    :cond_9f
    iput-boolean v7, p0, Lcom/teamspeak/ts3client/chat/j;->au:Z

    .line 346
    iget-object v2, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 24061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 24234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 346
    iget-object v3, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 25061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 25267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 346
    iget-object v3, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 26097
    iget-object v3, v3, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 26188
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 346
    const-string v6, "Request clientid"

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestClientIDs(JLjava/lang/String;Ljava/lang/String;)I

    .line 27033
    :cond_b8
    iget-object v2, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;->a:Ljava/lang/String;

    .line 348
    const-string v3, "database empty result set"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f1

    .line 27041
    iget-object v0, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 348
    const-string v2, "Request clientid"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f1

    .line 349
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/chat/j;->av:Z

    if-nez v0, :cond_f1

    .line 350
    iput-boolean v7, p0, Lcom/teamspeak/ts3client/chat/j;->av:Z

    .line 351
    new-instance v0, Lcom/teamspeak/ts3client/chat/y;

    const-string v2, ""

    const-string v3, "Chat partner disconnected out of view"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/chat/y;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 352
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/chat/a;->a(Lcom/teamspeak/ts3client/chat/y;)V

    .line 353
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 27061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 27238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 353
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 357
    :cond_f1
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ClientIDs;

    if-eqz v0, :cond_110

    move-object v0, p1

    .line 358
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/ClientIDs;

    .line 28029
    iget-object v1, v0, Lcom/teamspeak/ts3client/jni/events/ClientIDs;->a:Ljava/lang/String;

    .line 359
    iget-object v2, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 28097
    iget-object v2, v2, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 28188
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 359
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_110

    .line 360
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->g:Lcom/teamspeak/ts3client/chat/a;

    .line 29097
    iget-object v1, v1, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 30033
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/ClientIDs;->b:I

    .line 30239
    iput v0, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 361
    iput-boolean v8, p0, Lcom/teamspeak/ts3client/chat/j;->av:Z

    .line 364
    :cond_110
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ClientIDsFinished;

    if-eqz v0, :cond_9e

    .line 365
    invoke-direct {p0, v8}, Lcom/teamspeak/ts3client/chat/j;->a(Z)V

    goto :goto_9e
.end method

.method protected final b(Ljava/lang/String;)V
    .registers 9

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 90
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    .line 92
    iget-object v2, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    .line 93
    if-eq v1, v2, :cond_60

    .line 94
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x7

    invoke-virtual {p1, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 95
    iget-object v3, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    add-int/lit8 v3, v1, 0x3

    add-int/2addr v2, v3

    sub-int v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 106
    :goto_5f
    return-void

    .line 98
    :cond_60
    iget-object v2, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v2, v3, :cond_8a

    .line 99
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 102
    :goto_7d
    iget-object v2, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    add-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_5f

    .line 101
    :cond_8a
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_7d
.end method

.method public final c(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 110
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    .line 111
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 321
    return-void
.end method

.method public final s()V
    .registers 5

    .prologue
    .line 304
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->s()V

    .line 305
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->f:Lcom/teamspeak/ts3client/chat/h;

    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->c:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/h;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 306
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->e:Lcom/teamspeak/ts3client/chat/MListView;

    new-instance v1, Lcom/teamspeak/ts3client/chat/m;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/chat/m;-><init>(Lcom/teamspeak/ts3client/chat/j;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/chat/MListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 316
    return-void
.end method

.method public final t()V
    .registers 4

    .prologue
    .line 294
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->t()V

    .line 295
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->d:Lcom/teamspeak/ts3client/Ts3Application;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/Ts3Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 296
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->i:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 298
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/j;->f:Lcom/teamspeak/ts3client/chat/h;

    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/j;->c:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/h;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 299
    return-void
.end method

.method public final y()V
    .registers 1

    .prologue
    .line 87
    return-void
.end method
