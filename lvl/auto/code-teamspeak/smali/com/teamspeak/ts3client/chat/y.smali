.class public final Lcom/teamspeak/ts3client/chat/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Landroid/text/Spanned;

.field d:Z

.field e:Ljava/util/Date;

.field f:Z

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .registers 11

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/chat/y;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/chat/y;->d:Z

    .line 17
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/chat/y;->f:Z

    .line 20
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "*** "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 22
    :cond_1d
    if-eqz p1, :cond_25

    .line 23
    invoke-static {p1}, Lcom/teamspeak/ts3client/data/d/x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/y;->a:Ljava/lang/String;

    .line 24
    :cond_25
    iput-object p2, p0, Lcom/teamspeak/ts3client/chat/y;->g:Ljava/lang/String;

    .line 25
    invoke-static {p3}, Lcom/teamspeak/ts3client/data/d/x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/y;->b:Ljava/lang/String;

    .line 26
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/y;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/a;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/y;->c:Landroid/text/Spanned;

    .line 27
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/y;->e:Ljava/util/Date;

    .line 28
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/chat/y;->d:Z

    .line 29
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/chat/y;->f:Z

    .line 30
    return-void
.end method

.method private b()Landroid/text/Spanned;
    .registers 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/y;->c:Landroid/text/Spanned;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/y;->a:Ljava/lang/String;

    return-object v0
.end method

.method private d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/y;->g:Ljava/lang/String;

    return-object v0
.end method

.method private e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/y;->b:Ljava/lang/String;

    return-object v0
.end method

.method private f()Ljava/util/Date;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/y;->e:Ljava/util/Date;

    return-object v0
.end method

.method private g()Z
    .registers 2

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/chat/y;->f:Z

    return v0
.end method


# virtual methods
.method public final a()Z
    .registers 2

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/chat/y;->d:Z

    return v0
.end method
