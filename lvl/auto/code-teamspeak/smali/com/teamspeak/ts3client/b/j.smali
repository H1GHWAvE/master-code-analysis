.class public final Lcom/teamspeak/ts3client/b/j;
.super Landroid/widget/Filter;
.source "SourceFile"


# instance fields
.field a:Ljava/util/Vector;

.field final synthetic b:Lcom/teamspeak/ts3client/b/f;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/b/f;)V
    .registers 3

    .prologue
    .line 257
    iput-object p1, p0, Lcom/teamspeak/ts3client/b/j;->b:Lcom/teamspeak/ts3client/b/f;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 258
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/b/j;->a:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method protected final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .registers 6

    .prologue
    .line 262
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 263
    if-eqz p1, :cond_4b

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4b

    .line 264
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/b/j;->a:Ljava/util/Vector;

    .line 265
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/j;->b:Lcom/teamspeak/ts3client/b/f;

    iget-object v0, v0, Lcom/teamspeak/ts3client/b/f;->b:Lcom/teamspeak/ts3client/b/b;

    invoke-static {v0}, Lcom/teamspeak/ts3client/b/b;->b(Lcom/teamspeak/ts3client/b/b;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_24
    :goto_24
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;

    .line 1057
    iget-object v3, v0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 266
    invoke-virtual {v3, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_24

    .line 267
    iget-object v3, p0, Lcom/teamspeak/ts3client/b/j;->a:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_24

    .line 269
    :cond_3e
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/j;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 270
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/j;->a:Ljava/util/Vector;

    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 275
    :goto_4a
    return-object v1

    .line 272
    :cond_4b
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/j;->b:Lcom/teamspeak/ts3client/b/f;

    iget-object v0, v0, Lcom/teamspeak/ts3client/b/f;->b:Lcom/teamspeak/ts3client/b/b;

    invoke-static {v0}, Lcom/teamspeak/ts3client/b/b;->b(Lcom/teamspeak/ts3client/b/b;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 273
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/j;->b:Lcom/teamspeak/ts3client/b/f;

    iget-object v0, v0, Lcom/teamspeak/ts3client/b/f;->b:Lcom/teamspeak/ts3client/b/b;

    invoke-static {v0}, Lcom/teamspeak/ts3client/b/b;->b(Lcom/teamspeak/ts3client/b/b;)Ljava/util/Vector;

    move-result-object v0

    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    goto :goto_4a
.end method

.method protected final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .registers 5

    .prologue
    .line 280
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1077
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 280
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/b/k;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/b/k;-><init>(Lcom/teamspeak/ts3client/b/j;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 291
    return-void
.end method
