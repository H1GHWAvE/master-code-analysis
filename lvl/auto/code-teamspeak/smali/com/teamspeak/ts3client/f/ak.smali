.class public final Lcom/teamspeak/ts3client/f/ak;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:J

.field private b:Z

.field private c:Landroid/content/BroadcastReceiver;

.field private d:Landroid/widget/SeekBar;

.field private e:Landroid/widget/TextView;

.field private f:Ljava/lang/String;

.field private g:Lcom/teamspeak/ts3client/f/am;

.field private h:Landroid/support/v4/app/bi;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;)V
    .registers 13

    .prologue
    const/16 v6, 0x12

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x2

    .line 70
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 47
    new-instance v0, Lcom/teamspeak/ts3client/f/al;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/f/al;-><init>(Lcom/teamspeak/ts3client/f/ak;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/f/ak;->c:Landroid/content/BroadcastReceiver;

    .line 71
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/ak;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    invoke-virtual {p0, v2, v6, v2, v6}, Lcom/teamspeak/ts3client/f/ak;->setPadding(IIII)V

    .line 73
    iput-object p5, p0, Lcom/teamspeak/ts3client/f/ak;->h:Landroid/support/v4/app/bi;

    .line 74
    iput-object p3, p0, Lcom/teamspeak/ts3client/f/ak;->f:Ljava/lang/String;

    .line 75
    iput-object p4, p0, Lcom/teamspeak/ts3client/f/ak;->j:Ljava/lang/String;

    .line 76
    iput-object p2, p0, Lcom/teamspeak/ts3client/f/ak;->i:Ljava/lang/String;

    .line 77
    const v0, 0x7f0200b6

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/ak;->setBackgroundResource(I)V

    .line 78
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 79
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 81
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/ak;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 83
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 84
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setId(I)V

    .line 85
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/ak;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 87
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 88
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setId(I)V

    .line 90
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 91
    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 92
    invoke-virtual {p0, v0, v2}, Lcom/teamspeak/ts3client/f/ak;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 94
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 95
    const/4 v3, 0x3

    invoke-virtual {v0}, Landroid/widget/TextView;->getId()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 96
    invoke-virtual {p0, v1, v2}, Lcom/teamspeak/ts3client/f/ak;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 98
    invoke-virtual {p0, v5}, Lcom/teamspeak/ts3client/f/ak;->setClickable(Z)V

    .line 99
    invoke-virtual {p0, p0}, Lcom/teamspeak/ts3client/f/ak;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/ak;J)J
    .registers 4

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/teamspeak/ts3client/f/ak;->a:J

    return-wide p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/ak;Landroid/widget/SeekBar;)Landroid/widget/SeekBar;
    .registers 2

    .prologue
    .line 43
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/ak;->d:Landroid/widget/SeekBar;

    return-object p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/ak;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 43
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/ak;->e:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/ak;)Z
    .registers 2

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/f/ak;->b:Z

    return v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/ak;Z)Z
    .registers 2

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/f/ak;->b:Z

    return p1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/f/ak;)J
    .registers 3

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/teamspeak/ts3client/f/ak;->a:J

    return-wide v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/SeekBar;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ak;->d:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/f/ak;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ak;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ak;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/f/ak;)Lcom/teamspeak/ts3client/f/am;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ak;->g:Lcom/teamspeak/ts3client/f/am;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/f/ak;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ak;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/f/ak;)Landroid/content/BroadcastReceiver;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ak;->c:Landroid/content/BroadcastReceiver;

    return-object v0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 5

    .prologue
    .line 105
    new-instance v0, Lcom/teamspeak/ts3client/f/am;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/teamspeak/ts3client/f/am;-><init>(Lcom/teamspeak/ts3client/f/ak;B)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/f/ak;->g:Lcom/teamspeak/ts3client/f/am;

    .line 106
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ak;->g:Lcom/teamspeak/ts3client/f/am;

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/ak;->h:Landroid/support/v4/app/bi;

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/ak;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/f/am;->a(Landroid/support/v4/app/bi;Ljava/lang/String;)V

    .line 107
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 108
    return-void
.end method
