.class final Lcom/teamspeak/ts3client/f/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/f/am;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/f/am;)V
    .registers 2

    .prologue
    .line 112
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/an;->a:Lcom/teamspeak/ts3client/f/am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 5

    .prologue
    .line 116
    :goto_0
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/an;->a:Lcom/teamspeak/ts3client/f/am;

    iget-boolean v0, v0, Lcom/teamspeak/ts3client/f/am;->at:Z

    if-eqz v0, :cond_31

    .line 117
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/an;->a:Lcom/teamspeak/ts3client/f/am;

    iget-object v1, v1, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    const-string v1, "decibel_last_period"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getPreProcessorInfoValueFloat(JLjava/lang/String;)F

    move-result v0

    .line 119
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/an;->a:Lcom/teamspeak/ts3client/f/am;

    iget-object v1, v1, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->c(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/lit8 v0, v0, 0x32

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 121
    const-wide/16 v0, 0x14

    :try_start_2b
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2e
    .catch Ljava/lang/InterruptedException; {:try_start_2b .. :try_end_2e} :catch_2f

    goto :goto_0

    .line 124
    :catch_2f
    move-exception v0

    goto :goto_0

    .line 125
    :cond_31
    return-void
.end method
