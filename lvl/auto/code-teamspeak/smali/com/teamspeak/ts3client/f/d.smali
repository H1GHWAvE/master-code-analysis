.class final Lcom/teamspeak/ts3client/f/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/Ts3Application;

.field final synthetic b:Lcom/teamspeak/ts3client/f/c;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/f/c;Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 3

    .prologue
    .line 65
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/d;->b:Lcom/teamspeak/ts3client/f/c;

    iput-object p2, p0, Lcom/teamspeak/ts3client/f/d;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .registers 8

    .prologue
    const/4 v4, 0x0

    .line 69
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/d;->b:Lcom/teamspeak/ts3client/f/c;

    invoke-static {v0}, Lcom/teamspeak/ts3client/f/c;->a(Lcom/teamspeak/ts3client/f/c;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_88

    .line 70
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/d;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/d;->b:Lcom/teamspeak/ts3client/f/c;

    invoke-static {v2}, Lcom/teamspeak/ts3client/f/c;->b(Lcom/teamspeak/ts3client/f/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_warn"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3a

    iget-object v0, p0, Lcom/teamspeak/ts3client/f/d;->b:Lcom/teamspeak/ts3client/f/c;

    invoke-static {v0}, Lcom/teamspeak/ts3client/f/c;->c(Lcom/teamspeak/ts3client/f/c;)Z

    move-result v0

    if-eqz v0, :cond_72

    :cond_3a
    if-eqz p2, :cond_72

    .line 71
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/d;->b:Lcom/teamspeak/ts3client/f/c;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/f/c;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 72
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 73
    const-string v1, "Warning"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/d;->b:Lcom/teamspeak/ts3client/f/c;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/c;->a(Lcom/teamspeak/ts3client/f/c;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 75
    const/4 v1, -0x1

    const-string v2, "button.ok"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/f/e;

    invoke-direct {v3, p0, p2, v0}, Lcom/teamspeak/ts3client/f/e;-><init>(Lcom/teamspeak/ts3client/f/d;ZLandroid/app/AlertDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 85
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 86
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 92
    :goto_71
    return-void

    .line 88
    :cond_72
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/d;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 88
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/d;->b:Lcom/teamspeak/ts3client/f/c;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/c;->b(Lcom/teamspeak/ts3client/f/c;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_71

    .line 90
    :cond_88
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/d;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 90
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/d;->b:Lcom/teamspeak/ts3client/f/c;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/c;->b(Lcom/teamspeak/ts3client/f/c;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_71
.end method
