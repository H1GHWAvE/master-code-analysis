.class public final Lcom/teamspeak/ts3client/f/f;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/widget/EditText;

.field private b:Ljava/lang/String;

.field private c:Lcom/teamspeak/ts3client/f/h;

.field private d:Landroid/support/v4/app/bi;

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;I)V
    .registers 18

    .prologue
    .line 36
    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v9}, Lcom/teamspeak/ts3client/f/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;IB)V

    .line 37
    move/from16 v0, p7

    iput v0, p0, Lcom/teamspeak/ts3client/f/f;->i:I

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;IB)V
    .registers 16

    .prologue
    const/16 v6, 0x12

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, -0x2

    .line 41
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 34
    iput v2, p0, Lcom/teamspeak/ts3client/f/f;->i:I

    .line 42
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/f;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 43
    invoke-virtual {p0, v2, v6, v2, v6}, Lcom/teamspeak/ts3client/f/f;->setPadding(IIII)V

    .line 44
    const v0, 0x7f0200b6

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/f;->setBackgroundResource(I)V

    .line 45
    iput-object p6, p0, Lcom/teamspeak/ts3client/f/f;->d:Landroid/support/v4/app/bi;

    .line 46
    iput-object p4, p0, Lcom/teamspeak/ts3client/f/f;->b:Ljava/lang/String;

    .line 47
    iput-object p5, p0, Lcom/teamspeak/ts3client/f/f;->h:Ljava/lang/String;

    .line 48
    iput p7, p0, Lcom/teamspeak/ts3client/f/f;->e:I

    .line 49
    iput-object p2, p0, Lcom/teamspeak/ts3client/f/f;->f:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/teamspeak/ts3client/f/f;->g:Ljava/lang/String;

    .line 51
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 52
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 53
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 55
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 56
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setId(I)V

    .line 57
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/f;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 59
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 60
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setId(I)V

    .line 62
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 63
    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 64
    invoke-virtual {p0, v0, v2}, Lcom/teamspeak/ts3client/f/f;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 67
    const/4 v3, 0x3

    invoke-virtual {v0}, Landroid/widget/TextView;->getId()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 68
    invoke-virtual {p0, v1, v2}, Lcom/teamspeak/ts3client/f/f;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 70
    invoke-virtual {p0, v5}, Lcom/teamspeak/ts3client/f/f;->setClickable(Z)V

    .line 71
    invoke-virtual {p0, p0}, Lcom/teamspeak/ts3client/f/f;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/f;Landroid/widget/EditText;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 25
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/f;->a:Landroid/widget/EditText;

    return-object p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/f;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/f;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/f/f;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/f;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/f/f;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/f;->a:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/f/f;)I
    .registers 2

    .prologue
    .line 25
    iget v0, p0, Lcom/teamspeak/ts3client/f/f;->e:I

    return v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/f/f;)I
    .registers 2

    .prologue
    .line 25
    iget v0, p0, Lcom/teamspeak/ts3client/f/f;->i:I

    return v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/f/f;)Lcom/teamspeak/ts3client/f/h;
    .registers 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/f;->c:Lcom/teamspeak/ts3client/f/h;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/f/f;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/f;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 5

    .prologue
    .line 77
    new-instance v0, Lcom/teamspeak/ts3client/f/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/teamspeak/ts3client/f/h;-><init>(Lcom/teamspeak/ts3client/f/f;B)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/f/f;->c:Lcom/teamspeak/ts3client/f/h;

    .line 78
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/f;->c:Lcom/teamspeak/ts3client/f/h;

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/f;->d:Landroid/support/v4/app/bi;

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/f/h;->a(Landroid/support/v4/app/bi;Ljava/lang/String;)V

    .line 79
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 80
    return-void
.end method
