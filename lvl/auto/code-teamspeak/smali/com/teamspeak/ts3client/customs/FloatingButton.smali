.class public Lcom/teamspeak/ts3client/customs/FloatingButton;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field a:Landroid/os/Handler;

.field b:I

.field public c:Z

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Bitmap;

.field private g:Landroid/graphics/Paint;

.field private h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->d:Landroid/graphics/Paint;

    .line 22
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->e:Landroid/graphics/Paint;

    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->a:Landroid/os/Handler;

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->b:I

    .line 26
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->g:Landroid/graphics/Paint;

    .line 28
    new-instance v0, Lcom/teamspeak/ts3client/customs/i;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/customs/i;-><init>(Lcom/teamspeak/ts3client/customs/FloatingButton;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->h:Ljava/lang/Runnable;

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/teamspeak/ts3client/customs/FloatingButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/16 v1, 0xf

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 57
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->d:Landroid/graphics/Paint;

    .line 22
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->e:Landroid/graphics/Paint;

    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->a:Landroid/os/Handler;

    .line 24
    iput v7, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->b:I

    .line 26
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->g:Landroid/graphics/Paint;

    .line 28
    new-instance v0, Lcom/teamspeak/ts3client/customs/i;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/customs/i;-><init>(Lcom/teamspeak/ts3client/customs/FloatingButton;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->h:Ljava/lang/Runnable;

    .line 58
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 59
    invoke-virtual {p0, v6}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setClickable(Z)V

    .line 60
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 61
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00cc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->d:Landroid/graphics/Paint;

    const/high16 v1, 0x41200000    # 10.0f

    const/4 v2, 0x0

    const/high16 v3, 0x40600000    # 3.5f

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b009a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 63
    invoke-virtual {p0, v7}, Lcom/teamspeak/ts3client/customs/FloatingButton;->a(Z)V

    .line 64
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_70

    .line 65
    const/4 v0, 0x0

    invoke-virtual {p0, v6, v0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setLayerType(ILandroid/graphics/Paint;)V

    .line 66
    :cond_70
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/customs/FloatingButton;)Landroid/graphics/Paint;
    .registers 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->g:Landroid/graphics/Paint;

    return-object v0
.end method

.method private a()V
    .registers 2

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->c:Z

    if-eqz v0, :cond_5

    .line 93
    :goto_4
    return-void

    .line 91
    :cond_5
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->a(Z)V

    .line 92
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->invalidate()V

    goto :goto_4
.end method

.method private b()V
    .registers 2

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->a(Z)V

    .line 97
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->invalidate()V

    .line 98
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .registers 8

    .prologue
    const/high16 v5, 0x40a00000    # 5.0f

    const/4 v4, 0x0

    const v3, -0x42333333    # -0.1f

    .line 69
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->c:Z

    .line 70
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 71
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00cc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 72
    if-nez p1, :cond_32

    .line 73
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v5, v4, v3, v1}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 76
    :goto_31
    return-void

    .line 75
    :cond_32
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00cf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v5, v4, v3, v1}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    goto :goto_31
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 8

    .prologue
    const-wide v4, 0x4004cccccccccccdL    # 2.6

    .line 102
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getWidth()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v4

    double-to-float v2, v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 103
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getWidth()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v4

    double-to-float v2, v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 104
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_60

    .line 105
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->f:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 106
    :cond_60
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->c:Z

    if-eqz v0, :cond_6d

    .line 107
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->h:Ljava/lang/Runnable;

    const-wide/16 v2, 0x28

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 108
    :cond_6d
    return-void
.end method

.method protected onMeasure(II)V
    .registers 6

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getSuggestedMinimumWidth()I

    move-result v0

    .line 126
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 128
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getSuggestedMinimumHeight()I

    move-result v1

    .line 129
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 131
    invoke-virtual {p0, v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setMeasuredDimension(II)V

    .line 132
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 4

    .prologue
    .line 113
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1f

    .line 114
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 118
    :goto_12
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 119
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->invalidate()V

    .line 120
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 116
    :cond_1f
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_12
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .registers 2

    .prologue
    .line 84
    iput-object p1, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->f:Landroid/graphics/Bitmap;

    .line 85
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->invalidate()V

    .line 86
    return-void
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 79
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/FloatingButton;->f:Landroid/graphics/Bitmap;

    .line 80
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->invalidate()V

    .line 81
    return-void
.end method
