.class public Lcom/teamspeak/ts3client/customs/CustomCodecSettings;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Lcom/teamspeak/ts3client/data/d/ab;

.field private b:I

.field private c:Landroid/widget/SeekBar;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/teamspeak/ts3client/Ts3Application;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/Spinner;

.field private i:Landroid/widget/Spinner;

.field private j:Z

.field private k:Ljava/util/BitSet;

.field private l:I

.field private m:Lcom/teamspeak/ts3client/customs/a;

.field private n:Lcom/teamspeak/ts3client/customs/a;

.field private o:Z

.field private p:Z

.field private q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6

    .prologue
    const/4 v3, 0x0

    .line 47
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    new-instance v0, Lcom/teamspeak/ts3client/data/d/ab;

    invoke-direct {v0, v3, v3}, Lcom/teamspeak/ts3client/data/d/ab;-><init>(II)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 29
    iput v3, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b:I

    .line 37
    iput-boolean v3, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->j:Z

    .line 38
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    .line 39
    iput v3, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 49
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 50
    const v1, 0x7f030020

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 51
    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a(Landroid/view/View;)V

    .line 52
    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->addView(Landroid/view/View;)V

    .line 53
    invoke-virtual {v0}, Landroid/view/View;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_3b

    .line 54
    invoke-direct {p0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b()V

    .line 55
    :cond_3b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 58
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-instance v0, Lcom/teamspeak/ts3client/data/d/ab;

    invoke-direct {v0, v3, v3}, Lcom/teamspeak/ts3client/data/d/ab;-><init>(II)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 29
    iput v3, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b:I

    .line 37
    iput-boolean v3, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->j:Z

    .line 38
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    .line 39
    iput v3, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    .line 59
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 60
    const v1, 0x7f030020

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Landroid/view/View;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_35

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 64
    invoke-direct {p0, v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a(Landroid/view/View;)V

    .line 66
    :cond_35
    invoke-virtual {p0, v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->addView(Landroid/view/View;)V

    .line 67
    invoke-virtual {v1}, Landroid/view/View;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_41

    .line 68
    invoke-direct {p0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b()V

    .line 69
    :cond_41
    return-void
.end method

.method private a()V
    .registers 8

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 220
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_20

    .line 221
    iget v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 1144
    iget v1, v1, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 221
    if-ge v0, v1, :cond_17

    .line 222
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    iget v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    .line 1148
    iput v1, v0, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 223
    :cond_17
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 2144
    iget v1, v1, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 223
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 225
    :cond_20
    iget v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    if-nez v0, :cond_29

    .line 226
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 228
    :cond_29
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->b:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_3b

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_53

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_53

    .line 229
    :cond_3b
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v6}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 230
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_53

    .line 231
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 3136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 231
    if-ne v0, v6, :cond_4e

    .line 232
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 3140
    iput v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 233
    :cond_4e
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 236
    :cond_53
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->c:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_65

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_7d

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_7d

    .line 237
    :cond_65
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 238
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_7d

    .line 239
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 4136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 239
    if-ne v0, v2, :cond_78

    .line 240
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 4140
    iput v4, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 241
    :cond_78
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 244
    :cond_7d
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->d:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_8f

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_a7

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_a7

    .line 245
    :cond_8f
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 246
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_a7

    .line 247
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 5136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 247
    if-ne v0, v4, :cond_a2

    .line 248
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 5140
    iput v3, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 249
    :cond_a2
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 252
    :cond_a7
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->e:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_b9

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_d3

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_d3

    .line 253
    :cond_b9
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 254
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_d3

    .line 255
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 6136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 255
    if-nez v0, :cond_cd

    .line 256
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 6140
    const/4 v1, 0x5

    iput v1, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 257
    :cond_cd
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 260
    :cond_d3
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->a:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_e5

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_ff

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_ff

    .line 261
    :cond_e5
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 262
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_ff

    .line 263
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 7136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 263
    const/4 v1, 0x5

    if-ne v0, v1, :cond_fa

    .line 264
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 7140
    iput v6, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 265
    :cond_fa
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v6}, Landroid/widget/Spinner;->setSelection(I)V

    .line 268
    :cond_ff
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->f:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_111

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_129

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_129

    .line 269
    :cond_111
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 270
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_129

    .line 271
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 8136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 271
    if-ne v0, v5, :cond_124

    .line 272
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 8140
    iput v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 273
    :cond_124
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 276
    :cond_129
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->a:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_142

    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->f:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_142

    .line 277
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 279
    :cond_142
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->e:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_174

    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->d:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_174

    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->c:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_174

    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->b:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_174

    .line 280
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 281
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 283
    :cond_174
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->a:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_192

    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->f:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_192

    .line 284
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 285
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 288
    :cond_192
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1a3

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_1b7

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_1b7

    .line 289
    :cond_1a3
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 290
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 291
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 292
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 294
    :cond_1b7
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_1c4

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->p:Z

    if-nez v0, :cond_1c4

    .line 295
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 298
    :cond_1c4
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 6

    .prologue
    const/4 v3, 0x1

    .line 72
    const-string v0, "channeldialog.editpreset.text"

    const v1, 0x7f0c00a2

    invoke-static {v0, p1, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 73
    const-string v0, "channeldialog.editcodec.text"

    const v1, 0x7f0c00a5

    invoke-static {v0, p1, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 74
    const-string v0, "channeldialog.editquality.text"

    const v1, 0x7f0c00a8

    invoke-static {v0, p1, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 75
    const v0, 0x7f0c00a9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    .line 76
    const v0, 0x7f0c00ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->d:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f0c00ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->f:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f0c00aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->g:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0c00a6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    .line 80
    const v0, 0x7f0c00a3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->i:Landroid/widget/Spinner;

    .line 81
    const-string v0, "channeldialog.editpreset"

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->b(Ljava/lang/String;Landroid/content/Context;I)Lcom/teamspeak/ts3client/customs/a;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    .line 82
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->i:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 83
    const-string v0, "channeldialog.editcodec"

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->b(Ljava/lang/String;Landroid/content/Context;I)Lcom/teamspeak/ts3client/customs/a;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    .line 84
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 85
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 86
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 87
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 88
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setFocusableInTouchMode(Z)V

    .line 89
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->i:Landroid/widget/Spinner;

    new-instance v1, Lcom/teamspeak/ts3client/customs/b;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/customs/b;-><init>(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 132
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    new-instance v1, Lcom/teamspeak/ts3client/customs/c;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/customs/c;-><init>(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 167
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    new-instance v1, Lcom/teamspeak/ts3client/customs/d;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/customs/d;-><init>(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 216
    invoke-direct {p0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->getPerms()V

    .line 217
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Z
    .registers 2

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->j:Z

    return v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;Z)Z
    .registers 2

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->j:Z

    return p1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Ljava/util/BitSet;
    .registers 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    return-object v0
.end method

.method private b()V
    .registers 8

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 325
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 25136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 325
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 25144
    iget v1, v1, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 325
    iget-object v2, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    iget v3, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    invoke-static {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/y;->a(IILjava/util/BitSet;I)Lcom/teamspeak/ts3client/data/d/aa;

    move-result-object v0

    .line 326
    iput-boolean v5, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->j:Z

    .line 327
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 26144
    iget v2, v2, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 327
    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 328
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 27136
    iget v2, v2, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 328
    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 329
    sget-object v1, Lcom/teamspeak/ts3client/customs/e;->a:[I

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/d/aa;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_54

    .line 343
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->i:Landroid/widget/Spinner;

    invoke-virtual {v0, v6}, Landroid/widget/Spinner;->setSelection(I)V

    .line 345
    :goto_37
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->j:Z

    .line 347
    return-void

    .line 331
    :pswitch_3a
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->i:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_37

    .line 334
    :pswitch_40
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->i:Landroid/widget/Spinner;

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_37

    .line 337
    :pswitch_46
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->i:Landroid/widget/Spinner;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_37

    .line 340
    :pswitch_4d
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->i:Landroid/widget/Spinner;

    invoke-virtual {v0, v6}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_37

    .line 329
    nop

    :pswitch_data_54
    .packed-switch 0x1
        :pswitch_3a
        :pswitch_40
        :pswitch_46
        :pswitch_4d
    .end packed-switch
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)I
    .registers 2

    .prologue
    .line 26
    iget v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    return v0
.end method

.method private c()V
    .registers 9

    .prologue
    .line 362
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/d/y;->a(II)J

    move-result-wide v0

    .line 363
    iget-object v2, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->d:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/text/DecimalFormat;

    const-string v5, "#.##"

    invoke-direct {v4, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    long-to-float v5, v0

    const/high16 v6, 0x44800000    # 1024.0f

    div-float/2addr v5, v6

    const/high16 v6, 0x41000000    # 8.0f

    div-float/2addr v5, v6

    float-to-double v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " KiB/s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->setWarn(J)V

    .line 365
    return-void
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/SeekBar;
    .registers 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Lcom/teamspeak/ts3client/data/d/ab;
    .registers 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V
    .registers 9

    .prologue
    .line 26
    .line 35362
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/d/y;->a(II)J

    move-result-wide v0

    .line 35363
    iget-object v2, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->d:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/text/DecimalFormat;

    const-string v5, "#.##"

    invoke-direct {v4, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    long-to-float v5, v0

    const/high16 v6, 0x44800000    # 1024.0f

    div-float/2addr v5, v6

    const/high16 v6, 0x41000000    # 8.0f

    div-float/2addr v5, v6

    float-to-double v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " KiB/s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35364
    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->setWarn(J)V

    .line 26
    return-void
.end method

.method private getPerms()V
    .registers 4

    .prologue
    .line 301
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 301
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10250
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 301
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aQ:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 302
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 304
    :cond_1e
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 11061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 304
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12250
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 304
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aP:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 305
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 307
    :cond_3c
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 13061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 307
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 14061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 14250
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 307
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aN:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 308
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 310
    :cond_5a
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 15061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 310
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 16250
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 310
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aM:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_78

    .line 311
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 313
    :cond_78
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 313
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 18061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 18250
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 313
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aL:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_96

    .line 314
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 316
    :cond_96
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 316
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 20061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20250
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 316
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aO:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_b4

    .line 317
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 319
    :cond_b4
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 21181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 319
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->bj:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    .line 320
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 22181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 320
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->bk:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->p:Z

    .line 321
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 23061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 321
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 24061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 24250
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 321
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aR:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->b(I)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    .line 322
    return-void
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->i:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Z
    .registers 2

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    return v0
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)I
    .registers 2

    .prologue
    .line 26
    iget v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b:I

    return v0
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method private setWarn(J)V
    .registers 6

    .prologue
    .line 369
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->f:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    const-wide/16 v0, 0x7333

    cmp-long v0, p1, v0

    if-ltz v0, :cond_18

    .line 371
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->f:Landroid/widget/TextView;

    const-string v1, "codec.warning"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    :cond_18
    return-void
.end method


# virtual methods
.method public final a(Lcom/teamspeak/ts3client/data/d/ab;Z)V
    .registers 11

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 350
    iput-object p1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 351
    if-eqz p2, :cond_15

    .line 352
    sget-object v0, Lcom/teamspeak/ts3client/data/d/aa;->c:Lcom/teamspeak/ts3client/data/d/aa;

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    iget v2, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/d/y;->a(Lcom/teamspeak/ts3client/data/d/aa;Ljava/util/BitSet;I)Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 27144
    :cond_15
    iget v0, p1, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 353
    iput v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b:I

    .line 354
    iget v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b:I

    iget v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    if-ge v0, v1, :cond_23

    .line 355
    iget v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    iput v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b:I

    .line 356
    :cond_23
    iput-boolean p2, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    .line 27220
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_40

    .line 27221
    iget v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 28144
    iget v1, v1, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 27221
    if-ge v0, v1, :cond_37

    .line 27222
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    iget v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    .line 28148
    iput v1, v0, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 27223
    :cond_37
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 29144
    iget v1, v1, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 27223
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 27225
    :cond_40
    iget v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->l:I

    if-nez v0, :cond_49

    .line 27226
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 27228
    :cond_49
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->b:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_5b

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_73

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_73

    .line 27229
    :cond_5b
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v7}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27230
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_73

    .line 27231
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 30136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 27231
    if-ne v0, v7, :cond_6e

    .line 27232
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 30140
    iput v3, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 27233
    :cond_6e
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 27236
    :cond_73
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->c:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_85

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_9d

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_9d

    .line 27237
    :cond_85
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27238
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_9d

    .line 27239
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 31136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 27239
    if-ne v0, v3, :cond_98

    .line 27240
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 31140
    iput v5, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 27241
    :cond_98
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setSelection(I)V

    .line 27244
    :cond_9d
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->d:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_af

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_c7

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_c7

    .line 27245
    :cond_af
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27246
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_c7

    .line 27247
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 32136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 27247
    if-ne v0, v5, :cond_c2

    .line 27248
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 32140
    iput v4, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 27249
    :cond_c2
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 27252
    :cond_c7
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->e:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_d9

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_f3

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_f3

    .line 27253
    :cond_d9
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27254
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_f3

    .line 27255
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 33136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 27255
    if-nez v0, :cond_ed

    .line 27256
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 33140
    const/4 v1, 0x5

    iput v1, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 27257
    :cond_ed
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 27260
    :cond_f3
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->a:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_105

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_11f

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_11f

    .line 27261
    :cond_105
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27262
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_11f

    .line 27263
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 34136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 27263
    const/4 v1, 0x5

    if-ne v0, v1, :cond_11a

    .line 27264
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 34140
    iput v7, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 27265
    :cond_11a
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v7}, Landroid/widget/Spinner;->setSelection(I)V

    .line 27268
    :cond_11f
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->f:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_131

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_149

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_149

    .line 27269
    :cond_131
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->n:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v6}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27270
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-eqz v0, :cond_149

    .line 27271
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 35136
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 27271
    if-ne v0, v6, :cond_144

    .line 27272
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    .line 35140
    iput v3, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 27273
    :cond_144
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 27276
    :cond_149
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->a:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_162

    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->f:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_162

    .line 27277
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27279
    :cond_162
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->e:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_194

    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->d:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_194

    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->c:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_194

    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->b:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_194

    .line 27280
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27281
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27283
    :cond_194
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->a:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_1b2

    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/data/d/y;->f:I

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_1b2

    .line 27284
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27285
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v6}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27288
    :cond_1b2
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k:Ljava/util/BitSet;

    invoke-virtual {v0, v4}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1c3

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->q:Z

    if-nez v0, :cond_1d7

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_1d7

    .line 27289
    :cond_1c3
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27290
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27291
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27292
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->m:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v6}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 27294
    :cond_1d7
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->o:Z

    if-nez v0, :cond_1e4

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->p:Z

    if-nez v0, :cond_1e4

    .line 27295
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 358
    :cond_1e4
    invoke-direct {p0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b()V

    .line 359
    return-void
.end method

.method public getSettings()Lcom/teamspeak/ts3client/data/d/ab;
    .registers 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a:Lcom/teamspeak/ts3client/data/d/ab;

    return-object v0
.end method
