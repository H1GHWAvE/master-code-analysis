.class public final Lcom/teamspeak/ts3client/data/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/teamspeak/ts3client/data/x;


# static fields
.field private static d:Lcom/teamspeak/ts3client/data/v;


# instance fields
.field public a:Ljava/util/BitSet;

.field public b:Z

.field public c:Lcom/teamspeak/ts3client/data/y;

.field private e:Ljava/util/BitSet;

.field private f:Lcom/teamspeak/ts3client/Ts3Application;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/v;->a:Ljava/util/BitSet;

    .line 15
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/v;->e:Ljava/util/BitSet;

    .line 16
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/data/v;->b:Z

    .line 22
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/v;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 23
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 23
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 24
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 24
    const-string v1, "audio_ptt"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/v;->b:Z

    .line 25
    return-void
.end method

.method public static declared-synchronized a()Lcom/teamspeak/ts3client/data/v;
    .registers 2

    .prologue
    .line 28
    const-class v1, Lcom/teamspeak/ts3client/data/v;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/teamspeak/ts3client/data/v;->d:Lcom/teamspeak/ts3client/data/v;

    if-nez v0, :cond_e

    .line 29
    new-instance v0, Lcom/teamspeak/ts3client/data/v;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/v;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/data/v;->d:Lcom/teamspeak/ts3client/data/v;

    .line 31
    :cond_e
    sget-object v0, Lcom/teamspeak/ts3client/data/v;->d:Lcom/teamspeak/ts3client/data/v;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 28
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcom/teamspeak/ts3client/data/y;)V
    .registers 2

    .prologue
    .line 76
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/v;->c:Lcom/teamspeak/ts3client/data/y;

    .line 77
    return-void
.end method

.method private a(Z)V
    .registers 2

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/v;->b:Z

    .line 93
    return-void
.end method

.method private b()Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 58
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 72
    :cond_9
    :goto_9
    return v1

    .line 60
    :cond_a
    const/4 v0, 0x1

    .line 61
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/v;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v2

    :goto_11
    if-ltz v2, :cond_1c

    .line 62
    iget-object v3, p0, Lcom/teamspeak/ts3client/data/v;->a:Ljava/util/BitSet;

    invoke-virtual {v3, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    if-nez v3, :cond_31

    move v0, v1

    .line 67
    :cond_1c
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/v;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 67
    if-eqz v2, :cond_9

    .line 68
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/v;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7206
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 68
    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/t;->g(Z)V

    .line 69
    iget-boolean v2, p0, Lcom/teamspeak/ts3client/data/v;->g:Z

    if-eqz v2, :cond_9

    move v1, v0

    .line 70
    goto :goto_9

    .line 61
    :cond_31
    iget-object v3, p0, Lcom/teamspeak/ts3client/data/v;->e:Ljava/util/BitSet;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v3, v2}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v2

    goto :goto_11
.end method

.method private c()V
    .registers 2

    .prologue
    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/v;->c:Lcom/teamspeak/ts3client/data/y;

    .line 81
    return-void
.end method

.method private d()Ljava/util/BitSet;
    .registers 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->a:Ljava/util/BitSet;

    return-object v0
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/v;->b:Z

    return v0
.end method

.method private f()Ljava/util/BitSet;
    .registers 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->e:Ljava/util/BitSet;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/BitSet;Z)V
    .registers 3

    .prologue
    .line 100
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/v;->e:Ljava/util/BitSet;

    .line 101
    iput-boolean p2, p0, Lcom/teamspeak/ts3client/data/v;->g:Z

    .line 102
    return-void
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 36
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/v;->b:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->c:Lcom/teamspeak/ts3client/data/y;

    if-nez v0, :cond_a

    .line 3070
    :cond_9
    :goto_9
    return v1

    .line 38
    :cond_a
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_5c

    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->a:Ljava/util/BitSet;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_25

    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->a:Ljava/util/BitSet;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    .line 49
    :cond_25
    :goto_25
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/v;->b:Z

    if-eqz v0, :cond_84

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->c:Lcom/teamspeak/ts3client/data/y;

    if-nez v0, :cond_84

    .line 3058
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 3060
    const/4 v0, 0x1

    .line 3061
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/v;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v2

    :goto_3c
    if-ltz v2, :cond_47

    .line 3062
    iget-object v3, p0, Lcom/teamspeak/ts3client/data/v;->a:Ljava/util/BitSet;

    invoke-virtual {v3, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    if-nez v3, :cond_7b

    move v0, v1

    .line 3067
    :cond_47
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/v;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3067
    if-eqz v2, :cond_9

    .line 3068
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/v;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5206
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 3068
    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/t;->g(Z)V

    .line 3069
    iget-boolean v2, p0, Lcom/teamspeak/ts3client/data/v;->g:Z

    if-eqz v2, :cond_9

    move v1, v0

    .line 3070
    goto :goto_9

    .line 43
    :cond_5c
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->a:Ljava/util/BitSet;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 44
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->a:Ljava/util/BitSet;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->clear(I)V

    .line 45
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->c:Lcom/teamspeak/ts3client/data/y;

    if-eqz v0, :cond_25

    .line 46
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->c:Lcom/teamspeak/ts3client/data/y;

    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/y;->p_()V

    goto :goto_25

    .line 3061
    :cond_7b
    iget-object v3, p0, Lcom/teamspeak/ts3client/data/v;->e:Ljava/util/BitSet;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v3, v2}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v2

    goto :goto_3c

    .line 52
    :cond_84
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->c:Lcom/teamspeak/ts3client/data/y;

    if-eqz v0, :cond_9

    .line 53
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->c:Lcom/teamspeak/ts3client/data/y;

    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/y;->a()V

    goto/16 :goto_9
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 106
    const-string v0, "audio_ptt"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 107
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/v;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 107
    const-string v1, "audio_ptt"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/v;->b:Z

    .line 109
    :cond_15
    return-void
.end method
