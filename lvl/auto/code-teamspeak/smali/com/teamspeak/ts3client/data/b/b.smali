.class final Lcom/teamspeak/ts3client/data/b/b;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/data/b/a;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/b/a;Landroid/content/Context;)V
    .registers 6

    .prologue
    .line 221
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/b/b;->a:Lcom/teamspeak/ts3client/data/b/a;

    .line 222
    const-string v0, "Teamspeak-Bookmark"

    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 223
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 5

    .prologue
    .line 227
    const-string v0, "create table server (server_id integer primary key autoincrement, label text not null, address text not null, serverpassword blob null, nickname text not null, defaultchannel text not null, defaultchannelpassword text not null, ident int not null, subscribeall text not null,subscriptionlist int not null);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 228
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 229
    const-string v1, "label"

    const-string v2, "TeamSpeak Public"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const-string v1, "address"

    const-string v2, "voice.teamspeak.com"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const-string v1, "serverpassword"

    const-string v2, ""

    .line 1026
    invoke-static {v2}, Lcom/teamspeak/ts3client/data/b/a;->a(Ljava/lang/String;)[B

    move-result-object v2

    .line 231
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 232
    const-string v1, "nickname"

    const-string v2, "Android_Client"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v1, "defaultchannel"

    const-string v2, "Default Channel (Mobile)"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v1, "defaultchannelpassword"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v1, "ident"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 236
    const-string v1, "subscribeall"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 237
    const-string v1, "subscriptionlist"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    :try_start_53
    const-string v1, "server"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_59
    .catch Ljava/lang/Exception; {:try_start_53 .. :try_end_59} :catch_5a

    .line 242
    :goto_59
    return-void

    :catch_5a
    move-exception v0

    goto :goto_59
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 5

    .prologue
    .line 246
    if-ge p2, p3, :cond_15

    .line 247
    :goto_2
    add-int/lit8 v0, p2, -0x1

    if-ge v0, p3, :cond_15

    .line 248
    packed-switch p2, :pswitch_data_16

    .line 247
    :goto_9
    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    .line 254
    :pswitch_c
    const-string v0, "DROP TABLE IF EXISTS server"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 255
    invoke-virtual {p0, p1}, Lcom/teamspeak/ts3client/data/b/b;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_9

    .line 261
    :cond_15
    return-void

    .line 248
    :pswitch_data_16
    .packed-switch 0x5
        :pswitch_c
    .end packed-switch
.end method
