.class final Lcom/teamspeak/ts3client/data/b/d;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/data/b/c;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/b/c;Landroid/content/Context;)V
    .registers 6

    .prologue
    .line 268
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/b/d;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 269
    const-string v0, "Teamspeak-Contacts"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 270
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3

    .prologue
    .line 274
    const-string v0, "create table contacts (contact_id integer primary key autoincrement, u_identifier text not null, customname text not null , display integer not null, status integer not null, mute integer not null, ignorepublicchat integer not null, ignoreprivatechat integer not null, ignorepokes integer not null, hideaway integer not null,hideavatar integer not null, whisperallow integer not null, volumemodifier float not null);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 275
    const-string v0, "Create Index u_identifier_idx ON contacts(u_identifier);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 276
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 5

    .prologue
    .line 280
    if-ge p2, p3, :cond_12

    .line 281
    :goto_2
    add-int/lit8 v0, p2, -0x1

    if-ge v0, p3, :cond_12

    .line 282
    packed-switch p2, :pswitch_data_14

    .line 281
    :goto_9
    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    .line 284
    :pswitch_c
    const-string v0, "ALTER TABLE contacts ADD volumemodifier float not null DEFAULT(\'0\')"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_9

    .line 291
    :cond_12
    return-void

    .line 282
    nop

    :pswitch_data_14
    .packed-switch 0x2
        :pswitch_c
    .end packed-switch
.end method
