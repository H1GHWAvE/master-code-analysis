.class public final Lcom/teamspeak/ts3client/data/ai;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/teamspeak/ts3client/data/ai;

.field private static b:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static declared-synchronized a()Lcom/teamspeak/ts3client/data/ai;
    .registers 3

    .prologue
    .line 14
    const-class v1, Lcom/teamspeak/ts3client/data/ai;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/teamspeak/ts3client/data/ai;->a:Lcom/teamspeak/ts3client/data/ai;

    if-nez v0, :cond_1c

    .line 15
    new-instance v0, Lcom/teamspeak/ts3client/data/ai;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/ai;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/data/ai;->a:Lcom/teamspeak/ts3client/data/ai;

    .line 16
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    const-string v2, "vibrator"

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/Ts3Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    sput-object v0, Lcom/teamspeak/ts3client/data/ai;->b:Landroid/os/Vibrator;

    .line 18
    :cond_1c
    sget-object v0, Lcom/teamspeak/ts3client/data/ai;->a:Lcom/teamspeak/ts3client/data/ai;
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_20

    monitor-exit v1

    return-object v0

    .line 14
    :catchall_20
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(I)V
    .registers 5

    .prologue
    .line 22
    sget-object v0, Lcom/teamspeak/ts3client/data/ai;->b:Landroid/os/Vibrator;

    if-eqz v0, :cond_a

    .line 23
    sget-object v0, Lcom/teamspeak/ts3client/data/ai;->b:Landroid/os/Vibrator;

    int-to-long v2, p0

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 24
    :cond_a
    return-void
.end method

.method public static a([J)V
    .registers 3

    .prologue
    .line 27
    sget-object v0, Lcom/teamspeak/ts3client/data/ai;->b:Landroid/os/Vibrator;

    if-eqz v0, :cond_a

    .line 28
    sget-object v0, Lcom/teamspeak/ts3client/data/ai;->b:Landroid/os/Vibrator;

    const/4 v1, -0x1

    invoke-virtual {v0, p0, v1}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 29
    :cond_a
    return-void
.end method
