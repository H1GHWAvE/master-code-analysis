.class public final Lcom/teamspeak/ts3client/data/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:I

.field public d:J

.field public e:I

.field public f:I

.field private g:J

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public constructor <init>(JJLjava/lang/String;IJIIIIII)V
    .registers 20

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide p1, p0, Lcom/teamspeak/ts3client/data/a/a;->g:J

    .line 24
    iput-wide p3, p0, Lcom/teamspeak/ts3client/data/a/a;->a:J

    .line 25
    iput-object p5, p0, Lcom/teamspeak/ts3client/data/a/a;->b:Ljava/lang/String;

    .line 26
    iput p6, p0, Lcom/teamspeak/ts3client/data/a/a;->c:I

    .line 27
    const-wide v2, 0xffffffffL

    and-long/2addr v2, p7

    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a/a;->d:J

    .line 28
    iput p9, p0, Lcom/teamspeak/ts3client/data/a/a;->h:I

    .line 29
    iput p10, p0, Lcom/teamspeak/ts3client/data/a/a;->i:I

    .line 30
    move/from16 v0, p11

    iput v0, p0, Lcom/teamspeak/ts3client/data/a/a;->j:I

    .line 31
    move/from16 v0, p12

    iput v0, p0, Lcom/teamspeak/ts3client/data/a/a;->k:I

    .line 32
    move/from16 v0, p13

    iput v0, p0, Lcom/teamspeak/ts3client/data/a/a;->e:I

    .line 33
    move/from16 v0, p14

    iput v0, p0, Lcom/teamspeak/ts3client/data/a/a;->f:I

    .line 34
    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/data/a/a;)I
    .registers 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 82
    .line 1073
    iget v0, p0, Lcom/teamspeak/ts3client/data/a/a;->i:I

    .line 84
    if-eqz v0, :cond_1d

    .line 2073
    iget v0, p0, Lcom/teamspeak/ts3client/data/a/a;->i:I

    .line 3073
    :goto_8
    iget v3, p1, Lcom/teamspeak/ts3client/data/a/a;->i:I

    .line 91
    if-eqz v3, :cond_21

    .line 4073
    iget v3, p1, Lcom/teamspeak/ts3client/data/a/a;->i:I

    .line 96
    :goto_e
    if-ne v0, v3, :cond_27

    .line 6037
    iget-wide v4, p0, Lcom/teamspeak/ts3client/data/a/a;->a:J

    .line 7037
    iget-wide v6, p1, Lcom/teamspeak/ts3client/data/a/a;->a:J

    .line 96
    cmp-long v0, v4, v6

    if-gez v0, :cond_25

    move v0, v1

    .line 97
    :goto_19
    if-eqz v0, :cond_2d

    .line 98
    const/4 v0, -0x1

    .line 100
    :goto_1c
    return v0

    .line 3037
    :cond_1d
    iget-wide v4, p0, Lcom/teamspeak/ts3client/data/a/a;->a:J

    .line 87
    long-to-int v0, v4

    goto :goto_8

    .line 5037
    :cond_21
    iget-wide v4, p1, Lcom/teamspeak/ts3client/data/a/a;->a:J

    .line 94
    long-to-int v3, v4

    goto :goto_e

    :cond_25
    move v0, v2

    .line 96
    goto :goto_19

    :cond_27
    if-ge v0, v3, :cond_2b

    move v0, v1

    goto :goto_19

    :cond_2b
    move v0, v2

    goto :goto_19

    :cond_2d
    move v0, v1

    .line 100
    goto :goto_1c
.end method

.method private b()J
    .registers 3

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/a/a;->a:J

    return-wide v0
.end method

.method private c()J
    .registers 3

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/a/a;->d:J

    return-wide v0
.end method

.method private d()I
    .registers 2

    .prologue
    .line 49
    iget v0, p0, Lcom/teamspeak/ts3client/data/a/a;->j:I

    return v0
.end method

.method private e()I
    .registers 2

    .prologue
    .line 53
    iget v0, p0, Lcom/teamspeak/ts3client/data/a/a;->e:I

    return v0
.end method

.method private f()I
    .registers 2

    .prologue
    .line 57
    iget v0, p0, Lcom/teamspeak/ts3client/data/a/a;->f:I

    return v0
.end method

.method private g()I
    .registers 2

    .prologue
    .line 61
    iget v0, p0, Lcom/teamspeak/ts3client/data/a/a;->k:I

    return v0
.end method

.method private h()I
    .registers 2

    .prologue
    .line 65
    iget v0, p0, Lcom/teamspeak/ts3client/data/a/a;->h:I

    return v0
.end method

.method private i()J
    .registers 3

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/a/a;->g:J

    return-wide v0
.end method

.method private j()I
    .registers 2

    .prologue
    .line 73
    iget v0, p0, Lcom/teamspeak/ts3client/data/a/a;->i:I

    return v0
.end method

.method private k()I
    .registers 2

    .prologue
    .line 77
    iget v0, p0, Lcom/teamspeak/ts3client/data/a/a;->c:I

    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/a/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3
    check-cast p1, Lcom/teamspeak/ts3client/data/a/a;

    .line 8073
    iget v0, p0, Lcom/teamspeak/ts3client/data/a/a;->i:I

    .line 7084
    if-eqz v0, :cond_1f

    .line 9073
    iget v0, p0, Lcom/teamspeak/ts3client/data/a/a;->i:I

    .line 10073
    :goto_a
    iget v3, p1, Lcom/teamspeak/ts3client/data/a/a;->i:I

    .line 7091
    if-eqz v3, :cond_23

    .line 11073
    iget v3, p1, Lcom/teamspeak/ts3client/data/a/a;->i:I

    .line 7096
    :goto_10
    if-ne v0, v3, :cond_29

    .line 13037
    iget-wide v4, p0, Lcom/teamspeak/ts3client/data/a/a;->a:J

    .line 14037
    iget-wide v6, p1, Lcom/teamspeak/ts3client/data/a/a;->a:J

    .line 7096
    cmp-long v0, v4, v6

    if-gez v0, :cond_27

    move v0, v1

    .line 7097
    :goto_1b
    if-eqz v0, :cond_2f

    .line 7098
    const/4 v0, -0x1

    :goto_1e
    return v0

    .line 10037
    :cond_1f
    iget-wide v4, p0, Lcom/teamspeak/ts3client/data/a/a;->a:J

    .line 7087
    long-to-int v0, v4

    goto :goto_a

    .line 12037
    :cond_23
    iget-wide v4, p1, Lcom/teamspeak/ts3client/data/a/a;->a:J

    .line 7094
    long-to-int v3, v4

    goto :goto_10

    :cond_27
    move v0, v2

    .line 7096
    goto :goto_1b

    :cond_29
    if-ge v0, v3, :cond_2d

    move v0, v1

    goto :goto_1b

    :cond_2d
    move v0, v2

    goto :goto_1b

    :cond_2f
    move v0, v1

    .line 3
    goto :goto_1e
.end method
