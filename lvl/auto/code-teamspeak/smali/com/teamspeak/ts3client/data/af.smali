.class public final Lcom/teamspeak/ts3client/data/af;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static f:Lcom/teamspeak/ts3client/data/af;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field c:Z

.field public d:Lcom/teamspeak/ts3client/data/z;

.field public e:Z


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/teamspeak/ts3client/data/af;->a:I

    .line 27
    return-void
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/data/af;I)I
    .registers 2

    .prologue
    .line 17
    iput p1, p0, Lcom/teamspeak/ts3client/data/af;->a:I

    return p1
.end method

.method public static declared-synchronized a()Lcom/teamspeak/ts3client/data/af;
    .registers 2

    .prologue
    .line 30
    const-class v1, Lcom/teamspeak/ts3client/data/af;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/teamspeak/ts3client/data/af;->f:Lcom/teamspeak/ts3client/data/af;

    if-nez v0, :cond_e

    .line 31
    new-instance v0, Lcom/teamspeak/ts3client/data/af;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/af;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/data/af;->f:Lcom/teamspeak/ts3client/data/af;

    .line 33
    :cond_e
    sget-object v0, Lcom/teamspeak/ts3client/data/af;->f:Lcom/teamspeak/ts3client/data/af;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 30
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcom/teamspeak/ts3client/data/z;)V
    .registers 2

    .prologue
    .line 61
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    .line 62
    return-void
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/data/af;)Z
    .registers 2

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/af;->c:Z

    return v0
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/data/af;Z)Z
    .registers 2

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/af;->c:Z

    return p1
.end method

.method private static synthetic b(Lcom/teamspeak/ts3client/data/af;)Lcom/teamspeak/ts3client/data/z;
    .registers 2

    .prologue
    .line 17
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    return-object v0
.end method

.method private b()V
    .registers 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 37
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/af;->e:Z

    if-eqz v0, :cond_7

    .line 50
    :goto_6
    return-void

    .line 39
    :cond_7
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 41
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 1093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 41
    const-string v2, "LicenseAgreement"

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_33

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 2093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 41
    const-string v2, "la_lastcheck"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/32 v4, 0x5265c00

    sub-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_4e

    .line 42
    :cond_33
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_android_getUpdaterURL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/af;->b:Ljava/lang/String;

    .line 43
    iput-boolean v7, p0, Lcom/teamspeak/ts3client/data/af;->e:Z

    .line 44
    new-instance v0, Lcom/teamspeak/ts3client/data/ah;

    invoke-direct {v0, p0, v6}, Lcom/teamspeak/ts3client/data/ah;-><init>(Lcom/teamspeak/ts3client/data/af;B)V

    new-array v1, v7, [Ljava/lang/String;

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/af;->b:Ljava/lang/String;

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/ah;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_6

    .line 46
    :cond_4e
    const-string v0, "UpdateServerData"

    const-string v1, "skipped downloading new update info"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/z;->o()V

    goto :goto_6
.end method

.method private c()I
    .registers 2

    .prologue
    .line 53
    iget v0, p0, Lcom/teamspeak/ts3client/data/af;->a:I

    return v0
.end method

.method private static synthetic c(Lcom/teamspeak/ts3client/data/af;)Z
    .registers 2

    .prologue
    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/af;->e:Z

    return v0
.end method

.method private d()Z
    .registers 2

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/af;->c:Z

    return v0
.end method
