.class public final Lcom/teamspeak/ts3client/tsdns/j;
.super Ljava/lang/Thread;
.source "SourceFile"


# static fields
.field public static a:Z


# instance fields
.field private b:Ljava/lang/String;

.field private c:I

.field private d:I

.field private e:I

.field private f:Lcom/teamspeak/ts3client/tsdns/k;

.field private g:Ljava/lang/String;

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 22
    const/4 v0, 0x1

    sput-boolean v0, Lcom/teamspeak/ts3client/tsdns/j;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIILcom/teamspeak/ts3client/tsdns/k;)V
    .registers 8

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 29
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/teamspeak/ts3client/tsdns/j;->h:I

    .line 33
    iput-object p2, p0, Lcom/teamspeak/ts3client/tsdns/j;->g:Ljava/lang/String;

    .line 34
    iput-object p1, p0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    .line 35
    iput p3, p0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    .line 36
    iput p4, p0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    .line 37
    iput p5, p0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    .line 38
    iput-object p6, p0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    .line 39
    invoke-interface {p6}, Lcom/teamspeak/ts3client/tsdns/k;->b()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 40
    const/16 v0, 0x1770

    iput v0, p0, Lcom/teamspeak/ts3client/tsdns/j;->h:I

    .line 41
    :cond_1d
    invoke-interface {p6, p0}, Lcom/teamspeak/ts3client/tsdns/k;->a(Ljava/lang/Thread;)V

    .line 42
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .registers 10

    .prologue
    .line 46
    :try_start_0
    const-string v0, "."

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 47
    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 48
    :cond_13
    new-instance v1, Ljava/net/Socket;

    invoke-direct {v1}, Ljava/net/Socket;-><init>()V

    .line 49
    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, p1, p3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iget v2, p0, Lcom/teamspeak/ts3client/tsdns/j;->h:I

    invoke-virtual {v1, v0, v2}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 50
    new-instance v2, Ljava/io/PrintStream;

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    .line 51
    invoke-virtual {v2, p2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 52
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-direct {v0, v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v3, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 53
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 55
    invoke-virtual {v2}, Ljava/io/PrintStream;->close()V

    .line 56
    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 57
    const-string v1, "404"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_55

    .line 58
    const-string v0, "ERROR2"
    :try_end_55
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_55} :catch_56

    .line 61
    :cond_55
    :goto_55
    return-object v0

    :catch_56
    move-exception v0

    const-string v0, "ERROR1"

    goto :goto_55
.end method


# virtual methods
.method public final run()V
    .registers 17

    .prologue
    .line 67
    move-object/from16 v0, p0

    iget v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    sget v3, Lcom/teamspeak/ts3client/tsdns/f;->h:I

    if-ne v2, v3, :cond_69

    .line 69
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-static {v2}, Lorg/xbill/DNS/Address;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v3

    .line 70
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v6, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " -> TS_TYPE_DIRECT: Result for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 71
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->a:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V
    :try_end_69
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_69} :catch_154

    .line 78
    :cond_69
    :goto_69
    move-object/from16 v0, p0

    iget v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    sget v3, Lcom/teamspeak/ts3client/tsdns/f;->g:I

    if-ne v2, v3, :cond_d5

    .line 79
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    const v4, 0xa0b8

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/teamspeak/ts3client/tsdns/j;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 80
    const-string v2, "ERROR1"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a2

    .line 81
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> ERROR TS_TYPE_TSDNS: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 82
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->c:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V

    .line 97
    :cond_d5
    :goto_d5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    sget v3, Lcom/teamspeak/ts3client/tsdns/f;->f:I

    if-ne v2, v3, :cond_3d0

    .line 98
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_tsdns._tcp."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    .line 100
    :try_start_f4
    new-instance v2, Lorg/xbill/DNS/Lookup;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    const/16 v4, 0x21

    invoke-direct {v2, v3, v4}, Lorg/xbill/DNS/Lookup;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2}, Lorg/xbill/DNS/Lookup;->run()[Lorg/xbill/DNS/Record;

    move-result-object v9

    .line 101
    if-eqz v9, :cond_108

    array-length v2, v9

    if-nez v2, :cond_276

    .line 102
    :cond_108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> ERROR TS_TYPE_SRVTSDNS: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 103
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->c:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V
    :try_end_153
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_f4 .. :try_end_153} :catch_384

    .line 241
    :cond_153
    :goto_153
    return-void

    .line 74
    :catch_154
    move-exception v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> ERROR TS_TYPE_DIRECT: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 75
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->c:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V

    goto/16 :goto_69

    .line 83
    :cond_1a2
    const-string v2, "ERROR2"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f7

    .line 84
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> ERROR 404 TS_TYPE_TSDNS: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 85
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->b:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V

    goto/16 :goto_d5

    .line 87
    :cond_1f7
    const-string v2, ":"

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_223

    .line 88
    const-string v2, ":"

    invoke-virtual {v3, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 89
    const/4 v3, 0x1

    aget-object v3, v2, v3

    if-eqz v3, :cond_220

    const/4 v3, 0x1

    aget-object v3, v2, v3

    const-string v4, "$PORT"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_220

    .line 90
    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    .line 91
    :cond_220
    const/4 v3, 0x0

    aget-object v3, v2, v3

    .line 93
    :cond_223
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v6, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " -> TS_TYPE_TSDNS: Result for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 94
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->a:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V

    goto/16 :goto_d5

    .line 106
    :cond_276
    :try_start_276
    array-length v2, v9

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2ed

    .line 107
    const/4 v2, 0x0

    aget-object v2, v9, v2

    check-cast v2, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getTarget()Lorg/xbill/DNS/Name;

    move-result-object v2

    invoke-virtual {v2}, Lorg/xbill/DNS/Name;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->g:Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v9, v2

    check-cast v2, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getPort()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v2}, Lcom/teamspeak/ts3client/tsdns/j;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 108
    const-string v2, "ERROR1"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_32f

    .line 109
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> ERROR TS_TYPE_SRVTSDNS: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 110
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->c:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v10, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V

    .line 125
    :cond_2ed
    :goto_2ed
    array-length v2, v9

    const/4 v3, 0x1

    if-le v2, v3, :cond_3d0

    .line 126
    const-string v7, ""

    .line 127
    const/4 v6, 0x0

    .line 129
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 130
    const/4 v2, 0x0

    aget-object v2, v9, v2

    check-cast v2, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getPriority()I

    move-result v5

    .line 131
    const/4 v4, 0x0

    .line 132
    array-length v11, v9

    const/4 v2, 0x0

    move v8, v2

    :goto_306
    if-ge v8, v11, :cond_53d

    aget-object v3, v9, v8

    .line 133
    move-object v0, v3

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getPriority()I

    move-result v2

    if-gt v2, v5, :cond_830

    .line 136
    move-object v0, v3

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getPriority()I

    move-result v2

    if-ne v2, v5, :cond_51d

    .line 137
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    check-cast v3, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v3}, Lorg/xbill/DNS/SRVRecord;->getWeight()I

    move-result v2

    add-int/2addr v2, v4

    move v3, v5

    .line 132
    :goto_329
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v5, v3

    move v4, v2

    goto :goto_306

    .line 111
    :cond_32f
    const-string v2, "ERROR2"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_49e

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> ERROR 404 TS_TYPE_SRVTSDNS: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 113
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->b:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v10, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V
    :try_end_382
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_276 .. :try_end_382} :catch_384

    goto/16 :goto_2ed

    .line 181
    :catch_384
    move-exception v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> ERROR TS_TYPE_SRVTSDNS: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 182
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->c:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V

    .line 185
    :cond_3d0
    :goto_3d0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    sget v3, Lcom/teamspeak/ts3client/tsdns/f;->e:I

    if-ne v2, v3, :cond_153

    .line 186
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_ts3._udp."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    .line 188
    :try_start_3ef
    new-instance v2, Lorg/xbill/DNS/Lookup;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    const/16 v4, 0x21

    invoke-direct {v2, v3, v4}, Lorg/xbill/DNS/Lookup;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2}, Lorg/xbill/DNS/Lookup;->run()[Lorg/xbill/DNS/Record;

    move-result-object v9

    .line 189
    if-eqz v9, :cond_403

    array-length v2, v9

    if-nez v2, :cond_6aa

    .line 190
    :cond_403
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> ERROR TS_TYPE_SRVTS3: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 191
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->c:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V
    :try_end_44e
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_3ef .. :try_end_44e} :catch_450

    goto/16 :goto_153

    .line 237
    :catch_450
    move-exception v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> ERROR TS_TYPE_SRVTS3: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 238
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->c:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V

    goto/16 :goto_153

    .line 115
    :cond_49e
    :try_start_49e
    const-string v2, ":"

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4ca

    .line 116
    const-string v2, ":"

    invoke-virtual {v3, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 117
    const/4 v3, 0x1

    aget-object v3, v2, v3

    if-eqz v3, :cond_4c7

    const/4 v3, 0x1

    aget-object v3, v2, v3

    const-string v4, "$PORT"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4c7

    .line 118
    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    .line 119
    :cond_4c7
    const/4 v3, 0x0

    aget-object v3, v2, v3

    .line 121
    :cond_4ca
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v6, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " -> TS_TYPE_SRVTSDNS: Result for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 122
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->a:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v10, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V

    goto/16 :goto_2ed

    .line 141
    :cond_51d
    move-object v0, v3

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getPriority()I

    move-result v2

    if-ge v2, v5, :cond_830

    .line 142
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 143
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    move-object v0, v3

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getWeight()I

    move-result v2

    .line 145
    check-cast v3, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v3}, Lorg/xbill/DNS/SRVRecord;->getPriority()I

    move-result v3

    goto/16 :goto_329

    .line 149
    :cond_53d
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v12

    .line 151
    const-wide/16 v2, 0x0

    .line 153
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v8, v2

    :cond_548
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_82c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/xbill/DNS/Record;

    .line 154
    move-object v0, v3

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getWeight()I

    move-result v2

    int-to-double v10, v2

    int-to-double v14, v4

    div-double/2addr v10, v14

    add-double/2addr v8, v10

    .line 155
    cmpg-double v2, v12, v8

    if-gtz v2, :cond_548

    .line 156
    move-object v0, v3

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getTarget()Lorg/xbill/DNS/Name;

    move-result-object v2

    invoke-virtual {v2}, Lorg/xbill/DNS/Name;->toString()Ljava/lang/String;

    move-result-object v4

    .line 157
    check-cast v3, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v3}, Lorg/xbill/DNS/SRVRecord;->getPort()I

    move-result v2

    move-object v3, v4

    .line 162
    :goto_577
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v2}, Lcom/teamspeak/ts3client/tsdns/j;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 163
    const-string v2, "ERROR1"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5d6

    .line 164
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> ERROR TS_TYPE_SRVTSDNS: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 165
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->c:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V

    goto/16 :goto_3d0

    .line 166
    :cond_5d6
    const-string v2, "ERROR2"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_62b

    .line 167
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> ERROR 404 TS_TYPE_SRVTSDNS: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 168
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->b:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V

    goto/16 :goto_3d0

    .line 170
    :cond_62b
    const-string v2, ":"

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_657

    .line 171
    const-string v2, ":"

    invoke-virtual {v3, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 172
    const/4 v3, 0x1

    aget-object v3, v2, v3

    if-eqz v3, :cond_654

    const/4 v3, 0x1

    aget-object v3, v2, v3

    const-string v4, "$PORT"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_654

    .line 173
    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    .line 174
    :cond_654
    const/4 v3, 0x0

    aget-object v3, v2, v3

    .line 176
    :cond_657
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v6, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " -> TS_TYPE_SRVTSDNS: Result for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 177
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/teamspeak/ts3client/tsdns/j;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->a:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V
    :try_end_6a8
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_49e .. :try_end_6a8} :catch_384

    goto/16 :goto_3d0

    .line 194
    :cond_6aa
    :try_start_6aa
    array-length v2, v9

    const/4 v3, 0x1

    if-ne v2, v3, :cond_731

    .line 195
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v3

    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " -> TS_TYPE_SRVTS3: Result for "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ": "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v2, 0x0

    aget-object v2, v9, v2

    check-cast v2, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getTarget()Lorg/xbill/DNS/Name;

    move-result-object v2

    invoke-virtual {v2}, Lorg/xbill/DNS/Name;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ":"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v2, 0x0

    aget-object v2, v9, v2

    check-cast v2, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getPort()I

    move-result v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    const/4 v3, 0x0

    aget-object v3, v9, v3

    check-cast v3, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v3}, Lorg/xbill/DNS/SRVRecord;->getTarget()Lorg/xbill/DNS/Name;

    move-result-object v3

    invoke-virtual {v3}, Lorg/xbill/DNS/Name;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v4, v9, v4

    check-cast v4, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v4}, Lorg/xbill/DNS/SRVRecord;->getPort()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->a:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v10, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V

    .line 198
    :cond_731
    array-length v2, v9

    const/4 v3, 0x1

    if-le v2, v3, :cond_153

    .line 199
    const-string v7, ""

    .line 200
    const/4 v6, 0x0

    .line 202
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 203
    const/4 v2, 0x0

    aget-object v2, v9, v2

    check-cast v2, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getPriority()I

    move-result v5

    .line 204
    const/4 v4, 0x0

    .line 205
    array-length v11, v9

    const/4 v2, 0x0

    move v8, v2

    :goto_74a
    if-ge v8, v11, :cond_792

    aget-object v3, v9, v8

    .line 206
    move-object v0, v3

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getPriority()I

    move-result v2

    if-gt v2, v5, :cond_828

    .line 209
    move-object v0, v3

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getPriority()I

    move-result v2

    if-ne v2, v5, :cond_773

    .line 210
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    check-cast v3, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v3}, Lorg/xbill/DNS/SRVRecord;->getWeight()I

    move-result v2

    add-int/2addr v2, v4

    move v3, v5

    .line 205
    :goto_76d
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v5, v3

    move v4, v2

    goto :goto_74a

    .line 214
    :cond_773
    move-object v0, v3

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getPriority()I

    move-result v2

    if-ge v2, v5, :cond_828

    .line 215
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 216
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    move-object v0, v3

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getWeight()I

    move-result v2

    .line 218
    check-cast v3, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v3}, Lorg/xbill/DNS/SRVRecord;->getPriority()I

    move-result v3

    goto :goto_76d

    .line 222
    :cond_792
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v12

    .line 223
    const-wide/16 v2, 0x0

    .line 225
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v8, v2

    :cond_79d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_825

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/xbill/DNS/Record;

    .line 226
    move-object v0, v3

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getWeight()I

    move-result v2

    int-to-double v10, v2

    int-to-double v14, v4

    div-double/2addr v10, v14

    add-double/2addr v8, v10

    .line 227
    cmpg-double v2, v12, v8

    if-gtz v2, :cond_79d

    .line 228
    move-object v0, v3

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/xbill/DNS/SRVRecord;->getTarget()Lorg/xbill/DNS/Name;

    move-result-object v2

    invoke-virtual {v2}, Lorg/xbill/DNS/Name;->toString()Ljava/lang/String;

    move-result-object v2

    .line 229
    check-cast v3, Lorg/xbill/DNS/SRVRecord;

    invoke-virtual {v3}, Lorg/xbill/DNS/SRVRecord;->getPort()I

    move-result v4

    move-object v3, v2

    .line 233
    :goto_7cc
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    invoke-interface {v2}, Lcom/teamspeak/ts3client/tsdns/k;->c()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v5, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " -> TS_TYPE_SRVTS3: Result for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 234
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/teamspeak/ts3client/tsdns/j;->f:Lcom/teamspeak/ts3client/tsdns/k;

    new-instance v2, Lcom/teamspeak/ts3client/tsdns/h;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/tsdns/j;->e:I

    sget v6, Lcom/teamspeak/ts3client/tsdns/f;->a:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/teamspeak/ts3client/tsdns/j;->d:I

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-interface {v9, v2}, Lcom/teamspeak/ts3client/tsdns/k;->a(Lcom/teamspeak/ts3client/tsdns/h;)V
    :try_end_823
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_6aa .. :try_end_823} :catch_450

    goto/16 :goto_153

    :cond_825
    move v4, v6

    move-object v3, v7

    goto :goto_7cc

    :cond_828
    move v2, v4

    move v3, v5

    goto/16 :goto_76d

    :cond_82c
    move v2, v6

    move-object v3, v7

    goto/16 :goto_577

    :cond_830
    move v2, v4

    move v3, v5

    goto/16 :goto_329
.end method
