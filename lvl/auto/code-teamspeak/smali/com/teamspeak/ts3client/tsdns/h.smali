.class public final Lcom/teamspeak/ts3client/tsdns/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field b:I

.field public c:I

.field public d:I

.field e:I

.field f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/tsdns/h;)V
    .registers 9

    .prologue
    .line 89
    .line 1035
    iget-object v1, p1, Lcom/teamspeak/ts3client/tsdns/h;->a:Ljava/lang/String;

    .line 2027
    iget v2, p1, Lcom/teamspeak/ts3client/tsdns/h;->c:I

    .line 3019
    iget v3, p1, Lcom/teamspeak/ts3client/tsdns/h;->b:I

    .line 3043
    iget v4, p1, Lcom/teamspeak/ts3client/tsdns/h;->d:I

    .line 4011
    iget v5, p1, Lcom/teamspeak/ts3client/tsdns/h;->e:I

    .line 89
    const/4 v6, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    .line 90
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIIZ)V
    .registers 9

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/teamspeak/ts3client/tsdns/h;->f:Ljava/lang/String;

    .line 64
    iput p5, p0, Lcom/teamspeak/ts3client/tsdns/h;->e:I

    .line 66
    :try_start_7
    const-string v0, "_ts3._udp."

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 67
    const-string v0, "_ts3._udp."

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 68
    :cond_17
    const-string v0, "_tsdns._tcp."

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 69
    const-string v0, "_tsdns._tcp."

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 70
    :cond_27
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 71
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 72
    :cond_3a
    if-eqz p6, :cond_4d

    .line 73
    invoke-static {p1}, Lorg/xbill/DNS/Address;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/tsdns/h;->a:Ljava/lang/String;
    :try_end_46
    .catch Ljava/net/UnknownHostException; {:try_start_7 .. :try_end_46} :catch_50

    .line 83
    :goto_46
    iput p2, p0, Lcom/teamspeak/ts3client/tsdns/h;->c:I

    .line 84
    iput p3, p0, Lcom/teamspeak/ts3client/tsdns/h;->b:I

    .line 85
    iput p4, p0, Lcom/teamspeak/ts3client/tsdns/h;->d:I

    .line 86
    return-void

    .line 76
    :cond_4d
    :try_start_4d
    iput-object p1, p0, Lcom/teamspeak/ts3client/tsdns/h;->a:Ljava/lang/String;
    :try_end_4f
    .catch Ljava/net/UnknownHostException; {:try_start_4d .. :try_end_4f} :catch_50

    goto :goto_46

    .line 78
    :catch_50
    move-exception v0

    iput-object p1, p0, Lcom/teamspeak/ts3client/tsdns/h;->a:Ljava/lang/String;

    .line 79
    sget p4, Lcom/teamspeak/ts3client/tsdns/f;->c:I

    goto :goto_46
.end method

.method private a()I
    .registers 2

    .prologue
    .line 11
    iget v0, p0, Lcom/teamspeak/ts3client/tsdns/h;->e:I

    return v0
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 15
    iput p1, p0, Lcom/teamspeak/ts3client/tsdns/h;->e:I

    .line 16
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 39
    iput-object p1, p0, Lcom/teamspeak/ts3client/tsdns/h;->a:Ljava/lang/String;

    .line 40
    return-void
.end method

.method private b()I
    .registers 2

    .prologue
    .line 19
    iget v0, p0, Lcom/teamspeak/ts3client/tsdns/h;->b:I

    return v0
.end method

.method private b(I)V
    .registers 2

    .prologue
    .line 23
    iput p1, p0, Lcom/teamspeak/ts3client/tsdns/h;->b:I

    .line 24
    return-void
.end method

.method private c()I
    .registers 2

    .prologue
    .line 27
    iget v0, p0, Lcom/teamspeak/ts3client/tsdns/h;->c:I

    return v0
.end method

.method private c(I)V
    .registers 2

    .prologue
    .line 31
    iput p1, p0, Lcom/teamspeak/ts3client/tsdns/h;->c:I

    .line 32
    return-void
.end method

.method private d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method private d(I)V
    .registers 2

    .prologue
    .line 47
    iput p1, p0, Lcom/teamspeak/ts3client/tsdns/h;->d:I

    .line 48
    return-void
.end method

.method private e()I
    .registers 2

    .prologue
    .line 43
    iget v0, p0, Lcom/teamspeak/ts3client/tsdns/h;->d:I

    return v0
.end method

.method private f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/h;->f:Ljava/lang/String;

    return-object v0
.end method
