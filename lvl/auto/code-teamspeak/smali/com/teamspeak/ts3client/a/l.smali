.class final Lcom/teamspeak/ts3client/a/l;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/a/k;

.field private b:[S


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/a/k;)V
    .registers 2

    .prologue
    .line 197
    iput-object p1, p0, Lcom/teamspeak/ts3client/a/l;->a:Lcom/teamspeak/ts3client/a/k;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 203
    const/16 v0, -0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    move v0, v1

    .line 206
    :goto_8
    if-eqz v0, :cond_32

    .line 209
    :try_start_a
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/l;->a:Lcom/teamspeak/ts3client/a/k;

    .line 1016
    iget-object v0, v0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    .line 209
    if-nez v0, :cond_11

    .line 246
    :cond_10
    return-void

    .line 212
    :cond_11
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/l;->a:Lcom/teamspeak/ts3client/a/k;

    .line 2016
    iget-object v0, v0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    .line 212
    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V
    :try_end_18
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_18} :catch_1a

    move v0, v2

    .line 221
    goto :goto_8

    :catch_1a
    move-exception v0

    .line 215
    :try_start_1b
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/l;->a:Lcom/teamspeak/ts3client/a/k;

    .line 3016
    iget-object v0, v0, Lcom/teamspeak/ts3client/a/k;->h:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 215
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v4, "Audio Play IllegalStateException, retry"

    invoke-virtual {v0, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 217
    const-wide/16 v4, 0x64

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2d
    .catch Ljava/lang/InterruptedException; {:try_start_1b .. :try_end_2d} :catch_2f

    move v0, v1

    .line 220
    goto :goto_8

    .line 219
    :catch_2f
    move-exception v0

    move v0, v2

    .line 221
    goto :goto_8

    :cond_32
    move v0, v1

    .line 224
    :cond_33
    :goto_33
    iget-object v3, p0, Lcom/teamspeak/ts3client/a/l;->a:Lcom/teamspeak/ts3client/a/k;

    .line 4016
    iget-boolean v3, v3, Lcom/teamspeak/ts3client/a/k;->e:Z

    .line 224
    if-eqz v3, :cond_10

    .line 225
    iget-object v3, p0, Lcom/teamspeak/ts3client/a/l;->a:Lcom/teamspeak/ts3client/a/k;

    .line 5016
    iget-object v3, v3, Lcom/teamspeak/ts3client/a/k;->d:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 225
    invoke-virtual {v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->requestAudioData()[S

    move-result-object v3

    iput-object v3, p0, Lcom/teamspeak/ts3client/a/l;->b:[S

    .line 226
    iget-object v3, p0, Lcom/teamspeak/ts3client/a/l;->b:[S

    if-eqz v3, :cond_6e

    .line 227
    if-nez v0, :cond_51

    .line 228
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/l;->a:Lcom/teamspeak/ts3client/a/k;

    .line 6016
    iget-object v0, v0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    .line 228
    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    move v0, v1

    .line 231
    :cond_51
    iget-object v3, p0, Lcom/teamspeak/ts3client/a/l;->b:[S

    if-eqz v3, :cond_33

    .line 232
    iget-object v3, p0, Lcom/teamspeak/ts3client/a/l;->a:Lcom/teamspeak/ts3client/a/k;

    .line 7016
    iget-object v3, v3, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    .line 232
    invoke-virtual {v3}, Landroid/media/AudioTrack;->flush()V

    .line 233
    iget-object v3, p0, Lcom/teamspeak/ts3client/a/l;->a:Lcom/teamspeak/ts3client/a/k;

    .line 8016
    iget-object v3, v3, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    .line 233
    iget-object v4, p0, Lcom/teamspeak/ts3client/a/l;->b:[S

    iget-object v5, p0, Lcom/teamspeak/ts3client/a/l;->a:Lcom/teamspeak/ts3client/a/k;

    .line 9016
    iget v5, v5, Lcom/teamspeak/ts3client/a/k;->j:I

    .line 233
    div-int/lit8 v5, v5, 0x64

    mul-int/lit8 v5, v5, 0x2

    invoke-virtual {v3, v4, v2, v5}, Landroid/media/AudioTrack;->write([SII)I

    goto :goto_33

    .line 236
    :cond_6e
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/l;->a:Lcom/teamspeak/ts3client/a/k;

    .line 10016
    iget-object v0, v0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    .line 236
    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V

    .line 237
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/l;->a:Lcom/teamspeak/ts3client/a/k;

    .line 11016
    iget-object v0, v0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    .line 237
    invoke-virtual {v0}, Landroid/media/AudioTrack;->flush()V

    .line 240
    const-wide/16 v4, 0x64

    :try_start_7e
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_81
    .catch Ljava/lang/InterruptedException; {:try_start_7e .. :try_end_81} :catch_83

    move v0, v2

    .line 242
    goto :goto_33

    :catch_83
    move-exception v0

    move v0, v2

    goto :goto_33
.end method
