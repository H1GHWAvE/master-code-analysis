.class public final Lcom/teamspeak/ts3client/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static m:Lcom/teamspeak/ts3client/a/k;


# instance fields
.field a:Z

.field b:[S

.field c:Landroid/media/AudioRecord;

.field public d:Lcom/teamspeak/ts3client/jni/Ts3Jni;

.field e:Z

.field f:Landroid/media/AudioTrack;

.field public g:Landroid/media/AudioManager;

.field h:Lcom/teamspeak/ts3client/Ts3Application;

.field i:I

.field j:I

.field public k:Z

.field public l:Z

.field private n:Ljava/lang/Thread;

.field private o:I

.field private p:I

.field private q:I

.field private r:Ljava/lang/Thread;

.field private s:I

.field private t:I

.field private u:I

.field private v:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->a:Z

    .line 28
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->e:Z

    .line 42
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/k;->h:Lcom/teamspeak/ts3client/Ts3Application;

    .line 43
    return-void
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/a/k;)Landroid/media/AudioTrack;
    .registers 2

    .prologue
    .line 16
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    return-object v0
.end method

.method public static declared-synchronized a()Lcom/teamspeak/ts3client/a/k;
    .registers 2

    .prologue
    .line 46
    const-class v1, Lcom/teamspeak/ts3client/a/k;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/teamspeak/ts3client/a/k;->m:Lcom/teamspeak/ts3client/a/k;

    if-nez v0, :cond_e

    .line 47
    new-instance v0, Lcom/teamspeak/ts3client/a/k;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/a/k;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/a/k;->m:Lcom/teamspeak/ts3client/a/k;

    .line 49
    :cond_e
    sget-object v0, Lcom/teamspeak/ts3client/a/k;->m:Lcom/teamspeak/ts3client/a/k;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 46
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;I)Ljava/util/Vector;
    .registers 16

    .prologue
    const/4 v13, 0x5

    const/4 v12, 0x1

    const/4 v9, 0x0

    .line 81
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    .line 82
    invoke-static {p1}, Landroid/media/AudioTrack;->getNativeOutputSampleRate(I)I

    move-result v0

    mul-int/lit8 v10, v0, 0x2

    .line 83
    new-array v11, v13, [I

    fill-array-data v11, :array_6a

    .line 84
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move v8, v9

    .line 85
    :goto_17
    if-ge v8, v13, :cond_59

    aget v2, v11, v8

    .line 87
    if-ge v2, v10, :cond_3f

    .line 89
    const/4 v0, 0x4

    const/4 v1, 0x2

    :try_start_1f
    invoke-static {v2, v0, v1}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v5

    .line 90
    const/4 v0, -0x2

    if-eq v5, v0, :cond_3f

    .line 92
    new-instance v0, Landroid/media/AudioTrack;

    const/4 v3, 0x4

    const/4 v4, 0x2

    const/4 v6, 0x1

    move v1, p1

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    .line 93
    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v1

    if-ne v1, v12, :cond_3c

    .line 94
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 96
    :cond_3c
    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_3f} :catch_43

    .line 85
    :cond_3f
    :goto_3f
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_17

    .line 99
    :catch_43
    move-exception v0

    const-string v0, "Audio"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "notSupportedplay: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3f

    .line 102
    :cond_59
    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v0

    if-gtz v0, :cond_68

    .line 103
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-object v0, v7

    .line 106
    :goto_67
    return-object v0

    :cond_68
    move-object v0, v7

    goto :goto_67

    .line 83
    :array_6a
    .array-data 4
        0xac44
        0x7d00
        0x5622
        0x3e80
        0x1f40
    .end array-data
.end method

.method public static a(Landroid/content/Context;II)Ljava/util/Vector;
    .registers 15

    .prologue
    const/4 v11, 0x5

    const/4 v8, 0x0

    .line 53
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    .line 54
    invoke-static {p1}, Landroid/media/AudioTrack;->getNativeOutputSampleRate(I)I

    move-result v0

    mul-int/lit8 v9, v0, 0x2

    .line 55
    new-array v10, v11, [I

    fill-array-data v10, :array_6c

    .line 56
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move v7, v8

    .line 57
    :goto_16
    if-ge v7, v11, :cond_5a

    aget v2, v10, v7

    .line 59
    if-ge v2, v9, :cond_40

    .line 61
    const/16 v0, 0x10

    const/4 v1, 0x2

    :try_start_1f
    invoke-static {v2, v0, v1}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v5

    .line 62
    const/4 v0, -0x2

    if-eq v5, v0, :cond_40

    .line 63
    new-instance v0, Landroid/media/AudioRecord;

    const/16 v3, 0x10

    const/4 v4, 0x2

    move v1, p2

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    .line 64
    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3d

    .line 65
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_3d
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V
    :try_end_40
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_40} :catch_44

    .line 57
    :cond_40
    :goto_40
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_16

    .line 70
    :catch_44
    move-exception v0

    const-string v0, "Audio"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "notSupportedrec: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_40

    .line 73
    :cond_5a
    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v0

    if-gtz v0, :cond_69

    .line 74
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-object v0, v6

    .line 77
    :goto_68
    return-object v0

    :cond_69
    move-object v0, v6

    goto :goto_68

    .line 55
    nop

    :array_6c
    .array-data 4
        0xac44
        0x7d00
        0x5622
        0x3e80
        0x1f40
    .end array-data
.end method

.method private a(Lcom/teamspeak/ts3client/jni/Ts3Jni;)V
    .registers 2

    .prologue
    .line 110
    iput-object p1, p0, Lcom/teamspeak/ts3client/a/k;->d:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 111
    return-void
.end method

.method private static synthetic b(Lcom/teamspeak/ts3client/a/k;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 16
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->h:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method private static synthetic c(Lcom/teamspeak/ts3client/a/k;)Z
    .registers 2

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->e:Z

    return v0
.end method

.method private static synthetic d(Lcom/teamspeak/ts3client/a/k;)Lcom/teamspeak/ts3client/jni/Ts3Jni;
    .registers 2

    .prologue
    .line 16
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->d:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    return-object v0
.end method

.method private static synthetic e(Lcom/teamspeak/ts3client/a/k;)I
    .registers 2

    .prologue
    .line 16
    iget v0, p0, Lcom/teamspeak/ts3client/a/k;->j:I

    return v0
.end method

.method private static synthetic f(Lcom/teamspeak/ts3client/a/k;)Landroid/media/AudioRecord;
    .registers 2

    .prologue
    .line 16
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    return-object v0
.end method

.method private static synthetic g(Lcom/teamspeak/ts3client/a/k;)Z
    .registers 2

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->a:Z

    return v0
.end method

.method private h()V
    .registers 3

    .prologue
    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->k:Z

    .line 166
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    iget-boolean v1, p0, Lcom/teamspeak/ts3client/a/k;->k:Z

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 167
    return-void
.end method

.method private static synthetic h(Lcom/teamspeak/ts3client/a/k;)[S
    .registers 2

    .prologue
    .line 16
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->b:[S

    return-object v0
.end method

.method private static synthetic i(Lcom/teamspeak/ts3client/a/k;)I
    .registers 2

    .prologue
    .line 16
    iget v0, p0, Lcom/teamspeak/ts3client/a/k;->i:I

    return v0
.end method

.method private i()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 182
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->l:Z

    if-eqz v0, :cond_6

    .line 191
    :goto_5
    return-void

    .line 184
    :cond_6
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    if-eqz v0, :cond_1e

    .line 185
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 186
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 187
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 188
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 190
    :cond_1e
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    goto :goto_5
.end method

.method private j()V
    .registers 5

    .prologue
    .line 338
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    iget v1, p0, Lcom/teamspeak/ts3client/a/k;->t:I

    const/16 v2, 0xf

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 339
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)V
    .registers 4

    .prologue
    .line 160
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    if-eqz v0, :cond_d

    .line 161
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 162
    :cond_d
    return-void
.end method

.method public final a(Z)V
    .registers 6

    .prologue
    .line 149
    if-eqz p1, :cond_c

    .line 150
    :try_start_2
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    iget v1, p0, Lcom/teamspeak/ts3client/a/k;->t:I

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 157
    :goto_b
    return-void

    .line 152
    :cond_c
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    iget v1, p0, Lcom/teamspeak/ts3client/a/k;->t:I

    const/4 v2, -0x1

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->adjustStreamVolume(III)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_15} :catch_16

    goto :goto_b

    .line 157
    :catch_16
    move-exception v0

    goto :goto_b
.end method

.method public final a(IIII)Z
    .registers 15

    .prologue
    const/4 v9, 0x4

    const/4 v7, 0x0

    const/4 v8, 0x2

    const/4 v6, 0x1

    .line 114
    iput-boolean v6, p0, Lcom/teamspeak/ts3client/a/k;->l:Z

    .line 115
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    if-eqz v0, :cond_12

    .line 116
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/k;->f()V

    .line 117
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 119
    :cond_12
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    if-eqz v0, :cond_1e

    .line 120
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/k;->e()V

    .line 121
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    .line 123
    :cond_1e
    iput p1, p0, Lcom/teamspeak/ts3client/a/k;->i:I

    .line 124
    iput p2, p0, Lcom/teamspeak/ts3client/a/k;->j:I

    .line 125
    iput p3, p0, Lcom/teamspeak/ts3client/a/k;->t:I

    .line 126
    iput p4, p0, Lcom/teamspeak/ts3client/a/k;->u:I

    .line 127
    iput v8, p0, Lcom/teamspeak/ts3client/a/k;->p:I

    .line 128
    const/16 v0, 0x10

    iput v0, p0, Lcom/teamspeak/ts3client/a/k;->o:I

    .line 129
    iget v0, p0, Lcom/teamspeak/ts3client/a/k;->o:I

    iget v1, p0, Lcom/teamspeak/ts3client/a/k;->p:I

    invoke-static {p1, v0, v1}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/a/k;->q:I

    .line 130
    iget v0, p0, Lcom/teamspeak/ts3client/a/k;->q:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_3d

    move v6, v7

    .line 144
    :cond_3c
    :goto_3c
    return v6

    .line 132
    :cond_3d
    new-instance v0, Landroid/media/AudioRecord;

    iget v3, p0, Lcom/teamspeak/ts3client/a/k;->o:I

    iget v4, p0, Lcom/teamspeak/ts3client/a/k;->p:I

    iget v5, p0, Lcom/teamspeak/ts3client/a/k;->q:I

    move v1, p4

    move v2, p1

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    .line 133
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->h:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 133
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "recSa:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " PlaySa:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " RecS:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " PlayS:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 135
    div-int/lit8 v0, p1, 0x64

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/k;->b:[S

    .line 136
    iget v0, p0, Lcom/teamspeak/ts3client/a/k;->p:I

    invoke-static {p2, v9, v0}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/a/k;->s:I

    .line 137
    new-instance v0, Landroid/media/AudioTrack;

    iget v5, p0, Lcom/teamspeak/ts3client/a/k;->s:I

    move v1, p3

    move v2, p2

    move v3, v9

    move v4, v8

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    .line 138
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->h:Lcom/teamspeak/ts3client/Ts3Application;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/Ts3Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    .line 140
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->h:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 140
    const-string v1, "audio_handfree"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/a/k;->a(Ljava/lang/Boolean;)V

    .line 141
    const/4 v0, 0x3

    if-ne p3, v0, :cond_3c

    .line 142
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, v8}, Landroid/media/AudioManager;->setMode(I)V

    goto/16 :goto_3c
.end method

.method public final b()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 171
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->h:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 171
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Release Audio"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    if-eqz v0, :cond_15

    .line 173
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 174
    :cond_15
    iput-object v3, p0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    .line 175
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    if-eqz v0, :cond_20

    .line 176
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    .line 177
    :cond_20
    iput-object v3, p0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    .line 178
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->l:Z

    .line 179
    return-void
.end method

.method public final b(Z)V
    .registers 4

    .prologue
    .line 308
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/a/k;->v:Z

    .line 309
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    if-nez v0, :cond_7

    .line 329
    :goto_6
    return-void

    .line 311
    :cond_7
    if-eqz p1, :cond_21

    .line 312
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoAvailableOffCall()Z

    .line 313
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    .line 315
    :try_start_14
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->startBluetoothSco()V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_19} :catch_2f

    .line 327
    :goto_19
    invoke-static {}, Lcom/teamspeak/ts3client/a/j;->a()Lcom/teamspeak/ts3client/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/j;->b()V

    goto :goto_6

    .line 321
    :cond_21
    :try_start_21
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 322
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_2c} :catch_2d

    goto :goto_19

    :catch_2d
    move-exception v0

    goto :goto_19

    .line 318
    :catch_2f
    move-exception v0

    goto :goto_19
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->e:Z

    if-eqz v0, :cond_5

    .line 249
    :goto_4
    return-void

    .line 196
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->e:Z

    .line 197
    new-instance v0, Lcom/teamspeak/ts3client/a/l;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/a/l;-><init>(Lcom/teamspeak/ts3client/a/k;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/k;->r:Ljava/lang/Thread;

    .line 248
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->r:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_4
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->a:Z

    if-eqz v0, :cond_5

    .line 274
    :goto_4
    return-void

    .line 254
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->a:Z

    .line 255
    new-instance v0, Lcom/teamspeak/ts3client/a/m;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/a/m;-><init>(Lcom/teamspeak/ts3client/a/k;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/k;->n:Ljava/lang/Thread;

    .line 273
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->n:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_4
.end method

.method public final e()V
    .registers 4

    .prologue
    .line 277
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->e:Z

    .line 278
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->r:Ljava/lang/Thread;

    if-eqz v0, :cond_c

    .line 279
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->r:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 280
    :cond_c
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    if-nez v0, :cond_11

    .line 288
    :goto_10
    return-void

    .line 283
    :cond_11
    :try_start_11
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V

    .line 284
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->f:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->flush()V
    :try_end_1b
    .catch Ljava/lang/IllegalStateException; {:try_start_11 .. :try_end_1b} :catch_1c

    goto :goto_10

    .line 285
    :catch_1c
    move-exception v0

    .line 286
    iget-object v1, p0, Lcom/teamspeak/ts3client/a/k;->h:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 286
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_10
.end method

.method public final f()V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 291
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->a:Z

    .line 292
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->n:Ljava/lang/Thread;

    if-eqz v0, :cond_c

    .line 293
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->n:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 294
    :cond_c
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    if-eqz v0, :cond_1b

    .line 296
    :try_start_10
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 297
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioRecord;->setPositionNotificationPeriod(I)I
    :try_end_1b
    .catch Ljava/lang/IllegalStateException; {:try_start_10 .. :try_end_1b} :catch_1c

    .line 302
    :cond_1b
    :goto_1b
    return-void

    .line 298
    :catch_1c
    move-exception v0

    .line 299
    iget-object v1, p0, Lcom/teamspeak/ts3client/a/k;->h:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 299
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_1b
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 332
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/k;->k:Z

    .line 333
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    if-eqz v0, :cond_e

    .line 334
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/k;->g:Landroid/media/AudioManager;

    iget-boolean v1, p0, Lcom/teamspeak/ts3client/a/k;->k:Z

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 335
    :cond_e
    return-void
.end method
