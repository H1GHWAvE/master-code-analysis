.class public final Lcom/teamspeak/ts3client/a/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;
.implements Lcom/teamspeak/ts3client/a/n;


# instance fields
.field a:Landroid/speech/tts/TextToSpeech;

.field private b:Ljava/util/HashMap;

.field private c:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/r;->c:Ljava/util/HashMap;

    .line 86
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/r;)Ljava/util/HashMap;
    .registers 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/r;->c:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 61
    new-instance v0, Lcom/teamspeak/ts3client/a/s;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/a/s;-><init>(Lcom/teamspeak/ts3client/a/r;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/a/s;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 62
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .registers 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 67
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    invoke-direct {v0, p1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/r;->a:Landroid/speech/tts/TextToSpeech;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/r;->b:Ljava/util/HashMap;

    .line 69
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/r;->b:Ljava/util/HashMap;

    const-string v1, "streamType"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/r;->b:Ljava/util/HashMap;

    const-string v1, "volume"

    const-string v2, "0.4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_34

    .line 72
    new-instance v0, Lcom/teamspeak/ts3client/a/s;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/a/s;-><init>(Lcom/teamspeak/ts3client/a/r;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/s;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 76
    :goto_33
    return-void

    .line 74
    :cond_34
    new-instance v0, Lcom/teamspeak/ts3client/a/s;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/a/s;-><init>(Lcom/teamspeak/ts3client/a/r;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/a/s;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_33
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
    .registers 10

    .prologue
    .line 38
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/r;->c:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/h;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 39
    if-nez v0, :cond_f

    .line 51
    :goto_e
    return-void

    .line 42
    :cond_f
    if-eqz p2, :cond_29

    .line 43
    :try_start_11
    const-string v1, "${clientname}"

    .line 1023
    iget-object v2, p2, Lcom/teamspeak/ts3client/a/o;->a:Ljava/lang/String;

    .line 43
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_18} :catch_32

    move-result-object v1

    .line 44
    :try_start_19
    const-string v0, "${channelname}"

    .line 2019
    iget-object v2, p2, Lcom/teamspeak/ts3client/a/o;->c:Ljava/lang/String;

    .line 44
    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 45
    const-string v0, "${servername}"

    .line 2031
    iget-object v2, p2, Lcom/teamspeak/ts3client/a/o;->d:Ljava/lang/String;

    .line 45
    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_28} :catch_52

    move-result-object v0

    .line 50
    :cond_29
    :goto_29
    iget-object v1, p0, Lcom/teamspeak/ts3client/a/r;->a:Landroid/speech/tts/TextToSpeech;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/a/r;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0, v2, v3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    goto :goto_e

    .line 47
    :catch_32
    move-exception v1

    .line 48
    :goto_33
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 2085
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 48
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SOUND ERROR: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/h;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_29

    .line 47
    :catch_52
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto :goto_33
.end method

.method public final a(Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 55
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/r;->a:Landroid/speech/tts/TextToSpeech;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/teamspeak/ts3client/a/r;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v1, v2}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 57
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/r;->a:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 81
    return-void
.end method

.method public final onInit(I)V
    .registers 4

    .prologue
    .line 33
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/r;->a:Landroid/speech/tts/TextToSpeech;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 34
    return-void
.end method
