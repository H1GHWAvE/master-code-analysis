.class public final Lcom/teamspeak/ts3client/c/c;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field a:Ljava/util/ArrayList;

.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:Landroid/support/v4/app/bi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/bi;)V
    .registers 4

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/teamspeak/ts3client/c/c;->b:Landroid/content/Context;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/c/c;->a:Ljava/util/ArrayList;

    .line 36
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/c/c;->c:Landroid/view/LayoutInflater;

    .line 37
    iput-object p2, p0, Lcom/teamspeak/ts3client/c/c;->d:Landroid/support/v4/app/bi;

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/c/c;)Landroid/content/Context;
    .registers 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/c;->b:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lcom/teamspeak/ts3client/c/a;)V
    .registers 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_d
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/c/c;)Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/c;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(Lcom/teamspeak/ts3client/c/a;)V
    .registers 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 138
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 139
    :cond_d
    return-void
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/c/c;)Landroid/support/v4/app/bi;
    .registers 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/c;->d:Landroid/support/v4/app/bi;

    return-object v0
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 52
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    .prologue
    .line 57
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12

    .prologue
    const v7, 0x7f0200a8

    const/high16 v6, 0x41a00000    # 20.0f

    .line 62
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/c;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030033

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 64
    const v0, 0x7f0c0178

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 65
    const v1, 0x7f0c0175

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 66
    const v2, 0x7f0c0177

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 67
    const v3, 0x7f020060

    invoke-static {v3, v6, v6}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 68
    iget-object v3, p0, Lcom/teamspeak/ts3client/c/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/teamspeak/ts3client/c/a;

    .line 1054
    iget v3, v3, Lcom/teamspeak/ts3client/c/a;->e:I

    .line 68
    packed-switch v3, :pswitch_data_9c

    .line 83
    :goto_41
    const v1, 0x7f0c0176

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 84
    const v3, 0x7f020062

    invoke-static {v3, v6, v6}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 85
    new-instance v3, Lcom/teamspeak/ts3client/c/d;

    invoke-direct {v3, p0, p1}, Lcom/teamspeak/ts3client/c/d;-><init>(Lcom/teamspeak/ts3client/c/c;I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    new-instance v2, Lcom/teamspeak/ts3client/c/g;

    invoke-direct {v2, p0, p1}, Lcom/teamspeak/ts3client/c/g;-><init>(Lcom/teamspeak/ts3client/c/c;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v1, p0, Lcom/teamspeak/ts3client/c/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/c/a;

    .line 2030
    iget-object v1, v1, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 131
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    return-object v4

    .line 70
    :pswitch_72
    invoke-static {v7, v6, v6}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_41

    .line 73
    :pswitch_7a
    const v3, -0xff0100

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v3, v5}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 74
    invoke-static {v7, v6, v6}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_41

    .line 77
    :pswitch_8a
    const/high16 v3, -0x10000

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v3, v5}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 78
    const v1, 0x7f0200a7

    invoke-static {v1, v6, v6}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_41

    .line 68
    :pswitch_data_9c
    .packed-switch 0x0
        :pswitch_72
        :pswitch_8a
        :pswitch_7a
    .end packed-switch
.end method
