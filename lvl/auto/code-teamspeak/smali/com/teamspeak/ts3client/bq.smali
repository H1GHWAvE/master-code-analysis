.class public final Lcom/teamspeak/ts3client/bq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f030000

.field public static final abc_action_bar_up_container:I = 0x7f030001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f030002

.field public static final abc_action_menu_item_layout:I = 0x7f030003

.field public static final abc_action_menu_layout:I = 0x7f030004

.field public static final abc_action_mode_bar:I = 0x7f030005

.field public static final abc_action_mode_close_item_material:I = 0x7f030006

.field public static final abc_activity_chooser_view:I = 0x7f030007

.field public static final abc_activity_chooser_view_list_item:I = 0x7f030008

.field public static final abc_alert_dialog_material:I = 0x7f030009

.field public static final abc_dialog_title_material:I = 0x7f03000a

.field public static final abc_expanded_menu_layout:I = 0x7f03000b

.field public static final abc_list_menu_item_checkbox:I = 0x7f03000c

.field public static final abc_list_menu_item_icon:I = 0x7f03000d

.field public static final abc_list_menu_item_layout:I = 0x7f03000e

.field public static final abc_list_menu_item_radio:I = 0x7f03000f

.field public static final abc_popup_menu_item_layout:I = 0x7f030010

.field public static final abc_screen_content_include:I = 0x7f030011

.field public static final abc_screen_simple:I = 0x7f030012

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f030013

.field public static final abc_screen_toolbar:I = 0x7f030014

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f030015

.field public static final abc_search_view:I = 0x7f030016

.field public static final abc_select_dialog_material:I = 0x7f030017

.field public static final audiosettings:I = 0x7f030018

.field public static final backgroundcenter:I = 0x7f030019

.field public static final banlist_create:I = 0x7f03001a

.field public static final banlist_entry:I = 0x7f03001b

.field public static final banlist_list:I = 0x7f03001c

.field public static final bookmark_entry:I = 0x7f03001d

.field public static final bookmark_list:I = 0x7f03001e

.field public static final bookmark_list_entry:I = 0x7f03001f

.field public static final channel_info_codec_seekbar:I = 0x7f030020

.field public static final channel_info_edit:I = 0x7f030021

.field public static final channel_info_edit_description:I = 0x7f030022

.field public static final channel_info_edit_spinner:I = 0x7f030023

.field public static final channel_info_fragment:I = 0x7f030024

.field public static final chat_list:I = 0x7f030025

.field public static final chat_list_entry:I = 0x7f030026

.field public static final chat_window:I = 0x7f030027

.field public static final chat_window_entry:I = 0x7f030028

.field public static final client_connection_info:I = 0x7f030029

.field public static final client_group:I = 0x7f03002a

.field public static final client_group_row:I = 0x7f03002b

.field public static final client_info_action:I = 0x7f03002c

.field public static final client_info_action_ban:I = 0x7f03002d

.field public static final client_info_fragment:I = 0x7f03002e

.field public static final client_info_row:I = 0x7f03002f

.field public static final connection_listing:I = 0x7f030030

.field public static final contact_client_setting:I = 0x7f030031

.field public static final contact_list:I = 0x7f030032

.field public static final contact_list_entry:I = 0x7f030033

.field public static final design_layout_snackbar:I = 0x7f030034

.field public static final design_layout_snackbar_include:I = 0x7f030035

.field public static final design_layout_tab_icon:I = 0x7f030036

.field public static final design_layout_tab_text:I = 0x7f030037

.field public static final design_navigation_item:I = 0x7f030038

.field public static final design_navigation_item_header:I = 0x7f030039

.field public static final design_navigation_item_separator:I = 0x7f03003a

.field public static final design_navigation_item_subheader:I = 0x7f03003b

.field public static final design_navigation_menu:I = 0x7f03003c

.field public static final entry_channel:I = 0x7f03003d

.field public static final entry_client:I = 0x7f03003e

.field public static final expander_group_holo_dark:I = 0x7f03003f

.field public static final extended_menu_item:I = 0x7f030040

.field public static final ident_list:I = 0x7f030041

.field public static final ident_list_entry:I = 0x7f030042

.field public static final licenseagreement:I = 0x7f030043

.field public static final loading:I = 0x7f030044

.field public static final material_checkbox:I = 0x7f030045

.field public static final material_spinner:I = 0x7f030046

.field public static final notification_media_action:I = 0x7f030047

.field public static final notification_media_cancel_action:I = 0x7f030048

.field public static final notification_template_big_media:I = 0x7f030049

.field public static final notification_template_big_media_narrow:I = 0x7f03004a

.field public static final notification_template_lines:I = 0x7f03004b

.field public static final notification_template_media:I = 0x7f03004c

.field public static final notification_template_part_chronometer:I = 0x7f03004d

.field public static final notification_template_part_time:I = 0x7f03004e

.field public static final select_dialog_item_material:I = 0x7f03004f

.field public static final select_dialog_multichoice_material:I = 0x7f030050

.field public static final select_dialog_singlechoice_material:I = 0x7f030051

.field public static final server_connection_info:I = 0x7f030052

.field public static final splash:I = 0x7f030053

.field public static final start_gui_fragment:I = 0x7f030054

.field public static final support_simple_spinner_dropdown_item:I = 0x7f030055

.field public static final temppass_create:I = 0x7f030056

.field public static final temppass_info:I = 0x7f030057

.field public static final temppass_list:I = 0x7f030058

.field public static final temppass_list_entry:I = 0x7f030059

.field public static final virtual_server_edit:I = 0x7f03005a


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 2922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
