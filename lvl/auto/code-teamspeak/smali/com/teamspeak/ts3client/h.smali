.class public final Lcom/teamspeak/ts3client/h;
.super Landroid/support/v4/app/ax;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/d/s;
.implements Lcom/teamspeak/ts3client/data/w;


# static fields
.field private static at:Lcom/teamspeak/ts3client/h;

.field private static au:Ljava/util/regex/Pattern;

.field private static av:Lcom/teamspeak/ts3client/data/c;


# instance fields
.field private aA:Landroid/widget/TableLayout;

.field private aB:Landroid/support/v4/app/bi;

.field private aC:Lcom/teamspeak/ts3client/d/b/a;

.field private aD:Landroid/widget/TextView;

.field private aE:I

.field private aF:Landroid/widget/LinearLayout;

.field private aG:Landroid/content/Context;

.field private aH:Landroid/widget/TextView;

.field private aI:Landroid/widget/TableRow;

.field private aJ:Lcom/teamspeak/ts3client/customs/FloatingButton;

.field private aw:Lcom/teamspeak/ts3client/Ts3Application;

.field private ax:Landroid/widget/TextView;

.field private ay:Landroid/widget/TextView;

.field private az:Landroid/widget/TableLayout;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 59
    return-void
.end method

.method static synthetic A()Ljava/util/regex/Pattern;
    .registers 1

    .prologue
    .line 39
    sget-object v0, Lcom/teamspeak/ts3client/h;->au:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method private B()I
    .registers 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aw:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 98
    const/high16 v1, 0x41800000    # 16.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private C()V
    .registers 3

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/h;->l()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 175
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/h;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/j;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/j;-><init>(Lcom/teamspeak/ts3client/h;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 299
    :cond_12
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/h;)Landroid/support/v4/app/bi;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aB:Landroid/support/v4/app/bi;

    return-object v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/h;Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/d/b/a;
    .registers 2

    .prologue
    .line 39
    iput-object p1, p0, Lcom/teamspeak/ts3client/h;->aC:Lcom/teamspeak/ts3client/d/b/a;

    return-object p1
.end method

.method public static a(Lcom/teamspeak/ts3client/data/c;)Lcom/teamspeak/ts3client/h;
    .registers 2

    .prologue
    .line 62
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    if-ne v0, p0, :cond_7

    .line 63
    sget-object v0, Lcom/teamspeak/ts3client/h;->at:Lcom/teamspeak/ts3client/h;

    .line 69
    :goto_6
    return-object v0

    .line 65
    :cond_7
    new-instance v0, Lcom/teamspeak/ts3client/h;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/h;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/h;->at:Lcom/teamspeak/ts3client/h;

    .line 66
    sput-object p0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    .line 67
    const-string v0, ".*(\\[Build: (\\d+)\\]).*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/teamspeak/ts3client/h;->au:Ljava/util/regex/Pattern;

    .line 69
    sget-object v0, Lcom/teamspeak/ts3client/h;->at:Lcom/teamspeak/ts3client/h;

    goto :goto_6
.end method

.method private a(Landroid/widget/TableLayout;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .registers 7

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/h;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    .line 74
    if-eqz p3, :cond_2e

    .line 75
    const v1, 0x7f0c015a

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 76
    invoke-direct {p0}, Lcom/teamspeak/ts3client/h;->B()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 77
    invoke-direct {p0}, Lcom/teamspeak/ts3client/h;->B()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 78
    invoke-virtual {v1, p3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 80
    :cond_2e
    const v1, 0x7f0c015b

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    invoke-virtual {p1, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 82
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/h;Landroid/widget/TableLayout;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .registers 7

    .prologue
    .line 39
    .line 15073
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/h;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    .line 15074
    if-eqz p3, :cond_2e

    .line 15075
    const v1, 0x7f0c015a

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 15076
    invoke-direct {p0}, Lcom/teamspeak/ts3client/h;->B()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 15077
    invoke-direct {p0}, Lcom/teamspeak/ts3client/h;->B()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 15078
    invoke-virtual {v1, p3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 15080
    :cond_2e
    const v1, 0x7f0c015b

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 15081
    invoke-virtual {p1, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 39
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/h;)Lcom/teamspeak/ts3client/d/b/a;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aC:Lcom/teamspeak/ts3client/d/b/a;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/h;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->ax:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/h;)I
    .registers 2

    .prologue
    .line 39
    iget v0, p0, Lcom/teamspeak/ts3client/h;->aE:I

    return v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/h;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aw:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/h;)Landroid/widget/TableRow;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aI:Landroid/widget/TableRow;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/h;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aH:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/h;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->ay:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/h;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aD:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/h;)Landroid/widget/TableLayout;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->az:Landroid/widget/TableLayout;

    return-object v0
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/h;)Landroid/widget/TableLayout;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aA:Landroid/widget/TableLayout;

    return-object v0
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/h;)Landroid/widget/LinearLayout;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aF:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/h;)Landroid/content/Context;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aG:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic z()Lcom/teamspeak/ts3client/data/c;
    .registers 1

    .prologue
    .line 39
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    .prologue
    const/high16 v3, 0x41b00000    # 22.0f

    .line 140
    const v0, 0x7f03002e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 141
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/h;->aG:Landroid/content/Context;

    .line 13688
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 142
    iput-object v0, p0, Lcom/teamspeak/ts3client/h;->aB:Landroid/support/v4/app/bi;

    .line 143
    const v0, 0x7f0c014d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/h;->ax:Landroid/widget/TextView;

    .line 144
    const v0, 0x7f0c0151

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/h;->ay:Landroid/widget/TextView;

    .line 145
    const v0, 0x7f0c0153

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/h;->aD:Landroid/widget/TextView;

    .line 146
    const v0, 0x7f0c0155

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lcom/teamspeak/ts3client/h;->az:Landroid/widget/TableLayout;

    .line 147
    const v0, 0x7f0c0157

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lcom/teamspeak/ts3client/h;->aA:Landroid/widget/TableLayout;

    .line 148
    const v0, 0x7f0c0158

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/teamspeak/ts3client/h;->aF:Landroid/widget/LinearLayout;

    .line 149
    const v0, 0x7f0c0159

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/customs/FloatingButton;

    iput-object v0, p0, Lcom/teamspeak/ts3client/h;->aJ:Lcom/teamspeak/ts3client/customs/FloatingButton;

    .line 150
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aJ:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v2, 0x7f020075

    invoke-static {v2, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 151
    const v0, 0x7f0c014f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/h;->aH:Landroid/widget/TextView;

    .line 152
    const v0, 0x7f0c00a1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/teamspeak/ts3client/h;->aI:Landroid/widget/TableRow;

    .line 153
    const-string v0, "clientinfo.nickname"

    const v2, 0x7f0c014c

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 154
    const-string v0, "clientinfo.version"

    const v2, 0x7f0c0150

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 155
    const-string v0, "clientinfo.connected"

    const v2, 0x7f0c0152

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 156
    const-string v0, "clientinfo.sgroup"

    const v2, 0x7f0c0154

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 157
    const-string v0, "clientinfo.cgroup"

    const v2, 0x7f0c0156

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 158
    const-string v0, "clientinfo.description"

    const v2, 0x7f0c014e

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 160
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/h;->n()V

    .line 161
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aw:Lcom/teamspeak/ts3client/Ts3Application;

    .line 14204
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 161
    const-string v2, "dialog.client.info.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aJ:Lcom/teamspeak/ts3client/customs/FloatingButton;

    new-instance v2, Lcom/teamspeak/ts3client/i;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/i;-><init>(Lcom/teamspeak/ts3client/h;)V

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    return-object v1
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 131
    invoke-super {p0, p1}, Landroid/support/v4/app/ax;->a(Landroid/os/Bundle;)V

    .line 132
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/h;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/h;->aw:Lcom/teamspeak/ts3client/Ts3Application;

    .line 133
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/h;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/bb;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 134
    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/h;->aE:I

    .line 136
    return-void
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 3

    .prologue
    .line 328
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 329
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ax;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 330
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 4

    .prologue
    .line 86
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/UpdateClient;

    if-eqz v0, :cond_29

    .line 87
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/UpdateClient;

    .line 1029
    iget v0, p1, Lcom/teamspeak/ts3client/jni/events/UpdateClient;->a:I

    .line 88
    sget-object v1, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    .line 1235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 88
    if-ne v0, v1, :cond_29

    .line 2174
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/h;->l()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 2175
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/h;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/j;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/j;-><init>(Lcom/teamspeak/ts3client/h;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 90
    :cond_20
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aw:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 90
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 94
    :cond_29
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .registers 7

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/support/v4/app/ax;->c(Landroid/os/Bundle;)V

    .line 105
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aw:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 105
    if-eqz v0, :cond_5b

    .line 106
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aw:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 106
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 107
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    if-eqz v0, :cond_57

    .line 108
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aw:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 108
    iget-object v1, p0, Lcom/teamspeak/ts3client/h;->aw:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 108
    sget-object v1, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    .line 8235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 108
    const-string v4, "ClientInfoFragment"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestClientVariables(JILjava/lang/String;)I

    .line 109
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    .line 9211
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 109
    if-eqz v0, :cond_5c

    .line 110
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    .line 10211
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 11062
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/c/a;->k:Z

    .line 110
    if-nez v0, :cond_57

    .line 111
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    .line 11118
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/c;->y:Landroid/graphics/Bitmap;

    .line 111
    if-nez v0, :cond_57

    .line 112
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->b()Ljava/lang/String;

    .line 113
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_57

    .line 114
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/data/c;->a(Lcom/teamspeak/ts3client/data/d/s;)V

    .line 125
    :cond_57
    :goto_57
    iget-object v0, p0, Lcom/teamspeak/ts3client/h;->aw:Lcom/teamspeak/ts3client/Ts3Application;

    .line 13081
    iput-object p0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 127
    :cond_5b
    return-void

    .line 118
    :cond_5c
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    .line 12118
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/c;->y:Landroid/graphics/Bitmap;

    .line 118
    if-nez v0, :cond_57

    .line 119
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->b()Ljava/lang/String;

    .line 120
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_57

    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_57

    .line 121
    sget-object v0, Lcom/teamspeak/ts3client/h;->av:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/data/c;->a(Lcom/teamspeak/ts3client/data/d/s;)V

    goto :goto_57
.end method

.method public final y()V
    .registers 3

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/h;->l()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 304
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/h;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/k;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/k;-><init>(Lcom/teamspeak/ts3client/h;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 324
    :cond_12
    return-void
.end method
