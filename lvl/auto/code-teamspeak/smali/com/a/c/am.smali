.class final Lcom/a/c/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/c/ap;


# instance fields
.field private final a:Lcom/a/c/c/a;

.field private final b:Z

.field private final c:Ljava/lang/Class;

.field private final d:Lcom/a/c/ae;

.field private final e:Lcom/a/c/v;


# direct methods
.method private constructor <init>(Ljava/lang/Object;Lcom/a/c/c/a;ZLjava/lang/Class;)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    instance-of v0, p1, Lcom/a/c/ae;

    if-eqz v0, :cond_28

    move-object v0, p1

    check-cast v0, Lcom/a/c/ae;

    :goto_b
    iput-object v0, p0, Lcom/a/c/am;->d:Lcom/a/c/ae;

    .line 120
    instance-of v0, p1, Lcom/a/c/v;

    if-eqz v0, :cond_2a

    check-cast p1, Lcom/a/c/v;

    :goto_13
    iput-object p1, p0, Lcom/a/c/am;->e:Lcom/a/c/v;

    .line 123
    iget-object v0, p0, Lcom/a/c/am;->d:Lcom/a/c/ae;

    if-nez v0, :cond_1d

    iget-object v0, p0, Lcom/a/c/am;->e:Lcom/a/c/v;

    if-eqz v0, :cond_2c

    :cond_1d
    const/4 v0, 0x1

    :goto_1e
    invoke-static {v0}, Lcom/a/c/b/a;->a(Z)V

    .line 124
    iput-object p2, p0, Lcom/a/c/am;->a:Lcom/a/c/c/a;

    .line 125
    iput-boolean p3, p0, Lcom/a/c/am;->b:Z

    .line 126
    iput-object p4, p0, Lcom/a/c/am;->c:Ljava/lang/Class;

    .line 127
    return-void

    :cond_28
    move-object v0, v1

    .line 117
    goto :goto_b

    :cond_2a
    move-object p1, v1

    .line 120
    goto :goto_13

    .line 123
    :cond_2c
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method synthetic constructor <init>(Ljava/lang/Object;Lcom/a/c/c/a;ZLjava/lang/Class;B)V
    .registers 6

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/a/c/am;-><init>(Ljava/lang/Object;Lcom/a/c/c/a;ZLjava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an;
    .registers 10

    .prologue
    const/4 v6, 0x0

    .line 131
    iget-object v0, p0, Lcom/a/c/am;->a:Lcom/a/c/c/a;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/a/c/am;->a:Lcom/a/c/c/a;

    .line 132
    invoke-virtual {v0, p2}, Lcom/a/c/c/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    iget-boolean v0, p0, Lcom/a/c/am;->b:Z

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/a/c/am;->a:Lcom/a/c/c/a;

    .line 1101
    iget-object v0, v0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    .line 2094
    iget-object v1, p2, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 132
    if-ne v0, v1, :cond_29

    :cond_19
    const/4 v0, 0x1

    .line 134
    :goto_1a
    if-eqz v0, :cond_34

    new-instance v0, Lcom/a/c/ak;

    iget-object v1, p0, Lcom/a/c/am;->d:Lcom/a/c/ae;

    iget-object v2, p0, Lcom/a/c/am;->e:Lcom/a/c/v;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/a/c/ak;-><init>(Lcom/a/c/ae;Lcom/a/c/v;Lcom/a/c/k;Lcom/a/c/c/a;Lcom/a/c/ap;B)V

    :goto_28
    return-object v0

    :cond_29
    move v0, v6

    .line 132
    goto :goto_1a

    :cond_2b
    iget-object v0, p0, Lcom/a/c/am;->c:Ljava/lang/Class;

    .line 3094
    iget-object v1, p2, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 133
    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    goto :goto_1a

    .line 134
    :cond_34
    const/4 v0, 0x0

    goto :goto_28
.end method
