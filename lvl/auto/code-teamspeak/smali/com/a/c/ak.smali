.class final Lcom/a/c/ak;
.super Lcom/a/c/an;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/c/ae;

.field private final b:Lcom/a/c/v;

.field private final c:Lcom/a/c/k;

.field private final d:Lcom/a/c/c/a;

.field private final e:Lcom/a/c/ap;

.field private f:Lcom/a/c/an;


# direct methods
.method private constructor <init>(Lcom/a/c/ae;Lcom/a/c/v;Lcom/a/c/k;Lcom/a/c/c/a;Lcom/a/c/ap;)V
    .registers 6

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/a/c/ak;->a:Lcom/a/c/ae;

    .line 44
    iput-object p2, p0, Lcom/a/c/ak;->b:Lcom/a/c/v;

    .line 45
    iput-object p3, p0, Lcom/a/c/ak;->c:Lcom/a/c/k;

    .line 46
    iput-object p4, p0, Lcom/a/c/ak;->d:Lcom/a/c/c/a;

    .line 47
    iput-object p5, p0, Lcom/a/c/ak;->e:Lcom/a/c/ap;

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/c/ae;Lcom/a/c/v;Lcom/a/c/k;Lcom/a/c/c/a;Lcom/a/c/ap;B)V
    .registers 7

    .prologue
    .line 31
    invoke-direct/range {p0 .. p5}, Lcom/a/c/ak;-><init>(Lcom/a/c/ae;Lcom/a/c/v;Lcom/a/c/k;Lcom/a/c/c/a;Lcom/a/c/ap;)V

    return-void
.end method

.method private a()Lcom/a/c/an;
    .registers 4

    .prologue
    .line 75
    iget-object v0, p0, Lcom/a/c/ak;->f:Lcom/a/c/an;

    .line 76
    if-eqz v0, :cond_5

    .line 78
    :goto_4
    return-object v0

    .line 76
    :cond_5
    iget-object v0, p0, Lcom/a/c/ak;->c:Lcom/a/c/k;

    iget-object v1, p0, Lcom/a/c/ak;->e:Lcom/a/c/ap;

    iget-object v2, p0, Lcom/a/c/ak;->d:Lcom/a/c/c/a;

    .line 78
    invoke-virtual {v0, v1, v2}, Lcom/a/c/k;->a(Lcom/a/c/ap;Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/ak;->f:Lcom/a/c/an;

    goto :goto_4
.end method

.method public static a(Lcom/a/c/c/a;Ljava/lang/Object;)Lcom/a/c/ap;
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 85
    new-instance v0, Lcom/a/c/am;

    const/4 v4, 0x0

    move-object v1, p1

    move-object v2, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/a/c/am;-><init>(Ljava/lang/Object;Lcom/a/c/c/a;ZLjava/lang/Class;B)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/a/c/ap;
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 105
    new-instance v0, Lcom/a/c/am;

    const/4 v2, 0x0

    move-object v1, p1

    move-object v4, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/a/c/am;-><init>(Ljava/lang/Object;Lcom/a/c/c/a;ZLjava/lang/Class;B)V

    return-object v0
.end method

.method public static b(Lcom/a/c/c/a;Ljava/lang/Object;)Lcom/a/c/ap;
    .registers 8

    .prologue
    const/4 v5, 0x0

    .line 95
    .line 3101
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    .line 4094
    iget-object v1, p0, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 95
    if-ne v0, v1, :cond_11

    const/4 v3, 0x1

    .line 96
    :goto_8
    new-instance v0, Lcom/a/c/am;

    const/4 v4, 0x0

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/a/c/am;-><init>(Ljava/lang/Object;Lcom/a/c/c/a;ZLjava/lang/Class;B)V

    return-object v0

    :cond_11
    move v3, v5

    .line 95
    goto :goto_8
.end method


# virtual methods
.method public final a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 51
    iget-object v0, p0, Lcom/a/c/ak;->b:Lcom/a/c/v;

    if-nez v0, :cond_d

    .line 52
    invoke-direct {p0}, Lcom/a/c/ak;->a()Lcom/a/c/an;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/c/an;->a(Lcom/a/c/d/a;)Ljava/lang/Object;

    move-result-object v0

    .line 58
    :goto_c
    return-object v0

    .line 54
    :cond_d
    invoke-static {p1}, Lcom/a/c/b/aq;->a(Lcom/a/c/d/a;)Lcom/a/c/w;

    move-result-object v0

    .line 1074
    instance-of v1, v0, Lcom/a/c/y;

    .line 55
    if-eqz v1, :cond_17

    .line 56
    const/4 v0, 0x0

    goto :goto_c

    .line 58
    :cond_17
    iget-object v1, p0, Lcom/a/c/ak;->b:Lcom/a/c/v;

    iget-object v2, p0, Lcom/a/c/ak;->d:Lcom/a/c/c/a;

    .line 1101
    iget-object v2, v2, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    .line 58
    invoke-interface {v1, v0, v2}, Lcom/a/c/v;->a(Lcom/a/c/w;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_c
.end method

.method public final a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/a/c/ak;->a:Lcom/a/c/ae;

    if-nez v0, :cond_c

    .line 63
    invoke-direct {p0}, Lcom/a/c/ak;->a()Lcom/a/c/an;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/a/c/an;->a(Lcom/a/c/d/e;Ljava/lang/Object;)V

    .line 72
    :goto_b
    return-void

    .line 66
    :cond_c
    if-nez p2, :cond_12

    .line 67
    invoke-virtual {p1}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    goto :goto_b

    .line 70
    :cond_12
    iget-object v0, p0, Lcom/a/c/ak;->a:Lcom/a/c/ae;

    invoke-interface {v0, p2}, Lcom/a/c/ae;->a(Ljava/lang/Object;)Lcom/a/c/w;

    move-result-object v0

    .line 71
    invoke-static {v0, p1}, Lcom/a/c/b/aq;->a(Lcom/a/c/w;Lcom/a/c/d/e;)V

    goto :goto_b
.end method
