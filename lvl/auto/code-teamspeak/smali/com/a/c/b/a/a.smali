.class public final Lcom/a/c/b/a/a;
.super Lcom/a/c/an;
.source "SourceFile"


# static fields
.field public static final a:Lcom/a/c/ap;


# instance fields
.field private final b:Ljava/lang/Class;

.field private final c:Lcom/a/c/an;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 39
    new-instance v0, Lcom/a/c/b/a/b;

    invoke-direct {v0}, Lcom/a/c/b/a/b;-><init>()V

    sput-object v0, Lcom/a/c/b/a/a;->a:Lcom/a/c/ap;

    return-void
.end method

.method public constructor <init>(Lcom/a/c/k;Lcom/a/c/an;Ljava/lang/Class;)V
    .registers 5

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    .line 58
    new-instance v0, Lcom/a/c/b/a/y;

    invoke-direct {v0, p1, p2, p3}, Lcom/a/c/b/a/y;-><init>(Lcom/a/c/k;Lcom/a/c/an;Ljava/lang/reflect/Type;)V

    iput-object v0, p0, Lcom/a/c/b/a/a;->c:Lcom/a/c/an;

    .line 60
    iput-object p3, p0, Lcom/a/c/b/a/a;->b:Ljava/lang/Class;

    .line 61
    return-void
.end method


# virtual methods
.method public final a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 64
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 65
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 66
    const/4 v0, 0x0

    .line 80
    :goto_c
    return-object v0

    .line 69
    :cond_d
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 70
    invoke-virtual {p1}, Lcom/a/c/d/a;->a()V

    .line 71
    :goto_15
    invoke-virtual {p1}, Lcom/a/c/d/a;->e()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 72
    iget-object v0, p0, Lcom/a/c/b/a/a;->c:Lcom/a/c/an;

    invoke-virtual {v0, p1}, Lcom/a/c/an;->a(Lcom/a/c/d/a;)Ljava/lang/Object;

    move-result-object v0

    .line 73
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_15

    .line 75
    :cond_25
    invoke-virtual {p1}, Lcom/a/c/d/a;->b()V

    .line 76
    iget-object v0, p0, Lcom/a/c/b/a/a;->b:Ljava/lang/Class;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v1

    .line 77
    const/4 v0, 0x0

    :goto_33
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_43

    .line 78
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v1, v0, v3}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 77
    add-int/lit8 v0, v0, 0x1

    goto :goto_33

    :cond_43
    move-object v0, v1

    .line 80
    goto :goto_c
.end method

.method public final a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 7

    .prologue
    .line 85
    if-nez p2, :cond_6

    .line 86
    invoke-virtual {p1}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 96
    :goto_5
    return-void

    .line 90
    :cond_6
    invoke-virtual {p1}, Lcom/a/c/d/e;->b()Lcom/a/c/d/e;

    .line 91
    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    :goto_e
    if-ge v0, v1, :cond_1c

    .line 92
    invoke-static {p2, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    .line 93
    iget-object v3, p0, Lcom/a/c/b/a/a;->c:Lcom/a/c/an;

    invoke-virtual {v3, p1, v2}, Lcom/a/c/an;->a(Lcom/a/c/d/e;Ljava/lang/Object;)V

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 95
    :cond_1c
    invoke-virtual {p1}, Lcom/a/c/d/e;->c()Lcom/a/c/d/e;

    goto :goto_5
.end method
