.class public final Lcom/a/c/b/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/c/ap;


# instance fields
.field private final a:Lcom/a/c/b/f;


# direct methods
.method public constructor <init>(Lcom/a/c/b/f;)V
    .registers 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/a/c/b/a/g;->a:Lcom/a/c/b/f;

    .line 38
    return-void
.end method

.method static a(Lcom/a/c/b/f;Lcom/a/c/k;Lcom/a/c/c/a;Lcom/a/c/a/b;)Lcom/a/c/an;
    .registers 6

    .prologue
    .line 52
    invoke-interface {p3}, Lcom/a/c/a/b;->a()Ljava/lang/Class;

    move-result-object v0

    .line 53
    const-class v1, Lcom/a/c/an;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 55
    invoke-static {v0}, Lcom/a/c/c/a;->a(Ljava/lang/Class;)Lcom/a/c/c/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/c/b/f;->a(Lcom/a/c/c/a;)Lcom/a/c/b/ao;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/c/b/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/an;

    .line 61
    :goto_1a
    return-object v0

    .line 57
    :cond_1b
    const-class v1, Lcom/a/c/ap;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_36

    .line 59
    invoke-static {v0}, Lcom/a/c/c/a;->a(Ljava/lang/Class;)Lcom/a/c/c/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/c/b/f;->a(Lcom/a/c/c/a;)Lcom/a/c/b/ao;

    move-result-object v0

    .line 60
    invoke-interface {v0}, Lcom/a/c/b/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/ap;

    .line 61
    invoke-interface {v0, p1, p2}, Lcom/a/c/ap;->a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v0

    goto :goto_1a

    .line 64
    :cond_36
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "@JsonAdapter value must be TypeAdapter or TypeAdapterFactory reference."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an;
    .registers 5

    .prologue
    .line 42
    .line 1094
    iget-object v0, p2, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 42
    const-class v1, Lcom/a/c/a/b;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/a/c/a/b;

    .line 43
    if-nez v0, :cond_e

    .line 44
    const/4 v0, 0x0

    .line 46
    :goto_d
    return-object v0

    :cond_e
    iget-object v1, p0, Lcom/a/c/b/a/g;->a:Lcom/a/c/b/f;

    invoke-static {v1, p1, p2, v0}, Lcom/a/c/b/a/g;->a(Lcom/a/c/b/f;Lcom/a/c/k;Lcom/a/c/c/a;Lcom/a/c/a/b;)Lcom/a/c/an;

    move-result-object v0

    goto :goto_d
.end method
