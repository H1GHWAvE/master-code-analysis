.class final Lcom/a/c/b/a/ac;
.super Lcom/a/c/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/e;Ljava/lang/Number;)V
    .registers 2

    .prologue
    .line 318
    invoke-virtual {p0, p1}, Lcom/a/c/d/e;->a(Ljava/lang/Number;)Lcom/a/c/d/e;

    .line 319
    return-void
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/lang/Number;
    .registers 5

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    .line 306
    sget-object v1, Lcom/a/c/b/a/ba;->a:[I

    invoke-virtual {v0}, Lcom/a/c/d/d;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_34

    .line 313
    :pswitch_f
    new-instance v1, Lcom/a/c/ag;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expecting number, got: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/String;)V

    throw v1

    .line 308
    :pswitch_24
    invoke-virtual {p0}, Lcom/a/c/d/a;->k()V

    .line 309
    const/4 v0, 0x0

    .line 311
    :goto_28
    return-object v0

    :pswitch_29
    new-instance v0, Lcom/a/c/b/v;

    invoke-virtual {p0}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/c/b/v;-><init>(Ljava/lang/String;)V

    goto :goto_28

    .line 306
    nop

    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_29
        :pswitch_f
        :pswitch_f
        :pswitch_24
    .end packed-switch
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 302
    .line 1305
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    .line 1306
    sget-object v1, Lcom/a/c/b/a/ba;->a:[I

    invoke-virtual {v0}, Lcom/a/c/d/d;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_34

    .line 1313
    :pswitch_f
    new-instance v1, Lcom/a/c/ag;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expecting number, got: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1308
    :pswitch_24
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 1309
    const/4 v0, 0x0

    .line 1311
    :goto_28
    return-object v0

    :pswitch_29
    new-instance v0, Lcom/a/c/b/v;

    invoke-virtual {p1}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/c/b/v;-><init>(Ljava/lang/String;)V

    goto :goto_28

    .line 1306
    nop

    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_29
        :pswitch_f
        :pswitch_f
        :pswitch_24
    .end packed-switch
.end method

.method public final bridge synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 302
    check-cast p2, Ljava/lang/Number;

    .line 1318
    invoke-virtual {p1, p2}, Lcom/a/c/d/e;->a(Ljava/lang/Number;)Lcom/a/c/d/e;

    .line 302
    return-void
.end method
