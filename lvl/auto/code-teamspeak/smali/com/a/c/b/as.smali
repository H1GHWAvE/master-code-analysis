.class final Lcom/a/c/b/as;
.super Ljava/io/Writer;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/Appendable;

.field private final b:Lcom/a/c/b/at;


# direct methods
.method private constructor <init>(Ljava/lang/Appendable;)V
    .registers 3

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/io/Writer;-><init>()V

    .line 82
    new-instance v0, Lcom/a/c/b/at;

    invoke-direct {v0}, Lcom/a/c/b/at;-><init>()V

    iput-object v0, p0, Lcom/a/c/b/as;->b:Lcom/a/c/b/at;

    .line 85
    iput-object p1, p0, Lcom/a/c/b/as;->a:Ljava/lang/Appendable;

    .line 86
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Appendable;B)V
    .registers 3

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/a/c/b/as;-><init>(Ljava/lang/Appendable;)V

    return-void
.end method


# virtual methods
.method public final close()V
    .registers 1

    .prologue
    .line 98
    return-void
.end method

.method public final flush()V
    .registers 1

    .prologue
    .line 97
    return-void
.end method

.method public final write(I)V
    .registers 4

    .prologue
    .line 94
    iget-object v0, p0, Lcom/a/c/b/as;->a:Ljava/lang/Appendable;

    int-to-char v1, p1

    invoke-interface {v0, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 95
    return-void
.end method

.method public final write([CII)V
    .registers 7

    .prologue
    .line 89
    iget-object v0, p0, Lcom/a/c/b/as;->b:Lcom/a/c/b/at;

    iput-object p1, v0, Lcom/a/c/b/at;->a:[C

    .line 90
    iget-object v0, p0, Lcom/a/c/b/as;->a:Ljava/lang/Appendable;

    iget-object v1, p0, Lcom/a/c/b/as;->b:Lcom/a/c/b/at;

    add-int v2, p2, p3

    invoke-interface {v0, v1, p2, v2}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    .line 91
    return-void
.end method
