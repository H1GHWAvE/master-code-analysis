.class final Lcom/a/c/b/a/aq;
.super Lcom/a/c/an;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "year"

.field private static final b:Ljava/lang/String; = "month"

.field private static final c:Ljava/lang/String; = "dayOfMonth"

.field private static final d:Ljava/lang/String; = "hourOfDay"

.field private static final e:Ljava/lang/String; = "minute"

.field private static final f:Ljava/lang/String; = "second"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 537
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/e;Ljava/util/Calendar;)V
    .registers 4

    .prologue
    .line 581
    if-nez p1, :cond_6

    .line 582
    invoke-virtual {p0}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 599
    :goto_5
    return-void

    .line 585
    :cond_6
    invoke-virtual {p0}, Lcom/a/c/d/e;->d()Lcom/a/c/d/e;

    .line 586
    const-string v0, "year"

    invoke-virtual {p0, v0}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 587
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 588
    const-string v0, "month"

    invoke-virtual {p0, v0}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 589
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 590
    const-string v0, "dayOfMonth"

    invoke-virtual {p0, v0}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 591
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 592
    const-string v0, "hourOfDay"

    invoke-virtual {p0, v0}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 593
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 594
    const-string v0, "minute"

    invoke-virtual {p0, v0}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 595
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 596
    const-string v0, "second"

    invoke-virtual {p0, v0}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 597
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 598
    invoke-virtual {p0}, Lcom/a/c/d/e;->e()Lcom/a/c/d/e;

    goto :goto_5
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/util/Calendar;
    .registers 10

    .prologue
    const/4 v6, 0x0

    .line 547
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_e

    .line 548
    invoke-virtual {p0}, Lcom/a/c/d/a;->k()V

    .line 549
    const/4 v0, 0x0

    .line 576
    :goto_d
    return-object v0

    .line 551
    :cond_e
    invoke-virtual {p0}, Lcom/a/c/d/a;->c()V

    move v5, v6

    move v4, v6

    move v3, v6

    move v2, v6

    move v1, v6

    .line 558
    :cond_16
    :goto_16
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v7, Lcom/a/c/d/d;->d:Lcom/a/c/d/d;

    if-eq v0, v7, :cond_62

    .line 559
    invoke-virtual {p0}, Lcom/a/c/d/a;->h()Ljava/lang/String;

    move-result-object v7

    .line 560
    invoke-virtual {p0}, Lcom/a/c/d/a;->n()I

    move-result v0

    .line 561
    const-string v8, "year"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_30

    move v1, v0

    .line 562
    goto :goto_16

    .line 563
    :cond_30
    const-string v8, "month"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3a

    move v2, v0

    .line 564
    goto :goto_16

    .line 565
    :cond_3a
    const-string v8, "dayOfMonth"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_44

    move v3, v0

    .line 566
    goto :goto_16

    .line 567
    :cond_44
    const-string v8, "hourOfDay"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4e

    move v4, v0

    .line 568
    goto :goto_16

    .line 569
    :cond_4e
    const-string v8, "minute"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_58

    move v5, v0

    .line 570
    goto :goto_16

    .line 571
    :cond_58
    const-string v8, "second"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_16

    move v6, v0

    .line 572
    goto :goto_16

    .line 575
    :cond_62
    invoke-virtual {p0}, Lcom/a/c/d/a;->d()V

    .line 576
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct/range {v0 .. v6}, Ljava/util/GregorianCalendar;-><init>(IIIIII)V

    goto :goto_d
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 11

    .prologue
    const/4 v6, 0x0

    .line 537
    .line 1547
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_e

    .line 1548
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 1549
    const/4 v0, 0x0

    :goto_d
    return-object v0

    .line 1551
    :cond_e
    invoke-virtual {p1}, Lcom/a/c/d/a;->c()V

    move v5, v6

    move v4, v6

    move v3, v6

    move v2, v6

    move v1, v6

    .line 1558
    :cond_16
    :goto_16
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v7, Lcom/a/c/d/d;->d:Lcom/a/c/d/d;

    if-eq v0, v7, :cond_62

    .line 1559
    invoke-virtual {p1}, Lcom/a/c/d/a;->h()Ljava/lang/String;

    move-result-object v7

    .line 1560
    invoke-virtual {p1}, Lcom/a/c/d/a;->n()I

    move-result v0

    .line 1561
    const-string v8, "year"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_30

    move v1, v0

    .line 1562
    goto :goto_16

    .line 1563
    :cond_30
    const-string v8, "month"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3a

    move v2, v0

    .line 1564
    goto :goto_16

    .line 1565
    :cond_3a
    const-string v8, "dayOfMonth"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_44

    move v3, v0

    .line 1566
    goto :goto_16

    .line 1567
    :cond_44
    const-string v8, "hourOfDay"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4e

    move v4, v0

    .line 1568
    goto :goto_16

    .line 1569
    :cond_4e
    const-string v8, "minute"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_58

    move v5, v0

    .line 1570
    goto :goto_16

    .line 1571
    :cond_58
    const-string v8, "second"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_16

    move v6, v0

    .line 1572
    goto :goto_16

    .line 1575
    :cond_62
    invoke-virtual {p1}, Lcom/a/c/d/a;->d()V

    .line 1576
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct/range {v0 .. v6}, Ljava/util/GregorianCalendar;-><init>(IIIIII)V

    goto :goto_d
.end method

.method public final synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 537
    check-cast p2, Ljava/util/Calendar;

    .line 1581
    if-nez p2, :cond_8

    .line 1582
    invoke-virtual {p1}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 1583
    :goto_7
    return-void

    .line 1585
    :cond_8
    invoke-virtual {p1}, Lcom/a/c/d/e;->d()Lcom/a/c/d/e;

    .line 1586
    const-string v0, "year"

    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 1587
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 1588
    const-string v0, "month"

    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 1589
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 1590
    const-string v0, "dayOfMonth"

    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 1591
    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 1592
    const-string v0, "hourOfDay"

    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 1593
    const/16 v0, 0xb

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 1594
    const-string v0, "minute"

    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 1595
    const/16 v0, 0xc

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 1596
    const-string v0, "second"

    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 1597
    const/16 v0, 0xd

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 1598
    invoke-virtual {p1}, Lcom/a/c/d/e;->e()Lcom/a/c/d/e;

    goto :goto_7
.end method
