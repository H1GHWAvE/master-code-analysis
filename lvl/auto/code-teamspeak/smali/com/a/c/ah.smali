.class public abstract enum Lcom/a/c/ah;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/a/c/ah;

.field public static final enum b:Lcom/a/c/ah;

.field private static final synthetic c:[Lcom/a/c/ah;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 34
    new-instance v0, Lcom/a/c/ai;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1}, Lcom/a/c/ai;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/c/ah;->a:Lcom/a/c/ah;

    .line 45
    new-instance v0, Lcom/a/c/aj;

    const-string v1, "STRING"

    invoke-direct {v0, v1}, Lcom/a/c/aj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/c/ah;->b:Lcom/a/c/ah;

    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/c/ah;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/c/ah;->a:Lcom/a/c/ah;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/c/ah;->b:Lcom/a/c/ah;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/c/ah;->c:[Lcom/a/c/ah;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/a/c/ah;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/c/ah;
    .registers 2

    .prologue
    .line 27
    const-class v0, Lcom/a/c/ah;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/c/ah;

    return-object v0
.end method

.method public static values()[Lcom/a/c/ah;
    .registers 1

    .prologue
    .line 27
    sget-object v0, Lcom/a/c/ah;->c:[Lcom/a/c/ah;

    invoke-virtual {v0}, [Lcom/a/c/ah;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/c/ah;

    return-object v0
.end method


# virtual methods
.method public abstract a(Ljava/lang/Long;)Lcom/a/c/w;
.end method
