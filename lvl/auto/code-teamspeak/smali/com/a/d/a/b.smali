.class final enum Lcom/a/d/a/b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field public static final enum a:Lcom/a/d/a/b;

.field public static final enum b:Lcom/a/d/a/b;

.field private static final synthetic e:[Lcom/a/d/a/b;


# instance fields
.field private final c:C

.field private final d:C


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 28
    new-instance v0, Lcom/a/d/a/b;

    const-string v1, "PRIVATE"

    const/16 v2, 0x3a

    const/16 v3, 0x2c

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/a/d/a/b;-><init>(Ljava/lang/String;ICC)V

    sput-object v0, Lcom/a/d/a/b;->a:Lcom/a/d/a/b;

    .line 30
    new-instance v0, Lcom/a/d/a/b;

    const-string v1, "ICANN"

    const/16 v2, 0x21

    const/16 v3, 0x3f

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/a/d/a/b;-><init>(Ljava/lang/String;ICC)V

    sput-object v0, Lcom/a/d/a/b;->b:Lcom/a/d/a/b;

    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/d/a/b;

    sget-object v1, Lcom/a/d/a/b;->a:Lcom/a/d/a/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/a/d/a/b;->b:Lcom/a/d/a/b;

    aput-object v1, v0, v5

    sput-object v0, Lcom/a/d/a/b;->e:[Lcom/a/d/a/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ICC)V
    .registers 5

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput-char p3, p0, Lcom/a/d/a/b;->c:C

    .line 40
    iput-char p4, p0, Lcom/a/d/a/b;->d:C

    .line 41
    return-void
.end method

.method private a()C
    .registers 2

    .prologue
    .line 44
    iget-char v0, p0, Lcom/a/d/a/b;->d:C

    return v0
.end method

.method static a(C)Lcom/a/d/a/b;
    .registers 6

    .prologue
    .line 53
    invoke-static {}, Lcom/a/d/a/b;->values()[Lcom/a/d/a/b;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_16

    aget-object v3, v1, v0

    .line 1048
    iget-char v4, v3, Lcom/a/d/a/b;->c:C

    .line 54
    if-eq v4, p0, :cond_12

    .line 2044
    iget-char v4, v3, Lcom/a/d/a/b;->d:C

    .line 54
    if-ne v4, p0, :cond_13

    .line 55
    :cond_12
    return-object v3

    .line 53
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 58
    :cond_16
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x26

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "No enum corresponding to given code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Z)Lcom/a/d/a/b;
    .registers 2

    .prologue
    .line 62
    if-eqz p0, :cond_5

    sget-object v0, Lcom/a/d/a/b;->a:Lcom/a/d/a/b;

    :goto_4
    return-object v0

    :cond_5
    sget-object v0, Lcom/a/d/a/b;->b:Lcom/a/d/a/b;

    goto :goto_4
.end method

.method private b()C
    .registers 2

    .prologue
    .line 48
    iget-char v0, p0, Lcom/a/d/a/b;->c:C

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/d/a/b;
    .registers 2

    .prologue
    .line 24
    const-class v0, Lcom/a/d/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/d/a/b;

    return-object v0
.end method

.method public static values()[Lcom/a/d/a/b;
    .registers 1

    .prologue
    .line 24
    sget-object v0, Lcom/a/d/a/b;->e:[Lcom/a/d/a/b;

    invoke-virtual {v0}, [Lcom/a/d/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/d/a/b;

    return-object v0
.end method
