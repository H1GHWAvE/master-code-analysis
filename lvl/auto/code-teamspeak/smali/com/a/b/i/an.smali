.class public final Lcom/a/b/i/an;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:I = 0x800


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Readable;Ljava/lang/Appendable;)J
    .registers 8

    .prologue
    .line 64
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const/16 v0, 0x800

    invoke-static {v0}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v2

    .line 67
    const-wide/16 v0, 0x0

    .line 68
    :goto_e
    invoke-interface {p0, v2}, Ljava/lang/Readable;->read(Ljava/nio/CharBuffer;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_25

    .line 69
    invoke-virtual {v2}, Ljava/nio/CharBuffer;->flip()Ljava/nio/Buffer;

    .line 70
    invoke-interface {p1, v2}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 71
    invoke-virtual {v2}, Ljava/nio/CharBuffer;->remaining()I

    move-result v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 72
    invoke-virtual {v2}, Ljava/nio/CharBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_e

    .line 74
    :cond_25
    return-wide v0
.end method

.method private static a()Ljava/io/Writer;
    .registers 1

    .prologue
    .line 184
    invoke-static {}, Lcom/a/b/i/ap;->a()Lcom/a/b/i/ap;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Appendable;)Ljava/io/Writer;
    .registers 2

    .prologue
    .line 257
    instance-of v0, p0, Ljava/io/Writer;

    if-eqz v0, :cond_7

    .line 258
    check-cast p0, Ljava/io/Writer;

    .line 260
    :goto_6
    return-object p0

    :cond_7
    new-instance v0, Lcom/a/b/i/a;

    invoke-direct {v0, p0}, Lcom/a/b/i/a;-><init>(Ljava/lang/Appendable;)V

    move-object p0, v0

    goto :goto_6
.end method

.method private static a(Ljava/lang/Readable;Lcom/a/b/i/by;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 138
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    new-instance v0, Lcom/a/b/i/bz;

    invoke-direct {v0, p0}, Lcom/a/b/i/bz;-><init>(Ljava/lang/Readable;)V

    .line 143
    :goto_b
    invoke-virtual {v0}, Lcom/a/b/i/bz;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_15

    .line 144
    invoke-interface {p1, v1}, Lcom/a/b/i/by;->a(Ljava/lang/String;)Z

    goto :goto_b

    .line 148
    :cond_15
    invoke-interface {p1}, Lcom/a/b/i/by;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Readable;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 1098
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1099
    invoke-static {p0, v0}, Lcom/a/b/i/an;->a(Ljava/lang/Readable;Ljava/lang/Appendable;)J

    .line 86
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/io/Reader;J)V
    .registers 10

    .prologue
    const-wide/16 v4, 0x0

    .line 163
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    :goto_5
    cmp-long v0, p1, v4

    if-lez v0, :cond_24

    .line 165
    invoke-virtual {p0, p1, p2}, Ljava/io/Reader;->skip(J)J

    move-result-wide v0

    .line 166
    cmp-long v2, v0, v4

    if-nez v2, :cond_22

    .line 168
    invoke-virtual {p0}, Ljava/io/Reader;->read()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1e

    .line 169
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 171
    :cond_1e
    const-wide/16 v0, 0x1

    sub-long/2addr p1, v0

    goto :goto_5

    .line 173
    :cond_22
    sub-long/2addr p1, v0

    .line 175
    goto :goto_5

    .line 176
    :cond_24
    return-void
.end method

.method private static b(Ljava/lang/Readable;)Ljava/lang/StringBuilder;
    .registers 2

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    invoke-static {p0, v0}, Lcom/a/b/i/an;->a(Ljava/lang/Readable;Ljava/lang/Appendable;)J

    .line 100
    return-object v0
.end method

.method private static c(Ljava/lang/Readable;)Ljava/util/List;
    .registers 4

    .prologue
    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 118
    new-instance v1, Lcom/a/b/i/bz;

    invoke-direct {v1, p0}, Lcom/a/b/i/bz;-><init>(Ljava/lang/Readable;)V

    .line 120
    :goto_a
    invoke-virtual {v1}, Lcom/a/b/i/bz;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_14

    .line 121
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 123
    :cond_14
    return-object v0
.end method

.method private static d(Ljava/lang/Readable;)Ljava/io/Reader;
    .registers 2

    .prologue
    .line 266
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    instance-of v0, p0, Ljava/io/Reader;

    if-eqz v0, :cond_a

    .line 268
    check-cast p0, Ljava/io/Reader;

    .line 270
    :goto_9
    return-object p0

    :cond_a
    new-instance v0, Lcom/a/b/i/ao;

    invoke-direct {v0, p0}, Lcom/a/b/i/ao;-><init>(Ljava/lang/Readable;)V

    move-object p0, v0

    goto :goto_9
.end method
