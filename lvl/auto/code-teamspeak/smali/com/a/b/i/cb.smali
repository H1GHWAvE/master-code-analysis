.class public final Lcom/a/b/i/cb;
.super Ljava/io/FilterInputStream;
.source "SourceFile"

# interfaces
.implements Ljava/io/DataInput;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# direct methods
.method private constructor <init>(Ljava/io/InputStream;)V
    .registers 3

    .prologue
    .line 53
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    invoke-direct {p0, v0}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 54
    return-void
.end method

.method private a()B
    .registers 3

    .prologue
    .line 223
    iget-object v0, p0, Lcom/a/b/i/cb;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 225
    const/4 v1, -0x1

    if-ne v1, v0, :cond_f

    .line 226
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 229
    :cond_f
    int-to-byte v0, v0

    return v0
.end method


# virtual methods
.method public final readBoolean()Z
    .registers 2

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/a/b/i/cb;->readUnsignedByte()I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final readByte()B
    .registers 2

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/a/b/i/cb;->readUnsignedByte()I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public final readChar()C
    .registers 2

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/a/b/i/cb;->readUnsignedShort()I

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method public final readDouble()D
    .registers 3

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/a/b/i/cb;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public final readFloat()F
    .registers 2

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/a/b/i/cb;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public final readFully([B)V
    .registers 2

    .prologue
    .line 66
    invoke-static {p0, p1}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;[B)V

    .line 67
    return-void
.end method

.method public final readFully([BII)V
    .registers 4

    .prologue
    .line 71
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;[BII)V

    .line 72
    return-void
.end method

.method public final readInt()I
    .registers 5

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v0

    .line 117
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v1

    .line 118
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v2

    .line 119
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v3

    .line 121
    invoke-static {v3, v2, v1, v0}, Lcom/a/b/l/q;->a(BBBB)I

    move-result v0

    return v0
.end method

.method public final readLine()Ljava/lang/String;
    .registers 3

    .prologue
    .line 61
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "readLine is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final readLong()J
    .registers 9

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v7

    .line 135
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v6

    .line 136
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v5

    .line 137
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v4

    .line 138
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v3

    .line 139
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v2

    .line 140
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v1

    .line 141
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v0

    .line 143
    invoke-static/range {v0 .. v7}, Lcom/a/b/l/u;->a(BBBBBBBB)J

    move-result-wide v0

    return-wide v0
.end method

.method public final readShort()S
    .registers 2

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/a/b/i/cb;->readUnsignedShort()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public final readUTF()Ljava/lang/String;
    .registers 3

    .prologue
    .line 175
    new-instance v0, Ljava/io/DataInputStream;

    iget-object v1, p0, Lcom/a/b/i/cb;->in:Ljava/io/InputStream;

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final readUnsignedByte()I
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/a/b/i/cb;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 82
    if-gez v0, :cond_e

    .line 83
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 86
    :cond_e
    return v0
.end method

.method public final readUnsignedShort()I
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 100
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v0

    .line 101
    invoke-direct {p0}, Lcom/a/b/i/cb;->a()B

    move-result v1

    .line 103
    invoke-static {v2, v2, v1, v0}, Lcom/a/b/l/q;->a(BBBB)I

    move-result v0

    return v0
.end method

.method public final skipBytes(I)I
    .registers 6

    .prologue
    .line 76
    iget-object v0, p0, Lcom/a/b/i/cb;->in:Ljava/io/InputStream;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method
