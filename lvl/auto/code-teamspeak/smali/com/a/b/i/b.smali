.class public abstract Lcom/a/b/i/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final a:Lcom/a/b/i/b;

.field private static final b:Lcom/a/b/i/b;

.field private static final c:Lcom/a/b/i/b;

.field private static final d:Lcom/a/b/i/b;

.field private static final e:Lcom/a/b/i/b;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/16 v4, 0x3d

    .line 345
    new-instance v0, Lcom/a/b/i/j;

    const-string v1, "base64()"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/i/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    sput-object v0, Lcom/a/b/i/b;->a:Lcom/a/b/i/b;

    .line 365
    new-instance v0, Lcom/a/b/i/j;

    const-string v1, "base64Url()"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/i/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    sput-object v0, Lcom/a/b/i/b;->b:Lcom/a/b/i/b;

    .line 386
    new-instance v0, Lcom/a/b/i/j;

    const-string v1, "base32()"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/i/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    sput-object v0, Lcom/a/b/i/b;->c:Lcom/a/b/i/b;

    .line 406
    new-instance v0, Lcom/a/b/i/j;

    const-string v1, "base32Hex()"

    const-string v2, "0123456789ABCDEFGHIJKLMNOPQRSTUV"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/i/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    sput-object v0, Lcom/a/b/i/b;->d:Lcom/a/b/i/b;

    .line 425
    new-instance v0, Lcom/a/b/i/j;

    const-string v1, "base16()"

    const-string v2, "0123456789ABCDEF"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/i/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    sput-object v0, Lcom/a/b/i/b;->e:Lcom/a/b/i/b;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/b/i/bu;Lcom/a/b/b/m;)Lcom/a/b/i/bu;
    .registers 3

    .prologue
    .line 757
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 758
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 759
    new-instance v0, Lcom/a/b/i/e;

    invoke-direct {v0, p0, p1}, Lcom/a/b/i/e;-><init>(Lcom/a/b/i/bu;Lcom/a/b/b/m;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/i/bv;Ljava/lang/String;I)Lcom/a/b/i/bv;
    .registers 4

    .prologue
    .line 778
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 779
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 780
    if-lez p2, :cond_12

    const/4 v0, 0x1

    :goto_9
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 781
    new-instance v0, Lcom/a/b/i/f;

    invoke-direct {v0, p2, p1, p0}, Lcom/a/b/i/f;-><init>(ILjava/lang/String;Lcom/a/b/i/bv;)V

    return-object v0

    .line 780
    :cond_12
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private a(Lcom/a/b/i/ag;)Lcom/a/b/i/p;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "ByteSink,CharSink"
    .end annotation

    .prologue
    .line 196
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    new-instance v0, Lcom/a/b/i/c;

    invoke-direct {v0, p0, p1}, Lcom/a/b/i/c;-><init>(Lcom/a/b/i/b;Lcom/a/b/i/ag;)V

    return-object v0
.end method

.method private a(Lcom/a/b/i/ah;)Lcom/a/b/i/s;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "ByteSource,CharSource"
    .end annotation

    .prologue
    .line 272
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    new-instance v0, Lcom/a/b/i/d;

    invoke-direct {v0, p0, p1}, Lcom/a/b/i/d;-><init>(Lcom/a/b/i/b;Lcom/a/b/i/ah;)V

    return-object v0
.end method

.method private a(Ljava/io/Reader;)Ljava/io/InputStream;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "Reader,InputStream"
    .end annotation

    .prologue
    .line 263
    .line 5053
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5054
    new-instance v0, Lcom/a/b/i/bm;

    invoke-direct {v0, p1}, Lcom/a/b/i/bm;-><init>(Ljava/io/Reader;)V

    .line 263
    invoke-virtual {p0, v0}, Lcom/a/b/i/b;->a(Lcom/a/b/i/bu;)Lcom/a/b/i/bs;

    move-result-object v0

    .line 5104
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5105
    new-instance v1, Lcom/a/b/i/bo;

    invoke-direct {v1, v0}, Lcom/a/b/i/bo;-><init>(Lcom/a/b/i/bs;)V

    .line 263
    return-object v1
.end method

.method private a(Ljava/io/Writer;)Ljava/io/OutputStream;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "Writer,OutputStream"
    .end annotation

    .prologue
    .line 188
    .line 2187
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2188
    new-instance v0, Lcom/a/b/i/bq;

    invoke-direct {v0, p1}, Lcom/a/b/i/bq;-><init>(Ljava/io/Writer;)V

    .line 188
    invoke-virtual {p0, v0}, Lcom/a/b/i/b;->a(Lcom/a/b/i/bv;)Lcom/a/b/i/bt;

    move-result-object v0

    .line 3154
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3155
    new-instance v1, Lcom/a/b/i/bp;

    invoke-direct {v1, v0}, Lcom/a/b/i/bp;-><init>(Lcom/a/b/i/bt;)V

    .line 188
    return-object v1
.end method

.method private a([B)Ljava/lang/String;
    .registers 4

    .prologue
    .line 158
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    array-length v1, p1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/i/b;->a([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;)[B
    .registers 4

    .prologue
    .line 226
    :try_start_0
    invoke-direct {p0, p1}, Lcom/a/b/i/b;->b(Ljava/lang/CharSequence;)[B
    :try_end_3
    .catch Lcom/a/b/i/h; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    return-object v0

    .line 227
    :catch_5
    move-exception v0

    .line 228
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b(Ljava/lang/CharSequence;)[B
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 240
    invoke-virtual {p0}, Lcom/a/b/i/b;->a()Lcom/a/b/b/m;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/b/m;->k(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 4071
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4072
    new-instance v1, Lcom/a/b/i/bn;

    invoke-direct {v1, v0}, Lcom/a/b/i/bn;-><init>(Ljava/lang/CharSequence;)V

    .line 241
    invoke-virtual {p0, v1}, Lcom/a/b/i/b;->a(Lcom/a/b/i/bu;)Lcom/a/b/i/bs;

    move-result-object v5

    .line 242
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/a/b/i/b;->b(I)I

    move-result v0

    new-array v0, v0, [B

    .line 245
    :try_start_1f
    invoke-interface {v5}, Lcom/a/b/i/bs;->a()I

    move-result v1

    move v2, v3

    :goto_24
    const/4 v4, -0x1

    if-eq v1, v4, :cond_3b

    .line 246
    add-int/lit8 v4, v2, 0x1

    int-to-byte v1, v1

    aput-byte v1, v0, v2

    .line 245
    invoke-interface {v5}, Lcom/a/b/i/bs;->a()I
    :try_end_2f
    .catch Lcom/a/b/i/h; {:try_start_1f .. :try_end_2f} :catch_32
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_2f} :catch_34

    move-result v1

    move v2, v4

    goto :goto_24

    .line 249
    :catch_32
    move-exception v0

    throw v0

    .line 250
    :catch_34
    move-exception v0

    .line 251
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 4208
    :cond_3b
    array-length v1, v0

    if-ne v2, v1, :cond_3f

    .line 4209
    :goto_3e
    return-object v0

    .line 4211
    :cond_3f
    new-array v1, v2, [B

    .line 4212
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    .line 253
    goto :goto_3e
.end method

.method private static b([BI)[B
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 208
    array-length v0, p0

    if-ne p1, v0, :cond_5

    .line 213
    :goto_4
    return-object p0

    .line 211
    :cond_5
    new-array v0, p1, [B

    .line 212
    invoke-static {p0, v1, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 213
    goto :goto_4
.end method

.method public static e()Lcom/a/b/i/b;
    .registers 1

    .prologue
    .line 443
    sget-object v0, Lcom/a/b/i/b;->e:Lcom/a/b/i/b;

    return-object v0
.end method

.method private static f()Lcom/a/b/i/b;
    .registers 1

    .prologue
    .line 362
    sget-object v0, Lcom/a/b/i/b;->a:Lcom/a/b/i/b;

    return-object v0
.end method

.method private static g()Lcom/a/b/i/b;
    .registers 1

    .prologue
    .line 383
    sget-object v0, Lcom/a/b/i/b;->b:Lcom/a/b/i/b;

    return-object v0
.end method

.method private static h()Lcom/a/b/i/b;
    .registers 1

    .prologue
    .line 403
    sget-object v0, Lcom/a/b/i/b;->c:Lcom/a/b/i/b;

    return-object v0
.end method

.method private static i()Lcom/a/b/i/b;
    .registers 1

    .prologue
    .line 422
    sget-object v0, Lcom/a/b/i/b;->d:Lcom/a/b/i/b;

    return-object v0
.end method


# virtual methods
.method abstract a(I)I
.end method

.method abstract a()Lcom/a/b/b/m;
.end method

.method public abstract a(C)Lcom/a/b/i/b;
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;I)Lcom/a/b/i/b;
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method abstract a(Lcom/a/b/i/bu;)Lcom/a/b/i/bs;
.end method

.method abstract a(Lcom/a/b/i/bv;)Lcom/a/b/i/bt;
.end method

.method public final a([BI)Ljava/lang/String;
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 166
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    add-int/lit8 v1, p2, 0x0

    array-length v2, p1

    invoke-static {v0, v1, v2}, Lcom/a/b/b/cn;->a(III)V

    .line 168
    invoke-virtual {p0, p2}, Lcom/a/b/i/b;->a(I)I

    move-result v1

    .line 1211
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1212
    new-instance v1, Lcom/a/b/i/br;

    invoke-direct {v1, v2}, Lcom/a/b/i/br;-><init>(Ljava/lang/StringBuilder;)V

    .line 169
    invoke-virtual {p0, v1}, Lcom/a/b/i/b;->a(Lcom/a/b/i/bv;)Lcom/a/b/i/bt;

    move-result-object v2

    .line 171
    :goto_1c
    if-ge v0, p2, :cond_28

    .line 172
    add-int/lit8 v3, v0, 0x0

    :try_start_20
    aget-byte v3, p1, v3

    invoke-interface {v2, v3}, Lcom/a/b/i/bt;->a(B)V

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 174
    :cond_28
    invoke-interface {v2}, Lcom/a/b/i/bt;->b()V
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_2b} :catch_30

    .line 178
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 176
    :catch_30
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "impossible"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method abstract b(I)I
.end method

.method public abstract b()Lcom/a/b/i/b;
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public abstract c()Lcom/a/b/i/b;
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public abstract d()Lcom/a/b/i/b;
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method
