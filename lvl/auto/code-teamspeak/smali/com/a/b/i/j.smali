.class final Lcom/a/b/i/j;
.super Lcom/a/b/i/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/b/i/g;

.field private final b:Ljava/lang/Character;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private transient c:Lcom/a/b/i/b;

.field private transient d:Lcom/a/b/i/b;


# direct methods
.method private constructor <init>(Lcom/a/b/i/g;Ljava/lang/Character;)V
    .registers 7
    .param p2    # Ljava/lang/Character;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 574
    invoke-direct {p0}, Lcom/a/b/i/b;-><init>()V

    .line 575
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/i/g;

    iput-object v0, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    .line 576
    if-eqz p2, :cond_19

    invoke-virtual {p2}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p1, v0}, Lcom/a/b/i/g;->c(C)Z

    move-result v0

    if-nez v0, :cond_26

    :cond_19
    move v0, v2

    :goto_1a
    const-string v3, "Padding character %s was already in alphabet"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v1

    invoke-static {v0, v3, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 578
    iput-object p2, p0, Lcom/a/b/i/j;->b:Ljava/lang/Character;

    .line 579
    return-void

    :cond_26
    move v0, v1

    .line 576
    goto :goto_1a
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V
    .registers 6
    .param p3    # Ljava/lang/Character;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 571
    new-instance v0, Lcom/a/b/i/g;

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/a/b/i/g;-><init>(Ljava/lang/String;[C)V

    invoke-direct {p0, v0, p3}, Lcom/a/b/i/j;-><init>(Lcom/a/b/i/g;Ljava/lang/Character;)V

    .line 572
    return-void
.end method

.method static synthetic a(Lcom/a/b/i/j;)Lcom/a/b/i/g;
    .registers 2

    .prologue
    .line 563
    iget-object v0, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    return-object v0
.end method

.method static synthetic b(Lcom/a/b/i/j;)Ljava/lang/Character;
    .registers 2

    .prologue
    .line 563
    iget-object v0, p0, Lcom/a/b/i/j;->b:Ljava/lang/Character;

    return-object v0
.end method


# virtual methods
.method final a(I)I
    .registers 5

    .prologue
    .line 588
    iget-object v0, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    iget v0, v0, Lcom/a/b/i/g;->w:I

    iget-object v1, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    iget v1, v1, Lcom/a/b/i/g;->x:I

    sget-object v2, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {p1, v1, v2}, Lcom/a/b/j/g;->a(IILjava/math/RoundingMode;)I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method final a()Lcom/a/b/b/m;
    .registers 2

    .prologue
    .line 583
    iget-object v0, p0, Lcom/a/b/i/j;->b:Ljava/lang/Character;

    if-nez v0, :cond_7

    sget-object v0, Lcom/a/b/b/m;->m:Lcom/a/b/b/m;

    :goto_6
    return-object v0

    :cond_7
    iget-object v0, p0, Lcom/a/b/i/j;->b:Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/m;->a(C)Lcom/a/b/b/m;

    move-result-object v0

    goto :goto_6
.end method

.method public final a(C)Lcom/a/b/i/b;
    .registers 5

    .prologue
    .line 700
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    iget v1, v1, Lcom/a/b/i/g;->v:I

    rem-int/2addr v0, v1

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/a/b/i/j;->b:Ljava/lang/Character;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/a/b/i/j;->b:Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-ne v0, p1, :cond_16

    .line 704
    :cond_15
    :goto_15
    return-object p0

    :cond_16
    new-instance v0, Lcom/a/b/i/j;

    iget-object v1, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/i/j;-><init>(Lcom/a/b/i/g;Ljava/lang/Character;)V

    move-object p0, v0

    goto :goto_15
.end method

.method public final a(Ljava/lang/String;I)Lcom/a/b/i/b;
    .registers 5

    .prologue
    .line 710
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 711
    invoke-virtual {p0}, Lcom/a/b/i/j;->a()Lcom/a/b/b/m;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    invoke-virtual {v0, v1}, Lcom/a/b/b/m;->b(Lcom/a/b/b/m;)Lcom/a/b/b/m;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/b/m;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "Separator cannot contain alphabet or padding characters"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 713
    new-instance v0, Lcom/a/b/i/i;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/i/i;-><init>(Lcom/a/b/i/b;Ljava/lang/String;I)V

    return-object v0
.end method

.method final a(Lcom/a/b/i/bu;)Lcom/a/b/i/bs;
    .registers 3

    .prologue
    .line 644
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 645
    new-instance v0, Lcom/a/b/i/l;

    invoke-direct {v0, p0, p1}, Lcom/a/b/i/l;-><init>(Lcom/a/b/i/j;Lcom/a/b/i/bu;)V

    return-object v0
.end method

.method final a(Lcom/a/b/i/bv;)Lcom/a/b/i/bt;
    .registers 3

    .prologue
    .line 593
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 594
    new-instance v0, Lcom/a/b/i/k;

    invoke-direct {v0, p0, p1}, Lcom/a/b/i/k;-><init>(Lcom/a/b/i/j;Lcom/a/b/i/bv;)V

    return-object v0
.end method

.method final b(I)I
    .registers 6

    .prologue
    .line 639
    iget-object v0, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    iget v0, v0, Lcom/a/b/i/g;->v:I

    int-to-long v0, v0

    int-to-long v2, p1

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x7

    add-long/2addr v0, v2

    const-wide/16 v2, 0x8

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final b()Lcom/a/b/i/b;
    .registers 4

    .prologue
    .line 695
    iget-object v0, p0, Lcom/a/b/i/j;->b:Ljava/lang/Character;

    if-nez v0, :cond_5

    :goto_4
    return-object p0

    :cond_5
    new-instance v0, Lcom/a/b/i/j;

    iget-object v1, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/i/j;-><init>(Lcom/a/b/i/g;Ljava/lang/Character;)V

    move-object p0, v0

    goto :goto_4
.end method

.method public final c()Lcom/a/b/i/b;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 721
    iget-object v0, p0, Lcom/a/b/i/j;->c:Lcom/a/b/i/b;

    .line 722
    if-nez v0, :cond_15

    .line 723
    iget-object v2, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    .line 1527
    invoke-virtual {v2}, Lcom/a/b/i/g;->c()Z

    move-result v0

    if-nez v0, :cond_16

    move-object v0, v2

    .line 724
    :goto_e
    iget-object v1, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    if-ne v0, v1, :cond_4d

    move-object v0, p0

    :goto_13
    iput-object v0, p0, Lcom/a/b/i/j;->c:Lcom/a/b/i/b;

    .line 727
    :cond_15
    return-object v0

    .line 1530
    :cond_16
    invoke-virtual {v2}, Lcom/a/b/i/g;->d()Z

    move-result v0

    if-nez v0, :cond_39

    const/4 v0, 0x1

    :goto_1d
    const-string v3, "Cannot call upperCase() on a mixed-case alphabet"

    invoke-static {v0, v3}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 1531
    iget-object v0, v2, Lcom/a/b/i/g;->t:[C

    array-length v0, v0

    new-array v3, v0, [C

    .line 1532
    :goto_27
    iget-object v0, v2, Lcom/a/b/i/g;->t:[C

    array-length v0, v0

    if-ge v1, v0, :cond_3b

    .line 1533
    iget-object v0, v2, Lcom/a/b/i/g;->t:[C

    aget-char v0, v0, v1

    invoke-static {v0}, Lcom/a/b/b/e;->b(C)C

    move-result v0

    aput-char v0, v3, v1

    .line 1532
    add-int/lit8 v1, v1, 0x1

    goto :goto_27

    :cond_39
    move v0, v1

    .line 1530
    goto :goto_1d

    .line 1535
    :cond_3b
    new-instance v0, Lcom/a/b/i/g;

    iget-object v1, v2, Lcom/a/b/i/g;->s:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".upperCase()"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/a/b/i/g;-><init>(Ljava/lang/String;[C)V

    goto :goto_e

    .line 724
    :cond_4d
    new-instance v1, Lcom/a/b/i/j;

    iget-object v2, p0, Lcom/a/b/i/j;->b:Ljava/lang/Character;

    invoke-direct {v1, v0, v2}, Lcom/a/b/i/j;-><init>(Lcom/a/b/i/g;Ljava/lang/Character;)V

    move-object v0, v1

    goto :goto_13
.end method

.method public final d()Lcom/a/b/i/b;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 732
    iget-object v0, p0, Lcom/a/b/i/j;->d:Lcom/a/b/i/b;

    .line 733
    if-nez v0, :cond_15

    .line 734
    iget-object v2, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    .line 1540
    invoke-virtual {v2}, Lcom/a/b/i/g;->d()Z

    move-result v0

    if-nez v0, :cond_16

    move-object v0, v2

    .line 735
    :goto_e
    iget-object v1, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    if-ne v0, v1, :cond_4d

    move-object v0, p0

    :goto_13
    iput-object v0, p0, Lcom/a/b/i/j;->d:Lcom/a/b/i/b;

    .line 738
    :cond_15
    return-object v0

    .line 1543
    :cond_16
    invoke-virtual {v2}, Lcom/a/b/i/g;->c()Z

    move-result v0

    if-nez v0, :cond_39

    const/4 v0, 0x1

    :goto_1d
    const-string v3, "Cannot call lowerCase() on a mixed-case alphabet"

    invoke-static {v0, v3}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 1544
    iget-object v0, v2, Lcom/a/b/i/g;->t:[C

    array-length v0, v0

    new-array v3, v0, [C

    .line 1545
    :goto_27
    iget-object v0, v2, Lcom/a/b/i/g;->t:[C

    array-length v0, v0

    if-ge v1, v0, :cond_3b

    .line 1546
    iget-object v0, v2, Lcom/a/b/i/g;->t:[C

    aget-char v0, v0, v1

    invoke-static {v0}, Lcom/a/b/b/e;->a(C)C

    move-result v0

    aput-char v0, v3, v1

    .line 1545
    add-int/lit8 v1, v1, 0x1

    goto :goto_27

    :cond_39
    move v0, v1

    .line 1543
    goto :goto_1d

    .line 1548
    :cond_3b
    new-instance v0, Lcom/a/b/i/g;

    iget-object v1, v2, Lcom/a/b/i/g;->s:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".lowerCase()"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/a/b/i/g;-><init>(Ljava/lang/String;[C)V

    goto :goto_e

    .line 735
    :cond_4d
    new-instance v1, Lcom/a/b/i/j;

    iget-object v2, p0, Lcom/a/b/i/j;->b:Ljava/lang/Character;

    invoke-direct {v1, v0, v2}, Lcom/a/b/i/j;-><init>(Lcom/a/b/i/g;Ljava/lang/Character;)V

    move-object v0, v1

    goto :goto_13
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 743
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BaseEncoding."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 744
    iget-object v1, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    invoke-virtual {v1}, Lcom/a/b/i/g;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 745
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/a/b/i/j;->a:Lcom/a/b/i/g;

    iget v2, v2, Lcom/a/b/i/g;->v:I

    rem-int/2addr v1, v2

    if-eqz v1, :cond_22

    .line 746
    iget-object v1, p0, Lcom/a/b/i/j;->b:Ljava/lang/Character;

    if-nez v1, :cond_27

    .line 747
    const-string v1, ".omitPadding()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 752
    :cond_22
    :goto_22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 749
    :cond_27
    const-string v1, ".withPadChar("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/i/j;->b:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_22
.end method
