.class final Lcom/a/b/i/cd;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/Iterator;

.field private b:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/util/Iterator;)V
    .registers 3

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 46
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    iput-object v0, p0, Lcom/a/b/i/cd;->a:Ljava/util/Iterator;

    .line 47
    invoke-direct {p0}, Lcom/a/b/i/cd;->a()V

    .line 48
    return-void
.end method

.method private a()V
    .registers 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/a/b/i/cd;->close()V

    .line 65
    iget-object v0, p0, Lcom/a/b/i/cd;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 66
    iget-object v0, p0, Lcom/a/b/i/cd;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/i/s;

    invoke-virtual {v0}, Lcom/a/b/i/s;->a()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    .line 68
    :cond_19
    return-void
.end method


# virtual methods
.method public final available()I
    .registers 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    if-nez v0, :cond_6

    .line 72
    const/4 v0, 0x0

    .line 74
    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    goto :goto_5
.end method

.method public final close()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 51
    iget-object v0, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    if-eqz v0, :cond_c

    .line 53
    :try_start_5
    iget-object v0, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_d

    .line 55
    iput-object v1, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    .line 58
    :cond_c
    return-void

    .line 55
    :catchall_d
    move-exception v0

    iput-object v1, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    throw v0
.end method

.method public final markSupported()Z
    .registers 2

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .registers 3

    .prologue
    const/4 v0, -0x1

    .line 82
    :goto_1
    iget-object v1, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    if-nez v1, :cond_6

    .line 90
    :goto_5
    return v0

    .line 85
    :cond_6
    iget-object v1, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 86
    if-ne v1, v0, :cond_12

    .line 87
    invoke-direct {p0}, Lcom/a/b/i/cd;->a()V

    goto :goto_1

    :cond_12
    move v0, v1

    .line 90
    goto :goto_5
.end method

.method public final read([BII)I
    .registers 6
    .param p1    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, -0x1

    .line 94
    :goto_1
    iget-object v1, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    if-nez v1, :cond_6

    .line 102
    :goto_5
    return v0

    .line 97
    :cond_6
    iget-object v1, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 98
    if-ne v1, v0, :cond_12

    .line 99
    invoke-direct {p0}, Lcom/a/b/i/cd;->a()V

    goto :goto_1

    :cond_12
    move v0, v1

    .line 102
    goto :goto_5
.end method

.method public final skip(J)J
    .registers 12

    .prologue
    const-wide/16 v6, 0x1

    const-wide/16 v2, 0x0

    .line 106
    iget-object v0, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    if-eqz v0, :cond_c

    cmp-long v0, p1, v2

    if-gtz v0, :cond_e

    :cond_c
    move-wide v0, v2

    .line 116
    :cond_d
    :goto_d
    return-wide v0

    .line 109
    :cond_e
    iget-object v0, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 110
    cmp-long v4, v0, v2

    if-nez v4, :cond_d

    .line 113
    invoke-virtual {p0}, Lcom/a/b/i/cd;->read()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_21

    move-wide v0, v2

    .line 114
    goto :goto_d

    .line 116
    :cond_21
    iget-object v0, p0, Lcom/a/b/i/cd;->b:Ljava/io/InputStream;

    sub-long v2, p1, v6

    invoke-virtual {v0, v2, v3}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    add-long/2addr v0, v6

    goto :goto_d
.end method
