.class public final Lcom/a/b/g/bd;
.super Ljava/io/FilterInputStream;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private final a:Lcom/a/b/g/al;


# direct methods
.method private constructor <init>(Lcom/a/b/g/ak;Ljava/io/InputStream;)V
    .registers 4

    .prologue
    .line 42
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    invoke-direct {p0, v0}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 43
    invoke-interface {p1}, Lcom/a/b/g/ak;->a()Lcom/a/b/g/al;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/al;

    iput-object v0, p0, Lcom/a/b/g/bd;->a:Lcom/a/b/g/al;

    .line 44
    return-void
.end method

.method private a()Lcom/a/b/g/ag;
    .registers 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/a/b/g/bd;->a:Lcom/a/b/g/al;

    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final mark(I)V
    .registers 2

    .prologue
    .line 85
    return-void
.end method

.method public final markSupported()Z
    .registers 2

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .registers 4

    .prologue
    .line 52
    iget-object v0, p0, Lcom/a/b/g/bd;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 53
    const/4 v1, -0x1

    if-eq v0, v1, :cond_f

    .line 54
    iget-object v1, p0, Lcom/a/b/g/bd;->a:Lcom/a/b/g/al;

    int-to-byte v2, v0

    invoke-interface {v1, v2}, Lcom/a/b/g/al;->b(B)Lcom/a/b/g/al;

    .line 56
    :cond_f
    return v0
.end method

.method public final read([BII)I
    .registers 6

    .prologue
    .line 65
    iget-object v0, p0, Lcom/a/b/g/bd;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 66
    const/4 v1, -0x1

    if-eq v0, v1, :cond_e

    .line 67
    iget-object v1, p0, Lcom/a/b/g/bd;->a:Lcom/a/b/g/al;

    invoke-interface {v1, p1, p2, v0}, Lcom/a/b/g/al;->b([BII)Lcom/a/b/g/al;

    .line 69
    :cond_e
    return v0
.end method

.method public final reset()V
    .registers 3

    .prologue
    .line 93
    new-instance v0, Ljava/io/IOException;

    const-string v1, "reset not supported"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
