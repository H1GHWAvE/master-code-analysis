.class public abstract Lcom/a/b/g/i;
.super Lcom/a/b/g/d;
.source "SourceFile"


# instance fields
.field private final a:Ljava/nio/ByteBuffer;

.field private final b:I

.field private final c:I


# direct methods
.method protected constructor <init>(I)V
    .registers 2

    .prologue
    .line 95
    invoke-direct {p0, p1, p1}, Lcom/a/b/g/i;-><init>(II)V

    .line 96
    return-void
.end method

.method private constructor <init>(II)V
    .registers 5

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/a/b/g/d;-><init>()V

    .line 109
    rem-int v0, p2, p1

    if-nez v0, :cond_1e

    const/4 v0, 0x1

    :goto_8
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 112
    add-int/lit8 v0, p2, 0x7

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    .line 115
    iput p2, p0, Lcom/a/b/g/i;->b:I

    .line 116
    iput p1, p0, Lcom/a/b/g/i;->c:I

    .line 117
    return-void

    .line 109
    :cond_1e
    const/4 v0, 0x0

    goto :goto_8
.end method

.method private c(Ljava/nio/ByteBuffer;)Lcom/a/b/g/al;
    .registers 6

    .prologue
    .line 155
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-gt v0, v1, :cond_15

    .line 156
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 157
    invoke-direct {p0}, Lcom/a/b/g/i;->c()V

    .line 175
    :goto_14
    return-object p0

    .line 162
    :cond_15
    iget v0, p0, Lcom/a/b/g/i;->b:I

    iget-object v1, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    sub-int v1, v0, v1

    .line 163
    const/4 v0, 0x0

    :goto_20
    if-ge v0, v1, :cond_2e

    .line 164
    iget-object v2, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 163
    add-int/lit8 v0, v0, 0x1

    goto :goto_20

    .line 166
    :cond_2e
    invoke-direct {p0}, Lcom/a/b/g/i;->d()V

    .line 169
    :goto_31
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iget v1, p0, Lcom/a/b/g/i;->c:I

    if-lt v0, v1, :cond_3d

    .line 170
    invoke-virtual {p0, p1}, Lcom/a/b/g/i;->a(Ljava/nio/ByteBuffer;)V

    goto :goto_31

    .line 174
    :cond_3d
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    goto :goto_14
.end method

.method private c()V
    .registers 3

    .prologue
    .line 241
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_d

    .line 243
    invoke-direct {p0}, Lcom/a/b/g/i;->d()V

    .line 245
    :cond_d
    return-void
.end method

.method private d()V
    .registers 3

    .prologue
    .line 248
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 249
    :goto_5
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iget v1, p0, Lcom/a/b/g/i;->c:I

    if-lt v0, v1, :cond_15

    .line 252
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0}, Lcom/a/b/g/i;->a(Ljava/nio/ByteBuffer;)V

    goto :goto_5

    .line 254
    :cond_15
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    .line 255
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/g/ag;
    .registers 2

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/a/b/g/i;->d()V

    .line 230
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 231
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-lez v0, :cond_15

    .line 232
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0}, Lcom/a/b/g/i;->b(Ljava/nio/ByteBuffer;)V

    .line 234
    :cond_15
    invoke-virtual {p0}, Lcom/a/b/g/i;->b()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a(C)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 202
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putChar(C)Ljava/nio/ByteBuffer;

    .line 203
    invoke-direct {p0}, Lcom/a/b/g/i;->c()V

    .line 204
    return-object p0
.end method

.method public final a(I)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 209
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 210
    invoke-direct {p0}, Lcom/a/b/g/i;->c()V

    .line 211
    return-object p0
.end method

.method public final a(J)Lcom/a/b/g/al;
    .registers 4

    .prologue
    .line 216
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 217
    invoke-direct {p0}, Lcom/a/b/g/i;->c()V

    .line 218
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/a/b/g/al;
    .registers 4

    .prologue
    .line 180
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_11

    .line 181
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Lcom/a/b/g/i;->a(C)Lcom/a/b/g/al;

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 183
    :cond_11
    return-object p0
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 223
    invoke-interface {p2, p1, p0}, Lcom/a/b/g/w;->a(Ljava/lang/Object;Lcom/a/b/g/bn;)V

    .line 224
    return-object p0
.end method

.method public final a(S)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 195
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 196
    invoke-direct {p0}, Lcom/a/b/g/i;->c()V

    .line 197
    return-object p0
.end method

.method protected abstract a(Ljava/nio/ByteBuffer;)V
.end method

.method abstract b()Lcom/a/b/g/ag;
.end method

.method public final b(B)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 189
    invoke-direct {p0}, Lcom/a/b/g/i;->c()V

    .line 190
    return-object p0
.end method

.method public final b([B)Lcom/a/b/g/al;
    .registers 4

    .prologue
    .line 145
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/a/b/g/i;->b([BII)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final b([BII)Lcom/a/b/g/al;
    .registers 9

    .prologue
    .line 150
    invoke-static {p1, p2, p3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1155
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iget-object v2, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    if-gt v0, v2, :cond_1f

    .line 1156
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 1157
    invoke-direct {p0}, Lcom/a/b/g/i;->c()V

    .line 1158
    :goto_1e
    return-object p0

    .line 1162
    :cond_1f
    iget v0, p0, Lcom/a/b/g/i;->b:I

    iget-object v2, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    sub-int v2, v0, v2

    .line 1163
    const/4 v0, 0x0

    :goto_2a
    if-ge v0, v2, :cond_38

    .line 1164
    iget-object v3, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1163
    add-int/lit8 v0, v0, 0x1

    goto :goto_2a

    .line 1166
    :cond_38
    invoke-direct {p0}, Lcom/a/b/g/i;->d()V

    .line 1169
    :goto_3b
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iget v2, p0, Lcom/a/b/g/i;->c:I

    if-lt v0, v2, :cond_47

    .line 1170
    invoke-virtual {p0, v1}, Lcom/a/b/g/i;->a(Ljava/nio/ByteBuffer;)V

    goto :goto_3b

    .line 1174
    :cond_47
    iget-object v0, p0, Lcom/a/b/g/i;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    goto :goto_1e
.end method

.method public final synthetic b(C)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/a/b/g/i;->a(C)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(I)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/a/b/g/i;->a(I)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(J)Lcom/a/b/g/bn;
    .registers 4

    .prologue
    .line 77
    invoke-virtual {p0, p1, p2}, Lcom/a/b/g/i;->a(J)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/CharSequence;)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/a/b/g/i;->a(Ljava/lang/CharSequence;)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(S)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/a/b/g/i;->a(S)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method protected b(Ljava/nio/ByteBuffer;)V
    .registers 4

    .prologue
    .line 133
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 134
    iget v0, p0, Lcom/a/b/g/i;->c:I

    add-int/lit8 v0, v0, 0x7

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 135
    :goto_e
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iget v1, p0, Lcom/a/b/g/i;->c:I

    if-ge v0, v1, :cond_1c

    .line 136
    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto :goto_e

    .line 138
    :cond_1c
    iget v0, p0, Lcom/a/b/g/i;->c:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 139
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 140
    invoke-virtual {p0, p1}, Lcom/a/b/g/i;->a(Ljava/nio/ByteBuffer;)V

    .line 141
    return-void
.end method

.method public final synthetic c(B)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/a/b/g/i;->b(B)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c([B)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/a/b/g/i;->b([B)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c([BII)Lcom/a/b/g/bn;
    .registers 5

    .prologue
    .line 77
    invoke-virtual {p0, p1, p2, p3}, Lcom/a/b/g/i;->b([BII)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method
