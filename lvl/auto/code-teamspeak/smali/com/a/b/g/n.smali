.class abstract enum Lcom/a/b/g/n;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/g/m;


# static fields
.field public static final enum a:Lcom/a/b/g/n;

.field public static final enum b:Lcom/a/b/g/n;

.field private static final synthetic c:[Lcom/a/b/g/n;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 44
    new-instance v0, Lcom/a/b/g/o;

    const-string v1, "MURMUR128_MITZ_32"

    invoke-direct {v0, v1}, Lcom/a/b/g/o;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/g/n;->a:Lcom/a/b/g/n;

    .line 90
    new-instance v0, Lcom/a/b/g/p;

    const-string v1, "MURMUR128_MITZ_64"

    invoke-direct {v0, v1}, Lcom/a/b/g/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/g/n;->b:Lcom/a/b/g/n;

    .line 38
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/b/g/n;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/g/n;->a:Lcom/a/b/g/n;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/g/n;->b:Lcom/a/b/g/n;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/g/n;->c:[Lcom/a/b/g/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 140
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/a/b/g/n;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/g/n;
    .registers 2

    .prologue
    .line 38
    const-class v0, Lcom/a/b/g/n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/n;

    return-object v0
.end method

.method public static values()[Lcom/a/b/g/n;
    .registers 1

    .prologue
    .line 38
    sget-object v0, Lcom/a/b/g/n;->c:[Lcom/a/b/g/n;

    invoke-virtual {v0}, [Lcom/a/b/g/n;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/g/n;

    return-object v0
.end method
