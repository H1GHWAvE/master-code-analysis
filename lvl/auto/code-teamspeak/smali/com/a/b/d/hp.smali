.class public abstract Lcom/a/b/d/hp;
.super Lcom/a/b/d/hi;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedSet;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/a/b/d/hi;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/a/b/d/hp;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 99
    if-nez v0, :cond_d

    check-cast p1, Ljava/lang/Comparable;

    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    :goto_c
    return v0

    :cond_d
    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_c
.end method


# virtual methods
.method protected synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/a/b/d/hp;->c()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 164
    invoke-virtual {p0, p1}, Lcom/a/b/d/hp;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/a/b/d/hp;->c()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 115
    .line 116
    :try_start_1
    invoke-interface {p0, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v1

    .line 117
    invoke-direct {p0, v1, p1}, Lcom/a/b/d/hp;->b(Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_c
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_c} :catch_15
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_c} :catch_13
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_c} :catch_11

    move-result v1

    if-nez v1, :cond_10

    const/4 v0, 0x1

    .line 123
    :cond_10
    :goto_10
    return v0

    :catch_11
    move-exception v1

    goto :goto_10

    .line 121
    :catch_13
    move-exception v1

    goto :goto_10

    .line 119
    :catch_15
    move-exception v1

    goto :goto_10
.end method

.method protected abstract c()Ljava/util/SortedSet;
.end method

.method protected final c(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 138
    .line 139
    :try_start_1
    invoke-interface {p0, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 140
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 141
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 142
    invoke-direct {p0, v2, p1}, Lcom/a/b/d/hp;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-nez v2, :cond_1d

    .line 143
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_1c
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1c} :catch_20
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1c} :catch_1e

    .line 144
    const/4 v0, 0x1

    .line 152
    :cond_1d
    :goto_1d
    return v0

    .line 150
    :catch_1e
    move-exception v1

    goto :goto_1d

    .line 148
    :catch_20
    move-exception v1

    goto :goto_1d
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/a/b/d/hp;->c()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/a/b/d/hp;->c()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/a/b/d/hp;->c()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/a/b/d/hp;->c()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/a/b/d/hp;->c()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/a/b/d/hp;->c()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/a/b/d/hp;->c()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method
