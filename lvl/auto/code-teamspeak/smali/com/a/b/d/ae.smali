.class final Lcom/a/b/d/ae;
.super Lcom/a/b/d/ac;
.source "SourceFile"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field final synthetic d:Lcom/a/b/d/ad;


# direct methods
.method constructor <init>(Lcom/a/b/d/ad;)V
    .registers 2

    .prologue
    .line 852
    iput-object p1, p0, Lcom/a/b/d/ae;->d:Lcom/a/b/d/ad;

    invoke-direct {p0, p1}, Lcom/a/b/d/ac;-><init>(Lcom/a/b/d/ab;)V

    return-void
.end method

.method public constructor <init>(Lcom/a/b/d/ad;I)V
    .registers 4

    .prologue
    .line 854
    iput-object p1, p0, Lcom/a/b/d/ae;->d:Lcom/a/b/d/ad;

    .line 2445
    iget-object v0, p1, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    .line 1765
    check-cast v0, Ljava/util/List;

    .line 855
    invoke-interface {v0, p2}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/ac;-><init>(Lcom/a/b/d/ab;Ljava/util/Iterator;)V

    .line 856
    return-void
.end method

.method private b()Ljava/util/ListIterator;
    .registers 2

    .prologue
    .line 859
    .line 2497
    invoke-virtual {p0}, Lcom/a/b/d/ac;->a()V

    .line 2498
    iget-object v0, p0, Lcom/a/b/d/ac;->a:Ljava/util/Iterator;

    .line 859
    check-cast v0, Ljava/util/ListIterator;

    return-object v0
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 889
    iget-object v0, p0, Lcom/a/b/d/ae;->d:Lcom/a/b/d/ad;

    invoke-virtual {v0}, Lcom/a/b/d/ad;->isEmpty()Z

    move-result v0

    .line 890
    invoke-direct {p0}, Lcom/a/b/d/ae;->b()Ljava/util/ListIterator;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 891
    iget-object v1, p0, Lcom/a/b/d/ae;->d:Lcom/a/b/d/ad;

    iget-object v1, v1, Lcom/a/b/d/ad;->g:Lcom/a/b/d/n;

    invoke-static {v1}, Lcom/a/b/d/n;->c(Lcom/a/b/d/n;)I

    .line 892
    if-eqz v0, :cond_1b

    .line 893
    iget-object v0, p0, Lcom/a/b/d/ae;->d:Lcom/a/b/d/ad;

    invoke-virtual {v0}, Lcom/a/b/d/ad;->c()V

    .line 895
    :cond_1b
    return-void
.end method

.method public final hasPrevious()Z
    .registers 2

    .prologue
    .line 864
    invoke-direct {p0}, Lcom/a/b/d/ae;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public final nextIndex()I
    .registers 2

    .prologue
    .line 874
    invoke-direct {p0}, Lcom/a/b/d/ae;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 869
    invoke-direct {p0}, Lcom/a/b/d/ae;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .registers 2

    .prologue
    .line 879
    invoke-direct {p0}, Lcom/a/b/d/ae;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previousIndex()I

    move-result v0

    return v0
.end method

.method public final set(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 884
    invoke-direct {p0}, Lcom/a/b/d/ae;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 885
    return-void
.end method
