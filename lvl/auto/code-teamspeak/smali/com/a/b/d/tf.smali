.class final Lcom/a/b/d/tf;
.super Lcom/a/b/d/hp;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/SortedSet;


# direct methods
.method constructor <init>(Ljava/util/SortedSet;)V
    .registers 2

    .prologue
    .line 989
    iput-object p1, p0, Lcom/a/b/d/tf;->a:Ljava/util/SortedSet;

    invoke-direct {p0}, Lcom/a/b/d/hp;-><init>()V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 989
    .line 1992
    iget-object v0, p0, Lcom/a/b/d/tf;->a:Ljava/util/SortedSet;

    .line 989
    return-object v0
.end method

.method public final add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 997
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 1002
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 989
    .line 2992
    iget-object v0, p0, Lcom/a/b/d/tf;->a:Ljava/util/SortedSet;

    .line 989
    return-object v0
.end method

.method protected final c()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 992
    iget-object v0, p0, Lcom/a/b/d/tf;->a:Ljava/util/SortedSet;

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 1007
    invoke-super {p0, p1}, Lcom/a/b/d/hp;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 989
    .line 3992
    iget-object v0, p0, Lcom/a/b/d/tf;->a:Ljava/util/SortedSet;

    .line 989
    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 1012
    invoke-super {p0, p1, p2}, Lcom/a/b/d/hp;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 1017
    invoke-super {p0, p1}, Lcom/a/b/d/hp;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method
