.class final Lcom/a/b/d/do;
.super Lcom/a/b/d/gh;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Collection;

.field private final b:Lcom/a/b/d/dm;


# direct methods
.method public constructor <init>(Ljava/util/Collection;Lcom/a/b/d/dm;)V
    .registers 4

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/a/b/d/gh;-><init>()V

    .line 64
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iput-object v0, p0, Lcom/a/b/d/do;->a:Ljava/util/Collection;

    .line 65
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dm;

    iput-object v0, p0, Lcom/a/b/d/do;->b:Lcom/a/b/d/dm;

    .line 66
    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/a/b/d/do;->b:Lcom/a/b/d/dm;

    invoke-interface {v0, p1}, Lcom/a/b/d/dm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lcom/a/b/d/do;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 75
    iget-object v0, p0, Lcom/a/b/d/do;->a:Ljava/util/Collection;

    iget-object v1, p0, Lcom/a/b/d/do;->b:Lcom/a/b/d/dm;

    invoke-static {p1, v1}, Lcom/a/b/d/dn;->b(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method protected final b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/a/b/d/do;->a:Ljava/util/Collection;

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 58
    .line 1068
    iget-object v0, p0, Lcom/a/b/d/do;->a:Ljava/util/Collection;

    .line 58
    return-object v0
.end method
