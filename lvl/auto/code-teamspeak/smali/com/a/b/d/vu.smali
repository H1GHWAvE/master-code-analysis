.class public abstract Lcom/a/b/d/vu;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I = 0x2


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Class;)Lcom/a/b/d/wb;
    .registers 3

    .prologue
    .line 406
    const-string v0, "valueClass"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    new-instance v0, Lcom/a/b/d/wa;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/wa;-><init>(Lcom/a/b/d/vu;Ljava/lang/Class;)V

    return-object v0
.end method

.method private a(Ljava/util/Comparator;)Lcom/a/b/d/wc;
    .registers 3

    .prologue
    .line 390
    const-string v0, "comparator"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    new-instance v0, Lcom/a/b/d/vz;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/vz;-><init>(Lcom/a/b/d/vu;Ljava/util/Comparator;)V

    return-object v0
.end method

.method private b()Lcom/a/b/d/vt;
    .registers 3

    .prologue
    .line 290
    .line 1300
    const/4 v0, 0x2

    const-string v1, "expectedValuesPerKey"

    invoke-static {v0, v1}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 1301
    new-instance v0, Lcom/a/b/d/vv;

    invoke-direct {v0, p0}, Lcom/a/b/d/vv;-><init>(Lcom/a/b/d/vu;)V

    .line 290
    return-object v0
.end method

.method private c()Lcom/a/b/d/vt;
    .registers 3

    .prologue
    .line 300
    const/4 v0, 0x2

    const-string v1, "expectedValuesPerKey"

    invoke-static {v0, v1}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 301
    new-instance v0, Lcom/a/b/d/vv;

    invoke-direct {v0, p0}, Lcom/a/b/d/vv;-><init>(Lcom/a/b/d/vu;)V

    return-object v0
.end method

.method private d()Lcom/a/b/d/vt;
    .registers 2

    .prologue
    .line 315
    new-instance v0, Lcom/a/b/d/vw;

    invoke-direct {v0, p0}, Lcom/a/b/d/vw;-><init>(Lcom/a/b/d/vu;)V

    return-object v0
.end method

.method private e()Lcom/a/b/d/wb;
    .registers 3

    .prologue
    .line 329
    .line 1339
    const/4 v0, 0x2

    const-string v1, "expectedValuesPerKey"

    invoke-static {v0, v1}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 1340
    new-instance v0, Lcom/a/b/d/vx;

    invoke-direct {v0, p0}, Lcom/a/b/d/vx;-><init>(Lcom/a/b/d/vu;)V

    .line 329
    return-object v0
.end method

.method private f()Lcom/a/b/d/wb;
    .registers 3

    .prologue
    .line 339
    const/4 v0, 0x2

    const-string v1, "expectedValuesPerKey"

    invoke-static {v0, v1}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 340
    new-instance v0, Lcom/a/b/d/vx;

    invoke-direct {v0, p0}, Lcom/a/b/d/vx;-><init>(Lcom/a/b/d/vu;)V

    return-object v0
.end method

.method private g()Lcom/a/b/d/wb;
    .registers 3

    .prologue
    .line 354
    .line 1364
    const/4 v0, 0x2

    const-string v1, "expectedValuesPerKey"

    invoke-static {v0, v1}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 1365
    new-instance v0, Lcom/a/b/d/vy;

    invoke-direct {v0, p0}, Lcom/a/b/d/vy;-><init>(Lcom/a/b/d/vu;)V

    .line 354
    return-object v0
.end method

.method private h()Lcom/a/b/d/wb;
    .registers 3

    .prologue
    .line 364
    const/4 v0, 0x2

    const-string v1, "expectedValuesPerKey"

    invoke-static {v0, v1}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 365
    new-instance v0, Lcom/a/b/d/vy;

    invoke-direct {v0, p0}, Lcom/a/b/d/vy;-><init>(Lcom/a/b/d/vu;)V

    return-object v0
.end method

.method private i()Lcom/a/b/d/wc;
    .registers 3

    .prologue
    .line 380
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    .line 1390
    const-string v1, "comparator"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1391
    new-instance v1, Lcom/a/b/d/vz;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/vz;-><init>(Lcom/a/b/d/vu;Ljava/util/Comparator;)V

    .line 380
    return-object v1
.end method


# virtual methods
.method abstract a()Ljava/util/Map;
.end method
