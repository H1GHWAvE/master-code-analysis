.class Lcom/a/b/d/da;
.super Lcom/a/b/d/qy;
.source "SourceFile"


# static fields
.field private static final C:J = 0x4L


# instance fields
.field final a:Lcom/a/b/b/bj;


# direct methods
.method constructor <init>(Lcom/a/b/d/ql;Lcom/a/b/b/bj;)V
    .registers 4

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/a/b/d/qy;-><init>(Lcom/a/b/d/ql;)V

    .line 52
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/bj;

    iput-object v0, p0, Lcom/a/b/d/da;->a:Lcom/a/b/b/bj;

    .line 53
    return-void
.end method

.method private b(I)Lcom/a/b/d/dd;
    .registers 3

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dd;

    return-object v0
.end method


# virtual methods
.method final synthetic a(I)Lcom/a/b/d/sa;
    .registers 3

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/a/b/d/da;->b(I)Lcom/a/b/d/dd;

    move-result-object v0

    return-object v0
.end method

.method final a(II)Lcom/a/b/d/sa;
    .registers 4

    .prologue
    .line 57
    new-instance v0, Lcom/a/b/d/dd;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/d/dd;-><init>(Lcom/a/b/d/qy;II)V

    return-object v0
.end method

.method final a()Ljava/lang/Object;
    .registers 16

    .prologue
    .line 382
    new-instance v1, Lcom/a/b/d/de;

    iget-object v2, p0, Lcom/a/b/d/da;->o:Lcom/a/b/d/sh;

    iget-object v3, p0, Lcom/a/b/d/da;->p:Lcom/a/b/d/sh;

    iget-object v4, p0, Lcom/a/b/d/da;->m:Lcom/a/b/b/au;

    iget-object v5, p0, Lcom/a/b/d/da;->n:Lcom/a/b/b/au;

    iget-wide v6, p0, Lcom/a/b/d/da;->s:J

    iget-wide v8, p0, Lcom/a/b/d/da;->r:J

    iget v10, p0, Lcom/a/b/d/da;->q:I

    iget v11, p0, Lcom/a/b/d/da;->l:I

    iget-object v12, p0, Lcom/a/b/d/da;->u:Lcom/a/b/d/qw;

    iget-object v14, p0, Lcom/a/b/d/da;->a:Lcom/a/b/b/bj;

    move-object v13, p0

    invoke-direct/range {v1 .. v14}, Lcom/a/b/d/de;-><init>(Lcom/a/b/d/sh;Lcom/a/b/d/sh;Lcom/a/b/b/au;Lcom/a/b/b/au;JJIILcom/a/b/d/qw;Ljava/util/concurrent/ConcurrentMap;Lcom/a/b/b/bj;)V

    return-object v1
.end method

.method final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 66
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/da;->b(Ljava/lang/Object;)I

    move-result v0

    .line 67
    invoke-direct {p0, v0}, Lcom/a/b/d/da;->b(I)Lcom/a/b/d/dd;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/da;->a:Lcom/a/b/b/bj;

    invoke-virtual {v1, p1, v0, v2}, Lcom/a/b/d/dd;->a(Ljava/lang/Object;ILcom/a/b/b/bj;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
