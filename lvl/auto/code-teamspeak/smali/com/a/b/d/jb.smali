.class public abstract Lcom/a/b/d/jb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final c:I = 0x4


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    return-void
.end method

.method static a(II)I
    .registers 4

    .prologue
    .line 219
    if-gez p1, :cond_a

    .line 220
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "cannot store more than MAX_VALUE elements"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 223
    :cond_a
    shr-int/lit8 v0, p0, 0x1

    add-int/2addr v0, p0

    add-int/lit8 v0, v0, 0x1

    .line 224
    if-ge v0, p1, :cond_19

    .line 225
    add-int/lit8 v0, p1, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    .line 227
    :cond_19
    if-gez v0, :cond_1e

    .line 228
    const v0, 0x7fffffff

    .line 231
    :cond_1e
    return v0
.end method


# virtual methods
.method public abstract a()Lcom/a/b/d/iz;
.end method

.method public a(Ljava/lang/Iterable;)Lcom/a/b/d/jb;
    .registers 4

    .prologue
    .line 281
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 282
    invoke-virtual {p0, v1}, Lcom/a/b/d/jb;->b(Ljava/lang/Object;)Lcom/a/b/d/jb;

    goto :goto_4

    .line 284
    :cond_12
    return-object p0
.end method

.method public a(Ljava/util/Iterator;)Lcom/a/b/d/jb;
    .registers 3

    .prologue
    .line 300
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 301
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/jb;->b(Ljava/lang/Object;)Lcom/a/b/d/jb;

    goto :goto_0

    .line 303
    :cond_e
    return-object p0
.end method

.method public varargs a([Ljava/lang/Object;)Lcom/a/b/d/jb;
    .registers 5

    .prologue
    .line 262
    array-length v1, p1

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_c

    aget-object v2, p1, v0

    .line 263
    invoke-virtual {p0, v2}, Lcom/a/b/d/jb;->b(Ljava/lang/Object;)Lcom/a/b/d/jb;

    .line 262
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 265
    :cond_c
    return-object p0
.end method

.method public abstract b(Ljava/lang/Object;)Lcom/a/b/d/jb;
.end method
