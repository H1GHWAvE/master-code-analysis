.class abstract enum Lcom/a/b/d/re;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/a/b/d/re;

.field public static final enum b:Lcom/a/b/d/re;

.field public static final enum c:Lcom/a/b/d/re;

.field public static final enum d:Lcom/a/b/d/re;

.field public static final enum e:Lcom/a/b/d/re;

.field public static final enum f:Lcom/a/b/d/re;

.field public static final enum g:Lcom/a/b/d/re;

.field public static final enum h:Lcom/a/b/d/re;

.field static final i:I = 0x1

.field static final j:I = 0x2

.field static final k:[[Lcom/a/b/d/re;

.field private static final synthetic l:[Lcom/a/b/d/re;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 352
    new-instance v0, Lcom/a/b/d/rf;

    const-string v1, "STRONG"

    invoke-direct {v0, v1}, Lcom/a/b/d/rf;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/re;->a:Lcom/a/b/d/re;

    .line 359
    new-instance v0, Lcom/a/b/d/rg;

    const-string v1, "STRONG_EXPIRABLE"

    invoke-direct {v0, v1}, Lcom/a/b/d/rg;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/re;->b:Lcom/a/b/d/re;

    .line 374
    new-instance v0, Lcom/a/b/d/rh;

    const-string v1, "STRONG_EVICTABLE"

    invoke-direct {v0, v1}, Lcom/a/b/d/rh;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/re;->c:Lcom/a/b/d/re;

    .line 389
    new-instance v0, Lcom/a/b/d/ri;

    const-string v1, "STRONG_EXPIRABLE_EVICTABLE"

    invoke-direct {v0, v1}, Lcom/a/b/d/ri;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/re;->d:Lcom/a/b/d/re;

    .line 406
    new-instance v0, Lcom/a/b/d/rj;

    const-string v1, "WEAK"

    invoke-direct {v0, v1}, Lcom/a/b/d/rj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/re;->e:Lcom/a/b/d/re;

    .line 413
    new-instance v0, Lcom/a/b/d/rk;

    const-string v1, "WEAK_EXPIRABLE"

    invoke-direct {v0, v1}, Lcom/a/b/d/rk;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/re;->f:Lcom/a/b/d/re;

    .line 428
    new-instance v0, Lcom/a/b/d/rl;

    const-string v1, "WEAK_EVICTABLE"

    invoke-direct {v0, v1}, Lcom/a/b/d/rl;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/re;->g:Lcom/a/b/d/re;

    .line 443
    new-instance v0, Lcom/a/b/d/rm;

    const-string v1, "WEAK_EXPIRABLE_EVICTABLE"

    invoke-direct {v0, v1}, Lcom/a/b/d/rm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/re;->h:Lcom/a/b/d/re;

    .line 351
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/a/b/d/re;

    sget-object v1, Lcom/a/b/d/re;->a:Lcom/a/b/d/re;

    aput-object v1, v0, v3

    sget-object v1, Lcom/a/b/d/re;->b:Lcom/a/b/d/re;

    aput-object v1, v0, v4

    sget-object v1, Lcom/a/b/d/re;->c:Lcom/a/b/d/re;

    aput-object v1, v0, v5

    sget-object v1, Lcom/a/b/d/re;->d:Lcom/a/b/d/re;

    aput-object v1, v0, v6

    sget-object v1, Lcom/a/b/d/re;->e:Lcom/a/b/d/re;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/a/b/d/re;->f:Lcom/a/b/d/re;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/a/b/d/re;->g:Lcom/a/b/d/re;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/a/b/d/re;->h:Lcom/a/b/d/re;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/d/re;->l:[Lcom/a/b/d/re;

    .line 470
    new-array v0, v6, [[Lcom/a/b/d/re;

    new-array v1, v7, [Lcom/a/b/d/re;

    sget-object v2, Lcom/a/b/d/re;->a:Lcom/a/b/d/re;

    aput-object v2, v1, v3

    sget-object v2, Lcom/a/b/d/re;->b:Lcom/a/b/d/re;

    aput-object v2, v1, v4

    sget-object v2, Lcom/a/b/d/re;->c:Lcom/a/b/d/re;

    aput-object v2, v1, v5

    sget-object v2, Lcom/a/b/d/re;->d:Lcom/a/b/d/re;

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    new-array v1, v3, [Lcom/a/b/d/re;

    aput-object v1, v0, v4

    new-array v1, v7, [Lcom/a/b/d/re;

    sget-object v2, Lcom/a/b/d/re;->e:Lcom/a/b/d/re;

    aput-object v2, v1, v3

    sget-object v2, Lcom/a/b/d/re;->f:Lcom/a/b/d/re;

    aput-object v2, v1, v4

    sget-object v2, Lcom/a/b/d/re;->g:Lcom/a/b/d/re;

    aput-object v2, v1, v5

    sget-object v2, Lcom/a/b/d/re;->h:Lcom/a/b/d/re;

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    sput-object v0, Lcom/a/b/d/re;->k:[[Lcom/a/b/d/re;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 351
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 351
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/re;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Lcom/a/b/d/sh;ZZ)Lcom/a/b/d/re;
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 478
    if-eqz p1, :cond_13

    const/4 v1, 0x1

    :goto_4
    if-eqz p2, :cond_7

    const/4 v0, 0x2

    :cond_7
    or-int/2addr v0, v1

    .line 479
    sget-object v1, Lcom/a/b/d/re;->k:[[Lcom/a/b/d/re;

    invoke-virtual {p0}, Lcom/a/b/d/sh;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    aget-object v0, v1, v0

    return-object v0

    :cond_13
    move v1, v0

    .line 478
    goto :goto_4
.end method

.method static a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V
    .registers 4

    .prologue
    .line 509
    invoke-interface {p0}, Lcom/a/b/d/rz;->e()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/a/b/d/rz;->a(J)V

    .line 511
    invoke-interface {p0}, Lcom/a/b/d/rz;->g()Lcom/a/b/d/rz;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 512
    invoke-interface {p0}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 514
    invoke-static {p0}, Lcom/a/b/d/qy;->b(Lcom/a/b/d/rz;)V

    .line 515
    return-void
.end method

.method static b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 521
    invoke-interface {p0}, Lcom/a/b/d/rz;->i()Lcom/a/b/d/rz;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/qy;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 522
    invoke-interface {p0}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/qy;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 524
    invoke-static {p0}, Lcom/a/b/d/qy;->c(Lcom/a/b/d/rz;)V

    .line 525
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/d/re;
    .registers 2

    .prologue
    .line 351
    const-class v0, Lcom/a/b/d/re;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/re;

    return-object v0
.end method

.method public static values()[Lcom/a/b/d/re;
    .registers 1

    .prologue
    .line 351
    sget-object v0, Lcom/a/b/d/re;->l:[Lcom/a/b/d/re;

    invoke-virtual {v0}, [Lcom/a/b/d/re;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/d/re;

    return-object v0
.end method


# virtual methods
.method a(Lcom/a/b/d/sa;Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
    .registers 6

    .prologue
    .line 502
    invoke-interface {p2}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lcom/a/b/d/rz;->c()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lcom/a/b/d/re;->a(Lcom/a/b/d/sa;Ljava/lang/Object;ILcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Lcom/a/b/d/sa;Ljava/lang/Object;ILcom/a/b/d/rz;)Lcom/a/b/d/rz;
    .param p4    # Lcom/a/b/d/rz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method
