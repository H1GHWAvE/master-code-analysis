.class Lcom/a/b/d/xw;
.super Lcom/a/b/d/gy;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final d:J


# instance fields
.field final a:Lcom/a/b/d/xc;

.field transient b:Ljava/util/Set;

.field transient c:Ljava/util/Set;


# direct methods
.method constructor <init>(Lcom/a/b/d/xc;)V
    .registers 2

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/a/b/d/gy;-><init>()V

    .line 100
    iput-object p1, p0, Lcom/a/b/d/xw;->a:Lcom/a/b/d/xc;

    .line 101
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 144
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/a/b/d/xw;->c:Ljava/util/Set;

    .line 126
    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/a/b/d/xw;->a:Lcom/a/b/d/xc;

    invoke-interface {v0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/xw;->c:Ljava/util/Set;

    :cond_10
    return-object v0
.end method

.method public final a(Ljava/lang/Object;II)Z
    .registers 5

    .prologue
    .line 176
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 140
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 148
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 156
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 172
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method c()Ljava/util/Set;
    .registers 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/a/b/d/xw;->a:Lcom/a/b/d/xc;

    invoke-interface {v0}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 168
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected f()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/a/b/d/xw;->a:Lcom/a/b/d/xc;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/a/b/d/xw;->a:Lcom/a/b/d/xc;

    invoke-interface {v0}, Lcom/a/b/d/xc;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    return-object v0
.end method

.method public n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/a/b/d/xw;->b:Ljava/util/Set;

    .line 118
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/xw;->c()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/xw;->b:Ljava/util/Set;

    :cond_a
    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 152
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 160
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 164
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
