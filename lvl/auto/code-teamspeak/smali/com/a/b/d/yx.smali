.class final Lcom/a/b/d/yx;
.super Lcom/a/b/d/it;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field static final a:D = 1.2


# instance fields
.field final transient b:[Lcom/a/b/d/ka;

.field final transient c:I

.field private final transient d:[Lcom/a/b/d/ka;

.field private final transient e:[Lcom/a/b/d/ka;

.field private final transient f:I

.field private transient g:Lcom/a/b/d/it;


# direct methods
.method constructor <init>(I[Lcom/a/b/d/kb;)V
    .registers 21

    .prologue
    .line 55
    invoke-direct/range {p0 .. p0}, Lcom/a/b/d/it;-><init>()V

    .line 56
    const-wide v2, 0x3ff3333333333333L    # 1.2

    move/from16 v0, p1

    invoke-static {v0, v2, v3}, Lcom/a/b/d/iq;->a(ID)I

    move-result v2

    .line 57
    add-int/lit8 v3, v2, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/a/b/d/yx;->c:I

    .line 1183
    new-array v9, v2, [Lcom/a/b/d/ka;

    .line 2183
    new-array v10, v2, [Lcom/a/b/d/ka;

    .line 3183
    move/from16 v0, p1

    new-array v11, v0, [Lcom/a/b/d/ka;

    .line 61
    const/4 v3, 0x0

    .line 63
    const/4 v2, 0x0

    move v7, v2

    move v8, v3

    :goto_20
    move/from16 v0, p1

    if-ge v7, v0, :cond_a2

    .line 65
    aget-object v3, p2, v7

    .line 66
    invoke-virtual {v3}, Lcom/a/b/d/kb;->getKey()Ljava/lang/Object;

    move-result-object v5

    .line 67
    invoke-virtual {v3}, Lcom/a/b/d/kb;->getValue()Ljava/lang/Object;

    move-result-object v12

    .line 69
    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v13

    .line 70
    invoke-virtual {v12}, Ljava/lang/Object;->hashCode()I

    move-result v14

    .line 71
    invoke-static {v13}, Lcom/a/b/d/iq;->a(I)I

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/a/b/d/yx;->c:I

    and-int v15, v2, v4

    .line 72
    invoke-static {v14}, Lcom/a/b/d/iq;->a(I)I

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/a/b/d/yx;->c:I

    and-int v16, v2, v4

    .line 74
    aget-object v6, v9, v15

    move-object v4, v6

    .line 75
    :goto_4d
    if-eqz v4, :cond_69

    .line 77
    invoke-virtual {v4}, Lcom/a/b/d/ka;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_67

    const/4 v2, 0x1

    :goto_5a
    const-string v17, "key"

    move-object/from16 v0, v17

    invoke-static {v2, v0, v3, v4}, Lcom/a/b/d/yx;->a(ZLjava/lang/String;Ljava/util/Map$Entry;Ljava/util/Map$Entry;)V

    .line 76
    invoke-virtual {v4}, Lcom/a/b/d/ka;->a()Lcom/a/b/d/ka;

    move-result-object v2

    move-object v4, v2

    goto :goto_4d

    .line 77
    :cond_67
    const/4 v2, 0x0

    goto :goto_5a

    .line 79
    :cond_69
    aget-object v5, v10, v16

    move-object v4, v5

    .line 80
    :goto_6c
    if-eqz v4, :cond_88

    .line 82
    invoke-virtual {v4}, Lcom/a/b/d/ka;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_86

    const/4 v2, 0x1

    :goto_79
    const-string v17, "value"

    move-object/from16 v0, v17

    invoke-static {v2, v0, v3, v4}, Lcom/a/b/d/yx;->a(ZLjava/lang/String;Ljava/util/Map$Entry;Ljava/util/Map$Entry;)V

    .line 81
    invoke-virtual {v4}, Lcom/a/b/d/ka;->b()Lcom/a/b/d/ka;

    move-result-object v2

    move-object v4, v2

    goto :goto_6c

    .line 82
    :cond_86
    const/4 v2, 0x0

    goto :goto_79

    .line 84
    :cond_88
    if-nez v6, :cond_9c

    if-nez v5, :cond_9c

    move-object v2, v3

    .line 88
    :goto_8d
    aput-object v2, v9, v15

    .line 89
    aput-object v2, v10, v16

    .line 90
    aput-object v2, v11, v7

    .line 91
    xor-int v2, v13, v14

    add-int v3, v8, v2

    .line 63
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v8, v3

    goto :goto_20

    .line 84
    :cond_9c
    new-instance v2, Lcom/a/b/d/zd;

    invoke-direct {v2, v3, v6, v5}, Lcom/a/b/d/zd;-><init>(Lcom/a/b/d/ka;Lcom/a/b/d/ka;Lcom/a/b/d/ka;)V

    goto :goto_8d

    .line 94
    :cond_a2
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/a/b/d/yx;->d:[Lcom/a/b/d/ka;

    .line 95
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/a/b/d/yx;->b:[Lcom/a/b/d/ka;

    .line 96
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/a/b/d/yx;->e:[Lcom/a/b/d/ka;

    .line 97
    move-object/from16 v0, p0

    iput v8, v0, Lcom/a/b/d/yx;->f:I

    .line 98
    return-void
.end method

.method varargs constructor <init>([Lcom/a/b/d/kb;)V
    .registers 3

    .prologue
    .line 46
    array-length v0, p1

    invoke-direct {p0, v0, p1}, Lcom/a/b/d/yx;-><init>(I[Lcom/a/b/d/kb;)V

    .line 47
    return-void
.end method

.method constructor <init>([Ljava/util/Map$Entry;)V
    .registers 22

    .prologue
    .line 103
    invoke-direct/range {p0 .. p0}, Lcom/a/b/d/it;-><init>()V

    .line 104
    move-object/from16 v0, p1

    array-length v8, v0

    .line 105
    const-wide v2, 0x3ff3333333333333L    # 1.2

    invoke-static {v8, v2, v3}, Lcom/a/b/d/iq;->a(ID)I

    move-result v2

    .line 106
    add-int/lit8 v3, v2, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/a/b/d/yx;->c:I

    .line 4183
    new-array v9, v2, [Lcom/a/b/d/ka;

    .line 5183
    new-array v10, v2, [Lcom/a/b/d/ka;

    .line 6183
    new-array v11, v8, [Lcom/a/b/d/ka;

    .line 110
    const/4 v3, 0x0

    .line 112
    const/4 v2, 0x0

    move v6, v2

    move v7, v3

    :goto_1f
    if-ge v6, v8, :cond_a4

    .line 114
    aget-object v12, p1, v6

    .line 115
    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    .line 116
    invoke-interface {v12}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    .line 117
    invoke-static {v13, v14}, Lcom/a/b/d/cl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 118
    invoke-virtual {v13}, Ljava/lang/Object;->hashCode()I

    move-result v15

    .line 119
    invoke-virtual {v14}, Ljava/lang/Object;->hashCode()I

    move-result v16

    .line 120
    invoke-static {v15}, Lcom/a/b/d/iq;->a(I)I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/a/b/d/yx;->c:I

    and-int v17, v2, v3

    .line 121
    invoke-static/range {v16 .. v16}, Lcom/a/b/d/iq;->a(I)I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/a/b/d/yx;->c:I

    and-int v18, v2, v3

    .line 123
    aget-object v5, v9, v17

    move-object v3, v5

    .line 124
    :goto_4d
    if-eqz v3, :cond_67

    .line 126
    invoke-virtual {v3}, Lcom/a/b/d/ka;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_65

    const/4 v2, 0x1

    :goto_5a
    const-string v4, "key"

    invoke-static {v2, v4, v12, v3}, Lcom/a/b/d/yx;->a(ZLjava/lang/String;Ljava/util/Map$Entry;Ljava/util/Map$Entry;)V

    .line 125
    invoke-virtual {v3}, Lcom/a/b/d/ka;->a()Lcom/a/b/d/ka;

    move-result-object v2

    move-object v3, v2

    goto :goto_4d

    .line 126
    :cond_65
    const/4 v2, 0x0

    goto :goto_5a

    .line 128
    :cond_67
    aget-object v4, v10, v18

    move-object v3, v4

    .line 129
    :goto_6a
    if-eqz v3, :cond_86

    .line 131
    invoke-virtual {v3}, Lcom/a/b/d/ka;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_84

    const/4 v2, 0x1

    :goto_77
    const-string v19, "value"

    move-object/from16 v0, v19

    invoke-static {v2, v0, v12, v3}, Lcom/a/b/d/yx;->a(ZLjava/lang/String;Ljava/util/Map$Entry;Ljava/util/Map$Entry;)V

    .line 130
    invoke-virtual {v3}, Lcom/a/b/d/ka;->b()Lcom/a/b/d/ka;

    move-result-object v2

    move-object v3, v2

    goto :goto_6a

    .line 131
    :cond_84
    const/4 v2, 0x0

    goto :goto_77

    .line 133
    :cond_86
    if-nez v5, :cond_9e

    if-nez v4, :cond_9e

    new-instance v2, Lcom/a/b/d/kb;

    invoke-direct {v2, v13, v14}, Lcom/a/b/d/kb;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 137
    :goto_8f
    aput-object v2, v9, v17

    .line 138
    aput-object v2, v10, v18

    .line 139
    aput-object v2, v11, v6

    .line 140
    xor-int v2, v15, v16

    add-int v3, v7, v2

    .line 112
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v7, v3

    goto :goto_1f

    .line 133
    :cond_9e
    new-instance v2, Lcom/a/b/d/zd;

    invoke-direct {v2, v13, v14, v5, v4}, Lcom/a/b/d/zd;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/ka;Lcom/a/b/d/ka;)V

    goto :goto_8f

    .line 143
    :cond_a4
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/a/b/d/yx;->d:[Lcom/a/b/d/ka;

    .line 144
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/a/b/d/yx;->b:[Lcom/a/b/d/ka;

    .line 145
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/a/b/d/yx;->e:[Lcom/a/b/d/ka;

    .line 146
    move-object/from16 v0, p0

    iput v7, v0, Lcom/a/b/d/yx;->f:I

    .line 147
    return-void
.end method

.method private static a(I)[Lcom/a/b/d/ka;
    .registers 2

    .prologue
    .line 183
    new-array v0, p0, [Lcom/a/b/d/ka;

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/yx;)[Lcom/a/b/d/ka;
    .registers 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/a/b/d/yx;->e:[Lcom/a/b/d/ka;

    return-object v0
.end method

.method static synthetic b(Lcom/a/b/d/yx;)I
    .registers 2

    .prologue
    .line 35
    iget v0, p0, Lcom/a/b/d/yx;->f:I

    return v0
.end method

.method private static synthetic c(Lcom/a/b/d/yx;)I
    .registers 2

    .prologue
    .line 35
    iget v0, p0, Lcom/a/b/d/yx;->c:I

    return v0
.end method

.method private static synthetic d(Lcom/a/b/d/yx;)[Lcom/a/b/d/ka;
    .registers 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/a/b/d/yx;->b:[Lcom/a/b/d/ka;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/a/b/d/it;
    .registers 3

    .prologue
    .line 246
    iget-object v0, p0, Lcom/a/b/d/yx;->g:Lcom/a/b/d/it;

    .line 247
    if-nez v0, :cond_c

    new-instance v0, Lcom/a/b/d/yz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/yz;-><init>(Lcom/a/b/d/yx;B)V

    iput-object v0, p0, Lcom/a/b/d/yx;->g:Lcom/a/b/d/it;

    :cond_c
    return-object v0
.end method

.method public final synthetic b()Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/a/b/d/yx;->a()Lcom/a/b/d/it;

    move-result-object v0

    return-object v0
.end method

.method final d()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 204
    new-instance v0, Lcom/a/b/d/yy;

    invoke-direct {v0, p0}, Lcom/a/b/d/yy;-><init>(Lcom/a/b/d/yx;)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 189
    if-nez p1, :cond_4

    .line 199
    :cond_3
    :goto_3
    return-object v0

    .line 192
    :cond_4
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Lcom/a/b/d/iq;->a(I)I

    move-result v1

    iget v2, p0, Lcom/a/b/d/yx;->c:I

    and-int/2addr v1, v2

    .line 193
    iget-object v2, p0, Lcom/a/b/d/yx;->d:[Lcom/a/b/d/ka;

    aget-object v1, v2, v1

    :goto_13
    if-eqz v1, :cond_3

    .line 195
    invoke-virtual {v1}, Lcom/a/b/d/ka;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 196
    invoke-virtual {v1}, Lcom/a/b/d/ka;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_3

    .line 194
    :cond_24
    invoke-virtual {v1}, Lcom/a/b/d/ka;->a()Lcom/a/b/d/ka;

    move-result-object v1

    goto :goto_13
.end method

.method final i_()Z
    .registers 2

    .prologue
    .line 234
    const/4 v0, 0x0

    return v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 239
    iget-object v0, p0, Lcom/a/b/d/yx;->e:[Lcom/a/b/d/ka;

    array-length v0, v0

    return v0
.end method
