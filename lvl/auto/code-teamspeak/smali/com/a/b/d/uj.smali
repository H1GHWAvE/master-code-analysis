.class abstract Lcom/a/b/d/uj;
.super Ljava/util/AbstractMap;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field private transient a:Ljava/util/Set;

.field private transient b:Ljava/util/Set;

.field private transient c:Ljava/util/Collection;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 3308
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Ljava/util/Set;
.end method

.method c_()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 3342
    new-instance v0, Lcom/a/b/d/vb;

    invoke-direct {v0, p0}, Lcom/a/b/d/vb;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3331
    new-instance v0, Lcom/a/b/d/uk;

    invoke-direct {v0, p0}, Lcom/a/b/d/uk;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3319
    iget-object v0, p0, Lcom/a/b/d/uj;->a:Ljava/util/Set;

    .line 3320
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/uj;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/uj;->a:Ljava/util/Set;

    :cond_a
    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3326
    iget-object v0, p0, Lcom/a/b/d/uj;->b:Ljava/util/Set;

    .line 3327
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/uj;->e()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/uj;->b:Ljava/util/Set;

    :cond_a
    return-object v0
.end method

.method public values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 3337
    iget-object v0, p0, Lcom/a/b/d/uj;->c:Ljava/util/Collection;

    .line 3338
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/uj;->c_()Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/uj;->c:Ljava/util/Collection;

    :cond_a
    return-object v0
.end method
