.class Lcom/a/b/d/adt;
.super Lcom/a/b/d/adq;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedSet;


# static fields
.field private static final a:J


# direct methods
.method constructor <init>(Ljava/util/SortedSet;Ljava/lang/Object;)V
    .registers 3
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 250
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/adq;-><init>(Ljava/util/Set;Ljava/lang/Object;)V

    .line 251
    return-void
.end method


# virtual methods
.method a()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 254
    invoke-super {p0}, Lcom/a/b/d/adq;->c()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method synthetic c()Ljava/util/Set;
    .registers 2

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 3

    .prologue
    .line 259
    iget-object v1, p0, Lcom/a/b/d/adt;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 260
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 261
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 287
    iget-object v1, p0, Lcom/a/b/d/adt;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 288
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 289
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 273
    iget-object v1, p0, Lcom/a/b/d/adt;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 274
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adt;->h:Ljava/lang/Object;

    .line 2060
    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    .line 274
    monitor-exit v1

    return-object v0

    .line 275
    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    throw v0
.end method

.method public last()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 294
    iget-object v1, p0, Lcom/a/b/d/adt;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 295
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 296
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 6

    .prologue
    .line 266
    iget-object v1, p0, Lcom/a/b/d/adt;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 267
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adt;->h:Ljava/lang/Object;

    .line 1060
    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    .line 267
    monitor-exit v1

    return-object v0

    .line 268
    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    throw v0
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 280
    iget-object v1, p0, Lcom/a/b/d/adt;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 281
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adt;->h:Ljava/lang/Object;

    .line 3060
    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    .line 281
    monitor-exit v1

    return-object v0

    .line 282
    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    throw v0
.end method
