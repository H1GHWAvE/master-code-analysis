.class public final Lcom/a/b/d/lx;
.super Lcom/a/b/d/ju;
.source "SourceFile"


# instance fields
.field private final c:Ljava/util/Comparator;


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .registers 3

    .prologue
    .line 362
    invoke-direct {p0}, Lcom/a/b/d/ju;-><init>()V

    .line 363
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/lx;->c:Ljava/util/Comparator;

    .line 364
    return-void
.end method

.method private b()Lcom/a/b/d/lw;
    .registers 5

    .prologue
    .line 408
    iget-object v0, p0, Lcom/a/b/d/lx;->c:Ljava/util/Comparator;

    const/4 v1, 0x0

    iget v2, p0, Lcom/a/b/d/lx;->b:I

    iget-object v3, p0, Lcom/a/b/d/lx;->a:[Lcom/a/b/d/kb;

    invoke-static {v0, v1, v2, v3}, Lcom/a/b/d/lw;->a(Ljava/util/Comparator;ZI[Ljava/util/Map$Entry;)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lx;
    .registers 3

    .prologue
    .line 372
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 373
    return-object p0
.end method

.method private b(Ljava/util/Map$Entry;)Lcom/a/b/d/lx;
    .registers 2

    .prologue
    .line 385
    invoke-super {p0, p1}, Lcom/a/b/d/ju;->a(Ljava/util/Map$Entry;)Lcom/a/b/d/ju;

    .line 386
    return-object p0
.end method

.method private b(Ljava/util/Map;)Lcom/a/b/d/lx;
    .registers 2

    .prologue
    .line 397
    invoke-super {p0, p1}, Lcom/a/b/d/ju;->a(Ljava/util/Map;)Lcom/a/b/d/ju;

    .line 398
    return-object p0
.end method


# virtual methods
.method public final bridge synthetic a()Lcom/a/b/d/jt;
    .registers 5

    .prologue
    .line 354
    .line 1408
    iget-object v0, p0, Lcom/a/b/d/lx;->c:Ljava/util/Comparator;

    const/4 v1, 0x0

    iget v2, p0, Lcom/a/b/d/lx;->b:I

    iget-object v3, p0, Lcom/a/b/d/lx;->a:[Lcom/a/b/d/kb;

    invoke-static {v0, v1, v2, v3}, Lcom/a/b/d/lw;->a(Ljava/util/Comparator;ZI[Ljava/util/Map$Entry;)Lcom/a/b/d/lw;

    move-result-object v0

    .line 354
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;
    .registers 3

    .prologue
    .line 354
    .line 4372
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 354
    return-object p0
.end method

.method public final bridge synthetic a(Ljava/util/Map$Entry;)Lcom/a/b/d/ju;
    .registers 2

    .prologue
    .line 354
    .line 3385
    invoke-super {p0, p1}, Lcom/a/b/d/ju;->a(Ljava/util/Map$Entry;)Lcom/a/b/d/ju;

    .line 354
    return-object p0
.end method

.method public final bridge synthetic a(Ljava/util/Map;)Lcom/a/b/d/ju;
    .registers 2

    .prologue
    .line 354
    .line 2397
    invoke-super {p0, p1}, Lcom/a/b/d/ju;->a(Ljava/util/Map;)Lcom/a/b/d/ju;

    .line 354
    return-object p0
.end method
