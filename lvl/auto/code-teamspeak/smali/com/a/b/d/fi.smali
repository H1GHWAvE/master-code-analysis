.class Lcom/a/b/d/fi;
.super Lcom/a/b/d/an;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/ga;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field final a:Lcom/a/b/d/vi;

.field final b:Lcom/a/b/b/co;


# direct methods
.method constructor <init>(Lcom/a/b/d/vi;Lcom/a/b/b/co;)V
    .registers 4

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/a/b/d/an;-><init>()V

    .line 51
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/vi;

    iput-object v0, p0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    .line 52
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/co;

    iput-object v0, p0, Lcom/a/b/d/fi;->b:Lcom/a/b/b/co;

    .line 53
    return-void
.end method

.method static a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 90
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_b

    .line 91
    check-cast p0, Ljava/util/Set;

    invoke-static {p0, p1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;

    move-result-object v0

    .line 93
    :goto_a
    return-object v0

    :cond_b
    invoke-static {p0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_a
.end method

.method static synthetic a(Lcom/a/b/d/fi;Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 46
    .line 2071
    iget-object v0, p0, Lcom/a/b/d/fi;->b:Lcom/a/b/b/co;

    invoke-static {p1, p2}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    .line 46
    return v0
.end method

.method private d()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    instance-of v0, v0, Lcom/a/b/d/aac;

    if-eqz v0, :cond_b

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_a
.end method

.method private d(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 71
    iget-object v0, p0, Lcom/a/b/d/fi;->b:Lcom/a/b/b/co;

    invoke-static {p1, p2}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    return-object v0
.end method

.method final a(Lcom/a/b/b/co;)Z
    .registers 8

    .prologue
    .line 150
    iget-object v0, p0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 151
    const/4 v0, 0x0

    move v2, v0

    .line 152
    :goto_10
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 153
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 154
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    .line 155
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    new-instance v5, Lcom/a/b/d/fr;

    invoke-direct {v5, p0, v4}, Lcom/a/b/d/fr;-><init>(Lcom/a/b/d/fi;Ljava/lang/Object;)V

    invoke-static {v1, v5}, Lcom/a/b/d/fi;->a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;

    move-result-object v1

    .line 156
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5b

    invoke-static {v4, v1}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v4

    invoke-interface {p1, v4}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5b

    .line 157
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-ne v2, v0, :cond_56

    .line 158
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 162
    :goto_52
    const/4 v2, 0x1

    move v0, v2

    :goto_54
    move v2, v0

    .line 164
    goto :goto_10

    .line 160
    :cond_56
    invoke-interface {v1}, Ljava/util/Collection;->clear()V

    goto :goto_52

    .line 165
    :cond_5a
    return v2

    :cond_5b
    move v0, v2

    goto :goto_54
.end method

.method public final c()Lcom/a/b/b/co;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/a/b/d/fi;->b:Lcom/a/b/b/co;

    return-object v0
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 121
    iget-object v0, p0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    new-instance v1, Lcom/a/b/d/fr;

    invoke-direct {v1, p0, p1}, Lcom/a/b/d/fr;-><init>(Lcom/a/b/d/fi;Ljava/lang/Object;)V

    invoke-static {v0, v1}, Lcom/a/b/d/fi;->a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/a/b/d/fi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1109
    iget-object v0, p0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    instance-of v0, v0, Lcom/a/b/d/aac;

    if-eqz v0, :cond_19

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 104
    :goto_12
    invoke-static {v1, v0}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0

    .line 1109
    :cond_19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_12
.end method

.method public final f()I
    .registers 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/a/b/d/fi;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    return v0
.end method

.method public final f(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/a/b/d/fi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/a/b/d/fi;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 117
    return-void
.end method

.method final l()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 136
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method final m()Ljava/util/Map;
    .registers 2

    .prologue
    .line 141
    new-instance v0, Lcom/a/b/d/fj;

    invoke-direct {v0, p0}, Lcom/a/b/d/fj;-><init>(Lcom/a/b/d/fi;)V

    return-object v0
.end method

.method o()Ljava/util/Collection;
    .registers 3

    .prologue
    .line 126
    iget-object v0, p0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/fi;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/fi;->a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/util/Set;
    .registers 2

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/a/b/d/fi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method final r()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 326
    new-instance v0, Lcom/a/b/d/fo;

    invoke-direct {v0, p0}, Lcom/a/b/d/fo;-><init>(Lcom/a/b/d/fi;)V

    return-object v0
.end method

.method final s()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 131
    new-instance v0, Lcom/a/b/d/gb;

    invoke-direct {v0, p0}, Lcom/a/b/d/gb;-><init>(Lcom/a/b/d/ga;)V

    return-object v0
.end method
