.class final Lcom/a/b/d/dd;
.super Lcom/a/b/d/sa;
.source "SourceFile"


# direct methods
.method constructor <init>(Lcom/a/b/d/qy;II)V
    .registers 4

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/a/b/d/sa;-><init>(Lcom/a/b/d/qy;II)V

    .line 74
    return-void
.end method

.method private a(Ljava/lang/Object;ILcom/a/b/d/rz;Lcom/a/b/d/df;)Ljava/lang/Object;
    .registers 11

    .prologue
    const-wide/16 v4, 0x0

    .line 174
    const/4 v1, 0x0

    .line 175
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    .line 181
    :try_start_6
    monitor-enter p3
    :try_end_7
    .catchall {:try_start_6 .. :try_end_7} :catchall_3d

    .line 182
    :try_start_7
    invoke-virtual {p4, p1}, Lcom/a/b/d/df;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 183
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_2b

    move-result-wide v2

    .line 184
    :try_start_f
    monitor-exit p3
    :try_end_10
    .catchall {:try_start_f .. :try_end_10} :catchall_40

    .line 185
    if-eqz v1, :cond_1e

    .line 187
    const/4 v0, 0x1

    :try_start_13
    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/a/b/d/dd;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    .line 188
    if-eqz v0, :cond_1e

    .line 190
    sget-object v0, Lcom/a/b/d/qq;->b:Lcom/a/b/d/qq;

    invoke-virtual {p0, p1, v1, v0}, Lcom/a/b/d/dd;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
    :try_end_1e
    .catchall {:try_start_13 .. :try_end_1e} :catchall_2f

    .line 195
    :cond_1e
    cmp-long v0, v2, v4

    if-nez v0, :cond_25

    .line 196
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    .line 198
    :cond_25
    if-nez v1, :cond_2a

    .line 199
    invoke-virtual {p0, p1, p2, p4}, Lcom/a/b/d/dd;->b(Ljava/lang/Object;ILcom/a/b/d/sr;)Z

    :cond_2a
    return-object v1

    .line 184
    :catchall_2b
    move-exception v0

    move-wide v2, v4

    :goto_2d
    :try_start_2d
    monitor-exit p3
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_40

    :try_start_2e
    throw v0
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2f

    .line 195
    :catchall_2f
    move-exception v0

    :goto_30
    cmp-long v2, v2, v4

    if-nez v2, :cond_37

    .line 196
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    .line 198
    :cond_37
    if-nez v1, :cond_3c

    .line 199
    invoke-virtual {p0, p1, p2, p4}, Lcom/a/b/d/dd;->b(Ljava/lang/Object;ILcom/a/b/d/sr;)Z

    :cond_3c
    throw v0

    .line 195
    :catchall_3d
    move-exception v0

    move-wide v2, v4

    goto :goto_30

    .line 184
    :catchall_40
    move-exception v0

    goto :goto_2d
.end method


# virtual methods
.method final a(Ljava/lang/Object;ILcom/a/b/b/bj;)Ljava/lang/Object;
    .registers 15

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 81
    :cond_2
    :try_start_2
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/dd;->a(Ljava/lang/Object;I)Lcom/a/b/d/rz;

    move-result-object v1

    .line 82
    if-eqz v1, :cond_15

    .line 83
    invoke-virtual {p0, v1}, Lcom/a/b/d/dd;->c(Lcom/a/b/d/rz;)Ljava/lang/Object;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_15

    .line 85
    invoke-virtual {p0, v1}, Lcom/a/b/d/dd;->a(Lcom/a/b/d/rz;)V
    :try_end_11
    .catchall {:try_start_2 .. :try_end_11} :catchall_be

    .line 167
    invoke-virtual {p0}, Lcom/a/b/d/dd;->a()V

    :goto_14
    return-object v0

    .line 92
    :cond_15
    if-eqz v1, :cond_21

    :try_start_17
    invoke-interface {v1}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/sr;->b()Z

    move-result v0

    if-nez v0, :cond_de

    .line 94
    :cond_21
    const/4 v4, 0x0

    .line 95
    invoke-virtual {p0}, Lcom/a/b/d/dd;->lock()V
    :try_end_25
    .catchall {:try_start_17 .. :try_end_25} :catchall_be

    .line 4062
    :try_start_25
    invoke-virtual {p0}, Lcom/a/b/d/sa;->c()V

    .line 99
    iget v0, p0, Lcom/a/b/d/dd;->b:I

    add-int/lit8 v6, v0, -0x1

    .line 100
    iget-object v7, p0, Lcom/a/b/d/dd;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 101
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    .line 102
    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    move-object v5, v0

    .line 104
    :goto_3d
    if-eqz v5, :cond_102

    .line 105
    invoke-interface {v5}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v9

    .line 106
    invoke-interface {v5}, Lcom/a/b/d/rz;->c()I

    move-result v1

    if-ne v1, p2, :cond_d2

    if-eqz v9, :cond_d2

    iget-object v1, p0, Lcom/a/b/d/dd;->a:Lcom/a/b/d/qy;

    iget-object v1, v1, Lcom/a/b/d/qy;->m:Lcom/a/b/b/au;

    invoke-virtual {v1, p1, v9}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d2

    .line 108
    invoke-interface {v5}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v1

    .line 109
    invoke-interface {v1}, Lcom/a/b/d/sr;->b()Z

    move-result v1

    if-eqz v1, :cond_83

    move v6, v3

    .line 133
    :goto_60
    if-eqz v6, :cond_fe

    .line 134
    new-instance v1, Lcom/a/b/d/df;

    invoke-direct {v1, p3}, Lcom/a/b/d/df;-><init>(Lcom/a/b/b/bj;)V

    .line 136
    if-nez v5, :cond_d8

    .line 137
    invoke-virtual {p0, p1, p2, v0}, Lcom/a/b/d/dd;->a(Ljava/lang/Object;ILcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 138
    invoke-interface {v0, v1}, Lcom/a/b/d/rz;->a(Lcom/a/b/d/sr;)V

    .line 139
    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_73
    .catchall {:try_start_25 .. :try_end_73} :catchall_b6

    .line 145
    :goto_73
    :try_start_73
    invoke-virtual {p0}, Lcom/a/b/d/dd;->unlock()V

    .line 5069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 149
    if-eqz v6, :cond_dd

    .line 151
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/a/b/d/dd;->a(Ljava/lang/Object;ILcom/a/b/d/rz;Lcom/a/b/d/df;)Ljava/lang/Object;
    :try_end_7e
    .catchall {:try_start_73 .. :try_end_7e} :catchall_be

    move-result-object v0

    .line 167
    invoke-virtual {p0}, Lcom/a/b/d/dd;->a()V

    goto :goto_14

    .line 112
    :cond_83
    :try_start_83
    invoke-interface {v5}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v1

    .line 113
    if-nez v1, :cond_a0

    .line 114
    sget-object v10, Lcom/a/b/d/qq;->c:Lcom/a/b/d/qq;

    invoke-virtual {p0, v9, v1, v10}, Lcom/a/b/d/dd;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 125
    :goto_92
    iget-object v1, p0, Lcom/a/b/d/dd;->k:Ljava/util/Queue;

    invoke-interface {v1, v5}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 126
    iget-object v1, p0, Lcom/a/b/d/dd;->l:Ljava/util/Queue;

    invoke-interface {v1, v5}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 127
    iput v6, p0, Lcom/a/b/d/dd;->b:I

    move v6, v2

    .line 129
    goto :goto_60

    .line 115
    :cond_a0
    iget-object v10, p0, Lcom/a/b/d/dd;->a:Lcom/a/b/d/qy;

    invoke-virtual {v10}, Lcom/a/b/d/qy;->c()Z

    move-result v10

    if-eqz v10, :cond_c3

    iget-object v10, p0, Lcom/a/b/d/dd;->a:Lcom/a/b/d/qy;

    invoke-virtual {v10, v5}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;)Z

    move-result v10

    if-eqz v10, :cond_c3

    .line 118
    sget-object v10, Lcom/a/b/d/qq;->d:Lcom/a/b/d/qq;

    invoke-virtual {p0, v9, v1, v10}, Lcom/a/b/d/dd;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
    :try_end_b5
    .catchall {:try_start_83 .. :try_end_b5} :catchall_b6

    goto :goto_92

    .line 145
    :catchall_b6
    move-exception v0

    :try_start_b7
    invoke-virtual {p0}, Lcom/a/b/d/dd;->unlock()V

    .line 6069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 146
    throw v0
    :try_end_be
    .catchall {:try_start_b7 .. :try_end_be} :catchall_be

    .line 167
    :catchall_be
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/dd;->a()V

    throw v0

    .line 120
    :cond_c3
    :try_start_c3
    invoke-virtual {p0, v5}, Lcom/a/b/d/dd;->b(Lcom/a/b/d/rz;)V
    :try_end_c6
    .catchall {:try_start_c3 .. :try_end_c6} :catchall_b6

    .line 145
    :try_start_c6
    invoke-virtual {p0}, Lcom/a/b/d/dd;->unlock()V

    .line 4069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V
    :try_end_cc
    .catchall {:try_start_c6 .. :try_end_cc} :catchall_be

    .line 167
    invoke-virtual {p0}, Lcom/a/b/d/dd;->a()V

    move-object v0, v1

    goto/16 :goto_14

    .line 104
    :cond_d2
    :try_start_d2
    invoke-interface {v5}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v5

    goto/16 :goto_3d

    .line 141
    :cond_d8
    invoke-interface {v5, v1}, Lcom/a/b/d/rz;->a(Lcom/a/b/d/sr;)V
    :try_end_db
    .catchall {:try_start_d2 .. :try_end_db} :catchall_b6

    move-object v0, v5

    goto :goto_73

    :cond_dd
    move-object v1, v0

    .line 156
    :cond_de
    :try_start_de
    invoke-static {v1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_fc

    move v0, v2

    :goto_e5
    const-string v4, "Recursive computation"

    invoke-static {v0, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 158
    invoke-interface {v1}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/sr;->c()Ljava/lang/Object;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_2

    .line 160
    invoke-virtual {p0, v1}, Lcom/a/b/d/dd;->a(Lcom/a/b/d/rz;)V
    :try_end_f7
    .catchall {:try_start_de .. :try_end_f7} :catchall_be

    .line 167
    invoke-virtual {p0}, Lcom/a/b/d/dd;->a()V

    goto/16 :goto_14

    :cond_fc
    move v0, v3

    .line 156
    goto :goto_e5

    :cond_fe
    move-object v1, v4

    move-object v0, v5

    goto/16 :goto_73

    :cond_102
    move v6, v2

    goto/16 :goto_60
.end method
