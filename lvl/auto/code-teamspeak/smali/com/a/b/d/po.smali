.class public final Lcom/a/b/d/po;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/b/d/aac;Lcom/a/b/d/pn;)Lcom/a/b/d/aac;
    .registers 3

    .prologue
    .line 150
    new-instance v0, Lcom/a/b/d/qf;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/qf;-><init>(Lcom/a/b/d/aac;Lcom/a/b/d/pn;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/abs;Lcom/a/b/d/pn;)Lcom/a/b/d/abs;
    .registers 3

    .prologue
    .line 171
    new-instance v0, Lcom/a/b/d/qg;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/qg;-><init>(Lcom/a/b/d/abs;Lcom/a/b/d/pn;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/bw;Lcom/a/b/d/pn;)Lcom/a/b/d/bw;
    .registers 4

    .prologue
    .line 334
    new-instance v0, Lcom/a/b/d/pw;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/a/b/d/pw;-><init>(Lcom/a/b/d/bw;Lcom/a/b/d/bw;Lcom/a/b/d/pn;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/ou;Lcom/a/b/d/pn;)Lcom/a/b/d/ou;
    .registers 3

    .prologue
    .line 129
    new-instance v0, Lcom/a/b/d/qa;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/qa;-><init>(Lcom/a/b/d/ou;Lcom/a/b/d/pn;)V

    return-object v0
.end method

.method private static a()Lcom/a/b/d/pn;
    .registers 1

    .prologue
    .line 54
    sget-object v0, Lcom/a/b/d/qi;->a:Lcom/a/b/d/qi;

    return-object v0
.end method

.method private static a(Lcom/a/b/d/vi;Lcom/a/b/d/pn;)Lcom/a/b/d/vi;
    .registers 3

    .prologue
    .line 107
    new-instance v0, Lcom/a/b/d/qc;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/qc;-><init>(Lcom/a/b/d/vi;Lcom/a/b/d/pn;)V

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;Lcom/a/b/d/pn;)Ljava/util/Collection;
    .registers 6

    .prologue
    .line 2769
    invoke-static {p1}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2770
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2771
    invoke-interface {p2, p0, v2}, Lcom/a/b/d/pn;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_8

    .line 46
    :cond_16
    return-object v0
.end method

.method private static a(Ljava/util/Collection;Lcom/a/b/d/pn;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 264
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_b

    .line 265
    check-cast p0, Ljava/util/Set;

    invoke-static {p0, p1}, Lcom/a/b/d/po;->a(Ljava/util/Set;Lcom/a/b/d/pn;)Ljava/util/Set;

    move-result-object v0

    .line 267
    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Lcom/a/b/d/px;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/px;-><init>(Ljava/util/Collection;Lcom/a/b/d/pn;)V

    goto :goto_a
.end method

.method private static a(Ljava/util/Map$Entry;Lcom/a/b/d/pn;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 186
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    new-instance v0, Lcom/a/b/d/pp;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/pp;-><init>(Ljava/util/Map$Entry;Lcom/a/b/d/pn;)V

    return-object v0
.end method

.method private static a(Ljava/util/Map;Lcom/a/b/d/pn;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 86
    new-instance v0, Lcom/a/b/d/qb;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/qb;-><init>(Ljava/util/Map;Lcom/a/b/d/pn;)V

    return-object v0
.end method

.method static a(Ljava/util/Set;Lcom/a/b/d/pn;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 287
    new-instance v0, Lcom/a/b/d/pz;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/pz;-><init>(Ljava/util/Set;Lcom/a/b/d/pn;)V

    return-object v0
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/Iterable;Lcom/a/b/d/pn;)Ljava/util/Collection;
    .registers 6

    .prologue
    .line 769
    invoke-static {p1}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 770
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 771
    invoke-interface {p2, p0, v2}, Lcom/a/b/d/pn;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_8

    .line 773
    :cond_16
    return-object v0
.end method

.method private static synthetic b(Ljava/util/Collection;Lcom/a/b/d/pn;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 46
    .line 2264
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_b

    .line 2265
    check-cast p0, Ljava/util/Set;

    invoke-static {p0, p1}, Lcom/a/b/d/po;->a(Ljava/util/Set;Lcom/a/b/d/pn;)Ljava/util/Set;

    move-result-object v0

    :goto_a
    return-object v0

    .line 2267
    :cond_b
    new-instance v0, Lcom/a/b/d/px;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/px;-><init>(Ljava/util/Collection;Lcom/a/b/d/pn;)V

    goto :goto_a
.end method

.method private static b(Ljava/util/Map$Entry;Lcom/a/b/d/pn;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 212
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    new-instance v0, Lcom/a/b/d/pq;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/pq;-><init>(Ljava/util/Map$Entry;Lcom/a/b/d/pn;)V

    return-object v0
.end method

.method private static b(Ljava/util/Map;Lcom/a/b/d/pn;)Ljava/util/Map;
    .registers 6

    .prologue
    .line 778
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, p0}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 779
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 780
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v3, v0}, Lcom/a/b/d/pn;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_d

    .line 782
    :cond_25
    return-object v1
.end method

.method private static b(Ljava/util/Set;Lcom/a/b/d/pn;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 246
    new-instance v0, Lcom/a/b/d/ps;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ps;-><init>(Ljava/util/Set;Lcom/a/b/d/pn;)V

    return-object v0
.end method

.method private static synthetic c(Ljava/util/Map$Entry;Lcom/a/b/d/pn;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 46
    .line 3186
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3187
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3188
    new-instance v0, Lcom/a/b/d/pp;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/pp;-><init>(Ljava/util/Map$Entry;Lcom/a/b/d/pn;)V

    .line 46
    return-object v0
.end method

.method private static synthetic c(Ljava/util/Map;Lcom/a/b/d/pn;)Ljava/util/Map;
    .registers 6

    .prologue
    .line 1778
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, p0}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 1779
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1780
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v3, v0}, Lcom/a/b/d/pn;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_d

    .line 46
    :cond_25
    return-object v1
.end method

.method private static synthetic c(Ljava/util/Set;Lcom/a/b/d/pn;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/a/b/d/po;->a(Ljava/util/Set;Lcom/a/b/d/pn;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private static synthetic d(Ljava/util/Map$Entry;Lcom/a/b/d/pn;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 46
    .line 3212
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3213
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3214
    new-instance v0, Lcom/a/b/d/pq;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/pq;-><init>(Ljava/util/Map$Entry;Lcom/a/b/d/pn;)V

    .line 46
    return-object v0
.end method

.method private static synthetic d(Ljava/util/Set;Lcom/a/b/d/pn;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 2246
    new-instance v0, Lcom/a/b/d/ps;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ps;-><init>(Ljava/util/Set;Lcom/a/b/d/pn;)V

    .line 46
    return-object v0
.end method
