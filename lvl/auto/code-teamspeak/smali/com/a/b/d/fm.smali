.class final Lcom/a/b/d/fm;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Iterator;

.field final synthetic b:Lcom/a/b/d/fl;


# direct methods
.method constructor <init>(Lcom/a/b/d/fl;)V
    .registers 3

    .prologue
    .line 247
    iput-object p1, p0, Lcom/a/b/d/fm;->b:Lcom/a/b/d/fl;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    .line 248
    iget-object v0, p0, Lcom/a/b/d/fm;->b:Lcom/a/b/d/fl;

    iget-object v0, v0, Lcom/a/b/d/fl;->a:Lcom/a/b/d/fj;

    iget-object v0, v0, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    iget-object v0, v0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/fm;->a:Ljava/util/Iterator;

    return-void
.end method

.method private c()Ljava/util/Map$Entry;
    .registers 5

    .prologue
    .line 253
    :cond_0
    iget-object v0, p0, Lcom/a/b/d/fm;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 254
    iget-object v0, p0, Lcom/a/b/d/fm;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 255
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 256
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    new-instance v2, Lcom/a/b/d/fr;

    iget-object v3, p0, Lcom/a/b/d/fm;->b:Lcom/a/b/d/fl;

    iget-object v3, v3, Lcom/a/b/d/fl;->a:Lcom/a/b/d/fj;

    iget-object v3, v3, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    invoke-direct {v2, v3, v1}, Lcom/a/b/d/fr;-><init>(Lcom/a/b/d/fi;Ljava/lang/Object;)V

    invoke-static {v0, v2}, Lcom/a/b/d/fi;->a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;

    move-result-object v0

    .line 258
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 259
    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 262
    :goto_33
    return-object v0

    :cond_34
    invoke-virtual {p0}, Lcom/a/b/d/fm;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    goto :goto_33
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 5

    .prologue
    .line 247
    .line 1253
    :cond_0
    iget-object v0, p0, Lcom/a/b/d/fm;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 1254
    iget-object v0, p0, Lcom/a/b/d/fm;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1255
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 1256
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    new-instance v2, Lcom/a/b/d/fr;

    iget-object v3, p0, Lcom/a/b/d/fm;->b:Lcom/a/b/d/fl;

    iget-object v3, v3, Lcom/a/b/d/fl;->a:Lcom/a/b/d/fj;

    iget-object v3, v3, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    invoke-direct {v2, v3, v1}, Lcom/a/b/d/fr;-><init>(Lcom/a/b/d/fi;Ljava/lang/Object;)V

    invoke-static {v0, v2}, Lcom/a/b/d/fi;->a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;

    move-result-object v0

    .line 1258
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1259
    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    :goto_33
    return-object v0

    .line 1262
    :cond_34
    invoke-virtual {p0}, Lcom/a/b/d/fm;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    .line 247
    goto :goto_33
.end method
