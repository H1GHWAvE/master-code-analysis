.class Lcom/a/b/d/fx;
.super Lcom/a/b/d/gh;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/fu;


# direct methods
.method constructor <init>(Lcom/a/b/d/fu;)V
    .registers 2

    .prologue
    .line 183
    iput-object p1, p0, Lcom/a/b/d/fx;->a:Lcom/a/b/d/fu;

    invoke-direct {p0}, Lcom/a/b/d/gh;-><init>()V

    return-void
.end method


# virtual methods
.method protected final b()Ljava/util/Collection;
    .registers 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/a/b/d/fx;->a:Lcom/a/b/d/fu;

    iget-object v0, v0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/fx;->a:Lcom/a/b/d/fu;

    .line 1055
    iget-object v1, v1, Lcom/a/b/d/fu;->b:Lcom/a/b/b/co;

    invoke-static {v1}, Lcom/a/b/d/sz;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    .line 186
    invoke-static {v0, v1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/a/b/d/fx;->b()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 192
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_33

    .line 193
    check-cast p1, Ljava/util/Map$Entry;

    .line 194
    iget-object v0, p0, Lcom/a/b/d/fx;->a:Lcom/a/b/d/fu;

    iget-object v0, v0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/d/vi;->f(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/a/b/d/fx;->a:Lcom/a/b/d/fu;

    iget-object v0, v0, Lcom/a/b/d/fu;->b:Lcom/a/b/b/co;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 197
    iget-object v0, p0, Lcom/a/b/d/fx;->a:Lcom/a/b/d/fu;

    iget-object v0, v0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 200
    :goto_32
    return v0

    :cond_33
    const/4 v0, 0x0

    goto :goto_32
.end method
