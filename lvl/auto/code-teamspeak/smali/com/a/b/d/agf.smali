.class final Lcom/a/b/d/agf;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/aga;

.field private final b:Ljava/util/ArrayDeque;


# direct methods
.method constructor <init>(Lcom/a/b/d/aga;Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 147
    iput-object p1, p0, Lcom/a/b/d/agf;->a:Lcom/a/b/d/aga;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    .line 148
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/a/b/d/agf;->b:Ljava/util/ArrayDeque;

    .line 149
    iget-object v0, p0, Lcom/a/b/d/agf;->b:Ljava/util/ArrayDeque;

    invoke-direct {p0, p2}, Lcom/a/b/d/agf;->a(Ljava/lang/Object;)Lcom/a/b/d/agg;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 150
    return-void
.end method

.method private a(Ljava/lang/Object;)Lcom/a/b/d/agg;
    .registers 4

    .prologue
    .line 168
    new-instance v0, Lcom/a/b/d/agg;

    iget-object v1, p0, Lcom/a/b/d/agf;->a:Lcom/a/b/d/aga;

    invoke-virtual {v1, p1}, Lcom/a/b/d/aga;->a(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/a/b/d/agg;-><init>(Ljava/lang/Object;Ljava/util/Iterator;)V

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 154
    :goto_0
    iget-object v0, p0, Lcom/a/b/d/agf;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_30

    .line 155
    iget-object v0, p0, Lcom/a/b/d/agf;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/agg;

    .line 156
    iget-object v1, v0, Lcom/a/b/d/agg;->b:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 157
    iget-object v0, v0, Lcom/a/b/d/agg;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/a/b/d/agf;->b:Ljava/util/ArrayDeque;

    invoke-direct {p0, v0}, Lcom/a/b/d/agf;->a(Ljava/lang/Object;)Lcom/a/b/d/agg;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    goto :goto_0

    .line 160
    :cond_28
    iget-object v1, p0, Lcom/a/b/d/agf;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->removeLast()Ljava/lang/Object;

    .line 161
    iget-object v0, v0, Lcom/a/b/d/agg;->a:Ljava/lang/Object;

    .line 164
    :goto_2f
    return-object v0

    :cond_30
    invoke-virtual {p0}, Lcom/a/b/d/agf;->b()Ljava/lang/Object;

    move-result-object v0

    goto :goto_2f
.end method
