.class final Lcom/a/b/d/tw;
.super Lcom/a/b/d/ty;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/bw;


# instance fields
.field private final d:Lcom/a/b/d/bw;


# direct methods
.method constructor <init>(Lcom/a/b/d/bw;Lcom/a/b/b/co;)V
    .registers 6

    .prologue
    .line 3045
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/ty;-><init>(Ljava/util/Map;Lcom/a/b/b/co;)V

    .line 3046
    new-instance v0, Lcom/a/b/d/tw;

    invoke-interface {p1}, Lcom/a/b/d/bw;->b()Lcom/a/b/d/bw;

    move-result-object v1

    .line 4034
    new-instance v2, Lcom/a/b/d/tx;

    invoke-direct {v2, p2}, Lcom/a/b/d/tx;-><init>(Lcom/a/b/b/co;)V

    .line 3046
    invoke-direct {v0, v1, v2, p0}, Lcom/a/b/d/tw;-><init>(Lcom/a/b/d/bw;Lcom/a/b/b/co;Lcom/a/b/d/bw;)V

    iput-object v0, p0, Lcom/a/b/d/tw;->d:Lcom/a/b/d/bw;

    .line 3048
    return-void
.end method

.method private constructor <init>(Lcom/a/b/d/bw;Lcom/a/b/b/co;Lcom/a/b/d/bw;)V
    .registers 4

    .prologue
    .line 3053
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/ty;-><init>(Ljava/util/Map;Lcom/a/b/b/co;)V

    .line 3054
    iput-object p3, p0, Lcom/a/b/d/tw;->d:Lcom/a/b/d/bw;

    .line 3055
    return-void
.end method

.method private static a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
    .registers 2

    .prologue
    .line 3034
    new-instance v0, Lcom/a/b/d/tx;

    invoke-direct {v0, p0}, Lcom/a/b/d/tx;-><init>(Lcom/a/b/b/co;)V

    return-object v0
.end method

.method private d()Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 3058
    iget-object v0, p0, Lcom/a/b/d/tw;->a:Ljava/util/Map;

    check-cast v0, Lcom/a/b/d/bw;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3063
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/tw;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 4058
    iget-object v0, p0, Lcom/a/b/d/tw;->a:Ljava/util/Map;

    check-cast v0, Lcom/a/b/d/bw;

    .line 3064
    invoke-interface {v0, p1, p2}, Lcom/a/b/d/bw;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 3069
    iget-object v0, p0, Lcom/a/b/d/tw;->d:Lcom/a/b/d/bw;

    return-object v0
.end method

.method public final j_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3074
    iget-object v0, p0, Lcom/a/b/d/tw;->d:Lcom/a/b/d/bw;

    invoke-interface {v0}, Lcom/a/b/d/bw;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 3028
    invoke-virtual {p0}, Lcom/a/b/d/tw;->j_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
