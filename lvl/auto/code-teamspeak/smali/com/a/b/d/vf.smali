.class final Lcom/a/b/d/vf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/yd;

.field b:Lcom/a/b/d/vf;

.field final synthetic c:Lcom/a/b/d/vc;


# direct methods
.method constructor <init>(Lcom/a/b/d/vc;Lcom/a/b/d/yd;)V
    .registers 3

    .prologue
    .line 492
    iput-object p1, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 493
    iput-object p2, p0, Lcom/a/b/d/vf;->a:Lcom/a/b/d/yd;

    .line 494
    return-void
.end method

.method private a(I)I
    .registers 4

    .prologue
    .line 591
    .line 10726
    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 591
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/vf;->b(II)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 652
    iget-object v0, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v0}, Lcom/a/b/d/vc;->b(Lcom/a/b/d/vc;)I

    move-result v0

    .line 16734
    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v1, v0, 0x2

    .line 653
    if-eqz v1, :cond_47

    .line 17734
    add-int/lit8 v0, v1, -0x1

    div-int/lit8 v0, v0, 0x2

    .line 18730
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x2

    .line 656
    if-eq v0, v1, :cond_47

    .line 19726
    mul-int/lit8 v1, v0, 0x2

    add-int/lit8 v1, v1, 0x1

    .line 656
    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->b(Lcom/a/b/d/vc;)I

    move-result v2

    if-lt v1, v2, :cond_47

    .line 658
    iget-object v1, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 20290
    iget-object v1, v1, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    .line 659
    iget-object v2, p0, Lcom/a/b/d/vf;->a:Lcom/a/b/d/yd;

    invoke-virtual {v2, v1, p1}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_47

    .line 660
    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v2

    aput-object p1, v2, v0

    .line 661
    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v3}, Lcom/a/b/d/vc;->b(Lcom/a/b/d/vc;)I

    move-result v3

    aput-object v1, v2, v3

    .line 666
    :goto_46
    return v0

    :cond_47
    iget-object v0, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v0}, Lcom/a/b/d/vc;->b(Lcom/a/b/d/vc;)I

    move-result v0

    goto :goto_46
.end method

.method private a(IILjava/lang/Object;)Lcom/a/b/d/vg;
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 507
    .line 3726
    mul-int/lit8 v0, p2, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 3591
    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/a/b/d/vf;->b(II)I

    move-result v0

    .line 2679
    if-lez v0, :cond_35

    iget-object v2, p0, Lcom/a/b/d/vf;->a:Lcom/a/b/d/yd;

    iget-object v3, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 4290
    iget-object v3, v3, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v3, v3, v0

    .line 2679
    invoke-virtual {v2, v3, p3}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_35

    .line 2681
    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 5290
    iget-object v3, v3, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v3, v3, v0

    .line 2681
    aput-object v3, v2, p2

    .line 2682
    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v2

    aput-object p3, v2, v0

    move v2, v0

    .line 508
    :goto_31
    if-ne v2, p2, :cond_3b

    move-object v0, v1

    .line 529
    :goto_34
    return-object v0

    .line 2685
    :cond_35
    invoke-virtual {p0, p2, p3}, Lcom/a/b/d/vf;->b(ILjava/lang/Object;)I

    move-result v0

    move v2, v0

    goto :goto_31

    .line 517
    :cond_3b
    if-ge v2, p1, :cond_52

    .line 520
    iget-object v0, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 6290
    iget-object v0, v0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v0, v0, p1

    .line 525
    :goto_43
    iget-object v3, p0, Lcom/a/b/d/vf;->b:Lcom/a/b/d/vf;

    invoke-virtual {v3, v2, p3}, Lcom/a/b/d/vf;->a(ILjava/lang/Object;)I

    move-result v2

    if-ge v2, p1, :cond_5d

    .line 527
    new-instance v1, Lcom/a/b/d/vg;

    invoke-direct {v1, p3, v0}, Lcom/a/b/d/vg;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_34

    .line 522
    :cond_52
    iget-object v0, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 6734
    add-int/lit8 v3, p1, -0x1

    div-int/lit8 v3, v3, 0x2

    .line 7290
    iget-object v0, v0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v0, v0, v3

    goto :goto_43

    :cond_5d
    move-object v0, v1

    .line 529
    goto :goto_34
.end method

.method private static synthetic a(Lcom/a/b/d/vf;I)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 31706
    .line 31726
    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    .line 31706
    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->b(Lcom/a/b/d/vc;)I

    move-result v2

    if-ge v1, v2, :cond_18

    .line 32726
    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    .line 31706
    invoke-virtual {p0, p1, v1}, Lcom/a/b/d/vf;->a(II)I

    move-result v1

    if-lez v1, :cond_18

    .line 31718
    :cond_17
    :goto_17
    return v0

    .line 32730
    :cond_18
    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x2

    .line 31710
    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->b(Lcom/a/b/d/vc;)I

    move-result v2

    if-ge v1, v2, :cond_2e

    .line 33730
    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x2

    .line 31710
    invoke-virtual {p0, p1, v1}, Lcom/a/b/d/vf;->a(II)I

    move-result v1

    if-gtz v1, :cond_17

    .line 31714
    :cond_2e
    if-lez p1, :cond_3a

    .line 33734
    add-int/lit8 v1, p1, -0x1

    div-int/lit8 v1, v1, 0x2

    .line 31714
    invoke-virtual {p0, p1, v1}, Lcom/a/b/d/vf;->a(II)I

    move-result v1

    if-gtz v1, :cond_17

    .line 31717
    :cond_3a
    const/4 v1, 0x2

    if-le p1, v1, :cond_4b

    .line 34734
    add-int/lit8 v1, p1, -0x1

    div-int/lit8 v1, v1, 0x2

    .line 35734
    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x2

    .line 31717
    invoke-virtual {p0, v1, p1}, Lcom/a/b/d/vf;->a(II)I

    move-result v1

    if-gtz v1, :cond_17

    .line 31720
    :cond_4b
    const/4 v0, 0x1

    .line 488
    goto :goto_17
.end method

.method private b(I)I
    .registers 4

    .prologue
    .line 598
    .line 11726
    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 599
    if-gez v0, :cond_8

    .line 600
    const/4 v0, -0x1

    .line 602
    :goto_7
    return v0

    .line 12726
    :cond_8
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 602
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/vf;->b(II)I

    move-result v0

    goto :goto_7
.end method

.method private c(I)I
    .registers 5

    .prologue
    .line 698
    .line 22726
    :goto_0
    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 22599
    if-gez v0, :cond_19

    .line 22600
    const/4 v0, -0x1

    .line 698
    :goto_7
    if-lez v0, :cond_23

    .line 699
    iget-object v1, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v1}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 24290
    iget-object v2, v2, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v2, v2, v0

    .line 699
    aput-object v2, v1, p1

    move p1, v0

    .line 700
    goto :goto_0

    .line 23726
    :cond_19
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 22602
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/vf;->b(II)I

    move-result v0

    goto :goto_7

    .line 702
    :cond_23
    return p1
.end method

.method private c(ILjava/lang/Object;)V
    .registers 4

    .prologue
    .line 537
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/vf;->b(ILjava/lang/Object;)I

    move-result v0

    .line 540
    if-ne v0, p1, :cond_a

    .line 546
    :goto_6
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/vf;->a(ILjava/lang/Object;)I

    .line 547
    return-void

    .line 544
    :cond_a
    iget-object p0, p0, Lcom/a/b/d/vf;->b:Lcom/a/b/d/vf;

    move p1, v0

    goto :goto_6
.end method

.method private d(ILjava/lang/Object;)I
    .registers 6

    .prologue
    .line 676
    .line 20726
    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 20591
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/vf;->b(II)I

    move-result v0

    .line 679
    if-lez v0, :cond_30

    iget-object v1, p0, Lcom/a/b/d/vf;->a:Lcom/a/b/d/yd;

    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 21290
    iget-object v2, v2, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v2, v2, v0

    .line 679
    invoke-virtual {v1, v2, p2}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-gez v1, :cond_30

    .line 681
    iget-object v1, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v1}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 22290
    iget-object v2, v2, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v2, v2, v0

    .line 681
    aput-object v2, v1, p1

    .line 682
    iget-object v1, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v1}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v1

    aput-object p2, v1, v0

    .line 685
    :goto_2f
    return v0

    :cond_30
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/vf;->b(ILjava/lang/Object;)I

    move-result v0

    goto :goto_2f
.end method

.method private d(I)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 706
    .line 24726
    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    .line 706
    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->b(Lcom/a/b/d/vc;)I

    move-result v2

    if-ge v1, v2, :cond_18

    .line 25726
    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    .line 706
    invoke-virtual {p0, p1, v1}, Lcom/a/b/d/vf;->a(II)I

    move-result v1

    if-lez v1, :cond_18

    .line 720
    :cond_17
    :goto_17
    return v0

    .line 25730
    :cond_18
    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x2

    .line 710
    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->b(Lcom/a/b/d/vc;)I

    move-result v2

    if-ge v1, v2, :cond_2e

    .line 26730
    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x2

    .line 710
    invoke-virtual {p0, p1, v1}, Lcom/a/b/d/vf;->a(II)I

    move-result v1

    if-gtz v1, :cond_17

    .line 714
    :cond_2e
    if-lez p1, :cond_3a

    .line 26734
    add-int/lit8 v1, p1, -0x1

    div-int/lit8 v1, v1, 0x2

    .line 714
    invoke-virtual {p0, p1, v1}, Lcom/a/b/d/vf;->a(II)I

    move-result v1

    if-gtz v1, :cond_17

    .line 717
    :cond_3a
    const/4 v1, 0x2

    if-le p1, v1, :cond_4b

    .line 27734
    add-int/lit8 v1, p1, -0x1

    div-int/lit8 v1, v1, 0x2

    .line 28734
    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x2

    .line 717
    invoke-virtual {p0, v1, p1}, Lcom/a/b/d/vf;->a(II)I

    move-result v1

    if-gtz v1, :cond_17

    .line 720
    :cond_4b
    const/4 v0, 0x1

    goto :goto_17
.end method

.method private static e(I)I
    .registers 2

    .prologue
    .line 726
    mul-int/lit8 v0, p0, 0x2

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private static f(I)I
    .registers 2

    .prologue
    .line 730
    mul-int/lit8 v0, p0, 0x2

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private static g(I)I
    .registers 2

    .prologue
    .line 734
    add-int/lit8 v0, p0, -0x1

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private static h(I)I
    .registers 2

    .prologue
    .line 738
    .line 29734
    add-int/lit8 v0, p0, -0x1

    div-int/lit8 v0, v0, 0x2

    .line 30734
    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x2

    .line 738
    return v0
.end method


# virtual methods
.method final a(II)I
    .registers 6

    .prologue
    .line 497
    iget-object v0, p0, Lcom/a/b/d/vf;->a:Lcom/a/b/d/yd;

    iget-object v1, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 1290
    iget-object v1, v1, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v1, v1, p1

    .line 497
    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 2290
    iget-object v2, v2, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v2, v2, p2

    .line 497
    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method final a(ILjava/lang/Object;)I
    .registers 6

    .prologue
    .line 554
    :goto_0
    const/4 v0, 0x2

    if-le p1, v0, :cond_23

    .line 8734
    add-int/lit8 v0, p1, -0x1

    div-int/lit8 v0, v0, 0x2

    .line 9734
    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x2

    .line 556
    iget-object v1, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 10290
    iget-object v1, v1, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    .line 557
    iget-object v2, p0, Lcom/a/b/d/vf;->a:Lcom/a/b/d/yd;

    invoke-virtual {v2, v1, p2}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_23

    .line 560
    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v2

    aput-object v1, v2, p1

    move p1, v0

    .line 562
    goto :goto_0

    .line 563
    :cond_23
    iget-object v0, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v0}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v0

    aput-object p2, v0, p1

    .line 564
    return p1
.end method

.method final b(II)I
    .registers 7

    .prologue
    .line 573
    iget-object v0, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v0}, Lcom/a/b/d/vc;->b(Lcom/a/b/d/vc;)I

    move-result v0

    if-lt p1, v0, :cond_a

    .line 574
    const/4 v0, -0x1

    .line 584
    :cond_9
    return v0

    .line 576
    :cond_a
    if-lez p1, :cond_2c

    const/4 v0, 0x1

    :goto_d
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 577
    iget-object v0, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v0}, Lcom/a/b/d/vc;->b(Lcom/a/b/d/vc;)I

    move-result v0

    sub-int/2addr v0, p2

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int v2, v0, p2

    .line 579
    add-int/lit8 v1, p1, 0x1

    move v0, p1

    :goto_20
    if-ge v1, v2, :cond_9

    .line 580
    invoke-virtual {p0, v1, v0}, Lcom/a/b/d/vf;->a(II)I

    move-result v3

    if-gez v3, :cond_29

    move v0, v1

    .line 579
    :cond_29
    add-int/lit8 v1, v1, 0x1

    goto :goto_20

    .line 576
    :cond_2c
    const/4 v0, 0x0

    goto :goto_d
.end method

.method final b(ILjava/lang/Object;)I
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 611
    if-nez p1, :cond_c

    .line 612
    iget-object v0, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v0}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v0

    aput-object p2, v0, v1

    .line 639
    :goto_b
    return v1

    .line 12734
    :cond_c
    add-int/lit8 v0, p1, -0x1

    div-int/lit8 v3, v0, 0x2

    .line 616
    iget-object v0, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 13290
    iget-object v0, v0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v1, v0, v3

    .line 617
    if-eqz v3, :cond_60

    .line 13734
    add-int/lit8 v0, v3, -0x1

    div-int/lit8 v0, v0, 0x2

    .line 14730
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v2, v0, 0x2

    .line 624
    if-eq v2, v3, :cond_60

    .line 15726
    mul-int/lit8 v0, v2, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 624
    iget-object v4, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v4}, Lcom/a/b/d/vc;->b(Lcom/a/b/d/vc;)I

    move-result v4

    if-lt v0, v4, :cond_60

    .line 626
    iget-object v0, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 16290
    iget-object v0, v0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v0, v0, v2

    .line 627
    iget-object v4, p0, Lcom/a/b/d/vf;->a:Lcom/a/b/d/yd;

    invoke-virtual {v4, v0, v1}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-gez v4, :cond_60

    move v1, v2

    .line 633
    :goto_3d
    iget-object v2, p0, Lcom/a/b/d/vf;->a:Lcom/a/b/d/yd;

    invoke-virtual {v2, v0, p2}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_56

    .line 634
    iget-object v2, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v2

    aput-object v0, v2, p1

    .line 635
    iget-object v0, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v0}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v0

    aput-object p2, v0, v1

    goto :goto_b

    .line 638
    :cond_56
    iget-object v0, p0, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    invoke-static {v0}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v0

    aput-object p2, v0, p1

    move v1, p1

    .line 639
    goto :goto_b

    :cond_60
    move-object v0, v1

    move v1, v3

    goto :goto_3d
.end method
