.class final Lcom/a/b/d/ds;
.super Lcom/a/b/d/hi;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Set;

.field private final b:Lcom/a/b/d/dm;


# direct methods
.method public constructor <init>(Ljava/util/Set;Lcom/a/b/d/dm;)V
    .registers 4

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/a/b/d/hi;-><init>()V

    .line 102
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lcom/a/b/d/ds;->a:Ljava/util/Set;

    .line 103
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dm;

    iput-object v0, p0, Lcom/a/b/d/ds;->b:Lcom/a/b/d/dm;

    .line 104
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/a/b/d/ds;->a:Ljava/util/Set;

    return-object v0
.end method

.method public final add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 109
    iget-object v0, p0, Lcom/a/b/d/ds;->b:Lcom/a/b/d/dm;

    invoke-interface {v0, p1}, Lcom/a/b/d/dm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v0, p0, Lcom/a/b/d/ds;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 113
    iget-object v0, p0, Lcom/a/b/d/ds;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/a/b/d/ds;->b:Lcom/a/b/d/dm;

    invoke-static {p1, v1}, Lcom/a/b/d/dn;->b(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method protected final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 97
    .line 1106
    iget-object v0, p0, Lcom/a/b/d/ds;->a:Ljava/util/Set;

    .line 97
    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 97
    .line 2106
    iget-object v0, p0, Lcom/a/b/d/ds;->a:Ljava/util/Set;

    .line 97
    return-object v0
.end method
