.class abstract Lcom/a/b/d/n;
.super Lcom/a/b/d/an;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final c:J = 0x21f766b1f568c81dL


# instance fields
.field private transient a:Ljava/util/Map;

.field private transient b:I


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .registers 3

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/a/b/d/an;-><init>()V

    .line 123
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 124
    iput-object p1, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    .line 125
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/n;I)I
    .registers 3

    .prologue
    .line 91
    iget v0, p0, Lcom/a/b/d/n;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/a/b/d/n;->b:I

    return v0
.end method

.method static synthetic a(Lcom/a/b/d/n;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 91
    .line 3111
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 3113
    const/4 v1, 0x0

    .line 3114
    if-eqz v0, :cond_17

    .line 3115
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    .line 3116
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 3117
    iget v0, p0, Lcom/a/b/d/n;->b:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/a/b/d/n;->b:I

    :cond_17
    move v0, v1

    .line 91
    return v0
.end method

.method static synthetic a(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)Ljava/util/List;
    .registers 5

    .prologue
    .line 91
    invoke-direct {p0, p1, p2, p3}, Lcom/a/b/d/n;->a(Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)Ljava/util/List;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/a/b/d/ab;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 332
    instance-of v0, p2, Ljava/util/RandomAccess;

    if-eqz v0, :cond_a

    new-instance v0, Lcom/a/b/d/y;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/a/b/d/y;-><init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)V

    :goto_9
    return-object v0

    :cond_a
    new-instance v0, Lcom/a/b/d/ad;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/a/b/d/ad;-><init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)V

    goto :goto_9
.end method

.method static synthetic a(Lcom/a/b/d/n;)Ljava/util/Map;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/a/b/d/n;)I
    .registers 3

    .prologue
    .line 91
    iget v0, p0, Lcom/a/b/d/n;->b:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/a/b/d/n;->b:I

    return v0
.end method

.method static synthetic b(Lcom/a/b/d/n;I)I
    .registers 3

    .prologue
    .line 91
    iget v0, p0, Lcom/a/b/d/n;->b:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/a/b/d/n;->b:I

    return v0
.end method

.method static synthetic b(Ljava/util/Collection;)Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 91
    .line 2595
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_a
.end method

.method static synthetic c(Lcom/a/b/d/n;)I
    .registers 3

    .prologue
    .line 91
    iget v0, p0, Lcom/a/b/d/n;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/a/b/d/n;->b:I

    return v0
.end method

.method private static c(Ljava/util/Collection;)Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 595
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_a
.end method

.method private j(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 211
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 212
    if-nez v0, :cond_13

    .line 213
    invoke-virtual {p0, p1}, Lcom/a/b/d/n;->e(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 214
    iget-object v1, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    :cond_13
    return-object v0
.end method

.method private k(Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 1111
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 1113
    const/4 v1, 0x0

    .line 1114
    if-eqz v0, :cond_17

    .line 1115
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    .line 1116
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1117
    iget v0, p0, Lcom/a/b/d/n;->b:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/a/b/d/n;->b:I

    :cond_17
    move v0, v1

    .line 1119
    return v0
.end method


# virtual methods
.method a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 319
    instance-of v0, p2, Ljava/util/SortedSet;

    if-eqz v0, :cond_d

    .line 320
    new-instance v0, Lcom/a/b/d/ah;

    check-cast p2, Ljava/util/SortedSet;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/a/b/d/ah;-><init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/a/b/d/ab;)V

    .line 326
    :goto_c
    return-object v0

    .line 321
    :cond_d
    instance-of v0, p2, Ljava/util/Set;

    if-eqz v0, :cond_19

    .line 322
    new-instance v0, Lcom/a/b/d/ag;

    check-cast p2, Ljava/util/Set;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/d/ag;-><init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/Set;)V

    goto :goto_c

    .line 323
    :cond_19
    instance-of v0, p2, Ljava/util/List;

    if-eqz v0, :cond_24

    .line 324
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p1, p2, v1}, Lcom/a/b/d/n;->a(Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)Ljava/util/List;

    move-result-object v0

    goto :goto_c

    .line 326
    :cond_24
    new-instance v0, Lcom/a/b/d/ab;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/a/b/d/ab;-><init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/Collection;Lcom/a/b/d/ab;)V

    goto :goto_c
.end method

.method a(Ljava/util/Collection;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 274
    instance-of v0, p1, Ljava/util/SortedSet;

    if-eqz v0, :cond_b

    .line 275
    check-cast p1, Ljava/util/SortedSet;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    .line 281
    :goto_a
    return-object v0

    .line 276
    :cond_b
    instance-of v0, p1, Ljava/util/Set;

    if-eqz v0, :cond_16

    .line 277
    check-cast p1, Ljava/util/Set;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_a

    .line 278
    :cond_16
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_21

    .line 279
    check-cast p1, Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_a

    .line 281
    :cond_21
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_a
.end method

.method final a(Ljava/util/Map;)V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 129
    iput-object p1, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    .line 130
    iput v2, p0, Lcom/a/b/d/n;->b:I

    .line 131
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 132
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2d

    const/4 v1, 0x1

    :goto_20
    invoke-static {v1}, Lcom/a/b/b/cn;->a(Z)V

    .line 133
    iget v1, p0, Lcom/a/b/d/n;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/b/d/n;->b:I

    goto :goto_d

    :cond_2d
    move v1, v2

    .line 132
    goto :goto_20

    .line 135
    :cond_2f
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 192
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 193
    if-nez v0, :cond_2a

    .line 194
    invoke-virtual {p0, p1}, Lcom/a/b/d/n;->e(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 195
    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 196
    iget v2, p0, Lcom/a/b/d/n;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/a/b/d/n;->b:I

    .line 197
    iget-object v2, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 206
    :goto_21
    return v0

    .line 200
    :cond_22
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "New Collection violated the Collection spec"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 202
    :cond_2a
    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 203
    iget v0, p0, Lcom/a/b/d/n;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/n;->b:I

    move v0, v1

    .line 204
    goto :goto_21

    .line 206
    :cond_38
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 228
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 229
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_f

    .line 230
    invoke-virtual {p0, p1}, Lcom/a/b/d/n;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 247
    :goto_e
    return-object v0

    .line 2211
    :cond_f
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2212
    if-nez v0, :cond_22

    .line 2213
    invoke-virtual {p0, p1}, Lcom/a/b/d/n;->e(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 2214
    iget-object v2, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    :cond_22
    invoke-virtual {p0}, Lcom/a/b/d/n;->c()Ljava/util/Collection;

    move-result-object v2

    .line 236
    invoke-interface {v2, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 238
    iget v3, p0, Lcom/a/b/d/n;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/a/b/d/n;->b:I

    .line 239
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 241
    :cond_35
    :goto_35
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4c

    .line 242
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_35

    .line 243
    iget v3, p0, Lcom/a/b/d/n;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/a/b/d/n;->b:I

    goto :goto_35

    .line 247
    :cond_4c
    invoke-virtual {p0, v2}, Lcom/a/b/d/n;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_e
.end method

.method abstract c()Ljava/util/Collection;
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 304
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 305
    if-nez v0, :cond_e

    .line 306
    invoke-virtual {p0, p1}, Lcom/a/b/d/n;->e(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 308
    :cond_e
    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/n;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method d()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/a/b/d/n;->c()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/n;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 257
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 259
    if-nez v0, :cond_f

    .line 260
    invoke-virtual {p0}, Lcom/a/b/d/n;->d()Ljava/util/Collection;

    move-result-object v0

    .line 268
    :goto_e
    return-object v0

    .line 263
    :cond_f
    invoke-virtual {p0}, Lcom/a/b/d/n;->c()Ljava/util/Collection;

    move-result-object v1

    .line 264
    invoke-interface {v1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 265
    iget v2, p0, Lcom/a/b/d/n;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/a/b/d/n;->b:I

    .line 266
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 268
    invoke-virtual {p0, v1}, Lcom/a/b/d/n;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_e
.end method

.method e(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/a/b/d/n;->c()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method e()Ljava/util/Map;
    .registers 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    return-object v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 180
    iget v0, p0, Lcom/a/b/d/n;->b:I

    return v0
.end method

.method public f(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 185
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public g()V
    .registers 3

    .prologue
    .line 288
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 289
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    goto :goto_a

    .line 291
    :cond_1a
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 292
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/d/n;->b:I

    .line 293
    return-void
.end method

.method h()Ljava/util/Set;
    .registers 3

    .prologue
    .line 915
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_11

    new-instance v1, Lcom/a/b/d/aa;

    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/aa;-><init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V

    move-object v0, v1

    :goto_10
    return-object v0

    :cond_11
    new-instance v0, Lcom/a/b/d/u;

    iget-object v1, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/u;-><init>(Lcom/a/b/d/n;Ljava/util/Map;)V

    goto :goto_10
.end method

.method public i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1170
    invoke-super {p0}, Lcom/a/b/d/an;->i()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method j()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1175
    new-instance v0, Lcom/a/b/d/o;

    invoke-direct {v0, p0}, Lcom/a/b/d/o;-><init>(Lcom/a/b/d/n;)V

    return-object v0
.end method

.method public k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1201
    invoke-super {p0}, Lcom/a/b/d/an;->k()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method l()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1214
    new-instance v0, Lcom/a/b/d/p;

    invoke-direct {v0, p0}, Lcom/a/b/d/p;-><init>(Lcom/a/b/d/n;)V

    return-object v0
.end method

.method m()Ljava/util/Map;
    .registers 3

    .prologue
    .line 1226
    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_11

    new-instance v1, Lcom/a/b/d/z;

    iget-object v0, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/z;-><init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V

    move-object v0, v1

    :goto_10
    return-object v0

    :cond_11
    new-instance v0, Lcom/a/b/d/q;

    iget-object v1, p0, Lcom/a/b/d/n;->a:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/q;-><init>(Lcom/a/b/d/n;Ljava/util/Map;)V

    goto :goto_10
.end method
