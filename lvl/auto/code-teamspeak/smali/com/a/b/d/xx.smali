.class public final Lcom/a/b/d/xx;
.super Lcom/a/b/d/qb;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/ck;


# static fields
.field private static final a:Lcom/a/b/d/pn;

.field private static final c:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 63
    new-instance v0, Lcom/a/b/d/xy;

    invoke-direct {v0}, Lcom/a/b/d/xy;-><init>()V

    sput-object v0, Lcom/a/b/d/xx;->a:Lcom/a/b/d/pn;

    return-void
.end method

.method private constructor <init>(Ljava/util/Map;)V
    .registers 3

    .prologue
    .line 60
    sget-object v0, Lcom/a/b/d/xx;->a:Lcom/a/b/d/pn;

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/qb;-><init>(Ljava/util/Map;Lcom/a/b/d/pn;)V

    .line 61
    return-void
.end method

.method private static a(Ljava/util/Map;)Lcom/a/b/d/xx;
    .registers 2

    .prologue
    .line 56
    new-instance v0, Lcom/a/b/d/xx;

    invoke-direct {v0, p0}, Lcom/a/b/d/xx;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method private static b()Lcom/a/b/d/xx;
    .registers 2

    .prologue
    .line 45
    new-instance v0, Lcom/a/b/d/xx;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v0, v1}, Lcom/a/b/d/xx;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 36
    invoke-static {p0, p1}, Lcom/a/b/d/xx;->c(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 82
    invoke-static {p0}, Lcom/a/b/l/z;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 78
    invoke-virtual {p0, p1}, Lcom/a/b/d/xx;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/xx;->c(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 73
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/xx;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/xx;->c(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 36
    invoke-super {p0}, Lcom/a/b/d/qb;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic putAll(Ljava/util/Map;)V
    .registers 2

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/a/b/d/qb;->putAll(Ljava/util/Map;)V

    return-void
.end method
