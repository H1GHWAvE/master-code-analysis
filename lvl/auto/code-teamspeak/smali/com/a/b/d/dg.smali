.class public final Lcom/a/b/d/dg;
.super Lcom/a/b/d/as;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final b:J = 0x1L


# instance fields
.field private final transient a:Ljava/util/concurrent/ConcurrentMap;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/ConcurrentMap;)V
    .registers 3
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/a/b/d/as;-><init>()V

    .line 141
    invoke-interface {p1}, Ljava/util/concurrent/ConcurrentMap;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 142
    iput-object p1, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    .line 143
    return-void
.end method

.method private static a(Lcom/a/b/d/ql;)Lcom/a/b/d/dg;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 127
    new-instance v0, Lcom/a/b/d/dg;

    invoke-virtual {p0}, Lcom/a/b/d/ql;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/dg;-><init>(Ljava/util/concurrent/ConcurrentMap;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/dg;
    .registers 3

    .prologue
    .line 1087
    new-instance v0, Lcom/a/b/d/dg;

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-direct {v0, v1}, Lcom/a/b/d/dg;-><init>(Ljava/util/concurrent/ConcurrentMap;)V

    .line 100
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 101
    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/dg;)Ljava/util/concurrent/ConcurrentMap;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 4

    .prologue
    .line 567
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 569
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    .line 571
    sget-object v1, Lcom/a/b/d/dl;->a:Lcom/a/b/d/aab;

    invoke-virtual {v1, p0, v0}, Lcom/a/b/d/aab;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 572
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3

    .prologue
    .line 562
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 563
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 564
    return-void
.end method

.method private d(Ljava/lang/Object;I)Z
    .registers 9
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 316
    if-nez p2, :cond_5

    .line 337
    :cond_4
    :goto_4
    return v1

    .line 319
    :cond_5
    if-lez p2, :cond_21

    move v0, v1

    :goto_8
    const-string v3, "Invalid occurrences: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 321
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 322
    if-nez v0, :cond_23

    move v1, v2

    .line 323
    goto :goto_4

    :cond_21
    move v0, v2

    .line 319
    goto :goto_8

    .line 326
    :cond_23
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    .line 327
    if-ge v3, p2, :cond_2b

    move v1, v2

    .line 328
    goto :goto_4

    .line 330
    :cond_2b
    sub-int v4, v3, p2

    .line 331
    invoke-virtual {v0, v3, v4}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 332
    if-nez v4, :cond_4

    .line 335
    iget-object v2, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_4
.end method

.method private static g()Lcom/a/b/d/dg;
    .registers 2

    .prologue
    .line 87
    new-instance v0, Lcom/a/b/d/dg;

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-direct {v0, v1}, Lcom/a/b/d/dg;-><init>(Ljava/util/concurrent/ConcurrentMap;)V

    return-object v0
.end method

.method private h()Ljava/util/List;
    .registers 5

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/a/b/d/dg;->size()I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/ov;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 191
    invoke-virtual {p0}, Lcom/a/b/d/dg;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 192
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v3

    .line 193
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    :goto_24
    if-lez v0, :cond_10

    .line 194
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    add-int/lit8 v0, v0, -0x1

    goto :goto_24

    .line 197
    :cond_2c
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 154
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 155
    if-nez v0, :cond_c

    const/4 v0, 0x0

    :goto_b
    return v0

    :cond_c
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    goto :goto_b
.end method

.method public final a(Ljava/lang/Object;I)I
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 212
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    if-nez p2, :cond_c

    .line 214
    invoke-virtual {p0, p1}, Lcom/a/b/d/dg;->a(Ljava/lang/Object;)I

    move-result v2

    .line 248
    :cond_b
    :goto_b
    return v2

    .line 216
    :cond_c
    if-lez p2, :cond_47

    move v0, v1

    :goto_f
    const-string v3, "Invalid occurrences: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 219
    :cond_1c
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 220
    if-nez v0, :cond_35

    .line 221
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 222
    if-eqz v0, :cond_b

    .line 229
    :cond_35
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 230
    if-eqz v1, :cond_6f

    .line 232
    :try_start_3b
    invoke-static {v1, p2}, Lcom/a/b/j/g;->a(II)I

    move-result v3

    .line 233
    invoke-virtual {v0, v1, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z
    :try_end_42
    .catch Ljava/lang/ArithmeticException; {:try_start_3b .. :try_end_42} :catch_49

    move-result v3

    if-eqz v3, :cond_35

    move v2, v1

    .line 235
    goto :goto_b

    :cond_47
    move v0, v2

    .line 216
    goto :goto_f

    .line 238
    :catch_49
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x41

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Overflow adding "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " occurrences to a count of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245
    :cond_6f
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 246
    iget-object v3, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v3, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v3, p1, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    goto :goto_b
.end method

.method public final bridge synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 57
    invoke-super {p0}, Lcom/a/b/d/as;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;II)Z
    .registers 9

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 405
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    const-string v0, "oldCount"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 407
    const-string v0, "newCount"

    invoke-static {p3, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 409
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 410
    if-nez v0, :cond_32

    .line 411
    if-eqz p2, :cond_1d

    move v0, v1

    .line 443
    :goto_1c
    return v0

    .line 413
    :cond_1d
    if-nez p3, :cond_21

    move v0, v2

    .line 414
    goto :goto_1c

    .line 417
    :cond_21
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v3, p3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    invoke-interface {v0, p1, v3}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_30

    move v0, v2

    goto :goto_1c

    :cond_30
    move v0, v1

    goto :goto_1c

    .line 420
    :cond_32
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    .line 421
    if-ne v3, p2, :cond_6b

    .line 422
    if-nez v3, :cond_5c

    .line 423
    if-nez p3, :cond_43

    .line 425
    iget-object v1, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move v0, v2

    .line 426
    goto :goto_1c

    .line 428
    :cond_43
    new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v3, p3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 429
    iget-object v4, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, p1, v3}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_58

    iget-object v4, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, p1, v0, v3}, Ljava/util/concurrent/ConcurrentMap;->replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5a

    :cond_58
    move v0, v2

    goto :goto_1c

    :cond_5a
    move v0, v1

    goto :goto_1c

    .line 433
    :cond_5c
    invoke-virtual {v0, v3, p3}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v3

    if-eqz v3, :cond_6b

    .line 434
    if-nez p3, :cond_69

    .line 437
    iget-object v1, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    :cond_69
    move v0, v2

    .line 439
    goto :goto_1c

    :cond_6b
    move v0, v1

    .line 443
    goto :goto_1c
.end method

.method public final bridge synthetic add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/a/b/d/as;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic addAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/a/b/d/as;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 277
    if-nez p2, :cond_9

    .line 278
    invoke-virtual {p0, p1}, Lcom/a/b/d/dg;->a(Ljava/lang/Object;)I

    move-result v2

    .line 299
    :cond_8
    :goto_8
    return v2

    .line 280
    :cond_9
    if-lez p2, :cond_3e

    move v0, v1

    :goto_c
    const-string v3, "Invalid occurrences: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 282
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 283
    if-eqz v0, :cond_8

    .line 287
    :cond_23
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 288
    if-eqz v1, :cond_8

    .line 289
    sub-int v3, v1, p2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 290
    invoke-virtual {v0, v1, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 291
    if-nez v3, :cond_3c

    .line 294
    iget-object v2, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    :cond_3c
    move v2, v1

    .line 296
    goto :goto_8

    :cond_3e
    move v0, v2

    .line 280
    goto :goto_c
.end method

.method final b()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 490
    new-instance v0, Lcom/a/b/d/di;

    invoke-direct {v0, p0}, Lcom/a/b/d/di;-><init>(Lcom/a/b/d/dg;)V

    .line 508
    new-instance v1, Lcom/a/b/d/dj;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/dj;-><init>(Lcom/a/b/d/dg;Ljava/util/Iterator;)V

    return-object v1
.end method

.method final c()I
    .registers 2

    .prologue
    .line 480
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/Object;I)I
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 350
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    const-string v0, "count"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 353
    :cond_9
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 354
    if-nez v0, :cond_28

    .line 355
    if-nez p2, :cond_17

    move v0, v1

    .line 386
    :goto_16
    return v0

    .line 358
    :cond_17
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v2, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    invoke-interface {v0, p1, v2}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 359
    if-nez v0, :cond_28

    move v0, v1

    .line 360
    goto :goto_16

    .line 367
    :cond_28
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    .line 368
    if-nez v2, :cond_49

    .line 369
    if-nez p2, :cond_32

    move v0, v1

    .line 370
    goto :goto_16

    .line 372
    :cond_32
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v2, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 373
    iget-object v3, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v3, p1, v2}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_47

    iget-object v3, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v3, p1, v0, v2}, Ljava/util/concurrent/ConcurrentMap;->replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_47
    move v0, v1

    .line 375
    goto :goto_16

    .line 380
    :cond_49
    invoke-virtual {v0, v2, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 381
    if-nez p2, :cond_56

    .line 384
    iget-object v1, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    :cond_56
    move v0, v2

    .line 386
    goto :goto_16
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 529
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 530
    return-void
.end method

.method public final bridge synthetic contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/a/b/d/as;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final e()Ljava/util/Set;
    .registers 3

    .prologue
    .line 449
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 450
    new-instance v1, Lcom/a/b/d/dh;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/dh;-><init>(Lcom/a/b/d/dg;Ljava/util/Set;)V

    return-object v1
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/a/b/d/as;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final f()Ljava/util/Set;
    .registers 3

    .prologue
    .line 476
    new-instance v0, Lcom/a/b/d/dk;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/dk;-><init>(Lcom/a/b/d/dg;B)V

    return-object v0
.end method

.method public final bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 57
    invoke-super {p0}, Lcom/a/b/d/as;->hashCode()I

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 484
    iget-object v0, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 57
    invoke-super {p0}, Lcom/a/b/d/as;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 57
    invoke-super {p0}, Lcom/a/b/d/as;->n_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/a/b/d/as;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/a/b/d/as;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/a/b/d/as;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final size()I
    .registers 6

    .prologue
    .line 165
    const-wide/16 v0, 0x0

    .line 166
    iget-object v2, p0, Lcom/a/b/d/dg;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 167
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 168
    goto :goto_d

    .line 169
    :cond_21
    invoke-static {v2, v3}, Lcom/a/b/l/q;->b(J)I

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/a/b/d/dg;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/a/b/d/dg;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 57
    invoke-super {p0}, Lcom/a/b/d/as;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
