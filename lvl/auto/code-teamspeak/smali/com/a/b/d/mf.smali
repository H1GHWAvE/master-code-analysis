.class public final Lcom/a/b/d/mf;
.super Lcom/a/b/d/lp;
.source "SourceFile"


# instance fields
.field private final d:Ljava/util/Comparator;


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .registers 3

    .prologue
    .line 497
    invoke-direct {p0}, Lcom/a/b/d/lp;-><init>()V

    .line 498
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/mf;->d:Ljava/util/Comparator;

    .line 499
    return-void
.end method

.method private c(Ljava/lang/Iterable;)Lcom/a/b/d/mf;
    .registers 2

    .prologue
    .line 538
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->b(Ljava/lang/Iterable;)Lcom/a/b/d/lp;

    .line 539
    return-object p0
.end method


# virtual methods
.method public final synthetic a()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 490
    invoke-virtual {p0}, Lcom/a/b/d/mf;->c()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Lcom/a/b/d/ja;
    .registers 2

    .prologue
    .line 490
    .line 6512
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    .line 490
    return-object p0
.end method

.method public final synthetic a(Ljava/lang/Iterable;)Lcom/a/b/d/jb;
    .registers 2

    .prologue
    .line 490
    .line 4538
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->b(Ljava/lang/Iterable;)Lcom/a/b/d/lp;

    .line 490
    return-object p0
.end method

.method public final synthetic a(Ljava/util/Iterator;)Lcom/a/b/d/jb;
    .registers 2

    .prologue
    .line 490
    .line 6551
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->b(Ljava/util/Iterator;)Lcom/a/b/d/lp;

    .line 490
    return-object p0
.end method

.method public final synthetic a([Ljava/lang/Object;)Lcom/a/b/d/jb;
    .registers 2

    .prologue
    .line 490
    .line 5525
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->b([Ljava/lang/Object;)Lcom/a/b/d/lp;

    .line 490
    return-object p0
.end method

.method public final synthetic b(Ljava/lang/Object;)Lcom/a/b/d/jb;
    .registers 2

    .prologue
    .line 490
    .line 7512
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    .line 490
    return-object p0
.end method

.method public final synthetic b()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 490
    invoke-virtual {p0}, Lcom/a/b/d/mf;->c()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Iterable;)Lcom/a/b/d/lp;
    .registers 2

    .prologue
    .line 490
    .line 2538
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->b(Ljava/lang/Iterable;)Lcom/a/b/d/lp;

    .line 490
    return-object p0
.end method

.method public final bridge synthetic b(Ljava/util/Iterator;)Lcom/a/b/d/lp;
    .registers 2

    .prologue
    .line 490
    .line 1551
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->b(Ljava/util/Iterator;)Lcom/a/b/d/lp;

    .line 490
    return-object p0
.end method

.method public final bridge synthetic b([Ljava/lang/Object;)Lcom/a/b/d/lp;
    .registers 2

    .prologue
    .line 490
    .line 3525
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->b([Ljava/lang/Object;)Lcom/a/b/d/lp;

    .line 490
    return-object p0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)Lcom/a/b/d/lp;
    .registers 2

    .prologue
    .line 490
    .line 4512
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    .line 490
    return-object p0
.end method

.method public final c()Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 561
    iget-object v0, p0, Lcom/a/b/d/mf;->a:[Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 562
    iget-object v1, p0, Lcom/a/b/d/mf;->d:Ljava/util/Comparator;

    iget v2, p0, Lcom/a/b/d/mf;->b:I

    invoke-static {v1, v2, v0}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lcom/a/b/d/me;

    move-result-object v0

    .line 563
    invoke-virtual {v0}, Lcom/a/b/d/me;->size()I

    move-result v1

    iput v1, p0, Lcom/a/b/d/mf;->b:I

    .line 564
    return-object v0
.end method

.method public final c(Ljava/util/Iterator;)Lcom/a/b/d/mf;
    .registers 2

    .prologue
    .line 551
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->b(Ljava/util/Iterator;)Lcom/a/b/d/lp;

    .line 552
    return-object p0
.end method

.method public final varargs c([Ljava/lang/Object;)Lcom/a/b/d/mf;
    .registers 2

    .prologue
    .line 525
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->b([Ljava/lang/Object;)Lcom/a/b/d/lp;

    .line 526
    return-object p0
.end method

.method public final d(Ljava/lang/Object;)Lcom/a/b/d/mf;
    .registers 2

    .prologue
    .line 512
    invoke-super {p0, p1}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    .line 513
    return-object p0
.end method
