.class final Lcom/a/b/d/di;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/dg;

.field private b:Ljava/util/Iterator;


# direct methods
.method constructor <init>(Lcom/a/b/d/dg;)V
    .registers 3

    .prologue
    .line 491
    iput-object p1, p0, Lcom/a/b/d/di;->a:Lcom/a/b/d/dg;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    .line 492
    iget-object v0, p0, Lcom/a/b/d/di;->a:Lcom/a/b/d/dg;

    invoke-static {v0}, Lcom/a/b/d/dg;->a(Lcom/a/b/d/dg;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/di;->b:Ljava/util/Iterator;

    return-void
.end method

.method private c()Lcom/a/b/d/xd;
    .registers 3

    .prologue
    .line 496
    :cond_0
    iget-object v0, p0, Lcom/a/b/d/di;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_d

    .line 497
    invoke-virtual {p0}, Lcom/a/b/d/di;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    .line 502
    :goto_c
    return-object v0

    .line 499
    :cond_d
    iget-object v0, p0, Lcom/a/b/d/di;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 500
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 501
    if-eqz v1, :cond_0

    .line 502
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    goto :goto_c
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 491
    .line 1496
    :cond_0
    iget-object v0, p0, Lcom/a/b/d/di;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_d

    .line 1497
    invoke-virtual {p0}, Lcom/a/b/d/di;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    .line 1502
    :goto_c
    return-object v0

    .line 1499
    :cond_d
    iget-object v0, p0, Lcom/a/b/d/di;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1500
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 1501
    if-eqz v1, :cond_0

    .line 1502
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    goto :goto_c
.end method
