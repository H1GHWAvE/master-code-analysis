.class final Lcom/a/b/d/agk;
.super Lcom/a/b/d/xw;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/abn;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final e:J


# instance fields
.field private transient d:Lcom/a/b/d/agk;


# direct methods
.method constructor <init>(Lcom/a/b/d/abn;)V
    .registers 2

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/a/b/d/xw;-><init>(Lcom/a/b/d/xc;)V

    .line 37
    return-void
.end method

.method private e()Lcom/a/b/d/abn;
    .registers 2

    .prologue
    .line 41
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    return-object v0
.end method

.method private q()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 51
    .line 2041
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    .line 51
    invoke-interface {v0}, Lcom/a/b/d/abn;->e_()Ljava/util/NavigableSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/aad;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 6

    .prologue
    .line 103
    .line 7041
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    .line 103
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/a/b/d/abn;->a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/xe;->a(Lcom/a/b/d/abn;)Lcom/a/b/d/abn;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 32
    .line 12041
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    .line 32
    return-object v0
.end method

.method public final c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 4

    .prologue
    .line 109
    .line 8041
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    .line 109
    invoke-interface {v0, p1, p2}, Lcom/a/b/d/abn;->c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/xe;->a(Lcom/a/b/d/abn;)Lcom/a/b/d/abn;

    move-result-object v0

    return-object v0
.end method

.method final synthetic c()Ljava/util/Set;
    .registers 2

    .prologue
    .line 32
    .line 10041
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    .line 9051
    invoke-interface {v0}, Lcom/a/b/d/abn;->e_()Ljava/util/NavigableSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/aad;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    .line 32
    return-object v0
.end method

.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 46
    .line 1041
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    .line 46
    invoke-interface {v0}, Lcom/a/b/d/abn;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 4

    .prologue
    .line 95
    .line 6041
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    .line 95
    invoke-interface {v0, p1, p2}, Lcom/a/b/d/abn;->d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/xe;->a(Lcom/a/b/d/abn;)Lcom/a/b/d/abn;

    move-result-object v0

    return-object v0
.end method

.method public final e_()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 56
    invoke-super {p0}, Lcom/a/b/d/xw;->n_()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    return-object v0
.end method

.method protected final bridge synthetic f()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 32
    .line 11041
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    .line 32
    return-object v0
.end method

.method public final h()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 75
    .line 4041
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    .line 75
    invoke-interface {v0}, Lcom/a/b/d/abn;->h()Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 80
    .line 5041
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    .line 80
    invoke-interface {v0}, Lcom/a/b/d/abn;->i()Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 85
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final k()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 90
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 32
    .line 13041
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    .line 32
    return-object v0
.end method

.method public final m()Lcom/a/b/d/abn;
    .registers 3

    .prologue
    .line 63
    iget-object v0, p0, Lcom/a/b/d/agk;->d:Lcom/a/b/d/agk;

    .line 64
    if-nez v0, :cond_18

    .line 65
    new-instance v1, Lcom/a/b/d/agk;

    .line 3041
    invoke-super {p0}, Lcom/a/b/d/xw;->f()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    .line 65
    invoke-interface {v0}, Lcom/a/b/d/abn;->m()Lcom/a/b/d/abn;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/b/d/agk;-><init>(Lcom/a/b/d/abn;)V

    .line 67
    iput-object p0, v1, Lcom/a/b/d/agk;->d:Lcom/a/b/d/agk;

    .line 68
    iput-object v1, p0, Lcom/a/b/d/agk;->d:Lcom/a/b/d/agk;

    move-object v0, v1

    .line 70
    :cond_18
    return-object v0
.end method

.method public final synthetic n()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 32
    .line 13056
    invoke-super {p0}, Lcom/a/b/d/xw;->n_()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 32
    return-object v0
.end method

.method public final bridge synthetic n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 32
    .line 8056
    invoke-super {p0}, Lcom/a/b/d/xw;->n_()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 32
    return-object v0
.end method
