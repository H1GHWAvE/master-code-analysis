.class Lcom/a/b/d/tm;
.super Lcom/a/b/d/uj;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/b/bj;

.field private final b:Ljava/util/Set;


# direct methods
.method constructor <init>(Ljava/util/Set;Lcom/a/b/b/bj;)V
    .registers 4

    .prologue
    .line 763
    invoke-direct {p0}, Lcom/a/b/d/uj;-><init>()V

    .line 764
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lcom/a/b/d/tm;->b:Ljava/util/Set;

    .line 765
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/bj;

    iput-object v0, p0, Lcom/a/b/d/tm;->a:Lcom/a/b/b/bj;

    .line 766
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 817
    new-instance v0, Lcom/a/b/d/tn;

    invoke-direct {v0, p0}, Lcom/a/b/d/tn;-><init>(Lcom/a/b/d/tm;)V

    return-object v0
.end method

.method c()Ljava/util/Set;
    .registers 2

    .prologue
    .line 760
    iget-object v0, p0, Lcom/a/b/d/tm;->b:Ljava/util/Set;

    return-object v0
.end method

.method final c_()Ljava/util/Collection;
    .registers 3

    .prologue
    .line 775
    iget-object v0, p0, Lcom/a/b/d/tm;->b:Ljava/util/Set;

    iget-object v1, p0, Lcom/a/b/d/tm;->a:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Lcom/a/b/b/bj;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 812
    invoke-virtual {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 813
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 785
    invoke-virtual {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 770
    invoke-virtual {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->b(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 790
    invoke-virtual {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 793
    iget-object v0, p0, Lcom/a/b/d/tm;->a:Lcom/a/b/b/bj;

    invoke-interface {v0, p1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 795
    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 801
    invoke-virtual {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 804
    iget-object v0, p0, Lcom/a/b/d/tm;->a:Lcom/a/b/b/bj;

    invoke-interface {v0, p1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 806
    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public size()I
    .registers 2

    .prologue
    .line 780
    invoke-virtual {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
