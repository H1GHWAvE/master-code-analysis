.class Lcom/a/b/d/xa;
.super Lcom/a/b/d/wy;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/aac;


# static fields
.field private static final g:J


# direct methods
.method constructor <init>(Lcom/a/b/d/aac;)V
    .registers 2

    .prologue
    .line 615
    invoke-direct {p0, p1}, Lcom/a/b/d/wy;-><init>(Lcom/a/b/d/vi;)V

    .line 616
    return-void
.end method


# virtual methods
.method public a()Lcom/a/b/d/aac;
    .registers 2

    .prologue
    .line 618
    invoke-super {p0}, Lcom/a/b/d/wy;->c()Lcom/a/b/d/vi;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aac;

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 625
    invoke-virtual {p0}, Lcom/a/b/d/xa;->a()Lcom/a/b/d/aac;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/aac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 4

    .prologue
    .line 635
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 612
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/xa;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 631
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic c()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/a/b/d/xa;->a()Lcom/a/b/d/aac;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 612
    invoke-virtual {p0, p1}, Lcom/a/b/d/xa;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 612
    invoke-virtual {p0, p1}, Lcom/a/b/d/xa;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/a/b/d/xa;->u()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/a/b/d/xa;->a()Lcom/a/b/d/aac;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/util/Set;
    .registers 2

    .prologue
    .line 628
    invoke-virtual {p0}, Lcom/a/b/d/xa;->a()Lcom/a/b/d/aac;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/aac;->u()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
