.class public abstract Lcom/a/b/d/jl;
.super Lcom/a/b/d/iz;
.source "SourceFile"

# interfaces
.implements Ljava/util/List;
.implements Ljava/util/RandomAccess;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field static final c:Lcom/a/b/d/jl;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 66
    new-instance v0, Lcom/a/b/d/ze;

    sget-object v1, Lcom/a/b/d/yc;->a:[Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/a/b/d/ze;-><init>([Ljava/lang/Object;)V

    sput-object v0, Lcom/a/b/d/jl;->c:Lcom/a/b/d/jl;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 335
    invoke-direct {p0}, Lcom/a/b/d/iz;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 225
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_e

    check-cast p0, Ljava/util/Collection;

    invoke-static {p0}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v0

    :goto_d
    return-object v0

    :cond_e
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/jl;->a(Ljava/util/Iterator;)Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_d
.end method

.method public static a(Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 89
    new-instance v0, Lcom/a/b/d/aav;

    invoke-direct {v0, p0}, Lcom/a/b/d/aav;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 4

    .prologue
    .line 98
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 1303
    invoke-static {v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 1312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 98
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 5

    .prologue
    .line 107
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    .line 2303
    invoke-static {v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 2312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 107
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 6

    .prologue
    .line 116
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    .line 3303
    invoke-static {v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 3312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 116
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 7

    .prologue
    .line 125
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    .line 4303
    invoke-static {v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 4312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 125
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 8

    .prologue
    .line 134
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    .line 5303
    invoke-static {v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 5312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 134
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 9

    .prologue
    .line 144
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    .line 6303
    invoke-static {v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 6312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 144
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 10

    .prologue
    .line 154
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    const/4 v1, 0x7

    aput-object p7, v0, v1

    .line 7303
    invoke-static {v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 7312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 154
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 11

    .prologue
    .line 164
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    const/4 v1, 0x7

    aput-object p7, v0, v1

    const/16 v1, 0x8

    aput-object p8, v0, v1

    .line 8303
    invoke-static {v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 8312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 164
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 12

    .prologue
    .line 174
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    const/4 v1, 0x7

    aput-object p7, v0, v1

    const/16 v1, 0x8

    aput-object p8, v0, v1

    const/16 v1, 0x9

    aput-object p9, v0, v1

    .line 9303
    invoke-static {v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 9312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 174
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 13

    .prologue
    .line 184
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    const/4 v1, 0x7

    aput-object p7, v0, v1

    const/16 v1, 0x8

    aput-object p8, v0, v1

    const/16 v1, 0x9

    aput-object p9, v0, v1

    const/16 v1, 0xa

    aput-object p10, v0, v1

    .line 10303
    invoke-static {v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 10312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 184
    return-object v0
.end method

.method private static varargs a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 18

    .prologue
    .line 199
    move-object/from16 v0, p12

    array-length v1, v0

    add-int/lit8 v1, v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    .line 200
    const/4 v2, 0x0

    aput-object p0, v1, v2

    .line 201
    const/4 v2, 0x1

    aput-object p1, v1, v2

    .line 202
    const/4 v2, 0x2

    aput-object p2, v1, v2

    .line 203
    const/4 v2, 0x3

    aput-object p3, v1, v2

    .line 204
    const/4 v2, 0x4

    aput-object p4, v1, v2

    .line 205
    const/4 v2, 0x5

    aput-object p5, v1, v2

    .line 206
    const/4 v2, 0x6

    aput-object p6, v1, v2

    .line 207
    const/4 v2, 0x7

    aput-object p7, v1, v2

    .line 208
    const/16 v2, 0x8

    aput-object p8, v1, v2

    .line 209
    const/16 v2, 0x9

    aput-object p9, v1, v2

    .line 210
    const/16 v2, 0xa

    aput-object p10, v1, v2

    .line 211
    const/16 v2, 0xb

    aput-object p11, v1, v2

    .line 212
    const/4 v2, 0x0

    const/16 v3, 0xc

    move-object/from16 v0, p12

    array-length v4, v0

    move-object/from16 v0, p12

    invoke-static {v0, v2, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 11303
    invoke-static {v1}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    .line 11312
    array-length v2, v1

    invoke-static {v1, v2}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v1

    .line 213
    return-object v1
.end method

.method public static a(Ljava/util/Collection;)Lcom/a/b/d/jl;
    .registers 3

    .prologue
    .line 251
    instance-of v0, p0, Lcom/a/b/d/iz;

    if-eqz v0, :cond_1a

    .line 253
    check-cast p0, Lcom/a/b/d/iz;

    invoke-virtual {p0}, Lcom/a/b/d/iz;->f()Lcom/a/b/d/jl;

    move-result-object v0

    .line 254
    invoke-virtual {v0}, Lcom/a/b/d/jl;->h_()Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-virtual {v0}, Lcom/a/b/d/jl;->toArray()[Ljava/lang/Object;

    move-result-object v0

    .line 12312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 258
    :cond_19
    :goto_19
    return-object v0

    :cond_1a
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    .line 13303
    invoke-static {v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 13312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_19
.end method

.method public static a(Ljava/util/Iterator;)Lcom/a/b/d/jl;
    .registers 3

    .prologue
    .line 268
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_9

    .line 14077
    sget-object v0, Lcom/a/b/d/jl;->c:Lcom/a/b/d/jl;

    .line 275
    :goto_8
    return-object v0

    .line 271
    :cond_9
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 272
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_18

    .line 273
    invoke-static {v0}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_8

    .line 275
    :cond_18
    new-instance v1, Lcom/a/b/d/jn;

    invoke-direct {v1}, Lcom/a/b/d/jn;-><init>()V

    invoke-virtual {v1, v0}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/a/b/d/jn;->b(Ljava/util/Iterator;)Lcom/a/b/d/jn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_8
.end method

.method public static a([Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 3

    .prologue
    .line 289
    array-length v0, p0

    packed-switch v0, :pswitch_data_22

    .line 295
    new-instance v1, Lcom/a/b/d/ze;

    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/b/d/ze;-><init>([Ljava/lang/Object;)V

    move-object v0, v1

    :goto_14
    return-object v0

    .line 15077
    :pswitch_15
    sget-object v0, Lcom/a/b/d/jl;->c:Lcom/a/b/d/jl;

    goto :goto_14

    .line 293
    :pswitch_18
    new-instance v0, Lcom/a/b/d/aav;

    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-direct {v0, v1}, Lcom/a/b/d/aav;-><init>(Ljava/lang/Object;)V

    goto :goto_14

    .line 289
    nop

    :pswitch_data_22
    .packed-switch 0x0
        :pswitch_15
        :pswitch_18
    .end packed-switch
.end method

.method private b()Lcom/a/b/d/agj;
    .registers 2

    .prologue
    .line 344
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/a/b/d/jl;->a(I)Lcom/a/b/d/agj;

    move-result-object v0

    return-object v0
.end method

.method static b([Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 312
    array-length v0, p0

    invoke-static {p0, v0}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method static b([Ljava/lang/Object;I)Lcom/a/b/d/jl;
    .registers 4

    .prologue
    .line 320
    packed-switch p1, :pswitch_data_1c

    .line 328
    array-length v0, p0

    if-ge p1, v0, :cond_a

    .line 329
    invoke-static {p0, p1}, Lcom/a/b/d/yc;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p0

    .line 331
    :cond_a
    new-instance v0, Lcom/a/b/d/ze;

    invoke-direct {v0, p0}, Lcom/a/b/d/ze;-><init>([Ljava/lang/Object;)V

    :goto_f
    return-object v0

    .line 16077
    :pswitch_10
    sget-object v0, Lcom/a/b/d/jl;->c:Lcom/a/b/d/jl;

    goto :goto_f

    .line 325
    :pswitch_13
    new-instance v0, Lcom/a/b/d/aav;

    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-direct {v0, v1}, Lcom/a/b/d/aav;-><init>(Ljava/lang/Object;)V

    goto :goto_f

    .line 320
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_10
        :pswitch_13
    .end packed-switch
.end method

.method private static varargs c([Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 3

    .prologue
    .line 303
    invoke-static {p0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 15312
    array-length v1, v0

    invoke-static {v0, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 303
    return-object v0
.end method

.method public static d()Lcom/a/b/d/jl;
    .registers 1

    .prologue
    .line 77
    sget-object v0, Lcom/a/b/d/jl;->c:Lcom/a/b/d/jl;

    return-object v0
.end method

.method public static h()Lcom/a/b/d/jn;
    .registers 1

    .prologue
    .line 611
    new-instance v0, Lcom/a/b/d/jn;

    invoke-direct {v0}, Lcom/a/b/d/jn;-><init>()V

    return-object v0
.end method

.method private static i()V
    .registers 2

    .prologue
    .line 599
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Use SerializedForm"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method a([Ljava/lang/Object;I)I
    .registers 7

    .prologue
    .line 494
    invoke-virtual {p0}, Lcom/a/b/d/jl;->size()I

    move-result v1

    .line 495
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v1, :cond_12

    .line 496
    add-int v2, p2, v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, p1, v2

    .line 495
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 498
    :cond_12
    add-int v0, p2, v1

    return v0
.end method

.method public a(I)Lcom/a/b/d/agj;
    .registers 4

    .prologue
    .line 348
    new-instance v0, Lcom/a/b/d/jm;

    invoke-virtual {p0}, Lcom/a/b/d/jl;->size()I

    move-result v1

    invoke-direct {v0, p0, v1, p1}, Lcom/a/b/d/jm;-><init>(Lcom/a/b/d/jl;II)V

    return-object v0
.end method

.method public a(II)Lcom/a/b/d/jl;
    .registers 4

    .prologue
    .line 381
    invoke-virtual {p0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 382
    sub-int v0, p2, p1

    .line 383
    packed-switch v0, :pswitch_data_1e

    .line 389
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/jl;->b(II)Lcom/a/b/d/jl;

    move-result-object v0

    :goto_10
    return-object v0

    .line 17077
    :pswitch_11
    sget-object v0, Lcom/a/b/d/jl;->c:Lcom/a/b/d/jl;

    goto :goto_10

    .line 387
    :pswitch_14
    invoke-virtual {p0, p1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_10

    .line 383
    nop

    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_11
        :pswitch_14
    .end packed-switch
.end method

.method public final add(ILjava/lang/Object;)V
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 467
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(ILjava/util/Collection;)Z
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 443
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method b(II)Lcom/a/b/d/jl;
    .registers 5

    .prologue
    .line 399
    new-instance v0, Lcom/a/b/d/jq;

    sub-int v1, p2, p1

    invoke-direct {v0, p0, p1, v1}, Lcom/a/b/d/jq;-><init>(Lcom/a/b/d/jl;II)V

    return-object v0
.end method

.method public c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 340
    .line 16344
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/a/b/d/jl;->a(I)Lcom/a/b/d/agj;

    move-result-object v0

    .line 340
    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 368
    invoke-virtual {p0, p1}, Lcom/a/b/d/jl;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public e()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 510
    new-instance v0, Lcom/a/b/d/jo;

    invoke-direct {v0, p0}, Lcom/a/b/d/jo;-><init>(Lcom/a/b/d/jl;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 567
    invoke-static {p0, p1}, Lcom/a/b/d/ov;->a(Ljava/util/List;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final f()Lcom/a/b/d/jl;
    .registers 1

    .prologue
    .line 488
    return-object p0
.end method

.method g()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 603
    new-instance v0, Lcom/a/b/d/jp;

    invoke-virtual {p0}, Lcom/a/b/d/jl;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/jp;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method

.method public hashCode()I
    .registers 5

    .prologue
    .line 571
    const/4 v1, 0x1

    .line 572
    invoke-virtual {p0}, Lcom/a/b/d/jl;->size()I

    move-result v2

    .line 573
    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_1a

    .line 574
    mul-int/lit8 v1, v1, 0x1f

    invoke-virtual {p0, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int/2addr v1, v3

    .line 576
    xor-int/lit8 v1, v1, -0x1

    xor-int/lit8 v1, v1, -0x1

    .line 573
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 579
    :cond_1a
    return v1
.end method

.method public indexOf(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 358
    if-nez p1, :cond_4

    const/4 v0, -0x1

    :goto_3
    return v0

    :cond_4
    invoke-static {p0, p1}, Lcom/a/b/d/ov;->b(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    goto :goto_3
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 363
    if-nez p1, :cond_4

    const/4 v0, -0x1

    :goto_3
    return v0

    :cond_4
    invoke-static {p0, p1}, Lcom/a/b/d/ov;->c(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    goto :goto_3
.end method

.method public synthetic listIterator()Ljava/util/ListIterator;
    .registers 2

    .prologue
    .line 61
    .line 17344
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/a/b/d/jl;->a(I)Lcom/a/b/d/agj;

    move-result-object v0

    .line 61
    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .registers 3

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/a/b/d/jl;->a(I)Lcom/a/b/d/agj;

    move-result-object v0

    return-object v0
.end method

.method public final remove(I)Ljava/lang/Object;
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 479
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 455
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic subList(II)Ljava/util/List;
    .registers 4

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/jl;->a(II)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method
