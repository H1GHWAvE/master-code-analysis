.class public Lcom/a/b/d/lp;
.super Lcom/a/b/d/ja;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 463
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/d/lp;-><init>(B)V

    .line 464
    return-void
.end method

.method private constructor <init>(B)V
    .registers 3

    .prologue
    .line 467
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/a/b/d/ja;-><init>(I)V

    .line 468
    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 456
    invoke-virtual {p0}, Lcom/a/b/d/lp;->b()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Object;)Lcom/a/b/d/ja;
    .registers 3

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Iterable;)Lcom/a/b/d/jb;
    .registers 3

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Lcom/a/b/d/lp;->b(Ljava/lang/Iterable;)Lcom/a/b/d/lp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/util/Iterator;)Lcom/a/b/d/jb;
    .registers 3

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Lcom/a/b/d/lp;->b(Ljava/util/Iterator;)Lcom/a/b/d/lp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a([Ljava/lang/Object;)Lcom/a/b/d/jb;
    .registers 3

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Lcom/a/b/d/lp;->b([Ljava/lang/Object;)Lcom/a/b/d/lp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Lcom/a/b/d/jb;
    .registers 3

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 531
    iget v0, p0, Lcom/a/b/d/lp;->b:I

    iget-object v1, p0, Lcom/a/b/d/lp;->a:[Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/a/b/d/lo;->a(I[Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 534
    invoke-virtual {v0}, Lcom/a/b/d/lo;->size()I

    move-result v1

    iput v1, p0, Lcom/a/b/d/lp;->b:I

    .line 535
    return-object v0
.end method

.method public b(Ljava/lang/Iterable;)Lcom/a/b/d/lp;
    .registers 2

    .prologue
    .line 508
    invoke-super {p0, p1}, Lcom/a/b/d/ja;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jb;

    .line 509
    return-object p0
.end method

.method public b(Ljava/util/Iterator;)Lcom/a/b/d/lp;
    .registers 2

    .prologue
    .line 522
    invoke-super {p0, p1}, Lcom/a/b/d/ja;->a(Ljava/util/Iterator;)Lcom/a/b/d/jb;

    .line 523
    return-object p0
.end method

.method public varargs b([Ljava/lang/Object;)Lcom/a/b/d/lp;
    .registers 2

    .prologue
    .line 494
    invoke-super {p0, p1}, Lcom/a/b/d/ja;->a([Ljava/lang/Object;)Lcom/a/b/d/jb;

    .line 495
    return-object p0
.end method

.method public c(Ljava/lang/Object;)Lcom/a/b/d/lp;
    .registers 2

    .prologue
    .line 480
    invoke-super {p0, p1}, Lcom/a/b/d/ja;->a(Ljava/lang/Object;)Lcom/a/b/d/ja;

    .line 481
    return-object p0
.end method
