.class final Lcom/a/b/d/ag;
.super Lcom/a/b/d/ab;
.source "SourceFile"

# interfaces
.implements Ljava/util/Set;


# instance fields
.field final synthetic a:Lcom/a/b/d/n;


# direct methods
.method constructor <init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/Set;)V
    .registers 5
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 602
    iput-object p1, p0, Lcom/a/b/d/ag;->a:Lcom/a/b/d/n;

    .line 603
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/a/b/d/ab;-><init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/Collection;Lcom/a/b/d/ab;)V

    .line 604
    return-void
.end method


# virtual methods
.method public final removeAll(Ljava/util/Collection;)Z
    .registers 6

    .prologue
    .line 608
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 609
    const/4 v0, 0x0

    .line 622
    :cond_7
    :goto_7
    return v0

    .line 611
    :cond_8
    invoke-virtual {p0}, Lcom/a/b/d/ag;->size()I

    move-result v1

    .line 616
    iget-object v0, p0, Lcom/a/b/d/ag;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/Set;

    invoke-static {v0, p1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Ljava/util/Collection;)Z

    move-result v0

    .line 617
    if-eqz v0, :cond_7

    .line 618
    iget-object v2, p0, Lcom/a/b/d/ag;->c:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    .line 619
    iget-object v3, p0, Lcom/a/b/d/ag;->a:Lcom/a/b/d/n;

    sub-int v1, v2, v1

    invoke-static {v3, v1}, Lcom/a/b/d/n;->a(Lcom/a/b/d/n;I)I

    .line 620
    invoke-virtual {p0}, Lcom/a/b/d/ag;->b()V

    goto :goto_7
.end method
