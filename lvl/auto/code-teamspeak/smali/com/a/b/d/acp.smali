.class final Lcom/a/b/d/acp;
.super Lcom/a/b/d/gw;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/Map$Entry;

.field final synthetic b:Lcom/a/b/d/aco;


# direct methods
.method constructor <init>(Lcom/a/b/d/aco;Ljava/util/Map$Entry;)V
    .registers 3

    .prologue
    .line 361
    iput-object p1, p0, Lcom/a/b/d/acp;->b:Lcom/a/b/d/aco;

    iput-object p2, p0, Lcom/a/b/d/acp;->a:Ljava/util/Map$Entry;

    invoke-direct {p0}, Lcom/a/b/d/gw;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/a/b/d/acp;->a:Ljava/util/Map$Entry;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 371
    .line 1096
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_24

    .line 1097
    check-cast p1, Ljava/util/Map$Entry;

    .line 1098
    invoke-virtual {p0}, Lcom/a/b/d/gw;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    invoke-virtual {p0}, Lcom/a/b/d/gw;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    const/4 v0, 0x1

    :cond_24
    return v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 361
    .line 1363
    iget-object v0, p0, Lcom/a/b/d/acp;->a:Ljava/util/Map$Entry;

    .line 361
    return-object v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 366
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/a/b/d/gw;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
