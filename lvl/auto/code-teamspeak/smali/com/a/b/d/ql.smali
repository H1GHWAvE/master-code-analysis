.class public final Lcom/a/b/d/ql;
.super Lcom/a/b/d/ht;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field static final b:I = -0x1

.field private static final n:I = 0x10

.field private static final o:I = 0x4

.field private static final p:I


# instance fields
.field c:Z

.field d:I

.field e:I

.field f:I

.field g:Lcom/a/b/d/sh;

.field h:Lcom/a/b/d/sh;

.field i:J

.field j:J

.field k:Lcom/a/b/d/qq;

.field l:Lcom/a/b/b/au;

.field m:Lcom/a/b/b/ej;


# direct methods
.method public constructor <init>()V
    .registers 5

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, -0x1

    .line 136
    invoke-direct {p0}, Lcom/a/b/d/ht;-><init>()V

    .line 116
    iput v0, p0, Lcom/a/b/d/ql;->d:I

    .line 117
    iput v0, p0, Lcom/a/b/d/ql;->e:I

    .line 118
    iput v0, p0, Lcom/a/b/d/ql;->f:I

    .line 123
    iput-wide v2, p0, Lcom/a/b/d/ql;->i:J

    .line 124
    iput-wide v2, p0, Lcom/a/b/d/ql;->j:J

    .line 136
    return-void
.end method

.method private a(Lcom/a/b/d/qw;)Lcom/a/b/d/ht;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 482
    iget-object v0, p0, Lcom/a/b/d/ql;->a:Lcom/a/b/d/qw;

    if-nez v0, :cond_14

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 487
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/qw;

    iput-object v0, p0, Lcom/a/b/d/ht;->a:Lcom/a/b/d/qw;

    .line 488
    iput-boolean v1, p0, Lcom/a/b/d/ql;->c:Z

    .line 489
    return-object p0

    .line 482
    :cond_14
    const/4 v0, 0x0

    goto :goto_6
.end method

.method private e(JLjava/util/concurrent/TimeUnit;)V
    .registers 15

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 391
    iget-wide v4, p0, Lcom/a/b/d/ql;->i:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_48

    move v0, v1

    :goto_b
    const-string v3, "expireAfterWrite was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/a/b/d/ql;->i:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 393
    iget-wide v4, p0, Lcom/a/b/d/ql;->j:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_4a

    move v0, v1

    :goto_21
    const-string v3, "expireAfterAccess was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/a/b/d/ql;->j:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 395
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_4c

    move v0, v1

    :goto_37
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 396
    return-void

    :cond_48
    move v0, v2

    .line 391
    goto :goto_b

    :cond_4a
    move v0, v2

    .line 393
    goto :goto_21

    :cond_4c
    move v0, v2

    .line 395
    goto :goto_37
.end method

.method private j()Lcom/a/b/b/au;
    .registers 3

    .prologue
    .line 155
    iget-object v0, p0, Lcom/a/b/d/ql;->l:Lcom/a/b/b/au;

    invoke-virtual {p0}, Lcom/a/b/d/ql;->i()Lcom/a/b/d/sh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/sh;->a()Lcom/a/b/b/au;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    return-object v0
.end method

.method private k()Lcom/a/b/d/ql;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.lang.ref.WeakReference"
    .end annotation

    .prologue
    .line 265
    sget-object v0, Lcom/a/b/d/sh;->c:Lcom/a/b/d/sh;

    invoke-virtual {p0, v0}, Lcom/a/b/d/ql;->a(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    return-object v0
.end method

.method private l()Lcom/a/b/d/ql;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.lang.ref.WeakReference"
    .end annotation

    .prologue
    .line 303
    sget-object v0, Lcom/a/b/d/sh;->c:Lcom/a/b/d/sh;

    invoke-virtual {p0, v0}, Lcom/a/b/d/ql;->b(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    return-object v0
.end method

.method private m()Lcom/a/b/d/ql;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.lang.ref.SoftReference"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 335
    sget-object v0, Lcom/a/b/d/sh;->b:Lcom/a/b/d/sh;

    invoke-virtual {p0, v0}, Lcom/a/b/d/ql;->b(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    return-object v0
.end method

.method private n()Lcom/a/b/d/sh;
    .registers 3

    .prologue
    .line 349
    iget-object v0, p0, Lcom/a/b/d/ql;->h:Lcom/a/b/d/sh;

    sget-object v1, Lcom/a/b/d/sh;->a:Lcom/a/b/d/sh;

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/sh;

    return-object v0
.end method

.method private o()J
    .registers 5

    .prologue
    .line 399
    iget-wide v0, p0, Lcom/a/b/d/ql;->i:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    const-wide/16 v0, 0x0

    :goto_a
    return-wide v0

    :cond_b
    iget-wide v0, p0, Lcom/a/b/d/ql;->i:J

    goto :goto_a
.end method

.method private p()J
    .registers 5

    .prologue
    .line 442
    iget-wide v0, p0, Lcom/a/b/d/ql;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    const-wide/16 v0, 0x0

    :goto_a
    return-wide v0

    :cond_b
    iget-wide v0, p0, Lcom/a/b/d/ql;->j:J

    goto :goto_a
.end method

.method private q()Lcom/a/b/b/ej;
    .registers 3

    .prologue
    .line 447
    iget-object v0, p0, Lcom/a/b/d/ql;->m:Lcom/a/b/b/ej;

    invoke-static {}, Lcom/a/b/b/ej;->b()Lcom/a/b/b/ej;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/ej;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Lcom/a/b/d/ht;
    .registers 2

    .prologue
    .line 105
    .line 7265
    sget-object v0, Lcom/a/b/d/sh;->c:Lcom/a/b/d/sh;

    invoke-virtual {p0, v0}, Lcom/a/b/d/ql;->a(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    .line 105
    return-object v0
.end method

.method public final synthetic a(I)Lcom/a/b/d/ht;
    .registers 3

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/a/b/d/ql;->d(I)Lcom/a/b/d/ql;

    move-result-object v0

    return-object v0
.end method

.method final synthetic a(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/d/ht;
    .registers 5

    .prologue
    .line 105
    invoke-virtual {p0, p1, p2, p3}, Lcom/a/b/d/ql;->c(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/d/ql;

    move-result-object v0

    return-object v0
.end method

.method final synthetic a(Lcom/a/b/b/au;)Lcom/a/b/d/ht;
    .registers 3

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/a/b/d/ql;->b(Lcom/a/b/b/au;)Lcom/a/b/d/ql;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;
    .registers 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 269
    iget-object v0, p0, Lcom/a/b/d/ql;->g:Lcom/a/b/d/sh;

    if-nez v0, :cond_2d

    move v0, v1

    :goto_7
    const-string v3, "Key strength was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/a/b/d/ql;->g:Lcom/a/b/d/sh;

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 270
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/sh;

    iput-object v0, p0, Lcom/a/b/d/ql;->g:Lcom/a/b/d/sh;

    .line 271
    iget-object v0, p0, Lcom/a/b/d/ql;->g:Lcom/a/b/d/sh;

    sget-object v3, Lcom/a/b/d/sh;->b:Lcom/a/b/d/sh;

    if-eq v0, v3, :cond_21

    move v2, v1

    :cond_21
    const-string v0, "Soft keys are not supported"

    invoke-static {v2, v0}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 272
    sget-object v0, Lcom/a/b/d/sh;->a:Lcom/a/b/d/sh;

    if-eq p1, v0, :cond_2c

    .line 274
    iput-boolean v1, p0, Lcom/a/b/d/ql;->c:Z

    .line 276
    :cond_2c
    return-object p0

    :cond_2d
    move v0, v2

    .line 269
    goto :goto_7
.end method

.method final a(Lcom/a/b/b/bj;)Ljava/util/concurrent/ConcurrentMap;
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 585
    iget-object v0, p0, Lcom/a/b/d/ql;->k:Lcom/a/b/d/qq;

    if-nez v0, :cond_c

    new-instance v0, Lcom/a/b/d/qn;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/qn;-><init>(Lcom/a/b/d/ql;Lcom/a/b/b/bj;)V

    :goto_9
    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    return-object v0

    :cond_c
    new-instance v0, Lcom/a/b/d/qo;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/qo;-><init>(Lcom/a/b/d/ql;Lcom/a/b/b/bj;)V

    goto :goto_9
.end method

.method public final bridge synthetic b()Lcom/a/b/d/ht;
    .registers 2

    .prologue
    .line 105
    .line 6303
    sget-object v0, Lcom/a/b/d/sh;->c:Lcom/a/b/d/sh;

    invoke-virtual {p0, v0}, Lcom/a/b/d/ql;->b(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    .line 105
    return-object v0
.end method

.method final synthetic b(I)Lcom/a/b/d/ht;
    .registers 3

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/a/b/d/ql;->e(I)Lcom/a/b/d/ql;

    move-result-object v0

    return-object v0
.end method

.method final synthetic b(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/d/ht;
    .registers 5

    .prologue
    .line 105
    invoke-virtual {p0, p1, p2, p3}, Lcom/a/b/d/ql;->d(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/d/ql;

    move-result-object v0

    return-object v0
.end method

.method final b(Lcom/a/b/b/au;)Lcom/a/b/d/ql;
    .registers 8
    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 148
    iget-object v0, p0, Lcom/a/b/d/ql;->l:Lcom/a/b/b/au;

    if-nez v0, :cond_1d

    move v0, v1

    :goto_7
    const-string v3, "key equivalence was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/a/b/d/ql;->l:Lcom/a/b/b/au;

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 149
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    iput-object v0, p0, Lcom/a/b/d/ql;->l:Lcom/a/b/b/au;

    .line 150
    iput-boolean v1, p0, Lcom/a/b/d/ql;->c:Z

    .line 151
    return-object p0

    :cond_1d
    move v0, v2

    .line 148
    goto :goto_7
.end method

.method public final b(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;
    .registers 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 339
    iget-object v0, p0, Lcom/a/b/d/ql;->h:Lcom/a/b/d/sh;

    if-nez v0, :cond_21

    move v0, v1

    :goto_7
    const-string v3, "Value strength was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/a/b/d/ql;->h:Lcom/a/b/d/sh;

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 340
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/sh;

    iput-object v0, p0, Lcom/a/b/d/ql;->h:Lcom/a/b/d/sh;

    .line 341
    sget-object v0, Lcom/a/b/d/sh;->a:Lcom/a/b/d/sh;

    if-eq p1, v0, :cond_20

    .line 343
    iput-boolean v1, p0, Lcom/a/b/d/ql;->c:Z

    .line 345
    :cond_20
    return-object p0

    :cond_21
    move v0, v2

    .line 339
    goto :goto_7
.end method

.method public final synthetic c()Lcom/a/b/d/ht;
    .registers 2

    .prologue
    .line 105
    .line 5335
    sget-object v0, Lcom/a/b/d/sh;->b:Lcom/a/b/d/sh;

    invoke-virtual {p0, v0}, Lcom/a/b/d/ql;->b(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    .line 105
    return-object v0
.end method

.method public final synthetic c(I)Lcom/a/b/d/ht;
    .registers 3

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/a/b/d/ql;->f(I)Lcom/a/b/d/ql;

    move-result-object v0

    return-object v0
.end method

.method final c(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/d/ql;
    .registers 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 380
    invoke-direct {p0, p1, p2, p3}, Lcom/a/b/d/ql;->e(JLjava/util/concurrent/TimeUnit;)V

    .line 381
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/b/d/ql;->i:J

    .line 382
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/a/b/d/ql;->k:Lcom/a/b/d/qq;

    if-nez v0, :cond_17

    .line 384
    sget-object v0, Lcom/a/b/d/qq;->d:Lcom/a/b/d/qq;

    iput-object v0, p0, Lcom/a/b/d/ql;->k:Lcom/a/b/d/qq;

    .line 386
    :cond_17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/b/d/ql;->c:Z

    .line 387
    return-object p0
.end method

.method public final d(I)Lcom/a/b/d/ql;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 170
    iget v0, p0, Lcom/a/b/d/ql;->d:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1f

    move v0, v1

    :goto_8
    const-string v3, "initial capacity was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, Lcom/a/b/d/ql;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 172
    if-ltz p1, :cond_21

    :goto_19
    invoke-static {v1}, Lcom/a/b/b/cn;->a(Z)V

    .line 173
    iput p1, p0, Lcom/a/b/d/ql;->d:I

    .line 174
    return-object p0

    :cond_1f
    move v0, v2

    .line 170
    goto :goto_8

    :cond_21
    move v1, v2

    .line 172
    goto :goto_19
.end method

.method final d(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/d/ql;
    .registers 7
    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 431
    invoke-direct {p0, p1, p2, p3}, Lcom/a/b/d/ql;->e(JLjava/util/concurrent/TimeUnit;)V

    .line 432
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/b/d/ql;->j:J

    .line 433
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/a/b/d/ql;->k:Lcom/a/b/d/qq;

    if-nez v0, :cond_17

    .line 435
    sget-object v0, Lcom/a/b/d/qq;->d:Lcom/a/b/d/qq;

    iput-object v0, p0, Lcom/a/b/d/ql;->k:Lcom/a/b/d/qq;

    .line 437
    :cond_17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/b/d/ql;->c:Z

    .line 438
    return-object p0
.end method

.method final e(I)Lcom/a/b/d/ql;
    .registers 8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 207
    iget v0, p0, Lcom/a/b/d/ql;->f:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_2c

    move v0, v1

    :goto_8
    const-string v3, "maximum size was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, Lcom/a/b/d/ql;->f:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 209
    if-ltz p1, :cond_1a

    move v2, v1

    :cond_1a
    const-string v0, "maximum size must not be negative"

    invoke-static {v2, v0}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 210
    iput p1, p0, Lcom/a/b/d/ql;->f:I

    .line 211
    iput-boolean v1, p0, Lcom/a/b/d/ql;->c:Z

    .line 212
    iget v0, p0, Lcom/a/b/d/ql;->f:I

    if-nez v0, :cond_2b

    .line 214
    sget-object v0, Lcom/a/b/d/qq;->e:Lcom/a/b/d/qq;

    iput-object v0, p0, Lcom/a/b/d/ql;->k:Lcom/a/b/d/qq;

    .line 216
    :cond_2b
    return-object p0

    :cond_2c
    move v0, v2

    .line 207
    goto :goto_8
.end method

.method public final e()Ljava/util/concurrent/ConcurrentMap;
    .registers 5

    .prologue
    .line 506
    iget-boolean v0, p0, Lcom/a/b/d/ql;->c:Z

    if-nez v0, :cond_14

    .line 507
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Lcom/a/b/d/ql;->g()I

    move-result v1

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-virtual {p0}, Lcom/a/b/d/ql;->h()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 509
    :goto_13
    return-object v0

    :cond_14
    iget-object v0, p0, Lcom/a/b/d/ql;->k:Lcom/a/b/d/qq;

    if-nez v0, :cond_20

    new-instance v0, Lcom/a/b/d/qy;

    invoke-direct {v0, p0}, Lcom/a/b/d/qy;-><init>(Lcom/a/b/d/ql;)V

    :goto_1d
    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    goto :goto_13

    :cond_20
    new-instance v0, Lcom/a/b/d/qp;

    invoke-direct {v0, p0}, Lcom/a/b/d/qp;-><init>(Lcom/a/b/d/ql;)V

    goto :goto_1d
.end method

.method public final f(I)Lcom/a/b/d/ql;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 240
    iget v0, p0, Lcom/a/b/d/ql;->e:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1f

    move v0, v1

    :goto_8
    const-string v3, "concurrency level was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, Lcom/a/b/d/ql;->e:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 242
    if-lez p1, :cond_21

    :goto_19
    invoke-static {v1}, Lcom/a/b/b/cn;->a(Z)V

    .line 243
    iput p1, p0, Lcom/a/b/d/ql;->e:I

    .line 244
    return-object p0

    :cond_1f
    move v0, v2

    .line 240
    goto :goto_8

    :cond_21
    move v1, v2

    .line 242
    goto :goto_19
.end method

.method final f()Lcom/a/b/d/qy;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "MapMakerInternalMap"
    .end annotation

    .prologue
    .line 521
    new-instance v0, Lcom/a/b/d/qy;

    invoke-direct {v0, p0}, Lcom/a/b/d/qy;-><init>(Lcom/a/b/d/ql;)V

    return-object v0
.end method

.method final g()I
    .registers 3

    .prologue
    .line 178
    iget v0, p0, Lcom/a/b/d/ql;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_8

    const/16 v0, 0x10

    :goto_7
    return v0

    :cond_8
    iget v0, p0, Lcom/a/b/d/ql;->d:I

    goto :goto_7
.end method

.method final h()I
    .registers 3

    .prologue
    .line 248
    iget v0, p0, Lcom/a/b/d/ql;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    const/4 v0, 0x4

    :goto_6
    return v0

    :cond_7
    iget v0, p0, Lcom/a/b/d/ql;->e:I

    goto :goto_6
.end method

.method final i()Lcom/a/b/d/sh;
    .registers 3

    .prologue
    .line 280
    iget-object v0, p0, Lcom/a/b/d/ql;->g:Lcom/a/b/d/sh;

    sget-object v1, Lcom/a/b/d/sh;->a:Lcom/a/b/d/sh;

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/sh;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 9

    .prologue
    const-wide/16 v6, -0x1

    const/16 v5, 0x16

    const/4 v3, -0x1

    .line 596
    invoke-static {p0}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    .line 597
    iget v1, p0, Lcom/a/b/d/ql;->d:I

    if-eq v1, v3, :cond_14

    .line 598
    const-string v1, "initialCapacity"

    iget v2, p0, Lcom/a/b/d/ql;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;I)Lcom/a/b/b/cc;

    .line 600
    :cond_14
    iget v1, p0, Lcom/a/b/d/ql;->e:I

    if-eq v1, v3, :cond_1f

    .line 601
    const-string v1, "concurrencyLevel"

    iget v2, p0, Lcom/a/b/d/ql;->e:I

    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;I)Lcom/a/b/b/cc;

    .line 603
    :cond_1f
    iget v1, p0, Lcom/a/b/d/ql;->f:I

    if-eq v1, v3, :cond_2a

    .line 604
    const-string v1, "maximumSize"

    iget v2, p0, Lcom/a/b/d/ql;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;I)Lcom/a/b/b/cc;

    .line 606
    :cond_2a
    iget-wide v2, p0, Lcom/a/b/d/ql;->i:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4a

    .line 607
    const-string v1, "expireAfterWrite"

    iget-wide v2, p0, Lcom/a/b/d/ql;->i:J

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1185
    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 609
    :cond_4a
    iget-wide v2, p0, Lcom/a/b/d/ql;->j:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_6a

    .line 610
    const-string v1, "expireAfterAccess"

    iget-wide v2, p0, Lcom/a/b/d/ql;->j:J

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2185
    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 612
    :cond_6a
    iget-object v1, p0, Lcom/a/b/d/ql;->g:Lcom/a/b/d/sh;

    if-eqz v1, :cond_7d

    .line 613
    const-string v1, "keyStrength"

    iget-object v2, p0, Lcom/a/b/d/ql;->g:Lcom/a/b/d/sh;

    invoke-virtual {v2}, Lcom/a/b/d/sh;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/b/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3185
    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 615
    :cond_7d
    iget-object v1, p0, Lcom/a/b/d/ql;->h:Lcom/a/b/d/sh;

    if-eqz v1, :cond_90

    .line 616
    const-string v1, "valueStrength"

    iget-object v2, p0, Lcom/a/b/d/ql;->h:Lcom/a/b/d/sh;

    invoke-virtual {v2}, Lcom/a/b/d/sh;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/b/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4185
    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 618
    :cond_90
    iget-object v1, p0, Lcom/a/b/d/ql;->l:Lcom/a/b/b/au;

    if-eqz v1, :cond_99

    .line 619
    const-string v1, "keyEquivalence"

    .line 4257
    invoke-virtual {v0, v1}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 621
    :cond_99
    iget-object v1, p0, Lcom/a/b/d/ql;->a:Lcom/a/b/d/qw;

    if-eqz v1, :cond_a2

    .line 622
    const-string v1, "removalListener"

    .line 5257
    invoke-virtual {v0, v1}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 624
    :cond_a2
    invoke-virtual {v0}, Lcom/a/b/b/cc;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
