.class final Lcom/a/b/d/wg;
.super Lcom/a/b/d/tu;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/wf;


# direct methods
.method constructor <init>(Lcom/a/b/d/wf;)V
    .registers 2

    .prologue
    .line 1691
    iput-object p1, p0, Lcom/a/b/d/wg;->a:Lcom/a/b/d/wf;

    invoke-direct {p0}, Lcom/a/b/d/tu;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 1693
    iget-object v0, p0, Lcom/a/b/d/wg;->a:Lcom/a/b/d/wf;

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 1697
    iget-object v0, p0, Lcom/a/b/d/wg;->a:Lcom/a/b/d/wf;

    invoke-static {v0}, Lcom/a/b/d/wf;->a(Lcom/a/b/d/wf;)Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/a/b/d/wh;

    invoke-direct {v1, p0}, Lcom/a/b/d/wh;-><init>(Lcom/a/b/d/wg;)V

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/Set;Lcom/a/b/b/bj;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 1706
    invoke-virtual {p0, p1}, Lcom/a/b/d/wg;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1707
    const/4 v0, 0x0

    .line 1711
    :goto_7
    return v0

    .line 1709
    :cond_8
    check-cast p1, Ljava/util/Map$Entry;

    .line 1710
    iget-object v0, p0, Lcom/a/b/d/wg;->a:Lcom/a/b/d/wf;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 2688
    iget-object v0, v0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1711
    const/4 v0, 0x1

    goto :goto_7
.end method
