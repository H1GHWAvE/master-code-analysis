.class public final Lcom/a/b/d/ael;
.super Lcom/a/b/d/abu;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# static fields
.field private static final d:J


# instance fields
.field final c:Ljava/util/Comparator;


# direct methods
.method private constructor <init>(Ljava/util/Comparator;Ljava/util/Comparator;)V
    .registers 5

    .prologue
    .line 140
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0, p1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    new-instance v1, Lcom/a/b/d/aeo;

    invoke-direct {v1, p2}, Lcom/a/b/d/aeo;-><init>(Ljava/util/Comparator;)V

    invoke-direct {p0, v0, v1}, Lcom/a/b/d/abu;-><init>(Ljava/util/SortedMap;Lcom/a/b/b/dz;)V

    .line 142
    iput-object p2, p0, Lcom/a/b/d/ael;->c:Ljava/util/Comparator;

    .line 143
    return-void
.end method

.method private static a(Lcom/a/b/d/ael;)Lcom/a/b/d/ael;
    .registers 4

    .prologue
    .line 131
    new-instance v0, Lcom/a/b/d/ael;

    .line 1304
    invoke-super {p0}, Lcom/a/b/d/abu;->l_()Ljava/util/SortedSet;

    move-result-object v1

    .line 1152
    invoke-interface {v1}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v1

    .line 2160
    iget-object v2, p0, Lcom/a/b/d/ael;->c:Ljava/util/Comparator;

    .line 131
    invoke-direct {v0, v1, v2}, Lcom/a/b/d/ael;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    .line 3077
    invoke-super {v0, p0}, Lcom/a/b/d/abu;->a(Lcom/a/b/d/adv;)V

    .line 135
    return-object v0
.end method

.method private static a(Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/ael;
    .registers 3

    .prologue
    .line 120
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    new-instance v0, Lcom/a/b/d/ael;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ael;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    return-object v0
.end method

.method private f(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 177
    new-instance v0, Lcom/a/b/d/aep;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/aep;-><init>(Lcom/a/b/d/ael;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static p()Lcom/a/b/d/ael;
    .registers 3

    .prologue
    .line 106
    new-instance v0, Lcom/a/b/d/ael;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/ael;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    return-object v0
.end method

.method private q()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 152
    .line 3304
    invoke-super {p0}, Lcom/a/b/d/abu;->l_()Ljava/util/SortedSet;

    move-result-object v0

    .line 152
    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method private r()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/a/b/d/ael;->c:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 77
    invoke-super {p0, p1, p2, p3}, Lcom/a/b/d/abu;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 77
    .line 5304
    invoke-super {p0}, Lcom/a/b/d/abu;->l_()Ljava/util/SortedSet;

    move-result-object v0

    .line 77
    return-object v0
.end method

.method public final bridge synthetic a(Lcom/a/b/d/adv;)V
    .registers 2

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/a/b/d/abu;->a(Lcom/a/b/d/adv;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/a/b/d/abu;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 77
    invoke-super {p0, p1, p2}, Lcom/a/b/d/abu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 77
    invoke-super {p0, p1, p2}, Lcom/a/b/d/abu;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/util/Set;
    .registers 2

    .prologue
    .line 77
    invoke-super {p0}, Lcom/a/b/d/abu;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/a/b/d/abu;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 77
    invoke-super {p0, p1, p2}, Lcom/a/b/d/abu;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic c()Z
    .registers 2

    .prologue
    .line 77
    invoke-super {p0}, Lcom/a/b/d/abu;->c()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/a/b/d/abu;->c(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic d(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/a/b/d/abu;->d(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic d()V
    .registers 1

    .prologue
    .line 77
    invoke-super {p0}, Lcom/a/b/d/abu;->d()V

    return-void
.end method

.method public final synthetic e(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 77
    .line 6177
    new-instance v0, Lcom/a/b/d/aep;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/aep;-><init>(Lcom/a/b/d/ael;Ljava/lang/Object;)V

    .line 77
    return-object v0
.end method

.method public final bridge synthetic e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 77
    invoke-super {p0}, Lcom/a/b/d/abu;->e()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/a/b/d/abu;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic h()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 77
    invoke-super {p0}, Lcom/a/b/d/abu;->h()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 77
    invoke-super {p0}, Lcom/a/b/d/abu;->hashCode()I

    move-result v0

    return v0
.end method

.method public final j()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 308
    invoke-super {p0}, Lcom/a/b/d/abu;->j()Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic k()I
    .registers 2

    .prologue
    .line 77
    invoke-super {p0}, Lcom/a/b/d/abu;->k()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic l()Ljava/util/Map;
    .registers 2

    .prologue
    .line 77
    invoke-super {p0}, Lcom/a/b/d/abu;->l()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final l_()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 304
    invoke-super {p0}, Lcom/a/b/d/abu;->l_()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic m()Ljava/util/Map;
    .registers 2

    .prologue
    .line 77
    .line 4308
    invoke-super {p0}, Lcom/a/b/d/abu;->j()Ljava/util/SortedMap;

    move-result-object v0

    .line 77
    return-object v0
.end method

.method final o()Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 317
    .line 4160
    iget-object v0, p0, Lcom/a/b/d/ael;->c:Ljava/util/Comparator;

    .line 319
    iget-object v1, p0, Lcom/a/b/d/ael;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    new-instance v2, Lcom/a/b/d/aem;

    invoke-direct {v2, p0}, Lcom/a/b/d/aem;-><init>(Lcom/a/b/d/ael;)V

    invoke-static {v1, v2}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/d/nj;->a(Ljava/lang/Iterable;Ljava/util/Comparator;)Lcom/a/b/d/agi;

    move-result-object v1

    .line 328
    new-instance v2, Lcom/a/b/d/aen;

    invoke-direct {v2, p0, v1, v0}, Lcom/a/b/d/aen;-><init>(Lcom/a/b/d/ael;Ljava/util/Iterator;Ljava/util/Comparator;)V

    return-object v2
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 77
    invoke-super {p0}, Lcom/a/b/d/abu;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
