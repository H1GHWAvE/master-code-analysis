.class Lcom/a/b/d/uc;
.super Lcom/a/b/d/uk;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/ty;


# direct methods
.method constructor <init>(Lcom/a/b/d/ty;)V
    .registers 2

    .prologue
    .line 2731
    iput-object p1, p0, Lcom/a/b/d/uc;->a:Lcom/a/b/d/ty;

    .line 2732
    invoke-direct {p0, p1}, Lcom/a/b/d/uk;-><init>(Ljava/util/Map;)V

    .line 2733
    return-void
.end method

.method private a(Lcom/a/b/b/co;)Z
    .registers 5

    .prologue
    .line 2744
    iget-object v0, p0, Lcom/a/b/d/uc;->a:Lcom/a/b/d/ty;

    iget-object v0, v0, Lcom/a/b/d/ty;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/uc;->a:Lcom/a/b/d/ty;

    iget-object v1, v1, Lcom/a/b/d/ty;->b:Lcom/a/b/b/co;

    invoke-static {p1}, Lcom/a/b/d/sz;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 2736
    iget-object v0, p0, Lcom/a/b/d/uc;->a:Lcom/a/b/d/ty;

    invoke-virtual {v0, p1}, Lcom/a/b/d/ty;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2737
    iget-object v0, p0, Lcom/a/b/d/uc;->a:Lcom/a/b/d/ty;

    iget-object v0, v0, Lcom/a/b/d/ty;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2738
    const/4 v0, 0x1

    .line 2740
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 2750
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/uc;->a(Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 2755
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/uc;->a(Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2760
    invoke-virtual {p0}, Lcom/a/b/d/uc;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 2764
    invoke-virtual {p0}, Lcom/a/b/d/uc;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
