.class public final Lcom/a/b/d/bk;
.super Lcom/a/b/d/m;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final b:I = 0x3

.field private static final c:J
    .annotation build Lcom/a/b/a/c;
        a = "Not needed in emulated source."
    .end annotation
.end field


# instance fields
.field transient a:I
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, v0}, Lcom/a/b/d/m;-><init>(Ljava/util/Map;)V

    .line 108
    const/4 v0, 0x3

    iput v0, p0, Lcom/a/b/d/bk;->a:I

    .line 109
    return-void
.end method

.method private constructor <init>(II)V
    .registers 4

    .prologue
    .line 112
    invoke-static {p1}, Lcom/a/b/d/sz;->a(I)Ljava/util/HashMap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/m;-><init>(Ljava/util/Map;)V

    .line 113
    const-string v0, "expectedValuesPerKey"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 114
    iput p2, p0, Lcom/a/b/d/bk;->a:I

    .line 115
    return-void
.end method

.method private constructor <init>(Lcom/a/b/d/vi;)V
    .registers 4

    .prologue
    .line 118
    invoke-interface {p1}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    instance-of v0, p1, Lcom/a/b/d/bk;

    if-eqz v0, :cond_18

    move-object v0, p1

    check-cast v0, Lcom/a/b/d/bk;

    iget v0, v0, Lcom/a/b/d/bk;->a:I

    :goto_11
    invoke-direct {p0, v1, v0}, Lcom/a/b/d/bk;-><init>(II)V

    .line 122
    invoke-virtual {p0, p1}, Lcom/a/b/d/bk;->a(Lcom/a/b/d/vi;)Z

    .line 123
    return-void

    .line 118
    :cond_18
    const/4 v0, 0x3

    goto :goto_11
.end method

.method private static a(II)Lcom/a/b/d/bk;
    .registers 3

    .prologue
    .line 92
    new-instance v0, Lcom/a/b/d/bk;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/bk;-><init>(II)V

    return-object v0
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 158
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 159
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/a/b/d/bk;->a:I

    .line 1050
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    .line 161
    invoke-static {v0}, Lcom/a/b/d/sz;->a(I)Ljava/util/HashMap;

    move-result-object v1

    .line 162
    invoke-virtual {p0, v1}, Lcom/a/b/d/bk;->a(Ljava/util/Map;)V

    .line 163
    invoke-static {p0, p1, v0}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/vi;Ljava/io/ObjectInputStream;I)V

    .line 164
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 151
    iget v0, p0, Lcom/a/b/d/bk;->a:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 152
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/vi;Ljava/io/ObjectOutputStream;)V

    .line 153
    return-void
.end method

.method private static b(Lcom/a/b/d/vi;)Lcom/a/b/d/bk;
    .registers 2

    .prologue
    .line 103
    new-instance v0, Lcom/a/b/d/bk;

    invoke-direct {v0, p0}, Lcom/a/b/d/bk;-><init>(Lcom/a/b/d/vi;)V

    return-object v0
.end method

.method private static t()Lcom/a/b/d/bk;
    .registers 1

    .prologue
    .line 78
    new-instance v0, Lcom/a/b/d/bk;

    invoke-direct {v0}, Lcom/a/b/d/bk;-><init>()V

    return-object v0
.end method

.method private u()V
    .registers 3

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/a/b/d/bk;->e()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 138
    check-cast v0, Ljava/util/ArrayList;

    .line 139
    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    goto :goto_c

    .line 141
    :cond_1e
    return-void
.end method


# virtual methods
.method final a()Ljava/util/List;
    .registers 3

    .prologue
    .line 130
    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lcom/a/b/d/bk;->a:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/util/List;
    .registers 3

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/a/b/d/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
    .registers 4

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lcom/a/b/d/m;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/a/b/d/vi;)Z
    .registers 3

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/a/b/d/m;->a(Lcom/a/b/d/vi;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lcom/a/b/d/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Ljava/util/List;
    .registers 3

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/a/b/d/m;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/a/b/d/m;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lcom/a/b/d/m;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final synthetic c()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/a/b/d/bk;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 4

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lcom/a/b/d/m;->c(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lcom/a/b/d/m;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/a/b/d/m;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic f()I
    .registers 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/a/b/d/m;->f()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic f(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/a/b/d/m;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic g()V
    .registers 1

    .prologue
    .line 66
    invoke-super {p0}, Lcom/a/b/d/m;->g()V

    return-void
.end method

.method public final bridge synthetic g(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/a/b/d/m;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/a/b/d/m;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/a/b/d/m;->i()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/a/b/d/m;->k()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic n()Z
    .registers 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/a/b/d/m;->n()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic p()Ljava/util/Set;
    .registers 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/a/b/d/m;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic q()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/a/b/d/m;->q()Lcom/a/b/d/xc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/a/b/d/m;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
