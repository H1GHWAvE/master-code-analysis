.class final Lcom/a/b/d/et;
.super Lcom/a/b/d/du;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method constructor <init>(Lcom/a/b/d/ep;)V
    .registers 2

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/a/b/d/du;-><init>(Lcom/a/b/d/ep;)V

    .line 35
    return-void
.end method

.method private static k()Ljava/lang/Comparable;
    .registers 1

    .prologue
    .line 38
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method private static m()Ljava/lang/Comparable;
    .registers 1

    .prologue
    .line 42
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/a/b/d/du;)Lcom/a/b/d/du;
    .registers 2

    .prologue
    .line 50
    return-object p0
.end method

.method final a(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;
    .registers 3

    .prologue
    .line 62
    return-object p0
.end method

.method final a(Ljava/lang/Comparable;ZLjava/lang/Comparable;Z)Lcom/a/b/d/du;
    .registers 5

    .prologue
    .line 67
    return-object p0
.end method

.method final bridge synthetic a(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 30
    return-object p0
.end method

.method final bridge synthetic a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 5

    .prologue
    .line 30
    return-object p0
.end method

.method public final a(Lcom/a/b/d/ce;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;
    .registers 4

    .prologue
    .line 58
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method final b(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;
    .registers 3

    .prologue
    .line 71
    return-object p0
.end method

.method final bridge synthetic b(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 30
    return-object p0
.end method

.method final c(Ljava/lang/Object;)I
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "not used by GWT emulation"
    .end annotation

    .prologue
    .line 76
    const/4 v0, -0x1

    return v0
.end method

.method public final c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 80
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/a/b/d/agi;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 85
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 4085
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    .line 30
    return-object v0
.end method

.method final e()Lcom/a/b/d/me;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 139
    new-instance v0, Lcom/a/b/d/fc;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/fc;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 105
    instance-of v0, p1, Ljava/util/Set;

    if-eqz v0, :cond_b

    .line 106
    check-cast p1, Ljava/util/Set;

    .line 107
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    .line 109
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final f()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 97
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method public final f_()Lcom/a/b/d/yl;
    .registers 2

    .prologue
    .line 54
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public final synthetic first()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2038
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method final g()Ljava/lang/Object;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "serialization"
    .end annotation

    .prologue
    .line 134
    new-instance v0, Lcom/a/b/d/ev;

    iget-object v1, p0, Lcom/a/b/d/et;->a:Lcom/a/b/d/ep;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/ev;-><init>(Lcom/a/b/d/ep;B)V

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 5080
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    .line 30
    return-object v0
.end method

.method public final synthetic last()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1042
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 101
    const-string v0, "[]"

    return-object v0
.end method
