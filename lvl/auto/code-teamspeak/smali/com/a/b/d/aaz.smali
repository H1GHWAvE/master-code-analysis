.class final Lcom/a/b/d/aaz;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/SortedSet;)Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 54
    invoke-interface {p0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 55
    if-nez v0, :cond_a

    .line 56
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    .line 58
    :cond_a
    return-object v0
.end method

.method public static a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z
    .registers 3

    .prologue
    .line 38
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    instance-of v0, p1, Ljava/util/SortedSet;

    if-eqz v0, :cond_15

    .line 42
    check-cast p1, Ljava/util/SortedSet;

    invoke-static {p1}, Lcom/a/b/d/aaz;->a(Ljava/util/SortedSet;)Ljava/util/Comparator;

    move-result-object v0

    .line 48
    :goto_10
    invoke-interface {p0, v0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_14
    return v0

    .line 43
    :cond_15
    instance-of v0, p1, Lcom/a/b/d/aay;

    if-eqz v0, :cond_20

    .line 44
    check-cast p1, Lcom/a/b/d/aay;

    invoke-interface {p1}, Lcom/a/b/d/aay;->comparator()Ljava/util/Comparator;

    move-result-object v0

    goto :goto_10

    .line 46
    :cond_20
    const/4 v0, 0x0

    goto :goto_14
.end method
