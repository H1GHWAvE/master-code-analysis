.class abstract Lcom/a/b/d/xo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/xd;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 785
    instance-of v1, p1, Lcom/a/b/d/xd;

    if-eqz v1, :cond_20

    .line 786
    check-cast p1, Lcom/a/b/d/xd;

    .line 787
    invoke-virtual {p0}, Lcom/a/b/d/xo;->b()I

    move-result v1

    invoke-interface {p1}, Lcom/a/b/d/xd;->b()I

    move-result v2

    if-ne v1, v2, :cond_20

    invoke-virtual {p0}, Lcom/a/b/d/xo;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    const/4 v0, 0x1

    .line 790
    :cond_20
    return v0
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 798
    invoke-virtual {p0}, Lcom/a/b/d/xo;->a()Ljava/lang/Object;

    move-result-object v0

    .line 799
    if-nez v0, :cond_d

    const/4 v0, 0x0

    :goto_7
    invoke-virtual {p0}, Lcom/a/b/d/xo;->b()I

    move-result v1

    xor-int/2addr v0, v1

    return v0

    :cond_d
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_7
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 810
    invoke-virtual {p0}, Lcom/a/b/d/xo;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 811
    invoke-virtual {p0}, Lcom/a/b/d/xo;->b()I

    move-result v1

    .line 812
    const/4 v2, 0x1

    if-ne v1, v2, :cond_10

    :goto_f
    return-object v0

    :cond_10
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xe

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " x "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_f
.end method
