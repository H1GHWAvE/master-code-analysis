.class public final Lcom/a/b/d/ip;
.super Lcom/a/b/d/ai;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final b:J
    .annotation build Lcom/a/b/a/c;
        a = "Not needed in emulated source."
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, v0}, Lcom/a/b/d/ai;-><init>(Ljava/util/Map;)V

    .line 73
    return-void
.end method

.method private constructor <init>(I)V
    .registers 3

    .prologue
    .line 76
    invoke-static {p1}, Lcom/a/b/d/sz;->a(I)Ljava/util/HashMap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ai;-><init>(Ljava/util/Map;)V

    .line 77
    return-void
.end method

.method private static a(I)Lcom/a/b/d/ip;
    .registers 2

    .prologue
    .line 53
    new-instance v0, Lcom/a/b/d/ip;

    invoke-direct {v0, p0}, Lcom/a/b/d/ip;-><init>(I)V

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/a/b/d/ip;
    .registers 3

    .prologue
    .line 65
    invoke-static {p0}, Lcom/a/b/d/xe;->a(Ljava/lang/Iterable;)I

    move-result v0

    .line 1053
    new-instance v1, Lcom/a/b/d/ip;

    invoke-direct {v1, v0}, Lcom/a/b/d/ip;-><init>(I)V

    .line 67
    invoke-static {v1, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 68
    return-object v1
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 2050
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    .line 94
    invoke-static {v0}, Lcom/a/b/d/sz;->a(I)Ljava/util/HashMap;

    move-result-object v1

    .line 2068
    iput-object v1, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    .line 96
    invoke-static {p0, p1, v0}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/xc;Ljava/io/ObjectInputStream;I)V

    .line 97
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 86
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/xc;Ljava/io/ObjectOutputStream;)V

    .line 87
    return-void
.end method

.method private static g()Lcom/a/b/d/ip;
    .registers 1

    .prologue
    .line 42
    new-instance v0, Lcom/a/b/d/ip;

    invoke-direct {v0}, Lcom/a/b/d/ip;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ai;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 34
    invoke-super {p0}, Lcom/a/b/d/ai;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;II)Z
    .registers 5

    .prologue
    .line 34
    invoke-super {p0, p1, p2, p3}, Lcom/a/b/d/ai;->a(Ljava/lang/Object;II)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic addAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ai;->b(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ai;->c(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic clear()V
    .registers 1

    .prologue
    .line 34
    invoke-super {p0}, Lcom/a/b/d/ai;->clear()V

    return-void
.end method

.method public final bridge synthetic contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 34
    invoke-super {p0}, Lcom/a/b/d/ai;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic isEmpty()Z
    .registers 2

    .prologue
    .line 34
    invoke-super {p0}, Lcom/a/b/d/ai;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 34
    invoke-super {p0}, Lcom/a/b/d/ai;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 34
    invoke-super {p0}, Lcom/a/b/d/ai;->n_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic size()I
    .registers 2

    .prologue
    .line 34
    invoke-super {p0}, Lcom/a/b/d/ai;->size()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 34
    invoke-super {p0}, Lcom/a/b/d/ai;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
