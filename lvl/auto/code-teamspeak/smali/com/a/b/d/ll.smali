.class public final Lcom/a/b/d/ll;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/b/d/yr;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549
    invoke-static {}, Lcom/a/b/d/afm;->c()Lcom/a/b/d/afm;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ll;->a:Lcom/a/b/d/yr;

    .line 550
    return-void
.end method

.method private a()Lcom/a/b/d/lf;
    .registers 2

    .prologue
    .line 589
    iget-object v0, p0, Lcom/a/b/d/ll;->a:Lcom/a/b/d/yr;

    invoke-static {v0}, Lcom/a/b/d/lf;->d(Lcom/a/b/d/yr;)Lcom/a/b/d/lf;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/a/b/d/yl;)Lcom/a/b/d/ll;
    .registers 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 560
    invoke-virtual {p1}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 561
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "range must not be empty, but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 562
    :cond_2f
    iget-object v0, p0, Lcom/a/b/d/ll;->a:Lcom/a/b/d/yr;

    invoke-interface {v0}, Lcom/a/b/d/yr;->f()Lcom/a/b/d/yr;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/yr;->c(Lcom/a/b/d/yl;)Z

    move-result v0

    if-nez v0, :cond_79

    .line 563
    iget-object v0, p0, Lcom/a/b/d/ll;->a:Lcom/a/b/d/yr;

    invoke-interface {v0}, Lcom/a/b/d/yr;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_45
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_71

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 564
    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v1

    if-eqz v1, :cond_61

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/yl;->f()Z

    move-result v1

    if-eqz v1, :cond_6f

    :cond_61
    move v1, v3

    :goto_62
    const-string v5, "Ranges may not overlap, but received %s and %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v2

    aput-object p1, v6, v3

    invoke-static {v1, v5, v6}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_45

    :cond_6f
    move v1, v2

    goto :goto_62

    .line 568
    :cond_71
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should have thrown an IAE above"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 570
    :cond_79
    iget-object v0, p0, Lcom/a/b/d/ll;->a:Lcom/a/b/d/yr;

    invoke-interface {v0, p1}, Lcom/a/b/d/yr;->a(Lcom/a/b/d/yl;)V

    .line 571
    return-object p0
.end method

.method private a(Lcom/a/b/d/yr;)Lcom/a/b/d/ll;
    .registers 10

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 579
    invoke-interface {p1}, Lcom/a/b/d/yr;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_94

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 1560
    invoke-virtual {v0}, Lcom/a/b/d/yl;->f()Z

    move-result v2

    if-eqz v2, :cond_43

    .line 1561
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "range must not be empty, but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1562
    :cond_43
    iget-object v2, p0, Lcom/a/b/d/ll;->a:Lcom/a/b/d/yr;

    invoke-interface {v2}, Lcom/a/b/d/yr;->f()Lcom/a/b/d/yr;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/a/b/d/yr;->c(Lcom/a/b/d/yl;)Z

    move-result v2

    if-nez v2, :cond_8d

    .line 1563
    iget-object v1, p0, Lcom/a/b/d/ll;->a:Lcom/a/b/d/yr;

    invoke-interface {v1}, Lcom/a/b/d/yr;->g()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_59
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_85

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/d/yl;

    .line 1564
    invoke-virtual {v1, v0}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v2

    if-eqz v2, :cond_75

    invoke-virtual {v1, v0}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/b/d/yl;->f()Z

    move-result v2

    if-eqz v2, :cond_83

    :cond_75
    move v2, v4

    :goto_76
    const-string v6, "Ranges may not overlap, but received %s and %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v3

    aput-object v0, v7, v4

    invoke-static {v2, v6, v7}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_59

    :cond_83
    move v2, v3

    goto :goto_76

    .line 1568
    :cond_85
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should have thrown an IAE above"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1570
    :cond_8d
    iget-object v2, p0, Lcom/a/b/d/ll;->a:Lcom/a/b/d/yr;

    invoke-interface {v2, v0}, Lcom/a/b/d/yr;->a(Lcom/a/b/d/yl;)V

    goto/16 :goto_a

    .line 582
    :cond_94
    return-object p0
.end method
