.class final Lcom/a/b/d/agh;
.super Lcom/a/b/d/agi;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/aga;

.field private final b:Ljava/util/Deque;


# direct methods
.method constructor <init>(Lcom/a/b/d/aga;Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 87
    iput-object p1, p0, Lcom/a/b/d/agh;->a:Lcom/a/b/d/aga;

    invoke-direct {p0}, Lcom/a/b/d/agi;-><init>()V

    .line 88
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/a/b/d/agh;->b:Ljava/util/Deque;

    .line 89
    iget-object v0, p0, Lcom/a/b/d/agh;->b:Ljava/util/Deque;

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/nj;->a(Ljava/lang/Object;)Lcom/a/b/d/agi;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 90
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/a/b/d/agh;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final next()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 99
    iget-object v0, p0, Lcom/a/b/d/agh;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    .line 100
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 101
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 102
    iget-object v0, p0, Lcom/a/b/d/agh;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeLast()Ljava/lang/Object;

    .line 104
    :cond_1b
    iget-object v0, p0, Lcom/a/b/d/agh;->a:Lcom/a/b/d/aga;

    invoke-virtual {v0, v1}, Lcom/a/b/d/aga;->a(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 105
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_30

    .line 106
    iget-object v2, p0, Lcom/a/b/d/agh;->b:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 108
    :cond_30
    return-object v1
.end method
