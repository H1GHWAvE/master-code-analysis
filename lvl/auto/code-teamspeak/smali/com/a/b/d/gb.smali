.class final Lcom/a/b/d/gb;
.super Ljava/util/AbstractCollection;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field private final a:Lcom/a/b/d/ga;


# direct methods
.method constructor <init>(Lcom/a/b/d/ga;)V
    .registers 3

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 42
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ga;

    iput-object v0, p0, Lcom/a/b/d/gb;->a:Lcom/a/b/d/ga;

    .line 43
    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/a/b/d/gb;->a:Lcom/a/b/d/ga;

    invoke-interface {v0}, Lcom/a/b/d/ga;->g()V

    .line 93
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 52
    iget-object v0, p0, Lcom/a/b/d/gb;->a:Lcom/a/b/d/ga;

    invoke-interface {v0, p1}, Lcom/a/b/d/ga;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/a/b/d/gb;->a:Lcom/a/b/d/ga;

    invoke-interface {v0}, Lcom/a/b/d/ga;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->b(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 62
    iget-object v0, p0, Lcom/a/b/d/gb;->a:Lcom/a/b/d/ga;

    invoke-interface {v0}, Lcom/a/b/d/ga;->c()Lcom/a/b/b/co;

    move-result-object v1

    .line 63
    iget-object v0, p0, Lcom/a/b/d/gb;->a:Lcom/a/b/d/ga;

    invoke-interface {v0}, Lcom/a/b/d/ga;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 64
    :cond_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 65
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 66
    invoke-interface {v1, v0}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 67
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 68
    const/4 v0, 0x1

    .line 71
    :goto_34
    return v0

    :cond_35
    const/4 v0, 0x0

    goto :goto_34
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 5

    .prologue
    .line 76
    iget-object v0, p0, Lcom/a/b/d/gb;->a:Lcom/a/b/d/ga;

    invoke-interface {v0}, Lcom/a/b/d/ga;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/gb;->a:Lcom/a/b/d/ga;

    invoke-interface {v1}, Lcom/a/b/d/ga;->c()Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/d/sz;->b(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 5

    .prologue
    .line 84
    iget-object v0, p0, Lcom/a/b/d/gb;->a:Lcom/a/b/d/ga;

    invoke-interface {v0}, Lcom/a/b/d/ga;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/gb;->a:Lcom/a/b/d/ga;

    invoke-interface {v1}, Lcom/a/b/d/ga;->c()Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/d/sz;->b(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/a/b/d/gb;->a:Lcom/a/b/d/ga;

    invoke-interface {v0}, Lcom/a/b/d/ga;->f()I

    move-result v0

    return v0
.end method
