.class Lcom/a/b/d/wn;
.super Lcom/a/b/d/as;
.source "SourceFile"


# instance fields
.field final b:Lcom/a/b/d/vi;


# direct methods
.method constructor <init>(Lcom/a/b/d/vi;)V
    .registers 2

    .prologue
    .line 1517
    invoke-direct {p0}, Lcom/a/b/d/as;-><init>()V

    .line 1518
    iput-object p1, p0, Lcom/a/b/d/wn;->b:Lcom/a/b/d/vi;

    .line 1519
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1598
    iget-object v0, p0, Lcom/a/b/d/wn;->b:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 1599
    if-nez v0, :cond_10

    const/4 v0, 0x0

    :goto_f
    return v0

    :cond_10
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    goto :goto_f
.end method

.method public b(Ljava/lang/Object;I)I
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1603
    const-string v0, "occurrences"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 1604
    if-nez p2, :cond_d

    .line 1605
    invoke-virtual {p0, p1}, Lcom/a/b/d/wn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 1624
    :goto_c
    return v0

    .line 1608
    :cond_d
    iget-object v0, p0, Lcom/a/b/d/wn;->b:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 1610
    if-nez v0, :cond_1d

    move v0, v1

    .line 1611
    goto :goto_c

    .line 1614
    :cond_1d
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    .line 1615
    if-lt p2, v2, :cond_28

    .line 1616
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    :cond_26
    move v0, v2

    .line 1624
    goto :goto_c

    .line 1618
    :cond_28
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1619
    :goto_2c
    if-ge v1, p2, :cond_26

    .line 1620
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 1621
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 1619
    add-int/lit8 v1, v1, 0x1

    goto :goto_2c
.end method

.method final b()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 1522
    new-instance v0, Lcom/a/b/d/wo;

    iget-object v1, p0, Lcom/a/b/d/wn;->b:Lcom/a/b/d/vi;

    invoke-interface {v1}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/wo;-><init>(Lcom/a/b/d/wn;Ljava/util/Iterator;)V

    return-object v0
.end method

.method final c()I
    .registers 2

    .prologue
    .line 1543
    iget-object v0, p0, Lcom/a/b/d/wn;->b:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 1628
    iget-object v0, p0, Lcom/a/b/d/wn;->b:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->g()V

    .line 1629
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1590
    iget-object v0, p0, Lcom/a/b/d/wn;->b:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final f()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1547
    new-instance v0, Lcom/a/b/d/wq;

    invoke-direct {v0, p0}, Lcom/a/b/d/wq;-><init>(Lcom/a/b/d/wn;)V

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1594
    iget-object v0, p0, Lcom/a/b/d/wn;->b:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1632
    iget-object v0, p0, Lcom/a/b/d/wn;->b:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
