.class final Lcom/a/b/d/afr;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field a:Lcom/a/b/d/dw;

.field final synthetic b:Lcom/a/b/d/dw;

.field final synthetic c:Lcom/a/b/d/yi;

.field final synthetic d:Lcom/a/b/d/afq;


# direct methods
.method constructor <init>(Lcom/a/b/d/afq;Lcom/a/b/d/dw;Lcom/a/b/d/yi;)V
    .registers 5

    .prologue
    .line 484
    iput-object p1, p0, Lcom/a/b/d/afr;->d:Lcom/a/b/d/afq;

    iput-object p2, p0, Lcom/a/b/d/afr;->b:Lcom/a/b/d/dw;

    iput-object p3, p0, Lcom/a/b/d/afr;->c:Lcom/a/b/d/yi;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    .line 485
    iget-object v0, p0, Lcom/a/b/d/afr;->b:Lcom/a/b/d/dw;

    iput-object v0, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    return-void
.end method

.method private c()Ljava/util/Map$Entry;
    .registers 4

    .prologue
    .line 489
    iget-object v0, p0, Lcom/a/b/d/afr;->d:Lcom/a/b/d/afq;

    invoke-static {v0}, Lcom/a/b/d/afq;->a(Lcom/a/b/d/afq;)Lcom/a/b/d/yl;

    move-result-object v0

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v1, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v1

    if-ne v0, v1, :cond_1d

    .line 491
    :cond_18
    invoke-virtual {p0}, Lcom/a/b/d/afr;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    .line 502
    :goto_1c
    return-object v0

    .line 494
    :cond_1d
    iget-object v0, p0, Lcom/a/b/d/afr;->c:Lcom/a/b/d/yi;

    invoke-interface {v0}, Lcom/a/b/d/yi;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 495
    iget-object v0, p0, Lcom/a/b/d/afr;->c:Lcom/a/b/d/yi;

    invoke-interface {v0}, Lcom/a/b/d/yi;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 496
    iget-object v1, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    iget-object v2, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {v1, v2}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v1

    .line 497
    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iput-object v0, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    move-object v0, v1

    .line 502
    :goto_3a
    iget-object v1, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_1c

    .line 499
    :cond_41
    iget-object v0, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    .line 500
    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v1

    iput-object v1, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    goto :goto_3a
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 484
    .line 1489
    iget-object v0, p0, Lcom/a/b/d/afr;->d:Lcom/a/b/d/afq;

    invoke-static {v0}, Lcom/a/b/d/afq;->a(Lcom/a/b/d/afq;)Lcom/a/b/d/yl;

    move-result-object v0

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v1, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v1

    if-ne v0, v1, :cond_1d

    .line 1491
    :cond_18
    invoke-virtual {p0}, Lcom/a/b/d/afr;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    :goto_1c
    return-object v0

    .line 1494
    :cond_1d
    iget-object v0, p0, Lcom/a/b/d/afr;->c:Lcom/a/b/d/yi;

    invoke-interface {v0}, Lcom/a/b/d/yi;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 1495
    iget-object v0, p0, Lcom/a/b/d/afr;->c:Lcom/a/b/d/yi;

    invoke-interface {v0}, Lcom/a/b/d/yi;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 1496
    iget-object v1, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    iget-object v2, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {v1, v2}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v1

    .line 1497
    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iput-object v0, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    move-object v0, v1

    .line 1502
    :goto_3a
    iget-object v1, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_1c

    .line 1499
    :cond_41
    iget-object v0, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    .line 1500
    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v1

    iput-object v1, p0, Lcom/a/b/d/afr;->a:Lcom/a/b/d/dw;

    goto :goto_3a
.end method
