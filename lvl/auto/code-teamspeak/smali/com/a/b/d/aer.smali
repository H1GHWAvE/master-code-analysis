.class public final Lcom/a/b/d/aer;
.super Lcom/a/b/d/bc;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final e:J = 0x1L
    .annotation build Lcom/a/b/a/c;
        a = "not needed in emulated source"
    .end annotation
.end field


# instance fields
.field private final transient b:Lcom/a/b/d/afa;

.field private final transient c:Lcom/a/b/d/hs;

.field private final transient d:Lcom/a/b/d/aez;


# direct methods
.method private constructor <init>(Lcom/a/b/d/afa;Lcom/a/b/d/hs;Lcom/a/b/d/aez;)V
    .registers 5

    .prologue
    .line 116
    .line 1129
    iget-object v0, p2, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    .line 116
    invoke-direct {p0, v0}, Lcom/a/b/d/bc;-><init>(Ljava/util/Comparator;)V

    .line 117
    iput-object p1, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 118
    iput-object p2, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 119
    iput-object p3, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    .line 120
    return-void
.end method

.method private constructor <init>(Ljava/util/Comparator;)V
    .registers 5

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/a/b/d/bc;-><init>(Ljava/util/Comparator;)V

    .line 124
    invoke-static {p1}, Lcom/a/b/d/hs;->a(Ljava/util/Comparator;)Lcom/a/b/d/hs;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 125
    new-instance v0, Lcom/a/b/d/aez;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/aez;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    .line 126
    iget-object v0, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    iget-object v1, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    invoke-static {v0, v1}, Lcom/a/b/d/aer;->b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V

    .line 127
    new-instance v0, Lcom/a/b/d/afa;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/a/b/d/afa;-><init>(B)V

    iput-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 128
    return-void
.end method

.method static a(Lcom/a/b/d/aez;)I
    .registers 2
    .param p0    # Lcom/a/b/d/aez;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 501
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return v0

    .line 30519
    :cond_4
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    goto :goto_3
.end method

.method private a(Lcom/a/b/d/aew;)J
    .registers 8

    .prologue
    .line 162
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 1508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 162
    check-cast v0, Lcom/a/b/d/aez;

    .line 163
    invoke-virtual {p1, v0}, Lcom/a/b/d/aew;->b(Lcom/a/b/d/aez;)J

    move-result-wide v2

    .line 164
    iget-object v1, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 2133
    iget-boolean v1, v1, Lcom/a/b/d/hs;->b:Z

    .line 164
    if-eqz v1, :cond_15

    .line 165
    invoke-direct {p0, p1, v0}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aew;Lcom/a/b/d/aez;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 167
    :cond_15
    iget-object v1, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 2137
    iget-boolean v1, v1, Lcom/a/b/d/hs;->e:Z

    .line 167
    if-eqz v1, :cond_20

    .line 168
    invoke-direct {p0, p1, v0}, Lcom/a/b/d/aer;->b(Lcom/a/b/d/aew;Lcom/a/b/d/aez;)J

    move-result-wide v0

    sub-long/2addr v2, v0

    .line 170
    :cond_20
    return-wide v2
.end method

.method private a(Lcom/a/b/d/aew;Lcom/a/b/d/aez;)J
    .registers 7
    .param p2    # Lcom/a/b/d/aez;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 174
    :goto_0
    if-nez p2, :cond_5

    .line 175
    const-wide/16 v0, 0x0

    .line 190
    :goto_4
    return-wide v0

    .line 177
    :cond_5
    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 2269
    iget-object v1, v1, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 2519
    iget-object v2, p2, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 177
    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 178
    if-gez v0, :cond_18

    .line 3519
    iget-object p2, p2, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    goto :goto_0

    .line 180
    :cond_18
    if-nez v0, :cond_43

    .line 181
    sget-object v0, Lcom/a/b/d/aev;->a:[I

    iget-object v1, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 4273
    iget-object v1, v1, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    .line 181
    invoke-virtual {v1}, Lcom/a/b/d/ce;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_58

    .line 187
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 183
    :pswitch_2f
    invoke-virtual {p1, p2}, Lcom/a/b/d/aew;->a(Lcom/a/b/d/aez;)I

    move-result v0

    int-to-long v0, v0

    .line 4519
    iget-object v2, p2, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 183
    invoke-virtual {p1, v2}, Lcom/a/b/d/aew;->b(Lcom/a/b/d/aez;)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_4

    .line 5519
    :pswitch_3c
    iget-object v0, p2, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 185
    invoke-virtual {p1, v0}, Lcom/a/b/d/aew;->b(Lcom/a/b/d/aez;)J

    move-result-wide v0

    goto :goto_4

    .line 6519
    :cond_43
    iget-object v0, p2, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 190
    invoke-virtual {p1, v0}, Lcom/a/b/d/aew;->b(Lcom/a/b/d/aez;)J

    move-result-wide v0

    invoke-virtual {p1, p2}, Lcom/a/b/d/aew;->a(Lcom/a/b/d/aez;)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 7519
    iget-object v2, p2, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 190
    invoke-direct {p0, p1, v2}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aew;Lcom/a/b/d/aez;)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_4

    .line 181
    nop

    :pswitch_data_58
    .packed-switch 0x1
        :pswitch_2f
        :pswitch_3c
    .end packed-switch
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/aer;
    .registers 3

    .prologue
    .line 1074
    new-instance v0, Lcom/a/b/d/aer;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/aer;-><init>(Ljava/util/Comparator;)V

    .line 107
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 108
    return-object v0
.end method

.method public static a(Ljava/util/Comparator;)Lcom/a/b/d/aer;
    .registers 3
    .param p0    # Ljava/util/Comparator;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 91
    if-nez p0, :cond_c

    new-instance v0, Lcom/a/b/d/aer;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/aer;-><init>(Ljava/util/Comparator;)V

    :goto_b
    return-object v0

    :cond_c
    new-instance v0, Lcom/a/b/d/aer;

    invoke-direct {v0, p0}, Lcom/a/b/d/aer;-><init>(Ljava/util/Comparator;)V

    goto :goto_b
.end method

.method static synthetic a(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 59
    .line 33357
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 33508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 33357
    check-cast v0, Lcom/a/b/d/aez;

    .line 33358
    if-nez v0, :cond_b

    move-object v0, v1

    .line 33375
    :cond_a
    :goto_a
    return-object v0

    .line 33362
    :cond_b
    iget-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 34133
    iget-boolean v0, v0, Lcom/a/b/d/hs;->b:Z

    .line 33362
    if-eqz v0, :cond_4d

    .line 33363
    iget-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 34269
    iget-object v2, v0, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 33364
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 34508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 33364
    check-cast v0, Lcom/a/b/d/aez;

    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v3

    .line 34519
    invoke-virtual {v0, v3, v2}, Lcom/a/b/d/aez;->a(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;

    move-result-object v0

    .line 33365
    if-nez v0, :cond_27

    move-object v0, v1

    .line 33366
    goto :goto_a

    .line 33368
    :cond_27
    iget-object v3, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 35273
    iget-object v3, v3, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    .line 33368
    sget-object v4, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-ne v3, v4, :cond_3d

    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v3

    .line 35923
    iget-object v4, v0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 33368
    invoke-interface {v3, v2, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-nez v2, :cond_3d

    .line 36519
    iget-object v0, v0, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    .line 33375
    :cond_3d
    :goto_3d
    iget-object v2, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    if-eq v0, v2, :cond_4b

    iget-object v2, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 37923
    iget-object v3, v0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 33375
    invoke-virtual {v2, v3}, Lcom/a/b/d/hs;->c(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_4b
    move-object v0, v1

    goto :goto_a

    .line 33373
    :cond_4d
    iget-object v0, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    .line 37519
    iget-object v0, v0, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    goto :goto_3d
.end method

.method static synthetic a(Lcom/a/b/d/aer;Lcom/a/b/d/aez;)Lcom/a/b/d/xd;
    .registers 3

    .prologue
    .line 38335
    new-instance v0, Lcom/a/b/d/aes;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/aes;-><init>(Lcom/a/b/d/aer;Lcom/a/b/d/aez;)V

    .line 59
    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
    .registers 2

    .prologue
    .line 59
    invoke-static {p0, p1}, Lcom/a/b/d/aer;->b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V

    return-void
.end method

.method static synthetic a(Lcom/a/b/d/aez;Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
    .registers 3

    .prologue
    .line 59
    invoke-static {p0, p1, p2}, Lcom/a/b/d/aer;->b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V

    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 966
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 969
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    .line 970
    const-class v1, Lcom/a/b/d/bc;

    const-string v2, "comparator"

    invoke-static {v1, v2}, Lcom/a/b/d/zz;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/a/b/d/aab;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/a/b/d/aab;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 971
    const-class v1, Lcom/a/b/d/aer;

    const-string v2, "range"

    invoke-static {v1, v2}, Lcom/a/b/d/zz;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/a/b/d/aab;

    move-result-object v1

    invoke-static {v0}, Lcom/a/b/d/hs;->a(Ljava/util/Comparator;)Lcom/a/b/d/hs;

    move-result-object v0

    invoke-virtual {v1, p0, v0}, Lcom/a/b/d/aab;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 974
    const-class v0, Lcom/a/b/d/aer;

    const-string v1, "rootReference"

    invoke-static {v0, v1}, Lcom/a/b/d/zz;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/a/b/d/aab;

    move-result-object v0

    new-instance v1, Lcom/a/b/d/afa;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/a/b/d/afa;-><init>(B)V

    invoke-virtual {v0, p0, v1}, Lcom/a/b/d/aab;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 977
    new-instance v0, Lcom/a/b/d/aez;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/aez;-><init>(Ljava/lang/Object;I)V

    .line 978
    const-class v1, Lcom/a/b/d/aer;

    const-string v2, "header"

    invoke-static {v1, v2}, Lcom/a/b/d/zz;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/a/b/d/aab;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/a/b/d/aab;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 979
    invoke-static {v0, v0}, Lcom/a/b/d/aer;->b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V

    .line 980
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/xc;Ljava/io/ObjectInputStream;)V

    .line 981
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 959
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 33058
    invoke-super {p0}, Lcom/a/b/d/bc;->e_()Ljava/util/NavigableSet;

    move-result-object v0

    .line 960
    invoke-interface {v0}, Ljava/util/NavigableSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 961
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/xc;Ljava/io/ObjectOutputStream;)V

    .line 962
    return-void
.end method

.method private b(Lcom/a/b/d/aew;Lcom/a/b/d/aez;)J
    .registers 7
    .param p2    # Lcom/a/b/d/aez;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 196
    :goto_0
    if-nez p2, :cond_5

    .line 197
    const-wide/16 v0, 0x0

    .line 212
    :goto_4
    return-wide v0

    .line 199
    :cond_5
    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 8277
    iget-object v1, v1, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 8519
    iget-object v2, p2, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 199
    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 200
    if-lez v0, :cond_18

    .line 9519
    iget-object p2, p2, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    goto :goto_0

    .line 202
    :cond_18
    if-nez v0, :cond_43

    .line 203
    sget-object v0, Lcom/a/b/d/aev;->a:[I

    iget-object v1, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 10281
    iget-object v1, v1, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    .line 203
    invoke-virtual {v1}, Lcom/a/b/d/ce;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_58

    .line 209
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 205
    :pswitch_2f
    invoke-virtual {p1, p2}, Lcom/a/b/d/aew;->a(Lcom/a/b/d/aez;)I

    move-result v0

    int-to-long v0, v0

    .line 10519
    iget-object v2, p2, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 205
    invoke-virtual {p1, v2}, Lcom/a/b/d/aew;->b(Lcom/a/b/d/aez;)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_4

    .line 11519
    :pswitch_3c
    iget-object v0, p2, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 207
    invoke-virtual {p1, v0}, Lcom/a/b/d/aew;->b(Lcom/a/b/d/aez;)J

    move-result-wide v0

    goto :goto_4

    .line 12519
    :cond_43
    iget-object v0, p2, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 212
    invoke-virtual {p1, v0}, Lcom/a/b/d/aew;->b(Lcom/a/b/d/aez;)J

    move-result-wide v0

    invoke-virtual {p1, p2}, Lcom/a/b/d/aew;->a(Lcom/a/b/d/aez;)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 13519
    iget-object v2, p2, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 212
    invoke-direct {p0, p1, v2}, Lcom/a/b/d/aer;->b(Lcom/a/b/d/aew;Lcom/a/b/d/aez;)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_4

    .line 203
    nop

    :pswitch_data_58
    .packed-switch 0x1
        :pswitch_2f
        :pswitch_3c
    .end packed-switch
.end method

.method static synthetic b(Lcom/a/b/d/aer;)Lcom/a/b/d/hs;
    .registers 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    return-object v0
.end method

.method private b(Lcom/a/b/d/aez;)Lcom/a/b/d/xd;
    .registers 3

    .prologue
    .line 335
    new-instance v0, Lcom/a/b/d/aes;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/aes;-><init>(Lcom/a/b/d/aer;Lcom/a/b/d/aez;)V

    return-object v0
.end method

.method private static b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
    .registers 2

    .prologue
    .line 938
    .line 31519
    iput-object p1, p0, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    .line 32519
    iput-object p0, p1, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    .line 940
    return-void
.end method

.method private static b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
    .registers 3

    .prologue
    .line 943
    invoke-static {p0, p1}, Lcom/a/b/d/aer;->b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V

    .line 944
    invoke-static {p1, p2}, Lcom/a/b/d/aer;->b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V

    .line 945
    return-void
.end method

.method static synthetic c(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;
    .registers 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    return-object v0
.end method

.method static synthetic d(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 59
    .line 38379
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 38508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 38379
    check-cast v0, Lcom/a/b/d/aez;

    .line 38380
    if-nez v0, :cond_b

    move-object v0, v1

    .line 38397
    :cond_a
    :goto_a
    return-object v0

    .line 38384
    :cond_b
    iget-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 39137
    iget-boolean v0, v0, Lcom/a/b/d/hs;->e:Z

    .line 38384
    if-eqz v0, :cond_4d

    .line 38385
    iget-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 39277
    iget-object v2, v0, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 38386
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 39508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 38386
    check-cast v0, Lcom/a/b/d/aez;

    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v3

    .line 39519
    invoke-virtual {v0, v3, v2}, Lcom/a/b/d/aez;->b(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;

    move-result-object v0

    .line 38387
    if-nez v0, :cond_27

    move-object v0, v1

    .line 38388
    goto :goto_a

    .line 38390
    :cond_27
    iget-object v3, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 40281
    iget-object v3, v3, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    .line 38390
    sget-object v4, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-ne v3, v4, :cond_3d

    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v3

    .line 40923
    iget-object v4, v0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 38390
    invoke-interface {v3, v2, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-nez v2, :cond_3d

    .line 41519
    iget-object v0, v0, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    .line 38397
    :cond_3d
    :goto_3d
    iget-object v2, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    if-eq v0, v2, :cond_4b

    iget-object v2, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 42923
    iget-object v3, v0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 38397
    invoke-virtual {v2, v3}, Lcom/a/b/d/hs;->c(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_4b
    move-object v0, v1

    goto :goto_a

    .line 38395
    :cond_4d
    iget-object v0, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    .line 42519
    iget-object v0, v0, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    goto :goto_3d
.end method

.method private static o()Lcom/a/b/d/aer;
    .registers 2

    .prologue
    .line 74
    new-instance v0, Lcom/a/b/d/aer;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/aer;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private p()Lcom/a/b/d/aez;
    .registers 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 357
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 19508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 357
    check-cast v0, Lcom/a/b/d/aez;

    .line 358
    if-nez v0, :cond_b

    move-object v0, v1

    .line 375
    :cond_a
    :goto_a
    return-object v0

    .line 362
    :cond_b
    iget-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 20133
    iget-boolean v0, v0, Lcom/a/b/d/hs;->b:Z

    .line 362
    if-eqz v0, :cond_4d

    .line 363
    iget-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 20269
    iget-object v2, v0, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 364
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 20508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 364
    check-cast v0, Lcom/a/b/d/aez;

    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v3

    .line 20519
    invoke-virtual {v0, v3, v2}, Lcom/a/b/d/aez;->a(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;

    move-result-object v0

    .line 365
    if-nez v0, :cond_27

    move-object v0, v1

    .line 366
    goto :goto_a

    .line 368
    :cond_27
    iget-object v3, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 21273
    iget-object v3, v3, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    .line 368
    sget-object v4, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-ne v3, v4, :cond_3d

    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v3

    .line 21923
    iget-object v4, v0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 368
    invoke-interface {v3, v2, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-nez v2, :cond_3d

    .line 22519
    iget-object v0, v0, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    .line 375
    :cond_3d
    :goto_3d
    iget-object v2, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    if-eq v0, v2, :cond_4b

    iget-object v2, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 23923
    iget-object v3, v0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 375
    invoke-virtual {v2, v3}, Lcom/a/b/d/hs;->c(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_4b
    move-object v0, v1

    goto :goto_a

    .line 373
    :cond_4d
    iget-object v0, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    .line 23519
    iget-object v0, v0, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    goto :goto_3d
.end method

.method private q()Lcom/a/b/d/aez;
    .registers 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 379
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 24508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 379
    check-cast v0, Lcom/a/b/d/aez;

    .line 380
    if-nez v0, :cond_b

    move-object v0, v1

    .line 397
    :cond_a
    :goto_a
    return-object v0

    .line 384
    :cond_b
    iget-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 25137
    iget-boolean v0, v0, Lcom/a/b/d/hs;->e:Z

    .line 384
    if-eqz v0, :cond_4d

    .line 385
    iget-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 25277
    iget-object v2, v0, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 386
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 25508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 386
    check-cast v0, Lcom/a/b/d/aez;

    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v3

    .line 25519
    invoke-virtual {v0, v3, v2}, Lcom/a/b/d/aez;->b(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;

    move-result-object v0

    .line 387
    if-nez v0, :cond_27

    move-object v0, v1

    .line 388
    goto :goto_a

    .line 390
    :cond_27
    iget-object v3, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 26281
    iget-object v3, v3, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    .line 390
    sget-object v4, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-ne v3, v4, :cond_3d

    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v3

    .line 26923
    iget-object v4, v0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 390
    invoke-interface {v3, v2, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-nez v2, :cond_3d

    .line 27519
    iget-object v0, v0, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    .line 397
    :cond_3d
    :goto_3d
    iget-object v2, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    if-eq v0, v2, :cond_4b

    iget-object v2, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    .line 28923
    iget-object v3, v0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 397
    invoke-virtual {v2, v3}, Lcom/a/b/d/hs;->c(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_4b
    move-object v0, v1

    goto :goto_a

    .line 395
    :cond_4d
    iget-object v0, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    .line 28519
    iget-object v0, v0, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    goto :goto_3d
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 232
    :try_start_1
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 14508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 232
    check-cast v0, Lcom/a/b/d/aez;

    .line 233
    iget-object v2, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    invoke-virtual {v2, p1}, Lcom/a/b/d/hs;->c(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    if-nez v0, :cond_13

    :cond_11
    move v0, v1

    .line 14549
    :goto_12
    return v0

    .line 236
    :cond_13
    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v2

    .line 14545
    :goto_17
    iget-object v3, v0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    invoke-interface {v2, p1, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 14546
    if-gez v3, :cond_28

    .line 14547
    iget-object v3, v0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    if-nez v3, :cond_25

    move v0, v1

    goto :goto_12

    :cond_25
    iget-object v0, v0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    goto :goto_17

    .line 14548
    :cond_28
    if-lez v3, :cond_33

    .line 14549
    iget-object v3, v0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    if-nez v3, :cond_30

    move v0, v1

    goto :goto_12

    :cond_30
    iget-object v0, v0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    goto :goto_17

    .line 14551
    :cond_33
    iget v0, v0, Lcom/a/b/d/aez;->b:I
    :try_end_35
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_35} :catch_36
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_35} :catch_39

    goto :goto_12

    .line 238
    :catch_36
    move-exception v0

    move v0, v1

    goto :goto_12

    .line 240
    :catch_39
    move-exception v0

    move v0, v1

    goto :goto_12
.end method

.method public final a(Ljava/lang/Object;I)I
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 246
    const-string v0, "occurrences"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 247
    if-nez p2, :cond_d

    .line 248
    invoke-virtual {p0, p1}, Lcom/a/b/d/aer;->a(Ljava/lang/Object;)I

    move-result v0

    .line 262
    :goto_c
    return v0

    .line 250
    :cond_d
    iget-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    invoke-virtual {v0, p1}, Lcom/a/b/d/hs;->c(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 251
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 15508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 251
    check-cast v0, Lcom/a/b/d/aez;

    .line 252
    if-nez v0, :cond_38

    .line 253
    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-interface {v2, p1, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    .line 254
    new-instance v2, Lcom/a/b/d/aez;

    invoke-direct {v2, p1, p2}, Lcom/a/b/d/aez;-><init>(Ljava/lang/Object;I)V

    .line 255
    iget-object v3, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    iget-object v4, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    invoke-static {v3, v2, v4}, Lcom/a/b/d/aer;->b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V

    .line 256
    iget-object v3, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    invoke-virtual {v3, v0, v2}, Lcom/a/b/d/afa;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    move v0, v1

    .line 257
    goto :goto_c

    .line 259
    :cond_38
    const/4 v2, 0x1

    new-array v2, v2, [I

    .line 260
    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v3

    invoke-virtual {v0, v3, p1, p2, v2}, Lcom/a/b/d/aez;->a(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;

    move-result-object v3

    .line 261
    iget-object v4, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    invoke-virtual {v4, v0, v3}, Lcom/a/b/d/afa;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 262
    aget v0, v2, v1

    goto :goto_c
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 6

    .prologue
    .line 58
    invoke-super {p0, p1, p2, p3, p4}, Lcom/a/b/d/bc;->a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;II)Z
    .registers 12
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 313
    const-string v0, "newCount"

    invoke-static {p3, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 314
    const-string v0, "oldCount"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 315
    iget-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    invoke-virtual {v0, p1}, Lcom/a/b/d/hs;->c(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 317
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 18508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 317
    check-cast v0, Lcom/a/b/d/aez;

    .line 318
    if-nez v0, :cond_28

    .line 319
    if-nez p2, :cond_26

    .line 320
    if-lez p3, :cond_24

    .line 321
    invoke-virtual {p0, p1, p3}, Lcom/a/b/d/aer;->a(Ljava/lang/Object;I)I

    :cond_24
    move v0, v6

    .line 331
    :goto_25
    return v0

    :cond_26
    move v0, v7

    .line 325
    goto :goto_25

    .line 328
    :cond_28
    new-array v5, v6, [I

    .line 329
    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v1

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/a/b/d/aez;->a(Ljava/util/Comparator;Ljava/lang/Object;II[I)Lcom/a/b/d/aez;

    move-result-object v1

    .line 330
    iget-object v2, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    invoke-virtual {v2, v0, v1}, Lcom/a/b/d/afa;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 331
    aget v0, v5, v7

    if-ne v0, p2, :cond_40

    move v0, v6

    goto :goto_25

    :cond_40
    move v0, v7

    goto :goto_25
.end method

.method public final bridge synthetic add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/a/b/d/bc;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic addAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/a/b/d/bc;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 267
    const-string v0, "occurrences"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 268
    if-nez p2, :cond_d

    .line 269
    invoke-virtual {p0, p1}, Lcom/a/b/d/aer;->a(Ljava/lang/Object;)I

    move-result v0

    .line 287
    :goto_c
    return v0

    .line 271
    :cond_d
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 16508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 271
    check-cast v0, Lcom/a/b/d/aez;

    .line 272
    const/4 v2, 0x1

    new-array v2, v2, [I

    .line 277
    :try_start_16
    iget-object v3, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    invoke-virtual {v3, p1}, Lcom/a/b/d/hs;->c(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_20

    if-nez v0, :cond_22

    :cond_20
    move v0, v1

    .line 278
    goto :goto_c

    .line 280
    :cond_22
    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v3

    invoke-virtual {v0, v3, p1, p2, v2}, Lcom/a/b/d/aez;->b(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;
    :try_end_29
    .catch Ljava/lang/ClassCastException; {:try_start_16 .. :try_end_29} :catch_32
    .catch Ljava/lang/NullPointerException; {:try_start_16 .. :try_end_29} :catch_35

    move-result-object v3

    .line 286
    iget-object v4, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    invoke-virtual {v4, v0, v3}, Lcom/a/b/d/afa;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 287
    aget v0, v2, v1

    goto :goto_c

    .line 282
    :catch_32
    move-exception v0

    move v0, v1

    goto :goto_c

    .line 284
    :catch_35
    move-exception v0

    move v0, v1

    goto :goto_c
.end method

.method final b()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 402
    new-instance v0, Lcom/a/b/d/aet;

    invoke-direct {v0, p0}, Lcom/a/b/d/aet;-><init>(Lcom/a/b/d/aer;)V

    return-object v0
.end method

.method final c()I
    .registers 3

    .prologue
    .line 224
    sget-object v0, Lcom/a/b/d/aew;->b:Lcom/a/b/d/aew;

    invoke-direct {p0, v0}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aew;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/a/b/l/q;->b(J)I

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/Object;I)I
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 292
    const-string v0, "count"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 293
    iget-object v0, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    invoke-virtual {v0, p1}, Lcom/a/b/d/hs;->c(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 294
    if-nez p2, :cond_16

    move v0, v1

    :goto_12
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 308
    :cond_15
    :goto_15
    return v2

    :cond_16
    move v0, v2

    .line 294
    goto :goto_12

    .line 298
    :cond_18
    iget-object v0, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    .line 17508
    iget-object v0, v0, Lcom/a/b/d/afa;->a:Ljava/lang/Object;

    .line 298
    check-cast v0, Lcom/a/b/d/aez;

    .line 299
    if-nez v0, :cond_26

    .line 300
    if-lez p2, :cond_15

    .line 301
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/aer;->a(Ljava/lang/Object;I)I

    goto :goto_15

    .line 305
    :cond_26
    new-array v1, v1, [I

    .line 306
    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v3

    invoke-virtual {v0, v3, p1, p2, v1}, Lcom/a/b/d/aez;->c(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;

    move-result-object v3

    .line 307
    iget-object v4, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    invoke-virtual {v4, v0, v3}, Lcom/a/b/d/afa;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 308
    aget v2, v1, v2

    goto :goto_15
.end method

.method public final c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 14
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 494
    new-instance v8, Lcom/a/b/d/aer;

    iget-object v9, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    iget-object v10, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v1

    .line 30069
    new-instance v0, Lcom/a/b/d/hs;

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v7}, Lcom/a/b/d/hs;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V

    .line 494
    invoke-virtual {v10, v0}, Lcom/a/b/d/hs;->a(Lcom/a/b/d/hs;)Lcom/a/b/d/hs;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    invoke-direct {v8, v9, v0, v1}, Lcom/a/b/d/aer;-><init>(Lcom/a/b/d/afa;Lcom/a/b/d/hs;Lcom/a/b/d/aez;)V

    return-object v8
.end method

.method public final bridge synthetic clear()V
    .registers 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->clear()V

    return-void
.end method

.method public final bridge synthetic comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/a/b/d/bc;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 14
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 486
    new-instance v8, Lcom/a/b/d/aer;

    iget-object v9, p0, Lcom/a/b/d/aer;->b:Lcom/a/b/d/afa;

    iget-object v10, p0, Lcom/a/b/d/aer;->c:Lcom/a/b/d/hs;

    invoke-virtual {p0}, Lcom/a/b/d/aer;->comparator()Ljava/util/Comparator;

    move-result-object v1

    .line 29078
    new-instance v0, Lcom/a/b/d/hs;

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v4, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    const/4 v5, 0x1

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/a/b/d/hs;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V

    .line 486
    invoke-virtual {v10, v0}, Lcom/a/b/d/hs;->a(Lcom/a/b/d/hs;)Lcom/a/b/d/hs;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aer;->d:Lcom/a/b/d/aez;

    invoke-direct {v8, v9, v0, v1}, Lcom/a/b/d/aer;-><init>(Lcom/a/b/d/afa;Lcom/a/b/d/hs;Lcom/a/b/d/aez;)V

    return-object v8
.end method

.method public final bridge synthetic e_()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->e_()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/a/b/d/bc;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic h()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->h()Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic i()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->i()Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic isEmpty()Z
    .registers 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic j()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->j()Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic k()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->k()Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method final l()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 444
    new-instance v0, Lcom/a/b/d/aeu;

    invoke-direct {v0, p0}, Lcom/a/b/d/aeu;-><init>(Lcom/a/b/d/aer;)V

    return-object v0
.end method

.method public final bridge synthetic m()Lcom/a/b/d/abn;
    .registers 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->m()Lcom/a/b/d/abn;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/a/b/d/bc;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/a/b/d/bc;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/a/b/d/bc;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 219
    sget-object v0, Lcom/a/b/d/aew;->a:Lcom/a/b/d/aew;

    invoke-direct {p0, v0}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aew;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/a/b/l/q;->b(J)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/a/b/d/bc;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
