.class final Lcom/a/b/d/aep;
.super Lcom/a/b/d/acm;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# instance fields
.field final d:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final e:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field transient f:Ljava/util/SortedMap;

.field final synthetic g:Lcom/a/b/d/ael;


# direct methods
.method constructor <init>(Lcom/a/b/d/ael;Ljava/lang/Object;)V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 185
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/a/b/d/aep;-><init>(Lcom/a/b/d/ael;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 186
    return-void
.end method

.method private constructor <init>(Lcom/a/b/d/ael;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 6
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 188
    iput-object p1, p0, Lcom/a/b/d/aep;->g:Lcom/a/b/d/ael;

    .line 189
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/acm;-><init>(Lcom/a/b/d/abx;Ljava/lang/Object;)V

    .line 190
    iput-object p3, p0, Lcom/a/b/d/aep;->d:Ljava/lang/Object;

    .line 191
    iput-object p4, p0, Lcom/a/b/d/aep;->e:Ljava/lang/Object;

    .line 192
    if-eqz p3, :cond_13

    if-eqz p4, :cond_13

    invoke-direct {p0, p3, p4}, Lcom/a/b/d/aep;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_18

    :cond_13
    const/4 v0, 0x1

    :goto_14
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 194
    return-void

    .line 192
    :cond_18
    const/4 v0, 0x0

    goto :goto_14
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/a/b/d/aep;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 208
    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 212
    if-eqz p1, :cond_1c

    iget-object v0, p0, Lcom/a/b/d/aep;->d:Ljava/lang/Object;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/a/b/d/aep;->d:Ljava/lang/Object;

    invoke-direct {p0, v0, p1}, Lcom/a/b/d/aep;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_1c

    :cond_e
    iget-object v0, p0, Lcom/a/b/d/aep;->e:Ljava/lang/Object;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/a/b/d/aep;->e:Ljava/lang/Object;

    invoke-direct {p0, v0, p1}, Lcom/a/b/d/aep;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1c

    :cond_1a
    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method private g()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 197
    new-instance v0, Lcom/a/b/d/up;

    invoke-direct {v0, p0}, Lcom/a/b/d/up;-><init>(Ljava/util/SortedMap;)V

    return-object v0
.end method

.method private h()Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 255
    iget-object v0, p0, Lcom/a/b/d/aep;->f:Ljava/util/SortedMap;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/a/b/d/aep;->f:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/a/b/d/aep;->g:Lcom/a/b/d/ael;

    iget-object v0, v0, Lcom/a/b/d/ael;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/a/b/d/aep;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 257
    :cond_18
    iget-object v0, p0, Lcom/a/b/d/aep;->g:Lcom/a/b/d/ael;

    iget-object v0, v0, Lcom/a/b/d/ael;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/a/b/d/aep;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    iput-object v0, p0, Lcom/a/b/d/aep;->f:Ljava/util/SortedMap;

    .line 259
    :cond_26
    iget-object v0, p0, Lcom/a/b/d/aep;->f:Ljava/util/SortedMap;

    return-object v0
.end method

.method private i()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 264
    invoke-super {p0}, Lcom/a/b/d/acm;->c()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method private j()Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/a/b/d/aep;->h()Ljava/util/SortedMap;

    move-result-object v0

    .line 270
    if-eqz v0, :cond_1b

    .line 271
    iget-object v1, p0, Lcom/a/b/d/aep;->d:Ljava/lang/Object;

    if-eqz v1, :cond_10

    .line 272
    iget-object v1, p0, Lcom/a/b/d/aep;->d:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    .line 274
    :cond_10
    iget-object v1, p0, Lcom/a/b/d/aep;->e:Ljava/lang/Object;

    if-eqz v1, :cond_1a

    .line 275
    iget-object v1, p0, Lcom/a/b/d/aep;->e:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    .line 279
    :cond_1a
    :goto_1a
    return-object v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method


# virtual methods
.method final bridge synthetic c()Ljava/util/Map;
    .registers 2

    .prologue
    .line 180
    .line 5264
    invoke-super {p0}, Lcom/a/b/d/acm;->c()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    .line 180
    return-object v0
.end method

.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/a/b/d/aep;->g:Lcom/a/b/d/ael;

    .line 1160
    iget-object v0, v0, Lcom/a/b/d/ael;->c:Ljava/util/Comparator;

    .line 201
    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 292
    invoke-direct {p0, p1}, Lcom/a/b/d/aep;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-super {p0, p1}, Lcom/a/b/d/acm;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method final synthetic d()Ljava/util/Map;
    .registers 3

    .prologue
    .line 180
    .line 4269
    invoke-direct {p0}, Lcom/a/b/d/aep;->h()Ljava/util/SortedMap;

    move-result-object v0

    .line 4270
    if-eqz v0, :cond_1b

    .line 4271
    iget-object v1, p0, Lcom/a/b/d/aep;->d:Ljava/lang/Object;

    if-eqz v1, :cond_10

    .line 4272
    iget-object v1, p0, Lcom/a/b/d/aep;->d:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    .line 4274
    :cond_10
    iget-object v1, p0, Lcom/a/b/d/aep;->e:Ljava/lang/Object;

    if-eqz v1, :cond_1a

    .line 4275
    iget-object v1, p0, Lcom/a/b/d/aep;->e:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    .line 4277
    :cond_1a
    :goto_1a
    return-object v0

    .line 4279
    :cond_1b
    const/4 v0, 0x0

    .line 180
    goto :goto_1a
.end method

.method final f()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 284
    invoke-direct {p0}, Lcom/a/b/d/aep;->h()Ljava/util/SortedMap;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/a/b/d/aep;->f:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 285
    iget-object v0, p0, Lcom/a/b/d/aep;->g:Lcom/a/b/d/ael;

    iget-object v0, v0, Lcom/a/b/d/ael;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/a/b/d/aep;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    iput-object v2, p0, Lcom/a/b/d/aep;->f:Ljava/util/SortedMap;

    .line 287
    iput-object v2, p0, Lcom/a/b/d/aep;->b:Ljava/util/Map;

    .line 289
    :cond_1c
    return-void
.end method

.method public final firstKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 233
    .line 1264
    invoke-super {p0}, Lcom/a/b/d/acm;->c()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    .line 234
    if-nez v0, :cond_e

    .line 235
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 2264
    :cond_e
    invoke-super {p0}, Lcom/a/b/d/acm;->c()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    .line 237
    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 6

    .prologue
    .line 223
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/aep;->a(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 224
    new-instance v0, Lcom/a/b/d/aep;

    iget-object v1, p0, Lcom/a/b/d/aep;->g:Lcom/a/b/d/ael;

    iget-object v2, p0, Lcom/a/b/d/aep;->a:Ljava/lang/Object;

    iget-object v3, p0, Lcom/a/b/d/aep;->d:Ljava/lang/Object;

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/a/b/d/aep;-><init>(Lcom/a/b/d/ael;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 180
    .line 6197
    new-instance v0, Lcom/a/b/d/up;

    invoke-direct {v0, p0}, Lcom/a/b/d/up;-><init>(Ljava/util/SortedMap;)V

    .line 180
    return-object v0
.end method

.method public final lastKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 241
    .line 3264
    invoke-super {p0}, Lcom/a/b/d/acm;->c()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    .line 242
    if-nez v0, :cond_e

    .line 243
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 4264
    :cond_e
    invoke-super {p0}, Lcom/a/b/d/acm;->c()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    .line 245
    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 296
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/aep;->a(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 297
    invoke-super {p0, p1, p2}, Lcom/a/b/d/acm;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 6

    .prologue
    .line 217
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/aep;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/aep;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    const/4 v0, 0x1

    :goto_15
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 219
    new-instance v0, Lcom/a/b/d/aep;

    iget-object v1, p0, Lcom/a/b/d/aep;->g:Lcom/a/b/d/ael;

    iget-object v2, p0, Lcom/a/b/d/aep;->a:Ljava/lang/Object;

    invoke-direct {v0, v1, v2, p1, p2}, Lcom/a/b/d/aep;-><init>(Lcom/a/b/d/ael;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 217
    :cond_22
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public final tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 6

    .prologue
    .line 228
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/aep;->a(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 229
    new-instance v0, Lcom/a/b/d/aep;

    iget-object v1, p0, Lcom/a/b/d/aep;->g:Lcom/a/b/d/ael;

    iget-object v2, p0, Lcom/a/b/d/aep;->a:Ljava/lang/Object;

    iget-object v3, p0, Lcom/a/b/d/aep;->e:Ljava/lang/Object;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/a/b/d/aep;-><init>(Lcom/a/b/d/ael;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
