.class final Lcom/a/b/d/fj;
.super Lcom/a/b/d/uj;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/fi;


# direct methods
.method constructor <init>(Lcom/a/b/d/fi;)V
    .registers 2

    .prologue
    .line 168
    iput-object p1, p0, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    invoke-direct {p0}, Lcom/a/b/d/uj;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 181
    iget-object v0, p0, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    iget-object v0, v0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 182
    if-nez v0, :cond_13

    move-object v0, v1

    .line 188
    :cond_12
    :goto_12
    return-object v0

    .line 187
    :cond_13
    new-instance v2, Lcom/a/b/d/fr;

    iget-object v3, p0, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    invoke-direct {v2, v3, p1}, Lcom/a/b/d/fr;-><init>(Lcom/a/b/d/fi;Ljava/lang/Object;)V

    invoke-static {v0, v2}, Lcom/a/b/d/fi;->a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;

    move-result-object v0

    .line 188
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    move-object v0, v1

    goto :goto_12
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 7
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 193
    iget-object v0, p0, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    iget-object v0, v0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 194
    if-nez v0, :cond_13

    move-object v0, v1

    .line 213
    :goto_12
    return-object v0

    .line 1088
    :cond_13
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 200
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 201
    :cond_1c
    :goto_1c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_35

    .line 202
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 203
    iget-object v4, p0, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    invoke-static {v4, p1, v3}, Lcom/a/b/d/fi;->a(Lcom/a/b/d/fi;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 204
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 205
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1c

    .line 208
    :cond_35
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3d

    move-object v0, v1

    .line 209
    goto :goto_12

    .line 210
    :cond_3d
    iget-object v0, p0, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    iget-object v0, v0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    instance-of v0, v0, Lcom/a/b/d/aac;

    if-eqz v0, :cond_53

    .line 1325
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-static {v2}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 211
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_12

    .line 213
    :cond_53
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_12
.end method

.method final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 239
    new-instance v0, Lcom/a/b/d/fl;

    invoke-direct {v0, p0}, Lcom/a/b/d/fl;-><init>(Lcom/a/b/d/fj;)V

    return-object v0
.end method

.method final c_()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 286
    new-instance v0, Lcom/a/b/d/fn;

    invoke-direct {v0, p0, p0}, Lcom/a/b/d/fn;-><init>(Lcom/a/b/d/fj;Ljava/util/Map;)V

    return-object v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    invoke-virtual {v0}, Lcom/a/b/d/fi;->g()V

    .line 177
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lcom/a/b/d/fj;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method final e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 219
    new-instance v0, Lcom/a/b/d/fk;

    invoke-direct {v0, p0, p0}, Lcom/a/b/d/fk;-><init>(Lcom/a/b/d/fj;Ljava/util/Map;)V

    return-object v0
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 168
    invoke-direct {p0, p1}, Lcom/a/b/d/fj;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 168
    invoke-virtual {p0, p1}, Lcom/a/b/d/fj;->a(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
