.class public abstract Lcom/a/b/d/gx;
.super Lcom/a/b/d/hg;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/vi;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/a/b/d/hg;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/a/b/d/vi;)Z
    .registers 3

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->a(Lcom/a/b/d/vi;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/vi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/vi;->b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/vi;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected abstract c()Lcom/a/b/d/vi;
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 4

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 137
    if-eq p1, p0, :cond_c

    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final f()I
    .registers 2

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->f()I

    move-result v0

    return v0
.end method

.method public final f(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public g()V
    .registers 2

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->g()V

    .line 54
    return-void
.end method

.method public final g(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->i()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .registers 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->n()Z

    move-result v0

    return v0
.end method

.method public p()Ljava/util/Set;
    .registers 2

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public q()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/a/b/d/gx;->c()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->q()Lcom/a/b/d/xc;

    move-result-object v0

    return-object v0
.end method
