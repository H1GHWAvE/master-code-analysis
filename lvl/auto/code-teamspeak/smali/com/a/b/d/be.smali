.class abstract Lcom/a/b/d/be;
.super Lcom/a/b/d/ba;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/abs;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:J = 0x5faae81de71c4a4L


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .registers 2

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/a/b/d/ba;-><init>(Ljava/util/Map;)V

    .line 48
    return-void
.end method

.method private v()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/a/b/d/be;->d_()Ljava/util/Comparator;

    move-result-object v0

    .line 56
    if-nez v0, :cond_f

    .line 57
    invoke-virtual {p0}, Lcom/a/b/d/be;->y()Ljava/util/SortedSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    .line 59
    :goto_e
    return-object v0

    :cond_f
    invoke-virtual {p0}, Lcom/a/b/d/be;->d_()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;)Lcom/a/b/d/me;

    move-result-object v0

    goto :goto_e
.end method


# virtual methods
.method synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/a/b/d/be;->y()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/a/b/d/be;->h(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 4

    .prologue
    .line 37
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/be;->d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 37
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/be;->d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 123
    invoke-super {p0}, Lcom/a/b/d/ba;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/a/b/d/be;->i(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method synthetic c()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/a/b/d/be;->y()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/a/b/d/be;->h(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method final synthetic d()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/a/b/d/be;->v()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/a/b/d/be;->i(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 105
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ba;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method public h(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method public i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 133
    invoke-super {p0}, Lcom/a/b/d/ba;->i()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public i(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 90
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method final synthetic t()Ljava/util/Set;
    .registers 2

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/a/b/d/be;->v()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method abstract y()Ljava/util/SortedSet;
.end method
