.class final Lcom/a/b/d/afk;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/Iterator;

.field final synthetic b:Lcom/a/b/d/afj;


# direct methods
.method constructor <init>(Lcom/a/b/d/afj;Ljava/util/Iterator;)V
    .registers 3

    .prologue
    .line 536
    iput-object p1, p0, Lcom/a/b/d/afk;->b:Lcom/a/b/d/afj;

    iput-object p2, p0, Lcom/a/b/d/afk;->a:Ljava/util/Iterator;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    return-void
.end method

.method private c()Ljava/util/Map$Entry;
    .registers 4

    .prologue
    .line 540
    :cond_0
    iget-object v0, p0, Lcom/a/b/d/afk;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 541
    iget-object v0, p0, Lcom/a/b/d/afk;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    .line 1097
    iget-object v1, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    .line 542
    iget-object v2, p0, Lcom/a/b/d/afk;->b:Lcom/a/b/d/afj;

    iget-object v2, v2, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    iget-object v2, v2, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 1316
    iget-object v2, v2, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 542
    iget-object v2, v2, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v1, v2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v1

    if-gez v1, :cond_4f

    .line 2101
    iget-object v1, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 544
    iget-object v2, p0, Lcom/a/b/d/afk;->b:Lcom/a/b/d/afj;

    iget-object v2, v2, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    iget-object v2, v2, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 2316
    iget-object v2, v2, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 544
    iget-object v2, v2, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v1, v2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v1

    if-lez v1, :cond_0

    .line 3084
    iget-object v1, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    .line 546
    iget-object v2, p0, Lcom/a/b/d/afk;->b:Lcom/a/b/d/afj;

    iget-object v2, v2, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    iget-object v2, v2, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 3316
    iget-object v2, v2, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 546
    invoke-virtual {v1, v2}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v1

    invoke-virtual {v0}, Lcom/a/b/d/aff;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 550
    :goto_4e
    return-object v0

    :cond_4f
    invoke-virtual {p0}, Lcom/a/b/d/afk;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    goto :goto_4e
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 536
    .line 3540
    :cond_0
    iget-object v0, p0, Lcom/a/b/d/afk;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 3541
    iget-object v0, p0, Lcom/a/b/d/afk;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    .line 4097
    iget-object v1, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    .line 3542
    iget-object v2, p0, Lcom/a/b/d/afk;->b:Lcom/a/b/d/afj;

    iget-object v2, v2, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    iget-object v2, v2, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 4316
    iget-object v2, v2, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 3542
    iget-object v2, v2, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v1, v2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v1

    if-gez v1, :cond_4f

    .line 5101
    iget-object v1, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 3544
    iget-object v2, p0, Lcom/a/b/d/afk;->b:Lcom/a/b/d/afj;

    iget-object v2, v2, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    iget-object v2, v2, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 5316
    iget-object v2, v2, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 3544
    iget-object v2, v2, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v1, v2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v1

    if-lez v1, :cond_0

    .line 6084
    iget-object v1, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    .line 3546
    iget-object v2, p0, Lcom/a/b/d/afk;->b:Lcom/a/b/d/afj;

    iget-object v2, v2, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    iget-object v2, v2, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 6316
    iget-object v2, v2, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 3546
    invoke-virtual {v1, v2}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v1

    invoke-virtual {v0}, Lcom/a/b/d/aff;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    :goto_4e
    return-object v0

    .line 3550
    :cond_4f
    invoke-virtual {p0}, Lcom/a/b/d/afk;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    .line 536
    goto :goto_4e
.end method
