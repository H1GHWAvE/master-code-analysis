.class final Lcom/a/b/d/kx;
.super Lcom/a/b/d/lo;
.source "SourceFile"


# static fields
.field private static final c:J


# instance fields
.field final synthetic a:Lcom/a/b/d/ku;


# direct methods
.method private constructor <init>(Lcom/a/b/d/ku;)V
    .registers 2

    .prologue
    .line 356
    iput-object p1, p0, Lcom/a/b/d/kx;->a:Lcom/a/b/d/ku;

    invoke-direct {p0}, Lcom/a/b/d/lo;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/ku;B)V
    .registers 3

    .prologue
    .line 356
    invoke-direct {p0, p1}, Lcom/a/b/d/kx;-><init>(Lcom/a/b/d/ku;)V

    return-void
.end method


# virtual methods
.method public final c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 364
    invoke-virtual {p0}, Lcom/a/b/d/kx;->f()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 389
    instance-of v1, p1, Lcom/a/b/d/xd;

    if-eqz v1, :cond_d

    .line 390
    check-cast p1, Lcom/a/b/d/xd;

    .line 391
    invoke-interface {p1}, Lcom/a/b/d/xd;->b()I

    move-result v1

    if-gtz v1, :cond_e

    .line 397
    :cond_d
    :goto_d
    return v0

    .line 394
    :cond_e
    iget-object v1, p0, Lcom/a/b/d/kx;->a:Lcom/a/b/d/ku;

    invoke-interface {p1}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/a/b/d/ku;->a(Ljava/lang/Object;)I

    move-result v1

    .line 395
    invoke-interface {p1}, Lcom/a/b/d/xd;->b()I

    move-result v2

    if-ne v1, v2, :cond_d

    const/4 v0, 0x1

    goto :goto_d
.end method

.method final g()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 409
    new-instance v0, Lcom/a/b/d/kz;

    iget-object v1, p0, Lcom/a/b/d/kx;->a:Lcom/a/b/d/ku;

    invoke-direct {v0, v1}, Lcom/a/b/d/kz;-><init>(Lcom/a/b/d/ku;)V

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/a/b/d/kx;->a:Lcom/a/b/d/ku;

    invoke-virtual {v0}, Lcom/a/b/d/ku;->h_()Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 402
    iget-object v0, p0, Lcom/a/b/d/kx;->a:Lcom/a/b/d/ku;

    invoke-virtual {v0}, Lcom/a/b/d/ku;->hashCode()I

    move-result v0

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 356
    .line 1364
    invoke-virtual {p0}, Lcom/a/b/d/kx;->f()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    .line 356
    return-object v0
.end method

.method final l()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 369
    new-instance v0, Lcom/a/b/d/ky;

    invoke-direct {v0, p0}, Lcom/a/b/d/ky;-><init>(Lcom/a/b/d/kx;)V

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 384
    iget-object v0, p0, Lcom/a/b/d/kx;->a:Lcom/a/b/d/ku;

    invoke-virtual {v0}, Lcom/a/b/d/ku;->n_()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
