.class final Lcom/a/b/d/pm;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# static fields
.field private static final d:J


# instance fields
.field final a:Ljava/lang/Object;

.field final b:Ljava/lang/Object;

.field final c:[Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 366
    iput-object p1, p0, Lcom/a/b/d/pm;->a:Ljava/lang/Object;

    .line 367
    iput-object p2, p0, Lcom/a/b/d/pm;->b:Ljava/lang/Object;

    .line 368
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/a/b/d/pm;->c:[Ljava/lang/Object;

    .line 369
    return-void
.end method


# virtual methods
.method public final get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 374
    packed-switch p1, :pswitch_data_18

    .line 381
    invoke-virtual {p0}, Lcom/a/b/d/pm;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 382
    iget-object v0, p0, Lcom/a/b/d/pm;->c:[Ljava/lang/Object;

    add-int/lit8 v1, p1, -0x2

    aget-object v0, v0, v1

    :goto_10
    return-object v0

    .line 376
    :pswitch_11
    iget-object v0, p0, Lcom/a/b/d/pm;->a:Ljava/lang/Object;

    goto :goto_10

    .line 378
    :pswitch_14
    iget-object v0, p0, Lcom/a/b/d/pm;->b:Ljava/lang/Object;

    goto :goto_10

    .line 374
    nop

    :pswitch_data_18
    .packed-switch 0x0
        :pswitch_11
        :pswitch_14
    .end packed-switch
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/a/b/d/pm;->c:[Ljava/lang/Object;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method
