.class Lcom/a/b/d/sa;
.super Ljava/util/concurrent/locks/ReentrantLock;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/qy;

.field volatile b:I

.field c:I

.field d:I

.field volatile e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field final f:I

.field final g:Ljava/lang/ref/ReferenceQueue;

.field final h:Ljava/lang/ref/ReferenceQueue;

.field final i:Ljava/util/Queue;

.field final j:Ljava/util/concurrent/atomic/AtomicInteger;

.field final k:Ljava/util/Queue;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation
.end field

.field final l:Ljava/util/Queue;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/a/b/d/qy;II)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 2090
    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 2074
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/a/b/d/sa;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2091
    iput-object p1, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    .line 2092
    iput p3, p0, Lcom/a/b/d/sa;->f:I

    .line 2093
    invoke-static {p2}, Lcom/a/b/d/sa;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v0

    .line 3119
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/a/b/d/sa;->d:I

    .line 3120
    iget v2, p0, Lcom/a/b/d/sa;->d:I

    iget v3, p0, Lcom/a/b/d/sa;->f:I

    if-ne v2, v3, :cond_29

    .line 3122
    iget v2, p0, Lcom/a/b/d/sa;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/a/b/d/sa;->d:I

    .line 3124
    :cond_29
    iput-object v0, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2095
    invoke-virtual {p1}, Lcom/a/b/d/qy;->e()Z

    move-result v0

    if-eqz v0, :cond_73

    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :goto_36
    iput-object v0, p0, Lcom/a/b/d/sa;->g:Ljava/lang/ref/ReferenceQueue;

    .line 2098
    invoke-virtual {p1}, Lcom/a/b/d/qy;->f()Z

    move-result v0

    if-eqz v0, :cond_43

    new-instance v1, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :cond_43
    iput-object v1, p0, Lcom/a/b/d/sa;->h:Ljava/lang/ref/ReferenceQueue;

    .line 2101
    invoke-virtual {p1}, Lcom/a/b/d/qy;->b()Z

    move-result v0

    if-nez v0, :cond_51

    invoke-virtual {p1}, Lcom/a/b/d/qy;->d()Z

    move-result v0

    if-eqz v0, :cond_75

    :cond_51
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    :goto_56
    iput-object v0, p0, Lcom/a/b/d/sa;->i:Ljava/util/Queue;

    .line 2105
    invoke-virtual {p1}, Lcom/a/b/d/qy;->b()Z

    move-result v0

    if-eqz v0, :cond_7a

    new-instance v0, Lcom/a/b/d/rp;

    invoke-direct {v0}, Lcom/a/b/d/rp;-><init>()V

    :goto_63
    iput-object v0, p0, Lcom/a/b/d/sa;->k:Ljava/util/Queue;

    .line 2109
    invoke-virtual {p1}, Lcom/a/b/d/qy;->c()Z

    move-result v0

    if-eqz v0, :cond_7f

    new-instance v0, Lcom/a/b/d/rs;

    invoke-direct {v0}, Lcom/a/b/d/rs;-><init>()V

    :goto_70
    iput-object v0, p0, Lcom/a/b/d/sa;->l:Ljava/util/Queue;

    .line 2112
    return-void

    :cond_73
    move-object v0, v1

    .line 2095
    goto :goto_36

    .line 2101
    :cond_75
    invoke-static {}, Lcom/a/b/d/qy;->i()Ljava/util/Queue;

    move-result-object v0

    goto :goto_56

    .line 2105
    :cond_7a
    invoke-static {}, Lcom/a/b/d/qy;->i()Ljava/util/Queue;

    move-result-object v0

    goto :goto_63

    .line 2109
    :cond_7f
    invoke-static {}, Lcom/a/b/d/qy;->i()Ljava/util/Queue;

    move-result-object v0

    goto :goto_70
.end method

.method private static a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .registers 2

    .prologue
    .line 2115
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v0, p0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    return-object v0
.end method

.method private a(Lcom/a/b/d/rz;J)V
    .registers 6

    .prologue
    .line 2321
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v0, v0, Lcom/a/b/d/qy;->w:Lcom/a/b/b/ej;

    invoke-virtual {v0}, Lcom/a/b/b/ej;->a()J

    move-result-wide v0

    add-long/2addr v0, p2

    invoke-interface {p1, v0, v1}, Lcom/a/b/d/rz;->a(J)V

    .line 2322
    return-void
.end method

.method private a(Lcom/a/b/d/rz;Ljava/lang/Object;)V
    .registers 5
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 2160
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v0, v0, Lcom/a/b/d/qy;->p:Lcom/a/b/d/sh;

    invoke-virtual {v0, p0, p1, p2}, Lcom/a/b/d/sh;->a(Lcom/a/b/d/sa;Lcom/a/b/d/rz;Ljava/lang/Object;)Lcom/a/b/d/sr;

    move-result-object v0

    .line 2161
    invoke-interface {p1, v0}, Lcom/a/b/d/rz;->a(Lcom/a/b/d/sr;)V

    .line 3281
    invoke-direct {p0}, Lcom/a/b/d/sa;->l()V

    .line 3282
    iget-object v0, p0, Lcom/a/b/d/sa;->k:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 3283
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->c()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 3286
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->d()Z

    move-result v0

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-wide v0, v0, Lcom/a/b/d/qy;->r:J

    .line 3289
    :goto_27
    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;J)V

    .line 3290
    iget-object v0, p0, Lcom/a/b/d/sa;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2163
    :cond_2f
    return-void

    .line 3286
    :cond_30
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-wide v0, v0, Lcom/a/b/d/qy;->s:J

    goto :goto_27
.end method

.method private a(Ljava/util/concurrent/atomic/AtomicReferenceArray;)V
    .registers 4

    .prologue
    .line 2119
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/a/b/d/sa;->d:I

    .line 2120
    iget v0, p0, Lcom/a/b/d/sa;->d:I

    iget v1, p0, Lcom/a/b/d/sa;->f:I

    if-ne v0, v1, :cond_16

    .line 2122
    iget v0, p0, Lcom/a/b/d/sa;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/sa;->d:I

    .line 2124
    :cond_16
    iput-object p1, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2125
    return-void
.end method

.method private a(Lcom/a/b/d/rz;ILcom/a/b/d/qq;)Z
    .registers 10
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 2990
    iget v0, p0, Lcom/a/b/d/sa;->b:I

    .line 2991
    iget-object v2, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2992
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v3, p2, v0

    .line 2993
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    move-object v1, v0

    .line 2995
    :goto_13
    if-eqz v1, :cond_40

    .line 2996
    if-ne v1, p1, :cond_3b

    .line 2997
    iget v4, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/a/b/d/sa;->c:I

    .line 2998
    invoke-interface {v1}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v5

    invoke-interface {v5}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p0, v4, v5, p3}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2999
    invoke-direct {p0, v0, v1}, Lcom/a/b/d/sa;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 3000
    iget v1, p0, Lcom/a/b/d/sa;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 3001
    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3002
    iput v1, p0, Lcom/a/b/d/sa;->b:I

    .line 3003
    const/4 v0, 0x1

    .line 3007
    :goto_3a
    return v0

    .line 2995
    :cond_3b
    invoke-interface {v1}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v1

    goto :goto_13

    .line 3007
    :cond_40
    const/4 v0, 0x0

    goto :goto_3a
.end method

.method private static a(Lcom/a/b/d/sr;)Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 3015
    invoke-interface {p0}, Lcom/a/b/d/sr;->b()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 3018
    :cond_7
    :goto_7
    return v0

    :cond_8
    invoke-interface {p0}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_7

    const/4 v0, 0x1

    goto :goto_7
.end method

.method private a(Ljava/lang/Object;)Z
    .registers 9
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2475
    :try_start_1
    iget v0, p0, Lcom/a/b/d/sa;->b:I

    if-eqz v0, :cond_34

    .line 2476
    iget-object v3, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2477
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v4

    move v2, v1

    .line 2478
    :goto_c
    if-ge v2, v4, :cond_34

    .line 2479
    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    :goto_14
    if-eqz v0, :cond_30

    .line 2480
    invoke-virtual {p0, v0}, Lcom/a/b/d/sa;->c(Lcom/a/b/d/rz;)Ljava/lang/Object;

    move-result-object v5

    .line 2481
    if-eqz v5, :cond_2b

    .line 2484
    iget-object v6, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v6, v6, Lcom/a/b/d/qy;->n:Lcom/a/b/b/au;

    invoke-virtual {v6, p1, v5}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_23
    .catchall {:try_start_1 .. :try_end_23} :catchall_39

    move-result v5

    if-eqz v5, :cond_2b

    .line 2493
    invoke-virtual {p0}, Lcom/a/b/d/sa;->a()V

    const/4 v0, 0x1

    :goto_2a
    return v0

    .line 2479
    :cond_2b
    :try_start_2b
    invoke-interface {v0}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;
    :try_end_2e
    .catchall {:try_start_2b .. :try_end_2e} :catchall_39

    move-result-object v0

    goto :goto_14

    .line 2478
    :cond_30
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_c

    .line 2493
    :cond_34
    invoke-virtual {p0}, Lcom/a/b/d/sa;->a()V

    move v0, v1

    goto :goto_2a

    :catchall_39
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->a()V

    throw v0
.end method

.method private b(I)Lcom/a/b/d/rz;
    .registers 4

    .prologue
    .line 2394
    iget-object v0, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2395
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    return-object v0
.end method

.method private b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
    .registers 7
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 2864
    iget-object v0, p0, Lcom/a/b/d/sa;->k:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2865
    iget-object v0, p0, Lcom/a/b/d/sa;->l:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2867
    iget v2, p0, Lcom/a/b/d/sa;->b:I

    .line 2868
    invoke-interface {p2}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v1

    .line 2869
    :goto_10
    if-eq p1, p2, :cond_29

    .line 2870
    invoke-virtual {p0, p1, v1}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 2871
    if-eqz v0, :cond_20

    move v1, v2

    .line 2869
    :goto_19
    invoke-interface {p1}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object p1

    move v2, v1

    move-object v1, v0

    goto :goto_10

    .line 2874
    :cond_20
    invoke-direct {p0, p1}, Lcom/a/b/d/sa;->e(Lcom/a/b/d/rz;)V

    .line 2875
    add-int/lit8 v0, v2, -0x1

    move-object v3, v1

    move v1, v0

    move-object v0, v3

    goto :goto_19

    .line 2878
    :cond_29
    iput v2, p0, Lcom/a/b/d/sa;->b:I

    .line 2879
    return-object v1
.end method

.method private d(Lcom/a/b/d/rz;)V
    .registers 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 2281
    invoke-direct {p0}, Lcom/a/b/d/sa;->l()V

    .line 2282
    iget-object v0, p0, Lcom/a/b/d/sa;->k:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2283
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->c()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 2286
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->d()Z

    move-result v0

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-wide v0, v0, Lcom/a/b/d/qy;->r:J

    .line 2289
    :goto_1c
    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;J)V

    .line 2290
    iget-object v0, p0, Lcom/a/b/d/sa;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2292
    :cond_24
    return-void

    .line 2286
    :cond_25
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-wide v0, v0, Lcom/a/b/d/qy;->s:J

    goto :goto_1c
.end method

.method private e(Ljava/lang/Object;I)Lcom/a/b/d/rz;
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 2423
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;I)Lcom/a/b/d/rz;

    move-result-object v1

    .line 2424
    if-nez v1, :cond_8

    .line 2430
    :goto_7
    return-object v0

    .line 2426
    :cond_8
    iget-object v2, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v2}, Lcom/a/b/d/qy;->c()Z

    move-result v2

    if-eqz v2, :cond_1c

    iget-object v2, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v2, v1}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 2427
    invoke-direct {p0}, Lcom/a/b/d/sa;->m()V

    goto :goto_7

    :cond_1c
    move-object v0, v1

    .line 2430
    goto :goto_7
.end method

.method private e()V
    .registers 2

    .prologue
    .line 2171
    invoke-virtual {p0}, Lcom/a/b/d/sa;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2173
    :try_start_6
    invoke-direct {p0}, Lcom/a/b/d/sa;->f()V
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_d

    .line 2175
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 2178
    :cond_c
    return-void

    .line 2175
    :catchall_d
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    throw v0
.end method

.method private e(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 2883
    sget-object v0, Lcom/a/b/d/qq;->c:Lcom/a/b/d/qq;

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;Lcom/a/b/d/qq;)V

    .line 2884
    iget-object v0, p0, Lcom/a/b/d/sa;->k:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2885
    iget-object v0, p0, Lcom/a/b/d/sa;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2886
    return-void
.end method

.method private f()V
    .registers 7
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    const/16 v5, 0x10

    const/4 v2, 0x0

    .line 2186
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->e()Z

    move-result v0

    if-eqz v0, :cond_27

    move v1, v2

    .line 4198
    :goto_c
    iget-object v0, p0, Lcom/a/b/d/sa;->g:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 4200
    check-cast v0, Lcom/a/b/d/rz;

    .line 4201
    iget-object v3, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    .line 4862
    invoke-interface {v0}, Lcom/a/b/d/rz;->c()I

    move-result v4

    .line 4863
    invoke-virtual {v3, v4}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v3

    invoke-virtual {v3, v0, v4}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;I)Z

    .line 4202
    add-int/lit8 v0, v1, 0x1

    if-ne v0, v5, :cond_53

    .line 2189
    :cond_27
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->f()Z

    move-result v0

    if-eqz v0, :cond_52

    .line 5212
    :cond_2f
    iget-object v0, p0, Lcom/a/b/d/sa;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_52

    .line 5214
    check-cast v0, Lcom/a/b/d/sr;

    .line 5215
    iget-object v1, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    .line 5856
    invoke-interface {v0}, Lcom/a/b/d/sr;->a()Lcom/a/b/d/rz;

    move-result-object v3

    .line 5857
    invoke-interface {v3}, Lcom/a/b/d/rz;->c()I

    move-result v4

    .line 5858
    invoke-virtual {v1, v4}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v1

    invoke-interface {v3}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3, v4, v0}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;ILcom/a/b/d/sr;)Z

    .line 5216
    add-int/lit8 v2, v2, 0x1

    if-ne v2, v5, :cond_2f

    .line 2192
    :cond_52
    return-void

    :cond_53
    move v1, v0

    goto :goto_c
.end method

.method private g()V
    .registers 5
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 2197
    const/4 v0, 0x0

    move v1, v0

    .line 2198
    :goto_2
    iget-object v0, p0, Lcom/a/b/d/sa;->g:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 2200
    check-cast v0, Lcom/a/b/d/rz;

    .line 2201
    iget-object v2, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    .line 5862
    invoke-interface {v0}, Lcom/a/b/d/rz;->c()I

    move-result v3

    .line 5863
    invoke-virtual {v2, v3}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v2

    invoke-virtual {v2, v0, v3}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;I)Z

    .line 2202
    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x10

    if-ne v0, v1, :cond_20

    .line 2206
    :cond_1f
    return-void

    :cond_20
    move v1, v0

    goto :goto_2
.end method

.method private h()V
    .registers 6
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 2211
    const/4 v0, 0x0

    move v1, v0

    .line 2212
    :goto_2
    iget-object v0, p0, Lcom/a/b/d/sa;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 2214
    check-cast v0, Lcom/a/b/d/sr;

    .line 2215
    iget-object v2, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    .line 6856
    invoke-interface {v0}, Lcom/a/b/d/sr;->a()Lcom/a/b/d/rz;

    move-result-object v3

    .line 6857
    invoke-interface {v3}, Lcom/a/b/d/rz;->c()I

    move-result v4

    .line 6858
    invoke-virtual {v2, v4}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v2

    invoke-interface {v3}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v4, v0}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;ILcom/a/b/d/sr;)Z

    .line 2216
    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x10

    if-ne v0, v1, :cond_28

    .line 2220
    :cond_27
    return-void

    :cond_28
    move v1, v0

    goto :goto_2
.end method

.method private i()V
    .registers 2

    .prologue
    .line 2226
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->e()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 7235
    :cond_8
    iget-object v0, p0, Lcom/a/b/d/sa;->g:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_8

    .line 2229
    :cond_10
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->f()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 7239
    :cond_18
    iget-object v0, p0, Lcom/a/b/d/sa;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_18

    .line 2232
    :cond_20
    return-void
.end method

.method private j()V
    .registers 2

    .prologue
    .line 2235
    :cond_0
    iget-object v0, p0, Lcom/a/b/d/sa;->g:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2236
    return-void
.end method

.method private k()V
    .registers 2

    .prologue
    .line 2239
    :cond_0
    iget-object v0, p0, Lcom/a/b/d/sa;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2240
    return-void
.end method

.method private l()V
    .registers 3
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 2303
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/a/b/d/sa;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    if-eqz v0, :cond_2d

    .line 2308
    iget-object v1, p0, Lcom/a/b/d/sa;->k:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 2309
    iget-object v1, p0, Lcom/a/b/d/sa;->k:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2311
    :cond_17
    iget-object v1, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v1}, Lcom/a/b/d/qy;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/a/b/d/sa;->l:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2312
    iget-object v1, p0, Lcom/a/b/d/sa;->l:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2315
    :cond_2d
    return-void
.end method

.method private m()V
    .registers 2

    .prologue
    .line 2328
    invoke-virtual {p0}, Lcom/a/b/d/sa;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2330
    :try_start_6
    invoke-direct {p0}, Lcom/a/b/d/sa;->n()V
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_d

    .line 2332
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 2336
    :cond_c
    return-void

    .line 2332
    :catchall_d
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    throw v0
.end method

.method private n()V
    .registers 6
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 2340
    invoke-direct {p0}, Lcom/a/b/d/sa;->l()V

    .line 2342
    iget-object v0, p0, Lcom/a/b/d/sa;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2354
    :cond_b
    return-void

    .line 2347
    :cond_c
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v0, v0, Lcom/a/b/d/qy;->w:Lcom/a/b/b/ej;

    invoke-virtual {v0}, Lcom/a/b/b/ej;->a()J

    move-result-wide v2

    .line 2349
    :cond_14
    iget-object v0, p0, Lcom/a/b/d/sa;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    if-eqz v0, :cond_b

    invoke-static {v0, v2, v3}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;J)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2350
    invoke-interface {v0}, Lcom/a/b/d/rz;->c()I

    move-result v1

    sget-object v4, Lcom/a/b/d/qq;->d:Lcom/a/b/d/qq;

    invoke-direct {p0, v0, v1, v4}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;ILcom/a/b/d/qq;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 2351
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private o()Z
    .registers 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 2377
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->b()Z

    move-result v0

    if-eqz v0, :cond_2d

    iget v0, p0, Lcom/a/b/d/sa;->b:I

    iget v1, p0, Lcom/a/b/d/sa;->f:I

    if-lt v0, v1, :cond_2d

    .line 2378
    invoke-direct {p0}, Lcom/a/b/d/sa;->l()V

    .line 2380
    iget-object v0, p0, Lcom/a/b/d/sa;->k:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    .line 2381
    invoke-interface {v0}, Lcom/a/b/d/rz;->c()I

    move-result v1

    sget-object v2, Lcom/a/b/d/qq;->e:Lcom/a/b/d/qq;

    invoke-direct {p0, v0, v1, v2}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;ILcom/a/b/d/qq;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 2382
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2384
    :cond_2b
    const/4 v0, 0x1

    .line 2386
    :goto_2c
    return v0

    :cond_2d
    const/4 v0, 0x0

    goto :goto_2c
.end method

.method private p()V
    .registers 12
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 2570
    iget-object v7, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2571
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v8

    .line 2572
    const/high16 v0, 0x40000000    # 2.0f

    if-lt v8, v0, :cond_b

    .line 2635
    :goto_a
    return-void

    .line 2586
    :cond_b
    iget v5, p0, Lcom/a/b/d/sa;->b:I

    .line 2587
    shl-int/lit8 v0, v8, 0x1

    invoke-static {v0}, Lcom/a/b/d/sa;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v9

    .line 2588
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/a/b/d/sa;->d:I

    .line 2589
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    .line 2590
    const/4 v0, 0x0

    move v6, v0

    :goto_25
    if-ge v6, v8, :cond_7f

    .line 2593
    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    .line 2595
    if-eqz v0, :cond_86

    .line 2596
    invoke-interface {v0}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v2

    .line 2597
    invoke-interface {v0}, Lcom/a/b/d/rz;->c()I

    move-result v1

    and-int v4, v1, v10

    .line 2600
    if-nez v2, :cond_44

    .line 2601
    invoke-virtual {v9, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v2, v5

    .line 2590
    :cond_3f
    :goto_3f
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v5, v2

    goto :goto_25

    :cond_44
    move-object v1, v0

    .line 2608
    :goto_45
    if-eqz v2, :cond_55

    .line 2609
    invoke-interface {v2}, Lcom/a/b/d/rz;->c()I

    move-result v3

    and-int/2addr v3, v10

    .line 2610
    if-eq v3, v4, :cond_84

    move-object v1, v2

    .line 2608
    :goto_4f
    invoke-interface {v2}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v2

    move v4, v3

    goto :goto_45

    .line 2616
    :cond_55
    invoke-virtual {v9, v4, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move-object v3, v0

    move v2, v5

    .line 2619
    :goto_5a
    if-eq v3, v1, :cond_3f

    .line 2620
    invoke-interface {v3}, Lcom/a/b/d/rz;->c()I

    move-result v0

    and-int v4, v0, v10

    .line 2621
    invoke-virtual {v9, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    .line 2622
    invoke-virtual {p0, v3, v0}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 2623
    if-eqz v0, :cond_79

    .line 2624
    invoke-virtual {v9, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v0, v2

    .line 2619
    :goto_72
    invoke-interface {v3}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v2

    move-object v3, v2

    move v2, v0

    goto :goto_5a

    .line 2626
    :cond_79
    invoke-direct {p0, v3}, Lcom/a/b/d/sa;->e(Lcom/a/b/d/rz;)V

    .line 2627
    add-int/lit8 v0, v2, -0x1

    goto :goto_72

    .line 2633
    :cond_7f
    iput-object v9, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2634
    iput v5, p0, Lcom/a/b/d/sa;->b:I

    goto :goto_a

    :cond_84
    move v3, v4

    goto :goto_4f

    :cond_86
    move v2, v5

    goto :goto_3f
.end method

.method private q()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 2819
    iget v0, p0, Lcom/a/b/d/sa;->b:I

    if-eqz v0, :cond_86

    .line 2820
    invoke-virtual {p0}, Lcom/a/b/d/sa;->lock()V

    .line 2822
    :try_start_8
    iget-object v3, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2823
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v0, v0, Lcom/a/b/d/qy;->t:Ljava/util/Queue;

    sget-object v2, Lcom/a/b/d/qy;->y:Ljava/util/Queue;

    if-eq v0, v2, :cond_39

    move v2, v1

    .line 2824
    :goto_13
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_39

    .line 2825
    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    :goto_1f
    if-eqz v0, :cond_35

    .line 2827
    invoke-interface {v0}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v4

    invoke-interface {v4}, Lcom/a/b/d/sr;->b()Z

    move-result v4

    if-nez v4, :cond_30

    .line 2828
    sget-object v4, Lcom/a/b/d/qq;->a:Lcom/a/b/d/qq;

    invoke-virtual {p0, v0, v4}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;Lcom/a/b/d/qq;)V

    .line 2825
    :cond_30
    invoke-interface {v0}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v0

    goto :goto_1f

    .line 2824
    :cond_35
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_13

    :cond_39
    move v0, v1

    .line 2833
    :goto_3a
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_47

    .line 2834
    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2833
    add-int/lit8 v0, v0, 0x1

    goto :goto_3a

    .line 30226
    :cond_47
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->e()Z

    move-result v0

    if-eqz v0, :cond_57

    .line 30235
    :cond_4f
    iget-object v0, p0, Lcom/a/b/d/sa;->g:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_4f

    .line 30229
    :cond_57
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->f()Z

    move-result v0

    if-eqz v0, :cond_67

    .line 30239
    :cond_5f
    iget-object v0, p0, Lcom/a/b/d/sa;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_5f

    .line 2837
    :cond_67
    iget-object v0, p0, Lcom/a/b/d/sa;->k:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 2838
    iget-object v0, p0, Lcom/a/b/d/sa;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 2839
    iget-object v0, p0, Lcom/a/b/d/sa;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 2841
    iget v0, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/sa;->c:I

    .line 2842
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/d/sa;->b:I
    :try_end_80
    .catchall {:try_start_8 .. :try_end_80} :catchall_87

    .line 2844
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 31069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2848
    :cond_86
    return-void

    .line 2844
    :catchall_87
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 32069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2845
    throw v0
.end method

.method private r()V
    .registers 1
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 3062
    invoke-virtual {p0}, Lcom/a/b/d/sa;->c()V

    .line 3063
    return-void
.end method

.method private s()V
    .registers 1

    .prologue
    .line 3069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 3070
    return-void
.end method


# virtual methods
.method final a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
    .registers 7
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2138
    invoke-interface {p1}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_8

    .line 2152
    :cond_7
    :goto_7
    return-object v0

    .line 2143
    :cond_8
    invoke-interface {p1}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v1

    .line 2144
    invoke-interface {v1}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v2

    .line 2145
    if-nez v2, :cond_18

    invoke-interface {v1}, Lcom/a/b/d/sr;->b()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2150
    :cond_18
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v0, v0, Lcom/a/b/d/qy;->v:Lcom/a/b/d/re;

    invoke-virtual {v0, p0, p1, p2}, Lcom/a/b/d/re;->a(Lcom/a/b/d/sa;Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 2151
    iget-object v3, p0, Lcom/a/b/d/sa;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-interface {v1, v3, v2, v0}, Lcom/a/b/d/sr;->a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/a/b/d/rz;)Lcom/a/b/d/sr;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/d/rz;->a(Lcom/a/b/d/sr;)V

    goto :goto_7
.end method

.method final a(Ljava/lang/Object;I)Lcom/a/b/d/rz;
    .registers 6

    .prologue
    .line 2401
    iget v0, p0, Lcom/a/b/d/sa;->b:I

    if-eqz v0, :cond_34

    .line 7394
    iget-object v0, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 7395
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p2

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    .line 2402
    :goto_13
    if-eqz v0, :cond_34

    .line 2403
    invoke-interface {v0}, Lcom/a/b/d/rz;->c()I

    move-result v1

    if-ne v1, p2, :cond_24

    .line 2407
    invoke-interface {v0}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v1

    .line 2408
    if-nez v1, :cond_29

    .line 2409
    invoke-direct {p0}, Lcom/a/b/d/sa;->e()V

    .line 2402
    :cond_24
    invoke-interface {v0}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v0

    goto :goto_13

    .line 2413
    :cond_29
    iget-object v2, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v2, v2, Lcom/a/b/d/qy;->m:Lcom/a/b/b/au;

    invoke-virtual {v2, p1, v1}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 2419
    :goto_33
    return-object v0

    :cond_34
    const/4 v0, 0x0

    goto :goto_33
.end method

.method final a(Ljava/lang/Object;ILcom/a/b/d/rz;)Lcom/a/b/d/rz;
    .registers 5
    .param p3    # Lcom/a/b/d/rz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 2129
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v0, v0, Lcom/a/b/d/qy;->v:Lcom/a/b/d/re;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/a/b/d/re;->a(Lcom/a/b/d/sa;Ljava/lang/Object;ILcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .registers 12

    .prologue
    const/4 v1, 0x0

    .line 2689
    invoke-virtual {p0}, Lcom/a/b/d/sa;->lock()V

    .line 19062
    :try_start_4
    invoke-virtual {p0}, Lcom/a/b/d/sa;->c()V

    .line 2693
    iget-object v4, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2694
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 2695
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    move-object v3, v0

    .line 2697
    :goto_18
    if-eqz v3, :cond_7e

    .line 2698
    invoke-interface {v3}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v6

    .line 2699
    invoke-interface {v3}, Lcom/a/b/d/rz;->c()I

    move-result v2

    if-ne v2, p2, :cond_78

    if-eqz v6, :cond_78

    iget-object v2, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v2, v2, Lcom/a/b/d/qy;->m:Lcom/a/b/b/au;

    invoke-virtual {v2, p1, v6}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_78

    .line 2703
    invoke-interface {v3}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v7

    .line 2704
    invoke-interface {v7}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v2

    .line 2705
    if-nez v2, :cond_62

    .line 2706
    invoke-static {v7}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/sr;)Z

    move-result v7

    if-eqz v7, :cond_5a

    .line 2707
    iget v7, p0, Lcom/a/b/d/sa;->b:I

    .line 2708
    iget v7, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/a/b/d/sa;->c:I

    .line 2709
    sget-object v7, Lcom/a/b/d/qq;->c:Lcom/a/b/d/qq;

    invoke-virtual {p0, v6, v2, v7}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2710
    invoke-direct {p0, v0, v3}, Lcom/a/b/d/sa;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 2711
    iget v2, p0, Lcom/a/b/d/sa;->b:I

    add-int/lit8 v2, v2, -0x1

    .line 2712
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2713
    iput v2, p0, Lcom/a/b/d/sa;->b:I
    :try_end_5a
    .catchall {:try_start_4 .. :try_end_5a} :catchall_86

    .line 2727
    :cond_5a
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 19069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    move-object v0, v1

    .line 2728
    :goto_61
    return-object v0

    .line 2718
    :cond_62
    :try_start_62
    iget v0, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/sa;->c:I

    .line 2719
    sget-object v0, Lcom/a/b/d/qq;->b:Lcom/a/b/d/qq;

    invoke-virtual {p0, p1, v2, v0}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2720
    invoke-direct {p0, v3, p3}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;Ljava/lang/Object;)V
    :try_end_70
    .catchall {:try_start_62 .. :try_end_70} :catchall_86

    .line 2727
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 20069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    move-object v0, v2

    .line 2728
    goto :goto_61

    .line 2697
    :cond_78
    :try_start_78
    invoke-interface {v3}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;
    :try_end_7b
    .catchall {:try_start_78 .. :try_end_7b} :catchall_86

    move-result-object v2

    move-object v3, v2

    goto :goto_18

    .line 2727
    :cond_7e
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 21069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    move-object v0, v1

    .line 2728
    goto :goto_61

    .line 2727
    :catchall_86
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 22069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2728
    throw v0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
    .registers 16

    .prologue
    .line 2498
    invoke-virtual {p0}, Lcom/a/b/d/sa;->lock()V

    .line 8062
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/sa;->c()V

    .line 2502
    iget v0, p0, Lcom/a/b/d/sa;->b:I

    add-int/lit8 v1, v0, 0x1

    .line 2503
    iget v0, p0, Lcom/a/b/d/sa;->d:I

    if-le v1, v0, :cond_93

    .line 8570
    iget-object v7, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 8571
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v8

    .line 8572
    const/high16 v0, 0x40000000    # 2.0f

    if-ge v8, v0, :cond_8f

    .line 8586
    iget v1, p0, Lcom/a/b/d/sa;->b:I

    .line 8587
    shl-int/lit8 v0, v8, 0x1

    invoke-static {v0}, Lcom/a/b/d/sa;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v9

    .line 8588
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/a/b/d/sa;->d:I

    .line 8589
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    .line 8590
    const/4 v0, 0x0

    move v6, v0

    :goto_32
    if-ge v6, v8, :cond_8b

    .line 8593
    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    .line 8595
    if-eqz v0, :cond_145

    .line 8596
    invoke-interface {v0}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v3

    .line 8597
    invoke-interface {v0}, Lcom/a/b/d/rz;->c()I

    move-result v2

    and-int v5, v2, v10

    .line 8600
    if-nez v3, :cond_51

    .line 8601
    invoke-virtual {v9, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v0, v1

    .line 8590
    :goto_4c
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v1, v0

    goto :goto_32

    :cond_51
    move-object v4, v0

    .line 8608
    :goto_52
    if-eqz v3, :cond_64

    .line 8609
    invoke-interface {v3}, Lcom/a/b/d/rz;->c()I

    move-result v2

    and-int/2addr v2, v10

    .line 8610
    if-eq v2, v5, :cond_148

    move v4, v2

    move-object v2, v3

    .line 8608
    :goto_5d
    invoke-interface {v3}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v3

    move v5, v4

    move-object v4, v2

    goto :goto_52

    .line 8616
    :cond_64
    invoke-virtual {v9, v5, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move-object v2, v0

    .line 8619
    :goto_68
    if-eq v2, v4, :cond_145

    .line 8620
    invoke-interface {v2}, Lcom/a/b/d/rz;->c()I

    move-result v0

    and-int v3, v0, v10

    .line 8621
    invoke-virtual {v9, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    .line 8622
    invoke-virtual {p0, v2, v0}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 8623
    if-eqz v0, :cond_85

    .line 8624
    invoke-virtual {v9, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 8619
    :goto_7f
    invoke-interface {v2}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v0

    move-object v2, v0

    goto :goto_68

    .line 8626
    :cond_85
    invoke-direct {p0, v2}, Lcom/a/b/d/sa;->e(Lcom/a/b/d/rz;)V

    .line 8627
    add-int/lit8 v1, v1, -0x1

    goto :goto_7f

    .line 8633
    :cond_8b
    iput-object v9, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 8634
    iput v1, p0, Lcom/a/b/d/sa;->b:I

    .line 2505
    :cond_8f
    iget v0, p0, Lcom/a/b/d/sa;->b:I

    add-int/lit8 v1, v0, 0x1

    .line 2508
    :cond_93
    iget-object v3, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2509
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 2510
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    move-object v2, v0

    .line 2513
    :goto_a4
    if-eqz v2, :cond_117

    .line 2514
    invoke-interface {v2}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v5

    .line 2515
    invoke-interface {v2}, Lcom/a/b/d/rz;->c()I

    move-result v6

    if-ne v6, p2, :cond_112

    if-eqz v5, :cond_112

    iget-object v6, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v6, v6, Lcom/a/b/d/qy;->m:Lcom/a/b/b/au;

    invoke-virtual {v6, p1, v5}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_112

    .line 2519
    invoke-interface {v2}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v3

    .line 2520
    invoke-interface {v3}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2522
    if-nez v0, :cond_f1

    .line 2523
    iget v4, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/a/b/d/sa;->c:I

    .line 2524
    invoke-direct {p0, v2, p3}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;Ljava/lang/Object;)V

    .line 2525
    invoke-interface {v3}, Lcom/a/b/d/sr;->b()Z

    move-result v2

    if-nez v2, :cond_e6

    .line 2526
    sget-object v1, Lcom/a/b/d/qq;->c:Lcom/a/b/d/qq;

    invoke-virtual {p0, p1, v0, v1}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2527
    iget v1, p0, Lcom/a/b/d/sa;->b:I

    .line 2531
    :cond_dc
    :goto_dc
    iput v1, p0, Lcom/a/b/d/sa;->b:I
    :try_end_de
    .catchall {:try_start_3 .. :try_end_de} :catchall_13b

    .line 2560
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 9069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2561
    const/4 v0, 0x0

    :goto_e5
    return-object v0

    .line 2528
    :cond_e6
    :try_start_e6
    invoke-direct {p0}, Lcom/a/b/d/sa;->o()Z

    move-result v0

    if-eqz v0, :cond_dc

    .line 2529
    iget v0, p0, Lcom/a/b/d/sa;->b:I

    add-int/lit8 v1, v0, 0x1

    goto :goto_dc

    .line 2533
    :cond_f1
    if-eqz p4, :cond_fd

    .line 2537
    invoke-virtual {p0, v2}, Lcom/a/b/d/sa;->b(Lcom/a/b/d/rz;)V
    :try_end_f6
    .catchall {:try_start_e6 .. :try_end_f6} :catchall_13b

    .line 2560
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 10069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    goto :goto_e5

    .line 2541
    :cond_fd
    :try_start_fd
    iget v1, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/b/d/sa;->c:I

    .line 2542
    sget-object v1, Lcom/a/b/d/qq;->b:Lcom/a/b/d/qq;

    invoke-virtual {p0, p1, v0, v1}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2543
    invoke-direct {p0, v2, p3}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;Ljava/lang/Object;)V
    :try_end_10b
    .catchall {:try_start_fd .. :try_end_10b} :catchall_13b

    .line 2560
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 11069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    goto :goto_e5

    .line 2513
    :cond_112
    :try_start_112
    invoke-interface {v2}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v2

    goto :goto_a4

    .line 2550
    :cond_117
    iget v2, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/a/b/d/sa;->c:I

    .line 2551
    invoke-virtual {p0, p1, p2, v0}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;ILcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 2552
    invoke-direct {p0, v0, p3}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;Ljava/lang/Object;)V

    .line 2553
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2554
    invoke-direct {p0}, Lcom/a/b/d/sa;->o()Z

    move-result v0

    if-eqz v0, :cond_143

    .line 2555
    iget v0, p0, Lcom/a/b/d/sa;->b:I

    add-int/lit8 v0, v0, 0x1

    .line 2557
    :goto_131
    iput v0, p0, Lcom/a/b/d/sa;->b:I
    :try_end_133
    .catchall {:try_start_112 .. :try_end_133} :catchall_13b

    .line 2560
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 12069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2561
    const/4 v0, 0x0

    goto :goto_e5

    .line 2560
    :catchall_13b
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 13069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2561
    throw v0

    :cond_143
    move v0, v1

    goto :goto_131

    :cond_145
    move v0, v1

    goto/16 :goto_4c

    :cond_148
    move-object v2, v4

    move v4, v5

    goto/16 :goto_5d
.end method

.method final a()V
    .registers 2

    .prologue
    .line 3049
    iget-object v0, p0, Lcom/a/b/d/sa;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    if-nez v0, :cond_d

    .line 3050
    invoke-virtual {p0}, Lcom/a/b/d/sa;->b()V

    .line 3052
    :cond_d
    return-void
.end method

.method final a(Lcom/a/b/d/rz;)V
    .registers 4

    .prologue
    .line 2252
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->d()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2253
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-wide v0, v0, Lcom/a/b/d/qy;->r:J

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;J)V

    .line 2255
    :cond_f
    iget-object v0, p0, Lcom/a/b/d/sa;->i:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2256
    return-void
.end method

.method final a(Lcom/a/b/d/rz;Lcom/a/b/d/qq;)V
    .registers 5

    .prologue
    .line 2359
    invoke-interface {p1}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/rz;->c()I

    invoke-interface {p1}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2360
    return-void
.end method

.method final a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2363
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v0, v0, Lcom/a/b/d/qy;->t:Ljava/util/Queue;

    sget-object v1, Lcom/a/b/d/qy;->y:Ljava/util/Queue;

    if-eq v0, v1, :cond_14

    .line 2364
    new-instance v0, Lcom/a/b/d/qx;

    invoke-direct {v0, p1, p2, p3}, Lcom/a/b/d/qx;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2365
    iget-object v1, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v1, v1, Lcom/a/b/d/qy;->t:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 2367
    :cond_14
    return-void
.end method

.method final a(Lcom/a/b/d/rz;I)Z
    .registers 10

    .prologue
    .line 2892
    invoke-virtual {p0}, Lcom/a/b/d/sa;->lock()V

    .line 2894
    :try_start_3
    iget v0, p0, Lcom/a/b/d/sa;->b:I

    .line 2895
    iget-object v2, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2896
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v3, p2, v0

    .line 2897
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    move-object v1, v0

    .line 2899
    :goto_16
    if-eqz v1, :cond_4b

    .line 2900
    if-ne v1, p1, :cond_46

    .line 2901
    iget v4, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/a/b/d/sa;->c:I

    .line 2902
    invoke-interface {v1}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v5

    invoke-interface {v5}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lcom/a/b/d/qq;->c:Lcom/a/b/d/qq;

    invoke-virtual {p0, v4, v5, v6}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2904
    invoke-direct {p0, v0, v1}, Lcom/a/b/d/sa;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 2905
    iget v1, p0, Lcom/a/b/d/sa;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 2906
    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2907
    iput v1, p0, Lcom/a/b/d/sa;->b:I
    :try_end_3e
    .catchall {:try_start_3 .. :try_end_3e} :catchall_53

    .line 2914
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 33069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2915
    const/4 v0, 0x1

    :goto_45
    return v0

    .line 2899
    :cond_46
    :try_start_46
    invoke-interface {v1}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;
    :try_end_49
    .catchall {:try_start_46 .. :try_end_49} :catchall_53

    move-result-object v1

    goto :goto_16

    .line 2914
    :cond_4b
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 34069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2915
    const/4 v0, 0x0

    goto :goto_45

    .line 2914
    :catchall_53
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 35069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2915
    throw v0
.end method

.method final a(Ljava/lang/Object;ILcom/a/b/d/sr;)Z
    .registers 11

    .prologue
    const/4 v1, 0x0

    .line 2923
    invoke-virtual {p0}, Lcom/a/b/d/sa;->lock()V

    .line 2925
    :try_start_4
    iget v0, p0, Lcom/a/b/d/sa;->b:I

    .line 2926
    iget-object v3, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2927
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 2928
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    move-object v2, v0

    .line 2930
    :goto_17
    if-eqz v2, :cond_72

    .line 2931
    invoke-interface {v2}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v5

    .line 2932
    invoke-interface {v2}, Lcom/a/b/d/rz;->c()I

    move-result v6

    if-ne v6, p2, :cond_6d

    if-eqz v5, :cond_6d

    iget-object v6, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v6, v6, Lcom/a/b/d/qy;->m:Lcom/a/b/b/au;

    invoke-virtual {v6, p1, v5}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6d

    .line 2934
    invoke-interface {v2}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v5

    .line 2935
    if-ne v5, p3, :cond_5f

    .line 2936
    iget v1, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/b/d/sa;->c:I

    .line 2937
    invoke-interface {p3}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v5, Lcom/a/b/d/qq;->c:Lcom/a/b/d/qq;

    invoke-virtual {p0, p1, v1, v5}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2938
    invoke-direct {p0, v0, v2}, Lcom/a/b/d/sa;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 2939
    iget v1, p0, Lcom/a/b/d/sa;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 2940
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2941
    iput v1, p0, Lcom/a/b/d/sa;->b:I
    :try_end_51
    .catchall {:try_start_4 .. :try_end_51} :catchall_80

    .line 2950
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 2951
    invoke-virtual {p0}, Lcom/a/b/d/sa;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_5d

    .line 36069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2952
    :cond_5d
    const/4 v0, 0x1

    :goto_5e
    return v0

    .line 2950
    :cond_5f
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 2951
    invoke-virtual {p0}, Lcom/a/b/d/sa;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_6b

    .line 37069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    :cond_6b
    move v0, v1

    .line 2952
    goto :goto_5e

    .line 2930
    :cond_6d
    :try_start_6d
    invoke-interface {v2}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;
    :try_end_70
    .catchall {:try_start_6d .. :try_end_70} :catchall_80

    move-result-object v2

    goto :goto_17

    .line 2950
    :cond_72
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 2951
    invoke-virtual {p0}, Lcom/a/b/d/sa;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_7e

    .line 38069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    :cond_7e
    move v0, v1

    .line 2952
    goto :goto_5e

    .line 2950
    :catchall_80
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 2951
    invoke-virtual {p0}, Lcom/a/b/d/sa;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_8d

    .line 39069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2952
    :cond_8d
    throw v0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
    .registers 13

    .prologue
    const/4 v1, 0x0

    .line 2638
    invoke-virtual {p0}, Lcom/a/b/d/sa;->lock()V

    .line 14062
    :try_start_4
    invoke-virtual {p0}, Lcom/a/b/d/sa;->c()V

    .line 2642
    iget-object v3, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2643
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 2644
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    move-object v2, v0

    .line 2646
    :goto_18
    if-eqz v2, :cond_92

    .line 2647
    invoke-interface {v2}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v5

    .line 2648
    invoke-interface {v2}, Lcom/a/b/d/rz;->c()I

    move-result v6

    if-ne v6, p2, :cond_8d

    if-eqz v5, :cond_8d

    iget-object v6, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v6, v6, Lcom/a/b/d/qy;->m:Lcom/a/b/b/au;

    invoke-virtual {v6, p1, v5}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8d

    .line 2652
    invoke-interface {v2}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v6

    .line 2653
    invoke-interface {v6}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v7

    .line 2654
    if-nez v7, :cond_62

    .line 2655
    invoke-static {v6}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/sr;)Z

    move-result v6

    if-eqz v6, :cond_5a

    .line 2656
    iget v6, p0, Lcom/a/b/d/sa;->b:I

    .line 2657
    iget v6, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/a/b/d/sa;->c:I

    .line 2658
    sget-object v6, Lcom/a/b/d/qq;->c:Lcom/a/b/d/qq;

    invoke-virtual {p0, v5, v7, v6}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2659
    invoke-direct {p0, v0, v2}, Lcom/a/b/d/sa;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 2660
    iget v2, p0, Lcom/a/b/d/sa;->b:I

    add-int/lit8 v2, v2, -0x1

    .line 2661
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2662
    iput v2, p0, Lcom/a/b/d/sa;->b:I
    :try_end_5a
    .catchall {:try_start_4 .. :try_end_5a} :catchall_9a

    .line 2683
    :cond_5a
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 14069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    move v0, v1

    .line 2684
    :goto_61
    return v0

    .line 2667
    :cond_62
    :try_start_62
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v0, v0, Lcom/a/b/d/qy;->n:Lcom/a/b/b/au;

    invoke-virtual {v0, p3, v7}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_82

    .line 2668
    iget v0, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/sa;->c:I

    .line 2669
    sget-object v0, Lcom/a/b/d/qq;->b:Lcom/a/b/d/qq;

    invoke-virtual {p0, p1, v7, v0}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2670
    invoke-direct {p0, v2, p4}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;Ljava/lang/Object;)V
    :try_end_7a
    .catchall {:try_start_62 .. :try_end_7a} :catchall_9a

    .line 2683
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 15069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2684
    const/4 v0, 0x1

    goto :goto_61

    .line 2675
    :cond_82
    :try_start_82
    invoke-virtual {p0, v2}, Lcom/a/b/d/sa;->b(Lcom/a/b/d/rz;)V
    :try_end_85
    .catchall {:try_start_82 .. :try_end_85} :catchall_9a

    .line 2683
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 16069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    move v0, v1

    .line 2684
    goto :goto_61

    .line 2646
    :cond_8d
    :try_start_8d
    invoke-interface {v2}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;
    :try_end_90
    .catchall {:try_start_8d .. :try_end_90} :catchall_9a

    move-result-object v2

    goto :goto_18

    .line 2683
    :cond_92
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 17069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    move v0, v1

    .line 2684
    goto :goto_61

    .line 2683
    :catchall_9a
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 18069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2684
    throw v0
.end method

.method final b(Ljava/lang/Object;I)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 2435
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/sa;->e(Ljava/lang/Object;I)Lcom/a/b/d/rz;
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_20

    move-result-object v1

    .line 2436
    if-nez v1, :cond_b

    .line 2448
    invoke-virtual {p0}, Lcom/a/b/d/sa;->a()V

    const/4 v0, 0x0

    :goto_a
    return-object v0

    .line 2440
    :cond_b
    :try_start_b
    invoke-interface {v1}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2441
    if-eqz v0, :cond_1c

    .line 2442
    invoke-virtual {p0, v1}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;)V
    :try_end_18
    .catchall {:try_start_b .. :try_end_18} :catchall_20

    .line 2448
    :goto_18
    invoke-virtual {p0}, Lcom/a/b/d/sa;->a()V

    goto :goto_a

    .line 2444
    :cond_1c
    :try_start_1c
    invoke-direct {p0}, Lcom/a/b/d/sa;->e()V
    :try_end_1f
    .catchall {:try_start_1c .. :try_end_1f} :catchall_20

    goto :goto_18

    .line 2448
    :catchall_20
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->a()V

    throw v0
.end method

.method final b()V
    .registers 1

    .prologue
    .line 3073
    invoke-virtual {p0}, Lcom/a/b/d/sa;->c()V

    .line 3074
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 3075
    return-void
.end method

.method final b(Lcom/a/b/d/rz;)V
    .registers 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation

    .prologue
    .line 2267
    iget-object v0, p0, Lcom/a/b/d/sa;->k:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2268
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->d()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 2269
    iget-object v0, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-wide v0, v0, Lcom/a/b/d/qy;->r:J

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;J)V

    .line 2270
    iget-object v0, p0, Lcom/a/b/d/sa;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2272
    :cond_19
    return-void
.end method

.method final b(Ljava/lang/Object;ILcom/a/b/d/sr;)Z
    .registers 11

    .prologue
    const/4 v1, 0x0

    .line 2961
    invoke-virtual {p0}, Lcom/a/b/d/sa;->lock()V

    .line 2963
    :try_start_4
    iget-object v3, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2964
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 2965
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    move-object v2, v0

    .line 2967
    :goto_15
    if-eqz v2, :cond_4f

    .line 2968
    invoke-interface {v2}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v5

    .line 2969
    invoke-interface {v2}, Lcom/a/b/d/rz;->c()I

    move-result v6

    if-ne v6, p2, :cond_4a

    if-eqz v5, :cond_4a

    iget-object v6, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v6, v6, Lcom/a/b/d/qy;->m:Lcom/a/b/b/au;

    invoke-virtual {v6, p1, v5}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4a

    .line 2971
    invoke-interface {v2}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v5

    .line 2972
    if-ne v5, p3, :cond_42

    .line 2973
    invoke-direct {p0, v0, v2}, Lcom/a/b/d/sa;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 2974
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_3a
    .catchall {:try_start_4 .. :try_end_3a} :catchall_57

    .line 2983
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 40069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2984
    const/4 v0, 0x1

    :goto_41
    return v0

    .line 2983
    :cond_42
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 41069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    move v0, v1

    .line 2984
    goto :goto_41

    .line 2967
    :cond_4a
    :try_start_4a
    invoke-interface {v2}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;
    :try_end_4d
    .catchall {:try_start_4a .. :try_end_4d} :catchall_57

    move-result-object v2

    goto :goto_15

    .line 2983
    :cond_4f
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 42069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    move v0, v1

    .line 2984
    goto :goto_41

    .line 2983
    :catchall_57
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 43069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2984
    throw v0
.end method

.method final b(Ljava/lang/Object;ILjava/lang/Object;)Z
    .registers 13

    .prologue
    const/4 v1, 0x0

    .line 2776
    invoke-virtual {p0}, Lcom/a/b/d/sa;->lock()V

    .line 27062
    :try_start_4
    invoke-virtual {p0}, Lcom/a/b/d/sa;->c()V

    .line 2780
    iget v0, p0, Lcom/a/b/d/sa;->b:I

    .line 2781
    iget-object v4, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2782
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 2783
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    move-object v3, v0

    .line 2785
    :goto_1a
    if-eqz v3, :cond_81

    .line 2786
    invoke-interface {v3}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v6

    .line 2787
    invoke-interface {v3}, Lcom/a/b/d/rz;->c()I

    move-result v2

    if-ne v2, p2, :cond_7b

    if-eqz v6, :cond_7b

    iget-object v2, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v2, v2, Lcom/a/b/d/qy;->m:Lcom/a/b/b/au;

    invoke-virtual {v2, p1, v6}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7b

    .line 2789
    invoke-interface {v3}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v2

    .line 2790
    invoke-interface {v2}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v7

    .line 2793
    iget-object v8, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v8, v8, Lcom/a/b/d/qy;->n:Lcom/a/b/b/au;

    invoke-virtual {v8, p3, v7}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_69

    .line 2794
    sget-object v2, Lcom/a/b/d/qq;->a:Lcom/a/b/d/qq;

    .line 2801
    :goto_46
    iget v8, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/a/b/d/sa;->c:I

    .line 2802
    invoke-virtual {p0, v6, v7, v2}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2803
    invoke-direct {p0, v0, v3}, Lcom/a/b/d/sa;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 2804
    iget v3, p0, Lcom/a/b/d/sa;->b:I

    add-int/lit8 v3, v3, -0x1

    .line 2805
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2806
    iput v3, p0, Lcom/a/b/d/sa;->b:I

    .line 2807
    sget-object v0, Lcom/a/b/d/qq;->a:Lcom/a/b/d/qq;
    :try_end_5e
    .catchall {:try_start_4 .. :try_end_5e} :catchall_88

    if-ne v2, v0, :cond_79

    const/4 v0, 0x1

    .line 2813
    :goto_61
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 28069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    move v1, v0

    .line 2814
    :goto_68
    return v1

    .line 2795
    :cond_69
    :try_start_69
    invoke-static {v2}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/sr;)Z

    move-result v2

    if-eqz v2, :cond_72

    .line 2796
    sget-object v2, Lcom/a/b/d/qq;->c:Lcom/a/b/d/qq;
    :try_end_71
    .catchall {:try_start_69 .. :try_end_71} :catchall_88

    goto :goto_46

    .line 2813
    :cond_72
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 27069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    goto :goto_68

    :cond_79
    move v0, v1

    .line 2807
    goto :goto_61

    .line 2785
    :cond_7b
    :try_start_7b
    invoke-interface {v3}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;
    :try_end_7e
    .catchall {:try_start_7b .. :try_end_7e} :catchall_88

    move-result-object v2

    move-object v3, v2

    goto :goto_1a

    .line 2813
    :cond_81
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 29069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    goto :goto_68

    .line 2813
    :catchall_88
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 30069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2814
    throw v0
.end method

.method final c(Lcom/a/b/d/rz;)Ljava/lang/Object;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 3026
    invoke-interface {p1}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_b

    .line 3027
    invoke-direct {p0}, Lcom/a/b/d/sa;->e()V

    .line 3040
    :goto_a
    return-object v0

    .line 3030
    :cond_b
    invoke-interface {p1}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v1

    .line 3031
    if-nez v1, :cond_19

    .line 3032
    invoke-direct {p0}, Lcom/a/b/d/sa;->e()V

    goto :goto_a

    .line 3036
    :cond_19
    iget-object v2, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v2}, Lcom/a/b/d/qy;->c()Z

    move-result v2

    if-eqz v2, :cond_2d

    iget-object v2, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v2, p1}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;)Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 3037
    invoke-direct {p0}, Lcom/a/b/d/sa;->m()V

    goto :goto_a

    :cond_2d
    move-object v0, v1

    .line 3040
    goto :goto_a
.end method

.method final c()V
    .registers 3

    .prologue
    .line 3078
    invoke-virtual {p0}, Lcom/a/b/d/sa;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 3080
    :try_start_6
    invoke-direct {p0}, Lcom/a/b/d/sa;->f()V

    .line 3081
    invoke-direct {p0}, Lcom/a/b/d/sa;->n()V

    .line 3082
    iget-object v0, p0, Lcom/a/b/d/sa;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_12
    .catchall {:try_start_6 .. :try_end_12} :catchall_16

    .line 3084
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 3087
    :cond_15
    return-void

    .line 3084
    :catchall_16
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    throw v0
.end method

.method final c(Ljava/lang/Object;I)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 2454
    :try_start_1
    iget v1, p0, Lcom/a/b/d/sa;->b:I

    if-eqz v1, :cond_1e

    .line 2455
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/sa;->e(Ljava/lang/Object;I)Lcom/a/b/d/rz;
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_22

    move-result-object v1

    .line 2456
    if-nez v1, :cond_f

    .line 2464
    invoke-virtual {p0}, Lcom/a/b/d/sa;->a()V

    :goto_e
    return v0

    .line 2459
    :cond_f
    :try_start_f
    invoke-interface {v1}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;
    :try_end_16
    .catchall {:try_start_f .. :try_end_16} :catchall_22

    move-result-object v1

    if-eqz v1, :cond_1a

    const/4 v0, 0x1

    .line 2464
    :cond_1a
    invoke-virtual {p0}, Lcom/a/b/d/sa;->a()V

    goto :goto_e

    :cond_1e
    invoke-virtual {p0}, Lcom/a/b/d/sa;->a()V

    goto :goto_e

    :catchall_22
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->a()V

    throw v0
.end method

.method final d(Ljava/lang/Object;I)Ljava/lang/Object;
    .registers 11

    .prologue
    const/4 v1, 0x0

    .line 2733
    invoke-virtual {p0}, Lcom/a/b/d/sa;->lock()V

    .line 23062
    :try_start_4
    invoke-virtual {p0}, Lcom/a/b/d/sa;->c()V

    .line 2737
    iget v0, p0, Lcom/a/b/d/sa;->b:I

    .line 2738
    iget-object v4, p0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2739
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 2740
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    move-object v3, v0

    .line 2742
    :goto_1a
    if-eqz v3, :cond_73

    .line 2743
    invoke-interface {v3}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v6

    .line 2744
    invoke-interface {v3}, Lcom/a/b/d/rz;->c()I

    move-result v2

    if-ne v2, p2, :cond_6d

    if-eqz v6, :cond_6d

    iget-object v2, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v2, v2, Lcom/a/b/d/qy;->m:Lcom/a/b/b/au;

    invoke-virtual {v2, p1, v6}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6d

    .line 2746
    invoke-interface {v3}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v7

    .line 2747
    invoke-interface {v7}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v2

    .line 2750
    if-eqz v2, :cond_5c

    .line 2751
    sget-object v1, Lcom/a/b/d/qq;->a:Lcom/a/b/d/qq;

    .line 2758
    :goto_3e
    iget v7, p0, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/a/b/d/sa;->c:I

    .line 2759
    invoke-virtual {p0, v6, v2, v1}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V

    .line 2760
    invoke-direct {p0, v0, v3}, Lcom/a/b/d/sa;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    .line 2761
    iget v1, p0, Lcom/a/b/d/sa;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 2762
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2763
    iput v1, p0, Lcom/a/b/d/sa;->b:I
    :try_end_54
    .catchall {:try_start_4 .. :try_end_54} :catchall_7b

    .line 2770
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 24069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    move-object v0, v2

    .line 2771
    :goto_5b
    return-object v0

    .line 2752
    :cond_5c
    :try_start_5c
    invoke-static {v7}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/sr;)Z

    move-result v7

    if-eqz v7, :cond_65

    .line 2753
    sget-object v1, Lcom/a/b/d/qq;->c:Lcom/a/b/d/qq;
    :try_end_64
    .catchall {:try_start_5c .. :try_end_64} :catchall_7b

    goto :goto_3e

    .line 2770
    :cond_65
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 23069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    move-object v0, v1

    .line 2771
    goto :goto_5b

    .line 2742
    :cond_6d
    :try_start_6d
    invoke-interface {v3}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;
    :try_end_70
    .catchall {:try_start_6d .. :try_end_70} :catchall_7b

    move-result-object v2

    move-object v3, v2

    goto :goto_1a

    .line 2770
    :cond_73
    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 25069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    move-object v0, v1

    .line 2771
    goto :goto_5b

    .line 2770
    :catchall_7b
    move-exception v0

    invoke-virtual {p0}, Lcom/a/b/d/sa;->unlock()V

    .line 26069
    invoke-virtual {p0}, Lcom/a/b/d/sa;->d()V

    .line 2771
    throw v0
.end method

.method final d()V
    .registers 3

    .prologue
    .line 3091
    invoke-virtual {p0}, Lcom/a/b/d/sa;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_12

    .line 3092
    iget-object v1, p0, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    .line 43949
    :cond_8
    iget-object v0, v1, Lcom/a/b/d/qy;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/qx;

    if-nez v0, :cond_8

    .line 3094
    :cond_12
    return-void
.end method
