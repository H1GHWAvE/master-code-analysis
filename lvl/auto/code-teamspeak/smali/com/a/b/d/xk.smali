.class final Lcom/a/b/d/xk;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/Iterator;

.field final synthetic b:Ljava/util/Iterator;

.field final synthetic c:Lcom/a/b/d/xj;


# direct methods
.method constructor <init>(Lcom/a/b/d/xj;Ljava/util/Iterator;Ljava/util/Iterator;)V
    .registers 4

    .prologue
    .line 554
    iput-object p1, p0, Lcom/a/b/d/xk;->c:Lcom/a/b/d/xj;

    iput-object p2, p0, Lcom/a/b/d/xk;->a:Ljava/util/Iterator;

    iput-object p3, p0, Lcom/a/b/d/xk;->b:Ljava/util/Iterator;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    return-void
.end method

.method private c()Lcom/a/b/d/xd;
    .registers 4

    .prologue
    .line 557
    iget-object v0, p0, Lcom/a/b/d/xk;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 558
    iget-object v0, p0, Lcom/a/b/d/xk;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 559
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v1

    .line 560
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    iget-object v2, p0, Lcom/a/b/d/xk;->c:Lcom/a/b/d/xj;

    iget-object v2, v2, Lcom/a/b/d/xj;->b:Lcom/a/b/d/xc;

    invoke-interface {v2, v1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 561
    invoke-static {v1, v0}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    .line 570
    :goto_25
    return-object v0

    .line 563
    :cond_26
    iget-object v0, p0, Lcom/a/b/d/xk;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 564
    iget-object v0, p0, Lcom/a/b/d/xk;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 565
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v1

    .line 566
    iget-object v2, p0, Lcom/a/b/d/xk;->c:Lcom/a/b/d/xj;

    iget-object v2, v2, Lcom/a/b/d/xj;->a:Lcom/a/b/d/xc;

    invoke-interface {v2, v1}, Lcom/a/b/d/xc;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_26

    .line 567
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    invoke-static {v1, v0}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    goto :goto_25

    .line 570
    :cond_4d
    invoke-virtual {p0}, Lcom/a/b/d/xk;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    goto :goto_25
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 554
    .line 1557
    iget-object v0, p0, Lcom/a/b/d/xk;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 1558
    iget-object v0, p0, Lcom/a/b/d/xk;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 1559
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1560
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    iget-object v2, p0, Lcom/a/b/d/xk;->c:Lcom/a/b/d/xj;

    iget-object v2, v2, Lcom/a/b/d/xj;->b:Lcom/a/b/d/xc;

    invoke-interface {v2, v1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1561
    invoke-static {v1, v0}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    .line 1567
    :goto_25
    return-object v0

    .line 1563
    :cond_26
    iget-object v0, p0, Lcom/a/b/d/xk;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 1564
    iget-object v0, p0, Lcom/a/b/d/xk;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 1565
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1566
    iget-object v2, p0, Lcom/a/b/d/xk;->c:Lcom/a/b/d/xj;

    iget-object v2, v2, Lcom/a/b/d/xj;->a:Lcom/a/b/d/xc;

    invoke-interface {v2, v1}, Lcom/a/b/d/xc;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_26

    .line 1567
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    invoke-static {v1, v0}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    goto :goto_25

    .line 1570
    :cond_4d
    invoke-virtual {p0}, Lcom/a/b/d/xk;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    .line 554
    goto :goto_25
.end method
