.class final Lcom/a/b/d/wi;
.super Lcom/a/b/d/m;
.source "SourceFile"


# static fields
.field private static final b:J
    .annotation build Lcom/a/b/a/c;
        a = "java serialization not supported"
    .end annotation
.end field


# instance fields
.field transient a:Lcom/a/b/b/dz;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/a/b/b/dz;)V
    .registers 4

    .prologue
    .line 203
    invoke-direct {p0, p1}, Lcom/a/b/d/m;-><init>(Ljava/util/Map;)V

    .line 204
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/dz;

    iput-object v0, p0, Lcom/a/b/d/wi;->a:Lcom/a/b/b/dz;

    .line 205
    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 223
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 224
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/dz;

    iput-object v0, p0, Lcom/a/b/d/wi;->a:Lcom/a/b/b/dz;

    .line 225
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 226
    invoke-virtual {p0, v0}, Lcom/a/b/d/wi;->a(Ljava/util/Map;)V

    .line 227
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 214
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 215
    iget-object v0, p0, Lcom/a/b/d/wi;->a:Lcom/a/b/b/dz;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 216
    invoke-virtual {p0}, Lcom/a/b/d/wi;->e()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 217
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/List;
    .registers 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/a/b/d/wi;->a:Lcom/a/b/b/dz;

    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method protected final synthetic c()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/a/b/d/wi;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
