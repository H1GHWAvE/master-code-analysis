.class final Lcom/a/b/d/wl;
.super Lcom/a/b/d/be;
.source "SourceFile"


# static fields
.field private static final c:J
    .annotation build Lcom/a/b/a/c;
        a = "not needed in emulated source"
    .end annotation
.end field


# instance fields
.field transient a:Lcom/a/b/b/dz;

.field transient b:Ljava/util/Comparator;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/a/b/b/dz;)V
    .registers 4

    .prologue
    .line 360
    invoke-direct {p0, p1}, Lcom/a/b/d/be;-><init>(Ljava/util/Map;)V

    .line 361
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/dz;

    iput-object v0, p0, Lcom/a/b/d/wl;->a:Lcom/a/b/b/dz;

    .line 362
    invoke-interface {p2}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/wl;->b:Ljava/util/Comparator;

    .line 363
    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 385
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 386
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/dz;

    iput-object v0, p0, Lcom/a/b/d/wl;->a:Lcom/a/b/b/dz;

    .line 387
    iget-object v0, p0, Lcom/a/b/d/wl;->a:Lcom/a/b/b/dz;

    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/wl;->b:Ljava/util/Comparator;

    .line 388
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 389
    invoke-virtual {p0, v0}, Lcom/a/b/d/wl;->a(Ljava/util/Map;)V

    .line 390
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 376
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 377
    iget-object v0, p0, Lcom/a/b/d/wl;->a:Lcom/a/b/b/dz;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 378
    invoke-virtual {p0}, Lcom/a/b/d/wl;->e()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 379
    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/a/b/d/wl;->y()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic c()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/a/b/d/wl;->y()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final d_()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/a/b/d/wl;->b:Ljava/util/Comparator;

    return-object v0
.end method

.method protected final y()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/a/b/d/wl;->a:Lcom/a/b/b/dz;

    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method
