.class final Lcom/a/b/d/ro;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/qy;


# direct methods
.method constructor <init>(Lcom/a/b/d/qy;)V
    .registers 2

    .prologue
    .line 3834
    iput-object p1, p0, Lcom/a/b/d/ro;->a:Lcom/a/b/d/qy;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 3878
    iget-object v0, p0, Lcom/a/b/d/ro;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->clear()V

    .line 3879
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 3843
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_6

    .line 3853
    :cond_5
    :goto_5
    return v0

    .line 3846
    :cond_6
    check-cast p1, Ljava/util/Map$Entry;

    .line 3847
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 3848
    if-eqz v1, :cond_5

    .line 3851
    iget-object v2, p0, Lcom/a/b/d/ro;->a:Lcom/a/b/d/qy;

    invoke-virtual {v2, v1}, Lcom/a/b/d/qy;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 3853
    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/a/b/d/ro;->a:Lcom/a/b/d/qy;

    iget-object v2, v2, Lcom/a/b/d/qy;->n:Lcom/a/b/b/au;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 3873
    iget-object v0, p0, Lcom/a/b/d/ro;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 3838
    new-instance v0, Lcom/a/b/d/rn;

    iget-object v1, p0, Lcom/a/b/d/ro;->a:Lcom/a/b/d/qy;

    invoke-direct {v0, v1}, Lcom/a/b/d/rn;-><init>(Lcom/a/b/d/qy;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 3858
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_6

    .line 3863
    :cond_5
    :goto_5
    return v0

    .line 3861
    :cond_6
    check-cast p1, Ljava/util/Map$Entry;

    .line 3862
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 3863
    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/a/b/d/ro;->a:Lcom/a/b/d/qy;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/a/b/d/qy;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 3868
    iget-object v0, p0, Lcom/a/b/d/ro;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->size()I

    move-result v0

    return v0
.end method
