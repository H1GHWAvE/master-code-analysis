.class public abstract Lcom/a/b/d/iz;
.super Ljava/util/AbstractCollection;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# instance fields
.field private transient a:Lcom/a/b/d/jl;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    return-void
.end method


# virtual methods
.method a([Ljava/lang/Object;I)I
    .registers 6

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/a/b/d/iz;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 200
    add-int/lit8 v0, p2, 0x1

    aput-object v2, p1, p2

    move p2, v0

    .line 201
    goto :goto_4

    .line 202
    :cond_14
    return p2
.end method

.method public final add(Ljava/lang/Object;)Z
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 96
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 120
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public abstract c()Lcom/a/b/d/agi;
.end method

.method public final clear()V
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 156
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 84
    if-eqz p1, :cond_a

    invoke-super {p0, p1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public f()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/a/b/d/iz;->a:Lcom/a/b/d/jl;

    .line 172
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/iz;->l()Lcom/a/b/d/jl;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/iz;->a:Lcom/a/b/d/jl;

    :cond_a
    return-object v0
.end method

.method g()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 207
    new-instance v0, Lcom/a/b/d/jp;

    invoke-virtual {p0}, Lcom/a/b/d/iz;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/jp;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method

.method abstract h_()Z
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/a/b/d/iz;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method l()Lcom/a/b/d/jl;
    .registers 3

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/a/b/d/iz;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_24

    .line 182
    new-instance v0, Lcom/a/b/d/yw;

    invoke-virtual {p0}, Lcom/a/b/d/iz;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/yw;-><init>(Lcom/a/b/d/iz;[Ljava/lang/Object;)V

    :goto_10
    return-object v0

    .line 178
    :pswitch_11
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_10

    .line 180
    :pswitch_16
    invoke-virtual {p0}, Lcom/a/b/d/iz;->c()Lcom/a/b/d/agi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/agi;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_10

    .line 176
    nop

    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_11
        :pswitch_16
    .end packed-switch
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 108
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 132
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 144
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/a/b/d/iz;->size()I

    move-result v0

    .line 61
    if-nez v0, :cond_9

    .line 62
    sget-object v0, Lcom/a/b/d/yc;->a:[Ljava/lang/Object;

    .line 66
    :goto_8
    return-object v0

    .line 64
    :cond_9
    new-array v0, v0, [Ljava/lang/Object;

    .line 65
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/iz;->a([Ljava/lang/Object;I)I

    goto :goto_8
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 4

    .prologue
    .line 71
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    invoke-virtual {p0}, Lcom/a/b/d/iz;->size()I

    move-result v0

    .line 73
    array-length v1, p1

    if-ge v1, v0, :cond_13

    .line 74
    invoke-static {p1, v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    .line 78
    :cond_e
    :goto_e
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/iz;->a([Ljava/lang/Object;I)I

    .line 79
    return-object p1

    .line 75
    :cond_13
    array-length v1, p1

    if-le v1, v0, :cond_e

    .line 76
    const/4 v1, 0x0

    aput-object v1, p1, v0

    goto :goto_e
.end method
