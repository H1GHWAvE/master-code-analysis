.class Lcom/a/b/d/qc;
.super Lcom/a/b/d/gx;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field final a:Lcom/a/b/d/pn;

.field final b:Lcom/a/b/d/vi;

.field transient c:Ljava/util/Collection;

.field transient d:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/a/b/d/vi;Lcom/a/b/d/pn;)V
    .registers 4

    .prologue
    .line 406
    invoke-direct {p0}, Lcom/a/b/d/gx;-><init>()V

    .line 407
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/vi;

    iput-object v0, p0, Lcom/a/b/d/qc;->b:Lcom/a/b/d/vi;

    .line 408
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/pn;

    iput-object v0, p0, Lcom/a/b/d/qc;->a:Lcom/a/b/d/pn;

    .line 409
    return-void
.end method


# virtual methods
.method public final a(Lcom/a/b/d/vi;)Z
    .registers 6

    .prologue
    .line 494
    const/4 v0, 0x0

    .line 495
    invoke-interface {p1}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 496
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/a/b/d/qc;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    .line 497
    goto :goto_a

    .line 498
    :cond_25
    return v1
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 484
    iget-object v0, p0, Lcom/a/b/d/qc;->a:Lcom/a/b/d/pn;

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/pn;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 485
    iget-object v0, p0, Lcom/a/b/d/qc;->b:Lcom/a/b/d/vi;

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/vi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 5

    .prologue
    .line 503
    iget-object v0, p0, Lcom/a/b/d/qc;->b:Lcom/a/b/d/vi;

    iget-object v1, p0, Lcom/a/b/d/qc;->a:Lcom/a/b/d/pn;

    invoke-static {p1, p2, v1}, Lcom/a/b/d/po;->a(Ljava/lang/Object;Ljava/lang/Iterable;Lcom/a/b/d/pn;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/a/b/d/vi;->b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .registers 3

    .prologue
    .line 416
    iget-object v0, p0, Lcom/a/b/d/qc;->d:Ljava/util/Map;

    .line 417
    if-nez v0, :cond_11

    .line 418
    iget-object v0, p0, Lcom/a/b/d/qc;->b:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v1

    .line 420
    new-instance v0, Lcom/a/b/d/qd;

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/qd;-><init>(Lcom/a/b/d/qc;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/a/b/d/qc;->d:Ljava/util/Map;

    .line 461
    :cond_11
    return-object v0
.end method

.method protected final c()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 412
    iget-object v0, p0, Lcom/a/b/d/qc;->b:Lcom/a/b/d/vi;

    return-object v0
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 473
    iget-object v0, p0, Lcom/a/b/d/qc;->b:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    new-instance v1, Lcom/a/b/d/qe;

    invoke-direct {v1, p0, p1}, Lcom/a/b/d/qe;-><init>(Lcom/a/b/d/qc;Ljava/lang/Object;)V

    invoke-static {v0, v1}, Lcom/a/b/d/dn;->a(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 5

    .prologue
    .line 489
    iget-object v0, p0, Lcom/a/b/d/qc;->b:Lcom/a/b/d/vi;

    iget-object v1, p0, Lcom/a/b/d/qc;->a:Lcom/a/b/d/pn;

    invoke-static {p1, p2, v1}, Lcom/a/b/d/po;->a(Ljava/lang/Object;Ljava/lang/Iterable;Lcom/a/b/d/pn;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public k()Ljava/util/Collection;
    .registers 4

    .prologue
    .line 465
    iget-object v0, p0, Lcom/a/b/d/qc;->c:Ljava/util/Collection;

    .line 466
    if-nez v0, :cond_18

    .line 467
    iget-object v0, p0, Lcom/a/b/d/qc;->b:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/qc;->a:Lcom/a/b/d/pn;

    .line 1264
    instance-of v1, v0, Ljava/util/Set;

    if-eqz v1, :cond_19

    .line 1265
    check-cast v0, Ljava/util/Set;

    invoke-static {v0, v2}, Lcom/a/b/d/po;->a(Ljava/util/Set;Lcom/a/b/d/pn;)Ljava/util/Set;

    move-result-object v0

    .line 467
    :goto_16
    iput-object v0, p0, Lcom/a/b/d/qc;->c:Ljava/util/Collection;

    .line 469
    :cond_18
    return-object v0

    .line 1267
    :cond_19
    new-instance v1, Lcom/a/b/d/px;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/px;-><init>(Ljava/util/Collection;Lcom/a/b/d/pn;)V

    move-object v0, v1

    goto :goto_16
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 398
    .line 1412
    iget-object v0, p0, Lcom/a/b/d/qc;->b:Lcom/a/b/d/vi;

    .line 398
    return-object v0
.end method
