.class final Lcom/a/b/d/cd;
.super Lcom/a/b/d/agi;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/yi;


# instance fields
.field final synthetic a:Lcom/a/b/d/bx;

.field private final b:Ljava/util/Deque;


# direct methods
.method constructor <init>(Lcom/a/b/d/bx;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 102
    iput-object p1, p0, Lcom/a/b/d/cd;->a:Lcom/a/b/d/bx;

    invoke-direct {p0}, Lcom/a/b/d/agi;-><init>()V

    .line 103
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/a/b/d/cd;->b:Ljava/util/Deque;

    .line 104
    iget-object v0, p0, Lcom/a/b/d/cd;->b:Ljava/util/Deque;

    invoke-interface {v0, p2}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 105
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/a/b/d/cd;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final hasNext()Z
    .registers 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/a/b/d/cd;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final next()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 114
    iget-object v0, p0, Lcom/a/b/d/cd;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeLast()Ljava/lang/Object;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/a/b/d/cd;->b:Ljava/util/Deque;

    iget-object v2, p0, Lcom/a/b/d/cd;->a:Lcom/a/b/d/bx;

    invoke-virtual {v2}, Lcom/a/b/d/bx;->b()Lcom/a/b/b/ci;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/d/bx;->a(Ljava/util/Deque;Lcom/a/b/b/ci;)V

    .line 116
    iget-object v1, p0, Lcom/a/b/d/cd;->b:Ljava/util/Deque;

    iget-object v2, p0, Lcom/a/b/d/cd;->a:Lcom/a/b/d/bx;

    invoke-virtual {v2}, Lcom/a/b/d/bx;->a()Lcom/a/b/b/ci;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/d/bx;->a(Ljava/util/Deque;Lcom/a/b/b/ci;)V

    .line 117
    return-object v0
.end method
