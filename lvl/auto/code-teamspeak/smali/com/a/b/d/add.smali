.class Lcom/a/b/d/add;
.super Lcom/a/b/d/adn;
.source "SourceFile"

# interfaces
.implements Ljava/util/Collection;


# annotations
.annotation build Lcom/a/b/a/d;
.end annotation


# static fields
.field private static final a:J


# direct methods
.method private constructor <init>(Ljava/util/Collection;Ljava/lang/Object;)V
    .registers 3
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/adn;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 110
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Collection;Ljava/lang/Object;B)V
    .registers 4

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/add;-><init>(Ljava/util/Collection;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 119
    iget-object v1, p0, Lcom/a/b/d/add;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 120
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 121
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 126
    iget-object v1, p0, Lcom/a/b/d/add;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 127
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 128
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 114
    invoke-super {p0}, Lcom/a/b/d/adn;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public clear()V
    .registers 3

    .prologue
    .line 133
    iget-object v1, p0, Lcom/a/b/d/add;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 134
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 135
    monitor-exit v1

    return-void

    :catchall_c
    move-exception v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    throw v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 140
    iget-object v1, p0, Lcom/a/b/d/add;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 141
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 142
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 147
    iget-object v1, p0, Lcom/a/b/d/add;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 148
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 149
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public isEmpty()Z
    .registers 3

    .prologue
    .line 154
    iget-object v1, p0, Lcom/a/b/d/add;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 155
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 156
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 166
    iget-object v1, p0, Lcom/a/b/d/add;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 167
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 168
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 173
    iget-object v1, p0, Lcom/a/b/d/add;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 174
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 175
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 180
    iget-object v1, p0, Lcom/a/b/d/add;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 181
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 182
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public size()I
    .registers 3

    .prologue
    .line 187
    iget-object v1, p0, Lcom/a/b/d/add;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 188
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 189
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public toArray()[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 194
    iget-object v1, p0, Lcom/a/b/d/add;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 195
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 196
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 4

    .prologue
    .line 201
    iget-object v1, p0, Lcom/a/b/d/add;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 202
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 203
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method
