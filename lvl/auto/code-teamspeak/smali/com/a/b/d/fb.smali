.class final Lcom/a/b/d/fb;
.super Lcom/a/b/d/ma;
.source "SourceFile"


# instance fields
.field private final b:Lcom/a/b/d/me;


# direct methods
.method constructor <init>(Ljava/util/Comparator;)V
    .registers 3

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/a/b/d/ma;-><init>()V

    .line 34
    invoke-static {p1}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;)Lcom/a/b/d/me;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/fb;->b:Lcom/a/b/d/me;

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method final a([Ljava/lang/Object;I)I
    .registers 3

    .prologue
    .line 107
    return p2
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
    .registers 3

    .prologue
    .line 74
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    return-object p0
.end method

.method final a(I)Lcom/a/b/d/xd;
    .registers 4

    .prologue
    .line 69
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
    .registers 3

    .prologue
    .line 81
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    return-object p0
.end method

.method public final b()Lcom/a/b/d/me;
    .registers 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/a/b/d/fb;->b:Lcom/a/b/d/me;

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 4

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/fb;->b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 88
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 54
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final synthetic d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 4

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/fb;->a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic e_()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 29
    .line 2064
    iget-object v0, p0, Lcom/a/b/d/fb;->b:Lcom/a/b/d/me;

    .line 29
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 93
    instance-of v0, p1, Lcom/a/b/d/xc;

    if-eqz v0, :cond_b

    .line 94
    check-cast p1, Lcom/a/b/d/xc;

    .line 95
    invoke-interface {p1}, Lcom/a/b/d/xc;->isEmpty()Z

    move-result v0

    .line 97
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final f()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 112
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 39
    const/4 v0, 0x0

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method public final i()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1088
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    .line 29
    return-object v0
.end method

.method public final bridge synthetic n()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 29
    .line 3064
    iget-object v0, p0, Lcom/a/b/d/fb;->b:Lcom/a/b/d/me;

    .line 29
    return-object v0
.end method

.method public final bridge synthetic n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 29
    .line 4064
    iget-object v0, p0, Lcom/a/b/d/fb;->b:Lcom/a/b/d/me;

    .line 29
    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method
