.class final Lcom/a/b/d/aci;
.super Lcom/a/b/d/uj;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/abx;


# direct methods
.method private constructor <init>(Lcom/a/b/d/abx;)V
    .registers 2

    .prologue
    .line 763
    iput-object p1, p0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-direct {p0}, Lcom/a/b/d/uj;-><init>()V

    .line 852
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/abx;B)V
    .registers 3

    .prologue
    .line 763
    invoke-direct {p0, p1}, Lcom/a/b/d/aci;-><init>(Lcom/a/b/d/abx;)V

    return-void
.end method

.method private b(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 776
    iget-object v0, p0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v0, p1}, Lcom/a/b/d/abx;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-static {v0, p1}, Lcom/a/b/d/abx;->a(Lcom/a/b/d/abx;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 768
    iget-object v0, p0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v0, p1}, Lcom/a/b/d/abx;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v0, p1}, Lcom/a/b/d/abx;->d(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 780
    new-instance v0, Lcom/a/b/d/acj;

    invoke-direct {v0, p0}, Lcom/a/b/d/acj;-><init>(Lcom/a/b/d/aci;)V

    return-object v0
.end method

.method final c_()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 788
    new-instance v0, Lcom/a/b/d/acl;

    invoke-direct {v0, p0}, Lcom/a/b/d/acl;-><init>(Lcom/a/b/d/aci;)V

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 772
    iget-object v0, p0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v0, p1}, Lcom/a/b/d/abx;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 763
    invoke-virtual {p0, p1}, Lcom/a/b/d/aci;->a(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 784
    iget-object v0, p0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v0}, Lcom/a/b/d/abx;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 763
    .line 1776
    iget-object v0, p0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v0, p1}, Lcom/a/b/d/abx;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-static {v0, p1}, Lcom/a/b/d/abx;->a(Lcom/a/b/d/abx;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    .line 763
    goto :goto_e
.end method
