.class public abstract Lcom/a/b/d/du;
.super Lcom/a/b/d/me;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# instance fields
.field final a:Lcom/a/b/d/ep;


# direct methods
.method constructor <init>(Lcom/a/b/d/ep;)V
    .registers 3

    .prologue
    .line 82
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/me;-><init>(Ljava/util/Comparator;)V

    .line 83
    iput-object p1, p0, Lcom/a/b/d/du;->a:Lcom/a/b/d/ep;

    .line 84
    return-void
.end method

.method public static a(Lcom/a/b/d/yl;Lcom/a/b/d/ep;)Lcom/a/b/d/du;
    .registers 5

    .prologue
    .line 54
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    :try_start_6
    invoke-virtual {p0}, Lcom/a/b/d/yl;->d()Z

    move-result v0

    if-nez v0, :cond_5b

    .line 59
    invoke-virtual {p1}, Lcom/a/b/d/ep;->a()Ljava/lang/Comparable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v0

    .line 61
    :goto_18
    invoke-virtual {p0}, Lcom/a/b/d/yl;->e()Z

    move-result v1

    if-nez v1, :cond_2a

    .line 62
    invoke-virtual {p1}, Lcom/a/b/d/ep;->b()Ljava/lang/Comparable;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;)Lcom/a/b/d/yl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
    :try_end_29
    .catch Ljava/util/NoSuchElementException; {:try_start_6 .. :try_end_29} :catch_4b

    move-result-object v0

    .line 69
    :cond_2a
    invoke-virtual {v0}, Lcom/a/b/d/yl;->f()Z

    move-result v1

    if-nez v1, :cond_42

    iget-object v1, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v1, p1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/ep;)Ljava/lang/Comparable;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v2, p1}, Lcom/a/b/d/dw;->b(Lcom/a/b/d/ep;)Ljava/lang/Comparable;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v1

    if-lez v1, :cond_52

    :cond_42
    const/4 v1, 0x1

    .line 74
    :goto_43
    if-eqz v1, :cond_54

    new-instance v0, Lcom/a/b/d/et;

    invoke-direct {v0, p1}, Lcom/a/b/d/et;-><init>(Lcom/a/b/d/ep;)V

    :goto_4a
    return-object v0

    .line 64
    :catch_4b
    move-exception v0

    .line 65
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 69
    :cond_52
    const/4 v1, 0x0

    goto :goto_43

    .line 74
    :cond_54
    new-instance v1, Lcom/a/b/d/ys;

    invoke-direct {v1, v0, p1}, Lcom/a/b/d/ys;-><init>(Lcom/a/b/d/yl;Lcom/a/b/d/ep;)V

    move-object v0, v1

    goto :goto_4a

    :cond_5b
    move-object v0, p0

    goto :goto_18
.end method

.method private a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/du;
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 99
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    invoke-virtual {p0}, Lcom/a/b/d/du;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_1b

    move v0, v1

    :goto_13
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 102
    invoke-virtual {p0, p1, v1, p2, v2}, Lcom/a/b/d/du;->a(Ljava/lang/Comparable;ZLjava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0

    :cond_1b
    move v0, v2

    .line 101
    goto :goto_13
.end method

.method private b(Ljava/lang/Comparable;)Lcom/a/b/d/du;
    .registers 4

    .prologue
    .line 87
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/du;->a(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Comparable;ZLjava/lang/Comparable;Z)Lcom/a/b/d/du;
    .registers 6
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 111
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    invoke-virtual {p0}, Lcom/a/b/d/du;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_19

    const/4 v0, 0x1

    :goto_11
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 114
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/a/b/d/du;->a(Ljava/lang/Comparable;ZLjava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0

    .line 113
    :cond_19
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private c(Ljava/lang/Comparable;)Lcom/a/b/d/du;
    .registers 4

    .prologue
    .line 118
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/du;->b(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 95
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    invoke-virtual {p0, v0, p2}, Lcom/a/b/d/du;->a(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 126
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    invoke-virtual {p0, v0, p2}, Lcom/a/b/d/du;->b(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method private static k()Lcom/a/b/d/mf;
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 182
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method


# virtual methods
.method public abstract a(Lcom/a/b/d/du;)Lcom/a/b/d/du;
.end method

.method abstract a(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;
.end method

.method abstract a(Ljava/lang/Comparable;ZLjava/lang/Comparable;Z)Lcom/a/b/d/du;
.end method

.method public final synthetic a(Ljava/lang/Object;)Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    invoke-direct {p0, p1}, Lcom/a/b/d/du;->c(Ljava/lang/Comparable;)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    invoke-direct {p0, p1, p2}, Lcom/a/b/d/du;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method synthetic a(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/du;->b(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method bridge synthetic a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 6

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    check-cast p3, Ljava/lang/Comparable;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/a/b/d/du;->a(Ljava/lang/Comparable;ZLjava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Lcom/a/b/d/ce;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;
.end method

.method abstract b(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    invoke-direct {p0, p1}, Lcom/a/b/d/du;->b(Ljava/lang/Comparable;)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method synthetic b(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/du;->a(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 6

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    check-cast p3, Ljava/lang/Comparable;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/a/b/d/du;->b(Ljava/lang/Comparable;ZLjava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    invoke-direct {p0, p1, p2}, Lcom/a/b/d/du;->d(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    invoke-direct {p0, p1, p2}, Lcom/a/b/d/du;->c(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public abstract f_()Lcom/a/b/d/yl;
.end method

.method public synthetic headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    invoke-direct {p0, p1, p2}, Lcom/a/b/d/du;->c(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    invoke-direct {p0, p1}, Lcom/a/b/d/du;->b(Ljava/lang/Comparable;)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    check-cast p3, Ljava/lang/Comparable;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/a/b/d/du;->b(Ljava/lang/Comparable;ZLjava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    invoke-direct {p0, p1, p2}, Lcom/a/b/d/du;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    invoke-direct {p0, p1, p2}, Lcom/a/b/d/du;->d(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Comparable;

    invoke-direct {p0, p1}, Lcom/a/b/d/du;->c(Ljava/lang/Comparable;)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/a/b/d/du;->f_()Lcom/a/b/d/yl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/yl;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
