.class abstract Lcom/a/b/d/bc;
.super Lcom/a/b/d/as;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/abn;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# instance fields
.field final a:Ljava/util/Comparator;
    .annotation runtime Lcom/a/b/d/hv;
    .end annotation
.end field

.field private transient b:Lcom/a/b/d/abn;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 43
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/bc;-><init>(Ljava/util/Comparator;)V

    .line 44
    return-void
.end method

.method constructor <init>(Ljava/util/Comparator;)V
    .registers 3

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/a/b/d/as;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/bc;->a:Ljava/util/Comparator;

    .line 48
    return-void
.end method

.method private o()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 57
    new-instance v0, Lcom/a/b/d/abr;

    invoke-direct {v0, p0}, Lcom/a/b/d/abr;-><init>(Lcom/a/b/d/abn;)V

    return-object v0
.end method

.method private p()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/a/b/d/bc;->m()Lcom/a/b/d/abn;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/xe;->b(Lcom/a/b/d/xc;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private q()Lcom/a/b/d/abn;
    .registers 2

    .prologue
    .line 125
    new-instance v0, Lcom/a/b/d/bd;

    invoke-direct {v0, p0}, Lcom/a/b/d/bd;-><init>(Lcom/a/b/d/bc;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 105
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    invoke-static {p4}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/bc;->c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0, p3, p4}, Lcom/a/b/d/abn;->d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/a/b/d/bc;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method final synthetic e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 36
    .line 2057
    new-instance v0, Lcom/a/b/d/abr;

    invoke-direct {v0, p0}, Lcom/a/b/d/abr;-><init>(Lcom/a/b/d/abn;)V

    .line 36
    return-object v0
.end method

.method public e_()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 52
    invoke-super {p0}, Lcom/a/b/d/as;->n_()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    return-object v0
.end method

.method public h()Lcom/a/b/d/xd;
    .registers 3

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/a/b/d/bc;->b()Ljava/util/Iterator;

    move-result-object v0

    .line 68
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public i()Lcom/a/b/d/xd;
    .registers 3

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/a/b/d/bc;->l()Ljava/util/Iterator;

    move-result-object v0

    .line 74
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public j()Lcom/a/b/d/xd;
    .registers 4

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/a/b/d/bc;->b()Ljava/util/Iterator;

    move-result-object v1

    .line 80
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 81
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 82
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    invoke-static {v2, v0}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    .line 83
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 86
    :goto_1f
    return-object v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method public k()Lcom/a/b/d/xd;
    .registers 4

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/a/b/d/bc;->l()Ljava/util/Iterator;

    move-result-object v1

    .line 92
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 93
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 94
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    invoke-static {v2, v0}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    .line 95
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 98
    :goto_1f
    return-object v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method abstract l()Ljava/util/Iterator;
.end method

.method public m()Lcom/a/b/d/abn;
    .registers 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/a/b/d/bc;->b:Lcom/a/b/d/abn;

    .line 121
    if-nez v0, :cond_b

    .line 1125
    new-instance v0, Lcom/a/b/d/bd;

    invoke-direct {v0, p0}, Lcom/a/b/d/bd;-><init>(Lcom/a/b/d/bc;)V

    .line 121
    iput-object v0, p0, Lcom/a/b/d/bc;->b:Lcom/a/b/d/abn;

    :cond_b
    return-object v0
.end method

.method public final synthetic n()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/a/b/d/bc;->e_()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/a/b/d/bc;->e_()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method
