.class final Lcom/a/b/d/aax;
.super Lcom/a/b/d/mi;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field final a:Ljava/lang/Object;

.field final b:Ljava/lang/Object;

.field final c:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/a/b/d/adw;)V
    .registers 5

    .prologue
    .line 43
    invoke-interface {p1}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Lcom/a/b/d/adw;->c()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/a/b/d/aax;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 44
    return-void
.end method

.method constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/a/b/d/mi;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aax;->a:Ljava/lang/Object;

    .line 38
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aax;->b:Ljava/lang/Object;

    .line 39
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aax;->c:Ljava/lang/Object;

    .line 40
    return-void
.end method


# virtual methods
.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/a/b/d/aax;->f(Ljava/lang/Object;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/Object;)Lcom/a/b/d/jt;
    .registers 4

    .prologue
    .line 47
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    invoke-virtual {p0, p1}, Lcom/a/b/d/aax;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/a/b/d/aax;->a:Ljava/lang/Object;

    iget-object v1, p0, Lcom/a/b/d/aax;->c:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/a/b/d/jt;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;

    move-result-object v0

    :goto_11
    return-object v0

    :cond_12
    invoke-static {}, Lcom/a/b/d/jt;->k()Lcom/a/b/d/jt;

    move-result-object v0

    goto :goto_11
.end method

.method final synthetic f()Ljava/util/Set;
    .registers 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/a/b/d/aax;->q()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method final synthetic i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 30
    .line 1074
    iget-object v0, p0, Lcom/a/b/d/aax;->c:Ljava/lang/Object;

    invoke-static {v0}, Lcom/a/b/d/lo;->d(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 30
    return-object v0
.end method

.method public final k()I
    .registers 2

    .prologue
    .line 64
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic l()Ljava/util/Map;
    .registers 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/a/b/d/aax;->n()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic m()Ljava/util/Map;
    .registers 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/a/b/d/aax;->o()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/a/b/d/jt;
    .registers 4

    .prologue
    .line 54
    iget-object v0, p0, Lcom/a/b/d/aax;->b:Ljava/lang/Object;

    iget-object v1, p0, Lcom/a/b/d/aax;->a:Ljava/lang/Object;

    iget-object v2, p0, Lcom/a/b/d/aax;->c:Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/a/b/d/jt;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/jt;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/a/b/d/jt;
    .registers 4

    .prologue
    .line 59
    iget-object v0, p0, Lcom/a/b/d/aax;->a:Ljava/lang/Object;

    iget-object v1, p0, Lcom/a/b/d/aax;->b:Ljava/lang/Object;

    iget-object v2, p0, Lcom/a/b/d/aax;->c:Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/a/b/d/jt;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/jt;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method final q()Lcom/a/b/d/lo;
    .registers 4

    .prologue
    .line 69
    iget-object v0, p0, Lcom/a/b/d/aax;->a:Ljava/lang/Object;

    iget-object v1, p0, Lcom/a/b/d/aax;->b:Ljava/lang/Object;

    iget-object v2, p0, Lcom/a/b/d/aax;->c:Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/a/b/d/aax;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/lo;->d(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method final r()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/a/b/d/aax;->c:Ljava/lang/Object;

    invoke-static {v0}, Lcom/a/b/d/lo;->d(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method
