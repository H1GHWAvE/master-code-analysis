.class Lcom/a/b/d/jz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final c:J


# instance fields
.field private final a:[Ljava/lang/Object;

.field private final b:[Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/a/b/d/jt;)V
    .registers 7

    .prologue
    .line 530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 531
    invoke-virtual {p1}, Lcom/a/b/d/jt;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/a/b/d/jz;->a:[Ljava/lang/Object;

    .line 532
    invoke-virtual {p1}, Lcom/a/b/d/jt;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/a/b/d/jz;->b:[Ljava/lang/Object;

    .line 533
    const/4 v0, 0x0

    .line 534
    invoke-virtual {p1}, Lcom/a/b/d/jt;->e()Lcom/a/b/d/lo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 535
    iget-object v3, p0, Lcom/a/b/d/jz;->a:[Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v1

    .line 536
    iget-object v3, p0, Lcom/a/b/d/jz;->b:[Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v3, v1

    .line 537
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 538
    goto :goto_1d

    .line 539
    :cond_3d
    return-void
.end method


# virtual methods
.method a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 541
    new-instance v0, Lcom/a/b/d/ju;

    invoke-direct {v0}, Lcom/a/b/d/ju;-><init>()V

    .line 542
    invoke-virtual {p0, v0}, Lcom/a/b/d/jz;->a(Lcom/a/b/d/ju;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final a(Lcom/a/b/d/ju;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 545
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/a/b/d/jz;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_14

    .line 546
    iget-object v1, p0, Lcom/a/b/d/jz;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/a/b/d/jz;->b:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 545
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 548
    :cond_14
    invoke-virtual {p1}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method
