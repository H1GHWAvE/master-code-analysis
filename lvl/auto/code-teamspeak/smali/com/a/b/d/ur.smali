.class Lcom/a/b/d/ur;
.super Lcom/a/b/d/uj;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field final b:Lcom/a/b/d/tv;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/a/b/d/tv;)V
    .registers 4

    .prologue
    .line 1883
    invoke-direct {p0}, Lcom/a/b/d/uj;-><init>()V

    .line 1884
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/a/b/d/ur;->a:Ljava/util/Map;

    .line 1885
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/tv;

    iput-object v0, p0, Lcom/a/b/d/ur;->b:Lcom/a/b/d/tv;

    .line 1886
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1923
    new-instance v0, Lcom/a/b/d/us;

    invoke-direct {v0, p0}, Lcom/a/b/d/us;-><init>(Lcom/a/b/d/ur;)V

    return-object v0
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 1914
    iget-object v0, p0, Lcom/a/b/d/ur;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1915
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1893
    iget-object v0, p0, Lcom/a/b/d/ur;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1899
    iget-object v0, p0, Lcom/a/b/d/ur;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1900
    if-nez v0, :cond_10

    iget-object v1, p0, Lcom/a/b/d/ur;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    :cond_10
    iget-object v1, p0, Lcom/a/b/d/ur;->b:Lcom/a/b/d/tv;

    invoke-interface {v1, p1, v0}, Lcom/a/b/d/tv;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_16
    return-object v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1918
    iget-object v0, p0, Lcom/a/b/d/ur;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1908
    iget-object v0, p0, Lcom/a/b/d/ur;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/a/b/d/ur;->b:Lcom/a/b/d/tv;

    iget-object v1, p0, Lcom/a/b/d/ur;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/a/b/d/tv;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_14
    return-object v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public size()I
    .registers 2

    .prologue
    .line 1889
    iget-object v0, p0, Lcom/a/b/d/ur;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
