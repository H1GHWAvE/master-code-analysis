.class final Lcom/a/b/d/aeb;
.super Lcom/a/b/d/bf;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/adv;

.field final b:Lcom/a/b/b/bj;


# direct methods
.method constructor <init>(Lcom/a/b/d/adv;Lcom/a/b/b/bj;)V
    .registers 4

    .prologue
    .line 343
    invoke-direct {p0}, Lcom/a/b/d/bf;-><init>()V

    .line 344
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/adv;

    iput-object v0, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    .line 345
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/bj;

    iput-object v0, p0, Lcom/a/b/d/aeb;->b:Lcom/a/b/b/bj;

    .line 346
    return-void
.end method

.method private n()Lcom/a/b/b/bj;
    .registers 2

    .prologue
    .line 390
    new-instance v0, Lcom/a/b/d/aec;

    invoke-direct {v0, p0}, Lcom/a/b/d/aec;-><init>(Lcom/a/b/d/aeb;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 368
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 405
    iget-object v0, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v0}, Lcom/a/b/d/adv;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/a/b/d/adv;)V
    .registers 3

    .prologue
    .line 373
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 349
    iget-object v0, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/adv;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 355
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/aeb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/a/b/d/aeb;->b:Lcom/a/b/b/bj;

    iget-object v1, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v1, p1, p2}, Lcom/a/b/d/adv;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_12
    return-object v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public final b()Ljava/util/Set;
    .registers 2

    .prologue
    .line 409
    iget-object v0, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v0}, Lcom/a/b/d/adv;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 377
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/aeb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/a/b/d/aeb;->b:Lcom/a/b/b/bj;

    iget-object v1, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v1, p1, p2}, Lcom/a/b/d/adv;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_12
    return-object v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public final d(Ljava/lang/Object;)Ljava/util/Map;
    .registers 4

    .prologue
    .line 386
    iget-object v0, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v0, p1}, Lcom/a/b/d/adv;->d(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aeb;->b:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 364
    iget-object v0, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v0}, Lcom/a/b/d/adv;->d()V

    .line 365
    return-void
.end method

.method public final e(Ljava/lang/Object;)Ljava/util/Map;
    .registers 4

    .prologue
    .line 382
    iget-object v0, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v0, p1}, Lcom/a/b/d/adv;->e(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aeb;->b:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method final g()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 401
    iget-object v0, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v0}, Lcom/a/b/d/adv;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1390
    new-instance v1, Lcom/a/b/d/aec;

    invoke-direct {v1, p0}, Lcom/a/b/d/aec;-><init>(Lcom/a/b/d/aeb;)V

    .line 401
    invoke-static {v0, v1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Lcom/a/b/b/bj;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method final i()Ljava/util/Collection;
    .registers 3

    .prologue
    .line 414
    iget-object v0, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v0}, Lcom/a/b/d/adv;->h()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aeb;->b:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Lcom/a/b/b/bj;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final k()I
    .registers 2

    .prologue
    .line 360
    iget-object v0, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v0}, Lcom/a/b/d/adv;->k()I

    move-result v0

    return v0
.end method

.method public final l()Ljava/util/Map;
    .registers 3

    .prologue
    .line 428
    new-instance v0, Lcom/a/b/d/aee;

    invoke-direct {v0, p0}, Lcom/a/b/d/aee;-><init>(Lcom/a/b/d/aeb;)V

    .line 434
    iget-object v1, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v1}, Lcom/a/b/d/adv;->l()Ljava/util/Map;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/util/Map;
    .registers 3

    .prologue
    .line 418
    new-instance v0, Lcom/a/b/d/aed;

    invoke-direct {v0, p0}, Lcom/a/b/d/aed;-><init>(Lcom/a/b/d/aeb;)V

    .line 424
    iget-object v1, p0, Lcom/a/b/d/aeb;->a:Lcom/a/b/d/adv;

    invoke-interface {v1}, Lcom/a/b/d/adv;->m()Ljava/util/Map;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
