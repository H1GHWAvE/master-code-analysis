.class abstract Lcom/a/b/d/ht;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
    b = true
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field a:Lcom/a/b/d/qw;
    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Lcom/a/b/d/ht;
    .annotation build Lcom/a/b/a/c;
        a = "java.lang.ref.WeakReference"
    .end annotation
.end method

.method public abstract a(I)Lcom/a/b/d/ht;
.end method

.method abstract a(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/d/ht;
.end method

.method abstract a(Lcom/a/b/b/au;)Lcom/a/b/d/ht;
    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation
.end method

.method abstract a(Lcom/a/b/b/bj;)Ljava/util/concurrent/ConcurrentMap;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract b()Lcom/a/b/d/ht;
    .annotation build Lcom/a/b/a/c;
        a = "java.lang.ref.WeakReference"
    .end annotation
.end method

.method abstract b(I)Lcom/a/b/d/ht;
.end method

.method abstract b(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/d/ht;
    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation
.end method

.method public abstract c()Lcom/a/b/d/ht;
    .annotation build Lcom/a/b/a/c;
        a = "java.lang.ref.SoftReference"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract c(I)Lcom/a/b/d/ht;
.end method

.method final d()Lcom/a/b/d/qw;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/a/b/d/ht;->a:Lcom/a/b/d/qw;

    sget-object v1, Lcom/a/b/d/hu;->a:Lcom/a/b/d/hu;

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/qw;

    return-object v0
.end method

.method public abstract e()Ljava/util/concurrent/ConcurrentMap;
.end method

.method abstract f()Lcom/a/b/d/qy;
    .annotation build Lcom/a/b/a/c;
        a = "MapMakerInternalMap"
    .end annotation
.end method
