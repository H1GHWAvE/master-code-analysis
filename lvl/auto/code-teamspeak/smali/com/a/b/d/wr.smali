.class final Lcom/a/b/d/wr;
.super Lcom/a/b/d/an;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/aac;
.implements Ljava/io/Serializable;


# static fields
.field private static final b:J = 0x6cdfd44a398c560fL


# instance fields
.field final a:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/util/Map;)V
    .registers 3

    .prologue
    .line 931
    invoke-direct {p0}, Lcom/a/b/d/an;-><init>()V

    .line 932
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    .line 933
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 957
    new-instance v0, Lcom/a/b/d/ws;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ws;-><init>(Lcom/a/b/d/wr;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 4

    .prologue
    .line 1008
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/a/b/d/vi;)Z
    .registers 3

    .prologue
    .line 1003
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 993
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 927
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/wr;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 4

    .prologue
    .line 1018
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 1019
    iget-object v1, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 1023
    :goto_e
    return-object v0

    .line 1022
    :cond_f
    iget-object v1, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_e
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 952
    iget-object v0, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 927
    invoke-virtual {p0, p1}, Lcom/a/b/d/wr;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 4

    .prologue
    .line 998
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 1013
    iget-object v0, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 927
    invoke-virtual {p0, p1}, Lcom/a/b/d/wr;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .registers 2

    .prologue
    .line 937
    iget-object v0, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public final f(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 942
    iget-object v0, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1029
    return-void
.end method

.method public final g(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 947
    iget-object v0, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 1057
    iget-object v0, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 927
    invoke-virtual {p0}, Lcom/a/b/d/wr;->u()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method final l()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method final m()Ljava/util/Map;
    .registers 2

    .prologue
    .line 1053
    new-instance v0, Lcom/a/b/d/wf;

    invoke-direct {v0, p0}, Lcom/a/b/d/wf;-><init>(Lcom/a/b/d/vi;)V

    return-object v0
.end method

.method public final p()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1033
    iget-object v0, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1043
    iget-object v0, p0, Lcom/a/b/d/wr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
