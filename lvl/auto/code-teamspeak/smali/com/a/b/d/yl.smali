.class public final Lcom/a/b/d/yl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/co;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field static final a:Lcom/a/b/d/yd;

.field private static final d:Lcom/a/b/b/bj;

.field private static final e:Lcom/a/b/b/bj;

.field private static final f:Lcom/a/b/d/yl;

.field private static final g:J


# instance fields
.field final b:Lcom/a/b/d/dw;

.field final c:Lcom/a/b/d/dw;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 117
    new-instance v0, Lcom/a/b/d/ym;

    invoke-direct {v0}, Lcom/a/b/d/ym;-><init>()V

    sput-object v0, Lcom/a/b/d/yl;->d:Lcom/a/b/b/bj;

    .line 129
    new-instance v0, Lcom/a/b/d/yn;

    invoke-direct {v0}, Lcom/a/b/d/yn;-><init>()V

    sput-object v0, Lcom/a/b/d/yl;->e:Lcom/a/b/b/bj;

    .line 141
    new-instance v0, Lcom/a/b/d/yo;

    invoke-direct {v0}, Lcom/a/b/d/yo;-><init>()V

    sput-object v0, Lcom/a/b/d/yl;->a:Lcom/a/b/d/yd;

    .line 305
    new-instance v0, Lcom/a/b/d/yl;

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/yl;-><init>(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)V

    sput-object v0, Lcom/a/b/d/yl;->f:Lcom/a/b/d/yl;

    return-void
.end method

.method private constructor <init>(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)V
    .registers 7

    .prologue
    .line 360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361
    invoke-virtual {p1, p2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    if-gtz v0, :cond_15

    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v0

    if-eq p1, v0, :cond_15

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v0

    if-ne p2, v0, :cond_35

    .line 363
    :cond_15
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid range: "

    invoke-static {p1, p2}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2f

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2b
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2f
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2b

    .line 365
    :cond_35
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dw;

    iput-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    .line 366
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dw;

    iput-object v0, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 367
    return-void
.end method

.method static a()Lcom/a/b/b/bj;
    .registers 1

    .prologue
    .line 126
    sget-object v0, Lcom/a/b/d/yl;->d:Lcom/a/b/b/bj;

    return-object v0
.end method

.method static a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;
    .registers 3

    .prologue
    .line 153
    new-instance v0, Lcom/a/b/d/yl;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/yl;-><init>(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)V

    return-object v0
.end method

.method private a(Lcom/a/b/d/ep;)Lcom/a/b/d/yl;
    .registers 5

    .prologue
    .line 623
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0, p1}, Lcom/a/b/d/dw;->c(Lcom/a/b/d/ep;)Lcom/a/b/d/dw;

    move-result-object v0

    .line 625
    iget-object v1, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v1, p1}, Lcom/a/b/d/dw;->c(Lcom/a/b/d/ep;)Lcom/a/b/d/dw;

    move-result-object v1

    .line 626
    iget-object v2, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    if-ne v0, v2, :cond_18

    iget-object v2, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    if-ne v1, v2, :cond_18

    :goto_17
    return-object p0

    :cond_18
    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object p0

    goto :goto_17
.end method

.method public static a(Ljava/lang/Comparable;)Lcom/a/b/d/yl;
    .registers 3

    .prologue
    .line 246
    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v0

    invoke-static {p0}, Lcom/a/b/d/dw;->c(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;
    .registers 4

    .prologue
    .line 257
    sget-object v0, Lcom/a/b/d/yp;->a:[I

    invoke-virtual {p1}, Lcom/a/b/d/ce;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_24

    .line 263
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1236
    :pswitch_11
    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v0

    invoke-static {p0}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    .line 261
    :goto_1d
    return-object v0

    :pswitch_1e
    invoke-static {p0}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;)Lcom/a/b/d/yl;

    move-result-object v0

    goto :goto_1d

    .line 257
    nop

    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_11
        :pswitch_1e
    .end packed-switch
.end method

.method public static a(Ljava/lang/Comparable;Lcom/a/b/d/ce;Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;
    .registers 6

    .prologue
    .line 217
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-ne p1, v0, :cond_1b

    invoke-static {p0}, Lcom/a/b/d/dw;->c(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v0

    .line 223
    :goto_e
    sget-object v1, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-ne p3, v1, :cond_20

    invoke-static {p2}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v1

    .line 226
    :goto_16
    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0

    .line 220
    :cond_1b
    invoke-static {p0}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v0

    goto :goto_e

    .line 223
    :cond_20
    invoke-static {p2}, Lcom/a/b/d/dw;->c(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v1

    goto :goto_16
.end method

.method public static a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/yl;
    .registers 4

    .prologue
    .line 177
    invoke-static {p0}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v0

    invoke-static {p1}, Lcom/a/b/d/dw;->c(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/yl;
    .registers 6

    .prologue
    .line 342
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    instance-of v0, p0, Lcom/a/b/d/du;

    if-eqz v0, :cond_e

    .line 344
    check-cast p0, Lcom/a/b/d/du;

    invoke-virtual {p0}, Lcom/a/b/d/du;->f_()Lcom/a/b/d/yl;

    move-result-object v0

    .line 354
    :goto_d
    return-object v0

    .line 346
    :cond_e
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 347
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    move-object v2, v0

    move-object v1, v0

    .line 349
    :goto_1e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 350
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 351
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v4

    invoke-virtual {v4, v1, v0}, Lcom/a/b/d/yd;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    .line 352
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v4

    invoke-virtual {v4, v2, v0}, Lcom/a/b/d/yd;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    move-object v2, v0

    .line 353
    goto :goto_1e

    .line 354
    :cond_44
    invoke-static {v1, v2}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/yl;

    move-result-object v0

    goto :goto_d
.end method

.method static b(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    .registers 3

    .prologue
    .line 683
    invoke-interface {p0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static b()Lcom/a/b/b/bj;
    .registers 1

    .prologue
    .line 138
    sget-object v0, Lcom/a/b/d/yl;->e:Lcom/a/b/b/bj;

    return-object v0
.end method

.method public static b(Ljava/lang/Comparable;)Lcom/a/b/d/yl;
    .registers 3

    .prologue
    .line 284
    invoke-static {p0}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v0

    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;
    .registers 4

    .prologue
    .line 295
    sget-object v0, Lcom/a/b/d/yp;->a:[I

    invoke-virtual {p1}, Lcom/a/b/d/ce;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_24

    .line 301
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1274
    :pswitch_11
    invoke-static {p0}, Lcom/a/b/d/dw;->c(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v0

    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    .line 299
    :goto_1d
    return-object v0

    :pswitch_1e
    invoke-static {p0}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;)Lcom/a/b/d/yl;

    move-result-object v0

    goto :goto_1d

    .line 295
    nop

    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_11
        :pswitch_1e
    .end packed-switch
.end method

.method private static b(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 659
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 660
    invoke-virtual {p0, v0}, Lcom/a/b/d/dw;->a(Ljava/lang/StringBuilder;)V

    .line 661
    const/16 v1, 0x2025

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 662
    invoke-virtual {p1, v0}, Lcom/a/b/d/dw;->b(Ljava/lang/StringBuilder;)V

    .line 663
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Iterable;)Z
    .registers 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 464
    .line 1998
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_11

    move-object v0, p1

    .line 1999
    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    .line 464
    :goto_d
    if-eqz v0, :cond_1f

    move v0, v2

    .line 482
    :goto_10
    return v0

    .line 2001
    :cond_11
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1d

    move v0, v2

    goto :goto_d

    :cond_1d
    move v0, v3

    goto :goto_d

    .line 469
    :cond_1f
    instance-of v0, p1, Ljava/util/SortedSet;

    if-eqz v0, :cond_52

    move-object v0, p1

    .line 2670
    check-cast v0, Ljava/util/SortedSet;

    .line 471
    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v1

    .line 472
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_36

    if-nez v1, :cond_52

    .line 473
    :cond_36
    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    invoke-virtual {p0, v1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v1

    if-eqz v1, :cond_50

    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    invoke-virtual {p0, v0}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_50

    move v0, v2

    goto :goto_10

    :cond_50
    move v0, v3

    goto :goto_10

    .line 477
    :cond_52
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_56
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 478
    invoke-virtual {p0, v0}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    if-nez v0, :cond_56

    move v0, v3

    .line 479
    goto :goto_10

    :cond_6a
    move v0, v2

    .line 482
    goto :goto_10
.end method

.method public static c()Lcom/a/b/d/yl;
    .registers 1

    .prologue
    .line 315
    sget-object v0, Lcom/a/b/d/yl;->f:Lcom/a/b/d/yl;

    return-object v0
.end method

.method private static c(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/yl;
    .registers 4

    .prologue
    .line 165
    invoke-static {p0}, Lcom/a/b/d/dw;->c(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v0

    invoke-static {p1}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/lang/Iterable;)Ljava/util/SortedSet;
    .registers 1

    .prologue
    .line 670
    check-cast p0, Ljava/util/SortedSet;

    return-object p0
.end method

.method private d(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
    .registers 5

    .prologue
    .line 585
    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    .line 586
    iget-object v1, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v2, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v1, v2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v2

    .line 587
    if-gtz v0, :cond_15

    if-ltz v2, :cond_15

    .line 594
    :goto_14
    return-object p0

    .line 589
    :cond_15
    if-ltz v0, :cond_1b

    if-gtz v2, :cond_1b

    move-object p0, p1

    .line 590
    goto :goto_14

    .line 592
    :cond_1b
    if-gtz v0, :cond_29

    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    move-object v1, v0

    .line 593
    :goto_20
    if-ltz v2, :cond_2d

    iget-object v0, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 594
    :goto_24
    invoke-static {v1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object p0

    goto :goto_14

    .line 592
    :cond_29
    iget-object v0, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    move-object v1, v0

    goto :goto_20

    .line 593
    :cond_2d
    iget-object v0, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    goto :goto_24
.end method

.method private static d(Ljava/lang/Comparable;)Lcom/a/b/d/yl;
    .registers 3

    .prologue
    .line 236
    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v0

    invoke-static {p0}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/yl;
    .registers 4

    .prologue
    .line 190
    invoke-static {p0}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v0

    invoke-static {p1}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method private static e(Ljava/lang/Comparable;)Lcom/a/b/d/yl;
    .registers 3

    .prologue
    .line 274
    invoke-static {p0}, Lcom/a/b/d/dw;->c(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v0

    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method private static e(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/yl;
    .registers 4

    .prologue
    .line 203
    invoke-static {p0}, Lcom/a/b/d/dw;->c(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v0

    invoke-static {p1}, Lcom/a/b/d/dw;->c(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method private static f(Ljava/lang/Comparable;)Lcom/a/b/d/yl;
    .registers 2

    .prologue
    .line 326
    invoke-static {p0, p0}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method private g()Ljava/lang/Comparable;
    .registers 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0}, Lcom/a/b/d/dw;->c()Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method private g(Ljava/lang/Comparable;)Z
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    return v0
.end method

.method private h()Lcom/a/b/d/ce;
    .registers 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0}, Lcom/a/b/d/dw;->a()Lcom/a/b/d/ce;

    move-result-object v0

    return-object v0
.end method

.method private i()Ljava/lang/Comparable;
    .registers 2

    .prologue
    .line 411
    iget-object v0, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0}, Lcom/a/b/d/dw;->c()Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method private j()Lcom/a/b/d/ce;
    .registers 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0}, Lcom/a/b/d/dw;->b()Lcom/a/b/d/ce;

    move-result-object v0

    return-object v0
.end method

.method private k()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 674
    sget-object v0, Lcom/a/b/d/yl;->f:Lcom/a/b/d/yl;

    invoke-virtual {p0, v0}, Lcom/a/b/d/yl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 3315
    sget-object p0, Lcom/a/b/d/yl;->f:Lcom/a/b/d/yl;

    .line 677
    :cond_a
    return-object p0
.end method


# virtual methods
.method public final a(Lcom/a/b/d/yl;)Z
    .registers 4

    .prologue
    .line 510
    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    if-gtz v0, :cond_16

    iget-object v0, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v1, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    if-ltz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 113
    check-cast p1, Ljava/lang/Comparable;

    .line 3456
    invoke-virtual {p0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    .line 113
    return v0
.end method

.method public final b(Lcom/a/b/d/yl;)Z
    .registers 4

    .prologue
    .line 539
    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v1, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    if-gtz v0, :cond_16

    iget-object v0, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v1, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    if-gtz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public final c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
    .registers 5

    .prologue
    .line 560
    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    .line 561
    iget-object v1, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v2, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v1, v2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v2

    .line 562
    if-ltz v0, :cond_15

    if-gtz v2, :cond_15

    .line 569
    :goto_14
    return-object p0

    .line 564
    :cond_15
    if-gtz v0, :cond_1b

    if-ltz v2, :cond_1b

    move-object p0, p1

    .line 565
    goto :goto_14

    .line 567
    :cond_1b
    if-ltz v0, :cond_29

    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    move-object v1, v0

    .line 568
    :goto_20
    if-gtz v2, :cond_2d

    iget-object v0, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 569
    :goto_24
    invoke-static {v1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object p0

    goto :goto_14

    .line 567
    :cond_29
    iget-object v0, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    move-object v1, v0

    goto :goto_20

    .line 568
    :cond_2d
    iget-object v0, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    goto :goto_24
.end method

.method public final c(Ljava/lang/Comparable;)Z
    .registers 3

    .prologue
    .line 444
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0, p1}, Lcom/a/b/d/dw;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0, p1}, Lcom/a/b/d/dw;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-nez v0, :cond_15

    const/4 v0, 0x1

    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public final d()Z
    .registers 3

    .prologue
    .line 373
    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v1

    if-eq v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final e()Z
    .registers 3

    .prologue
    .line 401
    iget-object v0, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v1

    if-eq v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 637
    instance-of v1, p1, Lcom/a/b/d/yl;

    if-eqz v1, :cond_1c

    .line 638
    check-cast p1, Lcom/a/b/d/yl;

    .line 639
    iget-object v1, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v2, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v1, v2}, Lcom/a/b/d/dw;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v2, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v1, v2}, Lcom/a/b/d/dw;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const/4 v0, 0x1

    .line 642
    :cond_1c
    return v0
.end method

.method public final f()Z
    .registers 3

    .prologue
    .line 435
    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v1, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 647
    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 655
    iget-object v0, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v1, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
