.class abstract Lcom/a/b/d/m;
.super Lcom/a/b/d/n;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/ou;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:J = 0x5b6e85fc5d362ea5L


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .registers 2

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/a/b/d/n;-><init>(Ljava/util/Map;)V

    .line 47
    return-void
.end method

.method private static t()Ljava/util/List;
    .registers 1

    .prologue
    .line 53
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abstract a()Ljava/util/List;
.end method

.method public a(Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/a/b/d/n;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 89
    invoke-super {p0, p1, p2}, Lcom/a/b/d/n;->b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 100
    invoke-super {p0, p1, p2}, Lcom/a/b/d/n;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/m;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/a/b/d/n;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 110
    invoke-super {p0}, Lcom/a/b/d/n;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method synthetic c()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/a/b/d/m;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/a/b/d/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method final bridge synthetic d()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1053
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    .line 36
    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/a/b/d/m;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/a/b/d/n;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
