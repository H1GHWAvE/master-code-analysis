.class public abstract Lcom/a/b/d/vj;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:I = 0x8


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/a/b/d/vj;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;)Lcom/a/b/d/vu;
    .registers 2

    .prologue
    .line 178
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    new-instance v0, Lcom/a/b/d/vn;

    invoke-direct {v0, p0}, Lcom/a/b/d/vn;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method private static a(Ljava/util/Comparator;)Lcom/a/b/d/vu;
    .registers 2

    .prologue
    .line 164
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    new-instance v0, Lcom/a/b/d/vm;

    invoke-direct {v0, p0}, Lcom/a/b/d/vm;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private static b()Lcom/a/b/d/vu;
    .registers 2

    .prologue
    .line 1095
    const/16 v0, 0x8

    const-string v1, "expectedKeys"

    invoke-static {v0, v1}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 1096
    new-instance v0, Lcom/a/b/d/vk;

    invoke-direct {v0}, Lcom/a/b/d/vk;-><init>()V

    .line 85
    return-object v0
.end method

.method private static c()Lcom/a/b/d/vu;
    .registers 2

    .prologue
    .line 95
    const/16 v0, 0x8

    const-string v1, "expectedKeys"

    invoke-static {v0, v1}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 96
    new-instance v0, Lcom/a/b/d/vk;

    invoke-direct {v0}, Lcom/a/b/d/vk;-><init>()V

    return-object v0
.end method

.method private static d()Lcom/a/b/d/vu;
    .registers 2

    .prologue
    .line 1126
    const/16 v0, 0x8

    const-string v1, "expectedKeys"

    invoke-static {v0, v1}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 1127
    new-instance v0, Lcom/a/b/d/vl;

    invoke-direct {v0}, Lcom/a/b/d/vl;-><init>()V

    .line 113
    return-object v0
.end method

.method private static e()Lcom/a/b/d/vu;
    .registers 2

    .prologue
    .line 126
    const/16 v0, 0x8

    const-string v1, "expectedKeys"

    invoke-static {v0, v1}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 127
    new-instance v0, Lcom/a/b/d/vl;

    invoke-direct {v0}, Lcom/a/b/d/vl;-><init>()V

    return-object v0
.end method

.method private static f()Lcom/a/b/d/vu;
    .registers 2

    .prologue
    .line 147
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    .line 1164
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1165
    new-instance v1, Lcom/a/b/d/vm;

    invoke-direct {v1, v0}, Lcom/a/b/d/vm;-><init>(Ljava/util/Comparator;)V

    .line 147
    return-object v1
.end method


# virtual methods
.method public abstract a()Lcom/a/b/d/vi;
.end method

.method public a(Lcom/a/b/d/vi;)Lcom/a/b/d/vi;
    .registers 3

    .prologue
    .line 433
    invoke-virtual {p0}, Lcom/a/b/d/vj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    .line 434
    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->a(Lcom/a/b/d/vi;)Z

    .line 435
    return-object v0
.end method
