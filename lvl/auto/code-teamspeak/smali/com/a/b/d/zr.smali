.class abstract Lcom/a/b/d/zr;
.super Lcom/a/b/d/mi;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/a/b/d/mi;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/zr;
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 141
    invoke-static {p0, v0, v0}, Lcom/a/b/d/zr;->a(Ljava/lang/Iterable;Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/zr;

    move-result-object v0

    return-object v0
.end method

.method static final a(Ljava/lang/Iterable;Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/zr;
    .registers 13
    .param p1    # Ljava/util/Comparator;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Comparator;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 152
    invoke-static {}, Lcom/a/b/d/lo;->i()Lcom/a/b/d/lp;

    move-result-object v1

    .line 153
    invoke-static {}, Lcom/a/b/d/lo;->i()Lcom/a/b/d/lp;

    move-result-object v2

    .line 154
    invoke-static {p0}, Lcom/a/b/d/jl;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v3

    .line 155
    invoke-virtual {v3}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_10
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/adw;

    .line 156
    invoke-interface {v0}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    .line 157
    invoke-interface {v0}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    goto :goto_10

    .line 160
    :cond_2b
    invoke-virtual {v1}, Lcom/a/b/d/lp;->b()Lcom/a/b/d/lo;

    move-result-object v0

    .line 161
    if-eqz p1, :cond_3c

    .line 162
    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 163
    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 164
    invoke-static {v0}, Lcom/a/b/d/lo;->a(Ljava/util/Collection;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 166
    :cond_3c
    invoke-virtual {v2}, Lcom/a/b/d/lp;->b()Lcom/a/b/d/lo;

    move-result-object v1

    .line 167
    if-eqz p2, :cond_4d

    .line 168
    invoke-static {v1}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 169
    invoke-static {v1, p2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 170
    invoke-static {v1}, Lcom/a/b/d/lo;->a(Ljava/util/Collection;)Lcom/a/b/d/lo;

    move-result-object v1

    .line 175
    :cond_4d
    invoke-virtual {v3}, Lcom/a/b/d/jl;->size()I

    move-result v2

    int-to-long v4, v2

    invoke-virtual {v0}, Lcom/a/b/d/lo;->size()I

    move-result v2

    int-to-long v6, v2

    invoke-virtual {v1}, Lcom/a/b/d/lo;->size()I

    move-result v2

    int-to-long v8, v2

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x2

    div-long/2addr v6, v8

    cmp-long v2, v4, v6

    if-lez v2, :cond_6b

    new-instance v2, Lcom/a/b/d/ec;

    invoke-direct {v2, v3, v0, v1}, Lcom/a/b/d/ec;-><init>(Lcom/a/b/d/jl;Lcom/a/b/d/lo;Lcom/a/b/d/lo;)V

    move-object v0, v2

    :goto_6a
    return-object v0

    :cond_6b
    new-instance v2, Lcom/a/b/d/abt;

    invoke-direct {v2, v3, v0, v1}, Lcom/a/b/d/abt;-><init>(Lcom/a/b/d/jl;Lcom/a/b/d/lo;Lcom/a/b/d/lo;)V

    move-object v0, v2

    goto :goto_6a
.end method

.method static a(Ljava/util/List;Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/zr;
    .registers 4
    .param p1    # Ljava/util/Comparator;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Comparator;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 114
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    if-nez p1, :cond_7

    if-eqz p2, :cond_f

    .line 123
    :cond_7
    new-instance v0, Lcom/a/b/d/zs;

    invoke-direct {v0, p1, p2}, Lcom/a/b/d/zs;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    .line 134
    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 136
    :cond_f
    invoke-static {p0, p1, p2}, Lcom/a/b/d/zr;->a(Ljava/lang/Iterable;Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/zr;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abstract a(I)Lcom/a/b/d/adw;
.end method

.method abstract b(I)Ljava/lang/Object;
.end method

.method final synthetic f()Ljava/util/Set;
    .registers 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/a/b/d/zr;->q()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method final synthetic i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/a/b/d/zr;->r()Lcom/a/b/d/iz;

    move-result-object v0

    return-object v0
.end method

.method final q()Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/a/b/d/zr;->c()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Lcom/a/b/d/zt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/zt;-><init>(Lcom/a/b/d/zr;B)V

    goto :goto_a
.end method

.method final r()Lcom/a/b/d/iz;
    .registers 3

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/a/b/d/zr;->c()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Lcom/a/b/d/zv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/zv;-><init>(Lcom/a/b/d/zr;B)V

    goto :goto_a
.end method
