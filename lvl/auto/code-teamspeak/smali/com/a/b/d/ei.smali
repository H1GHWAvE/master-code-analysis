.class final Lcom/a/b/d/ei;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/eh;

.field private b:I

.field private final c:I


# direct methods
.method constructor <init>(Lcom/a/b/d/eh;)V
    .registers 3

    .prologue
    .line 132
    iput-object p1, p0, Lcom/a/b/d/ei;->a:Lcom/a/b/d/eh;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    .line 133
    const/4 v0, -0x1

    iput v0, p0, Lcom/a/b/d/ei;->b:I

    .line 134
    iget-object v0, p0, Lcom/a/b/d/ei;->a:Lcom/a/b/d/eh;

    iget-object v0, v0, Lcom/a/b/d/eh;->a:Lcom/a/b/d/eg;

    invoke-virtual {v0}, Lcom/a/b/d/eg;->a()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->size()I

    move-result v0

    iput v0, p0, Lcom/a/b/d/ei;->c:I

    return-void
.end method

.method private c()Ljava/util/Map$Entry;
    .registers 4

    .prologue
    .line 138
    iget v0, p0, Lcom/a/b/d/ei;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/ei;->b:I

    :goto_6
    iget v0, p0, Lcom/a/b/d/ei;->b:I

    iget v1, p0, Lcom/a/b/d/ei;->c:I

    if-ge v0, v1, :cond_3a

    .line 139
    iget-object v0, p0, Lcom/a/b/d/ei;->a:Lcom/a/b/d/eh;

    iget-object v0, v0, Lcom/a/b/d/eh;->a:Lcom/a/b/d/eg;

    iget v1, p0, Lcom/a/b/d/ei;->b:I

    invoke-virtual {v0, v1}, Lcom/a/b/d/eg;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_33

    .line 141
    iget-object v1, p0, Lcom/a/b/d/ei;->a:Lcom/a/b/d/eh;

    iget-object v1, v1, Lcom/a/b/d/eh;->a:Lcom/a/b/d/eg;

    iget v2, p0, Lcom/a/b/d/ei;->b:I

    .line 1102
    invoke-virtual {v1}, Lcom/a/b/d/eg;->a()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 141
    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 144
    :goto_32
    return-object v0

    .line 138
    :cond_33
    iget v0, p0, Lcom/a/b/d/ei;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/ei;->b:I

    goto :goto_6

    .line 144
    :cond_3a
    invoke-virtual {p0}, Lcom/a/b/d/ei;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    goto :goto_32
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 132
    .line 1138
    iget v0, p0, Lcom/a/b/d/ei;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/ei;->b:I

    :goto_6
    iget v0, p0, Lcom/a/b/d/ei;->b:I

    iget v1, p0, Lcom/a/b/d/ei;->c:I

    if-ge v0, v1, :cond_3a

    .line 1139
    iget-object v0, p0, Lcom/a/b/d/ei;->a:Lcom/a/b/d/eh;

    iget-object v0, v0, Lcom/a/b/d/eh;->a:Lcom/a/b/d/eg;

    iget v1, p0, Lcom/a/b/d/ei;->b:I

    invoke-virtual {v0, v1}, Lcom/a/b/d/eg;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 1140
    if-eqz v0, :cond_33

    .line 1141
    iget-object v1, p0, Lcom/a/b/d/ei;->a:Lcom/a/b/d/eh;

    iget-object v1, v1, Lcom/a/b/d/eh;->a:Lcom/a/b/d/eg;

    iget v2, p0, Lcom/a/b/d/ei;->b:I

    .line 2102
    invoke-virtual {v1}, Lcom/a/b/d/eg;->a()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 1141
    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    :goto_32
    return-object v0

    .line 1138
    :cond_33
    iget v0, p0, Lcom/a/b/d/ei;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/ei;->b:I

    goto :goto_6

    .line 1144
    :cond_3a
    invoke-virtual {p0}, Lcom/a/b/d/ei;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    .line 132
    goto :goto_32
.end method
