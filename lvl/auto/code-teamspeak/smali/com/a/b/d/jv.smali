.class final Lcom/a/b/d/jv;
.super Lcom/a/b/d/jt;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/b/d/jt;


# direct methods
.method constructor <init>(Lcom/a/b/d/jt;)V
    .registers 3

    .prologue
    .line 453
    invoke-direct {p0}, Lcom/a/b/d/jt;-><init>()V

    .line 454
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/jt;

    iput-object v0, p0, Lcom/a/b/d/jv;->a:Lcom/a/b/d/jt;

    .line 455
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/jv;)Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 449
    iget-object v0, p0, Lcom/a/b/d/jv;->a:Lcom/a/b/d/jt;

    return-object v0
.end method

.method private a(Ljava/lang/Object;)Lcom/a/b/d/lo;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 466
    iget-object v0, p0, Lcom/a/b/d/jv;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 467
    if-nez v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    invoke-static {v0}, Lcom/a/b/d/lo;->d(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_9
.end method


# virtual methods
.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 462
    iget-object v0, p0, Lcom/a/b/d/jv;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final d()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 475
    new-instance v0, Lcom/a/b/d/jw;

    invoke-direct {v0, p0}, Lcom/a/b/d/jw;-><init>(Lcom/a/b/d/jv;)V

    return-object v0
.end method

.method public final synthetic entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 449
    invoke-super {p0}, Lcom/a/b/d/jt;->e()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 449
    .line 1466
    iget-object v0, p0, Lcom/a/b/d/jv;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1467
    if-nez v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    invoke-static {v0}, Lcom/a/b/d/lo;->d(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_9
.end method

.method final i_()Z
    .registers 2

    .prologue
    .line 471
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 449
    invoke-super {p0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 458
    iget-object v0, p0, Lcom/a/b/d/jv;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 449
    invoke-super {p0}, Lcom/a/b/d/jt;->h()Lcom/a/b/d/iz;

    move-result-object v0

    return-object v0
.end method
