.class abstract Lcom/a/b/d/en;
.super Lcom/a/b/d/gy;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/abn;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# instance fields
.field private transient a:Ljava/util/Comparator;

.field private transient b:Ljava/util/NavigableSet;

.field private transient c:Ljava/util/Set;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/a/b/d/gy;-><init>()V

    return-void
.end method

.method private q()Ljava/util/Set;
    .registers 2

    .prologue
    .line 110
    new-instance v0, Lcom/a/b/d/eo;

    invoke-direct {v0, p0}, Lcom/a/b/d/eo;-><init>(Lcom/a/b/d/en;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 6

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/a/b/d/en;->c()Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0, p3, p4, p1, p2}, Lcom/a/b/d/abn;->a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->m()Lcom/a/b/d/abn;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/a/b/d/en;->c:Ljava/util/Set;

    .line 106
    if-nez v0, :cond_b

    .line 1110
    new-instance v0, Lcom/a/b/d/eo;

    invoke-direct {v0, p0}, Lcom/a/b/d/eo;-><init>(Lcom/a/b/d/en;)V

    .line 106
    iput-object v0, p0, Lcom/a/b/d/en;->c:Ljava/util/Set;

    :cond_b
    return-object v0
.end method

.method protected final synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 32
    .line 2085
    invoke-virtual {p0}, Lcom/a/b/d/en;->c()Lcom/a/b/d/abn;

    move-result-object v0

    .line 32
    return-object v0
.end method

.method abstract c()Lcom/a/b/d/abn;
.end method

.method public final c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 4

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/a/b/d/en;->c()Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/abn;->d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->m()Lcom/a/b/d/abn;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/a/b/d/en;->a:Ljava/util/Comparator;

    .line 41
    if-nez v0, :cond_16

    .line 42
    invoke-virtual {p0}, Lcom/a/b/d/en;->c()Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/en;->a:Ljava/util/Comparator;

    .line 45
    :cond_16
    return-object v0
.end method

.method public final d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 4

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/a/b/d/en;->c()Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/abn;->c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->m()Lcom/a/b/d/abn;

    move-result-object v0

    return-object v0
.end method

.method abstract e()Ljava/util/Iterator;
.end method

.method public final e_()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/a/b/d/en;->b:Ljava/util/NavigableSet;

    .line 52
    if-nez v0, :cond_b

    .line 53
    new-instance v0, Lcom/a/b/d/abr;

    invoke-direct {v0, p0}, Lcom/a/b/d/abr;-><init>(Lcom/a/b/d/abn;)V

    iput-object v0, p0, Lcom/a/b/d/en;->b:Ljava/util/NavigableSet;

    .line 55
    :cond_b
    return-object v0
.end method

.method protected final f()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/a/b/d/en;->c()Lcom/a/b/d/abn;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/a/b/d/en;->c()Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->i()Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/a/b/d/en;->c()Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->h()Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 126
    invoke-static {p0}, Lcom/a/b/d/xe;->b(Lcom/a/b/d/xc;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/a/b/d/en;->c()Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->k()Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/a/b/d/en;->c()Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->j()Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 32
    .line 3085
    invoke-virtual {p0}, Lcom/a/b/d/en;->c()Lcom/a/b/d/abn;

    move-result-object v0

    .line 32
    return-object v0
.end method

.method public final m()Lcom/a/b/d/abn;
    .registers 2

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/a/b/d/en;->c()Lcom/a/b/d/abn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic n()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/a/b/d/en;->e_()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/a/b/d/en;->e_()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/a/b/d/en;->p()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 134
    .line 1253
    invoke-static {p0, p1}, Lcom/a/b/d/yc;->a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 134
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/a/b/d/en;->a()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
