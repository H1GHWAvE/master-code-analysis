.class final Lcom/a/b/d/adu;
.super Lcom/a/b/d/adr;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/abs;


# static fields
.field private static final i:J


# direct methods
.method constructor <init>(Lcom/a/b/d/abs;)V
    .registers 2

    .prologue
    .line 786
    invoke-direct {p0, p1}, Lcom/a/b/d/adr;-><init>(Lcom/a/b/d/aac;)V

    .line 787
    return-void
.end method

.method private h()Lcom/a/b/d/abs;
    .registers 2

    .prologue
    .line 789
    invoke-super {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    return-object v0
.end method


# virtual methods
.method final synthetic a()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 782
    .line 6789
    invoke-super {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    .line 782
    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 782
    invoke-virtual {p0, p1}, Lcom/a/b/d/adu;->h(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 4

    .prologue
    .line 782
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/adu;->d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 782
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/adu;->d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 782
    invoke-virtual {p0, p1}, Lcom/a/b/d/adu;->i(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method final bridge synthetic c()Lcom/a/b/d/aac;
    .registers 2

    .prologue
    .line 782
    .line 5789
    invoke-super {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    .line 782
    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 782
    invoke-virtual {p0, p1}, Lcom/a/b/d/adu;->h(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method final synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 782
    .line 7789
    invoke-super {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    .line 782
    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 782
    invoke-virtual {p0, p1}, Lcom/a/b/d/adu;->i(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 803
    iget-object v1, p0, Lcom/a/b/d/adu;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 3789
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    .line 804
    invoke-interface {v0, p1, p2}, Lcom/a/b/d/abs;->d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 805
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final d_()Ljava/util/Comparator;
    .registers 3

    .prologue
    .line 809
    iget-object v1, p0, Lcom/a/b/d/adu;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 4789
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    .line 810
    invoke-interface {v0}, Lcom/a/b/d/abs;->d_()Ljava/util/Comparator;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 811
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final h(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 792
    iget-object v1, p0, Lcom/a/b/d/adu;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1789
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    .line 793
    invoke-interface {v0, p1}, Lcom/a/b/d/abs;->h(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adu;->h:Ljava/lang/Object;

    .line 2060
    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    .line 793
    monitor-exit v1

    return-object v0

    .line 794
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final i(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 797
    iget-object v1, p0, Lcom/a/b/d/adu;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 2789
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    .line 798
    invoke-interface {v0, p1}, Lcom/a/b/d/abs;->i(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 799
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method
