.class abstract Lcom/a/b/d/aek;
.super Lcom/a/b/d/aej;
.source "SourceFile"

# interfaces
.implements Ljava/util/ListIterator;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method constructor <init>(Ljava/util/ListIterator;)V
    .registers 2

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/a/b/d/aej;-><init>(Ljava/util/Iterator;)V

    .line 36
    return-void
.end method

.method private a()Ljava/util/ListIterator;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/a/b/d/aek;->c:Ljava/util/Iterator;

    invoke-static {v0}, Lcom/a/b/d/nj;->k(Ljava/util/Iterator;)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 69
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final hasPrevious()Z
    .registers 2

    .prologue
    .line 44
    .line 1039
    iget-object v0, p0, Lcom/a/b/d/aek;->c:Ljava/util/Iterator;

    invoke-static {v0}, Lcom/a/b/d/nj;->k(Ljava/util/Iterator;)Ljava/util/ListIterator;

    move-result-object v0

    .line 44
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public final nextIndex()I
    .registers 2

    .prologue
    .line 54
    .line 3039
    iget-object v0, p0, Lcom/a/b/d/aek;->c:Ljava/util/Iterator;

    invoke-static {v0}, Lcom/a/b/d/nj;->k(Ljava/util/Iterator;)Ljava/util/ListIterator;

    move-result-object v0

    .line 54
    invoke-interface {v0}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 49
    .line 2039
    iget-object v0, p0, Lcom/a/b/d/aek;->c:Ljava/util/Iterator;

    invoke-static {v0}, Lcom/a/b/d/nj;->k(Ljava/util/Iterator;)Ljava/util/ListIterator;

    move-result-object v0

    .line 49
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/aek;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .registers 2

    .prologue
    .line 59
    .line 4039
    iget-object v0, p0, Lcom/a/b/d/aek;->c:Ljava/util/Iterator;

    invoke-static {v0}, Lcom/a/b/d/nj;->k(Ljava/util/Iterator;)Ljava/util/ListIterator;

    move-result-object v0

    .line 59
    invoke-interface {v0}, Ljava/util/ListIterator;->previousIndex()I

    move-result v0

    return v0
.end method

.method public set(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 64
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
