.class final Lcom/a/b/d/age;
.super Lcom/a/b/d/agi;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/yi;


# instance fields
.field final synthetic a:Lcom/a/b/d/aga;

.field private final b:Ljava/util/Queue;


# direct methods
.method constructor <init>(Lcom/a/b/d/aga;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 193
    iput-object p1, p0, Lcom/a/b/d/age;->a:Lcom/a/b/d/aga;

    invoke-direct {p0}, Lcom/a/b/d/agi;-><init>()V

    .line 194
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/a/b/d/age;->b:Ljava/util/Queue;

    .line 195
    iget-object v0, p0, Lcom/a/b/d/age;->b:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 196
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/a/b/d/age;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->element()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final hasNext()Z
    .registers 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/a/b/d/age;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final next()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 210
    iget-object v0, p0, Lcom/a/b/d/age;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    .line 211
    iget-object v1, p0, Lcom/a/b/d/age;->b:Ljava/util/Queue;

    iget-object v2, p0, Lcom/a/b/d/age;->a:Lcom/a/b/d/aga;

    invoke-virtual {v2, v0}, Lcom/a/b/d/aga;->a(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 212
    return-object v0
.end method
