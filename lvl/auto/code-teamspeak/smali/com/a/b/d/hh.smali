.class public abstract Lcom/a/b/d/hh;
.super Lcom/a/b/d/gh;
.source "SourceFile"

# interfaces
.implements Ljava/util/Queue;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/a/b/d/gh;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 87
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/a/b/d/hh;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_3} :catch_5

    move-result v0

    .line 89
    :goto_4
    return v0

    :catch_5
    move-exception v0

    const/4 v0, 0x0

    goto :goto_4
.end method

.method private c()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 102
    :try_start_0
    invoke-virtual {p0}, Lcom/a/b/d/hh;->element()Ljava/lang/Object;
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    .line 104
    :goto_4
    return-object v0

    :catch_5
    move-exception v0

    const/4 v0, 0x0

    goto :goto_4
.end method

.method private d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 117
    :try_start_0
    invoke-virtual {p0}, Lcom/a/b/d/hh;->remove()Ljava/lang/Object;
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    .line 119
    :goto_4
    return-object v0

    :catch_5
    move-exception v0

    const/4 v0, 0x0

    goto :goto_4
.end method


# virtual methods
.method public abstract a()Ljava/util/Queue;
.end method

.method public synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/a/b/d/hh;->a()Ljava/util/Queue;

    move-result-object v0

    return-object v0
.end method

.method public element()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/a/b/d/hh;->a()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->element()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/a/b/d/hh;->a()Ljava/util/Queue;

    move-result-object v0

    return-object v0
.end method

.method public offer(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/a/b/d/hh;->a()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public peek()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/a/b/d/hh;->a()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public poll()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/a/b/d/hh;->a()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/a/b/d/hh;->a()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
