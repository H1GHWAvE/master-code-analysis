.class final Lcom/a/b/l/b;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final d:J


# instance fields
.field final a:[Z

.field final b:I

.field final c:I


# direct methods
.method constructor <init>([Z)V
    .registers 4

    .prologue
    .line 361
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/l/b;-><init>([ZII)V

    .line 362
    return-void
.end method

.method private constructor <init>([ZII)V
    .registers 4

    .prologue
    .line 364
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 365
    iput-object p1, p0, Lcom/a/b/l/b;->a:[Z

    .line 366
    iput p2, p0, Lcom/a/b/l/b;->b:I

    .line 367
    iput p3, p0, Lcom/a/b/l/b;->c:I

    .line 368
    return-void
.end method

.method private a(I)Ljava/lang/Boolean;
    .registers 4

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/a/b/l/b;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 380
    iget-object v0, p0, Lcom/a/b/l/b;->a:[Z

    iget v1, p0, Lcom/a/b/l/b;->b:I

    add-int/2addr v1, p1

    aget-boolean v0, v0, v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/Boolean;)Ljava/lang/Boolean;
    .registers 7

    .prologue
    .line 412
    invoke-virtual {p0}, Lcom/a/b/l/b;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 413
    iget-object v0, p0, Lcom/a/b/l/b;->a:[Z

    iget v1, p0, Lcom/a/b/l/b;->b:I

    add-int/2addr v1, p1

    aget-boolean v1, v0, v1

    .line 415
    iget-object v2, p0, Lcom/a/b/l/b;->a:[Z

    iget v0, p0, Lcom/a/b/l/b;->b:I

    add-int v3, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    aput-boolean v0, v2, v3

    .line 416
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private a()[Z
    .registers 6

    .prologue
    .line 467
    invoke-virtual {p0}, Lcom/a/b/l/b;->size()I

    move-result v0

    .line 468
    new-array v1, v0, [Z

    .line 469
    iget-object v2, p0, Lcom/a/b/l/b;->a:[Z

    iget v3, p0, Lcom/a/b/l/b;->b:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 470
    return-object v1
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    .line 385
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/a/b/l/b;->a:[Z

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget v2, p0, Lcom/a/b/l/b;->b:I

    iget v3, p0, Lcom/a/b/l/b;->c:I

    .line 1049
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/a;->a([ZZII)I

    move-result v0

    .line 385
    const/4 v1, -0x1

    if-eq v0, v1, :cond_19

    const/4 v0, 0x1

    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 429
    if-ne p1, p0, :cond_5

    .line 445
    :cond_4
    :goto_4
    return v0

    .line 432
    :cond_5
    instance-of v2, p1, Lcom/a/b/l/b;

    if-eqz v2, :cond_2f

    .line 433
    check-cast p1, Lcom/a/b/l/b;

    .line 434
    invoke-virtual {p0}, Lcom/a/b/l/b;->size()I

    move-result v3

    .line 435
    invoke-virtual {p1}, Lcom/a/b/l/b;->size()I

    move-result v2

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 436
    goto :goto_4

    :cond_17
    move v2, v1

    .line 438
    :goto_18
    if-ge v2, v3, :cond_4

    .line 439
    iget-object v4, p0, Lcom/a/b/l/b;->a:[Z

    iget v5, p0, Lcom/a/b/l/b;->b:I

    add-int/2addr v5, v2

    aget-boolean v4, v4, v5

    iget-object v5, p1, Lcom/a/b/l/b;->a:[Z

    iget v6, p1, Lcom/a/b/l/b;->b:I

    add-int/2addr v6, v2

    aget-boolean v5, v5, v6

    if-eq v4, v5, :cond_2c

    move v0, v1

    .line 440
    goto :goto_4

    .line 438
    :cond_2c
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    .line 445
    :cond_2f
    invoke-super {p0, p1}, Ljava/util/AbstractList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 353
    .line 4379
    invoke-virtual {p0}, Lcom/a/b/l/b;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 4380
    iget-object v0, p0, Lcom/a/b/l/b;->a:[Z

    iget v1, p0, Lcom/a/b/l/b;->b:I

    add-int/2addr v1, p1

    aget-boolean v0, v0, v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 353
    return-object v0
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 449
    const/4 v1, 0x1

    .line 450
    iget v0, p0, Lcom/a/b/l/b;->b:I

    :goto_3
    iget v2, p0, Lcom/a/b/l/b;->c:I

    if-ge v0, v2, :cond_18

    .line 451
    mul-int/lit8 v2, v1, 0x1f

    iget-object v1, p0, Lcom/a/b/l/b;->a:[Z

    aget-boolean v1, v1, v0

    .line 3060
    if-eqz v1, :cond_15

    const/16 v1, 0x4cf

    .line 451
    :goto_11
    add-int/2addr v1, v2

    .line 450
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 3060
    :cond_15
    const/16 v1, 0x4d5

    goto :goto_11

    .line 453
    :cond_18
    return v1
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 391
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_1a

    .line 392
    iget-object v0, p0, Lcom/a/b/l/b;->a:[Z

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget v2, p0, Lcom/a/b/l/b;->b:I

    iget v3, p0, Lcom/a/b/l/b;->c:I

    .line 2049
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/a;->a([ZZII)I

    move-result v0

    .line 393
    if-ltz v0, :cond_1a

    .line 394
    iget v1, p0, Lcom/a/b/l/b;->b:I

    sub-int/2addr v0, v1

    .line 397
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 375
    const/4 v0, 0x0

    return v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 402
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_1a

    .line 403
    iget-object v0, p0, Lcom/a/b/l/b;->a:[Z

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget v2, p0, Lcom/a/b/l/b;->b:I

    iget v3, p0, Lcom/a/b/l/b;->c:I

    .line 3049
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/a;->b([ZZII)I

    move-result v0

    .line 404
    if-ltz v0, :cond_1a

    .line 405
    iget v1, p0, Lcom/a/b/l/b;->b:I

    sub-int/2addr v0, v1

    .line 408
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 7

    .prologue
    .line 353
    check-cast p2, Ljava/lang/Boolean;

    .line 3412
    invoke-virtual {p0}, Lcom/a/b/l/b;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 3413
    iget-object v0, p0, Lcom/a/b/l/b;->a:[Z

    iget v1, p0, Lcom/a/b/l/b;->b:I

    add-int/2addr v1, p1

    aget-boolean v1, v0, v1

    .line 3415
    iget-object v2, p0, Lcom/a/b/l/b;->a:[Z

    iget v0, p0, Lcom/a/b/l/b;->b:I

    add-int v3, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    aput-boolean v0, v2, v3

    .line 3416
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 353
    return-object v0
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 371
    iget v0, p0, Lcom/a/b/l/b;->c:I

    iget v1, p0, Lcom/a/b/l/b;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final subList(II)Ljava/util/List;
    .registers 7

    .prologue
    .line 420
    invoke-virtual {p0}, Lcom/a/b/l/b;->size()I

    move-result v0

    .line 421
    invoke-static {p1, p2, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 422
    if-ne p1, p2, :cond_e

    .line 423
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 425
    :goto_d
    return-object v0

    :cond_e
    new-instance v0, Lcom/a/b/l/b;

    iget-object v1, p0, Lcom/a/b/l/b;->a:[Z

    iget v2, p0, Lcom/a/b/l/b;->b:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/a/b/l/b;->b:I

    add-int/2addr v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/l/b;-><init>([ZII)V

    goto :goto_d
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 457
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/a/b/l/b;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x7

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 458
    iget-object v0, p0, Lcom/a/b/l/b;->a:[Z

    iget v1, p0, Lcom/a/b/l/b;->b:I

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_2e

    const-string v0, "[true"

    :goto_15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 459
    iget v0, p0, Lcom/a/b/l/b;->b:I

    add-int/lit8 v0, v0, 0x1

    :goto_1c
    iget v1, p0, Lcom/a/b/l/b;->c:I

    if-ge v0, v1, :cond_34

    .line 460
    iget-object v1, p0, Lcom/a/b/l/b;->a:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_31

    const-string v1, ", true"

    :goto_28
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 459
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 458
    :cond_2e
    const-string v0, "[false"

    goto :goto_15

    .line 460
    :cond_31
    const-string v1, ", false"

    goto :goto_28

    .line 462
    :cond_34
    const/16 v0, 0x5d

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
