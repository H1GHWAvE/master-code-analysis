.class final Lcom/a/b/l/x;
.super Lcom/a/b/b/ak;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final a:Lcom/a/b/l/x;

.field private static final b:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 372
    new-instance v0, Lcom/a/b/l/x;

    invoke-direct {v0}, Lcom/a/b/l/x;-><init>()V

    sput-object v0, Lcom/a/b/l/x;->a:Lcom/a/b/l/x;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 371
    invoke-direct {p0}, Lcom/a/b/b/ak;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Long;
    .registers 2

    .prologue
    .line 376
    invoke-static {p0}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Long;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 381
    invoke-virtual {p0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 390
    sget-object v0, Lcom/a/b/l/x;->a:Lcom/a/b/l/x;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 371
    check-cast p1, Ljava/lang/Long;

    .line 1381
    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    .line 371
    return-object v0
.end method

.method protected final synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 371
    check-cast p1, Ljava/lang/String;

    .line 2376
    invoke-static {p1}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 371
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 386
    const-string v0, "Longs.stringConverter()"

    return-object v0
.end method
