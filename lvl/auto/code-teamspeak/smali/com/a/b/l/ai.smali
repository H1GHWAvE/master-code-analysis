.class final enum Lcom/a/b/l/ai;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field public static final enum a:Lcom/a/b/l/ai;

.field private static final synthetic b:[Lcom/a/b/l/ai;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 410
    new-instance v0, Lcom/a/b/l/ai;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lcom/a/b/l/ai;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/l/ai;->a:Lcom/a/b/l/ai;

    .line 409
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/l/ai;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/l/ai;->a:Lcom/a/b/l/ai;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/l/ai;->b:[Lcom/a/b/l/ai;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 409
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a([B[B)I
    .registers 6

    .prologue
    .line 413
    array-length v0, p0

    array-length v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 414
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-ge v1, v2, :cond_19

    .line 415
    aget-byte v0, p0, v1

    aget-byte v3, p1, v1

    invoke-static {v0, v3}, Lcom/a/b/l/ag;->a(BB)I

    move-result v0

    .line 416
    if-eqz v0, :cond_15

    .line 420
    :goto_14
    return v0

    .line 414
    :cond_15
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 420
    :cond_19
    array-length v0, p0

    array-length v1, p1

    sub-int/2addr v0, v1

    goto :goto_14
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/l/ai;
    .registers 2

    .prologue
    .line 409
    const-class v0, Lcom/a/b/l/ai;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/ai;

    return-object v0
.end method

.method public static values()[Lcom/a/b/l/ai;
    .registers 1

    .prologue
    .line 409
    sget-object v0, Lcom/a/b/l/ai;->b:[Lcom/a/b/l/ai;

    invoke-virtual {v0}, [Lcom/a/b/l/ai;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/l/ai;

    return-object v0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 7

    .prologue
    .line 409
    check-cast p1, [B

    check-cast p2, [B

    .line 1413
    array-length v0, p1

    array-length v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1414
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-ge v1, v2, :cond_1d

    .line 1415
    aget-byte v0, p1, v1

    aget-byte v3, p2, v1

    invoke-static {v0, v3}, Lcom/a/b/l/ag;->a(BB)I

    move-result v0

    .line 1416
    if-eqz v0, :cond_19

    .line 1417
    :goto_18
    return v0

    .line 1414
    :cond_19
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 1420
    :cond_1d
    array-length v0, p1

    array-length v1, p2

    sub-int/2addr v0, v1

    .line 409
    goto :goto_18
.end method
