.class public final Lcom/a/b/l/m;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field public static final a:I = 0x4


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static varargs a([F)F
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 218
    array-length v0, p0

    if-lez v0, :cond_17

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 219
    aget v0, p0, v2

    .line 220
    :goto_b
    array-length v2, p0

    if-ge v1, v2, :cond_19

    .line 221
    aget v2, p0, v1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 220
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_17
    move v0, v2

    .line 218
    goto :goto_6

    .line 223
    :cond_19
    return v0
.end method

.method private static a(F)I
    .registers 2

    .prologue
    .line 74
    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    return v0
.end method

.method private static a(FF)I
    .registers 3

    .prologue
    .line 92
    invoke-static {p0, p1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    return v0
.end method

.method static a([FFII)I
    .registers 6

    .prologue
    .line 142
    move v0, p2

    :goto_1
    if-ge v0, p3, :cond_d

    .line 143
    aget v1, p0, v0

    cmpl-float v1, v1, p1

    if-nez v1, :cond_a

    .line 147
    :goto_9
    return v0

    .line 142
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 147
    :cond_d
    const/4 v0, -0x1

    goto :goto_9
.end method

.method private static a([F[F)I
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 165
    const-string v0, "array"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    const-string v0, "target"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    array-length v0, p1

    if-nez v0, :cond_f

    .line 180
    :goto_e
    return v1

    :cond_f
    move v0, v1

    .line 172
    :goto_10
    array-length v2, p0

    array-length v3, p1

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    if-ge v0, v2, :cond_2d

    move v2, v1

    .line 173
    :goto_18
    array-length v3, p1

    if-ge v2, v3, :cond_28

    .line 174
    add-int v3, v0, v2

    aget v3, p0, v3

    aget v4, p1, v2

    cmpl-float v3, v3, v4

    if-nez v3, :cond_2a

    .line 173
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    :cond_28
    move v1, v0

    .line 178
    goto :goto_e

    .line 172
    :cond_2a
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 180
    :cond_2d
    const/4 v1, -0x1

    goto :goto_e
.end method

.method private static a()Lcom/a/b/b/ak;
    .registers 1
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 300
    sget-object v0, Lcom/a/b/l/o;->a:Lcom/a/b/l/o;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Float;
    .registers 2
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation build Lcom/a/b/a/c;
        a = "regular expressions"
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 600
    sget-object v0, Lcom/a/b/l/i;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 604
    :try_start_c
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;
    :try_end_13
    .catch Ljava/lang/NumberFormatException; {:try_start_c .. :try_end_13} :catch_15

    move-result-object v0

    .line 610
    :goto_14
    return-object v0

    :catch_15
    move-exception v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_14
.end method

.method private static varargs a(Ljava/lang/String;[F)Ljava/lang/String;
    .registers 6

    .prologue
    .line 350
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    array-length v0, p1

    if-nez v0, :cond_9

    .line 352
    const-string v0, ""

    .line 361
    :goto_8
    return-object v0

    .line 356
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p1

    mul-int/lit8 v0, v0, 0xc

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 357
    const/4 v0, 0x0

    aget v0, p1, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 358
    const/4 v0, 0x1

    :goto_18
    array-length v2, p1

    if-ge v0, v2, :cond_27

    .line 359
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 358
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 361
    :cond_27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method private static a([FF)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 117
    array-length v2, p0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_c

    aget v3, p0, v1

    .line 118
    cmpl-float v3, v3, p1

    if-nez v3, :cond_d

    .line 119
    const/4 v0, 0x1

    .line 122
    :cond_c
    return v0

    .line 117
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private static a(Ljava/util/Collection;)[F
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 416
    instance-of v0, p0, Lcom/a/b/l/n;

    if-eqz v0, :cond_15

    .line 417
    check-cast p0, Lcom/a/b/l/n;

    .line 1568
    invoke-virtual {p0}, Lcom/a/b/l/n;->size()I

    move-result v1

    .line 1569
    new-array v0, v1, [F

    .line 1570
    iget-object v3, p0, Lcom/a/b/l/n;->a:[F

    iget v4, p0, Lcom/a/b/l/n;->b:I

    invoke-static {v3, v4, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 427
    :goto_14
    return-object v0

    .line 420
    :cond_15
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v3

    .line 421
    array-length v4, v3

    .line 422
    new-array v1, v4, [F

    .line 423
    :goto_1c
    if-ge v2, v4, :cond_30

    .line 425
    aget-object v0, v3, v2

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    aput v0, v1, v2

    .line 423
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1c

    :cond_30
    move-object v0, v1

    .line 427
    goto :goto_14
.end method

.method private static a([FI)[F
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 330
    new-array v0, p1, [F

    .line 331
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 332
    return-object v0
.end method

.method private static a([FII)[F
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 321
    if-ltz p1, :cond_33

    move v0, v1

    :goto_5
    const-string v3, "Invalid minLength: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 322
    if-ltz p2, :cond_35

    move v0, v1

    :goto_15
    const-string v3, "Invalid padding: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 323
    array-length v0, p0

    if-ge v0, p1, :cond_32

    add-int v1, p1, p2

    .line 1330
    new-array v0, v1, [F

    .line 1331
    array-length v3, p0

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 323
    :cond_32
    return-object p0

    :cond_33
    move v0, v2

    .line 321
    goto :goto_5

    :cond_35
    move v0, v2

    .line 322
    goto :goto_15
.end method

.method private static varargs a([[F)[F
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 254
    .line 255
    array-length v3, p0

    move v0, v1

    move v2, v1

    :goto_4
    if-ge v0, v3, :cond_d

    aget-object v4, p0, v0

    .line 256
    array-length v4, v4

    add-int/2addr v2, v4

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 258
    :cond_d
    new-array v3, v2, [F

    .line 260
    array-length v4, p0

    move v0, v1

    move v2, v1

    :goto_12
    if-ge v2, v4, :cond_1f

    aget-object v5, p0, v2

    .line 261
    array-length v6, v5

    invoke-static {v5, v1, v3, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 262
    array-length v5, v5

    add-int/2addr v0, v5

    .line 260
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    .line 264
    :cond_1f
    return-object v3
.end method

.method private static varargs b([F)F
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 236
    array-length v0, p0

    if-lez v0, :cond_17

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 237
    aget v0, p0, v2

    .line 238
    :goto_b
    array-length v2, p0

    if-ge v1, v2, :cond_19

    .line 239
    aget v2, p0, v1

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 238
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_17
    move v0, v2

    .line 236
    goto :goto_6

    .line 241
    :cond_19
    return v0
.end method

.method private static b([FF)I
    .registers 4

    .prologue
    .line 136
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/l/m;->a([FFII)I

    move-result v0

    return v0
.end method

.method static b([FFII)I
    .registers 6

    .prologue
    .line 200
    add-int/lit8 v0, p3, -0x1

    :goto_2
    if-lt v0, p2, :cond_e

    .line 201
    aget v1, p0, v0

    cmpl-float v1, v1, p1

    if-nez v1, :cond_b

    .line 205
    :goto_a
    return v0

    .line 200
    :cond_b
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 205
    :cond_e
    const/4 v0, -0x1

    goto :goto_a
.end method

.method private static b()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 381
    sget-object v0, Lcom/a/b/l/p;->a:Lcom/a/b/l/p;

    return-object v0
.end method

.method private static b(F)Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 103
    const/high16 v2, -0x800000    # Float.NEGATIVE_INFINITY

    cmpg-float v2, v2, p0

    if-gez v2, :cond_11

    move v2, v0

    :goto_9
    const/high16 v3, 0x7f800000    # Float.POSITIVE_INFINITY

    cmpg-float v3, p0, v3

    if-gez v3, :cond_13

    :goto_f
    and-int/2addr v0, v2

    return v0

    :cond_11
    move v2, v1

    goto :goto_9

    :cond_13
    move v0, v1

    goto :goto_f
.end method

.method private static c([FF)I
    .registers 4

    .prologue
    .line 194
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/l/m;->b([FFII)I

    move-result v0

    return v0
.end method

.method private static synthetic c([FFII)I
    .registers 5

    .prologue
    .line 54
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/l/m;->a([FFII)I

    move-result v0

    return v0
.end method

.method private static varargs c([F)Ljava/util/List;
    .registers 2

    .prologue
    .line 448
    array-length v0, p0

    if-nez v0, :cond_8

    .line 449
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 451
    :goto_7
    return-object v0

    :cond_8
    new-instance v0, Lcom/a/b/l/n;

    invoke-direct {v0, p0}, Lcom/a/b/l/n;-><init>([F)V

    goto :goto_7
.end method

.method private static synthetic d([FFII)I
    .registers 5

    .prologue
    .line 54
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/l/m;->b([FFII)I

    move-result v0

    return v0
.end method
