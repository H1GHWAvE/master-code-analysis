.class final enum Lcom/a/b/l/l;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field public static final enum a:Lcom/a/b/l/l;

.field private static final synthetic b:[Lcom/a/b/l/l;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 389
    new-instance v0, Lcom/a/b/l/l;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lcom/a/b/l/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/l/l;->a:Lcom/a/b/l/l;

    .line 388
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/l/l;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/l/l;->a:Lcom/a/b/l/l;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/l/l;->b:[Lcom/a/b/l/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 388
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a([D[D)I
    .registers 10

    .prologue
    .line 393
    array-length v0, p0

    array-length v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 394
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-ge v1, v2, :cond_19

    .line 395
    aget-wide v4, p0, v1

    aget-wide v6, p1, v1

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    .line 396
    if-eqz v0, :cond_15

    .line 400
    :goto_14
    return v0

    .line 394
    :cond_15
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 400
    :cond_19
    array-length v0, p0

    array-length v1, p1

    sub-int/2addr v0, v1

    goto :goto_14
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/l/l;
    .registers 2

    .prologue
    .line 388
    const-class v0, Lcom/a/b/l/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/l;

    return-object v0
.end method

.method public static values()[Lcom/a/b/l/l;
    .registers 1

    .prologue
    .line 388
    sget-object v0, Lcom/a/b/l/l;->b:[Lcom/a/b/l/l;

    invoke-virtual {v0}, [Lcom/a/b/l/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/l/l;

    return-object v0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 11

    .prologue
    .line 388
    check-cast p1, [D

    check-cast p2, [D

    .line 1393
    array-length v0, p1

    array-length v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1394
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-ge v1, v2, :cond_1d

    .line 1395
    aget-wide v4, p1, v1

    aget-wide v6, p2, v1

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    .line 1396
    if-eqz v0, :cond_19

    .line 1397
    :goto_18
    return v0

    .line 1394
    :cond_19
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 1400
    :cond_1d
    array-length v0, p1

    array-length v1, p2

    sub-int/2addr v0, v1

    .line 388
    goto :goto_18
.end method
