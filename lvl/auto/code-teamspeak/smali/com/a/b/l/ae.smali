.class public final Lcom/a/b/l/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field public static final a:B = 0x40t


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(J)B
    .registers 6

    .prologue
    .line 61
    long-to-int v0, p0

    int-to-byte v0, v0

    .line 62
    int-to-long v2, v0

    cmp-long v1, v2, p0

    if-eqz v1, :cond_22

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Out of range: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_22
    return v0
.end method

.method private static varargs a([B)B
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 114
    array-length v0, p0

    if-lez v0, :cond_17

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 115
    aget-byte v0, p0, v2

    .line 116
    :goto_b
    array-length v2, p0

    if-ge v1, v2, :cond_19

    .line 117
    aget-byte v2, p0, v1

    if-ge v2, v0, :cond_14

    .line 118
    aget-byte v0, p0, v1

    .line 116
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_17
    move v0, v2

    .line 114
    goto :goto_6

    .line 121
    :cond_19
    return v0
.end method

.method private static a(BB)I
    .registers 3

    .prologue
    .line 102
    sub-int v0, p0, p1

    return v0
.end method

.method private static varargs a(Ljava/lang/String;[B)Ljava/lang/String;
    .registers 6

    .prologue
    .line 153
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    array-length v0, p1

    if-nez v0, :cond_9

    .line 155
    const-string v0, ""

    .line 164
    :goto_8
    return-object v0

    .line 159
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p1

    mul-int/lit8 v0, v0, 0x5

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 160
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 161
    const/4 v0, 0x1

    :goto_18
    array-length v2, p1

    if-ge v0, v2, :cond_27

    .line 162
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-byte v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 161
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 164
    :cond_27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method private static a()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 184
    sget-object v0, Lcom/a/b/l/af;->a:Lcom/a/b/l/af;

    return-object v0
.end method

.method private static b(J)B
    .registers 4

    .prologue
    .line 78
    const-wide/16 v0, 0x7f

    cmp-long v0, p0, v0

    if-lez v0, :cond_9

    .line 79
    const/16 v0, 0x7f

    .line 84
    :goto_8
    return v0

    .line 81
    :cond_9
    const-wide/16 v0, -0x80

    cmp-long v0, p0, v0

    if-gez v0, :cond_12

    .line 82
    const/16 v0, -0x80

    goto :goto_8

    .line 84
    :cond_12
    long-to-int v0, p0

    int-to-byte v0, v0

    goto :goto_8
.end method

.method private static varargs b([B)B
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 133
    array-length v0, p0

    if-lez v0, :cond_17

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 134
    aget-byte v0, p0, v2

    .line 135
    :goto_b
    array-length v2, p0

    if-ge v1, v2, :cond_19

    .line 136
    aget-byte v2, p0, v1

    if-le v2, v0, :cond_14

    .line 137
    aget-byte v0, p0, v1

    .line 135
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_17
    move v0, v2

    .line 133
    goto :goto_6

    .line 140
    :cond_19
    return v0
.end method
