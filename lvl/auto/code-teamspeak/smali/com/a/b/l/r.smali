.class final Lcom/a/b/l/r;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final d:J


# instance fields
.field final a:[I

.field final b:I

.field final c:I


# direct methods
.method constructor <init>([I)V
    .registers 4

    .prologue
    .line 524
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/l/r;-><init>([III)V

    .line 525
    return-void
.end method

.method private constructor <init>([III)V
    .registers 4

    .prologue
    .line 527
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 528
    iput-object p1, p0, Lcom/a/b/l/r;->a:[I

    .line 529
    iput p2, p0, Lcom/a/b/l/r;->b:I

    .line 530
    iput p3, p0, Lcom/a/b/l/r;->c:I

    .line 531
    return-void
.end method

.method private a(I)Ljava/lang/Integer;
    .registers 4

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/a/b/l/r;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 543
    iget-object v0, p0, Lcom/a/b/l/r;->a:[I

    iget v1, p0, Lcom/a/b/l/r;->b:I

    add-int/2addr v1, p1

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/Integer;)Ljava/lang/Integer;
    .registers 7

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/a/b/l/r;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 576
    iget-object v0, p0, Lcom/a/b/l/r;->a:[I

    iget v1, p0, Lcom/a/b/l/r;->b:I

    add-int/2addr v1, p1

    aget v1, v0, v1

    .line 578
    iget-object v2, p0, Lcom/a/b/l/r;->a:[I

    iget v0, p0, Lcom/a/b/l/r;->b:I

    add-int v3, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v3

    .line 579
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private a()[I
    .registers 6

    .prologue
    .line 630
    invoke-virtual {p0}, Lcom/a/b/l/r;->size()I

    move-result v0

    .line 631
    new-array v1, v0, [I

    .line 632
    iget-object v2, p0, Lcom/a/b/l/r;->a:[I

    iget v3, p0, Lcom/a/b/l/r;->b:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 633
    return-object v1
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    .line 548
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/a/b/l/r;->a:[I

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/a/b/l/r;->b:I

    iget v3, p0, Lcom/a/b/l/r;->c:I

    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/q;->a([IIII)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_19

    const/4 v0, 0x1

    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 592
    if-ne p1, p0, :cond_5

    .line 608
    :cond_4
    :goto_4
    return v0

    .line 595
    :cond_5
    instance-of v2, p1, Lcom/a/b/l/r;

    if-eqz v2, :cond_2f

    .line 596
    check-cast p1, Lcom/a/b/l/r;

    .line 597
    invoke-virtual {p0}, Lcom/a/b/l/r;->size()I

    move-result v3

    .line 598
    invoke-virtual {p1}, Lcom/a/b/l/r;->size()I

    move-result v2

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 599
    goto :goto_4

    :cond_17
    move v2, v1

    .line 601
    :goto_18
    if-ge v2, v3, :cond_4

    .line 602
    iget-object v4, p0, Lcom/a/b/l/r;->a:[I

    iget v5, p0, Lcom/a/b/l/r;->b:I

    add-int/2addr v5, v2

    aget v4, v4, v5

    iget-object v5, p1, Lcom/a/b/l/r;->a:[I

    iget v6, p1, Lcom/a/b/l/r;->b:I

    add-int/2addr v6, v2

    aget v5, v5, v6

    if-eq v4, v5, :cond_2c

    move v0, v1

    .line 603
    goto :goto_4

    .line 601
    :cond_2c
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    .line 608
    :cond_2f
    invoke-super {p0, p1}, Ljava/util/AbstractList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 516
    .line 2542
    invoke-virtual {p0}, Lcom/a/b/l/r;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 2543
    iget-object v0, p0, Lcom/a/b/l/r;->a:[I

    iget v1, p0, Lcom/a/b/l/r;->b:I

    add-int/2addr v1, p1

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 516
    return-object v0
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 612
    const/4 v1, 0x1

    .line 613
    iget v0, p0, Lcom/a/b/l/r;->b:I

    :goto_3
    iget v2, p0, Lcom/a/b/l/r;->c:I

    if-ge v0, v2, :cond_15

    .line 614
    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lcom/a/b/l/r;->a:[I

    aget v2, v2, v0

    invoke-static {v2}, Lcom/a/b/l/q;->a(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 613
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 616
    :cond_15
    return v1
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 554
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_1a

    .line 555
    iget-object v0, p0, Lcom/a/b/l/r;->a:[I

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/a/b/l/r;->b:I

    iget v3, p0, Lcom/a/b/l/r;->c:I

    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/q;->a([IIII)I

    move-result v0

    .line 556
    if-ltz v0, :cond_1a

    .line 557
    iget v1, p0, Lcom/a/b/l/r;->b:I

    sub-int/2addr v0, v1

    .line 560
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 538
    const/4 v0, 0x0

    return v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 565
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_1a

    .line 566
    iget-object v0, p0, Lcom/a/b/l/r;->a:[I

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/a/b/l/r;->b:I

    iget v3, p0, Lcom/a/b/l/r;->c:I

    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/q;->b([IIII)I

    move-result v0

    .line 567
    if-ltz v0, :cond_1a

    .line 568
    iget v1, p0, Lcom/a/b/l/r;->b:I

    sub-int/2addr v0, v1

    .line 571
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 7

    .prologue
    .line 516
    check-cast p2, Ljava/lang/Integer;

    .line 1575
    invoke-virtual {p0}, Lcom/a/b/l/r;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 1576
    iget-object v0, p0, Lcom/a/b/l/r;->a:[I

    iget v1, p0, Lcom/a/b/l/r;->b:I

    add-int/2addr v1, p1

    aget v1, v0, v1

    .line 1578
    iget-object v2, p0, Lcom/a/b/l/r;->a:[I

    iget v0, p0, Lcom/a/b/l/r;->b:I

    add-int v3, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v3

    .line 1579
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 516
    return-object v0
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 534
    iget v0, p0, Lcom/a/b/l/r;->c:I

    iget v1, p0, Lcom/a/b/l/r;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final subList(II)Ljava/util/List;
    .registers 7

    .prologue
    .line 583
    invoke-virtual {p0}, Lcom/a/b/l/r;->size()I

    move-result v0

    .line 584
    invoke-static {p1, p2, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 585
    if-ne p1, p2, :cond_e

    .line 586
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 588
    :goto_d
    return-object v0

    :cond_e
    new-instance v0, Lcom/a/b/l/r;

    iget-object v1, p0, Lcom/a/b/l/r;->a:[I

    iget v2, p0, Lcom/a/b/l/r;->b:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/a/b/l/r;->b:I

    add-int/2addr v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/l/r;-><init>([III)V

    goto :goto_d
.end method

.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 620
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/a/b/l/r;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 621
    const/16 v0, 0x5b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/l/r;->a:[I

    iget v3, p0, Lcom/a/b/l/r;->b:I

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 622
    iget v0, p0, Lcom/a/b/l/r;->b:I

    add-int/lit8 v0, v0, 0x1

    :goto_1e
    iget v2, p0, Lcom/a/b/l/r;->c:I

    if-ge v0, v2, :cond_32

    .line 623
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/l/r;->a:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 622
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 625
    :cond_32
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
