.class final Lcom/a/b/l/g;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final d:J


# instance fields
.field final a:[C

.field final b:I

.field final c:I


# direct methods
.method constructor <init>([C)V
    .registers 4

    .prologue
    .line 479
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/l/g;-><init>([CII)V

    .line 480
    return-void
.end method

.method private constructor <init>([CII)V
    .registers 4

    .prologue
    .line 482
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 483
    iput-object p1, p0, Lcom/a/b/l/g;->a:[C

    .line 484
    iput p2, p0, Lcom/a/b/l/g;->b:I

    .line 485
    iput p3, p0, Lcom/a/b/l/g;->c:I

    .line 486
    return-void
.end method

.method private a(I)Ljava/lang/Character;
    .registers 4

    .prologue
    .line 497
    invoke-virtual {p0}, Lcom/a/b/l/g;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 498
    iget-object v0, p0, Lcom/a/b/l/g;->a:[C

    iget v1, p0, Lcom/a/b/l/g;->b:I

    add-int/2addr v1, p1

    aget-char v0, v0, v1

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/Character;)Ljava/lang/Character;
    .registers 7

    .prologue
    .line 530
    invoke-virtual {p0}, Lcom/a/b/l/g;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 531
    iget-object v0, p0, Lcom/a/b/l/g;->a:[C

    iget v1, p0, Lcom/a/b/l/g;->b:I

    add-int/2addr v1, p1

    aget-char v1, v0, v1

    .line 533
    iget-object v2, p0, Lcom/a/b/l/g;->a:[C

    iget v0, p0, Lcom/a/b/l/g;->b:I

    add-int v3, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    aput-char v0, v2, v3

    .line 534
    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    return-object v0
.end method

.method private a()[C
    .registers 6

    .prologue
    .line 585
    invoke-virtual {p0}, Lcom/a/b/l/g;->size()I

    move-result v0

    .line 586
    new-array v1, v0, [C

    .line 587
    iget-object v2, p0, Lcom/a/b/l/g;->a:[C

    iget v3, p0, Lcom/a/b/l/g;->b:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 588
    return-object v1
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    .line 503
    instance-of v0, p1, Ljava/lang/Character;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/a/b/l/g;->a:[C

    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v1

    iget v2, p0, Lcom/a/b/l/g;->b:I

    iget v3, p0, Lcom/a/b/l/g;->c:I

    .line 1051
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/f;->a([CCII)I

    move-result v0

    .line 503
    const/4 v1, -0x1

    if-eq v0, v1, :cond_19

    const/4 v0, 0x1

    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 547
    if-ne p1, p0, :cond_5

    .line 563
    :cond_4
    :goto_4
    return v0

    .line 550
    :cond_5
    instance-of v2, p1, Lcom/a/b/l/g;

    if-eqz v2, :cond_2f

    .line 551
    check-cast p1, Lcom/a/b/l/g;

    .line 552
    invoke-virtual {p0}, Lcom/a/b/l/g;->size()I

    move-result v3

    .line 553
    invoke-virtual {p1}, Lcom/a/b/l/g;->size()I

    move-result v2

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 554
    goto :goto_4

    :cond_17
    move v2, v1

    .line 556
    :goto_18
    if-ge v2, v3, :cond_4

    .line 557
    iget-object v4, p0, Lcom/a/b/l/g;->a:[C

    iget v5, p0, Lcom/a/b/l/g;->b:I

    add-int/2addr v5, v2

    aget-char v4, v4, v5

    iget-object v5, p1, Lcom/a/b/l/g;->a:[C

    iget v6, p1, Lcom/a/b/l/g;->b:I

    add-int/2addr v6, v2

    aget-char v5, v5, v6

    if-eq v4, v5, :cond_2c

    move v0, v1

    .line 558
    goto :goto_4

    .line 556
    :cond_2c
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    .line 563
    :cond_2f
    invoke-super {p0, p1}, Ljava/util/AbstractList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 471
    .line 4497
    invoke-virtual {p0}, Lcom/a/b/l/g;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 4498
    iget-object v0, p0, Lcom/a/b/l/g;->a:[C

    iget v1, p0, Lcom/a/b/l/g;->b:I

    add-int/2addr v1, p1

    aget-char v0, v0, v1

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    .line 471
    return-object v0
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 567
    const/4 v1, 0x1

    .line 568
    iget v0, p0, Lcom/a/b/l/g;->b:I

    :goto_3
    iget v2, p0, Lcom/a/b/l/g;->c:I

    if-ge v0, v2, :cond_11

    .line 569
    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lcom/a/b/l/g;->a:[C

    aget-char v2, v2, v0

    add-int/2addr v1, v2

    .line 568
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 571
    :cond_11
    return v1
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 509
    instance-of v0, p1, Ljava/lang/Character;

    if-eqz v0, :cond_1a

    .line 510
    iget-object v0, p0, Lcom/a/b/l/g;->a:[C

    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v1

    iget v2, p0, Lcom/a/b/l/g;->b:I

    iget v3, p0, Lcom/a/b/l/g;->c:I

    .line 2051
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/f;->a([CCII)I

    move-result v0

    .line 511
    if-ltz v0, :cond_1a

    .line 512
    iget v1, p0, Lcom/a/b/l/g;->b:I

    sub-int/2addr v0, v1

    .line 515
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 493
    const/4 v0, 0x0

    return v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 520
    instance-of v0, p1, Ljava/lang/Character;

    if-eqz v0, :cond_1a

    .line 521
    iget-object v0, p0, Lcom/a/b/l/g;->a:[C

    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v1

    iget v2, p0, Lcom/a/b/l/g;->b:I

    iget v3, p0, Lcom/a/b/l/g;->c:I

    .line 3051
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/f;->b([CCII)I

    move-result v0

    .line 522
    if-ltz v0, :cond_1a

    .line 523
    iget v1, p0, Lcom/a/b/l/g;->b:I

    sub-int/2addr v0, v1

    .line 526
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 7

    .prologue
    .line 471
    check-cast p2, Ljava/lang/Character;

    .line 3530
    invoke-virtual {p0}, Lcom/a/b/l/g;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 3531
    iget-object v0, p0, Lcom/a/b/l/g;->a:[C

    iget v1, p0, Lcom/a/b/l/g;->b:I

    add-int/2addr v1, p1

    aget-char v1, v0, v1

    .line 3533
    iget-object v2, p0, Lcom/a/b/l/g;->a:[C

    iget v0, p0, Lcom/a/b/l/g;->b:I

    add-int v3, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    aput-char v0, v2, v3

    .line 3534
    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    .line 471
    return-object v0
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 489
    iget v0, p0, Lcom/a/b/l/g;->c:I

    iget v1, p0, Lcom/a/b/l/g;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final subList(II)Ljava/util/List;
    .registers 7

    .prologue
    .line 538
    invoke-virtual {p0}, Lcom/a/b/l/g;->size()I

    move-result v0

    .line 539
    invoke-static {p1, p2, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 540
    if-ne p1, p2, :cond_e

    .line 541
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 543
    :goto_d
    return-object v0

    :cond_e
    new-instance v0, Lcom/a/b/l/g;

    iget-object v1, p0, Lcom/a/b/l/g;->a:[C

    iget v2, p0, Lcom/a/b/l/g;->b:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/a/b/l/g;->b:I

    add-int/2addr v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/l/g;-><init>([CII)V

    goto :goto_d
.end method

.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 575
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/a/b/l/g;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 576
    const/16 v0, 0x5b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/l/g;->a:[C

    iget v3, p0, Lcom/a/b/l/g;->b:I

    aget-char v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 577
    iget v0, p0, Lcom/a/b/l/g;->b:I

    add-int/lit8 v0, v0, 0x1

    :goto_1e
    iget v2, p0, Lcom/a/b/l/g;->c:I

    if-ge v0, v2, :cond_32

    .line 578
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/l/g;->a:[C

    aget-char v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 577
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 580
    :cond_32
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
