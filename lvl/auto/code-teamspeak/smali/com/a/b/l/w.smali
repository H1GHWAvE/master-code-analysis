.class final Lcom/a/b/l/w;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final d:J


# instance fields
.field final a:[J

.field final b:I

.field final c:I


# direct methods
.method constructor <init>([J)V
    .registers 4

    .prologue
    .line 557
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/l/w;-><init>([JII)V

    .line 558
    return-void
.end method

.method private constructor <init>([JII)V
    .registers 4

    .prologue
    .line 560
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 561
    iput-object p1, p0, Lcom/a/b/l/w;->a:[J

    .line 562
    iput p2, p0, Lcom/a/b/l/w;->b:I

    .line 563
    iput p3, p0, Lcom/a/b/l/w;->c:I

    .line 564
    return-void
.end method

.method private a(I)Ljava/lang/Long;
    .registers 4

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/a/b/l/w;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 576
    iget-object v0, p0, Lcom/a/b/l/w;->a:[J

    iget v1, p0, Lcom/a/b/l/w;->b:I

    add-int/2addr v1, p1

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/Long;)Ljava/lang/Long;
    .registers 11

    .prologue
    .line 608
    invoke-virtual {p0}, Lcom/a/b/l/w;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 609
    iget-object v0, p0, Lcom/a/b/l/w;->a:[J

    iget v1, p0, Lcom/a/b/l/w;->b:I

    add-int/2addr v1, p1

    aget-wide v2, v0, v1

    .line 611
    iget-object v1, p0, Lcom/a/b/l/w;->a:[J

    iget v0, p0, Lcom/a/b/l/w;->b:I

    add-int v4, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v1, v4

    .line 612
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method private a()[J
    .registers 6

    .prologue
    .line 663
    invoke-virtual {p0}, Lcom/a/b/l/w;->size()I

    move-result v0

    .line 664
    new-array v1, v0, [J

    .line 665
    iget-object v2, p0, Lcom/a/b/l/w;->a:[J

    iget v3, p0, Lcom/a/b/l/w;->b:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 666
    return-object v1
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 7

    .prologue
    .line 581
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/a/b/l/w;->a:[J

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget v1, p0, Lcom/a/b/l/w;->b:I

    iget v4, p0, Lcom/a/b/l/w;->c:I

    invoke-static {v0, v2, v3, v1, v4}, Lcom/a/b/l/u;->b([JJII)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_19

    const/4 v0, 0x1

    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 625
    if-ne p1, p0, :cond_5

    .line 641
    :cond_4
    :goto_4
    return v0

    .line 628
    :cond_5
    instance-of v2, p1, Lcom/a/b/l/w;

    if-eqz v2, :cond_31

    .line 629
    check-cast p1, Lcom/a/b/l/w;

    .line 630
    invoke-virtual {p0}, Lcom/a/b/l/w;->size()I

    move-result v3

    .line 631
    invoke-virtual {p1}, Lcom/a/b/l/w;->size()I

    move-result v2

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 632
    goto :goto_4

    :cond_17
    move v2, v1

    .line 634
    :goto_18
    if-ge v2, v3, :cond_4

    .line 635
    iget-object v4, p0, Lcom/a/b/l/w;->a:[J

    iget v5, p0, Lcom/a/b/l/w;->b:I

    add-int/2addr v5, v2

    aget-wide v4, v4, v5

    iget-object v6, p1, Lcom/a/b/l/w;->a:[J

    iget v7, p1, Lcom/a/b/l/w;->b:I

    add-int/2addr v7, v2

    aget-wide v6, v6, v7

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2e

    move v0, v1

    .line 636
    goto :goto_4

    .line 634
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    .line 641
    :cond_31
    invoke-super {p0, p1}, Ljava/util/AbstractList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 549
    .line 2575
    invoke-virtual {p0}, Lcom/a/b/l/w;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 2576
    iget-object v0, p0, Lcom/a/b/l/w;->a:[J

    iget v1, p0, Lcom/a/b/l/w;->b:I

    add-int/2addr v1, p1

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 549
    return-object v0
.end method

.method public final hashCode()I
    .registers 7

    .prologue
    .line 645
    const/4 v1, 0x1

    .line 646
    iget v0, p0, Lcom/a/b/l/w;->b:I

    :goto_3
    iget v2, p0, Lcom/a/b/l/w;->c:I

    if-ge v0, v2, :cond_17

    .line 647
    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lcom/a/b/l/w;->a:[J

    aget-wide v2, v2, v0

    .line 1078
    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v2, v2

    .line 647
    add-int/2addr v1, v2

    .line 646
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 649
    :cond_17
    return v1
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .registers 7

    .prologue
    .line 587
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_1a

    .line 588
    iget-object v0, p0, Lcom/a/b/l/w;->a:[J

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget v1, p0, Lcom/a/b/l/w;->b:I

    iget v4, p0, Lcom/a/b/l/w;->c:I

    invoke-static {v0, v2, v3, v1, v4}, Lcom/a/b/l/u;->b([JJII)I

    move-result v0

    .line 589
    if-ltz v0, :cond_1a

    .line 590
    iget v1, p0, Lcom/a/b/l/w;->b:I

    sub-int/2addr v0, v1

    .line 593
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 571
    const/4 v0, 0x0

    return v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .registers 7

    .prologue
    .line 598
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_1a

    .line 599
    iget-object v0, p0, Lcom/a/b/l/w;->a:[J

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget v1, p0, Lcom/a/b/l/w;->b:I

    iget v4, p0, Lcom/a/b/l/w;->c:I

    .line 1049
    invoke-static {v0, v2, v3, v1, v4}, Lcom/a/b/l/u;->a([JJII)I

    move-result v0

    .line 600
    if-ltz v0, :cond_1a

    .line 601
    iget v1, p0, Lcom/a/b/l/w;->b:I

    sub-int/2addr v0, v1

    .line 604
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 11

    .prologue
    .line 549
    check-cast p2, Ljava/lang/Long;

    .line 1608
    invoke-virtual {p0}, Lcom/a/b/l/w;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 1609
    iget-object v0, p0, Lcom/a/b/l/w;->a:[J

    iget v1, p0, Lcom/a/b/l/w;->b:I

    add-int/2addr v1, p1

    aget-wide v2, v0, v1

    .line 1611
    iget-object v1, p0, Lcom/a/b/l/w;->a:[J

    iget v0, p0, Lcom/a/b/l/w;->b:I

    add-int v4, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v1, v4

    .line 1612
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 549
    return-object v0
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 567
    iget v0, p0, Lcom/a/b/l/w;->c:I

    iget v1, p0, Lcom/a/b/l/w;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final subList(II)Ljava/util/List;
    .registers 7

    .prologue
    .line 616
    invoke-virtual {p0}, Lcom/a/b/l/w;->size()I

    move-result v0

    .line 617
    invoke-static {p1, p2, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 618
    if-ne p1, p2, :cond_e

    .line 619
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 621
    :goto_d
    return-object v0

    :cond_e
    new-instance v0, Lcom/a/b/l/w;

    iget-object v1, p0, Lcom/a/b/l/w;->a:[J

    iget v2, p0, Lcom/a/b/l/w;->b:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/a/b/l/w;->b:I

    add-int/2addr v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/l/w;-><init>([JII)V

    goto :goto_d
.end method

.method public final toString()Ljava/lang/String;
    .registers 7

    .prologue
    .line 653
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/a/b/l/w;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 654
    const/16 v0, 0x5b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/l/w;->a:[J

    iget v3, p0, Lcom/a/b/l/w;->b:I

    aget-wide v2, v2, v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 655
    iget v0, p0, Lcom/a/b/l/w;->b:I

    add-int/lit8 v0, v0, 0x1

    :goto_1e
    iget v2, p0, Lcom/a/b/l/w;->c:I

    if-ge v0, v2, :cond_32

    .line 656
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/l/w;->a:[J

    aget-wide v4, v3, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 655
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 658
    :cond_32
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
