.class final Lcom/a/b/l/n;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final d:J


# instance fields
.field final a:[F

.field final b:I

.field final c:I


# direct methods
.method constructor <init>([F)V
    .registers 4

    .prologue
    .line 462
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/l/n;-><init>([FII)V

    .line 463
    return-void
.end method

.method private constructor <init>([FII)V
    .registers 4

    .prologue
    .line 465
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 466
    iput-object p1, p0, Lcom/a/b/l/n;->a:[F

    .line 467
    iput p2, p0, Lcom/a/b/l/n;->b:I

    .line 468
    iput p3, p0, Lcom/a/b/l/n;->c:I

    .line 469
    return-void
.end method

.method private a(I)Ljava/lang/Float;
    .registers 4

    .prologue
    .line 480
    invoke-virtual {p0}, Lcom/a/b/l/n;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 481
    iget-object v0, p0, Lcom/a/b/l/n;->a:[F

    iget v1, p0, Lcom/a/b/l/n;->b:I

    add-int/2addr v1, p1

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/Float;)Ljava/lang/Float;
    .registers 7

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/a/b/l/n;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 514
    iget-object v0, p0, Lcom/a/b/l/n;->a:[F

    iget v1, p0, Lcom/a/b/l/n;->b:I

    add-int/2addr v1, p1

    aget v1, v0, v1

    .line 516
    iget-object v2, p0, Lcom/a/b/l/n;->a:[F

    iget v0, p0, Lcom/a/b/l/n;->b:I

    add-int v3, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v2, v3

    .line 517
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method private a()[F
    .registers 6

    .prologue
    .line 568
    invoke-virtual {p0}, Lcom/a/b/l/n;->size()I

    move-result v0

    .line 569
    new-array v1, v0, [F

    .line 570
    iget-object v2, p0, Lcom/a/b/l/n;->a:[F

    iget v3, p0, Lcom/a/b/l/n;->b:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 571
    return-object v1
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    .line 486
    instance-of v0, p1, Ljava/lang/Float;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/a/b/l/n;->a:[F

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v2, p0, Lcom/a/b/l/n;->b:I

    iget v3, p0, Lcom/a/b/l/n;->c:I

    .line 1054
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/m;->a([FFII)I

    move-result v0

    .line 486
    const/4 v1, -0x1

    if-eq v0, v1, :cond_19

    const/4 v0, 0x1

    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 530
    if-ne p1, p0, :cond_5

    .line 546
    :cond_4
    :goto_4
    return v0

    .line 533
    :cond_5
    instance-of v2, p1, Lcom/a/b/l/n;

    if-eqz v2, :cond_31

    .line 534
    check-cast p1, Lcom/a/b/l/n;

    .line 535
    invoke-virtual {p0}, Lcom/a/b/l/n;->size()I

    move-result v3

    .line 536
    invoke-virtual {p1}, Lcom/a/b/l/n;->size()I

    move-result v2

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 537
    goto :goto_4

    :cond_17
    move v2, v1

    .line 539
    :goto_18
    if-ge v2, v3, :cond_4

    .line 540
    iget-object v4, p0, Lcom/a/b/l/n;->a:[F

    iget v5, p0, Lcom/a/b/l/n;->b:I

    add-int/2addr v5, v2

    aget v4, v4, v5

    iget-object v5, p1, Lcom/a/b/l/n;->a:[F

    iget v6, p1, Lcom/a/b/l/n;->b:I

    add-int/2addr v6, v2

    aget v5, v5, v6

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_2e

    move v0, v1

    .line 541
    goto :goto_4

    .line 539
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    .line 546
    :cond_31
    invoke-super {p0, p1}, Ljava/util/AbstractList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 454
    .line 4480
    invoke-virtual {p0}, Lcom/a/b/l/n;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 4481
    iget-object v0, p0, Lcom/a/b/l/n;->a:[F

    iget v1, p0, Lcom/a/b/l/n;->b:I

    add-int/2addr v1, p1

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 454
    return-object v0
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 550
    const/4 v1, 0x1

    .line 551
    iget v0, p0, Lcom/a/b/l/n;->b:I

    :goto_3
    iget v2, p0, Lcom/a/b/l/n;->c:I

    if-ge v0, v2, :cond_19

    .line 552
    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lcom/a/b/l/n;->a:[F

    aget v2, v2, v0

    .line 3074
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->hashCode()I

    move-result v2

    .line 552
    add-int/2addr v1, v2

    .line 551
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 554
    :cond_19
    return v1
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 492
    instance-of v0, p1, Ljava/lang/Float;

    if-eqz v0, :cond_1a

    .line 493
    iget-object v0, p0, Lcom/a/b/l/n;->a:[F

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v2, p0, Lcom/a/b/l/n;->b:I

    iget v3, p0, Lcom/a/b/l/n;->c:I

    .line 2054
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/m;->a([FFII)I

    move-result v0

    .line 494
    if-ltz v0, :cond_1a

    .line 495
    iget v1, p0, Lcom/a/b/l/n;->b:I

    sub-int/2addr v0, v1

    .line 498
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 476
    const/4 v0, 0x0

    return v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 503
    instance-of v0, p1, Ljava/lang/Float;

    if-eqz v0, :cond_1a

    .line 504
    iget-object v0, p0, Lcom/a/b/l/n;->a:[F

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v2, p0, Lcom/a/b/l/n;->b:I

    iget v3, p0, Lcom/a/b/l/n;->c:I

    .line 3054
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/m;->b([FFII)I

    move-result v0

    .line 505
    if-ltz v0, :cond_1a

    .line 506
    iget v1, p0, Lcom/a/b/l/n;->b:I

    sub-int/2addr v0, v1

    .line 509
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 7

    .prologue
    .line 454
    check-cast p2, Ljava/lang/Float;

    .line 3513
    invoke-virtual {p0}, Lcom/a/b/l/n;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 3514
    iget-object v0, p0, Lcom/a/b/l/n;->a:[F

    iget v1, p0, Lcom/a/b/l/n;->b:I

    add-int/2addr v1, p1

    aget v1, v0, v1

    .line 3516
    iget-object v2, p0, Lcom/a/b/l/n;->a:[F

    iget v0, p0, Lcom/a/b/l/n;->b:I

    add-int v3, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v2, v3

    .line 3517
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 454
    return-object v0
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 472
    iget v0, p0, Lcom/a/b/l/n;->c:I

    iget v1, p0, Lcom/a/b/l/n;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final subList(II)Ljava/util/List;
    .registers 7

    .prologue
    .line 521
    invoke-virtual {p0}, Lcom/a/b/l/n;->size()I

    move-result v0

    .line 522
    invoke-static {p1, p2, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 523
    if-ne p1, p2, :cond_e

    .line 524
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 526
    :goto_d
    return-object v0

    :cond_e
    new-instance v0, Lcom/a/b/l/n;

    iget-object v1, p0, Lcom/a/b/l/n;->a:[F

    iget v2, p0, Lcom/a/b/l/n;->b:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/a/b/l/n;->b:I

    add-int/2addr v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/l/n;-><init>([FII)V

    goto :goto_d
.end method

.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 558
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/a/b/l/n;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0xc

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 559
    const/16 v0, 0x5b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/l/n;->a:[F

    iget v3, p0, Lcom/a/b/l/n;->b:I

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 560
    iget v0, p0, Lcom/a/b/l/n;->b:I

    add-int/lit8 v0, v0, 0x1

    :goto_1e
    iget v2, p0, Lcom/a/b/l/n;->c:I

    if-ge v0, v2, :cond_32

    .line 561
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/l/n;->a:[F

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 560
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 563
    :cond_32
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
