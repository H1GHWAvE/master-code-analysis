.class final Lcom/a/b/m/y;
.super Lcom/a/b/m/ax;
.source "SourceFile"


# static fields
.field private static final a:Lcom/a/b/m/ac;


# instance fields
.field private final b:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 310
    new-instance v0, Lcom/a/b/m/ac;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/a/b/m/ac;-><init>(B)V

    sput-object v0, Lcom/a/b/m/y;->a:Lcom/a/b/m/ac;

    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/a/b/m/ax;-><init>()V

    .line 312
    invoke-static {}, Lcom/a/b/d/sz;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/m/y;->b:Ljava/util/Map;

    return-void
.end method

.method static a(Ljava/lang/reflect/Type;)Lcom/a/b/d/jt;
    .registers 5

    .prologue
    .line 320
    new-instance v0, Lcom/a/b/m/y;

    invoke-direct {v0}, Lcom/a/b/m/y;-><init>()V

    .line 321
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    sget-object v3, Lcom/a/b/m/y;->a:Lcom/a/b/m/ac;

    invoke-virtual {v3, p0}, Lcom/a/b/m/ac;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/a/b/m/y;->a([Ljava/lang/reflect/Type;)V

    .line 322
    iget-object v0, v0, Lcom/a/b/m/y;->b:Ljava/util/Map;

    invoke-static {v0}, Lcom/a/b/d/jt;->a(Ljava/util/Map;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/a/b/m/ab;Ljava/lang/reflect/Type;)V
    .registers 5

    .prologue
    .line 351
    iget-object v0, p0, Lcom/a/b/m/y;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 371
    :cond_8
    :goto_8
    return-void

    :cond_9
    move-object v0, p2

    .line 360
    :goto_a
    if-eqz v0, :cond_2f

    .line 361
    invoke-virtual {p1, v0}, Lcom/a/b/m/ab;->b(Ljava/lang/reflect/Type;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 366
    :goto_12
    if-eqz p2, :cond_8

    iget-object v0, p0, Lcom/a/b/m/y;->b:Ljava/util/Map;

    invoke-static {p2}, Lcom/a/b/m/ab;->a(Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    move-object p2, v0

    goto :goto_12

    .line 360
    :cond_22
    iget-object v1, p0, Lcom/a/b/m/y;->b:Ljava/util/Map;

    invoke-static {v0}, Lcom/a/b/m/ab;->a(Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    goto :goto_a

    .line 370
    :cond_2f
    iget-object v0, p0, Lcom/a/b/m/y;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8
.end method


# virtual methods
.method final a(Ljava/lang/Class;)V
    .registers 5

    .prologue
    .line 326
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/reflect/Type;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/a/b/m/y;->a([Ljava/lang/reflect/Type;)V

    .line 327
    invoke-virtual {p1}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/m/y;->a([Ljava/lang/reflect/Type;)V

    .line 328
    return-void
.end method

.method final a(Ljava/lang/reflect/ParameterizedType;)V
    .registers 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 331
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 332
    invoke-virtual {v0}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v6

    .line 333
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v7

    .line 334
    array-length v1, v6

    array-length v4, v7

    if-ne v1, v4, :cond_46

    move v1, v2

    :goto_15
    invoke-static {v1}, Lcom/a/b/b/cn;->b(Z)V

    move v4, v3

    .line 335
    :goto_19
    array-length v1, v6

    if-ge v4, v1, :cond_5e

    .line 336
    new-instance v8, Lcom/a/b/m/ab;

    aget-object v1, v6, v4

    invoke-direct {v8, v1}, Lcom/a/b/m/ab;-><init>(Ljava/lang/reflect/TypeVariable;)V

    aget-object v5, v7, v4

    .line 1351
    iget-object v1, p0, Lcom/a/b/m/y;->b:Ljava/util/Map;

    invoke-interface {v1, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5a

    move-object v1, v5

    .line 1360
    :goto_2e
    if-eqz v1, :cond_55

    .line 1361
    invoke-virtual {v8, v1}, Lcom/a/b/m/ab;->b(Ljava/lang/reflect/Type;)Z

    move-result v9

    if-eqz v9, :cond_48

    move-object v1, v5

    .line 1366
    :goto_37
    if-eqz v1, :cond_5a

    iget-object v5, p0, Lcom/a/b/m/y;->b:Ljava/util/Map;

    invoke-static {v1}, Lcom/a/b/m/ab;->a(Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Type;

    goto :goto_37

    :cond_46
    move v1, v3

    .line 334
    goto :goto_15

    .line 1360
    :cond_48
    iget-object v9, p0, Lcom/a/b/m/y;->b:Ljava/util/Map;

    invoke-static {v1}, Lcom/a/b/m/ab;->a(Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Type;

    goto :goto_2e

    .line 1370
    :cond_55
    iget-object v1, p0, Lcom/a/b/m/y;->b:Ljava/util/Map;

    invoke-interface {v1, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    :cond_5a
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_19

    .line 338
    :cond_5e
    new-array v1, v2, [Ljava/lang/reflect/Type;

    aput-object v0, v1, v3

    invoke-virtual {p0, v1}, Lcom/a/b/m/y;->a([Ljava/lang/reflect/Type;)V

    .line 339
    new-array v0, v2, [Ljava/lang/reflect/Type;

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcom/a/b/m/y;->a([Ljava/lang/reflect/Type;)V

    .line 340
    return-void
.end method

.method final a(Ljava/lang/reflect/TypeVariable;)V
    .registers 3

    .prologue
    .line 343
    invoke-interface {p1}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/m/y;->a([Ljava/lang/reflect/Type;)V

    .line 344
    return-void
.end method

.method final a(Ljava/lang/reflect/WildcardType;)V
    .registers 3

    .prologue
    .line 347
    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/m/y;->a([Ljava/lang/reflect/Type;)V

    .line 348
    return-void
.end method
