.class public abstract Lcom/a/b/m/ae;
.super Lcom/a/b/m/u;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field final a:Ljava/lang/reflect/Type;

.field private transient b:Lcom/a/b/m/w;


# direct methods
.method protected constructor <init>()V
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 112
    invoke-direct {p0}, Lcom/a/b/m/u;-><init>()V

    .line 113
    invoke-virtual {p0}, Lcom/a/b/m/ae;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 114
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-nez v0, :cond_1e

    move v0, v1

    :goto_12
    const-string v3, "Cannot construct a TypeToken for a type variable.\nYou probably meant to call new TypeToken<%s>(getClass()) that can resolve the type variable for you.\nIf you do need to create a TypeToken of a type variable, please use TypeToken.of() instead."

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 120
    return-void

    :cond_1e
    move v0, v2

    .line 114
    goto :goto_12
.end method

.method private constructor <init>(Ljava/lang/Class;)V
    .registers 4

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/a/b/m/u;-><init>()V

    .line 140
    invoke-super {p0}, Lcom/a/b/m/u;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 141
    instance-of v1, v0, Ljava/lang/Class;

    if-eqz v1, :cond_e

    .line 142
    iput-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 146
    :goto_d
    return-void

    .line 144
    :cond_e
    invoke-static {p1}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/a/b/m/ae;->b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    iput-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    goto :goto_d
.end method

.method private constructor <init>(Ljava/lang/reflect/Type;)V
    .registers 3

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/a/b/m/u;-><init>()V

    .line 149
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    iput-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 150
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/reflect/Type;B)V
    .registers 3

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/a/b/m/ae;-><init>(Ljava/lang/reflect/Type;)V

    return-void
.end method

.method static a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
    .registers 6

    .prologue
    .line 344
    invoke-static {}, Lcom/a/b/d/jl;->h()Lcom/a/b/d/jn;

    move-result-object v1

    .line 345
    array-length v2, p0

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_20

    aget-object v3, p0, v0

    .line 347
    invoke-static {v3}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v3

    .line 6177
    iget-object v4, v3, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v4}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v4

    .line 348
    invoke-virtual {v4}, Ljava/lang/Class;->isInterface()Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 349
    invoke-virtual {v1, v3}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 345
    :cond_1d
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 352
    :cond_20
    invoke-virtual {v1}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/a/b/m/v;Lcom/a/b/m/ae;)Lcom/a/b/m/ae;
    .registers 6

    .prologue
    .line 215
    new-instance v0, Lcom/a/b/m/w;

    invoke-direct {v0}, Lcom/a/b/m/w;-><init>()V

    new-instance v1, Lcom/a/b/m/ab;

    iget-object v2, p1, Lcom/a/b/m/v;->a:Ljava/lang/reflect/TypeVariable;

    invoke-direct {v1, v2}, Lcom/a/b/m/ab;-><init>(Ljava/lang/reflect/TypeVariable;)V

    iget-object v2, p2, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v1, v2}, Lcom/a/b/d/jt;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/m/w;->a(Ljava/util/Map;)Lcom/a/b/m/w;

    move-result-object v0

    .line 220
    new-instance v1, Lcom/a/b/m/am;

    iget-object v2, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v2}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/b/m/am;-><init>(Ljava/lang/reflect/Type;)V

    return-object v1
.end method

.method private a(Lcom/a/b/m/v;Ljava/lang/Class;)Lcom/a/b/m/ae;
    .registers 7

    .prologue
    .line 239
    invoke-static {p2}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 1215
    new-instance v1, Lcom/a/b/m/w;

    invoke-direct {v1}, Lcom/a/b/m/w;-><init>()V

    new-instance v2, Lcom/a/b/m/ab;

    iget-object v3, p1, Lcom/a/b/m/v;->a:Ljava/lang/reflect/TypeVariable;

    invoke-direct {v2, v3}, Lcom/a/b/m/ab;-><init>(Ljava/lang/reflect/TypeVariable;)V

    iget-object v0, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v2, v0}, Lcom/a/b/d/jt;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/m/w;->a(Ljava/util/Map;)Lcom/a/b/m/w;

    move-result-object v0

    .line 1220
    new-instance v1, Lcom/a/b/m/am;

    iget-object v2, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v2}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/b/m/am;-><init>(Ljava/lang/reflect/Type;)V

    .line 239
    return-object v1
.end method

.method public static a(Ljava/lang/Class;)Lcom/a/b/m/ae;
    .registers 2

    .prologue
    .line 154
    new-instance v0, Lcom/a/b/m/am;

    invoke-direct {v0, p0}, Lcom/a/b/m/am;-><init>(Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method private a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
    .registers 9

    .prologue
    .line 974
    array-length v1, p2

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_1c

    aget-object v2, p2, v0

    .line 976
    invoke-static {v2}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v2

    .line 977
    invoke-static {p1}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/a/b/m/ae;->a(Lcom/a/b/m/ae;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 979
    invoke-direct {v2, p1}, Lcom/a/b/m/ae;->b(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 980
    return-object v0

    .line 974
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 983
    :cond_1c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x17

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " isn\'t a super type of "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
    .registers 2

    .prologue
    .line 159
    new-instance v0, Lcom/a/b/m/am;

    invoke-direct {v0, p0}, Lcom/a/b/m/am;-><init>(Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method private a(Ljava/lang/reflect/Constructor;)Lcom/a/b/m/k;
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 522
    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    .line 13177
    iget-object v3, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v3}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v3

    .line 522
    if-ne v0, v3, :cond_27

    move v0, v1

    :goto_f
    const-string v3, "%s not declared by %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    .line 14177
    iget-object v2, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v2}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v2

    .line 522
    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 524
    new-instance v0, Lcom/a/b/m/ag;

    invoke-direct {v0, p0, p1}, Lcom/a/b/m/ag;-><init>(Lcom/a/b/m/ae;Ljava/lang/reflect/Constructor;)V

    return-object v0

    :cond_27
    move v0, v2

    .line 522
    goto :goto_f
.end method

.method private a(Ljava/lang/reflect/Method;)Lcom/a/b/m/k;
    .registers 6

    .prologue
    .line 495
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/a/b/m/ae;->a(Lcom/a/b/m/ae;)Z

    move-result v0

    const-string v1, "%s not declared by %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 497
    new-instance v0, Lcom/a/b/m/af;

    invoke-direct {v0, p0, p1}, Lcom/a/b/m/af;-><init>(Lcom/a/b/m/ae;Ljava/lang/reflect/Method;)V

    return-object v0
.end method

.method private static a(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;
    .registers 5

    .prologue
    .line 885
    :goto_0
    invoke-interface {p0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    .line 886
    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_13

    .line 887
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 17878
    instance-of v1, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v1, :cond_18

    .line 17879
    check-cast v0, Ljava/lang/reflect/WildcardType;

    move-object p0, v0

    goto :goto_0

    .line 888
    :cond_13
    array-length v0, v0

    if-nez v0, :cond_19

    .line 889
    const-class v0, Ljava/lang/Object;

    :cond_18
    return-object v0

    .line 891
    :cond_19
    new-instance v0, Ljava/lang/AssertionError;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "There should be at most one upper bound for wildcard type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private static a(Ljava/lang/reflect/GenericArrayType;Ljava/lang/reflect/Type;)Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 853
    instance-of v1, p1, Ljava/lang/Class;

    if-eqz v1, :cond_20

    .line 854
    check-cast p1, Ljava/lang/Class;

    .line 855
    invoke-virtual {p1}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-nez v1, :cond_13

    .line 856
    const-class v1, Ljava/lang/Object;

    if-ne p1, v1, :cond_12

    const/4 v0, 0x1

    .line 863
    :cond_12
    :goto_12
    return v0

    .line 858
    :cond_13
    invoke-interface {p0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v0

    goto :goto_12

    .line 859
    :cond_20
    instance-of v1, p1, Ljava/lang/reflect/GenericArrayType;

    if-eqz v1, :cond_12

    .line 860
    check-cast p1, Ljava/lang/reflect/GenericArrayType;

    .line 861
    invoke-interface {p0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v0

    goto :goto_12
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Z
    .registers 3

    .prologue
    .line 788
    invoke-static {p0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/GenericArrayType;)Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 838
    instance-of v1, p0, Ljava/lang/Class;

    if-eqz v1, :cond_1b

    .line 839
    check-cast p0, Ljava/lang/Class;

    .line 840
    invoke-virtual {p0}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-nez v1, :cond_e

    .line 848
    :cond_d
    :goto_d
    return v0

    .line 843
    :cond_e
    invoke-virtual {p0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v0

    goto :goto_d

    .line 844
    :cond_1b
    instance-of v1, p0, Ljava/lang/reflect/GenericArrayType;

    if-eqz v1, :cond_d

    .line 845
    check-cast p0, Ljava/lang/reflect/GenericArrayType;

    .line 846
    invoke-interface {p0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v0

    goto :goto_d
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/ParameterizedType;)Z
    .registers 11

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 814
    invoke-static {p1}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 815
    invoke-static {p0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 834
    :cond_10
    :goto_10
    return v2

    .line 818
    :cond_11
    invoke-virtual {v0}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v4

    .line 819
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v5

    .line 820
    invoke-static {p0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v6

    move v1, v2

    .line 821
    :goto_1e
    array-length v0, v4

    if-ge v1, v0, :cond_45

    .line 829
    aget-object v0, v4, v1

    invoke-virtual {v6, v0}, Lcom/a/b/m/ae;->b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    iget-object v7, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 830
    aget-object v0, v5, v1

    .line 17868
    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_38

    move v0, v3

    .line 830
    :goto_32
    if-eqz v0, :cond_10

    .line 821
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1e

    .line 17871
    :cond_38
    instance-of v8, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v8, :cond_43

    .line 17872
    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-static {v7, v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/WildcardType;)Z

    move-result v0

    goto :goto_32

    :cond_43
    move v0, v2

    .line 17874
    goto :goto_32

    :cond_45
    move v2, v3

    .line 834
    goto :goto_10
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
    .registers 11

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 747
    move-object v0, p1

    move-object v1, p0

    :goto_4
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    move v2, v3

    .line 15831
    :cond_b
    :goto_b
    return v2

    .line 750
    :cond_c
    instance-of v4, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v4, :cond_17

    .line 751
    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-static {v1, v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/WildcardType;)Z

    move-result v2

    goto :goto_b

    .line 755
    :cond_17
    instance-of v4, v1, Ljava/lang/reflect/TypeVariable;

    if-eqz v4, :cond_26

    .line 756
    check-cast v1, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v1}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/m/ae;->a([Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v2

    goto :goto_b

    .line 760
    :cond_26
    instance-of v4, v1, Ljava/lang/reflect/WildcardType;

    if-eqz v4, :cond_35

    .line 761
    check-cast v1, Ljava/lang/reflect/WildcardType;

    invoke-interface {v1}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/m/ae;->a([Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v2

    goto :goto_b

    .line 763
    :cond_35
    instance-of v4, v1, Ljava/lang/reflect/GenericArrayType;

    if-eqz v4, :cond_65

    .line 764
    check-cast v1, Ljava/lang/reflect/GenericArrayType;

    .line 14853
    instance-of v4, v0, Ljava/lang/Class;

    if-eqz v4, :cond_56

    .line 14854
    check-cast v0, Ljava/lang/Class;

    .line 14855
    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-nez v4, :cond_4d

    .line 14856
    const-class v1, Ljava/lang/Object;

    if-ne v0, v1, :cond_b

    move v2, v3

    goto :goto_b

    .line 14858
    :cond_4d
    invoke-interface {v1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    goto :goto_4

    .line 14859
    :cond_56
    instance-of v4, v0, Ljava/lang/reflect/GenericArrayType;

    if-eqz v4, :cond_b

    .line 14860
    check-cast v0, Ljava/lang/reflect/GenericArrayType;

    .line 14861
    invoke-interface {v1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-interface {v0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto :goto_4

    .line 767
    :cond_65
    instance-of v4, v0, Ljava/lang/Class;

    if-eqz v4, :cond_74

    .line 768
    check-cast v0, Ljava/lang/Class;

    .line 15788
    invoke-static {v1}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    goto :goto_b

    .line 769
    :cond_74
    instance-of v4, v0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v4, :cond_bf

    .line 770
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 15814
    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v4

    .line 15815
    invoke-static {v1}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 15818
    invoke-virtual {v4}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v4

    .line 15819
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v5

    .line 15820
    invoke-static {v1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v6

    move v1, v2

    .line 15821
    :goto_95
    array-length v0, v4

    if-ge v1, v0, :cond_bc

    .line 15829
    aget-object v0, v4, v1

    invoke-virtual {v6, v0}, Lcom/a/b/m/ae;->b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    iget-object v7, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 15830
    aget-object v0, v5, v1

    .line 15868
    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_af

    move v0, v3

    .line 15830
    :goto_a9
    if-eqz v0, :cond_b

    .line 15821
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_95

    .line 15871
    :cond_af
    instance-of v8, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v8, :cond_ba

    .line 15872
    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-static {v7, v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/WildcardType;)Z

    move-result v0

    goto :goto_a9

    :cond_ba
    move v0, v2

    .line 15874
    goto :goto_a9

    :cond_bc
    move v2, v3

    .line 770
    goto/16 :goto_b

    .line 771
    :cond_bf
    instance-of v4, v0, Ljava/lang/reflect/GenericArrayType;

    if-eqz v4, :cond_b

    .line 772
    check-cast v0, Ljava/lang/reflect/GenericArrayType;

    .line 16838
    instance-of v4, v1, Ljava/lang/Class;

    if-eqz v4, :cond_db

    .line 16839
    check-cast v1, Ljava/lang/Class;

    .line 16840
    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 16843
    invoke-virtual {v1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_4

    .line 16844
    :cond_db
    instance-of v4, v1, Ljava/lang/reflect/GenericArrayType;

    if-eqz v4, :cond_b

    .line 16845
    check-cast v1, Ljava/lang/reflect/GenericArrayType;

    .line 16846
    invoke-interface {v1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-interface {v0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_4
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/WildcardType;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 798
    invoke-static {p1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 17802
    invoke-static {p1}, Lcom/a/b/m/ae;->b(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;

    move-result-object v2

    .line 17803
    if-nez v2, :cond_16

    move v2, v0

    .line 798
    :goto_13
    if-eqz v2, :cond_23

    :goto_15
    return v0

    .line 17806
    :cond_16
    invoke-static {p0}, Lcom/a/b/m/ae;->i(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v3

    .line 17807
    if-nez v3, :cond_1e

    move v2, v1

    .line 17808
    goto :goto_13

    .line 17810
    :cond_1e
    invoke-static {v2, v3}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v2

    goto :goto_13

    :cond_23
    move v0, v1

    .line 798
    goto :goto_15
.end method

.method private static a([Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 779
    array-length v2, p0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_e

    aget-object v3, p0, v1

    .line 780
    invoke-static {v3, p1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 781
    const/4 v0, 0x1

    .line 784
    :cond_e
    return v0

    .line 779
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method static synthetic a(Lcom/a/b/m/ae;[Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
    .registers 4

    .prologue
    .line 20259
    const/4 v0, 0x0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_11

    .line 20260
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/a/b/m/ae;->b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v1

    .line 21196
    iget-object v1, v1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 20260
    aput-object v1, p1, v0

    .line 20259
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 95
    :cond_11
    return-object p1
.end method

.method private b(Ljava/lang/Class;)Lcom/a/b/m/ae;
    .registers 8

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 376
    .line 7177
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 376
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    const-string v1, "%s is not a super class of %s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v3

    aput-object p0, v2, v4

    invoke-static {v0, v1, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 378
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_2b

    .line 379
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 390
    :goto_2a
    return-object v0

    .line 381
    :cond_2b
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_3e

    .line 382
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_2a

    .line 384
    :cond_3e
    invoke-virtual {p1}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_6b

    .line 8000
    invoke-direct {p0}, Lcom/a/b/m/ae;->n()Lcom/a/b/m/ae;

    move-result-object v0

    const-string v1, "%s isn\'t a super type of %s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v3

    aput-object p0, v2, v4

    invoke-static {v0, v1, v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/m/ae;

    .line 8004
    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/m/ae;->b(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 8006
    iget-object v0, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 8050
    sget-object v1, Lcom/a/b/m/bh;->b:Lcom/a/b/m/bh;

    invoke-virtual {v1, v0}, Lcom/a/b/m/bh;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 8006
    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_2a

    .line 388
    :cond_6b
    invoke-static {p1}, Lcom/a/b/m/ae;->d(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-virtual {p0, v0}, Lcom/a/b/m/ae;->c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_2a
.end method

.method private b(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
    .registers 9

    .prologue
    .line 987
    array-length v0, p2

    if-lez v0, :cond_f

    const/4 v0, 0x0

    aget-object v0, p2, v0

    .line 989
    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 991
    invoke-direct {v0, p1}, Lcom/a/b/m/ae;->c(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    return-object v0

    .line 993
    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " isn\'t a subclass of "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic b(Lcom/a/b/m/ae;)Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    return-object v0
.end method

.method private static b(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;
    .registers 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 905
    invoke-interface {p0}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    .line 906
    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_10

    .line 907
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/a/b/m/ae;->i(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 909
    :goto_f
    return-object v0

    .line 908
    :cond_10
    array-length v0, v0

    if-nez v0, :cond_15

    .line 909
    const/4 v0, 0x0

    goto :goto_f

    .line 911
    :cond_15
    new-instance v0, Ljava/lang/AssertionError;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Wildcard should have at most one lower bound: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private static b(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
    .registers 3

    .prologue
    .line 868
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 869
    const/4 v0, 0x1

    .line 874
    :goto_7
    return v0

    .line 871
    :cond_8
    instance-of v0, p1, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_13

    .line 872
    check-cast p1, Ljava/lang/reflect/WildcardType;

    invoke-static {p0, p1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/WildcardType;)Z

    move-result v0

    goto :goto_7

    .line 874
    :cond_13
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static b(Ljava/lang/reflect/Type;Ljava/lang/reflect/WildcardType;)Z
    .registers 4

    .prologue
    .line 802
    invoke-static {p1}, Lcom/a/b/m/ae;->b(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 803
    if-nez v0, :cond_8

    .line 804
    const/4 v0, 0x1

    .line 810
    :goto_7
    return v0

    .line 806
    :cond_8
    invoke-static {p0}, Lcom/a/b/m/ae;->i(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    .line 807
    if-nez v1, :cond_10

    .line 808
    const/4 v0, 0x0

    goto :goto_7

    .line 810
    :cond_10
    invoke-static {v0, v1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v0

    goto :goto_7
.end method

.method private b([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
    .registers 4

    .prologue
    .line 259
    const/4 v0, 0x0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_11

    .line 260
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/a/b/m/ae;->b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v1

    .line 2196
    iget-object v1, v1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 260
    aput-object v1, p1, v0

    .line 259
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 262
    :cond_11
    return-object p1
.end method

.method private static synthetic c(Lcom/a/b/m/ae;)Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 95
    .line 22190
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->f(Ljava/lang/reflect/Type;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 95
    return-object v0
.end method

.method private c(Ljava/lang/Class;)Lcom/a/b/m/ae;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 399
    :goto_2
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-nez v0, :cond_2a

    move v0, v1

    :goto_9
    const-string v3, "Cannot get subtype of type variable <%s>"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p0, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 401
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_64

    .line 402
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    .line 8987
    array-length v3, v0

    if-lez v3, :cond_2c

    aget-object v0, v0, v2

    .line 8989
    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object p0

    goto :goto_2

    :cond_2a
    move v0, v2

    .line 399
    goto :goto_9

    .line 8993
    :cond_2c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " isn\'t a subclass of "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9177
    :cond_64
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 404
    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    const-string v3, "%s isn\'t a subclass of %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    aput-object p0, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 9431
    invoke-direct {p0}, Lcom/a/b/m/ae;->n()Lcom/a/b/m/ae;

    move-result-object v0

    if-eqz v0, :cond_9b

    .line 407
    :goto_80
    if-eqz v1, :cond_9d

    .line 10014
    invoke-direct {p0}, Lcom/a/b/m/ae;->n()Lcom/a/b/m/ae;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/m/ae;->c(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 10017
    iget-object v0, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 10050
    sget-object v1, Lcom/a/b/m/bh;->b:Lcom/a/b/m/bh;

    invoke-virtual {v1, v0}, Lcom/a/b/m/bh;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 10017
    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 413
    :goto_9a
    return-object v0

    :cond_9b
    move v1, v2

    .line 9431
    goto :goto_80

    .line 11024
    :cond_9d
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/Class;

    if-eqz v0, :cond_a8

    .line 411
    :goto_a3
    invoke-static {p1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_9a

    .line 11035
    :cond_a8
    invoke-static {p1}, Lcom/a/b/m/ae;->d(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v2

    .line 11177
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 11037
    invoke-direct {v2, v0}, Lcom/a/b/m/ae;->b(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 11040
    new-instance v3, Lcom/a/b/m/w;

    invoke-direct {v3}, Lcom/a/b/m/w;-><init>()V

    iget-object v1, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 12091
    invoke-static {}, Lcom/a/b/d/sz;->c()Ljava/util/HashMap;

    move-result-object v4

    .line 12092
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {v1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Type;

    invoke-static {v4, v0, v1}, Lcom/a/b/m/w;->a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V

    .line 12093
    invoke-virtual {v3, v4}, Lcom/a/b/m/w;->a(Ljava/util/Map;)Lcom/a/b/m/w;

    move-result-object v0

    .line 11040
    iget-object v1, v2, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object p1

    goto :goto_a3
.end method

.method private static d(Ljava/lang/Class;)Lcom/a/b/m/ae;
    .registers 3
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 953
    invoke-virtual {p0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 954
    invoke-virtual {p0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->d(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ay;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 958
    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 968
    :goto_18
    return-object v0

    .line 961
    :cond_19
    invoke-virtual {p0}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v0

    .line 962
    array-length v1, v0

    if-lez v1, :cond_29

    .line 964
    invoke-static {p0, v0}, Lcom/a/b/m/ay;->a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_18

    .line 968
    :cond_29
    invoke-static {p0}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_18
.end method

.method static d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
    .registers 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305
    invoke-static {p0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 4177
    iget-object v1, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v1}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    .line 306
    invoke-virtual {v1}, Ljava/lang/Class;->isInterface()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 307
    const/4 v0, 0x0

    .line 311
    :cond_11
    return-object v0
.end method

.method private d()Ljava/lang/Class;
    .registers 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 180
    return-object v0
.end method

.method private e()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->f(Ljava/lang/reflect/Type;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 191
    return-object v0
.end method

.method private e(Ljava/lang/Class;)Lcom/a/b/m/ae;
    .registers 6

    .prologue
    .line 1000
    invoke-direct {p0}, Lcom/a/b/m/ae;->n()Lcom/a/b/m/ae;

    move-result-object v0

    const-string v1, "%s isn\'t a super type of %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/m/ae;

    .line 1004
    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/m/ae;->b(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 1006
    iget-object v0, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 18050
    sget-object v1, Lcom/a/b/m/bh;->b:Lcom/a/b/m/bh;

    invoke-virtual {v1, v0}, Lcom/a/b/m/bh;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 1006
    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 1009
    return-object v0
.end method

.method static e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
    .registers 2
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 918
    invoke-static {p0}, Lcom/a/b/m/ae;->f(Ljava/lang/reflect/Type;)Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->c()Lcom/a/b/d/agi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/agi;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    return-object v0
.end method

.method static f(Ljava/lang/reflect/Type;)Lcom/a/b/d/lo;
    .registers 5
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 922
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 923
    invoke-static {}, Lcom/a/b/d/lo;->i()Lcom/a/b/d/lp;

    move-result-object v0

    .line 924
    new-instance v1, Lcom/a/b/m/ai;

    invoke-direct {v1, v0}, Lcom/a/b/m/ai;-><init>(Lcom/a/b/d/lp;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/reflect/Type;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v1, v2}, Lcom/a/b/m/ai;->a([Ljava/lang/reflect/Type;)V

    .line 942
    invoke-virtual {v0}, Lcom/a/b/d/lp;->b()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method private f(Ljava/lang/Class;)Lcom/a/b/m/ae;
    .registers 4

    .prologue
    .line 1014
    invoke-direct {p0}, Lcom/a/b/m/ae;->n()Lcom/a/b/m/ae;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/m/ae;->c(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 1017
    iget-object v0, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 19050
    sget-object v1, Lcom/a/b/m/bh;->b:Lcom/a/b/m/bh;

    invoke-virtual {v1, v0}, Lcom/a/b/m/bh;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 1017
    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 1020
    return-object v0
.end method

.method private f()Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    return-object v0
.end method

.method private g()Lcom/a/b/m/ae;
    .registers 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 287
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_16

    .line 289
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/a/b/m/ae;->d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 301
    :goto_15
    return-object v0

    .line 291
    :cond_16
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_2b

    .line 293
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/a/b/m/ae;->d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_15

    .line 3177
    :cond_2b
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 295
    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 296
    if-nez v0, :cond_39

    .line 297
    const/4 v0, 0x0

    goto :goto_15

    .line 300
    :cond_39
    invoke-virtual {p0, v0}, Lcom/a/b/m/ae;->c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_15
.end method

.method private g(Ljava/lang/Class;)Ljava/lang/reflect/Type;
    .registers 7

    .prologue
    .line 1024
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/Class;

    if-eqz v0, :cond_7

    .line 1040
    :goto_6
    return-object p1

    .line 1035
    :cond_7
    invoke-static {p1}, Lcom/a/b/m/ae;->d(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v2

    .line 19177
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 1037
    invoke-direct {v2, v0}, Lcom/a/b/m/ae;->b(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 1040
    new-instance v3, Lcom/a/b/m/w;

    invoke-direct {v3}, Lcom/a/b/m/w;-><init>()V

    iget-object v1, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 20091
    invoke-static {}, Lcom/a/b/d/sz;->c()Ljava/util/HashMap;

    move-result-object v4

    .line 20092
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {v1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Type;

    invoke-static {v4, v0, v1}, Lcom/a/b/m/w;->a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V

    .line 20093
    invoke-virtual {v3, v4}, Lcom/a/b/m/w;->a(Ljava/util/Map;)Lcom/a/b/m/w;

    move-result-object v0

    .line 1040
    iget-object v1, v2, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object p1

    goto :goto_6
.end method

.method private g(Ljava/lang/reflect/Type;)Z
    .registers 4

    .prologue
    .line 423
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    iget-object v1, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0, v1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v0

    return v0
.end method

.method private h()Lcom/a/b/d/jl;
    .registers 6

    .prologue
    .line 327
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_13

    .line 328
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 340
    :goto_12
    return-object v0

    .line 330
    :cond_13
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_26

    .line 331
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_12

    .line 333
    :cond_26
    invoke-static {}, Lcom/a/b/d/jl;->h()Lcom/a/b/d/jn;

    move-result-object v1

    .line 5177
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 334
    invoke-virtual {v0}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_36
    if-ge v0, v3, :cond_44

    aget-object v4, v2, v0

    .line 336
    invoke-virtual {p0, v4}, Lcom/a/b/m/ae;->c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v4

    .line 338
    invoke-virtual {v1, v4}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 334
    add-int/lit8 v0, v0, 0x1

    goto :goto_36

    .line 340
    :cond_44
    invoke-virtual {v1}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_12
.end method

.method private static h(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 878
    instance-of v0, p0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_a

    .line 879
    check-cast p0, Ljava/lang/reflect/WildcardType;

    invoke-static {p0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;

    move-result-object p0

    .line 881
    :cond_a
    return-object p0
.end method

.method private static i(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .registers 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 897
    instance-of v0, p0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_a

    .line 898
    check-cast p0, Ljava/lang/reflect/WildcardType;

    invoke-static {p0}, Lcom/a/b/m/ae;->b(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/Type;

    move-result-object p0

    .line 900
    :cond_a
    return-object p0
.end method

.method private i()Z
    .registers 2

    .prologue
    .line 431
    invoke-direct {p0}, Lcom/a/b/m/ae;->n()Lcom/a/b/m/ae;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static j(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 1050
    sget-object v0, Lcom/a/b/m/bh;->b:Lcom/a/b/m/bh;

    invoke-virtual {v0, p0}, Lcom/a/b/m/bh;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method private j()Z
    .registers 2

    .prologue
    .line 440
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/Class;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private k()Lcom/a/b/m/ae;
    .registers 2

    .prologue
    .line 450
    .line 12440
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/Class;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v0

    if-eqz v0, :cond_20

    const/4 v0, 0x1

    .line 450
    :goto_11
    if-eqz v0, :cond_1f

    .line 452
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/Class;

    .line 453
    invoke-static {v0}, Lcom/a/b/l/z;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object p0

    .line 455
    :cond_1f
    return-object p0

    .line 12440
    :cond_20
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private l()Z
    .registers 3

    .prologue
    .line 459
    invoke-static {}, Lcom/a/b/l/z;->a()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private m()Lcom/a/b/m/ae;
    .registers 3

    .prologue
    .line 469
    .line 12459
    invoke-static {}, Lcom/a/b/l/z;->a()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 469
    if-eqz v0, :cond_18

    .line 471
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/Class;

    .line 472
    invoke-static {v0}, Lcom/a/b/l/z;->b(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object p0

    .line 474
    :cond_18
    return-object p0
.end method

.method private n()Lcom/a/b/m/ae;
    .registers 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 482
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ay;->c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 483
    if-nez v0, :cond_a

    .line 484
    const/4 v0, 0x0

    .line 486
    :goto_9
    return-object v0

    :cond_a
    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_9
.end method

.method private o()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 718
    new-instance v0, Lcom/a/b/m/w;

    invoke-direct {v0}, Lcom/a/b/m/w;-><init>()V

    iget-object v1, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/a/b/m/ae;)Z
    .registers 4

    .prologue
    .line 418
    iget-object v0, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 12423
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    iget-object v1, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0, v1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v0

    .line 418
    return v0
.end method

.method public final b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
    .registers 4

    .prologue
    .line 250
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    iget-object v0, p0, Lcom/a/b/m/ae;->b:Lcom/a/b/m/w;

    .line 252
    if-nez v0, :cond_18

    .line 253
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 2068
    new-instance v1, Lcom/a/b/m/w;

    invoke-direct {v1}, Lcom/a/b/m/w;-><init>()V

    invoke-static {v0}, Lcom/a/b/m/y;->a(Ljava/lang/reflect/Type;)Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/m/w;->a(Ljava/util/Map;)Lcom/a/b/m/w;

    move-result-object v0

    .line 253
    iput-object v0, p0, Lcom/a/b/m/ae;->b:Lcom/a/b/m/w;

    .line 255
    :cond_18
    invoke-virtual {v0, p1}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/a/b/m/aw;
    .registers 2

    .prologue
    .line 367
    new-instance v0, Lcom/a/b/m/aw;

    invoke-direct {v0, p0}, Lcom/a/b/m/aw;-><init>(Lcom/a/b/m/ae;)V

    return-object v0
.end method

.method final c()Lcom/a/b/m/ae;
    .registers 5

    .prologue
    .line 726
    new-instance v0, Lcom/a/b/m/ah;

    invoke-direct {v0, p0}, Lcom/a/b/m/ah;-><init>(Lcom/a/b/m/ae;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/a/b/m/ah;->a([Ljava/lang/reflect/Type;)V

    .line 743
    return-object p0
.end method

.method final c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
    .registers 4

    .prologue
    .line 266
    invoke-virtual {p0, p1}, Lcom/a/b/m/ae;->b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 268
    iget-object v1, p0, Lcom/a/b/m/ae;->b:Lcom/a/b/m/w;

    iput-object v1, v0, Lcom/a/b/m/ae;->b:Lcom/a/b/m/w;

    .line 269
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 699
    instance-of v0, p1, Lcom/a/b/m/ae;

    if-eqz v0, :cond_f

    .line 700
    check-cast p1, Lcom/a/b/m/ae;

    .line 701
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    iget-object v1, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 703
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 707
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 711
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ay;->b(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
