.class public final Lcom/a/b/m/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/reflect/AnnotatedElement;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private final a:Lcom/a/b/m/k;

.field private final b:I

.field private final c:Lcom/a/b/m/ae;

.field private final d:Lcom/a/b/d/jl;


# direct methods
.method constructor <init>(Lcom/a/b/m/k;ILcom/a/b/m/ae;[Ljava/lang/annotation/Annotation;)V
    .registers 6

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/a/b/m/s;->a:Lcom/a/b/m/k;

    .line 50
    iput p2, p0, Lcom/a/b/m/s;->b:I

    .line 51
    iput-object p3, p0, Lcom/a/b/m/s;->c:Lcom/a/b/m/ae;

    .line 52
    invoke-static {p4}, Lcom/a/b/d/jl;->a([Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/m/s;->d:Lcom/a/b/d/jl;

    .line 53
    return-void
.end method

.method private a()Lcom/a/b/m/ae;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/a/b/m/s;->c:Lcom/a/b/m/ae;

    return-object v0
.end method

.method private a(Ljava/lang/Class;)[Ljava/lang/annotation/Annotation;
    .registers 3

    .prologue
    .line 90
    .line 1120
    iget-object v0, p0, Lcom/a/b/m/s;->d:Lcom/a/b/d/jl;

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/gd;->a(Ljava/lang/Class;)Lcom/a/b/d/gd;

    move-result-object v0

    .line 1474
    iget-object v0, v0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    .line 1120
    check-cast v0, [Ljava/lang/annotation/Annotation;

    .line 90
    return-object v0
.end method

.method private b()Lcom/a/b/m/k;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/a/b/m/s;->a:Lcom/a/b/m/k;

    return-object v0
.end method

.method private b(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .registers 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 107
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    iget-object v0, p0, Lcom/a/b/m/s;->d:Lcom/a/b/d/jl;

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/gd;->a(Ljava/lang/Class;)Lcom/a/b/d/gd;

    move-result-object v0

    .line 2275
    iget-object v0, v0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 2276
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_28

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/ci;->b(Ljava/lang/Object;)Lcom/a/b/b/ci;

    move-result-object v0

    .line 108
    :goto_21
    invoke-virtual {v0}, Lcom/a/b/b/ci;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;

    return-object v0

    .line 2276
    :cond_28
    invoke-static {}, Lcom/a/b/b/ci;->f()Lcom/a/b/b/ci;

    move-result-object v0

    goto :goto_21
.end method

.method private c(Ljava/lang/Class;)[Ljava/lang/annotation/Annotation;
    .registers 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/a/b/m/s;->d:Lcom/a/b/d/jl;

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/gd;->a(Ljava/lang/Class;)Lcom/a/b/d/gd;

    move-result-object v0

    .line 2474
    iget-object v0, v0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    .line 120
    check-cast v0, [Ljava/lang/annotation/Annotation;

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 126
    instance-of v1, p1, Lcom/a/b/m/s;

    if-eqz v1, :cond_18

    .line 127
    check-cast p1, Lcom/a/b/m/s;

    .line 128
    iget v1, p0, Lcom/a/b/m/s;->b:I

    iget v2, p1, Lcom/a/b/m/s;->b:I

    if-ne v1, v2, :cond_18

    iget-object v1, p0, Lcom/a/b/m/s;->a:Lcom/a/b/m/k;

    iget-object v2, p1, Lcom/a/b/m/s;->a:Lcom/a/b/m/k;

    invoke-virtual {v1, v2}, Lcom/a/b/m/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    const/4 v0, 0x1

    .line 130
    :cond_18
    return v0
.end method

.method public final getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .registers 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 72
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, Lcom/a/b/m/s;->d:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;

    .line 74
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 75
    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;

    .line 78
    :goto_21
    return-object v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public final getAnnotations()[Ljava/lang/annotation/Annotation;
    .registers 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/a/b/m/s;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public final getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;
    .registers 3

    .prologue
    .line 98
    iget-object v0, p0, Lcom/a/b/m/s;->d:Lcom/a/b/d/jl;

    iget-object v1, p0, Lcom/a/b/m/s;->d:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/annotation/Annotation;

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/annotation/Annotation;

    return-object v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 134
    iget v0, p0, Lcom/a/b/m/s;->b:I

    return v0
.end method

.method public final isAnnotationPresent(Ljava/lang/Class;)Z
    .registers 3

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/a/b/m/s;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 138
    iget-object v0, p0, Lcom/a/b/m/s;->c:Lcom/a/b/m/ae;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/a/b/m/s;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " arg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
