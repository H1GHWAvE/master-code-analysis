.class final Lcom/a/b/m/ap;
.super Lcom/a/b/m/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 1086
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/m/an;-><init>(B)V

    return-void
.end method

.method private static a(Ljava/lang/Class;)Ljava/lang/Class;
    .registers 1

    .prologue
    .line 1088
    return-object p0
.end method

.method private static b(Ljava/lang/Class;)Ljava/lang/Iterable;
    .registers 2

    .prologue
    .line 1092
    invoke-virtual {p0}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/lang/Class;)Ljava/lang/Class;
    .registers 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1097
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final bridge synthetic b(Ljava/lang/Object;)Ljava/lang/Class;
    .registers 2

    .prologue
    .line 1086
    check-cast p1, Ljava/lang/Class;

    return-object p1
.end method

.method final synthetic c(Ljava/lang/Object;)Ljava/lang/Iterable;
    .registers 3

    .prologue
    .line 1086
    check-cast p1, Ljava/lang/Class;

    .line 3092
    invoke-virtual {p1}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1086
    return-object v0
.end method

.method final synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1086
    check-cast p1, Ljava/lang/Class;

    .line 2097
    invoke-virtual {p1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 1086
    return-object v0
.end method
