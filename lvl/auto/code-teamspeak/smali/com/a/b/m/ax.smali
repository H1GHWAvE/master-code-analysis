.class abstract Lcom/a/b/m/ax;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:Ljava/util/Set;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 63
    iput-object v0, p0, Lcom/a/b/m/ax;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method a(Ljava/lang/Class;)V
    .registers 2

    .prologue
    .line 99
    return-void
.end method

.method a(Ljava/lang/reflect/GenericArrayType;)V
    .registers 2

    .prologue
    .line 100
    return-void
.end method

.method a(Ljava/lang/reflect/ParameterizedType;)V
    .registers 2

    .prologue
    .line 101
    return-void
.end method

.method a(Ljava/lang/reflect/TypeVariable;)V
    .registers 2

    .prologue
    .line 102
    return-void
.end method

.method a(Ljava/lang/reflect/WildcardType;)V
    .registers 2

    .prologue
    .line 103
    return-void
.end method

.method public final varargs a([Ljava/lang/reflect/Type;)V
    .registers 8

    .prologue
    .line 70
    array-length v4, p1

    const/4 v1, 0x0

    move v3, v1

    :goto_3
    if-ge v3, v4, :cond_7e

    aget-object v2, p1, v3

    .line 71
    if-eqz v2, :cond_1c

    iget-object v1, p0, Lcom/a/b/m/ax;->a:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 77
    :try_start_11
    instance-of v1, v2, Ljava/lang/reflect/TypeVariable;

    if-eqz v1, :cond_20

    .line 78
    move-object v0, v2

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lcom/a/b/m/ax;->a(Ljava/lang/reflect/TypeVariable;)V

    .line 70
    :cond_1c
    :goto_1c
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 79
    :cond_20
    instance-of v1, v2, Ljava/lang/reflect/WildcardType;

    if-eqz v1, :cond_33

    .line 80
    move-object v0, v2

    check-cast v0, Ljava/lang/reflect/WildcardType;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lcom/a/b/m/ax;->a(Ljava/lang/reflect/WildcardType;)V
    :try_end_2b
    .catchall {:try_start_11 .. :try_end_2b} :catchall_2c

    goto :goto_1c

    .line 92
    :catchall_2c
    move-exception v1

    .line 93
    iget-object v3, p0, Lcom/a/b/m/ax;->a:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    throw v1

    .line 81
    :cond_33
    :try_start_33
    instance-of v1, v2, Ljava/lang/reflect/ParameterizedType;

    if-eqz v1, :cond_3f

    .line 82
    move-object v0, v2

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lcom/a/b/m/ax;->a(Ljava/lang/reflect/ParameterizedType;)V

    goto :goto_1c

    .line 83
    :cond_3f
    instance-of v1, v2, Ljava/lang/Class;

    if-eqz v1, :cond_4b

    .line 84
    move-object v0, v2

    check-cast v0, Ljava/lang/Class;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lcom/a/b/m/ax;->a(Ljava/lang/Class;)V

    goto :goto_1c

    .line 85
    :cond_4b
    instance-of v1, v2, Ljava/lang/reflect/GenericArrayType;

    if-eqz v1, :cond_57

    .line 86
    move-object v0, v2

    check-cast v0, Ljava/lang/reflect/GenericArrayType;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lcom/a/b/m/ax;->a(Ljava/lang/reflect/GenericArrayType;)V

    goto :goto_1c

    .line 88
    :cond_57
    new-instance v1, Ljava/lang/AssertionError;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xe

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unknown type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_7e
    .catchall {:try_start_33 .. :try_end_7e} :catchall_2c

    .line 97
    :cond_7e
    return-void
.end method
