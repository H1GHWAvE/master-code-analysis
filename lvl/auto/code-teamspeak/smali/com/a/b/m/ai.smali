.class final Lcom/a/b/m/ai;
.super Lcom/a/b/m/ax;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/lp;


# direct methods
.method constructor <init>(Lcom/a/b/d/lp;)V
    .registers 2

    .prologue
    .line 924
    iput-object p1, p0, Lcom/a/b/m/ai;->a:Lcom/a/b/d/lp;

    invoke-direct {p0}, Lcom/a/b/m/ax;-><init>()V

    return-void
.end method


# virtual methods
.method final a(Ljava/lang/Class;)V
    .registers 3

    .prologue
    .line 935
    iget-object v0, p0, Lcom/a/b/m/ai;->a:Lcom/a/b/d/lp;

    invoke-virtual {v0, p1}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    .line 936
    return-void
.end method

.method final a(Ljava/lang/reflect/GenericArrayType;)V
    .registers 4

    .prologue
    .line 938
    iget-object v0, p0, Lcom/a/b/m/ai;->a:Lcom/a/b/d/lp;

    invoke-interface {p1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/m/ay;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    .line 939
    return-void
.end method

.method final a(Ljava/lang/reflect/ParameterizedType;)V
    .registers 4

    .prologue
    .line 932
    iget-object v1, p0, Lcom/a/b/m/ai;->a:Lcom/a/b/d/lp;

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v1, v0}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    .line 933
    return-void
.end method

.method final a(Ljava/lang/reflect/TypeVariable;)V
    .registers 3

    .prologue
    .line 926
    invoke-interface {p1}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/m/ai;->a([Ljava/lang/reflect/Type;)V

    .line 927
    return-void
.end method

.method final a(Ljava/lang/reflect/WildcardType;)V
    .registers 3

    .prologue
    .line 929
    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/m/ai;->a([Ljava/lang/reflect/Type;)V

    .line 930
    return-void
.end method
