.class final Lcom/a/b/m/bp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/reflect/WildcardType;


# static fields
.field private static final c:J


# instance fields
.field private final a:Lcom/a/b/d/jl;

.field private final b:Lcom/a/b/d/jl;


# direct methods
.method constructor <init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
    .registers 4

    .prologue
    .line 381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 382
    const-string v0, "lower bound for wildcard"

    invoke-static {p1, v0}, Lcom/a/b/m/ay;->a([Ljava/lang/reflect/Type;Ljava/lang/String;)V

    .line 383
    const-string v0, "upper bound for wildcard"

    invoke-static {p2, v0}, Lcom/a/b/m/ay;->a([Ljava/lang/reflect/Type;Ljava/lang/String;)V

    .line 384
    sget-object v0, Lcom/a/b/m/bh;->d:Lcom/a/b/m/bh;

    invoke-virtual {v0, p1}, Lcom/a/b/m/bh;->a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/m/bp;->a:Lcom/a/b/d/jl;

    .line 385
    sget-object v0, Lcom/a/b/m/bh;->d:Lcom/a/b/m/bh;

    invoke-virtual {v0, p2}, Lcom/a/b/m/bh;->a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/m/bp;->b:Lcom/a/b/d/jl;

    .line 386
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 397
    instance-of v1, p1, Ljava/lang/reflect/WildcardType;

    if-eqz v1, :cond_28

    .line 398
    check-cast p1, Ljava/lang/reflect/WildcardType;

    .line 399
    iget-object v1, p0, Lcom/a/b/m/bp;->a:Lcom/a/b/d/jl;

    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/a/b/d/jl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_28

    iget-object v1, p0, Lcom/a/b/m/bp;->b:Lcom/a/b/d/jl;

    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/a/b/d/jl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_28

    const/4 v0, 0x1

    .line 402
    :cond_28
    return v0
.end method

.method public final getLowerBounds()[Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 389
    iget-object v0, p0, Lcom/a/b/m/bp;->a:Lcom/a/b/d/jl;

    invoke-static {v0}, Lcom/a/b/m/ay;->a(Ljava/util/Collection;)[Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method public final getUpperBounds()[Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 393
    iget-object v0, p0, Lcom/a/b/m/bp;->b:Lcom/a/b/d/jl;

    invoke-static {v0}, Lcom/a/b/m/ay;->a(Ljava/util/Collection;)[Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 406
    iget-object v0, p0, Lcom/a/b/m/bp;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/m/bp;->b:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 410
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "?"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 411
    iget-object v0, p0, Lcom/a/b/m/bp;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    .line 412
    const-string v3, " super "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/a/b/m/bh;->d:Lcom/a/b/m/bh;

    invoke-virtual {v4, v0}, Lcom/a/b/m/bh;->c(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_d

    .line 414
    :cond_29
    iget-object v0, p0, Lcom/a/b/m/bp;->b:Lcom/a/b/d/jl;

    invoke-static {v0}, Lcom/a/b/m/ay;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_33
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    .line 415
    const-string v3, " extends "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/a/b/m/bh;->d:Lcom/a/b/m/bh;

    invoke-virtual {v4, v0}, Lcom/a/b/m/bh;->c(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_33

    .line 417
    :cond_4f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
