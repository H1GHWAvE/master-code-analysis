.class Lcom/a/b/m/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/b/d/jt;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    invoke-static {}, Lcom/a/b/d/jt;->k()Lcom/a/b/d/jt;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/m/z;->a:Lcom/a/b/d/jt;

    .line 219
    return-void
.end method

.method private constructor <init>(Lcom/a/b/d/jt;)V
    .registers 2

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    iput-object p1, p0, Lcom/a/b/m/z;->a:Lcom/a/b/d/jt;

    .line 223
    return-void
.end method

.method private a(Ljava/lang/reflect/TypeVariable;)Ljava/lang/reflect/Type;
    .registers 3

    .prologue
    .line 239
    .line 240
    new-instance v0, Lcom/a/b/m/aa;

    invoke-direct {v0, p0, p1, p0}, Lcom/a/b/m/aa;-><init>(Lcom/a/b/m/z;Ljava/lang/reflect/TypeVariable;Lcom/a/b/m/z;)V

    .line 249
    invoke-virtual {p0, p1, v0}, Lcom/a/b/m/z;->a(Ljava/lang/reflect/TypeVariable;Lcom/a/b/m/z;)Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a(Ljava/util/Map;)Lcom/a/b/m/z;
    .registers 11

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 227
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v5

    .line 228
    iget-object v0, p0, Lcom/a/b/m/z;->a:Lcom/a/b/d/jt;

    invoke-virtual {v5, v0}, Lcom/a/b/d/ju;->a(Ljava/util/Map;)Lcom/a/b/d/ju;

    .line 229
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_13
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 230
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/m/ab;

    .line 231
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    .line 232
    invoke-virtual {v1, v0}, Lcom/a/b/m/ab;->b(Ljava/lang/reflect/Type;)Z

    move-result v2

    if-nez v2, :cond_3f

    move v2, v3

    :goto_32
    const-string v7, "Type variable %s bound to itself"

    new-array v8, v3, [Ljava/lang/Object;

    aput-object v1, v8, v4

    invoke-static {v2, v7, v8}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 233
    invoke-virtual {v5, v1, v0}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    goto :goto_13

    :cond_3f
    move v2, v4

    .line 232
    goto :goto_32

    .line 235
    :cond_41
    new-instance v0, Lcom/a/b/m/z;

    invoke-virtual {v5}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/m/z;-><init>(Lcom/a/b/d/jt;)V

    return-object v0
.end method

.method a(Ljava/lang/reflect/TypeVariable;Lcom/a/b/m/z;)Ljava/lang/reflect/Type;
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 261
    iget-object v0, p0, Lcom/a/b/m/z;->a:Lcom/a/b/d/jt;

    new-instance v1, Lcom/a/b/m/ab;

    invoke-direct {v1, p1}, Lcom/a/b/m/ab;-><init>(Ljava/lang/reflect/TypeVariable;)V

    invoke-virtual {v0, v1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    .line 262
    if-nez v0, :cond_38

    .line 263
    invoke-interface {p1}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    .line 264
    array-length v1, v0

    if-nez v1, :cond_18

    .line 304
    :cond_17
    :goto_17
    return-object p1

    .line 267
    :cond_18
    new-instance v1, Lcom/a/b/m/w;

    invoke-direct {v1, p2, v2}, Lcom/a/b/m/w;-><init>(Lcom/a/b/m/z;B)V

    .line 1055
    invoke-virtual {v1, v0}, Lcom/a/b/m/w;->a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 296
    sget-boolean v2, Lcom/a/b/m/bm;->a:Z

    if-eqz v2, :cond_2b

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 300
    :cond_2b
    invoke-interface {p1}, Ljava/lang/reflect/TypeVariable;->getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/a/b/m/ay;->a(Ljava/lang/reflect/GenericDeclaration;Ljava/lang/String;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/TypeVariable;

    move-result-object p1

    goto :goto_17

    .line 304
    :cond_38
    new-instance v1, Lcom/a/b/m/w;

    invoke-direct {v1, p2, v2}, Lcom/a/b/m/w;-><init>(Lcom/a/b/m/z;B)V

    invoke-virtual {v1, v0}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object p1

    goto :goto_17
.end method
