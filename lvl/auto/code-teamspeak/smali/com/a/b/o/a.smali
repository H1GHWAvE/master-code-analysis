.class public final Lcom/a/b/o/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:C = '\u0000'

.field private static final b:C = '\u001f'

.field private static final c:Lcom/a/b/e/g;

.field private static final d:Lcom/a/b/e/g;

.field private static final e:Lcom/a/b/e/g;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/16 v5, 0xd

    const/16 v4, 0xa

    const/16 v3, 0x9

    const/4 v0, 0x0

    .line 120
    invoke-static {}, Lcom/a/b/e/i;->a()Lcom/a/b/e/l;

    move-result-object v1

    .line 1115
    iput-char v0, v1, Lcom/a/b/e/l;->a:C

    .line 1116
    const v2, 0xfffd

    iput-char v2, v1, Lcom/a/b/e/l;->b:C

    .line 126
    const-string v2, "\ufffd"

    .line 1130
    iput-object v2, v1, Lcom/a/b/e/l;->c:Ljava/lang/String;

    .line 138
    :goto_16
    const/16 v2, 0x1f

    if-gt v0, v2, :cond_29

    .line 139
    if-eq v0, v3, :cond_25

    if-eq v0, v4, :cond_25

    if-eq v0, v5, :cond_25

    .line 140
    const-string v2, "\ufffd"

    invoke-virtual {v1, v0, v2}, Lcom/a/b/e/l;->a(CLjava/lang/String;)Lcom/a/b/e/l;

    .line 138
    :cond_25
    add-int/lit8 v0, v0, 0x1

    int-to-char v0, v0

    goto :goto_16

    .line 146
    :cond_29
    const/16 v0, 0x26

    const-string v2, "&amp;"

    invoke-virtual {v1, v0, v2}, Lcom/a/b/e/l;->a(CLjava/lang/String;)Lcom/a/b/e/l;

    .line 147
    const/16 v0, 0x3c

    const-string v2, "&lt;"

    invoke-virtual {v1, v0, v2}, Lcom/a/b/e/l;->a(CLjava/lang/String;)Lcom/a/b/e/l;

    .line 148
    const/16 v0, 0x3e

    const-string v2, "&gt;"

    invoke-virtual {v1, v0, v2}, Lcom/a/b/e/l;->a(CLjava/lang/String;)Lcom/a/b/e/l;

    .line 149
    invoke-virtual {v1}, Lcom/a/b/e/l;->a()Lcom/a/b/e/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/o/a;->d:Lcom/a/b/e/g;

    .line 150
    const/16 v0, 0x27

    const-string v2, "&apos;"

    invoke-virtual {v1, v0, v2}, Lcom/a/b/e/l;->a(CLjava/lang/String;)Lcom/a/b/e/l;

    .line 151
    const/16 v0, 0x22

    const-string v2, "&quot;"

    invoke-virtual {v1, v0, v2}, Lcom/a/b/e/l;->a(CLjava/lang/String;)Lcom/a/b/e/l;

    .line 152
    invoke-virtual {v1}, Lcom/a/b/e/l;->a()Lcom/a/b/e/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/o/a;->c:Lcom/a/b/e/g;

    .line 153
    const-string v0, "&#x9;"

    invoke-virtual {v1, v3, v0}, Lcom/a/b/e/l;->a(CLjava/lang/String;)Lcom/a/b/e/l;

    .line 154
    const-string v0, "&#xA;"

    invoke-virtual {v1, v4, v0}, Lcom/a/b/e/l;->a(CLjava/lang/String;)Lcom/a/b/e/l;

    .line 155
    const-string v0, "&#xD;"

    invoke-virtual {v1, v5, v0}, Lcom/a/b/e/l;->a(CLjava/lang/String;)Lcom/a/b/e/l;

    .line 156
    invoke-virtual {v1}, Lcom/a/b/e/l;->a()Lcom/a/b/e/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/o/a;->e:Lcom/a/b/e/g;

    .line 157
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()Lcom/a/b/e/g;
    .registers 1

    .prologue
    .line 86
    sget-object v0, Lcom/a/b/o/a;->d:Lcom/a/b/e/g;

    return-object v0
.end method

.method private static b()Lcom/a/b/e/g;
    .registers 1

    .prologue
    .line 113
    sget-object v0, Lcom/a/b/o/a;->e:Lcom/a/b/e/g;

    return-object v0
.end method
