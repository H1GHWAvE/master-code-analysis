.class final Lcom/a/b/c/cq;
.super Ljava/util/AbstractQueue;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/c/bs;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 3594
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 3595
    new-instance v0, Lcom/a/b/c/cr;

    invoke-direct {v0, p0}, Lcom/a/b/c/cr;-><init>(Lcom/a/b/c/cq;)V

    iput-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    return-void
.end method

.method private a()Lcom/a/b/c/bs;
    .registers 3

    .prologue
    .line 3646
    iget-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    invoke-interface {v0}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v0

    .line 3647
    iget-object v1, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    if-ne v0, v1, :cond_b

    const/4 v0, 0x0

    :cond_b
    return-object v0
.end method

.method private a(Lcom/a/b/c/bs;)Z
    .registers 4

    .prologue
    .line 3635
    invoke-interface {p1}, Lcom/a/b/c/bs;->j()Lcom/a/b/c/bs;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/c/ao;->b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V

    .line 3638
    iget-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    invoke-interface {v0}, Lcom/a/b/c/bs;->j()Lcom/a/b/c/bs;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/c/ao;->b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V

    .line 3639
    iget-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    invoke-static {p1, v0}, Lcom/a/b/c/ao;->b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V

    .line 3641
    const/4 v0, 0x1

    return v0
.end method

.method private b()Lcom/a/b/c/bs;
    .registers 3

    .prologue
    .line 3652
    iget-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    invoke-interface {v0}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v0

    .line 3653
    iget-object v1, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    if-ne v0, v1, :cond_c

    .line 3654
    const/4 v0, 0x0

    .line 3658
    :goto_b
    return-object v0

    .line 3657
    :cond_c
    invoke-virtual {p0, v0}, Lcom/a/b/c/cq;->remove(Ljava/lang/Object;)Z

    goto :goto_b
.end method


# virtual methods
.method public final clear()V
    .registers 3

    .prologue
    .line 3697
    iget-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    invoke-interface {v0}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v0

    .line 3698
    :goto_6
    iget-object v1, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    if-eq v0, v1, :cond_13

    .line 3699
    invoke-interface {v0}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v1

    .line 3700
    invoke-static {v0}, Lcom/a/b/c/ao;->b(Lcom/a/b/c/bs;)V

    move-object v0, v1

    .line 3702
    goto :goto_6

    .line 3704
    :cond_13
    iget-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    iget-object v1, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    invoke-interface {v0, v1}, Lcom/a/b/c/bs;->c(Lcom/a/b/c/bs;)V

    .line 3705
    iget-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    iget-object v1, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    invoke-interface {v0, v1}, Lcom/a/b/c/bs;->d(Lcom/a/b/c/bs;)V

    .line 3706
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 3676
    check-cast p1, Lcom/a/b/c/bs;

    .line 3677
    invoke-interface {p1}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v0

    sget-object v1, Lcom/a/b/c/br;->a:Lcom/a/b/c/br;

    if-eq v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final isEmpty()Z
    .registers 3

    .prologue
    .line 3682
    iget-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    invoke-interface {v0}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 3710
    new-instance v0, Lcom/a/b/c/cs;

    invoke-direct {p0}, Lcom/a/b/c/cq;->a()Lcom/a/b/c/bs;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/c/cs;-><init>(Lcom/a/b/c/cq;Lcom/a/b/c/bs;)V

    return-object v0
.end method

.method public final synthetic offer(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 3594
    check-cast p1, Lcom/a/b/c/bs;

    .line 5635
    invoke-interface {p1}, Lcom/a/b/c/bs;->j()Lcom/a/b/c/bs;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/c/ao;->b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V

    .line 5638
    iget-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    invoke-interface {v0}, Lcom/a/b/c/bs;->j()Lcom/a/b/c/bs;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/c/ao;->b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V

    .line 5639
    iget-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    invoke-static {p1, v0}, Lcom/a/b/c/ao;->b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V

    .line 5641
    const/4 v0, 0x1

    .line 3594
    return v0
.end method

.method public final synthetic peek()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3594
    invoke-direct {p0}, Lcom/a/b/c/cq;->a()Lcom/a/b/c/bs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic poll()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3594
    .line 4652
    iget-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    invoke-interface {v0}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v0

    .line 4653
    iget-object v1, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    if-ne v0, v1, :cond_c

    .line 4654
    const/4 v0, 0x0

    :goto_b
    return-object v0

    .line 4657
    :cond_c
    invoke-virtual {p0, v0}, Lcom/a/b/c/cq;->remove(Ljava/lang/Object;)Z

    goto :goto_b
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 3664
    check-cast p1, Lcom/a/b/c/bs;

    .line 3665
    invoke-interface {p1}, Lcom/a/b/c/bs;->j()Lcom/a/b/c/bs;

    move-result-object v0

    .line 3666
    invoke-interface {p1}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v1

    .line 3667
    invoke-static {v0, v1}, Lcom/a/b/c/ao;->b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V

    .line 3668
    invoke-static {p1}, Lcom/a/b/c/ao;->b(Lcom/a/b/c/bs;)V

    .line 3670
    sget-object v0, Lcom/a/b/c/br;->a:Lcom/a/b/c/br;

    if-eq v1, v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public final size()I
    .registers 4

    .prologue
    .line 3687
    const/4 v1, 0x0

    .line 3688
    iget-object v0, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    invoke-interface {v0}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v0

    :goto_7
    iget-object v2, p0, Lcom/a/b/c/cq;->a:Lcom/a/b/c/bs;

    if-eq v0, v2, :cond_12

    .line 3690
    add-int/lit8 v1, v1, 0x1

    .line 3689
    invoke-interface {v0}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v0

    goto :goto_7

    .line 3692
    :cond_12
    return v1
.end method
