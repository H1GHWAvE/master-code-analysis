.class final Lcom/a/b/c/dn;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final p:Lsun/misc/Unsafe;

.field private static final q:J


# instance fields
.field volatile a:J

.field volatile b:J

.field volatile c:J

.field volatile d:J

.field volatile e:J

.field volatile f:J

.field volatile g:J

.field volatile h:J

.field volatile i:J

.field volatile j:J

.field volatile k:J

.field volatile l:J

.field volatile m:J

.field volatile n:J

.field volatile o:J


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 108
    :try_start_0
    invoke-static {}, Lcom/a/b/c/dl;->d()Lsun/misc/Unsafe;

    move-result-object v0

    sput-object v0, Lcom/a/b/c/dn;->p:Lsun/misc/Unsafe;

    .line 109
    const-class v0, Lcom/a/b/c/dn;

    .line 110
    sget-object v1, Lcom/a/b/c/dn;->p:Lsun/misc/Unsafe;

    const-string v2, "value"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {v1, v0}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v0

    sput-wide v0, Lcom/a/b/c/dn;->q:J
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_16} :catch_17

    .line 114
    return-void

    .line 112
    :catch_17
    move-exception v0

    .line 113
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method constructor <init>(J)V
    .registers 4

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/a/b/c/dn;->h:J

    return-void
.end method


# virtual methods
.method final a(JJ)Z
    .registers 14

    .prologue
    .line 100
    sget-object v0, Lcom/a/b/c/dn;->p:Lsun/misc/Unsafe;

    sget-wide v2, Lcom/a/b/c/dn;->q:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v0 .. v7}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v0

    return v0
.end method
