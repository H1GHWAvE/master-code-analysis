.class public abstract Lcom/a/b/c/aj;
.super Lcom/a/b/d/hg;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/c/e;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/a/b/d/hg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;)Lcom/a/b/d/jt;
    .registers 3

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/c/e;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/c/e;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .registers 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/c/e;->a()V

    .line 125
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/c/e;->a(Ljava/lang/Object;)V

    .line 92
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/c/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .registers 3

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/c/e;->a(Ljava/util/Map;)V

    .line 87
    return-void
.end method

.method public final b()J
    .registers 3

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/c/e;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public final b(Ljava/lang/Iterable;)V
    .registers 3

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/c/e;->b(Ljava/lang/Iterable;)V

    .line 100
    return-void
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/c/e;->c()V

    .line 105
    return-void
.end method

.method public final d()Lcom/a/b/c/ai;
    .registers 2

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/c/e;->d()Lcom/a/b/c/ai;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/c/e;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/util/concurrent/ConcurrentMap;
    .registers 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/c/e;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    return-object v0
.end method

.method protected abstract f()Lcom/a/b/c/e;
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/a/b/c/aj;->f()Lcom/a/b/c/e;

    move-result-object v0

    return-object v0
.end method
