.class final Lcom/a/b/c/bl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/c/cg;


# instance fields
.field volatile a:Lcom/a/b/c/cg;

.field final b:Lcom/a/b/n/a/fq;

.field final c:Lcom/a/b/b/dw;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 3474
    invoke-static {}, Lcom/a/b/c/ao;->j()Lcom/a/b/c/cg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/c/bl;-><init>(Lcom/a/b/c/cg;)V

    .line 3475
    return-void
.end method

.method public constructor <init>(Lcom/a/b/c/cg;)V
    .registers 3

    .prologue
    .line 3477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3470
    invoke-static {}, Lcom/a/b/n/a/fq;->a()Lcom/a/b/n/a/fq;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/c/bl;->b:Lcom/a/b/n/a/fq;

    .line 4089
    new-instance v0, Lcom/a/b/b/dw;

    invoke-direct {v0}, Lcom/a/b/b/dw;-><init>()V

    .line 3471
    iput-object v0, p0, Lcom/a/b/c/bl;->c:Lcom/a/b/b/dw;

    .line 3478
    iput-object p1, p0, Lcom/a/b/c/bl;->a:Lcom/a/b/c/cg;

    .line 3479
    return-void
.end method

.method private static b(Ljava/lang/Throwable;)Lcom/a/b/n/a/dp;
    .registers 2

    .prologue
    .line 3505
    invoke-static {p0}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Throwable;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method private g()Lcom/a/b/c/cg;
    .registers 2

    .prologue
    .line 3566
    iget-object v0, p0, Lcom/a/b/c/bl;->a:Lcom/a/b/c/cg;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 3493
    iget-object v0, p0, Lcom/a/b/c/bl;->a:Lcom/a/b/c/cg;

    invoke-interface {v0}, Lcom/a/b/c/cg;->a()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/a/b/c/bs;)Lcom/a/b/c/cg;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3577
    return-object p0
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/c/ab;)Lcom/a/b/n/a/dp;
    .registers 5

    .prologue
    .line 3523
    iget-object v0, p0, Lcom/a/b/c/bl;->c:Lcom/a/b/b/dw;

    invoke-virtual {v0}, Lcom/a/b/b/dw;->b()Lcom/a/b/b/dw;

    .line 3524
    iget-object v0, p0, Lcom/a/b/c/bl;->a:Lcom/a/b/c/cg;

    invoke-interface {v0}, Lcom/a/b/c/cg;->get()Ljava/lang/Object;

    move-result-object v0

    .line 3526
    if-nez v0, :cond_1f

    .line 3527
    :try_start_d
    invoke-virtual {p2, p1}, Lcom/a/b/c/ab;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 3528
    invoke-virtual {p0, v0}, Lcom/a/b/c/bl;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    iget-object v0, p0, Lcom/a/b/c/bl;->b:Lcom/a/b/n/a/fq;

    .line 3547
    :goto_19
    return-object v0

    .line 3528
    :cond_1a
    invoke-static {v0}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Object;)Lcom/a/b/n/a/dp;

    move-result-object v0

    goto :goto_19

    .line 3530
    :cond_1f
    invoke-virtual {p2, p1, v0}, Lcom/a/b/c/ab;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/n/a/dp;

    move-result-object v0

    .line 3531
    if-nez v0, :cond_2b

    .line 3532
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Object;)Lcom/a/b/n/a/dp;

    move-result-object v0

    goto :goto_19

    .line 3536
    :cond_2b
    new-instance v1, Lcom/a/b/c/bm;

    invoke-direct {v1, p0}, Lcom/a/b/c/bm;-><init>(Lcom/a/b/c/bl;)V

    invoke-static {v0, v1}, Lcom/a/b/n/a/ci;->a(Lcom/a/b/n/a/dp;Lcom/a/b/b/bj;)Lcom/a/b/n/a/dp;
    :try_end_33
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_33} :catch_35

    move-result-object v0

    goto :goto_19

    .line 3543
    :catch_35
    move-exception v0

    .line 3544
    instance-of v1, v0, Ljava/lang/InterruptedException;

    if-eqz v1, :cond_41

    .line 3545
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 3547
    :cond_41
    invoke-virtual {p0, v0}, Lcom/a/b/c/bl;->a(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_4a

    iget-object v0, p0, Lcom/a/b/c/bl;->b:Lcom/a/b/n/a/fq;

    goto :goto_19

    .line 4505
    :cond_4a
    invoke-static {v0}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Throwable;)Lcom/a/b/n/a/dp;

    move-result-object v0

    goto :goto_19
.end method

.method public final a(Ljava/lang/Object;)V
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3510
    if-eqz p1, :cond_6

    .line 3513
    invoke-virtual {p0, p1}, Lcom/a/b/c/bl;->b(Ljava/lang/Object;)Z

    .line 3520
    :goto_5
    return-void

    .line 3516
    :cond_6
    invoke-static {}, Lcom/a/b/c/ao;->j()Lcom/a/b/c/cg;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/c/bl;->a:Lcom/a/b/c/cg;

    goto :goto_5
.end method

.method public final a(Ljava/lang/Throwable;)Z
    .registers 3

    .prologue
    .line 3501
    iget-object v0, p0, Lcom/a/b/c/bl;->b:Lcom/a/b/n/a/fq;

    invoke-virtual {v0, p1}, Lcom/a/b/n/a/fq;->a(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public final b()Lcom/a/b/c/bs;
    .registers 2

    .prologue
    .line 3571
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3497
    iget-object v0, p0, Lcom/a/b/c/bl;->b:Lcom/a/b/n/a/fq;

    invoke-virtual {v0, p1}, Lcom/a/b/n/a/fq;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 3483
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Z
    .registers 2

    .prologue
    .line 3488
    iget-object v0, p0, Lcom/a/b/c/bl;->a:Lcom/a/b/c/cg;

    invoke-interface {v0}, Lcom/a/b/c/cg;->d()Z

    move-result v0

    return v0
.end method

.method public final e()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3557
    iget-object v0, p0, Lcom/a/b/c/bl;->b:Lcom/a/b/n/a/fq;

    invoke-static {v0}, Lcom/a/b/n/a/gs;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final f()J
    .registers 3

    .prologue
    .line 3552
    iget-object v0, p0, Lcom/a/b/c/bl;->c:Lcom/a/b/b/dw;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lcom/a/b/b/dw;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final get()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3562
    iget-object v0, p0, Lcom/a/b/c/bl;->a:Lcom/a/b/c/cg;

    invoke-interface {v0}, Lcom/a/b/c/cg;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
