.class abstract Lcom/a/b/c/dl;
.super Ljava/lang/Number;
.source "SourceFile"


# static fields
.field static final a:Ljava/lang/ThreadLocal;

.field static final b:Ljava/util/Random;

.field static final c:I

.field private static final g:Lsun/misc/Unsafe;

.field private static final h:J

.field private static final i:J


# instance fields
.field volatile transient d:[Lcom/a/b/c/dn;

.field volatile transient e:J

.field volatile transient f:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 125
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lcom/a/b/c/dl;->a:Ljava/lang/ThreadLocal;

    .line 130
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/a/b/c/dl;->b:Ljava/util/Random;

    .line 133
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sput v0, Lcom/a/b/c/dl;->c:I

    .line 298
    :try_start_18
    invoke-static {}, Lcom/a/b/c/dl;->b()Lsun/misc/Unsafe;

    move-result-object v0

    sput-object v0, Lcom/a/b/c/dl;->g:Lsun/misc/Unsafe;

    .line 299
    const-class v0, Lcom/a/b/c/dl;

    .line 300
    sget-object v1, Lcom/a/b/c/dl;->g:Lsun/misc/Unsafe;

    const-string v2, "base"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    invoke-virtual {v1, v2}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v2

    sput-wide v2, Lcom/a/b/c/dl;->h:J

    .line 302
    sget-object v1, Lcom/a/b/c/dl;->g:Lsun/misc/Unsafe;

    const-string v2, "busy"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {v1, v0}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v0

    sput-wide v0, Lcom/a/b/c/dl;->i:J
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_3c} :catch_3d

    .line 306
    return-void

    .line 304
    :catch_3d
    move-exception v0

    .line 305
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Number;-><init>()V

    .line 155
    return-void
.end method

.method private a()V
    .registers 7

    .prologue
    const-wide/16 v4, 0x0

    .line 280
    iget-object v1, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    .line 281
    iput-wide v4, p0, Lcom/a/b/c/dl;->e:J

    .line 282
    if-eqz v1, :cond_15

    .line 283
    array-length v2, v1

    .line 284
    const/4 v0, 0x0

    :goto_a
    if-ge v0, v2, :cond_15

    .line 285
    aget-object v3, v1, v0

    .line 286
    if-eqz v3, :cond_12

    .line 287
    iput-wide v4, v3, Lcom/a/b/c/dn;->h:J

    .line 284
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 290
    :cond_15
    return-void
.end method

.method private a(J[IZ)V
    .registers 16

    .prologue
    .line 195
    if-nez p3, :cond_53

    .line 196
    sget-object v0, Lcom/a/b/c/dl;->a:Ljava/lang/ThreadLocal;

    const/4 v1, 0x1

    new-array p3, v1, [I

    invoke-virtual {v0, p3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 197
    sget-object v0, Lcom/a/b/c/dl;->b:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 198
    const/4 v1, 0x0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :cond_14
    aput v0, p3, v1

    .line 202
    :goto_16
    const/4 v1, 0x0

    move v10, v1

    move v1, v0

    move v0, v10

    .line 205
    :cond_1a
    :goto_1a
    iget-object v3, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    if-eqz v3, :cond_b1

    array-length v2, v3

    if-lez v2, :cond_b1

    .line 206
    add-int/lit8 v4, v2, -0x1

    and-int/2addr v4, v1

    aget-object v4, v3, v4

    if-nez v4, :cond_6a

    .line 207
    iget v2, p0, Lcom/a/b/c/dl;->f:I

    if-nez v2, :cond_5c

    .line 208
    new-instance v3, Lcom/a/b/c/dn;

    invoke-direct {v3, p1, p2}, Lcom/a/b/c/dn;-><init>(J)V

    .line 209
    iget v2, p0, Lcom/a/b/c/dl;->f:I

    if-nez v2, :cond_5c

    invoke-virtual {p0}, Lcom/a/b/c/dl;->c()Z

    move-result v2

    if-eqz v2, :cond_5c

    .line 210
    const/4 v2, 0x0

    .line 213
    :try_start_3c
    iget-object v4, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    if-eqz v4, :cond_4d

    array-length v5, v4

    if-lez v5, :cond_4d

    add-int/lit8 v5, v5, -0x1

    and-int/2addr v5, v1

    aget-object v6, v4, v5

    if-nez v6, :cond_4d

    .line 216
    aput-object v3, v4, v5
    :try_end_4c
    .catchall {:try_start_3c .. :try_end_4c} :catchall_57

    .line 217
    const/4 v2, 0x1

    .line 220
    :cond_4d
    const/4 v3, 0x0

    iput v3, p0, Lcom/a/b/c/dl;->f:I

    .line 222
    if-eqz v2, :cond_1a

    .line 274
    :cond_52
    :goto_52
    return-void

    .line 201
    :cond_53
    const/4 v0, 0x0

    aget v0, p3, v0

    goto :goto_16

    .line 220
    :catchall_57
    move-exception v0

    const/4 v1, 0x0

    iput v1, p0, Lcom/a/b/c/dl;->f:I

    throw v0

    .line 227
    :cond_5c
    const/4 v0, 0x0

    .line 251
    :cond_5d
    :goto_5d
    shl-int/lit8 v2, v1, 0xd

    xor-int/2addr v1, v2

    .line 252
    ushr-int/lit8 v2, v1, 0x11

    xor-int/2addr v1, v2

    .line 253
    shl-int/lit8 v2, v1, 0x5

    xor-int/2addr v1, v2

    .line 254
    const/4 v2, 0x0

    aput v1, p3, v2

    goto :goto_1a

    .line 229
    :cond_6a
    if-nez p4, :cond_6e

    .line 230
    const/4 p4, 0x1

    goto :goto_5d

    .line 231
    :cond_6e
    iget-wide v6, v4, Lcom/a/b/c/dn;->h:J

    invoke-virtual {p0, v6, v7, p1, p2}, Lcom/a/b/c/dl;->a(JJ)J

    move-result-wide v8

    invoke-virtual {v4, v6, v7, v8, v9}, Lcom/a/b/c/dn;->a(JJ)Z

    move-result v4

    if-nez v4, :cond_52

    .line 233
    sget v4, Lcom/a/b/c/dl;->c:I

    if-ge v2, v4, :cond_82

    iget-object v4, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    if-eq v4, v3, :cond_84

    .line 234
    :cond_82
    const/4 v0, 0x0

    goto :goto_5d

    .line 235
    :cond_84
    if-nez v0, :cond_88

    .line 236
    const/4 v0, 0x1

    goto :goto_5d

    .line 237
    :cond_88
    iget v4, p0, Lcom/a/b/c/dl;->f:I

    if-nez v4, :cond_5d

    invoke-virtual {p0}, Lcom/a/b/c/dl;->c()Z

    move-result v4

    if-eqz v4, :cond_5d

    .line 239
    :try_start_92
    iget-object v0, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    if-ne v0, v3, :cond_a6

    .line 240
    shl-int/lit8 v0, v2, 0x1

    new-array v4, v0, [Lcom/a/b/c/dn;

    .line 241
    const/4 v0, 0x0

    :goto_9b
    if-ge v0, v2, :cond_a4

    .line 242
    aget-object v5, v3, v0

    aput-object v5, v4, v0

    .line 241
    add-int/lit8 v0, v0, 0x1

    goto :goto_9b

    .line 243
    :cond_a4
    iput-object v4, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;
    :try_end_a6
    .catchall {:try_start_92 .. :try_end_a6} :catchall_ac

    .line 246
    :cond_a6
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/c/dl;->f:I

    .line 248
    const/4 v0, 0x0

    .line 249
    goto/16 :goto_1a

    .line 246
    :catchall_ac
    move-exception v0

    const/4 v1, 0x0

    iput v1, p0, Lcom/a/b/c/dl;->f:I

    throw v0

    .line 256
    :cond_b1
    iget v2, p0, Lcom/a/b/c/dl;->f:I

    if-nez v2, :cond_df

    iget-object v2, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    if-ne v2, v3, :cond_df

    invoke-virtual {p0}, Lcom/a/b/c/dl;->c()Z

    move-result v2

    if-eqz v2, :cond_df

    .line 257
    const/4 v2, 0x0

    .line 259
    :try_start_c0
    iget-object v4, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    if-ne v4, v3, :cond_d3

    .line 260
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/a/b/c/dn;

    .line 261
    and-int/lit8 v3, v1, 0x1

    new-instance v4, Lcom/a/b/c/dn;

    invoke-direct {v4, p1, p2}, Lcom/a/b/c/dn;-><init>(J)V

    aput-object v4, v2, v3

    .line 262
    iput-object v2, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;
    :try_end_d2
    .catchall {:try_start_c0 .. :try_end_d2} :catchall_da

    .line 263
    const/4 v2, 0x1

    .line 266
    :cond_d3
    const/4 v3, 0x0

    iput v3, p0, Lcom/a/b/c/dl;->f:I

    .line 268
    if-nez v2, :cond_52

    goto/16 :goto_1a

    .line 266
    :catchall_da
    move-exception v0

    const/4 v1, 0x0

    iput v1, p0, Lcom/a/b/c/dl;->f:I

    throw v0

    .line 271
    :cond_df
    iget-wide v2, p0, Lcom/a/b/c/dl;->e:J

    invoke-virtual {p0, v2, v3, p1, p2}, Lcom/a/b/c/dl;->a(JJ)J

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/a/b/c/dl;->b(JJ)Z

    move-result v2

    if-eqz v2, :cond_1a

    goto/16 :goto_52
.end method

.method private static b()Lsun/misc/Unsafe;
    .registers 3

    .prologue
    .line 318
    :try_start_0
    invoke-static {}, Lsun/misc/Unsafe;->getUnsafe()Lsun/misc/Unsafe;
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    .line 321
    :goto_4
    return-object v0

    :catch_5
    move-exception v0

    :try_start_6
    new-instance v0, Lcom/a/b/c/dm;

    invoke-direct {v0}, Lcom/a/b/c/dm;-><init>()V

    invoke-static {v0}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedExceptionAction;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsun/misc/Unsafe;
    :try_end_11
    .catch Ljava/security/PrivilegedActionException; {:try_start_6 .. :try_end_11} :catch_12

    goto :goto_4

    .line 333
    :catch_12
    move-exception v0

    .line 334
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not initialize intrinsics"

    invoke-virtual {v0}, Ljava/security/PrivilegedActionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic d()Lsun/misc/Unsafe;
    .registers 1

    .prologue
    .line 21
    invoke-static {}, Lcom/a/b/c/dl;->b()Lsun/misc/Unsafe;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abstract a(JJ)J
.end method

.method final b(JJ)Z
    .registers 14

    .prologue
    .line 161
    sget-object v0, Lcom/a/b/c/dl;->g:Lsun/misc/Unsafe;

    sget-wide v2, Lcom/a/b/c/dl;->h:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v0 .. v7}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v0

    return v0
.end method

.method final c()Z
    .registers 7

    .prologue
    .line 168
    sget-object v0, Lcom/a/b/c/dl;->g:Lsun/misc/Unsafe;

    sget-wide v2, Lcom/a/b/c/dl;->i:J

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->compareAndSwapInt(Ljava/lang/Object;JII)Z

    move-result v0

    return v0
.end method
