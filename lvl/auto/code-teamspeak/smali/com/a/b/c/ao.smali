.class Lcom/a/b/c/ao;
.super Ljava/util/AbstractMap;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/ConcurrentMap;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field static final A:Ljava/util/Queue;

.field static final a:I = 0x40000000

.field static final b:I = 0x10000

.field static final c:I = 0x3

.field static final d:I = 0x3f

.field static final e:I = 0x10

.field static final f:Ljava/util/logging/Logger;

.field static final z:Lcom/a/b/c/cg;


# instance fields
.field B:Ljava/util/Set;

.field C:Ljava/util/Collection;

.field D:Ljava/util/Set;

.field final g:I

.field final h:I

.field final i:[Lcom/a/b/c/bt;

.field final j:I

.field final k:Lcom/a/b/b/au;

.field final l:Lcom/a/b/b/au;

.field final m:Lcom/a/b/c/bw;

.field final n:Lcom/a/b/c/bw;

.field final o:J

.field final p:Lcom/a/b/c/do;

.field final q:J

.field final r:J

.field final s:J

.field final t:Ljava/util/Queue;

.field final u:Lcom/a/b/c/dg;

.field final v:Lcom/a/b/b/ej;

.field final w:Lcom/a/b/c/aw;

.field final x:Lcom/a/b/c/c;

.field final y:Lcom/a/b/c/ab;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 157
    const-class v0, Lcom/a/b/c/ao;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/a/b/c/ao;->f:Ljava/util/logging/Logger;

    .line 687
    new-instance v0, Lcom/a/b/c/ap;

    invoke-direct {v0}, Lcom/a/b/c/ap;-><init>()V

    sput-object v0, Lcom/a/b/c/ao;->z:Lcom/a/b/c/cg;

    .line 1018
    new-instance v0, Lcom/a/b/c/aq;

    invoke-direct {v0}, Lcom/a/b/c/aq;-><init>()V

    sput-object v0, Lcom/a/b/c/ao;->A:Ljava/util/Queue;

    return-void
.end method

.method constructor <init>(Lcom/a/b/c/f;Lcom/a/b/c/ab;)V
    .registers 15
    .param p2    # Lcom/a/b/c/ab;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, -0x1

    const-wide/16 v10, -0x1

    const/4 v5, 0x1

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 236
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 4382
    iget v0, p1, Lcom/a/b/c/f;->h:I

    if-ne v0, v6, :cond_10e

    const/4 v0, 0x4

    .line 237
    :goto_f
    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/a/b/c/ao;->j:I

    .line 239
    invoke-virtual {p1}, Lcom/a/b/c/f;->b()Lcom/a/b/c/bw;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/c/ao;->m:Lcom/a/b/c/bw;

    .line 240
    invoke-virtual {p1}, Lcom/a/b/c/f;->c()Lcom/a/b/c/bw;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/c/ao;->n:Lcom/a/b/c/bw;

    .line 5299
    iget-object v0, p1, Lcom/a/b/c/f;->q:Lcom/a/b/b/au;

    invoke-virtual {p1}, Lcom/a/b/c/f;->b()Lcom/a/b/c/bw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/c/bw;->a()Lcom/a/b/b/au;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    .line 242
    iput-object v0, p0, Lcom/a/b/c/ao;->k:Lcom/a/b/b/au;

    .line 5318
    iget-object v0, p1, Lcom/a/b/c/f;->r:Lcom/a/b/b/au;

    invoke-virtual {p1}, Lcom/a/b/c/f;->c()Lcom/a/b/c/bw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/c/bw;->a()Lcom/a/b/b/au;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    .line 243
    iput-object v0, p0, Lcom/a/b/c/ao;->l:Lcom/a/b/b/au;

    .line 5491
    iget-wide v0, p1, Lcom/a/b/c/f;->n:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_53

    iget-wide v0, p1, Lcom/a/b/c/f;->o:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_112

    :cond_53
    move-wide v0, v2

    .line 245
    :goto_54
    iput-wide v0, p0, Lcom/a/b/c/ao;->o:J

    .line 5500
    iget-object v0, p1, Lcom/a/b/c/f;->k:Lcom/a/b/c/do;

    sget-object v1, Lcom/a/b/c/k;->a:Lcom/a/b/c/k;

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/do;

    .line 246
    iput-object v0, p0, Lcom/a/b/c/ao;->p:Lcom/a/b/c/do;

    .line 5648
    iget-wide v0, p1, Lcom/a/b/c/f;->o:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_11e

    move-wide v0, v2

    .line 247
    :goto_69
    iput-wide v0, p0, Lcom/a/b/c/ao;->q:J

    .line 6614
    iget-wide v0, p1, Lcom/a/b/c/f;->n:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_122

    move-wide v0, v2

    .line 248
    :goto_72
    iput-wide v0, p0, Lcom/a/b/c/ao;->r:J

    .line 6688
    iget-wide v0, p1, Lcom/a/b/c/f;->p:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_126

    .line 249
    :goto_7a
    iput-wide v2, p0, Lcom/a/b/c/ao;->s:J

    .line 6749
    iget-object v0, p1, Lcom/a/b/c/f;->s:Lcom/a/b/c/dg;

    sget-object v1, Lcom/a/b/c/j;->a:Lcom/a/b/c/j;

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/dg;

    .line 251
    iput-object v0, p0, Lcom/a/b/c/ao;->u:Lcom/a/b/c/dg;

    .line 252
    iget-object v0, p0, Lcom/a/b/c/ao;->u:Lcom/a/b/c/dg;

    sget-object v1, Lcom/a/b/c/j;->a:Lcom/a/b/c/j;

    if-ne v0, v1, :cond_12a

    .line 7050
    sget-object v0, Lcom/a/b/c/ao;->A:Ljava/util/Queue;

    .line 252
    :goto_90
    iput-object v0, p0, Lcom/a/b/c/ao;->t:Ljava/util/Queue;

    .line 7353
    invoke-virtual {p0}, Lcom/a/b/c/ao;->g()Z

    move-result v0

    if-nez v0, :cond_9e

    .line 8349
    invoke-virtual {p0}, Lcom/a/b/c/ao;->d()Z

    move-result v0

    .line 7353
    if-eqz v0, :cond_131

    :cond_9e
    move v0, v5

    .line 8707
    :goto_9f
    iget-object v1, p1, Lcom/a/b/c/f;->t:Lcom/a/b/b/ej;

    if-eqz v1, :cond_134

    .line 8708
    iget-object v0, p1, Lcom/a/b/c/f;->t:Lcom/a/b/b/ej;

    .line 256
    :goto_a5
    iput-object v0, p0, Lcom/a/b/c/ao;->v:Lcom/a/b/b/ej;

    .line 257
    iget-object v2, p0, Lcom/a/b/c/ao;->m:Lcom/a/b/c/bw;

    .line 9361
    invoke-virtual {p0}, Lcom/a/b/c/ao;->f()Z

    move-result v0

    if-nez v0, :cond_b5

    .line 10349
    invoke-virtual {p0}, Lcom/a/b/c/ao;->d()Z

    move-result v0

    .line 9361
    if-eqz v0, :cond_140

    :cond_b5
    move v0, v5

    .line 11341
    :goto_b6
    invoke-virtual {p0}, Lcom/a/b/c/ao;->c()Z

    move-result v1

    .line 10357
    if-nez v1, :cond_c2

    invoke-virtual {p0}, Lcom/a/b/c/ao;->g()Z

    move-result v1

    if-eqz v1, :cond_143

    :cond_c2
    move v1, v5

    .line 257
    :goto_c3
    invoke-static {v2, v0, v1}, Lcom/a/b/c/aw;->a(Lcom/a/b/c/bw;ZZ)Lcom/a/b/c/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/c/ao;->w:Lcom/a/b/c/aw;

    .line 11771
    iget-object v0, p1, Lcom/a/b/c/f;->u:Lcom/a/b/b/dz;

    .line 258
    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/c;

    iput-object v0, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    .line 259
    iput-object p2, p0, Lcom/a/b/c/ao;->y:Lcom/a/b/c/ab;

    .line 12340
    iget v0, p1, Lcom/a/b/c/f;->g:I

    if-ne v0, v6, :cond_146

    const/16 v0, 0x10

    .line 261
    :goto_db
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 262
    invoke-virtual {p0}, Lcom/a/b/c/ao;->a()Z

    move-result v1

    if-eqz v1, :cond_f4

    invoke-virtual {p0}, Lcom/a/b/c/ao;->b()Z

    move-result v1

    if-nez v1, :cond_f4

    .line 263
    iget-wide v2, p0, Lcom/a/b/c/ao;->o:J

    long-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_f4
    move v1, v5

    move v2, v4

    .line 274
    :goto_f6
    iget v3, p0, Lcom/a/b/c/ao;->j:I

    if-ge v1, v3, :cond_149

    invoke-virtual {p0}, Lcom/a/b/c/ao;->a()Z

    move-result v3

    if-eqz v3, :cond_109

    mul-int/lit8 v3, v1, 0x14

    int-to-long v6, v3

    iget-wide v8, p0, Lcom/a/b/c/ao;->o:J

    cmp-long v3, v6, v8

    if-gtz v3, :cond_149

    .line 275
    :cond_109
    add-int/lit8 v2, v2, 0x1

    .line 276
    shl-int/lit8 v1, v1, 0x1

    goto :goto_f6

    .line 4382
    :cond_10e
    iget v0, p1, Lcom/a/b/c/f;->h:I

    goto/16 :goto_f

    .line 5494
    :cond_112
    iget-object v0, p1, Lcom/a/b/c/f;->k:Lcom/a/b/c/do;

    if-nez v0, :cond_11a

    iget-wide v0, p1, Lcom/a/b/c/f;->i:J

    goto/16 :goto_54

    :cond_11a
    iget-wide v0, p1, Lcom/a/b/c/f;->j:J

    goto/16 :goto_54

    .line 5648
    :cond_11e
    iget-wide v0, p1, Lcom/a/b/c/f;->o:J

    goto/16 :goto_69

    .line 6614
    :cond_122
    iget-wide v0, p1, Lcom/a/b/c/f;->n:J

    goto/16 :goto_72

    .line 6688
    :cond_126
    iget-wide v2, p1, Lcom/a/b/c/f;->p:J

    goto/16 :goto_7a

    .line 252
    :cond_12a
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto/16 :goto_90

    :cond_131
    move v0, v4

    .line 7353
    goto/16 :goto_9f

    .line 8710
    :cond_134
    if-eqz v0, :cond_13c

    invoke-static {}, Lcom/a/b/b/ej;->b()Lcom/a/b/b/ej;

    move-result-object v0

    goto/16 :goto_a5

    :cond_13c
    sget-object v0, Lcom/a/b/c/f;->d:Lcom/a/b/b/ej;

    goto/16 :goto_a5

    :cond_140
    move v0, v4

    .line 9361
    goto/16 :goto_b6

    :cond_143
    move v1, v4

    .line 10357
    goto/16 :goto_c3

    .line 12340
    :cond_146
    iget v0, p1, Lcom/a/b/c/f;->g:I

    goto :goto_db

    .line 278
    :cond_149
    rsub-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/a/b/c/ao;->h:I

    .line 279
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/a/b/c/ao;->g:I

    .line 12965
    new-array v2, v1, [Lcom/a/b/c/bt;

    .line 281
    iput-object v2, p0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    .line 283
    div-int v2, v0, v1

    .line 284
    mul-int v3, v2, v1

    if-ge v3, v0, :cond_1b1

    .line 285
    add-int/lit8 v0, v2, 0x1

    .line 289
    :goto_15d
    if-ge v5, v0, :cond_162

    .line 290
    shl-int/lit8 v5, v5, 0x1

    goto :goto_15d

    .line 293
    :cond_162
    invoke-virtual {p0}, Lcom/a/b/c/ao;->a()Z

    move-result v0

    if-eqz v0, :cond_196

    .line 295
    iget-wide v2, p0, Lcom/a/b/c/ao;->o:J

    int-to-long v6, v1

    div-long/2addr v2, v6

    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    .line 296
    iget-wide v6, p0, Lcom/a/b/c/ao;->o:J

    int-to-long v0, v1

    rem-long/2addr v6, v0

    move-wide v0, v2

    .line 297
    :goto_174
    iget-object v2, p0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    array-length v2, v2

    if-ge v4, v2, :cond_1ae

    .line 298
    int-to-long v2, v4

    cmp-long v2, v2, v6

    if-nez v2, :cond_1af

    .line 299
    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    .line 301
    :goto_182
    iget-object v1, p0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    .line 13771
    iget-object v0, p1, Lcom/a/b/c/f;->u:Lcom/a/b/b/dz;

    .line 301
    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/c;

    invoke-direct {p0, v5, v2, v3, v0}, Lcom/a/b/c/ao;->a(IJLcom/a/b/c/c;)Lcom/a/b/c/bt;

    move-result-object v0

    aput-object v0, v1, v4

    .line 297
    add-int/lit8 v4, v4, 0x1

    move-wide v0, v2

    goto :goto_174

    .line 305
    :cond_196
    :goto_196
    iget-object v0, p0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    array-length v0, v0

    if-ge v4, v0, :cond_1ae

    .line 306
    iget-object v1, p0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    .line 14771
    iget-object v0, p1, Lcom/a/b/c/f;->u:Lcom/a/b/b/dz;

    .line 306
    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/c;

    invoke-direct {p0, v5, v10, v11, v0}, Lcom/a/b/c/ao;->a(IJLcom/a/b/c/c;)Lcom/a/b/c/bt;

    move-result-object v0

    aput-object v0, v1, v4

    .line 305
    add-int/lit8 v4, v4, 0x1

    goto :goto_196

    .line 310
    :cond_1ae
    return-void

    :cond_1af
    move-wide v2, v0

    goto :goto_182

    :cond_1b1
    move v0, v2

    goto :goto_15d
.end method

.method private a(Ljava/lang/Object;ILcom/a/b/c/bs;)Lcom/a/b/c/bs;
    .registers 6
    .param p3    # Lcom/a/b/c/bs;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 1809
    invoke-virtual {p0, p2}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v0

    .line 1810
    invoke-virtual {v0}, Lcom/a/b/c/bt;->lock()V

    .line 1812
    :try_start_7
    invoke-virtual {v0, p1, p2, p3}, Lcom/a/b/c/bt;->a(Ljava/lang/Object;ILcom/a/b/c/bs;)Lcom/a/b/c/bs;
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_f

    move-result-object v1

    .line 1814
    invoke-virtual {v0}, Lcom/a/b/c/bt;->unlock()V

    return-object v1

    :catchall_f
    move-exception v1

    invoke-virtual {v0}, Lcom/a/b/c/bt;->unlock()V

    throw v1
.end method

.method private a(IJLcom/a/b/c/c;)Lcom/a/b/c/bt;
    .registers 13

    .prologue
    .line 1876
    new-instance v1, Lcom/a/b/c/bt;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/a/b/c/bt;-><init>(Lcom/a/b/c/ao;IJLcom/a/b/c/c;)V

    return-object v1
.end method

.method private a(Lcom/a/b/c/bs;Ljava/lang/Object;I)Lcom/a/b/c/cg;
    .registers 7
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 1834
    invoke-interface {p1}, Lcom/a/b/c/bs;->c()I

    move-result v0

    .line 1835
    iget-object v1, p0, Lcom/a/b/c/ao;->n:Lcom/a/b/c/bw;

    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v0

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, p1, v2, p3}, Lcom/a/b/c/bw;->a(Lcom/a/b/c/bt;Lcom/a/b/c/bs;Ljava/lang/Object;I)Lcom/a/b/c/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Set;Lcom/a/b/c/ab;)Ljava/util/Map;
    .registers 10
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 4018
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4019
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4020
    invoke-static {}, Lcom/a/b/b/dw;->a()Lcom/a/b/b/dw;

    move-result-object v3

    .line 4025
    :try_start_c
    invoke-virtual {p2, p1}, Lcom/a/b/c/ab;->a(Ljava/lang/Iterable;)Ljava/util/Map;
    :try_end_f
    .catch Lcom/a/b/c/ah; {:try_start_c .. :try_end_f} :catch_44
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_f} :catch_55
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_f} :catch_67
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_f} :catch_6e
    .catch Ljava/lang/Error; {:try_start_c .. :try_end_f} :catch_75
    .catchall {:try_start_c .. :try_end_f} :catchall_63

    move-result-object v4

    .line 4046
    if-nez v4, :cond_7c

    .line 4047
    iget-object v0, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v1}, Lcom/a/b/b/dw;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/a/b/c/c;->b(J)V

    .line 4048
    new-instance v0, Lcom/a/b/c/af;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " returned null map from loadAll"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/c/af;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4028
    :catch_44
    move-exception v0

    .line 4030
    :try_start_45
    throw v0
    :try_end_46
    .catchall {:try_start_45 .. :try_end_46} :catchall_46

    .line 4041
    :catchall_46
    move-exception v0

    :goto_47
    if-nez v2, :cond_54

    .line 4042
    iget-object v1, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v2}, Lcom/a/b/b/dw;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/a/b/c/c;->b(J)V

    :cond_54
    throw v0

    .line 4031
    :catch_55
    move-exception v1

    .line 4032
    :try_start_56
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 4033
    new-instance v2, Ljava/util/concurrent/ExecutionException;

    invoke-direct {v2, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 4041
    :catchall_63
    move-exception v1

    move v2, v0

    move-object v0, v1

    goto :goto_47

    .line 4034
    :catch_67
    move-exception v1

    .line 4035
    new-instance v2, Lcom/a/b/n/a/gq;

    invoke-direct {v2, v1}, Lcom/a/b/n/a/gq;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 4036
    :catch_6e
    move-exception v1

    .line 4037
    new-instance v2, Ljava/util/concurrent/ExecutionException;

    invoke-direct {v2, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 4038
    :catch_75
    move-exception v1

    .line 4039
    new-instance v2, Lcom/a/b/n/a/bt;

    invoke-direct {v2, v1}, Lcom/a/b/n/a/bt;-><init>(Ljava/lang/Error;)V

    throw v2
    :try_end_7c
    .catchall {:try_start_56 .. :try_end_7c} :catchall_63

    .line 4051
    :cond_7c
    invoke-virtual {v3}, Lcom/a/b/b/dw;->c()Lcom/a/b/b/dw;

    .line 4054
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_88
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 4055
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    .line 4056
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 4057
    if-eqz v6, :cond_a0

    if-nez v0, :cond_a2

    :cond_a0
    move v1, v2

    .line 4059
    goto :goto_88

    .line 4061
    :cond_a2
    invoke-virtual {p0, v6, v0}, Lcom/a/b/c/ao;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_88

    .line 4065
    :cond_a6
    if-eqz v1, :cond_da

    .line 4066
    iget-object v0, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v1}, Lcom/a/b/b/dw;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/a/b/c/c;->b(J)V

    .line 4067
    new-instance v0, Lcom/a/b/c/af;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " returned null keys or values from loadAll"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/c/af;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4071
    :cond_da
    iget-object v0, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v1}, Lcom/a/b/b/dw;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/a/b/c/c;->a(J)V

    .line 4072
    return-object v4
.end method

.method static a(Lcom/a/b/c/bs;)V
    .registers 2

    .prologue
    .line 17015
    sget-object v0, Lcom/a/b/c/br;->a:Lcom/a/b/c/br;

    .line 1930
    invoke-interface {p0, v0}, Lcom/a/b/c/bs;->a(Lcom/a/b/c/bs;)V

    .line 1931
    invoke-interface {p0, v0}, Lcom/a/b/c/bs;->b(Lcom/a/b/c/bs;)V

    .line 1932
    return-void
.end method

.method static a(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V
    .registers 2

    .prologue
    .line 1923
    invoke-interface {p0, p1}, Lcom/a/b/c/bs;->a(Lcom/a/b/c/bs;)V

    .line 1924
    invoke-interface {p1, p0}, Lcom/a/b/c/bs;->b(Lcom/a/b/c/bs;)V

    .line 1925
    return-void
.end method

.method private a(Lcom/a/b/c/cg;)V
    .registers 5

    .prologue
    .line 1844
    invoke-interface {p1}, Lcom/a/b/c/cg;->b()Lcom/a/b/c/bs;

    move-result-object v0

    .line 1845
    invoke-interface {v0}, Lcom/a/b/c/bs;->c()I

    move-result v1

    .line 1846
    invoke-virtual {p0, v1}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v2

    invoke-interface {v0}, Lcom/a/b/c/bs;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0, v1, p1}, Lcom/a/b/c/bt;->a(Ljava/lang/Object;ILcom/a/b/c/cg;)Z

    .line 1847
    return-void
.end method

.method private static b(I)I
    .registers 4

    .prologue
    .line 1796
    shl-int/lit8 v0, p0, 0xf

    xor-int/lit16 v0, v0, -0x3283

    add-int/2addr v0, p0

    .line 1797
    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    .line 1798
    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    .line 1799
    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    .line 1800
    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1801
    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method private b(Ljava/lang/Iterable;)Lcom/a/b/d/jt;
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 3945
    .line 3948
    invoke-static {}, Lcom/a/b/d/sz;->d()Ljava/util/LinkedHashMap;

    move-result-object v2

    .line 3949
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 3950
    invoke-virtual {p0, v4}, Lcom/a/b/c/ao;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 3951
    if-nez v5, :cond_1d

    .line 3952
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 3957
    :cond_1d
    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3958
    add-int/lit8 v1, v1, 0x1

    .line 3960
    goto :goto_a

    .line 3961
    :cond_23
    iget-object v3, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-interface {v3, v1}, Lcom/a/b/c/c;->a(I)V

    .line 3962
    iget-object v1, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-interface {v1, v0}, Lcom/a/b/c/c;->b(I)V

    .line 3963
    invoke-static {v2}, Lcom/a/b/d/jt;->a(Ljava/util/Map;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 3925
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3926
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/a/b/c/bt;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .line 3927
    if-nez v0, :cond_19

    .line 3928
    iget-object v1, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-interface {v1, v2}, Lcom/a/b/c/c;->b(I)V

    .line 3932
    :goto_18
    return-object v0

    .line 3930
    :cond_19
    iget-object v1, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-interface {v1, v2}, Lcom/a/b/c/c;->a(I)V

    goto :goto_18
.end method

.method static b(Lcom/a/b/c/bs;)V
    .registers 2

    .prologue
    .line 18015
    sget-object v0, Lcom/a/b/c/br;->a:Lcom/a/b/c/br;

    .line 1943
    invoke-interface {p0, v0}, Lcom/a/b/c/bs;->c(Lcom/a/b/c/bs;)V

    .line 1944
    invoke-interface {p0, v0}, Lcom/a/b/c/bs;->d(Lcom/a/b/c/bs;)V

    .line 1945
    return-void
.end method

.method static b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V
    .registers 2

    .prologue
    .line 1936
    invoke-interface {p0, p1}, Lcom/a/b/c/bs;->c(Lcom/a/b/c/bs;)V

    .line 1937
    invoke-interface {p1, p0}, Lcom/a/b/c/bs;->d(Lcom/a/b/c/bs;)V

    .line 1938
    return-void
.end method

.method private b(Lcom/a/b/c/bs;J)Z
    .registers 6
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 1860
    invoke-interface {p1}, Lcom/a/b/c/bs;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/a/b/c/bt;->a(Lcom/a/b/c/bs;J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private c(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)Lcom/a/b/c/bs;
    .registers 4
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 1824
    invoke-interface {p1}, Lcom/a/b/c/bs;->c()I

    move-result v0

    .line 1825
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/a/b/c/bt;->a(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)Lcom/a/b/c/bs;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/a/b/c/bs;J)Ljava/lang/Object;
    .registers 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1887
    invoke-interface {p1}, Lcom/a/b/c/bs;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_8

    .line 1898
    :cond_7
    :goto_7
    return-object v0

    .line 1890
    :cond_8
    invoke-interface {p1}, Lcom/a/b/c/bs;->a()Lcom/a/b/c/cg;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/b/c/cg;->get()Ljava/lang/Object;

    move-result-object v1

    .line 1891
    if-eqz v1, :cond_7

    .line 1895
    invoke-virtual {p0, p1, p2, p3}, Lcom/a/b/c/ao;->a(Lcom/a/b/c/bs;J)Z

    move-result v2

    if-nez v2, :cond_7

    move-object v0, v1

    .line 1898
    goto :goto_7
.end method

.method private c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3941
    iget-object v0, p0, Lcom/a/b/c/ao;->y:Lcom/a/b/c/ab;

    invoke-virtual {p0, p1, v0}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;Lcom/a/b/c/ab;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/a/b/c/bs;)V
    .registers 4

    .prologue
    .line 1850
    invoke-interface {p1}, Lcom/a/b/c/bs;->c()I

    move-result v0

    .line 1851
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/a/b/c/bt;->a(Lcom/a/b/c/bs;I)Z

    .line 1852
    return-void
.end method

.method private c(Ljava/lang/Iterable;)V
    .registers 4

    .prologue
    .line 4213
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 4214
    invoke-virtual {p0, v1}, Lcom/a/b/c/ao;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 4216
    :cond_12
    return-void
.end method

.method private static c(I)[Lcom/a/b/c/bt;
    .registers 2

    .prologue
    .line 1965
    new-array v0, p0, [Lcom/a/b/c/bt;

    return-object v0
.end method

.method private d(Ljava/lang/Object;)Lcom/a/b/c/bs;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 4081
    if-nez p1, :cond_4

    .line 4082
    const/4 v0, 0x0

    .line 4085
    :goto_3
    return-object v0

    .line 4084
    :cond_4
    invoke-virtual {p0, p1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4085
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/a/b/c/bt;->a(Ljava/lang/Object;I)Lcom/a/b/c/bs;

    move-result-object v0

    goto :goto_3
.end method

.method private e(Ljava/lang/Object;)V
    .registers 6

    .prologue
    .line 4089
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4090
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/c/ao;->y:Lcom/a/b/c/ab;

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v0, v2, v3}, Lcom/a/b/c/bt;->a(Ljava/lang/Object;ILcom/a/b/c/ab;Z)Ljava/lang/Object;

    .line 4091
    return-void
.end method

.method static j()Lcom/a/b/c/cg;
    .registers 1

    .prologue
    .line 733
    sget-object v0, Lcom/a/b/c/ao;->z:Lcom/a/b/c/cg;

    return-object v0
.end method

.method static k()Lcom/a/b/c/bs;
    .registers 1

    .prologue
    .line 1015
    sget-object v0, Lcom/a/b/c/br;->a:Lcom/a/b/c/br;

    return-object v0
.end method

.method static l()Ljava/util/Queue;
    .registers 1

    .prologue
    .line 1050
    sget-object v0, Lcom/a/b/c/ao;->A:Ljava/util/Queue;

    return-object v0
.end method

.method private n()Z
    .registers 2

    .prologue
    .line 321
    invoke-virtual {p0}, Lcom/a/b/c/ao;->c()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/a/b/c/ao;->d()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private o()Z
    .registers 2

    .prologue
    .line 341
    invoke-virtual {p0}, Lcom/a/b/c/ao;->c()Z

    move-result v0

    return v0
.end method

.method private p()Z
    .registers 2

    .prologue
    .line 349
    invoke-virtual {p0}, Lcom/a/b/c/ao;->d()Z

    move-result v0

    return v0
.end method

.method private q()Z
    .registers 2

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/a/b/c/ao;->g()Z

    move-result v0

    if-nez v0, :cond_c

    .line 15349
    invoke-virtual {p0}, Lcom/a/b/c/ao;->d()Z

    move-result v0

    .line 353
    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private r()Z
    .registers 2

    .prologue
    .line 357
    .line 16341
    invoke-virtual {p0}, Lcom/a/b/c/ao;->c()Z

    move-result v0

    .line 357
    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/a/b/c/ao;->g()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private s()Z
    .registers 2

    .prologue
    .line 361
    invoke-virtual {p0}, Lcom/a/b/c/ao;->f()Z

    move-result v0

    if-nez v0, :cond_c

    .line 16349
    invoke-virtual {p0}, Lcom/a/b/c/ao;->d()Z

    move-result v0

    .line 361
    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private t()V
    .registers 5

    .prologue
    .line 1954
    :goto_0
    iget-object v0, p0, Lcom/a/b/c/ao;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/dk;

    if-eqz v0, :cond_1b

    .line 1956
    :try_start_a
    iget-object v1, p0, Lcom/a/b/c/ao;->u:Lcom/a/b/c/dg;

    invoke-interface {v1, v0}, Lcom/a/b/c/dg;->a(Lcom/a/b/c/dk;)V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_f} :catch_10

    goto :goto_0

    .line 1957
    :catch_10
    move-exception v0

    .line 1958
    sget-object v1, Lcom/a/b/c/ao;->f:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Exception thrown by removal listener"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1961
    :cond_1b
    return-void
.end method

.method private u()V
    .registers 5

    .prologue
    .line 3860
    iget-object v1, p0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 3861
    invoke-virtual {v3}, Lcom/a/b/c/bt;->b()V

    .line 3860
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 3863
    :cond_e
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/Object;)I
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1839
    iget-object v0, p0, Lcom/a/b/c/ao;->k:Lcom/a/b/b/au;

    invoke-virtual {v0, p1}, Lcom/a/b/b/au;->a(Ljava/lang/Object;)I

    move-result v0

    .line 16796
    shl-int/lit8 v1, v0, 0xf

    xor-int/lit16 v1, v1, -0x3283

    add-int/2addr v0, v1

    .line 16797
    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    .line 16798
    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    .line 16799
    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    .line 16800
    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 16801
    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    .line 1840
    return v0
.end method

.method final a(I)Lcom/a/b/c/bt;
    .registers 5

    .prologue
    .line 1871
    iget-object v0, p0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    iget v1, p0, Lcom/a/b/c/ao;->h:I

    ushr-int v1, p1, v1

    iget v2, p0, Lcom/a/b/c/ao;->g:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method final a(Ljava/lang/Iterable;)Lcom/a/b/d/jt;
    .registers 11

    .prologue
    const/4 v0, 0x0

    .line 3967
    .line 3970
    invoke-static {}, Lcom/a/b/d/sz;->d()Ljava/util/LinkedHashMap;

    move-result-object v3

    .line 18289
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 3972
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :cond_f
    :goto_f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_31

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 3973
    invoke-virtual {p0, v5}, Lcom/a/b/c/ao;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 3974
    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_f

    .line 3975
    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3976
    if-nez v6, :cond_2e

    .line 3977
    add-int/lit8 v0, v0, 0x1

    .line 3978
    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 3980
    :cond_2e
    add-int/lit8 v1, v1, 0x1

    goto :goto_f

    .line 3986
    :cond_31
    :try_start_31
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z
    :try_end_34
    .catchall {:try_start_31 .. :try_end_34} :catchall_a4

    move-result v4

    if-nez v4, :cond_b9

    .line 3988
    :try_start_37
    iget-object v4, p0, Lcom/a/b/c/ao;->y:Lcom/a/b/c/ab;

    invoke-direct {p0, v2, v4}, Lcom/a/b/c/ao;->a(Ljava/util/Set;Lcom/a/b/c/ab;)Ljava/util/Map;

    move-result-object v4

    .line 3989
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_41
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_a9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 3990
    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 3991
    if-nez v7, :cond_a0

    .line 3992
    new-instance v4, Lcom/a/b/c/af;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x25

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "loadAll failed to return a value for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/a/b/c/af;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_78
    .catch Lcom/a/b/c/ah; {:try_start_37 .. :try_end_78} :catch_78
    .catchall {:try_start_37 .. :try_end_78} :catchall_a4

    .line 3998
    :catch_78
    move-exception v4

    :try_start_79
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
    :try_end_7c
    .catchall {:try_start_79 .. :try_end_7c} :catchall_a4

    move-result-object v4

    move v2, v0

    :goto_7e
    :try_start_7e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_aa

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 3999
    add-int/lit8 v2, v2, -0x1

    .line 4000
    iget-object v5, p0, Lcom/a/b/c/ao;->y:Lcom/a/b/c/ab;

    invoke-virtual {p0, v0, v5}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;Lcom/a/b/c/ab;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_93
    .catchall {:try_start_7e .. :try_end_93} :catchall_94

    goto :goto_7e

    .line 4006
    :catchall_94
    move-exception v0

    :goto_95
    iget-object v3, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-interface {v3, v1}, Lcom/a/b/c/c;->a(I)V

    .line 4007
    iget-object v1, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-interface {v1, v2}, Lcom/a/b/c/c;->b(I)V

    throw v0

    .line 3994
    :cond_a0
    :try_start_a0
    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_a3
    .catch Lcom/a/b/c/ah; {:try_start_a0 .. :try_end_a3} :catch_78
    .catchall {:try_start_a0 .. :try_end_a3} :catchall_a4

    goto :goto_41

    .line 4006
    :catchall_a4
    move-exception v2

    move-object v8, v2

    move v2, v0

    move-object v0, v8

    goto :goto_95

    :cond_a9
    move v2, v0

    .line 4004
    :cond_aa
    :goto_aa
    :try_start_aa
    invoke-static {v3}, Lcom/a/b/d/jt;->a(Ljava/util/Map;)Lcom/a/b/d/jt;
    :try_end_ad
    .catchall {:try_start_aa .. :try_end_ad} :catchall_94

    move-result-object v0

    .line 4006
    iget-object v3, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-interface {v3, v1}, Lcom/a/b/c/c;->a(I)V

    .line 4007
    iget-object v1, p0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-interface {v1, v2}, Lcom/a/b/c/c;->b(I)V

    return-object v0

    :cond_b9
    move v2, v0

    goto :goto_aa
.end method

.method final a(Ljava/lang/Object;Lcom/a/b/c/ab;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 3936
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3937
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lcom/a/b/c/bt;->a(Ljava/lang/Object;ILcom/a/b/c/ab;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final a()Z
    .registers 5

    .prologue
    .line 313
    iget-wide v0, p0, Lcom/a/b/c/ao;->o:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method final a(Lcom/a/b/c/bs;J)Z
    .registers 10

    .prologue
    const/4 v0, 0x1

    .line 1907
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1908
    invoke-virtual {p0}, Lcom/a/b/c/ao;->d()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-interface {p1}, Lcom/a/b/c/bs;->e()J

    move-result-wide v2

    sub-long v2, p2, v2

    iget-wide v4, p0, Lcom/a/b/c/ao;->q:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_17

    .line 1916
    :cond_16
    :goto_16
    return v0

    .line 1912
    :cond_17
    invoke-virtual {p0}, Lcom/a/b/c/ao;->c()Z

    move-result v1

    if-eqz v1, :cond_29

    invoke-interface {p1}, Lcom/a/b/c/bs;->h()J

    move-result-wide v2

    sub-long v2, p2, v2

    iget-wide v4, p0, Lcom/a/b/c/ao;->r:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_16

    .line 1916
    :cond_29
    const/4 v0, 0x0

    goto :goto_16
.end method

.method final b()Z
    .registers 3

    .prologue
    .line 317
    iget-object v0, p0, Lcom/a/b/c/ao;->p:Lcom/a/b/c/do;

    sget-object v1, Lcom/a/b/c/k;->a:Lcom/a/b/c/k;

    if-eq v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method final c()Z
    .registers 5

    .prologue
    .line 325
    iget-wide v0, p0, Lcom/a/b/c/ao;->r:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public clear()V
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 4206
    iget-object v4, p0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    array-length v5, v4

    move v3, v1

    :goto_5
    if-ge v3, v5, :cond_93

    aget-object v6, v4, v3

    .line 19217
    iget v0, v6, Lcom/a/b/c/bt;->b:I

    if-eqz v0, :cond_86

    .line 19218
    invoke-virtual {v6}, Lcom/a/b/c/bt;->lock()V

    .line 19220
    :try_start_10
    iget-object v7, v6, Lcom/a/b/c/bt;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move v2, v1

    .line 19221
    :goto_13
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_39

    .line 19222
    invoke-virtual {v7, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/bs;

    :goto_1f
    if-eqz v0, :cond_35

    .line 19224
    invoke-interface {v0}, Lcom/a/b/c/bs;->a()Lcom/a/b/c/cg;

    move-result-object v8

    invoke-interface {v8}, Lcom/a/b/c/cg;->d()Z

    move-result v8

    if-eqz v8, :cond_30

    .line 19225
    sget-object v8, Lcom/a/b/c/da;->a:Lcom/a/b/c/da;

    invoke-virtual {v6, v0, v8}, Lcom/a/b/c/bt;->a(Lcom/a/b/c/bs;Lcom/a/b/c/da;)V

    .line 19222
    :cond_30
    invoke-interface {v0}, Lcom/a/b/c/bs;->b()Lcom/a/b/c/bs;

    move-result-object v0

    goto :goto_1f

    .line 19221
    :cond_35
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_13

    :cond_39
    move v0, v1

    .line 19229
    :goto_3a
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_47

    .line 19230
    const/4 v2, 0x0

    invoke-virtual {v7, v0, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 19229
    add-int/lit8 v0, v0, 0x1

    goto :goto_3a

    .line 19515
    :cond_47
    iget-object v0, v6, Lcom/a/b/c/bt;->a:Lcom/a/b/c/ao;

    invoke-virtual {v0}, Lcom/a/b/c/ao;->h()Z

    move-result v0

    if-eqz v0, :cond_57

    .line 19524
    :cond_4f
    iget-object v0, v6, Lcom/a/b/c/bt;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_4f

    .line 19518
    :cond_57
    iget-object v0, v6, Lcom/a/b/c/bt;->a:Lcom/a/b/c/ao;

    invoke-virtual {v0}, Lcom/a/b/c/ao;->i()Z

    move-result v0

    if-eqz v0, :cond_67

    .line 19528
    :cond_5f
    iget-object v0, v6, Lcom/a/b/c/bt;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_5f

    .line 19233
    :cond_67
    iget-object v0, v6, Lcom/a/b/c/bt;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 19234
    iget-object v0, v6, Lcom/a/b/c/bt;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 19235
    iget-object v0, v6, Lcom/a/b/c/bt;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 19237
    iget v0, v6, Lcom/a/b/c/bt;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/a/b/c/bt;->d:I

    .line 19238
    const/4 v0, 0x0

    iput v0, v6, Lcom/a/b/c/bt;->b:I
    :try_end_80
    .catchall {:try_start_10 .. :try_end_80} :catchall_8b

    .line 19240
    invoke-virtual {v6}, Lcom/a/b/c/bt;->unlock()V

    .line 20436
    invoke-virtual {v6}, Lcom/a/b/c/bt;->c()V

    .line 4206
    :cond_86
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_5

    .line 19240
    :catchall_8b
    move-exception v0

    invoke-virtual {v6}, Lcom/a/b/c/bt;->unlock()V

    .line 21436
    invoke-virtual {v6}, Lcom/a/b/c/bt;->c()V

    .line 19241
    throw v0

    .line 4209
    :cond_93
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 4096
    if-nez p1, :cond_4

    .line 4097
    const/4 v0, 0x0

    .line 4100
    :goto_3
    return v0

    .line 4099
    :cond_4
    invoke-virtual {p0, p1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4100
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/a/b/c/bt;->c(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_3
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .registers 22
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 4106
    if-nez p1, :cond_4

    .line 4107
    const/4 v4, 0x0

    .line 4141
    :goto_3
    return v4

    .line 4115
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/a/b/c/ao;->v:Lcom/a/b/b/ej;

    invoke-virtual {v4}, Lcom/a/b/b/ej;->a()J

    move-result-wide v14

    .line 4116
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    .line 4117
    const-wide/16 v8, -0x1

    .line 4118
    const/4 v4, 0x0

    move v10, v4

    move-wide v12, v8

    :goto_15
    const/4 v4, 0x3

    if-ge v10, v4, :cond_72

    .line 4119
    const-wide/16 v6, 0x0

    .line 4120
    array-length v0, v11

    move/from16 v16, v0

    const/4 v4, 0x0

    move-wide v8, v6

    move v6, v4

    :goto_20
    move/from16 v0, v16

    if-ge v6, v0, :cond_69

    aget-object v7, v11, v6

    .line 4123
    iget v4, v7, Lcom/a/b/c/bt;->b:I

    .line 4125
    iget-object v0, v7, Lcom/a/b/c/bt;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-object/from16 v17, v0

    .line 4126
    const/4 v4, 0x0

    move v5, v4

    :goto_2e
    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v4

    if-ge v5, v4, :cond_61

    .line 4127
    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/a/b/c/bs;

    :goto_3c
    if-eqz v4, :cond_5d

    .line 4128
    invoke-virtual {v7, v4, v14, v15}, Lcom/a/b/c/bt;->a(Lcom/a/b/c/bs;J)Ljava/lang/Object;

    move-result-object v18

    .line 4129
    if-eqz v18, :cond_58

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/a/b/c/ao;->l:Lcom/a/b/b/au;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_58

    .line 4130
    const/4 v4, 0x1

    goto :goto_3

    .line 4127
    :cond_58
    invoke-interface {v4}, Lcom/a/b/c/bs;->b()Lcom/a/b/c/bs;

    move-result-object v4

    goto :goto_3c

    .line 4126
    :cond_5d
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2e

    .line 4134
    :cond_61
    iget v4, v7, Lcom/a/b/c/bt;->d:I

    int-to-long v4, v4

    add-long/2addr v8, v4

    .line 4120
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_20

    .line 4136
    :cond_69
    cmp-long v4, v8, v12

    if-eqz v4, :cond_72

    .line 4118
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    move-wide v12, v8

    goto :goto_15

    .line 4141
    :cond_72
    const/4 v4, 0x0

    goto :goto_3
.end method

.method final d()Z
    .registers 5

    .prologue
    .line 329
    iget-wide v0, p0, Lcom/a/b/c/ao;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method final e()Z
    .registers 5

    .prologue
    .line 333
    iget-wide v0, p0, Lcom/a/b/c/ao;->s:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public entrySet()Ljava/util/Set;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "Not supported."
    .end annotation

    .prologue
    .line 4242
    iget-object v0, p0, Lcom/a/b/c/ao;->D:Ljava/util/Set;

    .line 4243
    if-eqz v0, :cond_5

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/a/b/c/bg;

    invoke-direct {v0, p0, p0}, Lcom/a/b/c/bg;-><init>(Lcom/a/b/c/ao;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lcom/a/b/c/ao;->D:Ljava/util/Set;

    goto :goto_4
.end method

.method final f()Z
    .registers 2

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/a/b/c/ao;->d()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/a/b/c/ao;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method final g()Z
    .registers 2

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/a/b/c/ao;->c()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/a/b/c/ao;->e()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 3916
    if-nez p1, :cond_4

    .line 3917
    const/4 v0, 0x0

    .line 3920
    :goto_3
    return-object v0

    .line 3919
    :cond_4
    invoke-virtual {p0, p1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3920
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/a/b/c/bt;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3
.end method

.method final h()Z
    .registers 3

    .prologue
    .line 365
    iget-object v0, p0, Lcom/a/b/c/ao;->m:Lcom/a/b/c/bw;

    sget-object v1, Lcom/a/b/c/bw;->a:Lcom/a/b/c/bw;

    if-eq v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method final i()Z
    .registers 3

    .prologue
    .line 369
    iget-object v0, p0, Lcom/a/b/c/ao;->n:Lcom/a/b/c/bw;

    sget-object v1, Lcom/a/b/c/bw;->a:Lcom/a/b/c/bw;

    if-eq v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public isEmpty()Z
    .registers 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 3876
    .line 3877
    iget-object v6, p0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    move v0, v1

    move-wide v2, v4

    .line 3878
    :goto_7
    array-length v7, v6

    if-ge v0, v7, :cond_1a

    .line 3879
    aget-object v7, v6, v0

    iget v7, v7, Lcom/a/b/c/bt;->b:I

    if-eqz v7, :cond_11

    .line 3896
    :cond_10
    :goto_10
    return v1

    .line 3882
    :cond_11
    aget-object v7, v6, v0

    iget v7, v7, Lcom/a/b/c/bt;->d:I

    int-to-long v8, v7

    add-long/2addr v2, v8

    .line 3878
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 3885
    :cond_1a
    cmp-long v0, v2, v4

    if-eqz v0, :cond_35

    move v0, v1

    .line 3886
    :goto_1f
    array-length v7, v6

    if-ge v0, v7, :cond_31

    .line 3887
    aget-object v7, v6, v0

    iget v7, v7, Lcom/a/b/c/bt;->b:I

    if-nez v7, :cond_10

    .line 3890
    aget-object v7, v6, v0

    iget v7, v7, Lcom/a/b/c/bt;->d:I

    int-to-long v8, v7

    sub-long/2addr v2, v8

    .line 3886
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 3892
    :cond_31
    cmp-long v0, v2, v4

    if-nez v0, :cond_10

    .line 3896
    :cond_35
    const/4 v1, 0x1

    goto :goto_10
.end method

.method public keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 4223
    iget-object v0, p0, Lcom/a/b/c/ao;->B:Ljava/util/Set;

    .line 4224
    if-eqz v0, :cond_5

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/a/b/c/bj;

    invoke-direct {v0, p0, p0}, Lcom/a/b/c/bj;-><init>(Lcom/a/b/c/ao;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lcom/a/b/c/ao;->B:Ljava/util/Set;

    goto :goto_4
.end method

.method final m()J
    .registers 7

    .prologue
    .line 3900
    iget-object v1, p0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    .line 3901
    const-wide/16 v2, 0x0

    .line 3902
    const/4 v0, 0x0

    :goto_5
    array-length v4, v1

    if-ge v0, v4, :cond_11

    .line 3903
    aget-object v4, v1, v0

    iget v4, v4, Lcom/a/b/c/bt;->b:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 3902
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 3905
    :cond_11
    return-wide v2
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 4146
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4147
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4148
    invoke-virtual {p0, p1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4149
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/a/b/c/bt;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .registers 5

    .prologue
    .line 4162
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 4163
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/a/b/c/ao;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 4165
    :cond_20
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 4154
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4155
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4156
    invoke-virtual {p0, p1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4157
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/a/b/c/bt;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 4169
    if-nez p1, :cond_4

    .line 4170
    const/4 v0, 0x0

    .line 4173
    :goto_3
    return-object v0

    .line 4172
    :cond_4
    invoke-virtual {p0, p1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4173
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/a/b/c/bt;->d(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 4178
    if-eqz p1, :cond_4

    if-nez p2, :cond_6

    .line 4179
    :cond_4
    const/4 v0, 0x0

    .line 4182
    :goto_5
    return v0

    .line 4181
    :cond_6
    invoke-virtual {p0, p1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4182
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lcom/a/b/c/bt;->b(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 4198
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4199
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4200
    invoke-virtual {p0, p1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4201
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lcom/a/b/c/bt;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 6
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 4187
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4188
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4189
    if-nez p2, :cond_a

    .line 4190
    const/4 v0, 0x0

    .line 4193
    :goto_9
    return v0

    .line 4192
    :cond_a
    invoke-virtual {p0, p1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4193
    invoke-virtual {p0, v0}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, Lcom/a/b/c/bt;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_9
.end method

.method public size()I
    .registers 3

    .prologue
    .line 3910
    invoke-virtual {p0}, Lcom/a/b/c/ao;->m()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/a/b/l/q;->b(J)I

    move-result v0

    return v0
.end method

.method public values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 4232
    iget-object v0, p0, Lcom/a/b/c/ao;->C:Ljava/util/Collection;

    .line 4233
    if-eqz v0, :cond_5

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/a/b/c/ch;

    invoke-direct {v0, p0, p0}, Lcom/a/b/c/ch;-><init>(Lcom/a/b/c/ao;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lcom/a/b/c/ao;->C:Ljava/util/Collection;

    goto :goto_4
.end method
