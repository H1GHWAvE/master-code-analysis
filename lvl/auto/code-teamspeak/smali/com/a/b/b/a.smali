.class final Lcom/a/b/b/a;
.super Lcom/a/b/b/ci;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field static final a:Lcom/a/b/b/a;

.field private static final b:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 33
    new-instance v0, Lcom/a/b/b/a;

    invoke-direct {v0}, Lcom/a/b/b/a;-><init>()V

    sput-object v0, Lcom/a/b/b/a;->a:Lcom/a/b/b/a;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/a/b/b/ci;-><init>()V

    return-void
.end method

.method static a()Lcom/a/b/b/ci;
    .registers 1

    .prologue
    .line 37
    sget-object v0, Lcom/a/b/b/a;->a:Lcom/a/b/b/a;

    return-object v0
.end method

.method private static g()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 90
    sget-object v0, Lcom/a/b/b/a;->a:Lcom/a/b/b/a;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/a/b/b/bj;)Lcom/a/b/b/ci;
    .registers 3

    .prologue
    .line 73
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2037
    sget-object v0, Lcom/a/b/b/a;->a:Lcom/a/b/b/a;

    .line 74
    return-object v0
.end method

.method public final a(Lcom/a/b/b/ci;)Lcom/a/b/b/ci;
    .registers 3

    .prologue
    .line 56
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/ci;

    return-object v0
.end method

.method public final a(Lcom/a/b/b/dz;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 60
    invoke-interface {p1}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "use Optional.orNull() instead of a Supplier that returns null"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 51
    const-string v0, "use Optional.orNull() instead of Optional.or(null)"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 47
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Optional.get() cannot be called on an absent value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()Ljava/lang/Object;
    .registers 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 65
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 69
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78
    if-ne p1, p0, :cond_4

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 82
    const v0, 0x598df91c

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 86
    const-string v0, "Optional.absent()"

    return-object v0
.end method
