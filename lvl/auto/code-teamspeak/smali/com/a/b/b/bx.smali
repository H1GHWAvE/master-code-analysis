.class final Lcom/a/b/b/bx;
.super Lcom/a/b/b/bv;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/a/b/b/bv;


# direct methods
.method constructor <init>(Lcom/a/b/b/bv;Lcom/a/b/b/bv;)V
    .registers 4

    .prologue
    .line 240
    iput-object p1, p0, Lcom/a/b/b/bx;->b:Lcom/a/b/b/bv;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/a/b/b/bv;-><init>(Lcom/a/b/b/bv;B)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Appendable;Ljava/util/Iterator;)Ljava/lang/Appendable;
    .registers 5

    .prologue
    .line 243
    const-string v0, "appendable"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    const-string v0, "parts"

    invoke-static {p2, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    :cond_a
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 246
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 247
    if-eqz v0, :cond_a

    .line 248
    iget-object v1, p0, Lcom/a/b/b/bx;->b:Lcom/a/b/b/bv;

    invoke-virtual {v1, v0}, Lcom/a/b/b/bv;->a(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 252
    :cond_1f
    :goto_1f
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 253
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 254
    if-eqz v0, :cond_1f

    .line 255
    iget-object v1, p0, Lcom/a/b/b/bx;->b:Lcom/a/b/b/bv;

    .line 1066
    iget-object v1, v1, Lcom/a/b/b/bv;->a:Ljava/lang/String;

    .line 255
    invoke-interface {p1, v1}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 256
    iget-object v1, p0, Lcom/a/b/b/bx;->b:Lcom/a/b/b/bv;

    invoke-virtual {v1, v0}, Lcom/a/b/b/bv;->a(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    goto :goto_1f

    .line 259
    :cond_3c
    return-object p1
.end method

.method public final b(Ljava/lang/String;)Lcom/a/b/b/bv;
    .registers 4

    .prologue
    .line 263
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "already specified skipNulls"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c(Ljava/lang/String;)Lcom/a/b/b/bz;
    .registers 4

    .prologue
    .line 267
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t use .skipNulls() with maps"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
