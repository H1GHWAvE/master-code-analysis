.class public final Lcom/a/b/b/ds;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "Chunk [%s] is not a valid entry"


# instance fields
.field private final b:Lcom/a/b/b/di;

.field private final c:Lcom/a/b/b/di;


# direct methods
.method private constructor <init>(Lcom/a/b/b/di;Lcom/a/b/b/di;)V
    .registers 4

    .prologue
    .line 479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 480
    iput-object p1, p0, Lcom/a/b/b/ds;->b:Lcom/a/b/b/di;

    .line 481
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/di;

    iput-object v0, p0, Lcom/a/b/b/ds;->c:Lcom/a/b/b/di;

    .line 482
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/b/di;Lcom/a/b/b/di;B)V
    .registers 4

    .prologue
    .line 473
    invoke-direct {p0, p1, p2}, Lcom/a/b/b/ds;-><init>(Lcom/a/b/b/di;Lcom/a/b/b/di;)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/util/Map;
    .registers 12

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 500
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    .line 501
    iget-object v0, p0, Lcom/a/b/b/ds;->b:Lcom/a/b/b/di;

    invoke-virtual {v0, p1}, Lcom/a/b/b/di;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_11
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_71

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 502
    iget-object v1, p0, Lcom/a/b/b/ds;->c:Lcom/a/b/b/di;

    .line 1103
    invoke-virtual {v1, v0}, Lcom/a/b/b/di;->b(Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v7

    .line 504
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const-string v2, "Chunk [%s] is not a valid entry"

    new-array v8, v3, [Ljava/lang/Object;

    aput-object v0, v8, v4

    invoke-static {v1, v2, v8}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 505
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 506
    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6d

    move v2, v3

    :goto_3d
    const-string v8, "Duplicate key [%s] found."

    new-array v9, v3, [Ljava/lang/Object;

    aput-object v1, v9, v4

    invoke-static {v2, v8, v9}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 508
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v8, "Chunk [%s] is not a valid entry"

    new-array v9, v3, [Ljava/lang/Object;

    aput-object v0, v9, v4

    invoke-static {v2, v8, v9}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 509
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 510
    invoke-interface {v5, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_6f

    move v1, v3

    :goto_63
    const-string v2, "Chunk [%s] is not a valid entry"

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v0, v7, v4

    invoke-static {v1, v2, v7}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_11

    :cond_6d
    move v2, v4

    .line 506
    goto :goto_3d

    :cond_6f
    move v1, v4

    .line 512
    goto :goto_63

    .line 514
    :cond_71
    invoke-static {v5}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
