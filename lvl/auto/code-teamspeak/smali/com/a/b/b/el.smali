.class public final Lcom/a/b/b/el;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/CharSequence;)I
    .registers 9

    .prologue
    const/16 v7, 0x800

    const/4 v0, 0x0

    .line 50
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    move v1, v0

    .line 55
    :goto_8
    if-ge v1, v3, :cond_93

    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v4, 0x80

    if-ge v2, v4, :cond_93

    .line 56
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 60
    :goto_15
    if-ge v1, v3, :cond_91

    .line 61
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    .line 62
    if-ge v4, v7, :cond_25

    .line 63
    rsub-int/lit8 v4, v4, 0x7f

    ushr-int/lit8 v4, v4, 0x1f

    add-int/2addr v2, v4

    .line 60
    add-int/lit8 v1, v1, 0x1

    goto :goto_15

    .line 1079
    :cond_25
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    .line 1081
    :goto_29
    if-ge v1, v4, :cond_6b

    .line 1082
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    .line 1083
    if-ge v5, v7, :cond_39

    .line 1084
    rsub-int/lit8 v5, v5, 0x7f

    ushr-int/lit8 v5, v5, 0x1f

    add-int/2addr v0, v5

    .line 1081
    :cond_36
    :goto_36
    add-int/lit8 v1, v1, 0x1

    goto :goto_29

    .line 1086
    :cond_39
    add-int/lit8 v0, v0, 0x2

    .line 1088
    const v6, 0xd800

    if-gt v6, v5, :cond_36

    const v6, 0xdfff

    if-gt v5, v6, :cond_36

    .line 1090
    invoke-static {p0, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v5

    .line 1091
    const/high16 v6, 0x10000

    if-ge v5, v6, :cond_68

    .line 1092
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unpaired surrogate at index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1094
    :cond_68
    add-int/lit8 v1, v1, 0x1

    goto :goto_36

    .line 65
    :cond_6b
    add-int/2addr v0, v2

    .line 70
    :goto_6c
    if-ge v0, v3, :cond_90

    .line 72
    new-instance v1, Ljava/lang/IllegalArgumentException;

    int-to-long v2, v0

    const-wide v4, 0x100000000L

    add-long/2addr v2, v4

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v4, 0x36

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "UTF-8 length does not fit in int: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 75
    :cond_90
    return v0

    :cond_91
    move v0, v2

    goto :goto_6c

    :cond_93
    move v2, v3

    goto :goto_15
.end method

.method private static a(Ljava/lang/CharSequence;I)I
    .registers 7

    .prologue
    .line 79
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 80
    const/4 v0, 0x0

    move v1, p1

    .line 81
    :goto_6
    if-ge v1, v2, :cond_4a

    .line 82
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 83
    const/16 v4, 0x800

    if-ge v3, v4, :cond_18

    .line 84
    rsub-int/lit8 v3, v3, 0x7f

    ushr-int/lit8 v3, v3, 0x1f

    add-int/2addr v0, v3

    .line 81
    :cond_15
    :goto_15
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 86
    :cond_18
    add-int/lit8 v0, v0, 0x2

    .line 88
    const v4, 0xd800

    if-gt v4, v3, :cond_15

    const v4, 0xdfff

    if-gt v3, v4, :cond_15

    .line 90
    invoke-static {p0, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v3

    .line 91
    const/high16 v4, 0x10000

    if-ge v3, v4, :cond_47

    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unpaired surrogate at index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_47
    add-int/lit8 v1, v1, 0x1

    goto :goto_15

    .line 98
    :cond_4a
    return v0
.end method

.method private static a([B)Z
    .registers 10

    .prologue
    const/16 v8, -0x20

    const/16 v7, -0x60

    const/16 v6, -0x41

    const/4 v1, 0x0

    .line 112
    array-length v0, p0

    .line 1125
    add-int/lit8 v3, v0, 0x0

    .line 1126
    array-length v0, p0

    invoke-static {v1, v3, v0}, Lcom/a/b/b/cn;->a(III)V

    move v0, v1

    .line 1128
    :goto_f
    if-ge v0, v3, :cond_77

    .line 1129
    aget-byte v2, p0, v0

    if-gez v2, :cond_74

    .line 1143
    :cond_15
    :goto_15
    if-ge v0, v3, :cond_77

    .line 1146
    add-int/lit8 v2, v0, 0x1

    aget-byte v0, p0, v0

    if-gez v0, :cond_79

    .line 1148
    if-ge v0, v8, :cond_2f

    .line 1150
    if-ne v2, v3, :cond_23

    move v0, v1

    .line 1189
    :goto_22
    return v0

    .line 1155
    :cond_23
    const/16 v4, -0x3e

    if-lt v0, v4, :cond_2d

    add-int/lit8 v0, v2, 0x1

    aget-byte v2, p0, v2

    if-le v2, v6, :cond_15

    :cond_2d
    move v0, v1

    .line 1156
    goto :goto_22

    .line 1158
    :cond_2f
    const/16 v4, -0x10

    if-ge v0, v4, :cond_51

    .line 1160
    add-int/lit8 v4, v2, 0x1

    if-lt v4, v3, :cond_39

    move v0, v1

    .line 1161
    goto :goto_22

    .line 1163
    :cond_39
    add-int/lit8 v4, v2, 0x1

    aget-byte v2, p0, v2

    .line 1164
    if-gt v2, v6, :cond_4f

    if-ne v0, v8, :cond_43

    if-lt v2, v7, :cond_4f

    :cond_43
    const/16 v5, -0x13

    if-ne v0, v5, :cond_49

    if-le v7, v2, :cond_4f

    :cond_49
    add-int/lit8 v0, v4, 0x1

    aget-byte v2, p0, v4

    if-le v2, v6, :cond_15

    :cond_4f
    move v0, v1

    .line 1171
    goto :goto_22

    .line 1175
    :cond_51
    add-int/lit8 v4, v2, 0x2

    if-lt v4, v3, :cond_57

    move v0, v1

    .line 1176
    goto :goto_22

    .line 1178
    :cond_57
    add-int/lit8 v4, v2, 0x1

    aget-byte v2, p0, v2

    .line 1179
    if-gt v2, v6, :cond_72

    shl-int/lit8 v0, v0, 0x1c

    add-int/lit8 v2, v2, 0x70

    add-int/2addr v0, v2

    shr-int/lit8 v0, v0, 0x1e

    if-nez v0, :cond_72

    add-int/lit8 v2, v4, 0x1

    aget-byte v0, p0, v4

    if-gt v0, v6, :cond_72

    add-int/lit8 v0, v2, 0x1

    aget-byte v2, p0, v2

    if-le v2, v6, :cond_15

    :cond_72
    move v0, v1

    .line 1189
    goto :goto_22

    .line 1128
    :cond_74
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 1133
    :cond_77
    const/4 v0, 0x1

    .line 112
    goto :goto_22

    :cond_79
    move v0, v2

    goto :goto_15
.end method

.method private static a([BI)Z
    .registers 12

    .prologue
    const/4 v1, 0x1

    const/16 v9, -0x20

    const/16 v8, -0x60

    const/16 v7, -0x41

    const/4 v2, 0x0

    .line 125
    add-int/lit8 v4, p1, 0x0

    .line 126
    array-length v0, p0

    invoke-static {v2, v4, v0}, Lcom/a/b/b/cn;->a(III)V

    move v0, v2

    .line 128
    :goto_f
    if-ge v0, v4, :cond_79

    .line 129
    aget-byte v3, p0, v0

    if-gez v3, :cond_76

    .line 2143
    :cond_15
    :goto_15
    if-lt v0, v4, :cond_19

    move v0, v1

    .line 2189
    :goto_18
    return v0

    .line 2146
    :cond_19
    add-int/lit8 v3, v0, 0x1

    aget-byte v0, p0, v0

    if-gez v0, :cond_7b

    .line 2148
    if-ge v0, v9, :cond_31

    .line 2150
    if-ne v3, v4, :cond_25

    move v0, v2

    .line 2151
    goto :goto_18

    .line 2155
    :cond_25
    const/16 v5, -0x3e

    if-lt v0, v5, :cond_2f

    add-int/lit8 v0, v3, 0x1

    aget-byte v3, p0, v3

    if-le v3, v7, :cond_15

    :cond_2f
    move v0, v2

    .line 2156
    goto :goto_18

    .line 2158
    :cond_31
    const/16 v5, -0x10

    if-ge v0, v5, :cond_53

    .line 2160
    add-int/lit8 v5, v3, 0x1

    if-lt v5, v4, :cond_3b

    move v0, v2

    .line 2161
    goto :goto_18

    .line 2163
    :cond_3b
    add-int/lit8 v5, v3, 0x1

    aget-byte v3, p0, v3

    .line 2164
    if-gt v3, v7, :cond_51

    if-ne v0, v9, :cond_45

    if-lt v3, v8, :cond_51

    :cond_45
    const/16 v6, -0x13

    if-ne v0, v6, :cond_4b

    if-le v8, v3, :cond_51

    :cond_4b
    add-int/lit8 v0, v5, 0x1

    aget-byte v3, p0, v5

    if-le v3, v7, :cond_15

    :cond_51
    move v0, v2

    .line 2171
    goto :goto_18

    .line 2175
    :cond_53
    add-int/lit8 v5, v3, 0x2

    if-lt v5, v4, :cond_59

    move v0, v2

    .line 2176
    goto :goto_18

    .line 2178
    :cond_59
    add-int/lit8 v5, v3, 0x1

    aget-byte v3, p0, v3

    .line 2179
    if-gt v3, v7, :cond_74

    shl-int/lit8 v0, v0, 0x1c

    add-int/lit8 v3, v3, 0x70

    add-int/2addr v0, v3

    shr-int/lit8 v0, v0, 0x1e

    if-nez v0, :cond_74

    add-int/lit8 v3, v5, 0x1

    aget-byte v0, p0, v5

    if-gt v0, v7, :cond_74

    add-int/lit8 v0, v3, 0x1

    aget-byte v3, p0, v3

    if-le v3, v7, :cond_15

    :cond_74
    move v0, v2

    .line 2189
    goto :goto_18

    .line 128
    :cond_76
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_79
    move v0, v1

    .line 133
    goto :goto_18

    :cond_7b
    move v0, v3

    goto :goto_15
.end method

.method private static a([BII)Z
    .registers 11

    .prologue
    const/16 v7, -0x20

    const/16 v6, -0x60

    const/4 v1, 0x0

    const/16 v5, -0x41

    .line 143
    :cond_7
    :goto_7
    if-lt p1, p2, :cond_b

    .line 144
    const/4 v0, 0x1

    .line 189
    :goto_a
    return v0

    .line 146
    :cond_b
    add-int/lit8 v2, p1, 0x1

    aget-byte v0, p0, p1

    if-gez v0, :cond_6a

    .line 148
    if-ge v0, v7, :cond_23

    .line 150
    if-ne v2, p2, :cond_17

    move v0, v1

    .line 151
    goto :goto_a

    .line 155
    :cond_17
    const/16 v3, -0x3e

    if-lt v0, v3, :cond_21

    add-int/lit8 v0, v2, 0x1

    aget-byte v2, p0, v2

    if-le v2, v5, :cond_68

    :cond_21
    move v0, v1

    .line 156
    goto :goto_a

    .line 158
    :cond_23
    const/16 v3, -0x10

    if-ge v0, v3, :cond_45

    .line 160
    add-int/lit8 v3, v2, 0x1

    if-lt v3, p2, :cond_2d

    move v0, v1

    .line 161
    goto :goto_a

    .line 163
    :cond_2d
    add-int/lit8 v3, v2, 0x1

    aget-byte v2, p0, v2

    .line 164
    if-gt v2, v5, :cond_43

    if-ne v0, v7, :cond_37

    if-lt v2, v6, :cond_43

    :cond_37
    const/16 v4, -0x13

    if-ne v0, v4, :cond_3d

    if-le v6, v2, :cond_43

    :cond_3d
    add-int/lit8 p1, v3, 0x1

    aget-byte v0, p0, v3

    if-le v0, v5, :cond_7

    :cond_43
    move v0, v1

    .line 171
    goto :goto_a

    .line 175
    :cond_45
    add-int/lit8 v3, v2, 0x2

    if-lt v3, p2, :cond_4b

    move v0, v1

    .line 176
    goto :goto_a

    .line 178
    :cond_4b
    add-int/lit8 v3, v2, 0x1

    aget-byte v2, p0, v2

    .line 179
    if-gt v2, v5, :cond_66

    shl-int/lit8 v0, v0, 0x1c

    add-int/lit8 v2, v2, 0x70

    add-int/2addr v0, v2

    shr-int/lit8 v0, v0, 0x1e

    if-nez v0, :cond_66

    add-int/lit8 v2, v3, 0x1

    aget-byte v0, p0, v3

    if-gt v0, v5, :cond_66

    add-int/lit8 v0, v2, 0x1

    aget-byte v2, p0, v2

    if-le v2, v5, :cond_68

    :cond_66
    move v0, v1

    .line 189
    goto :goto_a

    :cond_68
    move p1, v0

    .line 192
    goto :goto_7

    :cond_6a
    move p1, v2

    goto :goto_7
.end method
