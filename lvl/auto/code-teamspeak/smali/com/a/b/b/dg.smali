.class final Lcom/a/b/b/dg;
.super Lcom/a/b/b/ci;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final b:J


# instance fields
.field private final a:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/a/b/b/ci;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/a/b/b/dg;->a:Ljava/lang/Object;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Lcom/a/b/b/bj;)Lcom/a/b/b/ci;
    .registers 5

    .prologue
    .line 71
    new-instance v0, Lcom/a/b/b/dg;

    iget-object v1, p0, Lcom/a/b/b/dg;->a:Ljava/lang/Object;

    invoke-interface {p1, v1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "the Function passed to Optional.transform() must not return null."

    invoke-static {v1, v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/b/dg;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public final a(Lcom/a/b/b/ci;)Lcom/a/b/b/ci;
    .registers 2

    .prologue
    .line 53
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-object p0
.end method

.method public final a(Lcom/a/b/b/dz;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 58
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v0, p0, Lcom/a/b/b/dg;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 48
    const-string v0, "use Optional.orNull() instead of Optional.or(null)"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    iget-object v0, p0, Lcom/a/b/b/dg;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/a/b/b/dg;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/a/b/b/dg;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/a/b/b/dg;->a:Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 76
    instance-of v0, p1, Lcom/a/b/b/dg;

    if-eqz v0, :cond_f

    .line 77
    check-cast p1, Lcom/a/b/b/dg;

    .line 78
    iget-object v0, p0, Lcom/a/b/b/dg;->a:Ljava/lang/Object;

    iget-object v1, p1, Lcom/a/b/b/dg;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 80
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 84
    const v0, 0x598df91c

    iget-object v1, p0, Lcom/a/b/b/dg;->a:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 88
    iget-object v0, p0, Lcom/a/b/b/dg;->a:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Optional.of("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
