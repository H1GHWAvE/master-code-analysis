.class final Lcom/a/b/b/aq;
.super Lcom/a/b/b/ak;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final b:J


# instance fields
.field final a:Lcom/a/b/b/ak;


# direct methods
.method constructor <init>(Lcom/a/b/b/ak;)V
    .registers 2

    .prologue
    .line 220
    invoke-direct {p0}, Lcom/a/b/b/ak;-><init>()V

    .line 221
    iput-object p1, p0, Lcom/a/b/b/aq;->a:Lcom/a/b/b/ak;

    .line 222
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/b/ak;
    .registers 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/a/b/b/aq;->a:Lcom/a/b/b/ak;

    return-object v0
.end method

.method protected final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 238
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method protected final b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 233
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method final c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 244
    iget-object v0, p0, Lcom/a/b/b/aq;->a:Lcom/a/b/b/ak;

    invoke-virtual {v0, p1}, Lcom/a/b/b/ak;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250
    iget-object v0, p0, Lcom/a/b/b/aq;->a:Lcom/a/b/b/ak;

    invoke-virtual {v0, p1}, Lcom/a/b/b/ak;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 260
    instance-of v0, p1, Lcom/a/b/b/aq;

    if-eqz v0, :cond_f

    .line 261
    check-cast p1, Lcom/a/b/b/aq;

    .line 262
    iget-object v0, p0, Lcom/a/b/b/aq;->a:Lcom/a/b/b/ak;

    iget-object v1, p1, Lcom/a/b/b/aq;->a:Lcom/a/b/b/ak;

    invoke-virtual {v0, v1}, Lcom/a/b/b/ak;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 264
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/a/b/b/aq;->a:Lcom/a/b/b/ak;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 274
    iget-object v0, p0, Lcom/a/b/b/aq;->a:Lcom/a/b/b/ak;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xa

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".reverse()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
