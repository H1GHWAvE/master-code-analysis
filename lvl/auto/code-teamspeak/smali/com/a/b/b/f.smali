.class public abstract enum Lcom/a/b/b/f;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field public static final enum a:Lcom/a/b/b/f;

.field public static final enum b:Lcom/a/b/b/f;

.field public static final enum c:Lcom/a/b/b/f;

.field public static final enum d:Lcom/a/b/b/f;

.field public static final enum e:Lcom/a/b/b/f;

.field private static final synthetic h:[Lcom/a/b/b/f;


# instance fields
.field private final f:Lcom/a/b/b/m;

.field private final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/16 v6, 0x5f

    const/16 v5, 0x5a

    const/16 v4, 0x41

    .line 40
    new-instance v0, Lcom/a/b/b/g;

    const-string v1, "LOWER_HYPHEN"

    const/16 v2, 0x2d

    invoke-static {v2}, Lcom/a/b/b/m;->a(C)Lcom/a/b/b/m;

    move-result-object v2

    const-string v3, "-"

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/b/g;-><init>(Ljava/lang/String;Lcom/a/b/b/m;Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/f;->a:Lcom/a/b/b/f;

    .line 58
    new-instance v0, Lcom/a/b/b/h;

    const-string v1, "LOWER_UNDERSCORE"

    invoke-static {v6}, Lcom/a/b/b/m;->a(C)Lcom/a/b/b/m;

    move-result-object v2

    const-string v3, "_"

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/b/h;-><init>(Ljava/lang/String;Lcom/a/b/b/m;Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/f;->b:Lcom/a/b/b/f;

    .line 76
    new-instance v0, Lcom/a/b/b/i;

    const-string v1, "LOWER_CAMEL"

    invoke-static {v4, v5}, Lcom/a/b/b/m;->a(CC)Lcom/a/b/b/m;

    move-result-object v2

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/b/i;-><init>(Ljava/lang/String;Lcom/a/b/b/m;Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/f;->c:Lcom/a/b/b/f;

    .line 85
    new-instance v0, Lcom/a/b/b/j;

    const-string v1, "UPPER_CAMEL"

    invoke-static {v4, v5}, Lcom/a/b/b/m;->a(CC)Lcom/a/b/b/m;

    move-result-object v2

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/b/j;-><init>(Ljava/lang/String;Lcom/a/b/b/m;Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/f;->d:Lcom/a/b/b/f;

    .line 94
    new-instance v0, Lcom/a/b/b/k;

    const-string v1, "UPPER_UNDERSCORE"

    invoke-static {v6}, Lcom/a/b/b/m;->a(C)Lcom/a/b/b/m;

    move-result-object v2

    const-string v3, "_"

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/b/k;-><init>(Ljava/lang/String;Lcom/a/b/b/m;Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/f;->e:Lcom/a/b/b/f;

    .line 35
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/a/b/b/f;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/b/f;->a:Lcom/a/b/b/f;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/b/f;->b:Lcom/a/b/b/f;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/a/b/b/f;->c:Lcom/a/b/b/f;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/a/b/b/f;->d:Lcom/a/b/b/f;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/a/b/b/f;->e:Lcom/a/b/b/f;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/b/f;->h:[Lcom/a/b/b/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/a/b/b/m;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 113
    iput-object p3, p0, Lcom/a/b/b/f;->f:Lcom/a/b/b/m;

    .line 114
    iput-object p4, p0, Lcom/a/b/b/f;->g:Ljava/lang/String;

    .line 115
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/a/b/b/m;Ljava/lang/String;B)V
    .registers 6

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/a/b/b/f;-><init>(Ljava/lang/String;ILcom/a/b/b/m;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/a/b/b/f;)Lcom/a/b/b/ak;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 159
    new-instance v0, Lcom/a/b/b/l;

    invoke-direct {v0, p0, p1}, Lcom/a/b/b/l;-><init>(Lcom/a/b/b/f;Lcom/a/b/b/f;)V

    return-object v0
.end method

.method static synthetic b(Ljava/lang/String;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 36
    .line 1210
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_6
    return-object p0

    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/a/b/b/e;->b(C)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/b/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_6
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 206
    sget-object v0, Lcom/a/b/b/f;->c:Lcom/a/b/b/f;

    if-ne p0, v0, :cond_9

    invoke-static {p1}, Lcom/a/b/b/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_8
    return-object v0

    :cond_9
    invoke-virtual {p0, p1}, Lcom/a/b/b/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 210
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_6
    return-object p0

    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/a/b/b/e;->b(C)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/b/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_6
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/b/f;
    .registers 2

    .prologue
    .line 35
    const-class v0, Lcom/a/b/b/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/f;

    return-object v0
.end method

.method public static values()[Lcom/a/b/b/f;
    .registers 1

    .prologue
    .line 35
    sget-object v0, Lcom/a/b/b/f;->h:[Lcom/a/b/b/f;

    invoke-virtual {v0}, [Lcom/a/b/b/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/b/f;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/a/b/b/f;Ljava/lang/String;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 123
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    if-ne p1, p0, :cond_9

    :goto_8
    return-object p2

    :cond_9
    invoke-virtual {p0, p1, p2}, Lcom/a/b/b/f;->b(Lcom/a/b/b/f;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_8
.end method

.method abstract a(Ljava/lang/String;)Ljava/lang/String;
.end method

.method b(Lcom/a/b/b/f;Ljava/lang/String;)Ljava/lang/String;
    .registers 9

    .prologue
    const/4 v2, -0x1

    .line 133
    const/4 v1, 0x0

    .line 134
    const/4 v0, 0x0

    move v3, v0

    move-object v0, v1

    move v1, v2

    .line 136
    :goto_6
    iget-object v4, p0, Lcom/a/b/b/f;->f:Lcom/a/b/b/m;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v4, p2, v1}, Lcom/a/b/b/m;->a(Ljava/lang/CharSequence;I)I

    move-result v1

    if-eq v1, v2, :cond_48

    .line 137
    if-nez v3, :cond_3c

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v5, p0, Lcom/a/b/b/f;->g:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    mul-int/lit8 v5, v5, 0x4

    add-int/2addr v4, v5

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 140
    invoke-virtual {p2, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p1, v3}, Lcom/a/b/b/f;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    :goto_2f
    iget-object v3, p1, Lcom/a/b/b/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    iget-object v3, p0, Lcom/a/b/b/f;->g:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v1

    goto :goto_6

    .line 142
    :cond_3c
    invoke-virtual {p2, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/a/b/b/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2f

    .line 147
    :cond_48
    if-nez v3, :cond_4f

    invoke-direct {p1, p2}, Lcom/a/b/b/f;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4e
    return-object v0

    :cond_4f
    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/b/b/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4e
.end method
