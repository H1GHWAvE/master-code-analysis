.class public abstract Lcom/a/b/e/a;
.super Lcom/a/b/e/d;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field private final a:[[C

.field private final b:I

.field private final c:C

.field private final d:C


# direct methods
.method private constructor <init>(Lcom/a/b/e/b;CC)V
    .registers 5

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/a/b/e/d;-><init>()V

    .line 98
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069
    iget-object v0, p1, Lcom/a/b/e/b;->a:[[C

    .line 99
    iput-object v0, p0, Lcom/a/b/e/a;->a:[[C

    .line 100
    iget-object v0, p0, Lcom/a/b/e/a;->a:[[C

    array-length v0, v0

    iput v0, p0, Lcom/a/b/e/a;->b:I

    .line 101
    if-ge p3, p2, :cond_15

    .line 104
    const/4 p3, 0x0

    .line 105
    const p2, 0xffff

    .line 107
    :cond_15
    iput-char p2, p0, Lcom/a/b/e/a;->c:C

    .line 108
    iput-char p3, p0, Lcom/a/b/e/a;->d:C

    .line 109
    return-void
.end method

.method protected constructor <init>(Ljava/util/Map;CC)V
    .registers 5

    .prologue
    .line 77
    invoke-static {p1}, Lcom/a/b/e/b;->a(Ljava/util/Map;)Lcom/a/b/e/b;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/a/b/e/a;-><init>(Lcom/a/b/e/b;CC)V

    .line 78
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 118
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    const/4 v0, 0x0

    :goto_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_24

    .line 120
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 121
    iget v2, p0, Lcom/a/b/e/a;->b:I

    if-ge v1, v2, :cond_18

    iget-object v2, p0, Lcom/a/b/e/a;->a:[[C

    aget-object v2, v2, v1

    if-nez v2, :cond_20

    :cond_18
    iget-char v2, p0, Lcom/a/b/e/a;->d:C

    if-gt v1, v2, :cond_20

    iget-char v2, p0, Lcom/a/b/e/a;->c:C

    if-ge v1, v2, :cond_25

    .line 123
    :cond_20
    invoke-virtual {p0, p1, v0}, Lcom/a/b/e/a;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    .line 126
    :cond_24
    return-object p1

    .line 119
    :cond_25
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method protected abstract a()[C
.end method

.method protected final a(C)[C
    .registers 3

    .prologue
    .line 135
    iget v0, p0, Lcom/a/b/e/a;->b:I

    if-ge p1, v0, :cond_b

    .line 136
    iget-object v0, p0, Lcom/a/b/e/a;->a:[[C

    aget-object v0, v0, p1

    .line 137
    if-eqz v0, :cond_b

    .line 144
    :goto_a
    return-object v0

    .line 141
    :cond_b
    iget-char v0, p0, Lcom/a/b/e/a;->c:C

    if-lt p1, v0, :cond_15

    iget-char v0, p0, Lcom/a/b/e/a;->d:C

    if-gt p1, v0, :cond_15

    .line 142
    const/4 v0, 0x0

    goto :goto_a

    .line 144
    :cond_15
    invoke-virtual {p0}, Lcom/a/b/e/a;->a()[C

    move-result-object v0

    goto :goto_a
.end method
