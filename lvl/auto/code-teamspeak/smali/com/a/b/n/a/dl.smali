.class final Lcom/a/b/n/a/dl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final a:Ljava/util/concurrent/Callable;

.field b:Lcom/a/b/n/a/cx;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Callable;)V
    .registers 3

    .prologue
    .line 1034
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Callable;

    iput-object v0, p0, Lcom/a/b/n/a/dl;->a:Ljava/util/concurrent/Callable;

    .line 1036
    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1040
    :try_start_0
    iget-object v0, p0, Lcom/a/b/n/a/dl;->a:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_5
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_5} :catch_7
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_5} :catch_13

    move-result-object v0

    .line 1048
    :goto_6
    return-object v0

    .line 1041
    :catch_7
    move-exception v0

    .line 1042
    iget-object v1, p0, Lcom/a/b/n/a/dl;->b:Lcom/a/b/n/a/cx;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/n/a/cx;->setException(Ljava/lang/Throwable;)V

    .line 1048
    :goto_11
    const/4 v0, 0x0

    goto :goto_6

    .line 1044
    :catch_13
    move-exception v0

    iget-object v0, p0, Lcom/a/b/n/a/dl;->b:Lcom/a/b/n/a/cx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/cx;->cancel(Z)Z

    goto :goto_11
.end method
