.class final Lcom/a/b/n/a/ao;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field final a:Lcom/a/b/n/a/ew;

.field final b:Z

.field final c:Ljava/lang/Throwable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/a/b/n/a/ew;)V
    .registers 4

    .prologue
    .line 516
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/n/a/ao;-><init>(Lcom/a/b/n/a/ew;ZLjava/lang/Throwable;)V

    .line 517
    return-void
.end method

.method constructor <init>(Lcom/a/b/n/a/ew;ZLjava/lang/Throwable;)V
    .registers 9
    .param p3    # Ljava/lang/Throwable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521
    if-eqz p2, :cond_b

    sget-object v0, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    if-ne p1, v0, :cond_34

    :cond_b
    move v0, v2

    :goto_c
    const-string v3, "shudownWhenStartupFinishes can only be set if state is STARTING. Got %s instead."

    new-array v4, v2, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 524
    if-eqz p3, :cond_36

    move v0, v2

    :goto_18
    sget-object v3, Lcom/a/b/n/a/ew;->f:Lcom/a/b/n/a/ew;

    if-ne p1, v3, :cond_38

    move v3, v2

    :goto_1d
    xor-int/2addr v0, v3

    if-nez v0, :cond_3a

    move v0, v2

    :goto_21
    const-string v3, "A failure cause should be set if and only if the state is failed.  Got %s and %s instead."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v1

    aput-object p3, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 527
    iput-object p1, p0, Lcom/a/b/n/a/ao;->a:Lcom/a/b/n/a/ew;

    .line 528
    iput-boolean p2, p0, Lcom/a/b/n/a/ao;->b:Z

    .line 529
    iput-object p3, p0, Lcom/a/b/n/a/ao;->c:Ljava/lang/Throwable;

    .line 530
    return-void

    :cond_34
    move v0, v1

    .line 521
    goto :goto_c

    :cond_36
    move v0, v1

    .line 524
    goto :goto_18

    :cond_38
    move v3, v1

    goto :goto_1d

    :cond_3a
    move v0, v1

    goto :goto_21
.end method

.method private a()Lcom/a/b/n/a/ew;
    .registers 3

    .prologue
    .line 534
    iget-boolean v0, p0, Lcom/a/b/n/a/ao;->b:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/a/b/n/a/ao;->a:Lcom/a/b/n/a/ew;

    sget-object v1, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    if-ne v0, v1, :cond_d

    .line 535
    sget-object v0, Lcom/a/b/n/a/ew;->d:Lcom/a/b/n/a/ew;

    .line 537
    :goto_c
    return-object v0

    :cond_d
    iget-object v0, p0, Lcom/a/b/n/a/ao;->a:Lcom/a/b/n/a/ew;

    goto :goto_c
.end method

.method private b()Ljava/lang/Throwable;
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 543
    iget-object v0, p0, Lcom/a/b/n/a/ao;->a:Lcom/a/b/n/a/ew;

    sget-object v3, Lcom/a/b/n/a/ew;->f:Lcom/a/b/n/a/ew;

    if-ne v0, v3, :cond_17

    move v0, v1

    :goto_9
    const-string v3, "failureCause() is only valid if the service has failed, service is %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/a/b/n/a/ao;->a:Lcom/a/b/n/a/ew;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 545
    iget-object v0, p0, Lcom/a/b/n/a/ao;->c:Ljava/lang/Throwable;

    return-object v0

    :cond_17
    move v0, v2

    .line 543
    goto :goto_9
.end method
