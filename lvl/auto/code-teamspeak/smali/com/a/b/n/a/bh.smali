.class final Lcom/a/b/n/a/bh;
.super Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/n/a/bi;

.field final synthetic b:Lcom/a/b/n/a/bd;


# direct methods
.method constructor <init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bi;)V
    .registers 3

    .prologue
    .line 917
    iput-object p1, p0, Lcom/a/b/n/a/bh;->b:Lcom/a/b/n/a/bd;

    .line 918
    invoke-direct {p0, p2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;-><init>(Ljava/util/concurrent/locks/ReentrantReadWriteLock;)V

    .line 919
    iput-object p2, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    .line 920
    return-void
.end method


# virtual methods
.method public final lock()V
    .registers 3

    .prologue
    .line 924
    iget-object v0, p0, Lcom/a/b/n/a/bh;->b:Lcom/a/b/n/a/bd;

    iget-object v1, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0, v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V

    .line 926
    :try_start_7
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_10

    .line 928
    iget-object v0, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    .line 929
    return-void

    .line 928
    :catchall_10
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method

.method public final lockInterruptibly()V
    .registers 3

    .prologue
    .line 934
    iget-object v0, p0, Lcom/a/b/n/a/bh;->b:Lcom/a/b/n/a/bd;

    iget-object v1, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0, v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V

    .line 936
    :try_start_7
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lockInterruptibly()V
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_10

    .line 938
    iget-object v0, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    .line 939
    return-void

    .line 938
    :catchall_10
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method

.method public final tryLock()Z
    .registers 3

    .prologue
    .line 944
    iget-object v0, p0, Lcom/a/b/n/a/bh;->b:Lcom/a/b/n/a/bd;

    iget-object v1, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0, v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V

    .line 946
    :try_start_7
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->tryLock()Z
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_11

    move-result v0

    .line 948
    iget-object v1, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    return v0

    :catchall_11
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method

.method public final tryLock(JLjava/util/concurrent/TimeUnit;)Z
    .registers 7

    .prologue
    .line 955
    iget-object v0, p0, Lcom/a/b/n/a/bh;->b:Lcom/a/b/n/a/bd;

    iget-object v1, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0, v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V

    .line 957
    :try_start_7
    invoke-super {p0, p1, p2, p3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_11

    move-result v0

    .line 959
    iget-object v1, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    return v0

    :catchall_11
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method

.method public final unlock()V
    .registers 3

    .prologue
    .line 966
    :try_start_0
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_9

    .line 968
    iget-object v0, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    .line 969
    return-void

    .line 968
    :catchall_9
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/bh;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method
