.class public final Lcom/a/b/n/a/fd;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:Ljava/util/logging/Logger;

.field private static final b:Lcom/a/b/n/a/dt;

.field private static final c:Lcom/a/b/n/a/dt;


# instance fields
.field private final d:Lcom/a/b/n/a/fk;

.field private final e:Lcom/a/b/d/jl;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 127
    const-class v0, Lcom/a/b/n/a/fd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/a/b/n/a/fd;->a:Ljava/util/logging/Logger;

    .line 128
    new-instance v0, Lcom/a/b/n/a/fe;

    const-string v1, "healthy()"

    invoke-direct {v0, v1}, Lcom/a/b/n/a/fe;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/n/a/fd;->b:Lcom/a/b/n/a/dt;

    .line 133
    new-instance v0, Lcom/a/b/n/a/ff;

    const-string v1, "stopped()"

    invoke-direct {v0, v1}, Lcom/a/b/n/a/ff;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/n/a/fd;->c:Lcom/a/b/n/a/dt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Iterable;)V
    .registers 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    invoke-static {p1}, Lcom/a/b/d/jl;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_26

    .line 197
    sget-object v0, Lcom/a/b/n/a/fd;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "ServiceManager configured with no services.  Is your application configured properly?"

    new-instance v5, Lcom/a/b/n/a/fg;

    invoke-direct {v5, v3}, Lcom/a/b/n/a/fg;-><init>(B)V

    invoke-virtual {v0, v1, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 200
    new-instance v0, Lcom/a/b/n/a/fi;

    invoke-direct {v0, v3}, Lcom/a/b/n/a/fi;-><init>(B)V

    invoke-static {v0}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 202
    :cond_26
    new-instance v1, Lcom/a/b/n/a/fk;

    invoke-direct {v1, v0}, Lcom/a/b/n/a/fk;-><init>(Lcom/a/b/d/iz;)V

    iput-object v1, p0, Lcom/a/b/n/a/fd;->d:Lcom/a/b/n/a/fk;

    .line 203
    iput-object v0, p0, Lcom/a/b/n/a/fd;->e:Lcom/a/b/d/jl;

    .line 204
    new-instance v4, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/a/b/n/a/fd;->d:Lcom/a/b/n/a/fk;

    invoke-direct {v4, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 206
    invoke-virtual {v0}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_65

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/et;

    .line 207
    new-instance v1, Lcom/a/b/n/a/fj;

    invoke-direct {v1, v0, v4}, Lcom/a/b/n/a/fj;-><init>(Lcom/a/b/n/a/et;Ljava/lang/ref/WeakReference;)V

    .line 1450
    sget-object v6, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 207
    invoke-interface {v0, v1, v6}, Lcom/a/b/n/a/et;->a(Lcom/a/b/n/a/ev;Ljava/util/concurrent/Executor;)V

    .line 210
    invoke-interface {v0}, Lcom/a/b/n/a/et;->f()Lcom/a/b/n/a/ew;

    move-result-object v1

    sget-object v6, Lcom/a/b/n/a/ew;->a:Lcom/a/b/n/a/ew;

    if-ne v1, v6, :cond_63

    move v1, v2

    :goto_59
    const-string v6, "Can only manage NEW services, %s"

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v0, v7, v3

    invoke-static {v1, v6, v7}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3a

    :cond_63
    move v1, v3

    goto :goto_59

    .line 214
    :cond_65
    iget-object v0, p0, Lcom/a/b/n/a/fd;->d:Lcom/a/b/n/a/fk;

    invoke-virtual {v0}, Lcom/a/b/n/a/fk;->a()V

    .line 215
    return-void
.end method

.method static synthetic a()Ljava/util/logging/Logger;
    .registers 1

    .prologue
    .line 126
    sget-object v0, Lcom/a/b/n/a/fd;->a:Ljava/util/logging/Logger;

    return-object v0
.end method

.method private a(JLjava/util/concurrent/TimeUnit;)V
    .registers 11

    .prologue
    .line 316
    iget-object v1, p0, Lcom/a/b/n/a/fd;->d:Lcom/a/b/n/a/fk;

    .line 3542
    iget-object v0, v1, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    .line 4357
    iget-object v0, v0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 3544
    :try_start_9
    iget-object v0, v1, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    iget-object v2, v1, Lcom/a/b/n/a/fk;->h:Lcom/a/b/n/a/dx;

    invoke-virtual {v0, v2, p1, p2, p3}, Lcom/a/b/n/a/dw;->b(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_60

    .line 3545
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v2, "Timeout waiting for the services to become healthy. The following services have not started: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Lcom/a/b/n/a/fk;->b:Lcom/a/b/d/aac;

    sget-object v4, Lcom/a/b/n/a/ew;->a:Lcom/a/b/n/a/ew;

    sget-object v5, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    invoke-static {v4, v5}, Lcom/a/b/d/lo;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v4

    invoke-static {v4}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/a/b/d/we;->a(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_59
    .catchall {:try_start_9 .. :try_end_59} :catchall_59

    .line 3551
    :catchall_59
    move-exception v0

    iget-object v1, v1, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    throw v0

    .line 3549
    :cond_60
    :try_start_60
    invoke-virtual {v1}, Lcom/a/b/n/a/fk;->d()V
    :try_end_63
    .catchall {:try_start_60 .. :try_end_63} :catchall_59

    .line 3551
    iget-object v0, v1, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 3552
    return-void
.end method

.method private a(Lcom/a/b/n/a/fh;)V
    .registers 4

    .prologue
    .line 261
    iget-object v0, p0, Lcom/a/b/n/a/fd;->d:Lcom/a/b/n/a/fk;

    .line 2450
    sget-object v1, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 261
    invoke-virtual {v0, p1, v1}, Lcom/a/b/n/a/fk;->a(Lcom/a/b/n/a/fh;Ljava/util/concurrent/Executor;)V

    .line 262
    return-void
.end method

.method private a(Lcom/a/b/n/a/fh;Ljava/util/concurrent/Executor;)V
    .registers 4

    .prologue
    .line 241
    iget-object v0, p0, Lcom/a/b/n/a/fd;->d:Lcom/a/b/n/a/fk;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/n/a/fk;->a(Lcom/a/b/n/a/fh;Ljava/util/concurrent/Executor;)V

    .line 242
    return-void
.end method

.method static synthetic b()Lcom/a/b/n/a/dt;
    .registers 1

    .prologue
    .line 126
    sget-object v0, Lcom/a/b/n/a/fd;->c:Lcom/a/b/n/a/dt;

    return-object v0
.end method

.method private b(JLjava/util/concurrent/TimeUnit;)V
    .registers 11

    .prologue
    .line 351
    iget-object v1, p0, Lcom/a/b/n/a/fd;->d:Lcom/a/b/n/a/fk;

    .line 4561
    iget-object v0, v1, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    .line 5357
    iget-object v0, v0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 4563
    :try_start_9
    iget-object v0, v1, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    iget-object v2, v1, Lcom/a/b/n/a/fk;->i:Lcom/a/b/n/a/dx;

    invoke-virtual {v0, v2, p1, p2, p3}, Lcom/a/b/n/a/dw;->b(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_64

    .line 4564
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v2, "Timeout waiting for the services to stop. The following services have not stopped: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Lcom/a/b/n/a/fk;->b:Lcom/a/b/d/aac;

    sget-object v4, Lcom/a/b/n/a/ew;->e:Lcom/a/b/n/a/ew;

    sget-object v5, Lcom/a/b/n/a/ew;->f:Lcom/a/b/n/a/ew;

    invoke-static {v4, v5}, Lcom/a/b/d/lo;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v4

    invoke-static {v4}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v4

    invoke-static {v4}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/a/b/d/we;->a(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5d
    .catchall {:try_start_9 .. :try_end_5d} :catchall_5d

    .line 4570
    :catchall_5d
    move-exception v0

    iget-object v1, v1, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    throw v0

    :cond_64
    iget-object v0, v1, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 4571
    return-void
.end method

.method static synthetic c()Lcom/a/b/n/a/dt;
    .registers 1

    .prologue
    .line 126
    sget-object v0, Lcom/a/b/n/a/fd;->b:Lcom/a/b/n/a/dt;

    return-object v0
.end method

.method private d()Lcom/a/b/n/a/fd;
    .registers 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 273
    iget-object v0, p0, Lcom/a/b/n/a/fd;->e:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/et;

    .line 274
    invoke-interface {v0}, Lcom/a/b/n/a/et;->f()Lcom/a/b/n/a/ew;

    move-result-object v5

    .line 275
    sget-object v1, Lcom/a/b/n/a/ew;->a:Lcom/a/b/n/a/ew;

    if-ne v5, v1, :cond_2a

    move v1, v2

    :goto_1d
    const-string v6, "Service %s is %s, cannot start it."

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v0, v7, v3

    aput-object v5, v7, v2

    invoke-static {v1, v6, v7}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_8

    :cond_2a
    move v1, v3

    goto :goto_1d

    .line 277
    :cond_2c
    iget-object v0, p0, Lcom/a/b/n/a/fd;->e:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_32
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_94

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/et;

    .line 279
    :try_start_3e
    iget-object v3, p0, Lcom/a/b/n/a/fd;->d:Lcom/a/b/n/a/fk;

    .line 2481
    iget-object v1, v3, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    .line 3357
    iget-object v1, v1, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_47
    .catch Ljava/lang/IllegalStateException; {:try_start_3e .. :try_end_47} :catch_63

    .line 2483
    :try_start_47
    iget-object v1, v3, Lcom/a/b/n/a/fk;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/b/dw;

    .line 2484
    if-nez v1, :cond_5a

    .line 2485
    iget-object v1, v3, Lcom/a/b/n/a/fk;->d:Ljava/util/Map;

    invoke-static {}, Lcom/a/b/b/dw;->a()Lcom/a/b/b/dw;

    move-result-object v4

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5a
    .catchall {:try_start_47 .. :try_end_5a} :catchall_8d

    .line 2488
    :cond_5a
    :try_start_5a
    iget-object v1, v3, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    .line 280
    invoke-interface {v0}, Lcom/a/b/n/a/et;->h()Lcom/a/b/n/a/et;
    :try_end_62
    .catch Ljava/lang/IllegalStateException; {:try_start_5a .. :try_end_62} :catch_63

    goto :goto_32

    .line 281
    :catch_63
    move-exception v1

    .line 286
    sget-object v3, Lcom/a/b/n/a/fd;->a:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x18

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Unable to start Service "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_32

    .line 2488
    :catchall_8d
    move-exception v1

    :try_start_8e
    iget-object v3, v3, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    invoke-virtual {v3}, Lcom/a/b/n/a/dw;->a()V

    throw v1
    :try_end_94
    .catch Ljava/lang/IllegalStateException; {:try_start_8e .. :try_end_94} :catch_63

    .line 289
    :cond_94
    return-object p0
.end method

.method private e()V
    .registers 4

    .prologue
    .line 301
    iget-object v0, p0, Lcom/a/b/n/a/fd;->d:Lcom/a/b/n/a/fk;

    .line 3533
    iget-object v1, v0, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    iget-object v2, v0, Lcom/a/b/n/a/fk;->h:Lcom/a/b/n/a/dx;

    invoke-virtual {v1, v2}, Lcom/a/b/n/a/dw;->a(Lcom/a/b/n/a/dx;)V

    .line 3535
    :try_start_9
    invoke-virtual {v0}, Lcom/a/b/n/a/fk;->d()V
    :try_end_c
    .catchall {:try_start_9 .. :try_end_c} :catchall_12

    .line 3537
    iget-object v0, v0, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 3538
    return-void

    .line 3537
    :catchall_12
    move-exception v1

    iget-object v0, v0, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    throw v1
.end method

.method private f()Lcom/a/b/n/a/fd;
    .registers 3

    .prologue
    .line 326
    iget-object v0, p0, Lcom/a/b/n/a/fd;->e:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/et;

    .line 327
    invoke-interface {v0}, Lcom/a/b/n/a/et;->i()Lcom/a/b/n/a/et;

    goto :goto_6

    .line 329
    :cond_16
    return-object p0
.end method

.method private g()V
    .registers 4

    .prologue
    .line 338
    iget-object v0, p0, Lcom/a/b/n/a/fd;->d:Lcom/a/b/n/a/fk;

    .line 4556
    iget-object v1, v0, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    iget-object v2, v0, Lcom/a/b/n/a/fk;->i:Lcom/a/b/n/a/dx;

    invoke-virtual {v1, v2}, Lcom/a/b/n/a/dw;->a(Lcom/a/b/n/a/dx;)V

    .line 4557
    iget-object v0, v0, Lcom/a/b/n/a/fk;->a:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 339
    return-void
.end method

.method private h()Z
    .registers 3

    .prologue
    .line 361
    iget-object v0, p0, Lcom/a/b/n/a/fd;->e:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/et;

    .line 362
    invoke-interface {v0}, Lcom/a/b/n/a/et;->e()Z

    move-result v0

    if-nez v0, :cond_6

    .line 363
    const/4 v0, 0x0

    .line 366
    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x1

    goto :goto_19
.end method

.method private i()Lcom/a/b/d/kk;
    .registers 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/a/b/n/a/fd;->d:Lcom/a/b/n/a/fk;

    invoke-virtual {v0}, Lcom/a/b/n/a/fk;->b()Lcom/a/b/d/kk;

    move-result-object v0

    return-object v0
.end method

.method private j()Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 387
    iget-object v0, p0, Lcom/a/b/n/a/fd;->d:Lcom/a/b/n/a/fk;

    invoke-virtual {v0}, Lcom/a/b/n/a/fk;->c()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 391
    const-class v0, Lcom/a/b/n/a/fd;

    .line 6109
    new-instance v1, Lcom/a/b/b/cc;

    invoke-static {v0}, Lcom/a/b/b/ca;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/a/b/b/cc;-><init>(Ljava/lang/String;B)V

    .line 391
    const-string v0, "services"

    iget-object v2, p0, Lcom/a/b/n/a/fd;->e:Lcom/a/b/d/jl;

    const-class v3, Lcom/a/b/n/a/fi;

    invoke-static {v3}, Lcom/a/b/b/cp;->a(Ljava/lang/Class;)Lcom/a/b/b/co;

    move-result-object v3

    invoke-static {v3}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;

    move-result-object v2

    .line 6185
    invoke-virtual {v1, v0, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    .line 391
    invoke-virtual {v0}, Lcom/a/b/b/cc;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
