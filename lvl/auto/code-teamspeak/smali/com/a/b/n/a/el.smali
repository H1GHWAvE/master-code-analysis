.class public abstract Lcom/a/b/n/a/el;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:Lcom/a/b/n/a/em;

.field private volatile b:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/a/b/n/a/em;)V
    .registers 3

    .prologue
    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/em;

    iput-object v0, p0, Lcom/a/b/n/a/el;->a:Lcom/a/b/n/a/em;

    .line 204
    return-void
.end method

.method private static a(D)Lcom/a/b/n/a/el;
    .registers 4

    .prologue
    .line 1413
    new-instance v0, Lcom/a/b/n/a/en;

    invoke-direct {v0}, Lcom/a/b/n/a/en;-><init>()V

    .line 2138
    new-instance v1, Lcom/a/b/n/a/fw;

    invoke-direct {v1, v0}, Lcom/a/b/n/a/fw;-><init>(Lcom/a/b/n/a/em;)V

    .line 2139
    invoke-direct {v1, p0, p1}, Lcom/a/b/n/a/el;->b(D)V

    .line 129
    return-object v1
.end method

.method private static a(DJLjava/util/concurrent/TimeUnit;)Lcom/a/b/n/a/el;
    .registers 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 168
    const-wide/16 v4, 0x0

    cmp-long v0, p2, v4

    if-ltz v0, :cond_24

    move v0, v1

    :goto_9
    const-string v3, "warmupPeriod must not be negative: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2413
    new-instance v0, Lcom/a/b/n/a/en;

    invoke-direct {v0}, Lcom/a/b/n/a/en;-><init>()V

    .line 3175
    new-instance v1, Lcom/a/b/n/a/fx;

    invoke-direct {v1, v0, p2, p3, p4}, Lcom/a/b/n/a/fx;-><init>(Lcom/a/b/n/a/em;JLjava/util/concurrent/TimeUnit;)V

    .line 3176
    invoke-direct {v1, p0, p1}, Lcom/a/b/n/a/el;->b(D)V

    .line 169
    return-object v1

    :cond_24
    move v0, v2

    .line 168
    goto :goto_9
.end method

.method private static a(Lcom/a/b/n/a/em;D)Lcom/a/b/n/a/el;
    .registers 4
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 138
    new-instance v0, Lcom/a/b/n/a/fw;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/fw;-><init>(Lcom/a/b/n/a/em;)V

    .line 139
    invoke-direct {v0, p1, p2}, Lcom/a/b/n/a/el;->b(D)V

    .line 140
    return-object v0
.end method

.method private static a(Lcom/a/b/n/a/em;DJLjava/util/concurrent/TimeUnit;)Lcom/a/b/n/a/el;
    .registers 7
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 175
    new-instance v0, Lcom/a/b/n/a/fx;

    invoke-direct {v0, p0, p3, p4, p5}, Lcom/a/b/n/a/fx;-><init>(Lcom/a/b/n/a/em;JLjava/util/concurrent/TimeUnit;)V

    .line 176
    invoke-direct {v0, p1, p2}, Lcom/a/b/n/a/el;->b(D)V

    .line 177
    return-object v0
.end method

.method private a(I)Z
    .registers 5

    .prologue
    .line 320
    const-wide/16 v0, 0x0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/a/b/n/a/el;->a(IJLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method private a(IJLjava/util/concurrent/TimeUnit;)Z
    .registers 15

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 350
    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 351
    invoke-static {p1}, Lcom/a/b/n/a/el;->b(I)I

    .line 353
    invoke-direct {p0}, Lcom/a/b/n/a/el;->c()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 354
    :try_start_14
    iget-object v5, p0, Lcom/a/b/n/a/el;->a:Lcom/a/b/n/a/em;

    invoke-virtual {v5}, Lcom/a/b/n/a/em;->a()J

    move-result-wide v6

    .line 3366
    invoke-virtual {p0}, Lcom/a/b/n/a/el;->b()J

    move-result-wide v8

    sub-long v2, v8, v2

    cmp-long v2, v2, v6

    if-gtz v2, :cond_29

    move v2, v1

    .line 355
    :goto_25
    if-nez v2, :cond_2b

    .line 356
    monitor-exit v4

    .line 362
    :goto_28
    return v0

    :cond_29
    move v2, v0

    .line 3366
    goto :goto_25

    .line 358
    :cond_2b
    invoke-direct {p0, p1, v6, v7}, Lcom/a/b/n/a/el;->b(IJ)J

    move-result-wide v2

    .line 360
    monitor-exit v4
    :try_end_30
    .catchall {:try_start_14 .. :try_end_30} :catchall_37

    .line 361
    iget-object v0, p0, Lcom/a/b/n/a/el;->a:Lcom/a/b/n/a/em;

    invoke-virtual {v0, v2, v3}, Lcom/a/b/n/a/em;->a(J)V

    move v0, v1

    .line 362
    goto :goto_28

    .line 360
    :catchall_37
    move-exception v0

    :try_start_38
    monitor-exit v4
    :try_end_39
    .catchall {:try_start_38 .. :try_end_39} :catchall_37

    throw v0
.end method

.method private a(JJ)Z
    .registers 8

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/a/b/n/a/el;->b()J

    move-result-wide v0

    sub-long/2addr v0, p3

    cmp-long v0, v0, p1

    if-gtz v0, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private a(JLjava/util/concurrent/TimeUnit;)Z
    .registers 5

    .prologue
    .line 305
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/a/b/n/a/el;->a(IJLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method private static b(I)I
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 432
    if-lez p0, :cond_13

    move v0, v1

    :goto_5
    const-string v3, "Requested permits (%s) must be positive"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 433
    return p0

    :cond_13
    move v0, v2

    .line 432
    goto :goto_5
.end method

.method private b(IJ)J
    .registers 8

    .prologue
    .line 375
    invoke-virtual {p0, p1, p2, p3}, Lcom/a/b/n/a/el;->a(IJ)J

    move-result-wide v0

    .line 376
    sub-long/2addr v0, p2

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private b(D)V
    .registers 8

    .prologue
    .line 226
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_22

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_22

    const/4 v0, 0x1

    :goto_d
    const-string v1, "rate must be positive"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 228
    invoke-direct {p0}, Lcom/a/b/n/a/el;->c()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 229
    :try_start_17
    iget-object v0, p0, Lcom/a/b/n/a/el;->a:Lcom/a/b/n/a/em;

    invoke-virtual {v0}, Lcom/a/b/n/a/em;->a()J

    move-result-wide v2

    invoke-virtual {p0, p1, p2, v2, v3}, Lcom/a/b/n/a/el;->a(DJ)V

    .line 230
    monitor-exit v1

    return-void

    .line 226
    :cond_22
    const/4 v0, 0x0

    goto :goto_d

    .line 230
    :catchall_24
    move-exception v0

    monitor-exit v1
    :try_end_26
    .catchall {:try_start_17 .. :try_end_26} :catchall_24

    throw v0
.end method

.method private c()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/a/b/n/a/el;->b:Ljava/lang/Object;

    .line 191
    if-nez v0, :cond_11

    .line 192
    monitor-enter p0

    .line 193
    :try_start_5
    iget-object v0, p0, Lcom/a/b/n/a/el;->b:Ljava/lang/Object;

    .line 194
    if-nez v0, :cond_10

    .line 195
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/el;->b:Ljava/lang/Object;

    .line 197
    :cond_10
    monitor-exit p0

    .line 199
    :cond_11
    return-object v0

    .line 197
    :catchall_12
    move-exception v0

    monitor-exit p0
    :try_end_14
    .catchall {:try_start_5 .. :try_end_14} :catchall_12

    throw v0
.end method

.method private d()D
    .registers 5

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/a/b/n/a/el;->c()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 244
    :try_start_5
    invoke-virtual {p0}, Lcom/a/b/n/a/el;->a()D

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 245
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_b

    throw v0
.end method

.method private e()D
    .registers 7

    .prologue
    .line 260
    .line 3273
    invoke-direct {p0}, Lcom/a/b/n/a/el;->g()J

    move-result-wide v0

    .line 3274
    iget-object v2, p0, Lcom/a/b/n/a/el;->a:Lcom/a/b/n/a/em;

    invoke-virtual {v2, v0, v1}, Lcom/a/b/n/a/em;->a(J)V

    .line 3275
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    long-to-double v0, v0

    mul-double/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v2

    long-to-double v2, v2

    div-double/2addr v0, v2

    .line 260
    return-wide v0
.end method

.method private f()D
    .registers 7

    .prologue
    .line 273
    invoke-direct {p0}, Lcom/a/b/n/a/el;->g()J

    move-result-wide v0

    .line 274
    iget-object v2, p0, Lcom/a/b/n/a/el;->a:Lcom/a/b/n/a/em;

    invoke-virtual {v2, v0, v1}, Lcom/a/b/n/a/em;->a(J)V

    .line 275
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    long-to-double v0, v0

    mul-double/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v2

    long-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private g()J
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 285
    invoke-static {v0}, Lcom/a/b/n/a/el;->b(I)I

    .line 286
    invoke-direct {p0}, Lcom/a/b/n/a/el;->c()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 287
    const/4 v0, 0x1

    :try_start_a
    iget-object v2, p0, Lcom/a/b/n/a/el;->a:Lcom/a/b/n/a/em;

    invoke-virtual {v2}, Lcom/a/b/n/a/em;->a()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/a/b/n/a/el;->b(IJ)J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 288
    :catchall_16
    move-exception v0

    monitor-exit v1
    :try_end_18
    .catchall {:try_start_a .. :try_end_18} :catchall_16

    throw v0
.end method

.method private h()Z
    .registers 5

    .prologue
    .line 334
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/a/b/n/a/el;->a(IJLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method abstract a()D
.end method

.method abstract a(IJ)J
.end method

.method abstract a(DJ)V
.end method

.method abstract b()J
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    .prologue
    .line 398
    const-string v0, "RateLimiter[stableRate=%3.1fqps]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/a/b/n/a/el;->d()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
