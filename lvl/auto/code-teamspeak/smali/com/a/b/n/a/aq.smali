.class final Lcom/a/b/n/a/aq;
.super Lcom/a/b/n/a/cd;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/b/n/a/as;

.field private final b:Lcom/a/b/n/a/dp;


# direct methods
.method private constructor <init>()V
    .registers 3

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/a/b/n/a/cd;-><init>()V

    .line 44
    new-instance v0, Lcom/a/b/n/a/as;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/a/b/n/a/as;-><init>(B)V

    iput-object v0, p0, Lcom/a/b/n/a/aq;->a:Lcom/a/b/n/a/as;

    .line 45
    iget-object v0, p0, Lcom/a/b/n/a/aq;->a:Lcom/a/b/n/a/as;

    invoke-static {v0}, Lcom/a/b/n/a/ci;->a(Lcom/a/b/n/a/dp;)Lcom/a/b/n/a/dp;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/n/a/aq;->b:Lcom/a/b/n/a/dp;

    .line 47
    return-void
.end method

.method public static a()Lcom/a/b/n/a/aq;
    .registers 1

    .prologue
    .line 41
    new-instance v0, Lcom/a/b/n/a/aq;

    invoke-direct {v0}, Lcom/a/b/n/a/aq;-><init>()V

    return-object v0
.end method

.method private a(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 67
    invoke-static {p1}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Object;)Lcom/a/b/n/a/dp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/aq;->a(Lcom/a/b/n/a/dp;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/Throwable;)Z
    .registers 3

    .prologue
    .line 76
    invoke-static {p1}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Throwable;)Lcom/a/b/n/a/dp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/aq;->a(Lcom/a/b/n/a/dp;)Z

    move-result v0

    return v0
.end method

.method private d()Z
    .registers 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/a/b/n/a/aq;->a:Lcom/a/b/n/a/as;

    invoke-virtual {v0}, Lcom/a/b/n/a/as;->isDone()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/a/b/n/a/dp;)Z
    .registers 4

    .prologue
    .line 58
    iget-object v1, p0, Lcom/a/b/n/a/aq;->a:Lcom/a/b/n/a/as;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/dp;

    invoke-virtual {v1, v0}, Lcom/a/b/n/a/as;->a(Lcom/a/b/n/a/dp;)Z

    move-result v0

    return v0
.end method

.method protected final bridge synthetic b()Ljava/util/concurrent/Future;
    .registers 2

    .prologue
    .line 37
    .line 1050
    iget-object v0, p0, Lcom/a/b/n/a/aq;->b:Lcom/a/b/n/a/dp;

    .line 37
    return-object v0
.end method

.method protected final c()Lcom/a/b/n/a/dp;
    .registers 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/a/b/n/a/aq;->b:Lcom/a/b/n/a/dp;

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 37
    .line 2050
    iget-object v0, p0, Lcom/a/b/n/a/aq;->b:Lcom/a/b/n/a/dp;

    .line 37
    return-object v0
.end method
