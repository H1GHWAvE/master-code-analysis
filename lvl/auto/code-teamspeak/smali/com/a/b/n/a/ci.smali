.class public final Lcom/a/b/n/a/ci;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:Lcom/a/b/n/a/ap;

.field private static final b:Lcom/a/b/d/yd;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 979
    new-instance v0, Lcom/a/b/n/a/cn;

    invoke-direct {v0}, Lcom/a/b/n/a/cn;-><init>()V

    sput-object v0, Lcom/a/b/n/a/ci;->a:Lcom/a/b/n/a/ap;

    .line 1570
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    new-instance v1, Lcom/a/b/n/a/cq;

    invoke-direct {v1}, Lcom/a/b/n/a/cq;-><init>()V

    invoke-virtual {v0, v1}, Lcom/a/b/d/yd;->a(Lcom/a/b/b/bj;)Lcom/a/b/d/yd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v0

    sput-object v0, Lcom/a/b/n/a/ci;->b:Lcom/a/b/d/yd;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/b/b/bj;)Lcom/a/b/n/a/ap;
    .registers 2

    .prologue
    .line 758
    new-instance v0, Lcom/a/b/n/a/cl;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/cl;-><init>(Lcom/a/b/b/bj;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Exception;)Lcom/a/b/n/a/bc;
    .registers 2

    .prologue
    .line 300
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    new-instance v0, Lcom/a/b/n/a/dd;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/dd;-><init>(Ljava/lang/Exception;)V

    return-object v0
.end method

.method private static a()Lcom/a/b/n/a/dp;
    .registers 1

    .prologue
    .line 285
    new-instance v0, Lcom/a/b/n/a/dc;

    invoke-direct {v0}, Lcom/a/b/n/a/dc;-><init>()V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/jl;ZLjava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
    .registers 5

    .prologue
    .line 1777
    new-instance v0, Lcom/a/b/n/a/cu;

    new-instance v1, Lcom/a/b/n/a/cr;

    invoke-direct {v1}, Lcom/a/b/n/a/cr;-><init>()V

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/a/b/n/a/cu;-><init>(Lcom/a/b/d/iz;ZLjava/util/concurrent/Executor;Lcom/a/b/n/a/db;)V

    return-object v0
.end method

.method public static a(Lcom/a/b/n/a/dp;)Lcom/a/b/n/a/dp;
    .registers 4

    .prologue
    .line 973
    sget-object v0, Lcom/a/b/n/a/ci;->a:Lcom/a/b/n/a/ap;

    .line 6565
    new-instance v1, Lcom/a/b/n/a/cs;

    const/4 v2, 0x0

    invoke-direct {v1, v0, p0, v2}, Lcom/a/b/n/a/cs;-><init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;B)V

    .line 7450
    sget-object v0, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 6567
    invoke-interface {p0, v1, v0}, Lcom/a/b/n/a/dp;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 973
    return-object v1
.end method

.method public static a(Lcom/a/b/n/a/dp;Lcom/a/b/b/bj;)Lcom/a/b/n/a/dp;
    .registers 5

    .prologue
    .line 705
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 706
    new-instance v0, Lcom/a/b/n/a/cs;

    invoke-static {p1}, Lcom/a/b/n/a/ci;->a(Lcom/a/b/b/bj;)Lcom/a/b/n/a/ap;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/a/b/n/a/cs;-><init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;B)V

    .line 5450
    sget-object v1, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 708
    invoke-interface {p0, v0, v1}, Lcom/a/b/n/a/dp;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 709
    return-object v0
.end method

.method private static a(Lcom/a/b/n/a/dp;Lcom/a/b/b/bj;Ljava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
    .registers 6

    .prologue
    .line 751
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 752
    invoke-static {p1}, Lcom/a/b/n/a/ci;->a(Lcom/a/b/b/bj;)Lcom/a/b/n/a/ap;

    move-result-object v0

    .line 5613
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5614
    new-instance v1, Lcom/a/b/n/a/cs;

    const/4 v2, 0x0

    invoke-direct {v1, v0, p0, v2}, Lcom/a/b/n/a/cs;-><init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;B)V

    .line 5628
    new-instance v0, Lcom/a/b/n/a/cj;

    invoke-direct {v0, p2, v1, v1}, Lcom/a/b/n/a/cj;-><init>(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;Lcom/a/b/n/a/g;)V

    .line 6450
    sget-object v2, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 5616
    invoke-interface {p0, v0, v2}, Lcom/a/b/n/a/dp;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 752
    return-object v1
.end method

.method private static a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/ap;)Lcom/a/b/n/a/dp;
    .registers 4

    .prologue
    .line 565
    new-instance v0, Lcom/a/b/n/a/cs;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, Lcom/a/b/n/a/cs;-><init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;B)V

    .line 3450
    sget-object v1, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 567
    invoke-interface {p0, v0, v1}, Lcom/a/b/n/a/dp;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 568
    return-object v0
.end method

.method private static a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/ap;Ljava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
    .registers 6

    .prologue
    .line 613
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 614
    new-instance v0, Lcom/a/b/n/a/cs;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, Lcom/a/b/n/a/cs;-><init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;B)V

    .line 3628
    new-instance v1, Lcom/a/b/n/a/cj;

    invoke-direct {v1, p2, v0, v0}, Lcom/a/b/n/a/cj;-><init>(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;Lcom/a/b/n/a/g;)V

    .line 4450
    sget-object v2, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 616
    invoke-interface {p0, v1, v2}, Lcom/a/b/n/a/dp;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 617
    return-object v0
.end method

.method private static a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/ch;)Lcom/a/b/n/a/dp;
    .registers 4

    .prologue
    .line 379
    .line 2450
    sget-object v0, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 3443
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3444
    new-instance v1, Lcom/a/b/n/a/cy;

    invoke-direct {v1, p0, p1, v0}, Lcom/a/b/n/a/cy;-><init>(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/ch;Ljava/util/concurrent/Executor;)V

    .line 379
    return-object v1
.end method

.method private static a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/ch;Ljava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
    .registers 4

    .prologue
    .line 443
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    new-instance v0, Lcom/a/b/n/a/cy;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/n/a/cy;-><init>(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/ch;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/n/a/dp;
    .registers 4
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 1027
    invoke-static {p0}, Lcom/a/b/d/jl;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    const/4 v1, 0x1

    .line 9450
    sget-object v2, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 1027
    invoke-static {v0, v1, v2}, Lcom/a/b/n/a/ci;->a(Lcom/a/b/d/jl;ZLjava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Lcom/a/b/n/a/dp;
    .registers 2
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 247
    new-instance v0, Lcom/a/b/n/a/dh;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/dh;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Throwable;)Lcom/a/b/n/a/dp;
    .registers 2

    .prologue
    .line 274
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    new-instance v0, Lcom/a/b/n/a/de;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/de;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private static varargs a([Lcom/a/b/n/a/dp;)Lcom/a/b/n/a/dp;
    .registers 4
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 1005
    invoke-static {p0}, Lcom/a/b/d/jl;->a([Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    const/4 v1, 0x1

    .line 8450
    sget-object v2, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 1005
    invoke-static {v0, v1, v2}, Lcom/a/b/n/a/ci;->a(Lcom/a/b/d/jl;ZLjava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Throwable;)Ljava/lang/Exception;
    .registers 6

    .prologue
    .line 1549
    invoke-virtual {p0}, Ljava/lang/Class;->getConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 13567
    sget-object v1, Lcom/a/b/n/a/ci;->b:Lcom/a/b/d/yd;

    invoke-virtual {v1, v0}, Lcom/a/b/d/yd;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 1551
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Constructor;

    .line 1552
    invoke-static {v0, p1}, Lcom/a/b/n/a/ci;->a(Ljava/lang/reflect/Constructor;Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    .line 1553
    if-eqz v0, :cond_12

    .line 1554
    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-nez v1, :cond_2f

    .line 1555
    invoke-virtual {v0, p1}, Ljava/lang/Exception;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1557
    :cond_2f
    return-object v0

    .line 1560
    :cond_30
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x52

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No appropriate constructor for exception of type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in response to chained exception"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static a(Ljava/lang/reflect/Constructor;Ljava/lang/Throwable;)Ljava/lang/Object;
    .registers 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1579
    invoke-virtual {p0}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v2

    .line 1580
    array-length v0, v2

    new-array v3, v0, [Ljava/lang/Object;

    .line 1581
    const/4 v0, 0x0

    :goto_9
    array-length v4, v2

    if-ge v0, v4, :cond_2c

    .line 1582
    aget-object v4, v2, v0

    .line 1583
    const-class v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 1584
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 1581
    :goto_1c
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 1585
    :cond_1f
    const-class v5, Ljava/lang/Throwable;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 1586
    aput-object p1, v3, v0

    goto :goto_1c

    :cond_2a
    move-object v0, v1

    .line 1600
    :goto_2b
    return-object v0

    .line 1592
    :cond_2c
    :try_start_2c
    invoke-virtual {p0, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2c .. :try_end_2f} :catch_31
    .catch Ljava/lang/InstantiationException; {:try_start_2c .. :try_end_2f} :catch_34
    .catch Ljava/lang/IllegalAccessException; {:try_start_2c .. :try_end_2f} :catch_37
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2c .. :try_end_2f} :catch_3a

    move-result-object v0

    goto :goto_2b

    .line 1594
    :catch_31
    move-exception v0

    move-object v0, v1

    goto :goto_2b

    .line 1596
    :catch_34
    move-exception v0

    move-object v0, v1

    goto :goto_2b

    .line 1598
    :catch_37
    move-exception v0

    move-object v0, v1

    goto :goto_2b

    .line 1600
    :catch_3a
    move-exception v0

    move-object v0, v1

    goto :goto_2b
.end method

.method private static a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1507
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1509
    :try_start_3
    invoke-static {p0}, Lcom/a/b/n/a/gs;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_6} :catch_8

    move-result-object v0

    return-object v0

    .line 1511
    :catch_8
    move-exception v0

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 13517
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_19

    .line 13518
    new-instance v1, Lcom/a/b/n/a/bt;

    check-cast v0, Ljava/lang/Error;

    invoke-direct {v1, v0}, Lcom/a/b/n/a/bt;-><init>(Ljava/lang/Error;)V

    throw v1

    .line 13525
    :cond_19
    new-instance v1, Lcom/a/b/n/a/gq;

    invoke-direct {v1, v0}, Lcom/a/b/n/a/gq;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;Ljava/lang/Class;)Ljava/lang/Object;
    .registers 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1440
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1441
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1442
    const-class v0, Ljava/lang/RuntimeException;

    invoke-virtual {v0, p4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1f

    move v0, v1

    :goto_11
    const-string v3, "Futures.get exception type (%s) must not be a RuntimeException"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1446
    :try_start_1a
    invoke-interface {p0, p1, p2, p3}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_1d
    .catch Ljava/lang/InterruptedException; {:try_start_1a .. :try_end_1d} :catch_21
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1a .. :try_end_1d} :catch_2e
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1a .. :try_end_1d} :catch_34

    move-result-object v0

    return-object v0

    :cond_1f
    move v0, v2

    .line 1442
    goto :goto_11

    .line 1447
    :catch_21
    move-exception v0

    .line 1448
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 1449
    invoke-static {p4, v0}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Class;Ljava/lang/Throwable;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    .line 1450
    :catch_2e
    move-exception v0

    .line 1451
    invoke-static {p4, v0}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Class;Ljava/lang/Throwable;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    .line 1453
    :catch_34
    move-exception v0

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 1454
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private static a(Ljava/util/concurrent/Future;Ljava/lang/Class;)Ljava/lang/Object;
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1374
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1375
    const-class v0, Ljava/lang/RuntimeException;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1c

    move v0, v1

    :goto_e
    const-string v3, "Futures.get exception type (%s) must not be a RuntimeException"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1379
    :try_start_17
    invoke-interface {p0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_1a
    .catch Ljava/lang/InterruptedException; {:try_start_17 .. :try_end_1a} :catch_1e
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_17 .. :try_end_1a} :catch_2b

    move-result-object v0

    return-object v0

    :cond_1c
    move v0, v2

    .line 1375
    goto :goto_e

    .line 1380
    :catch_1e
    move-exception v0

    .line 1381
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 1382
    invoke-static {p1, v0}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Class;Ljava/lang/Throwable;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    .line 1384
    :catch_2b
    move-exception v0

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 1385
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private static a(Lcom/a/b/n/a/g;Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)Ljava/lang/Runnable;
    .registers 4

    .prologue
    .line 628
    new-instance v0, Lcom/a/b/n/a/cj;

    invoke-direct {v0, p2, p1, p0}, Lcom/a/b/n/a/cj;-><init>(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;Lcom/a/b/n/a/g;)V

    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .registers 2

    .prologue
    .line 1567
    sget-object v0, Lcom/a/b/n/a/ci;->b:Lcom/a/b/d/yd;

    invoke-virtual {v0, p0}, Lcom/a/b/d/yd;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/concurrent/Future;Lcom/a/b/b/bj;)Ljava/util/concurrent/Future;
    .registers 3

    .prologue
    .line 791
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 792
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 793
    new-instance v0, Lcom/a/b/n/a/cm;

    invoke-direct {v0, p0, p1}, Lcom/a/b/n/a/cm;-><init>(Ljava/util/concurrent/Future;Lcom/a/b/b/bj;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/cg;)V
    .registers 3

    .prologue
    .line 1258
    .line 13450
    sget-object v0, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 1258
    invoke-static {p0, p1, v0}, Lcom/a/b/n/a/ci;->a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/cg;Ljava/util/concurrent/Executor;)V

    .line 1259
    return-void
.end method

.method public static a(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/cg;Ljava/util/concurrent/Executor;)V
    .registers 4

    .prologue
    .line 1300
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1301
    new-instance v0, Lcom/a/b/n/a/cp;

    invoke-direct {v0, p0, p1}, Lcom/a/b/n/a/cp;-><init>(Lcom/a/b/n/a/dp;Lcom/a/b/n/a/cg;)V

    .line 1322
    invoke-interface {p0, v0, p2}, Lcom/a/b/n/a/dp;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1323
    return-void
.end method

.method private static a(Ljava/lang/Throwable;Ljava/lang/Class;)V
    .registers 3

    .prologue
    .line 1460
    instance-of v0, p0, Ljava/lang/Error;

    if-eqz v0, :cond_c

    .line 1461
    new-instance v0, Lcom/a/b/n/a/bt;

    check-cast p0, Ljava/lang/Error;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/bt;-><init>(Ljava/lang/Error;)V

    throw v0

    .line 1463
    :cond_c
    instance-of v0, p0, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_16

    .line 1464
    new-instance v0, Lcom/a/b/n/a/gq;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/gq;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 1466
    :cond_16
    invoke-static {p1, p0}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Class;Ljava/lang/Throwable;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private static b(Lcom/a/b/n/a/dp;Lcom/a/b/b/bj;)Lcom/a/b/n/a/bc;
    .registers 4

    .prologue
    .line 92
    new-instance v1, Lcom/a/b/n/a/di;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/dp;

    invoke-direct {v1, v0, p1}, Lcom/a/b/n/a/di;-><init>(Lcom/a/b/n/a/dp;Lcom/a/b/b/bj;)V

    return-object v1
.end method

.method private static b(Ljava/lang/Object;)Lcom/a/b/n/a/bc;
    .registers 2
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 260
    new-instance v0, Lcom/a/b/n/a/dg;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/dg;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private static b(Lcom/a/b/n/a/dp;)Lcom/a/b/n/a/dp;
    .registers 2

    .prologue
    .line 1091
    new-instance v0, Lcom/a/b/n/a/dj;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/dj;-><init>(Lcom/a/b/n/a/dp;)V

    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;)Lcom/a/b/n/a/dp;
    .registers 4
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 1158
    invoke-static {p0}, Lcom/a/b/d/jl;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    const/4 v1, 0x0

    .line 11450
    sget-object v2, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 1158
    invoke-static {v0, v1, v2}, Lcom/a/b/n/a/ci;->a(Lcom/a/b/d/jl;ZLjava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method private static varargs b([Lcom/a/b/n/a/dp;)Lcom/a/b/n/a/dp;
    .registers 4
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 1137
    invoke-static {p0}, Lcom/a/b/d/jl;->a([Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    const/4 v1, 0x0

    .line 10450
    sget-object v2, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 1137
    invoke-static {v0, v1, v2}, Lcom/a/b/n/a/ci;->a(Lcom/a/b/d/jl;ZLjava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/Throwable;)V
    .registers 2

    .prologue
    .line 1517
    instance-of v0, p0, Ljava/lang/Error;

    if-eqz v0, :cond_c

    .line 1518
    new-instance v0, Lcom/a/b/n/a/bt;

    check-cast p0, Ljava/lang/Error;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/bt;-><init>(Ljava/lang/Error;)V

    throw v0

    .line 1525
    :cond_c
    new-instance v0, Lcom/a/b/n/a/gq;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/gq;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static c(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
    .registers 8
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 12086
    new-instance v1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    .line 1181
    invoke-static {}, Lcom/a/b/d/jl;->h()Lcom/a/b/d/jn;

    move-result-object v2

    .line 1192
    new-instance v3, Lcom/a/b/n/a/eq;

    .line 12450
    sget-object v0, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 1192
    invoke-direct {v3, v0}, Lcom/a/b/n/a/eq;-><init>(Ljava/util/concurrent/Executor;)V

    .line 1193
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_14
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/dp;

    .line 1194
    invoke-static {}, Lcom/a/b/n/a/aq;->a()Lcom/a/b/n/a/aq;

    move-result-object v5

    .line 1196
    invoke-virtual {v1, v5}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 1197
    new-instance v6, Lcom/a/b/n/a/co;

    invoke-direct {v6, v1, v0}, Lcom/a/b/n/a/co;-><init>(Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/a/b/n/a/dp;)V

    invoke-interface {v0, v6, v3}, Lcom/a/b/n/a/dp;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1202
    invoke-virtual {v2, v5}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    goto :goto_14

    .line 1204
    :cond_33
    invoke-virtual {v2}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method
