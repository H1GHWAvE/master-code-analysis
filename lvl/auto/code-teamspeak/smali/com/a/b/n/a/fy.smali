.class public abstract Lcom/a/b/n/a/fy;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:I = 0x400

.field private static final b:Lcom/a/b/b/dz;

.field private static final c:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 282
    new-instance v0, Lcom/a/b/n/a/gd;

    invoke-direct {v0}, Lcom/a/b/n/a/gd;-><init>()V

    sput-object v0, Lcom/a/b/n/a/fy;->b:Lcom/a/b/b/dz;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/a/b/n/a/fy;-><init>()V

    return-void
.end method

.method private static a(II)Lcom/a/b/n/a/fy;
    .registers 5

    .prologue
    .line 236
    new-instance v0, Lcom/a/b/n/a/ge;

    new-instance v1, Lcom/a/b/n/a/gb;

    invoke-direct {v1, p1}, Lcom/a/b/n/a/gb;-><init>(I)V

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/a/b/n/a/ge;-><init>(ILcom/a/b/b/dz;B)V

    return-object v0
.end method

.method private static a(ILcom/a/b/b/dz;)Lcom/a/b/n/a/fy;
    .registers 3

    .prologue
    .line 222
    const/16 v0, 0x400

    if-ge p0, v0, :cond_a

    new-instance v0, Lcom/a/b/n/a/gj;

    invoke-direct {v0, p0, p1}, Lcom/a/b/n/a/gj;-><init>(ILcom/a/b/b/dz;)V

    :goto_9
    return-object v0

    :cond_a
    new-instance v0, Lcom/a/b/n/a/gf;

    invoke-direct {v0, p0, p1}, Lcom/a/b/n/a/gf;-><init>(ILcom/a/b/b/dz;)V

    goto :goto_9
.end method

.method private a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 146
    const-class v0, Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    .line 147
    array-length v0, v3

    if-nez v0, :cond_f

    .line 148
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    .line 186
    :goto_e
    return-object v0

    .line 150
    :cond_f
    array-length v0, v3

    new-array v4, v0, [I

    move v0, v1

    .line 151
    :goto_13
    array-length v2, v3

    if-ge v0, v2, :cond_21

    .line 152
    aget-object v2, v3, v0

    invoke-virtual {p0, v2}, Lcom/a/b/n/a/fy;->b(Ljava/lang/Object;)I

    move-result v2

    aput v2, v4, v0

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 154
    :cond_21
    invoke-static {v4}, Ljava/util/Arrays;->sort([I)V

    .line 156
    aget v2, v4, v1

    .line 157
    invoke-virtual {p0, v2}, Lcom/a/b/n/a/fy;->a(I)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v3, v1

    .line 158
    const/4 v0, 0x1

    move v1, v2

    :goto_2e
    array-length v2, v3

    if-ge v0, v2, :cond_46

    .line 159
    aget v2, v4, v0

    .line 160
    if-ne v2, v1, :cond_3e

    .line 161
    add-int/lit8 v2, v0, -0x1

    aget-object v2, v3, v2

    aput-object v2, v3, v0

    .line 158
    :goto_3b
    add-int/lit8 v0, v0, 0x1

    goto :goto_2e

    .line 163
    :cond_3e
    invoke-virtual {p0, v2}, Lcom/a/b/n/a/fy;->a(I)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v3, v0

    move v1, v2

    .line 164
    goto :goto_3b

    .line 185
    :cond_46
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 186
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_e
.end method

.method private static b(I)Lcom/a/b/n/a/fy;
    .registers 4

    .prologue
    .line 199
    new-instance v0, Lcom/a/b/n/a/ge;

    new-instance v1, Lcom/a/b/n/a/fz;

    invoke-direct {v1}, Lcom/a/b/n/a/fz;-><init>()V

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/a/b/n/a/ge;-><init>(ILcom/a/b/b/dz;B)V

    return-object v0
.end method

.method private static b(II)Lcom/a/b/n/a/fy;
    .registers 3

    .prologue
    .line 252
    new-instance v0, Lcom/a/b/n/a/gc;

    invoke-direct {v0, p1}, Lcom/a/b/n/a/gc;-><init>(I)V

    invoke-static {p0, v0}, Lcom/a/b/n/a/fy;->a(ILcom/a/b/b/dz;)Lcom/a/b/n/a/fy;

    move-result-object v0

    return-object v0
.end method

.method private static c(I)Lcom/a/b/n/a/fy;
    .registers 2

    .prologue
    .line 214
    new-instance v0, Lcom/a/b/n/a/ga;

    invoke-direct {v0}, Lcom/a/b/n/a/ga;-><init>()V

    invoke-static {p0, v0}, Lcom/a/b/n/a/fy;->a(ILcom/a/b/b/dz;)Lcom/a/b/n/a/fy;

    move-result-object v0

    return-object v0
.end method

.method private static d(I)Lcom/a/b/n/a/fy;
    .registers 4

    .prologue
    .line 267
    new-instance v0, Lcom/a/b/n/a/ge;

    sget-object v1, Lcom/a/b/n/a/fy;->b:Lcom/a/b/b/dz;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/a/b/n/a/ge;-><init>(ILcom/a/b/b/dz;B)V

    return-object v0
.end method

.method private static e(I)Lcom/a/b/n/a/fy;
    .registers 2

    .prologue
    .line 278
    sget-object v0, Lcom/a/b/n/a/fy;->b:Lcom/a/b/b/dz;

    invoke-static {p0, v0}, Lcom/a/b/n/a/fy;->a(ILcom/a/b/b/dz;)Lcom/a/b/n/a/fy;

    move-result-object v0

    return-object v0
.end method

.method private static f(I)I
    .registers 3

    .prologue
    .line 446
    const/4 v0, 0x1

    sget-object v1, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {p0, v1}, Lcom/a/b/j/g;->a(ILjava/math/RoundingMode;)I

    move-result v1

    shl-int/2addr v0, v1

    return v0
.end method

.method private static g(I)I
    .registers 3

    .prologue
    .line 459
    ushr-int/lit8 v0, p0, 0x14

    ushr-int/lit8 v1, p0, 0xc

    xor-int/2addr v0, v1

    xor-int/2addr v0, p0

    .line 460
    ushr-int/lit8 v1, v0, 0x7

    xor-int/2addr v1, v0

    ushr-int/lit8 v0, v0, 0x4

    xor-int/2addr v0, v1

    return v0
.end method

.method private static synthetic h(I)I
    .registers 3

    .prologue
    .line 1446
    const/4 v0, 0x1

    sget-object v1, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {p0, v1}, Lcom/a/b/j/g;->a(ILjava/math/RoundingMode;)I

    move-result v1

    shl-int/2addr v0, v1

    .line 88
    return v0
.end method

.method private static synthetic i(I)I
    .registers 3

    .prologue
    .line 88
    .line 1459
    ushr-int/lit8 v0, p0, 0x14

    ushr-int/lit8 v1, p0, 0xc

    xor-int/2addr v0, v1

    xor-int/2addr v0, p0

    .line 1460
    ushr-int/lit8 v1, v0, 0x7

    xor-int/2addr v1, v0

    ushr-int/lit8 v0, v0, 0x4

    xor-int/2addr v0, v1

    .line 88
    return v0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(I)Ljava/lang/Object;
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method abstract b(Ljava/lang/Object;)I
.end method
