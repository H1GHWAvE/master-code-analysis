.class public final Lcom/a/b/n/a/gs;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/concurrent/BlockingQueue;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 219
    const/4 v0, 0x0

    .line 223
    :goto_1
    :try_start_1
    invoke-interface {p0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_4} :catch_f
    .catchall {:try_start_1 .. :try_end_4} :catchall_12

    move-result-object v1

    .line 229
    if-eqz v0, :cond_e

    .line 230
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_e
    return-object v1

    .line 225
    :catch_f
    move-exception v0

    const/4 v0, 0x1

    .line 226
    goto :goto_1

    .line 229
    :catchall_12
    move-exception v1

    if-eqz v0, :cond_1c

    .line 230
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_1c
    throw v1
.end method

.method public static a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 133
    const/4 v0, 0x0

    .line 137
    :goto_1
    :try_start_1
    invoke-interface {p0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_4} :catch_f
    .catchall {:try_start_1 .. :try_end_4} :catchall_12

    move-result-object v1

    .line 143
    if-eqz v0, :cond_e

    .line 144
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_e
    return-object v1

    .line 139
    :catch_f
    move-exception v0

    const/4 v0, 0x1

    .line 140
    goto :goto_1

    .line 143
    :catchall_12
    move-exception v1

    if-eqz v0, :cond_1c

    .line 144
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_1c
    throw v1
.end method

.method public static a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 11

    .prologue
    .line 165
    const/4 v1, 0x0

    .line 167
    :try_start_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 168
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_23

    move-result-wide v4

    add-long/2addr v4, v2

    .line 173
    :goto_a
    :try_start_a
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p0, v2, v3, v0}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_f} :catch_1a
    .catchall {:try_start_a .. :try_end_f} :catchall_23

    move-result-object v0

    .line 180
    if-eqz v1, :cond_19

    .line 181
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_19
    return-object v0

    .line 175
    :catch_1a
    move-exception v0

    const/4 v1, 0x1

    .line 176
    :try_start_1c
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_1f
    .catchall {:try_start_1c .. :try_end_1f} :catchall_23

    move-result-wide v2

    sub-long v2, v4, v2

    .line 177
    goto :goto_a

    .line 180
    :catchall_23
    move-exception v0

    if-eqz v1, :cond_2d

    .line 181
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_2d
    throw v0
.end method

.method private static a(JLjava/util/concurrent/TimeUnit;)V
    .registers 9

    .prologue
    .line 268
    const/4 v1, 0x0

    .line 270
    :try_start_1
    invoke-virtual {p2, p0, p1}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 271
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_22

    move-result-wide v4

    add-long/2addr v4, v2

    .line 275
    :goto_a
    :try_start_a
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_f} :catch_19
    .catchall {:try_start_a .. :try_end_f} :catchall_22

    .line 283
    if-eqz v1, :cond_18

    .line 284
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_18
    return-void

    .line 278
    :catch_19
    move-exception v0

    const/4 v1, 0x1

    .line 279
    :try_start_1b
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_1e
    .catchall {:try_start_1b .. :try_end_1e} :catchall_22

    move-result-wide v2

    sub-long v2, v4, v2

    .line 280
    goto :goto_a

    .line 283
    :catchall_22
    move-exception v0

    if-eqz v1, :cond_2c

    .line 284
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_2c
    throw v0
.end method

.method private static a(Ljava/lang/Thread;)V
    .registers 3

    .prologue
    .line 102
    const/4 v0, 0x0

    .line 106
    :goto_1
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Thread;->join()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_4} :catch_e
    .catchall {:try_start_1 .. :try_end_4} :catchall_11

    .line 113
    if-eqz v0, :cond_d

    .line 114
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_d
    return-void

    .line 109
    :catch_e
    move-exception v0

    const/4 v0, 0x1

    .line 110
    goto :goto_1

    .line 113
    :catchall_11
    move-exception v1

    if-eqz v0, :cond_1b

    .line 114
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_1b
    throw v1
.end method

.method private static a(Ljava/lang/Thread;JLjava/util/concurrent/TimeUnit;)V
    .registers 11

    .prologue
    .line 193
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    const/4 v1, 0x0

    .line 196
    :try_start_4
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 197
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_25

    move-result-wide v4

    add-long/2addr v4, v2

    .line 201
    :goto_d
    :try_start_d
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, v2, v3}, Ljava/util/concurrent/TimeUnit;->timedJoin(Ljava/lang/Thread;J)V
    :try_end_12
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_12} :catch_1c
    .catchall {:try_start_d .. :try_end_12} :catchall_25

    .line 209
    if-eqz v1, :cond_1b

    .line 210
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_1b
    return-void

    .line 204
    :catch_1c
    move-exception v0

    const/4 v1, 0x1

    .line 205
    :try_start_1e
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_21
    .catchall {:try_start_1e .. :try_end_21} :catchall_25

    move-result-wide v2

    sub-long v2, v4, v2

    .line 206
    goto :goto_d

    .line 209
    :catchall_25
    move-exception v0

    if-eqz v1, :cond_2f

    .line 210
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_2f
    throw v0
.end method

.method private static a(Ljava/util/concurrent/BlockingQueue;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 245
    const/4 v0, 0x0

    .line 249
    :goto_1
    :try_start_1
    invoke-interface {p0, p1}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_4} :catch_e
    .catchall {:try_start_1 .. :try_end_4} :catchall_11

    .line 256
    if-eqz v0, :cond_d

    .line 257
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_d
    return-void

    .line 252
    :catch_e
    move-exception v0

    const/4 v0, 0x1

    .line 253
    goto :goto_1

    .line 256
    :catchall_11
    move-exception v1

    if-eqz v0, :cond_1b

    .line 257
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_1b
    throw v1
.end method

.method private static a(Ljava/util/concurrent/CountDownLatch;)V
    .registers 3

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 57
    :goto_1
    :try_start_1
    invoke-virtual {p0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_4} :catch_e
    .catchall {:try_start_1 .. :try_end_4} :catchall_11

    .line 64
    if-eqz v0, :cond_d

    .line 65
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_d
    return-void

    .line 60
    :catch_e
    move-exception v0

    const/4 v0, 0x1

    .line 61
    goto :goto_1

    .line 64
    :catchall_11
    move-exception v1

    if-eqz v0, :cond_1b

    .line 65
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_1b
    throw v1
.end method

.method private static a(Ljava/util/concurrent/CountDownLatch;JLjava/util/concurrent/TimeUnit;)Z
    .registers 11

    .prologue
    .line 77
    const/4 v1, 0x0

    .line 79
    :try_start_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 80
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_23

    move-result-wide v4

    add-long/2addr v4, v2

    .line 85
    :goto_a
    :try_start_a
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v2, v3, v0}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_f} :catch_1a
    .catchall {:try_start_a .. :try_end_f} :catchall_23

    move-result v0

    .line 92
    if-eqz v1, :cond_19

    .line 93
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_19
    return v0

    .line 87
    :catch_1a
    move-exception v0

    const/4 v1, 0x1

    .line 88
    :try_start_1c
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_1f
    .catchall {:try_start_1c .. :try_end_1f} :catchall_23

    move-result-wide v2

    sub-long v2, v4, v2

    .line 89
    goto :goto_a

    .line 92
    :catchall_23
    move-exception v0

    if-eqz v1, :cond_2d

    .line 93
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_2d
    throw v0
.end method

.method private static a(Ljava/util/concurrent/Semaphore;JLjava/util/concurrent/TimeUnit;)Z
    .registers 5

    .prologue
    .line 297
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/n/a/gs;->b(Ljava/util/concurrent/Semaphore;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/util/concurrent/Semaphore;JLjava/util/concurrent/TimeUnit;)Z
    .registers 13

    .prologue
    const/4 v2, 0x1

    .line 308
    const/4 v1, 0x0

    .line 310
    :try_start_2
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    .line 311
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_9
    .catchall {:try_start_2 .. :try_end_9} :catchall_26

    move-result-wide v6

    add-long/2addr v6, v4

    .line 316
    :goto_b
    const/4 v0, 0x1

    :try_start_c
    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0, v4, v5, v3}, Ljava/util/concurrent/Semaphore;->tryAcquire(IJLjava/util/concurrent/TimeUnit;)Z
    :try_end_11
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_11} :catch_1c
    .catchall {:try_start_c .. :try_end_11} :catchall_26

    move-result v0

    .line 323
    if-eqz v1, :cond_1b

    .line 324
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_1b
    return v0

    .line 318
    :catch_1c
    move-exception v0

    .line 319
    :try_start_1d
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_20
    .catchall {:try_start_1d .. :try_end_20} :catchall_31

    move-result-wide v0

    sub-long v0, v6, v0

    move-wide v4, v0

    move v1, v2

    .line 320
    goto :goto_b

    .line 323
    :catchall_26
    move-exception v0

    :goto_27
    if-eqz v1, :cond_30

    .line 324
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_30
    throw v0

    .line 323
    :catchall_31
    move-exception v0

    move v1, v2

    goto :goto_27
.end method
