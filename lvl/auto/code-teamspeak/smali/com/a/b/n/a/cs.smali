.class final Lcom/a/b/n/a/cs;
.super Lcom/a/b/n/a/g;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private b:Lcom/a/b/n/a/ap;

.field private c:Lcom/a/b/n/a/dp;

.field private volatile d:Lcom/a/b/n/a/dp;


# direct methods
.method private constructor <init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;)V
    .registers 4

    .prologue
    .line 860
    invoke-direct {p0}, Lcom/a/b/n/a/g;-><init>()V

    .line 861
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/ap;

    iput-object v0, p0, Lcom/a/b/n/a/cs;->b:Lcom/a/b/n/a/ap;

    .line 862
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/dp;

    iput-object v0, p0, Lcom/a/b/n/a/cs;->c:Lcom/a/b/n/a/dp;

    .line 863
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;B)V
    .registers 4

    .prologue
    .line 851
    invoke-direct {p0, p1, p2}, Lcom/a/b/n/a/cs;-><init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;)V

    return-void
.end method

.method static synthetic a(Lcom/a/b/n/a/cs;)Lcom/a/b/n/a/dp;
    .registers 2

    .prologue
    .line 851
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/n/a/cs;->d:Lcom/a/b/n/a/dp;

    return-object v0
.end method

.method private static a(Ljava/util/concurrent/Future;Z)V
    .registers 2
    .param p0    # Ljava/util/concurrent/Future;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 883
    if-eqz p0, :cond_5

    .line 884
    invoke-interface {p0, p1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 886
    :cond_5
    return-void
.end method


# virtual methods
.method public final cancel(Z)Z
    .registers 3

    .prologue
    .line 871
    invoke-super {p0, p1}, Lcom/a/b/n/a/g;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 874
    iget-object v0, p0, Lcom/a/b/n/a/cs;->c:Lcom/a/b/n/a/dp;

    invoke-static {v0, p1}, Lcom/a/b/n/a/cs;->a(Ljava/util/concurrent/Future;Z)V

    .line 875
    iget-object v0, p0, Lcom/a/b/n/a/cs;->d:Lcom/a/b/n/a/dp;

    invoke-static {v0, p1}, Lcom/a/b/n/a/cs;->a(Ljava/util/concurrent/Future;Z)V

    .line 876
    const/4 v0, 0x1

    .line 878
    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final run()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 893
    :try_start_1
    iget-object v0, p0, Lcom/a/b/n/a/cs;->c:Lcom/a/b/n/a/dp;

    invoke-static {v0}, Lcom/a/b/n/a/gs;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_6} :catch_2e
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_6} :catch_38
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_1 .. :try_end_6} :catch_54
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_6} :catch_61
    .catchall {:try_start_1 .. :try_end_6} :catchall_6a

    move-result-object v0

    .line 906
    :try_start_7
    iget-object v1, p0, Lcom/a/b/n/a/cs;->b:Lcom/a/b/n/a/ap;

    invoke-interface {v1, v0}, Lcom/a/b/n/a/ap;->a(Ljava/lang/Object;)Lcom/a/b/n/a/dp;

    move-result-object v0

    const-string v1, "AsyncFunction may not return null."

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/dp;

    iput-object v0, p0, Lcom/a/b/n/a/cs;->d:Lcom/a/b/n/a/dp;

    .line 909
    invoke-virtual {p0}, Lcom/a/b/n/a/cs;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_45

    .line 1160
    iget-object v1, p0, Lcom/a/b/n/a/g;->a:Lcom/a/b/n/a/h;

    invoke-virtual {v1}, Lcom/a/b/n/a/h;->d()Z

    move-result v1

    .line 910
    invoke-interface {v0, v1}, Lcom/a/b/n/a/dp;->cancel(Z)Z

    .line 911
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/n/a/cs;->d:Lcom/a/b/n/a/dp;
    :try_end_29
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_7 .. :try_end_29} :catch_54
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_29} :catch_61
    .catchall {:try_start_7 .. :try_end_29} :catchall_6a

    .line 943
    iput-object v3, p0, Lcom/a/b/n/a/cs;->b:Lcom/a/b/n/a/ap;

    .line 944
    iput-object v3, p0, Lcom/a/b/n/a/cs;->c:Lcom/a/b/n/a/dp;

    .line 945
    :goto_2d
    return-void

    .line 898
    :catch_2e
    move-exception v0

    const/4 v0, 0x0

    :try_start_30
    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cs;->cancel(Z)Z
    :try_end_33
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_30 .. :try_end_33} :catch_54
    .catch Ljava/lang/Throwable; {:try_start_30 .. :try_end_33} :catch_61
    .catchall {:try_start_30 .. :try_end_33} :catchall_6a

    .line 943
    iput-object v3, p0, Lcom/a/b/n/a/cs;->b:Lcom/a/b/n/a/ap;

    .line 944
    iput-object v3, p0, Lcom/a/b/n/a/cs;->c:Lcom/a/b/n/a/dp;

    goto :goto_2d

    .line 900
    :catch_38
    move-exception v0

    .line 902
    :try_start_39
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cs;->a(Ljava/lang/Throwable;)Z
    :try_end_40
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_39 .. :try_end_40} :catch_54
    .catch Ljava/lang/Throwable; {:try_start_39 .. :try_end_40} :catch_61
    .catchall {:try_start_39 .. :try_end_40} :catchall_6a

    .line 943
    iput-object v3, p0, Lcom/a/b/n/a/cs;->b:Lcom/a/b/n/a/ap;

    .line 944
    iput-object v3, p0, Lcom/a/b/n/a/cs;->c:Lcom/a/b/n/a/dp;

    goto :goto_2d

    .line 914
    :cond_45
    :try_start_45
    new-instance v1, Lcom/a/b/n/a/ct;

    invoke-direct {v1, p0, v0}, Lcom/a/b/n/a/ct;-><init>(Lcom/a/b/n/a/cs;Lcom/a/b/n/a/dp;)V

    .line 1450
    sget-object v2, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 914
    invoke-interface {v0, v1, v2}, Lcom/a/b/n/a/dp;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    :try_end_4f
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_45 .. :try_end_4f} :catch_54
    .catch Ljava/lang/Throwable; {:try_start_45 .. :try_end_4f} :catch_61
    .catchall {:try_start_45 .. :try_end_4f} :catchall_6a

    .line 943
    iput-object v3, p0, Lcom/a/b/n/a/cs;->b:Lcom/a/b/n/a/ap;

    .line 944
    iput-object v3, p0, Lcom/a/b/n/a/cs;->c:Lcom/a/b/n/a/dp;

    goto :goto_2d

    .line 934
    :catch_54
    move-exception v0

    .line 936
    :try_start_55
    invoke-virtual {v0}, Ljava/lang/reflect/UndeclaredThrowableException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cs;->a(Ljava/lang/Throwable;)Z
    :try_end_5c
    .catchall {:try_start_55 .. :try_end_5c} :catchall_6a

    .line 943
    iput-object v3, p0, Lcom/a/b/n/a/cs;->b:Lcom/a/b/n/a/ap;

    .line 944
    iput-object v3, p0, Lcom/a/b/n/a/cs;->c:Lcom/a/b/n/a/dp;

    goto :goto_2d

    .line 937
    :catch_61
    move-exception v0

    .line 940
    :try_start_62
    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cs;->a(Ljava/lang/Throwable;)Z
    :try_end_65
    .catchall {:try_start_62 .. :try_end_65} :catchall_6a

    .line 943
    iput-object v3, p0, Lcom/a/b/n/a/cs;->b:Lcom/a/b/n/a/ap;

    .line 944
    iput-object v3, p0, Lcom/a/b/n/a/cs;->c:Lcom/a/b/n/a/dp;

    goto :goto_2d

    .line 943
    :catchall_6a
    move-exception v0

    iput-object v3, p0, Lcom/a/b/n/a/cs;->b:Lcom/a/b/n/a/ap;

    .line 944
    iput-object v3, p0, Lcom/a/b/n/a/cs;->c:Lcom/a/b/n/a/dp;

    throw v0
.end method
