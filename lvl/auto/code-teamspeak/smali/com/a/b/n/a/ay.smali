.class public final Lcom/a/b/n/a/ay;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/lang/Runnable;Lcom/a/b/b/dz;)Ljava/lang/Runnable;
    .registers 3

    .prologue
    .line 87
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    new-instance v0, Lcom/a/b/n/a/bb;

    invoke-direct {v0, p1, p0}, Lcom/a/b/n/a/bb;-><init>(Lcom/a/b/b/dz;Ljava/lang/Runnable;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Object;)Ljava/util/concurrent/Callable;
    .registers 2
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 41
    new-instance v0, Lcom/a/b/n/a/az;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/az;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method static a(Ljava/util/concurrent/Callable;Lcom/a/b/b/dz;)Ljava/util/concurrent/Callable;
    .registers 3

    .prologue
    .line 59
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    new-instance v0, Lcom/a/b/n/a/ba;

    invoke-direct {v0, p1, p0}, Lcom/a/b/n/a/ba;-><init>(Lcom/a/b/b/dz;Ljava/util/concurrent/Callable;)V

    return-object v0
.end method

.method static a(Ljava/lang/String;Ljava/lang/Thread;)Z
    .registers 3

    .prologue
    .line 111
    :try_start_0
    invoke-virtual {p1, p0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_3} :catch_5

    .line 112
    const/4 v0, 0x1

    .line 114
    :goto_4
    return v0

    :catch_5
    move-exception v0

    const/4 v0, 0x0

    goto :goto_4
.end method

.method private static synthetic b(Ljava/lang/String;Ljava/lang/Thread;)Z
    .registers 3

    .prologue
    .line 33
    invoke-static {p0, p1}, Lcom/a/b/n/a/ay;->a(Ljava/lang/String;Ljava/lang/Thread;)Z

    move-result v0

    return v0
.end method
