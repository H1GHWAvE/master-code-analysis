.class public Lcom/a/b/n/a/at;
.super Ljava/lang/Number;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:J

.field private static final c:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;


# instance fields
.field private volatile transient b:J


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 60
    const-class v0, Lcom/a/b/n/a/at;

    const-string v1, "b"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, Lcom/a/b/n/a/at;->c:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Number;-><init>()V

    .line 77
    return-void
.end method

.method private constructor <init>(D)V
    .registers 6

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Number;-><init>()V

    .line 69
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/b/n/a/at;->b:J

    .line 70
    return-void
.end method

.method private a()D
    .registers 3

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/a/b/n/a/at;->b:J

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method private a(D)V
    .registers 6

    .prologue
    .line 94
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    .line 95
    iput-wide v0, p0, Lcom/a/b/n/a/at;->b:J

    .line 96
    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 4

    .prologue
    .line 250
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 252
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readDouble()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/a/b/n/a/at;->a(D)V

    .line 253
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 4

    .prologue
    .line 240
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 6085
    iget-wide v0, p0, Lcom/a/b/n/a/at;->b:J

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 242
    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeDouble(D)V

    .line 243
    return-void
.end method

.method private a(DD)Z
    .registers 12

    .prologue
    .line 132
    sget-object v0, Lcom/a/b/n/a/at;->c:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v2

    invoke-static {p3, p4}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v4

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->compareAndSet(Ljava/lang/Object;JJ)Z

    move-result v0

    return v0
.end method

.method private b(D)V
    .registers 4

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Lcom/a/b/n/a/at;->a(D)V

    .line 108
    return-void
.end method

.method private b(DD)Z
    .registers 12

    .prologue
    .line 153
    sget-object v0, Lcom/a/b/n/a/at;->c:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v2

    invoke-static {p3, p4}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v4

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->weakCompareAndSet(Ljava/lang/Object;JJ)Z

    move-result v0

    return v0
.end method

.method private c(D)D
    .registers 6

    .prologue
    .line 117
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    .line 118
    sget-object v2, Lcom/a/b/n/a/at;->c:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v2, p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndSet(Ljava/lang/Object;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method private d(D)D
    .registers 12

    .prologue
    .line 166
    :cond_0
    iget-wide v2, p0, Lcom/a/b/n/a/at;->b:J

    .line 167
    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    .line 168
    add-double v0, v6, p1

    .line 169
    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v4

    .line 170
    sget-object v0, Lcom/a/b/n/a/at;->c:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->compareAndSet(Ljava/lang/Object;JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    return-wide v6
.end method

.method private e(D)D
    .registers 12

    .prologue
    .line 184
    :cond_0
    iget-wide v2, p0, Lcom/a/b/n/a/at;->b:J

    .line 185
    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 186
    add-double v6, v0, p1

    .line 187
    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v4

    .line 188
    sget-object v0, Lcom/a/b/n/a/at;->c:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->compareAndSet(Ljava/lang/Object;JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    return-wide v6
.end method


# virtual methods
.method public doubleValue()D
    .registers 3

    .prologue
    .line 230
    .line 5085
    iget-wide v0, p0, Lcom/a/b/n/a/at;->b:J

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 230
    return-wide v0
.end method

.method public floatValue()F
    .registers 3

    .prologue
    .line 223
    .line 4085
    iget-wide v0, p0, Lcom/a/b/n/a/at;->b:J

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 223
    double-to-float v0, v0

    return v0
.end method

.method public intValue()I
    .registers 3

    .prologue
    .line 207
    .line 2085
    iget-wide v0, p0, Lcom/a/b/n/a/at;->b:J

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 207
    double-to-int v0, v0

    return v0
.end method

.method public longValue()J
    .registers 3

    .prologue
    .line 215
    .line 3085
    iget-wide v0, p0, Lcom/a/b/n/a/at;->b:J

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 215
    double-to-long v0, v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 199
    .line 1085
    iget-wide v0, p0, Lcom/a/b/n/a/at;->b:J

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 199
    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
