.class final Lcom/a/b/n/a/en;
.super Lcom/a/b/n/a/em;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/b/dw;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 413
    invoke-direct {p0}, Lcom/a/b/n/a/em;-><init>()V

    .line 414
    invoke-static {}, Lcom/a/b/b/dw;->a()Lcom/a/b/b/dw;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/n/a/en;->a:Lcom/a/b/b/dw;

    return-void
.end method


# virtual methods
.method final a()J
    .registers 3

    .prologue
    .line 418
    iget-object v0, p0, Lcom/a/b/n/a/en;->a:Lcom/a/b/b/dw;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lcom/a/b/b/dw;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method final a(J)V
    .registers 10

    .prologue
    .line 423
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_20

    .line 424
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1268
    const/4 v1, 0x0

    .line 1270
    :try_start_9
    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 1271
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_10
    .catchall {:try_start_9 .. :try_end_10} :catchall_2a

    move-result-wide v4

    add-long/2addr v4, v2

    .line 1275
    :goto_12
    :try_start_12
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
    :try_end_17
    .catch Ljava/lang/InterruptedException; {:try_start_12 .. :try_end_17} :catch_21
    .catchall {:try_start_12 .. :try_end_17} :catchall_2a

    .line 1283
    if-eqz v1, :cond_20

    .line 1284
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_20
    return-void

    .line 1278
    :catch_21
    move-exception v0

    const/4 v1, 0x1

    .line 1279
    :try_start_23
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_2a

    move-result-wide v2

    sub-long v2, v4, v2

    .line 1280
    goto :goto_12

    .line 1283
    :catchall_2a
    move-exception v0

    if-eqz v1, :cond_34

    .line 1284
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_34
    throw v0
.end method
