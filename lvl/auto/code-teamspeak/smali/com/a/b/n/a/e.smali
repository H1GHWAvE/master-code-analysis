.class final Lcom/a/b/n/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/a/b/n/a/c;


# direct methods
.method constructor <init>(Lcom/a/b/n/a/c;)V
    .registers 2

    .prologue
    .line 51
    iput-object p1, p0, Lcom/a/b/n/a/e;->a:Lcom/a/b/n/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 6

    .prologue
    .line 55
    :try_start_0
    iget-object v0, p0, Lcom/a/b/n/a/e;->a:Lcom/a/b/n/a/c;

    iget-object v0, v0, Lcom/a/b/n/a/c;->a:Lcom/a/b/n/a/b;

    invoke-static {}, Lcom/a/b/n/a/b;->a()V

    .line 56
    iget-object v0, p0, Lcom/a/b/n/a/e;->a:Lcom/a/b/n/a/c;

    invoke-virtual {v0}, Lcom/a/b/n/a/c;->c()V

    .line 58
    iget-object v0, p0, Lcom/a/b/n/a/e;->a:Lcom/a/b/n/a/c;

    invoke-virtual {v0}, Lcom/a/b/n/a/c;->e()Z
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_11} :catch_31

    move-result v0

    if-eqz v0, :cond_1b

    .line 60
    :try_start_14
    iget-object v0, p0, Lcom/a/b/n/a/e;->a:Lcom/a/b/n/a/c;

    iget-object v0, v0, Lcom/a/b/n/a/c;->a:Lcom/a/b/n/a/b;

    invoke-virtual {v0}, Lcom/a/b/n/a/b;->b()V
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_1b} :catch_28

    .line 73
    :cond_1b
    :try_start_1b
    iget-object v0, p0, Lcom/a/b/n/a/e;->a:Lcom/a/b/n/a/c;

    iget-object v0, v0, Lcom/a/b/n/a/c;->a:Lcom/a/b/n/a/b;

    invoke-static {}, Lcom/a/b/n/a/b;->c()V

    .line 74
    iget-object v0, p0, Lcom/a/b/n/a/e;->a:Lcom/a/b/n/a/c;

    invoke-virtual {v0}, Lcom/a/b/n/a/c;->d()V
    :try_end_27
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_27} :catch_31

    .line 78
    return-void

    .line 61
    :catch_28
    move-exception v0

    .line 63
    :try_start_29
    iget-object v1, p0, Lcom/a/b/n/a/e;->a:Lcom/a/b/n/a/c;

    iget-object v1, v1, Lcom/a/b/n/a/c;->a:Lcom/a/b/n/a/b;

    invoke-static {}, Lcom/a/b/n/a/b;->c()V
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_30} :catch_3c
    .catch Ljava/lang/Throwable; {:try_start_29 .. :try_end_30} :catch_31

    .line 69
    :goto_30
    :try_start_30
    throw v0
    :try_end_31
    .catch Ljava/lang/Throwable; {:try_start_30 .. :try_end_31} :catch_31

    .line 75
    :catch_31
    move-exception v0

    .line 76
    iget-object v1, p0, Lcom/a/b/n/a/e;->a:Lcom/a/b/n/a/c;

    invoke-virtual {v1, v0}, Lcom/a/b/n/a/c;->a(Ljava/lang/Throwable;)V

    .line 77
    invoke-static {v0}, Lcom/a/b/b/ei;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 64
    :catch_3c
    move-exception v1

    .line 65
    :try_start_3d
    invoke-static {}, Lcom/a/b/n/a/b;->l()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "Error while attempting to shut down the service after failure."

    invoke-virtual {v2, v3, v4, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_48
    .catch Ljava/lang/Throwable; {:try_start_3d .. :try_end_48} :catch_31

    goto :goto_30
.end method
