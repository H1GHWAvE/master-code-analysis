.class public final Lcom/a/b/n/a/av;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/ConcurrentHashMap;

.field private transient b:Ljava/util/Map;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/ConcurrentHashMap;)V
    .registers 3

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap;

    iput-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 59
    return-void
.end method

.method private a(Ljava/lang/Object;)J
    .registers 4

    .prologue
    .line 82
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 83
    if-nez v0, :cond_d

    const-wide/16 v0, 0x0

    :goto_c
    return-wide v0

    :cond_d
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    goto :goto_c
.end method

.method private a(Ljava/lang/Object;J)J
    .registers 10

    .prologue
    .line 106
    :cond_0
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 107
    if-nez v0, :cond_1a

    .line 108
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1, p2, p3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 109
    if-nez v0, :cond_1a

    .line 128
    :goto_19
    return-wide p2

    .line 116
    :cond_1a
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    .line 117
    const-wide/16 v2, 0x0

    cmp-long v1, v4, v2

    if-nez v1, :cond_32

    .line 119
    iget-object v1, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v2, p2, p3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    invoke-virtual {v1, p1, v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_19

    .line 126
    :cond_32
    add-long v2, v4, p2

    .line 127
    invoke-virtual {v0, v4, v5, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v1

    if-eqz v1, :cond_1a

    move-wide p2, v2

    .line 128
    goto :goto_19
.end method

.method private static a()Lcom/a/b/n/a/av;
    .registers 2

    .prologue
    .line 65
    new-instance v0, Lcom/a/b/n/a/av;

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-direct {v0, v1}, Lcom/a/b/n/a/av;-><init>(Ljava/util/concurrent/ConcurrentHashMap;)V

    return-object v0
.end method

.method private static a(Ljava/util/Map;)Lcom/a/b/n/a/av;
    .registers 3

    .prologue
    .line 1065
    new-instance v0, Lcom/a/b/n/a/av;

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-direct {v0, v1}, Lcom/a/b/n/a/av;-><init>(Ljava/util/concurrent/ConcurrentHashMap;)V

    .line 73
    invoke-direct {v0, p0}, Lcom/a/b/n/a/av;->b(Ljava/util/Map;)V

    .line 74
    return-object v0
.end method

.method private a(Ljava/lang/Object;JJ)Z
    .registers 14

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 398
    cmp-long v0, p2, v4

    if-nez v0, :cond_40

    .line 1366
    :cond_7
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 1367
    if-nez v0, :cond_27

    .line 1368
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v2, p4, p5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    invoke-virtual {v0, p1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 1369
    if-nez v0, :cond_27

    move-wide v2, v4

    .line 399
    :cond_21
    :goto_21
    cmp-long v0, v2, v4

    if-nez v0, :cond_3e

    const/4 v0, 0x1

    .line 402
    :goto_26
    return v0

    .line 1375
    :cond_27
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    .line 1376
    cmp-long v6, v2, v4

    if-nez v6, :cond_21

    .line 1378
    iget-object v2, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v3, p4, p5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    invoke-virtual {v2, p1, v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    move-wide v2, v4

    .line 1379
    goto :goto_21

    :cond_3e
    move v0, v1

    .line 399
    goto :goto_26

    .line 401
    :cond_40
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 402
    if-nez v0, :cond_4c

    move v0, v1

    goto :goto_26

    :cond_4c
    invoke-virtual {v0, p2, p3, p4, p5}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v0

    goto :goto_26
.end method

.method private b(Ljava/lang/Object;)J
    .registers 4

    .prologue
    .line 90
    const-wide/16 v0, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/n/a/av;->a(Ljava/lang/Object;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private b(Ljava/lang/Object;J)J
    .registers 12

    .prologue
    const-wide/16 v2, 0x0

    .line 155
    :cond_2
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 156
    if-nez v0, :cond_1d

    .line 157
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1, p2, p3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 158
    if-nez v0, :cond_1d

    move-wide v0, v2

    .line 177
    :goto_1c
    return-wide v0

    .line 165
    :cond_1d
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    .line 166
    cmp-long v1, v4, v2

    if-nez v1, :cond_34

    .line 168
    iget-object v1, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v4, p2, p3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    invoke-virtual {v1, p1, v0, v4}, Ljava/util/concurrent/ConcurrentHashMap;->replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-wide v0, v2

    .line 169
    goto :goto_1c

    .line 175
    :cond_34
    add-long v6, v4, p2

    .line 176
    invoke-virtual {v0, v4, v5, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v1

    if-eqz v1, :cond_1d

    move-wide v0, v4

    .line 177
    goto :goto_1c
.end method

.method private b()V
    .registers 9

    .prologue
    .line 258
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 259
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 260
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_a

    .line 261
    iget-object v3, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_a

    .line 264
    :cond_2e
    return-void
.end method

.method private b(Ljava/util/Map;)V
    .registers 12

    .prologue
    .line 225
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 226
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1190
    :cond_22
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 1191
    if-nez v0, :cond_3b

    .line 1192
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v3, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 1193
    if-eqz v0, :cond_8

    .line 1200
    :cond_3b
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    .line 1201
    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-nez v3, :cond_53

    .line 1203
    iget-object v3, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v6, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v6, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    invoke-virtual {v3, v2, v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    goto :goto_8

    .line 1210
    :cond_53
    invoke-virtual {v0, v6, v7, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v3

    if-eqz v3, :cond_3b

    goto :goto_8

    .line 228
    :cond_5a
    return-void
.end method

.method private c()J
    .registers 6

    .prologue
    .line 272
    const-wide/16 v0, 0x0

    .line 273
    iget-object v2, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 274
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 275
    goto :goto_d

    .line 276
    :cond_20
    return-wide v2
.end method

.method private c(Ljava/lang/Object;)J
    .registers 4

    .prologue
    .line 97
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/n/a/av;->a(Ljava/lang/Object;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private c(Ljava/lang/Object;J)J
    .registers 10

    .prologue
    const-wide/16 v2, 0x0

    .line 190
    :cond_2
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 191
    if-nez v0, :cond_1d

    .line 192
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1, p2, p3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 193
    if-nez v0, :cond_1d

    move-wide v0, v2

    .line 211
    :goto_1c
    return-wide v0

    .line 200
    :cond_1d
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    .line 201
    cmp-long v1, v4, v2

    if-nez v1, :cond_34

    .line 203
    iget-object v1, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v4, p2, p3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    invoke-virtual {v1, p1, v0, v4}, Ljava/util/concurrent/ConcurrentHashMap;->replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-wide v0, v2

    .line 204
    goto :goto_1c

    .line 210
    :cond_34
    invoke-virtual {v0, v4, v5, p2, p3}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v1

    if-eqz v1, :cond_1d

    move-wide v0, v4

    .line 211
    goto :goto_1c
.end method

.method private d(Ljava/lang/Object;)J
    .registers 4

    .prologue
    .line 139
    const-wide/16 v0, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/n/a/av;->b(Ljava/lang/Object;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private d(Ljava/lang/Object;J)J
    .registers 10

    .prologue
    const-wide/16 v2, 0x0

    .line 366
    :cond_2
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 367
    if-nez v0, :cond_1d

    .line 368
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1, p2, p3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 369
    if-nez v0, :cond_1d

    move-wide v0, v2

    .line 385
    :goto_1c
    return-wide v0

    .line 375
    :cond_1d
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    .line 376
    cmp-long v1, v4, v2

    if-nez v1, :cond_34

    .line 378
    iget-object v1, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v4, p2, p3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    invoke-virtual {v1, p1, v0, v4}, Ljava/util/concurrent/ConcurrentHashMap;->replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-wide v0, v2

    .line 379
    goto :goto_1c

    :cond_34
    move-wide v0, v4

    .line 385
    goto :goto_1c
.end method

.method private d()Ljava/util/Map;
    .registers 3

    .prologue
    .line 285
    iget-object v0, p0, Lcom/a/b/n/a/av;->b:Ljava/util/Map;

    .line 286
    if-nez v0, :cond_15

    .line 1290
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Lcom/a/b/n/a/aw;

    invoke-direct {v1, p0}, Lcom/a/b/n/a/aw;-><init>(Lcom/a/b/n/a/av;)V

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 286
    iput-object v0, p0, Lcom/a/b/n/a/av;->b:Ljava/util/Map;

    :cond_15
    return-object v0
.end method

.method private e(Ljava/lang/Object;)J
    .registers 4

    .prologue
    .line 146
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/n/a/av;->b(Ljava/lang/Object;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private e()Ljava/util/Map;
    .registers 3

    .prologue
    .line 290
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Lcom/a/b/n/a/aw;

    invoke-direct {v1, p0}, Lcom/a/b/n/a/aw;-><init>(Lcom/a/b/n/a/av;)V

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private e(Ljava/lang/Object;J)Z
    .registers 12

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 411
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 412
    if-nez v0, :cond_f

    move v0, v1

    .line 429
    :goto_e
    return v0

    .line 416
    :cond_f
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    .line 417
    cmp-long v4, v2, p2

    if-eqz v4, :cond_19

    move v0, v1

    .line 418
    goto :goto_e

    .line 421
    :cond_19
    cmp-long v4, v2, v6

    if-eqz v4, :cond_23

    invoke-virtual {v0, v2, v3, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 423
    :cond_23
    iget-object v1, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 425
    const/4 v0, 0x1

    goto :goto_e

    :cond_2a
    move v0, v1

    .line 429
    goto :goto_e
.end method

.method private f()I
    .registers 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    return v0
.end method

.method private f(Ljava/lang/Object;)J
    .registers 8

    .prologue
    const-wide/16 v2, 0x0

    .line 235
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 236
    if-nez v0, :cond_e

    move-wide v0, v2

    .line 246
    :goto_d
    return-wide v0

    .line 241
    :cond_e
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    .line 242
    cmp-long v1, v4, v2

    if-eqz v1, :cond_1c

    invoke-virtual {v0, v4, v5, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 244
    :cond_1c
    iget-object v1, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-wide v0, v4

    .line 246
    goto :goto_d
.end method

.method private g()Z
    .registers 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method private g(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 303
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private h()V
    .registers 2

    .prologue
    .line 328
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 329
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/a/b/n/a/av;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
