.class public final Lcom/a/b/k/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/a/b/k/b;->a:Ljava/lang/String;

    .line 58
    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/k/b;
    .registers 5

    .prologue
    .line 78
    invoke-static {p0}, Lcom/a/b/k/a;->a(Ljava/lang/String;)Lcom/a/b/k/a;

    move-result-object v1

    .line 79
    invoke-virtual {v1}, Lcom/a/b/k/a;->a()Z

    move-result v0

    if-nez v0, :cond_22

    const/4 v0, 0x1

    :goto_b
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 1094
    iget-object v2, v1, Lcom/a/b/k/a;->a:Ljava/lang/String;

    .line 86
    const/4 v0, 0x0

    .line 88
    :try_start_11
    invoke-static {v2}, Lcom/a/b/k/d;->a(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_14
    .catch Ljava/lang/IllegalArgumentException; {:try_start_11 .. :try_end_14} :catch_24

    move-result-object v0

    move-object v1, v0

    .line 93
    :goto_16
    if-eqz v1, :cond_27

    .line 94
    new-instance v0, Lcom/a/b/k/b;

    invoke-static {v1}, Lcom/a/b/k/d;->a(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/k/b;-><init>(Ljava/lang/String;)V

    .line 103
    :goto_21
    return-object v0

    .line 79
    :cond_22
    const/4 v0, 0x0

    goto :goto_b

    :catch_24
    move-exception v1

    move-object v1, v0

    goto :goto_16

    .line 100
    :cond_27
    invoke-static {v2}, Lcom/a/b/k/f;->a(Ljava/lang/String;)Lcom/a/b/k/f;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Lcom/a/b/k/f;->a()Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 103
    new-instance v0, Lcom/a/b/k/b;

    invoke-virtual {v1}, Lcom/a/b/k/f;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/k/b;-><init>(Ljava/lang/String;)V

    goto :goto_21

    .line 106
    :cond_3b
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "Domain name does not have a recognized public suffix: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_51

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4d
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_51
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4d
.end method

.method private static b(Ljava/lang/String;)Lcom/a/b/k/b;
    .registers 6

    .prologue
    .line 121
    :try_start_0
    invoke-static {p0}, Lcom/a/b/k/b;->a(Ljava/lang/String;)Lcom/a/b/k/b;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    return-object v0

    .line 122
    :catch_5
    move-exception v1

    .line 127
    new-instance v2, Ljava/text/ParseException;

    const-string v3, "Invalid host specifier: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_20

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_18
    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    .line 129
    invoke-virtual {v2, v1}, Ljava/text/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 130
    throw v2

    .line 127
    :cond_20
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_18
.end method

.method private static c(Ljava/lang/String;)Z
    .registers 2

    .prologue
    .line 141
    :try_start_0
    invoke-static {p0}, Lcom/a/b/k/b;->a(Ljava/lang/String;)Lcom/a/b/k/b;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_3} :catch_5

    .line 142
    const/4 v0, 0x1

    .line 144
    :goto_4
    return v0

    :catch_5
    move-exception v0

    const/4 v0, 0x0

    goto :goto_4
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 150
    if-ne p0, p1, :cond_4

    .line 151
    const/4 v0, 0x1

    .line 159
    :goto_3
    return v0

    .line 154
    :cond_4
    instance-of v0, p1, Lcom/a/b/k/b;

    if-eqz v0, :cond_13

    .line 155
    check-cast p1, Lcom/a/b/k/b;

    .line 156
    iget-object v0, p0, Lcom/a/b/k/b;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/a/b/k/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 159
    :cond_13
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/a/b/k/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/a/b/k/b;->a:Ljava/lang/String;

    return-object v0
.end method
