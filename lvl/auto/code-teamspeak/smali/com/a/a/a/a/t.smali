.class public final Lcom/a/a/a/a/t;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "PreferenceObfuscator"


# instance fields
.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/a/a/a/a/r;

.field private d:Landroid/content/SharedPreferences$Editor;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/a/a/a/a/r;)V
    .registers 4

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/a/a/a/a/t;->b:Landroid/content/SharedPreferences;

    .line 41
    iput-object p2, p0, Lcom/a/a/a/a/t;->c:Lcom/a/a/a/a/r;

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/a/a/t;->d:Landroid/content/SharedPreferences$Editor;

    .line 43
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/a/a/a/a/t;->d:Landroid/content/SharedPreferences$Editor;

    if-eqz v0, :cond_c

    .line 73
    iget-object v0, p0, Lcom/a/a/a/a/t;->d:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/a/a/t;->d:Landroid/content/SharedPreferences$Editor;

    .line 76
    :cond_c
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 46
    iget-object v0, p0, Lcom/a/a/a/a/t;->d:Landroid/content/SharedPreferences$Editor;

    if-nez v0, :cond_c

    .line 47
    iget-object v0, p0, Lcom/a/a/a/a/t;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/a/t;->d:Landroid/content/SharedPreferences$Editor;

    .line 49
    :cond_c
    iget-object v0, p0, Lcom/a/a/a/a/t;->c:Lcom/a/a/a/a/r;

    invoke-interface {v0, p2, p1}, Lcom/a/a/a/a/r;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/a/a/a/a/t;->d:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 51
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6

    .prologue
    .line 55
    iget-object v0, p0, Lcom/a/a/a/a/t;->b:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_f

    .line 58
    :try_start_9
    iget-object v1, p0, Lcom/a/a/a/a/t;->c:Lcom/a/a/a/a/r;

    invoke-interface {v1, v0, p1}, Lcom/a/a/a/a/r;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_e
    .catch Lcom/a/a/a/a/y; {:try_start_9 .. :try_end_e} :catch_10

    move-result-object p2

    .line 68
    :cond_f
    :goto_f
    return-object p2

    .line 61
    :catch_10
    move-exception v0

    const-string v0, "PreferenceObfuscator"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Validation error while reading preference: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f
.end method
