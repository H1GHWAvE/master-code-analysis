.class public final Landroid/support/v4/view/q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/support/v4/view/r;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V
    .registers 4

    .prologue
    .line 501
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/view/q;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;B)V

    .line 502
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;B)V
    .registers 6

    .prologue
    .line 514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 515
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-le v0, v1, :cond_11

    .line 516
    new-instance v0, Landroid/support/v4/view/u;

    invoke-direct {v0, p1, p2}, Landroid/support/v4/view/u;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Landroid/support/v4/view/q;->a:Landroid/support/v4/view/r;

    .line 520
    :goto_10
    return-void

    .line 518
    :cond_11
    new-instance v0, Landroid/support/v4/view/s;

    invoke-direct {v0, p1, p2}, Landroid/support/v4/view/s;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Landroid/support/v4/view/q;->a:Landroid/support/v4/view/r;

    goto :goto_10
.end method

.method private a(Landroid/view/GestureDetector$OnDoubleTapListener;)V
    .registers 3

    .prologue
    .line 562
    iget-object v0, p0, Landroid/support/v4/view/q;->a:Landroid/support/v4/view/r;

    invoke-interface {v0, p1}, Landroid/support/v4/view/r;->a(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 563
    return-void
.end method

.method private a(Z)V
    .registers 3

    .prologue
    .line 551
    iget-object v0, p0, Landroid/support/v4/view/q;->a:Landroid/support/v4/view/r;

    invoke-interface {v0, p1}, Landroid/support/v4/view/r;->a(Z)V

    .line 552
    return-void
.end method

.method private a()Z
    .registers 2

    .prologue
    .line 526
    iget-object v0, p0, Landroid/support/v4/view/q;->a:Landroid/support/v4/view/r;

    invoke-interface {v0}, Landroid/support/v4/view/r;->a()Z

    move-result v0

    return v0
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .registers 3

    .prologue
    .line 538
    iget-object v0, p0, Landroid/support/v4/view/q;->a:Landroid/support/v4/view/r;

    invoke-interface {v0, p1}, Landroid/support/v4/view/r;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
