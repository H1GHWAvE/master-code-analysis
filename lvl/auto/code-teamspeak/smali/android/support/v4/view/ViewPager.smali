.class public Landroid/support/v4/view/ViewPager;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# static fields
.field private static final W:I = -0x1

.field private static final af:I = 0x2

.field private static final at:I = 0x0

.field private static final au:I = 0x1

.field private static final av:I = 0x2

.field private static final ay:Landroid/support/v4/view/fa;

.field public static final b:I = 0x0

.field public static final c:I = 0x1

.field public static final d:I = 0x2

.field private static final e:Ljava/lang/String; = "ViewPager"

.field private static final f:Z = false

.field private static final g:Z = false

.field private static final h:I = 0x1

.field private static final i:I = 0x258

.field private static final j:I = 0x19

.field private static final k:I = 0x10

.field private static final l:I = 0x190

.field private static final m:[I

.field private static final o:Ljava/util/Comparator;

.field private static final p:Landroid/view/animation/Interpolator;


# instance fields
.field private A:I

.field private B:Landroid/graphics/drawable/Drawable;

.field private C:I

.field private D:I

.field private E:F

.field private F:F

.field private G:I

.field private H:I

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:I

.field private M:Z

.field private N:Z

.field private O:I

.field private P:I

.field private Q:I

.field private R:F

.field private S:F

.field private T:F

.field private U:F

.field private V:I

.field public a:Ljava/util/List;

.field private aA:I

.field private aa:Landroid/view/VelocityTracker;

.field private ab:I

.field private ac:I

.field private ad:I

.field private ae:I

.field private ag:Z

.field private ah:J

.field private ai:Landroid/support/v4/widget/al;

.field private aj:Landroid/support/v4/widget/al;

.field private ak:Z

.field private al:Z

.field private am:Z

.field private an:I

.field private ao:Landroid/support/v4/view/ev;

.field private ap:Landroid/support/v4/view/ev;

.field private aq:Landroid/support/v4/view/eu;

.field private ar:Landroid/support/v4/view/ew;

.field private as:Ljava/lang/reflect/Method;

.field private aw:I

.field private ax:Ljava/util/ArrayList;

.field private final az:Ljava/lang/Runnable;

.field private n:I

.field private final q:Ljava/util/ArrayList;

.field private final r:Landroid/support/v4/view/er;

.field private final s:Landroid/graphics/Rect;

.field private t:Landroid/support/v4/view/by;

.field private u:I

.field private v:I

.field private w:Landroid/os/Parcelable;

.field private x:Ljava/lang/ClassLoader;

.field private y:Landroid/widget/Scroller;

.field private z:Landroid/support/v4/view/ex;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 105
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100b3

    aput v2, v0, v1

    sput-object v0, Landroid/support/v4/view/ViewPager;->m:[I

    .line 123
    new-instance v0, Landroid/support/v4/view/en;

    invoke-direct {v0}, Landroid/support/v4/view/en;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewPager;->o:Ljava/util/Comparator;

    .line 130
    new-instance v0, Landroid/support/v4/view/eo;

    invoke-direct {v0}, Landroid/support/v4/view/eo;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewPager;->p:Landroid/view/animation/Interpolator;

    .line 230
    new-instance v0, Landroid/support/v4/view/fa;

    invoke-direct {v0}, Landroid/support/v4/view/fa;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewPager;->ay:Landroid/support/v4/view/fa;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 351
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    .line 138
    new-instance v0, Landroid/support/v4/view/er;

    invoke-direct {v0}, Landroid/support/v4/view/er;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->r:Landroid/support/v4/view/er;

    .line 140
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->s:Landroid/graphics/Rect;

    .line 144
    iput v1, p0, Landroid/support/v4/view/ViewPager;->v:I

    .line 145
    iput-object v4, p0, Landroid/support/v4/view/ViewPager;->w:Landroid/os/Parcelable;

    .line 146
    iput-object v4, p0, Landroid/support/v4/view/ViewPager;->x:Ljava/lang/ClassLoader;

    .line 158
    const v0, -0x800001

    iput v0, p0, Landroid/support/v4/view/ViewPager;->E:F

    .line 159
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Landroid/support/v4/view/ViewPager;->F:F

    .line 168
    iput v3, p0, Landroid/support/v4/view/ViewPager;->L:I

    .line 186
    iput v1, p0, Landroid/support/v4/view/ViewPager;->V:I

    .line 213
    iput-boolean v3, p0, Landroid/support/v4/view/ViewPager;->ak:Z

    .line 214
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->al:Z

    .line 248
    new-instance v0, Landroid/support/v4/view/ep;

    invoke-direct {v0, p0}, Landroid/support/v4/view/ep;-><init>(Landroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->az:Ljava/lang/Runnable;

    .line 255
    iput v2, p0, Landroid/support/v4/view/ViewPager;->aA:I

    .line 352
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->d()V

    .line 353
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 356
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    .line 138
    new-instance v0, Landroid/support/v4/view/er;

    invoke-direct {v0}, Landroid/support/v4/view/er;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->r:Landroid/support/v4/view/er;

    .line 140
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->s:Landroid/graphics/Rect;

    .line 144
    iput v1, p0, Landroid/support/v4/view/ViewPager;->v:I

    .line 145
    iput-object v4, p0, Landroid/support/v4/view/ViewPager;->w:Landroid/os/Parcelable;

    .line 146
    iput-object v4, p0, Landroid/support/v4/view/ViewPager;->x:Ljava/lang/ClassLoader;

    .line 158
    const v0, -0x800001

    iput v0, p0, Landroid/support/v4/view/ViewPager;->E:F

    .line 159
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Landroid/support/v4/view/ViewPager;->F:F

    .line 168
    iput v3, p0, Landroid/support/v4/view/ViewPager;->L:I

    .line 186
    iput v1, p0, Landroid/support/v4/view/ViewPager;->V:I

    .line 213
    iput-boolean v3, p0, Landroid/support/v4/view/ViewPager;->ak:Z

    .line 214
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->al:Z

    .line 248
    new-instance v0, Landroid/support/v4/view/ep;

    invoke-direct {v0, p0}, Landroid/support/v4/view/ep;-><init>(Landroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->az:Ljava/lang/Runnable;

    .line 255
    iput v2, p0, Landroid/support/v4/view/ViewPager;->aA:I

    .line 357
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->d()V

    .line 358
    return-void
.end method

.method private static a(F)F
    .registers 5

    .prologue
    .line 804
    const/high16 v0, 0x3f000000    # 0.5f

    sub-float v0, p0, v0

    .line 805
    float-to-double v0, v0

    const-wide v2, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v0, v2

    double-to-float v0, v0

    .line 806
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private a(IFII)I
    .registers 8

    .prologue
    .line 2239
    invoke-static {p4}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->ad:I

    if-le v0, v1, :cond_43

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->ab:I

    if-le v0, v1, :cond_43

    .line 2240
    if-lez p3, :cond_40

    .line 2246
    :goto_12
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3f

    .line 2247
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    .line 2248
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/er;

    .line 2251
    iget v0, v0, Landroid/support/v4/view/er;->b:I

    iget v1, v1, Landroid/support/v4/view/er;->b:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 2254
    :cond_3f
    return p1

    .line 2240
    :cond_40
    add-int/lit8 p1, p1, 0x1

    goto :goto_12

    .line 2242
    :cond_43
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    if-lt p1, v0, :cond_4f

    const v0, 0x3ecccccd    # 0.4f

    .line 2243
    :goto_4a
    int-to-float v1, p1

    add-float/2addr v1, p2

    add-float/2addr v0, v1

    float-to-int p1, v0

    goto :goto_12

    .line 2242
    :cond_4f
    const v0, 0x3f19999a    # 0.6f

    goto :goto_4a
.end method

.method private a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 2668
    if-nez p1, :cond_5e

    .line 2669
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2671
    :goto_8
    if-nez p2, :cond_f

    .line 2672
    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    move-object v0, v1

    .line 2690
    :goto_e
    return-object v0

    .line 2675
    :cond_f
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 2676
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 2677
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 2678
    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 2680
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2681
    :goto_2b
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_5c

    if-eq v0, p0, :cond_5c

    .line 2682
    check-cast v0, Landroid/view/ViewGroup;

    .line 2683
    iget v2, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLeft()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 2684
    iget v2, v1, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getRight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 2685
    iget v2, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 2686
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBottom()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 2688
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_2b

    :cond_5c
    move-object v0, v1

    .line 2690
    goto :goto_e

    :cond_5e
    move-object v1, p1

    goto :goto_8
.end method

.method private a(Landroid/view/View;)Landroid/support/v4/view/er;
    .registers 6

    .prologue
    .line 1364
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_21

    .line 1365
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    .line 1366
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    iget-object v3, v0, Landroid/support/v4/view/er;->a:Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, Landroid/support/v4/view/by;->a(Landroid/view/View;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 1370
    :goto_1c
    return-object v0

    .line 1364
    :cond_1d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1370
    :cond_21
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method private a(I)V
    .registers 20

    .prologue
    .line 956
    const/4 v3, 0x0

    .line 957
    const/4 v2, 0x2

    .line 958
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v4/view/ViewPager;->u:I

    move/from16 v0, p1

    if-eq v4, v0, :cond_31f

    .line 959
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/view/ViewPager;->u:I

    move/from16 v0, p1

    if-ge v2, v0, :cond_30

    const/16 v2, 0x42

    .line 960
    :goto_14
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v4/view/ViewPager;->u:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Landroid/support/v4/view/ViewPager;->b(I)Landroid/support/v4/view/er;

    move-result-object v3

    .line 961
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/ViewPager;->u:I

    move-object v4, v3

    move v3, v2

    .line 964
    :goto_26
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-nez v2, :cond_33

    .line 965
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->g()V

    .line 1141
    :cond_2f
    :goto_2f
    return-void

    .line 959
    :cond_30
    const/16 v2, 0x11

    goto :goto_14

    .line 973
    :cond_33
    move-object/from16 v0, p0

    iget-boolean v2, v0, Landroid/support/v4/view/ViewPager;->K:Z

    if-eqz v2, :cond_3d

    .line 975
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->g()V

    goto :goto_2f

    .line 982
    :cond_3d
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-eqz v2, :cond_2f

    .line 988
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/view/ViewPager;->L:I

    .line 989
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v4/view/ViewPager;->u:I

    sub-int/2addr v6, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 990
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v5}, Landroid/support/v4/view/by;->e()I

    move-result v12

    .line 991
    add-int/lit8 v5, v12, -0x1

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v4/view/ViewPager;->u:I

    add-int/2addr v2, v6

    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v13

    .line 993
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/view/ViewPager;->n:I

    if-eq v12, v2, :cond_cd

    .line 996
    :try_start_6a
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;
    :try_end_75
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_6a .. :try_end_75} :catch_c3

    move-result-object v2

    .line 1000
    :goto_76
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "The application\'s PagerAdapter changed the adapter\'s contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v4/view/ViewPager;->n:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Pager id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " Pager class: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " Problematic adapter: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 998
    :catch_c3
    move-exception v2

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_76

    .line 1010
    :cond_cd
    const/4 v6, 0x0

    .line 1011
    const/4 v2, 0x0

    move v5, v2

    :goto_d0
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v5, v2, :cond_31c

    .line 1012
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/er;

    .line 1013
    iget v7, v2, Landroid/support/v4/view/er;->b:I

    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/v4/view/ViewPager;->u:I

    if-lt v7, v8, :cond_160

    .line 1014
    iget v7, v2, Landroid/support/v4/view/er;->b:I

    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/v4/view/ViewPager;->u:I

    if-ne v7, v8, :cond_31c

    .line 1019
    :goto_f4
    if-nez v2, :cond_319

    if-lez v12, :cond_319

    .line 1020
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/view/ViewPager;->u:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5}, Landroid/support/v4/view/ViewPager;->b(II)Landroid/support/v4/view/er;

    move-result-object v2

    move-object v10, v2

    .line 1026
    :goto_103
    if-eqz v10, :cond_282

    .line 1027
    const/4 v9, 0x0

    .line 1028
    add-int/lit8 v8, v5, -0x1

    .line 1029
    if-ltz v8, :cond_165

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/er;

    .line 1030
    :goto_114
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v14

    .line 1031
    if-gtz v14, :cond_167

    const/4 v6, 0x0

    .line 1033
    :goto_11b
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v4/view/ViewPager;->u:I

    add-int/lit8 v7, v7, -0x1

    move/from16 v16, v7

    move v7, v9

    move/from16 v9, v16

    move/from16 v17, v8

    move v8, v5

    move/from16 v5, v17

    :goto_12b
    if-ltz v9, :cond_1ad

    .line 1034
    cmpl-float v15, v7, v6

    if-ltz v15, :cond_177

    if-ge v9, v11, :cond_177

    .line 1035
    if-eqz v2, :cond_1ad

    .line 1038
    iget v15, v2, Landroid/support/v4/view/er;->b:I

    if-ne v9, v15, :cond_15d

    iget-boolean v15, v2, Landroid/support/v4/view/er;->c:Z

    if-nez v15, :cond_15d

    .line 1039
    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v15, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1040
    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    iget-object v2, v2, Landroid/support/v4/view/er;->a:Ljava/lang/Object;

    invoke-virtual {v15, v9, v2}, Landroid/support/v4/view/by;->a(ILjava/lang/Object;)V

    .line 1045
    add-int/lit8 v5, v5, -0x1

    .line 1046
    add-int/lit8 v8, v8, -0x1

    .line 1047
    if-ltz v5, :cond_175

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/er;

    .line 1033
    :cond_15d
    :goto_15d
    add-int/lit8 v9, v9, -0x1

    goto :goto_12b

    .line 1011
    :cond_160
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_d0

    .line 1029
    :cond_165
    const/4 v2, 0x0

    goto :goto_114

    .line 1031
    :cond_167
    const/high16 v6, 0x40000000    # 2.0f

    iget v7, v10, Landroid/support/v4/view/er;->d:F

    sub-float/2addr v6, v7

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v7

    int-to-float v7, v7

    int-to-float v15, v14

    div-float/2addr v7, v15

    add-float/2addr v6, v7

    goto :goto_11b

    .line 1047
    :cond_175
    const/4 v2, 0x0

    goto :goto_15d

    .line 1049
    :cond_177
    if-eqz v2, :cond_191

    iget v15, v2, Landroid/support/v4/view/er;->b:I

    if-ne v9, v15, :cond_191

    .line 1050
    iget v2, v2, Landroid/support/v4/view/er;->d:F

    add-float/2addr v7, v2

    .line 1051
    add-int/lit8 v5, v5, -0x1

    .line 1052
    if-ltz v5, :cond_18f

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/er;

    goto :goto_15d

    :cond_18f
    const/4 v2, 0x0

    goto :goto_15d

    .line 1054
    :cond_191
    add-int/lit8 v2, v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v2}, Landroid/support/v4/view/ViewPager;->b(II)Landroid/support/v4/view/er;

    move-result-object v2

    .line 1055
    iget v2, v2, Landroid/support/v4/view/er;->d:F

    add-float/2addr v7, v2

    .line 1056
    add-int/lit8 v8, v8, 0x1

    .line 1057
    if-ltz v5, :cond_1ab

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/er;

    goto :goto_15d

    :cond_1ab
    const/4 v2, 0x0

    goto :goto_15d

    .line 1061
    :cond_1ad
    iget v6, v10, Landroid/support/v4/view/er;->d:F

    .line 1062
    add-int/lit8 v9, v8, 0x1

    .line 1063
    const/high16 v2, 0x40000000    # 2.0f

    cmpg-float v2, v6, v2

    if-gez v2, :cond_27d

    .line 1064
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v9, v2, :cond_21f

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/er;

    move-object v7, v2

    .line 1065
    :goto_1cc
    if-gtz v14, :cond_221

    const/4 v2, 0x0

    move v5, v2

    .line 1067
    :goto_1d0
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/view/ViewPager;->u:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v16, v7

    move v7, v9

    move v9, v2

    move-object/from16 v2, v16

    :goto_1dc
    if-ge v9, v12, :cond_27d

    .line 1068
    cmpl-float v11, v6, v5

    if-ltz v11, :cond_22f

    if-le v9, v13, :cond_22f

    .line 1069
    if-eqz v2, :cond_27d

    .line 1072
    iget v11, v2, Landroid/support/v4/view/er;->b:I

    if-ne v9, v11, :cond_312

    iget-boolean v11, v2, Landroid/support/v4/view/er;->c:Z

    if-nez v11, :cond_312

    .line 1073
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1074
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    iget-object v2, v2, Landroid/support/v4/view/er;->a:Ljava/lang/Object;

    invoke-virtual {v11, v9, v2}, Landroid/support/v4/view/by;->a(ILjava/lang/Object;)V

    .line 1079
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v7, v2, :cond_22d

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/er;

    :goto_212
    move/from16 v16, v6

    move-object v6, v2

    move/from16 v2, v16

    .line 1067
    :goto_217
    add-int/lit8 v9, v9, 0x1

    move/from16 v16, v2

    move-object v2, v6

    move/from16 v6, v16

    goto :goto_1dc

    .line 1064
    :cond_21f
    const/4 v7, 0x0

    goto :goto_1cc

    .line 1065
    :cond_221
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v2

    int-to-float v2, v2

    int-to-float v5, v14

    div-float/2addr v2, v5

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v2, v5

    move v5, v2

    goto :goto_1d0

    .line 1079
    :cond_22d
    const/4 v2, 0x0

    goto :goto_212

    .line 1081
    :cond_22f
    if-eqz v2, :cond_256

    iget v11, v2, Landroid/support/v4/view/er;->b:I

    if-ne v9, v11, :cond_256

    .line 1082
    iget v2, v2, Landroid/support/v4/view/er;->d:F

    add-float/2addr v6, v2

    .line 1083
    add-int/lit8 v7, v7, 0x1

    .line 1084
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v7, v2, :cond_254

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/er;

    :goto_24e
    move/from16 v16, v6

    move-object v6, v2

    move/from16 v2, v16

    goto :goto_217

    :cond_254
    const/4 v2, 0x0

    goto :goto_24e

    .line 1086
    :cond_256
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v7}, Landroid/support/v4/view/ViewPager;->b(II)Landroid/support/v4/view/er;

    move-result-object v2

    .line 1087
    add-int/lit8 v7, v7, 0x1

    .line 1088
    iget v2, v2, Landroid/support/v4/view/er;->d:F

    add-float/2addr v6, v2

    .line 1089
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v7, v2, :cond_27b

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/er;

    :goto_275
    move/from16 v16, v6

    move-object v6, v2

    move/from16 v2, v16

    goto :goto_217

    :cond_27b
    const/4 v2, 0x0

    goto :goto_275

    .line 1094
    :cond_27d
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v8, v4}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/er;ILandroid/support/v4/view/er;)V

    .line 1104
    :cond_282
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-eqz v10, :cond_2c9

    iget-object v2, v10, Landroid/support/v4/view/er;->a:Ljava/lang/Object;

    :goto_28a
    invoke-virtual {v4, v2}, Landroid/support/v4/view/by;->a(Ljava/lang/Object;)V

    .line 1106
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v2}, Landroid/support/v4/view/by;->c()V

    .line 1110
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v5

    .line 1111
    const/4 v2, 0x0

    move v4, v2

    :goto_29a
    if-ge v4, v5, :cond_2cb

    .line 1112
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1113
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/es;

    .line 1114
    iput v4, v2, Landroid/support/v4/view/es;->f:I

    .line 1115
    iget-boolean v7, v2, Landroid/support/v4/view/es;->a:Z

    if-nez v7, :cond_2c5

    iget v7, v2, Landroid/support/v4/view/es;->c:F

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-nez v7, :cond_2c5

    .line 1117
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)Landroid/support/v4/view/er;

    move-result-object v6

    .line 1118
    if-eqz v6, :cond_2c5

    .line 1119
    iget v7, v6, Landroid/support/v4/view/er;->d:F

    iput v7, v2, Landroid/support/v4/view/es;->c:F

    .line 1120
    iget v6, v6, Landroid/support/v4/view/er;->b:I

    iput v6, v2, Landroid/support/v4/view/es;->e:I

    .line 1111
    :cond_2c5
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_29a

    .line 1104
    :cond_2c9
    const/4 v2, 0x0

    goto :goto_28a

    .line 1124
    :cond_2cb
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->g()V

    .line 1126
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 1127
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 1128
    if-eqz v2, :cond_310

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Landroid/support/v4/view/ViewPager;->b(Landroid/view/View;)Landroid/support/v4/view/er;

    move-result-object v2

    .line 1129
    :goto_2e0
    if-eqz v2, :cond_2ea

    iget v2, v2, Landroid/support/v4/view/er;->b:I

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v4/view/ViewPager;->u:I

    if-eq v2, v4, :cond_2f

    .line 1130
    :cond_2ea
    const/4 v2, 0x0

    :goto_2eb
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_2f

    .line 1131
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1132
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)Landroid/support/v4/view/er;

    move-result-object v5

    .line 1133
    if-eqz v5, :cond_30d

    iget v5, v5, Landroid/support/v4/view/er;->b:I

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v4/view/ViewPager;->u:I

    if-ne v5, v6, :cond_30d

    .line 1134
    invoke-virtual {v4, v3}, Landroid/view/View;->requestFocus(I)Z

    move-result v4

    if-nez v4, :cond_2f

    .line 1130
    :cond_30d
    add-int/lit8 v2, v2, 0x1

    goto :goto_2eb

    .line 1128
    :cond_310
    const/4 v2, 0x0

    goto :goto_2e0

    :cond_312
    move/from16 v16, v6

    move-object v6, v2

    move/from16 v2, v16

    goto/16 :goto_217

    :cond_319
    move-object v10, v2

    goto/16 :goto_103

    :cond_31c
    move-object v2, v6

    goto/16 :goto_f4

    :cond_31f
    move-object v4, v3

    move v3, v2

    goto/16 :goto_26
.end method

.method private a(IF)V
    .registers 14
    .annotation build Landroid/support/a/h;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1711
    iget v0, p0, Landroid/support/v4/view/ViewPager;->an:I

    if-lez v0, :cond_71

    .line 1712
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v5

    .line 1713
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v1

    .line 1714
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v2

    .line 1715
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v6

    .line 1716
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v7

    move v4, v3

    .line 1717
    :goto_1a
    if-ge v4, v7, :cond_71

    .line 1718
    invoke-virtual {p0, v4}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1719
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/es;

    .line 1720
    iget-boolean v9, v0, Landroid/support/v4/view/es;->a:Z

    if-eqz v9, :cond_cb

    .line 1722
    iget v0, v0, Landroid/support/v4/view/es;->b:I

    and-int/lit8 v0, v0, 0x7

    .line 1724
    packed-switch v0, :pswitch_data_d0

    :pswitch_31
    move v0, v1

    move v10, v2

    move v2, v1

    move v1, v10

    .line 1741
    :goto_35
    add-int/2addr v0, v5

    .line 1743
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v9

    sub-int/2addr v0, v9

    .line 1744
    if-eqz v0, :cond_40

    .line 1745
    invoke-virtual {v8, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1717
    :cond_40
    :goto_40
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v10, v1

    move v1, v2

    move v2, v10

    goto :goto_1a

    .line 1730
    :pswitch_47
    invoke-virtual {v8}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/2addr v0, v1

    move v10, v1

    move v1, v2

    move v2, v0

    move v0, v10

    .line 1731
    goto :goto_35

    .line 1733
    :pswitch_51
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v6, v0

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v10, v2

    move v2, v1

    move v1, v10

    .line 1735
    goto :goto_35

    .line 1737
    :pswitch_61
    sub-int v0, v6, v2

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    sub-int/2addr v0, v9

    .line 1738
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v2, v9

    move v10, v2

    move v2, v1

    move v1, v10

    goto :goto_35

    .line 5770
    :cond_71
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ao:Landroid/support/v4/view/ev;

    if-eqz v0, :cond_7a

    .line 5771
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ao:Landroid/support/v4/view/ev;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/view/ev;->a(IF)V

    .line 5773
    :cond_7a
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    if-eqz v0, :cond_98

    .line 5774
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    move v1, v3

    :goto_85
    if-ge v1, v2, :cond_98

    .line 5775
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ev;

    .line 5776
    if-eqz v0, :cond_94

    .line 5777
    invoke-interface {v0, p1, p2}, Landroid/support/v4/view/ev;->a(IF)V

    .line 5774
    :cond_94
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_85

    .line 5781
    :cond_98
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ap:Landroid/support/v4/view/ev;

    if-eqz v0, :cond_a1

    .line 5782
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ap:Landroid/support/v4/view/ev;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/view/ev;->a(IF)V

    .line 1752
    :cond_a1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ar:Landroid/support/v4/view/ew;

    if-eqz v0, :cond_c7

    .line 1753
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    .line 1754
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    move v1, v3

    .line 1755
    :goto_ad
    if-ge v1, v2, :cond_c7

    .line 1756
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1757
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/es;

    .line 1759
    iget-boolean v0, v0, Landroid/support/v4/view/es;->a:Z

    if-nez v0, :cond_c3

    .line 1761
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    .line 1755
    :cond_c3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_ad

    .line 1766
    :cond_c7
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->am:Z

    .line 1767
    return-void

    :cond_cb
    move v10, v2

    move v2, v1

    move v1, v10

    goto/16 :goto_40

    .line 1724
    :pswitch_data_d0
    .packed-switch 0x1
        :pswitch_51
        :pswitch_31
        :pswitch_47
        :pswitch_31
        :pswitch_61
    .end packed-switch
.end method

.method private a(II)V
    .registers 4

    .prologue
    .line 816
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/view/ViewPager;->a(III)V

    .line 817
    return-void
.end method

.method private a(III)V
    .registers 16

    .prologue
    .line 827
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v0

    if-nez v0, :cond_b

    .line 829
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 865
    :goto_a
    return-void

    .line 832
    :cond_b
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v1

    .line 833
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v2

    .line 834
    sub-int v3, p1, v1

    .line 835
    sub-int v4, p2, v2

    .line 836
    if-nez v3, :cond_27

    if-nez v4, :cond_27

    .line 837
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->a(Z)V

    .line 838
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 839
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    goto :goto_a

    .line 843
    :cond_27
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 844
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    .line 846
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v0

    .line 847
    div-int/lit8 v5, v0, 0x2

    .line 848
    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    int-to-float v8, v0

    div-float/2addr v7, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 849
    int-to-float v7, v5

    int-to-float v5, v5

    .line 4804
    const/high16 v8, 0x3f000000    # 0.5f

    sub-float/2addr v6, v8

    .line 4805
    float-to-double v8, v6

    const-wide v10, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v8, v10

    double-to-float v6, v8

    .line 4806
    float-to-double v8, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v6, v8

    .line 849
    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    .line 853
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 854
    if-lez v6, :cond_7e

    .line 855
    const/high16 v0, 0x447a0000    # 1000.0f

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    mul-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 861
    :goto_6f
    const/16 v5, 0x258

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 863
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 864
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    goto :goto_a

    .line 857
    :cond_7e
    int-to-float v0, v0

    const/high16 v5, 0x3f800000    # 1.0f

    mul-float/2addr v0, v5

    .line 858
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Landroid/support/v4/view/ViewPager;->A:I

    int-to-float v6, v6

    add-float/2addr v0, v6

    div-float v0, v5, v0

    .line 859
    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v0, v5

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v0, v5

    float-to-int v0, v0

    goto :goto_6f
.end method

.method private a(IIII)V
    .registers 11

    .prologue
    const/4 v2, 0x0

    .line 1506
    if-lez p2, :cond_5d

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5d

    .line 1507
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/2addr v0, p3

    .line 1508
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v1

    sub-int v1, p2, v1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v3

    sub-int/2addr v1, v3

    add-int/2addr v1, p4

    .line 1510
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v3

    .line 1511
    int-to-float v3, v3

    int-to-float v1, v1

    div-float v1, v3, v1

    .line 1512
    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 1514
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 1515
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_5c

    .line 1517
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getDuration()I

    move-result v0

    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->timePassed()I

    move-result v3

    sub-int v5, v0, v3

    .line 1518
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->b(I)Landroid/support/v4/view/er;

    move-result-object v3

    .line 1519
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    iget v3, v3, Landroid/support/v4/view/er;->e:F

    int-to-float v4, p1

    mul-float/2addr v3, v4

    float-to-int v3, v3

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1532
    :cond_5c
    :goto_5c
    return-void

    .line 1523
    :cond_5d
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->b(I)Landroid/support/v4/view/er;

    move-result-object v0

    .line 1524
    if-eqz v0, :cond_8c

    iget v0, v0, Landroid/support/v4/view/er;->e:F

    iget v1, p0, Landroid/support/v4/view/ViewPager;->F:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1525
    :goto_6d
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v3

    sub-int/2addr v1, v3

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1527
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v1

    if-eq v0, v1, :cond_5c

    .line 1528
    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->a(Z)V

    .line 1529
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    goto :goto_5c

    .line 1524
    :cond_8c
    const/4 v0, 0x0

    goto :goto_6d
.end method

.method private a(IZIZ)V
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 559
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->b(I)Landroid/support/v4/view/er;

    move-result-object v0

    .line 561
    if-eqz v0, :cond_36

    .line 562
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v2

    .line 563
    int-to-float v2, v2

    iget v3, p0, Landroid/support/v4/view/ViewPager;->E:F

    iget v0, v0, Landroid/support/v4/view/er;->e:F

    iget v4, p0, Landroid/support/v4/view/ViewPager;->F:F

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 566
    :goto_1c
    if-eqz p2, :cond_27

    .line 567
    invoke-direct {p0, v0, v1, p3}, Landroid/support/v4/view/ViewPager;->a(III)V

    .line 568
    if-eqz p4, :cond_26

    .line 569
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->d(I)V

    .line 579
    :cond_26
    :goto_26
    return-void

    .line 572
    :cond_27
    if-eqz p4, :cond_2c

    .line 573
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->d(I)V

    .line 575
    :cond_2c
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->a(Z)V

    .line 576
    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 577
    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->c(I)Z

    goto :goto_26

    :cond_36
    move v0, v1

    goto :goto_1c
.end method

.method private a(IZZ)V
    .registers 5

    .prologue
    .line 514
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v4/view/ViewPager;->a(IZZI)V

    .line 515
    return-void
.end method

.method private a(IZZI)V
    .registers 9

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 518
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v0}, Landroid/support/v4/view/by;->e()I

    move-result v0

    if-gtz v0, :cond_12

    .line 519
    :cond_e
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 555
    :goto_11
    return-void

    .line 522
    :cond_12
    if-nez p3, :cond_24

    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    if-ne v0, p1, :cond_24

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_24

    .line 523
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    goto :goto_11

    .line 527
    :cond_24
    if-gez p1, :cond_4b

    move p1, v1

    .line 532
    :cond_27
    :goto_27
    iget v0, p0, Landroid/support/v4/view/ViewPager;->L:I

    .line 533
    iget v2, p0, Landroid/support/v4/view/ViewPager;->u:I

    add-int/2addr v2, v0

    if-gt p1, v2, :cond_34

    iget v2, p0, Landroid/support/v4/view/ViewPager;->u:I

    sub-int v0, v2, v0

    if-ge p1, v0, :cond_5c

    :cond_34
    move v2, v1

    .line 537
    :goto_35
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_5c

    .line 538
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    iput-boolean v3, v0, Landroid/support/v4/view/er;->c:Z

    .line 537
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_35

    .line 529
    :cond_4b
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v0}, Landroid/support/v4/view/by;->e()I

    move-result v0

    if-lt p1, v0, :cond_27

    .line 530
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v0}, Landroid/support/v4/view/by;->e()I

    move-result v0

    add-int/lit8 p1, v0, -0x1

    goto :goto_27

    .line 541
    :cond_5c
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    if-eq v0, p1, :cond_61

    move v1, v3

    .line 543
    :cond_61
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ak:Z

    if-eqz v0, :cond_70

    .line 546
    iput p1, p0, Landroid/support/v4/view/ViewPager;->u:I

    .line 547
    if-eqz v1, :cond_6c

    .line 548
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->d(I)V

    .line 550
    :cond_6c
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->requestLayout()V

    goto :goto_11

    .line 552
    :cond_70
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->a(I)V

    .line 553
    invoke-direct {p0, p1, p2, p4, v1}, Landroid/support/v4/view/ViewPager;->a(IZIZ)V

    goto :goto_11
.end method

.method static synthetic a(Landroid/support/v4/view/ViewPager;)V
    .registers 2

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    return-void
.end method

.method private a(Landroid/support/v4/view/er;ILandroid/support/v4/view/er;)V
    .registers 16

    .prologue
    const/4 v4, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 1160
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v0}, Landroid/support/v4/view/by;->e()I

    move-result v7

    .line 1161
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v0

    .line 1162
    if-lez v0, :cond_56

    iget v1, p0, Landroid/support/v4/view/ViewPager;->A:I

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    move v6, v0

    .line 1164
    :goto_16
    if-eqz p3, :cond_ba

    .line 1165
    iget v0, p3, Landroid/support/v4/view/er;->b:I

    .line 1167
    iget v1, p1, Landroid/support/v4/view/er;->b:I

    if-ge v0, v1, :cond_70

    .line 1170
    iget v1, p3, Landroid/support/v4/view/er;->e:F

    iget v2, p3, Landroid/support/v4/view/er;->d:F

    add-float/2addr v1, v2

    add-float/2addr v1, v6

    .line 1171
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v3, v4

    move v1, v0

    .line 1172
    :goto_29
    iget v0, p1, Landroid/support/v4/view/er;->b:I

    if-gt v1, v0, :cond_ba

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_ba

    .line 1173
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    .line 1174
    :goto_3d
    iget v5, v0, Landroid/support/v4/view/er;->b:I

    if-le v1, v5, :cond_14f

    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v3, v5, :cond_14f

    .line 1175
    add-int/lit8 v3, v3, 0x1

    .line 1176
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    goto :goto_3d

    .line 1162
    :cond_56
    const/4 v0, 0x0

    move v6, v0

    goto :goto_16

    .line 1178
    :goto_59
    iget v5, v0, Landroid/support/v4/view/er;->b:I

    if-ge v2, v5, :cond_65

    .line 1181
    add-float v5, v10, v6

    add-float/2addr v5, v1

    .line 1182
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v5

    goto :goto_59

    .line 1184
    :cond_65
    iput v1, v0, Landroid/support/v4/view/er;->e:F

    .line 1185
    iget v0, v0, Landroid/support/v4/view/er;->d:F

    add-float/2addr v0, v6

    add-float/2addr v1, v0

    .line 1172
    add-int/lit8 v0, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_29

    .line 1187
    :cond_70
    iget v1, p1, Landroid/support/v4/view/er;->b:I

    if-le v0, v1, :cond_ba

    .line 1188
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    .line 1190
    iget v1, p3, Landroid/support/v4/view/er;->e:F

    .line 1191
    add-int/lit8 v0, v0, -0x1

    move v3, v2

    move v2, v1

    move v1, v0

    .line 1192
    :goto_83
    iget v0, p1, Landroid/support/v4/view/er;->b:I

    if-lt v1, v0, :cond_ba

    if-ltz v3, :cond_ba

    .line 1193
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    .line 1194
    :goto_91
    iget v5, v0, Landroid/support/v4/view/er;->b:I

    if-ge v1, v5, :cond_14a

    if-lez v3, :cond_14a

    .line 1195
    add-int/lit8 v3, v3, -0x1

    .line 1196
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    goto :goto_91

    .line 1198
    :goto_a2
    iget v5, v0, Landroid/support/v4/view/er;->b:I

    if-le v2, v5, :cond_af

    .line 1201
    add-float v5, v10, v6

    sub-float v5, v1, v5

    .line 1202
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v5

    goto :goto_a2

    .line 1204
    :cond_af
    iget v5, v0, Landroid/support/v4/view/er;->d:F

    add-float/2addr v5, v6

    sub-float/2addr v1, v5

    .line 1205
    iput v1, v0, Landroid/support/v4/view/er;->e:F

    .line 1192
    add-int/lit8 v0, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_83

    .line 1211
    :cond_ba
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1212
    iget v1, p1, Landroid/support/v4/view/er;->e:F

    .line 1213
    iget v0, p1, Landroid/support/v4/view/er;->b:I

    add-int/lit8 v2, v0, -0x1

    .line 1214
    iget v0, p1, Landroid/support/v4/view/er;->b:I

    if-nez v0, :cond_f3

    iget v0, p1, Landroid/support/v4/view/er;->e:F

    :goto_cc
    iput v0, p0, Landroid/support/v4/view/ViewPager;->E:F

    .line 1215
    iget v0, p1, Landroid/support/v4/view/er;->b:I

    add-int/lit8 v3, v7, -0x1

    if-ne v0, v3, :cond_f7

    iget v0, p1, Landroid/support/v4/view/er;->e:F

    iget v3, p1, Landroid/support/v4/view/er;->d:F

    add-float/2addr v0, v3

    sub-float/2addr v0, v10

    :goto_da
    iput v0, p0, Landroid/support/v4/view/ViewPager;->F:F

    .line 1218
    add-int/lit8 v0, p2, -0x1

    move v3, v0

    :goto_df
    if-ltz v3, :cond_10d

    .line 1219
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    .line 1220
    :goto_e9
    iget v8, v0, Landroid/support/v4/view/er;->b:I

    if-le v2, v8, :cond_fb

    .line 1221
    add-int/lit8 v2, v2, -0x1

    add-float v8, v10, v6

    sub-float/2addr v1, v8

    goto :goto_e9

    .line 1214
    :cond_f3
    const v0, -0x800001

    goto :goto_cc

    .line 1215
    :cond_f7
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_da

    .line 1223
    :cond_fb
    iget v8, v0, Landroid/support/v4/view/er;->d:F

    add-float/2addr v8, v6

    sub-float/2addr v1, v8

    .line 1224
    iput v1, v0, Landroid/support/v4/view/er;->e:F

    .line 1225
    iget v0, v0, Landroid/support/v4/view/er;->b:I

    if-nez v0, :cond_107

    iput v1, p0, Landroid/support/v4/view/ViewPager;->E:F

    .line 1218
    :cond_107
    add-int/lit8 v0, v3, -0x1

    add-int/lit8 v2, v2, -0x1

    move v3, v0

    goto :goto_df

    .line 1227
    :cond_10d
    iget v0, p1, Landroid/support/v4/view/er;->e:F

    iget v1, p1, Landroid/support/v4/view/er;->d:F

    add-float/2addr v0, v1

    add-float v1, v0, v6

    .line 1228
    iget v0, p1, Landroid/support/v4/view/er;->b:I

    add-int/lit8 v2, v0, 0x1

    .line 1230
    add-int/lit8 v0, p2, 0x1

    move v3, v0

    :goto_11b
    if-ge v3, v5, :cond_147

    .line 1231
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    .line 1232
    :goto_125
    iget v8, v0, Landroid/support/v4/view/er;->b:I

    if-ge v2, v8, :cond_12f

    .line 1233
    add-int/lit8 v2, v2, 0x1

    add-float v8, v10, v6

    add-float/2addr v1, v8

    goto :goto_125

    .line 1235
    :cond_12f
    iget v8, v0, Landroid/support/v4/view/er;->b:I

    add-int/lit8 v9, v7, -0x1

    if-ne v8, v9, :cond_13b

    .line 1236
    iget v8, v0, Landroid/support/v4/view/er;->d:F

    add-float/2addr v8, v1

    sub-float/2addr v8, v10

    iput v8, p0, Landroid/support/v4/view/ViewPager;->F:F

    .line 1238
    :cond_13b
    iput v1, v0, Landroid/support/v4/view/er;->e:F

    .line 1239
    iget v0, v0, Landroid/support/v4/view/er;->d:F

    add-float/2addr v0, v6

    add-float/2addr v1, v0

    .line 1230
    add-int/lit8 v0, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    move v3, v0

    goto :goto_11b

    .line 1242
    :cond_147
    iput-boolean v4, p0, Landroid/support/v4/view/ViewPager;->al:Z

    .line 1243
    return-void

    :cond_14a
    move v11, v1

    move v1, v2

    move v2, v11

    goto/16 :goto_a2

    :cond_14f
    move v11, v1

    move v1, v2

    move v2, v11

    goto/16 :goto_59
.end method

.method private a(Landroid/view/MotionEvent;)V
    .registers 5

    .prologue
    .line 2472
    invoke-static {p1}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 2473
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 2474
    iget v2, p0, Landroid/support/v4/view/ViewPager;->V:I

    if-ne v1, v2, :cond_24

    .line 2477
    if-nez v0, :cond_25

    const/4 v0, 0x1

    .line 2478
    :goto_f
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    iput v1, p0, Landroid/support/v4/view/ViewPager;->R:F

    .line 2479
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->V:I

    .line 2480
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_24

    .line 2481
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 2484
    :cond_24
    return-void

    .line 2477
    :cond_25
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private a(Z)V
    .registers 9

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1821
    iget v0, p0, Landroid/support/v4/view/ViewPager;->aA:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_51

    move v0, v4

    .line 1822
    :goto_8
    if-eqz v0, :cond_32

    .line 1824
    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 1825
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1826
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v1

    .line 1827
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v3

    .line 1828
    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getCurrX()I

    move-result v5

    .line 1829
    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrY()I

    move-result v6

    .line 1830
    if-ne v1, v5, :cond_2a

    if-eq v3, v6, :cond_32

    .line 1831
    :cond_2a
    invoke-virtual {p0, v5, v6}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 1832
    if-eq v5, v1, :cond_32

    .line 1833
    invoke-direct {p0, v5}, Landroid/support/v4/view/ViewPager;->c(I)Z

    .line 1837
    :cond_32
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->K:Z

    move v1, v2

    move v3, v0

    .line 1838
    :goto_36
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_53

    .line 1839
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    .line 1840
    iget-boolean v5, v0, Landroid/support/v4/view/er;->c:Z

    if-eqz v5, :cond_4d

    .line 1842
    iput-boolean v2, v0, Landroid/support/v4/view/er;->c:Z

    move v3, v4

    .line 1838
    :cond_4d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_36

    :cond_51
    move v0, v2

    .line 1821
    goto :goto_8

    .line 1845
    :cond_53
    if-eqz v3, :cond_5c

    .line 1846
    if-eqz p1, :cond_5d

    .line 1847
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->az:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1852
    :cond_5c
    :goto_5c
    return-void

    .line 1849
    :cond_5d
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->az:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_5c
.end method

.method private a(ZLandroid/support/v4/view/ew;)V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 646
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_24

    .line 647
    if-eqz p2, :cond_25

    move v0, v1

    .line 648
    :goto_b
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->ar:Landroid/support/v4/view/ew;

    if-eqz v3, :cond_27

    move v3, v1

    :goto_10
    if-eq v0, v3, :cond_29

    move v3, v1

    .line 649
    :goto_13
    iput-object p2, p0, Landroid/support/v4/view/ViewPager;->ar:Landroid/support/v4/view/ew;

    .line 650
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setChildrenDrawingOrderEnabledCompat(Z)V

    .line 651
    if-eqz v0, :cond_2b

    .line 652
    if-eqz p1, :cond_1d

    const/4 v1, 0x2

    :cond_1d
    iput v1, p0, Landroid/support/v4/view/ViewPager;->aw:I

    .line 656
    :goto_1f
    if-eqz v3, :cond_24

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 658
    :cond_24
    return-void

    :cond_25
    move v0, v2

    .line 647
    goto :goto_b

    :cond_27
    move v3, v2

    .line 648
    goto :goto_10

    :cond_29
    move v3, v2

    goto :goto_13

    .line 654
    :cond_2b
    iput v2, p0, Landroid/support/v4/view/ViewPager;->aw:I

    goto :goto_1f
.end method

.method private a(FF)Z
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 1855
    iget v0, p0, Landroid/support/v4/view/ViewPager;->P:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_c

    cmpl-float v0, p2, v2

    if-gtz v0, :cond_1c

    :cond_c
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->P:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1e

    cmpg-float v0, p2, v2

    if-gez v0, :cond_1e

    :cond_1c
    const/4 v0, 0x1

    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method private a(Landroid/view/KeyEvent;)Z
    .registers 5

    .prologue
    .line 2576
    const/4 v0, 0x0

    .line 2577
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_e

    .line 2578
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_3c

    .line 2598
    :cond_e
    :goto_e
    return v0

    .line 2580
    :sswitch_f
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->f(I)Z

    move-result v0

    goto :goto_e

    .line 2583
    :sswitch_16
    const/16 v0, 0x42

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->f(I)Z

    move-result v0

    goto :goto_e

    .line 2586
    :sswitch_1d
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_e

    .line 2589
    invoke-static {p1}, Landroid/support/v4/view/ab;->b(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 2590
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->f(I)Z

    move-result v0

    goto :goto_e

    .line 2591
    :cond_2f
    invoke-static {p1}, Landroid/support/v4/view/ab;->a(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 2592
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->f(I)Z

    move-result v0

    goto :goto_e

    .line 2578
    nop

    :sswitch_data_3c
    .sparse-switch
        0x15 -> :sswitch_f
        0x16 -> :sswitch_16
        0x3d -> :sswitch_1d
    .end sparse-switch
.end method

.method private a(Landroid/view/View;ZIII)Z
    .registers 16

    .prologue
    const/4 v2, 0x1

    .line 2539
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_5a

    move-object v6, p1

    .line 2540
    check-cast v6, Landroid/view/ViewGroup;

    .line 2541
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v8

    .line 2542
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v9

    .line 2543
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 2545
    add-int/lit8 v0, v0, -0x1

    move v7, v0

    :goto_17
    if-ltz v7, :cond_5a

    .line 2548
    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2549
    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    if-lt v0, v3, :cond_56

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    if-ge v0, v3, :cond_56

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    if-lt v0, v3, :cond_56

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ge v0, v3, :cond_56

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v4, v0, v3

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v5, v0, v3

    move-object v0, p0

    move v3, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 2558
    :cond_55
    :goto_55
    return v2

    .line 2545
    :cond_56
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    goto :goto_17

    .line 2558
    :cond_5a
    if-eqz p2, :cond_63

    neg-int v0, p3

    invoke-static {p1, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_55

    :cond_63
    const/4 v2, 0x0

    goto :goto_55
.end method

.method static synthetic b(Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/by;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    return-object v0
.end method

.method private b(I)Landroid/support/v4/view/er;
    .registers 5

    .prologue
    .line 1385
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1b

    .line 1386
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    .line 1387
    iget v2, v0, Landroid/support/v4/view/er;->b:I

    if-ne v2, p1, :cond_17

    .line 1391
    :goto_16
    return-object v0

    .line 1385
    :cond_17
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1391
    :cond_1b
    const/4 v0, 0x0

    goto :goto_16
.end method

.method private b(II)Landroid/support/v4/view/er;
    .registers 5

    .prologue
    .line 868
    new-instance v0, Landroid/support/v4/view/er;

    invoke-direct {v0}, Landroid/support/v4/view/er;-><init>()V

    .line 869
    iput p1, v0, Landroid/support/v4/view/er;->b:I

    .line 870
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v1, p0, p1}, Landroid/support/v4/view/by;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/view/er;->a:Ljava/lang/Object;

    .line 871
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/support/v4/view/er;->d:F

    .line 872
    if-ltz p2, :cond_1d

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt p2, v1, :cond_23

    .line 873
    :cond_1d
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 877
    :goto_22
    return-object v0

    .line 875
    :cond_23
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_22
.end method

.method private b(Landroid/view/View;)Landroid/support/v4/view/er;
    .registers 4

    .prologue
    .line 1375
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_12

    .line 1376
    if-eqz v0, :cond_c

    instance-of v1, v0, Landroid/view/View;

    if-nez v1, :cond_e

    .line 1377
    :cond_c
    const/4 v0, 0x0

    .line 1381
    :goto_d
    return-object v0

    .line 1379
    :cond_e
    check-cast v0, Landroid/view/View;

    move-object p1, v0

    goto :goto_0

    .line 1381
    :cond_12
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)Landroid/support/v4/view/er;

    move-result-object v0

    goto :goto_d
.end method

.method private b(IF)V
    .registers 6

    .prologue
    .line 1770
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ao:Landroid/support/v4/view/ev;

    if-eqz v0, :cond_9

    .line 1771
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ao:Landroid/support/v4/view/ev;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/view/ev;->a(IF)V

    .line 1773
    :cond_9
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    if-eqz v0, :cond_28

    .line 1774
    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_15
    if-ge v1, v2, :cond_28

    .line 1775
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ev;

    .line 1776
    if-eqz v0, :cond_24

    .line 1777
    invoke-interface {v0, p1, p2}, Landroid/support/v4/view/ev;->a(IF)V

    .line 1774
    :cond_24
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_15

    .line 1781
    :cond_28
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ap:Landroid/support/v4/view/ev;

    if-eqz v0, :cond_31

    .line 1782
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ap:Landroid/support/v4/view/ev;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/view/ev;->a(IF)V

    .line 1784
    :cond_31
    return-void
.end method

.method private b(Landroid/support/v4/view/ev;)V
    .registers 3

    .prologue
    .line 606
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    if-nez v0, :cond_b

    .line 607
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    .line 609
    :cond_b
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 610
    return-void
.end method

.method private b(Z)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 1859
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v3

    move v2, v1

    .line 1860
    :goto_6
    if-ge v2, v3, :cond_19

    .line 1861
    if-eqz p1, :cond_17

    const/4 v0, 0x2

    .line 1863
    :goto_b
    invoke-virtual {p0, v2}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Landroid/support/v4/view/cx;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 1860
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_17
    move v0, v1

    .line 1861
    goto :goto_b

    .line 1865
    :cond_19
    return-void
.end method

.method private b(F)Z
    .registers 12

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2145
    .line 2147
    iget v0, p0, Landroid/support/v4/view/ViewPager;->R:F

    sub-float/2addr v0, p1

    .line 2148
    iput p1, p0, Landroid/support/v4/view/ViewPager;->R:F

    .line 2150
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    .line 2151
    add-float v5, v1, v0

    .line 2152
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v7

    .line 2154
    int-to-float v0, v7

    iget v1, p0, Landroid/support/v4/view/ViewPager;->E:F

    mul-float v4, v0, v1

    .line 2155
    int-to-float v0, v7

    iget v1, p0, Landroid/support/v4/view/ViewPager;->F:F

    mul-float v6, v0, v1

    .line 2159
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    .line 2160
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    iget-object v8, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/er;

    .line 2161
    iget v8, v0, Landroid/support/v4/view/er;->b:I

    if-eqz v8, :cond_93

    .line 2163
    iget v0, v0, Landroid/support/v4/view/er;->e:F

    int-to-float v4, v7

    mul-float/2addr v0, v4

    move v4, v0

    move v0, v2

    .line 2165
    :goto_3e
    iget v8, v1, Landroid/support/v4/view/er;->b:I

    iget-object v9, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v9}, Landroid/support/v4/view/by;->e()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-eq v8, v9, :cond_91

    .line 2167
    iget v1, v1, Landroid/support/v4/view/er;->e:F

    int-to-float v3, v7

    mul-float/2addr v1, v3

    move v3, v2

    .line 2170
    :goto_4f
    cmpg-float v6, v5, v4

    if-gez v6, :cond_79

    .line 2171
    if-eqz v0, :cond_63

    .line 2172
    sub-float v0, v4, v5

    .line 2173
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->ai:Landroid/support/v4/widget/al;

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v2, v7

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/al;->a(F)Z

    move-result v2

    .line 2184
    :cond_63
    :goto_63
    iget v0, p0, Landroid/support/v4/view/ViewPager;->R:F

    float-to-int v1, v4

    int-to-float v1, v1

    sub-float v1, v4, v1

    add-float/2addr v0, v1

    iput v0, p0, Landroid/support/v4/view/ViewPager;->R:F

    .line 2185
    float-to-int v0, v4

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 2186
    float-to-int v0, v4

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->c(I)Z

    .line 2188
    return v2

    .line 2176
    :cond_79
    cmpl-float v0, v5, v1

    if-lez v0, :cond_8f

    .line 2177
    if-eqz v3, :cond_8d

    .line 2178
    sub-float v0, v5, v1

    .line 2179
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->aj:Landroid/support/v4/widget/al;

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v3, v7

    div-float/2addr v0, v3

    invoke-virtual {v2, v0}, Landroid/support/v4/widget/al;->a(F)Z

    move-result v2

    :cond_8d
    move v4, v1

    .line 2181
    goto :goto_63

    :cond_8f
    move v4, v5

    goto :goto_63

    :cond_91
    move v1, v6

    goto :goto_4f

    :cond_93
    move v0, v3

    goto :goto_3e
.end method

.method static synthetic c(Landroid/support/v4/view/ViewPager;)I
    .registers 2

    .prologue
    .line 91
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    return v0
.end method

.method private c(F)V
    .registers 10

    .prologue
    const/4 v7, 0x0

    .line 2418
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ag:Z

    if-nez v0, :cond_d

    .line 2419
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No fake drag in progress. Call beginFakeDrag first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2422
    :cond_d
    iget v0, p0, Landroid/support/v4/view/ViewPager;->R:F

    add-float/2addr v0, p1

    iput v0, p0, Landroid/support/v4/view/ViewPager;->R:F

    .line 2424
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    .line 2425
    sub-float v3, v0, p1

    .line 2426
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v5

    .line 2428
    int-to-float v0, v5

    iget v1, p0, Landroid/support/v4/view/ViewPager;->E:F

    mul-float v2, v0, v1

    .line 2429
    int-to-float v0, v5

    iget v1, p0, Landroid/support/v4/view/ViewPager;->F:F

    mul-float v4, v0, v1

    .line 2431
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    .line 2432
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/er;

    .line 2433
    iget v6, v0, Landroid/support/v4/view/er;->b:I

    if-eqz v6, :cond_91

    .line 2434
    iget v0, v0, Landroid/support/v4/view/er;->e:F

    int-to-float v2, v5

    mul-float/2addr v0, v2

    .line 2436
    :goto_47
    iget v2, v1, Landroid/support/v4/view/er;->b:I

    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v6}, Landroid/support/v4/view/by;->e()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-eq v2, v6, :cond_8f

    .line 2437
    iget v1, v1, Landroid/support/v4/view/er;->e:F

    int-to-float v2, v5

    mul-float/2addr v1, v2

    .line 2440
    :goto_57
    cmpg-float v2, v3, v0

    if-gez v2, :cond_87

    .line 2446
    :goto_5b
    iget v1, p0, Landroid/support/v4/view/ViewPager;->R:F

    float-to-int v2, v0

    int-to-float v2, v2

    sub-float v2, v0, v2

    add-float/2addr v1, v2

    iput v1, p0, Landroid/support/v4/view/ViewPager;->R:F

    .line 2447
    float-to-int v1, v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 2448
    float-to-int v0, v0

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->c(I)Z

    .line 2451
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2452
    iget-wide v0, p0, Landroid/support/v4/view/ViewPager;->ah:J

    const/4 v4, 0x2

    iget v5, p0, Landroid/support/v4/view/ViewPager;->R:F

    const/4 v6, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 2454
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    invoke-virtual {v1, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2455
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 2456
    return-void

    .line 2442
    :cond_87
    cmpl-float v0, v3, v1

    if-lez v0, :cond_8d

    move v0, v1

    .line 2443
    goto :goto_5b

    :cond_8d
    move v0, v3

    goto :goto_5b

    :cond_8f
    move v1, v4

    goto :goto_57

    :cond_91
    move v0, v2

    goto :goto_47
.end method

.method private c(Landroid/support/v4/view/ev;)V
    .registers 3

    .prologue
    .line 619
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    if-eqz v0, :cond_9

    .line 620
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 622
    :cond_9
    return-void
.end method

.method private c(I)Z
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 1669
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1b

    .line 1670
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->am:Z

    .line 1671
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->a(IF)V

    .line 1672
    iget-boolean v1, p0, Landroid/support/v4/view/ViewPager;->am:Z

    if-nez v1, :cond_48

    .line 1673
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onPageScrolled did not call superclass implementation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1678
    :cond_1b
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->i()Landroid/support/v4/view/er;

    move-result-object v1

    .line 1679
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v2

    .line 1681
    iget v3, p0, Landroid/support/v4/view/ViewPager;->A:I

    int-to-float v3, v3

    int-to-float v4, v2

    div-float/2addr v3, v4

    .line 1682
    iget v4, v1, Landroid/support/v4/view/er;->b:I

    .line 1683
    int-to-float v5, p1

    int-to-float v2, v2

    div-float v2, v5, v2

    iget v5, v1, Landroid/support/v4/view/er;->e:F

    sub-float/2addr v2, v5

    iget v1, v1, Landroid/support/v4/view/er;->d:F

    add-float/2addr v1, v3

    div-float v1, v2, v1

    .line 1687
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->am:Z

    .line 1688
    invoke-direct {p0, v4, v1}, Landroid/support/v4/view/ViewPager;->a(IF)V

    .line 1689
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->am:Z

    if-nez v0, :cond_47

    .line 1690
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onPageScrolled did not call superclass implementation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1693
    :cond_47
    const/4 v0, 0x1

    :cond_48
    return v0
.end method

.method static synthetic c()[I
    .registers 1

    .prologue
    .line 91
    sget-object v0, Landroid/support/v4/view/ViewPager;->m:[I

    return-object v0
.end method

.method private d()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 361
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setWillNotDraw(Z)V

    .line 362
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setDescendantFocusability(I)V

    .line 363
    invoke-virtual {p0, v4}, Landroid/support/v4/view/ViewPager;->setFocusable(Z)V

    .line 364
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 365
    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Landroid/support/v4/view/ViewPager;->p:Landroid/view/animation/Interpolator;

    invoke-direct {v1, v0, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    .line 366
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 367
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 369
    invoke-static {v1}, Landroid/support/v4/view/du;->a(Landroid/view/ViewConfiguration;)I

    move-result v3

    iput v3, p0, Landroid/support/v4/view/ViewPager;->Q:I

    .line 370
    const/high16 v3, 0x43c80000    # 400.0f

    mul-float/2addr v3, v2

    float-to-int v3, v3

    iput v3, p0, Landroid/support/v4/view/ViewPager;->ab:I

    .line 371
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Landroid/support/v4/view/ViewPager;->ac:I

    .line 372
    new-instance v1, Landroid/support/v4/widget/al;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/al;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v4/view/ViewPager;->ai:Landroid/support/v4/widget/al;

    .line 373
    new-instance v1, Landroid/support/v4/widget/al;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/al;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v4/view/ViewPager;->aj:Landroid/support/v4/widget/al;

    .line 375
    const/high16 v0, 0x41c80000    # 25.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->ad:I

    .line 376
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->ae:I

    .line 377
    const/high16 v0, 0x41800000    # 16.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->O:I

    .line 379
    new-instance v0, Landroid/support/v4/view/et;

    invoke-direct {v0, p0}, Landroid/support/v4/view/et;-><init>(Landroid/support/v4/view/ViewPager;)V

    invoke-static {p0, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    .line 381
    invoke-static {p0}, Landroid/support/v4/view/cx;->c(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_6b

    .line 383
    invoke-static {p0, v4}, Landroid/support/v4/view/cx;->c(Landroid/view/View;I)V

    .line 386
    :cond_6b
    return-void
.end method

.method private d(I)V
    .registers 5

    .prologue
    .line 1787
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ao:Landroid/support/v4/view/ev;

    if-eqz v0, :cond_9

    .line 1788
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ao:Landroid/support/v4/view/ev;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ev;->b(I)V

    .line 1790
    :cond_9
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    if-eqz v0, :cond_28

    .line 1791
    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_15
    if-ge v1, v2, :cond_28

    .line 1792
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ev;

    .line 1793
    if-eqz v0, :cond_24

    .line 1794
    invoke-interface {v0, p1}, Landroid/support/v4/view/ev;->b(I)V

    .line 1791
    :cond_24
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_15

    .line 1798
    :cond_28
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ap:Landroid/support/v4/view/ev;

    if-eqz v0, :cond_31

    .line 1799
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ap:Landroid/support/v4/view/ev;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ev;->b(I)V

    .line 1801
    :cond_31
    return-void
.end method

.method private e()V
    .registers 3

    .prologue
    .line 459
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1f

    .line 460
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 461
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/es;

    .line 462
    iget-boolean v0, v0, Landroid/support/v4/view/es;->a:Z

    if-nez v0, :cond_1b

    .line 463
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->removeViewAt(I)V

    .line 464
    add-int/lit8 v1, v1, -0x1

    .line 459
    :cond_1b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 467
    :cond_1f
    return-void
.end method

.method private e(I)V
    .registers 5

    .prologue
    .line 1804
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ao:Landroid/support/v4/view/ev;

    if-eqz v0, :cond_9

    .line 1805
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ao:Landroid/support/v4/view/ev;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ev;->a(I)V

    .line 1807
    :cond_9
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    if-eqz v0, :cond_28

    .line 1808
    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_15
    if-ge v1, v2, :cond_28

    .line 1809
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ev;

    .line 1810
    if-eqz v0, :cond_24

    .line 1811
    invoke-interface {v0, p1}, Landroid/support/v4/view/ev;->a(I)V

    .line 1808
    :cond_24
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_15

    .line 1815
    :cond_28
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ap:Landroid/support/v4/view/ev;

    if-eqz v0, :cond_31

    .line 1816
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ap:Landroid/support/v4/view/ev;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ev;->a(I)V

    .line 1818
    :cond_31
    return-void
.end method

.method private f()V
    .registers 2

    .prologue
    .line 628
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    if-eqz v0, :cond_9

    .line 629
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 631
    :cond_9
    return-void
.end method

.method private f(I)Z
    .registers 11

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x42

    const/16 v7, 0x11

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2602
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 2603
    if-ne v2, p0, :cond_3f

    move-object v0, v1

    .line 2630
    :goto_e
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    invoke-virtual {v1, p0, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 2632
    if-eqz v1, :cond_bd

    if-eq v1, v0, :cond_bd

    .line 2633
    if-ne p1, v7, :cond_a1

    .line 2636
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->s:Landroid/graphics/Rect;

    invoke-direct {p0, v2, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->left:I

    .line 2637
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->s:Landroid/graphics/Rect;

    invoke-direct {p0, v3, v0}, Landroid/support/v4/view/ViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    .line 2638
    if-eqz v0, :cond_9c

    if-lt v2, v3, :cond_9c

    .line 2639
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->n()Z

    move-result v0

    :goto_34
    move v4, v0

    .line 2661
    :cond_35
    if-eqz v4, :cond_3e

    .line 2662
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->playSoundEffect(I)V

    .line 2664
    :cond_3e
    return v4

    .line 2605
    :cond_3f
    if-eqz v2, :cond_e9

    .line 2607
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_45
    instance-of v5, v0, Landroid/view/ViewGroup;

    if-eqz v5, :cond_ec

    .line 2609
    if-ne v0, p0, :cond_7c

    move v0, v3

    .line 2614
    :goto_4c
    if-nez v0, :cond_e9

    .line 2616
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2617
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2618
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_62
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_81

    .line 2620
    const-string v2, " => "

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2619
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_62

    .line 2608
    :cond_7c
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_45

    .line 2622
    :cond_81
    const-string v0, "ViewPager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "arrowScroll tried to find focus based on non-child current focused view "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 2624
    goto/16 :goto_e

    .line 2641
    :cond_9c
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    move-result v0

    goto :goto_34

    .line 2643
    :cond_a1
    if-ne p1, v8, :cond_35

    .line 2646
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->s:Landroid/graphics/Rect;

    invoke-direct {p0, v2, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->left:I

    .line 2647
    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->s:Landroid/graphics/Rect;

    invoke-direct {p0, v5, v0}, Landroid/support/v4/view/ViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Rect;->left:I

    .line 2648
    if-eqz v0, :cond_b7

    if-le v2, v5, :cond_cc

    .line 2651
    :cond_b7
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    move-result v0

    goto/16 :goto_34

    .line 2654
    :cond_bd
    if-eq p1, v7, :cond_c1

    if-ne p1, v3, :cond_c7

    .line 2656
    :cond_c1
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->n()Z

    move-result v0

    goto/16 :goto_34

    .line 2657
    :cond_c7
    if-eq p1, v8, :cond_cc

    const/4 v0, 0x2

    if-ne p1, v0, :cond_35

    .line 6702
    :cond_cc
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-eqz v0, :cond_e6

    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v1}, Landroid/support/v4/view/by;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_e6

    .line 6703
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem$2563266(I)V

    move v0, v3

    .line 6704
    goto/16 :goto_34

    :cond_e6
    move v0, v4

    .line 6706
    goto/16 :goto_34

    :cond_e9
    move-object v0, v2

    goto/16 :goto_e

    :cond_ec
    move v0, v4

    goto/16 :goto_4c
.end method

.method private g()V
    .registers 5

    .prologue
    .line 1144
    iget v0, p0, Landroid/support/v4/view/ViewPager;->aw:I

    if-eqz v0, :cond_2f

    .line 1145
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ax:Ljava/util/ArrayList;

    if-nez v0, :cond_22

    .line 1146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->ax:Ljava/util/ArrayList;

    .line 1150
    :goto_f
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v1

    .line 1151
    const/4 v0, 0x0

    :goto_14
    if-ge v0, v1, :cond_28

    .line 1152
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1153
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->ax:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1151
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 1148
    :cond_22
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ax:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_f

    .line 1155
    :cond_28
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ax:Ljava/util/ArrayList;

    sget-object v1, Landroid/support/v4/view/ViewPager;->ay:Landroid/support/v4/view/fa;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1157
    :cond_2f
    return-void
.end method

.method private getClientWidth()I
    .registers 3

    .prologue
    .line 483
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private h()V
    .registers 3

    .prologue
    .line 2138
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2139
    if-eqz v0, :cond_a

    .line 2140
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2142
    :cond_a
    return-void
.end method

.method private i()Landroid/support/v4/view/er;
    .registers 14

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2196
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v1

    .line 2197
    if-lez v1, :cond_68

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    int-to-float v4, v1

    div-float/2addr v0, v4

    move v9, v0

    .line 2198
    :goto_10
    if-lez v1, :cond_6a

    iget v0, p0, Landroid/support/v4/view/ViewPager;->A:I

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    move v1, v0

    .line 2199
    :goto_18
    const/4 v5, -0x1

    .line 2202
    const/4 v4, 0x1

    .line 2204
    const/4 v0, 0x0

    move v6, v2

    move v7, v2

    move v8, v5

    move v2, v3

    move v5, v4

    move-object v4, v0

    .line 2205
    :goto_21
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_67

    .line 2206
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    .line 2208
    if-nez v5, :cond_79

    iget v10, v0, Landroid/support/v4/view/er;->b:I

    add-int/lit8 v11, v8, 0x1

    if-eq v10, v11, :cond_79

    .line 2210
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->r:Landroid/support/v4/view/er;

    .line 2211
    add-float/2addr v6, v7

    add-float/2addr v6, v1

    iput v6, v0, Landroid/support/v4/view/er;->e:F

    .line 2212
    add-int/lit8 v6, v8, 0x1

    iput v6, v0, Landroid/support/v4/view/er;->b:I

    .line 2213
    const/high16 v6, 0x3f800000    # 1.0f

    iput v6, v0, Landroid/support/v4/view/er;->d:F

    .line 2214
    add-int/lit8 v2, v2, -0x1

    move-object v12, v0

    move v0, v2

    move-object v2, v12

    .line 2216
    :goto_4c
    iget v6, v2, Landroid/support/v4/view/er;->e:F

    .line 2219
    iget v7, v2, Landroid/support/v4/view/er;->d:F

    add-float/2addr v7, v6

    add-float/2addr v7, v1

    .line 2220
    if-nez v5, :cond_58

    cmpl-float v5, v9, v6

    if-ltz v5, :cond_67

    .line 2221
    :cond_58
    cmpg-float v4, v9, v7

    if-ltz v4, :cond_66

    iget-object v4, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_6c

    :cond_66
    move-object v4, v2

    .line 2234
    :cond_67
    return-object v4

    :cond_68
    move v9, v2

    .line 2197
    goto :goto_10

    :cond_6a
    move v1, v2

    .line 2198
    goto :goto_18

    .line 2228
    :cond_6c
    iget v5, v2, Landroid/support/v4/view/er;->b:I

    .line 2230
    iget v4, v2, Landroid/support/v4/view/er;->d:F

    .line 2205
    add-int/lit8 v0, v0, 0x1

    move v7, v6

    move v8, v5

    move v5, v3

    move v6, v4

    move-object v4, v2

    move v2, v0

    goto :goto_21

    :cond_79
    move-object v12, v0

    move v0, v2

    move-object v2, v12

    goto :goto_4c
.end method

.method private j()Z
    .registers 10

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 2361
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->M:Z

    if-eqz v0, :cond_8

    .line 2377
    :goto_7
    return v4

    .line 2364
    :cond_8
    iput-boolean v8, p0, Landroid/support/v4/view/ViewPager;->ag:Z

    .line 2365
    invoke-direct {p0, v8}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    .line 2366
    iput v5, p0, Landroid/support/v4/view/ViewPager;->R:F

    iput v5, p0, Landroid/support/v4/view/ViewPager;->T:F

    .line 2367
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    if-nez v0, :cond_32

    .line 2368
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    .line 2372
    :goto_1b
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    move-wide v2, v0

    move v6, v5

    move v7, v4

    .line 2373
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v2

    .line 2374
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    invoke-virtual {v3, v2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2375
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 2376
    iput-wide v0, p0, Landroid/support/v4/view/ViewPager;->ah:J

    move v4, v8

    .line 2377
    goto :goto_7

    .line 2370
    :cond_32
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_1b
.end method

.method private k()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    .line 2387
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ag:Z

    if-nez v0, :cond_d

    .line 2388
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No fake drag in progress. Call beginFakeDrag first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2391
    :cond_d
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    .line 2392
    const/16 v1, 0x3e8

    iget v2, p0, Landroid/support/v4/view/ViewPager;->ac:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 2393
    iget v1, p0, Landroid/support/v4/view/ViewPager;->V:I

    invoke-static {v0, v1}, Landroid/support/v4/view/cs;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    float-to-int v0, v0

    .line 2395
    iput-boolean v5, p0, Landroid/support/v4/view/ViewPager;->K:Z

    .line 2396
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v1

    .line 2397
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v2

    .line 2398
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->i()Landroid/support/v4/view/er;

    move-result-object v3

    .line 2399
    iget v4, v3, Landroid/support/v4/view/er;->b:I

    .line 2400
    int-to-float v2, v2

    int-to-float v1, v1

    div-float v1, v2, v1

    iget v2, v3, Landroid/support/v4/view/er;->e:F

    sub-float/2addr v1, v2

    iget v2, v3, Landroid/support/v4/view/er;->d:F

    div-float/2addr v1, v2

    .line 2401
    iget v2, p0, Landroid/support/v4/view/ViewPager;->R:F

    iget v3, p0, Landroid/support/v4/view/ViewPager;->T:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 2402
    invoke-direct {p0, v4, v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IFII)I

    move-result v1

    .line 2404
    invoke-direct {p0, v1, v5, v5, v0}, Landroid/support/v4/view/ViewPager;->a(IZZI)V

    .line 2405
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->m()V

    .line 2407
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ag:Z

    .line 2408
    return-void
.end method

.method private l()Z
    .registers 2

    .prologue
    .line 2468
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ag:Z

    return v0
.end method

.method private m()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 2487
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->M:Z

    .line 2488
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->N:Z

    .line 2490
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_11

    .line 2491
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 2492
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    .line 2494
    :cond_11
    return-void
.end method

.method private n()Z
    .registers 2

    .prologue
    .line 2694
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    if-lez v0, :cond_d

    .line 2695
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem$2563266(I)V

    .line 2696
    const/4 v0, 0x1

    .line 2698
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private o()Z
    .registers 3

    .prologue
    .line 2702
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-eqz v0, :cond_19

    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v1}, Landroid/support/v4/view/by;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_19

    .line 2703
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem$2563266(I)V

    .line 2704
    const/4 v0, 0x1

    .line 2706
    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method private setCurrentItem$2563266(I)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 505
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->K:Z

    .line 506
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    .line 507
    return-void
.end method

.method private setScrollState(I)V
    .registers 9

    .prologue
    const/4 v1, 0x0

    .line 395
    iget v0, p0, Landroid/support/v4/view/ViewPager;->aA:I

    if-ne v0, p1, :cond_6

    .line 405
    :cond_5
    :goto_5
    return-void

    .line 399
    :cond_6
    iput p1, p0, Landroid/support/v4/view/ViewPager;->aA:I

    .line 400
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ar:Landroid/support/v4/view/ew;

    if-eqz v0, :cond_29

    .line 402
    if-eqz p1, :cond_25

    const/4 v0, 0x1

    .line 2859
    :goto_f
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v4

    move v3, v1

    .line 2860
    :goto_14
    if-ge v3, v4, :cond_29

    .line 2861
    if-eqz v0, :cond_27

    const/4 v2, 0x2

    .line 2863
    :goto_19
    invoke-virtual {p0, v3}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v2, v6}, Landroid/support/v4/view/cx;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 2860
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_14

    :cond_25
    move v0, v1

    .line 402
    goto :goto_f

    :cond_27
    move v2, v1

    .line 2861
    goto :goto_19

    .line 3804
    :cond_29
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ao:Landroid/support/v4/view/ev;

    if-eqz v0, :cond_32

    .line 3805
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ao:Landroid/support/v4/view/ev;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ev;->a(I)V

    .line 3807
    :cond_32
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    if-eqz v0, :cond_4e

    .line 3808
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    :goto_3c
    if-ge v1, v2, :cond_4e

    .line 3809
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ev;

    .line 3810
    if-eqz v0, :cond_4b

    .line 3811
    invoke-interface {v0, p1}, Landroid/support/v4/view/ev;->a(I)V

    .line 3808
    :cond_4b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3c

    .line 3815
    :cond_4e
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ap:Landroid/support/v4/view/ev;

    if-eqz v0, :cond_5

    .line 3816
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ap:Landroid/support/v4/view/ev;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ev;->a(I)V

    goto :goto_5
.end method

.method private setScrollingCacheEnabled(Z)V
    .registers 3

    .prologue
    .line 2497
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->J:Z

    if-eq v0, p1, :cond_6

    .line 2498
    iput-boolean p1, p0, Landroid/support/v4/view/ViewPager;->J:Z

    .line 2509
    :cond_6
    return-void
.end method


# virtual methods
.method final a(Landroid/support/v4/view/ev;)Landroid/support/v4/view/ev;
    .registers 3

    .prologue
    .line 692
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ap:Landroid/support/v4/view/ev;

    .line 693
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->ap:Landroid/support/v4/view/ev;

    .line 694
    return-object v0
.end method

.method final a()V
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 883
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v0}, Landroid/support/v4/view/by;->e()I

    move-result v0

    .line 884
    iput v0, p0, Landroid/support/v4/view/ViewPager;->n:I

    .line 885
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iget v4, p0, Landroid/support/v4/view/ViewPager;->L:I

    mul-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    if-ge v3, v4, :cond_34

    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v3, v0, :cond_34

    move v0, v1

    .line 887
    :goto_21
    iget v4, p0, Landroid/support/v4/view/ViewPager;->u:I

    move v3, v2

    .line 890
    :goto_24
    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_36

    .line 891
    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 890
    add-int/lit8 v3, v3, 0x1

    goto :goto_24

    :cond_34
    move v0, v2

    .line 885
    goto :goto_21

    .line 933
    :cond_36
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    sget-object v5, Landroid/support/v4/view/ViewPager;->o:Ljava/util/Comparator;

    invoke-static {v3, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 935
    if-eqz v0, :cond_61

    .line 937
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v5

    move v3, v2

    .line 938
    :goto_44
    if-ge v3, v5, :cond_5b

    .line 939
    invoke-virtual {p0, v3}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 940
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/es;

    .line 941
    iget-boolean v6, v0, Landroid/support/v4/view/es;->a:Z

    if-nez v6, :cond_57

    .line 942
    const/4 v6, 0x0

    iput v6, v0, Landroid/support/v4/view/es;->c:F

    .line 938
    :cond_57
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_44

    .line 946
    :cond_5b
    invoke-direct {p0, v4, v2, v1}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    .line 947
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->requestLayout()V

    .line 949
    :cond_61
    return-void
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .registers 10

    .prologue
    .line 2714
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 2716
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getDescendantFocusability()I

    move-result v2

    .line 2718
    const/high16 v0, 0x60000

    if-eq v2, v0, :cond_2f

    .line 2719
    const/4 v0, 0x0

    :goto_d
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_2f

    .line 2720
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2721
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_2c

    .line 2722
    invoke-direct {p0, v3}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)Landroid/support/v4/view/er;

    move-result-object v4

    .line 2723
    if-eqz v4, :cond_2c

    iget v4, v4, Landroid/support/v4/view/er;->b:I

    iget v5, p0, Landroid/support/v4/view/ViewPager;->u:I

    if-ne v4, v5, :cond_2c

    .line 2724
    invoke-virtual {v3, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 2719
    :cond_2c
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 2734
    :cond_2f
    const/high16 v0, 0x40000

    if-ne v2, v0, :cond_39

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v1, v0, :cond_3f

    .line 2740
    :cond_39
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->isFocusable()Z

    move-result v0

    if-nez v0, :cond_40

    .line 2751
    :cond_3f
    :goto_3f
    return-void

    .line 2743
    :cond_40
    and-int/lit8 v0, p3, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_51

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_51

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->isFocusableInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 2747
    :cond_51
    if-eqz p1, :cond_3f

    .line 2748
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3f
.end method

.method public addTouchables(Ljava/util/ArrayList;)V
    .registers 6

    .prologue
    .line 2761
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_23

    .line 2762
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2763
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_20

    .line 2764
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)Landroid/support/v4/view/er;

    move-result-object v2

    .line 2765
    if-eqz v2, :cond_20

    iget v2, v2, Landroid/support/v4/view/er;->b:I

    iget v3, p0, Landroid/support/v4/view/ViewPager;->u:I

    if-ne v2, v3, :cond_20

    .line 2766
    invoke-virtual {v1, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    .line 2761
    :cond_20
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2770
    :cond_23
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 8

    .prologue
    .line 1330
    invoke-virtual {p0, p3}, Landroid/support/v4/view/ViewPager;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-nez v0, :cond_31

    .line 1331
    invoke-virtual {p0, p3}, Landroid/support/v4/view/ViewPager;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    :goto_a
    move-object v0, v1

    .line 1333
    check-cast v0, Landroid/support/v4/view/es;

    .line 1334
    iget-boolean v2, v0, Landroid/support/v4/view/es;->a:Z

    instance-of v3, p1, Landroid/support/v4/view/eq;

    or-int/2addr v2, v3

    iput-boolean v2, v0, Landroid/support/v4/view/es;->a:Z

    .line 1335
    iget-boolean v2, p0, Landroid/support/v4/view/ViewPager;->I:Z

    if-eqz v2, :cond_2d

    .line 1336
    if-eqz v0, :cond_26

    iget-boolean v2, v0, Landroid/support/v4/view/es;->a:Z

    if-eqz v2, :cond_26

    .line 1337
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add pager decor view during layout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1339
    :cond_26
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/support/v4/view/es;->d:Z

    .line 1340
    invoke-virtual {p0, p1, p2, v1}, Landroid/support/v4/view/ViewPager;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 1352
    :goto_2c
    return-void

    .line 1342
    :cond_2d
    invoke-super {p0, p1, p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2c

    :cond_31
    move-object v1, p3

    goto :goto_a
.end method

.method final b()V
    .registers 2

    .prologue
    .line 952
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->a(I)V

    .line 953
    return-void
.end method

.method public canScrollHorizontally(I)Z
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2512
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-nez v2, :cond_7

    .line 2523
    :cond_6
    :goto_6
    return v0

    .line 2516
    :cond_7
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v2

    .line 2517
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v3

    .line 2518
    if-gez p1, :cond_1a

    .line 2519
    int-to-float v2, v2

    iget v4, p0, Landroid/support/v4/view/ViewPager;->E:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    if-le v3, v2, :cond_6

    move v0, v1

    goto :goto_6

    .line 2520
    :cond_1a
    if-lez p1, :cond_6

    .line 2521
    int-to-float v2, v2

    iget v4, p0, Landroid/support/v4/view/ViewPager;->F:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    if-ge v3, v2, :cond_6

    move v0, v1

    goto :goto_6
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3

    .prologue
    .line 2840
    instance-of v0, p1, Landroid/support/v4/view/es;

    if-eqz v0, :cond_c

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public computeScroll()V
    .registers 5

    .prologue
    .line 1645
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_3e

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 1646
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v0

    .line 1647
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v1

    .line 1648
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    .line 1649
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrY()I

    move-result v3

    .line 1651
    if-ne v0, v2, :cond_28

    if-eq v1, v3, :cond_3a

    .line 1652
    :cond_28
    invoke-virtual {p0, v2, v3}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 1653
    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->c(I)Z

    move-result v0

    if-nez v0, :cond_3a

    .line 1654
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1655
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v3}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 1660
    :cond_3a
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 1666
    :goto_3d
    return-void

    .line 1665
    :cond_3e
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->a(Z)V

    goto :goto_3d
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2564
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_18

    .line 6577
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_15

    .line 6578
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_46

    :cond_15
    move v2, v0

    .line 2564
    :goto_16
    if-eqz v2, :cond_19

    :cond_18
    move v0, v1

    :cond_19
    return v0

    .line 6580
    :sswitch_1a
    const/16 v2, 0x11

    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->f(I)Z

    move-result v2

    goto :goto_16

    .line 6583
    :sswitch_21
    const/16 v2, 0x42

    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->f(I)Z

    move-result v2

    goto :goto_16

    .line 6586
    :sswitch_28
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_15

    .line 6589
    invoke-static {p1}, Landroid/support/v4/view/ab;->b(Landroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_3a

    .line 6590
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->f(I)Z

    move-result v2

    goto :goto_16

    .line 6591
    :cond_3a
    invoke-static {p1}, Landroid/support/v4/view/ab;->a(Landroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 6592
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->f(I)Z

    move-result v2

    goto :goto_16

    .line 6578
    nop

    :sswitch_data_46
    .sparse-switch
        0x15 -> :sswitch_1a
        0x16 -> :sswitch_21
        0x3d -> :sswitch_28
    .end sparse-switch
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 2808
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_e

    .line 2809
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 2825
    :cond_d
    :goto_d
    return v0

    .line 2813
    :cond_e
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    move v1, v0

    .line 2814
    :goto_13
    if-ge v1, v2, :cond_d

    .line 2815
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2816
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_33

    .line 2817
    invoke-direct {p0, v3}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)Landroid/support/v4/view/er;

    move-result-object v4

    .line 2818
    if-eqz v4, :cond_33

    iget v4, v4, Landroid/support/v4/view/er;->b:I

    iget v5, p0, Landroid/support/v4/view/ViewPager;->u:I

    if-ne v4, v5, :cond_33

    invoke-virtual {v3, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v3

    if-eqz v3, :cond_33

    .line 2820
    const/4 v0, 0x1

    goto :goto_d

    .line 2814
    :cond_33
    add-int/lit8 v1, v1, 0x1

    goto :goto_13
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 9

    .prologue
    const/4 v2, 0x1

    .line 2259
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 2260
    const/4 v0, 0x0

    .line 2262
    invoke-static {p0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;)I

    move-result v1

    .line 2263
    if-eqz v1, :cond_19

    if-ne v1, v2, :cond_a3

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-eqz v1, :cond_a3

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v1}, Landroid/support/v4/view/by;->e()I

    move-result v1

    if-le v1, v2, :cond_a3

    .line 2266
    :cond_19
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->ai:Landroid/support/v4/widget/al;

    invoke-virtual {v1}, Landroid/support/v4/widget/al;->a()Z

    move-result v1

    if-nez v1, :cond_5a

    .line 2267
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 2268
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v0, v2

    .line 2269
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v2

    .line 2271
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2272
    neg-int v3, v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Landroid/support/v4/view/ViewPager;->E:F

    int-to-float v5, v2

    mul-float/2addr v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2273
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->ai:Landroid/support/v4/widget/al;

    invoke-virtual {v3, v0, v2}, Landroid/support/v4/widget/al;->a(II)V

    .line 2274
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ai:Landroid/support/v4/widget/al;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/al;->a(Landroid/graphics/Canvas;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 2275
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2277
    :cond_5a
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->aj:Landroid/support/v4/widget/al;

    invoke-virtual {v1}, Landroid/support/v4/widget/al;->a()Z

    move-result v1

    if-nez v1, :cond_9d

    .line 2278
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 2279
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v2

    .line 2280
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    .line 2282
    const/high16 v4, 0x42b40000    # 90.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2283
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    iget v5, p0, Landroid/support/v4/view/ViewPager;->F:F

    const/high16 v6, 0x3f800000    # 1.0f

    add-float/2addr v5, v6

    neg-float v5, v5

    int-to-float v6, v2

    mul-float/2addr v5, v6

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2284
    iget-object v4, p0, Landroid/support/v4/view/ViewPager;->aj:Landroid/support/v4/widget/al;

    invoke-virtual {v4, v3, v2}, Landroid/support/v4/widget/al;->a(II)V

    .line 2285
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->aj:Landroid/support/v4/widget/al;

    invoke-virtual {v2, p1}, Landroid/support/v4/widget/al;->a(Landroid/graphics/Canvas;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 2286
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2293
    :cond_9d
    :goto_9d
    if-eqz v0, :cond_a2

    .line 2295
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 2297
    :cond_a2
    return-void

    .line 2289
    :cond_a3
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->ai:Landroid/support/v4/widget/al;

    invoke-virtual {v1}, Landroid/support/v4/widget/al;->b()V

    .line 2290
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->aj:Landroid/support/v4/widget/al;

    invoke-virtual {v1}, Landroid/support/v4/widget/al;->b()V

    goto :goto_9d
.end method

.method protected drawableStateChanged()V
    .registers 3

    .prologue
    .line 792
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 793
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->B:Landroid/graphics/drawable/Drawable;

    .line 794
    if-eqz v0, :cond_14

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_14

    .line 795
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 797
    :cond_14
    return-void
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    .prologue
    .line 2830
    new-instance v0, Landroid/support/v4/view/es;

    invoke-direct {v0}, Landroid/support/v4/view/es;-><init>()V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4

    .prologue
    .line 2845
    new-instance v0, Landroid/support/v4/view/es;

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v4/view/es;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .prologue
    .line 2835
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/support/v4/view/by;
    .registers 2

    .prologue
    .line 475
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    return-object v0
.end method

.method protected getChildDrawingOrder(II)I
    .registers 5

    .prologue
    .line 680
    iget v0, p0, Landroid/support/v4/view/ViewPager;->aw:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    add-int/lit8 v0, p1, -0x1

    sub-int p2, v0, p2

    .line 681
    :cond_9
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ax:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/es;

    iget v0, v0, Landroid/support/v4/view/es;->f:I

    .line 682
    return v0
.end method

.method public getCurrentItem()I
    .registers 2

    .prologue
    .line 510
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    return v0
.end method

.method public getOffscreenPageLimit()I
    .registers 2

    .prologue
    .line 705
    iget v0, p0, Landroid/support/v4/view/ViewPager;->L:I

    return v0
.end method

.method public getPageMargin()I
    .registers 2

    .prologue
    .line 761
    iget v0, p0, Landroid/support/v4/view/ViewPager;->A:I

    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 1396
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 1397
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ak:Z

    .line 1398
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 390
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->az:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 391
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 392
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 18

    .prologue
    .line 2301
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 2304
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/view/ViewPager;->A:I

    if-lez v1, :cond_c0

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->B:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_c0

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_c0

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-eqz v1, :cond_c0

    .line 2305
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v6

    .line 2306
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v7

    .line 2308
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/view/ViewPager;->A:I

    int-to-float v1, v1

    int-to-float v2, v7

    div-float v8, v1, v2

    .line 2309
    const/4 v5, 0x0

    .line 2310
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/er;

    .line 2311
    iget v4, v1, Landroid/support/v4/view/er;->e:F

    .line 2312
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 2313
    iget v3, v1, Landroid/support/v4/view/er;->b:I

    .line 2314
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    add-int/lit8 v10, v9, -0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/er;

    iget v10, v2, Landroid/support/v4/view/er;->b:I

    move v2, v5

    move v5, v3

    .line 2315
    :goto_57
    if-ge v5, v10, :cond_c0

    .line 2316
    :goto_59
    iget v3, v1, Landroid/support/v4/view/er;->b:I

    if-le v5, v3, :cond_6c

    if-ge v2, v9, :cond_6c

    .line 2317
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/er;

    goto :goto_59

    .line 2321
    :cond_6c
    iget v3, v1, Landroid/support/v4/view/er;->b:I

    if-ne v5, v3, :cond_b6

    .line 2322
    iget v3, v1, Landroid/support/v4/view/er;->e:F

    iget v4, v1, Landroid/support/v4/view/er;->d:F

    add-float/2addr v3, v4

    int-to-float v4, v7

    mul-float/2addr v3, v4

    .line 2323
    iget v4, v1, Landroid/support/v4/view/er;->e:F

    iget v11, v1, Landroid/support/v4/view/er;->d:F

    add-float/2addr v4, v11

    add-float/2addr v4, v8

    .line 2330
    :goto_7d
    move-object/from16 v0, p0

    iget v11, v0, Landroid/support/v4/view/ViewPager;->A:I

    int-to-float v11, v11

    add-float/2addr v11, v3

    int-to-float v12, v6

    cmpl-float v11, v11, v12

    if-lez v11, :cond_ab

    .line 2331
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/v4/view/ViewPager;->B:Landroid/graphics/drawable/Drawable;

    float-to-int v12, v3

    move-object/from16 v0, p0

    iget v13, v0, Landroid/support/v4/view/ViewPager;->C:I

    move-object/from16 v0, p0

    iget v14, v0, Landroid/support/v4/view/ViewPager;->A:I

    int-to-float v14, v14

    add-float/2addr v14, v3

    const/high16 v15, 0x3f000000    # 0.5f

    add-float/2addr v14, v15

    float-to-int v14, v14

    move-object/from16 v0, p0

    iget v15, v0, Landroid/support/v4/view/ViewPager;->D:I

    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2333
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/v4/view/ViewPager;->B:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2336
    :cond_ab
    add-int v11, v6, v7

    int-to-float v11, v11

    cmpl-float v3, v3, v11

    if-gtz v3, :cond_c0

    .line 2315
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_57

    .line 2326
    :cond_b6
    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    int-to-float v11, v7

    mul-float/2addr v3, v11

    .line 2327
    const/high16 v11, 0x3f800000    # 1.0f

    add-float/2addr v11, v8

    add-float/2addr v4, v11

    goto :goto_7d

    .line 2341
    :cond_c0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 15

    .prologue
    const/4 v3, -0x1

    const/4 v12, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1875
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 1878
    const/4 v1, 0x3

    if-eq v0, v1, :cond_f

    if-ne v0, v6, :cond_22

    .line 1881
    :cond_f
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->M:Z

    .line 1882
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->N:Z

    .line 1883
    iput v3, p0, Landroid/support/v4/view/ViewPager;->V:I

    .line 1884
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_21

    .line 1885
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 1886
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    .line 2008
    :cond_21
    :goto_21
    return v2

    .line 1893
    :cond_22
    if-eqz v0, :cond_2e

    .line 1894
    iget-boolean v1, p0, Landroid/support/v4/view/ViewPager;->M:Z

    if-eqz v1, :cond_2a

    move v2, v6

    .line 1896
    goto :goto_21

    .line 1898
    :cond_2a
    iget-boolean v1, p0, Landroid/support/v4/view/ViewPager;->N:Z

    if-nez v1, :cond_21

    .line 1904
    :cond_2e
    sparse-switch v0, :sswitch_data_13a

    .line 1999
    :cond_31
    :goto_31
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    if-nez v0, :cond_3b

    .line 2000
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    .line 2002
    :cond_3b
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2008
    iget-boolean v2, p0, Landroid/support/v4/view/ViewPager;->M:Z

    goto :goto_21

    .line 1915
    :sswitch_43
    iget v0, p0, Landroid/support/v4/view/ViewPager;->V:I

    .line 1916
    if-eq v0, v3, :cond_31

    .line 1921
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1922
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v7

    .line 1923
    iget v1, p0, Landroid/support/v4/view/ViewPager;->R:F

    sub-float v8, v7, v1

    .line 1924
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 1925
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v10

    .line 1926
    iget v0, p0, Landroid/support/v4/view/ViewPager;->U:F

    sub-float v0, v10, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v11

    .line 1929
    cmpl-float v0, v8, v12

    if-eqz v0, :cond_9b

    iget v0, p0, Landroid/support/v4/view/ViewPager;->R:F

    .line 5855
    iget v1, p0, Landroid/support/v4/view/ViewPager;->P:I

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-gez v1, :cond_74

    cmpl-float v1, v8, v12

    if-gtz v1, :cond_84

    :cond_74
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v1

    iget v3, p0, Landroid/support/v4/view/ViewPager;->P:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_99

    cmpg-float v0, v8, v12

    if-gez v0, :cond_99

    :cond_84
    move v0, v6

    .line 1929
    :goto_85
    if-nez v0, :cond_9b

    float-to-int v3, v8

    float-to-int v4, v7

    float-to-int v5, v10

    move-object v0, p0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 1932
    iput v7, p0, Landroid/support/v4/view/ViewPager;->R:F

    .line 1933
    iput v10, p0, Landroid/support/v4/view/ViewPager;->S:F

    .line 1934
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->N:Z

    goto :goto_21

    :cond_99
    move v0, v2

    .line 5855
    goto :goto_85

    .line 1937
    :cond_9b
    iget v0, p0, Landroid/support/v4/view/ViewPager;->Q:I

    int-to-float v0, v0

    cmpl-float v0, v9, v0

    if-lez v0, :cond_d8

    const/high16 v0, 0x3f000000    # 0.5f

    mul-float/2addr v0, v9

    cmpl-float v0, v0, v11

    if-lez v0, :cond_d8

    .line 1939
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->M:Z

    .line 1940
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->h()V

    .line 1941
    invoke-direct {p0, v6}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    .line 1942
    cmpl-float v0, v8, v12

    if-lez v0, :cond_d1

    iget v0, p0, Landroid/support/v4/view/ViewPager;->T:F

    iget v1, p0, Landroid/support/v4/view/ViewPager;->Q:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    :goto_bb
    iput v0, p0, Landroid/support/v4/view/ViewPager;->R:F

    .line 1944
    iput v10, p0, Landroid/support/v4/view/ViewPager;->S:F

    .line 1945
    invoke-direct {p0, v6}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 1954
    :cond_c2
    :goto_c2
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->M:Z

    if-eqz v0, :cond_31

    .line 1956
    invoke-direct {p0, v7}, Landroid/support/v4/view/ViewPager;->b(F)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 1957
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    goto/16 :goto_31

    .line 1942
    :cond_d1
    iget v0, p0, Landroid/support/v4/view/ViewPager;->T:F

    iget v1, p0, Landroid/support/v4/view/ViewPager;->Q:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    goto :goto_bb

    .line 1946
    :cond_d8
    iget v0, p0, Landroid/support/v4/view/ViewPager;->Q:I

    int-to-float v0, v0

    cmpl-float v0, v11, v0

    if-lez v0, :cond_c2

    .line 1952
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->N:Z

    goto :goto_c2

    .line 1968
    :sswitch_e2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->T:F

    iput v0, p0, Landroid/support/v4/view/ViewPager;->R:F

    .line 1969
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->U:F

    iput v0, p0, Landroid/support/v4/view/ViewPager;->S:F

    .line 1970
    invoke-static {p1, v2}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->V:I

    .line 1971
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->N:Z

    .line 1973
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 1974
    iget v0, p0, Landroid/support/v4/view/ViewPager;->aA:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_12d

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalX()I

    move-result v0

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->ae:I

    if-le v0, v1, :cond_12d

    .line 1977
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1978
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->K:Z

    .line 1979
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 1980
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->M:Z

    .line 1981
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->h()V

    .line 1982
    invoke-direct {p0, v6}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    goto/16 :goto_31

    .line 1984
    :cond_12d
    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->a(Z)V

    .line 1985
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->M:Z

    goto/16 :goto_31

    .line 1995
    :sswitch_134
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_31

    .line 1904
    nop

    :sswitch_data_13a
    .sparse-switch
        0x0 -> :sswitch_e2
        0x2 -> :sswitch_43
        0x6 -> :sswitch_134
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 23

    .prologue
    .line 1536
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v9

    .line 1537
    sub-int v10, p4, p2

    .line 1538
    sub-int v11, p5, p3

    .line 1539
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v6

    .line 1540
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v2

    .line 1541
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v5

    .line 1542
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingBottom()I

    move-result v3

    .line 1543
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v12

    .line 1545
    const/4 v4, 0x0

    .line 1549
    const/4 v1, 0x0

    move v8, v1

    :goto_1f
    if-ge v8, v9, :cond_be

    .line 1550
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    .line 1551
    invoke-virtual {v13}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v7, 0x8

    if-eq v1, v7, :cond_141

    .line 1552
    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/es;

    .line 1555
    iget-boolean v7, v1, Landroid/support/v4/view/es;->a:Z

    if-eqz v7, :cond_141

    .line 1556
    iget v7, v1, Landroid/support/v4/view/es;->b:I

    and-int/lit8 v7, v7, 0x7

    .line 1557
    iget v1, v1, Landroid/support/v4/view/es;->b:I

    and-int/lit8 v14, v1, 0x70

    .line 1558
    packed-switch v7, :pswitch_data_148

    :pswitch_44
    move v7, v6

    .line 1575
    :goto_45
    sparse-switch v14, :sswitch_data_156

    move v1, v2

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 1592
    :goto_4e
    add-int/2addr v7, v12

    .line 1593
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    add-int/2addr v14, v7

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    add-int/2addr v15, v1

    invoke-virtual {v13, v7, v1, v14, v15}, Landroid/view/View;->layout(IIII)V

    .line 1596
    add-int/lit8 v1, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v5

    move v5, v6

    .line 1549
    :goto_62
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    move v6, v5

    move v5, v2

    move v2, v4

    move v4, v1

    goto :goto_1f

    .line 1564
    :pswitch_6a
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v6

    move v7, v6

    move v6, v1

    .line 1565
    goto :goto_45

    .line 1567
    :pswitch_72
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v10, v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v7, v1

    .line 1569
    goto :goto_45

    .line 1571
    :pswitch_80
    sub-int v1, v10, v5

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    sub-int/2addr v1, v7

    .line 1572
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v5, v7

    move v7, v1

    goto :goto_45

    .line 1581
    :sswitch_8e
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v2

    move/from16 v16, v2

    move v2, v3

    move v3, v1

    move/from16 v1, v16

    .line 1582
    goto :goto_4e

    .line 1584
    :sswitch_9a
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v11, v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 1586
    goto :goto_4e

    .line 1588
    :sswitch_ac
    sub-int v1, v11, v3

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    sub-int/2addr v1, v14

    .line 1589
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    add-int/2addr v3, v14

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    goto :goto_4e

    .line 1601
    :cond_be
    sub-int v1, v10, v6

    sub-int v7, v1, v5

    .line 1603
    const/4 v1, 0x0

    move v5, v1

    :goto_c4
    if-ge v5, v9, :cond_11b

    .line 1604
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1605
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v10, 0x8

    if-eq v1, v10, :cond_117

    .line 1606
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/es;

    .line 1608
    iget-boolean v10, v1, Landroid/support/v4/view/es;->a:Z

    if-nez v10, :cond_117

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)Landroid/support/v4/view/er;

    move-result-object v10

    if-eqz v10, :cond_117

    .line 1609
    int-to-float v12, v7

    iget v10, v10, Landroid/support/v4/view/er;->e:F

    mul-float/2addr v10, v12

    float-to-int v10, v10

    .line 1610
    add-int/2addr v10, v6

    .line 1612
    iget-boolean v12, v1, Landroid/support/v4/view/es;->d:Z

    if-eqz v12, :cond_10a

    .line 1615
    const/4 v12, 0x0

    iput-boolean v12, v1, Landroid/support/v4/view/es;->d:Z

    .line 1616
    int-to-float v12, v7

    iget v1, v1, Landroid/support/v4/view/es;->c:F

    mul-float/2addr v1, v12

    float-to-int v1, v1

    const/high16 v12, 0x40000000    # 2.0f

    invoke-static {v1, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1619
    sub-int v12, v11, v2

    sub-int/2addr v12, v3

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v12, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    .line 1622
    invoke-virtual {v8, v1, v12}, Landroid/view/View;->measure(II)V

    .line 1627
    :cond_10a
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v10

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v2

    invoke-virtual {v8, v10, v2, v1, v12}, Landroid/view/View;->layout(IIII)V

    .line 1603
    :cond_117
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_c4

    .line 1633
    :cond_11b
    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v4/view/ViewPager;->C:I

    .line 1634
    sub-int v1, v11, v3

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/view/ViewPager;->D:I

    .line 1635
    move-object/from16 v0, p0

    iput v4, v0, Landroid/support/v4/view/ViewPager;->an:I

    .line 1637
    move-object/from16 v0, p0

    iget-boolean v1, v0, Landroid/support/v4/view/ViewPager;->ak:Z

    if-eqz v1, :cond_13b

    .line 1638
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/view/ViewPager;->u:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/support/v4/view/ViewPager;->a(IZIZ)V

    .line 1640
    :cond_13b
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/v4/view/ViewPager;->ak:Z

    .line 1641
    return-void

    :cond_141
    move v1, v4

    move v4, v2

    move v2, v5

    move v5, v6

    goto/16 :goto_62

    .line 1558
    nop

    :pswitch_data_148
    .packed-switch 0x1
        :pswitch_72
        :pswitch_44
        :pswitch_6a
        :pswitch_44
        :pswitch_80
    .end packed-switch

    .line 1575
    :sswitch_data_156
    .sparse-switch
        0x10 -> :sswitch_9a
        0x30 -> :sswitch_8e
        0x50 -> :sswitch_ac
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .registers 16

    .prologue
    .line 1407
    const/4 v0, 0x0

    invoke-static {v0, p1}, Landroid/support/v4/view/ViewPager;->getDefaultSize(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1, p2}, Landroid/support/v4/view/ViewPager;->getDefaultSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->setMeasuredDimension(II)V

    .line 1410
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getMeasuredWidth()I

    move-result v0

    .line 1411
    div-int/lit8 v1, v0, 0xa

    .line 1412
    iget v2, p0, Landroid/support/v4/view/ViewPager;->O:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Landroid/support/v4/view/ViewPager;->P:I

    .line 1415
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v1

    sub-int v3, v0, v1

    .line 1416
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingBottom()I

    move-result v1

    sub-int v5, v0, v1

    .line 1423
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v9

    .line 1424
    const/4 v0, 0x0

    move v8, v0

    :goto_3b
    if-ge v8, v9, :cond_bc

    .line 1425
    invoke-virtual {p0, v8}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 1426
    invoke-virtual {v10}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_a5

    .line 1427
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/es;

    .line 1428
    if-eqz v0, :cond_a5

    iget-boolean v1, v0, Landroid/support/v4/view/es;->a:Z

    if-eqz v1, :cond_a5

    .line 1429
    iget v1, v0, Landroid/support/v4/view/es;->b:I

    and-int/lit8 v6, v1, 0x7

    .line 1430
    iget v1, v0, Landroid/support/v4/view/es;->b:I

    and-int/lit8 v4, v1, 0x70

    .line 1431
    const/high16 v2, -0x80000000

    .line 1432
    const/high16 v1, -0x80000000

    .line 1433
    const/16 v7, 0x30

    if-eq v4, v7, :cond_69

    const/16 v7, 0x50

    if-ne v4, v7, :cond_a9

    :cond_69
    const/4 v4, 0x1

    move v7, v4

    .line 1434
    :goto_6b
    const/4 v4, 0x3

    if-eq v6, v4, :cond_71

    const/4 v4, 0x5

    if-ne v6, v4, :cond_ac

    :cond_71
    const/4 v4, 0x1

    move v6, v4

    .line 1436
    :goto_73
    if-eqz v7, :cond_af

    .line 1437
    const/high16 v2, 0x40000000    # 2.0f

    .line 1444
    :cond_77
    :goto_77
    iget v4, v0, Landroid/support/v4/view/es;->width:I

    const/4 v11, -0x2

    if-eq v4, v11, :cond_10f

    .line 1445
    const/high16 v4, 0x40000000    # 2.0f

    .line 1446
    iget v2, v0, Landroid/support/v4/view/es;->width:I

    const/4 v11, -0x1

    if-eq v2, v11, :cond_10c

    .line 1447
    iget v2, v0, Landroid/support/v4/view/es;->width:I

    .line 1450
    :goto_85
    iget v11, v0, Landroid/support/v4/view/es;->height:I

    const/4 v12, -0x2

    if-eq v11, v12, :cond_10a

    .line 1451
    const/high16 v1, 0x40000000    # 2.0f

    .line 1452
    iget v11, v0, Landroid/support/v4/view/es;->height:I

    const/4 v12, -0x1

    if-eq v11, v12, :cond_10a

    .line 1453
    iget v0, v0, Landroid/support/v4/view/es;->height:I

    .line 1456
    :goto_93
    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1457
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1458
    invoke-virtual {v10, v2, v0}, Landroid/view/View;->measure(II)V

    .line 1460
    if-eqz v7, :cond_b4

    .line 1461
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v5, v0

    .line 1424
    :cond_a5
    :goto_a5
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_3b

    .line 1433
    :cond_a9
    const/4 v4, 0x0

    move v7, v4

    goto :goto_6b

    .line 1434
    :cond_ac
    const/4 v4, 0x0

    move v6, v4

    goto :goto_73

    .line 1438
    :cond_af
    if-eqz v6, :cond_77

    .line 1439
    const/high16 v1, 0x40000000    # 2.0f

    goto :goto_77

    .line 1462
    :cond_b4
    if-eqz v6, :cond_a5

    .line 1463
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v3, v0

    goto :goto_a5

    .line 1469
    :cond_bc
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->G:I

    .line 1470
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->H:I

    .line 1473
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->I:Z

    .line 1474
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 1475
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->I:Z

    .line 1478
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    .line 1479
    const/4 v0, 0x0

    move v1, v0

    :goto_db
    if-ge v1, v2, :cond_109

    .line 1480
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1481
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v5, 0x8

    if-eq v0, v5, :cond_105

    .line 1485
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/es;

    .line 1486
    if-eqz v0, :cond_f5

    iget-boolean v5, v0, Landroid/support/v4/view/es;->a:Z

    if-nez v5, :cond_105

    .line 1487
    :cond_f5
    int-to-float v5, v3

    iget v0, v0, Landroid/support/v4/view/es;->c:F

    mul-float/2addr v0, v5

    float-to-int v0, v0

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1489
    iget v5, p0, Landroid/support/v4/view/ViewPager;->H:I

    invoke-virtual {v4, v0, v5}, Landroid/view/View;->measure(II)V

    .line 1479
    :cond_105
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_db

    .line 1493
    :cond_109
    return-void

    :cond_10a
    move v0, v5

    goto :goto_93

    :cond_10c
    move v2, v3

    goto/16 :goto_85

    :cond_10f
    move v4, v2

    move v2, v3

    goto/16 :goto_85
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .registers 11

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 2781
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    .line 2782
    and-int/lit8 v4, p1, 0x2

    if-eqz v4, :cond_2c

    move v3, v0

    move v4, v1

    .line 2791
    :goto_d
    if-eq v4, v2, :cond_33

    .line 2792
    invoke-virtual {p0, v4}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 2793
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_31

    .line 2794
    invoke-direct {p0, v5}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)Landroid/support/v4/view/er;

    move-result-object v6

    .line 2795
    if-eqz v6, :cond_31

    iget v6, v6, Landroid/support/v4/view/er;->b:I

    iget v7, p0, Landroid/support/v4/view/ViewPager;->u:I

    if-ne v6, v7, :cond_31

    .line 2796
    invoke-virtual {v5, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_31

    .line 2802
    :goto_2b
    return v0

    .line 2787
    :cond_2c
    add-int/lit8 v2, v2, -0x1

    move v4, v2

    move v2, v3

    .line 2789
    goto :goto_d

    .line 2791
    :cond_31
    add-int/2addr v4, v3

    goto :goto_d

    :cond_33
    move v0, v1

    .line 2802
    goto :goto_2b
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5

    .prologue
    .line 1310
    instance-of v0, p1, Landroid/support/v4/view/ViewPager$SavedState;

    if-nez v0, :cond_8

    .line 1311
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1326
    :goto_7
    return-void

    .line 1315
    :cond_8
    check-cast p1, Landroid/support/v4/view/ViewPager$SavedState;

    .line 1316
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1318
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-eqz v0, :cond_26

    .line 1319
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    iget-object v1, p1, Landroid/support/v4/view/ViewPager$SavedState;->b:Landroid/os/Parcelable;

    iget-object v2, p1, Landroid/support/v4/view/ViewPager$SavedState;->c:Ljava/lang/ClassLoader;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/by;->a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 1320
    iget v0, p1, Landroid/support/v4/view/ViewPager$SavedState;->a:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    goto :goto_7

    .line 1322
    :cond_26
    iget v0, p1, Landroid/support/v4/view/ViewPager$SavedState;->a:I

    iput v0, p0, Landroid/support/v4/view/ViewPager;->v:I

    .line 1323
    iget-object v0, p1, Landroid/support/v4/view/ViewPager$SavedState;->b:Landroid/os/Parcelable;

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->w:Landroid/os/Parcelable;

    .line 1324
    iget-object v0, p1, Landroid/support/v4/view/ViewPager$SavedState;->c:Ljava/lang/ClassLoader;

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->x:Ljava/lang/ClassLoader;

    goto :goto_7
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 3

    .prologue
    .line 1299
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1300
    new-instance v1, Landroid/support/v4/view/ViewPager$SavedState;

    invoke-direct {v1, v0}, Landroid/support/v4/view/ViewPager$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1301
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    iput v0, v1, Landroid/support/v4/view/ViewPager$SavedState;->a:I

    .line 1302
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-eqz v0, :cond_19

    .line 1303
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v0}, Landroid/support/v4/view/by;->d()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, v1, Landroid/support/v4/view/ViewPager$SavedState;->b:Landroid/os/Parcelable;

    .line 1305
    :cond_19
    return-object v1
.end method

.method protected onSizeChanged(IIII)V
    .registers 7

    .prologue
    .line 1497
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 1500
    if-eq p1, p3, :cond_c

    .line 1501
    iget v0, p0, Landroid/support/v4/view/ViewPager;->A:I

    iget v1, p0, Landroid/support/v4/view/ViewPager;->A:I

    invoke-direct {p0, p1, p3, v0, v1}, Landroid/support/v4/view/ViewPager;->a(IIII)V

    .line 1503
    :cond_c
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 9

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2013
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ag:Z

    if-eqz v0, :cond_9

    move v0, v1

    .line 2134
    :goto_8
    return v0

    .line 2020
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_17

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v0

    if-eqz v0, :cond_17

    move v0, v2

    .line 2023
    goto :goto_8

    .line 2026
    :cond_17
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-eqz v0, :cond_23

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v0}, Landroid/support/v4/view/by;->e()I

    move-result v0

    if-nez v0, :cond_25

    :cond_23
    move v0, v2

    .line 2028
    goto :goto_8

    .line 2031
    :cond_25
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    if-nez v0, :cond_2f

    .line 2032
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    .line 2034
    :cond_2f
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2036
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 2039
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_16e

    .line 2131
    :cond_3d
    :goto_3d
    :pswitch_3d
    if-eqz v2, :cond_42

    .line 2132
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    :cond_42
    move v0, v1

    .line 2134
    goto :goto_8

    .line 2041
    :pswitch_44
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->y:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 2042
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->K:Z

    .line 2043
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 2046
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->T:F

    iput v0, p0, Landroid/support/v4/view/ViewPager;->R:F

    .line 2047
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->U:F

    iput v0, p0, Landroid/support/v4/view/ViewPager;->S:F

    .line 2048
    invoke-static {p1, v2}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->V:I

    goto :goto_3d

    .line 2052
    :pswitch_65
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->M:Z

    if-nez v0, :cond_b9

    .line 2053
    iget v0, p0, Landroid/support/v4/view/ViewPager;->V:I

    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 2054
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 2055
    iget v4, p0, Landroid/support/v4/view/ViewPager;->R:F

    sub-float v4, v3, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 2056
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v5

    .line 2057
    iget v0, p0, Landroid/support/v4/view/ViewPager;->S:F

    sub-float v0, v5, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 2059
    iget v6, p0, Landroid/support/v4/view/ViewPager;->Q:I

    int-to-float v6, v6

    cmpl-float v6, v4, v6

    if-lez v6, :cond_b9

    cmpl-float v0, v4, v0

    if-lez v0, :cond_b9

    .line 2061
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->M:Z

    .line 2062
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->h()V

    .line 2063
    iget v0, p0, Landroid/support/v4/view/ViewPager;->T:F

    sub-float v0, v3, v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_cf

    iget v0, p0, Landroid/support/v4/view/ViewPager;->T:F

    iget v3, p0, Landroid/support/v4/view/ViewPager;->Q:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    :goto_a6
    iput v0, p0, Landroid/support/v4/view/ViewPager;->R:F

    .line 2065
    iput v5, p0, Landroid/support/v4/view/ViewPager;->S:F

    .line 2066
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    .line 2067
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 2070
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2071
    if-eqz v0, :cond_b9

    .line 2072
    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2077
    :cond_b9
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->M:Z

    if-eqz v0, :cond_3d

    .line 2079
    iget v0, p0, Landroid/support/v4/view/ViewPager;->V:I

    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 2081
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 2082
    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->b(F)Z

    move-result v0

    or-int/lit8 v2, v0, 0x0

    .line 2083
    goto/16 :goto_3d

    .line 2063
    :cond_cf
    iget v0, p0, Landroid/support/v4/view/ViewPager;->T:F

    iget v3, p0, Landroid/support/v4/view/ViewPager;->Q:I

    int-to-float v3, v3

    sub-float/2addr v0, v3

    goto :goto_a6

    .line 2086
    :pswitch_d6
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->M:Z

    if-eqz v0, :cond_3d

    .line 2087
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->aa:Landroid/view/VelocityTracker;

    .line 2088
    const/16 v2, 0x3e8

    iget v3, p0, Landroid/support/v4/view/ViewPager;->ac:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 2089
    iget v2, p0, Landroid/support/v4/view/ViewPager;->V:I

    invoke-static {v0, v2}, Landroid/support/v4/view/cs;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    float-to-int v0, v0

    .line 2091
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->K:Z

    .line 2092
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v2

    .line 2093
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v3

    .line 2094
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->i()Landroid/support/v4/view/er;

    move-result-object v4

    .line 2095
    iget v5, v4, Landroid/support/v4/view/er;->b:I

    .line 2096
    int-to-float v3, v3

    int-to-float v2, v2

    div-float v2, v3, v2

    iget v3, v4, Landroid/support/v4/view/er;->e:F

    sub-float/2addr v2, v3

    iget v3, v4, Landroid/support/v4/view/er;->d:F

    div-float/2addr v2, v3

    .line 2097
    iget v3, p0, Landroid/support/v4/view/ViewPager;->V:I

    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 2099
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 2100
    iget v4, p0, Landroid/support/v4/view/ViewPager;->T:F

    sub-float/2addr v3, v4

    float-to-int v3, v3

    .line 2101
    invoke-direct {p0, v5, v2, v0, v3}, Landroid/support/v4/view/ViewPager;->a(IFII)I

    move-result v2

    .line 2103
    invoke-direct {p0, v2, v1, v1, v0}, Landroid/support/v4/view/ViewPager;->a(IZZI)V

    .line 2105
    iput v6, p0, Landroid/support/v4/view/ViewPager;->V:I

    .line 2106
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->m()V

    .line 2107
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ai:Landroid/support/v4/widget/al;

    invoke-virtual {v0}, Landroid/support/v4/widget/al;->c()Z

    move-result v0

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->aj:Landroid/support/v4/widget/al;

    invoke-virtual {v2}, Landroid/support/v4/widget/al;->c()Z

    move-result v2

    or-int/2addr v2, v0

    .line 2108
    goto/16 :goto_3d

    .line 2111
    :pswitch_12e
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->M:Z

    if-eqz v0, :cond_3d

    .line 2112
    iget v0, p0, Landroid/support/v4/view/ViewPager;->u:I

    invoke-direct {p0, v0, v1, v2, v2}, Landroid/support/v4/view/ViewPager;->a(IZIZ)V

    .line 2113
    iput v6, p0, Landroid/support/v4/view/ViewPager;->V:I

    .line 2114
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->m()V

    .line 2115
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ai:Landroid/support/v4/widget/al;

    invoke-virtual {v0}, Landroid/support/v4/widget/al;->c()Z

    move-result v0

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->aj:Landroid/support/v4/widget/al;

    invoke-virtual {v2}, Landroid/support/v4/widget/al;->c()Z

    move-result v2

    or-int/2addr v2, v0

    goto/16 :goto_3d

    .line 2119
    :pswitch_14b
    invoke-static {p1}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 2120
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 2121
    iput v3, p0, Landroid/support/v4/view/ViewPager;->R:F

    .line 2122
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->V:I

    goto/16 :goto_3d

    .line 2126
    :pswitch_15d
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/MotionEvent;)V

    .line 2127
    iget v0, p0, Landroid/support/v4/view/ViewPager;->V:I

    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->R:F

    goto/16 :goto_3d

    .line 2039
    :pswitch_data_16e
    .packed-switch 0x0
        :pswitch_44
        :pswitch_d6
        :pswitch_65
        :pswitch_12e
        :pswitch_3d
        :pswitch_14b
        :pswitch_15d
    .end packed-switch
.end method

.method public removeView(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 1356
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->I:Z

    if-eqz v0, :cond_8

    .line 1357
    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->removeViewInLayout(Landroid/view/View;)V

    .line 1361
    :goto_7
    return-void

    .line 1359
    :cond_8
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_7
.end method

.method public setAdapter(Landroid/support/v4/view/by;)V
    .registers 9

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 413
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-eqz v0, :cond_59

    .line 414
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->z:Landroid/support/v4/view/ex;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/by;->b(Landroid/database/DataSetObserver;)V

    move v1, v2

    .line 416
    :goto_f
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2c

    .line 417
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/er;

    .line 418
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    iget v4, v0, Landroid/support/v4/view/er;->b:I

    iget-object v0, v0, Landroid/support/v4/view/er;->a:Ljava/lang/Object;

    invoke-virtual {v3, v4, v0}, Landroid/support/v4/view/by;->a(ILjava/lang/Object;)V

    .line 416
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f

    .line 420
    :cond_2c
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v0}, Landroid/support/v4/view/by;->c()V

    .line 421
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v1, v2

    .line 4459
    :goto_37
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_54

    .line 4460
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4461
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/es;

    .line 4462
    iget-boolean v0, v0, Landroid/support/v4/view/es;->a:Z

    if-nez v0, :cond_50

    .line 4463
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->removeViewAt(I)V

    .line 4464
    add-int/lit8 v1, v1, -0x1

    .line 4459
    :cond_50
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_37

    .line 423
    :cond_54
    iput v2, p0, Landroid/support/v4/view/ViewPager;->u:I

    .line 424
    invoke-virtual {p0, v2, v2}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 427
    :cond_59
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    .line 428
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    .line 429
    iput v2, p0, Landroid/support/v4/view/ViewPager;->n:I

    .line 431
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    if-eqz v1, :cond_9c

    .line 432
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->z:Landroid/support/v4/view/ex;

    if-nez v1, :cond_6e

    .line 433
    new-instance v1, Landroid/support/v4/view/ex;

    invoke-direct {v1, p0, v2}, Landroid/support/v4/view/ex;-><init>(Landroid/support/v4/view/ViewPager;B)V

    iput-object v1, p0, Landroid/support/v4/view/ViewPager;->z:Landroid/support/v4/view/ex;

    .line 435
    :cond_6e
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->z:Landroid/support/v4/view/ex;

    invoke-virtual {v1, v3}, Landroid/support/v4/view/by;->a(Landroid/database/DataSetObserver;)V

    .line 436
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->K:Z

    .line 437
    iget-boolean v1, p0, Landroid/support/v4/view/ViewPager;->ak:Z

    .line 438
    iput-boolean v5, p0, Landroid/support/v4/view/ViewPager;->ak:Z

    .line 439
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    invoke-virtual {v3}, Landroid/support/v4/view/by;->e()I

    move-result v3

    iput v3, p0, Landroid/support/v4/view/ViewPager;->n:I

    .line 440
    iget v3, p0, Landroid/support/v4/view/ViewPager;->v:I

    if-ltz v3, :cond_a8

    .line 441
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->t:Landroid/support/v4/view/by;

    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->w:Landroid/os/Parcelable;

    iget-object v4, p0, Landroid/support/v4/view/ViewPager;->x:Ljava/lang/ClassLoader;

    invoke-virtual {v1, v3, v4}, Landroid/support/v4/view/by;->a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 442
    iget v1, p0, Landroid/support/v4/view/ViewPager;->v:I

    invoke-direct {p0, v1, v2, v5}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    .line 443
    const/4 v1, -0x1

    iput v1, p0, Landroid/support/v4/view/ViewPager;->v:I

    .line 444
    iput-object v6, p0, Landroid/support/v4/view/ViewPager;->w:Landroid/os/Parcelable;

    .line 445
    iput-object v6, p0, Landroid/support/v4/view/ViewPager;->x:Ljava/lang/ClassLoader;

    .line 453
    :cond_9c
    :goto_9c
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->aq:Landroid/support/v4/view/eu;

    if-eqz v1, :cond_a7

    if-eq v0, p1, :cond_a7

    .line 454
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->aq:Landroid/support/v4/view/eu;

    invoke-interface {v1, v0, p1}, Landroid/support/v4/view/eu;->a(Landroid/support/v4/view/by;Landroid/support/v4/view/by;)V

    .line 456
    :cond_a7
    return-void

    .line 446
    :cond_a8
    if-nez v1, :cond_ae

    .line 447
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    goto :goto_9c

    .line 449
    :cond_ae
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->requestLayout()V

    goto :goto_9c
.end method

.method setChildrenDrawingOrderEnabledCompat(Z)V
    .registers 7

    .prologue
    .line 661
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x7

    if-lt v0, v1, :cond_2a

    .line 662
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->as:Ljava/lang/reflect/Method;

    if-nez v0, :cond_1b

    .line 664
    :try_start_9
    const-class v0, Landroid/view/ViewGroup;

    const-string v1, "setChildrenDrawingOrderEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->as:Ljava/lang/reflect/Method;
    :try_end_1b
    .catch Ljava/lang/NoSuchMethodException; {:try_start_9 .. :try_end_1b} :catch_2b

    .line 671
    :cond_1b
    :goto_1b
    :try_start_1b
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->as:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_2a} :catch_34

    .line 676
    :cond_2a
    :goto_2a
    return-void

    .line 666
    :catch_2b
    move-exception v0

    .line 667
    const-string v1, "ViewPager"

    const-string v2, "Can\'t find setChildrenDrawingOrderEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1b

    .line 672
    :catch_34
    move-exception v0

    .line 673
    const-string v1, "ViewPager"

    const-string v2, "Error changing children drawing order"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2a
.end method

.method public setCurrentItem(I)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 494
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->K:Z

    .line 495
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ak:Z

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_8
    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    .line 496
    return-void

    :cond_c
    move v0, v1

    .line 495
    goto :goto_8
.end method

.method public setOffscreenPageLimit(I)V
    .registers 5

    .prologue
    .line 726
    if-gtz p1, :cond_1d

    .line 727
    const-string v0, "ViewPager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested offscreen page limit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " too small; defaulting to 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    const/4 p1, 0x1

    .line 731
    :cond_1d
    iget v0, p0, Landroid/support/v4/view/ViewPager;->L:I

    if-eq p1, v0, :cond_26

    .line 732
    iput p1, p0, Landroid/support/v4/view/ViewPager;->L:I

    .line 733
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 735
    :cond_26
    return-void
.end method

.method setOnAdapterChangeListener(Landroid/support/v4/view/eu;)V
    .registers 2

    .prologue
    .line 479
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->aq:Landroid/support/v4/view/eu;

    .line 480
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ev;)V
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 592
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->ao:Landroid/support/v4/view/ev;

    .line 593
    return-void
.end method

.method public setPageMargin(I)V
    .registers 4

    .prologue
    .line 746
    iget v0, p0, Landroid/support/v4/view/ViewPager;->A:I

    .line 747
    iput p1, p0, Landroid/support/v4/view/ViewPager;->A:I

    .line 749
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v1

    .line 750
    invoke-direct {p0, v1, v1, p1, v0}, Landroid/support/v4/view/ViewPager;->a(IIII)V

    .line 752
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->requestLayout()V

    .line 753
    return-void
.end method

.method public setPageMarginDrawable(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 782
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 783
    return-void
.end method

.method public setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 770
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->B:Landroid/graphics/drawable/Drawable;

    .line 771
    if-eqz p1, :cond_7

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->refreshDrawableState()V

    .line 772
    :cond_7
    if-nez p1, :cond_11

    const/4 v0, 0x1

    :goto_a
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setWillNotDraw(Z)V

    .line 773
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->invalidate()V

    .line 774
    return-void

    .line 772
    :cond_11
    const/4 v0, 0x0

    goto :goto_a
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3

    .prologue
    .line 787
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->B:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method
