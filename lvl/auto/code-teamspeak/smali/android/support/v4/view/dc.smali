.class Landroid/support/v4/view/dc;
.super Landroid/support/v4/view/db;
.source "SourceFile"


# static fields
.field static b:Ljava/lang/reflect/Field;

.field static c:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 1154
    const/4 v0, 0x0

    sput-boolean v0, Landroid/support/v4/view/dc;->c:Z

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 1152
    invoke-direct {p0}, Landroid/support/v4/view/db;-><init>()V

    return-void
.end method


# virtual methods
.method public final H(Landroid/view/View;)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1207
    iget-object v0, p0, Landroid/support/v4/view/dc;->a:Ljava/util/WeakHashMap;

    if-nez v0, :cond_b

    .line 1208
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/dc;->a:Ljava/util/WeakHashMap;

    .line 1211
    :cond_b
    iget-object v0, p0, Landroid/support/v4/view/dc;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/fk;

    .line 1212
    if-nez v0, :cond_1f

    .line 1213
    new-instance v0, Landroid/support/v4/view/fk;

    invoke-direct {v0, p1}, Landroid/support/v4/view/fk;-><init>(Landroid/view/View;)V

    .line 1214
    iget-object v1, p0, Landroid/support/v4/view/dc;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1216
    :cond_1f
    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
    .registers 4

    .prologue
    .line 1173
    .line 3258
    iget-object v0, p2, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    .line 4051
    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, v0}, Landroid/view/View;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1174
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/support/v4/view/a;)V
    .registers 4
    .param p2    # Landroid/support/v4/view/a;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 1178
    if-nez p2, :cond_9

    const/4 v0, 0x0

    .line 5039
    :goto_3
    check-cast v0, Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 1180
    return-void

    .line 4318
    :cond_9
    iget-object v0, p2, Landroid/support/v4/view/a;->b:Ljava/lang/Object;

    goto :goto_3
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3

    .prologue
    .line 1165
    .line 3043
    invoke-virtual {p1, p2}, Landroid/view/View;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1166
    return-void
.end method

.method public final a(Landroid/view/View;I)Z
    .registers 4

    .prologue
    .line 1157
    .line 3031
    invoke-virtual {p1, p2}, Landroid/view/View;->canScrollHorizontally(I)Z

    move-result v0

    .line 1157
    return v0
.end method

.method public final b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3

    .prologue
    .line 1169
    .line 3047
    invoke-virtual {p1, p2}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1170
    return-void
.end method

.method public final b(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 1221
    .line 5055
    invoke-virtual {p1, p2}, Landroid/view/View;->setFitsSystemWindows(Z)V

    .line 1222
    return-void
.end method

.method public final b(Landroid/view/View;)Z
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1184
    sget-boolean v2, Landroid/support/v4/view/dc;->c:Z

    if-eqz v2, :cond_7

    .line 1201
    :cond_6
    :goto_6
    return v0

    .line 1187
    :cond_7
    sget-object v2, Landroid/support/v4/view/dc;->b:Ljava/lang/reflect/Field;

    if-nez v2, :cond_19

    .line 1189
    :try_start_b
    const-class v2, Landroid/view/View;

    const-string v3, "mAccessibilityDelegate"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 1191
    sput-object v2, Landroid/support/v4/view/dc;->b:Ljava/lang/reflect/Field;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_19} :catch_23

    .line 1198
    :cond_19
    :try_start_19
    sget-object v2, Landroid/support/v4/view/dc;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v2, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_1e} :catch_27

    move-result-object v2

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_6

    .line 1193
    :catch_23
    move-exception v2

    sput-boolean v1, Landroid/support/v4/view/dc;->c:Z

    goto :goto_6

    .line 1200
    :catch_27
    move-exception v2

    sput-boolean v1, Landroid/support/v4/view/dc;->c:Z

    goto :goto_6
.end method

.method public final b(Landroid/view/View;I)Z
    .registers 4

    .prologue
    .line 1161
    .line 3035
    invoke-virtual {p1, p2}, Landroid/view/View;->canScrollVertically(I)Z

    move-result v0

    .line 1161
    return v0
.end method
