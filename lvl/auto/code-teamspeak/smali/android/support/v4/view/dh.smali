.class final Landroid/support/v4/view/dh;
.super Landroid/support/v4/view/dg;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1400
    invoke-direct {p0}, Landroid/support/v4/view/dg;-><init>()V

    return-void
.end method


# virtual methods
.method public final K(Landroid/view/View;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 1408
    .line 2031
    invoke-virtual {p1}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v0

    .line 1408
    return-object v0
.end method

.method public final M(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 1413
    .line 2035
    invoke-virtual {p1}, Landroid/view/View;->requestApplyInsets()V

    .line 1414
    return-void
.end method

.method public final N(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1423
    .line 2043
    invoke-virtual {p1}, Landroid/view/View;->getElevation()F

    move-result v0

    .line 1423
    return v0
.end method

.method public final O(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1433
    .line 2051
    invoke-virtual {p1}, Landroid/view/View;->getTranslationZ()F

    move-result v0

    .line 1433
    return v0
.end method

.method public final U(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1448
    .line 2124
    invoke-virtual {p1}, Landroid/view/View;->isNestedScrollingEnabled()Z

    move-result v0

    .line 1448
    return v0
.end method

.method public final V(Landroid/view/View;)Landroid/content/res/ColorStateList;
    .registers 3

    .prologue
    .line 1498
    .line 3074
    invoke-virtual {p1}, Landroid/view/View;->getBackgroundTintList()Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 1498
    return-object v0
.end method

.method public final W(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;
    .registers 3

    .prologue
    .line 1513
    .line 4082
    invoke-virtual {p1}, Landroid/view/View;->getBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    .line 1513
    return-object v0
.end method

.method public final X(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 1458
    .line 2132
    invoke-virtual {p1}, Landroid/view/View;->stopNestedScroll()V

    .line 1459
    return-void
.end method

.method public final Y(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1463
    .line 2136
    invoke-virtual {p1}, Landroid/view/View;->hasNestedScrollingParent()Z

    move-result v0

    .line 1463
    return v0
.end method

.method public final a(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
    .registers 5

    .prologue
    .line 4090
    instance-of v0, p2, Landroid/support/v4/view/gi;

    if-eqz v0, :cond_14

    move-object v0, p2

    .line 4092
    check-cast v0, Landroid/support/v4/view/gi;

    .line 4116
    iget-object v0, v0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    .line 4094
    invoke-virtual {p1, v0}, Landroid/view/View;->onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;

    move-result-object v1

    .line 4096
    if-eq v1, v0, :cond_14

    .line 4098
    new-instance p2, Landroid/support/v4/view/gi;

    invoke-direct {p2, v1}, Landroid/support/v4/view/gi;-><init>(Landroid/view/WindowInsets;)V

    .line 1518
    :cond_14
    return-object p2
.end method

.method public final a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
    .registers 3

    .prologue
    .line 1503
    .line 3078
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 1504
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
    .registers 3

    .prologue
    .line 1508
    .line 3086
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 1509
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/support/v4/view/bx;)V
    .registers 4

    .prologue
    .line 1438
    .line 2056
    new-instance v0, Landroid/support/v4/view/dt;

    invoke-direct {v0, p2}, Landroid/support/v4/view/dt;-><init>(Landroid/support/v4/view/bx;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    .line 1439
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 1403
    .line 2027
    invoke-virtual {p1, p2}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 1404
    return-void
.end method

.method public final a(Landroid/view/View;FF)Z
    .registers 5

    .prologue
    .line 1488
    .line 2156
    invoke-virtual {p1, p2, p3}, Landroid/view/View;->dispatchNestedPreFling(FF)Z

    move-result v0

    .line 1488
    return v0
.end method

.method public final a(Landroid/view/View;FFZ)Z
    .registers 6

    .prologue
    .line 1483
    .line 2152
    invoke-virtual {p1, p2, p3, p4}, Landroid/view/View;->dispatchNestedFling(FFZ)Z

    move-result v0

    .line 1483
    return v0
.end method

.method public final a(Landroid/view/View;IIII[I)Z
    .registers 8

    .prologue
    .line 1469
    .line 2141
    invoke-virtual/range {p1 .. p6}, Landroid/view/View;->dispatchNestedScroll(IIII[I)Z

    move-result v0

    .line 1469
    return v0
.end method

.method public final a(Landroid/view/View;II[I[I)Z
    .registers 7

    .prologue
    .line 1476
    .line 2147
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->dispatchNestedPreScroll(II[I[I)Z

    move-result v0

    .line 1476
    return v0
.end method

.method public final aa(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1528
    .line 6160
    invoke-virtual {p1}, Landroid/view/View;->getZ()F

    move-result v0

    .line 1528
    return v0
.end method

.method public final b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
    .registers 5

    .prologue
    .line 5105
    instance-of v0, p2, Landroid/support/v4/view/gi;

    if-eqz v0, :cond_14

    move-object v0, p2

    .line 5107
    check-cast v0, Landroid/support/v4/view/gi;

    .line 6116
    iget-object v0, v0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    .line 5109
    invoke-virtual {p1, v0}, Landroid/view/View;->dispatchApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;

    move-result-object v1

    .line 5111
    if-eq v1, v0, :cond_14

    .line 5113
    new-instance p2, Landroid/support/v4/view/gi;

    invoke-direct {p2, v1}, Landroid/support/v4/view/gi;-><init>(Landroid/view/WindowInsets;)V

    .line 1523
    :cond_14
    return-object p2
.end method

.method public final d(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 1443
    .line 2120
    invoke-virtual {p1, p2}, Landroid/view/View;->setNestedScrollingEnabled(Z)V

    .line 1444
    return-void
.end method

.method public final f(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1493
    .line 3070
    invoke-virtual {p1}, Landroid/view/View;->isImportantForAccessibility()Z

    move-result v0

    .line 1493
    return v0
.end method

.method public final h(Landroid/view/View;I)Z
    .registers 4

    .prologue
    .line 1453
    .line 2128
    invoke-virtual {p1, p2}, Landroid/view/View;->startNestedScroll(I)Z

    move-result v0

    .line 1453
    return v0
.end method

.method public final m(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1418
    .line 2039
    invoke-virtual {p1, p2}, Landroid/view/View;->setElevation(F)V

    .line 1419
    return-void
.end method

.method public final n(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1428
    .line 2047
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationZ(F)V

    .line 1429
    return-void
.end method
