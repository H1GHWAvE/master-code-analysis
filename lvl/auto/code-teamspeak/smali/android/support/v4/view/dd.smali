.class Landroid/support/v4/view/dd;
.super Landroid/support/v4/view/dc;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1225
    invoke-direct {p0}, Landroid/support/v4/view/dc;-><init>()V

    return-void
.end method


# virtual methods
.method public final F(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1284
    .line 2074
    invoke-virtual {p1}, Landroid/view/View;->getMinimumWidth()I

    move-result v0

    .line 1284
    return v0
.end method

.method public final G(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1289
    .line 2078
    invoke-virtual {p1}, Landroid/view/View;->getMinimumHeight()I

    move-result v0

    .line 1289
    return v0
.end method

.method public M(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 1294
    .line 2082
    invoke-virtual {p1}, Landroid/view/View;->requestFitSystemWindows()V

    .line 1295
    return-void
.end method

.method public final Q(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1299
    .line 2086
    invoke-virtual {p1}, Landroid/view/View;->getFitsSystemWindows()Z

    move-result v0

    .line 1299
    return v0
.end method

.method public final a(Landroid/view/View;IIII)V
    .registers 6

    .prologue
    .line 1240
    .line 2042
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->postInvalidate(IIII)V

    .line 1241
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Runnable;)V
    .registers 3

    .prologue
    .line 1244
    .line 2046
    invoke-virtual {p1, p2}, Landroid/view/View;->postOnAnimation(Ljava/lang/Runnable;)V

    .line 1245
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Runnable;J)V
    .registers 6

    .prologue
    .line 1248
    .line 2050
    invoke-virtual {p1, p2, p3, p4}, Landroid/view/View;->postOnAnimationDelayed(Ljava/lang/Runnable;J)V

    .line 1249
    return-void
.end method

.method public final a(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 1232
    .line 2033
    invoke-virtual {p1, p2}, Landroid/view/View;->setHasTransientState(Z)V

    .line 1233
    return-void
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .registers 5

    .prologue
    .line 1266
    .line 2062
    invoke-virtual {p1, p2, p3}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    .line 1266
    return v0
.end method

.method public final c(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1228
    .line 2029
    invoke-virtual {p1}, Landroid/view/View;->hasTransientState()Z

    move-result v0

    .line 1228
    return v0
.end method

.method public final d(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 1236
    .line 2037
    invoke-virtual {p1}, Landroid/view/View;->postInvalidateOnAnimation()V

    .line 1237
    return-void
.end method

.method public d(Landroid/view/View;I)V
    .registers 4

    .prologue
    .line 1259
    const/4 v0, 0x4

    if-ne p2, v0, :cond_4

    .line 1260
    const/4 p2, 0x2

    .line 2058
    :cond_4
    invoke-virtual {p1, p2}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 1263
    return-void
.end method

.method public final e(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1252
    .line 2054
    invoke-virtual {p1}, Landroid/view/View;->getImportantForAccessibility()I

    move-result v0

    .line 1252
    return v0
.end method

.method public final g(Landroid/view/View;)Landroid/support/v4/view/a/aq;
    .registers 4

    .prologue
    .line 1270
    .line 2066
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v1

    .line 1271
    if-eqz v1, :cond_c

    .line 1272
    new-instance v0, Landroid/support/v4/view/a/aq;

    invoke-direct {v0, v1}, Landroid/support/v4/view/a/aq;-><init>(Ljava/lang/Object;)V

    .line 1274
    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final l(Landroid/view/View;)Landroid/view/ViewParent;
    .registers 3

    .prologue
    .line 1279
    .line 2070
    invoke-virtual {p1}, Landroid/view/View;->getParentForAccessibility()Landroid/view/ViewParent;

    move-result-object v0

    .line 1279
    return-object v0
.end method

.method public final v(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1304
    .line 2090
    invoke-virtual {p1}, Landroid/view/View;->hasOverlappingRendering()Z

    move-result v0

    .line 1304
    return v0
.end method
