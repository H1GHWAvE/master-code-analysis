.class Landroid/support/v4/view/a/aa;
.super Landroid/support/v4/view/a/z;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1607
    invoke-direct {p0}, Landroid/support/v4/view/a/z;-><init>()V

    return-void
.end method


# virtual methods
.method public final H(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1610
    .line 2027
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getLiveRegion()I

    move-result v0

    .line 1610
    return v0
.end method

.method public final I(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1620
    .line 2035
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getCollectionInfo()Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;

    move-result-object v0

    .line 1620
    return-object v0
.end method

.method public final J(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1659
    .line 3039
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getCollectionItemInfo()Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    move-result-object v0

    .line 1659
    return-object v0
.end method

.method public final K(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1664
    .line 3053
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getRangeInfo()Landroid/view/accessibility/AccessibilityNodeInfo$RangeInfo;

    move-result-object v0

    .line 1664
    return-object v0
.end method

.method public final L(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1644
    .line 2117
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;->getColumnCount()I

    move-result v0

    .line 1644
    return v0
.end method

.method public final M(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1649
    .line 2121
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;->getRowCount()I

    move-result v0

    .line 1649
    return v0
.end method

.method public final N(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1654
    .line 2125
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;->isHierarchical()Z

    move-result v0

    .line 1654
    return v0
.end method

.method public final O(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1674
    .line 3131
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;->getColumnIndex()I

    move-result v0

    .line 1674
    return v0
.end method

.method public final P(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1679
    .line 3135
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;->getColumnSpan()I

    move-result v0

    .line 1679
    return v0
.end method

.method public final Q(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1684
    .line 3139
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;->getRowIndex()I

    move-result v0

    .line 1684
    return v0
.end method

.method public final R(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1689
    .line 3143
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;->getRowSpan()I

    move-result v0

    .line 1689
    return v0
.end method

.method public final S(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1694
    .line 3147
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;->isHeading()Z

    move-result v0

    .line 1694
    return v0
.end method

.method public final T(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 1704
    .line 4072
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setContentInvalid(Z)V

    .line 1705
    return-void
.end method

.method public final U(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1709
    .line 4076
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isContentInvalid()Z

    move-result v0

    .line 1709
    return v0
.end method

.method public final X(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1714
    .line 4080
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->canOpenPopup()Z

    move-result v0

    .line 1714
    return v0
.end method

.method public final Y(Ljava/lang/Object;)Landroid/os/Bundle;
    .registers 3

    .prologue
    .line 1724
    .line 4088
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1724
    return-object v0
.end method

.method public final Z(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1729
    .line 4092
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getInputType()I

    move-result v0

    .line 1729
    return v0
.end method

.method public a(IIIIZZ)Ljava/lang/Object;
    .registers 8

    .prologue
    .line 1638
    .line 2067
    invoke-static {p1, p2, p3, p4, p5}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;->obtain(IIIIZ)Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    move-result-object v0

    .line 1638
    return-object v0
.end method

.method public a(IIZI)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 1631
    .line 2062
    invoke-static {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;->obtain(IIZ)Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;

    move-result-object v0

    .line 1631
    return-object v0
.end method

.method public final ac(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1739
    .line 4100
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isDismissable()Z

    move-result v0

    .line 1739
    return v0
.end method

.method public final ae(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1749
    .line 4108
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isMultiLine()Z

    move-result v0

    .line 1749
    return v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 1625
    .line 2043
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    check-cast p2, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCollectionInfo(Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;)V

    .line 1626
    return-void
.end method

.method public final d(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 1699
    .line 4048
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    check-cast p2, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCollectionItemInfo(Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;)V

    .line 1700
    return-void
.end method

.method public final e(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 1669
    .line 3057
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    check-cast p2, Landroid/view/accessibility/AccessibilityNodeInfo$RangeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setRangeInfo(Landroid/view/accessibility/AccessibilityNodeInfo$RangeInfo;)V

    .line 1670
    return-void
.end method

.method public final h(Ljava/lang/Object;I)V
    .registers 3

    .prologue
    .line 1615
    .line 2031
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLiveRegion(I)V

    .line 1616
    return-void
.end method

.method public final i(Ljava/lang/Object;I)V
    .registers 3

    .prologue
    .line 1734
    .line 4096
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setInputType(I)V

    .line 1735
    return-void
.end method

.method public final m(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1719
    .line 4084
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCanOpenPopup(Z)V

    .line 1720
    return-void
.end method

.method public final n(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1744
    .line 4104
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setDismissable(Z)V

    .line 1745
    return-void
.end method

.method public final p(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1754
    .line 4112
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setMultiLine(Z)V

    .line 1755
    return-void
.end method
