.class final Landroid/support/v4/view/ds;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
    .registers 4

    .prologue
    .line 90
    instance-of v0, p1, Landroid/support/v4/view/gi;

    if-eqz v0, :cond_14

    move-object v0, p1

    .line 92
    check-cast v0, Landroid/support/v4/view/gi;

    .line 1116
    iget-object v0, v0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    .line 94
    invoke-virtual {p0, v0}, Landroid/view/View;->onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;

    move-result-object v1

    .line 96
    if-eq v1, v0, :cond_14

    .line 98
    new-instance p1, Landroid/support/v4/view/gi;

    invoke-direct {p1, v1}, Landroid/support/v4/view/gi;-><init>(Landroid/view/WindowInsets;)V

    .line 101
    :cond_14
    return-object p1
.end method

.method private static a(Landroid/view/View;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 31
    invoke-virtual {p0}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/view/View;F)V
    .registers 2

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Landroid/view/View;->setElevation(F)V

    .line 40
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
    .registers 2

    .prologue
    .line 78
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 79
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
    .registers 2

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 87
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/support/v4/view/bx;)V
    .registers 3

    .prologue
    .line 56
    new-instance v0, Landroid/support/v4/view/dt;

    invoke-direct {v0, p1}, Landroid/support/v4/view/dt;-><init>(Landroid/support/v4/view/bx;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    .line 67
    return-void
.end method

.method private static a(Landroid/view/View;Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method private static a(Landroid/view/View;Z)V
    .registers 2

    .prologue
    .line 120
    invoke-virtual {p0, p1}, Landroid/view/View;->setNestedScrollingEnabled(Z)V

    .line 121
    return-void
.end method

.method private static a(Landroid/view/View;FF)Z
    .registers 4

    .prologue
    .line 156
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->dispatchNestedPreFling(FF)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/view/View;FFZ)Z
    .registers 5

    .prologue
    .line 152
    invoke-virtual {p0, p1, p2, p3}, Landroid/view/View;->dispatchNestedFling(FFZ)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/view/View;I)Z
    .registers 3

    .prologue
    .line 128
    invoke-virtual {p0, p1}, Landroid/view/View;->startNestedScroll(I)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/view/View;IIII[I)Z
    .registers 7

    .prologue
    .line 141
    invoke-virtual/range {p0 .. p5}, Landroid/view/View;->dispatchNestedScroll(IIII[I)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/view/View;II[I[I)Z
    .registers 6

    .prologue
    .line 147
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/view/View;->dispatchNestedPreScroll(II[I[I)Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
    .registers 4

    .prologue
    .line 105
    instance-of v0, p1, Landroid/support/v4/view/gi;

    if-eqz v0, :cond_14

    move-object v0, p1

    .line 107
    check-cast v0, Landroid/support/v4/view/gi;

    .line 2116
    iget-object v0, v0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    .line 109
    invoke-virtual {p0, v0}, Landroid/view/View;->dispatchApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;

    move-result-object v1

    .line 111
    if-eq v1, v0, :cond_14

    .line 113
    new-instance p1, Landroid/support/v4/view/gi;

    invoke-direct {p1, v1}, Landroid/support/v4/view/gi;-><init>(Landroid/view/WindowInsets;)V

    .line 116
    :cond_14
    return-object p1
.end method

.method private static b(Landroid/view/View;)V
    .registers 1

    .prologue
    .line 35
    invoke-virtual {p0}, Landroid/view/View;->requestApplyInsets()V

    .line 36
    return-void
.end method

.method private static b(Landroid/view/View;F)V
    .registers 2

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Landroid/view/View;->setTranslationZ(F)V

    .line 48
    return-void
.end method

.method private static c(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 43
    invoke-virtual {p0}, Landroid/view/View;->getElevation()F

    move-result v0

    return v0
.end method

.method private static d(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 51
    invoke-virtual {p0}, Landroid/view/View;->getTranslationZ()F

    move-result v0

    return v0
.end method

.method private static e(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 70
    invoke-virtual {p0}, Landroid/view/View;->isImportantForAccessibility()Z

    move-result v0

    return v0
.end method

.method private static f(Landroid/view/View;)Landroid/content/res/ColorStateList;
    .registers 2

    .prologue
    .line 74
    invoke-virtual {p0}, Landroid/view/View;->getBackgroundTintList()Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private static g(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;
    .registers 2

    .prologue
    .line 82
    invoke-virtual {p0}, Landroid/view/View;->getBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    return-object v0
.end method

.method private static h(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 124
    invoke-virtual {p0}, Landroid/view/View;->isNestedScrollingEnabled()Z

    move-result v0

    return v0
.end method

.method private static i(Landroid/view/View;)V
    .registers 1

    .prologue
    .line 132
    invoke-virtual {p0}, Landroid/view/View;->stopNestedScroll()V

    .line 133
    return-void
.end method

.method private static j(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 136
    invoke-virtual {p0}, Landroid/view/View;->hasNestedScrollingParent()Z

    move-result v0

    return v0
.end method

.method private static k(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 160
    invoke-virtual {p0}, Landroid/view/View;->getZ()F

    move-result v0

    return v0
.end method
