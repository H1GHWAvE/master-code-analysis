.class public final Landroid/support/v4/m/u;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/Locale;

.field private static final b:Landroid/support/v4/m/w;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 114
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 115
    const/16 v1, 0x11

    if-lt v0, v1, :cond_22

    .line 116
    new-instance v0, Landroid/support/v4/m/x;

    invoke-direct {v0, v2}, Landroid/support/v4/m/x;-><init>(B)V

    sput-object v0, Landroid/support/v4/m/u;->b:Landroid/support/v4/m/w;

    .line 146
    :goto_e
    new-instance v0, Ljava/util/Locale;

    const-string v1, ""

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Landroid/support/v4/m/u;->a:Ljava/util/Locale;

    .line 148
    const-string v0, "Arab"

    sput-object v0, Landroid/support/v4/m/u;->c:Ljava/lang/String;

    .line 149
    const-string v0, "Hebr"

    sput-object v0, Landroid/support/v4/m/u;->d:Ljava/lang/String;

    return-void

    .line 118
    :cond_22
    new-instance v0, Landroid/support/v4/m/w;

    invoke-direct {v0, v2}, Landroid/support/v4/m/w;-><init>(B)V

    sput-object v0, Landroid/support/v4/m/u;->b:Landroid/support/v4/m/w;

    goto :goto_e
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    return-void
.end method

.method public static a(Ljava/util/Locale;)I
    .registers 2
    .param p0    # Ljava/util/Locale;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 143
    sget-object v0, Landroid/support/v4/m/u;->b:Landroid/support/v4/m/w;

    invoke-virtual {v0, p0}, Landroid/support/v4/m/w;->a(Ljava/util/Locale;)I

    move-result v0

    return v0
.end method

.method static synthetic a()Ljava/lang/String;
    .registers 1

    .prologue
    .line 26
    sget-object v0, Landroid/support/v4/m/u;->c:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 129
    sget-object v0, Landroid/support/v4/m/u;->b:Landroid/support/v4/m/w;

    invoke-virtual {v0, p0}, Landroid/support/v4/m/w;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .registers 1

    .prologue
    .line 26
    sget-object v0, Landroid/support/v4/m/u;->d:Ljava/lang/String;

    return-object v0
.end method
