.class public final Landroid/support/v4/m/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Landroid/support/v4/m/l;

.field public static final b:Landroid/support/v4/m/l;

.field public static final c:Landroid/support/v4/m/l;

.field public static final d:Landroid/support/v4/m/l;

.field public static final e:Landroid/support/v4/m/l;

.field public static final f:Landroid/support/v4/m/l;

.field private static final g:I = 0x0

.field private static final h:I = 0x1

.field private static final i:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Landroid/support/v4/m/s;

    invoke-direct {v0, v1, v2, v2}, Landroid/support/v4/m/s;-><init>(Landroid/support/v4/m/q;ZB)V

    sput-object v0, Landroid/support/v4/m/m;->a:Landroid/support/v4/m/l;

    .line 39
    new-instance v0, Landroid/support/v4/m/s;

    invoke-direct {v0, v1, v3, v2}, Landroid/support/v4/m/s;-><init>(Landroid/support/v4/m/q;ZB)V

    sput-object v0, Landroid/support/v4/m/m;->b:Landroid/support/v4/m/l;

    .line 47
    new-instance v0, Landroid/support/v4/m/s;

    sget-object v1, Landroid/support/v4/m/p;->a:Landroid/support/v4/m/p;

    invoke-direct {v0, v1, v2, v2}, Landroid/support/v4/m/s;-><init>(Landroid/support/v4/m/q;ZB)V

    sput-object v0, Landroid/support/v4/m/m;->c:Landroid/support/v4/m/l;

    .line 55
    new-instance v0, Landroid/support/v4/m/s;

    sget-object v1, Landroid/support/v4/m/p;->a:Landroid/support/v4/m/p;

    invoke-direct {v0, v1, v3, v2}, Landroid/support/v4/m/s;-><init>(Landroid/support/v4/m/q;ZB)V

    sput-object v0, Landroid/support/v4/m/m;->d:Landroid/support/v4/m/l;

    .line 62
    new-instance v0, Landroid/support/v4/m/s;

    sget-object v1, Landroid/support/v4/m/o;->a:Landroid/support/v4/m/o;

    invoke-direct {v0, v1, v2, v2}, Landroid/support/v4/m/s;-><init>(Landroid/support/v4/m/q;ZB)V

    sput-object v0, Landroid/support/v4/m/m;->e:Landroid/support/v4/m/l;

    .line 68
    sget-object v0, Landroid/support/v4/m/t;->a:Landroid/support/v4/m/t;

    sput-object v0, Landroid/support/v4/m/m;->f:Landroid/support/v4/m/l;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    return-void
.end method

.method static synthetic a(I)I
    .registers 2

    .prologue
    .line 28
    .line 1091
    sparse-switch p0, :sswitch_data_a

    .line 1102
    const/4 v0, 0x2

    .line 1100
    :goto_4
    return v0

    .line 1095
    :sswitch_5
    const/4 v0, 0x1

    goto :goto_4

    .line 1100
    :sswitch_7
    const/4 v0, 0x0

    goto :goto_4

    .line 1091
    nop

    :sswitch_data_a
    .sparse-switch
        0x0 -> :sswitch_5
        0x1 -> :sswitch_7
        0x2 -> :sswitch_7
        0xe -> :sswitch_5
        0xf -> :sswitch_5
        0x10 -> :sswitch_7
        0x11 -> :sswitch_7
    .end sparse-switch
.end method

.method static synthetic b(I)I
    .registers 2

    .prologue
    .line 28
    .line 2079
    packed-switch p0, :pswitch_data_a

    .line 2086
    const/4 v0, 0x2

    .line 2084
    :goto_4
    return v0

    .line 2081
    :pswitch_5
    const/4 v0, 0x1

    goto :goto_4

    .line 2084
    :pswitch_7
    const/4 v0, 0x0

    goto :goto_4

    .line 2079
    nop

    :pswitch_data_a
    .packed-switch 0x0
        :pswitch_5
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method private static c(I)I
    .registers 2

    .prologue
    .line 79
    packed-switch p0, :pswitch_data_a

    .line 86
    const/4 v0, 0x2

    :goto_4
    return v0

    .line 81
    :pswitch_5
    const/4 v0, 0x1

    goto :goto_4

    .line 84
    :pswitch_7
    const/4 v0, 0x0

    goto :goto_4

    .line 79
    nop

    :pswitch_data_a
    .packed-switch 0x0
        :pswitch_5
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method private static d(I)I
    .registers 2

    .prologue
    .line 91
    sparse-switch p0, :sswitch_data_a

    .line 102
    const/4 v0, 0x2

    :goto_4
    return v0

    .line 95
    :sswitch_5
    const/4 v0, 0x1

    goto :goto_4

    .line 100
    :sswitch_7
    const/4 v0, 0x0

    goto :goto_4

    .line 91
    nop

    :sswitch_data_a
    .sparse-switch
        0x0 -> :sswitch_5
        0x1 -> :sswitch_7
        0x2 -> :sswitch_7
        0xe -> :sswitch_5
        0xf -> :sswitch_5
        0x10 -> :sswitch_7
        0x11 -> :sswitch_7
    .end sparse-switch
.end method
