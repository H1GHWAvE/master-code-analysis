.class public final Landroid/support/v4/m/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Landroid/support/v4/m/l; = null

.field private static final b:C = '\u202a'

.field private static final c:C = '\u202b'

.field private static final d:C = '\u202c'

.field private static final e:C = '\u200e'

.field private static final f:C = '\u200f'

.field private static final g:Ljava/lang/String;

.field private static final h:Ljava/lang/String;

.field private static final i:Ljava/lang/String; = ""

.field private static final j:I = 0x2

.field private static final k:I = 0x2

.field private static final l:Landroid/support/v4/m/a;

.field private static final m:Landroid/support/v4/m/a;

.field private static final q:I = -0x1

.field private static final r:I = 0x0

.field private static final s:I = 0x1


# instance fields
.field private final n:Z

.field private final o:I

.field private final p:Landroid/support/v4/m/l;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x2

    .line 83
    sget-object v0, Landroid/support/v4/m/m;->c:Landroid/support/v4/m/l;

    sput-object v0, Landroid/support/v4/m/a;->a:Landroid/support/v4/m/l;

    .line 113
    const/16 v0, 0x200e

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v4/m/a;->g:Ljava/lang/String;

    .line 118
    const/16 v0, 0x200f

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v4/m/a;->h:Ljava/lang/String;

    .line 215
    new-instance v0, Landroid/support/v4/m/a;

    const/4 v1, 0x0

    sget-object v2, Landroid/support/v4/m/a;->a:Landroid/support/v4/m/l;

    invoke-direct {v0, v1, v3, v2}, Landroid/support/v4/m/a;-><init>(ZILandroid/support/v4/m/l;)V

    sput-object v0, Landroid/support/v4/m/a;->l:Landroid/support/v4/m/a;

    .line 220
    new-instance v0, Landroid/support/v4/m/a;

    const/4 v1, 0x1

    sget-object v2, Landroid/support/v4/m/a;->a:Landroid/support/v4/m/l;

    invoke-direct {v0, v1, v3, v2}, Landroid/support/v4/m/a;-><init>(ZILandroid/support/v4/m/l;)V

    sput-object v0, Landroid/support/v4/m/a;->m:Landroid/support/v4/m/a;

    return-void
.end method

.method private constructor <init>(ZILandroid/support/v4/m/l;)V
    .registers 4

    .prologue
    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    iput-boolean p1, p0, Landroid/support/v4/m/a;->n:Z

    .line 262
    iput p2, p0, Landroid/support/v4/m/a;->o:I

    .line 263
    iput-object p3, p0, Landroid/support/v4/m/a;->p:Landroid/support/v4/m/l;

    .line 264
    return-void
.end method

.method synthetic constructor <init>(ZILandroid/support/v4/m/l;B)V
    .registers 5

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/m/a;-><init>(ZILandroid/support/v4/m/l;)V

    return-void
.end method

.method private static a(Z)Landroid/support/v4/m/a;
    .registers 2

    .prologue
    .line 243
    new-instance v0, Landroid/support/v4/m/c;

    invoke-direct {v0, p0}, Landroid/support/v4/m/c;-><init>(Z)V

    invoke-virtual {v0}, Landroid/support/v4/m/c;->a()Landroid/support/v4/m/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Landroid/support/v4/m/l;
    .registers 1

    .prologue
    .line 78
    sget-object v0, Landroid/support/v4/m/a;->a:Landroid/support/v4/m/l;

    return-object v0
.end method

.method private a(Ljava/lang/String;Landroid/support/v4/m/l;)Ljava/lang/String;
    .registers 6

    .prologue
    .line 297
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-interface {p2, p1, v0, v1}, Landroid/support/v4/m/l;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    .line 299
    iget-boolean v1, p0, Landroid/support/v4/m/a;->n:Z

    if-nez v1, :cond_19

    if-nez v0, :cond_16

    invoke-static {p1}, Landroid/support/v4/m/a;->c(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_19

    .line 300
    :cond_16
    sget-object v0, Landroid/support/v4/m/a;->g:Ljava/lang/String;

    .line 305
    :goto_18
    return-object v0

    .line 302
    :cond_19
    iget-boolean v1, p0, Landroid/support/v4/m/a;->n:Z

    if-eqz v1, :cond_29

    if-eqz v0, :cond_26

    invoke-static {p1}, Landroid/support/v4/m/a;->c(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_29

    .line 303
    :cond_26
    sget-object v0, Landroid/support/v4/m/a;->h:Ljava/lang/String;

    goto :goto_18

    .line 305
    :cond_29
    const-string v0, ""

    goto :goto_18
.end method

.method private a(Ljava/lang/String;Landroid/support/v4/m/l;Z)Ljava/lang/String;
    .registers 11

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 376
    if-nez p1, :cond_7

    const/4 v0, 0x0

    .line 394
    :goto_6
    return-object v0

    .line 377
    :cond_7
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-interface {p2, p1, v2, v0}, Landroid/support/v4/m/l;->a(Ljava/lang/CharSequence;II)Z

    move-result v3

    .line 378
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1278
    iget v0, p0, Landroid/support/v4/m/a;->o:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_73

    move v0, v1

    .line 379
    :goto_1b
    if-eqz v0, :cond_3c

    if-eqz p3, :cond_3c

    .line 380
    if-eqz v3, :cond_75

    sget-object v0, Landroid/support/v4/m/m;->b:Landroid/support/v4/m/l;

    .line 1324
    :goto_23
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-interface {v0, p1, v2, v5}, Landroid/support/v4/m/l;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    .line 1326
    iget-boolean v5, p0, Landroid/support/v4/m/a;->n:Z

    if-nez v5, :cond_78

    if-nez v0, :cond_37

    invoke-static {p1}, Landroid/support/v4/m/a;->d(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v1, :cond_78

    .line 1327
    :cond_37
    sget-object v0, Landroid/support/v4/m/a;->g:Ljava/lang/String;

    .line 380
    :goto_39
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    :cond_3c
    iget-boolean v0, p0, Landroid/support/v4/m/a;->n:Z

    if-eq v3, v0, :cond_8d

    .line 384
    if-eqz v3, :cond_8a

    const/16 v0, 0x202b

    :goto_44
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 385
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    const/16 v0, 0x202c

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 390
    :goto_4f
    if-eqz p3, :cond_6e

    .line 391
    if-eqz v3, :cond_91

    sget-object v0, Landroid/support/v4/m/m;->b:Landroid/support/v4/m/l;

    .line 2297
    :goto_55
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-interface {v0, p1, v2, v3}, Landroid/support/v4/m/l;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    .line 2299
    iget-boolean v2, p0, Landroid/support/v4/m/a;->n:Z

    if-nez v2, :cond_94

    if-nez v0, :cond_69

    invoke-static {p1}, Landroid/support/v4/m/a;->c(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v1, :cond_94

    .line 2300
    :cond_69
    sget-object v0, Landroid/support/v4/m/a;->g:Ljava/lang/String;

    .line 391
    :goto_6b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    :cond_6e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_73
    move v0, v2

    .line 1278
    goto :goto_1b

    .line 380
    :cond_75
    sget-object v0, Landroid/support/v4/m/m;->a:Landroid/support/v4/m/l;

    goto :goto_23

    .line 1329
    :cond_78
    iget-boolean v5, p0, Landroid/support/v4/m/a;->n:Z

    if-eqz v5, :cond_87

    if-eqz v0, :cond_84

    invoke-static {p1}, Landroid/support/v4/m/a;->d(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v6, :cond_87

    .line 1330
    :cond_84
    sget-object v0, Landroid/support/v4/m/a;->h:Ljava/lang/String;

    goto :goto_39

    .line 1332
    :cond_87
    const-string v0, ""

    goto :goto_39

    .line 384
    :cond_8a
    const/16 v0, 0x202a

    goto :goto_44

    .line 388
    :cond_8d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4f

    .line 391
    :cond_91
    sget-object v0, Landroid/support/v4/m/m;->a:Landroid/support/v4/m/l;

    goto :goto_55

    .line 2302
    :cond_94
    iget-boolean v1, p0, Landroid/support/v4/m/a;->n:Z

    if-eqz v1, :cond_a3

    if-eqz v0, :cond_a0

    invoke-static {p1}, Landroid/support/v4/m/a;->c(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v6, :cond_a3

    .line 2303
    :cond_a0
    sget-object v0, Landroid/support/v4/m/a;->h:Ljava/lang/String;

    goto :goto_6b

    .line 2305
    :cond_a3
    const-string v0, ""

    goto :goto_6b
.end method

.method private a(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 4

    .prologue
    .line 419
    iget-object v0, p0, Landroid/support/v4/m/a;->p:Landroid/support/v4/m/l;

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v4/m/a;->a(Ljava/lang/String;Landroid/support/v4/m/l;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Z
    .registers 5

    .prologue
    .line 343
    iget-object v0, p0, Landroid/support/v4/m/a;->p:Landroid/support/v4/m/l;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, p1, v1, v2}, Landroid/support/v4/m/l;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Ljava/util/Locale;)Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 78
    .line 4440
    invoke-static {p0}, Landroid/support/v4/m/u;->a(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    .line 78
    goto :goto_7
.end method

.method static synthetic b()Landroid/support/v4/m/a;
    .registers 1

    .prologue
    .line 78
    sget-object v0, Landroid/support/v4/m/a;->m:Landroid/support/v4/m/a;

    return-object v0
.end method

.method private static b(Ljava/util/Locale;)Landroid/support/v4/m/a;
    .registers 2

    .prologue
    .line 252
    new-instance v0, Landroid/support/v4/m/c;

    invoke-direct {v0, p0}, Landroid/support/v4/m/c;-><init>(Ljava/util/Locale;)V

    invoke-virtual {v0}, Landroid/support/v4/m/c;->a()Landroid/support/v4/m/a;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 430
    iget-object v0, p0, Landroid/support/v4/m/a;->p:Landroid/support/v4/m/l;

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/m/a;->a(Ljava/lang/String;Landroid/support/v4/m/l;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;Landroid/support/v4/m/l;)Ljava/lang/String;
    .registers 6

    .prologue
    .line 324
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-interface {p2, p1, v0, v1}, Landroid/support/v4/m/l;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    .line 326
    iget-boolean v1, p0, Landroid/support/v4/m/a;->n:Z

    if-nez v1, :cond_19

    if-nez v0, :cond_16

    invoke-static {p1}, Landroid/support/v4/m/a;->d(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_19

    .line 327
    :cond_16
    sget-object v0, Landroid/support/v4/m/a;->g:Ljava/lang/String;

    .line 332
    :goto_18
    return-object v0

    .line 329
    :cond_19
    iget-boolean v1, p0, Landroid/support/v4/m/a;->n:Z

    if-eqz v1, :cond_29

    if-eqz v0, :cond_26

    invoke-static {p1}, Landroid/support/v4/m/a;->d(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_29

    .line 330
    :cond_26
    sget-object v0, Landroid/support/v4/m/a;->h:Ljava/lang/String;

    goto :goto_18

    .line 332
    :cond_29
    const-string v0, ""

    goto :goto_18
.end method

.method private static c(Ljava/lang/String;)I
    .registers 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 465
    new-instance v5, Landroid/support/v4/m/d;

    invoke-direct {v5, p0}, Landroid/support/v4/m/d;-><init>(Ljava/lang/String;)V

    .line 2668
    iget v0, v5, Landroid/support/v4/m/d;->c:I

    iput v0, v5, Landroid/support/v4/m/d;->d:I

    move v0, v1

    move v2, v1

    .line 2671
    :cond_e
    :goto_e
    :pswitch_e
    iget v6, v5, Landroid/support/v4/m/d;->d:I

    if-lez v6, :cond_20

    .line 2672
    invoke-virtual {v5}, Landroid/support/v4/m/d;->a()B

    move-result v6

    packed-switch v6, :pswitch_data_3e

    .line 2710
    :pswitch_19
    if-nez v0, :cond_e

    move v0, v2

    .line 2711
    goto :goto_e

    .line 2674
    :pswitch_1d
    if-nez v2, :cond_21

    move v1, v3

    .line 2700
    :cond_20
    :goto_20
    return v1

    .line 2677
    :cond_21
    if-nez v0, :cond_e

    move v0, v2

    .line 2678
    goto :goto_e

    .line 2683
    :pswitch_25
    if-ne v0, v2, :cond_29

    move v1, v3

    .line 2684
    goto :goto_20

    .line 2686
    :cond_29
    add-int/lit8 v2, v2, -0x1

    .line 2687
    goto :goto_e

    .line 2690
    :pswitch_2c
    if-nez v2, :cond_30

    move v1, v4

    .line 2691
    goto :goto_20

    .line 2693
    :cond_30
    if-nez v0, :cond_e

    move v0, v2

    .line 2694
    goto :goto_e

    .line 2699
    :pswitch_34
    if-ne v0, v2, :cond_38

    move v1, v4

    .line 2700
    goto :goto_20

    .line 2702
    :cond_38
    add-int/lit8 v2, v2, -0x1

    .line 2703
    goto :goto_e

    .line 2705
    :pswitch_3b
    add-int/lit8 v2, v2, 0x1

    .line 2706
    goto :goto_e

    .line 2672
    :pswitch_data_3e
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_2c
        :pswitch_2c
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_e
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_25
        :pswitch_25
        :pswitch_34
        :pswitch_34
        :pswitch_3b
    .end packed-switch
.end method

.method static synthetic c()Landroid/support/v4/m/a;
    .registers 1

    .prologue
    .line 78
    sget-object v0, Landroid/support/v4/m/a;->l:Landroid/support/v4/m/a;

    return-object v0
.end method

.method private c(Ljava/lang/String;Landroid/support/v4/m/l;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 406
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/m/a;->a(Ljava/lang/String;Landroid/support/v4/m/l;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/util/Locale;)Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 440
    invoke-static {p0}, Landroid/support/v4/m/u;->a(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static d(Ljava/lang/String;)I
    .registers 15

    .prologue
    const/16 v13, 0x3c

    const/16 v7, 0xc

    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 482
    new-instance v8, Landroid/support/v4/m/d;

    invoke-direct {v8, p0}, Landroid/support/v4/m/d;-><init>(Ljava/lang/String;)V

    .line 3570
    iput v1, v8, Landroid/support/v4/m/d;->d:I

    move v0, v1

    move v3, v1

    move v2, v1

    .line 3574
    :goto_11
    :pswitch_11
    iget v6, v8, Landroid/support/v4/m/d;->d:I

    iget v9, v8, Landroid/support/v4/m/d;->c:I

    if-ge v6, v9, :cond_e0

    if-nez v0, :cond_e0

    .line 3740
    iget-object v6, v8, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v9, v8, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v6, v9}, Ljava/lang/String;->charAt(I)C

    move-result v6

    iput-char v6, v8, Landroid/support/v4/m/d;->e:C

    .line 3741
    iget-char v6, v8, Landroid/support/v4/m/d;->e:C

    invoke-static {v6}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_45

    .line 3742
    iget-object v6, v8, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v9, v8, Landroid/support/v4/m/d;->d:I

    invoke-static {v6, v9}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v6

    .line 3743
    iget v9, v8, Landroid/support/v4/m/d;->d:I

    invoke-static {v6}, Ljava/lang/Character;->charCount(I)I

    move-result v10

    add-int/2addr v9, v10

    iput v9, v8, Landroid/support/v4/m/d;->d:I

    .line 3744
    invoke-static {v6}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v6

    .line 3575
    :cond_40
    :goto_40
    packed-switch v6, :pswitch_data_104

    :pswitch_43
    move v0, v2

    .line 3610
    goto :goto_11

    .line 3746
    :cond_45
    iget v6, v8, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v8, Landroid/support/v4/m/d;->d:I

    .line 3747
    iget-char v6, v8, Landroid/support/v4/m/d;->e:C

    invoke-static {v6}, Landroid/support/v4/m/d;->a(C)B

    move-result v6

    .line 3748
    iget-boolean v9, v8, Landroid/support/v4/m/d;->b:Z

    if-eqz v9, :cond_40

    .line 3750
    iget-char v9, v8, Landroid/support/v4/m/d;->e:C

    if-ne v9, v13, :cond_a3

    .line 3796
    iget v6, v8, Landroid/support/v4/m/d;->d:I

    .line 3797
    :cond_5b
    :goto_5b
    iget v9, v8, Landroid/support/v4/m/d;->d:I

    iget v10, v8, Landroid/support/v4/m/d;->c:I

    if-ge v9, v10, :cond_9c

    .line 3798
    iget-object v9, v8, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v10, v8, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v11, v10, 0x1

    iput v11, v8, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    iput-char v9, v8, Landroid/support/v4/m/d;->e:C

    .line 3799
    iget-char v9, v8, Landroid/support/v4/m/d;->e:C

    const/16 v10, 0x3e

    if-ne v9, v10, :cond_77

    move v6, v7

    .line 3801
    goto :goto_40

    .line 3803
    :cond_77
    iget-char v9, v8, Landroid/support/v4/m/d;->e:C

    const/16 v10, 0x22

    if-eq v9, v10, :cond_83

    iget-char v9, v8, Landroid/support/v4/m/d;->e:C

    const/16 v10, 0x27

    if-ne v9, v10, :cond_5b

    .line 3805
    :cond_83
    iget-char v9, v8, Landroid/support/v4/m/d;->e:C

    .line 3806
    :cond_85
    iget v10, v8, Landroid/support/v4/m/d;->d:I

    iget v11, v8, Landroid/support/v4/m/d;->c:I

    if-ge v10, v11, :cond_5b

    iget-object v10, v8, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v11, v8, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v12, v11, 0x1

    iput v12, v8, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v10

    iput-char v10, v8, Landroid/support/v4/m/d;->e:C

    if-ne v10, v9, :cond_85

    goto :goto_5b

    .line 3810
    :cond_9c
    iput v6, v8, Landroid/support/v4/m/d;->d:I

    .line 3811
    iput-char v13, v8, Landroid/support/v4/m/d;->e:C

    .line 3812
    const/16 v6, 0xd

    goto :goto_40

    .line 3752
    :cond_a3
    iget-char v9, v8, Landroid/support/v4/m/d;->e:C

    const/16 v10, 0x26

    if-ne v9, v10, :cond_40

    .line 3853
    :cond_a9
    iget v6, v8, Landroid/support/v4/m/d;->d:I

    iget v9, v8, Landroid/support/v4/m/d;->c:I

    if-ge v6, v9, :cond_c1

    iget-object v6, v8, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v9, v8, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v10, v9, 0x1

    iput v10, v8, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v6, v9}, Ljava/lang/String;->charAt(I)C

    move-result v6

    iput-char v6, v8, Landroid/support/v4/m/d;->e:C

    const/16 v9, 0x3b

    if-ne v6, v9, :cond_a9

    :cond_c1
    move v6, v7

    .line 3753
    goto/16 :goto_40

    .line 3578
    :pswitch_c4
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    .line 3580
    goto/16 :goto_11

    .line 3583
    :pswitch_c9
    add-int/lit8 v2, v2, 0x1

    move v3, v5

    .line 3585
    goto/16 :goto_11

    .line 3587
    :pswitch_ce
    add-int/lit8 v2, v2, -0x1

    move v3, v1

    .line 3592
    goto/16 :goto_11

    .line 3596
    :pswitch_d3
    if-nez v2, :cond_d6

    .line 3642
    :cond_d5
    :goto_d5
    return v4

    :cond_d6
    move v0, v2

    .line 3600
    goto/16 :goto_11

    .line 3603
    :pswitch_d9
    if-nez v2, :cond_dd

    move v4, v5

    .line 3604
    goto :goto_d5

    :cond_dd
    move v0, v2

    .line 3607
    goto/16 :goto_11

    .line 3616
    :cond_e0
    if-eqz v0, :cond_101

    .line 3623
    if-eqz v3, :cond_e6

    move v4, v3

    .line 3625
    goto :goto_d5

    .line 3630
    :cond_e6
    :goto_e6
    iget v3, v8, Landroid/support/v4/m/d;->d:I

    if-lez v3, :cond_101

    .line 3631
    invoke-virtual {v8}, Landroid/support/v4/m/d;->a()B

    move-result v3

    packed-switch v3, :pswitch_data_12e

    goto :goto_e6

    .line 3634
    :pswitch_f2
    if-eq v0, v2, :cond_d5

    .line 3637
    add-int/lit8 v2, v2, -0x1

    .line 3638
    goto :goto_e6

    .line 3641
    :pswitch_f7
    if-ne v0, v2, :cond_fb

    move v4, v5

    .line 3642
    goto :goto_d5

    .line 3644
    :cond_fb
    add-int/lit8 v2, v2, -0x1

    .line 3645
    goto :goto_e6

    .line 3647
    :pswitch_fe
    add-int/lit8 v2, v2, 0x1

    goto :goto_e6

    :cond_101
    move v4, v1

    .line 482
    goto :goto_d5

    .line 3575
    nop

    :pswitch_data_104
    .packed-switch 0x0
        :pswitch_d3
        :pswitch_d9
        :pswitch_d9
        :pswitch_43
        :pswitch_43
        :pswitch_43
        :pswitch_43
        :pswitch_43
        :pswitch_43
        :pswitch_11
        :pswitch_43
        :pswitch_43
        :pswitch_43
        :pswitch_43
        :pswitch_c4
        :pswitch_c4
        :pswitch_c9
        :pswitch_c9
        :pswitch_ce
    .end packed-switch

    .line 3631
    :pswitch_data_12e
    .packed-switch 0xe
        :pswitch_f2
        :pswitch_f2
        :pswitch_f7
        :pswitch_f7
        :pswitch_fe
    .end packed-switch
.end method

.method private static d()Landroid/support/v4/m/a;
    .registers 1

    .prologue
    .line 234
    new-instance v0, Landroid/support/v4/m/c;

    invoke-direct {v0}, Landroid/support/v4/m/c;-><init>()V

    invoke-virtual {v0}, Landroid/support/v4/m/c;->a()Landroid/support/v4/m/a;

    move-result-object v0

    return-object v0
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 270
    iget-boolean v0, p0, Landroid/support/v4/m/a;->n:Z

    return v0
.end method

.method private f()Z
    .registers 2

    .prologue
    .line 278
    iget v0, p0, Landroid/support/v4/m/a;->o:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method
