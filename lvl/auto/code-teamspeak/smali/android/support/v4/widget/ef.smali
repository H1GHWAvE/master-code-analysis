.class public interface abstract Landroid/support/v4/widget/ef;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getSupportButtonTintList()Landroid/content/res/ColorStateList;
    .annotation build Landroid/support/a/z;
    .end annotation
.end method

.method public abstract getSupportButtonTintMode()Landroid/graphics/PorterDuff$Mode;
    .annotation build Landroid/support/a/z;
    .end annotation
.end method

.method public abstract setSupportButtonTintList(Landroid/content/res/ColorStateList;)V
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
.end method

.method public abstract setSupportButtonTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .param p1    # Landroid/graphics/PorterDuff$Mode;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
.end method
