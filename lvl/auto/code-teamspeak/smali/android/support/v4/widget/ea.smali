.class Landroid/support/v4/widget/ea;
.super Landroid/support/v4/widget/dz;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/support/v4/widget/dz;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/TextView;IIII)V
    .registers 8
    .param p1    # Landroid/widget/TextView;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 94
    .line 1044
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_11

    move v1, v0

    .line 1045
    :goto_8
    if-eqz v1, :cond_14

    move v0, p4

    :goto_b
    if-eqz v1, :cond_16

    :goto_d
    invoke-virtual {p1, v0, p3, p2, p5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 96
    return-void

    .line 1044
    :cond_11
    const/4 v0, 0x0

    move v1, v0

    goto :goto_8

    :cond_14
    move v0, p2

    .line 1045
    goto :goto_b

    :cond_16
    move p2, p4

    goto :goto_d
.end method

.method public a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;)V
    .registers 6
    .param p1    # Landroid/widget/TextView;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 80
    .line 1030
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayoutDirection()I

    move-result v2

    if-ne v2, v1, :cond_12

    move v2, v1

    .line 1031
    :goto_9
    if-eqz v2, :cond_15

    move-object v1, v0

    :goto_c
    if-eqz v2, :cond_17

    :goto_e
    invoke-virtual {p1, v1, v0, p2, v0}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 81
    return-void

    .line 1030
    :cond_12
    const/4 v1, 0x0

    move v2, v1

    goto :goto_9

    :cond_15
    move-object v1, p2

    .line 1031
    goto :goto_c

    :cond_17
    move-object p2, v0

    goto :goto_e
.end method

.method public a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 8
    .param p1    # Landroid/widget/TextView;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .param p5    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 87
    .line 1037
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_11

    move v1, v0

    .line 1038
    :goto_8
    if-eqz v1, :cond_14

    move-object v0, p4

    :goto_b
    if-eqz v1, :cond_16

    :goto_d
    invoke-virtual {p1, v0, p3, p2, p5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 89
    return-void

    .line 1037
    :cond_11
    const/4 v0, 0x0

    move v1, v0

    goto :goto_8

    :cond_14
    move-object v0, p2

    .line 1038
    goto :goto_b

    :cond_16
    move-object p2, p4

    goto :goto_d
.end method
