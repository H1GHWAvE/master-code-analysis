.class public final Landroid/support/v4/widget/o;
.super Landroid/widget/ProgressBar;
.source "SourceFile"


# static fields
.field private static final a:I = 0x1f4

.field private static final b:I = 0x1f4


# instance fields
.field private c:J

.field private d:Z

.field private e:Z

.field private f:Z

.field private final g:Ljava/lang/Runnable;

.field private final h:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/o;-><init>(Landroid/content/Context;B)V

    .line 66
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v4/widget/o;->c:J

    .line 36
    iput-boolean v2, p0, Landroid/support/v4/widget/o;->d:Z

    .line 38
    iput-boolean v2, p0, Landroid/support/v4/widget/o;->e:Z

    .line 40
    iput-boolean v2, p0, Landroid/support/v4/widget/o;->f:Z

    .line 42
    new-instance v0, Landroid/support/v4/widget/p;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/p;-><init>(Landroid/support/v4/widget/o;)V

    iput-object v0, p0, Landroid/support/v4/widget/o;->g:Ljava/lang/Runnable;

    .line 52
    new-instance v0, Landroid/support/v4/widget/q;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/q;-><init>(Landroid/support/v4/widget/o;)V

    iput-object v0, p0, Landroid/support/v4/widget/o;->h:Ljava/lang/Runnable;

    .line 70
    return-void
.end method

.method static synthetic a(Landroid/support/v4/widget/o;J)J
    .registers 4

    .prologue
    .line 30
    iput-wide p1, p0, Landroid/support/v4/widget/o;->c:J

    return-wide p1
.end method

.method private a()V
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, Landroid/support/v4/widget/o;->g:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/o;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 86
    iget-object v0, p0, Landroid/support/v4/widget/o;->h:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/o;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 87
    return-void
.end method

.method static synthetic a(Landroid/support/v4/widget/o;)Z
    .registers 2

    .prologue
    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/o;->d:Z

    return v0
.end method

.method private b()V
    .registers 11

    .prologue
    const-wide/16 v8, 0x1f4

    const/4 v6, 0x1

    .line 95
    iput-boolean v6, p0, Landroid/support/v4/widget/o;->f:Z

    .line 96
    iget-object v0, p0, Landroid/support/v4/widget/o;->h:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/o;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 97
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/support/v4/widget/o;->c:J

    sub-long/2addr v0, v2

    .line 98
    cmp-long v2, v0, v8

    if-gez v2, :cond_1d

    iget-wide v2, p0, Landroid/support/v4/widget/o;->c:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_23

    .line 102
    :cond_1d
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/o;->setVisibility(I)V

    .line 112
    :cond_22
    :goto_22
    return-void

    .line 107
    :cond_23
    iget-boolean v2, p0, Landroid/support/v4/widget/o;->d:Z

    if-nez v2, :cond_22

    .line 108
    iget-object v2, p0, Landroid/support/v4/widget/o;->g:Ljava/lang/Runnable;

    sub-long v0, v8, v0

    invoke-virtual {p0, v2, v0, v1}, Landroid/support/v4/widget/o;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 109
    iput-boolean v6, p0, Landroid/support/v4/widget/o;->d:Z

    goto :goto_22
.end method

.method static synthetic b(Landroid/support/v4/widget/o;)Z
    .registers 2

    .prologue
    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/o;->e:Z

    return v0
.end method

.method private c()V
    .registers 5

    .prologue
    .line 120
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v4/widget/o;->c:J

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/o;->f:Z

    .line 122
    iget-object v0, p0, Landroid/support/v4/widget/o;->g:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/o;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 123
    iget-boolean v0, p0, Landroid/support/v4/widget/o;->e:Z

    if-nez v0, :cond_1a

    .line 124
    iget-object v0, p0, Landroid/support/v4/widget/o;->h:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Landroid/support/v4/widget/o;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/o;->e:Z

    .line 127
    :cond_1a
    return-void
.end method

.method static synthetic c(Landroid/support/v4/widget/o;)Z
    .registers 2

    .prologue
    .line 30
    iget-boolean v0, p0, Landroid/support/v4/widget/o;->f:Z

    return v0
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .registers 1

    .prologue
    .line 74
    invoke-super {p0}, Landroid/widget/ProgressBar;->onAttachedToWindow()V

    .line 75
    invoke-direct {p0}, Landroid/support/v4/widget/o;->a()V

    .line 76
    return-void
.end method

.method public final onDetachedFromWindow()V
    .registers 1

    .prologue
    .line 80
    invoke-super {p0}, Landroid/widget/ProgressBar;->onDetachedFromWindow()V

    .line 81
    invoke-direct {p0}, Landroid/support/v4/widget/o;->a()V

    .line 82
    return-void
.end method
