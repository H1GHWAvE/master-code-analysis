.class final Landroid/support/v4/widget/do;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Landroid/support/v4/widget/dn;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/dn;)V
    .registers 2

    .prologue
    .line 165
    iput-object p1, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .registers 5

    .prologue
    .line 176
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->a(Landroid/support/v4/widget/dn;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 178
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->b(Landroid/support/v4/widget/dn;)Landroid/support/v4/widget/bb;

    move-result-object v0

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/bb;->setAlpha(I)V

    .line 179
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->b(Landroid/support/v4/widget/dn;)Landroid/support/v4/widget/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/bb;->start()V

    .line 180
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->c(Landroid/support/v4/widget/dn;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 181
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->d(Landroid/support/v4/widget/dn;)Landroid/support/v4/widget/dx;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 182
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->d(Landroid/support/v4/widget/dn;)Landroid/support/v4/widget/dx;

    .line 197
    :cond_31
    :goto_31
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    iget-object v1, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-static {v1}, Landroid/support/v4/widget/dn;->e(Landroid/support/v4/widget/dn;)Landroid/support/v4/widget/e;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/widget/e;->getTop()I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v4/widget/dn;->a(Landroid/support/v4/widget/dn;I)I

    .line 198
    return-void

    .line 186
    :cond_41
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->b(Landroid/support/v4/widget/dn;)Landroid/support/v4/widget/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/bb;->stop()V

    .line 187
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->e(Landroid/support/v4/widget/dn;)Landroid/support/v4/widget/e;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/e;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->f(Landroid/support/v4/widget/dn;)V

    .line 190
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->g(Landroid/support/v4/widget/dn;)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 191
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/widget/dn;->a(Landroid/support/v4/widget/dn;F)V

    goto :goto_31

    .line 193
    :cond_69
    iget-object v0, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    iget-object v1, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    iget v1, v1, Landroid/support/v4/widget/dn;->d:I

    iget-object v2, p0, Landroid/support/v4/widget/do;->a:Landroid/support/v4/widget/dn;

    invoke-static {v2}, Landroid/support/v4/widget/dn;->h(Landroid/support/v4/widget/dn;)I

    move-result v2

    sub-int/2addr v1, v2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/support/v4/widget/dn;->a(Landroid/support/v4/widget/dn;IZ)V

    goto :goto_31
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .registers 2

    .prologue
    .line 172
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .registers 2

    .prologue
    .line 168
    return-void
.end method
