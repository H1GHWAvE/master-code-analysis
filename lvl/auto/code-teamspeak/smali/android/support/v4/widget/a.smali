.class public abstract Landroid/support/v4/widget/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final A:I = 0x13b

.field private static final B:I = 0x627

.field private static final C:F = 3.4028235E38f

.field private static final D:F = 0.2f

.field private static final E:F = 1.0f

.field private static final F:I

.field private static final G:I = 0x1f4

.field private static final H:I = 0x1f4

.field public static final a:F = 0.0f

.field public static final b:F = 3.4028235E38f

.field public static final c:F = 0.0f

.field public static final d:I = 0x0

.field public static final e:I = 0x1

.field public static final f:I = 0x2

.field private static final g:I = 0x0

.field private static final h:I = 0x1

.field private static final z:I = 0x1


# instance fields
.field private final i:Landroid/support/v4/widget/c;

.field private final j:Landroid/view/animation/Interpolator;

.field private final k:Landroid/view/View;

.field private l:Ljava/lang/Runnable;

.field private m:[F

.field private n:[F

.field private o:I

.field private p:I

.field private q:[F

.field private r:[F

.field private s:[F

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 194
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Landroid/support/v4/widget/a;->F:I

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .registers 10

    .prologue
    const v7, 0x3a83126f    # 0.001f

    const/high16 v6, 0x447a0000    # 1000.0f

    const/4 v1, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    new-instance v0, Landroid/support/v4/widget/c;

    invoke-direct {v0}, Landroid/support/v4/widget/c;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/a;->i:Landroid/support/v4/widget/c;

    .line 140
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/a;->j:Landroid/view/animation/Interpolator;

    .line 149
    new-array v0, v1, [F

    fill-array-data v0, :array_aa

    iput-object v0, p0, Landroid/support/v4/widget/a;->m:[F

    .line 152
    new-array v0, v1, [F

    fill-array-data v0, :array_b2

    iput-object v0, p0, Landroid/support/v4/widget/a;->n:[F

    .line 161
    new-array v0, v1, [F

    fill-array-data v0, :array_ba

    iput-object v0, p0, Landroid/support/v4/widget/a;->q:[F

    .line 164
    new-array v0, v1, [F

    fill-array-data v0, :array_c2

    iput-object v0, p0, Landroid/support/v4/widget/a;->r:[F

    .line 167
    new-array v0, v1, [F

    fill-array-data v0, :array_ca

    iput-object v0, p0, Landroid/support/v4/widget/a;->s:[F

    .line 210
    iput-object p1, p0, Landroid/support/v4/widget/a;->k:Landroid/view/View;

    .line 212
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 213
    const v1, 0x44c4e000    # 1575.0f

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 214
    const v2, 0x439d8000    # 315.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v0, v2

    float-to-int v0, v0

    .line 215
    int-to-float v1, v1

    .line 1294
    iget-object v2, p0, Landroid/support/v4/widget/a;->s:[F

    div-float v3, v1, v6

    aput v3, v2, v5

    .line 1295
    iget-object v2, p0, Landroid/support/v4/widget/a;->s:[F

    div-float/2addr v1, v6

    aput v1, v2, v4

    .line 216
    int-to-float v0, v0

    .line 1312
    iget-object v1, p0, Landroid/support/v4/widget/a;->r:[F

    div-float v2, v0, v6

    aput v2, v1, v5

    .line 1313
    iget-object v1, p0, Landroid/support/v4/widget/a;->r:[F

    div-float/2addr v0, v6

    aput v0, v1, v4

    .line 1354
    iput v4, p0, Landroid/support/v4/widget/a;->o:I

    .line 1395
    iget-object v0, p0, Landroid/support/v4/widget/a;->n:[F

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    aput v1, v0, v5

    .line 1396
    iget-object v0, p0, Landroid/support/v4/widget/a;->n:[F

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    aput v1, v0, v4

    .line 2373
    iget-object v0, p0, Landroid/support/v4/widget/a;->m:[F

    const v1, 0x3e4ccccd    # 0.2f

    aput v1, v0, v5

    .line 2374
    iget-object v0, p0, Landroid/support/v4/widget/a;->m:[F

    const v1, 0x3e4ccccd    # 0.2f

    aput v1, v0, v4

    .line 3333
    iget-object v0, p0, Landroid/support/v4/widget/a;->q:[F

    aput v7, v0, v5

    .line 3334
    iget-object v0, p0, Landroid/support/v4/widget/a;->q:[F

    aput v7, v0, v4

    .line 222
    sget v0, Landroid/support/v4/widget/a;->F:I

    .line 3412
    iput v0, p0, Landroid/support/v4/widget/a;->p:I

    .line 3427
    iget-object v0, p0, Landroid/support/v4/widget/a;->i:Landroid/support/v4/widget/c;

    .line 3752
    const/16 v1, 0x1f4

    iput v1, v0, Landroid/support/v4/widget/c;->a:I

    .line 4442
    iget-object v0, p0, Landroid/support/v4/widget/a;->i:Landroid/support/v4/widget/c;

    .line 4756
    const/16 v1, 0x1f4

    iput v1, v0, Landroid/support/v4/widget/c;->b:I

    .line 225
    return-void

    .line 149
    nop

    :array_aa
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 152
    :array_b2
    .array-data 4
        0x7f7fffff    # Float.MAX_VALUE
        0x7f7fffff    # Float.MAX_VALUE
    .end array-data

    .line 161
    :array_ba
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 164
    :array_c2
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 167
    :array_ca
    .array-data 4
        0x7f7fffff    # Float.MAX_VALUE
        0x7f7fffff    # Float.MAX_VALUE
    .end array-data
.end method

.method static synthetic a(F)F
    .registers 3

    .prologue
    .line 84
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p0, v0, v1}, Landroid/support/v4/widget/a;->a(FFF)F

    move-result v0

    return v0
.end method

.method private static a(FFF)F
    .registers 4

    .prologue
    .line 663
    cmpl-float v0, p0, p2

    if-lez v0, :cond_5

    .line 668
    :goto_4
    return p2

    .line 665
    :cond_5
    cmpg-float v0, p0, p1

    if-gez v0, :cond_b

    move p2, p1

    .line 666
    goto :goto_4

    :cond_b
    move p2, p0

    .line 668
    goto :goto_4
.end method

.method private a(FFFF)F
    .registers 9

    .prologue
    const/4 v0, 0x0

    .line 607
    mul-float v1, p1, p2

    invoke-static {v1, v0, p3}, Landroid/support/v4/widget/a;->a(FFF)F

    move-result v1

    .line 608
    invoke-direct {p0, p4, v1}, Landroid/support/v4/widget/a;->c(FF)F

    move-result v2

    .line 609
    sub-float v3, p2, p4

    invoke-direct {p0, v3, v1}, Landroid/support/v4/widget/a;->c(FF)F

    move-result v1

    .line 610
    sub-float/2addr v1, v2

    .line 612
    cmpg-float v2, v1, v0

    if-gez v2, :cond_27

    .line 613
    iget-object v0, p0, Landroid/support/v4/widget/a;->j:Landroid/view/animation/Interpolator;

    neg-float v1, v1

    invoke-interface {v0, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    neg-float v0, v0

    .line 620
    :goto_1e
    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2}, Landroid/support/v4/widget/a;->a(FFF)F

    move-result v0

    :cond_26
    return v0

    .line 614
    :cond_27
    cmpl-float v2, v1, v0

    if-lez v2, :cond_26

    .line 615
    iget-object v0, p0, Landroid/support/v4/widget/a;->j:Landroid/view/animation/Interpolator;

    invoke-interface {v0, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_1e
.end method

.method private a(IFFF)F
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 539
    iget-object v0, p0, Landroid/support/v4/widget/a;->m:[F

    aget v0, v0, p1

    .line 540
    iget-object v2, p0, Landroid/support/v4/widget/a;->n:[F

    aget v2, v2, p1

    .line 10607
    mul-float/2addr v0, p3

    invoke-static {v0, v1, v2}, Landroid/support/v4/widget/a;->a(FFF)F

    move-result v0

    .line 10608
    invoke-direct {p0, p2, v0}, Landroid/support/v4/widget/a;->c(FF)F

    move-result v2

    .line 10609
    sub-float v3, p3, p2

    invoke-direct {p0, v3, v0}, Landroid/support/v4/widget/a;->c(FF)F

    move-result v0

    .line 10610
    sub-float/2addr v0, v2

    .line 10612
    cmpg-float v2, v0, v1

    if-gez v2, :cond_33

    .line 10613
    iget-object v2, p0, Landroid/support/v4/widget/a;->j:Landroid/view/animation/Interpolator;

    neg-float v0, v0

    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    neg-float v0, v0

    .line 10620
    :goto_25
    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v0, v2, v3}, Landroid/support/v4/widget/a;->a(FFF)F

    move-result v0

    .line 542
    :goto_2d
    cmpl-float v2, v0, v1

    if-nez v2, :cond_40

    move v0, v1

    .line 558
    :goto_32
    return v0

    .line 10614
    :cond_33
    cmpl-float v2, v0, v1

    if-lez v2, :cond_3e

    .line 10615
    iget-object v2, p0, Landroid/support/v4/widget/a;->j:Landroid/view/animation/Interpolator;

    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_25

    :cond_3e
    move v0, v1

    .line 10617
    goto :goto_2d

    .line 547
    :cond_40
    iget-object v2, p0, Landroid/support/v4/widget/a;->q:[F

    aget v2, v2, p1

    .line 548
    iget-object v3, p0, Landroid/support/v4/widget/a;->r:[F

    aget v3, v3, p1

    .line 549
    iget-object v4, p0, Landroid/support/v4/widget/a;->s:[F

    aget v4, v4, p1

    .line 550
    mul-float/2addr v2, p4

    .line 555
    cmpl-float v1, v0, v1

    if-lez v1, :cond_57

    .line 556
    mul-float/2addr v0, v2

    invoke-static {v0, v3, v4}, Landroid/support/v4/widget/a;->a(FFF)F

    move-result v0

    goto :goto_32

    .line 558
    :cond_57
    neg-float v0, v0

    mul-float/2addr v0, v2

    invoke-static {v0, v3, v4}, Landroid/support/v4/widget/a;->a(FFF)F

    move-result v0

    neg-float v0, v0

    goto :goto_32
.end method

.method static synthetic a(II)I
    .registers 2

    .prologue
    .line 84
    .line 11653
    if-le p0, p1, :cond_3

    .line 11656
    :goto_2
    return p1

    .line 11655
    :cond_3
    if-gez p0, :cond_7

    .line 11656
    const/4 p1, 0x0

    goto :goto_2

    :cond_7
    move p1, p0

    .line 84
    goto :goto_2
.end method

.method private static a(III)I
    .registers 3

    .prologue
    .line 653
    if-le p0, p2, :cond_3

    .line 658
    :goto_2
    return p2

    .line 655
    :cond_3
    if-ge p0, p1, :cond_7

    move p2, p1

    .line 656
    goto :goto_2

    :cond_7
    move p2, p0

    .line 658
    goto :goto_2
.end method

.method private a(FF)Landroid/support/v4/widget/a;
    .registers 7

    .prologue
    const/high16 v3, 0x447a0000    # 1000.0f

    .line 294
    iget-object v0, p0, Landroid/support/v4/widget/a;->s:[F

    const/4 v1, 0x0

    div-float v2, p1, v3

    aput v2, v0, v1

    .line 295
    iget-object v0, p0, Landroid/support/v4/widget/a;->s:[F

    const/4 v1, 0x1

    div-float v2, p2, v3

    aput v2, v0, v1

    .line 296
    return-object p0
.end method

.method static synthetic a(Landroid/support/v4/widget/a;)Z
    .registers 2

    .prologue
    .line 84
    iget-boolean v0, p0, Landroid/support/v4/widget/a;->w:Z

    return v0
.end method

.method private b(FF)Landroid/support/v4/widget/a;
    .registers 7

    .prologue
    const/high16 v3, 0x447a0000    # 1000.0f

    .line 312
    iget-object v0, p0, Landroid/support/v4/widget/a;->r:[F

    const/4 v1, 0x0

    div-float v2, p1, v3

    aput v2, v0, v1

    .line 313
    iget-object v0, p0, Landroid/support/v4/widget/a;->r:[F

    const/4 v1, 0x1

    div-float v2, p2, v3

    aput v2, v0, v1

    .line 314
    return-object p0
.end method

.method private b(Z)Landroid/support/v4/widget/a;
    .registers 2

    .prologue
    .line 263
    iput-boolean p1, p0, Landroid/support/v4/widget/a;->y:Z

    .line 264
    return-object p0
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 247
    iget-boolean v0, p0, Landroid/support/v4/widget/a;->x:Z

    return v0
.end method

.method static synthetic b(Landroid/support/v4/widget/a;)Z
    .registers 2

    .prologue
    .line 84
    iget-boolean v0, p0, Landroid/support/v4/widget/a;->u:Z

    return v0
.end method

.method private c(FF)F
    .registers 7

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 624
    cmpl-float v2, p2, v0

    if-nez v2, :cond_8

    .line 649
    :cond_7
    :goto_7
    return v0

    .line 628
    :cond_8
    iget v2, p0, Landroid/support/v4/widget/a;->o:I

    packed-switch v2, :pswitch_data_2e

    goto :goto_7

    .line 631
    :pswitch_e
    cmpg-float v2, p1, p2

    if-gez v2, :cond_7

    .line 632
    cmpl-float v2, p1, v0

    if-ltz v2, :cond_1b

    .line 634
    div-float v0, p1, p2

    sub-float v0, v1, v0

    goto :goto_7

    .line 635
    :cond_1b
    iget-boolean v2, p0, Landroid/support/v4/widget/a;->w:Z

    if-eqz v2, :cond_7

    iget v2, p0, Landroid/support/v4/widget/a;->o:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    move v0, v1

    .line 637
    goto :goto_7

    .line 642
    :pswitch_26
    cmpg-float v1, p1, v0

    if-gez v1, :cond_7

    .line 644
    neg-float v0, p2

    div-float v0, p1, v0

    goto :goto_7

    .line 628
    :pswitch_data_2e
    .packed-switch 0x0
        :pswitch_e
        :pswitch_e
        :pswitch_26
    .end packed-switch
.end method

.method private c(I)Landroid/support/v4/widget/a;
    .registers 2

    .prologue
    .line 412
    iput p1, p0, Landroid/support/v4/widget/a;->p:I

    .line 413
    return-object p0
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 276
    iget-boolean v0, p0, Landroid/support/v4/widget/a;->y:Z

    return v0
.end method

.method static synthetic c(Landroid/support/v4/widget/a;)Z
    .registers 2

    .prologue
    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/a;->u:Z

    return v0
.end method

.method private d()Landroid/support/v4/widget/a;
    .registers 4

    .prologue
    const v2, 0x3a83126f    # 0.001f

    .line 333
    iget-object v0, p0, Landroid/support/v4/widget/a;->q:[F

    const/4 v1, 0x0

    aput v2, v0, v1

    .line 334
    iget-object v0, p0, Landroid/support/v4/widget/a;->q:[F

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 335
    return-object p0
.end method

.method static synthetic d(Landroid/support/v4/widget/a;)Landroid/support/v4/widget/c;
    .registers 2

    .prologue
    .line 84
    iget-object v0, p0, Landroid/support/v4/widget/a;->i:Landroid/support/v4/widget/c;

    return-object v0
.end method

.method private e()Landroid/support/v4/widget/a;
    .registers 2

    .prologue
    .line 354
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v4/widget/a;->o:I

    .line 355
    return-object p0
.end method

.method static synthetic e(Landroid/support/v4/widget/a;)Z
    .registers 2

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/support/v4/widget/a;->j()Z

    move-result v0

    return v0
.end method

.method private f()Landroid/support/v4/widget/a;
    .registers 4

    .prologue
    const v2, 0x3e4ccccd    # 0.2f

    .line 373
    iget-object v0, p0, Landroid/support/v4/widget/a;->m:[F

    const/4 v1, 0x0

    aput v2, v0, v1

    .line 374
    iget-object v0, p0, Landroid/support/v4/widget/a;->m:[F

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 375
    return-object p0
.end method

.method static synthetic f(Landroid/support/v4/widget/a;)Z
    .registers 2

    .prologue
    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/a;->w:Z

    return v0
.end method

.method private g()Landroid/support/v4/widget/a;
    .registers 4

    .prologue
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    .line 395
    iget-object v0, p0, Landroid/support/v4/widget/a;->n:[F

    const/4 v1, 0x0

    aput v2, v0, v1

    .line 396
    iget-object v0, p0, Landroid/support/v4/widget/a;->n:[F

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 397
    return-object p0
.end method

.method static synthetic g(Landroid/support/v4/widget/a;)Z
    .registers 2

    .prologue
    .line 84
    iget-boolean v0, p0, Landroid/support/v4/widget/a;->v:Z

    return v0
.end method

.method private h()Landroid/support/v4/widget/a;
    .registers 3

    .prologue
    .line 427
    iget-object v0, p0, Landroid/support/v4/widget/a;->i:Landroid/support/v4/widget/c;

    .line 5752
    const/16 v1, 0x1f4

    iput v1, v0, Landroid/support/v4/widget/c;->a:I

    .line 428
    return-object p0
.end method

.method static synthetic h(Landroid/support/v4/widget/a;)Z
    .registers 2

    .prologue
    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/a;->v:Z

    return v0
.end method

.method private i()Landroid/support/v4/widget/a;
    .registers 3

    .prologue
    .line 442
    iget-object v0, p0, Landroid/support/v4/widget/a;->i:Landroid/support/v4/widget/c;

    .line 5756
    const/16 v1, 0x1f4

    iput v1, v0, Landroid/support/v4/widget/c;->b:I

    .line 443
    return-object p0
.end method

.method static synthetic i(Landroid/support/v4/widget/a;)V
    .registers 9

    .prologue
    const/4 v5, 0x0

    .line 10677
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 10678
    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 10680
    iget-object v1, p0, Landroid/support/v4/widget/a;->k:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 10681
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 84
    return-void
.end method

.method static synthetic j(Landroid/support/v4/widget/a;)Landroid/view/View;
    .registers 2

    .prologue
    .line 84
    iget-object v0, p0, Landroid/support/v4/widget/a;->k:Landroid/view/View;

    return-object v0
.end method

.method private j()Z
    .registers 4

    .prologue
    .line 492
    iget-object v0, p0, Landroid/support/v4/widget/a;->i:Landroid/support/v4/widget/c;

    .line 6848
    iget v1, v0, Landroid/support/v4/widget/c;->d:F

    iget v2, v0, Landroid/support/v4/widget/c;->d:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 7844
    iget v2, v0, Landroid/support/v4/widget/c;->c:F

    iget v0, v0, Landroid/support/v4/widget/c;->c:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float v0, v2, v0

    float-to-int v0, v0

    .line 496
    if-eqz v1, :cond_1f

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/a;->b(I)Z

    move-result v1

    if-nez v1, :cond_23

    :cond_1f
    if-eqz v0, :cond_21

    :cond_21
    const/4 v0, 0x0

    :goto_22
    return v0

    :cond_23
    const/4 v0, 0x1

    goto :goto_22
.end method

.method private k()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 504
    iget-object v0, p0, Landroid/support/v4/widget/a;->l:Ljava/lang/Runnable;

    if-nez v0, :cond_d

    .line 505
    new-instance v0, Landroid/support/v4/widget/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v4/widget/d;-><init>(Landroid/support/v4/widget/a;B)V

    iput-object v0, p0, Landroid/support/v4/widget/a;->l:Ljava/lang/Runnable;

    .line 508
    :cond_d
    iput-boolean v4, p0, Landroid/support/v4/widget/a;->w:Z

    .line 509
    iput-boolean v4, p0, Landroid/support/v4/widget/a;->u:Z

    .line 511
    iget-boolean v0, p0, Landroid/support/v4/widget/a;->t:Z

    if-nez v0, :cond_26

    iget v0, p0, Landroid/support/v4/widget/a;->p:I

    if-lez v0, :cond_26

    .line 512
    iget-object v0, p0, Landroid/support/v4/widget/a;->k:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/widget/a;->l:Ljava/lang/Runnable;

    iget v2, p0, Landroid/support/v4/widget/a;->p:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 519
    :goto_23
    iput-boolean v4, p0, Landroid/support/v4/widget/a;->t:Z

    .line 520
    return-void

    .line 514
    :cond_26
    iget-object v0, p0, Landroid/support/v4/widget/a;->l:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_23
.end method

.method private l()V
    .registers 9

    .prologue
    const/4 v1, 0x0

    .line 528
    iget-boolean v0, p0, Landroid/support/v4/widget/a;->u:Z

    if-eqz v0, :cond_8

    .line 531
    iput-boolean v1, p0, Landroid/support/v4/widget/a;->w:Z

    .line 535
    :goto_7
    return-void

    .line 533
    :cond_8
    iget-object v3, p0, Landroid/support/v4/widget/a;->i:Landroid/support/v4/widget/c;

    .line 8775
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    .line 8776
    iget-wide v6, v3, Landroid/support/v4/widget/c;->e:J

    sub-long v6, v4, v6

    long-to-int v2, v6

    iget v0, v3, Landroid/support/v4/widget/c;->b:I

    .line 9653
    if-le v2, v0, :cond_22

    .line 8776
    :goto_17
    iput v0, v3, Landroid/support/v4/widget/c;->k:I

    .line 8777
    invoke-virtual {v3, v4, v5}, Landroid/support/v4/widget/c;->a(J)F

    move-result v0

    iput v0, v3, Landroid/support/v4/widget/c;->j:F

    .line 8778
    iput-wide v4, v3, Landroid/support/v4/widget/c;->i:J

    goto :goto_7

    .line 9655
    :cond_22
    if-gez v2, :cond_26

    move v0, v1

    .line 9656
    goto :goto_17

    :cond_26
    move v0, v2

    .line 9658
    goto :goto_17
.end method

.method private m()V
    .registers 9

    .prologue
    const/4 v5, 0x0

    .line 677
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 678
    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 680
    iget-object v1, p0, Landroid/support/v4/widget/a;->k:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 681
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 682
    return-void
.end method


# virtual methods
.method public final a(Z)Landroid/support/v4/widget/a;
    .registers 3

    .prologue
    .line 235
    iget-boolean v0, p0, Landroid/support/v4/widget/a;->x:Z

    if-eqz v0, :cond_9

    if-nez p1, :cond_9

    .line 236
    invoke-direct {p0}, Landroid/support/v4/widget/a;->l()V

    .line 239
    :cond_9
    iput-boolean p1, p0, Landroid/support/v4/widget/a;->x:Z

    .line 240
    return-object p0
.end method

.method public abstract a(I)V
.end method

.method public abstract a()Z
.end method

.method public abstract b(I)Z
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 456
    iget-boolean v2, p0, Landroid/support/v4/widget/a;->x:Z

    if-nez v2, :cond_7

    .line 485
    :cond_6
    :goto_6
    return v0

    .line 460
    :cond_7
    invoke-static {p2}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 461
    packed-switch v2, :pswitch_data_82

    .line 485
    :cond_e
    :goto_e
    iget-boolean v2, p0, Landroid/support/v4/widget/a;->y:Z

    if-eqz v2, :cond_6

    iget-boolean v2, p0, Landroid/support/v4/widget/a;->w:Z

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_6

    .line 463
    :pswitch_18
    iput-boolean v1, p0, Landroid/support/v4/widget/a;->v:Z

    .line 464
    iput-boolean v0, p0, Landroid/support/v4/widget/a;->t:Z

    .line 467
    :pswitch_1c
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Landroid/support/v4/widget/a;->k:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {p0, v0, v2, v3, v4}, Landroid/support/v4/widget/a;->a(IFFF)F

    move-result v2

    .line 469
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Landroid/support/v4/widget/a;->k:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-direct {p0, v1, v3, v4, v5}, Landroid/support/v4/widget/a;->a(IFFF)F

    move-result v3

    .line 471
    iget-object v4, p0, Landroid/support/v4/widget/a;->i:Landroid/support/v4/widget/c;

    .line 5839
    iput v2, v4, Landroid/support/v4/widget/c;->c:F

    .line 5840
    iput v3, v4, Landroid/support/v4/widget/c;->d:F

    .line 475
    iget-boolean v2, p0, Landroid/support/v4/widget/a;->w:Z

    if-nez v2, :cond_e

    invoke-direct {p0}, Landroid/support/v4/widget/a;->j()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 6504
    iget-object v2, p0, Landroid/support/v4/widget/a;->l:Ljava/lang/Runnable;

    if-nez v2, :cond_5f

    .line 6505
    new-instance v2, Landroid/support/v4/widget/d;

    invoke-direct {v2, p0, v0}, Landroid/support/v4/widget/d;-><init>(Landroid/support/v4/widget/a;B)V

    iput-object v2, p0, Landroid/support/v4/widget/a;->l:Ljava/lang/Runnable;

    .line 6508
    :cond_5f
    iput-boolean v1, p0, Landroid/support/v4/widget/a;->w:Z

    .line 6509
    iput-boolean v1, p0, Landroid/support/v4/widget/a;->u:Z

    .line 6511
    iget-boolean v2, p0, Landroid/support/v4/widget/a;->t:Z

    if-nez v2, :cond_78

    iget v2, p0, Landroid/support/v4/widget/a;->p:I

    if-lez v2, :cond_78

    .line 6512
    iget-object v2, p0, Landroid/support/v4/widget/a;->k:Landroid/view/View;

    iget-object v3, p0, Landroid/support/v4/widget/a;->l:Ljava/lang/Runnable;

    iget v4, p0, Landroid/support/v4/widget/a;->p:I

    int-to-long v4, v4

    invoke-static {v2, v3, v4, v5}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 6519
    :goto_75
    iput-boolean v1, p0, Landroid/support/v4/widget/a;->t:Z

    goto :goto_e

    .line 6514
    :cond_78
    iget-object v2, p0, Landroid/support/v4/widget/a;->l:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto :goto_75

    .line 481
    :pswitch_7e
    invoke-direct {p0}, Landroid/support/v4/widget/a;->l()V

    goto :goto_e

    .line 461
    :pswitch_data_82
    .packed-switch 0x0
        :pswitch_18
        :pswitch_7e
        :pswitch_1c
        :pswitch_7e
    .end packed-switch
.end method
