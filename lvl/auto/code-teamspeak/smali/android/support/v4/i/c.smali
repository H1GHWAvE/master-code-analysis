.class public final Landroid/support/v4/i/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/Object;

.field public c:Z

.field private d:Landroid/support/v4/i/d;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method

.method private a(Landroid/support/v4/i/d;)V
    .registers 3

    .prologue
    .line 109
    monitor-enter p0

    .line 1150
    :goto_1
    :try_start_1
    iget-boolean v0, p0, Landroid/support/v4/i/c;->c:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_1b

    if-eqz v0, :cond_b

    .line 1152
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_8} :catch_9
    .catchall {:try_start_5 .. :try_end_8} :catchall_1b

    goto :goto_1

    .line 1154
    :catch_9
    move-exception v0

    goto :goto_1

    .line 112
    :cond_b
    :try_start_b
    iget-object v0, p0, Landroid/support/v4/i/c;->d:Landroid/support/v4/i/d;

    if-ne v0, p1, :cond_11

    .line 113
    monitor-exit p0

    .line 119
    :goto_10
    return-void

    .line 115
    :cond_11
    iput-object p1, p0, Landroid/support/v4/i/c;->d:Landroid/support/v4/i/d;

    .line 116
    iget-boolean v0, p0, Landroid/support/v4/i/c;->a:Z

    if-eqz v0, :cond_19

    if-nez p1, :cond_1e

    .line 117
    :cond_19
    monitor-exit p0

    goto :goto_10

    .line 119
    :catchall_1b
    move-exception v0

    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_b .. :try_end_1d} :catchall_1b

    throw v0

    :cond_1e
    :try_start_1e
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1b

    goto :goto_10
.end method

.method private c()V
    .registers 2

    .prologue
    .line 55
    invoke-virtual {p0}, Landroid/support/v4/i/c;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 56
    new-instance v0, Landroid/support/v4/i/h;

    invoke-direct {v0}, Landroid/support/v4/i/h;-><init>()V

    throw v0

    .line 58
    :cond_c
    return-void
.end method

.method private d()V
    .registers 3

    .prologue
    .line 67
    monitor-enter p0

    .line 68
    :try_start_1
    iget-boolean v0, p0, Landroid/support/v4/i/c;->a:Z

    if-eqz v0, :cond_7

    .line 69
    monitor-exit p0

    .line 88
    :goto_6
    return-void

    .line 71
    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/i/c;->a:Z

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/i/c;->c:Z

    .line 74
    iget-object v0, p0, Landroid/support/v4/i/c;->b:Ljava/lang/Object;

    .line 75
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_23

    .line 81
    if-eqz v0, :cond_17

    .line 1025
    :try_start_12
    check-cast v0, Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V
    :try_end_17
    .catchall {:try_start_12 .. :try_end_17} :catchall_26

    .line 85
    :cond_17
    monitor-enter p0

    .line 86
    const/4 v0, 0x0

    :try_start_19
    iput-boolean v0, p0, Landroid/support/v4/i/c;->c:Z

    .line 87
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 88
    monitor-exit p0

    goto :goto_6

    :catchall_20
    move-exception v0

    monitor-exit p0
    :try_end_22
    .catchall {:try_start_19 .. :try_end_22} :catchall_20

    throw v0

    .line 75
    :catchall_23
    move-exception v0

    :try_start_24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_23

    throw v0

    .line 85
    :catchall_26
    move-exception v0

    monitor-enter p0

    .line 86
    const/4 v1, 0x0

    :try_start_29
    iput-boolean v1, p0, Landroid/support/v4/i/c;->c:Z

    .line 87
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 88
    monitor-exit p0
    :try_end_2f
    .catchall {:try_start_29 .. :try_end_2f} :catchall_30

    throw v0

    :catchall_30
    move-exception v0

    :try_start_31
    monitor-exit p0
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_30

    throw v0
.end method

.method private e()V
    .registers 2

    .prologue
    .line 150
    :goto_0
    iget-boolean v0, p0, Landroid/support/v4/i/c;->c:Z

    if-eqz v0, :cond_a

    .line 152
    :try_start_4
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_7} :catch_8

    goto :goto_0

    .line 154
    :catch_8
    move-exception v0

    goto :goto_0

    .line 156
    :cond_a
    return-void
.end method


# virtual methods
.method public final a()Z
    .registers 2

    .prologue
    .line 44
    monitor-enter p0

    .line 45
    :try_start_1
    iget-boolean v0, p0, Landroid/support/v4/i/c;->a:Z

    monitor-exit p0

    return v0

    .line 46
    :catchall_5
    move-exception v0

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    throw v0
.end method

.method public final b()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 135
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_8

    .line 136
    const/4 v0, 0x0

    .line 145
    :goto_7
    return-object v0

    .line 138
    :cond_8
    monitor-enter p0

    .line 139
    :try_start_9
    iget-object v0, p0, Landroid/support/v4/i/c;->b:Ljava/lang/Object;

    if-nez v0, :cond_1f

    .line 2021
    new-instance v0, Landroid/os/CancellationSignal;

    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    .line 140
    iput-object v0, p0, Landroid/support/v4/i/c;->b:Ljava/lang/Object;

    .line 141
    iget-boolean v0, p0, Landroid/support/v4/i/c;->a:Z

    if-eqz v0, :cond_1f

    .line 142
    iget-object v0, p0, Landroid/support/v4/i/c;->b:Ljava/lang/Object;

    .line 2025
    check-cast v0, Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    .line 145
    :cond_1f
    iget-object v0, p0, Landroid/support/v4/i/c;->b:Ljava/lang/Object;

    monitor-exit p0

    goto :goto_7

    .line 146
    :catchall_23
    move-exception v0

    monitor-exit p0
    :try_end_25
    .catchall {:try_start_9 .. :try_end_25} :catchall_23

    throw v0
.end method
