.class public final Landroid/support/v4/e/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/e/a/c;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 215
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 216
    const/16 v1, 0x17

    if-lt v0, v1, :cond_e

    .line 217
    new-instance v0, Landroid/support/v4/e/a/i;

    invoke-direct {v0}, Landroid/support/v4/e/a/i;-><init>()V

    sput-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    .line 231
    :goto_d
    return-void

    .line 218
    :cond_e
    const/16 v1, 0x16

    if-lt v0, v1, :cond_1a

    .line 219
    new-instance v0, Landroid/support/v4/e/a/h;

    invoke-direct {v0}, Landroid/support/v4/e/a/h;-><init>()V

    sput-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    goto :goto_d

    .line 220
    :cond_1a
    const/16 v1, 0x15

    if-lt v0, v1, :cond_26

    .line 221
    new-instance v0, Landroid/support/v4/e/a/g;

    invoke-direct {v0}, Landroid/support/v4/e/a/g;-><init>()V

    sput-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    goto :goto_d

    .line 222
    :cond_26
    const/16 v1, 0x13

    if-lt v0, v1, :cond_32

    .line 223
    new-instance v0, Landroid/support/v4/e/a/f;

    invoke-direct {v0}, Landroid/support/v4/e/a/f;-><init>()V

    sput-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    goto :goto_d

    .line 224
    :cond_32
    const/16 v1, 0x11

    if-lt v0, v1, :cond_3e

    .line 225
    new-instance v0, Landroid/support/v4/e/a/e;

    invoke-direct {v0}, Landroid/support/v4/e/a/e;-><init>()V

    sput-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    goto :goto_d

    .line 226
    :cond_3e
    const/16 v1, 0xb

    if-lt v0, v1, :cond_4a

    .line 227
    new-instance v0, Landroid/support/v4/e/a/d;

    invoke-direct {v0}, Landroid/support/v4/e/a/d;-><init>()V

    sput-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    goto :goto_d

    .line 229
    :cond_4a
    new-instance v0, Landroid/support/v4/e/a/b;

    invoke-direct {v0}, Landroid/support/v4/e/a/b;-><init>()V

    sput-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 242
    sget-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    invoke-interface {v0, p0}, Landroid/support/v4/e/a/c;->a(Landroid/graphics/drawable/Drawable;)V

    .line 243
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;FF)V
    .registers 4

    .prologue
    .line 284
    sget-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/v4/e/a/c;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 285
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;I)V
    .registers 3

    .prologue
    .line 305
    sget-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/e/a/c;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 306
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;IIII)V
    .registers 11

    .prologue
    .line 295
    sget-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/e/a/c;->a(Landroid/graphics/drawable/Drawable;IIII)V

    .line 296
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
    .registers 3

    .prologue
    .line 315
    sget-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/e/a/c;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 316
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
    .registers 3

    .prologue
    .line 325
    sget-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/e/a/c;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 326
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Z)V
    .registers 3

    .prologue
    .line 258
    sget-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/e/a/c;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 259
    return-void
.end method

.method public static b(Landroid/graphics/drawable/Drawable;I)V
    .registers 3

    .prologue
    .line 375
    sget-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/e/a/c;->b(Landroid/graphics/drawable/Drawable;I)V

    .line 376
    return-void
.end method

.method public static b(Landroid/graphics/drawable/Drawable;)Z
    .registers 2

    .prologue
    .line 273
    sget-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    invoke-interface {v0, p0}, Landroid/support/v4/e/a/c;->b(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 344
    sget-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    invoke-interface {v0, p0}, Landroid/support/v4/e/a/c;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 358
    instance-of v0, p0, Landroid/support/v4/e/a/q;

    if-eqz v0, :cond_a

    .line 359
    check-cast p0, Landroid/support/v4/e/a/q;

    invoke-interface {p0}, Landroid/support/v4/e/a/q;->a()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 361
    :cond_a
    return-object p0
.end method

.method public static e(Landroid/graphics/drawable/Drawable;)I
    .registers 2

    .prologue
    .line 386
    sget-object v0, Landroid/support/v4/e/a/a;->a:Landroid/support/v4/e/a/c;

    invoke-interface {v0, p0}, Landroid/support/v4/e/a/c;->d(Landroid/graphics/drawable/Drawable;)I

    move-result v0

    return v0
.end method
