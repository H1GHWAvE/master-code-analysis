.class public final Landroid/support/v4/h/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/support/v4/h/c;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 84
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_e

    .line 85
    new-instance v0, Landroid/support/v4/h/f;

    invoke-direct {v0}, Landroid/support/v4/h/f;-><init>()V

    sput-object v0, Landroid/support/v4/h/a;->a:Landroid/support/v4/h/c;

    .line 93
    :goto_d
    return-void

    .line 86
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_1c

    .line 87
    new-instance v0, Landroid/support/v4/h/e;

    invoke-direct {v0}, Landroid/support/v4/h/e;-><init>()V

    sput-object v0, Landroid/support/v4/h/a;->a:Landroid/support/v4/h/c;

    goto :goto_d

    .line 88
    :cond_1c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_2a

    .line 89
    new-instance v0, Landroid/support/v4/h/d;

    invoke-direct {v0}, Landroid/support/v4/h/d;-><init>()V

    sput-object v0, Landroid/support/v4/h/a;->a:Landroid/support/v4/h/c;

    goto :goto_d

    .line 91
    :cond_2a
    new-instance v0, Landroid/support/v4/h/b;

    invoke-direct {v0}, Landroid/support/v4/h/b;-><init>()V

    sput-object v0, Landroid/support/v4/h/a;->a:Landroid/support/v4/h/c;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    return-void
.end method

.method private static a(Landroid/net/ConnectivityManager;Landroid/content/Intent;)Landroid/net/NetworkInfo;
    .registers 3

    .prologue
    .line 114
    const-string v0, "networkInfo"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 115
    if-eqz v0, :cond_13

    .line 116
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 118
    :goto_12
    return-object v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method private static a(Landroid/net/ConnectivityManager;)Z
    .registers 2

    .prologue
    .line 103
    sget-object v0, Landroid/support/v4/h/a;->a:Landroid/support/v4/h/c;

    invoke-interface {v0, p0}, Landroid/support/v4/h/c;->a(Landroid/net/ConnectivityManager;)Z

    move-result v0

    return v0
.end method
