.class public abstract Landroid/support/v4/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Landroid/support/v4/b/c;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 27
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_e

    .line 28
    new-instance v0, Landroid/support/v4/b/h;

    invoke-direct {v0}, Landroid/support/v4/b/h;-><init>()V

    sput-object v0, Landroid/support/v4/b/a;->a:Landroid/support/v4/b/c;

    .line 32
    :goto_d
    return-void

    .line 30
    :cond_e
    new-instance v0, Landroid/support/v4/b/e;

    invoke-direct {v0}, Landroid/support/v4/b/e;-><init>()V

    sput-object v0, Landroid/support/v4/b/a;->a:Landroid/support/v4/b/c;

    goto :goto_d
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method private static a()Landroid/support/v4/b/l;
    .registers 1

    .prologue
    .line 35
    sget-object v0, Landroid/support/v4/b/a;->a:Landroid/support/v4/b/c;

    invoke-interface {v0}, Landroid/support/v4/b/c;->a()Landroid/support/v4/b/l;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 43
    sget-object v0, Landroid/support/v4/b/a;->a:Landroid/support/v4/b/c;

    invoke-interface {v0, p0}, Landroid/support/v4/b/c;->a(Landroid/view/View;)V

    .line 44
    return-void
.end method
