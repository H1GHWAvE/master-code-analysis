.class public abstract Landroid/support/v4/media/session/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:Landroid/support/v4/media/session/j;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 343
    iput-boolean v2, p0, Landroid/support/v4/media/session/i;->c:Z

    .line 346
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_19

    .line 347
    new-instance v0, Landroid/support/v4/media/session/k;

    invoke-direct {v0, p0, v2}, Landroid/support/v4/media/session/k;-><init>(Landroid/support/v4/media/session/i;B)V

    .line 1042
    new-instance v1, Landroid/support/v4/media/session/x;

    invoke-direct {v1, v0}, Landroid/support/v4/media/session/x;-><init>(Landroid/support/v4/media/session/w;)V

    .line 347
    iput-object v1, p0, Landroid/support/v4/media/session/i;->a:Ljava/lang/Object;

    .line 351
    :goto_18
    return-void

    .line 349
    :cond_19
    new-instance v0, Landroid/support/v4/media/session/l;

    invoke-direct {v0, p0, v2}, Landroid/support/v4/media/session/l;-><init>(Landroid/support/v4/media/session/i;B)V

    iput-object v0, p0, Landroid/support/v4/media/session/i;->a:Ljava/lang/Object;

    goto :goto_18
.end method

.method static synthetic a(Landroid/support/v4/media/session/i;)Landroid/support/v4/media/session/j;
    .registers 2

    .prologue
    .line 339
    iget-object v0, p0, Landroid/support/v4/media/session/i;->b:Landroid/support/v4/media/session/j;

    return-object v0
.end method

.method private static a()V
    .registers 0

    .prologue
    .line 358
    return-void
.end method

.method private a(Landroid/os/Handler;)V
    .registers 4

    .prologue
    .line 435
    new-instance v0, Landroid/support/v4/media/session/j;

    invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/support/v4/media/session/j;-><init>(Landroid/support/v4/media/session/i;Landroid/os/Looper;)V

    iput-object v0, p0, Landroid/support/v4/media/session/i;->b:Landroid/support/v4/media/session/j;

    .line 436
    return-void
.end method

.method static synthetic a(Landroid/support/v4/media/session/i;Landroid/os/Handler;)V
    .registers 4

    .prologue
    .line 339
    .line 1435
    new-instance v0, Landroid/support/v4/media/session/j;

    invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/support/v4/media/session/j;-><init>(Landroid/support/v4/media/session/i;Landroid/os/Looper;)V

    iput-object v0, p0, Landroid/support/v4/media/session/i;->b:Landroid/support/v4/media/session/j;

    .line 339
    return-void
.end method

.method static synthetic a(Landroid/support/v4/media/session/i;Z)Z
    .registers 2

    .prologue
    .line 339
    iput-boolean p1, p0, Landroid/support/v4/media/session/i;->c:Z

    return p1
.end method

.method private static b()V
    .registers 0

    .prologue
    .line 369
    return-void
.end method

.method static synthetic b(Landroid/support/v4/media/session/i;)Z
    .registers 2

    .prologue
    .line 339
    iget-boolean v0, p0, Landroid/support/v4/media/session/i;->c:Z

    return v0
.end method

.method static synthetic c(Landroid/support/v4/media/session/i;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 339
    iget-object v0, p0, Landroid/support/v4/media/session/i;->a:Ljava/lang/Object;

    return-object v0
.end method

.method private static c()V
    .registers 0

    .prologue
    .line 377
    return-void
.end method

.method private static d()V
    .registers 0

    .prologue
    .line 386
    return-void
.end method

.method private static e()V
    .registers 0

    .prologue
    .line 397
    return-void
.end method

.method private static f()V
    .registers 0

    .prologue
    .line 407
    return-void
.end method

.method private static g()V
    .registers 0

    .prologue
    .line 416
    return-void
.end method

.method private static h()V
    .registers 0

    .prologue
    .line 424
    return-void
.end method


# virtual methods
.method public binderDied()V
    .registers 1

    .prologue
    .line 429
    return-void
.end method
