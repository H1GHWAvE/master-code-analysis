.class public final Landroid/support/v4/media/RatingCompat;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x3

.field public static final e:I = 0x4

.field public static final f:I = 0x5

.field public static final g:I = 0x6

.field private static final h:Ljava/lang/String; = "Rating"

.field private static final i:F = -1.0f


# instance fields
.field private final j:I

.field private final k:F

.field private l:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 121
    new-instance v0, Landroid/support/v4/media/o;

    invoke-direct {v0}, Landroid/support/v4/media/o;-><init>()V

    sput-object v0, Landroid/support/v4/media/RatingCompat;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IF)V
    .registers 3

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput p1, p0, Landroid/support/v4/media/RatingCompat;->j:I

    .line 101
    iput p2, p0, Landroid/support/v4/media/RatingCompat;->k:F

    .line 102
    return-void
.end method

.method synthetic constructor <init>(IFB)V
    .registers 4

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/support/v4/media/RatingCompat;-><init>(IF)V

    return-void
.end method

.method private static a(F)Landroid/support/v4/media/RatingCompat;
    .registers 3

    .prologue
    .line 228
    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-ltz v0, :cond_b

    const/high16 v0, 0x42c80000    # 100.0f

    cmpl-float v0, p0, v0

    if-lez v0, :cond_14

    .line 229
    :cond_b
    const-string v0, "Rating"

    const-string v1, "Invalid percentage-based rating value"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const/4 v0, 0x0

    .line 232
    :goto_13
    return-object v0

    :cond_14
    new-instance v0, Landroid/support/v4/media/RatingCompat;

    const/4 v1, 0x6

    invoke-direct {v0, v1, p0}, Landroid/support/v4/media/RatingCompat;-><init>(IF)V

    goto :goto_13
.end method

.method private static a(I)Landroid/support/v4/media/RatingCompat;
    .registers 3

    .prologue
    .line 149
    packed-switch p0, :pswitch_data_e

    .line 158
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 156
    :pswitch_5
    new-instance v0, Landroid/support/v4/media/RatingCompat;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-direct {v0, p0, v1}, Landroid/support/v4/media/RatingCompat;-><init>(IF)V

    goto :goto_4

    .line 149
    nop

    :pswitch_data_e
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method private static a(IF)Landroid/support/v4/media/RatingCompat;
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 199
    packed-switch p0, :pswitch_data_3e

    .line 210
    const-string v1, "Rating"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid rating style ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") for a star rating"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :goto_1e
    return-object v0

    .line 201
    :pswitch_1f
    const/high16 v1, 0x40400000    # 3.0f

    .line 213
    :goto_21
    const/4 v2, 0x0

    cmpg-float v2, p1, v2

    if-ltz v2, :cond_2a

    cmpl-float v1, p1, v1

    if-lez v1, :cond_38

    .line 214
    :cond_2a
    const-string v1, "Rating"

    const-string v2, "Trying to set out of range star-based rating"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1e

    .line 204
    :pswitch_32
    const/high16 v1, 0x40800000    # 4.0f

    .line 205
    goto :goto_21

    .line 207
    :pswitch_35
    const/high16 v1, 0x40a00000    # 5.0f

    .line 208
    goto :goto_21

    .line 217
    :cond_38
    new-instance v0, Landroid/support/v4/media/RatingCompat;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/media/RatingCompat;-><init>(IF)V

    goto :goto_1e

    .line 199
    :pswitch_data_3e
    .packed-switch 0x3
        :pswitch_1f
        :pswitch_32
        :pswitch_35
    .end packed-switch
.end method

.method public static a(Ljava/lang/Object;)Landroid/support/v4/media/RatingCompat;
    .registers 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 322
    if-eqz p0, :cond_c

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-ge v0, v4, :cond_d

    .line 352
    :cond_c
    :goto_c
    return-object v3

    :cond_d
    move-object v0, p0

    .line 1047
    check-cast v0, Landroid/media/Rating;

    invoke-virtual {v0}, Landroid/media/Rating;->getRatingStyle()I

    move-result v4

    move-object v0, p0

    .line 2043
    check-cast v0, Landroid/media/Rating;

    invoke-virtual {v0}, Landroid/media/Rating;->isRated()Z

    move-result v0

    .line 328
    if-eqz v0, :cond_ae

    .line 329
    packed-switch v4, :pswitch_data_bc

    goto :goto_c

    :pswitch_21
    move-object v0, p0

    .line 2051
    check-cast v0, Landroid/media/Rating;

    invoke-virtual {v0}, Landroid/media/Rating;->hasHeart()Z

    move-result v0

    .line 2170
    new-instance v3, Landroid/support/v4/media/RatingCompat;

    const/4 v4, 0x1

    if-eqz v0, :cond_34

    move v0, v1

    :goto_2e
    invoke-direct {v3, v4, v0}, Landroid/support/v4/media/RatingCompat;-><init>(IF)V

    .line 351
    :goto_31
    iput-object p0, v3, Landroid/support/v4/media/RatingCompat;->l:Ljava/lang/Object;

    goto :goto_c

    :cond_34
    move v0, v2

    .line 2170
    goto :goto_2e

    :pswitch_36
    move-object v0, p0

    .line 3055
    check-cast v0, Landroid/media/Rating;

    invoke-virtual {v0}, Landroid/media/Rating;->isThumbUp()Z

    move-result v0

    .line 3181
    new-instance v3, Landroid/support/v4/media/RatingCompat;

    const/4 v4, 0x2

    if-eqz v0, :cond_46

    :goto_42
    invoke-direct {v3, v4, v1}, Landroid/support/v4/media/RatingCompat;-><init>(IF)V

    goto :goto_31

    :cond_46
    move v1, v2

    goto :goto_42

    :pswitch_48
    move-object v0, p0

    .line 4059
    check-cast v0, Landroid/media/Rating;

    invoke-virtual {v0}, Landroid/media/Rating;->getStarRating()F

    move-result v1

    .line 4199
    packed-switch v4, :pswitch_data_cc

    .line 4210
    const-string v0, "Rating"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid rating style ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") for a star rating"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    :goto_6d
    move-object v3, v0

    .line 341
    goto :goto_31

    .line 4201
    :pswitch_6f
    const/high16 v0, 0x40400000    # 3.0f

    .line 4213
    :goto_71
    cmpg-float v2, v1, v2

    if-ltz v2, :cond_79

    cmpl-float v0, v1, v0

    if-lez v0, :cond_88

    .line 4214
    :cond_79
    const-string v0, "Rating"

    const-string v1, "Trying to set out of range star-based rating"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 4215
    goto :goto_6d

    .line 4204
    :pswitch_82
    const/high16 v0, 0x40800000    # 4.0f

    .line 4205
    goto :goto_71

    .line 4207
    :pswitch_85
    const/high16 v0, 0x40a00000    # 5.0f

    .line 4208
    goto :goto_71

    .line 4217
    :cond_88
    new-instance v0, Landroid/support/v4/media/RatingCompat;

    invoke-direct {v0, v4, v1}, Landroid/support/v4/media/RatingCompat;-><init>(IF)V

    goto :goto_6d

    :pswitch_8e
    move-object v0, p0

    .line 5063
    check-cast v0, Landroid/media/Rating;

    invoke-virtual {v0}, Landroid/media/Rating;->getPercentRating()F

    move-result v0

    .line 5228
    cmpg-float v1, v0, v2

    if-ltz v1, :cond_9f

    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_a7

    .line 5229
    :cond_9f
    const-string v0, "Rating"

    const-string v1, "Invalid percentage-based rating value"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_31

    .line 5232
    :cond_a7
    new-instance v3, Landroid/support/v4/media/RatingCompat;

    const/4 v1, 0x6

    invoke-direct {v3, v1, v0}, Landroid/support/v4/media/RatingCompat;-><init>(IF)V

    goto :goto_31

    .line 6149
    :cond_ae
    packed-switch v4, :pswitch_data_d6

    goto :goto_31

    .line 6156
    :pswitch_b2
    new-instance v3, Landroid/support/v4/media/RatingCompat;

    const/high16 v0, -0x40800000    # -1.0f

    invoke-direct {v3, v4, v0}, Landroid/support/v4/media/RatingCompat;-><init>(IF)V

    goto/16 :goto_31

    .line 329
    nop

    :pswitch_data_bc
    .packed-switch 0x1
        :pswitch_21
        :pswitch_36
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_8e
    .end packed-switch

    .line 4199
    :pswitch_data_cc
    .packed-switch 0x3
        :pswitch_6f
        :pswitch_82
        :pswitch_85
    .end packed-switch

    .line 6149
    :pswitch_data_d6
    .packed-switch 0x1
        :pswitch_b2
        :pswitch_b2
        :pswitch_b2
        :pswitch_b2
        :pswitch_b2
        :pswitch_b2
    .end packed-switch
.end method

.method private static a(Z)Landroid/support/v4/media/RatingCompat;
    .registers 4

    .prologue
    .line 170
    new-instance v1, Landroid/support/v4/media/RatingCompat;

    const/4 v2, 0x1

    if-eqz p0, :cond_b

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_7
    invoke-direct {v1, v2, v0}, Landroid/support/v4/media/RatingCompat;-><init>(IF)V

    return-object v1

    :cond_b
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static b(Z)Landroid/support/v4/media/RatingCompat;
    .registers 4

    .prologue
    .line 181
    new-instance v1, Landroid/support/v4/media/RatingCompat;

    const/4 v2, 0x2

    if-eqz p0, :cond_b

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_7
    invoke-direct {v1, v2, v0}, Landroid/support/v4/media/RatingCompat;-><init>(IF)V

    return-object v1

    :cond_b
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private b()Z
    .registers 3

    .prologue
    .line 241
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->k:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method private c()I
    .registers 2

    .prologue
    .line 252
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->j:I

    return v0
.end method

.method private d()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 261
    iget v2, p0, Landroid/support/v4/media/RatingCompat;->j:I

    if-eq v2, v1, :cond_7

    .line 264
    :cond_6
    :goto_6
    return v0

    :cond_7
    iget v2, p0, Landroid/support/v4/media/RatingCompat;->k:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_6
.end method

.method private e()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 274
    iget v1, p0, Landroid/support/v4/media/RatingCompat;->j:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    .line 277
    :cond_6
    :goto_6
    return v0

    :cond_7
    iget v1, p0, Landroid/support/v4/media/RatingCompat;->k:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_6

    const/4 v0, 0x1

    goto :goto_6
.end method

.method private f()F
    .registers 2

    .prologue
    .line 287
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->j:I

    packed-switch v0, :pswitch_data_12

    .line 295
    :cond_5
    const/high16 v0, -0x40800000    # -1.0f

    :goto_7
    return v0

    .line 291
    :pswitch_8
    invoke-direct {p0}, Landroid/support/v4/media/RatingCompat;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 292
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->k:F

    goto :goto_7

    .line 287
    nop

    :pswitch_data_12
    .packed-switch 0x3
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method private g()F
    .registers 3

    .prologue
    .line 305
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->j:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_b

    invoke-direct {p0}, Landroid/support/v4/media/RatingCompat;->b()Z

    move-result v0

    if-nez v0, :cond_e

    .line 306
    :cond_b
    const/high16 v0, -0x40800000    # -1.0f

    .line 308
    :goto_d
    return v0

    :cond_e
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->k:F

    goto :goto_d
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .registers 7

    .prologue
    const/4 v1, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v0, 0x1

    .line 364
    iget-object v3, p0, Landroid/support/v4/media/RatingCompat;->l:Ljava/lang/Object;

    if-nez v3, :cond_10

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-ge v3, v4, :cond_13

    .line 365
    :cond_10
    iget-object v0, p0, Landroid/support/v4/media/RatingCompat;->l:Ljava/lang/Object;

    .line 389
    :goto_12
    return-object v0

    .line 368
    :cond_13
    invoke-direct {p0}, Landroid/support/v4/media/RatingCompat;->b()Z

    move-result v3

    if-eqz v3, :cond_76

    .line 369
    iget v3, p0, Landroid/support/v4/media/RatingCompat;->j:I

    packed-switch v3, :pswitch_data_80

    .line 384
    :goto_1e
    const/4 v0, 0x0

    goto :goto_12

    .line 6261
    :pswitch_20
    iget v2, p0, Landroid/support/v4/media/RatingCompat;->j:I

    if-ne v2, v0, :cond_33

    .line 6264
    iget v2, p0, Landroid/support/v4/media/RatingCompat;->k:F

    cmpl-float v2, v2, v5

    if-nez v2, :cond_33

    .line 7027
    :goto_2a
    invoke-static {v0}, Landroid/media/Rating;->newHeartRating(Z)Landroid/media/Rating;

    move-result-object v0

    .line 371
    iput-object v0, p0, Landroid/support/v4/media/RatingCompat;->l:Ljava/lang/Object;

    .line 389
    :goto_30
    iget-object v0, p0, Landroid/support/v4/media/RatingCompat;->l:Ljava/lang/Object;

    goto :goto_12

    :cond_33
    move v0, v1

    .line 6264
    goto :goto_2a

    .line 7274
    :pswitch_35
    iget v2, p0, Landroid/support/v4/media/RatingCompat;->j:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_47

    .line 7277
    iget v2, p0, Landroid/support/v4/media/RatingCompat;->k:F

    cmpl-float v2, v2, v5

    if-nez v2, :cond_47

    .line 8031
    :goto_40
    invoke-static {v0}, Landroid/media/Rating;->newThumbRating(Z)Landroid/media/Rating;

    move-result-object v0

    .line 374
    iput-object v0, p0, Landroid/support/v4/media/RatingCompat;->l:Ljava/lang/Object;

    goto :goto_30

    :cond_47
    move v0, v1

    .line 7277
    goto :goto_40

    .line 379
    :pswitch_49
    iget v1, p0, Landroid/support/v4/media/RatingCompat;->j:I

    .line 8287
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->j:I

    packed-switch v0, :pswitch_data_90

    :cond_50
    move v0, v2

    .line 9035
    :goto_51
    invoke-static {v1, v0}, Landroid/media/Rating;->newStarRating(IF)Landroid/media/Rating;

    move-result-object v0

    .line 379
    iput-object v0, p0, Landroid/support/v4/media/RatingCompat;->l:Ljava/lang/Object;

    goto :goto_30

    .line 8291
    :pswitch_58
    invoke-direct {p0}, Landroid/support/v4/media/RatingCompat;->b()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 8292
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->k:F

    goto :goto_51

    .line 9305
    :pswitch_61
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->j:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_6c

    invoke-direct {p0}, Landroid/support/v4/media/RatingCompat;->b()Z

    move-result v0

    if-nez v0, :cond_73

    .line 10039
    :cond_6c
    :goto_6c
    invoke-static {v2}, Landroid/media/Rating;->newPercentageRating(F)Landroid/media/Rating;

    move-result-object v0

    .line 382
    iput-object v0, p0, Landroid/support/v4/media/RatingCompat;->l:Ljava/lang/Object;

    goto :goto_1e

    .line 9308
    :cond_73
    iget v2, p0, Landroid/support/v4/media/RatingCompat;->k:F

    goto :goto_6c

    .line 387
    :cond_76
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->j:I

    .line 11023
    invoke-static {v0}, Landroid/media/Rating;->newUnratedRating(I)Landroid/media/Rating;

    move-result-object v0

    .line 387
    iput-object v0, p0, Landroid/support/v4/media/RatingCompat;->l:Ljava/lang/Object;

    goto :goto_30

    .line 369
    nop

    :pswitch_data_80
    .packed-switch 0x1
        :pswitch_20
        :pswitch_35
        :pswitch_49
        :pswitch_49
        :pswitch_49
        :pswitch_61
    .end packed-switch

    .line 8287
    :pswitch_data_90
    .packed-switch 0x3
        :pswitch_58
        :pswitch_58
        :pswitch_58
    .end packed-switch
.end method

.method public final describeContents()I
    .registers 2

    .prologue
    .line 112
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->j:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Rating:style="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/media/RatingCompat;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " rating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Landroid/support/v4/media/RatingCompat;->k:F

    const/4 v2, 0x0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_25

    const-string v0, "unrated"

    :goto_1c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_25
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->k:F

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    goto :goto_1c
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .registers 4

    .prologue
    .line 117
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->j:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 118
    iget v0, p0, Landroid/support/v4/media/RatingCompat;->k:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 119
    return-void
.end method
