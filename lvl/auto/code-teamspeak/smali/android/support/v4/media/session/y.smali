.class public final Landroid/support/v4/media/session/y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I = 0x4

.field private static final b:I = 0x6

.field private static final c:I = 0x7


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/media/AudioAttributes;)I
    .registers 5

    .prologue
    const/4 v2, 0x4

    const/4 v0, 0x3

    const/4 v1, 0x1

    .line 209
    invoke-virtual {p0}, Landroid/media/AudioAttributes;->getFlags()I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_d

    .line 211
    const/4 v0, 0x7

    .line 242
    :goto_c
    :pswitch_c
    return v0

    .line 213
    :cond_d
    invoke-virtual {p0}, Landroid/media/AudioAttributes;->getFlags()I

    move-result v3

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v2, :cond_17

    .line 214
    const/4 v0, 0x6

    goto :goto_c

    .line 218
    :cond_17
    invoke-virtual {p0}, Landroid/media/AudioAttributes;->getUsage()I

    move-result v3

    packed-switch v3, :pswitch_data_2c

    goto :goto_c

    .line 227
    :pswitch_1f
    const/4 v0, 0x0

    goto :goto_c

    :pswitch_21
    move v0, v1

    .line 225
    goto :goto_c

    .line 229
    :pswitch_23
    const/16 v0, 0x8

    goto :goto_c

    :pswitch_26
    move v0, v2

    .line 231
    goto :goto_c

    .line 233
    :pswitch_28
    const/4 v0, 0x2

    goto :goto_c

    .line 239
    :pswitch_2a
    const/4 v0, 0x5

    goto :goto_c

    .line 218
    :pswitch_data_2c
    .packed-switch 0x1
        :pswitch_c
        :pswitch_1f
        :pswitch_23
        :pswitch_26
        :pswitch_2a
        :pswitch_28
        :pswitch_2a
        :pswitch_2a
        :pswitch_2a
        :pswitch_2a
        :pswitch_c
        :pswitch_c
        :pswitch_21
        :pswitch_c
    .end packed-switch
.end method

.method private static a(Ljava/lang/Object;)I
    .registers 2

    .prologue
    .line 177
    check-cast p0, Landroid/media/session/MediaController$PlaybackInfo;

    invoke-virtual {p0}, Landroid/media/session/MediaController$PlaybackInfo;->getPlaybackType()I

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/Object;)Landroid/media/AudioAttributes;
    .registers 2

    .prologue
    .line 181
    check-cast p0, Landroid/media/session/MediaController$PlaybackInfo;

    invoke-virtual {p0}, Landroid/media/session/MediaController$PlaybackInfo;->getAudioAttributes()Landroid/media/AudioAttributes;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/lang/Object;)I
    .registers 6

    .prologue
    const/4 v2, 0x4

    const/4 v0, 0x3

    const/4 v1, 0x1

    .line 185
    .line 1181
    check-cast p0, Landroid/media/session/MediaController$PlaybackInfo;

    invoke-virtual {p0}, Landroid/media/session/MediaController$PlaybackInfo;->getAudioAttributes()Landroid/media/AudioAttributes;

    move-result-object v3

    .line 1209
    invoke-virtual {v3}, Landroid/media/AudioAttributes;->getFlags()I

    move-result v4

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v1, :cond_13

    .line 1211
    const/4 v0, 0x7

    .line 1239
    :goto_12
    :pswitch_12
    return v0

    .line 1213
    :cond_13
    invoke-virtual {v3}, Landroid/media/AudioAttributes;->getFlags()I

    move-result v4

    and-int/lit8 v4, v4, 0x4

    if-ne v4, v2, :cond_1d

    .line 1214
    const/4 v0, 0x6

    goto :goto_12

    .line 1218
    :cond_1d
    invoke-virtual {v3}, Landroid/media/AudioAttributes;->getUsage()I

    move-result v3

    packed-switch v3, :pswitch_data_32

    goto :goto_12

    .line 1227
    :pswitch_25
    const/4 v0, 0x0

    goto :goto_12

    :pswitch_27
    move v0, v1

    .line 1225
    goto :goto_12

    .line 1229
    :pswitch_29
    const/16 v0, 0x8

    goto :goto_12

    :pswitch_2c
    move v0, v2

    .line 1231
    goto :goto_12

    .line 1233
    :pswitch_2e
    const/4 v0, 0x2

    goto :goto_12

    .line 1239
    :pswitch_30
    const/4 v0, 0x5

    goto :goto_12

    .line 1218
    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_12
        :pswitch_25
        :pswitch_29
        :pswitch_2c
        :pswitch_30
        :pswitch_2e
        :pswitch_30
        :pswitch_30
        :pswitch_30
        :pswitch_30
        :pswitch_12
        :pswitch_12
        :pswitch_27
        :pswitch_12
    .end packed-switch
.end method

.method private static d(Ljava/lang/Object;)I
    .registers 2

    .prologue
    .line 190
    check-cast p0, Landroid/media/session/MediaController$PlaybackInfo;

    invoke-virtual {p0}, Landroid/media/session/MediaController$PlaybackInfo;->getVolumeControl()I

    move-result v0

    return v0
.end method

.method private static e(Ljava/lang/Object;)I
    .registers 2

    .prologue
    .line 194
    check-cast p0, Landroid/media/session/MediaController$PlaybackInfo;

    invoke-virtual {p0}, Landroid/media/session/MediaController$PlaybackInfo;->getMaxVolume()I

    move-result v0

    return v0
.end method

.method private static f(Ljava/lang/Object;)I
    .registers 2

    .prologue
    .line 198
    check-cast p0, Landroid/media/session/MediaController$PlaybackInfo;

    invoke-virtual {p0}, Landroid/media/session/MediaController$PlaybackInfo;->getCurrentVolume()I

    move-result v0

    return v0
.end method
