.class final Landroid/support/v4/media/session/u;
.super Landroid/support/v4/media/session/r;
.source "SourceFile"


# instance fields
.field private a:Landroid/support/v4/media/session/d;


# direct methods
.method public constructor <init>(Landroid/support/v4/media/session/d;)V
    .registers 2

    .prologue
    .line 1004
    invoke-direct {p0}, Landroid/support/v4/media/session/r;-><init>()V

    .line 1005
    iput-object p1, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    .line 1006
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 5

    .prologue
    .line 1011
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->g()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1015
    :goto_5
    return-void

    .line 1012
    :catch_6
    move-exception v0

    .line 1013
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in play. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final a(J)V
    .registers 8

    .prologue
    .line 1047
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/media/session/d;->a(J)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1051
    :goto_5
    return-void

    .line 1048
    :catch_6
    move-exception v0

    .line 1049
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in skipToQueueItem. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final a(Landroid/net/Uri;Landroid/os/Bundle;)V
    .registers 7

    .prologue
    .line 1038
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/media/session/d;->a(Landroid/net/Uri;Landroid/os/Bundle;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1042
    :goto_5
    return-void

    .line 1039
    :catch_6
    move-exception v0

    .line 1040
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in playFromUri. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final a(Landroid/support/v4/media/RatingCompat;)V
    .registers 6

    .prologue
    .line 1119
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/d;->a(Landroid/support/v4/media/RatingCompat;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1123
    :goto_5
    return-void

    .line 1120
    :catch_6
    move-exception v0

    .line 1121
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in setRating. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final a(Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;Landroid/os/Bundle;)V
    .registers 7

    .prologue
    .line 1127
    .line 1647
    iget-object v0, p1, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;->a:Ljava/lang/String;

    .line 2133
    :try_start_2
    iget-object v1, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v1, v0, p2}, Landroid/support/v4/media/session/d;->c(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_7} :catch_8

    .line 2136
    :goto_7
    return-void

    .line 2134
    :catch_8
    move-exception v0

    .line 2135
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in sendCustomAction. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 7

    .prologue
    .line 1020
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/media/session/d;->a(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1024
    :goto_5
    return-void

    .line 1021
    :catch_6
    move-exception v0

    .line 1022
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in playFromMediaId. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final b()V
    .registers 5

    .prologue
    .line 1056
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->h()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1060
    :goto_5
    return-void

    .line 1057
    :catch_6
    move-exception v0

    .line 1058
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in pause. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final b(J)V
    .registers 8

    .prologue
    .line 1074
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/media/session/d;->b(J)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1078
    :goto_5
    return-void

    .line 1075
    :catch_6
    move-exception v0

    .line 1076
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in seekTo. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 7

    .prologue
    .line 1029
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/media/session/d;->b(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1033
    :goto_5
    return-void

    .line 1030
    :catch_6
    move-exception v0

    .line 1031
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in playFromSearch. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final c()V
    .registers 5

    .prologue
    .line 1065
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->i()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1069
    :goto_5
    return-void

    .line 1066
    :catch_6
    move-exception v0

    .line 1067
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in stop. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final c(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 7

    .prologue
    .line 1133
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/media/session/d;->c(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1137
    :goto_5
    return-void

    .line 1134
    :catch_6
    move-exception v0

    .line 1135
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in sendCustomAction. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final d()V
    .registers 5

    .prologue
    .line 1083
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->l()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1087
    :goto_5
    return-void

    .line 1084
    :catch_6
    move-exception v0

    .line 1085
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in fastForward. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final e()V
    .registers 5

    .prologue
    .line 1092
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->j()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1096
    :goto_5
    return-void

    .line 1093
    :catch_6
    move-exception v0

    .line 1094
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in skipToNext. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final f()V
    .registers 5

    .prologue
    .line 1101
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->m()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1105
    :goto_5
    return-void

    .line 1102
    :catch_6
    move-exception v0

    .line 1103
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in rewind. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public final g()V
    .registers 5

    .prologue
    .line 1110
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/u;->a:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->k()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    .line 1114
    :goto_5
    return-void

    .line 1111
    :catch_6
    move-exception v0

    .line 1112
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in skipToPrevious. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method
