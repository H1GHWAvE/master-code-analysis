.class final Landroid/support/v4/media/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;
.implements Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/media/AudioManager;

.field final c:Landroid/view/View;

.field final d:Landroid/support/v4/media/w;

.field final e:Ljava/lang/String;

.field final f:Landroid/content/IntentFilter;

.field final g:Landroid/content/Intent;

.field final h:Landroid/view/ViewTreeObserver$OnWindowAttachListener;

.field final i:Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;

.field final j:Landroid/content/BroadcastReceiver;

.field k:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field l:Landroid/app/PendingIntent;

.field m:Landroid/media/RemoteControlClient;

.field n:Z

.field o:I

.field p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/media/AudioManager;Landroid/view/View;Landroid/support/v4/media/w;)V
    .registers 7

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Landroid/support/v4/media/y;

    invoke-direct {v0, p0}, Landroid/support/v4/media/y;-><init>(Landroid/support/v4/media/x;)V

    iput-object v0, p0, Landroid/support/v4/media/x;->h:Landroid/view/ViewTreeObserver$OnWindowAttachListener;

    .line 52
    new-instance v0, Landroid/support/v4/media/z;

    invoke-direct {v0, p0}, Landroid/support/v4/media/z;-><init>(Landroid/support/v4/media/x;)V

    iput-object v0, p0, Landroid/support/v4/media/x;->i:Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;

    .line 60
    new-instance v0, Landroid/support/v4/media/aa;

    invoke-direct {v0, p0}, Landroid/support/v4/media/aa;-><init>(Landroid/support/v4/media/x;)V

    iput-object v0, p0, Landroid/support/v4/media/x;->j:Landroid/content/BroadcastReceiver;

    .line 71
    new-instance v0, Landroid/support/v4/media/ab;

    invoke-direct {v0, p0}, Landroid/support/v4/media/ab;-><init>(Landroid/support/v4/media/x;)V

    iput-object v0, p0, Landroid/support/v4/media/x;->k:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 82
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/media/x;->o:I

    .line 87
    iput-object p1, p0, Landroid/support/v4/media/x;->a:Landroid/content/Context;

    .line 88
    iput-object p2, p0, Landroid/support/v4/media/x;->b:Landroid/media/AudioManager;

    .line 89
    iput-object p3, p0, Landroid/support/v4/media/x;->c:Landroid/view/View;

    .line 90
    iput-object p4, p0, Landroid/support/v4/media/x;->d:Landroid/support/v4/media/w;

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":transport:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/x;->e:Ljava/lang/String;

    .line 92
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Landroid/support/v4/media/x;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroid/support/v4/media/x;->g:Landroid/content/Intent;

    .line 93
    iget-object v0, p0, Landroid/support/v4/media/x;->g:Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Landroid/support/v4/media/x;->f:Landroid/content/IntentFilter;

    .line 95
    iget-object v0, p0, Landroid/support/v4/media/x;->f:Landroid/content/IntentFilter;

    iget-object v1, p0, Landroid/support/v4/media/x;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Landroid/support/v4/media/x;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/media/x;->h:Landroid/view/ViewTreeObserver$OnWindowAttachListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnWindowAttachListener(Landroid/view/ViewTreeObserver$OnWindowAttachListener;)V

    .line 97
    iget-object v0, p0, Landroid/support/v4/media/x;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/media/x;->i:Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnWindowFocusChangeListener(Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;)V

    .line 98
    return-void
.end method

.method private a(ZJI)V
    .registers 9

    .prologue
    .line 159
    iget-object v0, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_16

    .line 160
    iget-object v2, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    if-eqz p1, :cond_17

    const/4 v0, 0x3

    move v1, v0

    :goto_a
    if-eqz p1, :cond_1a

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_e
    invoke-virtual {v2, v1, p2, p3, v0}, Landroid/media/RemoteControlClient;->setPlaybackState(IJF)V

    .line 162
    iget-object v0, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, p4}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 164
    :cond_16
    return-void

    .line 160
    :cond_17
    const/4 v0, 0x1

    move v1, v0

    goto :goto_a

    :cond_1a
    const/4 v0, 0x0

    goto :goto_e
.end method

.method private e()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 101
    iget-object v0, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    return-object v0
.end method

.method private f()V
    .registers 3

    .prologue
    .line 105
    invoke-virtual {p0}, Landroid/support/v4/media/x;->d()V

    .line 106
    iget-object v0, p0, Landroid/support/v4/media/x;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/media/x;->h:Landroid/view/ViewTreeObserver$OnWindowAttachListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnWindowAttachListener(Landroid/view/ViewTreeObserver$OnWindowAttachListener;)V

    .line 107
    iget-object v0, p0, Landroid/support/v4/media/x;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/media/x;->i:Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnWindowFocusChangeListener(Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;)V

    .line 108
    return-void
.end method

.method private g()V
    .registers 5

    .prologue
    .line 111
    iget-object v0, p0, Landroid/support/v4/media/x;->a:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/media/x;->j:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Landroid/support/v4/media/x;->f:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 112
    iget-object v0, p0, Landroid/support/v4/media/x;->a:Landroid/content/Context;

    const/4 v1, 0x0

    iget-object v2, p0, Landroid/support/v4/media/x;->g:Landroid/content/Intent;

    const/high16 v3, 0x10000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/x;->l:Landroid/app/PendingIntent;

    .line 114
    new-instance v0, Landroid/media/RemoteControlClient;

    iget-object v1, p0, Landroid/support/v4/media/x;->l:Landroid/app/PendingIntent;

    invoke-direct {v0, v1}, Landroid/media/RemoteControlClient;-><init>(Landroid/app/PendingIntent;)V

    iput-object v0, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    .line 115
    iget-object v0, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, p0}, Landroid/media/RemoteControlClient;->setOnGetPlaybackPositionListener(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    .line 116
    iget-object v0, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, p0}, Landroid/media/RemoteControlClient;->setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V

    .line 117
    return-void
.end method

.method private h()V
    .registers 3

    .prologue
    .line 120
    iget-boolean v0, p0, Landroid/support/v4/media/x;->n:Z

    if-nez v0, :cond_1d

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/media/x;->n:Z

    .line 122
    iget-object v0, p0, Landroid/support/v4/media/x;->b:Landroid/media/AudioManager;

    iget-object v1, p0, Landroid/support/v4/media/x;->l:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/app/PendingIntent;)V

    .line 123
    iget-object v0, p0, Landroid/support/v4/media/x;->b:Landroid/media/AudioManager;

    iget-object v1, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 124
    iget v0, p0, Landroid/support/v4/media/x;->o:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1d

    .line 125
    invoke-virtual {p0}, Landroid/support/v4/media/x;->a()V

    .line 128
    :cond_1d
    return-void
.end method

.method private i()V
    .registers 3

    .prologue
    const/4 v1, 0x3

    .line 139
    iget v0, p0, Landroid/support/v4/media/x;->o:I

    if-eq v0, v1, :cond_c

    .line 140
    iput v1, p0, Landroid/support/v4/media/x;->o:I

    .line 141
    iget-object v0, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    .line 143
    :cond_c
    iget-boolean v0, p0, Landroid/support/v4/media/x;->n:Z

    if-eqz v0, :cond_13

    .line 144
    invoke-virtual {p0}, Landroid/support/v4/media/x;->a()V

    .line 146
    :cond_13
    return-void
.end method

.method private j()V
    .registers 4

    .prologue
    const/4 v2, 0x2

    .line 167
    iget v0, p0, Landroid/support/v4/media/x;->o:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_d

    .line 168
    iput v2, p0, Landroid/support/v4/media/x;->o:I

    .line 169
    iget-object v0, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v2}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    .line 171
    :cond_d
    invoke-virtual {p0}, Landroid/support/v4/media/x;->b()V

    .line 172
    return-void
.end method

.method private k()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 175
    iget v0, p0, Landroid/support/v4/media/x;->o:I

    if-eq v0, v1, :cond_c

    .line 176
    iput v1, p0, Landroid/support/v4/media/x;->o:I

    .line 177
    iget-object v0, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    .line 179
    :cond_c
    invoke-virtual {p0}, Landroid/support/v4/media/x;->b()V

    .line 180
    return-void
.end method


# virtual methods
.method final a()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 131
    iget-boolean v0, p0, Landroid/support/v4/media/x;->p:Z

    if-nez v0, :cond_f

    .line 132
    iput-boolean v3, p0, Landroid/support/v4/media/x;->p:Z

    .line 133
    iget-object v0, p0, Landroid/support/v4/media/x;->b:Landroid/media/AudioManager;

    iget-object v1, p0, Landroid/support/v4/media/x;->k:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 136
    :cond_f
    return-void
.end method

.method final b()V
    .registers 3

    .prologue
    .line 183
    iget-boolean v0, p0, Landroid/support/v4/media/x;->p:Z

    if-eqz v0, :cond_e

    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/media/x;->p:Z

    .line 185
    iget-object v0, p0, Landroid/support/v4/media/x;->b:Landroid/media/AudioManager;

    iget-object v1, p0, Landroid/support/v4/media/x;->k:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 187
    :cond_e
    return-void
.end method

.method final c()V
    .registers 3

    .prologue
    .line 190
    invoke-virtual {p0}, Landroid/support/v4/media/x;->b()V

    .line 191
    iget-boolean v0, p0, Landroid/support/v4/media/x;->n:Z

    if-eqz v0, :cond_18

    .line 192
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/media/x;->n:Z

    .line 193
    iget-object v0, p0, Landroid/support/v4/media/x;->b:Landroid/media/AudioManager;

    iget-object v1, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 194
    iget-object v0, p0, Landroid/support/v4/media/x;->b:Landroid/media/AudioManager;

    iget-object v1, p0, Landroid/support/v4/media/x;->l:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/app/PendingIntent;)V

    .line 196
    :cond_18
    return-void
.end method

.method final d()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 199
    invoke-virtual {p0}, Landroid/support/v4/media/x;->c()V

    .line 200
    iget-object v0, p0, Landroid/support/v4/media/x;->l:Landroid/app/PendingIntent;

    if-eqz v0, :cond_18

    .line 201
    iget-object v0, p0, Landroid/support/v4/media/x;->a:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/media/x;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 202
    iget-object v0, p0, Landroid/support/v4/media/x;->l:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 203
    iput-object v2, p0, Landroid/support/v4/media/x;->l:Landroid/app/PendingIntent;

    .line 204
    iput-object v2, p0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    .line 206
    :cond_18
    return-void
.end method

.method public final onGetPlaybackPosition()J
    .registers 3

    .prologue
    .line 150
    iget-object v0, p0, Landroid/support/v4/media/x;->d:Landroid/support/v4/media/w;

    invoke-interface {v0}, Landroid/support/v4/media/w;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public final onPlaybackPositionUpdate(J)V
    .registers 3

    .prologue
    .line 156
    return-void
.end method
