.class Landroid/support/v4/media/session/bb;
.super Landroid/media/session/MediaSession$Callback;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/support/v4/media/session/ba;


# direct methods
.method public constructor <init>(Landroid/support/v4/media/session/ba;)V
    .registers 2

    .prologue
    .line 156
    invoke-direct {p0}, Landroid/media/session/MediaSession$Callback;-><init>()V

    .line 157
    iput-object p1, p0, Landroid/support/v4/media/session/bb;->a:Landroid/support/v4/media/session/ba;

    .line 158
    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
    .registers 4

    .prologue
    .line 163
    return-void
.end method

.method public onCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 234
    return-void
.end method

.method public onFastForward()V
    .registers 1

    .prologue
    .line 209
    return-void
.end method

.method public onMediaButtonEvent(Landroid/content/Intent;)Z
    .registers 3

    .prologue
    .line 167
    invoke-super {p0, p1}, Landroid/media/session/MediaSession$Callback;->onMediaButtonEvent(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public onPause()V
    .registers 1

    .prologue
    .line 194
    return-void
.end method

.method public onPlay()V
    .registers 1

    .prologue
    .line 174
    return-void
.end method

.method public onPlayFromMediaId(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 179
    return-void
.end method

.method public onPlayFromSearch(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 184
    return-void
.end method

.method public onRewind()V
    .registers 1

    .prologue
    .line 214
    return-void
.end method

.method public onSeekTo(J)V
    .registers 3

    .prologue
    .line 224
    return-void
.end method

.method public onSetRating(Landroid/media/Rating;)V
    .registers 3

    .prologue
    .line 228
    iget-object v0, p0, Landroid/support/v4/media/session/bb;->a:Landroid/support/v4/media/session/ba;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ba;->a(Ljava/lang/Object;)V

    .line 229
    return-void
.end method

.method public onSkipToNext()V
    .registers 1

    .prologue
    .line 199
    return-void
.end method

.method public onSkipToPrevious()V
    .registers 1

    .prologue
    .line 204
    return-void
.end method

.method public onSkipToQueueItem(J)V
    .registers 3

    .prologue
    .line 189
    return-void
.end method

.method public onStop()V
    .registers 1

    .prologue
    .line 219
    return-void
.end method
