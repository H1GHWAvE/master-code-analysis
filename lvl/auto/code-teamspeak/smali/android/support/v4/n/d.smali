.class public final Landroid/support/v4/n/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:[Ljava/lang/Object;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/n/d;-><init>(B)V

    .line 48
    return-void
.end method

.method private constructor <init>(B)V
    .registers 5

    .prologue
    const/4 v2, 0x1

    const/16 v0, 0x8

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {v0}, Ljava/lang/Integer;->bitCount(I)I

    move-result v1

    if-eq v1, v2, :cond_14

    .line 63
    invoke-static {v0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    shl-int v0, v2, v0

    .line 65
    :cond_14
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Landroid/support/v4/n/d;->d:I

    .line 66
    new-array v0, v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    .line 67
    return-void
.end method

.method private a()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 28
    iget-object v0, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    array-length v1, v0

    .line 29
    iget v0, p0, Landroid/support/v4/n/d;->b:I

    sub-int v2, v1, v0

    .line 30
    shl-int/lit8 v3, v1, 0x1

    .line 31
    if-gez v3, :cond_14

    .line 32
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Max array capacity exceeded"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_14
    new-array v0, v3, [Ljava/lang/Object;

    .line 35
    iget-object v4, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    iget v5, p0, Landroid/support/v4/n/d;->b:I

    invoke-static {v4, v5, v0, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 36
    iget-object v4, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    iget v5, p0, Landroid/support/v4/n/d;->b:I

    invoke-static {v4, v6, v0, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 37
    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    .line 38
    iput v6, p0, Landroid/support/v4/n/d;->b:I

    .line 39
    iput v1, p0, Landroid/support/v4/n/d;->c:I

    .line 40
    add-int/lit8 v0, v3, -0x1

    iput v0, p0, Landroid/support/v4/n/d;->d:I

    .line 41
    return-void
.end method

.method private a(I)V
    .registers 6

    .prologue
    const/4 v3, 0x0

    .line 139
    if-gtz p1, :cond_4

    .line 162
    :cond_3
    :goto_3
    return-void

    .line 142
    :cond_4
    invoke-direct {p0}, Landroid/support/v4/n/d;->g()I

    move-result v0

    if-le p1, v0, :cond_10

    .line 143
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 145
    :cond_10
    iget-object v0, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    array-length v0, v0

    .line 146
    iget v1, p0, Landroid/support/v4/n/d;->b:I

    sub-int v1, v0, v1

    if-ge p1, v1, :cond_1c

    .line 147
    iget v0, p0, Landroid/support/v4/n/d;->b:I

    add-int/2addr v0, p1

    .line 149
    :cond_1c
    iget v1, p0, Landroid/support/v4/n/d;->b:I

    :goto_1e
    if-ge v1, v0, :cond_27

    .line 150
    iget-object v2, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    aput-object v3, v2, v1

    .line 149
    add-int/lit8 v1, v1, 0x1

    goto :goto_1e

    .line 152
    :cond_27
    iget v1, p0, Landroid/support/v4/n/d;->b:I

    sub-int/2addr v0, v1

    .line 153
    sub-int v1, p1, v0

    .line 154
    iget v2, p0, Landroid/support/v4/n/d;->b:I

    add-int/2addr v0, v2

    iget v2, p0, Landroid/support/v4/n/d;->d:I

    and-int/2addr v0, v2

    iput v0, p0, Landroid/support/v4/n/d;->b:I

    .line 155
    if-lez v1, :cond_3

    .line 157
    const/4 v0, 0x0

    :goto_37
    if-ge v0, v1, :cond_40

    .line 158
    iget-object v2, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    aput-object v3, v2, v0

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_37

    .line 160
    :cond_40
    iput v1, p0, Landroid/support/v4/n/d;->b:I

    goto :goto_3
.end method

.method private a(Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 74
    iget v0, p0, Landroid/support/v4/n/d;->b:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Landroid/support/v4/n/d;->d:I

    and-int/2addr v0, v1

    iput v0, p0, Landroid/support/v4/n/d;->b:I

    .line 75
    iget-object v0, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    iget v1, p0, Landroid/support/v4/n/d;->b:I

    aput-object p1, v0, v1

    .line 76
    iget v0, p0, Landroid/support/v4/n/d;->b:I

    iget v1, p0, Landroid/support/v4/n/d;->c:I

    if-ne v0, v1, :cond_18

    .line 77
    invoke-direct {p0}, Landroid/support/v4/n/d;->a()V

    .line 79
    :cond_18
    return-void
.end method

.method private b()Ljava/lang/Object;
    .registers 5

    .prologue
    .line 99
    iget v0, p0, Landroid/support/v4/n/d;->b:I

    iget v1, p0, Landroid/support/v4/n/d;->c:I

    if-ne v0, v1, :cond_c

    .line 100
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 102
    :cond_c
    iget-object v0, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    iget v1, p0, Landroid/support/v4/n/d;->b:I

    aget-object v0, v0, v1

    .line 103
    iget-object v1, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    iget v2, p0, Landroid/support/v4/n/d;->b:I

    const/4 v3, 0x0

    aput-object v3, v1, v2

    .line 104
    iget v1, p0, Landroid/support/v4/n/d;->b:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Landroid/support/v4/n/d;->d:I

    and-int/2addr v1, v2

    iput v1, p0, Landroid/support/v4/n/d;->b:I

    .line 105
    return-object v0
.end method

.method private b(I)V
    .registers 6

    .prologue
    const/4 v3, 0x0

    .line 172
    if-gtz p1, :cond_4

    .line 197
    :cond_3
    :goto_3
    return-void

    .line 175
    :cond_4
    invoke-direct {p0}, Landroid/support/v4/n/d;->g()I

    move-result v0

    if-le p1, v0, :cond_10

    .line 176
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 178
    :cond_10
    const/4 v0, 0x0

    .line 179
    iget v1, p0, Landroid/support/v4/n/d;->c:I

    if-ge p1, v1, :cond_18

    .line 180
    iget v0, p0, Landroid/support/v4/n/d;->c:I

    sub-int/2addr v0, p1

    :cond_18
    move v1, v0

    .line 182
    :goto_19
    iget v2, p0, Landroid/support/v4/n/d;->c:I

    if-ge v1, v2, :cond_24

    .line 183
    iget-object v2, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    aput-object v3, v2, v1

    .line 182
    add-int/lit8 v1, v1, 0x1

    goto :goto_19

    .line 185
    :cond_24
    iget v1, p0, Landroid/support/v4/n/d;->c:I

    sub-int v0, v1, v0

    .line 186
    sub-int v1, p1, v0

    .line 187
    iget v2, p0, Landroid/support/v4/n/d;->c:I

    sub-int v0, v2, v0

    iput v0, p0, Landroid/support/v4/n/d;->c:I

    .line 188
    if-lez v1, :cond_3

    .line 190
    iget-object v0, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    array-length v0, v0

    iput v0, p0, Landroid/support/v4/n/d;->c:I

    .line 191
    iget v0, p0, Landroid/support/v4/n/d;->c:I

    sub-int v1, v0, v1

    move v0, v1

    .line 192
    :goto_3c
    iget v2, p0, Landroid/support/v4/n/d;->c:I

    if-ge v0, v2, :cond_47

    .line 193
    iget-object v2, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    aput-object v3, v2, v0

    .line 192
    add-int/lit8 v0, v0, 0x1

    goto :goto_3c

    .line 195
    :cond_47
    iput v1, p0, Landroid/support/v4/n/d;->c:I

    goto :goto_3
.end method

.method private b(Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    iget v1, p0, Landroid/support/v4/n/d;->c:I

    aput-object p1, v0, v1

    .line 87
    iget v0, p0, Landroid/support/v4/n/d;->c:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Landroid/support/v4/n/d;->d:I

    and-int/2addr v0, v1

    iput v0, p0, Landroid/support/v4/n/d;->c:I

    .line 88
    iget v0, p0, Landroid/support/v4/n/d;->c:I

    iget v1, p0, Landroid/support/v4/n/d;->b:I

    if-ne v0, v1, :cond_18

    .line 89
    invoke-direct {p0}, Landroid/support/v4/n/d;->a()V

    .line 91
    :cond_18
    return-void
.end method

.method private c()Ljava/lang/Object;
    .registers 5

    .prologue
    .line 114
    iget v0, p0, Landroid/support/v4/n/d;->b:I

    iget v1, p0, Landroid/support/v4/n/d;->c:I

    if-ne v0, v1, :cond_c

    .line 115
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 117
    :cond_c
    iget v0, p0, Landroid/support/v4/n/d;->c:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Landroid/support/v4/n/d;->d:I

    and-int/2addr v0, v1

    .line 118
    iget-object v1, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    .line 119
    iget-object v2, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    .line 120
    iput v0, p0, Landroid/support/v4/n/d;->c:I

    .line 121
    return-object v1
.end method

.method private c(I)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 230
    if-ltz p1, :cond_8

    invoke-direct {p0}, Landroid/support/v4/n/d;->g()I

    move-result v0

    if-lt p1, v0, :cond_e

    .line 231
    :cond_8
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 233
    :cond_e
    iget-object v0, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    iget v1, p0, Landroid/support/v4/n/d;->b:I

    add-int/2addr v1, p1

    iget v2, p0, Landroid/support/v4/n/d;->d:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method private d()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 128
    invoke-direct {p0}, Landroid/support/v4/n/d;->g()I

    move-result v2

    .line 1139
    if-lez v2, :cond_45

    .line 1142
    invoke-direct {p0}, Landroid/support/v4/n/d;->g()I

    move-result v0

    if-le v2, v0, :cond_13

    .line 1143
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1145
    :cond_13
    iget-object v0, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    array-length v0, v0

    .line 1146
    iget v1, p0, Landroid/support/v4/n/d;->b:I

    sub-int v1, v0, v1

    if-ge v2, v1, :cond_1f

    .line 1147
    iget v0, p0, Landroid/support/v4/n/d;->b:I

    add-int/2addr v0, v2

    .line 1149
    :cond_1f
    iget v1, p0, Landroid/support/v4/n/d;->b:I

    :goto_21
    if-ge v1, v0, :cond_2a

    .line 1150
    iget-object v3, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    aput-object v4, v3, v1

    .line 1149
    add-int/lit8 v1, v1, 0x1

    goto :goto_21

    .line 1152
    :cond_2a
    iget v1, p0, Landroid/support/v4/n/d;->b:I

    sub-int/2addr v0, v1

    .line 1153
    sub-int v1, v2, v0

    .line 1154
    iget v2, p0, Landroid/support/v4/n/d;->b:I

    add-int/2addr v0, v2

    iget v2, p0, Landroid/support/v4/n/d;->d:I

    and-int/2addr v0, v2

    iput v0, p0, Landroid/support/v4/n/d;->b:I

    .line 1155
    if-lez v1, :cond_45

    .line 1157
    const/4 v0, 0x0

    :goto_3a
    if-ge v0, v1, :cond_43

    .line 1158
    iget-object v2, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    aput-object v4, v2, v0

    .line 1157
    add-int/lit8 v0, v0, 0x1

    goto :goto_3a

    .line 1160
    :cond_43
    iput v1, p0, Landroid/support/v4/n/d;->b:I

    .line 129
    :cond_45
    return-void
.end method

.method private e()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 205
    iget v0, p0, Landroid/support/v4/n/d;->b:I

    iget v1, p0, Landroid/support/v4/n/d;->c:I

    if-ne v0, v1, :cond_c

    .line 206
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 208
    :cond_c
    iget-object v0, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    iget v1, p0, Landroid/support/v4/n/d;->b:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method private f()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 217
    iget v0, p0, Landroid/support/v4/n/d;->b:I

    iget v1, p0, Landroid/support/v4/n/d;->c:I

    if-ne v0, v1, :cond_c

    .line 218
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 220
    :cond_c
    iget-object v0, p0, Landroid/support/v4/n/d;->a:[Ljava/lang/Object;

    iget v1, p0, Landroid/support/v4/n/d;->c:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Landroid/support/v4/n/d;->d:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method private g()I
    .registers 3

    .prologue
    .line 241
    iget v0, p0, Landroid/support/v4/n/d;->c:I

    iget v1, p0, Landroid/support/v4/n/d;->b:I

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/support/v4/n/d;->d:I

    and-int/2addr v0, v1

    return v0
.end method

.method private h()Z
    .registers 3

    .prologue
    .line 249
    iget v0, p0, Landroid/support/v4/n/d;->b:I

    iget v1, p0, Landroid/support/v4/n/d;->c:I

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method
