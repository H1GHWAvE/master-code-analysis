.class final Landroid/support/v4/j/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/j/h;


# instance fields
.field private final a:Landroid/support/v4/j/i;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    new-instance v0, Landroid/support/v4/j/i;

    invoke-direct {v0, p1}, Landroid/support/v4/j/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v4/j/d;->a:Landroid/support/v4/j/i;

    .line 161
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 170
    iget-object v0, p0, Landroid/support/v4/j/d;->a:Landroid/support/v4/j/i;

    .line 1121
    iget v0, v0, Landroid/support/v4/j/i;->j:I

    .line 170
    return v0
.end method

.method public final a(I)V
    .registers 3

    .prologue
    .line 165
    iget-object v0, p0, Landroid/support/v4/j/d;->a:Landroid/support/v4/j/i;

    .line 1111
    iput p1, v0, Landroid/support/v4/j/i;->j:I

    .line 166
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/support/v4/j/c;)V
    .registers 12

    .prologue
    .line 196
    const/4 v5, 0x0

    .line 197
    if-eqz p3, :cond_8

    .line 198
    new-instance v5, Landroid/support/v4/j/e;

    invoke-direct {v5, p0, p3}, Landroid/support/v4/j/e;-><init>(Landroid/support/v4/j/d;Landroid/support/v4/j/c;)V

    .line 205
    :cond_8
    iget-object v1, p0, Landroid/support/v4/j/d;->a:Landroid/support/v4/j/i;

    .line 2175
    if-eqz p2, :cond_44

    .line 2178
    iget v4, v1, Landroid/support/v4/j/i;->j:I

    .line 2179
    iget-object v0, v1, Landroid/support/v4/j/i;->a:Landroid/content/Context;

    const-string v2, "print"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/print/PrintManager;

    .line 2180
    sget-object v0, Landroid/print/PrintAttributes$MediaSize;->UNKNOWN_PORTRAIT:Landroid/print/PrintAttributes$MediaSize;

    .line 2181
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-le v2, v3, :cond_27

    .line 2182
    sget-object v0, Landroid/print/PrintAttributes$MediaSize;->UNKNOWN_LANDSCAPE:Landroid/print/PrintAttributes$MediaSize;

    .line 2184
    :cond_27
    new-instance v2, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v2}, Landroid/print/PrintAttributes$Builder;-><init>()V

    invoke-virtual {v2, v0}, Landroid/print/PrintAttributes$Builder;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;

    move-result-object v0

    iget v2, v1, Landroid/support/v4/j/i;->k:I

    invoke-virtual {v0, v2}, Landroid/print/PrintAttributes$Builder;->setColorMode(I)Landroid/print/PrintAttributes$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v7

    .line 2189
    new-instance v0, Landroid/support/v4/j/j;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/j/j;-><init>(Landroid/support/v4/j/i;Ljava/lang/String;Landroid/graphics/Bitmap;ILandroid/support/v4/j/n;)V

    invoke-virtual {v6, p1, v0, v7}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    .line 206
    :cond_44
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/c;)V
    .registers 10

    .prologue
    .line 211
    const/4 v4, 0x0

    .line 212
    if-eqz p3, :cond_8

    .line 213
    new-instance v4, Landroid/support/v4/j/f;

    invoke-direct {v4, p0, p3}, Landroid/support/v4/j/f;-><init>(Landroid/support/v4/j/d;Landroid/support/v4/j/c;)V

    .line 220
    :cond_8
    iget-object v1, p0, Landroid/support/v4/j/d;->a:Landroid/support/v4/j/i;

    .line 2314
    iget v5, v1, Landroid/support/v4/j/i;->j:I

    .line 2316
    new-instance v0, Landroid/support/v4/j/k;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/j/k;-><init>(Landroid/support/v4/j/i;Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/n;I)V

    .line 2477
    iget-object v2, v1, Landroid/support/v4/j/i;->a:Landroid/content/Context;

    const-string v3, "print"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/print/PrintManager;

    .line 2478
    new-instance v3, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v3}, Landroid/print/PrintAttributes$Builder;-><init>()V

    .line 2479
    iget v4, v1, Landroid/support/v4/j/i;->k:I

    invoke-virtual {v3, v4}, Landroid/print/PrintAttributes$Builder;->setColorMode(I)Landroid/print/PrintAttributes$Builder;

    .line 2481
    iget v4, v1, Landroid/support/v4/j/i;->l:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_39

    .line 2482
    sget-object v1, Landroid/print/PrintAttributes$MediaSize;->UNKNOWN_LANDSCAPE:Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual {v3, v1}, Landroid/print/PrintAttributes$Builder;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;

    .line 2486
    :cond_31
    :goto_31
    invoke-virtual {v3}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v1

    .line 2488
    invoke-virtual {v2, p1, v0, v1}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    .line 221
    return-void

    .line 2483
    :cond_39
    iget v1, v1, Landroid/support/v4/j/i;->l:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_31

    .line 2484
    sget-object v1, Landroid/print/PrintAttributes$MediaSize;->UNKNOWN_PORTRAIT:Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual {v3, v1}, Landroid/print/PrintAttributes$Builder;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;

    goto :goto_31
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 180
    iget-object v0, p0, Landroid/support/v4/j/d;->a:Landroid/support/v4/j/i;

    .line 1163
    iget v0, v0, Landroid/support/v4/j/i;->k:I

    .line 180
    return v0
.end method

.method public final b(I)V
    .registers 3

    .prologue
    .line 175
    iget-object v0, p0, Landroid/support/v4/j/d;->a:Landroid/support/v4/j/i;

    .line 1133
    iput p1, v0, Landroid/support/v4/j/i;->k:I

    .line 176
    return-void
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 190
    iget-object v0, p0, Landroid/support/v4/j/d;->a:Landroid/support/v4/j/i;

    .line 2153
    iget v0, v0, Landroid/support/v4/j/i;->l:I

    .line 190
    return v0
.end method

.method public final c(I)V
    .registers 3

    .prologue
    .line 185
    iget-object v0, p0, Landroid/support/v4/j/d;->a:Landroid/support/v4/j/i;

    .line 2143
    iput p1, v0, Landroid/support/v4/j/i;->l:I

    .line 186
    return-void
.end method
