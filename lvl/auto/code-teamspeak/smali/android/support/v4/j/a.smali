.class public final Landroid/support/v4/j/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x1

.field public static final b:I = 0x2

.field public static final c:I = 0x1

.field public static final d:I = 0x2

.field public static final e:I = 0x1

.field public static final f:I = 0x2


# instance fields
.field g:Landroid/support/v4/j/h;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1081
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_15

    .line 1083
    const/4 v0, 0x1

    .line 231
    :goto_b
    if-eqz v0, :cond_17

    .line 232
    new-instance v0, Landroid/support/v4/j/d;

    invoke-direct {v0, p1}, Landroid/support/v4/j/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v4/j/a;->g:Landroid/support/v4/j/h;

    .line 236
    :goto_14
    return-void

    :cond_15
    move v0, v1

    .line 1085
    goto :goto_b

    .line 234
    :cond_17
    new-instance v0, Landroid/support/v4/j/g;

    invoke-direct {v0, v1}, Landroid/support/v4/j/g;-><init>(B)V

    iput-object v0, p0, Landroid/support/v4/j/a;->g:Landroid/support/v4/j/h;

    goto :goto_14
.end method

.method private a(I)V
    .registers 3

    .prologue
    .line 248
    iget-object v0, p0, Landroid/support/v4/j/a;->g:Landroid/support/v4/j/h;

    invoke-interface {v0, p1}, Landroid/support/v4/j/h;->a(I)V

    .line 249
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .registers 5

    .prologue
    .line 312
    iget-object v0, p0, Landroid/support/v4/j/a;->g:Landroid/support/v4/j/h;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Landroid/support/v4/j/h;->a(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/support/v4/j/c;)V

    .line 313
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/support/v4/j/c;)V
    .registers 5

    .prologue
    .line 323
    iget-object v0, p0, Landroid/support/v4/j/a;->g:Landroid/support/v4/j/h;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/j/h;->a(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/support/v4/j/c;)V

    .line 324
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;)V
    .registers 5

    .prologue
    .line 336
    iget-object v0, p0, Landroid/support/v4/j/a;->g:Landroid/support/v4/j/h;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Landroid/support/v4/j/h;->a(Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/c;)V

    .line 337
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/c;)V
    .registers 5

    .prologue
    .line 351
    iget-object v0, p0, Landroid/support/v4/j/a;->g:Landroid/support/v4/j/h;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/j/h;->a(Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/c;)V

    .line 352
    return-void
.end method

.method private static a()Z
    .registers 2

    .prologue
    .line 81
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_8

    .line 83
    const/4 v0, 0x1

    .line 85
    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private b()I
    .registers 2

    .prologue
    .line 258
    iget-object v0, p0, Landroid/support/v4/j/a;->g:Landroid/support/v4/j/h;

    invoke-interface {v0}, Landroid/support/v4/j/h;->a()I

    move-result v0

    return v0
.end method

.method private b(I)V
    .registers 3

    .prologue
    .line 270
    iget-object v0, p0, Landroid/support/v4/j/a;->g:Landroid/support/v4/j/h;

    invoke-interface {v0, p1}, Landroid/support/v4/j/h;->b(I)V

    .line 271
    return-void
.end method

.method private c()I
    .registers 2

    .prologue
    .line 280
    iget-object v0, p0, Landroid/support/v4/j/a;->g:Landroid/support/v4/j/h;

    invoke-interface {v0}, Landroid/support/v4/j/h;->b()I

    move-result v0

    return v0
.end method

.method private c(I)V
    .registers 3

    .prologue
    .line 291
    iget-object v0, p0, Landroid/support/v4/j/a;->g:Landroid/support/v4/j/h;

    invoke-interface {v0, p1}, Landroid/support/v4/j/h;->c(I)V

    .line 292
    return-void
.end method

.method private d()I
    .registers 2

    .prologue
    .line 301
    iget-object v0, p0, Landroid/support/v4/j/a;->g:Landroid/support/v4/j/h;

    invoke-interface {v0}, Landroid/support/v4/j/h;->c()I

    move-result v0

    return v0
.end method
