.class public final Landroid/support/v4/c/av;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Landroid/support/v4/c/av;


# instance fields
.field private final b:Landroid/support/v4/c/ay;


# direct methods
.method private constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_12

    .line 53
    new-instance v0, Landroid/support/v4/c/aw;

    invoke-direct {v0, v2}, Landroid/support/v4/c/aw;-><init>(B)V

    iput-object v0, p0, Landroid/support/v4/c/av;->b:Landroid/support/v4/c/ay;

    .line 57
    :goto_11
    return-void

    .line 55
    :cond_12
    new-instance v0, Landroid/support/v4/c/ax;

    invoke-direct {v0, v2}, Landroid/support/v4/c/ax;-><init>(B)V

    iput-object v0, p0, Landroid/support/v4/c/av;->b:Landroid/support/v4/c/ay;

    goto :goto_11
.end method

.method private static a()Landroid/support/v4/c/av;
    .registers 1

    .prologue
    .line 60
    sget-object v0, Landroid/support/v4/c/av;->a:Landroid/support/v4/c/av;

    if-nez v0, :cond_b

    .line 61
    new-instance v0, Landroid/support/v4/c/av;

    invoke-direct {v0}, Landroid/support/v4/c/av;-><init>()V

    sput-object v0, Landroid/support/v4/c/av;->a:Landroid/support/v4/c/av;

    .line 63
    :cond_b
    sget-object v0, Landroid/support/v4/c/av;->a:Landroid/support/v4/c/av;

    return-object v0
.end method

.method private a(Landroid/content/SharedPreferences$Editor;)V
    .registers 3
    .param p1    # Landroid/content/SharedPreferences$Editor;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 67
    iget-object v0, p0, Landroid/support/v4/c/av;->b:Landroid/support/v4/c/ay;

    invoke-interface {v0, p1}, Landroid/support/v4/c/ay;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 68
    return-void
.end method
