.class public Landroid/support/v4/app/dm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final D:I = 0x1400


# instance fields
.field A:Landroid/app/Notification;

.field public B:Landroid/app/Notification;

.field public C:Ljava/util/ArrayList;

.field public a:Landroid/content/Context;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Landroid/app/PendingIntent;

.field e:Landroid/app/PendingIntent;

.field f:Landroid/widget/RemoteViews;

.field public g:Landroid/graphics/Bitmap;

.field public h:Ljava/lang/CharSequence;

.field public i:I

.field j:I

.field k:Z

.field public l:Z

.field public m:Landroid/support/v4/app/ed;

.field public n:Ljava/lang/CharSequence;

.field o:I

.field p:I

.field q:Z

.field r:Ljava/lang/String;

.field s:Z

.field t:Ljava/lang/String;

.field public u:Ljava/util/ArrayList;

.field v:Z

.field w:Ljava/lang/String;

.field x:Landroid/os/Bundle;

.field y:I

.field z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 898
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/dm;->k:Z

    .line 912
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/dm;->u:Ljava/util/ArrayList;

    .line 913
    iput-boolean v4, p0, Landroid/support/v4/app/dm;->v:Z

    .line 916
    iput v4, p0, Landroid/support/v4/app/dm;->y:I

    .line 917
    iput v4, p0, Landroid/support/v4/app/dm;->z:I

    .line 921
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    .line 936
    iput-object p1, p0, Landroid/support/v4/app/dm;->a:Landroid/content/Context;

    .line 939
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    .line 940
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 941
    iput v4, p0, Landroid/support/v4/app/dm;->j:I

    .line 942
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/dm;->C:Ljava/util/ArrayList;

    .line 943
    return-void
.end method

.method private a(II)Landroid/support/v4/app/dm;
    .registers 4

    .prologue
    .line 1002
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 1003
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput p2, v0, Landroid/app/Notification;->iconLevel:I

    .line 1004
    return-object p0
.end method

.method private a(III)Landroid/support/v4/app/dm;
    .registers 9
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1204
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->ledARGB:I

    .line 1205
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput p2, v0, Landroid/app/Notification;->ledOnMS:I

    .line 1206
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput p3, v0, Landroid/app/Notification;->ledOffMS:I

    .line 1207
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iget v0, v0, Landroid/app/Notification;->ledOnMS:I

    if-eqz v0, :cond_2a

    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iget v0, v0, Landroid/app/Notification;->ledOffMS:I

    if-eqz v0, :cond_2a

    move v0, v1

    .line 1208
    :goto_1b
    iget-object v3, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iget-object v4, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iget v4, v4, Landroid/app/Notification;->flags:I

    and-int/lit8 v4, v4, -0x2

    if-eqz v0, :cond_2c

    :goto_25
    or-int v0, v4, v1

    iput v0, v3, Landroid/app/Notification;->flags:I

    .line 1210
    return-object p0

    :cond_2a
    move v0, v2

    .line 1207
    goto :goto_1b

    :cond_2c
    move v1, v2

    .line 1208
    goto :goto_25
.end method

.method private a(IIZ)Landroid/support/v4/app/dm;
    .registers 4

    .prologue
    .line 1060
    iput p1, p0, Landroid/support/v4/app/dm;->o:I

    .line 1061
    iput p2, p0, Landroid/support/v4/app/dm;->p:I

    .line 1062
    iput-boolean p3, p0, Landroid/support/v4/app/dm;->q:Z

    .line 1063
    return-object p0
.end method

.method private a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/dm;
    .registers 6

    .prologue
    .line 1461
    iget-object v0, p0, Landroid/support/v4/app/dm;->u:Ljava/util/ArrayList;

    new-instance v1, Landroid/support/v4/app/df;

    invoke-direct {v1, p1, p2, p3}, Landroid/support/v4/app/df;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1462
    return-object p0
.end method

.method private a(Landroid/app/Notification;)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1535
    iput-object p1, p0, Landroid/support/v4/app/dm;->A:Landroid/app/Notification;

    .line 1536
    return-object p0
.end method

.method private a(Landroid/app/PendingIntent;Z)Landroid/support/v4/app/dm;
    .registers 4

    .prologue
    .line 1118
    iput-object p1, p0, Landroid/support/v4/app/dm;->e:Landroid/app/PendingIntent;

    .line 1119
    const/16 v0, 0x80

    invoke-virtual {p0, v0, p2}, Landroid/support/v4/app/dm;->a(IZ)V

    .line 1120
    return-object p0
.end method

.method private a(Landroid/net/Uri;)Landroid/support/v4/app/dm;
    .registers 4

    .prologue
    .line 1160
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 1161
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 1162
    return-object p0
.end method

.method private a(Landroid/net/Uri;I)Landroid/support/v4/app/dm;
    .registers 4

    .prologue
    .line 1177
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 1178
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput p2, v0, Landroid/app/Notification;->audioStreamType:I

    .line 1179
    return-object p0
.end method

.method private a(Landroid/os/Bundle;)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1400
    if-eqz p1, :cond_d

    .line 1401
    iget-object v0, p0, Landroid/support/v4/app/dm;->x:Landroid/os/Bundle;

    if-nez v0, :cond_e

    .line 1402
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Landroid/support/v4/app/dm;->x:Landroid/os/Bundle;

    .line 1407
    :cond_d
    :goto_d
    return-object p0

    .line 1404
    :cond_e
    iget-object v0, p0, Landroid/support/v4/app/dm;->x:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_d
.end method

.method private a(Landroid/support/v4/app/df;)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1480
    iget-object v0, p0, Landroid/support/v4/app/dm;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1481
    return-object p0
.end method

.method private a(Landroid/support/v4/app/ds;)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1544
    invoke-interface {p1, p0}, Landroid/support/v4/app/ds;->a(Landroid/support/v4/app/dm;)Landroid/support/v4/app/dm;

    .line 1545
    return-object p0
.end method

.method private a(Landroid/support/v4/app/ed;)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1493
    iget-object v0, p0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    if-eq v0, p1, :cond_f

    .line 1494
    iput-object p1, p0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    .line 1495
    iget-object v0, p0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    if-eqz v0, :cond_f

    .line 1496
    iget-object v0, p0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    invoke-virtual {v0, p0}, Landroid/support/v4/app/ed;->a(Landroid/support/v4/app/dm;)V

    .line 1499
    :cond_f
    return-object p0
.end method

.method private a(Landroid/widget/RemoteViews;)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1070
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 1071
    return-object p0
.end method

.method private a(Ljava/lang/CharSequence;Landroid/widget/RemoteViews;)Landroid/support/v4/app/dm;
    .registers 5

    .prologue
    .line 1138
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    invoke-static {p1}, Landroid/support/v4/app/dm;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 1139
    iput-object p2, p0, Landroid/support/v4/app/dm;->f:Landroid/widget/RemoteViews;

    .line 1140
    return-object p0
.end method

.method private a(Ljava/lang/String;)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1268
    iput-object p1, p0, Landroid/support/v4/app/dm;->w:Ljava/lang/String;

    .line 1269
    return-object p0
.end method

.method private a(Z)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 959
    iput-boolean p1, p0, Landroid/support/v4/app/dm;->k:Z

    .line 960
    return-object p0
.end method

.method private a([J)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1194
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->vibrate:[J

    .line 1195
    return-object p0
.end method

.method private b(I)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1043
    iput p1, p0, Landroid/support/v4/app/dm;->i:I

    .line 1044
    return-object p0
.end method

.method private b(Landroid/app/PendingIntent;)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1095
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 1096
    return-object p0
.end method

.method private b(Landroid/os/Bundle;)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1423
    iput-object p1, p0, Landroid/support/v4/app/dm;->x:Landroid/os/Bundle;

    .line 1424
    return-object p0
.end method

.method private b(Ljava/lang/String;)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1343
    iget-object v0, p0, Landroid/support/v4/app/dm;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1344
    return-object p0
.end method

.method private b(Z)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 975
    iput-boolean p1, p0, Landroid/support/v4/app/dm;->l:Z

    .line 976
    return-object p0
.end method

.method private c(I)Landroid/support/v4/app/dm;
    .registers 4

    .prologue
    .line 1283
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->defaults:I

    .line 1284
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_10

    .line 1285
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1287
    :cond_10
    return-object p0
.end method

.method private c(Ljava/lang/String;)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1359
    iput-object p1, p0, Landroid/support/v4/app/dm;->r:Ljava/lang/String;

    .line 1360
    return-object p0
.end method

.method private c(Z)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1234
    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Landroid/support/v4/app/dm;->a(IZ)V

    .line 1235
    return-object p0
.end method

.method private d(I)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1316
    iput p1, p0, Landroid/support/v4/app/dm;->j:I

    .line 1317
    return-object p0
.end method

.method private d(Ljava/lang/String;)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1388
    iput-object p1, p0, Landroid/support/v4/app/dm;->t:Ljava/lang/String;

    .line 1389
    return-object p0
.end method

.method private d(Z)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1256
    iput-boolean p1, p0, Landroid/support/v4/app/dm;->v:Z

    .line 1257
    return-object p0
.end method

.method protected static d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    const/16 v1, 0x1400

    .line 1572
    if-nez p0, :cond_5

    .line 1576
    :cond_4
    :goto_4
    return-object p0

    .line 1573
    :cond_5
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_4

    .line 1574
    const/4 v0, 0x0

    invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_4
.end method

.method private e()Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1225
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/dm;->a(IZ)V

    .line 1226
    return-object p0
.end method

.method private e(I)Landroid/support/v4/app/dm;
    .registers 2
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param

    .prologue
    .line 1510
    iput p1, p0, Landroid/support/v4/app/dm;->y:I

    .line 1511
    return-object p0
.end method

.method private e(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1033
    invoke-static {p1}, Landroid/support/v4/app/dm;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/dm;->n:Ljava/lang/CharSequence;

    .line 1034
    return-object p0
.end method

.method private e(Z)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1371
    iput-boolean p1, p0, Landroid/support/v4/app/dm;->s:Z

    .line 1372
    return-object p0
.end method

.method private f()Landroid/app/Notification;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1553
    invoke-virtual {p0}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method private f(I)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1522
    iput p1, p0, Landroid/support/v4/app/dm;->z:I

    .line 1523
    return-object p0
.end method

.method private f(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1051
    invoke-static {p1}, Landroid/support/v4/app/dm;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/dm;->h:Ljava/lang/CharSequence;

    .line 1052
    return-object p0
.end method


# virtual methods
.method public final a()Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1245
    const/16 v0, 0x10

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/dm;->a(IZ)V

    .line 1246
    return-object p0
.end method

.method public final a(I)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 987
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 988
    return-object p0
.end method

.method public final a(J)Landroid/support/v4/app/dm;
    .registers 4

    .prologue
    .line 950
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iput-wide p1, v0, Landroid/app/Notification;->when:J

    .line 951
    return-object p0
.end method

.method public final a(Landroid/app/PendingIntent;)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1083
    iput-object p1, p0, Landroid/support/v4/app/dm;->d:Landroid/app/PendingIntent;

    .line 1084
    return-object p0
.end method

.method public final a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/dm;
    .registers 2

    .prologue
    .line 1147
    iput-object p1, p0, Landroid/support/v4/app/dm;->g:Landroid/graphics/Bitmap;

    .line 1148
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1011
    invoke-static {p1}, Landroid/support/v4/app/dm;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/dm;->b:Ljava/lang/CharSequence;

    .line 1012
    return-object p0
.end method

.method public final a(IZ)V
    .registers 6

    .prologue
    .line 1291
    if-eqz p2, :cond_a

    .line 1292
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1296
    :goto_9
    return-void

    .line 1294
    :cond_a
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_9
.end method

.method public final b()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 1438
    iget-object v0, p0, Landroid/support/v4/app/dm;->x:Landroid/os/Bundle;

    if-nez v0, :cond_b

    .line 1439
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/dm;->x:Landroid/os/Bundle;

    .line 1441
    :cond_b
    iget-object v0, p0, Landroid/support/v4/app/dm;->x:Landroid/os/Bundle;

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
    .registers 3

    .prologue
    .line 1019
    invoke-static {p1}, Landroid/support/v4/app/dm;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/dm;->c:Ljava/lang/CharSequence;

    .line 1020
    return-object p0
.end method

.method public final c()Landroid/app/Notification;
    .registers 3

    .prologue
    .line 1561
    invoke-static {}, Landroid/support/v4/app/dd;->a()Landroid/support/v4/app/du;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/dm;->d()Landroid/support/v4/app/dn;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Landroid/support/v4/app/du;->a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dn;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
    .registers 4

    .prologue
    .line 1128
    iget-object v0, p0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    invoke-static {p1}, Landroid/support/v4/app/dm;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 1129
    return-object p0
.end method

.method public d()Landroid/support/v4/app/dn;
    .registers 2

    .prologue
    .line 1568
    new-instance v0, Landroid/support/v4/app/dn;

    invoke-direct {v0}, Landroid/support/v4/app/dn;-><init>()V

    return-object v0
.end method
