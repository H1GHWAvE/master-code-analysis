.class public final Landroid/support/v4/app/gl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Iterable;


# static fields
.field private static final c:Ljava/lang/String; = "TaskStackBuilder"

.field private static final d:Landroid/support/v4/app/gn;


# instance fields
.field public final a:Ljava/util/ArrayList;

.field public final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 115
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_e

    .line 116
    new-instance v0, Landroid/support/v4/app/gp;

    invoke-direct {v0}, Landroid/support/v4/app/gp;-><init>()V

    sput-object v0, Landroid/support/v4/app/gl;->d:Landroid/support/v4/app/gn;

    .line 120
    :goto_d
    return-void

    .line 118
    :cond_e
    new-instance v0, Landroid/support/v4/app/go;

    invoke-direct {v0}, Landroid/support/v4/app/go;-><init>()V

    sput-object v0, Landroid/support/v4/app/gl;->d:Landroid/support/v4/app/gn;

    goto :goto_d
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    .line 126
    iput-object p1, p0, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    .line 127
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 256
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method private a(II)Landroid/app/PendingIntent;
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 344
    .line 1361
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1362
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No intents added to TaskStackBuilder; cannot getPendingIntent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1366
    :cond_11
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    .line 1367
    new-instance v1, Landroid/content/Intent;

    aget-object v2, v0, v3

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const v2, 0x1000c000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    .line 1371
    sget-object v1, Landroid/support/v4/app/gl;->d:Landroid/support/v4/app/gn;

    iget-object v2, p0, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-interface {v1, v2, v0, p1, p2}, Landroid/support/v4/app/gn;->a(Landroid/content/Context;[Landroid/content/Intent;II)Landroid/app/PendingIntent;

    move-result-object v0

    .line 344
    return-object v0
.end method

.method private a(I)Landroid/content/Intent;
    .registers 3

    .prologue
    .line 270
    .line 1282
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 270
    return-object v0
.end method

.method private a(Landroid/app/Activity;)Landroid/support/v4/app/gl;
    .registers 4

    .prologue
    .line 197
    const/4 v0, 0x0

    .line 198
    instance-of v1, p1, Landroid/support/v4/app/gm;

    if-eqz v1, :cond_c

    move-object v0, p1

    .line 199
    check-cast v0, Landroid/support/v4/app/gm;

    invoke-interface {v0}, Landroid/support/v4/app/gm;->a_()Landroid/content/Intent;

    move-result-object v0

    .line 201
    :cond_c
    if-nez v0, :cond_2c

    .line 202
    invoke-static {p1}, Landroid/support/v4/app/cv;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 205
    :goto_13
    if-eqz v1, :cond_2b

    .line 208
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 209
    if-nez v0, :cond_25

    .line 210
    iget-object v0, p0, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    .line 212
    :cond_25
    invoke-virtual {p0, v0}, Landroid/support/v4/app/gl;->a(Landroid/content/ComponentName;)Landroid/support/v4/app/gl;

    .line 213
    invoke-virtual {p0, v1}, Landroid/support/v4/app/gl;->a(Landroid/content/Intent;)Landroid/support/v4/app/gl;

    .line 215
    :cond_2b
    return-object p0

    :cond_2c
    move-object v1, v0

    goto :goto_13
.end method

.method public static a(Landroid/content/Context;)Landroid/support/v4/app/gl;
    .registers 2

    .prologue
    .line 137
    new-instance v0, Landroid/support/v4/app/gl;

    invoke-direct {v0, p0}, Landroid/support/v4/app/gl;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private a(Ljava/lang/Class;)Landroid/support/v4/app/gl;
    .registers 4

    .prologue
    .line 226
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/support/v4/app/gl;->a(Landroid/content/ComponentName;)Landroid/support/v4/app/gl;

    move-result-object v0

    return-object v0
.end method

.method private b(II)Landroid/app/PendingIntent;
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 361
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 362
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No intents added to TaskStackBuilder; cannot getPendingIntent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 366
    :cond_11
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    .line 367
    new-instance v1, Landroid/content/Intent;

    aget-object v2, v0, v3

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const v2, 0x1000c000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    .line 371
    sget-object v1, Landroid/support/v4/app/gl;->d:Landroid/support/v4/app/gn;

    iget-object v2, p0, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-interface {v1, v2, v0, p1, p2}, Landroid/support/v4/app/gn;->a(Landroid/content/Context;[Landroid/content/Intent;II)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private b(I)Landroid/content/Intent;
    .registers 3

    .prologue
    .line 282
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    return-object v0
.end method

.method private static b(Landroid/content/Context;)Landroid/support/v4/app/gl;
    .registers 2

    .prologue
    .line 150
    invoke-static {p0}, Landroid/support/v4/app/gl;->a(Landroid/content/Context;)Landroid/support/v4/app/gl;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Intent;)Landroid/support/v4/app/gl;
    .registers 3

    .prologue
    .line 178
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 179
    if-nez v0, :cond_10

    .line 180
    iget-object v0, p0, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    .line 182
    :cond_10
    if-eqz v0, :cond_15

    .line 183
    invoke-virtual {p0, v0}, Landroid/support/v4/app/gl;->a(Landroid/content/ComponentName;)Landroid/support/v4/app/gl;

    .line 185
    :cond_15
    invoke-virtual {p0, p1}, Landroid/support/v4/app/gl;->a(Landroid/content/Intent;)Landroid/support/v4/app/gl;

    .line 186
    return-object p0
.end method

.method private b()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 301
    .line 1316
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1317
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No intents added to TaskStackBuilder; cannot startActivities"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1321
    :cond_11
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    .line 1322
    new-instance v1, Landroid/content/Intent;

    aget-object v2, v0, v3

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const v2, 0x1000c000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    .line 1325
    iget-object v1, p0, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Landroid/support/v4/c/h;->a(Landroid/content/Context;[Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_4d

    .line 1326
    new-instance v1, Landroid/content/Intent;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v0, v2

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 1327
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1328
    iget-object v0, p0, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 302
    :cond_4d
    return-void
.end method

.method private c()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 316
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 317
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No intents added to TaskStackBuilder; cannot startActivities"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 321
    :cond_11
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    .line 322
    new-instance v1, Landroid/content/Intent;

    aget-object v2, v0, v3

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const v2, 0x1000c000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    .line 325
    iget-object v1, p0, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Landroid/support/v4/c/h;->a(Landroid/content/Context;[Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_4d

    .line 326
    new-instance v1, Landroid/content/Intent;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v0, v2

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 327
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 328
    iget-object v0, p0, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 330
    :cond_4d
    return-void
.end method

.method private d()[Landroid/content/Intent;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 382
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Landroid/content/Intent;

    .line 383
    array-length v0, v2

    if-nez v0, :cond_e

    move-object v0, v2

    .line 391
    :goto_d
    return-object v0

    .line 385
    :cond_e
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const v0, 0x1000c000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v2, v3

    .line 388
    const/4 v0, 0x1

    move v1, v0

    :goto_26
    array-length v0, v2

    if-ge v1, v0, :cond_3c

    .line 389
    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    aput-object v3, v2, v1

    .line 388
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_26

    :cond_3c
    move-object v0, v2

    .line 391
    goto :goto_d
.end method


# virtual methods
.method public final a(Landroid/content/ComponentName;)Landroid/support/v4/app/gl;
    .registers 5

    .prologue
    .line 238
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 240
    :try_start_6
    iget-object v0, p0, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Landroid/support/v4/app/cv;->a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 241
    :goto_c
    if-eqz v0, :cond_2c

    .line 242
    iget-object v2, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 243
    iget-object v2, p0, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/support/v4/app/cv;->a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Intent;
    :try_end_1c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_1c} :catch_1e

    move-result-object v0

    goto :goto_c

    .line 245
    :catch_1e
    move-exception v0

    .line 246
    const-string v1, "TaskStackBuilder"

    const-string v2, "Bad ComponentName while traversing activity parent metadata"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 249
    :cond_2c
    return-object p0
.end method

.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/gl;
    .registers 3

    .prologue
    .line 161
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    return-object p0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 289
    iget-object v0, p0, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
