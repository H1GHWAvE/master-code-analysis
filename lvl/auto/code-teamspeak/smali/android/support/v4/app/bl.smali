.class final Landroid/support/v4/app/bl;
.super Landroid/support/v4/app/bi;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/as;


# static fields
.field static final G:Landroid/view/animation/Interpolator;

.field static final H:Landroid/view/animation/Interpolator;

.field static final I:Landroid/view/animation/Interpolator;

.field static final J:Landroid/view/animation/Interpolator;

.field static final K:I = 0xdc

.field public static final L:I = 0x1

.field public static final M:I = 0x2

.field public static final N:I = 0x3

.field public static final O:I = 0x4

.field public static final P:I = 0x5

.field public static final Q:I = 0x6

.field static b:Z = false

.field static final c:Ljava/lang/String; = "FragmentManager"

.field static final d:Z

.field static final e:Ljava/lang/String; = "android:target_req_state"

.field static final f:Ljava/lang/String; = "android:target_state"

.field static final g:Ljava/lang/String; = "android:view_state"

.field static final h:Ljava/lang/String; = "android:user_visible_hint"


# instance fields
.field A:Z

.field B:Ljava/lang/String;

.field C:Z

.field D:Landroid/os/Bundle;

.field E:Landroid/util/SparseArray;

.field F:Ljava/lang/Runnable;

.field i:Ljava/util/ArrayList;

.field j:[Ljava/lang/Runnable;

.field k:Z

.field l:Ljava/util/ArrayList;

.field m:Ljava/util/ArrayList;

.field n:Ljava/util/ArrayList;

.field o:Ljava/util/ArrayList;

.field p:Ljava/util/ArrayList;

.field q:Ljava/util/ArrayList;

.field r:Ljava/util/ArrayList;

.field s:Ljava/util/ArrayList;

.field t:I

.field u:Landroid/support/v4/app/bh;

.field v:Landroid/support/v4/app/bg;

.field w:Landroid/support/v4/app/bf;

.field x:Landroid/support/v4/app/Fragment;

.field y:Z

.field z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    const/high16 v4, 0x40200000    # 2.5f

    const/high16 v3, 0x3fc00000    # 1.5f

    .line 405
    sput-boolean v0, Landroid/support/v4/app/bl;->b:Z

    .line 408
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_e

    const/4 v0, 0x1

    :cond_e
    sput-boolean v0, Landroid/support/v4/app/bl;->d:Z

    .line 805
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Landroid/support/v4/app/bl;->G:Landroid/view/animation/Interpolator;

    .line 806
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Landroid/support/v4/app/bl;->H:Landroid/view/animation/Interpolator;

    .line 807
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    sput-object v0, Landroid/support/v4/app/bl;->I:Landroid/view/animation/Interpolator;

    .line 808
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    sput-object v0, Landroid/support/v4/app/bl;->J:Landroid/view/animation/Interpolator;

    return-void
.end method

.method constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 404
    invoke-direct {p0}, Landroid/support/v4/app/bi;-><init>()V

    .line 474
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/app/bl;->t:I

    .line 487
    iput-object v1, p0, Landroid/support/v4/app/bl;->D:Landroid/os/Bundle;

    .line 488
    iput-object v1, p0, Landroid/support/v4/app/bl;->E:Landroid/util/SparseArray;

    .line 490
    new-instance v0, Landroid/support/v4/app/bm;

    invoke-direct {v0, p0}, Landroid/support/v4/app/bm;-><init>(Landroid/support/v4/app/bl;)V

    iput-object v0, p0, Landroid/support/v4/app/bl;->F:Ljava/lang/Runnable;

    .line 2289
    return-void
.end method

.method private A()Landroid/support/v4/view/as;
    .registers 1

    .prologue
    .line 2286
    return-object p0
.end method

.method private static a(IZ)I
    .registers 3

    .prologue
    .line 2177
    const/4 v0, -0x1

    .line 2178
    sparse-switch p0, :sswitch_data_18

    .line 2189
    :goto_4
    return v0

    .line 2180
    :sswitch_5
    if-eqz p1, :cond_9

    const/4 v0, 0x1

    goto :goto_4

    :cond_9
    const/4 v0, 0x2

    goto :goto_4

    .line 2183
    :sswitch_b
    if-eqz p1, :cond_f

    const/4 v0, 0x3

    goto :goto_4

    :cond_f
    const/4 v0, 0x4

    goto :goto_4

    .line 2186
    :sswitch_11
    if-eqz p1, :cond_15

    const/4 v0, 0x5

    goto :goto_4

    :cond_15
    const/4 v0, 0x6

    goto :goto_4

    .line 2178
    nop

    :sswitch_data_18
    .sparse-switch
        0x1001 -> :sswitch_5
        0x1003 -> :sswitch_11
        0x2002 -> :sswitch_b
    .end sparse-switch
.end method

.method private static a(FF)Landroid/view/animation/Animation;
    .registers 6

    .prologue
    .line 828
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p0, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 829
    sget-object v1, Landroid/support/v4/app/bl;->H:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 830
    const-wide/16 v2, 0xdc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 831
    return-object v0
.end method

.method private static a(FFFF)Landroid/view/animation/Animation;
    .registers 16

    .prologue
    const-wide/16 v10, 0xdc

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    .line 814
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x0

    invoke-direct {v9, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 815
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v1, p0

    move v2, p1

    move v3, p0

    move v4, p1

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 817
    sget-object v1, Landroid/support/v4/app/bl;->G:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 818
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 819
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 820
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 821
    sget-object v1, Landroid/support/v4/app/bl;->H:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 822
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 823
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 824
    return-object v9
.end method

.method private a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;
    .registers 11

    .prologue
    const v5, 0x3f79999a    # 0.975f

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 836
    iget v0, p1, Landroid/support/v4/app/Fragment;->aa:I

    invoke-static {}, Landroid/support/v4/app/Fragment;->r()Landroid/view/animation/Animation;

    .line 842
    iget v0, p1, Landroid/support/v4/app/Fragment;->aa:I

    if-eqz v0, :cond_1d

    .line 843
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 5166
    iget-object v0, v0, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    .line 843
    iget v2, p1, Landroid/support/v4/app/Fragment;->aa:I

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 844
    if-eqz v0, :cond_1d

    .line 890
    :goto_1c
    return-object v0

    .line 849
    :cond_1d
    if-nez p2, :cond_21

    move-object v0, v1

    .line 850
    goto :goto_1c

    .line 5177
    :cond_21
    const/4 v0, -0x1

    .line 5178
    sparse-switch p2, :sswitch_data_78

    .line 854
    :goto_25
    if-gez v0, :cond_3b

    move-object v0, v1

    .line 855
    goto :goto_1c

    .line 5180
    :sswitch_29
    if-eqz p3, :cond_2d

    const/4 v0, 0x1

    goto :goto_25

    :cond_2d
    const/4 v0, 0x2

    goto :goto_25

    .line 5183
    :sswitch_2f
    if-eqz p3, :cond_33

    const/4 v0, 0x3

    goto :goto_25

    :cond_33
    const/4 v0, 0x4

    goto :goto_25

    .line 5186
    :sswitch_35
    if-eqz p3, :cond_39

    const/4 v0, 0x5

    goto :goto_25

    :cond_39
    const/4 v0, 0x6

    goto :goto_25

    .line 858
    :cond_3b
    packed-switch v0, :pswitch_data_86

    .line 873
    if-nez p4, :cond_4e

    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    invoke-virtual {v0}, Landroid/support/v4/app/bh;->e()Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 874
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    invoke-virtual {v0}, Landroid/support/v4/app/bh;->f()I

    move-result p4

    .line 876
    :cond_4e
    if-nez p4, :cond_75

    move-object v0, v1

    .line 877
    goto :goto_1c

    .line 860
    :pswitch_52
    const/high16 v0, 0x3f900000    # 1.125f

    invoke-static {v0, v3, v4, v3}, Landroid/support/v4/app/bl;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1c

    .line 862
    :pswitch_59
    invoke-static {v3, v5, v3, v4}, Landroid/support/v4/app/bl;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1c

    .line 864
    :pswitch_5e
    invoke-static {v5, v3, v4, v3}, Landroid/support/v4/app/bl;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1c

    .line 866
    :pswitch_63
    const v0, 0x3f89999a    # 1.075f

    invoke-static {v3, v0, v3, v4}, Landroid/support/v4/app/bl;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1c

    .line 868
    :pswitch_6b
    invoke-static {v4, v3}, Landroid/support/v4/app/bl;->a(FF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1c

    .line 870
    :pswitch_70
    invoke-static {v3, v4}, Landroid/support/v4/app/bl;->a(FF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1c

    :cond_75
    move-object v0, v1

    .line 890
    goto :goto_1c

    .line 5178
    nop

    :sswitch_data_78
    .sparse-switch
        0x1001 -> :sswitch_29
        0x1003 -> :sswitch_35
        0x2002 -> :sswitch_2f
    .end sparse-switch

    .line 858
    :pswitch_data_86
    .packed-switch 0x1
        :pswitch_52
        :pswitch_59
        :pswitch_5e
        :pswitch_63
        :pswitch_6b
        :pswitch_70
    .end packed-switch
.end method

.method private a(ILandroid/support/v4/app/ak;)V
    .registers 7

    .prologue
    .line 1504
    monitor-enter p0

    .line 1505
    :try_start_1
    iget-object v0, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    if-nez v0, :cond_c

    .line 1506
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    .line 1508
    :cond_c
    iget-object v0, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1509
    if-ge p1, v0, :cond_3d

    .line 1510
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_36

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting back stack index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1511
    :cond_36
    iget-object v0, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1525
    :goto_3b
    monitor-exit p0

    return-void

    .line 1513
    :cond_3d
    :goto_3d
    if-ge v0, p1, :cond_74

    .line 1514
    iget-object v1, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1515
    iget-object v1, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    if-nez v1, :cond_50

    .line 1516
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    .line 1518
    :cond_50
    sget-boolean v1, Landroid/support/v4/app/bl;->b:Z

    if-eqz v1, :cond_68

    const-string v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adding available back stack index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1519
    :cond_68
    iget-object v1, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1520
    add-int/lit8 v0, v0, 0x1

    goto :goto_3d

    .line 1522
    :cond_74
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_96

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Adding back stack index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1523
    :cond_96
    iget-object v0, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3b

    .line 1525
    :catchall_9c
    move-exception v0

    monitor-exit p0
    :try_end_9e
    .catchall {:try_start_1 .. :try_end_9e} :catchall_9c

    throw v0
.end method

.method private a(Ljava/lang/RuntimeException;)V
    .registers 6

    .prologue
    .line 518
    const-string v0, "FragmentManager"

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    const-string v0, "FragmentManager"

    const-string v1, "Activity state:"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    new-instance v0, Landroid/support/v4/n/h;

    const-string v1, "FragmentManager"

    invoke-direct {v0, v1}, Landroid/support/v4/n/h;-><init>(Ljava/lang/String;)V

    .line 521
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 522
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    if-eqz v0, :cond_34

    .line 524
    :try_start_20
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    const-string v2, "  "

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Landroid/support/v4/app/bh;->a(Ljava/lang/String;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_2a} :catch_2b

    .line 535
    :goto_2a
    throw p1

    .line 525
    :catch_2b
    move-exception v0

    .line 526
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2a

    .line 530
    :cond_34
    :try_start_34
    const-string v0, "  "

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v1, v3}, Landroid/support/v4/app/bl;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_3d} :catch_3e

    goto :goto_2a

    .line 531
    :catch_3e
    move-exception v0

    .line 532
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2a
.end method

.method static a(Landroid/view/View;Landroid/view/animation/Animation;)Z
    .registers 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 512
    invoke-static {p0}, Landroid/support/v4/view/cx;->e(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_16

    invoke-static {p0}, Landroid/support/v4/view/cx;->x(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 2498
    instance-of v0, p1, Landroid/view/animation/AlphaAnimation;

    if-eqz v0, :cond_17

    move v0, v2

    .line 512
    :goto_13
    if-eqz v0, :cond_16

    move v1, v2

    :cond_16
    return v1

    .line 2500
    :cond_17
    instance-of v0, p1, Landroid/view/animation/AnimationSet;

    if-eqz v0, :cond_35

    .line 2501
    check-cast p1, Landroid/view/animation/AnimationSet;

    invoke-virtual {p1}, Landroid/view/animation/AnimationSet;->getAnimations()Ljava/util/List;

    move-result-object v3

    move v0, v1

    .line 2502
    :goto_22
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_35

    .line 2503
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Landroid/view/animation/AlphaAnimation;

    if-eqz v4, :cond_32

    move v0, v2

    .line 2504
    goto :goto_13

    .line 2502
    :cond_32
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    :cond_35
    move v0, v1

    .line 2508
    goto :goto_13
.end method

.method private static a(Landroid/view/animation/Animation;)Z
    .registers 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 498
    instance-of v0, p0, Landroid/view/animation/AlphaAnimation;

    if-eqz v0, :cond_8

    move v1, v2

    .line 508
    :cond_7
    :goto_7
    return v1

    .line 500
    :cond_8
    instance-of v0, p0, Landroid/view/animation/AnimationSet;

    if-eqz v0, :cond_7

    .line 501
    check-cast p0, Landroid/view/animation/AnimationSet;

    invoke-virtual {p0}, Landroid/view/animation/AnimationSet;->getAnimations()Ljava/util/List;

    move-result-object v3

    move v0, v1

    .line 502
    :goto_13
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_7

    .line 503
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Landroid/view/animation/AlphaAnimation;

    if-eqz v4, :cond_23

    move v1, v2

    .line 504
    goto :goto_7

    .line 502
    :cond_23
    add-int/lit8 v0, v0, 0x1

    goto :goto_13
.end method

.method private b(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 1435
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_5a

    if-eqz p1, :cond_5a

    .line 1436
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_10
    if-ltz v3, :cond_5a

    .line 1437
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1438
    if-eqz v0, :cond_56

    .line 17906
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 1438
    :cond_24
    :goto_24
    if-eqz v0, :cond_56

    .line 1443
    :goto_26
    return-object v0

    .line 17909
    :cond_27
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v2, :cond_54

    .line 17910
    iget-object v4, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 18435
    iget-object v0, v4, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_52

    if-eqz p1, :cond_52

    .line 18436
    iget-object v0, v4, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_3c
    if-ltz v2, :cond_52

    .line 18437
    iget-object v0, v4, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 18438
    if-eqz v0, :cond_4e

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_24

    .line 18436
    :cond_4e
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_3c

    :cond_52
    move-object v0, v1

    .line 17910
    goto :goto_24

    :cond_54
    move-object v0, v1

    .line 17912
    goto :goto_24

    .line 1436
    :cond_56
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_10

    :cond_5a
    move-object v0, v1

    .line 1443
    goto :goto_26
.end method

.method private b(Landroid/support/v4/app/ak;)V
    .registers 3

    .prologue
    .line 1604
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    if-nez v0, :cond_b

    .line 1605
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    .line 1607
    :cond_b
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1608
    invoke-virtual {p0}, Landroid/support/v4/app/bl;->l()V

    .line 1609
    return-void
.end method

.method private static b(Landroid/view/View;Landroid/view/animation/Animation;)V
    .registers 3

    .prologue
    .line 913
    if-eqz p0, :cond_4

    if-nez p1, :cond_5

    .line 919
    :cond_4
    :goto_4
    return-void

    .line 916
    :cond_5
    invoke-static {p0, p1}, Landroid/support/v4/app/bl;->a(Landroid/view/View;Landroid/view/animation/Animation;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 917
    new-instance v0, Landroid/support/v4/app/br;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/app/br;-><init>(Landroid/view/View;Landroid/view/animation/Animation;)V

    invoke-virtual {p1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_4
.end method

.method private c(Landroid/support/v4/app/Fragment;)V
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 1185
    iget v2, p0, Landroid/support/v4/app/bl;->t:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 1186
    return-void
.end method

.method public static d(I)I
    .registers 2

    .prologue
    .line 2153
    const/4 v0, 0x0

    .line 2154
    sparse-switch p0, :sswitch_data_e

    .line 2165
    :goto_4
    return v0

    .line 2156
    :sswitch_5
    const/16 v0, 0x2002

    .line 2157
    goto :goto_4

    .line 2159
    :sswitch_8
    const/16 v0, 0x1001

    .line 2160
    goto :goto_4

    .line 2162
    :sswitch_b
    const/16 v0, 0x1003

    goto :goto_4

    .line 2154
    :sswitch_data_e
    .sparse-switch
        0x1001 -> :sswitch_5
        0x1003 -> :sswitch_b
        0x2002 -> :sswitch_8
    .end sparse-switch
.end method

.method private d(Landroid/support/v4/app/Fragment;)V
    .registers 5

    .prologue
    .line 1237
    iget v0, p1, Landroid/support/v4/app/Fragment;->z:I

    if-ltz v0, :cond_5

    .line 1253
    :cond_4
    :goto_4
    return-void

    .line 1241
    :cond_5
    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_11

    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_45

    .line 1242
    :cond_11
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_1c

    .line 1243
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    .line 1245
    :cond_1c
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->a(ILandroid/support/v4/app/Fragment;)V

    .line 1246
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1252
    :goto_2c
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Allocated fragment index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 1249
    :cond_45
    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->a(ILandroid/support/v4/app/Fragment;)V

    .line 1250
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    iget v1, p1, Landroid/support/v4/app/Fragment;->z:I

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2c
.end method

.method private e(I)V
    .registers 5

    .prologue
    .line 1529
    monitor-enter p0

    .line 1530
    :try_start_1
    iget-object v0, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1531
    iget-object v0, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    if-nez v0, :cond_12

    .line 1532
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    .line 1534
    :cond_12
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_2a

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Freeing back stack index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1535
    :cond_2a
    iget-object v0, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1536
    monitor-exit p0

    return-void

    :catchall_35
    move-exception v0

    monitor-exit p0
    :try_end_37
    .catchall {:try_start_1 .. :try_end_37} :catchall_35

    throw v0
.end method

.method private e(Landroid/support/v4/app/Fragment;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1256
    iget v0, p1, Landroid/support/v4/app/Fragment;->z:I

    if-gez v0, :cond_7

    .line 1268
    :goto_6
    return-void

    .line 1260
    :cond_7
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_1f

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Freeing fragment index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1261
    :cond_1f
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    iget v1, p1, Landroid/support/v4/app/Fragment;->z:I

    invoke-virtual {v0, v1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1262
    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    if-nez v0, :cond_31

    .line 1263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    .line 1265
    :cond_31
    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    iget v1, p1, Landroid/support/v4/app/Fragment;->z:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1266
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bh;->b(Ljava/lang/String;)V

    .line 16387
    const/4 v0, -0x1

    iput v0, p1, Landroid/support/v4/app/Fragment;->z:I

    .line 16388
    iput-object v4, p1, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    .line 16389
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->F:Z

    .line 16390
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->G:Z

    .line 16391
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->H:Z

    .line 16392
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->I:Z

    .line 16393
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->J:Z

    .line 16394
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->K:Z

    .line 16395
    iput v3, p1, Landroid/support/v4/app/Fragment;->L:I

    .line 16396
    iput-object v4, p1, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 16397
    iput-object v4, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 16398
    iput-object v4, p1, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    .line 16399
    iput v3, p1, Landroid/support/v4/app/Fragment;->Q:I

    .line 16400
    iput v3, p1, Landroid/support/v4/app/Fragment;->R:I

    .line 16401
    iput-object v4, p1, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    .line 16402
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->T:Z

    .line 16403
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->U:Z

    .line 16404
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->W:Z

    .line 16405
    iput-object v4, p1, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    .line 16406
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->ah:Z

    .line 16407
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->ai:Z

    goto :goto_6
.end method

.method private f(Landroid/support/v4/app/Fragment;)V
    .registers 4

    .prologue
    .line 1705
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ad:Landroid/view/View;

    if-nez v0, :cond_5

    .line 1718
    :cond_4
    :goto_4
    return-void

    .line 1708
    :cond_5
    iget-object v0, p0, Landroid/support/v4/app/bl;->E:Landroid/util/SparseArray;

    if-nez v0, :cond_27

    .line 1709
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->E:Landroid/util/SparseArray;

    .line 1713
    :goto_10
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ad:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/app/bl;->E:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 1714
    iget-object v0, p0, Landroid/support/v4/app/bl;->E:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 1715
    iget-object v0, p0, Landroid/support/v4/app/bl;->E:Landroid/util/SparseArray;

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    .line 1716
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/bl;->E:Landroid/util/SparseArray;

    goto :goto_4

    .line 1711
    :cond_27
    iget-object v0, p0, Landroid/support/v4/app/bl;->E:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    goto :goto_10
.end method

.method private g(Landroid/support/v4/app/Fragment;)Landroid/os/Bundle;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 1721
    .line 1723
    iget-object v0, p0, Landroid/support/v4/app/bl;->D:Landroid/os/Bundle;

    if-nez v0, :cond_c

    .line 1724
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->D:Landroid/os/Bundle;

    .line 1726
    :cond_c
    iget-object v0, p0, Landroid/support/v4/app/bl;->D:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->f(Landroid/os/Bundle;)V

    .line 1727
    iget-object v0, p0, Landroid/support/v4/app/bl;->D:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_49

    .line 1728
    iget-object v0, p0, Landroid/support/v4/app/bl;->D:Landroid/os/Bundle;

    .line 1729
    iput-object v1, p0, Landroid/support/v4/app/bl;->D:Landroid/os/Bundle;

    .line 1732
    :goto_1d
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-eqz v1, :cond_24

    .line 1733
    invoke-direct {p0, p1}, Landroid/support/v4/app/bl;->f(Landroid/support/v4/app/Fragment;)V

    .line 1735
    :cond_24
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    if-eqz v1, :cond_36

    .line 1736
    if-nez v0, :cond_2f

    .line 1737
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1739
    :cond_2f
    const-string v1, "android:view_state"

    iget-object v2, p1, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 1742
    :cond_36
    iget-boolean v1, p1, Landroid/support/v4/app/Fragment;->af:Z

    if-nez v1, :cond_48

    .line 1743
    if-nez v0, :cond_41

    .line 1744
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1747
    :cond_41
    const-string v1, "android:user_visible_hint"

    iget-boolean v2, p1, Landroid/support/v4/app/Fragment;->af:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1750
    :cond_48
    return-object v0

    :cond_49
    move-object v0, v1

    goto :goto_1d
.end method

.method private u()V
    .registers 4

    .prologue
    .line 1447
    iget-boolean v0, p0, Landroid/support/v4/app/bl;->z:Z

    if-eqz v0, :cond_c

    .line 1448
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not perform this action after onSaveInstanceState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1451
    :cond_c
    iget-object v0, p0, Landroid/support/v4/app/bl;->B:Ljava/lang/String;

    if-eqz v0, :cond_27

    .line 1452
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not perform this action inside of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v4/app/bl;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1455
    :cond_27
    return-void
.end method

.method private v()Ljava/util/ArrayList;
    .registers 7

    .prologue
    .line 1686
    const/4 v1, 0x0

    .line 1687
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_52

    .line 1688
    const/4 v0, 0x0

    move v3, v0

    :goto_7
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_52

    .line 1689
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1690
    if-eqz v0, :cond_4c

    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->V:Z

    if-eqz v2, :cond_4c

    .line 1691
    if-nez v1, :cond_24

    .line 1692
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1694
    :cond_24
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1695
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/support/v4/app/Fragment;->W:Z

    .line 1696
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    if-eqz v2, :cond_50

    iget-object v2, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    iget v2, v2, Landroid/support/v4/app/Fragment;->z:I

    :goto_32
    iput v2, v0, Landroid/support/v4/app/Fragment;->D:I

    .line 1697
    sget-boolean v2, Landroid/support/v4/app/bl;->b:Z

    if-eqz v2, :cond_4c

    const-string v2, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "retainNonConfig: keeping retained "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1688
    :cond_4c
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_7

    .line 1696
    :cond_50
    const/4 v2, -0x1

    goto :goto_32

    .line 1701
    :cond_52
    return-object v1
.end method

.method private w()V
    .registers 2

    .prologue
    .line 1991
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/bl;->z:Z

    .line 1992
    return-void
.end method

.method private x()V
    .registers 2

    .prologue
    .line 2015
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/support/v4/app/bl;->c(I)V

    .line 2016
    return-void
.end method

.method private y()V
    .registers 2

    .prologue
    .line 2028
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/support/v4/app/bl;->c(I)V

    .line 2029
    return-void
.end method

.method private z()V
    .registers 2

    .prologue
    .line 2032
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/bl;->c(I)V

    .line 2033
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/ak;)I
    .registers 6

    .prologue
    .line 1484
    monitor-enter p0

    .line 1485
    :try_start_1
    iget-object v0, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    if-eqz v0, :cond_d

    iget-object v0, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_47

    .line 1486
    :cond_d
    iget-object v0, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    if-nez v0, :cond_18

    .line 1487
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    .line 1489
    :cond_18
    iget-object v0, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1490
    sget-boolean v1, Landroid/support/v4/app/bl;->b:Z

    if-eqz v1, :cond_40

    const-string v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setting back stack index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1491
    :cond_40
    iget-object v1, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1492
    monitor-exit p0

    .line 1498
    :goto_46
    return v0

    .line 1495
    :cond_47
    iget-object v0, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1496
    sget-boolean v1, Landroid/support/v4/app/bl;->b:Z

    if-eqz v1, :cond_7d

    const-string v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adding back stack index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1497
    :cond_7d
    iget-object v1, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1498
    monitor-exit p0

    goto :goto_46

    .line 1500
    :catchall_84
    move-exception v0

    monitor-exit p0
    :try_end_86
    .catchall {:try_start_1 .. :try_end_86} :catchall_84

    throw v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 661
    iget v1, p1, Landroid/support/v4/app/Fragment;->z:I

    if-gez v1, :cond_22

    .line 662
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not currently in the FragmentManager"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Landroid/support/v4/app/bl;->a(Ljava/lang/RuntimeException;)V

    .line 665
    :cond_22
    iget v1, p1, Landroid/support/v4/app/Fragment;->u:I

    if-lez v1, :cond_31

    .line 666
    invoke-direct {p0, p1}, Landroid/support/v4/app/bl;->g(Landroid/support/v4/app/Fragment;)Landroid/os/Bundle;

    move-result-object v1

    .line 667
    if-eqz v1, :cond_31

    new-instance v0, Landroid/support/v4/app/Fragment$SavedState;

    invoke-direct {v0, v1}, Landroid/support/v4/app/Fragment$SavedState;-><init>(Landroid/os/Bundle;)V

    .line 669
    :cond_31
    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .registers 5

    .prologue
    .line 1391
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_22

    .line 1393
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_d
    if-ltz v1, :cond_22

    .line 1394
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1395
    if-eqz v0, :cond_1e

    iget v2, v0, Landroid/support/v4/app/Fragment;->Q:I

    if-ne v2, p1, :cond_1e

    .line 1409
    :cond_1d
    :goto_1d
    return-object v0

    .line 1393
    :cond_1e
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_d

    .line 1400
    :cond_22
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_43

    .line 1402
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2f
    if-ltz v1, :cond_43

    .line 1403
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1404
    if-eqz v0, :cond_3f

    iget v2, v0, Landroid/support/v4/app/Fragment;->Q:I

    if-eq v2, p1, :cond_1d

    .line 1402
    :cond_3f
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2f

    .line 1409
    :cond_43
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .registers 8

    .prologue
    const/4 v0, -0x1

    .line 638
    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 639
    if-ne v1, v0, :cond_9

    .line 640
    const/4 v0, 0x0

    .line 651
    :cond_8
    :goto_8
    return-object v0

    .line 642
    :cond_9
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_32

    .line 643
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment no longer exists for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Landroid/support/v4/app/bl;->a(Ljava/lang/RuntimeException;)V

    .line 646
    :cond_32
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 647
    if-nez v0, :cond_8

    .line 648
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fragment no longer exists for key "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Landroid/support/v4/app/bl;->a(Ljava/lang/RuntimeException;)V

    goto :goto_8
.end method

.method public final a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .registers 5

    .prologue
    .line 1413
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_28

    if-eqz p1, :cond_28

    .line 1415
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_f
    if-ltz v1, :cond_28

    .line 1416
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1417
    if-eqz v0, :cond_24

    iget-object v2, v0, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 1431
    :cond_23
    :goto_23
    return-object v0

    .line 1415
    :cond_24
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_f

    .line 1422
    :cond_28
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_4f

    if-eqz p1, :cond_4f

    .line 1424
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_37
    if-ltz v1, :cond_4f

    .line 1425
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1426
    if-eqz v0, :cond_4b

    iget-object v2, v0, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    .line 1424
    :cond_4b
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_37

    .line 1431
    :cond_4f
    const/4 v0, 0x0

    goto :goto_23
.end method

.method public final a()Landroid/support/v4/app/cd;
    .registers 2

    .prologue
    .line 540
    new-instance v0, Landroid/support/v4/app/ak;

    invoke-direct {v0, p0}, Landroid/support/v4/app/ak;-><init>(Landroid/support/v4/app/bl;)V

    return-object v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 15

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v5, -0x1

    const/4 v2, 0x1

    .line 2194
    const-string v0, "fragment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    move-object v0, v4

    .line 2282
    :goto_d
    return-object v0

    .line 2198
    :cond_e
    const-string v0, "class"

    invoke-interface {p4, v4, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2199
    sget-object v1, Landroid/support/v4/app/bu;->a:[I

    invoke-virtual {p3, p4, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2200
    if-nez v0, :cond_177

    .line 2201
    invoke-virtual {v1, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 2203
    :goto_21
    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    .line 2204
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2205
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2207
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 26166
    iget-object v0, v0, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    .line 2207
    invoke-static {v0, v6}, Landroid/support/v4/app/Fragment;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_39

    move-object v0, v4

    .line 2210
    goto :goto_d

    .line 2213
    :cond_39
    if-eqz p1, :cond_66

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 2214
    :goto_3f
    if-ne v1, v5, :cond_68

    if-ne v7, v5, :cond_68

    if-nez v8, :cond_68

    .line 2215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p4}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Must specify unique android:id, android:tag, or have a parent with an id for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_66
    move v1, v3

    .line 2213
    goto :goto_3f

    .line 2222
    :cond_68
    if-eq v7, v5, :cond_fd

    invoke-virtual {p0, v7}, Landroid/support/v4/app/bl;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2223
    :goto_6e
    if-nez v0, :cond_76

    if-eqz v8, :cond_76

    .line 2224
    invoke-virtual {p0, v8}, Landroid/support/v4/app/bl;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2226
    :cond_76
    if-nez v0, :cond_7e

    if-eq v1, v5, :cond_7e

    .line 2227
    invoke-virtual {p0, v1}, Landroid/support/v4/app/bl;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2230
    :cond_7e
    sget-boolean v4, Landroid/support/v4/app/bl;->b:Z

    if-eqz v4, :cond_ae

    const-string v4, "FragmentManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "onCreateView: id=0x"

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " fname="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " existing="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2233
    :cond_ae
    if-nez v0, :cond_102

    .line 2234
    invoke-static {p3, v6}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    .line 2235
    iput-boolean v2, v4, Landroid/support/v4/app/Fragment;->I:Z

    .line 2236
    if-eqz v7, :cond_100

    move v0, v7

    :goto_b9
    iput v0, v4, Landroid/support/v4/app/Fragment;->Q:I

    .line 2237
    iput v1, v4, Landroid/support/v4/app/Fragment;->R:I

    .line 2238
    iput-object v8, v4, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    .line 2239
    iput-boolean v2, v4, Landroid/support/v4/app/Fragment;->J:Z

    .line 2240
    iput-object p0, v4, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 2241
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    iput-object v0, v4, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    .line 2242
    iget-object v0, v4, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->q()V

    .line 2243
    invoke-virtual {p0, v4, v2}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;Z)V

    move-object v1, v4

    .line 2266
    :goto_d0
    iget v0, p0, Landroid/support/v4/app/bl;->t:I

    if-gtz v0, :cond_15b

    iget-boolean v0, v1, Landroid/support/v4/app/Fragment;->I:Z

    if-eqz v0, :cond_15b

    move-object v0, p0

    move v4, v3

    move v5, v3

    .line 2267
    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 2272
    :goto_de
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-nez v0, :cond_15f

    .line 2273
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not create a view."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_fd
    move-object v0, v4

    .line 2222
    goto/16 :goto_6e

    :cond_100
    move v0, v1

    .line 2236
    goto :goto_b9

    .line 2245
    :cond_102
    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->J:Z

    if-eqz v4, :cond_14d

    .line 2248
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p4}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Duplicate id 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", tag "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", or parent id 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with another fragment for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2255
    :cond_14d
    iput-boolean v2, v0, Landroid/support/v4/app/Fragment;->J:Z

    .line 2259
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->W:Z

    if-nez v1, :cond_158

    .line 2260
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->q()V

    :cond_158
    move-object v1, v0

    goto/16 :goto_d0

    .line 2269
    :cond_15b
    invoke-direct {p0, v1}, Landroid/support/v4/app/bl;->c(Landroid/support/v4/app/Fragment;)V

    goto :goto_de

    .line 2276
    :cond_15f
    if-eqz v7, :cond_166

    .line 2277
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    .line 2279
    :cond_166
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_173

    .line 2280
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2282
    :cond_173
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    goto/16 :goto_d

    :cond_177
    move-object v6, v0

    goto/16 :goto_21
.end method

.method final a(IIIZ)V
    .registers 13

    .prologue
    const/4 v5, 0x0

    .line 1193
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    if-nez v0, :cond_f

    if-eqz p1, :cond_f

    .line 1194
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No host"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1197
    :cond_f
    if-nez p4, :cond_16

    iget v0, p0, Landroid/support/v4/app/bl;->t:I

    if-ne v0, p1, :cond_16

    .line 1223
    :cond_15
    :goto_15
    return-void

    .line 1201
    :cond_16
    iput p1, p0, Landroid/support/v4/app/bl;->t:I

    .line 1202
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_15

    move v6, v5

    move v7, v5

    .line 1204
    :goto_1e
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_48

    .line 1205
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 1206
    if-eqz v1, :cond_62

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    .line 1207
    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 1208
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_62

    .line 1209
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->a()Z

    move-result v0

    or-int/2addr v7, v0

    move v1, v7

    .line 1204
    :goto_43
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v7, v1

    goto :goto_1e

    .line 1214
    :cond_48
    if-nez v7, :cond_4d

    .line 1215
    invoke-virtual {p0}, Landroid/support/v4/app/bl;->j()V

    .line 1218
    :cond_4d
    iget-boolean v0, p0, Landroid/support/v4/app/bl;->y:Z

    if-eqz v0, :cond_15

    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    if-eqz v0, :cond_15

    iget v0, p0, Landroid/support/v4/app/bl;->t:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_15

    .line 1219
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    invoke-virtual {v0}, Landroid/support/v4/app/bh;->d()V

    .line 1220
    iput-boolean v5, p0, Landroid/support/v4/app/bl;->y:Z

    goto :goto_15

    :cond_62
    move v1, v7

    goto :goto_43
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .registers 5

    .prologue
    .line 2045
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_28

    .line 2046
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_28

    .line 2047
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 2048
    if-eqz v0, :cond_24

    .line 26017
    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 26018
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v2, :cond_24

    .line 26019
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->a(Landroid/content/res/Configuration;)V

    .line 2046
    :cond_24
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 2053
    :cond_28
    return-void
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
    .registers 7

    .prologue
    .line 629
    iget v0, p3, Landroid/support/v4/app/Fragment;->z:I

    if-gez v0, :cond_21

    .line 630
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not currently in the FragmentManager"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Landroid/support/v4/app/bl;->a(Ljava/lang/RuntimeException;)V

    .line 633
    :cond_21
    iget v0, p3, Landroid/support/v4/app/Fragment;->z:I

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 634
    return-void
.end method

.method final a(Landroid/os/Parcelable;Ljava/util/List;)V
    .registers 13

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 1872
    if-nez p1, :cond_6

    .line 1980
    :cond_5
    :goto_5
    return-void

    .line 1873
    :cond_6
    check-cast p1, Landroid/support/v4/app/FragmentManagerState;

    .line 1874
    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->a:[Landroid/support/v4/app/FragmentState;

    if-eqz v0, :cond_5

    .line 1878
    if-eqz p2, :cond_68

    move v1, v2

    .line 1879
    :goto_f
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_68

    .line 1880
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1881
    sget-boolean v3, Landroid/support/v4/app/bl;->b:Z

    if-eqz v3, :cond_33

    const-string v3, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "restoreAllState: re-attaching retained "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1882
    :cond_33
    iget-object v3, p1, Landroid/support/v4/app/FragmentManagerState;->a:[Landroid/support/v4/app/FragmentState;

    iget v4, v0, Landroid/support/v4/app/Fragment;->z:I

    aget-object v3, v3, v4

    .line 1883
    iput-object v0, v3, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    .line 1884
    iput-object v8, v0, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    .line 1885
    iput v2, v0, Landroid/support/v4/app/Fragment;->L:I

    .line 1886
    iput-boolean v2, v0, Landroid/support/v4/app/Fragment;->J:Z

    .line 1887
    iput-boolean v2, v0, Landroid/support/v4/app/Fragment;->F:Z

    .line 1888
    iput-object v8, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    .line 1889
    iget-object v4, v3, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    if-eqz v4, :cond_64

    .line 1890
    iget-object v4, v3, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    iget-object v5, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 24166
    iget-object v5, v5, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    .line 1890
    invoke-virtual {v5}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1891
    iget-object v4, v3, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    const-string v5, "android:view_state"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v4

    iput-object v4, v0, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    .line 1893
    iget-object v3, v3, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    iput-object v3, v0, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    .line 1879
    :cond_64
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f

    .line 1900
    :cond_68
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->a:[Landroid/support/v4/app/FragmentState;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    .line 1901
    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_7b

    .line 1902
    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_7b
    move v0, v2

    .line 1904
    :goto_7c
    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->a:[Landroid/support/v4/app/FragmentState;

    array-length v1, v1

    if-ge v0, v1, :cond_16b

    .line 1905
    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->a:[Landroid/support/v4/app/FragmentState;

    aget-object v1, v1, v0

    .line 1906
    if-eqz v1, :cond_139

    .line 1907
    iget-object v3, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    iget-object v4, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    .line 25093
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    if-nez v5, :cond_10a

    .line 25166
    iget-object v5, v3, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    .line 25098
    iget-object v6, v1, Landroid/support/v4/app/FragmentState;->i:Landroid/os/Bundle;

    if-eqz v6, :cond_9e

    .line 25099
    iget-object v6, v1, Landroid/support/v4/app/FragmentState;->i:Landroid/os/Bundle;

    invoke-virtual {v5}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 25102
    :cond_9e
    iget-object v6, v1, Landroid/support/v4/app/FragmentState;->a:Ljava/lang/String;

    iget-object v7, v1, Landroid/support/v4/app/FragmentState;->i:Landroid/os/Bundle;

    invoke-static {v5, v6, v7}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v6

    iput-object v6, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    .line 25104
    iget-object v6, v1, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    if-eqz v6, :cond_bb

    .line 25105
    iget-object v6, v1, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    invoke-virtual {v5}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 25106
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    iget-object v6, v1, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    iput-object v6, v5, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    .line 25108
    :cond_bb
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    iget v6, v1, Landroid/support/v4/app/FragmentState;->b:I

    invoke-virtual {v5, v6, v4}, Landroid/support/v4/app/Fragment;->a(ILandroid/support/v4/app/Fragment;)V

    .line 25109
    iget-object v4, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    iget-boolean v5, v1, Landroid/support/v4/app/FragmentState;->c:Z

    iput-boolean v5, v4, Landroid/support/v4/app/Fragment;->I:Z

    .line 25110
    iget-object v4, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    iput-boolean v9, v4, Landroid/support/v4/app/Fragment;->K:Z

    .line 25111
    iget-object v4, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    iget v5, v1, Landroid/support/v4/app/FragmentState;->d:I

    iput v5, v4, Landroid/support/v4/app/Fragment;->Q:I

    .line 25112
    iget-object v4, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    iget v5, v1, Landroid/support/v4/app/FragmentState;->e:I

    iput v5, v4, Landroid/support/v4/app/Fragment;->R:I

    .line 25113
    iget-object v4, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->f:Ljava/lang/String;

    iput-object v5, v4, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    .line 25114
    iget-object v4, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    iget-boolean v5, v1, Landroid/support/v4/app/FragmentState;->g:Z

    iput-boolean v5, v4, Landroid/support/v4/app/Fragment;->V:Z

    .line 25115
    iget-object v4, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    iget-boolean v5, v1, Landroid/support/v4/app/FragmentState;->h:Z

    iput-boolean v5, v4, Landroid/support/v4/app/Fragment;->U:Z

    .line 25116
    iget-object v4, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    iget-object v3, v3, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    iput-object v3, v4, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 25118
    sget-boolean v3, Landroid/support/v4/app/bl;->b:Z

    if-eqz v3, :cond_10a

    const-string v3, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Instantiated fragment "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 25121
    :cond_10a
    iget-object v3, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    .line 1908
    sget-boolean v4, Landroid/support/v4/app/bl;->b:Z

    if-eqz v4, :cond_12e

    const-string v4, "FragmentManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "restoreAllState: active #"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1909
    :cond_12e
    iget-object v4, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1913
    iput-object v8, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/support/v4/app/Fragment;

    .line 1904
    :goto_135
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_7c

    .line 1915
    :cond_139
    iget-object v1, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1916
    iget-object v1, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    if-nez v1, :cond_149

    .line 1917
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    .line 1919
    :cond_149
    sget-boolean v1, Landroid/support/v4/app/bl;->b:Z

    if-eqz v1, :cond_161

    const-string v1, "FragmentManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "restoreAllState: avail #"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1920
    :cond_161
    iget-object v1, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_135

    .line 1925
    :cond_16b
    if-eqz p2, :cond_1bb

    move v3, v2

    .line 1926
    :goto_16e
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1bb

    .line 1927
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1928
    iget v1, v0, Landroid/support/v4/app/Fragment;->D:I

    if-ltz v1, :cond_194

    .line 1929
    iget v1, v0, Landroid/support/v4/app/Fragment;->D:I

    iget-object v4, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_198

    .line 1930
    iget-object v1, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    iget v4, v0, Landroid/support/v4/app/Fragment;->D:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    iput-object v1, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    .line 1926
    :cond_194
    :goto_194
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_16e

    .line 1932
    :cond_198
    const-string v1, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Re-attaching retained fragment "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " target no longer exists: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Landroid/support/v4/app/Fragment;->D:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1934
    iput-object v8, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    goto :goto_194

    .line 1941
    :cond_1bb
    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->b:[I

    if-eqz v0, :cond_235

    .line 1942
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->b:[I

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    move v1, v2

    .line 1943
    :goto_1ca
    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->b:[I

    array-length v0, v0

    if-ge v1, v0, :cond_237

    .line 1944
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    iget-object v3, p1, Landroid/support/v4/app/FragmentManagerState;->b:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1945
    if-nez v0, :cond_1f8

    .line 1946
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No instantiated fragment for index #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Landroid/support/v4/app/FragmentManagerState;->b:[I

    aget v5, v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Landroid/support/v4/app/bl;->a(Ljava/lang/RuntimeException;)V

    .line 1949
    :cond_1f8
    iput-boolean v9, v0, Landroid/support/v4/app/Fragment;->F:Z

    .line 1950
    sget-boolean v3, Landroid/support/v4/app/bl;->b:Z

    if-eqz v3, :cond_21c

    const-string v3, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "restoreAllState: added #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1951
    :cond_21c
    iget-object v3, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_22c

    .line 1952
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already added!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1954
    :cond_22c
    iget-object v3, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1943
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1ca

    .line 1957
    :cond_235
    iput-object v8, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    .line 1961
    :cond_237
    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->c:[Landroid/support/v4/app/BackStackState;

    if-eqz v0, :cond_2a3

    .line 1962
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->c:[Landroid/support/v4/app/BackStackState;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    move v0, v2

    .line 1963
    :goto_246
    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->c:[Landroid/support/v4/app/BackStackState;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 1964
    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->c:[Landroid/support/v4/app/BackStackState;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Landroid/support/v4/app/BackStackState;->a(Landroid/support/v4/app/bl;)Landroid/support/v4/app/ak;

    move-result-object v1

    .line 1965
    sget-boolean v3, Landroid/support/v4/app/bl;->b:Z

    if-eqz v3, :cond_292

    .line 1966
    const-string v3, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "restoreAllState: back stack #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (index "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/support/v4/app/ak;->y:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1968
    new-instance v3, Landroid/support/v4/n/h;

    const-string v4, "FragmentManager"

    invoke-direct {v3, v4}, Landroid/support/v4/n/h;-><init>(Ljava/lang/String;)V

    .line 1969
    new-instance v4, Ljava/io/PrintWriter;

    invoke-direct {v4, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 1970
    const-string v3, "  "

    invoke-virtual {v1, v3, v4, v2}, Landroid/support/v4/app/ak;->a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    .line 1972
    :cond_292
    iget-object v3, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1973
    iget v3, v1, Landroid/support/v4/app/ak;->y:I

    if-ltz v3, :cond_2a0

    .line 1974
    iget v3, v1, Landroid/support/v4/app/ak;->y:I

    invoke-direct {p0, v3, v1}, Landroid/support/v4/app/bl;->a(ILandroid/support/v4/app/ak;)V

    .line 1963
    :cond_2a0
    add-int/lit8 v0, v0, 0x1

    goto :goto_246

    .line 1978
    :cond_2a3
    iput-object v8, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    goto/16 :goto_5
.end method

.method public final a(Landroid/support/v4/app/Fragment;II)V
    .registers 10

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 1293
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_26

    const-string v0, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "remove: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " nesting="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/support/v4/app/Fragment;->L:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 17489
    :cond_26
    iget v0, p1, Landroid/support/v4/app/Fragment;->L:I

    if-lez v0, :cond_56

    move v0, v1

    .line 1294
    :goto_2b
    if-nez v0, :cond_58

    move v0, v1

    .line 1295
    :goto_2e
    iget-boolean v2, p1, Landroid/support/v4/app/Fragment;->U:Z

    if-eqz v2, :cond_34

    if-eqz v0, :cond_55

    .line 1296
    :cond_34
    iget-object v2, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v2, :cond_3d

    .line 1297
    iget-object v2, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1299
    :cond_3d
    iget-boolean v2, p1, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v2, :cond_47

    iget-boolean v2, p1, Landroid/support/v4/app/Fragment;->Y:Z

    if-eqz v2, :cond_47

    .line 1300
    iput-boolean v1, p0, Landroid/support/v4/app/bl;->y:Z

    .line 1302
    :cond_47
    iput-boolean v5, p1, Landroid/support/v4/app/Fragment;->F:Z

    .line 1303
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->G:Z

    .line 1304
    if-eqz v0, :cond_5a

    move v2, v5

    :goto_4e
    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 1307
    :cond_55
    return-void

    :cond_56
    move v0, v5

    .line 17489
    goto :goto_2b

    :cond_58
    move v0, v5

    .line 1294
    goto :goto_2e

    :cond_5a
    move v2, v1

    .line 1304
    goto :goto_4e
.end method

.method final a(Landroid/support/v4/app/Fragment;IIIZ)V
    .registers 16

    .prologue
    const/4 v9, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 924
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->F:Z

    if-eqz v0, :cond_d

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->U:Z

    if-eqz v0, :cond_10

    :cond_d
    if-le p2, v5, :cond_10

    move p2, v5

    .line 927
    :cond_10
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->G:Z

    if-eqz v0, :cond_1a

    iget v0, p1, Landroid/support/v4/app/Fragment;->u:I

    if-le p2, v0, :cond_1a

    .line 929
    iget p2, p1, Landroid/support/v4/app/Fragment;->u:I

    .line 933
    :cond_1a
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->ae:Z

    if-eqz v0, :cond_25

    iget v0, p1, Landroid/support/v4/app/Fragment;->u:I

    if-ge v0, v9, :cond_25

    if-le p2, v6, :cond_25

    move p2, v6

    .line 936
    :cond_25
    iget v0, p1, Landroid/support/v4/app/Fragment;->u:I

    if-ge v0, p2, :cond_379

    .line 940
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->I:Z

    if-eqz v0, :cond_32

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->J:Z

    if-nez v0, :cond_32

    .line 1182
    :goto_31
    return-void

    .line 943
    :cond_32
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->v:Landroid/view/View;

    if-eqz v0, :cond_40

    .line 948
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->v:Landroid/view/View;

    .line 949
    iget v2, p1, Landroid/support/v4/app/Fragment;->w:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 951
    :cond_40
    iget v0, p1, Landroid/support/v4/app/Fragment;->u:I

    packed-switch v0, :pswitch_data_5da

    .line 1181
    :cond_45
    :goto_45
    iput p2, p1, Landroid/support/v4/app/Fragment;->u:I

    goto :goto_31

    .line 953
    :pswitch_48
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_60

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "moveto CREATED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    :cond_60
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    if-eqz v0, :cond_a6

    .line 955
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    iget-object v1, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 6166
    iget-object v1, v1, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    .line 955
    invoke-virtual {v1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 956
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    const-string v1, "android:view_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    .line 958
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    const-string v1, "android:target_state"

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/bl;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    .line 960
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_93

    .line 961
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    const-string v1, "android:target_req_state"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Landroid/support/v4/app/Fragment;->E:I

    .line 964
    :cond_93
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    const-string v1, "android:user_visible_hint"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p1, Landroid/support/v4/app/Fragment;->af:Z

    .line 966
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->af:Z

    if-nez v0, :cond_a6

    .line 967
    iput-boolean v5, p1, Landroid/support/v4/app/Fragment;->ae:Z

    .line 968
    if-le p2, v6, :cond_a6

    move p2, v6

    .line 973
    :cond_a6
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    .line 974
    iget-object v0, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->P:Landroid/support/v4/app/Fragment;

    .line 975
    iget-object v0, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_e7

    iget-object v0, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    iget-object v0, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    :goto_b6
    iput-object v0, p1, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 977
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 7148
    iput-boolean v5, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 7149
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-nez v0, :cond_ec

    move-object v0, v7

    .line 7150
    :goto_c1
    if-eqz v0, :cond_c8

    .line 7151
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 7152
    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    .line 979
    :cond_c8
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_f1

    .line 980
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onAttach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 975
    :cond_e7
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 6174
    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    goto :goto_b6

    .line 7149
    :cond_ec
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    .line 7162
    iget-object v0, v0, Landroid/support/v4/app/bh;->b:Landroid/app/Activity;

    goto :goto_c1

    .line 983
    :cond_f1
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->P:Landroid/support/v4/app/Fragment;

    .line 987
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->W:Z

    if-nez v0, :cond_140

    .line 988
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    .line 7935
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v1, :cond_101

    .line 7936
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 7991
    iput-boolean v3, v1, Landroid/support/v4/app/bl;->z:Z

    .line 7938
    :cond_101
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 7939
    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 7940
    iget-boolean v1, p1, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v1, :cond_125

    .line 7941
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onCreate()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7944
    :cond_125
    if-eqz v0, :cond_140

    .line 7945
    const-string v1, "android:support:fragments"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 7947
    if-eqz v0, :cond_140

    .line 7948
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-nez v1, :cond_136

    .line 7949
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->w()V

    .line 7951
    :cond_136
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v1, v0, v7}, Landroid/support/v4/app/bl;->a(Landroid/os/Parcelable;Ljava/util/List;)V

    .line 7952
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->n()V

    .line 990
    :cond_140
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->W:Z

    .line 991
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->I:Z

    if-eqz v0, :cond_179

    .line 995
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v7}, Landroid/support/v4/app/Fragment;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 997
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-eqz v0, :cond_26a

    .line 998
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->ad:Landroid/view/View;

    .line 999
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_260

    .line 1000
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->w(Landroid/view/View;)V

    .line 1004
    :goto_167
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->T:Z

    if-eqz v0, :cond_172

    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1005
    :cond_172
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1011
    :cond_179
    :goto_179
    :pswitch_179
    if-le p2, v5, :cond_2bb

    .line 1012
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_193

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "moveto ACTIVITY_CREATED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    :cond_193
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->I:Z

    if-nez v0, :cond_232

    .line 1015
    iget v0, p1, Landroid/support/v4/app/Fragment;->R:I

    if-eqz v0, :cond_5d7

    .line 1016
    iget-object v0, p0, Landroid/support/v4/app/bl;->w:Landroid/support/v4/app/bf;

    iget v1, p1, Landroid/support/v4/app/Fragment;->R:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bf;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1017
    if-nez v0, :cond_1e6

    iget-boolean v1, p1, Landroid/support/v4/app/Fragment;->K:Z

    if-nez v1, :cond_1e6

    .line 1018
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "No view found for id 0x"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Landroid/support/v4/app/Fragment;->R:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->j()Landroid/content/res/Resources;

    move-result-object v4

    iget v8, p1, Landroid/support/v4/app/Fragment;->R:I

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") for fragment "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Landroid/support/v4/app/bl;->a(Ljava/lang/RuntimeException;)V

    .line 1025
    :cond_1e6
    :goto_1e6
    iput-object v0, p1, Landroid/support/v4/app/Fragment;->ab:Landroid/view/ViewGroup;

    .line 1026
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    invoke-virtual {p1, v1}, Landroid/support/v4/app/Fragment;->b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    invoke-virtual {p1, v1, v0}, Landroid/support/v4/app/Fragment;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 1028
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-eqz v1, :cond_277

    .line 1029
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    iput-object v1, p1, Landroid/support/v4/app/Fragment;->ad:Landroid/view/View;

    .line 1030
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_26e

    .line 1031
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/cx;->w(Landroid/view/View;)V

    .line 1035
    :goto_209
    if-eqz v0, :cond_220

    .line 1036
    invoke-direct {p0, p1, p3, v5, p4}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;

    move-result-object v1

    .line 1038
    if-eqz v1, :cond_21b

    .line 1039
    iget-object v2, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-static {v2, v1}, Landroid/support/v4/app/bl;->b(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1040
    iget-object v2, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1042
    :cond_21b
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1044
    :cond_220
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->T:Z

    if-eqz v0, :cond_22b

    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1045
    :cond_22b
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1051
    :cond_232
    :goto_232
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    .line 8966
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v1, :cond_23c

    .line 8967
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 8991
    iput-boolean v3, v1, Landroid/support/v4/app/bl;->z:Z

    .line 8969
    :cond_23c
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 8970
    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    .line 8971
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_27a

    .line 8972
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onActivityCreated()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1002
    :cond_260
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/app/da;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    goto/16 :goto_167

    .line 1007
    :cond_26a
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->ad:Landroid/view/View;

    goto/16 :goto_179

    .line 1033
    :cond_26e
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/app/da;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    goto :goto_209

    .line 1047
    :cond_277
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->ad:Landroid/view/View;

    goto :goto_232

    .line 8975
    :cond_27a
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_283

    .line 8976
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->o()V

    .line 1052
    :cond_283
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-eqz v0, :cond_2b9

    .line 1053
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    .line 9467
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    if-eqz v0, :cond_296

    .line 9468
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ad:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 9469
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    .line 9471
    :cond_296
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 10269
    iput-boolean v5, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 9473
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_2b9

    .line 9474
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onViewStateRestored()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1055
    :cond_2b9
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    .line 1059
    :cond_2bb
    :pswitch_2bb
    if-le p2, v6, :cond_318

    .line 1060
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_2d5

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "moveto STARTED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 10981
    :cond_2d5
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_2e2

    .line 10982
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 11991
    iput-boolean v3, v0, Landroid/support/v4/app/bl;->z:Z

    .line 10983
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->k()Z

    .line 10985
    :cond_2e2
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 10986
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->e()V

    .line 10987
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_306

    .line 10988
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStart()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10991
    :cond_306
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_30f

    .line 10992
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->p()V

    .line 10994
    :cond_30f
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_318

    .line 10995
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->f()V

    .line 1064
    :cond_318
    :pswitch_318
    if-le p2, v9, :cond_45

    .line 1065
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_332

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "moveto RESUMED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    :cond_332
    iput-boolean v5, p1, Landroid/support/v4/app/Fragment;->H:Z

    .line 12000
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_341

    .line 12001
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 12991
    iput-boolean v3, v0, Landroid/support/v4/app/bl;->z:Z

    .line 12002
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->k()Z

    .line 12004
    :cond_341
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 12005
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->s()V

    .line 12006
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_365

    .line 12007
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onResume()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 12010
    :cond_365
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_373

    .line 12011
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->q()V

    .line 12012
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->k()Z

    .line 1068
    :cond_373
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    .line 1069
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    goto/16 :goto_45

    .line 1072
    :cond_379
    iget v0, p1, Landroid/support/v4/app/Fragment;->u:I

    if-le v0, p2, :cond_45

    .line 1073
    iget v0, p1, Landroid/support/v4/app/Fragment;->u:I

    packed-switch v0, :pswitch_data_5e8

    goto/16 :goto_45

    .line 1133
    :cond_384
    :goto_384
    :pswitch_384
    if-gtz p2, :cond_45

    .line 1134
    iget-boolean v0, p0, Landroid/support/v4/app/bl;->A:Z

    if-eqz v0, :cond_395

    .line 1135
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->v:Landroid/view/View;

    if-eqz v0, :cond_395

    .line 1142
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->v:Landroid/view/View;

    .line 1143
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->v:Landroid/view/View;

    .line 1144
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 1147
    :cond_395
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->v:Landroid/view/View;

    if-eqz v0, :cond_4ea

    .line 1152
    iput p2, p1, Landroid/support/v4/app/Fragment;->w:I

    move p2, v5

    .line 1153
    goto/16 :goto_45

    .line 1075
    :pswitch_39e
    const/4 v0, 0x5

    if-ge p2, v0, :cond_3e8

    .line 1076
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_3b9

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "movefrom RESUMED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 13119
    :cond_3b9
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_3c2

    .line 13120
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 14015
    invoke-virtual {v0, v9}, Landroid/support/v4/app/bl;->c(I)V

    .line 13122
    :cond_3c2
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 13123
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->t()V

    .line 13124
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_3e6

    .line 13125
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onPause()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1078
    :cond_3e6
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->H:Z

    .line 1081
    :cond_3e8
    :pswitch_3e8
    if-ge p2, v9, :cond_42f

    .line 1082
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_402

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "movefrom STARTED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 14131
    :cond_402
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_40b

    .line 14132
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->r()V

    .line 14134
    :cond_40b
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 14135
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->f()V

    .line 14136
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_42f

    .line 14137
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStop()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1086
    :cond_42f
    :pswitch_42f
    if-ge p2, v6, :cond_44c

    .line 1087
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_449

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "movefrom STOPPED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1088
    :cond_449
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->x()V

    .line 1091
    :cond_44c
    :pswitch_44c
    const/4 v0, 0x2

    if-ge p2, v0, :cond_384

    .line 1092
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_467

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "movefrom ACTIVITY_CREATED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    :cond_467
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-eqz v0, :cond_47a

    .line 1096
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    invoke-virtual {v0}, Landroid/support/v4/app/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_47a

    iget-object v0, p1, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    if-nez v0, :cond_47a

    .line 1097
    invoke-direct {p0, p1}, Landroid/support/v4/app/bl;->f(Landroid/support/v4/app/Fragment;)V

    .line 14163
    :cond_47a
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_483

    .line 14164
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 15032
    invoke-virtual {v0, v5}, Landroid/support/v4/app/bl;->c(I)V

    .line 14166
    :cond_483
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 14167
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->g()V

    .line 14168
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_4a7

    .line 14169
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroyView()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 14172
    :cond_4a7
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_4b0

    .line 14173
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->e()V

    .line 1101
    :cond_4b0
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-eqz v0, :cond_4e2

    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ab:Landroid/view/ViewGroup;

    if-eqz v0, :cond_4e2

    .line 1103
    iget v0, p0, Landroid/support/v4/app/bl;->t:I

    if-lez v0, :cond_5d4

    iget-boolean v0, p0, Landroid/support/v4/app/bl;->A:Z

    if-nez v0, :cond_5d4

    .line 1104
    invoke-direct {p0, p1, p3, v3, p4}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1107
    :goto_4c4
    if-eqz v0, :cond_4db

    .line 1109
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    iput-object v1, p1, Landroid/support/v4/app/Fragment;->v:Landroid/view/View;

    .line 1110
    iput p2, p1, Landroid/support/v4/app/Fragment;->w:I

    .line 1111
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 1112
    new-instance v2, Landroid/support/v4/app/bq;

    invoke-direct {v2, p0, v1, v0, p1}, Landroid/support/v4/app/bq;-><init>(Landroid/support/v4/app/bl;Landroid/view/View;Landroid/view/animation/Animation;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1124
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1126
    :cond_4db
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ab:Landroid/view/ViewGroup;

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1128
    :cond_4e2
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->ab:Landroid/view/ViewGroup;

    .line 1129
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 1130
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->ad:Landroid/view/View;

    goto/16 :goto_384

    .line 1155
    :cond_4ea
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_502

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "movefrom CREATED: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1156
    :cond_502
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->W:Z

    if-nez v0, :cond_533

    .line 15178
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_50f

    .line 15179
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->s()V

    .line 15181
    :cond_50f
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 15182
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->u()V

    .line 15183
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_533

    .line 15184
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroy()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1160
    :cond_533
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1161
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->c()V

    .line 1162
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_557

    .line 1163
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDetach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1166
    :cond_557
    if-nez p5, :cond_45

    .line 1167
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->W:Z

    if-nez v0, :cond_5ca

    .line 15256
    iget v0, p1, Landroid/support/v4/app/Fragment;->z:I

    if-ltz v0, :cond_45

    .line 15260
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_579

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Freeing fragment index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 15261
    :cond_579
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    iget v1, p1, Landroid/support/v4/app/Fragment;->z:I

    invoke-virtual {v0, v1, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 15262
    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    if-nez v0, :cond_58b

    .line 15263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    .line 15265
    :cond_58b
    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    iget v1, p1, Landroid/support/v4/app/Fragment;->z:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 15266
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bh;->b(Ljava/lang/String;)V

    .line 15387
    const/4 v0, -0x1

    iput v0, p1, Landroid/support/v4/app/Fragment;->z:I

    .line 15388
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    .line 15389
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->F:Z

    .line 15390
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->G:Z

    .line 15391
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->H:Z

    .line 15392
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->I:Z

    .line 15393
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->J:Z

    .line 15394
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->K:Z

    .line 15395
    iput v3, p1, Landroid/support/v4/app/Fragment;->L:I

    .line 15396
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 15397
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 15398
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    .line 15399
    iput v3, p1, Landroid/support/v4/app/Fragment;->Q:I

    .line 15400
    iput v3, p1, Landroid/support/v4/app/Fragment;->R:I

    .line 15401
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    .line 15402
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->T:Z

    .line 15403
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->U:Z

    .line 15404
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->W:Z

    .line 15405
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    .line 15406
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->ah:Z

    .line 15407
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->ai:Z

    goto/16 :goto_45

    .line 1170
    :cond_5ca
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    .line 1171
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->P:Landroid/support/v4/app/Fragment;

    .line 1172
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 1173
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    goto/16 :goto_45

    :cond_5d4
    move-object v0, v7

    goto/16 :goto_4c4

    :cond_5d7
    move-object v0, v7

    goto/16 :goto_1e6

    .line 951
    :pswitch_data_5da
    .packed-switch 0x0
        :pswitch_48
        :pswitch_179
        :pswitch_2bb
        :pswitch_2bb
        :pswitch_318
    .end packed-switch

    .line 1073
    :pswitch_data_5e8
    .packed-switch 0x1
        :pswitch_384
        :pswitch_44c
        :pswitch_42f
        :pswitch_3e8
        :pswitch_39e
    .end packed-switch
.end method

.method public final a(Landroid/support/v4/app/Fragment;Z)V
    .registers 7

    .prologue
    const/4 v3, 0x1

    .line 1271
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-nez v0, :cond_c

    .line 1272
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    .line 1274
    :cond_c
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_24

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "add: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 17237
    :cond_24
    iget v0, p1, Landroid/support/v4/app/Fragment;->z:I

    if-gez v0, :cond_67

    .line 17241
    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_34

    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_88

    .line 17242
    :cond_34
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_3f

    .line 17243
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    .line 17245
    :cond_3f
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->a(ILandroid/support/v4/app/Fragment;)V

    .line 17246
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 17252
    :goto_4f
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_67

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Allocated fragment index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1276
    :cond_67
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->U:Z

    if-nez v0, :cond_c2

    .line 1277
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a9

    .line 1278
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17249
    :cond_88
    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->a(ILandroid/support/v4/app/Fragment;)V

    .line 17250
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    iget v1, p1, Landroid/support/v4/app/Fragment;->z:I

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_4f

    .line 1280
    :cond_a9
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1281
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->F:Z

    .line 1282
    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/support/v4/app/Fragment;->G:Z

    .line 1283
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v0, :cond_bd

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Y:Z

    if-eqz v0, :cond_bd

    .line 1284
    iput-boolean v3, p0, Landroid/support/v4/app/bl;->y:Z

    .line 1286
    :cond_bd
    if-eqz p2, :cond_c2

    .line 1287
    invoke-direct {p0, p1}, Landroid/support/v4/app/bl;->c(Landroid/support/v4/app/Fragment;)V

    .line 1290
    :cond_c2
    return-void
.end method

.method public final a(Landroid/support/v4/app/bh;Landroid/support/v4/app/bf;Landroid/support/v4/app/Fragment;)V
    .registers 6

    .prologue
    .line 1984
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    if-eqz v0, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1985
    :cond_c
    iput-object p1, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 1986
    iput-object p2, p0, Landroid/support/v4/app/bl;->w:Landroid/support/v4/app/bf;

    .line 1987
    iput-object p3, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    .line 1988
    return-void
.end method

.method public final a(Landroid/support/v4/app/bk;)V
    .registers 3

    .prologue
    .line 614
    iget-object v0, p0, Landroid/support/v4/app/bl;->s:Ljava/util/ArrayList;

    if-nez v0, :cond_b

    .line 615
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->s:Ljava/util/ArrayList;

    .line 617
    :cond_b
    iget-object v0, p0, Landroid/support/v4/app/bl;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 618
    return-void
.end method

.method public final a(Ljava/lang/Runnable;Z)V
    .registers 5

    .prologue
    .line 1465
    if-nez p2, :cond_5

    .line 1466
    invoke-direct {p0}, Landroid/support/v4/app/bl;->u()V

    .line 1468
    :cond_5
    monitor-enter p0

    .line 1469
    :try_start_6
    iget-boolean v0, p0, Landroid/support/v4/app/bl;->A:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    if-nez v0, :cond_19

    .line 1470
    :cond_e
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity has been destroyed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1480
    :catchall_16
    move-exception v0

    monitor-exit p0
    :try_end_18
    .catchall {:try_start_6 .. :try_end_18} :catchall_16

    throw v0

    .line 1472
    :cond_19
    :try_start_19
    iget-object v0, p0, Landroid/support/v4/app/bl;->i:Ljava/util/ArrayList;

    if-nez v0, :cond_24

    .line 1473
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->i:Ljava/util/ArrayList;

    .line 1475
    :cond_24
    iget-object v0, p0, Landroid/support/v4/app/bl;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1476
    iget-object v0, p0, Landroid/support/v4/app/bl;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_44

    .line 1477
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 19170
    iget-object v0, v0, Landroid/support/v4/app/bh;->d:Landroid/os/Handler;

    .line 1477
    iget-object v1, p0, Landroid/support/v4/app/bl;->F:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1478
    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 20170
    iget-object v0, v0, Landroid/support/v4/app/bh;->d:Landroid/os/Handler;

    .line 1478
    iget-object v1, p0, Landroid/support/v4/app/bl;->F:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1480
    :cond_44
    monitor-exit p0
    :try_end_45
    .catchall {:try_start_19 .. :try_end_45} :catchall_16

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 13

    .prologue
    const/4 v1, 0x0

    .line 694
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 697
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_272

    .line 698
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 699
    if-lez v4, :cond_272

    .line 700
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Active Fragments in "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 701
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 702
    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 703
    :goto_39
    if-ge v2, v4, :cond_272

    .line 704
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 705
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 706
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 707
    if-eqz v0, :cond_26d

    .line 4829
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mFragmentId=#"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 4830
    iget v5, v0, Landroid/support/v4/app/Fragment;->Q:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 4831
    const-string v5, " mContainerId=#"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 4832
    iget v5, v0, Landroid/support/v4/app/Fragment;->R:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 4833
    const-string v5, " mTag="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 4834
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mState="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Landroid/support/v4/app/Fragment;->u:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(I)V

    .line 4835
    const-string v5, " mIndex="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Landroid/support/v4/app/Fragment;->z:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(I)V

    .line 4836
    const-string v5, " mWho="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 4837
    const-string v5, " mBackStackNesting="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Landroid/support/v4/app/Fragment;->L:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(I)V

    .line 4838
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mAdded="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Landroid/support/v4/app/Fragment;->F:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    .line 4839
    const-string v5, " mRemoving="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Landroid/support/v4/app/Fragment;->G:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    .line 4840
    const-string v5, " mResumed="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Landroid/support/v4/app/Fragment;->H:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    .line 4841
    const-string v5, " mFromLayout="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Landroid/support/v4/app/Fragment;->I:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    .line 4842
    const-string v5, " mInLayout="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Landroid/support/v4/app/Fragment;->J:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Z)V

    .line 4843
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mHidden="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Landroid/support/v4/app/Fragment;->T:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    .line 4844
    const-string v5, " mDetached="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Landroid/support/v4/app/Fragment;->U:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    .line 4845
    const-string v5, " mMenuVisible="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Landroid/support/v4/app/Fragment;->Y:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    .line 4846
    const-string v5, " mHasMenu="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Landroid/support/v4/app/Fragment;->X:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Z)V

    .line 4847
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mRetainInstance="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Landroid/support/v4/app/Fragment;->V:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    .line 4848
    const-string v5, " mRetaining="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Landroid/support/v4/app/Fragment;->W:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    .line 4849
    const-string v5, " mUserVisibleHint="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Landroid/support/v4/app/Fragment;->af:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Z)V

    .line 4850
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    if-eqz v5, :cond_13e

    .line 4851
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mFragmentManager="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 4852
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 4854
    :cond_13e
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-eqz v5, :cond_14f

    .line 4855
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mHost="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 4856
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 4858
    :cond_14f
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->P:Landroid/support/v4/app/Fragment;

    if-eqz v5, :cond_160

    .line 4859
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mParentFragment="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 4860
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->P:Landroid/support/v4/app/Fragment;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 4862
    :cond_160
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->B:Landroid/os/Bundle;

    if-eqz v5, :cond_171

    .line 4863
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mArguments="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Landroid/support/v4/app/Fragment;->B:Landroid/os/Bundle;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 4865
    :cond_171
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    if-eqz v5, :cond_182

    .line 4866
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mSavedFragmentState="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 4867
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 4869
    :cond_182
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    if-eqz v5, :cond_193

    .line 4870
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mSavedViewState="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 4871
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 4873
    :cond_193
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    if-eqz v5, :cond_1ae

    .line 4874
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mTarget="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 4875
    const-string v5, " mTargetRequestCode="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 4876
    iget v5, v0, Landroid/support/v4/app/Fragment;->E:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(I)V

    .line 4878
    :cond_1ae
    iget v5, v0, Landroid/support/v4/app/Fragment;->aa:I

    if-eqz v5, :cond_1bf

    .line 4879
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mNextAnim="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Landroid/support/v4/app/Fragment;->aa:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(I)V

    .line 4881
    :cond_1bf
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->ab:Landroid/view/ViewGroup;

    if-eqz v5, :cond_1d0

    .line 4882
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mContainer="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Landroid/support/v4/app/Fragment;->ab:Landroid/view/ViewGroup;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 4884
    :cond_1d0
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-eqz v5, :cond_1e1

    .line 4885
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mView="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 4887
    :cond_1e1
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->ad:Landroid/view/View;

    if-eqz v5, :cond_1f2

    .line 4888
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mInnerView="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 4890
    :cond_1f2
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->v:Landroid/view/View;

    if-eqz v5, :cond_210

    .line 4891
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mAnimatingAway="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Landroid/support/v4/app/Fragment;->v:Landroid/view/View;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 4892
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mStateAfterAnimating="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 4893
    iget v5, v0, Landroid/support/v4/app/Fragment;->w:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(I)V

    .line 4895
    :cond_210
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    if-eqz v5, :cond_234

    .line 4896
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "Loader Manager:"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 4897
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, p2, p3, p4}, Landroid/support/v4/app/ct;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 4899
    :cond_234
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v5, :cond_26d

    .line 4900
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Child "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 4901
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5, p2, p3, p4}, Landroid/support/v4/app/bl;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 703
    :cond_26d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_39

    .line 714
    :cond_272
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_2ac

    .line 715
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 716
    if-lez v4, :cond_2ac

    .line 717
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Added Fragments:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 718
    :goto_287
    if-ge v2, v4, :cond_2ac

    .line 719
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 720
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 721
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 718
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_287

    .line 726
    :cond_2ac
    iget-object v0, p0, Landroid/support/v4/app/bl;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_2e6

    .line 727
    iget-object v0, p0, Landroid/support/v4/app/bl;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 728
    if-lez v4, :cond_2e6

    .line 729
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Fragments Created Menus:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 730
    :goto_2c1
    if-ge v2, v4, :cond_2e6

    .line 731
    iget-object v0, p0, Landroid/support/v4/app/bl;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 732
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 733
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 730
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2c1

    .line 738
    :cond_2e6
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_323

    .line 739
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 740
    if-lez v4, :cond_323

    .line 741
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 742
    :goto_2fb
    if-ge v2, v4, :cond_323

    .line 743
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ak;

    .line 744
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 745
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/ak;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 746
    invoke-virtual {v0, v3, p3}, Landroid/support/v4/app/ak;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 742
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2fb

    .line 751
    :cond_323
    monitor-enter p0

    .line 752
    :try_start_324
    iget-object v0, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    if-eqz v0, :cond_35a

    .line 753
    iget-object v0, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 754
    if-lez v3, :cond_35a

    .line 755
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack Indices:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 756
    :goto_339
    if-ge v2, v3, :cond_35a

    .line 757
    iget-object v0, p0, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ak;

    .line 758
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 759
    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 756
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_339

    .line 764
    :cond_35a
    iget-object v0, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    if-eqz v0, :cond_37b

    iget-object v0, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_37b

    .line 765
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAvailBackStackIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 766
    iget-object v0, p0, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 768
    :cond_37b
    monitor-exit p0
    :try_end_37c
    .catchall {:try_start_324 .. :try_end_37c} :catchall_3b1

    .line 770
    iget-object v0, p0, Landroid/support/v4/app/bl;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_3b4

    .line 771
    iget-object v0, p0, Landroid/support/v4/app/bl;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 772
    if-lez v2, :cond_3b4

    .line 773
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Actions:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 774
    :goto_390
    if-ge v1, v2, :cond_3b4

    .line 775
    iget-object v0, p0, Landroid/support/v4/app/bl;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 776
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    .line 777
    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 774
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_390

    .line 768
    :catchall_3b1
    move-exception v0

    :try_start_3b2
    monitor-exit p0
    :try_end_3b3
    .catchall {:try_start_3b2 .. :try_end_3b3} :catchall_3b1

    throw v0

    .line 782
    :cond_3b4
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "FragmentManager misc state:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 783
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mHost="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 784
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/bl;->w:Landroid/support/v4/app/bf;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 785
    iget-object v0, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_3e7

    .line 786
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mParent="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 788
    :cond_3e7
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mCurState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/bl;->t:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 789
    const-string v0, " mStateSaved="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/bl;->z:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 790
    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/bl;->A:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 791
    iget-boolean v0, p0, Landroid/support/v4/app/bl;->y:Z

    if-eqz v0, :cond_419

    .line 792
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNeedMenuInvalidate="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 793
    iget-boolean v0, p0, Landroid/support/v4/app/bl;->y:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 795
    :cond_419
    iget-object v0, p0, Landroid/support/v4/app/bl;->B:Ljava/lang/String;

    if-eqz v0, :cond_42a

    .line 796
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNoTransactionsBecause="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 797
    iget-object v0, p0, Landroid/support/v4/app/bl;->B:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 799
    :cond_42a
    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_44b

    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_44b

    .line 800
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mAvailIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 801
    iget-object v0, p0, Landroid/support/v4/app/bl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 803
    :cond_44b
    return-void
.end method

.method public final a(II)Z
    .registers 6

    .prologue
    .line 594
    invoke-direct {p0}, Landroid/support/v4/app/bl;->u()V

    .line 4545
    invoke-virtual {p0}, Landroid/support/v4/app/bl;->k()Z

    .line 596
    if-gez p1, :cond_1d

    .line 597
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 599
    :cond_1d
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Landroid/support/v4/app/bl;->a(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/Menu;)Z
    .registers 9

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 2099
    .line 2100
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_39

    move v1, v2

    move v3, v2

    .line 2101
    :goto_8
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3a

    .line 2102
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 2103
    if-eqz v0, :cond_35

    .line 26055
    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->T:Z

    if-nez v4, :cond_3d

    .line 26056
    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v4, :cond_3b

    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->Y:Z

    if-eqz v4, :cond_3b

    move v4, v5

    .line 26060
    :goto_27
    iget-object v6, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v6, :cond_32

    .line 26061
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->a(Landroid/view/Menu;)Z

    move-result v0

    or-int/2addr v4, v0

    .line 2104
    :cond_32
    :goto_32
    if-eqz v4, :cond_35

    move v3, v5

    .line 2101
    :cond_35
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    :cond_39
    move v3, v2

    .line 2110
    :cond_3a
    return v3

    :cond_3b
    move v4, v2

    goto :goto_27

    :cond_3d
    move v4, v2

    goto :goto_32
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .registers 11

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 2067
    .line 2068
    const/4 v1, 0x0

    .line 2069
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_48

    move v4, v5

    move v3, v5

    .line 2070
    :goto_9
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_49

    .line 2071
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 2072
    if-eqz v0, :cond_6e

    .line 26041
    iget-boolean v6, v0, Landroid/support/v4/app/Fragment;->T:Z

    if-nez v6, :cond_72

    .line 26042
    iget-boolean v6, v0, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v6, :cond_70

    iget-boolean v6, v0, Landroid/support/v4/app/Fragment;->Y:Z

    if-eqz v6, :cond_70

    .line 26044
    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    move v6, v2

    .line 26046
    :goto_2b
    iget-object v7, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v7, :cond_36

    .line 26047
    iget-object v7, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v7, p1, p2}, Landroid/support/v4/app/bl;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v7

    or-int/2addr v6, v7

    .line 2073
    :cond_36
    :goto_36
    if-eqz v6, :cond_6e

    .line 2075
    if-nez v1, :cond_3f

    .line 2076
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2078
    :cond_3f
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v2

    .line 2070
    :goto_43
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    goto :goto_9

    :cond_48
    move v3, v5

    .line 2084
    :cond_49
    iget-object v0, p0, Landroid/support/v4/app/bl;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_6b

    .line 2085
    :goto_4d
    iget-object v0, p0, Landroid/support/v4/app/bl;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v5, v0, :cond_6b

    .line 2086
    iget-object v0, p0, Landroid/support/v4/app/bl;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 2087
    if-eqz v1, :cond_65

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_68

    .line 2088
    :cond_65
    invoke-static {}, Landroid/support/v4/app/Fragment;->v()V

    .line 2085
    :cond_68
    add-int/lit8 v5, v5, 0x1

    goto :goto_4d

    .line 2093
    :cond_6b
    iput-object v1, p0, Landroid/support/v4/app/bl;->p:Ljava/util/ArrayList;

    .line 2095
    return v3

    :cond_6e
    move v0, v3

    goto :goto_43

    :cond_70
    move v6, v5

    goto :goto_2b

    :cond_72
    move v6, v5

    goto :goto_36
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .registers 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2114
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_2f

    move v1, v2

    .line 2115
    :goto_7
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2f

    .line 2116
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 2117
    if-eqz v0, :cond_40

    .line 26068
    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->T:Z

    if-nez v4, :cond_3e

    .line 26069
    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v4, :cond_30

    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->Y:Z

    if-eqz v4, :cond_30

    .line 26070
    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/view/MenuItem;)Z

    move-result v4

    if-eqz v4, :cond_30

    move v0, v3

    .line 2118
    :goto_2c
    if-eqz v0, :cond_40

    move v2, v3

    .line 2124
    :cond_2f
    return v2

    .line 26074
    :cond_30
    iget-object v4, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v4, :cond_3e

    .line 26075
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_3e

    move v0, v3

    .line 26076
    goto :goto_2c

    :cond_3e
    move v0, v2

    .line 26080
    goto :goto_2c

    .line 2115
    :cond_40
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7
.end method

.method public final a(Ljava/lang/String;I)Z
    .registers 4

    .prologue
    .line 575
    invoke-direct {p0}, Landroid/support/v4/app/bl;->u()V

    .line 3545
    invoke-virtual {p0}, Landroid/support/v4/app/bl;->k()Z

    .line 577
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0, p2}, Landroid/support/v4/app/bl;->a(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method final a(Ljava/lang/String;II)Z
    .registers 15

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1613
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    if-nez v0, :cond_8

    .line 1682
    :cond_7
    :goto_7
    return v3

    .line 1616
    :cond_8
    if-nez p1, :cond_37

    if-gez p2, :cond_37

    and-int/lit8 v0, p3, 0x1

    if-nez v0, :cond_37

    .line 1617
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1618
    if-ltz v0, :cond_7

    .line 1621
    iget-object v1, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ak;

    .line 1622
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 1623
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    .line 1624
    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/ak;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    .line 1625
    invoke-virtual {v0, v2, v4, v1, v3}, Landroid/support/v4/app/ak;->a(ZLandroid/support/v4/app/ap;Landroid/util/SparseArray;Landroid/util/SparseArray;)Landroid/support/v4/app/ap;

    .line 1680
    :cond_32
    invoke-virtual {p0}, Landroid/support/v4/app/bl;->l()V

    move v3, v2

    .line 1682
    goto :goto_7

    .line 1628
    :cond_37
    const/4 v0, -0x1

    .line 1629
    if-nez p1, :cond_3c

    if-ltz p2, :cond_87

    .line 1632
    :cond_3c
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 1633
    :goto_44
    if-ltz v1, :cond_61

    .line 1634
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ak;

    .line 1635
    if-eqz p1, :cond_58

    .line 22971
    iget-object v5, v0, Landroid/support/v4/app/ak;->w:Ljava/lang/String;

    .line 1635
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_61

    .line 1638
    :cond_58
    if-ltz p2, :cond_5e

    iget v0, v0, Landroid/support/v4/app/ak;->y:I

    if-eq p2, v0, :cond_61

    .line 1641
    :cond_5e
    add-int/lit8 v1, v1, -0x1

    .line 1642
    goto :goto_44

    .line 1643
    :cond_61
    if-ltz v1, :cond_7

    .line 1646
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_86

    .line 1647
    add-int/lit8 v1, v1, -0x1

    .line 1649
    :goto_69
    if-ltz v1, :cond_86

    .line 1650
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ak;

    .line 1651
    if-eqz p1, :cond_7d

    .line 23971
    iget-object v5, v0, Landroid/support/v4/app/ak;->w:Ljava/lang/String;

    .line 1651
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_83

    :cond_7d
    if-ltz p2, :cond_86

    iget v0, v0, Landroid/support/v4/app/ak;->y:I

    if-ne p2, v0, :cond_86

    .line 1653
    :cond_83
    add-int/lit8 v1, v1, -0x1

    .line 1654
    goto :goto_69

    :cond_86
    move v0, v1

    .line 1660
    :cond_87
    iget-object v1, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_7

    .line 1663
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1665
    iget-object v1, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_9e
    if-le v1, v0, :cond_ac

    .line 1666
    iget-object v5, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1665
    add-int/lit8 v1, v1, -0x1

    goto :goto_9e

    .line 1668
    :cond_ac
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    .line 1669
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    .line 1670
    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9}, Landroid/util/SparseArray;-><init>()V

    move v1, v3

    .line 1671
    :goto_bd
    if-gt v1, v7, :cond_cc

    .line 1672
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ak;

    invoke-virtual {v0, v8, v9}, Landroid/support/v4/app/ak;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    .line 1671
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_bd

    :cond_cc
    move-object v5, v4

    move v4, v3

    .line 1675
    :goto_ce
    if-gt v4, v7, :cond_32

    .line 1676
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_ec

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v10, "Popping back stack state: "

    invoke-direct {v1, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1677
    :cond_ec
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ak;

    if-ne v4, v7, :cond_fe

    move v1, v2

    :goto_f5
    invoke-virtual {v0, v1, v5, v8, v9}, Landroid/support/v4/app/ak;->a(ZLandroid/support/v4/app/ap;Landroid/util/SparseArray;Landroid/util/SparseArray;)Landroid/support/v4/app/ap;

    move-result-object v1

    .line 1675
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move-object v5, v1

    goto :goto_ce

    :cond_fe
    move v1, v3

    .line 1677
    goto :goto_f5
.end method

.method public final b(I)V
    .registers 5

    .prologue
    .line 582
    if-gez p1, :cond_17

    .line 583
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 585
    :cond_17
    new-instance v0, Landroid/support/v4/app/bp;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/app/bp;-><init>(Landroid/support/v4/app/bl;I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/bl;->a(Ljava/lang/Runnable;Z)V

    .line 590
    return-void
.end method

.method public final b(Landroid/support/v4/app/Fragment;)V
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 894
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->ae:Z

    if-eqz v0, :cond_c

    .line 895
    iget-boolean v0, p0, Landroid/support/v4/app/bl;->k:Z

    if-eqz v0, :cond_d

    .line 897
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/bl;->C:Z

    .line 903
    :cond_c
    :goto_c
    return-void

    .line 900
    :cond_d
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->ae:Z

    .line 901
    iget v2, p0, Landroid/support/v4/app/bl;->t:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    goto :goto_c
.end method

.method public final b(Landroid/support/v4/app/Fragment;II)V
    .registers 8

    .prologue
    const/4 v3, 0x1

    .line 1310
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_19

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hide: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1311
    :cond_19
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->T:Z

    if-nez v0, :cond_4c

    .line 1312
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->T:Z

    .line 1313
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-eqz v0, :cond_3b

    .line 1314
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1316
    if-eqz v0, :cond_34

    .line 1317
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-static {v1, v0}, Landroid/support/v4/app/bl;->b(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1318
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1320
    :cond_34
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1322
    :cond_3b
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->F:Z

    if-eqz v0, :cond_49

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v0, :cond_49

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Y:Z

    if-eqz v0, :cond_49

    .line 1323
    iput-boolean v3, p0, Landroid/support/v4/app/bl;->y:Z

    .line 1325
    :cond_49
    invoke-static {}, Landroid/support/v4/app/Fragment;->m()V

    .line 1327
    :cond_4c
    return-void
.end method

.method public final b(Landroid/support/v4/app/bk;)V
    .registers 3

    .prologue
    .line 622
    iget-object v0, p0, Landroid/support/v4/app/bl;->s:Ljava/util/ArrayList;

    if-eqz v0, :cond_9

    .line 623
    iget-object v0, p0, Landroid/support/v4/app/bl;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 625
    :cond_9
    return-void
.end method

.method public final b(Landroid/view/Menu;)V
    .registers 5

    .prologue
    .line 2142
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_29

    .line 2143
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_29

    .line 2144
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 2145
    if-eqz v0, :cond_25

    .line 26098
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->T:Z

    if-nez v2, :cond_25

    .line 26102
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v2, :cond_25

    .line 26103
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->b(Landroid/view/Menu;)V

    .line 2143
    :cond_25
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 2150
    :cond_29
    return-void
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 545
    invoke-virtual {p0}, Landroid/support/v4/app/bl;->k()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/MenuItem;)Z
    .registers 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2128
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_2d

    move v1, v2

    .line 2129
    :goto_7
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2d

    .line 2130
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 2131
    if-eqz v0, :cond_30

    .line 26084
    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->T:Z

    if-nez v4, :cond_2e

    .line 26088
    iget-object v4, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v4, :cond_2e

    .line 26089
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->b(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_2e

    move v0, v3

    .line 2132
    :goto_2a
    if-eqz v0, :cond_30

    move v2, v3

    .line 2138
    :cond_2d
    return v2

    :cond_2e
    move v0, v2

    .line 26094
    goto :goto_2a

    .line 2129
    :cond_30
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 550
    new-instance v0, Landroid/support/v4/app/bn;

    invoke-direct {v0, p0}, Landroid/support/v4/app/bn;-><init>(Landroid/support/v4/app/bl;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/bl;->a(Ljava/lang/Runnable;Z)V

    .line 555
    return-void
.end method

.method final c(I)V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 1189
    invoke-virtual {p0, p1, v0, v0, v0}, Landroid/support/v4/app/bl;->a(IIIZ)V

    .line 1190
    return-void
.end method

.method public final c(Landroid/support/v4/app/Fragment;II)V
    .registers 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1330
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_1a

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "show: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1331
    :cond_1a
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->T:Z

    if-eqz v0, :cond_4a

    .line 1332
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->T:Z

    .line 1333
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-eqz v0, :cond_39

    .line 1334
    invoke-direct {p0, p1, p2, v4, p3}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1336
    if-eqz v0, :cond_34

    .line 1337
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-static {v1, v0}, Landroid/support/v4/app/bl;->b(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1338
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1340
    :cond_34
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1342
    :cond_39
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->F:Z

    if-eqz v0, :cond_47

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v0, :cond_47

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Y:Z

    if-eqz v0, :cond_47

    .line 1343
    iput-boolean v4, p0, Landroid/support/v4/app/bl;->y:Z

    .line 1345
    :cond_47
    invoke-static {}, Landroid/support/v4/app/Fragment;->m()V

    .line 1347
    :cond_4a
    return-void
.end method

.method public final d(Landroid/support/v4/app/Fragment;II)V
    .registers 10

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 1350
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_1a

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "detach: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1351
    :cond_1a
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->U:Z

    if-nez v0, :cond_58

    .line 1352
    iput-boolean v2, p1, Landroid/support/v4/app/Fragment;->U:Z

    .line 1353
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->F:Z

    if-eqz v0, :cond_58

    .line 1355
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_45

    .line 1356
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_40

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "remove from detach: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1357
    :cond_40
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1359
    :cond_45
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v0, :cond_4f

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Y:Z

    if-eqz v0, :cond_4f

    .line 1360
    iput-boolean v2, p0, Landroid/support/v4/app/bl;->y:Z

    .line 1362
    :cond_4f
    iput-boolean v5, p1, Landroid/support/v4/app/Fragment;->F:Z

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    .line 1363
    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 1366
    :cond_58
    return-void
.end method

.method public final d()Z
    .registers 4

    .prologue
    .line 559
    invoke-direct {p0}, Landroid/support/v4/app/bl;->u()V

    .line 2545
    invoke-virtual {p0}, Landroid/support/v4/app/bl;->k()Z

    .line 561
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/support/v4/app/bl;->a(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 566
    new-instance v0, Landroid/support/v4/app/bo;

    invoke-direct {v0, p0}, Landroid/support/v4/app/bo;-><init>(Landroid/support/v4/app/bl;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/bl;->a(Ljava/lang/Runnable;Z)V

    .line 571
    return-void
.end method

.method public final e(Landroid/support/v4/app/Fragment;II)V
    .registers 10

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1369
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_1a

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "attach: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1370
    :cond_1a
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->U:Z

    if-eqz v0, :cond_7e

    .line 1371
    iput-boolean v5, p1, Landroid/support/v4/app/Fragment;->U:Z

    .line 1372
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->F:Z

    if-nez v0, :cond_7e

    .line 1373
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-nez v0, :cond_2f

    .line 1374
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    .line 1376
    :cond_2f
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 1377
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1379
    :cond_4c
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_64

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "add from attach: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1380
    :cond_64
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1381
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->F:Z

    .line 1382
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v0, :cond_75

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->Y:Z

    if-eqz v0, :cond_75

    .line 1383
    iput-boolean v3, p0, Landroid/support/v4/app/bl;->y:Z

    .line 1385
    :cond_75
    iget v2, p0, Landroid/support/v4/app/bl;->t:I

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 1388
    :cond_7e
    return-void
.end method

.method public final f()I
    .registers 2

    .prologue
    .line 604
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final g()Landroid/support/v4/app/bj;
    .registers 3

    .prologue
    .line 609
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/bj;

    return-object v0
.end method

.method public final h()Ljava/util/List;
    .registers 2

    .prologue
    .line 656
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final i()Z
    .registers 2

    .prologue
    .line 674
    iget-boolean v0, p0, Landroid/support/v4/app/bl;->A:Z

    return v0
.end method

.method final j()V
    .registers 3

    .prologue
    .line 1226
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_5

    .line 1234
    :cond_4
    return-void

    .line 1228
    :cond_5
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1229
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1230
    if-eqz v0, :cond_1c

    .line 1231
    invoke-virtual {p0, v0}, Landroid/support/v4/app/bl;->b(Landroid/support/v4/app/Fragment;)V

    .line 1228
    :cond_1c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7
.end method

.method public final k()Z
    .registers 7

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1543
    iget-boolean v1, p0, Landroid/support/v4/app/bl;->k:Z

    if-eqz v1, :cond_e

    .line 1544
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Recursive entry to executePendingTransactions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1547
    :cond_e
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v3, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 21170
    iget-object v3, v3, Landroid/support/v4/app/bh;->d:Landroid/os/Handler;

    .line 1547
    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v1, v3, :cond_24

    .line 1548
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be called from main thread of process"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_24
    move v1, v2

    .line 1556
    :goto_25
    monitor-enter p0

    .line 1557
    :try_start_26
    iget-object v3, p0, Landroid/support/v4/app/bl;->i:Ljava/util/ArrayList;

    if-eqz v3, :cond_32

    iget-object v3, p0, Landroid/support/v4/app/bl;->i:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_5a

    .line 1558
    :cond_32
    monitor-exit p0
    :try_end_33
    .catchall {:try_start_26 .. :try_end_33} :catchall_97

    .line 1579
    iget-boolean v0, p0, Landroid/support/v4/app/bl;->C:Z

    if-eqz v0, :cond_a5

    move v3, v2

    move v4, v2

    .line 1581
    :goto_39
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_9e

    .line 1582
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1583
    if-eqz v0, :cond_56

    iget-object v5, v0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    if-eqz v5, :cond_56

    .line 1584
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->a()Z

    move-result v0

    or-int/2addr v4, v0

    .line 1581
    :cond_56
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_39

    .line 1561
    :cond_5a
    :try_start_5a
    iget-object v1, p0, Landroid/support/v4/app/bl;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1562
    iget-object v1, p0, Landroid/support/v4/app/bl;->j:[Ljava/lang/Runnable;

    if-eqz v1, :cond_69

    iget-object v1, p0, Landroid/support/v4/app/bl;->j:[Ljava/lang/Runnable;

    array-length v1, v1

    if-ge v1, v3, :cond_6d

    .line 1563
    :cond_69
    new-array v1, v3, [Ljava/lang/Runnable;

    iput-object v1, p0, Landroid/support/v4/app/bl;->j:[Ljava/lang/Runnable;

    .line 1565
    :cond_6d
    iget-object v1, p0, Landroid/support/v4/app/bl;->i:Ljava/util/ArrayList;

    iget-object v4, p0, Landroid/support/v4/app/bl;->j:[Ljava/lang/Runnable;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1566
    iget-object v1, p0, Landroid/support/v4/app/bl;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1567
    iget-object v1, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 22170
    iget-object v1, v1, Landroid/support/v4/app/bh;->d:Landroid/os/Handler;

    .line 1567
    iget-object v4, p0, Landroid/support/v4/app/bl;->F:Ljava/lang/Runnable;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1568
    monitor-exit p0
    :try_end_83
    .catchall {:try_start_5a .. :try_end_83} :catchall_97

    .line 1570
    iput-boolean v0, p0, Landroid/support/v4/app/bl;->k:Z

    move v1, v2

    .line 1571
    :goto_86
    if-ge v1, v3, :cond_9a

    .line 1572
    iget-object v4, p0, Landroid/support/v4/app/bl;->j:[Ljava/lang/Runnable;

    aget-object v4, v4, v1

    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    .line 1573
    iget-object v4, p0, Landroid/support/v4/app/bl;->j:[Ljava/lang/Runnable;

    const/4 v5, 0x0

    aput-object v5, v4, v1

    .line 1571
    add-int/lit8 v1, v1, 0x1

    goto :goto_86

    .line 1568
    :catchall_97
    move-exception v0

    :try_start_98
    monitor-exit p0
    :try_end_99
    .catchall {:try_start_98 .. :try_end_99} :catchall_97

    throw v0

    .line 1575
    :cond_9a
    iput-boolean v2, p0, Landroid/support/v4/app/bl;->k:Z

    move v1, v0

    .line 1577
    goto :goto_25

    .line 1587
    :cond_9e
    if-nez v4, :cond_a5

    .line 1588
    iput-boolean v2, p0, Landroid/support/v4/app/bl;->C:Z

    .line 1589
    invoke-virtual {p0}, Landroid/support/v4/app/bl;->j()V

    .line 1592
    :cond_a5
    return v1
.end method

.method final l()V
    .registers 3

    .prologue
    .line 1596
    iget-object v0, p0, Landroid/support/v4/app/bl;->s:Ljava/util/ArrayList;

    if-eqz v0, :cond_1d

    .line 1597
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    iget-object v0, p0, Landroid/support/v4/app/bl;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1d

    .line 1598
    iget-object v0, p0, Landroid/support/v4/app/bl;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/bk;

    invoke-interface {v0}, Landroid/support/v4/app/bk;->a()V

    .line 1597
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1601
    :cond_1d
    return-void
.end method

.method final m()Landroid/os/Parcelable;
    .registers 12

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1756
    invoke-virtual {p0}, Landroid/support/v4/app/bl;->k()Z

    .line 1758
    sget-boolean v0, Landroid/support/v4/app/bl;->d:Z

    if-eqz v0, :cond_c

    .line 1768
    iput-boolean v1, p0, Landroid/support/v4/app/bl;->z:Z

    .line 1771
    :cond_c
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_18

    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_19

    .line 1866
    :cond_18
    :goto_18
    return-object v3

    .line 1776
    :cond_19
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1777
    new-array v7, v6, [Landroid/support/v4/app/FragmentState;

    move v5, v4

    move v2, v4

    .line 1779
    :goto_23
    if-ge v5, v6, :cond_e9

    .line 1780
    iget-object v0, p0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1781
    if-eqz v0, :cond_1c9

    .line 1782
    iget v2, v0, Landroid/support/v4/app/Fragment;->z:I

    if-gez v2, :cond_56

    .line 1783
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Failure saving state: active "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has cleared index: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, Landroid/support/v4/app/Fragment;->z:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Landroid/support/v4/app/bl;->a(Ljava/lang/RuntimeException;)V

    .line 1790
    :cond_56
    new-instance v2, Landroid/support/v4/app/FragmentState;

    invoke-direct {v2, v0}, Landroid/support/v4/app/FragmentState;-><init>(Landroid/support/v4/app/Fragment;)V

    .line 1791
    aput-object v2, v7, v5

    .line 1793
    iget v8, v0, Landroid/support/v4/app/Fragment;->u:I

    if-lez v8, :cond_e4

    iget-object v8, v2, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    if-nez v8, :cond_e4

    .line 1794
    invoke-direct {p0, v0}, Landroid/support/v4/app/bl;->g(Landroid/support/v4/app/Fragment;)Landroid/os/Bundle;

    move-result-object v8

    iput-object v8, v2, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    .line 1796
    iget-object v8, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    if-eqz v8, :cond_b9

    .line 1797
    iget-object v8, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    iget v8, v8, Landroid/support/v4/app/Fragment;->z:I

    if-gez v8, :cond_98

    .line 1798
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failure saving state: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " has target not in fragment manager: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v8}, Landroid/support/v4/app/bl;->a(Ljava/lang/RuntimeException;)V

    .line 1802
    :cond_98
    iget-object v8, v2, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    if-nez v8, :cond_a3

    .line 1803
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    iput-object v8, v2, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    .line 1805
    :cond_a3
    iget-object v8, v2, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    const-string v9, "android:target_state"

    iget-object v10, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v8, v9, v10}, Landroid/support/v4/app/bl;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 1807
    iget v8, v0, Landroid/support/v4/app/Fragment;->E:I

    if-eqz v8, :cond_b9

    .line 1808
    iget-object v8, v2, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    const-string v9, "android:target_req_state"

    iget v10, v0, Landroid/support/v4/app/Fragment;->E:I

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1818
    :cond_b9
    :goto_b9
    sget-boolean v8, Landroid/support/v4/app/bl;->b:Z

    if-eqz v8, :cond_dd

    const-string v8, "FragmentManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Saved state of "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, ": "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v2, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_dd
    move v0, v1

    .line 1779
    :goto_de
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v0

    goto/16 :goto_23

    .line 1815
    :cond_e4
    iget-object v8, v0, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    iput-object v8, v2, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    goto :goto_b9

    .line 1823
    :cond_e9
    if-nez v2, :cond_f8

    .line 1824
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_18

    const-string v0, "FragmentManager"

    const-string v1, "saveAllState: no fragments!"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_18

    .line 1832
    :cond_f8
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_16e

    .line 1833
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1834
    if-lez v5, :cond_16e

    .line 1835
    new-array v1, v5, [I

    move v2, v4

    .line 1836
    :goto_107
    if-ge v2, v5, :cond_16f

    .line 1837
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    iget v0, v0, Landroid/support/v4/app/Fragment;->z:I

    aput v0, v1, v2

    .line 1838
    aget v0, v1, v2

    if-gez v0, :cond_142

    .line 1839
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Failure saving state: active "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " has cleared index: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v8, v1, v2

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Landroid/support/v4/app/bl;->a(Ljava/lang/RuntimeException;)V

    .line 1843
    :cond_142
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_16a

    const-string v0, "FragmentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "saveAllState: adding fragment #"

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1836
    :cond_16a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_107

    :cond_16e
    move-object v1, v3

    .line 1850
    :cond_16f
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_1bb

    .line 1851
    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1852
    if-lez v5, :cond_1bb

    .line 1853
    new-array v3, v5, [Landroid/support/v4/app/BackStackState;

    move v2, v4

    .line 1854
    :goto_17e
    if-ge v2, v5, :cond_1bb

    .line 1855
    new-instance v4, Landroid/support/v4/app/BackStackState;

    iget-object v0, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ak;

    invoke-direct {v4, v0}, Landroid/support/v4/app/BackStackState;-><init>(Landroid/support/v4/app/ak;)V

    aput-object v4, v3, v2

    .line 1856
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_1b7

    const-string v0, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "saveAllState: adding back stack #"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1854
    :cond_1b7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_17e

    .line 1862
    :cond_1bb
    new-instance v0, Landroid/support/v4/app/FragmentManagerState;

    invoke-direct {v0}, Landroid/support/v4/app/FragmentManagerState;-><init>()V

    .line 1863
    iput-object v7, v0, Landroid/support/v4/app/FragmentManagerState;->a:[Landroid/support/v4/app/FragmentState;

    .line 1864
    iput-object v1, v0, Landroid/support/v4/app/FragmentManagerState;->b:[I

    .line 1865
    iput-object v3, v0, Landroid/support/v4/app/FragmentManagerState;->c:[Landroid/support/v4/app/BackStackState;

    move-object v3, v0

    .line 1866
    goto/16 :goto_18

    :cond_1c9
    move v0, v2

    goto/16 :goto_de
.end method

.method public final n()V
    .registers 2

    .prologue
    .line 1995
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/bl;->z:Z

    .line 1996
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/bl;->c(I)V

    .line 1997
    return-void
.end method

.method public final o()V
    .registers 2

    .prologue
    .line 2000
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/bl;->z:Z

    .line 2001
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/support/v4/app/bl;->c(I)V

    .line 2002
    return-void
.end method

.method public final p()V
    .registers 2

    .prologue
    .line 2005
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/bl;->z:Z

    .line 2006
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/support/v4/app/bl;->c(I)V

    .line 2007
    return-void
.end method

.method public final q()V
    .registers 2

    .prologue
    .line 2010
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/bl;->z:Z

    .line 2011
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Landroid/support/v4/app/bl;->c(I)V

    .line 2012
    return-void
.end method

.method public final r()V
    .registers 2

    .prologue
    .line 2022
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/bl;->z:Z

    .line 2024
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/support/v4/app/bl;->c(I)V

    .line 2025
    return-void
.end method

.method public final s()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 2036
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/bl;->A:Z

    .line 2037
    invoke-virtual {p0}, Landroid/support/v4/app/bl;->k()Z

    .line 2038
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/app/bl;->c(I)V

    .line 2039
    iput-object v1, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 2040
    iput-object v1, p0, Landroid/support/v4/app/bl;->w:Landroid/support/v4/app/bf;

    .line 2041
    iput-object v1, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    .line 2042
    return-void
.end method

.method public final t()V
    .registers 4

    .prologue
    .line 2056
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_28

    .line 2057
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_28

    .line 2058
    iget-object v0, p0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 2059
    if-eqz v0, :cond_24

    .line 26024
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->onLowMemory()V

    .line 26025
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v2, :cond_24

    .line 26026
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->t()V

    .line 2057
    :cond_24
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 2064
    :cond_28
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 679
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 680
    const-string v1, "FragmentManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 681
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 683
    iget-object v1, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    if-eqz v1, :cond_2f

    .line 684
    iget-object v1, p0, Landroid/support/v4/app/bl;->x:Landroid/support/v4/app/Fragment;

    invoke-static {v1, v0}, Landroid/support/v4/n/g;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 688
    :goto_25
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 689
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 686
    :cond_2f
    iget-object v1, p0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    invoke-static {v1, v0}, Landroid/support/v4/n/g;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    goto :goto_25
.end method
