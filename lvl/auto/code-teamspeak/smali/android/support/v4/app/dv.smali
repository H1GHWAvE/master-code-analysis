.class Landroid/support/v4/app/dv;
.super Landroid/support/v4/app/ec;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 704
    invoke-direct {p0}, Landroid/support/v4/app/ec;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dn;)Landroid/app/Notification;
    .registers 29

    .prologue
    .line 707
    new-instance v2, Landroid/support/v4/app/eg;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v4/app/dm;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/support/v4/app/dm;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/support/v4/app/dm;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/support/v4/app/dm;->f:Landroid/widget/RemoteViews;

    move-object/from16 v0, p1

    iget v9, v0, Landroid/support/v4/app/dm;->i:I

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/support/v4/app/dm;->d:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v11, v0, Landroid/support/v4/app/dm;->e:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v12, v0, Landroid/support/v4/app/dm;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget v13, v0, Landroid/support/v4/app/dm;->o:I

    move-object/from16 v0, p1

    iget v14, v0, Landroid/support/v4/app/dm;->p:I

    move-object/from16 v0, p1

    iget-boolean v15, v0, Landroid/support/v4/app/dm;->q:Z

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->k:Z

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->l:Z

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/support/v4/app/dm;->j:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->n:Ljava/lang/CharSequence;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->v:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->C:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->x:Landroid/os/Bundle;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->r:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->s:Z

    move/from16 v24, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->t:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-direct/range {v2 .. v25}, Landroid/support/v4/app/eg;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZZILjava/lang/CharSequence;ZLjava/util/ArrayList;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V

    .line 713
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->u:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Landroid/support/v4/app/dd;->a(Landroid/support/v4/app/db;Ljava/util/ArrayList;)V

    .line 714
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    invoke-static {v2, v3}, Landroid/support/v4/app/dd;->a(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V

    .line 715
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/dn;->a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dc;)Landroid/app/Notification;

    move-result-object v2

    return-object v2
.end method

.method public final a(Landroid/app/Notification;I)Landroid/support/v4/app/df;
    .registers 6

    .prologue
    .line 720
    sget-object v0, Landroid/support/v4/app/df;->e:Landroid/support/v4/app/el;

    sget-object v1, Landroid/support/v4/app/fn;->c:Landroid/support/v4/app/fx;

    .line 1119
    iget-object v2, p1, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    aget-object v2, v2, p2

    invoke-static {v2, v0, v1}, Landroid/support/v4/app/ef;->a(Landroid/app/Notification$Action;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;

    move-result-object v0

    .line 720
    check-cast v0, Landroid/support/v4/app/df;

    return-object v0
.end method

.method public final a([Landroid/support/v4/app/df;)Ljava/util/ArrayList;
    .registers 6

    .prologue
    .line 734
    .line 1174
    if-nez p1, :cond_4

    .line 1175
    const/4 v0, 0x0

    :cond_3
    return-object v0

    .line 1177
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1178
    array-length v2, p1

    const/4 v1, 0x0

    :goto_c
    if-ge v1, v2, :cond_3

    aget-object v3, p1, v1

    .line 1179
    invoke-static {v3}, Landroid/support/v4/app/ef;->a(Landroid/support/v4/app/ek;)Landroid/app/Notification$Action;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1178
    add-int/lit8 v1, v1, 0x1

    goto :goto_c
.end method

.method public final a(Ljava/util/ArrayList;)[Landroid/support/v4/app/df;
    .registers 7

    .prologue
    .line 727
    sget-object v3, Landroid/support/v4/app/df;->e:Landroid/support/v4/app/el;

    sget-object v4, Landroid/support/v4/app/fn;->c:Landroid/support/v4/app/fx;

    .line 1156
    if-nez p1, :cond_c

    .line 1157
    const/4 v0, 0x0

    .line 727
    :goto_7
    check-cast v0, [Landroid/support/v4/app/df;

    check-cast v0, [Landroid/support/v4/app/df;

    return-object v0

    .line 1159
    :cond_c
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-interface {v3, v0}, Landroid/support/v4/app/el;->a(I)[Landroid/support/v4/app/ek;

    move-result-object v2

    .line 1160
    const/4 v0, 0x0

    move v1, v0

    :goto_16
    array-length v0, v2

    if-ge v1, v0, :cond_29

    .line 1161
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Notification$Action;

    .line 1162
    invoke-static {v0, v3, v4}, Landroid/support/v4/app/ef;->a(Landroid/app/Notification$Action;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;

    move-result-object v0

    aput-object v0, v2, v1

    .line 1160
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_16

    :cond_29
    move-object v0, v2

    .line 1164
    goto :goto_7
.end method

.method public final d(Landroid/app/Notification;)Z
    .registers 3

    .prologue
    .line 739
    .line 1185
    iget v0, p1, Landroid/app/Notification;->flags:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    .line 739
    goto :goto_7
.end method

.method public final e(Landroid/app/Notification;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 744
    .line 1189
    invoke-virtual {p1}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v0

    .line 744
    return-object v0
.end method

.method public final f(Landroid/app/Notification;)Z
    .registers 3

    .prologue
    .line 749
    .line 1193
    iget v0, p1, Landroid/app/Notification;->flags:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    .line 749
    goto :goto_7
.end method

.method public final g(Landroid/app/Notification;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 754
    .line 1197
    invoke-virtual {p1}, Landroid/app/Notification;->getSortKey()Ljava/lang/String;

    move-result-object v0

    .line 754
    return-object v0
.end method
