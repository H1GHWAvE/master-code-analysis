.class public final Landroid/support/v4/app/dr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/lang/String;

.field private c:Landroid/support/v4/app/fn;

.field private d:Landroid/app/PendingIntent;

.field private e:Landroid/app/PendingIntent;

.field private f:J


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 3186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/dr;->a:Ljava/util/List;

    .line 3187
    iput-object p1, p0, Landroid/support/v4/app/dr;->b:Ljava/lang/String;

    .line 3188
    return-void
.end method

.method private a()Landroid/support/v4/app/dp;
    .registers 9

    .prologue
    .line 3256
    iget-object v0, p0, Landroid/support/v4/app/dr;->a:Ljava/util/List;

    iget-object v1, p0, Landroid/support/v4/app/dr;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 3257
    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v2, p0, Landroid/support/v4/app/dr;->b:Ljava/lang/String;

    aput-object v2, v5, v0

    .line 3258
    new-instance v0, Landroid/support/v4/app/dp;

    iget-object v2, p0, Landroid/support/v4/app/dr;->c:Landroid/support/v4/app/fn;

    iget-object v3, p0, Landroid/support/v4/app/dr;->e:Landroid/app/PendingIntent;

    iget-object v4, p0, Landroid/support/v4/app/dr;->d:Landroid/app/PendingIntent;

    iget-wide v6, p0, Landroid/support/v4/app/dr;->f:J

    invoke-direct/range {v0 .. v7}, Landroid/support/v4/app/dp;-><init>([Ljava/lang/String;Landroid/support/v4/app/fn;Landroid/app/PendingIntent;Landroid/app/PendingIntent;[Ljava/lang/String;J)V

    return-object v0
.end method

.method private a(J)Landroid/support/v4/app/dr;
    .registers 4

    .prologue
    .line 3246
    iput-wide p1, p0, Landroid/support/v4/app/dr;->f:J

    .line 3247
    return-object p0
.end method

.method private a(Landroid/app/PendingIntent;)Landroid/support/v4/app/dr;
    .registers 2

    .prologue
    .line 3230
    iput-object p1, p0, Landroid/support/v4/app/dr;->d:Landroid/app/PendingIntent;

    .line 3231
    return-object p0
.end method

.method private a(Landroid/app/PendingIntent;Landroid/support/v4/app/fn;)Landroid/support/v4/app/dr;
    .registers 3

    .prologue
    .line 3216
    iput-object p2, p0, Landroid/support/v4/app/dr;->c:Landroid/support/v4/app/fn;

    .line 3217
    iput-object p1, p0, Landroid/support/v4/app/dr;->e:Landroid/app/PendingIntent;

    .line 3219
    return-object p0
.end method

.method private a(Ljava/lang/String;)Landroid/support/v4/app/dr;
    .registers 3

    .prologue
    .line 3199
    iget-object v0, p0, Landroid/support/v4/app/dr;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3200
    return-object p0
.end method
