.class public final Landroid/support/v4/app/dk;
.super Landroid/support/v4/app/ed;
.source "SourceFile"


# instance fields
.field a:Landroid/graphics/Bitmap;

.field b:Landroid/graphics/Bitmap;

.field c:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 1636
    invoke-direct {p0}, Landroid/support/v4/app/ed;-><init>()V

    .line 1637
    return-void
.end method

.method private constructor <init>(Landroid/support/v4/app/dm;)V
    .registers 2

    .prologue
    .line 1639
    invoke-direct {p0}, Landroid/support/v4/app/ed;-><init>()V

    .line 1640
    invoke-virtual {p0, p1}, Landroid/support/v4/app/dk;->a(Landroid/support/v4/app/dm;)V

    .line 1641
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/dk;
    .registers 2

    .prologue
    .line 1665
    iput-object p1, p0, Landroid/support/v4/app/dk;->a:Landroid/graphics/Bitmap;

    .line 1666
    return-object p0
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dk;
    .registers 3

    .prologue
    .line 1648
    invoke-static {p1}, Landroid/support/v4/app/dm;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/dk;->e:Ljava/lang/CharSequence;

    .line 1649
    return-object p0
.end method

.method private b(Landroid/graphics/Bitmap;)Landroid/support/v4/app/dk;
    .registers 3

    .prologue
    .line 1673
    iput-object p1, p0, Landroid/support/v4/app/dk;->b:Landroid/graphics/Bitmap;

    .line 1674
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/dk;->c:Z

    .line 1675
    return-object p0
.end method

.method private b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dk;
    .registers 3

    .prologue
    .line 1656
    invoke-static {p1}, Landroid/support/v4/app/dm;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/dk;->f:Ljava/lang/CharSequence;

    .line 1657
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/dk;->g:Z

    .line 1658
    return-object p0
.end method
