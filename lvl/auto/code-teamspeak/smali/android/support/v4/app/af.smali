.class public final Landroid/support/v4/app/af;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x3

.field private static final d:Landroid/support/v4/app/ai;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 83
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_f

    .line 84
    new-instance v0, Landroid/support/v4/app/ah;

    invoke-direct {v0, v2}, Landroid/support/v4/app/ah;-><init>(B)V

    sput-object v0, Landroid/support/v4/app/af;->d:Landroid/support/v4/app/ai;

    .line 88
    :goto_e
    return-void

    .line 86
    :cond_f
    new-instance v0, Landroid/support/v4/app/ai;

    invoke-direct {v0, v2}, Landroid/support/v4/app/ai;-><init>(B)V

    sput-object v0, Landroid/support/v4/app/af;->d:Landroid/support/v4/app/ai;

    goto :goto_e
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)I
    .registers 5
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 117
    sget-object v0, Landroid/support/v4/app/af;->d:Landroid/support/v4/app/ai;

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/support/v4/app/ai;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I
    .registers 4
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 138
    sget-object v0, Landroid/support/v4/app/af;->d:Landroid/support/v4/app/ai;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v4/app/ai;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 97
    sget-object v0, Landroid/support/v4/app/af;->d:Landroid/support/v4/app/ai;

    invoke-virtual {v0, p0}, Landroid/support/v4/app/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
