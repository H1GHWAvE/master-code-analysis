.class final Landroid/support/v4/app/u;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method private static a(Landroid/app/Activity;[Ljava/lang/String;I)V
    .registers 4

    .prologue
    .line 28
    instance-of v0, p0, Landroid/support/v4/app/v;

    if-eqz v0, :cond_a

    move-object v0, p0

    .line 29
    check-cast v0, Landroid/support/v4/app/v;

    invoke-interface {v0, p2}, Landroid/support/v4/app/v;->a(I)V

    .line 32
    :cond_a
    invoke-virtual {p0, p1, p2}, Landroid/app/Activity;->requestPermissions([Ljava/lang/String;I)V

    .line 33
    return-void
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;)Z
    .registers 3

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Landroid/app/Activity;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
