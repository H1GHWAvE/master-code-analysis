.class final Landroid/support/v4/app/ef;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method static a(Landroid/support/v4/app/ek;)Landroid/app/Notification$Action;
    .registers 6

    .prologue
    .line 133
    new-instance v0, Landroid/app/Notification$Action$Builder;

    invoke-virtual {p0}, Landroid/support/v4/app/ek;->a()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/app/ek;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/ek;->c()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Landroid/app/Notification$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {p0}, Landroid/support/v4/app/ek;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Action$Builder;->addExtras(Landroid/os/Bundle;)Landroid/app/Notification$Action$Builder;

    move-result-object v1

    .line 136
    invoke-virtual {p0}, Landroid/support/v4/app/ek;->e()[Landroid/support/v4/app/fw;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_2f

    .line 138
    invoke-static {v0}, Landroid/support/v4/app/fu;->a([Landroid/support/v4/app/fw;)[Landroid/app/RemoteInput;

    move-result-object v2

    .line 139
    array-length v3, v2

    const/4 v0, 0x0

    :goto_25
    if-ge v0, v3, :cond_2f

    aget-object v4, v2, v0

    .line 140
    invoke-virtual {v1, v4}, Landroid/app/Notification$Action$Builder;->addRemoteInput(Landroid/app/RemoteInput;)Landroid/app/Notification$Action$Builder;

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_25

    .line 143
    :cond_2f
    invoke-virtual {v1}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/app/Notification$Action;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
    .registers 12

    .prologue
    .line 125
    invoke-virtual {p0}, Landroid/app/Notification$Action;->getRemoteInputs()[Landroid/app/RemoteInput;

    move-result-object v8

    .line 1026
    if-nez v8, :cond_17

    .line 1027
    const/4 v5, 0x0

    .line 127
    :goto_7
    iget v1, p0, Landroid/app/Notification$Action;->icon:I

    iget-object v2, p0, Landroid/app/Notification$Action;->title:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/app/Notification$Action;->actionIntent:Landroid/app/PendingIntent;

    invoke-virtual {p0}, Landroid/app/Notification$Action;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/app/el;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fw;)Landroid/support/v4/app/ek;

    move-result-object v0

    return-object v0

    .line 1029
    :cond_17
    array-length v0, v8

    invoke-interface {p2, v0}, Landroid/support/v4/app/fx;->a(I)[Landroid/support/v4/app/fw;

    move-result-object v7

    .line 1030
    const/4 v0, 0x0

    move v6, v0

    :goto_1e
    array-length v0, v8

    if-ge v6, v0, :cond_42

    .line 1031
    aget-object v0, v8, v6

    .line 1032
    invoke-virtual {v0}, Landroid/app/RemoteInput;->getResultKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/RemoteInput;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0}, Landroid/app/RemoteInput;->getChoices()[Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0}, Landroid/app/RemoteInput;->getAllowFreeFormInput()Z

    move-result v4

    invoke-virtual {v0}, Landroid/app/RemoteInput;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    move-object v0, p2

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/app/fx;->a(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)Landroid/support/v4/app/fw;

    move-result-object v0

    aput-object v0, v7, v6

    .line 1030
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1e

    :cond_42
    move-object v5, v7

    .line 1035
    goto :goto_7
.end method

.method private static a(Landroid/app/Notification;ILandroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
    .registers 5

    .prologue
    .line 119
    iget-object v0, p0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    aget-object v0, v0, p1

    invoke-static {v0, p2, p3}, Landroid/support/v4/app/ef;->a(Landroid/app/Notification$Action;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;

    move-result-object v0

    return-object v0
.end method

.method private static a([Landroid/support/v4/app/ek;)Ljava/util/ArrayList;
    .registers 5

    .prologue
    .line 174
    if-nez p0, :cond_4

    .line 175
    const/4 v0, 0x0

    .line 181
    :cond_3
    return-object v0

    .line 177
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 178
    array-length v2, p0

    const/4 v1, 0x0

    :goto_c
    if-ge v1, v2, :cond_3

    aget-object v3, p0, v1

    .line 179
    invoke-static {v3}, Landroid/support/v4/app/ef;->a(Landroid/support/v4/app/ek;)Landroid/app/Notification$Action;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    add-int/lit8 v1, v1, 0x1

    goto :goto_c
.end method

.method public static a(Landroid/app/Notification$Builder;Landroid/support/v4/app/ek;)V
    .registers 7

    .prologue
    .line 102
    new-instance v1, Landroid/app/Notification$Action$Builder;

    invoke-virtual {p1}, Landroid/support/v4/app/ek;->a()I

    move-result v0

    invoke-virtual {p1}, Landroid/support/v4/app/ek;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1}, Landroid/support/v4/app/ek;->c()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Landroid/app/Notification$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 104
    invoke-virtual {p1}, Landroid/support/v4/app/ek;->e()[Landroid/support/v4/app/fw;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 105
    invoke-virtual {p1}, Landroid/support/v4/app/ek;->e()[Landroid/support/v4/app/fw;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/app/fu;->a([Landroid/support/v4/app/fw;)[Landroid/app/RemoteInput;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_21
    if-ge v0, v3, :cond_2b

    aget-object v4, v2, v0

    .line 107
    invoke-virtual {v1, v4}, Landroid/app/Notification$Action$Builder;->addRemoteInput(Landroid/app/RemoteInput;)Landroid/app/Notification$Action$Builder;

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_21

    .line 110
    :cond_2b
    invoke-virtual {p1}, Landroid/support/v4/app/ek;->d()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 111
    invoke-virtual {p1}, Landroid/support/v4/app/ek;->d()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Action$Builder;->addExtras(Landroid/os/Bundle;)Landroid/app/Notification$Action$Builder;

    .line 113
    :cond_38
    invoke-virtual {v1}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    .line 114
    return-void
.end method

.method private static a(Landroid/app/Notification;)Z
    .registers 2

    .prologue
    .line 185
    iget v0, p0, Landroid/app/Notification;->flags:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static a(Ljava/util/ArrayList;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/ek;
    .registers 6

    .prologue
    .line 156
    if-nez p0, :cond_4

    .line 157
    const/4 v0, 0x0

    .line 164
    :goto_3
    return-object v0

    .line 159
    :cond_4
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-interface {p1, v0}, Landroid/support/v4/app/el;->a(I)[Landroid/support/v4/app/ek;

    move-result-object v2

    .line 160
    const/4 v0, 0x0

    move v1, v0

    :goto_e
    array-length v0, v2

    if-ge v1, v0, :cond_21

    .line 161
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Notification$Action;

    .line 162
    invoke-static {v0, p1, p2}, Landroid/support/v4/app/ef;->a(Landroid/app/Notification$Action;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;

    move-result-object v0

    aput-object v0, v2, v1

    .line 160
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_e

    :cond_21
    move-object v0, v2

    .line 164
    goto :goto_3
.end method

.method private static b(Landroid/app/Notification;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 189
    invoke-virtual {p0}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/app/Notification;)Z
    .registers 2

    .prologue
    .line 193
    iget v0, p0, Landroid/app/Notification;->flags:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static d(Landroid/app/Notification;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 197
    invoke-virtual {p0}, Landroid/app/Notification;->getSortKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
