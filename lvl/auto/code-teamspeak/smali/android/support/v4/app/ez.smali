.class final Landroid/support/v4/app/ez;
.super Landroid/support/v4/app/cm;
.source "SourceFile"


# instance fields
.field final synthetic d:Landroid/support/v4/app/ex;


# direct methods
.method private constructor <init>(Landroid/support/v4/app/ex;)V
    .registers 2

    .prologue
    .line 73
    iput-object p1, p0, Landroid/support/v4/app/ez;->d:Landroid/support/v4/app/ex;

    invoke-direct {p0}, Landroid/support/v4/app/cm;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v4/app/ex;B)V
    .registers 3

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/support/v4/app/ez;-><init>(Landroid/support/v4/app/ex;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 99
    iget-object v0, p0, Landroid/support/v4/app/ez;->d:Landroid/support/v4/app/ex;

    invoke-static {}, Landroid/support/v4/app/ez;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1, p1}, Landroid/support/v4/app/ex;->a(Landroid/support/v4/app/ex;ILjava/lang/String;)V

    .line 100
    invoke-static {}, Landroid/support/v4/app/ez;->clearCallingIdentity()J

    move-result-wide v0

    .line 104
    invoke-static {v0, v1}, Landroid/support/v4/app/ez;->restoreCallingIdentity(J)V

    .line 105
    return-void
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;)V
    .registers 6

    .prologue
    .line 88
    iget-object v0, p0, Landroid/support/v4/app/ez;->d:Landroid/support/v4/app/ex;

    invoke-static {}, Landroid/support/v4/app/ez;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1, p1}, Landroid/support/v4/app/ex;->a(Landroid/support/v4/app/ex;ILjava/lang/String;)V

    .line 89
    invoke-static {}, Landroid/support/v4/app/ez;->clearCallingIdentity()J

    move-result-wide v0

    .line 93
    invoke-static {v0, v1}, Landroid/support/v4/app/ez;->restoreCallingIdentity(J)V

    .line 94
    return-void
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;Landroid/app/Notification;)V
    .registers 7

    .prologue
    .line 77
    iget-object v0, p0, Landroid/support/v4/app/ez;->d:Landroid/support/v4/app/ex;

    invoke-static {}, Landroid/support/v4/app/ez;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1, p1}, Landroid/support/v4/app/ex;->a(Landroid/support/v4/app/ex;ILjava/lang/String;)V

    .line 78
    invoke-static {}, Landroid/support/v4/app/ez;->clearCallingIdentity()J

    move-result-wide v0

    .line 82
    invoke-static {v0, v1}, Landroid/support/v4/app/ez;->restoreCallingIdentity(J)V

    .line 83
    return-void
.end method
