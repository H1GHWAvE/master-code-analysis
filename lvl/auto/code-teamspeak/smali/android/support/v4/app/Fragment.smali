.class public Landroid/support/v4/app/Fragment;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ComponentCallbacks;
.implements Landroid/view/View$OnCreateContextMenuListener;


# static fields
.field private static final a:Landroid/support/v4/n/v;

.field static final n:Ljava/lang/Object;

.field static final o:I = 0x0

.field static final p:I = 0x1

.field static final q:I = 0x2

.field static final r:I = 0x3

.field static final s:I = 0x4

.field static final t:I = 0x5


# instance fields
.field A:Ljava/lang/String;

.field public B:Landroid/os/Bundle;

.field C:Landroid/support/v4/app/Fragment;

.field D:I

.field E:I

.field F:Z

.field G:Z

.field H:Z

.field I:Z

.field J:Z

.field K:Z

.field L:I

.field public M:Landroid/support/v4/app/bl;

.field public N:Landroid/support/v4/app/bh;

.field O:Landroid/support/v4/app/bl;

.field P:Landroid/support/v4/app/Fragment;

.field Q:I

.field R:I

.field S:Ljava/lang/String;

.field T:Z

.field U:Z

.field V:Z

.field W:Z

.field X:Z

.field Y:Z

.field Z:Z

.field aa:I

.field ab:Landroid/view/ViewGroup;

.field public ac:Landroid/view/View;

.field ad:Landroid/view/View;

.field ae:Z

.field af:Z

.field ag:Landroid/support/v4/app/ct;

.field ah:Z

.field ai:Z

.field aj:Ljava/lang/Object;

.field ak:Ljava/lang/Object;

.field al:Ljava/lang/Object;

.field am:Ljava/lang/Object;

.field an:Ljava/lang/Object;

.field ao:Ljava/lang/Object;

.field ap:Ljava/lang/Boolean;

.field aq:Ljava/lang/Boolean;

.field ar:Landroid/support/v4/app/gj;

.field as:Landroid/support/v4/app/gj;

.field u:I

.field v:Landroid/view/View;

.field w:I

.field x:Landroid/os/Bundle;

.field y:Landroid/util/SparseArray;

.field z:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 169
    new-instance v0, Landroid/support/v4/n/v;

    invoke-direct {v0}, Landroid/support/v4/n/v;-><init>()V

    sput-object v0, Landroid/support/v4/app/Fragment;->a:Landroid/support/v4/n/v;

    .line 172
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/app/Fragment;->u:I

    .line 197
    iput v2, p0, Landroid/support/v4/app/Fragment;->z:I

    .line 209
    iput v2, p0, Landroid/support/v4/app/Fragment;->D:I

    .line 280
    iput-boolean v3, p0, Landroid/support/v4/app/Fragment;->Y:Z

    .line 302
    iput-boolean v3, p0, Landroid/support/v4/app/Fragment;->af:Z

    .line 308
    iput-object v1, p0, Landroid/support/v4/app/Fragment;->aj:Ljava/lang/Object;

    .line 309
    sget-object v0, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->ak:Ljava/lang/Object;

    .line 310
    iput-object v1, p0, Landroid/support/v4/app/Fragment;->al:Ljava/lang/Object;

    .line 311
    sget-object v0, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->am:Ljava/lang/Object;

    .line 312
    iput-object v1, p0, Landroid/support/v4/app/Fragment;->an:Ljava/lang/Object;

    .line 313
    sget-object v0, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->ao:Ljava/lang/Object;

    .line 317
    iput-object v1, p0, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    .line 318
    iput-object v1, p0, Landroid/support/v4/app/Fragment;->as:Landroid/support/v4/app/gj;

    .line 391
    return-void
.end method

.method private A()Landroid/support/v4/app/Fragment;
    .registers 2

    .prologue
    .line 600
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method private B()I
    .registers 2

    .prologue
    .line 607
    iget v0, p0, Landroid/support/v4/app/Fragment;->E:I

    return v0
.end method

.method private C()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 631
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    invoke-virtual {v0}, Landroid/support/v4/app/bh;->h()Ljava/lang/Object;

    move-result-object v0

    goto :goto_5
.end method

.method private D()Landroid/support/v4/app/bi;
    .registers 2

    .prologue
    .line 688
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    return-object v0
.end method

.method private E()Landroid/support/v4/app/bi;
    .registers 3

    .prologue
    .line 696
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-nez v0, :cond_11

    .line 697
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->w()V

    .line 698
    iget v0, p0, Landroid/support/v4/app/Fragment;->u:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_14

    .line 699
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->q()V

    .line 708
    :cond_11
    :goto_11
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    return-object v0

    .line 700
    :cond_14
    iget v0, p0, Landroid/support/v4/app/Fragment;->u:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_1f

    .line 701
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->p()V

    goto :goto_11

    .line 702
    :cond_1f
    iget v0, p0, Landroid/support/v4/app/Fragment;->u:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_2a

    .line 703
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->o()V

    goto :goto_11

    .line 704
    :cond_2a
    iget v0, p0, Landroid/support/v4/app/Fragment;->u:I

    if-lez v0, :cond_11

    .line 705
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->n()V

    goto :goto_11
.end method

.method private F()Landroid/support/v4/app/Fragment;
    .registers 2

    .prologue
    .line 716
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->P:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method private G()Z
    .registers 2

    .prologue
    .line 732
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->U:Z

    return v0
.end method

.method private H()Z
    .registers 2

    .prologue
    .line 741
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->G:Z

    return v0
.end method

.method private I()Z
    .registers 2

    .prologue
    .line 752
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->J:Z

    return v0
.end method

.method private J()Z
    .registers 2

    .prologue
    .line 760
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->H:Z

    return v0
.end method

.method private K()Z
    .registers 2

    .prologue
    .line 781
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->T:Z

    return v0
.end method

.method private L()Z
    .registers 2

    .prologue
    .line 786
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->X:Z

    return v0
.end method

.method private M()Z
    .registers 2

    .prologue
    .line 791
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->Y:Z

    return v0
.end method

.method private N()Z
    .registers 2

    .prologue
    .line 827
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->V:Z

    return v0
.end method

.method private O()Z
    .registers 2

    .prologue
    .line 890
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->af:Z

    return v0
.end method

.method private P()Landroid/support/v4/app/cr;
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 897
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_8

    .line 898
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    .line 905
    :goto_7
    return-object v0

    .line 900
    :cond_8
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-nez v0, :cond_27

    .line 901
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 903
    :cond_27
    iput-boolean v3, p0, Landroid/support/v4/app/Fragment;->ai:Z

    .line 904
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    iget-boolean v2, p0, Landroid/support/v4/app/Fragment;->ah:Z

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/bh;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    .line 905
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    goto :goto_7
.end method

.method private Q()V
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1140
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1141
    return-void
.end method

.method private R()V
    .registers 3

    .prologue
    .line 1148
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1149
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-nez v0, :cond_11

    const/4 v0, 0x0

    .line 1150
    :goto_8
    if-eqz v0, :cond_10

    .line 1151
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1152
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    .line 1154
    :cond_10
    return-void

    .line 1149
    :cond_11
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    .line 11162
    iget-object v0, v0, Landroid/support/v4/app/bh;->b:Landroid/app/Activity;

    goto :goto_8
.end method

.method private S()Landroid/view/View;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 1237
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    return-object v0
.end method

.method private T()V
    .registers 2

    .prologue
    .line 1269
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1270
    return-void
.end method

.method private U()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1387
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/app/Fragment;->z:I

    .line 1388
    iput-object v2, p0, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    .line 1389
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->F:Z

    .line 1390
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->G:Z

    .line 1391
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->H:Z

    .line 1392
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->I:Z

    .line 1393
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->J:Z

    .line 1394
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->K:Z

    .line 1395
    iput v1, p0, Landroid/support/v4/app/Fragment;->L:I

    .line 1396
    iput-object v2, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 1397
    iput-object v2, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 1398
    iput-object v2, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    .line 1399
    iput v1, p0, Landroid/support/v4/app/Fragment;->Q:I

    .line 1400
    iput v1, p0, Landroid/support/v4/app/Fragment;->R:I

    .line 1401
    iput-object v2, p0, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    .line 1402
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->T:Z

    .line 1403
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->U:Z

    .line 1404
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->W:Z

    .line 1405
    iput-object v2, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    .line 1406
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->ah:Z

    .line 1407
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->ai:Z

    .line 1408
    return-void
.end method

.method private static V()V
    .registers 0

    .prologue
    .line 1449
    return-void
.end method

.method private static W()V
    .registers 0

    .prologue
    .line 1491
    return-void
.end method

.method private static X()Z
    .registers 1

    .prologue
    .line 1558
    const/4 v0, 0x0

    return v0
.end method

.method private Y()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1607
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->aj:Ljava/lang/Object;

    return-object v0
.end method

.method private Z()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1641
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ak:Ljava/lang/Object;

    sget-object v1, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    if-ne v0, v1, :cond_9

    .line 11607
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->aj:Ljava/lang/Object;

    .line 1641
    :goto_8
    return-object v0

    :cond_9
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ak:Ljava/lang/Object;

    goto :goto_8
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .registers 3

    .prologue
    .line 398
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;
    .registers 7
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 417
    :try_start_0
    sget-object v0, Landroid/support/v4/app/Fragment;->a:Landroid/support/v4/n/v;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/v;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 418
    if-nez v0, :cond_17

    .line 420
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 421
    sget-object v1, Landroid/support/v4/app/Fragment;->a:Landroid/support/v4/n/v;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/n/v;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    :cond_17
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 424
    if-eqz p2, :cond_2c

    .line 425
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 426
    iput-object p2, v0, Landroid/support/v4/app/Fragment;->B:Landroid/os/Bundle;
    :try_end_2c
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_2c} :catch_2d
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_2c} :catch_49
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_2c} :catch_65

    .line 428
    :cond_2c
    return-object v0

    .line 429
    :catch_2d
    move-exception v0

    .line 430
    new-instance v1, Landroid/support/v4/app/az;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v4/app/az;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 433
    :catch_49
    move-exception v0

    .line 434
    new-instance v1, Landroid/support/v4/app/az;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v4/app/az;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 437
    :catch_65
    move-exception v0

    .line 438
    new-instance v1, Landroid/support/v4/app/az;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v4/app/az;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method private a(I)Ljava/lang/CharSequence;
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param

    .prologue
    .line 651
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private varargs a(I[Ljava/lang/Object;)Ljava/lang/String;
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param

    .prologue
    .line 674
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .registers 4

    .prologue
    .line 467
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    if-eqz v0, :cond_e

    .line 468
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ad:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 469
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    .line 471
    :cond_e
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 3269
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 473
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_33

    .line 474
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onViewStateRestored()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 477
    :cond_33
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .registers 5

    .prologue
    .line 913
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-nez v0, :cond_1f

    .line 914
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 916
    :cond_1f
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    const/4 v1, -0x1

    invoke-virtual {v0, p0, p1, v1}, Landroid/support/v4/app/bh;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 917
    return-void
.end method

.method private a(Landroid/content/Intent;I)V
    .registers 6

    .prologue
    .line 924
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-nez v0, :cond_1f

    .line 925
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 927
    :cond_1f
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v4/app/bh;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 928
    return-void
.end method

.method private a(Landroid/content/res/Configuration;)V
    .registers 3

    .prologue
    .line 2017
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2018
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_c

    .line 2019
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->a(Landroid/content/res/Configuration;)V

    .line 2021
    :cond_c
    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment$SavedState;)V
    .registers 4

    .prologue
    .line 573
    iget v0, p0, Landroid/support/v4/app/Fragment;->z:I

    if-ltz v0, :cond_c

    .line 574
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 576
    :cond_c
    if-eqz p1, :cond_17

    iget-object v0, p1, Landroid/support/v4/app/Fragment$SavedState;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_17

    iget-object v0, p1, Landroid/support/v4/app/Fragment$SavedState;->a:Landroid/os/Bundle;

    :goto_14
    iput-object v0, p0, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    .line 578
    return-void

    .line 576
    :cond_17
    const/4 v0, 0x0

    goto :goto_14
.end method

.method private a(Landroid/support/v4/app/Fragment;I)V
    .registers 3

    .prologue
    .line 592
    iput-object p1, p0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    .line 593
    iput p2, p0, Landroid/support/v4/app/Fragment;->E:I

    .line 594
    return-void
.end method

.method private a(Landroid/support/v4/app/gj;)V
    .registers 2

    .prologue
    .line 1569
    iput-object p1, p0, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    .line 1570
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 1526
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 1527
    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 1594
    iput-object p1, p0, Landroid/support/v4/app/Fragment;->aj:Ljava/lang/Object;

    .line 1595
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8

    .prologue
    .line 1829
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mFragmentId=#"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1830
    iget v0, p0, Landroid/support/v4/app/Fragment;->Q:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1831
    const-string v0, " mContainerId=#"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1832
    iget v0, p0, Landroid/support/v4/app/Fragment;->R:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1833
    const-string v0, " mTag="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1834
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/Fragment;->u:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 1835
    const-string v0, " mIndex="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/Fragment;->z:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 1836
    const-string v0, " mWho="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1837
    const-string v0, " mBackStackNesting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/Fragment;->L:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1838
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAdded="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->F:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1839
    const-string v0, " mRemoving="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->G:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1840
    const-string v0, " mResumed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->H:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1841
    const-string v0, " mFromLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->I:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1842
    const-string v0, " mInLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->J:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 1843
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHidden="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->T:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1844
    const-string v0, " mDetached="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->U:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1845
    const-string v0, " mMenuVisible="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->Y:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1846
    const-string v0, " mHasMenu="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->X:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 1847
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mRetainInstance="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->V:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1848
    const-string v0, " mRetaining="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->W:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1849
    const-string v0, " mUserVisibleHint="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->af:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 1850
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_e6

    .line 1851
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mFragmentManager="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1852
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1854
    :cond_e6
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-eqz v0, :cond_f7

    .line 1855
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHost="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1856
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1858
    :cond_f7
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->P:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_108

    .line 1859
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mParentFragment="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1860
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->P:Landroid/support/v4/app/Fragment;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1862
    :cond_108
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Landroid/os/Bundle;

    if-eqz v0, :cond_119

    .line 1863
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mArguments="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1865
    :cond_119
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    if-eqz v0, :cond_12a

    .line 1866
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSavedFragmentState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1867
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->x:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1869
    :cond_12a
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    if-eqz v0, :cond_13b

    .line 1870
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSavedViewState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1871
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->y:Landroid/util/SparseArray;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1873
    :cond_13b
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_156

    .line 1874
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTarget="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 1875
    const-string v0, " mTargetRequestCode="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1876
    iget v0, p0, Landroid/support/v4/app/Fragment;->E:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1878
    :cond_156
    iget v0, p0, Landroid/support/v4/app/Fragment;->aa:I

    if-eqz v0, :cond_167

    .line 1879
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mNextAnim="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/Fragment;->aa:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1881
    :cond_167
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ab:Landroid/view/ViewGroup;

    if-eqz v0, :cond_178

    .line 1882
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ab:Landroid/view/ViewGroup;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1884
    :cond_178
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-eqz v0, :cond_189

    .line 1885
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mView="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1887
    :cond_189
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ad:Landroid/view/View;

    if-eqz v0, :cond_19a

    .line 1888
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mInnerView="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1890
    :cond_19a
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->v:Landroid/view/View;

    if-eqz v0, :cond_1b8

    .line 1891
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAnimatingAway="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->v:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1892
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStateAfterAnimating="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1893
    iget v0, p0, Landroid/support/v4/app/Fragment;->w:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1895
    :cond_1b8
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_1dc

    .line 1896
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Loader Manager:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1897
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/app/ct;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1899
    :cond_1dc
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_215

    .line 1900
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Child "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1901
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/app/bl;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1903
    :cond_215
    return-void
.end method

.method private a(Z)V
    .registers 4

    .prologue
    .line 819
    if-eqz p1, :cond_e

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->P:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_e

    .line 820
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t retain fragements that are nested in other fragments"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 823
    :cond_e
    iput-boolean p1, p0, Landroid/support/v4/app/Fragment;->V:Z

    .line 824
    return-void
.end method

.method private a([Ljava/lang/String;I)V
    .registers 6
    .param p1    # [Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 1015
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-nez v0, :cond_1f

    .line 1016
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1018
    :cond_1f
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v4/app/bh;->a(Landroid/support/v4/app/Fragment;[Ljava/lang/String;I)V

    .line 1019
    return-void
.end method

.method private a(Landroid/view/Menu;)Z
    .registers 4

    .prologue
    .line 2054
    const/4 v0, 0x0

    .line 2055
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->T:Z

    if-nez v1, :cond_19

    .line 2056
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v1, :cond_e

    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->Y:Z

    if-eqz v1, :cond_e

    .line 2057
    const/4 v0, 0x1

    .line 2060
    :cond_e
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v1, :cond_19

    .line 2061
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v1, p1}, Landroid/support/v4/app/bl;->a(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 2064
    :cond_19
    return v0
.end method

.method private aa()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1675
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->al:Ljava/lang/Object;

    return-object v0
.end method

.method private ab()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1708
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->am:Ljava/lang/Object;

    sget-object v1, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    if-ne v0, v1, :cond_9

    .line 11675
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->al:Ljava/lang/Object;

    .line 1708
    :goto_8
    return-object v0

    :cond_9
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->am:Ljava/lang/Object;

    goto :goto_8
.end method

.method private ac()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1735
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->an:Ljava/lang/Object;

    return-object v0
.end method

.method private ad()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1767
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ao:Ljava/lang/Object;

    sget-object v1, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    if-ne v0, v1, :cond_9

    .line 11735
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->an:Ljava/lang/Object;

    .line 1767
    :goto_8
    return-object v0

    :cond_9
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ao:Ljava/lang/Object;

    goto :goto_8
.end method

.method private ae()Z
    .registers 2

    .prologue
    .line 1792
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->aq:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->aq:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_5
.end method

.method private af()Z
    .registers 2

    .prologue
    .line 1816
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ap:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ap:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_5
.end method

.method private ag()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 1981
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_e

    .line 1982
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 15991
    iput-boolean v1, v0, Landroid/support/v4/app/bl;->z:Z

    .line 1983
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->k()Z

    .line 1985
    :cond_e
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1986
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->e()V

    .line 1987
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_32

    .line 1988
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStart()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1991
    :cond_32
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_3b

    .line 1992
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->p()V

    .line 1994
    :cond_3b
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_44

    .line 1995
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->f()V

    .line 1997
    :cond_44
    return-void
.end method

.method private ah()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 2000
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_e

    .line 2001
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 16991
    iput-boolean v1, v0, Landroid/support/v4/app/bl;->z:Z

    .line 2002
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->k()Z

    .line 2004
    :cond_e
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 2005
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->s()V

    .line 2006
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_32

    .line 2007
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onResume()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2010
    :cond_32
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_40

    .line 2011
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->q()V

    .line 2012
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->k()Z

    .line 2014
    :cond_40
    return-void
.end method

.method private ai()V
    .registers 2

    .prologue
    .line 2024
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->onLowMemory()V

    .line 2025
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_c

    .line 2026
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->t()V

    .line 2028
    :cond_c
    return-void
.end method

.method private aj()V
    .registers 4

    .prologue
    .line 2119
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_a

    .line 2120
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 17015
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bl;->c(I)V

    .line 2122
    :cond_a
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 2123
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->t()V

    .line 2124
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_2f

    .line 2125
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onPause()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2128
    :cond_2f
    return-void
.end method

.method private ak()V
    .registers 4

    .prologue
    .line 2131
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_9

    .line 2132
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->r()V

    .line 2134
    :cond_9
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 2135
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->f()V

    .line 2136
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_2e

    .line 2137
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStop()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2140
    :cond_2e
    return-void
.end method

.method private al()V
    .registers 4

    .prologue
    .line 2163
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_a

    .line 2164
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 17032
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bl;->c(I)V

    .line 2166
    :cond_a
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 2167
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->g()V

    .line 2168
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_2f

    .line 2169
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroyView()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2172
    :cond_2f
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_38

    .line 2173
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->e()V

    .line 2175
    :cond_38
    return-void
.end method

.method private am()V
    .registers 4

    .prologue
    .line 2178
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_9

    .line 2179
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->s()V

    .line 2181
    :cond_9
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 2182
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->u()V

    .line 2183
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_2e

    .line 2184
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroy()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2187
    :cond_2e
    return-void
.end method

.method private b(I)Ljava/lang/String;
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param

    .prologue
    .line 661
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/support/v4/app/gj;)V
    .registers 2

    .prologue
    .line 1580
    iput-object p1, p0, Landroid/support/v4/app/Fragment;->as:Landroid/support/v4/app/gj;

    .line 1581
    return-void
.end method

.method private b(Landroid/view/Menu;)V
    .registers 3

    .prologue
    .line 2098
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->T:Z

    if-nez v0, :cond_d

    .line 2102
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_d

    .line 2103
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->b(Landroid/view/Menu;)V

    .line 2106
    :cond_d
    return-void
.end method

.method private static b(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 1537
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 1538
    return-void
.end method

.method private b(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 1625
    iput-object p1, p0, Landroid/support/v4/app/Fragment;->ak:Ljava/lang/Object;

    .line 1626
    return-void
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 489
    iget v0, p0, Landroid/support/v4/app/Fragment;->L:I

    if-lez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method static b(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 4

    .prologue
    .line 454
    :try_start_0
    sget-object v0, Landroid/support/v4/app/Fragment;->a:Landroid/support/v4/n/v;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/v;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 455
    if-nez v0, :cond_17

    .line 457
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 458
    sget-object v1, Landroid/support/v4/app/Fragment;->a:Landroid/support/v4/n/v;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/n/v;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    :cond_17
    const-class v1, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
    :try_end_1c
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_1c} :catch_1e

    move-result v0

    .line 462
    :goto_1d
    return v0

    :catch_1e
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1d
.end method

.method private b(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .registers 5

    .prologue
    .line 2040
    const/4 v0, 0x0

    .line 2041
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->T:Z

    if-nez v1, :cond_1c

    .line 2042
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v1, :cond_11

    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->Y:Z

    if-eqz v1, :cond_11

    .line 2043
    const/4 v0, 0x1

    .line 2044
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2046
    :cond_11
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v1, :cond_1c

    .line 2047
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v1, p1, p2}, Landroid/support/v4/app/bl;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 2050
    :cond_1c
    return v0
.end method

.method private b(Landroid/view/MenuItem;)Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 2068
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->T:Z

    if-nez v1, :cond_20

    .line 2069
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v1, :cond_14

    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->Y:Z

    if-eqz v1, :cond_14

    .line 2070
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 2080
    :cond_13
    :goto_13
    return v0

    .line 2074
    :cond_14
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v1, :cond_20

    .line 2075
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v1, p1}, Landroid/support/v4/app/bl;->a(Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 2080
    :cond_20
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private b(Ljava/lang/String;)Z
    .registers 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 1064
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-eqz v0, :cond_b

    .line 1065
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bh;->a(Ljava/lang/String;)Z

    move-result v0

    .line 1067
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private c(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 1659
    iput-object p1, p0, Landroid/support/v4/app/Fragment;->al:Ljava/lang/Object;

    .line 1660
    return-void
.end method

.method private c(Landroid/view/MenuItem;)Z
    .registers 3

    .prologue
    .line 2084
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->T:Z

    if-nez v0, :cond_12

    .line 2088
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_12

    .line 2089
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->b(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2090
    const/4 v0, 0x1

    .line 2094
    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private d()I
    .registers 2

    .prologue
    .line 532
    iget v0, p0, Landroid/support/v4/app/Fragment;->Q:I

    return v0
.end method

.method private d(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 1692
    iput-object p1, p0, Landroid/support/v4/app/Fragment;->am:Ljava/lang/Object;

    .line 1693
    return-void
.end method

.method private d(Z)V
    .registers 3

    .prologue
    .line 1780
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->aq:Ljava/lang/Boolean;

    .line 1781
    return-void
.end method

.method private e(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 1722
    iput-object p1, p0, Landroid/support/v4/app/Fragment;->an:Ljava/lang/Object;

    .line 1723
    return-void
.end method

.method private e(Z)V
    .registers 3

    .prologue
    .line 1804
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->ap:Ljava/lang/Boolean;

    .line 1805
    return-void
.end method

.method private f(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 1751
    iput-object p1, p0, Landroid/support/v4/app/Fragment;->ao:Ljava/lang/Object;

    .line 1752
    return-void
.end method

.method private g(Landroid/os/Bundle;)V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 1935
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_9

    .line 1936
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 12991
    iput-boolean v1, v0, Landroid/support/v4/app/bl;->z:Z

    .line 1938
    :cond_9
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1939
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 1940
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_2d

    .line 1941
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onCreate()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1944
    :cond_2d
    if-eqz p1, :cond_49

    .line 1945
    const-string v0, "android:support:fragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 1947
    if-eqz v0, :cond_49

    .line 1948
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-nez v1, :cond_3e

    .line 1949
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->w()V

    .line 1951
    :cond_3e
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/bl;->a(Landroid/os/Parcelable;Ljava/util/List;)V

    .line 1952
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->n()V

    .line 1955
    :cond_49
    return-void
.end method

.method private h(Landroid/os/Bundle;)V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 1966
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_9

    .line 1967
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 14991
    iput-boolean v1, v0, Landroid/support/v4/app/bl;->z:Z

    .line 1969
    :cond_9
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1970
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    .line 1971
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    if-nez v0, :cond_2d

    .line 1972
    new-instance v0, Landroid/support/v4/app/gk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onActivityCreated()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/gk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1975
    :cond_2d
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_36

    .line 1976
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->o()V

    .line 1978
    :cond_36
    return-void
.end method

.method public static m()V
    .registers 0

    .prologue
    .line 802
    return-void
.end method

.method public static o()V
    .registers 0

    .prologue
    .line 945
    return-void
.end method

.method public static p()V
    .registers 0

    .prologue
    .line 1041
    return-void
.end method

.method public static r()Landroid/view/animation/Animation;
    .registers 1

    .prologue
    .line 1170
    const/4 v0, 0x0

    return-object v0
.end method

.method public static v()V
    .registers 0

    .prologue
    .line 1459
    return-void
.end method

.method private y()Ljava/lang/String;
    .registers 2

    .prologue
    .line 539
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    return-object v0
.end method

.method private z()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 561
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Landroid/os/Bundle;

    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 1906
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 12439
    :cond_9
    :goto_9
    return-object p0

    .line 1909
    :cond_a
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_37

    .line 1910
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 12435
    iget-object v0, v3, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_35

    if-eqz p1, :cond_35

    .line 12436
    iget-object v0, v3, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1f
    if-ltz v2, :cond_35

    .line 12437
    iget-object v0, v3, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 12438
    if-eqz v0, :cond_31

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p0

    if-nez p0, :cond_9

    .line 12436
    :cond_31
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1f

    :cond_35
    move-object p0, v1

    .line 1910
    goto :goto_9

    :cond_37
    move-object p0, v1

    .line 1912
    goto :goto_9
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 4
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 1213
    const/4 v0, 0x0

    return-object v0
.end method

.method final a(ILandroid/support/v4/app/Fragment;)V
    .registers 5

    .prologue
    .line 480
    iput p1, p0, Landroid/support/v4/app/Fragment;->z:I

    .line 481
    if-eqz p2, :cond_22

    .line 482
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p2, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v4/app/Fragment;->z:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    .line 486
    :goto_21
    return-void

    .line 484
    :cond_22
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android:fragment:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/app/Fragment;->z:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    goto :goto_21
.end method

.method public a(Landroid/app/Activity;)V
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1163
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1164
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 1188
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1189
    return-void
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 3

    .prologue
    .line 1432
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 1227
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .registers 3

    .prologue
    .line 1480
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .registers 5

    .prologue
    .line 1076
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    invoke-virtual {v0}, Landroid/support/v4/app/bh;->c()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 8696
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-nez v1, :cond_17

    .line 8697
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->w()V

    .line 8698
    iget v1, p0, Landroid/support/v4/app/Fragment;->u:I

    const/4 v2, 0x5

    if-lt v1, v2, :cond_1d

    .line 8699
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v1}, Landroid/support/v4/app/bl;->q()V

    .line 1078
    :cond_17
    :goto_17
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-static {v0, v1}, Landroid/support/v4/view/ai;->a(Landroid/view/LayoutInflater;Landroid/support/v4/view/as;)V

    .line 1079
    return-object v0

    .line 8700
    :cond_1d
    iget v1, p0, Landroid/support/v4/app/Fragment;->u:I

    const/4 v2, 0x4

    if-lt v1, v2, :cond_28

    .line 8701
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v1}, Landroid/support/v4/app/bl;->p()V

    goto :goto_17

    .line 8702
    :cond_28
    iget v1, p0, Landroid/support/v4/app/Fragment;->u:I

    const/4 v2, 0x2

    if-lt v1, v2, :cond_33

    .line 8703
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v1}, Landroid/support/v4/app/bl;->o()V

    goto :goto_17

    .line 8704
    :cond_33
    iget v1, p0, Landroid/support/v4/app/Fragment;->u:I

    if-lez v1, :cond_17

    .line 8705
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v1}, Landroid/support/v4/app/bl;->n()V

    goto :goto_17
.end method

.method final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5

    .prologue
    .line 1959
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_9

    .line 1960
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 13991
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/app/bl;->z:Z

    .line 1962
    :cond_9
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)V
    .registers 3

    .prologue
    .line 856
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->Y:Z

    if-eq v0, p1, :cond_19

    .line 857
    iput-boolean p1, p0, Landroid/support/v4/app/Fragment;->Y:Z

    .line 858
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->X:Z

    if-eqz v0, :cond_19

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->k()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 7781
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->T:Z

    .line 858
    if-nez v0, :cond_19

    .line 859
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    invoke-virtual {v0}, Landroid/support/v4/app/bh;->d()V

    .line 862
    :cond_19
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 1415
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1416
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 1254
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1255
    return-void
.end method

.method public final c(Z)V
    .registers 4

    .prologue
    .line 878
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->af:Z

    if-nez v0, :cond_10

    if-eqz p1, :cond_10

    iget v0, p0, Landroid/support/v4/app/Fragment;->u:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_10

    .line 879
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p0}, Landroid/support/v4/app/bl;->b(Landroid/support/v4/app/Fragment;)V

    .line 881
    :cond_10
    iput-boolean p1, p0, Landroid/support/v4/app/Fragment;->af:Z

    .line 882
    if-nez p1, :cond_18

    const/4 v0, 0x1

    :goto_15
    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->ae:Z

    .line 883
    return-void

    .line 882
    :cond_18
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public d(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 1322
    return-void
.end method

.method public e()V
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 1278
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1280
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->ah:Z

    if-nez v0, :cond_25

    .line 1281
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->ah:Z

    .line 1282
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->ai:Z

    if-nez v0, :cond_1c

    .line 1283
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->ai:Z

    .line 1284
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    iget-boolean v2, p0, Landroid/support/v4/app/Fragment;->ah:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/bh;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    .line 1286
    :cond_1c
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_25

    .line 1287
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->b()V

    .line 1290
    :cond_25
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 550
    iget v0, p0, Landroid/support/v4/app/Fragment;->z:I

    if-ltz v0, :cond_c

    .line 551
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 553
    :cond_c
    iput-object p1, p0, Landroid/support/v4/app/Fragment;->B:Landroid/os/Bundle;

    .line 554
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 496
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()V
    .registers 2

    .prologue
    .line 1343
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1344
    return-void
.end method

.method final f(Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 2109
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    .line 2110
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_14

    .line 2111
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->m()Landroid/os/Parcelable;

    move-result-object v0

    .line 2112
    if-eqz v0, :cond_14

    .line 2113
    const-string v1, "android:support:fragments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2116
    :cond_14
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 1360
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1361
    return-void
.end method

.method public final h()Landroid/content/Context;
    .registers 2

    .prologue
    .line 614
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    .line 4166
    iget-object v0, v0, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    goto :goto_5
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 503
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()Landroid/support/v4/app/bb;
    .registers 2

    .prologue
    .line 623
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    .line 5162
    iget-object v0, v0, Landroid/support/v4/app/bh;->b:Landroid/app/Activity;

    .line 623
    check-cast v0, Landroid/support/v4/app/bb;

    goto :goto_5
.end method

.method public final j()Landroid/content/res/Resources;
    .registers 4

    .prologue
    .line 638
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-nez v0, :cond_1f

    .line 639
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 641
    :cond_1f
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    .line 5166
    iget-object v0, v0, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    .line 641
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .registers 2

    .prologue
    .line 723
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->F:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final l()Z
    .registers 2

    .prologue
    .line 769
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->k()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 5781
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->T:Z

    .line 769
    if-nez v0, :cond_20

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-eqz v0, :cond_20

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_20

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_20

    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method public final n()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 838
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->X:Z

    if-eq v0, v1, :cond_16

    .line 839
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->X:Z

    .line 840
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->k()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 6781
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->T:Z

    .line 840
    if-nez v0, :cond_16

    .line 841
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    invoke-virtual {v0}, Landroid/support/v4/app/bh;->d()V

    .line 844
    :cond_16
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3

    .prologue
    .line 1325
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1326
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 5

    .prologue
    .line 1512
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/bb;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 1513
    return-void
.end method

.method public onLowMemory()V
    .registers 2

    .prologue
    .line 1347
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1348
    return-void
.end method

.method public final q()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 1125
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1126
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-nez v0, :cond_10

    const/4 v0, 0x0

    .line 1127
    :goto_8
    if-eqz v0, :cond_f

    .line 1128
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 11140
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1131
    :cond_f
    return-void

    .line 1126
    :cond_10
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    .line 10162
    iget-object v0, v0, Landroid/support/v4/app/bh;->b:Landroid/app/Activity;

    goto :goto_8
.end method

.method public s()V
    .registers 2

    .prologue
    .line 1299
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1300
    return-void
.end method

.method public t()V
    .registers 2

    .prologue
    .line 1334
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1335
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 508
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 509
    invoke-static {p0, v0}, Landroid/support/v4/n/g;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 510
    iget v1, p0, Landroid/support/v4/app/Fragment;->z:I

    if-ltz v1, :cond_18

    .line 511
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    iget v1, p0, Landroid/support/v4/app/Fragment;->z:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 514
    :cond_18
    iget v1, p0, Landroid/support/v4/app/Fragment;->Q:I

    if-eqz v1, :cond_2a

    .line 515
    const-string v1, " id=0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    iget v1, p0, Landroid/support/v4/app/Fragment;->Q:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 518
    :cond_2a
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    if-eqz v1, :cond_38

    .line 519
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 522
    :cond_38
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 523
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()V
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 1368
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->Z:Z

    .line 1371
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->ai:Z

    if-nez v0, :cond_16

    .line 1372
    iput-boolean v1, p0, Landroid/support/v4/app/Fragment;->ai:Z

    .line 1373
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    iget-boolean v2, p0, Landroid/support/v4/app/Fragment;->ah:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/bh;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    .line 1375
    :cond_16
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_1f

    .line 1376
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->g()V

    .line 1378
    :cond_1f
    return-void
.end method

.method final w()V
    .registers 4

    .prologue
    .line 1916
    new-instance v0, Landroid/support/v4/app/bl;

    invoke-direct {v0}, Landroid/support/v4/app/bl;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 1917
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    new-instance v2, Landroid/support/v4/app/ay;

    invoke-direct {v2, p0}, Landroid/support/v4/app/ay;-><init>(Landroid/support/v4/app/Fragment;)V

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/bh;Landroid/support/v4/app/bf;Landroid/support/v4/app/Fragment;)V

    .line 1932
    return-void
.end method

.method final x()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 2143
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    if-eqz v0, :cond_b

    .line 2144
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->O:Landroid/support/v4/app/bl;

    .line 17028
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bl;->c(I)V

    .line 2146
    :cond_b
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->ah:Z

    if-eqz v0, :cond_31

    .line 2147
    iput-boolean v3, p0, Landroid/support/v4/app/Fragment;->ah:Z

    .line 2148
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->ai:Z

    if-nez v0, :cond_24

    .line 2149
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/Fragment;->ai:Z

    .line 2150
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->A:Ljava/lang/String;

    iget-boolean v2, p0, Landroid/support/v4/app/Fragment;->ah:Z

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/bh;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    .line 2152
    :cond_24
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_31

    .line 2153
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->W:Z

    if-nez v0, :cond_32

    .line 2154
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->c()V

    .line 2160
    :cond_31
    :goto_31
    return-void

    .line 2156
    :cond_32
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->d()V

    goto :goto_31
.end method
