.class Landroid/support/v4/app/br;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private a:Z

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/animation/Animation;)V
    .registers 4

    .prologue
    .line 419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/br;->a:Z

    .line 420
    if-eqz p1, :cond_a

    if-nez p2, :cond_b

    .line 424
    :cond_a
    :goto_a
    return-void

    .line 423
    :cond_b
    iput-object p1, p0, Landroid/support/v4/app/br;->b:Landroid/view/View;

    goto :goto_a
.end method

.method static synthetic a(Landroid/support/v4/app/br;)Landroid/view/View;
    .registers 2

    .prologue
    .line 416
    iget-object v0, p0, Landroid/support/v4/app/br;->b:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .registers 4
    .annotation build Landroid/support/a/h;
    .end annotation

    .prologue
    .line 443
    iget-boolean v0, p0, Landroid/support/v4/app/br;->a:Z

    if-eqz v0, :cond_e

    .line 444
    iget-object v0, p0, Landroid/support/v4/app/br;->b:Landroid/view/View;

    new-instance v1, Landroid/support/v4/app/bt;

    invoke-direct {v1, p0}, Landroid/support/v4/app/bt;-><init>(Landroid/support/v4/app/br;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 451
    :cond_e
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .registers 2

    .prologue
    .line 455
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .registers 4
    .annotation build Landroid/support/a/h;
    .end annotation

    .prologue
    .line 429
    iget-object v0, p0, Landroid/support/v4/app/br;->b:Landroid/view/View;

    invoke-static {v0, p1}, Landroid/support/v4/app/bl;->a(Landroid/view/View;Landroid/view/animation/Animation;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v4/app/br;->a:Z

    .line 430
    iget-boolean v0, p0, Landroid/support/v4/app/br;->a:Z

    if-eqz v0, :cond_16

    .line 431
    iget-object v0, p0, Landroid/support/v4/app/br;->b:Landroid/view/View;

    new-instance v1, Landroid/support/v4/app/bs;

    invoke-direct {v1, p0}, Landroid/support/v4/app/bs;-><init>(Landroid/support/v4/app/br;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 438
    :cond_16
    return-void
.end method
