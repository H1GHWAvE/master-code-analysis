.class final Landroid/support/design/widget/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .registers 10

    .prologue
    const-wide/16 v6, 0xfa

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 159
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_ec

    move v0, v3

    .line 167
    :goto_a
    return v0

    .line 161
    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/support/design/widget/Snackbar;

    .line 2423
    iget-object v1, v0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_4d

    .line 2424
    iget-object v1, v0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2426
    instance-of v4, v1, Landroid/support/design/widget/w;

    if-eqz v4, :cond_46

    .line 2429
    new-instance v4, Landroid/support/design/widget/bc;

    invoke-direct {v4, v0}, Landroid/support/design/widget/bc;-><init>(Landroid/support/design/widget/Snackbar;)V

    .line 3143
    const v5, 0x3dcccccd    # 0.1f

    invoke-static {v5}, Landroid/support/design/widget/SwipeDismissBehavior;->a(F)F

    move-result v5

    iput v5, v4, Landroid/support/design/widget/SwipeDismissBehavior;->k:F

    .line 3152
    const v5, 0x3f19999a    # 0.6f

    invoke-static {v5}, Landroid/support/design/widget/SwipeDismissBehavior;->a(F)F

    move-result v5

    iput v5, v4, Landroid/support/design/widget/SwipeDismissBehavior;->l:F

    .line 4125
    iput v3, v4, Landroid/support/design/widget/SwipeDismissBehavior;->j:I

    .line 2433
    new-instance v3, Landroid/support/design/widget/aw;

    invoke-direct {v3, v0}, Landroid/support/design/widget/aw;-><init>(Landroid/support/design/widget/Snackbar;)V

    .line 5115
    iput-object v3, v4, Landroid/support/design/widget/SwipeDismissBehavior;->i:Landroid/support/design/widget/bp;

    .line 2454
    check-cast v1, Landroid/support/design/widget/w;

    invoke-virtual {v1, v4}, Landroid/support/design/widget/w;->a(Landroid/support/design/widget/t;)V

    .line 2457
    :cond_46
    iget-object v1, v0, Landroid/support/design/widget/Snackbar;->d:Landroid/view/ViewGroup;

    iget-object v3, v0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2460
    :cond_4d
    iget-object v1, v0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-static {v1}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_5a

    .line 2462
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->a()V

    :goto_58
    move v0, v2

    .line 162
    goto :goto_a

    .line 2465
    :cond_5a
    iget-object v1, v0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    new-instance v3, Landroid/support/design/widget/ax;

    invoke-direct {v3, v0}, Landroid/support/design/widget/ax;-><init>(Landroid/support/design/widget/Snackbar;)V

    invoke-virtual {v1, v3}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->setOnLayoutChangeListener(Landroid/support/design/widget/bg;)V

    goto :goto_58

    .line 164
    :pswitch_65
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/support/design/widget/Snackbar;

    iget v4, p1, Landroid/os/Message;->arg1:I

    .line 5556
    iget-object v1, v0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_94

    .line 5578
    iget-object v1, v0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 5580
    instance-of v5, v1, Landroid/support/design/widget/w;

    if-eqz v5, :cond_92

    .line 5581
    check-cast v1, Landroid/support/design/widget/w;

    .line 6281
    iget-object v1, v1, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 5584
    instance-of v5, v1, Landroid/support/design/widget/SwipeDismissBehavior;

    if-eqz v5, :cond_92

    .line 5585
    check-cast v1, Landroid/support/design/widget/SwipeDismissBehavior;

    .line 6369
    iget-object v5, v1, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    if-eqz v5, :cond_9a

    iget-object v1, v1, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    .line 6421
    iget v1, v1, Landroid/support/v4/widget/eg;->m:I

    .line 5585
    :goto_8f
    if-eqz v1, :cond_92

    move v3, v2

    .line 5556
    :cond_92
    if-eqz v3, :cond_9c

    .line 5557
    :cond_94
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->b()V

    :goto_97
    move v0, v2

    .line 165
    goto/16 :goto_a

    :cond_9a
    move v1, v3

    .line 6369
    goto :goto_8f

    .line 6520
    :cond_9c
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v1, v3, :cond_ca

    .line 6521
    iget-object v1, v0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-static {v1}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v1

    iget-object v3, v0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v3}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v1

    sget-object v3, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v3}, Landroid/support/v4/view/fk;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    move-result-object v1

    new-instance v3, Landroid/support/design/widget/ba;

    invoke-direct {v3, v0, v4}, Landroid/support/design/widget/ba;-><init>(Landroid/support/design/widget/Snackbar;I)V

    invoke-virtual {v1, v3}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->b()V

    goto :goto_97

    .line 6536
    :cond_ca
    iget-object v1, v0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v3, Landroid/support/design/c;->design_snackbar_out:I

    invoke-static {v1, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 6537
    sget-object v3, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 6538
    invoke-virtual {v1, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 6539
    new-instance v3, Landroid/support/design/widget/bb;

    invoke-direct {v3, v0, v4}, Landroid/support/design/widget/bb;-><init>(Landroid/support/design/widget/Snackbar;I)V

    invoke-virtual {v1, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 6551
    iget-object v0, v0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_97

    .line 159
    :pswitch_data_ec
    .packed-switch 0x0
        :pswitch_b
        :pswitch_65
    .end packed-switch
.end method
