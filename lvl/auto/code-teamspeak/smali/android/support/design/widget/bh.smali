.class final Landroid/support/design/widget/bh;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:I = 0x0

.field private static final f:I = 0x5dc

.field private static final g:I = 0xabe

.field private static h:Landroid/support/design/widget/bh;


# instance fields
.field final a:Ljava/lang/Object;

.field final b:Landroid/os/Handler;

.field c:Landroid/support/design/widget/bk;

.field d:Landroid/support/design/widget/bk;


# direct methods
.method private constructor <init>()V
    .registers 4

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/bh;->a:Ljava/lang/Object;

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Landroid/support/design/widget/bi;

    invoke-direct {v2, p0}, Landroid/support/design/widget/bi;-><init>(Landroid/support/design/widget/bh;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Landroid/support/design/widget/bh;->b:Landroid/os/Handler;

    .line 63
    return-void
.end method

.method static a()Landroid/support/design/widget/bh;
    .registers 1

    .prologue
    .line 38
    sget-object v0, Landroid/support/design/widget/bh;->h:Landroid/support/design/widget/bh;

    if-nez v0, :cond_b

    .line 39
    new-instance v0, Landroid/support/design/widget/bh;

    invoke-direct {v0}, Landroid/support/design/widget/bh;-><init>()V

    sput-object v0, Landroid/support/design/widget/bh;->h:Landroid/support/design/widget/bh;

    .line 41
    :cond_b
    sget-object v0, Landroid/support/design/widget/bh;->h:Landroid/support/design/widget/bh;

    return-object v0
.end method

.method private a(ILandroid/support/design/widget/bj;)V
    .registers 6

    .prologue
    .line 71
    iget-object v1, p0, Landroid/support/design/widget/bh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_3
    invoke-virtual {p0, p2}, Landroid/support/design/widget/bh;->d(Landroid/support/design/widget/bj;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 74
    iget-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    .line 1156
    iput p1, v0, Landroid/support/design/widget/bk;->b:I

    .line 78
    iget-object v0, p0, Landroid/support/design/widget/bh;->b:Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 79
    iget-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    invoke-virtual {p0, v0}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bk;)V

    .line 80
    monitor-exit v1

    .line 99
    :goto_1a
    return-void

    .line 81
    :cond_1b
    invoke-virtual {p0, p2}, Landroid/support/design/widget/bh;->e(Landroid/support/design/widget/bj;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 83
    iget-object v0, p0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    .line 2156
    iput p1, v0, Landroid/support/design/widget/bk;->b:I

    .line 89
    :goto_25
    iget-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    if-eqz v0, :cond_3f

    iget-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bk;I)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 92
    monitor-exit v1

    goto :goto_1a

    .line 99
    :catchall_34
    move-exception v0

    monitor-exit v1
    :try_end_36
    .catchall {:try_start_3 .. :try_end_36} :catchall_34

    throw v0

    .line 86
    :cond_37
    :try_start_37
    new-instance v0, Landroid/support/design/widget/bk;

    invoke-direct {v0, p1, p2}, Landroid/support/design/widget/bk;-><init>(ILandroid/support/design/widget/bj;)V

    iput-object v0, p0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    goto :goto_25

    .line 95
    :cond_3f
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    .line 97
    invoke-virtual {p0}, Landroid/support/design/widget/bh;->b()V

    .line 99
    monitor-exit v1
    :try_end_46
    .catchall {:try_start_37 .. :try_end_46} :catchall_34

    goto :goto_1a
.end method

.method private static synthetic a(Landroid/support/design/widget/bh;Landroid/support/design/widget/bk;)V
    .registers 4

    .prologue
    .line 28
    .line 8219
    iget-object v1, p0, Landroid/support/design/widget/bh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 8220
    :try_start_3
    iget-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    if-eq v0, p1, :cond_b

    iget-object v0, p0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    if-ne v0, p1, :cond_f

    .line 8221
    :cond_b
    const/4 v0, 0x2

    invoke-static {p1, v0}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bk;I)Z

    .line 8223
    :cond_f
    monitor-exit v1

    return-void

    :catchall_11
    move-exception v0

    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    throw v0
.end method

.method private a(Landroid/support/design/widget/bj;I)V
    .registers 5

    .prologue
    .line 103
    iget-object v1, p0, Landroid/support/design/widget/bh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 104
    :try_start_3
    invoke-virtual {p0, p1}, Landroid/support/design/widget/bh;->d(Landroid/support/design/widget/bj;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 105
    iget-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    invoke-static {v0, p2}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bk;I)Z

    .line 109
    :cond_e
    :goto_e
    monitor-exit v1

    return-void

    .line 106
    :cond_10
    invoke-virtual {p0, p1}, Landroid/support/design/widget/bh;->e(Landroid/support/design/widget/bj;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 107
    iget-object v0, p0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    invoke-static {v0, p2}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bk;I)Z

    goto :goto_e

    .line 109
    :catchall_1c
    move-exception v0

    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    throw v0
.end method

.method static a(Landroid/support/design/widget/bk;I)Z
    .registers 3

    .prologue
    .line 186
    .line 4156
    iget-object v0, p0, Landroid/support/design/widget/bk;->a:Ljava/lang/ref/WeakReference;

    .line 186
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bj;

    .line 187
    if-eqz v0, :cond_f

    .line 188
    invoke-interface {v0, p1}, Landroid/support/design/widget/bj;->a(I)V

    .line 189
    const/4 v0, 0x1

    .line 191
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method private b(Landroid/support/design/widget/bk;)V
    .registers 4

    .prologue
    .line 219
    iget-object v1, p0, Landroid/support/design/widget/bh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 220
    :try_start_3
    iget-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    if-eq v0, p1, :cond_b

    iget-object v0, p0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    if-ne v0, p1, :cond_f

    .line 221
    :cond_b
    const/4 v0, 0x2

    invoke-static {p1, v0}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bk;I)Z

    .line 223
    :cond_f
    monitor-exit v1

    return-void

    :catchall_11
    move-exception v0

    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    throw v0
.end method

.method private f(Landroid/support/design/widget/bj;)V
    .registers 4

    .prologue
    .line 117
    iget-object v1, p0, Landroid/support/design/widget/bh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 118
    :try_start_3
    invoke-virtual {p0, p1}, Landroid/support/design/widget/bh;->d(Landroid/support/design/widget/bj;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    .line 121
    iget-object v0, p0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    if-eqz v0, :cond_13

    .line 122
    invoke-virtual {p0}, Landroid/support/design/widget/bh;->b()V

    .line 125
    :cond_13
    monitor-exit v1

    return-void

    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method


# virtual methods
.method public final a(Landroid/support/design/widget/bj;)V
    .registers 4

    .prologue
    .line 133
    iget-object v1, p0, Landroid/support/design/widget/bh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 134
    :try_start_3
    invoke-virtual {p0, p1}, Landroid/support/design/widget/bh;->d(Landroid/support/design/widget/bj;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 135
    iget-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    invoke-virtual {p0, v0}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bk;)V

    .line 137
    :cond_e
    monitor-exit v1

    return-void

    :catchall_10
    move-exception v0

    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    throw v0
.end method

.method final a(Landroid/support/design/widget/bk;)V
    .registers 8

    .prologue
    .line 203
    .line 5156
    iget v0, p1, Landroid/support/design/widget/bk;->b:I

    .line 203
    const/4 v1, -0x2

    if-ne v0, v1, :cond_6

    .line 216
    :goto_5
    return-void

    .line 208
    :cond_6
    const/16 v0, 0xabe

    .line 6156
    iget v1, p1, Landroid/support/design/widget/bk;->b:I

    .line 209
    if-lez v1, :cond_21

    .line 7156
    iget v0, p1, Landroid/support/design/widget/bk;->b:I

    .line 214
    :cond_e
    :goto_e
    iget-object v1, p0, Landroid/support/design/widget/bh;->b:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 215
    iget-object v1, p0, Landroid/support/design/widget/bh;->b:Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/design/widget/bh;->b:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-static {v2, v3, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_5

    .line 8156
    :cond_21
    iget v1, p1, Landroid/support/design/widget/bk;->b:I

    .line 211
    const/4 v2, -0x1

    if-ne v1, v2, :cond_e

    .line 212
    const/16 v0, 0x5dc

    goto :goto_e
.end method

.method final b()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 171
    iget-object v0, p0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    if-eqz v0, :cond_1a

    .line 172
    iget-object v0, p0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    iput-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    .line 173
    iput-object v1, p0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    .line 175
    iget-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    .line 3156
    iget-object v0, v0, Landroid/support/design/widget/bk;->a:Ljava/lang/ref/WeakReference;

    .line 175
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bj;

    .line 176
    if-eqz v0, :cond_1b

    .line 177
    invoke-interface {v0}, Landroid/support/design/widget/bj;->a()V

    .line 183
    :cond_1a
    :goto_1a
    return-void

    .line 180
    :cond_1b
    iput-object v1, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    goto :goto_1a
.end method

.method public final b(Landroid/support/design/widget/bj;)V
    .registers 5

    .prologue
    .line 141
    iget-object v1, p0, Landroid/support/design/widget/bh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 142
    :try_start_3
    invoke-virtual {p0, p1}, Landroid/support/design/widget/bh;->d(Landroid/support/design/widget/bj;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 143
    iget-object v0, p0, Landroid/support/design/widget/bh;->b:Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 145
    :cond_10
    monitor-exit v1

    return-void

    :catchall_12
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    throw v0
.end method

.method public final c(Landroid/support/design/widget/bj;)V
    .registers 4

    .prologue
    .line 149
    iget-object v1, p0, Landroid/support/design/widget/bh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 150
    :try_start_3
    invoke-virtual {p0, p1}, Landroid/support/design/widget/bh;->d(Landroid/support/design/widget/bj;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 151
    iget-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    invoke-virtual {p0, v0}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bk;)V

    .line 153
    :cond_e
    monitor-exit v1

    return-void

    :catchall_10
    move-exception v0

    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    throw v0
.end method

.method final d(Landroid/support/design/widget/bj;)Z
    .registers 3

    .prologue
    .line 195
    iget-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/bk;->a(Landroid/support/design/widget/bj;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method final e(Landroid/support/design/widget/bj;)Z
    .registers 3

    .prologue
    .line 199
    iget-object v0, p0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/bk;->a(Landroid/support/design/widget/bj;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method
