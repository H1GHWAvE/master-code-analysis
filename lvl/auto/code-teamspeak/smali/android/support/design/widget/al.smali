.class abstract Landroid/support/design/widget/al;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final b:I = 0xc8

.field static final c:[I

.field static final d:[I

.field static final e:[I


# instance fields
.field final f:Landroid/view/View;

.field final g:Landroid/support/design/widget/as;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x2

    .line 30
    new-array v0, v1, [I

    fill-array-data v0, :array_16

    sput-object v0, Landroid/support/design/widget/al;->c:[I

    .line 32
    new-array v0, v1, [I

    fill-array-data v0, :array_1e

    sput-object v0, Landroid/support/design/widget/al;->d:[I

    .line 34
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Landroid/support/design/widget/al;->e:[I

    return-void

    .line 30
    nop

    :array_16
    .array-data 4
        0x10100a7
        0x101009e
    .end array-data

    .line 32
    :array_1e
    .array-data 4
        0x101009c
        0x101009e
    .end array-data
.end method

.method constructor <init>(Landroid/view/View;Landroid/support/design/widget/as;)V
    .registers 3

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Landroid/support/design/widget/al;->f:Landroid/view/View;

    .line 41
    iput-object p2, p0, Landroid/support/design/widget/al;->g:Landroid/support/design/widget/as;

    .line 42
    return-void
.end method


# virtual methods
.method final a(ILandroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;
    .registers 10

    .prologue
    const/4 v6, 0x1

    .line 66
    iget-object v0, p0, Landroid/support/design/widget/al;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 67
    invoke-virtual {p0}, Landroid/support/design/widget/al;->d()Landroid/support/design/widget/j;

    move-result-object v1

    .line 68
    sget v2, Landroid/support/design/f;->design_fab_stroke_top_outer_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget v3, Landroid/support/design/f;->design_fab_stroke_top_inner_color:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sget v4, Landroid/support/design/f;->design_fab_stroke_end_inner_color:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    sget v5, Landroid/support/design/f;->design_fab_stroke_end_outer_color:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1064
    iput v2, v1, Landroid/support/design/widget/j;->e:I

    .line 1065
    iput v3, v1, Landroid/support/design/widget/j;->f:I

    .line 1066
    iput v4, v1, Landroid/support/design/widget/j;->g:I

    .line 1067
    iput v0, v1, Landroid/support/design/widget/j;->h:I

    .line 73
    int-to-float v0, p1

    .line 1074
    iget v2, v1, Landroid/support/design/widget/j;->d:F

    cmpl-float v2, v2, v0

    if-eqz v2, :cond_42

    .line 1075
    iput v0, v1, Landroid/support/design/widget/j;->d:F

    .line 1076
    iget-object v2, v1, Landroid/support/design/widget/j;->a:Landroid/graphics/Paint;

    const v3, 0x3faaa993    # 1.3333f

    mul-float/2addr v0, v3

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1077
    iput-boolean v6, v1, Landroid/support/design/widget/j;->j:Z

    .line 1078
    invoke-virtual {v1}, Landroid/support/design/widget/j;->invalidateSelf()V

    .line 74
    :cond_42
    invoke-virtual {p2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    .line 1119
    iput v0, v1, Landroid/support/design/widget/j;->i:I

    .line 1120
    iput-boolean v6, v1, Landroid/support/design/widget/j;->j:Z

    .line 1121
    invoke-virtual {v1}, Landroid/support/design/widget/j;->invalidateSelf()V

    .line 75
    return-object v1
.end method

.method abstract a()V
.end method

.method abstract a(F)V
.end method

.method abstract a(I)V
.end method

.method abstract a(Landroid/content/res/ColorStateList;)V
.end method

.method abstract a(Landroid/graphics/PorterDuff$Mode;)V
.end method

.method abstract a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;II)V
.end method

.method abstract a([I)V
.end method

.method abstract b()V
.end method

.method abstract b(F)V
.end method

.method abstract c()V
.end method

.method d()Landroid/support/design/widget/j;
    .registers 2

    .prologue
    .line 79
    new-instance v0, Landroid/support/design/widget/j;

    invoke-direct {v0}, Landroid/support/design/widget/j;-><init>()V

    return-object v0
.end method
