.class final Landroid/support/design/widget/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/support/design/widget/AppBarLayout$Behavior;

.field private final b:Landroid/support/design/widget/CoordinatorLayout;

.field private final c:Landroid/support/design/widget/AppBarLayout;


# direct methods
.method constructor <init>(Landroid/support/design/widget/AppBarLayout$Behavior;Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V
    .registers 4

    .prologue
    .line 922
    iput-object p1, p0, Landroid/support/design/widget/e;->a:Landroid/support/design/widget/AppBarLayout$Behavior;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 923
    iput-object p2, p0, Landroid/support/design/widget/e;->b:Landroid/support/design/widget/CoordinatorLayout;

    .line 924
    iput-object p3, p0, Landroid/support/design/widget/e;->c:Landroid/support/design/widget/AppBarLayout;

    .line 925
    return-void
.end method


# virtual methods
.method public final run()V
    .registers 5

    .prologue
    .line 929
    iget-object v0, p0, Landroid/support/design/widget/e;->c:Landroid/support/design/widget/AppBarLayout;

    if-eqz v0, :cond_30

    iget-object v0, p0, Landroid/support/design/widget/e;->a:Landroid/support/design/widget/AppBarLayout$Behavior;

    invoke-static {v0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/AppBarLayout$Behavior;)Landroid/support/v4/widget/ca;

    move-result-object v0

    if-eqz v0, :cond_30

    iget-object v0, p0, Landroid/support/design/widget/e;->a:Landroid/support/design/widget/AppBarLayout$Behavior;

    invoke-static {v0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/AppBarLayout$Behavior;)Landroid/support/v4/widget/ca;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/ca;->f()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 930
    iget-object v0, p0, Landroid/support/design/widget/e;->a:Landroid/support/design/widget/AppBarLayout$Behavior;

    iget-object v1, p0, Landroid/support/design/widget/e;->b:Landroid/support/design/widget/CoordinatorLayout;

    iget-object v2, p0, Landroid/support/design/widget/e;->c:Landroid/support/design/widget/AppBarLayout;

    iget-object v3, p0, Landroid/support/design/widget/e;->a:Landroid/support/design/widget/AppBarLayout$Behavior;

    invoke-static {v3}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/AppBarLayout$Behavior;)Landroid/support/v4/widget/ca;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/widget/ca;->c()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)I

    .line 933
    iget-object v0, p0, Landroid/support/design/widget/e;->c:Landroid/support/design/widget/AppBarLayout;

    invoke-static {v0, p0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 935
    :cond_30
    return-void
.end method
