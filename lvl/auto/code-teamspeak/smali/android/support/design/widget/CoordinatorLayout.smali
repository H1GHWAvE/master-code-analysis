.class public Landroid/support/design/widget/CoordinatorLayout;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/bv;


# static fields
.field static final a:Ljava/lang/String; = "CoordinatorLayout"

.field static final b:Ljava/lang/String;

.field static final c:[Ljava/lang/Class;

.field static final d:Ljava/lang/ThreadLocal;

.field static final f:Ljava/util/Comparator;

.field static final g:Landroid/support/design/widget/aa;

.field private static final k:I = 0x0

.field private static final l:I = 0x1


# instance fields
.field private A:Landroid/graphics/drawable/Drawable;

.field private B:Landroid/view/ViewGroup$OnHierarchyChangeListener;

.field private final C:Landroid/support/v4/view/bw;

.field final e:Ljava/util/Comparator;

.field final h:Ljava/util/List;

.field final i:Landroid/graphics/Rect;

.field final j:Landroid/graphics/Rect;

.field private final m:Ljava/util/List;

.field private final n:Ljava/util/List;

.field private final o:Landroid/graphics/Rect;

.field private final p:[I

.field private q:Landroid/graphics/Paint;

.field private r:Z

.field private s:[I

.field private t:Landroid/view/View;

.field private u:Landroid/view/View;

.field private v:Landroid/view/View;

.field private w:Landroid/support/design/widget/x;

.field private x:Z

.field private y:Landroid/support/v4/view/gh;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 88
    const-class v0, Landroid/support/design/widget/CoordinatorLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/design/widget/CoordinatorLayout;->b:Ljava/lang/String;

    .line 94
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_38

    .line 95
    new-instance v0, Landroid/support/design/widget/z;

    invoke-direct {v0}, Landroid/support/design/widget/z;-><init>()V

    sput-object v0, Landroid/support/design/widget/CoordinatorLayout;->f:Ljava/util/Comparator;

    .line 96
    new-instance v0, Landroid/support/design/widget/ab;

    invoke-direct {v0}, Landroid/support/design/widget/ab;-><init>()V

    sput-object v0, Landroid/support/design/widget/CoordinatorLayout;->g:Landroid/support/design/widget/aa;

    .line 103
    :goto_21
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Landroid/util/AttributeSet;

    aput-object v2, v0, v1

    sput-object v0, Landroid/support/design/widget/CoordinatorLayout;->c:[Ljava/lang/Class;

    .line 108
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Landroid/support/design/widget/CoordinatorLayout;->d:Ljava/lang/ThreadLocal;

    return-void

    .line 98
    :cond_38
    sput-object v2, Landroid/support/design/widget/CoordinatorLayout;->f:Ljava/util/Comparator;

    .line 99
    sput-object v2, Landroid/support/design/widget/CoordinatorLayout;->g:Landroid/support/design/widget/aa;

    goto :goto_21
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 161
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/CoordinatorLayout;-><init>(Landroid/content/Context;B)V

    .line 162
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4

    .prologue
    .line 165
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/CoordinatorLayout;-><init>(Landroid/content/Context;C)V

    .line 166
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .registers 9

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 169
    invoke-direct {p0, p1, v3, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 111
    new-instance v1, Landroid/support/design/widget/r;

    invoke-direct {v1, p0}, Landroid/support/design/widget/r;-><init>(Landroid/support/design/widget/CoordinatorLayout;)V

    iput-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->e:Ljava/util/Comparator;

    .line 131
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    .line 132
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->m:Ljava/util/List;

    .line 133
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->n:Ljava/util/List;

    .line 134
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 135
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 136
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->o:Landroid/graphics/Rect;

    .line 137
    const/4 v1, 0x2

    new-array v1, v1, [I

    iput-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->p:[I

    .line 157
    new-instance v1, Landroid/support/v4/view/bw;

    invoke-direct {v1, p0}, Landroid/support/v4/view/bw;-><init>(Landroid/view/ViewGroup;)V

    iput-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->C:Landroid/support/v4/view/bw;

    .line 171
    sget-object v1, Landroid/support/design/n;->CoordinatorLayout:[I

    sget v2, Landroid/support/design/m;->Widget_Design_CoordinatorLayout:I

    invoke-virtual {p1, v3, v1, v0, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 173
    sget v2, Landroid/support/design/n;->CoordinatorLayout_keylines:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 174
    if-eqz v2, :cond_73

    .line 175
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 176
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    iput-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->s:[I

    .line 177
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 178
    iget-object v3, p0, Landroid/support/design/widget/CoordinatorLayout;->s:[I

    array-length v3, v3

    .line 179
    :goto_65
    if-ge v0, v3, :cond_73

    .line 180
    iget-object v4, p0, Landroid/support/design/widget/CoordinatorLayout;->s:[I

    aget v5, v4, v0

    int-to-float v5, v5

    mul-float/2addr v5, v2

    float-to-int v5, v5

    aput v5, v4, v0

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_65

    .line 183
    :cond_73
    sget v0, Landroid/support/design/n;->CoordinatorLayout_statusBarBackground:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->A:Landroid/graphics/drawable/Drawable;

    .line 184
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 186
    sget-object v0, Landroid/support/design/widget/CoordinatorLayout;->g:Landroid/support/design/widget/aa;

    if-eqz v0, :cond_8c

    .line 187
    sget-object v0, Landroid/support/design/widget/CoordinatorLayout;->g:Landroid/support/design/widget/aa;

    new-instance v1, Landroid/support/design/widget/s;

    invoke-direct {v1, p0}, Landroid/support/design/widget/s;-><init>(Landroid/support/design/widget/CoordinatorLayout;)V

    invoke-interface {v0, p0, v1}, Landroid/support/design/widget/aa;->a(Landroid/view/View;Landroid/support/v4/view/bx;)V

    .line 189
    :cond_8c
    new-instance v0, Landroid/support/design/widget/v;

    invoke-direct {v0, p0}, Landroid/support/design/widget/v;-><init>(Landroid/support/design/widget/CoordinatorLayout;)V

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 190
    return-void
.end method

.method private a(I)I
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 476
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->s:[I

    if-nez v1, :cond_24

    .line 477
    const-string v1, "CoordinatorLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No keylines defined for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - attempted index lookup "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    :goto_23
    return v0

    .line 481
    :cond_24
    if-ltz p1, :cond_2b

    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->s:[I

    array-length v1, v1

    if-lt p1, v1, :cond_4a

    .line 482
    :cond_2b
    const-string v1, "CoordinatorLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Keyline index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " out of range for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_23

    .line 486
    :cond_4a
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->s:[I

    aget v0, v0, p1

    goto :goto_23
.end method

.method static a(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/String;)Landroid/support/design/widget/t;
    .registers 7

    .prologue
    const/16 v2, 0x2e

    .line 490
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 491
    const/4 v0, 0x0

    .line 520
    :goto_9
    return-object v0

    .line 495
    :cond_a
    const-string v0, "."

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 497
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 507
    :cond_27
    :goto_27
    :try_start_27
    sget-object v0, Landroid/support/design/widget/CoordinatorLayout;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 508
    if-nez v0, :cond_9e

    .line 509
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 510
    sget-object v1, Landroid/support/design/widget/CoordinatorLayout;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    move-object v1, v0

    .line 512
    :goto_3c
    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Constructor;

    .line 513
    if-nez v0, :cond_5a

    .line 514
    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-static {p2, v0, v2}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    .line 516
    sget-object v2, Landroid/support/design/widget/CoordinatorLayout;->c:[Ljava/lang/Class;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 517
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 518
    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    :cond_5a
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/t;
    :try_end_69
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_69} :catch_88

    goto :goto_9

    .line 498
    :cond_6a
    invoke-virtual {p2, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_27

    .line 503
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/support/design/widget/CoordinatorLayout;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_27

    .line 521
    :catch_88
    move-exception v0

    .line 522
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not inflate Behavior subclass "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_9e
    move-object v1, v0

    goto :goto_3c
.end method

.method private a(Landroid/util/AttributeSet;)Landroid/support/design/widget/w;
    .registers 4

    .prologue
    .line 1374
    new-instance v0, Landroid/support/design/widget/w;

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/design/widget/w;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/design/widget/w;
    .registers 2

    .prologue
    .line 1379
    instance-of v0, p0, Landroid/support/design/widget/w;

    if-eqz v0, :cond_c

    .line 1380
    new-instance v0, Landroid/support/design/widget/w;

    check-cast p0, Landroid/support/design/widget/w;

    invoke-direct {v0, p0}, Landroid/support/design/widget/w;-><init>(Landroid/support/design/widget/w;)V

    .line 1384
    :goto_b
    return-object v0

    .line 1381
    :cond_c
    instance-of v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_18

    .line 1382
    new-instance v0, Landroid/support/design/widget/w;

    check-cast p0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p0}, Landroid/support/design/widget/w;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_b

    .line 1384
    :cond_18
    new-instance v0, Landroid/support/design/widget/w;

    invoke-direct {v0, p0}, Landroid/support/design/widget/w;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_b
.end method

.method static synthetic a(Landroid/support/design/widget/CoordinatorLayout;)Landroid/view/ViewGroup$OnHierarchyChangeListener;
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->B:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    return-object v0
.end method

.method private a()V
    .registers 10

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 288
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->t:Landroid/view/View;

    if-eqz v0, :cond_28

    .line 289
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 4281
    iget-object v8, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 290
    if-eqz v8, :cond_25

    .line 291
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 292
    const/4 v4, 0x3

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 294
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->t:Landroid/view/View;

    invoke-virtual {v8, p0, v1, v0}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 295
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 297
    :cond_25
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->t:Landroid/view/View;

    .line 300
    :cond_28
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v2

    move v1, v7

    .line 301
    :goto_2d
    if-ge v1, v2, :cond_3f

    .line 302
    invoke-virtual {p0, v1}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 303
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 4368
    iput-boolean v7, v0, Landroid/support/design/widget/w;->i:Z

    .line 301
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2d

    .line 306
    :cond_3f
    return-void
.end method

.method static synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/v4/view/gh;)V
    .registers 2

    .prologue
    .line 86
    invoke-direct {p0, p1}, Landroid/support/design/widget/CoordinatorLayout;->setWindowInsets(Landroid/support/v4/view/gh;)V

    return-void
.end method

.method private a(Landroid/support/v4/view/gh;)V
    .registers 7

    .prologue
    .line 695
    invoke-virtual {p1}, Landroid/support/v4/view/gh;->g()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 721
    :cond_6
    return-void

    .line 699
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v3

    move v2, v0

    move-object v1, p1

    :goto_e
    if-ge v2, v3, :cond_6

    .line 700
    invoke-virtual {p0, v2}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 701
    invoke-static {v4}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 702
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 14281
    iget-object v0, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 705
    if-eqz v0, :cond_2a

    .line 708
    invoke-virtual {v1}, Landroid/support/v4/view/gh;->g()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_2a
    move-object v0, v1

    .line 715
    invoke-static {v4, v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;

    move-result-object v1

    .line 716
    invoke-virtual {v1}, Landroid/support/v4/view/gh;->g()Z

    move-result v0

    if-nez v0, :cond_6

    .line 699
    :cond_35
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_e
.end method

.method private a(Landroid/view/View;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 14

    .prologue
    .line 833
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 834
    iget v1, v0, Landroid/support/design/widget/w;->c:I

    .line 18058
    if-nez v1, :cond_c

    const/16 v1, 0x11

    .line 834
    :cond_c
    invoke-static {v1, p2}, Landroid/support/v4/view/v;->a(II)I

    move-result v1

    .line 836
    iget v2, v0, Landroid/support/design/widget/w;->d:I

    invoke-static {v2}, Landroid/support/design/widget/CoordinatorLayout;->b(I)I

    move-result v2

    invoke-static {v2, p2}, Landroid/support/v4/view/v;->a(II)I

    move-result v2

    .line 840
    and-int/lit8 v3, v1, 0x7

    .line 841
    and-int/lit8 v4, v1, 0x70

    .line 842
    and-int/lit8 v1, v2, 0x7

    .line 843
    and-int/lit8 v5, v2, 0x70

    .line 845
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 846
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 854
    sparse-switch v1, :sswitch_data_a0

    .line 857
    iget v2, p3, Landroid/graphics/Rect;->left:I

    .line 867
    :goto_2f
    sparse-switch v5, :sswitch_data_aa

    .line 870
    iget v1, p3, Landroid/graphics/Rect;->top:I

    .line 881
    :goto_34
    sparse-switch v3, :sswitch_data_b4

    .line 884
    sub-int/2addr v2, v6

    .line 894
    :goto_38
    :sswitch_38
    sparse-switch v4, :sswitch_data_be

    .line 897
    sub-int/2addr v1, v7

    .line 907
    :goto_3c
    :sswitch_3c
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getWidth()I

    move-result v3

    .line 908
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getHeight()I

    move-result v4

    .line 911
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingLeft()I

    move-result v5

    iget v8, v0, Landroid/support/design/widget/w;->leftMargin:I

    add-int/2addr v5, v8

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingRight()I

    move-result v8

    sub-int/2addr v3, v8

    sub-int/2addr v3, v6

    iget v8, v0, Landroid/support/design/widget/w;->rightMargin:I

    sub-int/2addr v3, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 914
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingTop()I

    move-result v3

    iget v5, v0, Landroid/support/design/widget/w;->topMargin:I

    add-int/2addr v3, v5

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    sub-int/2addr v4, v7

    iget v0, v0, Landroid/support/design/widget/w;->bottomMargin:I

    sub-int v0, v4, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 918
    add-int v1, v2, v6

    add-int v3, v0, v7

    invoke-virtual {p4, v2, v0, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 919
    return-void

    .line 860
    :sswitch_7d
    iget v2, p3, Landroid/graphics/Rect;->right:I

    goto :goto_2f

    .line 863
    :sswitch_80
    iget v1, p3, Landroid/graphics/Rect;->left:I

    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v1

    goto :goto_2f

    .line 873
    :sswitch_8a
    iget v1, p3, Landroid/graphics/Rect;->bottom:I

    goto :goto_34

    .line 876
    :sswitch_8d
    iget v1, p3, Landroid/graphics/Rect;->top:I

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v1, v5

    goto :goto_34

    .line 890
    :sswitch_97
    div-int/lit8 v3, v6, 0x2

    sub-int/2addr v2, v3

    goto :goto_38

    .line 903
    :sswitch_9b
    div-int/lit8 v3, v7, 0x2

    sub-int/2addr v1, v3

    goto :goto_3c

    .line 854
    nop

    :sswitch_data_a0
    .sparse-switch
        0x1 -> :sswitch_80
        0x5 -> :sswitch_7d
    .end sparse-switch

    .line 867
    :sswitch_data_aa
    .sparse-switch
        0x10 -> :sswitch_8d
        0x50 -> :sswitch_8a
    .end sparse-switch

    .line 881
    :sswitch_data_b4
    .sparse-switch
        0x1 -> :sswitch_97
        0x5 -> :sswitch_38
    .end sparse-switch

    .line 894
    :sswitch_data_be
    .sparse-switch
        0x10 -> :sswitch_9b
        0x50 -> :sswitch_3c
    .end sparse-switch
.end method

.method private a(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 3

    .prologue
    .line 579
    invoke-static {p0, p1, p2}, Landroid/support/design/widget/cz;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 580
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;I)V
    .registers 8

    .prologue
    .line 929
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 931
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 932
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 18579
    invoke-static {p0, p2, v0}, Landroid/support/design/widget/cz;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 934
    invoke-direct {p0, p1, p3, v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 936
    iget v0, v1, Landroid/graphics/Rect;->left:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v0, v2, v3, v1}, Landroid/view/View;->layout(IIII)V

    .line 937
    return-void
.end method

.method private a(Ljava/util/List;)V
    .registers 6

    .prologue
    .line 313
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 315
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->isChildrenDrawingOrderEnabled()Z

    move-result v2

    .line 316
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v3

    .line 317
    add-int/lit8 v1, v3, -0x1

    :goto_d
    if-ltz v1, :cond_21

    .line 318
    if-eqz v2, :cond_1f

    invoke-virtual {p0, v3, v1}, Landroid/support/design/widget/CoordinatorLayout;->getChildDrawingOrder(II)I

    move-result v0

    .line 319
    :goto_15
    invoke-virtual {p0, v0}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 320
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 317
    add-int/lit8 v1, v1, -0x1

    goto :goto_d

    :cond_1f
    move v0, v1

    .line 318
    goto :goto_15

    .line 323
    :cond_21
    sget-object v0, Landroid/support/design/widget/CoordinatorLayout;->f:Ljava/util/Comparator;

    if-eqz v0, :cond_2a

    .line 324
    sget-object v0, Landroid/support/design/widget/CoordinatorLayout;->f:Ljava/util/Comparator;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 326
    :cond_2a
    return-void
.end method

.method private a(Landroid/view/MotionEvent;I)Z
    .registers 24

    .prologue
    .line 329
    const/4 v14, 0x0

    .line 330
    const/4 v13, 0x0

    .line 332
    const/4 v6, 0x0

    .line 334
    invoke-static/range {p1 .. p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v16

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/design/widget/CoordinatorLayout;->m:Ljava/util/List;

    move-object/from16 v17, v0

    .line 5313
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->clear()V

    .line 5315
    invoke-virtual/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->isChildrenDrawingOrderEnabled()Z

    move-result v7

    .line 5316
    invoke-virtual/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v8

    .line 5317
    add-int/lit8 v5, v8, -0x1

    :goto_1a
    if-ltz v5, :cond_34

    .line 5318
    if-eqz v7, :cond_32

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v5}, Landroid/support/design/widget/CoordinatorLayout;->getChildDrawingOrder(II)I

    move-result v4

    .line 5319
    :goto_24
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 5320
    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5317
    add-int/lit8 v5, v5, -0x1

    goto :goto_1a

    :cond_32
    move v4, v5

    .line 5318
    goto :goto_24

    .line 5323
    :cond_34
    sget-object v4, Landroid/support/design/widget/CoordinatorLayout;->f:Ljava/util/Comparator;

    if-eqz v4, :cond_3f

    .line 5324
    sget-object v4, Landroid/support/design/widget/CoordinatorLayout;->f:Ljava/util/Comparator;

    move-object/from16 v0, v17

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 340
    :cond_3f
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v18

    .line 341
    const/4 v4, 0x0

    move v15, v4

    move-object v5, v6

    :goto_46
    move/from16 v0, v18

    if-ge v15, v0, :cond_dd

    .line 342
    move-object/from16 v0, v17

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Landroid/view/View;

    .line 343
    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/support/design/widget/w;

    .line 6281
    iget-object v0, v4, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    move-object/from16 v19, v0

    .line 346
    if-nez v14, :cond_61

    if-eqz v13, :cond_92

    :cond_61
    if-eqz v16, :cond_92

    .line 349
    if-eqz v19, :cond_e4

    .line 350
    if-nez v5, :cond_e2

    .line 351
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 352
    const/4 v8, 0x3

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide v6, v4

    invoke-static/range {v4 .. v11}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v4

    .line 355
    :goto_74
    packed-switch p2, :pswitch_data_e8

    :goto_77
    move v5, v13

    move v6, v14

    .line 341
    :goto_79
    add-int/lit8 v7, v15, 0x1

    move v15, v7

    move v13, v5

    move v14, v6

    move-object v5, v4

    goto :goto_46

    .line 357
    :pswitch_80
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v12, v4}, Landroid/support/design/widget/t;->b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z

    move v5, v13

    move v6, v14

    .line 358
    goto :goto_79

    .line 360
    :pswitch_8a
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v12, v4}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z

    goto :goto_77

    .line 367
    :cond_92
    if-nez v14, :cond_9f

    if-eqz v19, :cond_9f

    .line 368
    packed-switch p2, :pswitch_data_f0

    .line 376
    :goto_99
    if-eqz v14, :cond_9f

    .line 377
    move-object/from16 v0, p0

    iput-object v12, v0, Landroid/support/design/widget/CoordinatorLayout;->t:Landroid/view/View;

    :cond_9f
    move v6, v14

    .line 6333
    iget-object v7, v4, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    if-nez v7, :cond_a7

    .line 6334
    const/4 v7, 0x0

    iput-boolean v7, v4, Landroid/support/design/widget/w;->i:Z

    .line 6336
    :cond_a7
    iget-boolean v8, v4, Landroid/support/design/widget/w;->i:Z

    .line 6351
    iget-boolean v7, v4, Landroid/support/design/widget/w;->i:Z

    if-eqz v7, :cond_d4

    .line 6352
    const/4 v4, 0x1

    move v7, v4

    .line 385
    :goto_af
    if-eqz v7, :cond_db

    if-nez v8, :cond_db

    const/4 v4, 0x1

    .line 386
    :goto_b4
    if-eqz v7, :cond_b8

    if-eqz v4, :cond_de

    :cond_b8
    move-object/from16 v20, v5

    move v5, v4

    move-object/from16 v4, v20

    goto :goto_79

    .line 370
    :pswitch_be
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v12, v2}, Landroid/support/design/widget/t;->b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v14

    goto :goto_99

    .line 373
    :pswitch_c9
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v12, v2}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v14

    goto :goto_99

    .line 6355
    :cond_d4
    iget-boolean v7, v4, Landroid/support/design/widget/w;->i:Z

    or-int/lit8 v7, v7, 0x0

    iput-boolean v7, v4, Landroid/support/design/widget/w;->i:Z

    goto :goto_af

    .line 385
    :cond_db
    const/4 v4, 0x0

    goto :goto_b4

    :cond_dd
    move v6, v14

    .line 393
    :cond_de
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->clear()V

    .line 395
    return v6

    :cond_e2
    move-object v4, v5

    goto :goto_74

    :cond_e4
    move-object v4, v5

    move v6, v14

    move v5, v13

    goto :goto_79

    .line 355
    :pswitch_data_e8
    .packed-switch 0x0
        :pswitch_80
        :pswitch_8a
    .end packed-switch

    .line 368
    :pswitch_data_f0
    .packed-switch 0x0
        :pswitch_be
        :pswitch_c9
    .end packed-switch
.end method

.method private a(Landroid/view/View;Landroid/view/View;)Z
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1360
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3f

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3f

    .line 1361
    iget-object v3, p0, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 1362
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_40

    move v0, v1

    :goto_17
    invoke-virtual {p0, p1, v0, v3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 1363
    iget-object v4, p0, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 1364
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_42

    move v0, v1

    :goto_23
    invoke-virtual {p0, p2, v0, v4}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 1366
    iget v0, v3, Landroid/graphics/Rect;->left:I

    iget v5, v4, Landroid/graphics/Rect;->right:I

    if-gt v0, v5, :cond_3f

    iget v0, v3, Landroid/graphics/Rect;->top:I

    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    if-gt v0, v5, :cond_3f

    iget v0, v3, Landroid/graphics/Rect;->right:I

    iget v5, v4, Landroid/graphics/Rect;->left:I

    if-lt v0, v5, :cond_3f

    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    iget v3, v4, Landroid/graphics/Rect;->top:I

    if-lt v0, v3, :cond_3f

    move v2, v1

    .line 1369
    :cond_3f
    return v2

    :cond_40
    move v0, v2

    .line 1362
    goto :goto_17

    :cond_42
    move v0, v2

    .line 1364
    goto :goto_23
.end method

.method private static b(I)I
    .registers 1

    .prologue
    .line 1042
    if-nez p0, :cond_5

    const p0, 0x800033

    :cond_5
    return p0
.end method

.method private static b(Landroid/view/View;)Landroid/support/design/widget/w;
    .registers 7

    .prologue
    .line 527
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 528
    iget-boolean v1, v0, Landroid/support/design/widget/w;->b:Z

    if-nez v1, :cond_33

    .line 529
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 530
    const/4 v1, 0x0

    .line 531
    :goto_f
    if-eqz v2, :cond_20

    const-class v1, Landroid/support/design/widget/u;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/u;

    if-nez v1, :cond_20

    .line 533
    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    goto :goto_f

    :cond_20
    move-object v2, v1

    .line 535
    if-eqz v2, :cond_30

    .line 537
    :try_start_23
    invoke-interface {v2}, Landroid/support/design/widget/u;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/t;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/w;->a(Landroid/support/design/widget/t;)V
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_30} :catch_34

    .line 543
    :cond_30
    :goto_30
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/design/widget/w;->b:Z

    .line 545
    :cond_33
    return-object v0

    .line 538
    :catch_34
    move-exception v1

    .line 539
    const-string v3, "CoordinatorLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Default behavior class "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Landroid/support/design/widget/u;->a()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " could not be instantiated. Did you forget a default constructor?"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_30
.end method

.method private b()V
    .registers 12

    .prologue
    const/4 v3, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 549
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v6

    .line 551
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v6, :cond_28

    move v2, v3

    :goto_10
    move v5, v4

    .line 553
    :goto_11
    if-ge v5, v6, :cond_d0

    .line 554
    invoke-virtual {p0, v5}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 555
    invoke-static {v7}, Landroid/support/design/widget/CoordinatorLayout;->b(Landroid/view/View;)Landroid/support/design/widget/w;

    move-result-object v8

    .line 8428
    iget v0, v8, Landroid/support/design/widget/w;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2a

    .line 8429
    iput-object v10, v8, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v10, v8, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 553
    :cond_24
    :goto_24
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_11

    :cond_28
    move v2, v4

    .line 551
    goto :goto_10

    .line 8433
    :cond_2a
    iget-object v0, v8, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-eqz v0, :cond_3b

    .line 8494
    iget-object v0, v8, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    iget v1, v8, Landroid/support/design/widget/w;->f:I

    if-eq v0, v1, :cond_60

    move v0, v4

    .line 8433
    :goto_39
    if-nez v0, :cond_24

    .line 9458
    :cond_3b
    iget v0, v8, Landroid/support/design/widget/w;->f:I

    invoke-virtual {p0, v0}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v8, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 9459
    iget-object v0, v8, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-eqz v0, :cond_9b

    .line 9460
    iget-object v0, v8, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 9461
    iget-object v1, v8, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 9462
    :goto_4f
    if-eq v1, p0, :cond_98

    if-eqz v1, :cond_98

    .line 9464
    if-ne v1, v7, :cond_8c

    .line 9465
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_84

    .line 9466
    iput-object v10, v8, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v10, v8, Landroid/support/design/widget/w;->g:Landroid/view/View;

    goto :goto_24

    .line 8498
    :cond_60
    iget-object v0, v8, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 8499
    iget-object v1, v8, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 8500
    :goto_68
    if-eq v1, p0, :cond_80

    .line 8502
    if-eqz v1, :cond_6e

    if-ne v1, v7, :cond_74

    .line 8503
    :cond_6e
    iput-object v10, v8, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v10, v8, Landroid/support/design/widget/w;->g:Landroid/view/View;

    move v0, v4

    .line 8504
    goto :goto_39

    .line 8506
    :cond_74
    instance-of v9, v1, Landroid/view/View;

    if-eqz v9, :cond_7b

    move-object v0, v1

    .line 8507
    check-cast v0, Landroid/view/View;

    .line 8501
    :cond_7b
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_68

    .line 8510
    :cond_80
    iput-object v0, v8, Landroid/support/design/widget/w;->h:Landroid/view/View;

    move v0, v3

    .line 8511
    goto :goto_39

    .line 9469
    :cond_84
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Anchor must not be a descendant of the anchored view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9472
    :cond_8c
    instance-of v9, v1, Landroid/view/View;

    if-eqz v9, :cond_93

    move-object v0, v1

    .line 9473
    check-cast v0, Landroid/view/View;

    .line 9463
    :cond_93
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_4f

    .line 9476
    :cond_98
    iput-object v0, v8, Landroid/support/design/widget/w;->h:Landroid/view/View;

    goto :goto_24

    .line 9478
    :cond_9b
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_a7

    .line 9479
    iput-object v10, v8, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v10, v8, Landroid/support/design/widget/w;->g:Landroid/view/View;

    goto/16 :goto_24

    .line 9482
    :cond_a7
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find CoordinatorLayout descendant view with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, v8, Landroid/support/design/widget/w;->f:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to anchor view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 562
    :cond_d0
    if-eqz v2, :cond_ec

    .line 563
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 564
    :goto_d7
    if-ge v4, v6, :cond_e5

    .line 565
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-virtual {p0, v4}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 564
    add-int/lit8 v4, v4, 0x1

    goto :goto_d7

    .line 567
    :cond_e5
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->e:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 569
    :cond_ec
    return-void
.end method

.method private b(Landroid/view/View;I)V
    .registers 10

    .prologue
    .line 1014
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 1015
    iget-object v3, p0, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 1016
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingLeft()I

    move-result v1

    iget v2, v0, Landroid/support/design/widget/w;->leftMargin:I

    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingTop()I

    move-result v2

    iget v4, v0, Landroid/support/design/widget/w;->topMargin:I

    add-int/2addr v2, v4

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, v0, Landroid/support/design/widget/w;->rightMargin:I

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v5, v6

    iget v6, v0, Landroid/support/design/widget/w;->bottomMargin:I

    sub-int/2addr v5, v6

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 1021
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    if-eqz v1, :cond_6d

    invoke-static {p0}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_6d

    invoke-static {p1}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_6d

    .line 1025
    iget v1, v3, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v2}, Landroid/support/v4/view/gh;->a()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v3, Landroid/graphics/Rect;->left:I

    .line 1026
    iget v1, v3, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v2}, Landroid/support/v4/view/gh;->b()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v3, Landroid/graphics/Rect;->top:I

    .line 1027
    iget v1, v3, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v2}, Landroid/support/v4/view/gh;->c()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v3, Landroid/graphics/Rect;->right:I

    .line 1028
    iget v1, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v2}, Landroid/support/v4/view/gh;->d()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v3, Landroid/graphics/Rect;->bottom:I

    .line 1031
    :cond_6d
    iget-object v4, p0, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 1032
    iget v0, v0, Landroid/support/design/widget/w;->c:I

    invoke-static {v0}, Landroid/support/design/widget/CoordinatorLayout;->b(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    move v5, p2

    invoke-static/range {v0 .. v5}, Landroid/support/v4/view/v;->a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 1034
    iget v0, v4, Landroid/graphics/Rect;->left:I

    iget v1, v4, Landroid/graphics/Rect;->top:I

    iget v2, v4, Landroid/graphics/Rect;->right:I

    iget v3, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 1035
    return-void
.end method

.method private b(Landroid/view/View;II)V
    .registers 13

    .prologue
    .line 951
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 952
    iget v1, v0, Landroid/support/design/widget/w;->c:I

    invoke-static {v1}, Landroid/support/design/widget/CoordinatorLayout;->c(I)I

    move-result v1

    invoke-static {v1, p3}, Landroid/support/v4/view/v;->a(II)I

    move-result v1

    .line 955
    and-int/lit8 v3, v1, 0x7

    .line 956
    and-int/lit8 v4, v1, 0x70

    .line 957
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getWidth()I

    move-result v5

    .line 958
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getHeight()I

    move-result v6

    .line 959
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 960
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 962
    const/4 v1, 0x1

    if-ne p3, v1, :cond_29

    .line 963
    sub-int p2, v5, p2

    .line 966
    :cond_29
    invoke-direct {p0, p2}, Landroid/support/design/widget/CoordinatorLayout;->a(I)I

    move-result v1

    sub-int v2, v1, v7

    .line 967
    const/4 v1, 0x0

    .line 969
    sparse-switch v3, :sswitch_data_80

    .line 982
    :goto_33
    sparse-switch v4, :sswitch_data_8a

    .line 996
    :goto_36
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingLeft()I

    move-result v3

    iget v4, v0, Landroid/support/design/widget/w;->leftMargin:I

    add-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingRight()I

    move-result v4

    sub-int v4, v5, v4

    sub-int/2addr v4, v7

    iget v5, v0, Landroid/support/design/widget/w;->rightMargin:I

    sub-int/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 999
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingTop()I

    move-result v3

    iget v4, v0, Landroid/support/design/widget/w;->topMargin:I

    add-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingBottom()I

    move-result v4

    sub-int v4, v6, v4

    sub-int/2addr v4, v8

    iget v0, v0, Landroid/support/design/widget/w;->bottomMargin:I

    sub-int v0, v4, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1003
    add-int v1, v2, v7

    add-int v3, v0, v8

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/view/View;->layout(IIII)V

    .line 1004
    return-void

    .line 975
    :sswitch_71
    add-int/2addr v2, v7

    .line 976
    goto :goto_33

    .line 978
    :sswitch_73
    div-int/lit8 v3, v7, 0x2

    add-int/2addr v2, v3

    goto :goto_33

    .line 988
    :sswitch_77
    add-int/lit8 v1, v8, 0x0

    .line 989
    goto :goto_36

    .line 991
    :sswitch_7a
    div-int/lit8 v1, v8, 0x2

    add-int/lit8 v1, v1, 0x0

    goto :goto_36

    .line 969
    nop

    :sswitch_data_80
    .sparse-switch
        0x1 -> :sswitch_73
        0x5 -> :sswitch_71
    .end sparse-switch

    .line 982
    :sswitch_data_8a
    .sparse-switch
        0x10 -> :sswitch_7a
        0x50 -> :sswitch_77
    .end sparse-switch
.end method

.method private static b(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 3

    .prologue
    .line 786
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 17306
    iget-object v0, v0, Landroid/support/design/widget/w;->l:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 788
    return-void
.end method

.method private static c(I)I
    .registers 1

    .prologue
    .line 1050
    if-nez p0, :cond_5

    const p0, 0x800035

    :cond_5
    return p0
.end method

.method private c()V
    .registers 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1223
    .line 1224
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v5

    move v4, v2

    .line 1225
    :goto_7
    if-ge v4, v5, :cond_6e

    .line 1226
    invoke-virtual {p0, v4}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 27246
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 27247
    iget-object v3, v0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-eqz v3, :cond_3c

    move v0, v1

    .line 1227
    :goto_18
    if-eqz v0, :cond_56

    move v0, v1

    .line 1233
    :goto_1b
    iget-boolean v3, p0, Landroid/support/design/widget/CoordinatorLayout;->x:Z

    if-eq v0, v3, :cond_3b

    .line 1234
    if-eqz v0, :cond_5a

    .line 27269
    iget-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->r:Z

    if-eqz v0, :cond_39

    .line 27271
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    if-nez v0, :cond_30

    .line 27272
    new-instance v0, Landroid/support/design/widget/x;

    invoke-direct {v0, p0}, Landroid/support/design/widget/x;-><init>(Landroid/support/design/widget/CoordinatorLayout;)V

    iput-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    .line 27274
    :cond_30
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 27275
    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 27280
    :cond_39
    iput-boolean v1, p0, Landroid/support/design/widget/CoordinatorLayout;->x:Z

    .line 1240
    :cond_3b
    :goto_3b
    return-void

    .line 27251
    :cond_3c
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v7

    move v3, v2

    .line 27252
    :goto_41
    if-ge v3, v7, :cond_54

    .line 27253
    invoke-virtual {p0, v3}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 27254
    if-eq v8, v6, :cond_51

    .line 27257
    invoke-virtual {v0, v8}, Landroid/support/design/widget/w;->a(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_51

    move v0, v1

    .line 27258
    goto :goto_18

    .line 27252
    :cond_51
    add-int/lit8 v3, v3, 0x1

    goto :goto_41

    :cond_54
    move v0, v2

    .line 27261
    goto :goto_18

    .line 1225
    :cond_56
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_7

    .line 27288
    :cond_5a
    iget-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->r:Z

    if-eqz v0, :cond_6b

    .line 27289
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    if-eqz v0, :cond_6b

    .line 27290
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 27291
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 27294
    :cond_6b
    iput-boolean v2, p0, Landroid/support/design/widget/CoordinatorLayout;->x:Z

    goto :goto_3b

    :cond_6e
    move v0, v2

    goto :goto_1b
.end method

.method private c(Landroid/view/View;)V
    .registers 7

    .prologue
    .line 1147
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v2

    .line 1148
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v2, :cond_23

    .line 1149
    invoke-virtual {p0, v1}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1150
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 25281
    iget-object v0, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 1153
    if-eqz v0, :cond_1f

    invoke-virtual {v0, p1}, Landroid/support/design/widget/t;->b(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 1154
    invoke-virtual {v0, v3, p1}, Landroid/support/design/widget/t;->a(Landroid/view/View;Landroid/view/View;)V

    .line 1148
    :cond_1f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1157
    :cond_23
    return-void
.end method

.method private c(Landroid/view/View;I)V
    .registers 8

    .prologue
    .line 1306
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 1307
    iget-object v1, v0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-eqz v1, :cond_3e

    .line 1308
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 1309
    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 1310
    iget-object v3, p0, Landroid/support/design/widget/CoordinatorLayout;->o:Landroid/graphics/Rect;

    .line 1312
    iget-object v4, v0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 27579
    invoke-static {p0, v4, v1}, Landroid/support/design/widget/cz;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1313
    const/4 v4, 0x0

    invoke-virtual {p0, p1, v4, v2}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 1314
    invoke-direct {p0, p1, p2, v1, v3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 1316
    iget v1, v3, Landroid/graphics/Rect;->left:I

    iget v4, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v4

    .line 1317
    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v2, v3, v2

    .line 1319
    if-eqz v1, :cond_2c

    .line 1320
    invoke-virtual {p1, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1322
    :cond_2c
    if-eqz v2, :cond_31

    .line 1323
    invoke-virtual {p1, v2}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1326
    :cond_31
    if-nez v1, :cond_35

    if-eqz v2, :cond_3e

    .line 28281
    :cond_35
    iget-object v1, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 1329
    if-eqz v1, :cond_3e

    .line 1330
    iget-object v0, v0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v1, p0, p1, v0}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z

    .line 1334
    :cond_3e
    return-void
.end method

.method private static c(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 3

    .prologue
    .line 798
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 17314
    iget-object v0, v0, Landroid/support/design/widget/w;->l:Landroid/graphics/Rect;

    .line 799
    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 800
    return-void
.end method

.method private static d(I)I
    .registers 1

    .prologue
    .line 1058
    if-nez p0, :cond_4

    const/16 p0, 0x11

    :cond_4
    return p0
.end method

.method private d()V
    .registers 3

    .prologue
    .line 1269
    iget-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->r:Z

    if-eqz v0, :cond_18

    .line 1271
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    if-nez v0, :cond_f

    .line 1272
    new-instance v0, Landroid/support/design/widget/x;

    invoke-direct {v0, p0}, Landroid/support/design/widget/x;-><init>(Landroid/support/design/widget/CoordinatorLayout;)V

    iput-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    .line 1274
    :cond_f
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1275
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1280
    :cond_18
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->x:Z

    .line 1281
    return-void
.end method

.method private d(Landroid/view/View;)V
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 1170
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    .line 1172
    :goto_9
    if-ge v3, v4, :cond_32

    .line 1173
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1174
    if-ne v0, p1, :cond_1b

    .line 1176
    const/4 v0, 0x1

    .line 1172
    :goto_16
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_9

    .line 1179
    :cond_1b
    if-eqz v2, :cond_30

    .line 1180
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/w;

    .line 26281
    iget-object v5, v1, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 1183
    if-eqz v5, :cond_30

    invoke-virtual {v1, p1}, Landroid/support/design/widget/w;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_30

    .line 1184
    invoke-virtual {v5, p0, v0, p1}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z

    :cond_30
    move v0, v2

    goto :goto_16

    .line 1188
    :cond_32
    return-void
.end method

.method private e()V
    .registers 3

    .prologue
    .line 1288
    iget-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->r:Z

    if-eqz v0, :cond_11

    .line 1289
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    if-eqz v0, :cond_11

    .line 1290
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1291
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1294
    :cond_11
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->x:Z

    .line 1295
    return-void
.end method

.method private e(Landroid/view/View;)Z
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1246
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 1247
    iget-object v3, v0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-eqz v3, :cond_e

    move v0, v1

    .line 1261
    :goto_d
    return v0

    .line 1251
    :cond_e
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v4

    move v3, v2

    .line 1252
    :goto_13
    if-ge v3, v4, :cond_26

    .line 1253
    invoke-virtual {p0, v3}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1254
    if-eq v5, p1, :cond_23

    .line 1257
    invoke-virtual {v0, v5}, Landroid/support/design/widget/w;->a(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_23

    move v0, v1

    .line 1258
    goto :goto_d

    .line 1252
    :cond_23
    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    :cond_26
    move v0, v2

    .line 1261
    goto :goto_d
.end method

.method private static f()Landroid/support/design/widget/w;
    .registers 1

    .prologue
    .line 1389
    new-instance v0, Landroid/support/design/widget/w;

    invoke-direct {v0}, Landroid/support/design/widget/w;-><init>()V

    return-object v0
.end method

.method private setWindowInsets(Landroid/support/v4/view/gh;)V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 272
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    if-eq v0, p1, :cond_5c

    .line 273
    iput-object p1, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    .line 274
    if-eqz p1, :cond_55

    invoke-virtual {p1}, Landroid/support/v4/view/gh;->b()I

    move-result v0

    if-lez v0, :cond_55

    move v0, v1

    :goto_11
    iput-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->z:Z

    .line 275
    iget-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->z:Z

    if-nez v0, :cond_57

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_57

    :goto_1d
    invoke-virtual {p0, v1}, Landroid/support/design/widget/CoordinatorLayout;->setWillNotDraw(Z)V

    .line 2695
    invoke-virtual {p1}, Landroid/support/v4/view/gh;->g()Z

    move-result v0

    if-nez v0, :cond_59

    .line 2699
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v3

    move-object v1, p1

    :goto_2b
    if-ge v2, v3, :cond_59

    .line 2700
    invoke-virtual {p0, v2}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2701
    invoke-static {v4}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 2702
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 3281
    iget-object v0, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 2705
    if-eqz v0, :cond_47

    .line 2708
    invoke-virtual {v1}, Landroid/support/v4/view/gh;->g()Z

    move-result v0

    if-nez v0, :cond_59

    :cond_47
    move-object v0, v1

    .line 2715
    invoke-static {v4, v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;

    move-result-object v1

    .line 2716
    invoke-virtual {v1}, Landroid/support/v4/view/gh;->g()Z

    move-result v0

    if-nez v0, :cond_59

    .line 2699
    :cond_52
    add-int/lit8 v2, v2, 0x1

    goto :goto_2b

    :cond_55
    move v0, v2

    .line 274
    goto :goto_11

    :cond_57
    move v1, v2

    .line 275
    goto :goto_1d

    .line 277
    :cond_59
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->requestLayout()V

    .line 279
    :cond_5c
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Ljava/util/List;
    .registers 8

    .prologue
    .line 1201
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 1202
    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->n:Ljava/util/List;

    .line 1203
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1205
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v3

    .line 1206
    const/4 v1, 0x0

    :goto_10
    if-ge v1, v3, :cond_24

    .line 1207
    invoke-virtual {p0, v1}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1208
    if-eq v4, p1, :cond_21

    .line 1211
    invoke-virtual {v0, v4}, Landroid/support/design/widget/w;->a(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 1212
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1206
    :cond_21
    add-int/lit8 v1, v1, 0x1

    goto :goto_10

    .line 1216
    :cond_24
    return-object v2
.end method

.method public final a(Landroid/view/View;I)V
    .registers 13

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 734
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 15322
    iget-object v1, v0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-nez v1, :cond_1c

    iget v1, v0, Landroid/support/design/widget/w;->f:I

    const/4 v4, -0x1

    if-eq v1, v4, :cond_1c

    move v1, v3

    .line 735
    :goto_12
    if-eqz v1, :cond_1e

    .line 736
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "An anchor may not be changed after CoordinatorLayout measurement begins before layout is complete."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1c
    move v1, v2

    .line 15322
    goto :goto_12

    .line 739
    :cond_1e
    iget-object v1, v0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-eqz v1, :cond_3d

    .line 740
    iget-object v0, v0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 15929
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 15931
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 15932
    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 16579
    invoke-static {p0, v0, v1}, Landroid/support/design/widget/cz;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 15934
    invoke-direct {p0, p1, p2, v1, v2}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 15936
    iget v0, v2, Landroid/graphics/Rect;->left:I

    iget v1, v2, Landroid/graphics/Rect;->top:I

    iget v3, v2, Landroid/graphics/Rect;->right:I

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v0, v1, v3, v2}, Landroid/view/View;->layout(IIII)V

    .line 746
    :goto_3c
    return-void

    .line 741
    :cond_3d
    iget v1, v0, Landroid/support/design/widget/w;->e:I

    if-ltz v1, :cond_c3

    .line 742
    iget v1, v0, Landroid/support/design/widget/w;->e:I

    .line 16951
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 16952
    iget v4, v0, Landroid/support/design/widget/w;->c:I

    invoke-static {v4}, Landroid/support/design/widget/CoordinatorLayout;->c(I)I

    move-result v4

    invoke-static {v4, p2}, Landroid/support/v4/view/v;->a(II)I

    move-result v4

    .line 16955
    and-int/lit8 v5, v4, 0x7

    .line 16956
    and-int/lit8 v4, v4, 0x70

    .line 16957
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getWidth()I

    move-result v6

    .line 16958
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getHeight()I

    move-result v7

    .line 16959
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 16960
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 16962
    if-ne p2, v3, :cond_6b

    .line 16963
    sub-int v1, v6, v1

    .line 16966
    :cond_6b
    invoke-direct {p0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(I)I

    move-result v1

    sub-int/2addr v1, v8

    .line 16969
    sparse-switch v5, :sswitch_data_152

    move v3, v1

    .line 16982
    :goto_74
    sparse-switch v4, :sswitch_data_15c

    move v1, v2

    .line 16996
    :goto_78
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingLeft()I

    move-result v2

    iget v4, v0, Landroid/support/design/widget/w;->leftMargin:I

    add-int/2addr v2, v4

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingRight()I

    move-result v4

    sub-int v4, v6, v4

    sub-int/2addr v4, v8

    iget v5, v0, Landroid/support/design/widget/w;->rightMargin:I

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 16999
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingTop()I

    move-result v3

    iget v4, v0, Landroid/support/design/widget/w;->topMargin:I

    add-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingBottom()I

    move-result v4

    sub-int v4, v7, v4

    sub-int/2addr v4, v9

    iget v0, v0, Landroid/support/design/widget/w;->bottomMargin:I

    sub-int v0, v4, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 17003
    add-int v1, v2, v8

    add-int v3, v0, v9

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/view/View;->layout(IIII)V

    goto :goto_3c

    .line 16975
    :sswitch_b3
    add-int/2addr v1, v8

    move v3, v1

    .line 16976
    goto :goto_74

    .line 16978
    :sswitch_b6
    div-int/lit8 v3, v8, 0x2

    add-int/2addr v1, v3

    move v3, v1

    goto :goto_74

    .line 16988
    :sswitch_bb
    add-int/lit8 v1, v9, 0x0

    .line 16989
    goto :goto_78

    .line 16991
    :sswitch_be
    div-int/lit8 v1, v9, 0x2

    add-int/lit8 v1, v1, 0x0

    goto :goto_78

    .line 17014
    :cond_c3
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 17015
    iget-object v3, p0, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 17016
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingLeft()I

    move-result v1

    iget v2, v0, Landroid/support/design/widget/w;->leftMargin:I

    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingTop()I

    move-result v2

    iget v4, v0, Landroid/support/design/widget/w;->topMargin:I

    add-int/2addr v2, v4

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, v0, Landroid/support/design/widget/w;->rightMargin:I

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v5, v6

    iget v6, v0, Landroid/support/design/widget/w;->bottomMargin:I

    sub-int/2addr v5, v6

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 17021
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    if-eqz v1, :cond_130

    invoke-static {p0}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_130

    invoke-static {p1}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_130

    .line 17025
    iget v1, v3, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v2}, Landroid/support/v4/view/gh;->a()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v3, Landroid/graphics/Rect;->left:I

    .line 17026
    iget v1, v3, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v2}, Landroid/support/v4/view/gh;->b()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v3, Landroid/graphics/Rect;->top:I

    .line 17027
    iget v1, v3, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v2}, Landroid/support/v4/view/gh;->c()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v3, Landroid/graphics/Rect;->right:I

    .line 17028
    iget v1, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v2}, Landroid/support/v4/view/gh;->d()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v3, Landroid/graphics/Rect;->bottom:I

    .line 17031
    :cond_130
    iget-object v4, p0, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 17032
    iget v0, v0, Landroid/support/design/widget/w;->c:I

    invoke-static {v0}, Landroid/support/design/widget/CoordinatorLayout;->b(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    move v5, p2

    invoke-static/range {v0 .. v5}, Landroid/support/v4/view/v;->a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 17034
    iget v0, v4, Landroid/graphics/Rect;->left:I

    iget v1, v4, Landroid/graphics/Rect;->top:I

    iget v2, v4, Landroid/graphics/Rect;->right:I

    iget v3, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_3c

    .line 16969
    nop

    :sswitch_data_152
    .sparse-switch
        0x1 -> :sswitch_b6
        0x5 -> :sswitch_b3
    .end sparse-switch

    .line 16982
    :sswitch_data_15c
    .sparse-switch
        0x10 -> :sswitch_be
        0x50 -> :sswitch_bb
    .end sparse-switch
.end method

.method public final a(Landroid/view/View;III)V
    .registers 11

    .prologue
    .line 607
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/design/widget/CoordinatorLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 609
    return-void
.end method

.method final a(Landroid/view/View;ZLandroid/graphics/Rect;)V
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 812
    invoke-virtual {p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-nez v0, :cond_f

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_13

    .line 813
    :cond_f
    invoke-virtual {p3, v2, v2, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 821
    :goto_12
    return-void

    .line 816
    :cond_13
    if-eqz p2, :cond_19

    .line 17579
    invoke-static {p0, p1, p3}, Landroid/support/design/widget/cz;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_12

    .line 819
    :cond_19
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-virtual {p3, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_12
.end method

.method final a(Z)V
    .registers 14

    .prologue
    const/4 v4, 0x0

    .line 1095
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v6

    .line 1096
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    move v5, v4

    .line 1097
    :goto_c
    if-ge v5, v7, :cond_c6

    .line 1098
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1099
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/w;

    move v3, v4

    .line 1102
    :goto_1d
    if-ge v3, v5, :cond_6c

    .line 1103
    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1105
    iget-object v8, v1, Landroid/support/design/widget/w;->h:Landroid/view/View;

    if-ne v8, v2, :cond_68

    .line 19306
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/design/widget/w;

    .line 19307
    iget-object v8, v2, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-eqz v8, :cond_68

    .line 19308
    iget-object v8, p0, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 19309
    iget-object v9, p0, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 19310
    iget-object v10, p0, Landroid/support/design/widget/CoordinatorLayout;->o:Landroid/graphics/Rect;

    .line 19312
    iget-object v11, v2, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 19579
    invoke-static {p0, v11, v8}, Landroid/support/design/widget/cz;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 19313
    invoke-virtual {p0, v0, v4, v9}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 19314
    invoke-direct {p0, v0, v6, v8, v10}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 19316
    iget v8, v10, Landroid/graphics/Rect;->left:I

    iget v11, v9, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v11

    .line 19317
    iget v10, v10, Landroid/graphics/Rect;->top:I

    iget v9, v9, Landroid/graphics/Rect;->top:I

    sub-int v9, v10, v9

    .line 19319
    if-eqz v8, :cond_56

    .line 19320
    invoke-virtual {v0, v8}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 19322
    :cond_56
    if-eqz v9, :cond_5b

    .line 19323
    invoke-virtual {v0, v9}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 19326
    :cond_5b
    if-nez v8, :cond_5f

    if-eqz v9, :cond_68

    .line 20281
    :cond_5f
    iget-object v8, v2, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 19329
    if-eqz v8, :cond_68

    .line 19330
    iget-object v2, v2, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v8, p0, v0, v2}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z

    .line 1102
    :cond_68
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1d

    .line 1111
    :cond_6c
    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 1112
    iget-object v3, p0, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 20798
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/w;

    .line 21314
    iget-object v1, v1, Landroid/support/design/widget/w;->l:Landroid/graphics/Rect;

    .line 20799
    invoke-virtual {v2, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1114
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 1115
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c1

    .line 21786
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/w;

    .line 22306
    iget-object v1, v1, Landroid/support/design/widget/w;->l:Landroid/graphics/Rect;

    invoke-virtual {v1, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1121
    add-int/lit8 v1, v5, 0x1

    move v3, v1

    :goto_93
    if-ge v3, v7, :cond_c1

    .line 1122
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1123
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/design/widget/w;

    .line 23281
    iget-object v8, v2, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 1126
    if-eqz v8, :cond_b5

    invoke-virtual {v8, v0}, Landroid/support/design/widget/t;->b(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_b5

    .line 1127
    if-nez p1, :cond_b9

    .line 23384
    iget-boolean v9, v2, Landroid/support/design/widget/w;->k:Z

    .line 1127
    if-eqz v9, :cond_b9

    .line 23392
    iput-boolean v4, v2, Landroid/support/design/widget/w;->k:Z

    .line 1121
    :cond_b5
    :goto_b5
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_93

    .line 1134
    :cond_b9
    invoke-virtual {v8, p0, v1, v0}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z

    .line 1136
    if-eqz p1, :cond_b5

    .line 24388
    iput-boolean v4, v2, Landroid/support/design/widget/w;->k:Z

    goto :goto_b5

    .line 1097
    :cond_c1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto/16 :goto_c

    .line 1144
    :cond_c6
    return-void
.end method

.method public final a(Landroid/view/View;II)Z
    .registers 5

    .prologue
    .line 1346
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 28579
    invoke-static {p0, p1, v0}, Landroid/support/design/widget/cz;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1348
    invoke-virtual {v0, p2, p3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3

    .prologue
    .line 1394
    instance-of v0, p1, Landroid/support/design/widget/w;

    if-eqz v0, :cond_c

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .registers 6

    .prologue
    .line 1063
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 1074
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    return v0
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    .prologue
    .line 38389
    new-instance v0, Landroid/support/design/widget/w;

    invoke-direct {v0}, Landroid/support/design/widget/w;-><init>()V

    .line 86
    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4

    .prologue
    .line 86
    .line 40374
    new-instance v0, Landroid/support/design/widget/w;

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/design/widget/w;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 86
    return-object v0
.end method

.method protected synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .prologue
    .line 86
    .line 39379
    instance-of v0, p1, Landroid/support/design/widget/w;

    if-eqz v0, :cond_c

    .line 39380
    new-instance v0, Landroid/support/design/widget/w;

    check-cast p1, Landroid/support/design/widget/w;

    invoke-direct {v0, p1}, Landroid/support/design/widget/w;-><init>(Landroid/support/design/widget/w;)V

    .line 39382
    :goto_b
    return-object v0

    .line 39381
    :cond_c
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_18

    .line 39382
    new-instance v0, Landroid/support/design/widget/w;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/support/design/widget/w;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_b

    .line 39384
    :cond_18
    new-instance v0, Landroid/support/design/widget/w;

    invoke-direct {v0, p1}, Landroid/support/design/widget/w;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_b
.end method

.method public getNestedScrollAxes()I
    .registers 2

    .prologue
    .line 1564
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->C:Landroid/support/v4/view/bw;

    .line 37069
    iget v0, v0, Landroid/support/v4/view/bw;->a:I

    .line 1564
    return v0
.end method

.method public getStatusBarBackground()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 247
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->A:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method protected getSuggestedMinimumHeight()I
    .registers 4

    .prologue
    .line 589
    invoke-super {p0}, Landroid/view/ViewGroup;->getSuggestedMinimumHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected getSuggestedMinimumWidth()I
    .registers 4

    .prologue
    .line 584
    invoke-super {p0}, Landroid/view/ViewGroup;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public onAttachedToWindow()V
    .registers 3

    .prologue
    .line 199
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 200
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout;->a()V

    .line 201
    iget-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->x:Z

    if-eqz v0, :cond_1e

    .line 202
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    if-nez v0, :cond_15

    .line 203
    new-instance v0, Landroid/support/design/widget/x;

    invoke-direct {v0, p0}, Landroid/support/design/widget/x;-><init>(Landroid/support/design/widget/CoordinatorLayout;)V

    iput-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    .line 205
    :cond_15
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 206
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 208
    :cond_1e
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    if-nez v0, :cond_2b

    invoke-static {p0}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 211
    invoke-static {p0}, Landroid/support/v4/view/cx;->t(Landroid/view/View;)V

    .line 213
    :cond_2b
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->r:Z

    .line 214
    return-void
.end method

.method public onDetachedFromWindow()V
    .registers 3

    .prologue
    .line 218
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 219
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout;->a()V

    .line 220
    iget-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->x:Z

    if-eqz v0, :cond_17

    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    if-eqz v0, :cond_17

    .line 221
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 222
    iget-object v1, p0, Landroid/support/design/widget/CoordinatorLayout;->w:Landroid/support/design/widget/x;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 224
    :cond_17
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->v:Landroid/view/View;

    if-eqz v0, :cond_20

    .line 225
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->v:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/design/widget/CoordinatorLayout;->onStopNestedScroll(Landroid/view/View;)V

    .line 227
    :cond_20
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->r:Z

    .line 228
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 765
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 766
    iget-boolean v0, p0, Landroid/support/design/widget/CoordinatorLayout;->z:Z

    if-eqz v0, :cond_26

    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->A:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_26

    .line 767
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    if-eqz v0, :cond_27

    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v0}, Landroid/support/v4/view/gh;->b()I

    move-result v0

    .line 768
    :goto_16
    if-lez v0, :cond_26

    .line 769
    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->A:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getWidth()I

    move-result v3

    invoke-virtual {v2, v1, v1, v3, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 770
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->A:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 773
    :cond_26
    return-void

    :cond_27
    move v0, v1

    .line 767
    goto :goto_16
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 5

    .prologue
    .line 402
    invoke-static {p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 405
    if-nez v0, :cond_9

    .line 406
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout;->a()V

    .line 409
    :cond_9
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/MotionEvent;I)Z

    move-result v1

    .line 415
    const/4 v2, 0x1

    if-eq v0, v2, :cond_14

    const/4 v2, 0x3

    if-ne v0, v2, :cond_17

    .line 416
    :cond_14
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout;->a()V

    .line 419
    :cond_17
    return v1
.end method

.method protected onLayout(ZIIII)V
    .registers 11

    .prologue
    .line 750
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v3

    .line 751
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .line 752
    const/4 v0, 0x0

    move v2, v0

    :goto_c
    if-ge v2, v4, :cond_2d

    .line 753
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 754
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/w;

    .line 17281
    iget-object v1, v1, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 757
    if-eqz v1, :cond_26

    invoke-virtual {v1, p0, v0, v3}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z

    move-result v1

    if-nez v1, :cond_29

    .line 758
    :cond_26
    invoke-virtual {p0, v0, v3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;I)V

    .line 752
    :cond_29
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_c

    .line 761
    :cond_2d
    return-void
.end method

.method protected onMeasure(II)V
    .registers 27

    .prologue
    .line 613
    .line 9549
    invoke-virtual/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v5

    .line 9551
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v1, v5, :cond_2c

    const/4 v1, 0x1

    move v3, v1

    .line 9553
    :goto_10
    const/4 v1, 0x0

    move v4, v1

    :goto_12
    if-ge v4, v5, :cond_de

    .line 9554
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 9555
    invoke-static {v6}, Landroid/support/design/widget/CoordinatorLayout;->b(Landroid/view/View;)Landroid/support/design/widget/w;

    move-result-object v7

    .line 11428
    iget v1, v7, Landroid/support/design/widget/w;->f:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2f

    .line 11429
    const/4 v1, 0x0

    iput-object v1, v7, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v1, v7, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 9553
    :cond_28
    :goto_28
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_12

    .line 9551
    :cond_2c
    const/4 v1, 0x0

    move v3, v1

    goto :goto_10

    .line 11433
    :cond_2f
    iget-object v1, v7, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-eqz v1, :cond_40

    .line 11494
    iget-object v1, v7, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    iget v2, v7, Landroid/support/design/widget/w;->f:I

    if-eq v1, v2, :cond_6a

    .line 11495
    const/4 v1, 0x0

    .line 11433
    :goto_3e
    if-nez v1, :cond_28

    .line 12458
    :cond_40
    iget v1, v7, Landroid/support/design/widget/w;->f:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v7, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 12459
    iget-object v1, v7, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-eqz v1, :cond_a8

    .line 12460
    iget-object v1, v7, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 12461
    iget-object v2, v7, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 12462
    :goto_56
    move-object/from16 v0, p0

    if-eq v2, v0, :cond_a5

    if-eqz v2, :cond_a5

    .line 12464
    if-ne v2, v6, :cond_99

    .line 12465
    invoke-virtual/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->isInEditMode()Z

    move-result v1

    if-eqz v1, :cond_91

    .line 12466
    const/4 v1, 0x0

    iput-object v1, v7, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v1, v7, Landroid/support/design/widget/w;->g:Landroid/view/View;

    goto :goto_28

    .line 11498
    :cond_6a
    iget-object v1, v7, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 11499
    iget-object v2, v7, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 11500
    :goto_72
    move-object/from16 v0, p0

    if-eq v2, v0, :cond_8d

    .line 11502
    if-eqz v2, :cond_7a

    if-ne v2, v6, :cond_81

    .line 11503
    :cond_7a
    const/4 v1, 0x0

    iput-object v1, v7, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v1, v7, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 11504
    const/4 v1, 0x0

    goto :goto_3e

    .line 11506
    :cond_81
    instance-of v8, v2, Landroid/view/View;

    if-eqz v8, :cond_88

    move-object v1, v2

    .line 11507
    check-cast v1, Landroid/view/View;

    .line 11501
    :cond_88
    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    goto :goto_72

    .line 11510
    :cond_8d
    iput-object v1, v7, Landroid/support/design/widget/w;->h:Landroid/view/View;

    .line 11511
    const/4 v1, 0x1

    goto :goto_3e

    .line 12469
    :cond_91
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Anchor must not be a descendant of the anchored view"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 12472
    :cond_99
    instance-of v8, v2, Landroid/view/View;

    if-eqz v8, :cond_a0

    move-object v1, v2

    .line 12473
    check-cast v1, Landroid/view/View;

    .line 12463
    :cond_a0
    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    goto :goto_56

    .line 12476
    :cond_a5
    iput-object v1, v7, Landroid/support/design/widget/w;->h:Landroid/view/View;

    goto :goto_28

    .line 12478
    :cond_a8
    invoke-virtual/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->isInEditMode()Z

    move-result v1

    if-eqz v1, :cond_b5

    .line 12479
    const/4 v1, 0x0

    iput-object v1, v7, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v1, v7, Landroid/support/design/widget/w;->g:Landroid/view/View;

    goto/16 :goto_28

    .line 12482
    :cond_b5
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find CoordinatorLayout descendant view with id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, v7, Landroid/support/design/widget/w;->f:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to anchor view "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 9562
    :cond_de
    if-eqz v3, :cond_105

    .line 9563
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 9564
    const/4 v1, 0x0

    :goto_e8
    if-ge v1, v5, :cond_fa

    .line 9565
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 9564
    add-int/lit8 v1, v1, 0x1

    goto :goto_e8

    .line 9567
    :cond_fa
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/design/widget/CoordinatorLayout;->e:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 614
    :cond_105
    invoke-direct/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->c()V

    .line 616
    invoke-virtual/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingLeft()I

    move-result v14

    .line 617
    invoke-virtual/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingTop()I

    move-result v2

    .line 618
    invoke-virtual/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingRight()I

    move-result v15

    .line 619
    invoke-virtual/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->getPaddingBottom()I

    move-result v3

    .line 620
    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v16

    .line 621
    const/4 v1, 0x1

    move/from16 v0, v16

    if-ne v0, v1, :cond_21b

    const/4 v1, 0x1

    move v8, v1

    .line 622
    :goto_123
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v17

    .line 623
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v18

    .line 624
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v19

    .line 625
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v20

    .line 627
    add-int v21, v14, v15

    .line 628
    add-int v22, v2, v3

    .line 629
    invoke-virtual/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->getSuggestedMinimumWidth()I

    move-result v4

    .line 630
    invoke-virtual/range {p0 .. p0}, Landroid/support/design/widget/CoordinatorLayout;->getSuggestedMinimumHeight()I

    move-result v3

    .line 631
    const/4 v2, 0x0

    .line 633
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    if-eqz v1, :cond_21f

    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_21f

    const/4 v1, 0x1

    move v9, v1

    .line 635
    :goto_14e
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v23

    .line 636
    const/4 v1, 0x0

    move v10, v1

    move v11, v2

    move v12, v3

    move v13, v4

    :goto_15b
    move/from16 v0, v23

    if-ge v10, v0, :cond_235

    .line 637
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 638
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Landroid/support/design/widget/w;

    .line 640
    const/4 v5, 0x0

    .line 641
    iget v1, v7, Landroid/support/design/widget/w;->e:I

    if-ltz v1, :cond_1a0

    if-eqz v17, :cond_1a0

    .line 642
    iget v1, v7, Landroid/support/design/widget/w;->e:I

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(I)I

    move-result v1

    .line 643
    iget v2, v7, Landroid/support/design/widget/w;->c:I

    invoke-static {v2}, Landroid/support/design/widget/CoordinatorLayout;->c(I)I

    move-result v2

    move/from16 v0, v16

    invoke-static {v2, v0}, Landroid/support/v4/view/v;->a(II)I

    move-result v2

    and-int/lit8 v2, v2, 0x7

    .line 646
    const/4 v4, 0x3

    if-ne v2, v4, :cond_192

    if-eqz v8, :cond_197

    :cond_192
    const/4 v4, 0x5

    if-ne v2, v4, :cond_223

    if-eqz v8, :cond_223

    .line 648
    :cond_197
    const/4 v2, 0x0

    sub-int v4, v18, v15

    sub-int v1, v4, v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 657
    :cond_1a0
    :goto_1a0
    if-eqz v9, :cond_24c

    invoke-static {v3}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_24c

    .line 660
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v1}, Landroid/support/v4/view/gh;->a()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v2}, Landroid/support/v4/view/gh;->c()I

    move-result v2

    add-int/2addr v1, v2

    .line 662
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v2}, Landroid/support/v4/view/gh;->b()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/design/widget/CoordinatorLayout;->y:Landroid/support/v4/view/gh;

    invoke-virtual {v4}, Landroid/support/v4/view/gh;->d()I

    move-result v4

    add-int/2addr v2, v4

    .line 665
    sub-int v1, v18, v1

    move/from16 v0, v17

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 667
    sub-int v1, v20, v2

    move/from16 v0, v19

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 13281
    :goto_1da
    iget-object v1, v7, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 672
    if-eqz v1, :cond_1e6

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v6}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)Z

    move-result v1

    if-nez v1, :cond_1eb

    .line 674
    :cond_1e6
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;III)V

    .line 678
    :cond_1eb
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int v1, v1, v21

    iget v2, v7, Landroid/support/design/widget/w;->leftMargin:I

    add-int/2addr v1, v2

    iget v2, v7, Landroid/support/design/widget/w;->rightMargin:I

    add-int/2addr v1, v2

    invoke-static {v13, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 681
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int v1, v1, v22

    iget v2, v7, Landroid/support/design/widget/w;->topMargin:I

    add-int/2addr v1, v2

    iget v2, v7, Landroid/support/design/widget/w;->bottomMargin:I

    add-int/2addr v1, v2

    invoke-static {v12, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 683
    invoke-static {v3}, Landroid/support/v4/view/cx;->j(Landroid/view/View;)I

    move-result v1

    invoke-static {v11, v1}, Landroid/support/v4/view/cx;->a(II)I

    move-result v2

    .line 636
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    move v11, v2

    move v12, v4

    move v13, v5

    goto/16 :goto_15b

    .line 621
    :cond_21b
    const/4 v1, 0x0

    move v8, v1

    goto/16 :goto_123

    .line 633
    :cond_21f
    const/4 v1, 0x0

    move v9, v1

    goto/16 :goto_14e

    .line 649
    :cond_223
    const/4 v4, 0x5

    if-ne v2, v4, :cond_228

    if-eqz v8, :cond_22d

    :cond_228
    const/4 v4, 0x3

    if-ne v2, v4, :cond_1a0

    if-eqz v8, :cond_1a0

    .line 651
    :cond_22d
    const/4 v2, 0x0

    sub-int/2addr v1, v14

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    goto/16 :goto_1a0

    .line 687
    :cond_235
    const/high16 v1, -0x1000000

    and-int/2addr v1, v11

    move/from16 v0, p1

    invoke-static {v13, v0, v1}, Landroid/support/v4/view/cx;->a(III)I

    move-result v1

    .line 689
    shl-int/lit8 v2, v11, 0x10

    move/from16 v0, p2

    invoke-static {v12, v0, v2}, Landroid/support/v4/view/cx;->a(III)I

    move-result v2

    .line 691
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/CoordinatorLayout;->setMeasuredDimension(II)V

    .line 692
    return-void

    :cond_24c
    move/from16 v6, p2

    move/from16 v4, p1

    goto :goto_1da
.end method

.method public onNestedFling(Landroid/view/View;FFZ)Z
    .registers 11

    .prologue
    const/4 v0, 0x0

    .line 1522
    .line 1524
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v3

    move v2, v0

    move v1, v0

    .line 1525
    :goto_7
    if-ge v2, v3, :cond_25

    .line 1526
    invoke-virtual {p0, v2}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1527
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 35380
    iget-boolean v5, v0, Landroid/support/design/widget/w;->j:Z

    .line 1528
    if-eqz v5, :cond_2c

    .line 36281
    iget-object v0, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 1533
    if-eqz v0, :cond_2c

    .line 1534
    invoke-virtual {v0, p0, v4, p3, p4}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;FZ)Z

    move-result v0

    or-int/2addr v0, v1

    .line 1525
    :goto_20
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_7

    .line 1538
    :cond_25
    if-eqz v1, :cond_2b

    .line 1539
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/design/widget/CoordinatorLayout;->a(Z)V

    .line 1541
    :cond_2b
    return v1

    :cond_2c
    move v0, v1

    goto :goto_20
.end method

.method public onNestedPreFling(Landroid/view/View;FF)Z
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 1547
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v2

    move v0, v1

    .line 1548
    :goto_6
    if-ge v0, v2, :cond_12

    .line 1549
    invoke-virtual {p0, v0}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1550
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 1548
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1560
    :cond_12
    return v1
.end method

.method public onNestedPreScroll(Landroid/view/View;II[I)V
    .registers 15

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1487
    .line 1491
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v7

    move v5, v6

    move v2, v6

    move v3, v6

    move v4, v6

    .line 1492
    :goto_a
    if-ge v5, v7, :cond_5b

    .line 1493
    invoke-virtual {p0, v5}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1494
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 34380
    iget-boolean v9, v0, Landroid/support/design/widget/w;->j:Z

    .line 1495
    if-eqz v9, :cond_65

    .line 35281
    iget-object v0, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 1500
    if-eqz v0, :cond_65

    .line 1501
    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->p:[I

    iget-object v9, p0, Landroid/support/design/widget/CoordinatorLayout;->p:[I

    aput v6, v9, v1

    aput v6, v2, v6

    .line 1502
    iget-object v2, p0, Landroid/support/design/widget/CoordinatorLayout;->p:[I

    invoke-virtual {v0, p0, v8, p3, v2}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I[I)V

    .line 1504
    if-lez p2, :cond_49

    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->p:[I

    aget v0, v0, v6

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1506
    :goto_35
    if-lez p3, :cond_52

    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->p:[I

    aget v0, v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_3f
    move v3, v2

    move v2, v0

    move v0, v1

    .line 1492
    :goto_42
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    move v3, v2

    move v2, v0

    goto :goto_a

    .line 1504
    :cond_49
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->p:[I

    aget v0, v0, v6

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_35

    .line 1506
    :cond_52
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->p:[I

    aget v0, v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_3f

    .line 1513
    :cond_5b
    aput v4, p4, v6

    .line 1514
    aput v3, p4, v1

    .line 1516
    if-eqz v2, :cond_64

    .line 1517
    invoke-virtual {p0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Z)V

    .line 1519
    :cond_64
    return-void

    :cond_65
    move v0, v2

    move v2, v3

    move v3, v4

    goto :goto_42
.end method

.method public onNestedScroll(Landroid/view/View;IIII)V
    .registers 13

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1463
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v4

    move v3, v0

    move v2, v0

    .line 1466
    :goto_8
    if-ge v3, v4, :cond_25

    .line 1467
    invoke-virtual {p0, v3}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1468
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 33380
    iget-boolean v6, v0, Landroid/support/design/widget/w;->j:Z

    .line 1469
    if-eqz v6, :cond_2b

    .line 34281
    iget-object v0, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 1474
    if-eqz v0, :cond_2b

    .line 1475
    invoke-virtual {v0, p0, v5, p5}, Landroid/support/design/widget/t;->b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)V

    move v0, v1

    .line 1466
    :goto_20
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_8

    .line 1481
    :cond_25
    if-eqz v2, :cond_2a

    .line 1482
    invoke-virtual {p0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Z)V

    .line 1484
    :cond_2a
    return-void

    :cond_2b
    move v0, v2

    goto :goto_20
.end method

.method public onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
    .registers 7

    .prologue
    .line 1419
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->C:Landroid/support/v4/view/bw;

    .line 31058
    iput p3, v0, Landroid/support/v4/view/bw;->a:I

    .line 1420
    iput-object p1, p0, Landroid/support/design/widget/CoordinatorLayout;->u:Landroid/view/View;

    .line 1421
    iput-object p2, p0, Landroid/support/design/widget/CoordinatorLayout;->v:Landroid/view/View;

    .line 1423
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v1

    .line 1424
    const/4 v0, 0x0

    :goto_d
    if-ge v0, v1, :cond_19

    .line 1425
    invoke-virtual {p0, v0}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1426
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 1424
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 1436
    :cond_19
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 9

    .prologue
    .line 2543
    check-cast p1, Landroid/support/design/widget/CoordinatorLayout$SavedState;

    .line 2544
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2546
    iget-object v2, p1, Landroid/support/design/widget/CoordinatorLayout$SavedState;->a:Landroid/util/SparseArray;

    .line 2548
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v3

    move v1, v0

    :goto_11
    if-ge v1, v3, :cond_35

    .line 2549
    invoke-virtual {p0, v1}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2550
    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v0

    .line 2551
    invoke-static {v4}, Landroid/support/design/widget/CoordinatorLayout;->b(Landroid/view/View;)Landroid/support/design/widget/w;

    move-result-object v5

    .line 37281
    iget-object v5, v5, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 2554
    const/4 v6, -0x1

    if-eq v0, v6, :cond_31

    if-eqz v5, :cond_31

    .line 2555
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 2556
    if-eqz v0, :cond_31

    .line 2557
    invoke-virtual {v5, p0, v4, v0}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V

    .line 2548
    :cond_31
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_11

    .line 2561
    :cond_35
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 9

    .prologue
    .line 2565
    new-instance v2, Landroid/support/design/widget/CoordinatorLayout$SavedState;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/support/design/widget/CoordinatorLayout$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2567
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    .line 2568
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v4

    move v1, v0

    :goto_14
    if-ge v1, v4, :cond_38

    .line 2569
    invoke-virtual {p0, v1}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 2570
    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v6

    .line 2571
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 38281
    iget-object v0, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 2574
    const/4 v7, -0x1

    if-eq v6, v7, :cond_34

    if-eqz v0, :cond_34

    .line 2576
    invoke-virtual {v0, p0, v5}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;

    move-result-object v0

    .line 2577
    if-eqz v0, :cond_34

    .line 2578
    invoke-virtual {v3, v6, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 2568
    :cond_34
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_14

    .line 2582
    :cond_38
    iput-object v3, v2, Landroid/support/design/widget/CoordinatorLayout$SavedState;->a:Landroid/util/SparseArray;

    .line 2583
    return-object v2
.end method

.method public onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
    .registers 11

    .prologue
    const/4 v3, 0x0

    .line 1398
    .line 1400
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v4

    move v2, v3

    move v1, v3

    .line 1401
    :goto_7
    if-ge v2, v4, :cond_28

    .line 1402
    invoke-virtual {p0, v2}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1403
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 29281
    iget-object v6, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 1405
    if-eqz v6, :cond_24

    .line 1406
    invoke-virtual {v6, p0, v5, p1, p3}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;I)Z

    move-result v5

    .line 1408
    or-int/2addr v1, v5

    .line 29376
    iput-boolean v5, v0, Landroid/support/design/widget/w;->j:Z

    move v0, v1

    .line 1401
    :goto_1f
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_7

    .line 30376
    :cond_24
    iput-boolean v3, v0, Landroid/support/design/widget/w;->j:Z

    move v0, v1

    goto :goto_1f

    .line 1415
    :cond_28
    return v1
.end method

.method public onStopNestedScroll(Landroid/view/View;)V
    .registers 8

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1439
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->C:Landroid/support/v4/view/bw;

    .line 31082
    iput v2, v0, Landroid/support/v4/view/bw;->a:I

    .line 1441
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v3

    move v1, v2

    .line 1442
    :goto_b
    if-ge v1, v3, :cond_2a

    .line 1443
    invoke-virtual {p0, v1}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1444
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 31380
    iget-boolean v4, v0, Landroid/support/design/widget/w;->j:Z

    .line 1445
    if-eqz v4, :cond_26

    .line 32281
    iget-object v4, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 1450
    if-eqz v4, :cond_22

    .line 1451
    invoke-virtual {v4, p1}, Landroid/support/design/widget/t;->a(Landroid/view/View;)V

    .line 32372
    :cond_22
    iput-boolean v2, v0, Landroid/support/design/widget/w;->j:Z

    .line 32392
    iput-boolean v2, v0, Landroid/support/design/widget/w;->k:Z

    .line 1442
    :cond_26
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 1457
    :cond_2a
    iput-object v5, p0, Landroid/support/design/widget/CoordinatorLayout;->u:Landroid/view/View;

    .line 1458
    iput-object v5, p0, Landroid/support/design/widget/CoordinatorLayout;->v:Landroid/view/View;

    .line 1459
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 13

    .prologue
    const/4 v4, 0x3

    const/4 v10, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 424
    .line 426
    const/4 v2, 0x0

    .line 428
    invoke-static {p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v9

    .line 430
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->t:Landroid/view/View;

    if-nez v0, :cond_55

    invoke-direct {p0, p1, v10}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/MotionEvent;I)Z

    move-result v0

    if-eqz v0, :cond_52

    move v1, v0

    .line 433
    :goto_14
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 7281
    iget-object v0, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 435
    if-eqz v0, :cond_50

    .line 436
    iget-object v3, p0, Landroid/support/design/widget/CoordinatorLayout;->t:Landroid/view/View;

    invoke-virtual {v0, p0, v3, p1}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    move v8, v0

    .line 441
    :goto_27
    iget-object v0, p0, Landroid/support/design/widget/CoordinatorLayout;->t:Landroid/view/View;

    if-nez v0, :cond_3e

    .line 442
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/2addr v8, v0

    move-object v0, v2

    .line 456
    :goto_31
    if-eqz v0, :cond_36

    .line 457
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 460
    :cond_36
    if-eq v9, v10, :cond_3a

    if-ne v9, v4, :cond_3d

    .line 461
    :cond_3a
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout;->a()V

    .line 464
    :cond_3d
    return v8

    .line 443
    :cond_3e
    if-eqz v1, :cond_4e

    .line 445
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    move-wide v2, v0

    move v6, v5

    .line 446
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 449
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_31

    :cond_4e
    move-object v0, v2

    goto :goto_31

    :cond_50
    move v8, v7

    goto :goto_27

    :cond_52
    move v1, v0

    move v8, v7

    goto :goto_27

    :cond_55
    move v1, v7

    goto :goto_14
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .registers 2

    .prologue
    .line 469
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 470
    if-eqz p1, :cond_8

    .line 471
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout;->a()V

    .line 473
    :cond_8
    return-void
.end method

.method public setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V
    .registers 2

    .prologue
    .line 194
    iput-object p1, p0, Landroid/support/design/widget/CoordinatorLayout;->B:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    .line 195
    return-void
.end method

.method public setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 237
    iput-object p1, p0, Landroid/support/design/widget/CoordinatorLayout;->A:Landroid/graphics/drawable/Drawable;

    .line 238
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->invalidate()V

    .line 239
    return-void
.end method

.method public setStatusBarBackgroundColor(I)V
    .registers 3

    .prologue
    .line 268
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Landroid/support/design/widget/CoordinatorLayout;->setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 269
    return-void
.end method

.method public setStatusBarBackgroundResource(I)V
    .registers 3

    .prologue
    .line 257
    if-eqz p1, :cond_e

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/c/h;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_a
    invoke-virtual {p0, v0}, Landroid/support/design/widget/CoordinatorLayout;->setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 258
    return-void

    .line 257
    :cond_e
    const/4 v0, 0x0

    goto :goto_a
.end method
