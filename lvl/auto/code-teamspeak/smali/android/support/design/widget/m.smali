.class public final Landroid/support/design/widget/m;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# static fields
.field private static final a:I = 0x258


# instance fields
.field private b:Z

.field private c:I

.field private d:Landroid/support/v7/widget/Toolbar;

.field private e:Landroid/view/View;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private final j:Landroid/graphics/Rect;

.field private final k:Landroid/support/design/widget/l;

.field private l:Z

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:I

.field private p:Z

.field private q:Landroid/support/design/widget/ck;

.field private r:Landroid/support/design/widget/i;

.field private s:I

.field private t:Landroid/support/v4/view/gh;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 119
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/m;-><init>(Landroid/content/Context;B)V

    .line 120
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4

    .prologue
    .line 123
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/m;-><init>(Landroid/content/Context;C)V

    .line 124
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .registers 9

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 127
    invoke-direct {p0, p1, v4, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 92
    iput-boolean v1, p0, Landroid/support/design/widget/m;->b:Z

    .line 102
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/m;->j:Landroid/graphics/Rect;

    .line 129
    new-instance v0, Landroid/support/design/widget/l;

    invoke-direct {v0, p0}, Landroid/support/design/widget/l;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    .line 130
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    sget-object v3, Landroid/support/design/widget/a;->c:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v3}, Landroid/support/design/widget/l;->a(Landroid/view/animation/Interpolator;)V

    .line 132
    sget-object v0, Landroid/support/design/n;->CollapsingToolbarLayout:[I

    sget v3, Landroid/support/design/m;->Widget_Design_CollapsingToolbar:I

    invoke-virtual {p1, v4, v0, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 136
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    sget v4, Landroid/support/design/n;->CollapsingToolbarLayout_expandedTitleGravity:I

    const v5, 0x800053

    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/support/design/widget/l;->c(I)V

    .line 139
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    sget v4, Landroid/support/design/n;->CollapsingToolbarLayout_collapsedTitleGravity:I

    const v5, 0x800013

    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/support/design/widget/l;->d(I)V

    .line 143
    sget v0, Landroid/support/design/n;->CollapsingToolbarLayout_expandedTitleMargin:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/m;->i:I

    iput v0, p0, Landroid/support/design/widget/m;->h:I

    iput v0, p0, Landroid/support/design/widget/m;->g:I

    iput v0, p0, Landroid/support/design/widget/m;->f:I

    .line 146
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    if-ne v0, v1, :cond_109

    move v0, v1

    .line 148
    :goto_56
    sget v4, Landroid/support/design/n;->CollapsingToolbarLayout_expandedTitleMarginStart:I

    invoke-virtual {v3, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v4

    if-eqz v4, :cond_68

    .line 149
    sget v4, Landroid/support/design/n;->CollapsingToolbarLayout_expandedTitleMarginStart:I

    invoke-virtual {v3, v4, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 151
    if-eqz v0, :cond_10c

    .line 152
    iput v4, p0, Landroid/support/design/widget/m;->h:I

    .line 157
    :cond_68
    :goto_68
    sget v4, Landroid/support/design/n;->CollapsingToolbarLayout_expandedTitleMarginEnd:I

    invoke-virtual {v3, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v4

    if-eqz v4, :cond_7a

    .line 158
    sget v4, Landroid/support/design/n;->CollapsingToolbarLayout_expandedTitleMarginEnd:I

    invoke-virtual {v3, v4, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 160
    if-eqz v0, :cond_110

    .line 161
    iput v4, p0, Landroid/support/design/widget/m;->f:I

    .line 166
    :cond_7a
    :goto_7a
    sget v0, Landroid/support/design/n;->CollapsingToolbarLayout_expandedTitleMarginTop:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_8a

    .line 167
    sget v0, Landroid/support/design/n;->CollapsingToolbarLayout_expandedTitleMarginTop:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/m;->g:I

    .line 170
    :cond_8a
    sget v0, Landroid/support/design/n;->CollapsingToolbarLayout_expandedTitleMarginBottom:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_9a

    .line 171
    sget v0, Landroid/support/design/n;->CollapsingToolbarLayout_expandedTitleMarginBottom:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/m;->i:I

    .line 175
    :cond_9a
    sget v0, Landroid/support/design/n;->CollapsingToolbarLayout_titleEnabled:I

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/design/widget/m;->l:Z

    .line 177
    sget v0, Landroid/support/design/n;->CollapsingToolbarLayout_title:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/m;->setTitle(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    sget v1, Landroid/support/design/m;->TextAppearance_Design_CollapsingToolbar_Expanded:I

    invoke-virtual {v0, v1}, Landroid/support/design/widget/l;->f(I)V

    .line 182
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    sget v1, Landroid/support/design/m;->TextAppearance_AppCompat_Widget_ActionBar_Title:I

    invoke-virtual {v0, v1}, Landroid/support/design/widget/l;->e(I)V

    .line 186
    sget v0, Landroid/support/design/n;->CollapsingToolbarLayout_expandedTitleTextAppearance:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_cc

    .line 187
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    sget v1, Landroid/support/design/n;->CollapsingToolbarLayout_expandedTitleTextAppearance:I

    invoke-virtual {v3, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/l;->f(I)V

    .line 191
    :cond_cc
    sget v0, Landroid/support/design/n;->CollapsingToolbarLayout_collapsedTitleTextAppearance:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_df

    .line 192
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    sget v1, Landroid/support/design/n;->CollapsingToolbarLayout_collapsedTitleTextAppearance:I

    invoke-virtual {v3, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/l;->e(I)V

    .line 198
    :cond_df
    sget v0, Landroid/support/design/n;->CollapsingToolbarLayout_contentScrim:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/m;->setContentScrim(Landroid/graphics/drawable/Drawable;)V

    .line 199
    sget v0, Landroid/support/design/n;->CollapsingToolbarLayout_statusBarScrim:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/m;->setStatusBarScrim(Landroid/graphics/drawable/Drawable;)V

    .line 201
    sget v0, Landroid/support/design/n;->CollapsingToolbarLayout_toolbarId:I

    const/4 v1, -0x1

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/m;->c:I

    .line 203
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 205
    invoke-virtual {p0, v2}, Landroid/support/design/widget/m;->setWillNotDraw(Z)V

    .line 207
    new-instance v0, Landroid/support/design/widget/n;

    invoke-direct {v0, p0}, Landroid/support/design/widget/n;-><init>(Landroid/support/design/widget/m;)V

    invoke-static {p0, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/bx;)V

    .line 217
    return-void

    :cond_109
    move v0, v2

    .line 146
    goto/16 :goto_56

    .line 154
    :cond_10c
    iput v4, p0, Landroid/support/design/widget/m;->f:I

    goto/16 :goto_68

    .line 163
    :cond_110
    iput v4, p0, Landroid/support/design/widget/m;->h:I

    goto/16 :goto_7a
.end method

.method static synthetic a(Landroid/view/View;)Landroid/support/design/widget/dg;
    .registers 2

    .prologue
    .line 88
    invoke-static {p0}, Landroid/support/design/widget/m;->b(Landroid/view/View;)Landroid/support/design/widget/dg;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/support/design/widget/m;)Landroid/support/v4/view/gh;
    .registers 2

    .prologue
    .line 88
    iget-object v0, p0, Landroid/support/design/widget/m;->t:Landroid/support/v4/view/gh;

    return-object v0
.end method

.method static synthetic a(Landroid/support/design/widget/m;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
    .registers 2

    .prologue
    .line 88
    iput-object p1, p0, Landroid/support/design/widget/m;->t:Landroid/support/v4/view/gh;

    return-object p1
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/FrameLayout$LayoutParams;
    .registers 2

    .prologue
    .line 738
    new-instance v0, Landroid/support/design/widget/p;

    invoke-direct {v0, p0}, Landroid/support/design/widget/p;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method private a()V
    .registers 9

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 297
    iget-boolean v0, p0, Landroid/support/design/widget/m;->b:Z

    if-nez v0, :cond_7

    .line 333
    :goto_6
    return-void

    .line 303
    :cond_7
    invoke-virtual {p0}, Landroid/support/design/widget/m;->getChildCount()I

    move-result v5

    move v3, v4

    move-object v1, v2

    :goto_d
    if-ge v3, v5, :cond_40

    .line 304
    invoke-virtual {p0, v3}, Landroid/support/design/widget/m;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 305
    instance-of v6, v0, Landroid/support/v7/widget/Toolbar;

    if-eqz v6, :cond_3e

    .line 306
    iget v6, p0, Landroid/support/design/widget/m;->c:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_39

    .line 308
    iget v6, p0, Landroid/support/design/widget/m;->c:I

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v7

    if-ne v6, v7, :cond_30

    .line 310
    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 325
    :goto_26
    if-nez v0, :cond_3c

    .line 330
    :goto_28
    iput-object v1, p0, Landroid/support/design/widget/m;->d:Landroid/support/v7/widget/Toolbar;

    .line 331
    invoke-direct {p0}, Landroid/support/design/widget/m;->b()V

    .line 332
    iput-boolean v4, p0, Landroid/support/design/widget/m;->b:Z

    goto :goto_6

    .line 313
    :cond_30
    if-nez v1, :cond_3e

    .line 315
    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 303
    :goto_34
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    goto :goto_d

    .line 319
    :cond_39
    check-cast v0, Landroid/support/v7/widget/Toolbar;

    goto :goto_26

    :cond_3c
    move-object v1, v0

    goto :goto_28

    :cond_3e
    move-object v0, v1

    goto :goto_34

    :cond_40
    move-object v0, v2

    goto :goto_26
.end method

.method private a(I)V
    .registers 4

    .prologue
    .line 488
    invoke-direct {p0}, Landroid/support/design/widget/m;->a()V

    .line 489
    iget-object v0, p0, Landroid/support/design/widget/m;->q:Landroid/support/design/widget/ck;

    if-nez v0, :cond_34

    .line 490
    invoke-static {}, Landroid/support/design/widget/dh;->a()Landroid/support/design/widget/ck;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/m;->q:Landroid/support/design/widget/ck;

    .line 491
    iget-object v0, p0, Landroid/support/design/widget/m;->q:Landroid/support/design/widget/ck;

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Landroid/support/design/widget/ck;->a(I)V

    .line 492
    iget-object v0, p0, Landroid/support/design/widget/m;->q:Landroid/support/design/widget/ck;

    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/ck;->a(Landroid/view/animation/Interpolator;)V

    .line 493
    iget-object v0, p0, Landroid/support/design/widget/m;->q:Landroid/support/design/widget/ck;

    new-instance v1, Landroid/support/design/widget/o;

    invoke-direct {v1, p0}, Landroid/support/design/widget/o;-><init>(Landroid/support/design/widget/m;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/ck;->a(Landroid/support/design/widget/cp;)V

    .line 503
    :cond_25
    :goto_25
    iget-object v0, p0, Landroid/support/design/widget/m;->q:Landroid/support/design/widget/ck;

    iget v1, p0, Landroid/support/design/widget/m;->o:I

    invoke-virtual {v0, v1, p1}, Landroid/support/design/widget/ck;->a(II)V

    .line 504
    iget-object v0, p0, Landroid/support/design/widget/m;->q:Landroid/support/design/widget/ck;

    .line 4116
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->a()V

    .line 505
    return-void

    .line 499
    :cond_34
    iget-object v0, p0, Landroid/support/design/widget/m;->q:Landroid/support/design/widget/ck;

    .line 3120
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->b()Z

    move-result v0

    .line 499
    if-eqz v0, :cond_25

    .line 500
    iget-object v0, p0, Landroid/support/design/widget/m;->q:Landroid/support/design/widget/ck;

    .line 3184
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->e()V

    goto :goto_25
.end method

.method static synthetic a(Landroid/support/design/widget/m;I)V
    .registers 2

    .prologue
    .line 88
    invoke-direct {p0, p1}, Landroid/support/design/widget/m;->setScrimAlpha(I)V

    return-void
.end method

.method static synthetic b(Landroid/support/design/widget/m;I)I
    .registers 2

    .prologue
    .line 88
    iput p1, p0, Landroid/support/design/widget/m;->s:I

    return p1
.end method

.method static synthetic b(Landroid/support/design/widget/m;)Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 88
    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private static b(Landroid/view/View;)Landroid/support/design/widget/dg;
    .registers 3

    .prologue
    .line 405
    sget v0, Landroid/support/design/i;->view_offset_helper:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/dg;

    .line 406
    if-nez v0, :cond_14

    .line 407
    new-instance v0, Landroid/support/design/widget/dg;

    invoke-direct {v0, p0}, Landroid/support/design/widget/dg;-><init>(Landroid/view/View;)V

    .line 408
    sget v1, Landroid/support/design/i;->view_offset_helper:I

    invoke-virtual {p0, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 410
    :cond_14
    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    const/4 v2, -0x1

    .line 336
    iget-boolean v0, p0, Landroid/support/design/widget/m;->l:Z

    if-nez v0, :cond_1a

    iget-object v0, p0, Landroid/support/design/widget/m;->e:Landroid/view/View;

    if-eqz v0, :cond_1a

    .line 338
    iget-object v0, p0, Landroid/support/design/widget/m;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 339
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1a

    .line 340
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/design/widget/m;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 343
    :cond_1a
    iget-boolean v0, p0, Landroid/support/design/widget/m;->l:Z

    if-eqz v0, :cond_40

    iget-object v0, p0, Landroid/support/design/widget/m;->d:Landroid/support/v7/widget/Toolbar;

    if-eqz v0, :cond_40

    .line 344
    iget-object v0, p0, Landroid/support/design/widget/m;->e:Landroid/view/View;

    if-nez v0, :cond_31

    .line 345
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/design/widget/m;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/design/widget/m;->e:Landroid/view/View;

    .line 347
    :cond_31
    iget-object v0, p0, Landroid/support/design/widget/m;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_40

    .line 348
    iget-object v0, p0, Landroid/support/design/widget/m;->d:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/design/widget/m;->e:Landroid/view/View;

    invoke-virtual {v0, v1, v2, v2}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;II)V

    .line 351
    :cond_40
    return-void
.end method

.method static synthetic c(Landroid/support/design/widget/m;)Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 88
    iget-object v0, p0, Landroid/support/design/widget/m;->n:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 462
    iget-boolean v0, p0, Landroid/support/design/widget/m;->l:Z

    return v0
.end method

.method private d()V
    .registers 3

    .prologue
    const/16 v1, 0xff

    .line 466
    iget-boolean v0, p0, Landroid/support/design/widget/m;->p:Z

    if-nez v0, :cond_18

    .line 467
    invoke-static {p0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-virtual {p0}, Landroid/support/design/widget/m;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_19

    .line 468
    invoke-direct {p0, v1}, Landroid/support/design/widget/m;->a(I)V

    .line 472
    :goto_15
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/m;->p:Z

    .line 474
    :cond_18
    return-void

    .line 470
    :cond_19
    invoke-direct {p0, v1}, Landroid/support/design/widget/m;->setScrimAlpha(I)V

    goto :goto_15
.end method

.method static synthetic d(Landroid/support/design/widget/m;)V
    .registers 3

    .prologue
    const/16 v1, 0xff

    .line 88
    .line 6466
    iget-boolean v0, p0, Landroid/support/design/widget/m;->p:Z

    if-nez v0, :cond_18

    .line 6467
    invoke-static {p0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-virtual {p0}, Landroid/support/design/widget/m;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_19

    .line 6468
    invoke-direct {p0, v1}, Landroid/support/design/widget/m;->a(I)V

    .line 6472
    :goto_15
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/m;->p:Z

    .line 88
    :cond_18
    return-void

    .line 6470
    :cond_19
    invoke-direct {p0, v1}, Landroid/support/design/widget/m;->setScrimAlpha(I)V

    goto :goto_15
.end method

.method private e()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 477
    iget-boolean v0, p0, Landroid/support/design/widget/m;->p:Z

    if-eqz v0, :cond_16

    .line 478
    invoke-static {p0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-virtual {p0}, Landroid/support/design/widget/m;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_17

    .line 479
    invoke-direct {p0, v1}, Landroid/support/design/widget/m;->a(I)V

    .line 483
    :goto_14
    iput-boolean v1, p0, Landroid/support/design/widget/m;->p:Z

    .line 485
    :cond_16
    return-void

    .line 481
    :cond_17
    invoke-direct {p0, v1}, Landroid/support/design/widget/m;->setScrimAlpha(I)V

    goto :goto_14
.end method

.method static synthetic e(Landroid/support/design/widget/m;)V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 88
    .line 6477
    iget-boolean v0, p0, Landroid/support/design/widget/m;->p:Z

    if-eqz v0, :cond_16

    .line 6478
    invoke-static {p0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-virtual {p0}, Landroid/support/design/widget/m;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_17

    .line 6479
    invoke-direct {p0, v1}, Landroid/support/design/widget/m;->a(I)V

    .line 6483
    :goto_14
    iput-boolean v1, p0, Landroid/support/design/widget/m;->p:Z

    .line 88
    :cond_16
    return-void

    .line 6481
    :cond_17
    invoke-direct {p0, v1}, Landroid/support/design/widget/m;->setScrimAlpha(I)V

    goto :goto_14
.end method

.method static synthetic f(Landroid/support/design/widget/m;)Landroid/support/design/widget/l;
    .registers 2

    .prologue
    .line 88
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    return-object v0
.end method

.method private f()Landroid/support/design/widget/p;
    .registers 3

    .prologue
    .line 728
    new-instance v0, Landroid/support/design/widget/p;

    invoke-super {p0}, Landroid/widget/FrameLayout;->generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/design/widget/p;-><init>(Landroid/widget/FrameLayout$LayoutParams;)V

    return-object v0
.end method

.method private setScrimAlpha(I)V
    .registers 3

    .prologue
    .line 508
    iget v0, p0, Landroid/support/design/widget/m;->o:I

    if-eq p1, v0, :cond_16

    .line 509
    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    .line 510
    if-eqz v0, :cond_11

    iget-object v0, p0, Landroid/support/design/widget/m;->d:Landroid/support/v7/widget/Toolbar;

    if-eqz v0, :cond_11

    .line 511
    iget-object v0, p0, Landroid/support/design/widget/m;->d:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 513
    :cond_11
    iput p1, p0, Landroid/support/design/widget/m;->o:I

    .line 514
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 516
    :cond_16
    return-void
.end method


# virtual methods
.method protected final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3

    .prologue
    .line 723
    instance-of v0, p1, Landroid/support/design/widget/p;

    return v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 246
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 250
    invoke-direct {p0}, Landroid/support/design/widget/m;->a()V

    .line 251
    iget-object v0, p0, Landroid/support/design/widget/m;->d:Landroid/support/v7/widget/Toolbar;

    if-nez v0, :cond_23

    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_23

    iget v0, p0, Landroid/support/design/widget/m;->o:I

    if-lez v0, :cond_23

    .line 252
    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v2, p0, Landroid/support/design/widget/m;->o:I

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 253
    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 257
    :cond_23
    iget-boolean v0, p0, Landroid/support/design/widget/m;->l:Z

    if-eqz v0, :cond_2c

    .line 258
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/l;->a(Landroid/graphics/Canvas;)V

    .line 262
    :cond_2c
    iget-object v0, p0, Landroid/support/design/widget/m;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_5f

    iget v0, p0, Landroid/support/design/widget/m;->o:I

    if-lez v0, :cond_5f

    .line 263
    iget-object v0, p0, Landroid/support/design/widget/m;->t:Landroid/support/v4/view/gh;

    if-eqz v0, :cond_60

    iget-object v0, p0, Landroid/support/design/widget/m;->t:Landroid/support/v4/view/gh;

    invoke-virtual {v0}, Landroid/support/v4/view/gh;->b()I

    move-result v0

    .line 264
    :goto_3e
    if-lez v0, :cond_5f

    .line 265
    iget-object v2, p0, Landroid/support/design/widget/m;->n:Landroid/graphics/drawable/Drawable;

    iget v3, p0, Landroid/support/design/widget/m;->s:I

    neg-int v3, v3

    invoke-virtual {p0}, Landroid/support/design/widget/m;->getWidth()I

    move-result v4

    iget v5, p0, Landroid/support/design/widget/m;->s:I

    sub-int/2addr v0, v5

    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 267
    iget-object v0, p0, Landroid/support/design/widget/m;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Landroid/support/design/widget/m;->o:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 268
    iget-object v0, p0, Landroid/support/design/widget/m;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 271
    :cond_5f
    return-void

    :cond_60
    move v0, v1

    .line 263
    goto :goto_3e
.end method

.method protected final drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .registers 8

    .prologue
    .line 278
    invoke-direct {p0}, Landroid/support/design/widget/m;->a()V

    .line 279
    iget-object v0, p0, Landroid/support/design/widget/m;->d:Landroid/support/v7/widget/Toolbar;

    if-ne p2, v0, :cond_1f

    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1f

    iget v0, p0, Landroid/support/design/widget/m;->o:I

    if-lez v0, :cond_1f

    .line 280
    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Landroid/support/design/widget/m;->o:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 281
    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 285
    :cond_1f
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    return v0
.end method

.method protected final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    .prologue
    .line 88
    invoke-direct {p0}, Landroid/support/design/widget/m;->f()Landroid/support/design/widget/p;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .registers 2

    .prologue
    .line 88
    invoke-direct {p0}, Landroid/support/design/widget/m;->f()Landroid/support/design/widget/p;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .prologue
    .line 88
    invoke-virtual {p0, p1}, Landroid/support/design/widget/m;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .prologue
    .line 5738
    new-instance v0, Landroid/support/design/widget/p;

    invoke-direct {v0, p1}, Landroid/support/design/widget/p;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 88
    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .registers 4

    .prologue
    .line 733
    new-instance v0, Landroid/support/design/widget/p;

    invoke-virtual {p0}, Landroid/support/design/widget/m;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/design/widget/p;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final getCollapsedTitleGravity()I
    .registers 2

    .prologue
    .line 672
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    .line 4185
    iget v0, v0, Landroid/support/design/widget/l;->c:I

    .line 672
    return v0
.end method

.method public final getContentScrim()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 573
    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getExpandedTitleGravity()I
    .registers 2

    .prologue
    .line 711
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    .line 5174
    iget v0, v0, Landroid/support/design/widget/l;->b:I

    .line 711
    return v0
.end method

.method final getScrimTriggerOffset()I
    .registers 2

    .prologue
    .line 718
    invoke-static {p0}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public final getStatusBarScrim()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 633
    iget-object v0, p0, Landroid/support/design/widget/m;->n:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 433
    iget-boolean v0, p0, Landroid/support/design/widget/m;->l:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    .line 2526
    iget-object v0, v0, Landroid/support/design/widget/l;->g:Ljava/lang/CharSequence;

    .line 433
    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method protected final onAttachedToWindow()V
    .registers 4

    .prologue
    .line 221
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 224
    invoke-virtual {p0}, Landroid/support/design/widget/m;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 225
    instance-of v1, v0, Landroid/support/design/widget/AppBarLayout;

    if-eqz v1, :cond_2a

    .line 226
    iget-object v1, p0, Landroid/support/design/widget/m;->r:Landroid/support/design/widget/i;

    if-nez v1, :cond_17

    .line 227
    new-instance v1, Landroid/support/design/widget/q;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Landroid/support/design/widget/q;-><init>(Landroid/support/design/widget/m;B)V

    iput-object v1, p0, Landroid/support/design/widget/m;->r:Landroid/support/design/widget/i;

    .line 229
    :cond_17
    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    iget-object v1, p0, Landroid/support/design/widget/m;->r:Landroid/support/design/widget/i;

    .line 1180
    if-eqz v1, :cond_2a

    iget-object v2, v0, Landroid/support/design/widget/AppBarLayout;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2a

    .line 1181
    iget-object v0, v0, Landroid/support/design/widget/AppBarLayout;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    :cond_2a
    return-void
.end method

.method protected final onDetachedFromWindow()V
    .registers 3

    .prologue
    .line 236
    invoke-virtual {p0}, Landroid/support/design/widget/m;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 237
    iget-object v1, p0, Landroid/support/design/widget/m;->r:Landroid/support/design/widget/i;

    if-eqz v1, :cond_17

    instance-of v1, v0, Landroid/support/design/widget/AppBarLayout;

    if-eqz v1, :cond_17

    .line 238
    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    iget-object v1, p0, Landroid/support/design/widget/m;->r:Landroid/support/design/widget/i;

    .line 1191
    if-eqz v1, :cond_17

    .line 1192
    iget-object v0, v0, Landroid/support/design/widget/AppBarLayout;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 241
    :cond_17
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 242
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .registers 12

    .prologue
    .line 361
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 364
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/design/widget/m;->getChildCount()I

    move-result v1

    :goto_8
    if-ge v0, v1, :cond_31

    .line 365
    invoke-virtual {p0, v0}, Landroid/support/design/widget/m;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 367
    iget-object v3, p0, Landroid/support/design/widget/m;->t:Landroid/support/v4/view/gh;

    if-eqz v3, :cond_27

    invoke-static {v2}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v3

    if-nez v3, :cond_27

    .line 368
    iget-object v3, p0, Landroid/support/design/widget/m;->t:Landroid/support/v4/view/gh;

    invoke-virtual {v3}, Landroid/support/v4/view/gh;->b()I

    move-result v3

    .line 369
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    if-ge v4, v3, :cond_27

    .line 372
    invoke-virtual {v2, v3}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 376
    :cond_27
    invoke-static {v2}, Landroid/support/design/widget/m;->b(Landroid/view/View;)Landroid/support/design/widget/dg;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/design/widget/dg;->a()V

    .line 364
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 380
    :cond_31
    iget-boolean v0, p0, Landroid/support/design/widget/m;->l:Z

    if-eqz v0, :cond_72

    iget-object v0, p0, Landroid/support/design/widget/m;->e:Landroid/view/View;

    if-eqz v0, :cond_72

    .line 381
    iget-object v0, p0, Landroid/support/design/widget/m;->e:Landroid/view/View;

    iget-object v1, p0, Landroid/support/design/widget/m;->j:Landroid/graphics/Rect;

    invoke-static {p0, v0, v1}, Landroid/support/design/widget/cz;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 382
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    iget-object v1, p0, Landroid/support/design/widget/m;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Landroid/support/design/widget/m;->j:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int v2, p5, v2

    iget-object v3, p0, Landroid/support/design/widget/m;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0, v1, v2, v3, p5}, Landroid/support/design/widget/l;->b(IIII)V

    .line 385
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    iget v1, p0, Landroid/support/design/widget/m;->f:I

    iget-object v2, p0, Landroid/support/design/widget/m;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Landroid/support/design/widget/m;->g:I

    add-int/2addr v2, v3

    sub-int v3, p4, p2

    iget v4, p0, Landroid/support/design/widget/m;->h:I

    sub-int/2addr v3, v4

    sub-int v4, p5, p3

    iget v5, p0, Landroid/support/design/widget/m;->i:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/design/widget/l;->a(IIII)V

    .line 391
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    invoke-virtual {v0}, Landroid/support/design/widget/l;->a()V

    .line 395
    :cond_72
    iget-object v0, p0, Landroid/support/design/widget/m;->d:Landroid/support/v7/widget/Toolbar;

    if-eqz v0, :cond_98

    .line 396
    iget-boolean v0, p0, Landroid/support/design/widget/m;->l:Z

    if-eqz v0, :cond_8f

    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    .line 1526
    iget-object v0, v0, Landroid/support/design/widget/l;->g:Ljava/lang/CharSequence;

    .line 396
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 398
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    iget-object v1, p0, Landroid/support/design/widget/m;->d:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/l;->a(Ljava/lang/CharSequence;)V

    .line 400
    :cond_8f
    iget-object v0, p0, Landroid/support/design/widget/m;->d:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/m;->setMinimumHeight(I)V

    .line 402
    :cond_98
    return-void
.end method

.method protected final onMeasure(II)V
    .registers 3

    .prologue
    .line 355
    invoke-direct {p0}, Landroid/support/design/widget/m;->a()V

    .line 356
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 357
    return-void
.end method

.method protected final onSizeChanged(IIII)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 290
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 291
    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_d

    .line 292
    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v1, p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 294
    :cond_d
    return-void
.end method

.method public final setCollapsedTitleGravity(I)V
    .registers 3

    .prologue
    .line 663
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/l;->c(I)V

    .line 664
    return-void
.end method

.method public final setCollapsedTitleTextAppearance(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/ai;
        .end annotation
    .end param

    .prologue
    .line 643
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/l;->e(I)V

    .line 644
    return-void
.end method

.method public final setCollapsedTitleTextColor(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param

    .prologue
    .line 652
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/l;->a(I)V

    .line 653
    return-void
.end method

.method public final setContentScrim(Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 528
    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_2b

    .line 529
    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_f

    .line 530
    iget-object v0, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 533
    :cond_f
    iput-object p1, p0, Landroid/support/design/widget/m;->m:Landroid/graphics/drawable/Drawable;

    .line 534
    invoke-virtual {p0}, Landroid/support/design/widget/m;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/design/widget/m;->getHeight()I

    move-result v1

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 535
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 536
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Landroid/support/design/widget/m;->o:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 537
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 539
    :cond_2b
    return-void
.end method

.method public final setContentScrimColor(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param

    .prologue
    .line 550
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Landroid/support/design/widget/m;->setContentScrim(Landroid/graphics/drawable/Drawable;)V

    .line 551
    return-void
.end method

.method public final setContentScrimResource(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 562
    invoke-virtual {p0}, Landroid/support/design/widget/m;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/c/h;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/m;->setContentScrim(Landroid/graphics/drawable/Drawable;)V

    .line 564
    return-void
.end method

.method public final setExpandedTitleColor(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param

    .prologue
    .line 691
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/l;->b(I)V

    .line 692
    return-void
.end method

.method public final setExpandedTitleGravity(I)V
    .registers 3

    .prologue
    .line 702
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/l;->c(I)V

    .line 703
    return-void
.end method

.method public final setExpandedTitleTextAppearance(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/ai;
        .end annotation
    .end param

    .prologue
    .line 682
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/l;->f(I)V

    .line 683
    return-void
.end method

.method public final setStatusBarScrim(Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 588
    iget-object v0, p0, Landroid/support/design/widget/m;->n:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_1f

    .line 589
    iget-object v0, p0, Landroid/support/design/widget/m;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_e

    .line 590
    iget-object v0, p0, Landroid/support/design/widget/m;->n:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 593
    :cond_e
    iput-object p1, p0, Landroid/support/design/widget/m;->n:Landroid/graphics/drawable/Drawable;

    .line 594
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 595
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Landroid/support/design/widget/m;->o:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 596
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 598
    :cond_1f
    return-void
.end method

.method public final setStatusBarScrimColor(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param

    .prologue
    .line 611
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Landroid/support/design/widget/m;->setStatusBarScrim(Landroid/graphics/drawable/Drawable;)V

    .line 612
    return-void
.end method

.method public final setStatusBarScrimResource(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 623
    invoke-virtual {p0}, Landroid/support/design/widget/m;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/c/h;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/m;->setStatusBarScrim(Landroid/graphics/drawable/Drawable;)V

    .line 624
    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 422
    iget-object v0, p0, Landroid/support/design/widget/m;->k:Landroid/support/design/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/l;->a(Ljava/lang/CharSequence;)V

    .line 423
    return-void
.end method

.method public final setTitleEnabled(Z)V
    .registers 3

    .prologue
    .line 447
    iget-boolean v0, p0, Landroid/support/design/widget/m;->l:Z

    if-eq p1, v0, :cond_c

    .line 448
    iput-boolean p1, p0, Landroid/support/design/widget/m;->l:Z

    .line 449
    invoke-direct {p0}, Landroid/support/design/widget/m;->b()V

    .line 450
    invoke-virtual {p0}, Landroid/support/design/widget/m;->requestLayout()V

    .line 452
    :cond_c
    return-void
.end method
