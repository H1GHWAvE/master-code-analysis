.class final Landroid/support/design/widget/ck;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/support/design/widget/cr;


# direct methods
.method constructor <init>(Landroid/support/design/widget/cr;)V
    .registers 2

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-object p1, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    .line 113
    return-void
.end method

.method private a()V
    .registers 2

    .prologue
    .line 116
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->a()V

    .line 117
    return-void
.end method

.method private a(Landroid/support/design/widget/cn;)V
    .registers 4

    .prologue
    .line 142
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    new-instance v1, Landroid/support/design/widget/cm;

    invoke-direct {v1, p0, p1}, Landroid/support/design/widget/cm;-><init>(Landroid/support/design/widget/ck;Landroid/support/design/widget/cn;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/cr;->a(Landroid/support/design/widget/cs;)V

    .line 161
    return-void
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 120
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->b()Z

    move-result v0

    return v0
.end method

.method private c()I
    .registers 2

    .prologue
    .line 168
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->c()I

    move-result v0

    return v0
.end method

.method private d()F
    .registers 2

    .prologue
    .line 176
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->d()F

    move-result v0

    return v0
.end method

.method private e()V
    .registers 2

    .prologue
    .line 184
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->e()V

    .line 185
    return-void
.end method

.method private f()F
    .registers 2

    .prologue
    .line 188
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->f()F

    move-result v0

    return v0
.end method

.method private g()V
    .registers 2

    .prologue
    .line 192
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->g()V

    .line 193
    return-void
.end method


# virtual methods
.method public final a(FF)V
    .registers 4

    .prologue
    .line 172
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0, p1, p2}, Landroid/support/design/widget/cr;->a(FF)V

    .line 173
    return-void
.end method

.method public final a(I)V
    .registers 3

    .prologue
    .line 180
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/cr;->a(I)V

    .line 181
    return-void
.end method

.method public final a(II)V
    .registers 4

    .prologue
    .line 164
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0, p1, p2}, Landroid/support/design/widget/cr;->a(II)V

    .line 165
    return-void
.end method

.method public final a(Landroid/support/design/widget/cp;)V
    .registers 4

    .prologue
    .line 129
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    new-instance v1, Landroid/support/design/widget/cl;

    invoke-direct {v1, p0, p1}, Landroid/support/design/widget/cl;-><init>(Landroid/support/design/widget/ck;Landroid/support/design/widget/cp;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/cr;->a(Landroid/support/design/widget/ct;)V

    .line 138
    return-void
.end method

.method public final a(Landroid/view/animation/Interpolator;)V
    .registers 3

    .prologue
    .line 124
    iget-object v0, p0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/cr;->a(Landroid/view/animation/Interpolator;)V

    .line 125
    return-void
.end method
