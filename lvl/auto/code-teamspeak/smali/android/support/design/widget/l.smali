.class final Landroid/support/design/widget/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final j:Z

.field private static final k:Z

.field private static final l:Landroid/graphics/Paint;


# instance fields
.field private A:Z

.field private B:Landroid/graphics/Bitmap;

.field private C:Landroid/graphics/Paint;

.field private D:F

.field private E:F

.field private F:F

.field private G:F

.field private H:Z

.field private I:Landroid/view/animation/Interpolator;

.field a:F

.field b:I

.field c:I

.field d:F

.field e:F

.field f:I

.field g:Ljava/lang/CharSequence;

.field final h:Landroid/text/TextPaint;

.field i:Landroid/view/animation/Interpolator;

.field private final m:Landroid/view/View;

.field private n:Z

.field private final o:Landroid/graphics/Rect;

.field private final p:Landroid/graphics/Rect;

.field private final q:Landroid/graphics/RectF;

.field private r:I

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private w:F

.field private x:F

.field private y:Ljava/lang/CharSequence;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 42
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_d

    const/4 v0, 0x1

    :goto_7
    sput-boolean v0, Landroid/support/design/widget/l;->j:Z

    .line 47
    const/4 v0, 0x0

    .line 48
    sput-object v0, Landroid/support/design/widget/l;->l:Landroid/graphics/Paint;

    .line 52
    return-void

    .line 42
    :cond_d
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public constructor <init>(Landroid/view/View;)V
    .registers 4

    .prologue
    const/16 v1, 0x10

    const/high16 v0, 0x41700000    # 15.0f

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput v1, p0, Landroid/support/design/widget/l;->b:I

    .line 63
    iput v1, p0, Landroid/support/design/widget/l;->c:I

    .line 64
    iput v0, p0, Landroid/support/design/widget/l;->d:F

    .line 65
    iput v0, p0, Landroid/support/design/widget/l;->e:F

    .line 97
    iput-object p1, p0, Landroid/support/design/widget/l;->m:Landroid/view/View;

    .line 99
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    .line 100
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 102
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    .line 103
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    .line 104
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/l;->q:Landroid/graphics/RectF;

    .line 105
    return-void
.end method

.method private static a(FFFLandroid/view/animation/Interpolator;)F
    .registers 5

    .prologue
    .line 569
    if-eqz p3, :cond_6

    .line 570
    invoke-interface {p3, p2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p2

    .line 572
    :cond_6
    invoke-static {p0, p1, p2}, Landroid/support/design/widget/a;->a(FFF)F

    move-result v0

    return v0
.end method

.method private static a(IIF)I
    .registers 8

    .prologue
    .line 559
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    .line 560
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p2

    add-float/2addr v1, v2

    .line 561
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    .line 562
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p2

    add-float/2addr v3, v4

    .line 563
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v0, v4

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p2

    add-float/2addr v0, v4

    .line 564
    float-to-int v1, v1

    float-to-int v2, v2

    float-to-int v3, v3

    float-to-int v0, v0

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method private static a(FF)Z
    .registers 4

    .prologue
    .line 541
    sub-float v0, p0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3a83126f    # 0.001f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_f

    const/4 v0, 0x1

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method private static a(Landroid/graphics/Rect;IIII)Z
    .registers 6

    .prologue
    .line 576
    iget v0, p0, Landroid/graphics/Rect;->left:I

    if-ne v0, p1, :cond_12

    iget v0, p0, Landroid/graphics/Rect;->top:I

    if-ne v0, p2, :cond_12

    iget v0, p0, Landroid/graphics/Rect;->right:I

    if-ne v0, p3, :cond_12

    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    if-ne v0, p4, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private b()V
    .registers 2

    .prologue
    .line 162
    iget-object v0, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_24

    iget-object v0, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-lez v0, :cond_24

    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_24

    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-lez v0, :cond_24

    const/4 v0, 0x1

    :goto_21
    iput-boolean v0, p0, Landroid/support/design/widget/l;->n:Z

    .line 164
    return-void

    .line 162
    :cond_24
    const/4 v0, 0x0

    goto :goto_21
.end method

.method private b(F)V
    .registers 3

    .prologue
    .line 118
    iget v0, p0, Landroid/support/design/widget/l;->d:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_b

    .line 119
    iput p1, p0, Landroid/support/design/widget/l;->d:F

    .line 120
    invoke-virtual {p0}, Landroid/support/design/widget/l;->a()V

    .line 122
    :cond_b
    return-void
.end method

.method private b(Landroid/view/animation/Interpolator;)V
    .registers 2

    .prologue
    .line 113
    iput-object p1, p0, Landroid/support/design/widget/l;->i:Landroid/view/animation/Interpolator;

    .line 114
    invoke-virtual {p0}, Landroid/support/design/widget/l;->a()V

    .line 115
    return-void
.end method

.method private b(Ljava/lang/CharSequence;)Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 413
    iget-object v2, p0, Landroid/support/design/widget/l;->m:Landroid/view/View;

    invoke-static {v2}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v2

    if-ne v2, v0, :cond_17

    .line 415
    :goto_a
    if-eqz v0, :cond_19

    sget-object v0, Landroid/support/v4/m/m;->d:Landroid/support/v4/m/l;

    :goto_e
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-interface {v0, p1, v1, v2}, Landroid/support/v4/m/l;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    return v0

    :cond_17
    move v0, v1

    .line 413
    goto :goto_a

    .line 415
    :cond_19
    sget-object v0, Landroid/support/v4/m/m;->c:Landroid/support/v4/m/l;

    goto :goto_e
.end method

.method private c()I
    .registers 2

    .prologue
    .line 174
    iget v0, p0, Landroid/support/design/widget/l;->b:I

    return v0
.end method

.method private c(F)V
    .registers 3

    .prologue
    .line 125
    iget v0, p0, Landroid/support/design/widget/l;->e:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_b

    .line 126
    iput p1, p0, Landroid/support/design/widget/l;->e:F

    .line 127
    invoke-virtual {p0}, Landroid/support/design/widget/l;->a()V

    .line 129
    :cond_b
    return-void
.end method

.method private d()I
    .registers 2

    .prologue
    .line 185
    iget v0, p0, Landroid/support/design/widget/l;->c:I

    return v0
.end method

.method private d(F)V
    .registers 6

    .prologue
    .line 354
    iget-object v0, p0, Landroid/support/design/widget/l;->q:Landroid/graphics/RectF;

    iget-object v1, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget-object v3, p0, Landroid/support/design/widget/l;->i:Landroid/view/animation/Interpolator;

    invoke-static {v1, v2, p1, v3}, Landroid/support/design/widget/l;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 356
    iget-object v0, p0, Landroid/support/design/widget/l;->q:Landroid/graphics/RectF;

    iget v1, p0, Landroid/support/design/widget/l;->s:F

    iget v2, p0, Landroid/support/design/widget/l;->t:F

    iget-object v3, p0, Landroid/support/design/widget/l;->i:Landroid/view/animation/Interpolator;

    invoke-static {v1, v2, p1, v3}, Landroid/support/design/widget/l;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 358
    iget-object v0, p0, Landroid/support/design/widget/l;->q:Landroid/graphics/RectF;

    iget-object v1, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget-object v2, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iget-object v3, p0, Landroid/support/design/widget/l;->i:Landroid/view/animation/Interpolator;

    invoke-static {v1, v2, p1, v3}, Landroid/support/design/widget/l;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 360
    iget-object v0, p0, Landroid/support/design/widget/l;->q:Landroid/graphics/RectF;

    iget-object v1, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    iget-object v2, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iget-object v3, p0, Landroid/support/design/widget/l;->i:Landroid/view/animation/Interpolator;

    invoke-static {v1, v2, p1, v3}, Landroid/support/design/widget/l;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 362
    return-void
.end method

.method private e()Landroid/graphics/Typeface;
    .registers 2

    .prologue
    .line 229
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method private e(F)V
    .registers 10

    .prologue
    const/4 v4, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 421
    iget-object v0, p0, Landroid/support/design/widget/l;->g:Ljava/lang/CharSequence;

    if-nez v0, :cond_a

    .line 471
    :goto_9
    return-void

    .line 427
    :cond_a
    iget v0, p0, Landroid/support/design/widget/l;->e:F

    invoke-static {p1, v0}, Landroid/support/design/widget/l;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 428
    iget-object v0, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v3, v0

    .line 429
    iget v0, p0, Landroid/support/design/widget/l;->e:F

    .line 430
    iput v7, p0, Landroid/support/design/widget/l;->F:F

    move v5, v3

    move v3, v0

    .line 444
    :goto_1f
    cmpl-float v0, v5, v4

    if-lez v0, :cond_135

    .line 445
    iget v0, p0, Landroid/support/design/widget/l;->G:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2d

    iget-boolean v0, p0, Landroid/support/design/widget/l;->H:Z

    if-eqz v0, :cond_bc

    :cond_2d
    move v0, v1

    .line 446
    :goto_2e
    iput v3, p0, Landroid/support/design/widget/l;->G:F

    .line 447
    iput-boolean v2, p0, Landroid/support/design/widget/l;->H:Z

    .line 450
    :goto_32
    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    if-eqz v3, :cond_38

    if-eqz v0, :cond_70

    .line 451
    :cond_38
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v3, p0, Landroid/support/design/widget/l;->G:F

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 454
    iget-object v0, p0, Landroid/support/design/widget/l;->g:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v3, v5, v6}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 456
    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    if-eqz v3, :cond_55

    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_57

    .line 457
    :cond_55
    iput-object v0, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    .line 459
    :cond_57
    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    .line 3413
    iget-object v0, p0, Landroid/support/design/widget/l;->m:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    if-ne v0, v1, :cond_bf

    move v0, v1

    .line 3415
    :goto_62
    if-eqz v0, :cond_c1

    sget-object v0, Landroid/support/v4/m/m;->d:Landroid/support/v4/m/l;

    :goto_66
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-interface {v0, v3, v2, v5}, Landroid/support/v4/m/l;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    .line 459
    iput-boolean v0, p0, Landroid/support/design/widget/l;->z:Z

    .line 463
    :cond_70
    sget-boolean v0, Landroid/support/design/widget/l;->j:Z

    if-eqz v0, :cond_c4

    iget v0, p0, Landroid/support/design/widget/l;->F:F

    cmpl-float v0, v0, v7

    if-eqz v0, :cond_c4

    :goto_7a
    iput-boolean v1, p0, Landroid/support/design/widget/l;->A:Z

    .line 465
    iget-boolean v0, p0, Landroid/support/design/widget/l;->A:Z

    if-eqz v0, :cond_94

    .line 3474
    iget-object v0, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    if-nez v0, :cond_94

    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_94

    iget-object v0, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c6

    .line 470
    :cond_94
    :goto_94
    iget-object v0, p0, Landroid/support/design/widget/l;->m:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    goto/16 :goto_9

    .line 432
    :cond_9b
    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v3, v0

    .line 433
    iget v0, p0, Landroid/support/design/widget/l;->d:F

    .line 435
    iget v5, p0, Landroid/support/design/widget/l;->d:F

    invoke-static {p1, v5}, Landroid/support/design/widget/l;->a(FF)Z

    move-result v5

    if-eqz v5, :cond_b2

    .line 437
    iput v7, p0, Landroid/support/design/widget/l;->F:F

    move v5, v3

    move v3, v0

    goto/16 :goto_1f

    .line 440
    :cond_b2
    iget v5, p0, Landroid/support/design/widget/l;->d:F

    div-float v5, p1, v5

    iput v5, p0, Landroid/support/design/widget/l;->F:F

    move v5, v3

    move v3, v0

    goto/16 :goto_1f

    :cond_bc
    move v0, v2

    .line 445
    goto/16 :goto_2e

    :cond_bf
    move v0, v2

    .line 3413
    goto :goto_62

    .line 3415
    :cond_c1
    sget-object v0, Landroid/support/v4/m/m;->c:Landroid/support/v4/m/l;

    goto :goto_66

    :cond_c4
    move v1, v2

    .line 463
    goto :goto_7a

    .line 3479
    :cond_c6
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v1, p0, Landroid/support/design/widget/l;->d:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 3480
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v1, p0, Landroid/support/design/widget/l;->r:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 3481
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->ascent()F

    move-result v0

    iput v0, p0, Landroid/support/design/widget/l;->D:F

    .line 3482
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->descent()F

    move-result v0

    iput v0, p0, Landroid/support/design/widget/l;->E:F

    .line 3484
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget-object v1, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 3485
    iget v1, p0, Landroid/support/design/widget/l;->E:F

    iget v3, p0, Landroid/support/design/widget/l;->D:F

    sub-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 3487
    if-gtz v0, :cond_103

    if-lez v5, :cond_94

    .line 3491
    :cond_103
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v5, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    .line 3493
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 3494
    iget-object v1, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    int-to-float v5, v5

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->descent()F

    move-result v6

    sub-float/2addr v5, v6

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 3496
    iget-object v0, p0, Landroid/support/design/widget/l;->C:Landroid/graphics/Paint;

    if-nez v0, :cond_94

    .line 3498
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Landroid/support/design/widget/l;->C:Landroid/graphics/Paint;

    goto/16 :goto_94

    :cond_135
    move v0, v2

    goto/16 :goto_32
.end method

.method private f()F
    .registers 2

    .prologue
    .line 249
    iget v0, p0, Landroid/support/design/widget/l;->a:F

    return v0
.end method

.method private g()F
    .registers 2

    .prologue
    .line 253
    iget v0, p0, Landroid/support/design/widget/l;->e:F

    return v0
.end method

.method private h()F
    .registers 2

    .prologue
    .line 257
    iget v0, p0, Landroid/support/design/widget/l;->d:F

    return v0
.end method

.method private i()V
    .registers 10

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 261
    iget v7, p0, Landroid/support/design/widget/l;->a:F

    .line 1354
    iget-object v0, p0, Landroid/support/design/widget/l;->q:Landroid/graphics/RectF;

    iget-object v3, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget-object v5, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    iget-object v6, p0, Landroid/support/design/widget/l;->i:Landroid/view/animation/Interpolator;

    invoke-static {v3, v5, v7, v6}, Landroid/support/design/widget/l;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v3

    iput v3, v0, Landroid/graphics/RectF;->left:F

    .line 1356
    iget-object v0, p0, Landroid/support/design/widget/l;->q:Landroid/graphics/RectF;

    iget v3, p0, Landroid/support/design/widget/l;->s:F

    iget v5, p0, Landroid/support/design/widget/l;->t:F

    iget-object v6, p0, Landroid/support/design/widget/l;->i:Landroid/view/animation/Interpolator;

    invoke-static {v3, v5, v7, v6}, Landroid/support/design/widget/l;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v3

    iput v3, v0, Landroid/graphics/RectF;->top:F

    .line 1358
    iget-object v0, p0, Landroid/support/design/widget/l;->q:Landroid/graphics/RectF;

    iget-object v3, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget-object v5, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    iget-object v6, p0, Landroid/support/design/widget/l;->i:Landroid/view/animation/Interpolator;

    invoke-static {v3, v5, v7, v6}, Landroid/support/design/widget/l;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v3

    iput v3, v0, Landroid/graphics/RectF;->right:F

    .line 1360
    iget-object v0, p0, Landroid/support/design/widget/l;->q:Landroid/graphics/RectF;

    iget-object v3, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    iget-object v5, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    iget-object v6, p0, Landroid/support/design/widget/l;->i:Landroid/view/animation/Interpolator;

    invoke-static {v3, v5, v7, v6}, Landroid/support/design/widget/l;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v3

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 264
    iget v0, p0, Landroid/support/design/widget/l;->u:F

    iget v3, p0, Landroid/support/design/widget/l;->v:F

    iget-object v5, p0, Landroid/support/design/widget/l;->i:Landroid/view/animation/Interpolator;

    invoke-static {v0, v3, v7, v5}, Landroid/support/design/widget/l;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v0

    iput v0, p0, Landroid/support/design/widget/l;->w:F

    .line 266
    iget v0, p0, Landroid/support/design/widget/l;->s:F

    iget v3, p0, Landroid/support/design/widget/l;->t:F

    iget-object v5, p0, Landroid/support/design/widget/l;->i:Landroid/view/animation/Interpolator;

    invoke-static {v0, v3, v7, v5}, Landroid/support/design/widget/l;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v0

    iput v0, p0, Landroid/support/design/widget/l;->x:F

    .line 269
    iget v0, p0, Landroid/support/design/widget/l;->d:F

    iget v3, p0, Landroid/support/design/widget/l;->e:F

    iget-object v5, p0, Landroid/support/design/widget/l;->I:Landroid/view/animation/Interpolator;

    invoke-static {v0, v3, v7, v5}, Landroid/support/design/widget/l;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v5

    .line 1421
    iget-object v0, p0, Landroid/support/design/widget/l;->g:Ljava/lang/CharSequence;

    if-eqz v0, :cond_106

    .line 1427
    iget v0, p0, Landroid/support/design/widget/l;->e:F

    invoke-static {v5, v0}, Landroid/support/design/widget/l;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_159

    .line 1428
    iget-object v0, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v3, v0

    .line 1429
    iget v0, p0, Landroid/support/design/widget/l;->e:F

    .line 1430
    iput v8, p0, Landroid/support/design/widget/l;->F:F

    move v5, v3

    move v3, v0

    .line 1444
    :goto_8c
    cmpl-float v0, v5, v4

    if-lez v0, :cond_1fe

    .line 1445
    iget v0, p0, Landroid/support/design/widget/l;->G:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_9a

    iget-boolean v0, p0, Landroid/support/design/widget/l;->H:Z

    if-eqz v0, :cond_179

    :cond_9a
    move v0, v1

    .line 1446
    :goto_9b
    iput v3, p0, Landroid/support/design/widget/l;->G:F

    .line 1447
    iput-boolean v2, p0, Landroid/support/design/widget/l;->H:Z

    .line 1450
    :goto_9f
    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    if-eqz v3, :cond_a5

    if-eqz v0, :cond_dd

    .line 1451
    :cond_a5
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v3, p0, Landroid/support/design/widget/l;->G:F

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1454
    iget-object v0, p0, Landroid/support/design/widget/l;->g:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v3, v5, v6}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1456
    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    if-eqz v3, :cond_c2

    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c4

    .line 1457
    :cond_c2
    iput-object v0, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    .line 1459
    :cond_c4
    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    .line 2413
    iget-object v0, p0, Landroid/support/design/widget/l;->m:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    if-ne v0, v1, :cond_17c

    move v0, v1

    .line 2415
    :goto_cf
    if-eqz v0, :cond_17f

    sget-object v0, Landroid/support/v4/m/m;->d:Landroid/support/v4/m/l;

    :goto_d3
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-interface {v0, v3, v2, v5}, Landroid/support/v4/m/l;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    .line 1459
    iput-boolean v0, p0, Landroid/support/design/widget/l;->z:Z

    .line 1463
    :cond_dd
    sget-boolean v0, Landroid/support/design/widget/l;->j:Z

    if-eqz v0, :cond_183

    iget v0, p0, Landroid/support/design/widget/l;->F:F

    cmpl-float v0, v0, v8

    if-eqz v0, :cond_183

    :goto_e7
    iput-boolean v1, p0, Landroid/support/design/widget/l;->A:Z

    .line 1465
    iget-boolean v0, p0, Landroid/support/design/widget/l;->A:Z

    if-eqz v0, :cond_101

    .line 2474
    iget-object v0, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    if-nez v0, :cond_101

    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_101

    iget-object v0, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_186

    .line 1470
    :cond_101
    :goto_101
    iget-object v0, p0, Landroid/support/design/widget/l;->m:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 272
    :cond_106
    iget v0, p0, Landroid/support/design/widget/l;->f:I

    iget v1, p0, Landroid/support/design/widget/l;->r:I

    if-eq v0, v1, :cond_1f5

    .line 275
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v1, p0, Landroid/support/design/widget/l;->r:I

    iget v2, p0, Landroid/support/design/widget/l;->f:I

    .line 2559
    sub-float v3, v8, v7

    .line 2560
    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v3

    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    .line 2561
    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v3

    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    .line 2562
    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v3

    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v7

    add-float/2addr v6, v8

    .line 2563
    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v7

    add-float/2addr v1, v2

    .line 2564
    float-to-int v2, v4

    float-to-int v3, v5

    float-to-int v4, v6

    float-to-int v1, v1

    invoke-static {v2, v3, v4, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    .line 275
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 280
    :goto_153
    iget-object v0, p0, Landroid/support/design/widget/l;->m:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 281
    return-void

    .line 1432
    :cond_159
    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v3, v0

    .line 1433
    iget v0, p0, Landroid/support/design/widget/l;->d:F

    .line 1435
    iget v6, p0, Landroid/support/design/widget/l;->d:F

    invoke-static {v5, v6}, Landroid/support/design/widget/l;->a(FF)Z

    move-result v6

    if-eqz v6, :cond_170

    .line 1437
    iput v8, p0, Landroid/support/design/widget/l;->F:F

    move v5, v3

    move v3, v0

    goto/16 :goto_8c

    .line 1440
    :cond_170
    iget v6, p0, Landroid/support/design/widget/l;->d:F

    div-float/2addr v5, v6

    iput v5, p0, Landroid/support/design/widget/l;->F:F

    move v5, v3

    move v3, v0

    goto/16 :goto_8c

    :cond_179
    move v0, v2

    .line 1445
    goto/16 :goto_9b

    :cond_17c
    move v0, v2

    .line 2413
    goto/16 :goto_cf

    .line 2415
    :cond_17f
    sget-object v0, Landroid/support/v4/m/m;->c:Landroid/support/v4/m/l;

    goto/16 :goto_d3

    :cond_183
    move v1, v2

    .line 1463
    goto/16 :goto_e7

    .line 2479
    :cond_186
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v1, p0, Landroid/support/design/widget/l;->d:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 2480
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v1, p0, Landroid/support/design/widget/l;->r:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 2481
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->ascent()F

    move-result v0

    iput v0, p0, Landroid/support/design/widget/l;->D:F

    .line 2482
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->descent()F

    move-result v0

    iput v0, p0, Landroid/support/design/widget/l;->E:F

    .line 2484
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget-object v1, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 2485
    iget v1, p0, Landroid/support/design/widget/l;->E:F

    iget v3, p0, Landroid/support/design/widget/l;->D:F

    sub-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 2487
    if-gtz v0, :cond_1c3

    if-lez v5, :cond_101

    .line 2491
    :cond_1c3
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v5, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    .line 2493
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2494
    iget-object v1, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    int-to-float v5, v5

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->descent()F

    move-result v6

    sub-float/2addr v5, v6

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 2496
    iget-object v0, p0, Landroid/support/design/widget/l;->C:Landroid/graphics/Paint;

    if-nez v0, :cond_101

    .line 2498
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Landroid/support/design/widget/l;->C:Landroid/graphics/Paint;

    goto/16 :goto_101

    .line 277
    :cond_1f5
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v1, p0, Landroid/support/design/widget/l;->f:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    goto/16 :goto_153

    :cond_1fe
    move v0, v2

    goto/16 :goto_9f
.end method

.method private j()V
    .registers 9

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 285
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v2, p0, Landroid/support/design/widget/l;->e:F

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 286
    iget-object v0, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    if-eqz v0, :cond_b0

    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget-object v2, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    iget-object v5, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-virtual {v0, v2, v4, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    .line 288
    :goto_1e
    iget v5, p0, Landroid/support/design/widget/l;->c:I

    iget-boolean v2, p0, Landroid/support/design/widget/l;->z:Z

    if-eqz v2, :cond_b3

    move v2, v3

    :goto_25
    invoke-static {v5, v2}, Landroid/support/v4/view/v;->a(II)I

    move-result v2

    .line 290
    and-int/lit8 v5, v2, 0x70

    sparse-switch v5, :sswitch_data_114

    .line 299
    iget-object v5, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v5}, Landroid/text/TextPaint;->descent()F

    move-result v5

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->ascent()F

    move-result v6

    sub-float/2addr v5, v6

    .line 300
    div-float/2addr v5, v7

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->descent()F

    move-result v6

    sub-float/2addr v5, v6

    .line 301
    iget-object v6, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    iput v5, p0, Landroid/support/design/widget/l;->t:F

    .line 304
    :goto_4d
    and-int/lit8 v2, v2, 0x7

    sparse-switch v2, :sswitch_data_11e

    .line 313
    iget-object v0, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iput v0, p0, Landroid/support/design/widget/l;->v:F

    .line 317
    :goto_59
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v2, p0, Landroid/support/design/widget/l;->d:F

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 318
    iget-object v0, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    if-eqz v0, :cond_72

    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget-object v1, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    iget-object v2, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v0, v1, v4, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v1

    .line 320
    :cond_72
    iget v0, p0, Landroid/support/design/widget/l;->b:I

    iget-boolean v2, p0, Landroid/support/design/widget/l;->z:Z

    if-eqz v2, :cond_e5

    :goto_78
    invoke-static {v0, v3}, Landroid/support/v4/view/v;->a(II)I

    move-result v0

    .line 322
    and-int/lit8 v2, v0, 0x70

    sparse-switch v2, :sswitch_data_128

    .line 331
    iget-object v2, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    iget-object v3, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    sub-float/2addr v2, v3

    .line 332
    div-float/2addr v2, v7

    iget-object v3, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sub-float/2addr v2, v3

    .line 333
    iget-object v3, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    iput v2, p0, Landroid/support/design/widget/l;->s:F

    .line 336
    :goto_a0
    and-int/lit8 v0, v0, 0x7

    sparse-switch v0, :sswitch_data_132

    .line 345
    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iput v0, p0, Landroid/support/design/widget/l;->u:F

    .line 350
    :goto_ac
    invoke-direct {p0}, Landroid/support/design/widget/l;->m()V

    .line 351
    return-void

    :cond_b0
    move v0, v1

    .line 286
    goto/16 :goto_1e

    :cond_b3
    move v2, v4

    .line 288
    goto/16 :goto_25

    .line 292
    :sswitch_b6
    iget-object v5, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    iput v5, p0, Landroid/support/design/widget/l;->t:F

    goto :goto_4d

    .line 295
    :sswitch_be
    iget-object v5, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->ascent()F

    move-result v6

    sub-float/2addr v5, v6

    iput v5, p0, Landroid/support/design/widget/l;->t:F

    goto :goto_4d

    .line 306
    :sswitch_cd
    iget-object v2, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v7

    sub-float v0, v2, v0

    iput v0, p0, Landroid/support/design/widget/l;->v:F

    goto :goto_59

    .line 309
    :sswitch_da
    iget-object v2, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    sub-float v0, v2, v0

    iput v0, p0, Landroid/support/design/widget/l;->v:F

    goto/16 :goto_59

    :cond_e5
    move v3, v4

    .line 320
    goto :goto_78

    .line 324
    :sswitch_e7
    iget-object v2, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iput v2, p0, Landroid/support/design/widget/l;->s:F

    goto :goto_a0

    .line 327
    :sswitch_ef
    iget-object v2, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    sub-float/2addr v2, v3

    iput v2, p0, Landroid/support/design/widget/l;->s:F

    goto :goto_a0

    .line 338
    :sswitch_fe
    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v1, v7

    sub-float/2addr v0, v1

    iput v0, p0, Landroid/support/design/widget/l;->u:F

    goto :goto_ac

    .line 341
    :sswitch_10a
    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    sub-float/2addr v0, v1

    iput v0, p0, Landroid/support/design/widget/l;->u:F

    goto :goto_ac

    .line 290
    nop

    :sswitch_data_114
    .sparse-switch
        0x30 -> :sswitch_be
        0x50 -> :sswitch_b6
    .end sparse-switch

    .line 304
    :sswitch_data_11e
    .sparse-switch
        0x1 -> :sswitch_cd
        0x5 -> :sswitch_da
    .end sparse-switch

    .line 322
    :sswitch_data_128
    .sparse-switch
        0x30 -> :sswitch_ef
        0x50 -> :sswitch_e7
    .end sparse-switch

    .line 336
    :sswitch_data_132
    .sparse-switch
        0x1 -> :sswitch_fe
        0x5 -> :sswitch_10a
    .end sparse-switch
.end method

.method private k()V
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 474
    iget-object v0, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    if-nez v0, :cond_15

    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 500
    :cond_15
    :goto_15
    return-void

    .line 479
    :cond_16
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v1, p0, Landroid/support/design/widget/l;->d:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 480
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v1, p0, Landroid/support/design/widget/l;->r:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 481
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->ascent()F

    move-result v0

    iput v0, p0, Landroid/support/design/widget/l;->D:F

    .line 482
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->descent()F

    move-result v0

    iput v0, p0, Landroid/support/design/widget/l;->E:F

    .line 484
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget-object v1, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 485
    iget v1, p0, Landroid/support/design/widget/l;->E:F

    iget v3, p0, Landroid/support/design/widget/l;->D:F

    sub-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 487
    if-gtz v0, :cond_53

    if-lez v5, :cond_15

    .line 491
    :cond_53
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v5, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    .line 493
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 494
    iget-object v1, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/4 v4, 0x0

    int-to-float v5, v5

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->descent()F

    move-result v6

    sub-float/2addr v5, v6

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 496
    iget-object v0, p0, Landroid/support/design/widget/l;->C:Landroid/graphics/Paint;

    if-nez v0, :cond_15

    .line 498
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Landroid/support/design/widget/l;->C:Landroid/graphics/Paint;

    goto :goto_15
.end method

.method private l()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 526
    iget-object v0, p0, Landroid/support/design/widget/l;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private m()V
    .registers 2

    .prologue
    .line 530
    iget-object v0, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_c

    .line 531
    iget-object v0, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 532
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    .line 534
    :cond_c
    return-void
.end method

.method private n()I
    .registers 2

    .prologue
    .line 545
    iget v0, p0, Landroid/support/design/widget/l;->r:I

    return v0
.end method

.method private o()I
    .registers 2

    .prologue
    .line 549
    iget v0, p0, Landroid/support/design/widget/l;->f:I

    return v0
.end method


# virtual methods
.method public final a()V
    .registers 9

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 503
    iget-object v0, p0, Landroid/support/design/widget/l;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lez v0, :cond_c2

    iget-object v0, p0, Landroid/support/design/widget/l;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-lez v0, :cond_c2

    .line 4285
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v2, p0, Landroid/support/design/widget/l;->e:F

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 4286
    iget-object v0, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    if-eqz v0, :cond_c3

    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget-object v2, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    iget-object v5, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-virtual {v0, v2, v4, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    .line 4288
    :goto_2e
    iget v5, p0, Landroid/support/design/widget/l;->c:I

    iget-boolean v2, p0, Landroid/support/design/widget/l;->z:Z

    if-eqz v2, :cond_c6

    move v2, v3

    :goto_35
    invoke-static {v5, v2}, Landroid/support/v4/view/v;->a(II)I

    move-result v2

    .line 4290
    and-int/lit8 v5, v2, 0x70

    sparse-switch v5, :sswitch_data_128

    .line 4299
    iget-object v5, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v5}, Landroid/text/TextPaint;->descent()F

    move-result v5

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->ascent()F

    move-result v6

    sub-float/2addr v5, v6

    .line 4300
    div-float/2addr v5, v7

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->descent()F

    move-result v6

    sub-float/2addr v5, v6

    .line 4301
    iget-object v6, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    iput v5, p0, Landroid/support/design/widget/l;->t:F

    .line 4304
    :goto_5d
    and-int/lit8 v2, v2, 0x7

    sparse-switch v2, :sswitch_data_132

    .line 4313
    iget-object v0, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iput v0, p0, Landroid/support/design/widget/l;->v:F

    .line 4317
    :goto_69
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v2, p0, Landroid/support/design/widget/l;->d:F

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 4318
    iget-object v0, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    if-eqz v0, :cond_82

    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget-object v1, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    iget-object v2, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v0, v1, v4, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v1

    .line 4320
    :cond_82
    iget v0, p0, Landroid/support/design/widget/l;->b:I

    iget-boolean v2, p0, Landroid/support/design/widget/l;->z:Z

    if-eqz v2, :cond_fa

    :goto_88
    invoke-static {v0, v3}, Landroid/support/v4/view/v;->a(II)I

    move-result v0

    .line 4322
    and-int/lit8 v2, v0, 0x70

    sparse-switch v2, :sswitch_data_13c

    .line 4331
    iget-object v2, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    iget-object v3, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    sub-float/2addr v2, v3

    .line 4332
    div-float/2addr v2, v7

    iget-object v3, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sub-float/2addr v2, v3

    .line 4333
    iget-object v3, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    iput v2, p0, Landroid/support/design/widget/l;->s:F

    .line 4336
    :goto_b0
    and-int/lit8 v0, v0, 0x7

    sparse-switch v0, :sswitch_data_146

    .line 4345
    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iput v0, p0, Landroid/support/design/widget/l;->u:F

    .line 4350
    :goto_bc
    invoke-direct {p0}, Landroid/support/design/widget/l;->m()V

    .line 507
    invoke-direct {p0}, Landroid/support/design/widget/l;->i()V

    .line 509
    :cond_c2
    return-void

    :cond_c3
    move v0, v1

    .line 4286
    goto/16 :goto_2e

    :cond_c6
    move v2, v4

    .line 4288
    goto/16 :goto_35

    .line 4292
    :sswitch_c9
    iget-object v5, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    iput v5, p0, Landroid/support/design/widget/l;->t:F

    goto :goto_5d

    .line 4295
    :sswitch_d1
    iget-object v5, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->ascent()F

    move-result v6

    sub-float/2addr v5, v6

    iput v5, p0, Landroid/support/design/widget/l;->t:F

    goto/16 :goto_5d

    .line 4306
    :sswitch_e1
    iget-object v2, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v7

    sub-float v0, v2, v0

    iput v0, p0, Landroid/support/design/widget/l;->v:F

    goto/16 :goto_69

    .line 4309
    :sswitch_ef
    iget-object v2, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    sub-float v0, v2, v0

    iput v0, p0, Landroid/support/design/widget/l;->v:F

    goto/16 :goto_69

    :cond_fa
    move v3, v4

    .line 4320
    goto :goto_88

    .line 4324
    :sswitch_fc
    iget-object v2, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iput v2, p0, Landroid/support/design/widget/l;->s:F

    goto :goto_b0

    .line 4327
    :sswitch_104
    iget-object v2, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    sub-float/2addr v2, v3

    iput v2, p0, Landroid/support/design/widget/l;->s:F

    goto :goto_b0

    .line 4338
    :sswitch_113
    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v1, v7

    sub-float/2addr v0, v1

    iput v0, p0, Landroid/support/design/widget/l;->u:F

    goto :goto_bc

    .line 4341
    :sswitch_11f
    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    sub-float/2addr v0, v1

    iput v0, p0, Landroid/support/design/widget/l;->u:F

    goto :goto_bc

    .line 4290
    :sswitch_data_128
    .sparse-switch
        0x30 -> :sswitch_d1
        0x50 -> :sswitch_c9
    .end sparse-switch

    .line 4304
    :sswitch_data_132
    .sparse-switch
        0x1 -> :sswitch_e1
        0x5 -> :sswitch_ef
    .end sparse-switch

    .line 4322
    :sswitch_data_13c
    .sparse-switch
        0x30 -> :sswitch_104
        0x50 -> :sswitch_fc
    .end sparse-switch

    .line 4336
    :sswitch_data_146
    .sparse-switch
        0x1 -> :sswitch_113
        0x5 -> :sswitch_11f
    .end sparse-switch
.end method

.method final a(F)V
    .registers 5

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 240
    .line 1026
    cmpg-float v2, p1, v0

    if-gez v2, :cond_14

    move p1, v0

    .line 242
    :cond_8
    :goto_8
    iget v0, p0, Landroid/support/design/widget/l;->a:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_13

    .line 243
    iput p1, p0, Landroid/support/design/widget/l;->a:F

    .line 244
    invoke-direct {p0}, Landroid/support/design/widget/l;->i()V

    .line 246
    :cond_13
    return-void

    .line 1026
    :cond_14
    cmpl-float v0, p1, v1

    if-lez v0, :cond_8

    move p1, v1

    goto :goto_8
.end method

.method final a(I)V
    .registers 3

    .prologue
    .line 132
    iget v0, p0, Landroid/support/design/widget/l;->f:I

    if-eq v0, p1, :cond_9

    .line 133
    iput p1, p0, Landroid/support/design/widget/l;->f:I

    .line 134
    invoke-virtual {p0}, Landroid/support/design/widget/l;->a()V

    .line 136
    :cond_9
    return-void
.end method

.method final a(IIII)V
    .registers 6

    .prologue
    .line 146
    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-static {v0, p1, p2, p3, p4}, Landroid/support/design/widget/l;->a(Landroid/graphics/Rect;IIII)Z

    move-result v0

    if-nez v0, :cond_13

    .line 147
    iget-object v0, p0, Landroid/support/design/widget/l;->o:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/l;->H:Z

    .line 149
    invoke-direct {p0}, Landroid/support/design/widget/l;->b()V

    .line 151
    :cond_13
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .registers 10

    .prologue
    const/4 v2, 0x0

    .line 365
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v7

    .line 367
    iget-object v0, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    if-eqz v0, :cond_43

    iget-boolean v0, p0, Landroid/support/design/widget/l;->n:Z

    if-eqz v0, :cond_43

    .line 368
    iget v4, p0, Landroid/support/design/widget/l;->w:F

    .line 369
    iget v5, p0, Landroid/support/design/widget/l;->x:F

    .line 371
    iget-boolean v0, p0, Landroid/support/design/widget/l;->A:Z

    if-eqz v0, :cond_47

    iget-object v0, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_47

    const/4 v0, 0x1

    .line 377
    :goto_1a
    iget-object v1, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    iget v3, p0, Landroid/support/design/widget/l;->G:F

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 379
    if-eqz v0, :cond_49

    .line 380
    iget v1, p0, Landroid/support/design/widget/l;->D:F

    iget v3, p0, Landroid/support/design/widget/l;->F:F

    mul-float/2addr v1, v3

    .line 393
    :goto_28
    if-eqz v0, :cond_2b

    .line 394
    add-float/2addr v5, v1

    .line 397
    :cond_2b
    iget v1, p0, Landroid/support/design/widget/l;->F:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_3a

    .line 398
    iget v1, p0, Landroid/support/design/widget/l;->F:F

    iget v3, p0, Landroid/support/design/widget/l;->F:F

    invoke-virtual {p1, v1, v3, v4, v5}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 401
    :cond_3a
    if-eqz v0, :cond_55

    .line 403
    iget-object v0, p0, Landroid/support/design/widget/l;->B:Landroid/graphics/Bitmap;

    iget-object v1, p0, Landroid/support/design/widget/l;->C:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4, v5, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 409
    :cond_43
    :goto_43
    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 410
    return-void

    :cond_47
    move v0, v2

    .line 371
    goto :goto_1a

    .line 383
    :cond_49
    iget-object v1, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->ascent()F

    const/4 v1, 0x0

    .line 384
    iget-object v3, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    goto :goto_28

    .line 405
    :cond_55
    iget-object v1, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    iget-object v0, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    iget-object v6, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    goto :goto_43
.end method

.method final a(Landroid/graphics/Typeface;)V
    .registers 3

    .prologue
    .line 219
    if-nez p1, :cond_4

    .line 220
    sget-object p1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 222
    :cond_4
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-eq v0, p1, :cond_14

    .line 223
    iget-object v0, p0, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 224
    invoke-virtual {p0}, Landroid/support/design/widget/l;->a()V

    .line 226
    :cond_14
    return-void
.end method

.method final a(Landroid/view/animation/Interpolator;)V
    .registers 2

    .prologue
    .line 108
    iput-object p1, p0, Landroid/support/design/widget/l;->I:Landroid/view/animation/Interpolator;

    .line 109
    invoke-virtual {p0}, Landroid/support/design/widget/l;->a()V

    .line 110
    return-void
.end method

.method final a(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 517
    if-eqz p1, :cond_a

    iget-object v0, p0, Landroid/support/design/widget/l;->g:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 518
    :cond_a
    iput-object p1, p0, Landroid/support/design/widget/l;->g:Ljava/lang/CharSequence;

    .line 519
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/l;->y:Ljava/lang/CharSequence;

    .line 520
    invoke-direct {p0}, Landroid/support/design/widget/l;->m()V

    .line 521
    invoke-virtual {p0}, Landroid/support/design/widget/l;->a()V

    .line 523
    :cond_15
    return-void
.end method

.method final b(I)V
    .registers 3

    .prologue
    .line 139
    iget v0, p0, Landroid/support/design/widget/l;->r:I

    if-eq v0, p1, :cond_9

    .line 140
    iput p1, p0, Landroid/support/design/widget/l;->r:I

    .line 141
    invoke-virtual {p0}, Landroid/support/design/widget/l;->a()V

    .line 143
    :cond_9
    return-void
.end method

.method final b(IIII)V
    .registers 6

    .prologue
    .line 154
    iget-object v0, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    invoke-static {v0, p1, p2, p3, p4}, Landroid/support/design/widget/l;->a(Landroid/graphics/Rect;IIII)Z

    move-result v0

    if-nez v0, :cond_13

    .line 155
    iget-object v0, p0, Landroid/support/design/widget/l;->p:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/l;->H:Z

    .line 157
    invoke-direct {p0}, Landroid/support/design/widget/l;->b()V

    .line 159
    :cond_13
    return-void
.end method

.method final c(I)V
    .registers 3

    .prologue
    .line 167
    iget v0, p0, Landroid/support/design/widget/l;->b:I

    if-eq v0, p1, :cond_9

    .line 168
    iput p1, p0, Landroid/support/design/widget/l;->b:I

    .line 169
    invoke-virtual {p0}, Landroid/support/design/widget/l;->a()V

    .line 171
    :cond_9
    return-void
.end method

.method final d(I)V
    .registers 3

    .prologue
    .line 178
    iget v0, p0, Landroid/support/design/widget/l;->c:I

    if-eq v0, p1, :cond_9

    .line 179
    iput p1, p0, Landroid/support/design/widget/l;->c:I

    .line 180
    invoke-virtual {p0}, Landroid/support/design/widget/l;->a()V

    .line 182
    :cond_9
    return-void
.end method

.method final e(I)V
    .registers 5

    .prologue
    .line 189
    iget-object v0, p0, Landroid/support/design/widget/l;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/design/n;->TextAppearance:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 190
    sget v1, Landroid/support/design/n;->TextAppearance_android_textColor:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 191
    sget v1, Landroid/support/design/n;->TextAppearance_android_textColor:I

    iget v2, p0, Landroid/support/design/widget/l;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/l;->f:I

    .line 194
    :cond_1e
    sget v1, Landroid/support/design/n;->TextAppearance_android_textSize:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 195
    sget v1, Landroid/support/design/n;->TextAppearance_android_textSize:I

    iget v2, p0, Landroid/support/design/widget/l;->e:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Landroid/support/design/widget/l;->e:F

    .line 198
    :cond_32
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 200
    invoke-virtual {p0}, Landroid/support/design/widget/l;->a()V

    .line 201
    return-void
.end method

.method final f(I)V
    .registers 5

    .prologue
    .line 204
    iget-object v0, p0, Landroid/support/design/widget/l;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/design/n;->TextAppearance:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 205
    sget v1, Landroid/support/design/n;->TextAppearance_android_textColor:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 206
    sget v1, Landroid/support/design/n;->TextAppearance_android_textColor:I

    iget v2, p0, Landroid/support/design/widget/l;->r:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/l;->r:I

    .line 209
    :cond_1e
    sget v1, Landroid/support/design/n;->TextAppearance_android_textSize:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 210
    sget v1, Landroid/support/design/n;->TextAppearance_android_textSize:I

    iget v2, p0, Landroid/support/design/widget/l;->d:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Landroid/support/design/widget/l;->d:F

    .line 213
    :cond_32
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 215
    invoke-virtual {p0}, Landroid/support/design/widget/l;->a()V

    .line 216
    return-void
.end method
