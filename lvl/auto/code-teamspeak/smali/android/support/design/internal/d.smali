.class final Landroid/support/design/internal/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/support/v7/internal/view/menu/m;

.field final b:I

.field final c:I


# direct methods
.method private constructor <init>(Landroid/support/v7/internal/view/menu/m;II)V
    .registers 4

    .prologue
    .line 512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 513
    iput-object p1, p0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    .line 514
    iput p2, p0, Landroid/support/design/internal/d;->b:I

    .line 515
    iput p3, p0, Landroid/support/design/internal/d;->c:I

    .line 516
    return-void
.end method

.method public static a(II)Landroid/support/design/internal/d;
    .registers 4

    .prologue
    .line 523
    new-instance v0, Landroid/support/design/internal/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, p1}, Landroid/support/design/internal/d;-><init>(Landroid/support/v7/internal/view/menu/m;II)V

    return-object v0
.end method

.method public static a(Landroid/support/v7/internal/view/menu/m;)Landroid/support/design/internal/d;
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 519
    new-instance v0, Landroid/support/design/internal/d;

    invoke-direct {v0, p0, v1, v1}, Landroid/support/design/internal/d;-><init>(Landroid/support/v7/internal/view/menu/m;II)V

    return-object v0
.end method

.method private a()Z
    .registers 2

    .prologue
    .line 527
    iget-object v0, p0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private b()I
    .registers 2

    .prologue
    .line 531
    iget v0, p0, Landroid/support/design/internal/d;->b:I

    return v0
.end method

.method private c()I
    .registers 2

    .prologue
    .line 535
    iget v0, p0, Landroid/support/design/internal/d;->c:I

    return v0
.end method

.method private d()Landroid/support/v7/internal/view/menu/m;
    .registers 2

    .prologue
    .line 539
    iget-object v0, p0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    return-object v0
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 544
    iget-object v0, p0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    if-eqz v0, :cond_16

    iget-object v0, p0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->hasSubMenu()Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method
