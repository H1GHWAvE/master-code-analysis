.class public interface abstract Landroid/support/v7/internal/widget/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(I)V
.end method

.method public abstract a(Landroid/util/SparseArray;)V
.end method

.method public abstract a(Landroid/view/Menu;Landroid/support/v7/internal/view/menu/y;)V
.end method

.method public abstract a()Z
.end method

.method public abstract b(Landroid/util/SparseArray;)V
.end method

.method public abstract b()Z
.end method

.method public abstract c()Z
.end method

.method public abstract d()Z
.end method

.method public abstract e()Z
.end method

.method public abstract f()Z
.end method

.method public abstract g()Z
.end method

.method public abstract getTitle()Ljava/lang/CharSequence;
.end method

.method public abstract h()V
.end method

.method public abstract i()V
.end method

.method public abstract setIcon(I)V
.end method

.method public abstract setIcon(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract setLogo(I)V
.end method

.method public abstract setUiOptions(I)V
.end method

.method public abstract setWindowCallback(Landroid/view/Window$Callback;)V
.end method

.method public abstract setWindowTitle(Ljava/lang/CharSequence;)V
.end method
