.class public final Landroid/support/v7/internal/view/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/ArrayList;

.field b:Landroid/support/v4/view/gd;

.field c:Z

.field private d:J

.field private e:Landroid/view/animation/Interpolator;

.field private final f:Landroid/support/v4/view/ge;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v7/internal/view/i;->d:J

    .line 115
    new-instance v0, Landroid/support/v7/internal/view/j;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/view/j;-><init>(Landroid/support/v7/internal/view/i;)V

    iput-object v0, p0, Landroid/support/v7/internal/view/i;->f:Landroid/support/v4/view/ge;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/view/i;->a:Ljava/util/ArrayList;

    .line 45
    return-void
.end method

.method private static synthetic a(Landroid/support/v7/internal/view/i;)Landroid/support/v4/view/gd;
    .registers 2

    .prologue
    .line 33
    iget-object v0, p0, Landroid/support/v7/internal/view/i;->b:Landroid/support/v4/view/gd;

    return-object v0
.end method

.method private a(Landroid/support/v4/view/fk;Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;
    .registers 5

    .prologue
    .line 56
    iget-object v0, p0, Landroid/support/v7/internal/view/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1795
    iget-object v0, p1, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_1e

    .line 1796
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, v0}, Landroid/support/v4/view/fu;->a(Landroid/view/View;)J

    move-result-wide v0

    .line 57
    :goto_15
    invoke-virtual {p2, v0, v1}, Landroid/support/v4/view/fk;->b(J)Landroid/support/v4/view/fk;

    .line 58
    iget-object v0, p0, Landroid/support/v7/internal/view/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    return-object p0

    .line 1798
    :cond_1e
    const-wide/16 v0, 0x0

    goto :goto_15
.end method

.method private static synthetic b(Landroid/support/v7/internal/view/i;)V
    .registers 2

    .prologue
    .line 33
    .line 2081
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/i;->c:Z

    .line 33
    return-void
.end method

.method private static synthetic c(Landroid/support/v7/internal/view/i;)Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 33
    iget-object v0, p0, Landroid/support/v7/internal/view/i;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method private d()V
    .registers 2

    .prologue
    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/i;->c:Z

    .line 82
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;
    .registers 3

    .prologue
    .line 48
    iget-boolean v0, p0, Landroid/support/v7/internal/view/i;->c:Z

    if-nez v0, :cond_9

    .line 49
    iget-object v0, p0, Landroid/support/v7/internal/view/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_9
    return-object p0
.end method

.method public final a(Landroid/support/v4/view/gd;)Landroid/support/v7/internal/view/i;
    .registers 3

    .prologue
    .line 109
    iget-boolean v0, p0, Landroid/support/v7/internal/view/i;->c:Z

    if-nez v0, :cond_6

    .line 110
    iput-object p1, p0, Landroid/support/v7/internal/view/i;->b:Landroid/support/v4/view/gd;

    .line 112
    :cond_6
    return-object p0
.end method

.method public final a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/i;
    .registers 3

    .prologue
    .line 102
    iget-boolean v0, p0, Landroid/support/v7/internal/view/i;->c:Z

    if-nez v0, :cond_6

    .line 103
    iput-object p1, p0, Landroid/support/v7/internal/view/i;->e:Landroid/view/animation/Interpolator;

    .line 105
    :cond_6
    return-object p0
.end method

.method public final a()V
    .registers 7

    .prologue
    .line 63
    iget-boolean v0, p0, Landroid/support/v7/internal/view/i;->c:Z

    if-eqz v0, :cond_5

    .line 78
    :goto_4
    return-void

    .line 64
    :cond_5
    iget-object v0, p0, Landroid/support/v7/internal/view/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/fk;

    .line 65
    iget-wide v2, p0, Landroid/support/v7/internal/view/i;->d:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_24

    .line 66
    iget-wide v2, p0, Landroid/support/v7/internal/view/i;->d:J

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    .line 68
    :cond_24
    iget-object v2, p0, Landroid/support/v7/internal/view/i;->e:Landroid/view/animation/Interpolator;

    if-eqz v2, :cond_2d

    .line 69
    iget-object v2, p0, Landroid/support/v7/internal/view/i;->e:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/fk;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;

    .line 71
    :cond_2d
    iget-object v2, p0, Landroid/support/v7/internal/view/i;->b:Landroid/support/v4/view/gd;

    if-eqz v2, :cond_36

    .line 72
    iget-object v2, p0, Landroid/support/v7/internal/view/i;->f:Landroid/support/v4/view/ge;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    .line 74
    :cond_36
    invoke-virtual {v0}, Landroid/support/v4/view/fk;->b()V

    goto :goto_b

    .line 77
    :cond_3a
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/view/i;->c:Z

    goto :goto_4
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 85
    iget-boolean v0, p0, Landroid/support/v7/internal/view/i;->c:Z

    if-nez v0, :cond_5

    .line 92
    :goto_4
    return-void

    .line 88
    :cond_5
    iget-object v0, p0, Landroid/support/v7/internal/view/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/fk;

    .line 89
    invoke-virtual {v0}, Landroid/support/v4/view/fk;->a()V

    goto :goto_b

    .line 91
    :cond_1b
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/i;->c:Z

    goto :goto_4
.end method

.method public final c()Landroid/support/v7/internal/view/i;
    .registers 3

    .prologue
    .line 95
    iget-boolean v0, p0, Landroid/support/v7/internal/view/i;->c:Z

    if-nez v0, :cond_8

    .line 96
    const-wide/16 v0, 0xfa

    iput-wide v0, p0, Landroid/support/v7/internal/view/i;->d:J

    .line 98
    :cond_8
    return-object p0
.end method
