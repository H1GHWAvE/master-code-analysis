.class public Landroid/support/v7/internal/a/l;
.super Landroid/support/v7/app/a;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/widget/j;


# static fields
.field private static final L:I = -0x1

.field private static final M:J = 0x64L

.field private static final N:J = 0xc8L

.field static final synthetic r:Z

.field private static final s:Ljava/lang/String; = "WindowDecorActionBar"

.field private static final t:Landroid/view/animation/Interpolator;

.field private static final u:Landroid/view/animation/Interpolator;

.field private static final v:Z


# instance fields
.field private A:Landroid/support/v7/internal/widget/ActionBarContainer;

.field private B:Landroid/support/v7/internal/widget/ad;

.field private C:Landroid/support/v7/internal/widget/ActionBarContextView;

.field private D:Landroid/view/View;

.field private E:Landroid/support/v7/internal/widget/al;

.field private F:Ljava/util/ArrayList;

.field private G:Landroid/support/v7/internal/a/q;

.field private H:I

.field private I:Z

.field private J:Z

.field private K:Ljava/util/ArrayList;

.field private O:Z

.field private P:I

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Z

.field private U:Z

.field private V:Landroid/support/v7/internal/view/i;

.field private W:Z

.field i:Landroid/content/Context;

.field j:Landroid/support/v7/internal/a/p;

.field k:Landroid/support/v7/c/a;

.field l:Landroid/support/v7/c/b;

.field m:Z

.field n:Landroid/support/v7/internal/widget/av;

.field final o:Landroid/support/v4/view/gd;

.field final p:Landroid/support/v4/view/gd;

.field final q:Landroid/support/v4/view/gf;

.field private w:Landroid/content/Context;

.field private x:Landroid/app/Activity;

.field private y:Landroid/app/Dialog;

.field private z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    const-class v0, Landroid/support/v7/internal/a/l;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_24

    move v0, v1

    :goto_b
    sput-boolean v0, Landroid/support/v7/internal/a/l;->r:Z

    .line 79
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sput-object v0, Landroid/support/v7/internal/a/l;->t:Landroid/view/animation/Interpolator;

    .line 80
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Landroid/support/v7/internal/a/l;->u:Landroid/view/animation/Interpolator;

    .line 85
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v0, v3, :cond_26

    :goto_21
    sput-boolean v1, Landroid/support/v7/internal/a/l;->v:Z

    return-void

    :cond_24
    move v0, v2

    .line 75
    goto :goto_b

    :cond_26
    move v1, v2

    .line 85
    goto :goto_21
.end method

.method public constructor <init>(Landroid/app/Activity;Z)V
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 171
    invoke-direct {p0}, Landroid/support/v7/app/a;-><init>()V

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/internal/a/l;->H:I

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->K:Ljava/util/ArrayList;

    .line 122
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/internal/a/l;->P:I

    .line 124
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->Q:Z

    .line 129
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->U:Z

    .line 137
    new-instance v0, Landroid/support/v7/internal/a/m;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/m;-><init>(Landroid/support/v7/internal/a/l;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->o:Landroid/support/v4/view/gd;

    .line 154
    new-instance v0, Landroid/support/v7/internal/a/n;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/n;-><init>(Landroid/support/v7/internal/a/l;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->p:Landroid/support/v4/view/gd;

    .line 162
    new-instance v0, Landroid/support/v7/internal/a/o;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/o;-><init>(Landroid/support/v7/internal/a/l;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->q:Landroid/support/v4/view/gf;

    .line 172
    iput-object p1, p0, Landroid/support/v7/internal/a/l;->x:Landroid/app/Activity;

    .line 173
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 175
    invoke-direct {p0, v0}, Landroid/support/v7/internal/a/l;->b(Landroid/view/View;)V

    .line 176
    if-nez p2, :cond_49

    .line 177
    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    .line 179
    :cond_49
    return-void
.end method

.method public constructor <init>(Landroid/app/Dialog;)V
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 181
    invoke-direct {p0}, Landroid/support/v7/app/a;-><init>()V

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/internal/a/l;->H:I

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->K:Ljava/util/ArrayList;

    .line 122
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/internal/a/l;->P:I

    .line 124
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->Q:Z

    .line 129
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->U:Z

    .line 137
    new-instance v0, Landroid/support/v7/internal/a/m;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/m;-><init>(Landroid/support/v7/internal/a/l;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->o:Landroid/support/v4/view/gd;

    .line 154
    new-instance v0, Landroid/support/v7/internal/a/n;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/n;-><init>(Landroid/support/v7/internal/a/l;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->p:Landroid/support/v4/view/gd;

    .line 162
    new-instance v0, Landroid/support/v7/internal/a/o;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/o;-><init>(Landroid/support/v7/internal/a/l;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->q:Landroid/support/v4/view/gf;

    .line 182
    iput-object p1, p0, Landroid/support/v7/internal/a/l;->y:Landroid/app/Dialog;

    .line 183
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/internal/a/l;->b(Landroid/view/View;)V

    .line 184
    return-void
.end method

.method private constructor <init>(Landroid/view/View;)V
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 190
    invoke-direct {p0}, Landroid/support/v7/app/a;-><init>()V

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/internal/a/l;->H:I

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->K:Ljava/util/ArrayList;

    .line 122
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/internal/a/l;->P:I

    .line 124
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->Q:Z

    .line 129
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->U:Z

    .line 137
    new-instance v0, Landroid/support/v7/internal/a/m;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/m;-><init>(Landroid/support/v7/internal/a/l;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->o:Landroid/support/v4/view/gd;

    .line 154
    new-instance v0, Landroid/support/v7/internal/a/n;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/n;-><init>(Landroid/support/v7/internal/a/l;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->p:Landroid/support/v4/view/gd;

    .line 162
    new-instance v0, Landroid/support/v7/internal/a/o;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/o;-><init>(Landroid/support/v7/internal/a/l;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->q:Landroid/support/v4/view/gf;

    .line 191
    sget-boolean v0, Landroid/support/v7/internal/a/l;->r:Z

    if-nez v0, :cond_41

    invoke-virtual {p1}, Landroid/view/View;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_41

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 192
    :cond_41
    invoke-direct {p0, p1}, Landroid/support/v7/internal/a/l;->b(Landroid/view/View;)V

    .line 193
    return-void
.end method

.method private E()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 288
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    if-eqz v0, :cond_6

    .line 309
    :goto_5
    return-void

    .line 292
    :cond_6
    new-instance v0, Landroid/support/v7/internal/widget/al;

    iget-object v1, p0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v7/internal/widget/al;-><init>(Landroid/content/Context;)V

    .line 294
    iget-boolean v1, p0, Landroid/support/v7/internal/a/l;->O:Z

    if-eqz v1, :cond_1c

    .line 295
    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/al;->setVisibility(I)V

    .line 296
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v1, v0}, Landroid/support/v7/internal/widget/ad;->a(Landroid/support/v7/internal/widget/al;)V

    .line 308
    :goto_19
    iput-object v0, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    goto :goto_5

    .line 298
    :cond_1c
    invoke-virtual {p0}, Landroid/support/v7/internal/a/l;->g()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_35

    .line 299
    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/al;->setVisibility(I)V

    .line 300
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_2f

    .line 301
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v1}, Landroid/support/v4/view/cx;->t(Landroid/view/View;)V

    .line 306
    :cond_2f
    :goto_2f
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTabContainer(Landroid/support/v7/internal/widget/al;)V

    goto :goto_19

    .line 304
    :cond_35
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/al;->setVisibility(I)V

    goto :goto_2f
.end method

.method private F()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 312
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->l:Landroid/support/v7/c/b;

    if-eqz v0, :cond_10

    .line 313
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->l:Landroid/support/v7/c/b;

    iget-object v1, p0, Landroid/support/v7/internal/a/l;->k:Landroid/support/v7/c/a;

    invoke-interface {v0, v1}, Landroid/support/v7/c/b;->a(Landroid/support/v7/c/a;)V

    .line 314
    iput-object v2, p0, Landroid/support/v7/internal/a/l;->k:Landroid/support/v7/c/a;

    .line 315
    iput-object v2, p0, Landroid/support/v7/internal/a/l;->l:Landroid/support/v7/c/b;

    .line 317
    :cond_10
    return-void
.end method

.method private G()V
    .registers 3

    .prologue
    .line 422
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    if-eqz v0, :cond_8

    .line 423
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/app/g;)V

    .line 425
    :cond_8
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 426
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    if-eqz v0, :cond_2e

    .line 427
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    .line 6359
    iget-object v0, v1, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj;->removeAllViews()V

    .line 6360
    iget-object v0, v1, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_27

    .line 6361
    iget-object v0, v1, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 6363
    :cond_27
    iget-boolean v0, v1, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_2e

    .line 6364
    invoke-virtual {v1}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 429
    :cond_2e
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/internal/a/l;->H:I

    .line 430
    return-void
.end method

.method private H()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 653
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->T:Z

    if-nez v0, :cond_14

    .line 654
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->T:Z

    .line 655
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_10

    .line 656
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setShowingForActionMode(Z)V

    .line 658
    :cond_10
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/internal/a/l;->l(Z)V

    .line 660
    :cond_14
    return-void
.end method

.method private I()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 678
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->T:Z

    if-eqz v0, :cond_13

    .line 679
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->T:Z

    .line 680
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_10

    .line 681
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setShowingForActionMode(Z)V

    .line 683
    :cond_10
    invoke-direct {p0, v1}, Landroid/support/v7/internal/a/l;->l(Z)V

    .line 685
    :cond_13
    return-void
.end method

.method private J()Z
    .registers 2

    .prologue
    .line 1317
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->i()Z

    move-result v0

    return v0
.end method

.method private K()Z
    .registers 2

    .prologue
    .line 1331
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->j()Z

    move-result v0

    return v0
.end method

.method private L()Landroid/support/v7/internal/widget/av;
    .registers 2

    .prologue
    .line 1341
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->n:Landroid/support/v7/internal/widget/av;

    if-nez v0, :cond_c

    .line 1342
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->n:Landroid/support/v7/internal/widget/av;

    .line 1344
    :cond_c
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->n:Landroid/support/v7/internal/widget/av;

    return-object v0
.end method

.method static synthetic a(Landroid/support/v7/internal/a/l;)Z
    .registers 2

    .prologue
    .line 75
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->Q:Z

    return v0
.end method

.method static synthetic a(ZZ)Z
    .registers 3

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/support/v7/internal/a/l;->a(ZZZ)Z

    move-result v0

    return v0
.end method

.method private static a(ZZZ)Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 725
    if-eqz p2, :cond_4

    .line 730
    :cond_3
    :goto_3
    return v0

    .line 727
    :cond_4
    if-nez p0, :cond_8

    if-eqz p1, :cond_3

    .line 728
    :cond_8
    const/4 v0, 0x0

    goto :goto_3
.end method

.method static synthetic b(Landroid/support/v7/internal/a/l;)Landroid/view/View;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    return-object v0
.end method

.method private b(Landroid/support/v7/app/g;I)V
    .registers 6

    .prologue
    .line 512
    check-cast p1, Landroid/support/v7/internal/a/q;

    .line 7124
    iget-object v0, p1, Landroid/support/v7/internal/a/q;->b:Landroid/support/v7/app/h;

    .line 515
    if-nez v0, :cond_e

    .line 516
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action Bar Tab must have a Callback"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7164
    :cond_e
    iput p2, p1, Landroid/support/v7/internal/a/q;->c:I

    .line 520
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 522
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 523
    add-int/lit8 v0, p2, 0x1

    move v1, v0

    :goto_1e
    if-ge v1, v2, :cond_2e

    .line 524
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/a/q;

    .line 8164
    iput v1, v0, Landroid/support/v7/internal/a/q;->c:I

    .line 523
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1e

    .line 526
    :cond_2e
    return-void
.end method

.method private b(Landroid/view/View;)V
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 196
    sget v0, Landroid/support/v7/a/i;->decor_content_parent:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    .line 197
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_15

    .line 198
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setActionBarVisibilityCallback(Landroid/support/v7/internal/widget/j;)V

    .line 200
    :cond_15
    sget v0, Landroid/support/v7/a/i;->action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2238
    instance-of v3, v0, Landroid/support/v7/internal/widget/ad;

    if-eqz v3, :cond_64

    .line 2239
    check-cast v0, Landroid/support/v7/internal/widget/ad;

    .line 200
    :goto_21
    iput-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    .line 201
    sget v0, Landroid/support/v7/a/i;->action_context_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->C:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 203
    sget v0, Landroid/support/v7/a/i;->action_bar_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    .line 206
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    if-eqz v0, :cond_43

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->C:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_43

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-nez v0, :cond_91

    .line 207
    :cond_43
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can only be used with a compatible window decor layout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2240
    :cond_64
    instance-of v3, v0, Landroid/support/v7/widget/Toolbar;

    if-eqz v3, :cond_6f

    .line 2241
    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getWrapper()Landroid/support/v7/internal/widget/ad;

    move-result-object v0

    goto :goto_21

    .line 2243
    :cond_6f
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t make a decor toolbar out of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8e

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    :goto_8a
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8e
    const-string v0, "null"

    goto :goto_8a

    .line 211
    :cond_91
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->b()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    .line 214
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->r()I

    move-result v0

    .line 215
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_de

    move v0, v1

    .line 216
    :goto_a4
    if-eqz v0, :cond_a8

    .line 217
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->I:Z

    .line 220
    :cond_a8
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v7/internal/view/a;->a(Landroid/content/Context;)Landroid/support/v7/internal/view/a;

    move-result-object v0

    .line 3089
    iget-object v3, v0, Landroid/support/v7/internal/view/a;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    .line 222
    invoke-virtual {v0}, Landroid/support/v7/internal/view/a;->a()Z

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/internal/a/l;->k(Z)V

    .line 224
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    const/4 v3, 0x0

    sget-object v4, Landroid/support/v7/a/n;->ActionBar:[I

    sget v5, Landroid/support/v7/a/d;->actionBarStyle:I

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 227
    sget v3, Landroid/support/v7/a/n;->ActionBar_hideOnContentScroll:I

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    if-eqz v3, :cond_e7

    .line 3696
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    .line 4188
    iget-boolean v3, v3, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a:Z

    .line 3696
    if-nez v3, :cond_e0

    .line 3697
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_de
    move v0, v2

    .line 215
    goto :goto_a4

    .line 3700
    :cond_e0
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->m:Z

    .line 3701
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v3, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V

    .line 230
    :cond_e7
    sget v1, Landroid/support/v7/a/n;->ActionBar_elevation:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 231
    if-eqz v1, :cond_f5

    .line 232
    int-to-float v1, v1

    .line 4250
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2, v1}, Landroid/support/v4/view/cx;->f(Landroid/view/View;F)V

    .line 234
    :cond_f5
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 235
    return-void
.end method

.method static synthetic c(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContainer;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    return-object v0
.end method

.method private static c(Landroid/view/View;)Landroid/support/v7/internal/widget/ad;
    .registers 4

    .prologue
    .line 238
    instance-of v0, p0, Landroid/support/v7/internal/widget/ad;

    if-eqz v0, :cond_7

    .line 239
    check-cast p0, Landroid/support/v7/internal/widget/ad;

    .line 241
    :goto_6
    return-object p0

    .line 240
    :cond_7
    instance-of v0, p0, Landroid/support/v7/widget/Toolbar;

    if-eqz v0, :cond_12

    .line 241
    check-cast p0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getWrapper()Landroid/support/v7/internal/widget/ad;

    move-result-object p0

    goto :goto_6

    .line 243
    :cond_12
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t make a decor toolbar out of "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_31

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    :goto_2d
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_31
    const-string v0, "null"

    goto :goto_2d
.end method

.method static synthetic d(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/view/i;
    .registers 2

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    return-object v0
.end method

.method static synthetic e(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    return-object v0
.end method

.method static synthetic f(Landroid/support/v7/internal/a/l;)Z
    .registers 2

    .prologue
    .line 75
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->R:Z

    return v0
.end method

.method static synthetic g(Landroid/support/v7/internal/a/l;)Z
    .registers 2

    .prologue
    .line 75
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->S:Z

    return v0
.end method

.method static synthetic h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->C:Landroid/support/v7/internal/widget/ActionBarContextView;

    return-object v0
.end method

.method static synthetic i(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ad;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    return-object v0
.end method

.method static synthetic j(Landroid/support/v7/internal/a/l;)Landroid/content/Context;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    return-object v0
.end method

.method private k(Z)V
    .registers 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 263
    iput-boolean p1, p0, Landroid/support/v7/internal/a/l;->O:Z

    .line 265
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->O:Z

    if-nez v0, :cond_49

    .line 266
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, v3}, Landroid/support/v7/internal/widget/ad;->a(Landroid/support/v7/internal/widget/al;)V

    .line 267
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v3, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTabContainer(Landroid/support/v7/internal/widget/al;)V

    .line 272
    :goto_15
    invoke-virtual {p0}, Landroid/support/v7/internal/a/l;->g()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_56

    move v0, v1

    .line 273
    :goto_1d
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    if-eqz v3, :cond_31

    .line 274
    if-eqz v0, :cond_58

    .line 275
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    invoke-virtual {v3, v2}, Landroid/support/v7/internal/widget/al;->setVisibility(I)V

    .line 276
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v3, :cond_31

    .line 277
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v3}, Landroid/support/v4/view/cx;->t(Landroid/view/View;)V

    .line 283
    :cond_31
    :goto_31
    iget-object v4, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    iget-boolean v3, p0, Landroid/support/v7/internal/a/l;->O:Z

    if-nez v3, :cond_60

    if-eqz v0, :cond_60

    move v3, v1

    :goto_3a
    invoke-interface {v4, v3}, Landroid/support/v7/internal/widget/ad;->a(Z)V

    .line 284
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-boolean v4, p0, Landroid/support/v7/internal/a/l;->O:Z

    if-nez v4, :cond_62

    if-eqz v0, :cond_62

    :goto_45
    invoke-virtual {v3, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHasNonEmbeddedTabs(Z)V

    .line 285
    return-void

    .line 269
    :cond_49
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTabContainer(Landroid/support/v7/internal/widget/al;)V

    .line 270
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    iget-object v3, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    invoke-interface {v0, v3}, Landroid/support/v7/internal/widget/ad;->a(Landroid/support/v7/internal/widget/al;)V

    goto :goto_15

    :cond_56
    move v0, v2

    .line 272
    goto :goto_1d

    .line 280
    :cond_58
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/support/v7/internal/widget/al;->setVisibility(I)V

    goto :goto_31

    :cond_60
    move v3, v2

    .line 283
    goto :goto_3a

    :cond_62
    move v1, v2

    .line 284
    goto :goto_45
.end method

.method private l(Z)V
    .registers 10

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 736
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->R:Z

    iget-boolean v1, p0, Landroid/support/v7/internal/a/l;->S:Z

    iget-boolean v2, p0, Landroid/support/v7/internal/a/l;->T:Z

    invoke-static {v0, v1, v2}, Landroid/support/v7/internal/a/l;->a(ZZZ)Z

    move-result v0

    .line 739
    if-eqz v0, :cond_c1

    .line 740
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->U:Z

    if-nez v0, :cond_a2

    .line 741
    iput-boolean v5, p0, Landroid/support/v7/internal/a/l;->U:Z

    .line 18753
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    if-eqz v0, :cond_21

    .line 18754
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/i;->b()V

    .line 18756
    :cond_21
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v6}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 18758
    iget v0, p0, Landroid/support/v7/internal/a/l;->P:I

    if-nez v0, :cond_a3

    sget-boolean v0, Landroid/support/v7/internal/a/l;->v:Z

    if-eqz v0, :cond_a3

    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->W:Z

    if-nez v0, :cond_34

    if-eqz p1, :cond_a3

    .line 18761
    :cond_34
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 18762
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 18763
    if-eqz p1, :cond_51

    .line 18764
    new-array v1, v7, [I

    fill-array-data v1, :array_148

    .line 18765
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    .line 18766
    aget v1, v1, v5

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 18768
    :cond_51
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v1, v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 18769
    new-instance v1, Landroid/support/v7/internal/view/i;

    invoke-direct {v1}, Landroid/support/v7/internal/view/i;-><init>()V

    .line 18770
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v2

    .line 18771
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->q:Landroid/support/v4/view/gf;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gf;)Landroid/support/v4/view/fk;

    .line 18772
    invoke-virtual {v1, v2}, Landroid/support/v7/internal/view/i;->a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;

    .line 18773
    iget-boolean v2, p0, Landroid/support/v7/internal/a/l;->Q:Z

    if-eqz v2, :cond_87

    iget-object v2, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    if-eqz v2, :cond_87

    .line 18774
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    invoke-static {v2, v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 18775
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/i;->a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;

    .line 18777
    :cond_87
    sget-object v0, Landroid/support/v7/internal/a/l;->u:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/i;->a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/i;

    .line 18778
    invoke-virtual {v1}, Landroid/support/v7/internal/view/i;->c()Landroid/support/v7/internal/view/i;

    .line 18786
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->p:Landroid/support/v4/view/gd;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/i;->a(Landroid/support/v4/view/gd;)Landroid/support/v7/internal/view/i;

    .line 18787
    iput-object v1, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    .line 18788
    invoke-virtual {v1}, Landroid/support/v7/internal/view/i;->a()V

    .line 18797
    :goto_99
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_a2

    .line 18798
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v0}, Landroid/support/v4/view/cx;->t(Landroid/view/View;)V

    .line 18829
    :cond_a2
    :goto_a2
    return-void

    .line 18790
    :cond_a3
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v3}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    .line 18791
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 18792
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->Q:Z

    if-eqz v0, :cond_ba

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    if-eqz v0, :cond_ba

    .line 18793
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    invoke-static {v0, v4}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 18795
    :cond_ba
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->p:Landroid/support/v4/view/gd;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/support/v4/view/gd;->b(Landroid/view/View;)V

    goto :goto_99

    .line 745
    :cond_c1
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->U:Z

    if-eqz v0, :cond_a2

    .line 746
    iput-boolean v6, p0, Landroid/support/v7/internal/a/l;->U:Z

    .line 18803
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    if-eqz v0, :cond_d0

    .line 18804
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/i;->b()V

    .line 18807
    :cond_d0
    iget v0, p0, Landroid/support/v7/internal/a/l;->P:I

    if-nez v0, :cond_140

    sget-boolean v0, Landroid/support/v7/internal/a/l;->v:Z

    if-eqz v0, :cond_140

    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->W:Z

    if-nez v0, :cond_de

    if-eqz p1, :cond_140

    .line 18809
    :cond_de
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v3}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    .line 18810
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTransitioning(Z)V

    .line 18811
    new-instance v1, Landroid/support/v7/internal/view/i;

    invoke-direct {v1}, Landroid/support/v7/internal/view/i;-><init>()V

    .line 18812
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 18813
    if-eqz p1, :cond_105

    .line 18814
    new-array v2, v7, [I

    fill-array-data v2, :array_150

    .line 18815
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v3, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    .line 18816
    aget v2, v2, v5

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 18818
    :cond_105
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v2

    .line 18819
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->q:Landroid/support/v4/view/gf;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gf;)Landroid/support/v4/view/fk;

    .line 18820
    invoke-virtual {v1, v2}, Landroid/support/v7/internal/view/i;->a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;

    .line 18821
    iget-boolean v2, p0, Landroid/support/v7/internal/a/l;->Q:Z

    if-eqz v2, :cond_12c

    iget-object v2, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    if-eqz v2, :cond_12c

    .line 18822
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    invoke-static {v2}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/i;->a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;

    .line 18824
    :cond_12c
    sget-object v0, Landroid/support/v7/internal/a/l;->t:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/i;->a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/i;

    .line 18825
    invoke-virtual {v1}, Landroid/support/v7/internal/view/i;->c()Landroid/support/v7/internal/view/i;

    .line 18826
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->o:Landroid/support/v4/view/gd;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/i;->a(Landroid/support/v4/view/gd;)Landroid/support/v7/internal/view/i;

    .line 18827
    iput-object v1, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    .line 18828
    invoke-virtual {v1}, Landroid/support/v7/internal/view/i;->a()V

    goto/16 :goto_a2

    .line 18830
    :cond_140
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->o:Landroid/support/v4/view/gd;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/support/v4/view/gd;->b(Landroid/view/View;)V

    goto/16 :goto_a2

    .line 18764
    :array_148
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 18814
    :array_150
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private m(Z)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 753
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    if-eqz v0, :cond_a

    .line 754
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/i;->b()V

    .line 756
    :cond_a
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 758
    iget v0, p0, Landroid/support/v7/internal/a/l;->P:I

    if-nez v0, :cond_8f

    sget-boolean v0, Landroid/support/v7/internal/a/l;->v:Z

    if-eqz v0, :cond_8f

    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->W:Z

    if-nez v0, :cond_1e

    if-eqz p1, :cond_8f

    .line 761
    :cond_1e
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 762
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 763
    if-eqz p1, :cond_3d

    .line 764
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_b0

    .line 765
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    .line 766
    const/4 v2, 0x1

    aget v1, v1, v2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 768
    :cond_3d
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v1, v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 769
    new-instance v1, Landroid/support/v7/internal/view/i;

    invoke-direct {v1}, Landroid/support/v7/internal/view/i;-><init>()V

    .line 770
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v2

    .line 771
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->q:Landroid/support/v4/view/gf;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gf;)Landroid/support/v4/view/fk;

    .line 772
    invoke-virtual {v1, v2}, Landroid/support/v7/internal/view/i;->a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;

    .line 773
    iget-boolean v2, p0, Landroid/support/v7/internal/a/l;->Q:Z

    if-eqz v2, :cond_73

    iget-object v2, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    if-eqz v2, :cond_73

    .line 774
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    invoke-static {v2, v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 775
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/i;->a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;

    .line 777
    :cond_73
    sget-object v0, Landroid/support/v7/internal/a/l;->u:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/i;->a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/i;

    .line 778
    invoke-virtual {v1}, Landroid/support/v7/internal/view/i;->c()Landroid/support/v7/internal/view/i;

    .line 786
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->p:Landroid/support/v4/view/gd;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/i;->a(Landroid/support/v4/view/gd;)Landroid/support/v7/internal/view/i;

    .line 787
    iput-object v1, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    .line 788
    invoke-virtual {v1}, Landroid/support/v7/internal/view/i;->a()V

    .line 797
    :goto_85
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_8e

    .line 798
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v0}, Landroid/support/v4/view/cx;->t(Landroid/view/View;)V

    .line 800
    :cond_8e
    return-void

    .line 790
    :cond_8f
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    .line 791
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 792
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->Q:Z

    if-eqz v0, :cond_a8

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    if-eqz v0, :cond_a8

    .line 793
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    invoke-static {v0, v4}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 795
    :cond_a8
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->p:Landroid/support/v4/view/gd;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/support/v4/view/gd;->b(Landroid/view/View;)V

    goto :goto_85

    .line 764
    nop

    :array_b0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private n(Z)V
    .registers 7

    .prologue
    const/4 v4, 0x1

    .line 803
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    if-eqz v0, :cond_a

    .line 804
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/i;->b()V

    .line 807
    :cond_a
    iget v0, p0, Landroid/support/v7/internal/a/l;->P:I

    if-nez v0, :cond_7c

    sget-boolean v0, Landroid/support/v7/internal/a/l;->v:Z

    if-eqz v0, :cond_7c

    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->W:Z

    if-nez v0, :cond_18

    if-eqz p1, :cond_7c

    .line 809
    :cond_18
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    .line 810
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v4}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTransitioning(Z)V

    .line 811
    new-instance v1, Landroid/support/v7/internal/view/i;

    invoke-direct {v1}, Landroid/support/v7/internal/view/i;-><init>()V

    .line 812
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 813
    if-eqz p1, :cond_42

    .line 814
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_84

    .line 815
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v3, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    .line 816
    aget v2, v2, v4

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 818
    :cond_42
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v2

    .line 819
    iget-object v3, p0, Landroid/support/v7/internal/a/l;->q:Landroid/support/v4/view/gf;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gf;)Landroid/support/v4/view/fk;

    .line 820
    invoke-virtual {v1, v2}, Landroid/support/v7/internal/view/i;->a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;

    .line 821
    iget-boolean v2, p0, Landroid/support/v7/internal/a/l;->Q:Z

    if-eqz v2, :cond_69

    iget-object v2, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    if-eqz v2, :cond_69

    .line 822
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->D:Landroid/view/View;

    invoke-static {v2}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/i;->a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;

    .line 824
    :cond_69
    sget-object v0, Landroid/support/v7/internal/a/l;->t:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/i;->a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/i;

    .line 825
    invoke-virtual {v1}, Landroid/support/v7/internal/view/i;->c()Landroid/support/v7/internal/view/i;

    .line 826
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->o:Landroid/support/v4/view/gd;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/i;->a(Landroid/support/v4/view/gd;)Landroid/support/v7/internal/view/i;

    .line 827
    iput-object v1, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    .line 828
    invoke-virtual {v1}, Landroid/support/v7/internal/view/i;->a()V

    .line 832
    :goto_7b
    return-void

    .line 830
    :cond_7c
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->o:Landroid/support/v4/view/gd;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/support/v4/view/gd;->b(Landroid/view/View;)V

    goto :goto_7b

    .line 814
    nop

    :array_84
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public final A()V
    .registers 2

    .prologue
    .line 663
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->S:Z

    if-eqz v0, :cond_b

    .line 664
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/a/l;->S:Z

    .line 665
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v7/internal/a/l;->l(Z)V

    .line 667
    :cond_b
    return-void
.end method

.method public final B()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 688
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->S:Z

    if-nez v0, :cond_a

    .line 689
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->S:Z

    .line 690
    invoke-direct {p0, v1}, Landroid/support/v7/internal/a/l;->l(Z)V

    .line 692
    :cond_a
    return-void
.end method

.method public final C()V
    .registers 2

    .prologue
    .line 908
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    if-eqz v0, :cond_c

    .line 909
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/i;->b()V

    .line 910
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    .line 912
    :cond_c
    return-void
.end method

.method public final D()V
    .registers 1

    .prologue
    .line 916
    return-void
.end method

.method public final a()I
    .registers 3

    .prologue
    const/4 v0, -0x1

    .line 1243
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/ad;->v()I

    move-result v1

    packed-switch v1, :pswitch_data_1c

    .line 1249
    :cond_a
    :goto_a
    return v0

    .line 1245
    :pswitch_b
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    if-eqz v1, :cond_a

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    .line 22160
    iget v0, v0, Landroid/support/v7/internal/a/q;->c:I

    goto :goto_a

    .line 1247
    :pswitch_14
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->w()I

    move-result v0

    goto :goto_a

    .line 1243
    nop

    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_14
        :pswitch_b
    .end packed-switch
.end method

.method public final a(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;
    .registers 5

    .prologue
    .line 493
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->j:Landroid/support/v7/internal/a/p;

    if-eqz v0, :cond_9

    .line 494
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->j:Landroid/support/v7/internal/a/p;

    invoke-virtual {v0}, Landroid/support/v7/internal/a/p;->c()V

    .line 497
    :cond_9
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V

    .line 498
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->C:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->i()V

    .line 499
    new-instance v0, Landroid/support/v7/internal/a/p;

    iget-object v1, p0, Landroid/support/v7/internal/a/l;->C:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Landroid/support/v7/internal/a/p;-><init>(Landroid/support/v7/internal/a/l;Landroid/content/Context;Landroid/support/v7/c/b;)V

    .line 500
    invoke-virtual {v0}, Landroid/support/v7/internal/a/p;->e()Z

    move-result v1

    if-eqz v1, :cond_3b

    .line 501
    invoke-virtual {v0}, Landroid/support/v7/internal/a/p;->d()V

    .line 502
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->C:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Landroid/support/v7/c/a;)V

    .line 503
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/a/l;->j(Z)V

    .line 504
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->C:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    .line 505
    iput-object v0, p0, Landroid/support/v7/internal/a/l;->j:Landroid/support/v7/internal/a/p;

    .line 508
    :goto_3a
    return-object v0

    :cond_3b
    const/4 v0, 0x0

    goto :goto_3a
.end method

.method public final a(F)V
    .registers 3

    .prologue
    .line 250
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, p1}, Landroid/support/v4/view/cx;->f(Landroid/view/View;F)V

    .line 251
    return-void
.end method

.method public final a(I)V
    .registers 5

    .prologue
    .line 359
    invoke-virtual {p0}, Landroid/support/v7/internal/a/l;->r()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/ad;->a()Landroid/view/ViewGroup;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/l;->a(Landroid/view/View;)V

    .line 361
    return-void
.end method

.method public final a(II)V
    .registers 7

    .prologue
    .line 453
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->r()I

    move-result v0

    .line 454
    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_d

    .line 455
    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->I:Z

    .line 457
    :cond_d
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-interface {v1, v0}, Landroid/support/v7/internal/widget/ad;->c(I)V

    .line 458
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .registers 3

    .prologue
    .line 259
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v7/internal/view/a;->a(Landroid/content/Context;)Landroid/support/v7/internal/view/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/view/a;->a()Z

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/internal/a/l;->k(Z)V

    .line 260
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 1313
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1314
    return-void
.end method

.method public final a(Landroid/support/v7/app/e;)V
    .registers 3

    .prologue
    .line 338
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->K:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 339
    return-void
.end method

.method public final a(Landroid/support/v7/app/g;)V
    .registers 7

    .prologue
    .line 530
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    .line 8540
    invoke-direct {p0}, Landroid/support/v7/internal/a/l;->E()V

    .line 8541
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    .line 9309
    const/4 v0, 0x0

    invoke-virtual {v2, p1, v0}, Landroid/support/v7/internal/widget/al;->a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;

    move-result-object v3

    .line 9310
    iget-object v0, v2, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    new-instance v4, Landroid/support/v7/widget/al;

    invoke-direct {v4}, Landroid/support/v7/widget/al;-><init>()V

    invoke-virtual {v0, v3, v4}, Landroid/support/v7/widget/aj;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 9312
    iget-object v0, v2, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_29

    .line 9313
    iget-object v0, v2, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 9315
    :cond_29
    if-eqz v1, :cond_2f

    .line 9316
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/ap;->setSelected(Z)V

    .line 9318
    :cond_2f
    iget-boolean v0, v2, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_36

    .line 9319
    invoke-virtual {v2}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 8542
    :cond_36
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/a/l;->b(Landroid/support/v7/app/g;I)V

    .line 8543
    if-eqz v1, :cond_44

    .line 8544
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/app/g;)V

    .line 531
    :cond_44
    return-void
.end method

.method public final a(Landroid/support/v7/app/g;I)V
    .registers 8

    .prologue
    .line 535
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    .line 9550
    invoke-direct {p0}, Landroid/support/v7/internal/a/l;->E()V

    .line 9551
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    .line 10324
    const/4 v0, 0x0

    invoke-virtual {v2, p1, v0}, Landroid/support/v7/internal/widget/al;->a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;

    move-result-object v3

    .line 10325
    iget-object v0, v2, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    new-instance v4, Landroid/support/v7/widget/al;

    invoke-direct {v4}, Landroid/support/v7/widget/al;-><init>()V

    invoke-virtual {v0, v3, p2, v4}, Landroid/support/v7/widget/aj;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 10327
    iget-object v0, v2, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_29

    .line 10328
    iget-object v0, v2, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 10330
    :cond_29
    if-eqz v1, :cond_2f

    .line 10331
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/ap;->setSelected(Z)V

    .line 10333
    :cond_2f
    iget-boolean v0, v2, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_36

    .line 10334
    invoke-virtual {v2}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 9552
    :cond_36
    invoke-direct {p0, p1, p2}, Landroid/support/v7/internal/a/l;->b(Landroid/support/v7/app/g;I)V

    .line 9553
    if-eqz v1, :cond_3e

    .line 9554
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/app/g;)V

    .line 536
    :cond_3e
    return-void
.end method

.method public final a(Landroid/support/v7/app/g;IZ)V
    .registers 8

    .prologue
    .line 550
    invoke-direct {p0}, Landroid/support/v7/internal/a/l;->E()V

    .line 551
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    .line 11324
    const/4 v0, 0x0

    invoke-virtual {v1, p1, v0}, Landroid/support/v7/internal/widget/al;->a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;

    move-result-object v2

    .line 11325
    iget-object v0, v1, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    new-instance v3, Landroid/support/v7/widget/al;

    invoke-direct {v3}, Landroid/support/v7/widget/al;-><init>()V

    invoke-virtual {v0, v2, p2, v3}, Landroid/support/v7/widget/aj;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 11327
    iget-object v0, v1, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_23

    .line 11328
    iget-object v0, v1, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 11330
    :cond_23
    if-eqz p3, :cond_29

    .line 11331
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/support/v7/internal/widget/ap;->setSelected(Z)V

    .line 11333
    :cond_29
    iget-boolean v0, v1, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_30

    .line 11334
    invoke-virtual {v1}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 552
    :cond_30
    invoke-direct {p0, p1, p2}, Landroid/support/v7/internal/a/l;->b(Landroid/support/v7/app/g;I)V

    .line 553
    if-eqz p3, :cond_38

    .line 554
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/app/g;)V

    .line 556
    :cond_38
    return-void
.end method

.method public final a(Landroid/support/v7/app/g;Z)V
    .registers 7

    .prologue
    .line 540
    invoke-direct {p0}, Landroid/support/v7/internal/a/l;->E()V

    .line 541
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    .line 11309
    const/4 v0, 0x0

    invoke-virtual {v1, p1, v0}, Landroid/support/v7/internal/widget/al;->a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;

    move-result-object v2

    .line 11310
    iget-object v0, v1, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    new-instance v3, Landroid/support/v7/widget/al;

    invoke-direct {v3}, Landroid/support/v7/widget/al;-><init>()V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/widget/aj;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 11312
    iget-object v0, v1, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_23

    .line 11313
    iget-object v0, v1, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 11315
    :cond_23
    if-eqz p2, :cond_29

    .line 11316
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/support/v7/internal/widget/ap;->setSelected(Z)V

    .line 11318
    :cond_29
    iget-boolean v0, v1, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_30

    .line 11319
    invoke-virtual {v1}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 542
    :cond_30
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/a/l;->b(Landroid/support/v7/app/g;I)V

    .line 543
    if-eqz p2, :cond_3e

    .line 544
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/app/g;)V

    .line 546
    :cond_3e
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 1227
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->a(Landroid/view/View;)V

    .line 1228
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/support/v7/app/c;)V
    .registers 4

    .prologue
    .line 1232
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1233
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->a(Landroid/view/View;)V

    .line 1234
    return-void
.end method

.method public final a(Landroid/widget/SpinnerAdapter;Landroid/support/v7/app/f;)V
    .registers 5

    .prologue
    .line 1238
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    new-instance v1, Landroid/support/v7/internal/a/b;

    invoke-direct {v1, p2}, Landroid/support/v7/internal/a/b;-><init>(Landroid/support/v7/app/f;)V

    invoke-interface {v0, p1, v1}, Landroid/support/v7/internal/widget/ad;->a(Landroid/widget/SpinnerAdapter;Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1239
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 433
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->b(Ljava/lang/CharSequence;)V

    .line 434
    return-void
.end method

.method public final a(Z)V
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 365
    if-eqz p1, :cond_8

    move v0, v1

    :goto_4
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/a/l;->a(II)V

    .line 366
    return-void

    .line 365
    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 1255
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->v()I

    move-result v0

    packed-switch v0, :pswitch_data_1a

    .line 1261
    const/4 v0, 0x0

    :goto_a
    return v0

    .line 1257
    :pswitch_b
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_a

    .line 1259
    :pswitch_12
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->x()I

    move-result v0

    goto :goto_a

    .line 1255
    nop

    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_12
        :pswitch_b
    .end packed-switch
.end method

.method public final b(I)V
    .registers 3

    .prologue
    .line 1308
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->a(I)V

    .line 1309
    return-void
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 1327
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1328
    return-void
.end method

.method public final b(Landroid/support/v7/app/e;)V
    .registers 3

    .prologue
    .line 342
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->K:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 343
    return-void
.end method

.method public final b(Landroid/support/v7/app/g;)V
    .registers 7

    .prologue
    .line 565
    invoke-virtual {p1}, Landroid/support/v7/app/g;->a()I

    move-result v3

    .line 11570
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    if-eqz v0, :cond_64

    .line 11575
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    if-eqz v0, :cond_52

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    .line 12160
    iget v0, v0, Landroid/support/v7/internal/a/q;->c:I

    move v1, v0

    .line 11577
    :goto_11
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    .line 12349
    iget-object v0, v2, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/aj;->removeViewAt(I)V

    .line 12350
    iget-object v0, v2, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_27

    .line 12351
    iget-object v0, v2, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 12353
    :cond_27
    iget-boolean v0, v2, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_2e

    .line 12354
    invoke-virtual {v2}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 11578
    :cond_2e
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/a/q;

    .line 11579
    if-eqz v0, :cond_3b

    .line 13164
    const/4 v2, -0x1

    iput v2, v0, Landroid/support/v7/internal/a/q;->c:I

    .line 11583
    :cond_3b
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    .line 11584
    :goto_42
    if-ge v2, v4, :cond_56

    .line 11585
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/a/q;

    .line 14164
    iput v2, v0, Landroid/support/v7/internal/a/q;->c:I

    .line 11584
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_42

    .line 11575
    :cond_52
    iget v0, p0, Landroid/support/v7/internal/a/l;->H:I

    move v1, v0

    goto :goto_11

    .line 11588
    :cond_56
    if-ne v1, v3, :cond_64

    .line 11589
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_65

    const/4 v0, 0x0

    :goto_61
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/app/g;)V

    .line 566
    :cond_64
    return-void

    .line 11589
    :cond_65
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    const/4 v1, 0x0

    add-int/lit8 v2, v3, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/a/q;

    goto :goto_61
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 442
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->c(Ljava/lang/CharSequence;)V

    .line 443
    return-void
.end method

.method public final b(Z)V
    .registers 4

    .prologue
    const/4 v1, 0x2

    .line 370
    if-eqz p1, :cond_8

    move v0, v1

    :goto_4
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/a/l;->a(II)V

    .line 371
    return-void

    .line 370
    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final c()V
    .registers 1

    .prologue
    .line 470
    return-void
.end method

.method public final c(I)V
    .registers 3

    .prologue
    .line 1322
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->b(I)V

    .line 1323
    return-void
.end method

.method public final c(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 461
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContainer;->setPrimaryBackground(Landroid/graphics/drawable/Drawable;)V

    .line 462
    return-void
.end method

.method public final c(Landroid/support/v7/app/g;)V
    .registers 5

    .prologue
    const/4 v1, -0x1

    .line 595
    invoke-virtual {p0}, Landroid/support/v7/internal/a/l;->g()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_13

    .line 596
    if-eqz p1, :cond_11

    invoke-virtual {p1}, Landroid/support/v7/app/g;->a()I

    move-result v0

    :goto_e
    iput v0, p0, Landroid/support/v7/internal/a/l;->H:I

    .line 628
    :cond_10
    :goto_10
    return-void

    :cond_11
    move v0, v1

    .line 596
    goto :goto_e

    .line 601
    :cond_13
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->x:Landroid/app/Activity;

    instance-of v0, v0, Landroid/support/v4/app/bb;

    if-eqz v0, :cond_52

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_52

    .line 603
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->x:Landroid/app/Activity;

    check-cast v0, Landroid/support/v4/app/bb;

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/cd;->h()Landroid/support/v4/app/cd;

    move-result-object v0

    .line 609
    :goto_35
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    if-ne v2, p1, :cond_54

    .line 610
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    if-eqz v1, :cond_46

    .line 612
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    invoke-virtual {p1}, Landroid/support/v7/app/g;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/al;->a(I)V

    .line 625
    :cond_46
    :goto_46
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Landroid/support/v4/app/cd;->l()Z

    move-result v1

    if-nez v1, :cond_10

    .line 626
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->i()I

    goto :goto_10

    .line 606
    :cond_52
    const/4 v0, 0x0

    goto :goto_35

    .line 615
    :cond_54
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    if-eqz p1, :cond_5c

    invoke-virtual {p1}, Landroid/support/v7/app/g;->a()I

    move-result v1

    :cond_5c
    invoke-virtual {v2, v1}, Landroid/support/v7/internal/widget/al;->setTabSelected(I)V

    .line 619
    check-cast p1, Landroid/support/v7/internal/a/q;

    iput-object p1, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    goto :goto_46
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 898
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->d(Ljava/lang/CharSequence;)V

    .line 899
    return-void
.end method

.method public final c(Z)V
    .registers 4

    .prologue
    const/4 v1, 0x4

    .line 375
    if-eqz p1, :cond_8

    move v0, v1

    :goto_4
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/a/l;->a(II)V

    .line 376
    return-void

    .line 375
    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final d()Landroid/view/View;
    .registers 2

    .prologue
    .line 473
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->y()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)V
    .registers 4

    .prologue
    .line 404
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->v()I

    move-result v0

    packed-switch v0, :pswitch_data_24

    .line 412
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setSelectedNavigationIndex not valid for current navigation mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 406
    :pswitch_11
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/g;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/app/g;)V

    .line 410
    :goto_1c
    return-void

    .line 409
    :pswitch_1d
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->e(I)V

    goto :goto_1c

    .line 404
    nop

    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_11
    .end packed-switch
.end method

.method public final d(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 465
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContainer;->setStackedBackground(Landroid/graphics/drawable/Drawable;)V

    .line 466
    return-void
.end method

.method public final d(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 438
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->a(Ljava/lang/CharSequence;)V

    .line 439
    return-void
.end method

.method public final d(Z)V
    .registers 4

    .prologue
    const/16 v1, 0x8

    .line 380
    if-eqz p1, :cond_9

    move v0, v1

    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/a/l;->a(II)V

    .line 381
    return-void

    .line 380
    :cond_9
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final e()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 477
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->e()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final e(I)V
    .registers 3

    .prologue
    .line 395
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/l;->a(Ljava/lang/CharSequence;)V

    .line 396
    return-void
.end method

.method public final e(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 888
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->c(Landroid/graphics/drawable/Drawable;)V

    .line 889
    return-void
.end method

.method public final e(Z)V
    .registers 4

    .prologue
    const/16 v1, 0x10

    .line 385
    if-eqz p1, :cond_9

    move v0, v1

    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/a/l;->a(II)V

    .line 386
    return-void

    .line 385
    :cond_9
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final f()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 481
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->f()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final f(I)V
    .registers 3

    .prologue
    .line 400
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/l;->b(Ljava/lang/CharSequence;)V

    .line 401
    return-void
.end method

.method public final f(Z)V
    .registers 4

    .prologue
    const/4 v1, 0x4

    .line 1335
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->I:Z

    if-nez v0, :cond_b

    .line 24375
    if-eqz p1, :cond_c

    move v0, v1

    :goto_8
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/a/l;->a(II)V

    .line 1338
    :cond_b
    return-void

    .line 24375
    :cond_c
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final g()I
    .registers 2

    .prologue
    .line 485
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->v()I

    move-result v0

    return v0
.end method

.method public final g(I)V
    .registers 3

    .prologue
    .line 446
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_7

    .line 447
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/a/l;->I:Z

    .line 449
    :cond_7
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->c(I)V

    .line 450
    return-void
.end method

.method public final g(Z)V
    .registers 3

    .prologue
    .line 331
    iput-boolean p1, p0, Landroid/support/v7/internal/a/l;->W:Z

    .line 332
    if-nez p1, :cond_d

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    if-eqz v0, :cond_d

    .line 333
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->V:Landroid/support/v7/internal/view/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/i;->b()V

    .line 335
    :cond_d
    return-void
.end method

.method public final h()I
    .registers 2

    .prologue
    .line 489
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->r()I

    move-result v0

    return v0
.end method

.method public final h(I)V
    .registers 9

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 1272
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->v()I

    move-result v4

    .line 1273
    packed-switch v4, :pswitch_data_9e

    .line 1280
    :goto_d
    if-eq v4, p1, :cond_1c

    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->O:Z

    if-nez v0, :cond_1c

    .line 1281
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_1c

    .line 1282
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v0}, Landroid/support/v4/view/cx;->t(Landroid/view/View;)V

    .line 1285
    :cond_1c
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->d(I)V

    .line 1286
    packed-switch p1, :pswitch_data_a4

    .line 1296
    :cond_24
    :goto_24
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    if-ne p1, v6, :cond_99

    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->O:Z

    if-nez v0, :cond_99

    move v0, v2

    :goto_2d
    invoke-interface {v1, v0}, Landroid/support/v7/internal/widget/ad;->a(Z)V

    .line 1297
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-ne p1, v6, :cond_9b

    iget-boolean v1, p0, Landroid/support/v7/internal/a/l;->O:Z

    if-nez v1, :cond_9b

    :goto_38
    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHasNonEmbeddedTabs(Z)V

    .line 1298
    return-void

    .line 22243
    :pswitch_3c
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->v()I

    move-result v0

    packed-switch v0, :pswitch_data_aa

    move v0, v1

    .line 1275
    :goto_46
    iput v0, p0, Landroid/support/v7/internal/a/l;->H:I

    .line 1276
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/app/g;)V

    .line 1277
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/al;->setVisibility(I)V

    goto :goto_d

    .line 22245
    :pswitch_54
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    if-eqz v0, :cond_5d

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    .line 23160
    iget v0, v0, Landroid/support/v7/internal/a/q;->c:I

    goto :goto_46

    :cond_5d
    move v0, v1

    .line 22245
    goto :goto_46

    .line 22247
    :pswitch_5f
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->w()I

    move-result v0

    goto :goto_46

    .line 1288
    :pswitch_66
    invoke-direct {p0}, Landroid/support/v7/internal/a/l;->E()V

    .line 1289
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/al;->setVisibility(I)V

    .line 1290
    iget v0, p0, Landroid/support/v7/internal/a/l;->H:I

    if-eq v0, v1, :cond_24

    .line 1291
    iget v0, p0, Landroid/support/v7/internal/a/l;->H:I

    .line 23404
    iget-object v4, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v4}, Landroid/support/v7/internal/widget/ad;->v()I

    move-result v4

    packed-switch v4, :pswitch_data_b2

    .line 23412
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setSelectedNavigationIndex not valid for current navigation mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23406
    :pswitch_85
    iget-object v4, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/g;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/app/g;)V

    .line 1292
    :goto_90
    iput v1, p0, Landroid/support/v7/internal/a/l;->H:I

    goto :goto_24

    .line 23409
    :pswitch_93
    iget-object v4, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v4, v0}, Landroid/support/v7/internal/widget/ad;->e(I)V

    goto :goto_90

    :cond_99
    move v0, v3

    .line 1296
    goto :goto_2d

    :cond_9b
    move v2, v3

    .line 1297
    goto :goto_38

    .line 1273
    nop

    :pswitch_data_9e
    .packed-switch 0x2
        :pswitch_3c
    .end packed-switch

    .line 1286
    :pswitch_data_a4
    .packed-switch 0x2
        :pswitch_66
    .end packed-switch

    .line 22243
    :pswitch_data_aa
    .packed-switch 0x1
        :pswitch_5f
        :pswitch_54
    .end packed-switch

    .line 23404
    :pswitch_data_b2
    .packed-switch 0x1
        :pswitch_93
        :pswitch_85
    .end packed-switch
.end method

.method public final h(Z)V
    .registers 5

    .prologue
    .line 346
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->J:Z

    if-ne p1, v0, :cond_5

    .line 355
    :cond_4
    return-void

    .line 349
    :cond_5
    iput-boolean p1, p0, Landroid/support/v7/internal/a/l;->J:Z

    .line 351
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->K:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 352
    const/4 v0, 0x0

    :goto_e
    if-ge v0, v1, :cond_4

    .line 353
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->K:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 352
    add-int/lit8 v0, v0, 0x1

    goto :goto_e
.end method

.method public final i()Landroid/support/v7/app/g;
    .registers 2

    .prologue
    .line 560
    new-instance v0, Landroid/support/v7/internal/a/q;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/q;-><init>(Landroid/support/v7/internal/a/l;)V

    return-object v0
.end method

.method public final i(I)V
    .registers 6

    .prologue
    .line 570
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    if-nez v0, :cond_5

    .line 591
    :cond_4
    :goto_4
    return-void

    .line 575
    :cond_5
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    if-eqz v0, :cond_4f

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    .line 15160
    iget v0, v0, Landroid/support/v7/internal/a/q;->c:I

    move v1, v0

    .line 577
    :goto_e
    iget-object v2, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    .line 15349
    iget-object v0, v2, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/aj;->removeViewAt(I)V

    .line 15350
    iget-object v0, v2, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_24

    .line 15351
    iget-object v0, v2, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 15353
    :cond_24
    iget-boolean v0, v2, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_2b

    .line 15354
    invoke-virtual {v2}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 578
    :cond_2b
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/a/q;

    .line 579
    if-eqz v0, :cond_38

    .line 16164
    const/4 v2, -0x1

    iput v2, v0, Landroid/support/v7/internal/a/q;->c:I

    .line 583
    :cond_38
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, p1

    .line 584
    :goto_3f
    if-ge v2, v3, :cond_53

    .line 585
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/a/q;

    .line 17164
    iput v2, v0, Landroid/support/v7/internal/a/q;->c:I

    .line 584
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3f

    .line 575
    :cond_4f
    iget v0, p0, Landroid/support/v7/internal/a/l;->H:I

    move v1, v0

    goto :goto_e

    .line 588
    :cond_53
    if-ne v1, p1, :cond_4

    .line 589
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_62

    const/4 v0, 0x0

    :goto_5e
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/app/g;)V

    goto :goto_4

    :cond_62
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    const/4 v1, 0x0

    add-int/lit8 v2, p1, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/a/q;

    goto :goto_5e
.end method

.method public final i(Z)V
    .registers 2

    .prologue
    .line 641
    iput-boolean p1, p0, Landroid/support/v7/internal/a/l;->Q:Z

    .line 642
    return-void
.end method

.method public final j(I)Landroid/support/v7/app/g;
    .registers 3

    .prologue
    .line 1302
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/g;

    return-object v0
.end method

.method public final j()V
    .registers 3

    .prologue
    .line 418
    .line 4422
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    if-eqz v0, :cond_8

    .line 4423
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/app/g;)V

    .line 4425
    :cond_8
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4426
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    if-eqz v0, :cond_2e

    .line 4427
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->E:Landroid/support/v7/internal/widget/al;

    .line 5359
    iget-object v0, v1, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj;->removeAllViews()V

    .line 5360
    iget-object v0, v1, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_27

    .line 5361
    iget-object v0, v1, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 5363
    :cond_27
    iget-boolean v0, v1, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_2e

    .line 5364
    invoke-virtual {v1}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 4429
    :cond_2e
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/internal/a/l;->H:I

    .line 419
    return-void
.end method

.method public final j(Z)V
    .registers 10

    .prologue
    const-wide/16 v6, 0xc8

    const-wide/16 v4, 0x64

    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 841
    if-eqz p1, :cond_50

    .line 20653
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->T:Z

    if-nez v0, :cond_1c

    .line 20654
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->T:Z

    .line 20655
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_19

    .line 20656
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setShowingForActionMode(Z)V

    .line 20658
    :cond_19
    invoke-direct {p0, v2}, Landroid/support/v7/internal/a/l;->l(Z)V

    .line 848
    :cond_1c
    :goto_1c
    if-eqz p1, :cond_63

    .line 849
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, v3, v4, v5}, Landroid/support/v7/internal/widget/ad;->a(IJ)Landroid/support/v4/view/fk;

    move-result-object v0

    .line 851
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->C:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1, v2, v6, v7}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(IJ)Landroid/support/v4/view/fk;

    move-result-object v1

    .line 859
    :goto_2a
    new-instance v4, Landroid/support/v7/internal/view/i;

    invoke-direct {v4}, Landroid/support/v7/internal/view/i;-><init>()V

    .line 21056
    iget-object v2, v4, Landroid/support/v7/internal/view/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21795
    iget-object v0, v0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_70

    .line 21796
    sget-object v2, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v2, v0}, Landroid/support/v4/view/fu;->a(Landroid/view/View;)J

    move-result-wide v2

    .line 21057
    :goto_44
    invoke-virtual {v1, v2, v3}, Landroid/support/v4/view/fk;->b(J)Landroid/support/v4/view/fk;

    .line 21058
    iget-object v0, v4, Landroid/support/v7/internal/view/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 861
    invoke-virtual {v4}, Landroid/support/v7/internal/view/i;->a()V

    .line 863
    return-void

    .line 20678
    :cond_50
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->T:Z

    if-eqz v0, :cond_1c

    .line 20679
    iput-boolean v2, p0, Landroid/support/v7/internal/a/l;->T:Z

    .line 20680
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_5f

    .line 20681
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setShowingForActionMode(Z)V

    .line 20683
    :cond_5f
    invoke-direct {p0, v2}, Landroid/support/v7/internal/a/l;->l(Z)V

    goto :goto_1c

    .line 854
    :cond_63
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, v2, v6, v7}, Landroid/support/v7/internal/widget/ad;->a(IJ)Landroid/support/v4/view/fk;

    move-result-object v1

    .line 856
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->C:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v3, v4, v5}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(IJ)Landroid/support/v4/view/fk;

    move-result-object v0

    goto :goto_2a

    .line 21798
    :cond_70
    const-wide/16 v2, 0x0

    goto :goto_44
.end method

.method public final k()Landroid/support/v7/app/g;
    .registers 2

    .prologue
    .line 632
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->G:Landroid/support/v7/internal/a/q;

    return-object v0
.end method

.method public final k(I)V
    .registers 3

    .prologue
    .line 893
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->g(I)V

    .line 894
    return-void
.end method

.method public final l()I
    .registers 2

    .prologue
    .line 1267
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final l(I)V
    .registers 3

    .prologue
    .line 903
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->h(I)V

    .line 904
    return-void
.end method

.method public final m()I
    .registers 2

    .prologue
    .line 637
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    return v0
.end method

.method public final m(I)V
    .registers 4

    .prologue
    .line 716
    if-eqz p1, :cond_10

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    .line 18188
    iget-boolean v0, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a:Z

    .line 716
    if-nez v0, :cond_10

    .line 717
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to set a non-zero hide offset"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 720
    :cond_10
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setActionBarHideOffset(I)V

    .line 721
    return-void
.end method

.method public final n()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 646
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->R:Z

    if-eqz v0, :cond_a

    .line 647
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->R:Z

    .line 648
    invoke-direct {p0, v1}, Landroid/support/v7/internal/a/l;->l(Z)V

    .line 650
    :cond_a
    return-void
.end method

.method public final n(I)V
    .registers 2

    .prologue
    .line 320
    iput p1, p0, Landroid/support/v7/internal/a/l;->P:I

    .line 321
    return-void
.end method

.method public final o()V
    .registers 2

    .prologue
    .line 671
    iget-boolean v0, p0, Landroid/support/v7/internal/a/l;->R:Z

    if-nez v0, :cond_b

    .line 672
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/a/l;->R:Z

    .line 673
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/internal/a/l;->l(Z)V

    .line 675
    :cond_b
    return-void
.end method

.method public final p()Z
    .registers 3

    .prologue
    .line 835
    .line 19637
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    .line 837
    iget-boolean v1, p0, Landroid/support/v7/internal/a/l;->U:Z

    if-eqz v1, :cond_16

    if-eqz v0, :cond_14

    .line 19711
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->getActionBarHideOffset()I

    move-result v1

    .line 837
    if-ge v1, v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public final q()V
    .registers 1

    .prologue
    .line 391
    return-void
.end method

.method public final r()Landroid/content/Context;
    .registers 5

    .prologue
    .line 866
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->w:Landroid/content/Context;

    if-nez v0, :cond_22

    .line 867
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 868
    iget-object v1, p0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 869
    sget v2, Landroid/support/v7/a/d;->actionBarWidgetTheme:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 870
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    .line 872
    if-eqz v0, :cond_25

    .line 873
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Landroid/support/v7/internal/a/l;->w:Landroid/content/Context;

    .line 878
    :cond_22
    :goto_22
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->w:Landroid/content/Context;

    return-object v0

    .line 875
    :cond_25
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    iput-object v0, p0, Landroid/support/v7/internal/a/l;->w:Landroid/content/Context;

    goto :goto_22
.end method

.method public final s()Z
    .registers 2

    .prologue
    .line 883
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->t()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final t()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 696
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    .line 17188
    iget-boolean v0, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a:Z

    .line 696
    if-nez v0, :cond_f

    .line 697
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 700
    :cond_f
    iput-boolean v1, p0, Landroid/support/v7/internal/a/l;->m:Z

    .line 701
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V

    .line 702
    return-void
.end method

.method public final u()Z
    .registers 2

    .prologue
    .line 706
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    .line 17558
    iget-boolean v0, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b:Z

    .line 706
    return v0
.end method

.method public final v()I
    .registers 2

    .prologue
    .line 711
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->z:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->getActionBarHideOffset()I

    move-result v0

    return v0
.end method

.method public final w()F
    .registers 2

    .prologue
    .line 255
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->A:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, Landroid/support/v4/view/cx;->r(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public final z()Z
    .registers 2

    .prologue
    .line 920
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    if-eqz v0, :cond_13

    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->c()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 921
    iget-object v0, p0, Landroid/support/v7/internal/a/l;->B:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->d()V

    .line 922
    const/4 v0, 0x1

    .line 924
    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method
