.class final Landroid/support/v7/internal/view/menu/w;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/internal/view/menu/v;

.field private b:Landroid/support/v7/internal/view/menu/i;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/view/menu/v;Landroid/support/v7/internal/view/menu/i;)V
    .registers 4

    .prologue
    .line 343
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/w;->a:Landroid/support/v7/internal/view/menu/v;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 341
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/internal/view/menu/w;->c:I

    .line 344
    iput-object p2, p0, Landroid/support/v7/internal/view/menu/w;->b:Landroid/support/v7/internal/view/menu/i;

    .line 345
    invoke-direct {p0}, Landroid/support/v7/internal/view/menu/w;->a()V

    .line 346
    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/view/menu/w;)Landroid/support/v7/internal/view/menu/i;
    .registers 2

    .prologue
    .line 339
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/w;->b:Landroid/support/v7/internal/view/menu/i;

    return-object v0
.end method

.method private a()V
    .registers 6

    .prologue
    .line 386
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/w;->a:Landroid/support/v7/internal/view/menu/v;

    invoke-static {v0}, Landroid/support/v7/internal/view/menu/v;->c(Landroid/support/v7/internal/view/menu/v;)Landroid/support/v7/internal/view/menu/i;

    move-result-object v0

    .line 2358
    iget-object v2, v0, Landroid/support/v7/internal/view/menu/i;->n:Landroid/support/v7/internal/view/menu/m;

    .line 387
    if-eqz v2, :cond_2b

    .line 388
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/w;->a:Landroid/support/v7/internal/view/menu/v;

    invoke-static {v0}, Landroid/support/v7/internal/view/menu/v;->c(Landroid/support/v7/internal/view/menu/v;)Landroid/support/v7/internal/view/menu/i;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->j()Ljava/util/ArrayList;

    move-result-object v3

    .line 389
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 390
    const/4 v0, 0x0

    move v1, v0

    :goto_1a
    if-ge v1, v4, :cond_2b

    .line 391
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 392
    if-ne v0, v2, :cond_27

    .line 393
    iput v1, p0, Landroid/support/v7/internal/view/menu/w;->c:I

    .line 399
    :goto_26
    return-void

    .line 390
    :cond_27
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1a

    .line 398
    :cond_2b
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/internal/view/menu/w;->c:I

    goto :goto_26
.end method


# virtual methods
.method public final a(I)Landroid/support/v7/internal/view/menu/m;
    .registers 4

    .prologue
    .line 358
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/w;->a:Landroid/support/v7/internal/view/menu/v;

    invoke-static {v0}, Landroid/support/v7/internal/view/menu/v;->a(Landroid/support/v7/internal/view/menu/v;)Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/w;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->j()Ljava/util/ArrayList;

    move-result-object v0

    .line 360
    :goto_e
    iget v1, p0, Landroid/support/v7/internal/view/menu/w;->c:I

    if-ltz v1, :cond_18

    iget v1, p0, Landroid/support/v7/internal/view/menu/w;->c:I

    if-lt p1, v1, :cond_18

    .line 361
    add-int/lit8 p1, p1, 0x1

    .line 363
    :cond_18
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    return-object v0

    .line 358
    :cond_1f
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/w;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->h()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_e
.end method

.method public final getCount()I
    .registers 3

    .prologue
    .line 349
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/w;->a:Landroid/support/v7/internal/view/menu/v;

    invoke-static {v0}, Landroid/support/v7/internal/view/menu/v;->a(Landroid/support/v7/internal/view/menu/v;)Z

    move-result v0

    if-eqz v0, :cond_17

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/w;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->j()Ljava/util/ArrayList;

    move-result-object v0

    .line 351
    :goto_e
    iget v1, p0, Landroid/support/v7/internal/view/menu/w;->c:I

    if-gez v1, :cond_1e

    .line 352
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 354
    :goto_16
    return v0

    .line 349
    :cond_17
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/w;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->h()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_e

    .line 354
    :cond_1e
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_16
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 339
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/view/menu/w;->a(I)Landroid/support/v7/internal/view/menu/m;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    .prologue
    .line 369
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8

    .prologue
    .line 373
    if-nez p2, :cond_27

    .line 374
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/w;->a:Landroid/support/v7/internal/view/menu/v;

    invoke-static {v0}, Landroid/support/v7/internal/view/menu/v;->b(Landroid/support/v7/internal/view/menu/v;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Landroid/support/v7/internal/view/menu/v;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_f
    move-object v0, v1

    .line 377
    check-cast v0, Landroid/support/v7/internal/view/menu/aa;

    .line 378
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/w;->a:Landroid/support/v7/internal/view/menu/v;

    iget-boolean v2, v2, Landroid/support/v7/internal/view/menu/v;->e:Z

    if-eqz v2, :cond_1f

    move-object v2, v1

    .line 379
    check-cast v2, Landroid/support/v7/internal/view/menu/ListMenuItemView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v7/internal/view/menu/ListMenuItemView;->setForceShowIcon(Z)V

    .line 381
    :cond_1f
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/view/menu/w;->a(I)Landroid/support/v7/internal/view/menu/m;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/support/v7/internal/view/menu/aa;->a(Landroid/support/v7/internal/view/menu/m;)V

    .line 382
    return-object v1

    :cond_27
    move-object v1, p2

    goto :goto_f
.end method

.method public final notifyDataSetChanged()V
    .registers 1

    .prologue
    .line 403
    invoke-direct {p0}, Landroid/support/v7/internal/view/menu/w;->a()V

    .line 404
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 405
    return-void
.end method
