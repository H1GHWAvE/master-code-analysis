.class public Landroid/support/v7/internal/widget/l;
.super Landroid/database/DataSetObservable;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String; = "activity_choser_model_history.xml"

.field public static final b:I = 0x32

.field private static final h:Z = false

.field private static final i:Ljava/lang/String;

.field private static final j:Ljava/lang/String; = "historical-records"

.field private static final k:Ljava/lang/String; = "historical-record"

.field private static final l:Ljava/lang/String; = "activity"

.field private static final m:Ljava/lang/String; = "time"

.field private static final n:Ljava/lang/String; = "weight"

.field private static final o:I = 0x5

.field private static final p:F = 1.0f

.field private static final q:Ljava/lang/String; = ".xml"

.field private static final r:I = -0x1

.field private static final s:Ljava/lang/Object;

.field private static final t:Ljava/util/Map;


# instance fields
.field private A:Z

.field private B:Z

.field public final c:Ljava/lang/Object;

.field final d:Ljava/util/List;

.field public e:Landroid/content/Intent;

.field public f:Z

.field public g:Landroid/support/v7/internal/widget/s;

.field private final u:Ljava/util/List;

.field private final v:Landroid/content/Context;

.field private final w:Ljava/lang/String;

.field private x:Landroid/support/v7/internal/widget/p;

.field private y:I

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 160
    const-class v0, Landroid/support/v7/internal/widget/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v7/internal/widget/l;->i:Ljava/lang/String;

    .line 221
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/v7/internal/widget/l;->s:Ljava/lang/Object;

    .line 226
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Landroid/support/v7/internal/widget/l;->t:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 349
    invoke-direct {p0}, Landroid/database/DataSetObservable;-><init>()V

    .line 232
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    .line 237
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    .line 242
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/l;->u:Ljava/util/List;

    .line 262
    new-instance v0, Landroid/support/v7/internal/widget/q;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/internal/widget/q;-><init>(Landroid/support/v7/internal/widget/l;B)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/l;->x:Landroid/support/v7/internal/widget/p;

    .line 267
    const/16 v0, 0x32

    iput v0, p0, Landroid/support/v7/internal/widget/l;->y:I

    .line 277
    iput-boolean v2, p0, Landroid/support/v7/internal/widget/l;->z:Z

    .line 288
    iput-boolean v1, p0, Landroid/support/v7/internal/widget/l;->A:Z

    .line 296
    iput-boolean v2, p0, Landroid/support/v7/internal/widget/l;->B:Z

    .line 301
    iput-boolean v1, p0, Landroid/support/v7/internal/widget/l;->f:Z

    .line 350
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/l;->v:Landroid/content/Context;

    .line 351
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_57

    const-string v0, ".xml"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_57

    .line 353
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    .line 357
    :goto_56
    return-void

    .line 355
    :cond_57
    iput-object p2, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    goto :goto_56
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/l;)Landroid/content/Context;
    .registers 2

    .prologue
    .line 93
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->v:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/l;
    .registers 5

    .prologue
    .line 333
    sget-object v1, Landroid/support/v7/internal/widget/l;->s:Ljava/lang/Object;

    monitor-enter v1

    .line 334
    :try_start_3
    sget-object v0, Landroid/support/v7/internal/widget/l;->t:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/l;

    .line 335
    if-nez v0, :cond_17

    .line 336
    new-instance v0, Landroid/support/v7/internal/widget/l;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/internal/widget/l;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 337
    sget-object v2, Landroid/support/v7/internal/widget/l;->t:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    :cond_17
    monitor-exit v1

    return-object v0

    .line 340
    :catchall_19
    move-exception v0

    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    throw v0
.end method

.method private a(Landroid/content/Intent;)V
    .registers 4

    .prologue
    .line 369
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 370
    :try_start_3
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->e:Landroid/content/Intent;

    if-ne v0, p1, :cond_9

    .line 371
    monitor-exit v1

    .line 376
    :goto_8
    return-void

    .line 373
    :cond_9
    iput-object p1, p0, Landroid/support/v7/internal/widget/l;->e:Landroid/content/Intent;

    .line 374
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/l;->f:Z

    .line 375
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/l;->d()V

    .line 376
    monitor-exit v1

    goto :goto_8

    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    throw v0
.end method

.method private a(Landroid/support/v7/internal/widget/p;)V
    .registers 4

    .prologue
    .line 590
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 591
    :try_start_3
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->x:Landroid/support/v7/internal/widget/p;

    if-ne v0, p1, :cond_9

    .line 592
    monitor-exit v1

    .line 598
    :goto_8
    return-void

    .line 594
    :cond_9
    iput-object p1, p0, Landroid/support/v7/internal/widget/l;->x:Landroid/support/v7/internal/widget/p;

    .line 595
    invoke-direct {p0}, Landroid/support/v7/internal/widget/l;->i()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 596
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/l;->notifyChanged()V

    .line 598
    :cond_14
    monitor-exit v1

    goto :goto_8

    :catchall_16
    move-exception v0

    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    throw v0
.end method

.method private a(Landroid/support/v7/internal/widget/s;)V
    .registers 4

    .prologue
    .line 500
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 501
    :try_start_3
    iput-object p1, p0, Landroid/support/v7/internal/widget/l;->g:Landroid/support/v7/internal/widget/s;

    .line 502
    monitor-exit v1

    return-void

    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method static synthetic b(Landroid/support/v7/internal/widget/l;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 93
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    return-object v0
.end method

.method private c(I)V
    .registers 8

    .prologue
    .line 535
    iget-object v2, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 536
    :try_start_3
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/l;->d()V

    .line 538
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/o;

    .line 539
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/widget/o;

    .line 542
    if-eqz v1, :cond_40

    .line 544
    iget v1, v1, Landroid/support/v7/internal/widget/o;->b:F

    iget v3, v0, Landroid/support/v7/internal/widget/o;->b:F

    sub-float/2addr v1, v3

    const/high16 v3, 0x40a00000    # 5.0f

    add-float/2addr v1, v3

    .line 550
    :goto_21
    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, v0, Landroid/support/v7/internal/widget/o;->a:Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/support/v7/internal/widget/o;->a:Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    new-instance v0, Landroid/support/v7/internal/widget/r;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v0, v3, v4, v5, v1}, Landroid/support/v7/internal/widget/r;-><init>(Landroid/content/ComponentName;JF)V

    .line 555
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/l;->a(Landroid/support/v7/internal/widget/r;)Z

    .line 556
    monitor-exit v2

    return-void

    .line 547
    :cond_40
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_21

    .line 556
    :catchall_43
    move-exception v0

    monitor-exit v2
    :try_end_45
    .catchall {:try_start_3 .. :try_end_45} :catchall_43

    throw v0
.end method

.method static synthetic c(Landroid/support/v7/internal/widget/l;)Z
    .registers 2

    .prologue
    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/l;->z:Z

    return v0
.end method

.method private d(I)V
    .registers 4

    .prologue
    .line 615
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 616
    :try_start_3
    iget v0, p0, Landroid/support/v7/internal/widget/l;->y:I

    if-ne v0, p1, :cond_9

    .line 617
    monitor-exit v1

    .line 624
    :goto_8
    return-void

    .line 619
    :cond_9
    iput p1, p0, Landroid/support/v7/internal/widget/l;->y:I

    .line 620
    invoke-direct {p0}, Landroid/support/v7/internal/widget/l;->l()V

    .line 621
    invoke-direct {p0}, Landroid/support/v7/internal/widget/l;->i()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 622
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/l;->notifyChanged()V

    .line 624
    :cond_17
    monitor-exit v1

    goto :goto_8

    :catchall_19
    move-exception v0

    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    throw v0
.end method

.method static synthetic e()Ljava/lang/String;
    .registers 1

    .prologue
    .line 93
    sget-object v0, Landroid/support/v7/internal/widget/l;->i:Ljava/lang/String;

    return-object v0
.end method

.method private f()Landroid/content/Intent;
    .registers 3

    .prologue
    .line 385
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 386
    :try_start_3
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->e:Landroid/content/Intent;

    monitor-exit v1

    return-object v0

    .line 387
    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method private g()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 569
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/l;->A:Z

    if-nez v0, :cond_d

    .line 570
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No preceding call to #readHistoricalData"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572
    :cond_d
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/l;->B:Z

    if-nez v0, :cond_12

    .line 1045
    :cond_11
    :goto_11
    return-void

    .line 575
    :cond_12
    iput-boolean v4, p0, Landroid/support/v7/internal/widget/l;->B:Z

    .line 576
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 577
    new-instance v0, Landroid/support/v7/internal/widget/t;

    invoke-direct {v0, p0, v4}, Landroid/support/v7/internal/widget/t;-><init>(Landroid/support/v7/internal/widget/l;B)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Landroid/support/v7/internal/widget/l;->u:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 1043
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_3e

    .line 2029
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, v2, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_11

    .line 1048
    :cond_3e
    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_11
.end method

.method private h()I
    .registers 3

    .prologue
    .line 633
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 634
    :try_start_3
    iget v0, p0, Landroid/support/v7/internal/widget/l;->y:I

    monitor-exit v1

    return v0

    .line 635
    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method private i()Z
    .registers 4

    .prologue
    .line 674
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->x:Landroid/support/v7/internal/widget/p;

    if-eqz v0, :cond_27

    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->e:Landroid/content/Intent;

    if-eqz v0, :cond_27

    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_27

    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_27

    .line 676
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->x:Landroid/support/v7/internal/widget/p;

    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    iget-object v2, p0, Landroid/support/v7/internal/widget/l;->u:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/support/v7/internal/widget/p;->a(Ljava/util/List;Ljava/util/List;)V

    .line 678
    const/4 v0, 0x1

    .line 680
    :goto_26
    return v0

    :cond_27
    const/4 v0, 0x0

    goto :goto_26
.end method

.method private j()Z
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 690
    iget-boolean v1, p0, Landroid/support/v7/internal/widget/l;->f:Z

    if-eqz v1, :cond_38

    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->e:Landroid/content/Intent;

    if-eqz v1, :cond_38

    .line 691
    iput-boolean v0, p0, Landroid/support/v7/internal/widget/l;->f:Z

    .line 692
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 693
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->v:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/internal/widget/l;->e:Landroid/content/Intent;

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 695
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    .line 696
    :goto_21
    if-ge v1, v3, :cond_37

    .line 697
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 698
    iget-object v4, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    new-instance v5, Landroid/support/v7/internal/widget/o;

    invoke-direct {v5, p0, v0}, Landroid/support/v7/internal/widget/o;-><init>(Landroid/support/v7/internal/widget/l;Landroid/content/pm/ResolveInfo;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 696
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_21

    .line 700
    :cond_37
    const/4 v0, 0x1

    .line 702
    :cond_38
    return v0
.end method

.method private k()Z
    .registers 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 713
    iget-boolean v2, p0, Landroid/support/v7/internal/widget/l;->z:Z

    if-eqz v2, :cond_db

    iget-boolean v2, p0, Landroid/support/v7/internal/widget/l;->B:Z

    if-eqz v2, :cond_db

    iget-object v2, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_db

    .line 715
    iput-boolean v1, p0, Landroid/support/v7/internal/widget/l;->z:Z

    .line 716
    iput-boolean v0, p0, Landroid/support/v7/internal/widget/l;->A:Z

    .line 2964
    :try_start_16
    iget-object v2, p0, Landroid/support/v7/internal/widget/l;->v:Landroid/content/Context;

    iget-object v3, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_1d
    .catch Ljava/io/FileNotFoundException; {:try_start_16 .. :try_end_1d} :catch_e1

    move-result-object v2

    .line 2972
    :try_start_1e
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 2973
    const-string v4, "UTF-8"

    invoke-interface {v3, v2, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 2976
    :goto_27
    if-eq v1, v0, :cond_31

    const/4 v4, 0x2

    if-eq v1, v4, :cond_31

    .line 2977
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    goto :goto_27

    .line 2980
    :cond_31
    const-string v1, "historical-records"

    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_62

    .line 2981
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v3, "Share records file does not start with historical-records tag."

    invoke-direct {v1, v3}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_45
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1e .. :try_end_45} :catch_45
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_45} :catch_87
    .catchall {:try_start_1e .. :try_end_45} :catchall_cc

    .line 3017
    :catch_45
    move-exception v1

    .line 3018
    :try_start_46
    sget-object v3, Landroid/support/v7/internal/widget/l;->i:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error reading historical recrod file: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5c
    .catchall {:try_start_46 .. :try_end_5c} :catchall_cc

    .line 3022
    if-eqz v2, :cond_61

    .line 3024
    :try_start_5e
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_61
    .catch Ljava/io/IOException; {:try_start_5e .. :try_end_61} :catch_dd

    .line 720
    :cond_61
    :goto_61
    return v0

    .line 2985
    :cond_62
    :try_start_62
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->u:Ljava/util/List;

    .line 2986
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2989
    :cond_67
    :goto_67
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    .line 2990
    if-eq v4, v0, :cond_d3

    .line 2993
    const/4 v5, 0x3

    if-eq v4, v5, :cond_67

    const/4 v5, 0x4

    if-eq v4, v5, :cond_67

    .line 2996
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 2997
    const-string v5, "historical-record"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a6

    .line 2998
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v3, "Share records file not well-formed."

    invoke-direct {v1, v3}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_87
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_62 .. :try_end_87} :catch_45
    .catch Ljava/io/IOException; {:try_start_62 .. :try_end_87} :catch_87
    .catchall {:try_start_62 .. :try_end_87} :catchall_cc

    .line 3019
    :catch_87
    move-exception v1

    .line 3020
    :try_start_88
    sget-object v3, Landroid/support/v7/internal/widget/l;->i:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error reading historical recrod file: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9e
    .catchall {:try_start_88 .. :try_end_9e} :catchall_cc

    .line 3022
    if-eqz v2, :cond_61

    .line 3024
    :try_start_a0
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_a3
    .catch Ljava/io/IOException; {:try_start_a0 .. :try_end_a3} :catch_a4

    goto :goto_61

    .line 3027
    :catch_a4
    move-exception v1

    goto :goto_61

    .line 3001
    :cond_a6
    const/4 v4, 0x0

    :try_start_a7
    const-string v5, "activity"

    invoke-interface {v3, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3002
    const/4 v5, 0x0

    const-string v6, "time"

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 3004
    const/4 v5, 0x0

    const-string v8, "weight"

    invoke-interface {v3, v5, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    .line 3006
    new-instance v8, Landroid/support/v7/internal/widget/r;

    invoke-direct {v8, v4, v6, v7, v5}, Landroid/support/v7/internal/widget/r;-><init>(Ljava/lang/String;JF)V

    .line 3007
    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_cb
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_a7 .. :try_end_cb} :catch_45
    .catch Ljava/io/IOException; {:try_start_a7 .. :try_end_cb} :catch_87
    .catchall {:try_start_a7 .. :try_end_cb} :catchall_cc

    goto :goto_67

    .line 3022
    :catchall_cc
    move-exception v0

    if-eqz v2, :cond_d2

    .line 3024
    :try_start_cf
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_d2
    .catch Ljava/io/IOException; {:try_start_cf .. :try_end_d2} :catch_df

    .line 3027
    :cond_d2
    :goto_d2
    throw v0

    .line 3022
    :cond_d3
    if-eqz v2, :cond_61

    .line 3024
    :try_start_d5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_d8
    .catch Ljava/io/IOException; {:try_start_d5 .. :try_end_d8} :catch_d9

    goto :goto_61

    .line 3027
    :catch_d9
    move-exception v1

    goto :goto_61

    :cond_db
    move v0, v1

    .line 720
    goto :goto_61

    .line 3027
    :catch_dd
    move-exception v1

    goto :goto_61

    :catch_df
    move-exception v1

    goto :goto_d2

    .line 2969
    :catch_e1
    move-exception v1

    goto/16 :goto_61
.end method

.method private l()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 745
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v2, p0, Landroid/support/v7/internal/widget/l;->y:I

    sub-int v2, v0, v2

    .line 746
    if-gtz v2, :cond_e

    .line 756
    :cond_d
    return-void

    .line 749
    :cond_e
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/l;->B:Z

    move v0, v1

    .line 750
    :goto_12
    if-ge v0, v2, :cond_d

    .line 751
    iget-object v3, p0, Landroid/support/v7/internal/widget/l;->u:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 750
    add-int/lit8 v0, v0, 0x1

    goto :goto_12
.end method

.method private m()V
    .registers 10

    .prologue
    const/4 v8, 0x1

    .line 964
    :try_start_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->v:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_8} :catch_cb

    move-result-object v1

    .line 972
    :try_start_9
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 973
    const-string v0, "UTF-8"

    invoke-interface {v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 975
    const/4 v0, 0x0

    .line 976
    :goto_13
    if-eq v0, v8, :cond_1d

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1d

    .line 977
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_13

    .line 980
    :cond_1d
    const-string v0, "historical-records"

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4e

    .line 981
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "Share records file does not start with historical-records tag."

    invoke-direct {v0, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_31
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9 .. :try_end_31} :catch_31
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_31} :catch_73
    .catchall {:try_start_9 .. :try_end_31} :catchall_b8

    .line 1017
    :catch_31
    move-exception v0

    .line 1018
    :try_start_32
    sget-object v2, Landroid/support/v7/internal/widget/l;->i:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error reading historical recrod file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_48
    .catchall {:try_start_32 .. :try_end_48} :catchall_b8

    .line 1022
    if-eqz v1, :cond_4d

    .line 1024
    :try_start_4a
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4d} :catch_c7

    .line 1030
    :cond_4d
    :goto_4d
    return-void

    .line 985
    :cond_4e
    :try_start_4e
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->u:Ljava/util/List;

    .line 986
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 989
    :cond_53
    :goto_53
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .line 990
    if-eq v3, v8, :cond_bf

    .line 993
    const/4 v4, 0x3

    if-eq v3, v4, :cond_53

    const/4 v4, 0x4

    if-eq v3, v4, :cond_53

    .line 996
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 997
    const-string v4, "historical-record"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_92

    .line 998
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "Share records file not well-formed."

    invoke-direct {v0, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_73
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4e .. :try_end_73} :catch_31
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_73} :catch_73
    .catchall {:try_start_4e .. :try_end_73} :catchall_b8

    .line 1019
    :catch_73
    move-exception v0

    .line 1020
    :try_start_74
    sget-object v2, Landroid/support/v7/internal/widget/l;->i:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error reading historical recrod file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8a
    .catchall {:try_start_74 .. :try_end_8a} :catchall_b8

    .line 1022
    if-eqz v1, :cond_4d

    .line 1024
    :try_start_8c
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_8f
    .catch Ljava/io/IOException; {:try_start_8c .. :try_end_8f} :catch_90

    goto :goto_4d

    .line 1027
    :catch_90
    move-exception v0

    goto :goto_4d

    .line 1001
    :cond_92
    const/4 v3, 0x0

    :try_start_93
    const-string v4, "activity"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1002
    const/4 v4, 0x0

    const-string v5, "time"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1004
    const/4 v6, 0x0

    const-string v7, "weight"

    invoke-interface {v2, v6, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    .line 1006
    new-instance v7, Landroid/support/v7/internal/widget/r;

    invoke-direct {v7, v3, v4, v5, v6}, Landroid/support/v7/internal/widget/r;-><init>(Ljava/lang/String;JF)V

    .line 1007
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_b7
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_93 .. :try_end_b7} :catch_31
    .catch Ljava/io/IOException; {:try_start_93 .. :try_end_b7} :catch_73
    .catchall {:try_start_93 .. :try_end_b7} :catchall_b8

    goto :goto_53

    .line 1022
    :catchall_b8
    move-exception v0

    if-eqz v1, :cond_be

    .line 1024
    :try_start_bb
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_be
    .catch Ljava/io/IOException; {:try_start_bb .. :try_end_be} :catch_c9

    .line 1027
    :cond_be
    :goto_be
    throw v0

    .line 1022
    :cond_bf
    if-eqz v1, :cond_4d

    .line 1024
    :try_start_c1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_c4
    .catch Ljava/io/IOException; {:try_start_c1 .. :try_end_c4} :catch_c5

    goto :goto_4d

    .line 1027
    :catch_c5
    move-exception v0

    goto :goto_4d

    :catch_c7
    move-exception v0

    goto :goto_4d

    :catch_c9
    move-exception v1

    goto :goto_be

    .line 969
    :catch_cb
    move-exception v0

    goto :goto_4d
.end method


# virtual methods
.method public final a()I
    .registers 3

    .prologue
    .line 398
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 399
    :try_start_3
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/l;->d()V

    .line 400
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 401
    :catchall_e
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    throw v0
.end method

.method public final a(Landroid/content/pm/ResolveInfo;)I
    .registers 7

    .prologue
    .line 427
    iget-object v2, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 428
    :try_start_3
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/l;->d()V

    .line 429
    iget-object v3, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    .line 430
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 431
    const/4 v1, 0x0

    :goto_d
    if-ge v1, v4, :cond_1f

    .line 432
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/o;

    .line 433
    iget-object v0, v0, Landroid/support/v7/internal/widget/o;->a:Landroid/content/pm/ResolveInfo;

    if-ne v0, p1, :cond_1c

    .line 434
    monitor-exit v2

    move v0, v1

    .line 437
    :goto_1b
    return v0

    .line 431
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 437
    :cond_1f
    const/4 v0, -0x1

    monitor-exit v2

    goto :goto_1b

    .line 438
    :catchall_22
    move-exception v0

    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    throw v0
.end method

.method public final a(I)Landroid/content/pm/ResolveInfo;
    .registers 4

    .prologue
    .line 413
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 414
    :try_start_3
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/l;->d()V

    .line 415
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/o;

    iget-object v0, v0, Landroid/support/v7/internal/widget/o;->a:Landroid/content/pm/ResolveInfo;

    monitor-exit v1

    return-object v0

    .line 416
    :catchall_12
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    throw v0
.end method

.method final a(Landroid/support/v7/internal/widget/r;)Z
    .registers 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 730
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 731
    if-eqz v0, :cond_4f

    .line 732
    iput-boolean v6, p0, Landroid/support/v7/internal/widget/l;->B:Z

    .line 733
    invoke-direct {p0}, Landroid/support/v7/internal/widget/l;->l()V

    .line 3569
    iget-boolean v1, p0, Landroid/support/v7/internal/widget/l;->A:Z

    if-nez v1, :cond_1b

    .line 3570
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No preceding call to #readHistoricalData"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3572
    :cond_1b
    iget-boolean v1, p0, Landroid/support/v7/internal/widget/l;->B:Z

    if-eqz v1, :cond_49

    .line 3575
    iput-boolean v5, p0, Landroid/support/v7/internal/widget/l;->B:Z

    .line 3576
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_49

    .line 3577
    new-instance v1, Landroid/support/v7/internal/widget/t;

    invoke-direct {v1, p0, v5}, Landroid/support/v7/internal/widget/t;-><init>(Landroid/support/v7/internal/widget/l;B)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Landroid/support/v7/internal/widget/l;->u:Ljava/util/List;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    aput-object v3, v2, v5

    iget-object v3, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    aput-object v3, v2, v6

    .line 4043
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_50

    .line 5029
    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {v1, v3, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 735
    :cond_49
    :goto_49
    invoke-direct {p0}, Landroid/support/v7/internal/widget/l;->i()Z

    .line 736
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/l;->notifyChanged()V

    .line 738
    :cond_4f
    return v0

    .line 4048
    :cond_50
    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_49
.end method

.method public final b(I)Landroid/content/Intent;
    .registers 9

    .prologue
    .line 460
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 461
    :try_start_3
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->e:Landroid/content/Intent;

    if-nez v0, :cond_a

    .line 462
    const/4 v0, 0x0

    monitor-exit v1

    .line 490
    :goto_9
    return-object v0

    .line 465
    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/l;->d()V

    .line 467
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/o;

    .line 469
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, v0, Landroid/support/v7/internal/widget/o;->a:Landroid/content/pm/ResolveInfo;

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/support/v7/internal/widget/o;->a:Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Landroid/support/v7/internal/widget/l;->e:Landroid/content/Intent;

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 474
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 476
    iget-object v3, p0, Landroid/support/v7/internal/widget/l;->g:Landroid/support/v7/internal/widget/s;

    if-eqz v3, :cond_39

    .line 478
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 486
    :cond_39
    new-instance v3, Landroid/support/v7/internal/widget/r;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v3, v2, v4, v5, v6}, Landroid/support/v7/internal/widget/r;-><init>(Landroid/content/ComponentName;JF)V

    .line 488
    invoke-virtual {p0, v3}, Landroid/support/v7/internal/widget/l;->a(Landroid/support/v7/internal/widget/r;)Z

    .line 490
    monitor-exit v1

    goto :goto_9

    .line 491
    :catchall_49
    move-exception v0

    monitor-exit v1
    :try_end_4b
    .catchall {:try_start_3 .. :try_end_4b} :catchall_49

    throw v0
.end method

.method public final b()Landroid/content/pm/ResolveInfo;
    .registers 4

    .prologue
    .line 515
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 516
    :try_start_3
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/l;->d()V

    .line 517
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 518
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/o;

    iget-object v0, v0, Landroid/support/v7/internal/widget/o;->a:Landroid/content/pm/ResolveInfo;

    monitor-exit v1

    .line 521
    :goto_1a
    return-object v0

    .line 520
    :cond_1b
    monitor-exit v1

    .line 521
    const/4 v0, 0x0

    goto :goto_1a

    .line 520
    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    throw v0
.end method

.method public final c()I
    .registers 3

    .prologue
    .line 644
    iget-object v1, p0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 645
    :try_start_3
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/l;->d()V

    .line 646
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 647
    :catchall_e
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    throw v0
.end method

.method public final d()V
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 657
    .line 2690
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/l;->f:Z

    if-eqz v0, :cond_5d

    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->e:Landroid/content/Intent;

    if-eqz v0, :cond_5d

    .line 2691
    iput-boolean v2, p0, Landroid/support/v7/internal/widget/l;->f:Z

    .line 2692
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2693
    iget-object v0, p0, Landroid/support/v7/internal/widget/l;->v:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v3, p0, Landroid/support/v7/internal/widget/l;->e:Landroid/content/Intent;

    invoke-virtual {v0, v3, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    .line 2695
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    .line 2696
    :goto_22
    if-ge v3, v5, :cond_38

    .line 2697
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 2698
    iget-object v6, p0, Landroid/support/v7/internal/widget/l;->d:Ljava/util/List;

    new-instance v7, Landroid/support/v7/internal/widget/o;

    invoke-direct {v7, p0, v0}, Landroid/support/v7/internal/widget/o;-><init>(Landroid/support/v7/internal/widget/l;Landroid/content/pm/ResolveInfo;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2696
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_22

    :cond_38
    move v0, v1

    .line 2713
    :goto_39
    iget-boolean v3, p0, Landroid/support/v7/internal/widget/l;->z:Z

    if-eqz v3, :cond_5f

    iget-boolean v3, p0, Landroid/support/v7/internal/widget/l;->B:Z

    if-eqz v3, :cond_5f

    iget-object v3, p0, Landroid/support/v7/internal/widget/l;->w:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5f

    .line 2715
    iput-boolean v2, p0, Landroid/support/v7/internal/widget/l;->z:Z

    .line 2716
    iput-boolean v1, p0, Landroid/support/v7/internal/widget/l;->A:Z

    .line 2717
    invoke-direct {p0}, Landroid/support/v7/internal/widget/l;->m()V

    .line 658
    :goto_50
    or-int/2addr v0, v1

    .line 659
    invoke-direct {p0}, Landroid/support/v7/internal/widget/l;->l()V

    .line 660
    if-eqz v0, :cond_5c

    .line 661
    invoke-direct {p0}, Landroid/support/v7/internal/widget/l;->i()Z

    .line 662
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/l;->notifyChanged()V

    .line 664
    :cond_5c
    return-void

    :cond_5d
    move v0, v2

    .line 2702
    goto :goto_39

    :cond_5f
    move v1, v2

    .line 2720
    goto :goto_50
.end method
