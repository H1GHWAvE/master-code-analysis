.class public final Landroid/support/v7/internal/widget/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/gd;


# instance fields
.field final synthetic a:Landroid/support/v7/internal/widget/al;

.field private b:Z

.field private c:I


# direct methods
.method protected constructor <init>(Landroid/support/v7/internal/widget/al;)V
    .registers 3

    .prologue
    .line 586
    iput-object p1, p0, Landroid/support/v7/internal/widget/aq;->a:Landroid/support/v7/internal/widget/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 587
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/aq;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/view/fk;I)Landroid/support/v7/internal/widget/aq;
    .registers 4

    .prologue
    .line 592
    iput p2, p0, Landroid/support/v7/internal/widget/aq;->c:I

    .line 593
    iget-object v0, p0, Landroid/support/v7/internal/widget/aq;->a:Landroid/support/v7/internal/widget/al;

    iput-object p1, v0, Landroid/support/v7/internal/widget/al;->g:Landroid/support/v4/view/fk;

    .line 594
    return-object p0
.end method

.method public final a(Landroid/view/View;)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 599
    iget-object v0, p0, Landroid/support/v7/internal/widget/aq;->a:Landroid/support/v7/internal/widget/al;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/al;->setVisibility(I)V

    .line 600
    iput-boolean v1, p0, Landroid/support/v7/internal/widget/aq;->b:Z

    .line 601
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 605
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/aq;->b:Z

    if-eqz v0, :cond_5

    .line 609
    :goto_4
    return-void

    .line 607
    :cond_5
    iget-object v0, p0, Landroid/support/v7/internal/widget/aq;->a:Landroid/support/v7/internal/widget/al;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/internal/widget/al;->g:Landroid/support/v4/view/fk;

    .line 608
    iget-object v0, p0, Landroid/support/v7/internal/widget/aq;->a:Landroid/support/v7/internal/widget/al;

    iget v1, p0, Landroid/support/v7/internal/widget/aq;->c:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/al;->setVisibility(I)V

    goto :goto_4
.end method

.method public final c(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 613
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/aq;->b:Z

    .line 614
    return-void
.end method
