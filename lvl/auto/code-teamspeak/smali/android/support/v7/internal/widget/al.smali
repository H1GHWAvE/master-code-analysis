.class public final Landroid/support/v7/internal/widget/al;
.super Landroid/widget/HorizontalScrollView;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field private static final i:Ljava/lang/String; = "ScrollingTabContainerView"

.field private static final m:Landroid/view/animation/Interpolator;

.field private static final n:I = 0xc8


# instance fields
.field a:Ljava/lang/Runnable;

.field public b:Landroid/support/v7/widget/aj;

.field public c:Landroid/widget/Spinner;

.field public d:Z

.field e:I

.field f:I

.field protected g:Landroid/support/v4/view/fk;

.field protected final h:Landroid/support/v7/internal/widget/aq;

.field private j:Landroid/support/v7/internal/widget/ao;

.field private k:I

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 76
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Landroid/support/v7/internal/widget/al;->m:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 8

    .prologue
    const/4 v5, -0x1

    const/4 v4, -0x2

    .line 81
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 74
    new-instance v0, Landroid/support/v7/internal/widget/aq;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/widget/aq;-><init>(Landroid/support/v7/internal/widget/al;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/al;->h:Landroid/support/v7/internal/widget/aq;

    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/al;->setHorizontalScrollBarEnabled(Z)V

    .line 85
    invoke-static {p1}, Landroid/support/v7/internal/view/a;->a(Landroid/content/Context;)Landroid/support/v7/internal/view/a;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Landroid/support/v7/internal/view/a;->b()I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/al;->setContentHeight(I)V

    .line 87
    invoke-virtual {v0}, Landroid/support/v7/internal/view/a;->c()I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/widget/al;->f:I

    .line 1203
    new-instance v0, Landroid/support/v7/widget/aj;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    sget v3, Landroid/support/v7/a/d;->actionBarTabBarStyle:I

    invoke-direct {v0, v1, v2, v3}, Landroid/support/v7/widget/aj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1205
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->setMeasureWithLargestChildEnabled(Z)V

    .line 1206
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->setGravity(I)V

    .line 1207
    new-instance v1, Landroid/support/v7/widget/al;

    invoke-direct {v1, v4, v5}, Landroid/support/v7/widget/al;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 89
    iput-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    .line 90
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/widget/al;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 92
    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/al;Landroid/support/v7/app/g;)Landroid/support/v7/internal/widget/ap;
    .registers 3

    .prologue
    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/internal/widget/al;->a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/al;)Landroid/support/v7/widget/aj;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    return-object v0
.end method

.method private a(Landroid/support/v7/app/g;IZ)V
    .registers 7

    .prologue
    .line 324
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/internal/widget/al;->a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;

    move-result-object v1

    .line 325
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    new-instance v2, Landroid/support/v7/widget/al;

    invoke-direct {v2}, Landroid/support/v7/widget/al;-><init>()V

    invoke-virtual {v0, v1, p2, v2}, Landroid/support/v7/widget/aj;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 327
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_1e

    .line 328
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 330
    :cond_1e
    if-eqz p3, :cond_24

    .line 331
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ap;->setSelected(Z)V

    .line 333
    :cond_24
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_2b

    .line 334
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 336
    :cond_2b
    return-void
.end method

.method private a()Z
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private b()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, -0x2

    .line 153
    invoke-direct {p0}, Landroid/support/v7/internal/widget/al;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 169
    :goto_9
    return-void

    .line 155
    :cond_a
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-nez v0, :cond_26

    .line 3213
    new-instance v0, Landroid/support/v7/widget/aa;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Landroid/support/v7/a/d;->actionDropDownStyle:I

    invoke-direct {v0, v1, v5, v2}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 3215
    new-instance v1, Landroid/support/v7/widget/al;

    invoke-direct {v1, v3, v4}, Landroid/support/v7/widget/al;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3218
    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 156
    iput-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    .line 158
    :cond_26
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/al;->removeView(Landroid/view/View;)V

    .line 159
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/widget/al;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 161
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-nez v0, :cond_48

    .line 162
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    new-instance v1, Landroid/support/v7/internal/widget/an;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Landroid/support/v7/internal/widget/an;-><init>(Landroid/support/v7/internal/widget/al;B)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 164
    :cond_48
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_53

    .line 165
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/al;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 166
    iput-object v5, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    .line 168
    :cond_53
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    iget v1, p0, Landroid/support/v7/internal/widget/al;->l:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_9
.end method

.method private b(Landroid/support/v7/app/g;Z)V
    .registers 6

    .prologue
    .line 309
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/internal/widget/al;->a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;

    move-result-object v1

    .line 310
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    new-instance v2, Landroid/support/v7/widget/al;

    invoke-direct {v2}, Landroid/support/v7/widget/al;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/aj;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 312
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_1e

    .line 313
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 315
    :cond_1e
    if-eqz p2, :cond_24

    .line 316
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ap;->setSelected(Z)V

    .line 318
    :cond_24
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_2b

    .line 319
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 321
    :cond_2b
    return-void
.end method

.method private c(I)V
    .registers 6

    .prologue
    const-wide/16 v2, 0xc8

    const/4 v1, 0x0

    .line 235
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->g:Landroid/support/v4/view/fk;

    if-eqz v0, :cond_c

    .line 236
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->g:Landroid/support/v4/view/fk;

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->a()V

    .line 238
    :cond_c
    if-nez p1, :cond_36

    .line 239
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_17

    .line 240
    invoke-static {p0, v1}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    .line 243
    :cond_17
    invoke-static {p0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    .line 244
    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    .line 246
    sget-object v1, Landroid/support/v7/internal/widget/al;->m:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;

    .line 247
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->h:Landroid/support/v7/internal/widget/aq;

    invoke-virtual {v1, v0, p1}, Landroid/support/v7/internal/widget/aq;->a(Landroid/support/v4/view/fk;I)Landroid/support/v7/internal/widget/aq;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    .line 248
    invoke-virtual {v0}, Landroid/support/v4/view/fk;->b()V

    .line 257
    :goto_35
    return-void

    .line 250
    :cond_36
    invoke-static {p0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    .line 251
    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    .line 253
    sget-object v1, Landroid/support/v7/internal/widget/al;->m:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;

    .line 254
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->h:Landroid/support/v7/internal/widget/aq;

    invoke-virtual {v1, v0, p1}, Landroid/support/v7/internal/widget/aq;->a(Landroid/support/v4/view/fk;I)Landroid/support/v7/internal/widget/aq;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    .line 255
    invoke-virtual {v0}, Landroid/support/v4/view/fk;->b()V

    goto :goto_35
.end method

.method private c()Z
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 172
    invoke-direct {p0}, Landroid/support/v7/internal/widget/al;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 178
    :goto_7
    return v4

    .line 174
    :cond_8
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/al;->removeView(Landroid/view/View;)V

    .line 175
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/widget/al;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 177
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/al;->setTabSelected(I)V

    goto :goto_7
.end method

.method private d()Landroid/support/v7/widget/aj;
    .registers 5

    .prologue
    .line 203
    new-instance v0, Landroid/support/v7/widget/aj;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    sget v3, Landroid/support/v7/a/d;->actionBarTabBarStyle:I

    invoke-direct {v0, v1, v2, v3}, Landroid/support/v7/widget/aj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 205
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->setMeasureWithLargestChildEnabled(Z)V

    .line 206
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->setGravity(I)V

    .line 207
    new-instance v1, Landroid/support/v7/widget/al;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/support/v7/widget/al;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 209
    return-object v0
.end method

.method private d(I)V
    .registers 3

    .prologue
    .line 349
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/aj;->removeViewAt(I)V

    .line 350
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_14

    .line 351
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 353
    :cond_14
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_1b

    .line 354
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 356
    :cond_1b
    return-void
.end method

.method private e()Landroid/widget/Spinner;
    .registers 5

    .prologue
    .line 213
    new-instance v0, Landroid/support/v7/widget/aa;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    sget v3, Landroid/support/v7/a/d;->actionDropDownStyle:I

    invoke-direct {v0, v1, v2, v3}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 215
    new-instance v1, Landroid/support/v7/widget/al;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/support/v7/widget/al;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 218
    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 219
    return-object v0
.end method

.method private f()V
    .registers 2

    .prologue
    .line 359
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj;->removeAllViews()V

    .line 360
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_14

    .line 361
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 363
    :cond_14
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_1b

    .line 364
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 366
    :cond_1b
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;
    .registers 7

    .prologue
    .line 292
    new-instance v0, Landroid/support/v7/internal/widget/ap;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Landroid/support/v7/internal/widget/ap;-><init>(Landroid/support/v7/internal/widget/al;Landroid/content/Context;Landroid/support/v7/app/g;Z)V

    .line 293
    if-eqz p2, :cond_1b

    .line 294
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ap;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 295
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Landroid/support/v7/internal/widget/al;->k:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ap;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 305
    :goto_1a
    return-object v0

    .line 298
    :cond_1b
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ap;->setFocusable(Z)V

    .line 300
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->j:Landroid/support/v7/internal/widget/ao;

    if-nez v1, :cond_2b

    .line 301
    new-instance v1, Landroid/support/v7/internal/widget/ao;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Landroid/support/v7/internal/widget/ao;-><init>(Landroid/support/v7/internal/widget/al;B)V

    iput-object v1, p0, Landroid/support/v7/internal/widget/al;->j:Landroid/support/v7/internal/widget/ao;

    .line 303
    :cond_2b
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->j:Landroid/support/v7/internal/widget/ao;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ap;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1a
.end method

.method public final a(I)V
    .registers 4

    .prologue
    .line 260
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 261
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_f

    .line 262
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/al;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 264
    :cond_f
    new-instance v1, Landroid/support/v7/internal/widget/am;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/internal/widget/am;-><init>(Landroid/support/v7/internal/widget/al;Landroid/view/View;)V

    iput-object v1, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    .line 271
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/al;->post(Ljava/lang/Runnable;)Z

    .line 272
    return-void
.end method

.method public final b(I)V
    .registers 3

    .prologue
    .line 339
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ap;->a()V

    .line 340
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_1a

    .line 341
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/an;->notifyDataSetChanged()V

    .line 343
    :cond_1a
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v0, :cond_21

    .line 344
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 346
    :cond_21
    return-void
.end method

.method public final onAttachedToWindow()V
    .registers 2

    .prologue
    .line 276
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onAttachedToWindow()V

    .line 277
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_c

    .line 279
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/al;->post(Ljava/lang/Runnable;)Z

    .line 281
    :cond_c
    return-void
.end method

.method protected final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4

    .prologue
    .line 223
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_9

    .line 224
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 227
    :cond_9
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/internal/view/a;->a(Landroid/content/Context;)Landroid/support/v7/internal/view/a;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Landroid/support/v7/internal/view/a;->b()I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/al;->setContentHeight(I)V

    .line 231
    invoke-virtual {v0}, Landroid/support/v7/internal/view/a;->c()I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/widget/al;->f:I

    .line 232
    return-void
.end method

.method public final onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 285
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onDetachedFromWindow()V

    .line 286
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_c

    .line 287
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/al;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 289
    :cond_c
    return-void
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7

    .prologue
    .line 370
    check-cast p2, Landroid/support/v7/internal/widget/ap;

    .line 3543
    iget-object v0, p2, Landroid/support/v7/internal/widget/ap;->a:Landroid/support/v7/app/g;

    .line 371
    invoke-virtual {v0}, Landroid/support/v7/app/g;->f()V

    .line 372
    return-void
.end method

.method public final onMeasure(II)V
    .registers 12

    .prologue
    const/4 v8, -0x2

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/4 v7, -0x1

    const/4 v2, 0x0

    .line 96
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 97
    if-ne v3, v6, :cond_c2

    move v0, v1

    .line 98
    :goto_d
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/al;->setFillViewport(Z)V

    .line 100
    iget-object v4, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v4}, Landroid/support/v7/widget/aj;->getChildCount()I

    move-result v4

    .line 101
    if-le v4, v1, :cond_cf

    if-eq v3, v6, :cond_1e

    const/high16 v5, -0x80000000

    if-ne v3, v5, :cond_cf

    .line 103
    :cond_1e
    const/4 v3, 0x2

    if-le v4, v3, :cond_c5

    .line 104
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3ecccccd    # 0.4f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Landroid/support/v7/internal/widget/al;->e:I

    .line 108
    :goto_2d
    iget v3, p0, Landroid/support/v7/internal/widget/al;->e:I

    iget v4, p0, Landroid/support/v7/internal/widget/al;->f:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Landroid/support/v7/internal/widget/al;->e:I

    .line 113
    :goto_37
    iget v3, p0, Landroid/support/v7/internal/widget/al;->k:I

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 115
    if-nez v0, :cond_d3

    iget-boolean v4, p0, Landroid/support/v7/internal/widget/al;->d:Z

    if-eqz v4, :cond_d3

    .line 117
    :goto_43
    if-eqz v1, :cond_da

    .line 119
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/widget/aj;->measure(II)V

    .line 120
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v1}, Landroid/support/v7/widget/aj;->getMeasuredWidth()I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    if-le v1, v4, :cond_d6

    .line 2153
    invoke-direct {p0}, Landroid/support/v7/internal/widget/al;->a()Z

    move-result v1

    if-nez v1, :cond_ad

    .line 2155
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-nez v1, :cond_79

    .line 2213
    new-instance v1, Landroid/support/v7/widget/aa;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    sget v6, Landroid/support/v7/a/d;->actionDropDownStyle:I

    invoke-direct {v1, v4, v5, v6}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2215
    new-instance v4, Landroid/support/v7/widget/al;

    invoke-direct {v4, v8, v7}, Landroid/support/v7/widget/al;-><init>(II)V

    invoke-virtual {v1, v4}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2218
    invoke-virtual {v1, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2156
    iput-object v1, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    .line 2158
    :cond_79
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/al;->removeView(Landroid/view/View;)V

    .line 2159
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v8, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v4}, Landroid/support/v7/internal/widget/al;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2161
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v1

    if-nez v1, :cond_9a

    .line 2162
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    new-instance v4, Landroid/support/v7/internal/widget/an;

    invoke-direct {v4, p0, v2}, Landroid/support/v7/internal/widget/an;-><init>(Landroid/support/v7/internal/widget/al;B)V

    invoke-virtual {v1, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 2164
    :cond_9a
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_a6

    .line 2165
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/al;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2166
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v7/internal/widget/al;->a:Ljava/lang/Runnable;

    .line 2168
    :cond_a6
    iget-object v1, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    iget v2, p0, Landroid/support/v7/internal/widget/al;->l:I

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 129
    :cond_ad
    :goto_ad
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->getMeasuredWidth()I

    move-result v1

    .line 130
    invoke-super {p0, p1, v3}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    .line 131
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->getMeasuredWidth()I

    move-result v2

    .line 133
    if-eqz v0, :cond_c1

    if-eq v1, v2, :cond_c1

    .line 135
    iget v0, p0, Landroid/support/v7/internal/widget/al;->l:I

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/al;->setTabSelected(I)V

    .line 137
    :cond_c1
    return-void

    :cond_c2
    move v0, v2

    .line 97
    goto/16 :goto_d

    .line 106
    :cond_c5
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iput v3, p0, Landroid/support/v7/internal/widget/al;->e:I

    goto/16 :goto_2d

    .line 110
    :cond_cf
    iput v7, p0, Landroid/support/v7/internal/widget/al;->e:I

    goto/16 :goto_37

    :cond_d3
    move v1, v2

    .line 115
    goto/16 :goto_43

    .line 123
    :cond_d6
    invoke-direct {p0}, Landroid/support/v7/internal/widget/al;->c()Z

    goto :goto_ad

    .line 126
    :cond_da
    invoke-direct {p0}, Landroid/support/v7/internal/widget/al;->c()Z

    goto :goto_ad
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 2

    .prologue
    .line 377
    return-void
.end method

.method public final setAllowCollapse(Z)V
    .registers 2

    .prologue
    .line 149
    iput-boolean p1, p0, Landroid/support/v7/internal/widget/al;->d:Z

    .line 150
    return-void
.end method

.method public final setContentHeight(I)V
    .registers 2

    .prologue
    .line 198
    iput p1, p0, Landroid/support/v7/internal/widget/al;->k:I

    .line 199
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/al;->requestLayout()V

    .line 200
    return-void
.end method

.method public final setTabSelected(I)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 182
    iput p1, p0, Landroid/support/v7/internal/widget/al;->l:I

    .line 183
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj;->getChildCount()I

    move-result v3

    move v2, v1

    .line 184
    :goto_a
    if-ge v2, v3, :cond_23

    .line 185
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->b:Landroid/support/v7/widget/aj;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 186
    if-ne v2, p1, :cond_21

    const/4 v0, 0x1

    .line 187
    :goto_15
    invoke-virtual {v4, v0}, Landroid/view/View;->setSelected(Z)V

    .line 188
    if-eqz v0, :cond_1d

    .line 189
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/widget/al;->a(I)V

    .line 184
    :cond_1d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_a

    :cond_21
    move v0, v1

    .line 186
    goto :goto_15

    .line 192
    :cond_23
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    if-eqz v0, :cond_2e

    if-ltz p1, :cond_2e

    .line 193
    iget-object v0, p0, Landroid/support/v7/internal/widget/al;->c:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 195
    :cond_2e
    return-void
.end method
