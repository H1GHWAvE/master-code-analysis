.class final Landroid/support/v7/internal/a/k;
.super Landroid/support/v7/internal/view/k;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/internal/a/e;


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/a/e;Landroid/view/Window$Callback;)V
    .registers 3

    .prologue
    .line 549
    iput-object p1, p0, Landroid/support/v7/internal/a/k;->a:Landroid/support/v7/internal/a/e;

    .line 550
    invoke-direct {p0, p2}, Landroid/support/v7/internal/view/k;-><init>(Landroid/view/Window$Callback;)V

    .line 551
    return-void
.end method


# virtual methods
.method public final onCreatePanelView(I)Landroid/view/View;
    .registers 12

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    const/4 v8, 0x1

    .line 565
    packed-switch p1, :pswitch_data_b4

    .line 573
    :cond_6
    invoke-super {p0, p1}, Landroid/support/v7/internal/view/k;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    .line 5510
    :goto_a
    return-object v0

    .line 567
    :pswitch_b
    iget-object v0, p0, Landroid/support/v7/internal/a/k;->a:Landroid/support/v7/internal/a/e;

    .line 4051
    iget-object v0, v0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    .line 567
    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->B()Landroid/view/Menu;

    move-result-object v1

    .line 568
    invoke-virtual {p0, p1, v2, v1}, Landroid/support/v7/internal/a/k;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0, p1, v1}, Landroid/support/v7/internal/a/k;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 569
    iget-object v3, p0, Landroid/support/v7/internal/a/k;->a:Landroid/support/v7/internal/a/e;

    .line 5516
    iget-object v0, v3, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    if-nez v0, :cond_85

    instance-of v0, v1, Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_85

    move-object v0, v1

    .line 5517
    check-cast v0, Landroid/support/v7/internal/view/menu/i;

    .line 5519
    iget-object v4, v3, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v4}, Landroid/support/v7/internal/widget/ad;->b()Landroid/content/Context;

    move-result-object v4

    .line 5520
    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    .line 5521
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    .line 5522
    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 5525
    sget v7, Landroid/support/v7/a/d;->actionBarPopupTheme:I

    invoke-virtual {v6, v7, v5, v8}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 5526
    iget v7, v5, Landroid/util/TypedValue;->resourceId:I

    if-eqz v7, :cond_54

    .line 5527
    iget v7, v5, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 5531
    :cond_54
    sget v7, Landroid/support/v7/a/d;->panelMenuListTheme:I

    invoke-virtual {v6, v7, v5, v8}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 5532
    iget v7, v5, Landroid/util/TypedValue;->resourceId:I

    if-eqz v7, :cond_8e

    .line 5533
    iget v5, v5, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v6, v5, v8}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 5538
    :goto_62
    new-instance v5, Landroid/view/ContextThemeWrapper;

    invoke-direct {v5, v4, v9}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 5539
    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 5542
    new-instance v4, Landroid/support/v7/internal/view/menu/g;

    sget v6, Landroid/support/v7/a/k;->abc_list_menu_item_layout:I

    invoke-direct {v4, v5, v6}, Landroid/support/v7/internal/view/menu/g;-><init>(Landroid/content/Context;I)V

    iput-object v4, v3, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    .line 5543
    iget-object v4, v3, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    new-instance v5, Landroid/support/v7/internal/a/j;

    invoke-direct {v5, v3, v9}, Landroid/support/v7/internal/a/j;-><init>(Landroid/support/v7/internal/a/e;B)V

    .line 6134
    iput-object v5, v4, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    .line 5544
    iget-object v4, v3, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0, v4}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;)V

    .line 5505
    :cond_85
    if-eqz v1, :cond_8b

    iget-object v0, v3, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    if-nez v0, :cond_94

    :cond_8b
    move-object v0, v2

    .line 5506
    goto/16 :goto_a

    .line 5535
    :cond_8e
    sget v5, Landroid/support/v7/a/m;->Theme_AppCompat_CompactMenu:I

    invoke-virtual {v6, v5, v8}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto :goto_62

    .line 5509
    :cond_94
    iget-object v0, v3, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/g;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_b0

    .line 5510
    iget-object v0, v3, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    iget-object v1, v3, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/ad;->a()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/g;->a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto/16 :goto_a

    :cond_b0
    move-object v0, v2

    .line 569
    goto/16 :goto_a

    .line 565
    nop

    :pswitch_data_b4
    .packed-switch 0x0
        :pswitch_b
    .end packed-switch
.end method

.method public final onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .registers 7

    .prologue
    .line 555
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/internal/view/k;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    .line 556
    if-eqz v0, :cond_18

    iget-object v1, p0, Landroid/support/v7/internal/a/k;->a:Landroid/support/v7/internal/a/e;

    .line 1051
    iget-boolean v1, v1, Landroid/support/v7/internal/a/e;->j:Z

    .line 556
    if-nez v1, :cond_18

    .line 557
    iget-object v1, p0, Landroid/support/v7/internal/a/k;->a:Landroid/support/v7/internal/a/e;

    .line 2051
    iget-object v1, v1, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    .line 557
    invoke-interface {v1}, Landroid/support/v7/internal/widget/ad;->p()V

    .line 558
    iget-object v1, p0, Landroid/support/v7/internal/a/k;->a:Landroid/support/v7/internal/a/e;

    .line 3051
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/support/v7/internal/a/e;->j:Z

    .line 560
    :cond_18
    return v0
.end method
