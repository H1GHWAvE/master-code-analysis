.class public final Landroid/support/v7/internal/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:I = 0x3

.field static final b:I = 0x5


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(I)I
    .registers 2

    .prologue
    .line 158
    const/4 v0, 0x3

    if-gt p0, v0, :cond_6

    .line 159
    sget v0, Landroid/support/v7/a/k;->notification_template_big_media_narrow:I

    .line 161
    :goto_5
    return v0

    :cond_6
    sget v0, Landroid/support/v7/a/k;->notification_template_big_media:I

    goto :goto_5
.end method

.method public static a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;
    .registers 6

    .prologue
    .line 144
    invoke-virtual {p1}, Landroid/support/v4/app/ek;->c()Landroid/app/PendingIntent;

    move-result-object v0

    if-nez v0, :cond_36

    const/4 v0, 0x1

    .line 145
    :goto_7
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget v3, Landroid/support/v7/a/k;->notification_media_action:I

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 147
    sget v2, Landroid/support/v7/a/i;->action0:I

    invoke-virtual {p1}, Landroid/support/v4/app/ek;->a()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 148
    if-nez v0, :cond_26

    .line 149
    sget v0, Landroid/support/v7/a/i;->action0:I

    invoke-virtual {p1}, Landroid/support/v4/app/ek;->c()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 151
    :cond_26
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-lt v0, v2, :cond_35

    .line 152
    sget v0, Landroid/support/v7/a/i;->action0:I

    invoke-virtual {p1}, Landroid/support/v4/app/ek;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 154
    :cond_35
    return-object v1

    .line 144
    :cond_36
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;
    .registers 24

    .prologue
    .line 169
    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    move/from16 v0, p10

    invoke-direct {v2, v3, v0}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 170
    const/4 v4, 0x0

    .line 171
    const/4 v3, 0x0

    .line 175
    if-eqz p5, :cond_ac

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x10

    if-lt v5, v6, :cond_ac

    .line 176
    sget v5, Landroid/support/v7/a/i;->icon:I

    move-object/from16 v0, p5

    invoke-virtual {v2, v5, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 180
    :goto_1c
    if-eqz p1, :cond_23

    .line 181
    sget v5, Landroid/support/v7/a/i;->title:I

    invoke-virtual {v2, v5, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 183
    :cond_23
    if-eqz p2, :cond_2b

    .line 184
    sget v4, Landroid/support/v7/a/i;->text:I

    invoke-virtual {v2, v4, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 185
    const/4 v4, 0x1

    .line 187
    :cond_2b
    if-eqz p3, :cond_b5

    .line 188
    sget v4, Landroid/support/v7/a/i;->info:I

    invoke-virtual {v2, v4, p3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 189
    sget v4, Landroid/support/v7/a/i;->info:I

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 190
    const/4 v4, 0x1

    move v8, v4

    .line 208
    :goto_3a
    if-eqz p6, :cond_57

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_57

    .line 209
    sget v4, Landroid/support/v7/a/i;->text:I

    move-object/from16 v0, p6

    invoke-virtual {v2, v4, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 210
    if-eqz p2, :cond_f9

    .line 211
    sget v3, Landroid/support/v7/a/i;->text2:I

    invoke-virtual {v2, v3, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 212
    sget v3, Landroid/support/v7/a/i;->text2:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 213
    const/4 v3, 0x1

    .line 220
    :cond_57
    :goto_57
    if-eqz v3, :cond_7b

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_7b

    .line 221
    if-eqz p11, :cond_72

    .line 223
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 224
    sget v4, Landroid/support/v7/a/g;->notification_subtext_size:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    .line 226
    sget v4, Landroid/support/v7/a/i;->text:I

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5, v3}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    .line 229
    :cond_72
    sget v3, Landroid/support/v7/a/i;->line1:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    .line 232
    :cond_7b
    const-wide/16 v4, 0x0

    cmp-long v3, p8, v4

    if-eqz v3, :cond_a3

    .line 233
    if-eqz p7, :cond_102

    .line 234
    sget v3, Landroid/support/v7/a/i;->chronometer:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 235
    sget v3, Landroid/support/v7/a/i;->chronometer:I

    const-string v4, "setBase"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v6, v10

    add-long v6, v6, p8

    invoke-virtual {v2, v3, v4, v6, v7}, Landroid/widget/RemoteViews;->setLong(ILjava/lang/String;J)V

    .line 237
    sget v3, Landroid/support/v7/a/i;->chronometer:I

    const-string v4, "setStarted"

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    .line 243
    :cond_a3
    :goto_a3
    sget v4, Landroid/support/v7/a/i;->line3:I

    if-eqz v8, :cond_112

    const/4 v3, 0x0

    :goto_a8
    invoke-virtual {v2, v4, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 244
    return-object v2

    .line 178
    :cond_ac
    sget v5, Landroid/support/v7/a/i;->icon:I

    const/16 v6, 0x8

    invoke-virtual {v2, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_1c

    .line 191
    :cond_b5
    if-lez p4, :cond_ef

    .line 192
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Landroid/support/v7/a/j;->status_bar_notification_info_maxnum:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 194
    move/from16 v0, p4

    if-le v0, v4, :cond_de

    .line 195
    sget v4, Landroid/support/v7/a/i;->info:I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Landroid/support/v7/a/l;->status_bar_notification_info_overflow:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 201
    :goto_d4
    sget v4, Landroid/support/v7/a/i;->info:I

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 202
    const/4 v4, 0x1

    move v8, v4

    .line 203
    goto/16 :goto_3a

    .line 198
    :cond_de
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v4

    .line 199
    sget v5, Landroid/support/v7/a/i;->info:I

    move/from16 v0, p4

    int-to-long v6, v0

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_d4

    .line 204
    :cond_ef
    sget v5, Landroid/support/v7/a/i;->info:I

    const/16 v6, 0x8

    invoke-virtual {v2, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    move v8, v4

    goto/16 :goto_3a

    .line 215
    :cond_f9
    sget v4, Landroid/support/v7/a/i;->text2:I

    const/16 v5, 0x8

    invoke-virtual {v2, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_57

    .line 239
    :cond_102
    sget v3, Landroid/support/v7/a/i;->time:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 240
    sget v3, Landroid/support/v7/a/i;->time:I

    const-string v4, "setTime"

    move-wide/from16 v0, p8

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/widget/RemoteViews;->setLong(ILjava/lang/String;J)V

    goto :goto_a3

    .line 243
    :cond_112
    const/16 v3, 0x8

    goto :goto_a8
.end method

.method private static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJLjava/util/List;ZLandroid/app/PendingIntent;)Landroid/widget/RemoteViews;
    .registers 29

    .prologue
    .line 119
    invoke-interface/range {p10 .. p10}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v14

    .line 2158
    const/4 v2, 0x3

    if-gt v14, v2, :cond_47

    .line 2159
    sget v12, Landroid/support/v7/a/k;->notification_template_big_media_narrow:I

    .line 120
    :goto_e
    const/4 v13, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    move-wide/from16 v10, p8

    invoke-static/range {v2 .. v13}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;

    move-result-object v4

    .line 124
    sget v2, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v4, v2}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 125
    if-lez v14, :cond_4a

    .line 126
    const/4 v2, 0x0

    move v3, v2

    :goto_2e
    if-ge v3, v14, :cond_4a

    .line 127
    move-object/from16 v0, p10

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/ek;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;

    move-result-object v2

    .line 128
    sget v5, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v4, v5, v2}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 126
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2e

    .line 2161
    :cond_47
    sget v12, Landroid/support/v7/a/k;->notification_template_big_media:I

    goto :goto_e

    .line 131
    :cond_4a
    if-eqz p11, :cond_6b

    .line 132
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const/4 v3, 0x0

    invoke-virtual {v4, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 133
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const-string v3, "setAlpha"

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Landroid/support/v7/a/j;->cancel_button_image_alpha:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-virtual {v4, v2, v3, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 135
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    move-object/from16 v0, p12

    invoke-virtual {v4, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 139
    :goto_6a
    return-object v4

    .line 137
    :cond_6b
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const/16 v3, 0x8

    invoke-virtual {v4, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_6a
.end method

.method private static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJLjava/util/List;[IZLandroid/app/PendingIntent;)Landroid/widget/RemoteViews;
    .registers 28

    .prologue
    .line 66
    sget v12, Landroid/support/v7/a/k;->notification_template_media:I

    const/4 v13, 0x1

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    move-wide/from16 v10, p8

    invoke-static/range {v2 .. v13}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;

    move-result-object v5

    .line 70
    invoke-interface/range {p10 .. p10}, Ljava/util/List;->size()I

    move-result v6

    .line 71
    if-nez p11, :cond_4b

    const/4 v2, 0x0

    move v3, v2

    .line 74
    :goto_1f
    sget v2, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v5, v2}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 75
    if-lez v3, :cond_6c

    .line 76
    const/4 v2, 0x0

    move v4, v2

    :goto_28
    if-ge v4, v3, :cond_6c

    .line 77
    if-lt v4, v6, :cond_55

    .line 78
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "setShowActionsInCompactView: action %d out of bounds (max %d)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v7

    const/4 v4, 0x1

    add-int/lit8 v6, v6, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 71
    :cond_4b
    move-object/from16 v0, p11

    array-length v2, v0

    const/4 v3, 0x3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v3, v2

    goto :goto_1f

    .line 83
    :cond_55
    aget v2, p11, v4

    move-object/from16 v0, p10

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/ek;

    .line 84
    invoke-static {p0, v2}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;

    move-result-object v2

    .line 85
    sget v7, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v5, v7, v2}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 76
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_28

    .line 88
    :cond_6c
    if-eqz p12, :cond_94

    .line 89
    sget v2, Landroid/support/v7/a/i;->end_padder:I

    const/16 v3, 0x8

    invoke-virtual {v5, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 90
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 91
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    move-object/from16 v0, p13

    invoke-virtual {v5, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 92
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const-string v3, "setAlpha"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v6, Landroid/support/v7/a/j;->cancel_button_image_alpha:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    invoke-virtual {v5, v2, v3, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 98
    :goto_93
    return-object v5

    .line 95
    :cond_94
    sget v2, Landroid/support/v7/a/i;->end_padder:I

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 96
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const/16 v3, 0x8

    invoke-virtual {v5, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_93
.end method

.method private static a(Landroid/app/Notification;Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJLjava/util/List;ZLandroid/app/PendingIntent;)V
    .registers 29

    .prologue
    .line 106
    .line 1119
    invoke-interface/range {p11 .. p11}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v14

    .line 1158
    const/4 v2, 0x3

    if-gt v14, v2, :cond_47

    .line 1159
    sget v12, Landroid/support/v7/a/k;->notification_template_big_media_narrow:I

    .line 1120
    :goto_e
    const/4 v13, 0x0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-wide/from16 v10, p9

    invoke-static/range {v2 .. v13}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;

    move-result-object v4

    .line 1124
    sget v2, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v4, v2}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 1125
    if-lez v14, :cond_4a

    .line 1126
    const/4 v2, 0x0

    move v3, v2

    :goto_2e
    if-ge v3, v14, :cond_4a

    .line 1127
    move-object/from16 v0, p11

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/ek;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;

    move-result-object v2

    .line 1128
    sget v5, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v4, v5, v2}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 1126
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2e

    .line 1161
    :cond_47
    sget v12, Landroid/support/v7/a/k;->notification_template_big_media:I

    goto :goto_e

    .line 1131
    :cond_4a
    if-eqz p12, :cond_75

    .line 1132
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const/4 v3, 0x0

    invoke-virtual {v4, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1133
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const-string v3, "setAlpha"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Landroid/support/v7/a/j;->cancel_button_image_alpha:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-virtual {v4, v2, v3, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 1135
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    move-object/from16 v0, p13

    invoke-virtual {v4, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 106
    :goto_6a
    iput-object v4, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 109
    if-eqz p12, :cond_74

    .line 110
    iget v2, p0, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Landroid/app/Notification;->flags:I

    .line 112
    :cond_74
    return-void

    .line 1137
    :cond_75
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const/16 v3, 0x8

    invoke-virtual {v4, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_6a
.end method

.method private static a(Landroid/support/v4/app/dc;Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJLjava/util/List;[IZLandroid/app/PendingIntent;)V
    .registers 30

    .prologue
    .line 52
    .line 1066
    sget v12, Landroid/support/v7/a/k;->notification_template_media:I

    const/4 v13, 0x1

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-wide/from16 v10, p9

    invoke-static/range {v2 .. v13}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;

    move-result-object v5

    .line 1070
    invoke-interface/range {p11 .. p11}, Ljava/util/List;->size()I

    move-result v6

    .line 1071
    if-nez p12, :cond_4d

    const/4 v2, 0x0

    move v3, v2

    .line 1074
    :goto_21
    sget v2, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v5, v2}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 1075
    if-lez v3, :cond_70

    .line 1076
    const/4 v2, 0x0

    move v4, v2

    :goto_2a
    if-ge v4, v3, :cond_70

    .line 1077
    if-lt v4, v6, :cond_57

    .line 1078
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "setShowActionsInCompactView: action %d out of bounds (max %d)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v7

    const/4 v4, 0x1

    add-int/lit8 v6, v6, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1071
    :cond_4d
    move-object/from16 v0, p12

    array-length v2, v0

    const/4 v3, 0x3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v3, v2

    goto :goto_21

    .line 1083
    :cond_57
    aget v2, p12, v4

    move-object/from16 v0, p11

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/ek;

    .line 1084
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;

    move-result-object v2

    .line 1085
    sget v7, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v5, v7, v2}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 1076
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2a

    .line 1088
    :cond_70
    if-eqz p13, :cond_a9

    .line 1089
    sget v2, Landroid/support/v7/a/i;->end_padder:I

    const/16 v3, 0x8

    invoke-virtual {v5, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1090
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1091
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    move-object/from16 v0, p14

    invoke-virtual {v5, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1092
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const-string v3, "setAlpha"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v6, Landroid/support/v7/a/j;->cancel_button_image_alpha:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    invoke-virtual {v5, v2, v3, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 55
    :goto_97
    invoke-interface {p0}, Landroid/support/v4/app/dc;->a()Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/Notification$Builder;->setContent(Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;

    .line 56
    if-eqz p13, :cond_a8

    .line 57
    invoke-interface {p0}, Landroid/support/v4/app/dc;->a()Landroid/app/Notification$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 59
    :cond_a8
    return-void

    .line 1095
    :cond_a9
    sget v2, Landroid/support/v7/a/i;->end_padder:I

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1096
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const/16 v3, 0x8

    invoke-virtual {v5, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_97
.end method
