.class public Landroid/support/v7/internal/view/menu/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/g/a/a;


# static fields
.field private static final p:Ljava/lang/String; = "MenuBuilder"

.field private static final q:Ljava/lang/String; = "android:menu:presenters"

.field private static final r:Ljava/lang/String; = "android:menu:actionviewstates"

.field private static final s:Ljava/lang/String; = "android:menu:expandedactionview"

.field private static final t:[I


# instance fields
.field private A:Z

.field private B:Landroid/view/ContextMenu$ContextMenuInfo;

.field private C:Landroid/util/SparseArray;

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Ljava/util/ArrayList;

.field private H:Ljava/util/concurrent/CopyOnWriteArrayList;

.field public final e:Landroid/content/Context;

.field public f:Landroid/support/v7/internal/view/menu/j;

.field g:Ljava/util/ArrayList;

.field public h:Ljava/util/ArrayList;

.field public i:I

.field j:Ljava/lang/CharSequence;

.field k:Landroid/graphics/drawable/Drawable;

.field l:Landroid/view/View;

.field m:Z

.field n:Landroid/support/v7/internal/view/menu/m;

.field public o:Z

.field private final u:Landroid/content/res/Resources;

.field private v:Z

.field private w:Z

.field private x:Ljava/util/ArrayList;

.field private y:Z

.field private z:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 62
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Landroid/support/v7/internal/view/menu/i;->t:[I

    return-void

    nop

    :array_a
    .array-data 4
        0x1
        0x4
        0x5
        0x3
        0x2
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput v0, p0, Landroid/support/v7/internal/view/menu/i;->i:I

    .line 160
    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->D:Z

    .line 162
    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->E:Z

    .line 164
    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->m:Z

    .line 166
    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->F:Z

    .line 168
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/i;->G:Ljava/util/ArrayList;

    .line 170
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 215
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    .line 216
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/i;->u:Landroid/content/res/Resources;

    .line 218
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/i;->x:Ljava/util/ArrayList;

    .line 221
    iput-boolean v1, p0, Landroid/support/v7/internal/view/menu/i;->y:Z

    .line 223
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/i;->h:Ljava/util/ArrayList;

    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/i;->z:Ljava/util/ArrayList;

    .line 225
    iput-boolean v1, p0, Landroid/support/v7/internal/view/menu/i;->A:Z

    .line 227
    invoke-direct {p0, v1}, Landroid/support/v7/internal/view/menu/i;->e(Z)V

    .line 228
    return-void
.end method

.method private static a(Ljava/util/ArrayList;I)I
    .registers 4

    .prologue
    .line 824
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_7
    if-ltz v1, :cond_1a

    .line 825
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 5213
    iget v0, v0, Landroid/support/v7/internal/view/menu/m;->f:I

    .line 826
    if-gt v0, p1, :cond_16

    .line 827
    add-int/lit8 v0, v1, 0x1

    .line 831
    :goto_15
    return v0

    .line 824
    :cond_16
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_7

    .line 831
    :cond_1a
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private a(Landroid/view/View;)Landroid/support/v7/internal/view/menu/i;
    .registers 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1269
    move-object v0, p0

    move v3, v1

    move-object v4, v2

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/internal/view/menu/i;->a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    .line 1270
    return-object p0
.end method

.method private a(IIIILjava/lang/CharSequence;I)Landroid/support/v7/internal/view/menu/m;
    .registers 15

    .prologue
    .line 452
    new-instance v0, Landroid/support/v7/internal/view/menu/m;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Landroid/support/v7/internal/view/menu/m;-><init>(Landroid/support/v7/internal/view/menu/i;IIIILjava/lang/CharSequence;I)V

    return-object v0
.end method

.method private a(ILandroid/view/KeyEvent;)Landroid/support/v7/internal/view/menu/m;
    .registers 15

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 903
    iget-object v5, p0, Landroid/support/v7/internal/view/menu/i;->G:Ljava/util/ArrayList;

    .line 904
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 905
    invoke-direct {p0, v5, p1, p2}, Landroid/support/v7/internal/view/menu/i;->a(Ljava/util/List;ILandroid/view/KeyEvent;)V

    .line 907
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    move-object v0, v2

    .line 938
    :cond_11
    :goto_11
    return-object v0

    .line 911
    :cond_12
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v6

    .line 912
    new-instance v7, Landroid/view/KeyCharacterMap$KeyData;

    invoke-direct {v7}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    .line 914
    invoke-virtual {p2, v7}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    .line 917
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 918
    const/4 v0, 0x1

    if-ne v8, v0, :cond_2c

    .line 919
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    goto :goto_11

    .line 922
    :cond_2c
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->b()Z

    move-result v9

    move v3, v4

    .line 925
    :goto_31
    if-ge v3, v8, :cond_67

    .line 926
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 927
    if-eqz v9, :cond_62

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getAlphabeticShortcut()C

    move-result v1

    .line 929
    :goto_3f
    iget-object v10, v7, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    aget-char v10, v10, v4

    if-ne v1, v10, :cond_49

    and-int/lit8 v10, v6, 0x2

    if-eqz v10, :cond_11

    :cond_49
    iget-object v10, v7, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    const/4 v11, 0x2

    aget-char v10, v10, v11

    if-ne v1, v10, :cond_54

    and-int/lit8 v10, v6, 0x2

    if-nez v10, :cond_11

    :cond_54
    if-eqz v9, :cond_5e

    const/16 v10, 0x8

    if-ne v1, v10, :cond_5e

    const/16 v1, 0x43

    if-eq p1, v1, :cond_11

    .line 925
    :cond_5e
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_31

    .line 927
    :cond_62
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getNumericShortcut()C

    move-result v1

    goto :goto_3f

    :cond_67
    move-object v0, v2

    .line 938
    goto :goto_11
.end method

.method private a(I)V
    .registers 3

    .prologue
    .line 570
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/view/menu/i;->a(IZ)V

    .line 571
    return-void
.end method

.method private a(IZ)V
    .registers 4

    .prologue
    .line 562
    if-ltz p1, :cond_a

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_b

    .line 567
    :cond_a
    :goto_a
    return-void

    .line 564
    :cond_b
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 566
    if-eqz p2, :cond_a

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    goto :goto_a
.end method

.method private a(Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 2

    .prologue
    .line 1302
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/i;->B:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 1303
    return-void
.end method

.method private a(Landroid/view/MenuItem;)V
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 593
    invoke-interface {p1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v4

    .line 595
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 596
    :goto_c
    if-ge v3, v5, :cond_34

    .line 597
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 598
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v1

    if-ne v1, v4, :cond_2e

    .line 599
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->e()Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 600
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->isCheckable()Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 603
    if-ne v0, p1, :cond_32

    const/4 v1, 0x1

    :goto_2b
    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/m;->b(Z)V

    .line 596
    :cond_2e
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_c

    :cond_32
    move v1, v2

    .line 603
    goto :goto_2b

    .line 606
    :cond_34
    return-void
.end method

.method private a(Ljava/util/List;ILandroid/view/KeyEvent;)V
    .registers 15

    .prologue
    const/16 v10, 0x43

    const/4 v3, 0x0

    .line 859
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->b()Z

    move-result v4

    .line 860
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v5

    .line 861
    new-instance v6, Landroid/view/KeyCharacterMap$KeyData;

    invoke-direct {v6}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    .line 863
    invoke-virtual {p3, v6}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    move-result v0

    .line 865
    if-nez v0, :cond_19

    if-eq p2, v10, :cond_19

    .line 887
    :cond_18
    return-void

    .line 870
    :cond_19
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v2, v3

    .line 871
    :goto_20
    if-ge v2, v7, :cond_18

    .line 872
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 873
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->hasSubMenu()Z

    move-result v1

    if-eqz v1, :cond_39

    .line 874
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/i;

    invoke-direct {v1, p1, p2, p3}, Landroid/support/v7/internal/view/menu/i;->a(Ljava/util/List;ILandroid/view/KeyEvent;)V

    .line 876
    :cond_39
    if-eqz v4, :cond_67

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getAlphabeticShortcut()C

    move-result v1

    .line 877
    :goto_3f
    and-int/lit8 v8, v5, 0x5

    if-nez v8, :cond_63

    if-eqz v1, :cond_63

    iget-object v8, v6, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    aget-char v8, v8, v3

    if-eq v1, v8, :cond_5a

    iget-object v8, v6, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    const/4 v9, 0x2

    aget-char v8, v8, v9

    if-eq v1, v8, :cond_5a

    if-eqz v4, :cond_63

    const/16 v8, 0x8

    if-ne v1, v8, :cond_63

    if-ne p2, v10, :cond_63

    :cond_5a
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_63

    .line 884
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 871
    :cond_63
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_20

    .line 876
    :cond_67
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getNumericShortcut()C

    move-result v1

    goto :goto_3f
.end method

.method private a(Landroid/support/v7/internal/view/menu/ad;Landroid/support/v7/internal/view/menu/x;)Z
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 292
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 309
    :goto_9
    return v0

    .line 297
    :cond_a
    if-eqz p2, :cond_10

    .line 298
    invoke-interface {p2, p1}, Landroid/support/v7/internal/view/menu/x;->a(Landroid/support/v7/internal/view/menu/ad;)Z

    move-result v0

    .line 301
    :cond_10
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 302
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/x;

    .line 303
    if-nez v1, :cond_31

    .line 304
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_17

    .line 305
    :cond_31
    if-nez v2, :cond_3b

    .line 306
    invoke-interface {v1, p1}, Landroid/support/v7/internal/view/menu/x;->a(Landroid/support/v7/internal/view/menu/ad;)Z

    move-result v0

    :goto_37
    move v2, v0

    .line 308
    goto :goto_17

    :cond_39
    move v0, v2

    .line 309
    goto :goto_9

    :cond_3b
    move v0, v2

    goto :goto_37
.end method

.method private a(Landroid/view/MenuItem;I)Z
    .registers 4

    .prologue
    .line 948
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z

    move-result v0

    return v0
.end method

.method private b(I)I
    .registers 5

    .prologue
    .line 689
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->size()I

    move-result v2

    .line 691
    const/4 v1, 0x0

    :goto_5
    if-ge v1, v2, :cond_1a

    .line 692
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 693
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getItemId()I

    move-result v0

    if-ne v0, p1, :cond_17

    move v0, v1

    .line 698
    :goto_16
    return v0

    .line 691
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 698
    :cond_1a
    const/4 v0, -0x1

    goto :goto_16
.end method

.method private c(I)I
    .registers 5

    .prologue
    .line 702
    .line 4706
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->size()I

    move-result v2

    .line 4712
    const/4 v1, 0x0

    :goto_5
    if-ge v1, v2, :cond_1a

    .line 4713
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 4715
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v0

    if-ne v0, p1, :cond_17

    move v0, v1

    .line 4716
    :goto_16
    return v0

    .line 4712
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 4720
    :cond_1a
    const/4 v0, -0x1

    .line 702
    goto :goto_16
.end method

.method private d(I)I
    .registers 5

    .prologue
    .line 706
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->size()I

    move-result v2

    .line 712
    const/4 v1, 0x0

    :goto_5
    if-ge v1, v2, :cond_1a

    .line 713
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 715
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v0

    if-ne v0, p1, :cond_17

    move v0, v1

    .line 720
    :goto_16
    return v0

    .line 712
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 720
    :cond_1a
    const/4 v0, -0x1

    goto :goto_16
.end method

.method private d(Z)V
    .registers 5

    .prologue
    .line 276
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 288
    :goto_8
    return-void

    .line 278
    :cond_9
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->d()V

    .line 279
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 280
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/x;

    .line 281
    if-nez v1, :cond_2c

    .line 282
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_12

    .line 284
    :cond_2c
    invoke-interface {v1, p1}, Landroid/support/v7/internal/view/menu/x;->a(Z)V

    goto :goto_12

    .line 287
    :cond_30
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->e()V

    goto :goto_8
.end method

.method private static e(I)I
    .registers 3

    .prologue
    .line 757
    const/high16 v0, -0x10000

    and-int/2addr v0, p0

    shr-int/lit8 v0, v0, 0x10

    .line 759
    if-ltz v0, :cond_c

    sget-object v1, Landroid/support/v7/internal/view/menu/i;->t:[I

    array-length v1, v1

    if-lt v0, v1, :cond_14

    .line 760
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "order does not contain a valid category."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 763
    :cond_14
    sget-object v1, Landroid/support/v7/internal/view/menu/i;->t:[I

    aget v0, v1, v0

    shl-int/lit8 v0, v0, 0x10

    const v1, 0xffff

    and-int/2addr v1, p0

    or-int/2addr v0, v1

    return v0
.end method

.method private e(Landroid/os/Bundle;)V
    .registers 6

    .prologue
    .line 313
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 333
    :goto_8
    return-void

    .line 315
    :cond_9
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    .line 317
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_14
    :goto_14
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 318
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/x;

    .line 319
    if-nez v1, :cond_2e

    .line 320
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_14

    .line 322
    :cond_2e
    invoke-interface {v1}, Landroid/support/v7/internal/view/menu/x;->b()I

    move-result v0

    .line 323
    if-lez v0, :cond_14

    .line 324
    invoke-interface {v1}, Landroid/support/v7/internal/view/menu/x;->c()Landroid/os/Parcelable;

    move-result-object v1

    .line 325
    if-eqz v1, :cond_14

    .line 326
    invoke-virtual {v2, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_14

    .line 332
    :cond_3e
    const-string v0, "android:menu:presenters"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    goto :goto_8
.end method

.method private e(Z)V
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 790
    if-eqz p1, :cond_1a

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->u:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->keyboard:I

    if-eq v1, v0, :cond_1a

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->u:Landroid/content/res/Resources;

    sget v2, Landroid/support/v7/a/e;->abc_config_showMenuShortcutsWhenKeyboardPresent:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_1a

    :goto_17
    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->w:Z

    .line 793
    return-void

    .line 790
    :cond_1a
    const/4 v0, 0x0

    goto :goto_17
.end method

.method private f(I)Landroid/support/v7/internal/view/menu/i;
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 1233
    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/internal/view/menu/i;->a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    .line 1234
    return-object p0
.end method

.method private f(Landroid/os/Bundle;)V
    .registers 6

    .prologue
    .line 336
    const-string v0, "android:menu:presenters"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v2

    .line 338
    if-eqz v2, :cond_10

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 354
    :cond_10
    return-void

    .line 340
    :cond_11
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_17
    :goto_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 341
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/x;

    .line 342
    if-nez v1, :cond_31

    .line 343
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_17

    .line 345
    :cond_31
    invoke-interface {v1}, Landroid/support/v7/internal/view/menu/x;->b()I

    move-result v0

    .line 346
    if-lez v0, :cond_17

    .line 347
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 348
    if-eqz v0, :cond_17

    .line 349
    invoke-interface {v1, v0}, Landroid/support/v7/internal/view/menu/x;->a(Landroid/os/Parcelable;)V

    goto :goto_17
.end method

.method private f(Z)V
    .registers 2

    .prologue
    .line 1306
    iput-boolean p1, p0, Landroid/support/v7/internal/view/menu/i;->m:Z

    .line 1307
    return-void
.end method

.method private g(I)Landroid/support/v7/internal/view/menu/i;
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 1257
    const/4 v1, 0x0

    move-object v0, p0

    move v3, p1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/internal/view/menu/i;->a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    .line 1258
    return-object p0
.end method

.method private g(Z)V
    .registers 2

    .prologue
    .line 1367
    iput-boolean p1, p0, Landroid/support/v7/internal/view/menu/i;->o:Z

    .line 1368
    return-void
.end method

.method private l()Landroid/support/v7/internal/view/menu/i;
    .registers 2

    .prologue
    .line 231
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v7/internal/view/menu/i;->i:I

    .line 232
    return-object p0
.end method

.method private m()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 574
    iput-boolean v1, p0, Landroid/support/v7/internal/view/menu/i;->D:Z

    .line 575
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->clear()V

    .line 576
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->clearHeader()V

    .line 577
    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->D:Z

    .line 578
    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->E:Z

    .line 579
    invoke-virtual {p0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 580
    return-void
.end method

.method private n()Landroid/content/res/Resources;
    .registers 2

    .prologue
    .line 803
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->u:Landroid/content/res/Resources;

    return-object v0
.end method

.method private o()Landroid/content/Context;
    .registers 2

    .prologue
    .line 807
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    return-object v0
.end method

.method private p()V
    .registers 2

    .prologue
    .line 818
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->f:Landroid/support/v7/internal/view/menu/j;

    if-eqz v0, :cond_9

    .line 819
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->f:Landroid/support/v7/internal/view/menu/j;

    invoke-interface {v0, p0}, Landroid/support/v7/internal/view/menu/j;->a(Landroid/support/v7/internal/view/menu/i;)V

    .line 821
    :cond_9
    return-void
.end method

.method private q()Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 1165
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->i()V

    .line 1166
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method private r()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 1274
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private s()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 1278
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->k:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private t()Landroid/view/View;
    .registers 2

    .prologue
    .line 1282
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->l:Landroid/view/View;

    return-object v0
.end method

.method private u()Z
    .registers 2

    .prologue
    .line 1310
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->m:Z

    return v0
.end method

.method private v()Landroid/support/v7/internal/view/menu/m;
    .registers 2

    .prologue
    .line 1358
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->n:Landroid/support/v7/internal/view/menu/m;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/graphics/drawable/Drawable;)Landroid/support/v7/internal/view/menu/i;
    .registers 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1245
    move-object v0, p0

    move v3, v1

    move-object v4, p1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/internal/view/menu/i;->a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    .line 1246
    return-object p0
.end method

.method protected final a(Ljava/lang/CharSequence;)Landroid/support/v7/internal/view/menu/i;
    .registers 8

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 1221
    move-object v0, p0

    move-object v2, p1

    move v3, v1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/internal/view/menu/i;->a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    .line 1222
    return-object p0
.end method

.method public final a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 13

    .prologue
    .line 433
    .line 2757
    const/high16 v0, -0x10000

    and-int/2addr v0, p3

    shr-int/lit8 v0, v0, 0x10

    .line 2759
    if-ltz v0, :cond_c

    sget-object v1, Landroid/support/v7/internal/view/menu/i;->t:[I

    array-length v1, v1

    if-lt v0, v1, :cond_14

    .line 2760
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "order does not contain a valid category."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2763
    :cond_14
    sget-object v1, Landroid/support/v7/internal/view/menu/i;->t:[I

    aget v0, v1, v0

    shl-int/lit8 v0, v0, 0x10

    const v1, 0xffff

    and-int/2addr v1, p3

    or-int v5, v0, v1

    .line 435
    iget v7, p0, Landroid/support/v7/internal/view/menu/i;->i:I

    .line 3452
    new-instance v0, Landroid/support/v7/internal/view/menu/m;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v7}, Landroid/support/v7/internal/view/menu/m;-><init>(Landroid/support/v7/internal/view/menu/i;IIIILjava/lang/CharSequence;I)V

    .line 438
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->B:Landroid/view/ContextMenu$ContextMenuInfo;

    if-eqz v1, :cond_34

    .line 440
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->B:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 3545
    iput-object v1, v0, Landroid/support/v7/internal/view/menu/m;->k:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 443
    :cond_34
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-static {v2, v5}, Landroid/support/v7/internal/view/menu/i;->a(Ljava/util/ArrayList;I)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 444
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 446
    return-object v0
.end method

.method protected a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 422
    const-string v0, "android:menu:actionviewstates"

    return-object v0
.end method

.method final a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 1184
    .line 9803
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->u:Landroid/content/res/Resources;

    .line 1186
    if-eqz p5, :cond_10

    .line 1187
    iput-object p5, p0, Landroid/support/v7/internal/view/menu/i;->l:Landroid/view/View;

    .line 1190
    iput-object v1, p0, Landroid/support/v7/internal/view/menu/i;->j:Ljava/lang/CharSequence;

    .line 1191
    iput-object v1, p0, Landroid/support/v7/internal/view/menu/i;->k:Landroid/graphics/drawable/Drawable;

    .line 1210
    :goto_b
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 1211
    return-void

    .line 1193
    :cond_10
    if-lez p1, :cond_25

    .line 1194
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/i;->j:Ljava/lang/CharSequence;

    .line 1199
    :cond_18
    :goto_18
    if-lez p3, :cond_2a

    .line 9807
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    .line 1200
    invoke-static {v0, p3}, Landroid/support/v4/c/h;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/i;->k:Landroid/graphics/drawable/Drawable;

    .line 1206
    :cond_22
    :goto_22
    iput-object v1, p0, Landroid/support/v7/internal/view/menu/i;->l:Landroid/view/View;

    goto :goto_b

    .line 1195
    :cond_25
    if-eqz p2, :cond_18

    .line 1196
    iput-object p2, p0, Landroid/support/v7/internal/view/menu/i;->j:Ljava/lang/CharSequence;

    goto :goto_18

    .line 1201
    :cond_2a
    if-eqz p4, :cond_22

    .line 1202
    iput-object p4, p0, Landroid/support/v7/internal/view/menu/i;->k:Landroid/graphics/drawable/Drawable;

    goto :goto_22
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 6

    .prologue
    .line 357
    .line 2313
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_42

    .line 2315
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    .line 2317
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_13
    :goto_13
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 2318
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/x;

    .line 2319
    if-nez v1, :cond_2d

    .line 2320
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_13

    .line 2322
    :cond_2d
    invoke-interface {v1}, Landroid/support/v7/internal/view/menu/x;->b()I

    move-result v0

    .line 2323
    if-lez v0, :cond_13

    .line 2324
    invoke-interface {v1}, Landroid/support/v7/internal/view/menu/x;->c()Landroid/os/Parcelable;

    move-result-object v1

    .line 2325
    if-eqz v1, :cond_13

    .line 2326
    invoke-virtual {v2, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_13

    .line 2332
    :cond_3d
    const-string v0, "android:menu:presenters"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 358
    :cond_42
    return-void
.end method

.method public a(Landroid/support/v7/internal/view/menu/j;)V
    .registers 2

    .prologue
    .line 426
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/i;->f:Landroid/support/v7/internal/view/menu/j;

    .line 427
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/x;)V
    .registers 3

    .prologue
    .line 242
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V

    .line 243
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V
    .registers 5

    .prologue
    .line 255
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 256
    invoke-interface {p1, p2, p0}, Landroid/support/v7/internal/view/menu/x;->a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V

    .line 257
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->A:Z

    .line 258
    return-void
.end method

.method public a(Z)V
    .registers 3

    .prologue
    .line 781
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->w:Z

    if-ne v0, p1, :cond_5

    .line 787
    :goto_4
    return-void

    .line 785
    :cond_5
    invoke-direct {p0, p1}, Landroid/support/v7/internal/view/menu/i;->e(Z)V

    .line 786
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    goto :goto_4
.end method

.method a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .registers 4

    .prologue
    .line 811
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->f:Landroid/support/v7/internal/view/menu/j;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->f:Landroid/support/v7/internal/view/menu/j;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/internal/view/menu/j;->a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public a(Landroid/support/v7/internal/view/menu/m;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 1314
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1332
    :cond_9
    :goto_9
    return v0

    .line 1318
    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->d()V

    .line 1319
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_14
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1320
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/x;

    .line 1321
    if-nez v1, :cond_2e

    .line 1322
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_14

    .line 1323
    :cond_2e
    invoke-interface {v1, p1}, Landroid/support/v7/internal/view/menu/x;->a(Landroid/support/v7/internal/view/menu/m;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 1327
    :goto_34
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->e()V

    .line 1329
    if-eqz v0, :cond_9

    .line 1330
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/i;->n:Landroid/support/v7/internal/view/menu/m;

    goto :goto_9

    :cond_3c
    move v2, v0

    goto :goto_14

    :cond_3e
    move v0, v2

    goto :goto_34
.end method

.method public final a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z
    .registers 11

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 952
    check-cast p1, Landroid/support/v7/internal/view/menu/m;

    .line 954
    if-eqz p1, :cond_c

    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/m;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_e

    :cond_c
    move v0, v2

    .line 984
    :cond_d
    :goto_d
    return v0

    .line 958
    :cond_e
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/m;->b()Z

    move-result v5

    .line 7651
    iget-object v4, p1, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    .line 961
    if-eqz v4, :cond_2e

    invoke-virtual {v4}, Landroid/support/v4/view/n;->f()Z

    move-result v0

    if-eqz v0, :cond_2e

    move v1, v3

    .line 962
    :goto_1d
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/m;->i()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 963
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/m;->expandActionView()Z

    move-result v0

    or-int/2addr v0, v5

    .line 964
    if-eqz v0, :cond_d

    invoke-virtual {p0, v3}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    goto :goto_d

    :cond_2e
    move v1, v2

    .line 961
    goto :goto_1d

    .line 965
    :cond_30
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/m;->hasSubMenu()Z

    move-result v0

    if-nez v0, :cond_38

    if-eqz v1, :cond_97

    .line 966
    :cond_38
    invoke-virtual {p0, v2}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 968
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/m;->hasSubMenu()Z

    move-result v0

    if-nez v0, :cond_4b

    .line 969
    new-instance v0, Landroid/support/v7/internal/view/menu/ad;

    .line 7807
    iget-object v6, p0, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    .line 969
    invoke-direct {v0, v6, p0, p1}, Landroid/support/v7/internal/view/menu/ad;-><init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/support/v7/internal/view/menu/m;)V

    invoke-virtual {p1, v0}, Landroid/support/v7/internal/view/menu/m;->a(Landroid/support/v7/internal/view/menu/ad;)V

    .line 972
    :cond_4b
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/m;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ad;

    .line 973
    if-eqz v1, :cond_56

    .line 974
    invoke-virtual {v4, v0}, Landroid/support/v4/view/n;->a(Landroid/view/SubMenu;)V

    .line 8292
    :cond_56
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_66

    .line 976
    :goto_5e
    or-int v0, v5, v2

    .line 977
    if-nez v0, :cond_d

    invoke-virtual {p0, v3}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    goto :goto_d

    .line 8297
    :cond_66
    if-eqz p2, :cond_6c

    .line 8298
    invoke-interface {p2, v0}, Landroid/support/v7/internal/view/menu/x;->a(Landroid/support/v7/internal/view/menu/ad;)Z

    move-result v2

    .line 8301
    :cond_6c
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v2

    :goto_73
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_95

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 8302
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/internal/view/menu/x;

    .line 8303
    if-nez v2, :cond_8d

    .line 8304
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_73

    .line 8305
    :cond_8d
    if-nez v4, :cond_a1

    .line 8306
    invoke-interface {v2, v0}, Landroid/support/v7/internal/view/menu/x;->a(Landroid/support/v7/internal/view/menu/ad;)Z

    move-result v2

    :goto_93
    move v4, v2

    .line 8308
    goto :goto_73

    :cond_95
    move v2, v4

    .line 8309
    goto :goto_5e

    .line 979
    :cond_97
    and-int/lit8 v0, p3, 0x1

    if-nez v0, :cond_9e

    .line 980
    invoke-virtual {p0, v3}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    :cond_9e
    move v0, v5

    goto/16 :goto_d

    :cond_a1
    move v2, v4

    goto :goto_93
.end method

.method public add(I)Landroid/view/MenuItem;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 462
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->u:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v1, v1, v0}, Landroid/support/v7/internal/view/menu/i;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIII)Landroid/view/MenuItem;
    .registers 6

    .prologue
    .line 472
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->u:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/v7/internal/view/menu/i;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 6

    .prologue
    .line 467
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/support/v7/internal/view/menu/i;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 457
    invoke-virtual {p0, v0, v0, v0, p1}, Landroid/support/v7/internal/view/menu/i;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .registers 18

    .prologue
    .line 502
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 503
    const/4 v0, 0x0

    invoke-virtual {v4, p4, p5, p6, v0}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 505
    if-eqz v5, :cond_61

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    move v3, v0

    .line 507
    :goto_12
    and-int/lit8 v0, p7, 0x1

    if-nez v0, :cond_19

    .line 508
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/view/menu/i;->removeGroup(I)V

    .line 511
    :cond_19
    const/4 v0, 0x0

    move v2, v0

    :goto_1b
    if-ge v2, v3, :cond_69

    .line 512
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 513
    new-instance v6, Landroid/content/Intent;

    iget v1, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    if-gez v1, :cond_64

    move-object v1, p6

    :goto_2a
    invoke-direct {v6, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 515
    new-instance v1, Landroid/content/ComponentName;

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v8, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 518
    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, p1, p2, p3, v1}, Landroid/support/v7/internal/view/menu/i;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    move-result-object v1

    .line 521
    if-eqz p8, :cond_5d

    iget v6, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    if-ltz v6, :cond_5d

    .line 522
    iget v0, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    aput-object v1, p8, v0

    .line 511
    :cond_5d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1b

    .line 505
    :cond_61
    const/4 v0, 0x0

    move v3, v0

    goto :goto_12

    .line 513
    :cond_64
    iget v1, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    aget-object v1, p5, v1

    goto :goto_2a

    .line 526
    :cond_69
    return v3
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 482
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->u:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v1, v1, v0}, Landroid/support/v7/internal/view/menu/i;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
    .registers 6

    .prologue
    .line 496
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->u:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/v7/internal/view/menu/i;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 8

    .prologue
    .line 487
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/support/v7/internal/view/menu/i;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 488
    new-instance v1, Landroid/support/v7/internal/view/menu/ad;

    iget-object v2, p0, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    invoke-direct {v1, v2, p0, v0}, Landroid/support/v7/internal/view/menu/ad;-><init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/support/v7/internal/view/menu/m;)V

    .line 489
    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/m;->a(Landroid/support/v7/internal/view/menu/ad;)V

    .line 491
    return-object v1
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 477
    invoke-virtual {p0, v0, v0, v0, p1}, Landroid/support/v7/internal/view/menu/i;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .registers 6

    .prologue
    .line 361
    .line 2336
    const-string v0, "android:menu:presenters"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v2

    .line 2338
    if-eqz v2, :cond_10

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    return-void

    .line 2340
    :cond_11
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_17
    :goto_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 2341
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/x;

    .line 2342
    if-nez v1, :cond_31

    .line 2343
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_17

    .line 2345
    :cond_31
    invoke-interface {v1}, Landroid/support/v7/internal/view/menu/x;->b()I

    move-result v0

    .line 2346
    if-lez v0, :cond_17

    .line 2347
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 2348
    if-eqz v0, :cond_17

    .line 2349
    invoke-interface {v1, v0}, Landroid/support/v7/internal/view/menu/x;->a(Landroid/os/Parcelable;)V

    goto :goto_17
.end method

.method public final b(Landroid/support/v7/internal/view/menu/x;)V
    .registers 5

    .prologue
    .line 267
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 268
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/x;

    .line 269
    if-eqz v1, :cond_1c

    if-ne v1, p1, :cond_6

    .line 270
    :cond_1c
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_6

    .line 273
    :cond_22
    return-void
.end method

.method public final b(Z)V
    .registers 5

    .prologue
    .line 997
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->F:Z

    if-eqz v0, :cond_5

    .line 1009
    :goto_4
    return-void

    .line 999
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->F:Z

    .line 1000
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1001
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/x;

    .line 1002
    if-nez v1, :cond_28

    .line 1003
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_e

    .line 1005
    :cond_28
    invoke-interface {v1, p0, p1}, Landroid/support/v7/internal/view/menu/x;->a(Landroid/support/v7/internal/view/menu/i;Z)V

    goto :goto_e

    .line 1008
    :cond_2c
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->F:Z

    goto :goto_4
.end method

.method b()Z
    .registers 2

    .prologue
    .line 770
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->v:Z

    return v0
.end method

.method public b(Landroid/support/v7/internal/view/menu/m;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 1336
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->n:Landroid/support/v7/internal/view/menu/m;

    if-eq v1, p1, :cond_e

    .line 1354
    :cond_d
    :goto_d
    return v0

    .line 1340
    :cond_e
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->d()V

    .line 1341
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_18
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1342
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/x;

    .line 1343
    if-nez v1, :cond_32

    .line 1344
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_18

    .line 1345
    :cond_32
    invoke-interface {v1, p1}, Landroid/support/v7/internal/view/menu/x;->b(Landroid/support/v7/internal/view/menu/m;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 1349
    :goto_38
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->e()V

    .line 1351
    if-eqz v0, :cond_d

    .line 1352
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v7/internal/view/menu/i;->n:Landroid/support/v7/internal/view/menu/m;

    goto :goto_d

    :cond_41
    move v2, v0

    goto :goto_18

    :cond_43
    move v0, v2

    goto :goto_38
.end method

.method public final c(Landroid/os/Bundle;)V
    .registers 9

    .prologue
    .line 365
    const/4 v1, 0x0

    .line 367
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->size()I

    move-result v3

    .line 368
    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    :goto_8
    if-ge v2, v3, :cond_49

    .line 369
    invoke-virtual {p0, v2}, Landroid/support/v7/internal/view/menu/i;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 370
    invoke-static {v4}, Landroid/support/v4/view/az;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v1

    .line 371
    if-eqz v1, :cond_34

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_34

    .line 372
    if-nez v0, :cond_22

    .line 373
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 375
    :cond_22
    invoke-virtual {v1, v0}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 376
    invoke-static {v4}, Landroid/support/v4/view/az;->c(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 377
    const-string v1, "android:menu:expandedactionview"

    invoke-interface {v4}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    invoke-virtual {p1, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_34
    move-object v1, v0

    .line 380
    invoke-interface {v4}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 381
    invoke-interface {v4}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ad;

    .line 382
    invoke-virtual {v0, p1}, Landroid/support/v7/internal/view/menu/ad;->c(Landroid/os/Bundle;)V

    .line 368
    :cond_44
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v0, v1

    goto :goto_8

    .line 386
    :cond_49
    if-eqz v0, :cond_52

    .line 387
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 389
    :cond_52
    return-void
.end method

.method public final c(Z)V
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 1024
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->D:Z

    if-nez v0, :cond_3e

    .line 1025
    if-eqz p1, :cond_b

    .line 1026
    iput-boolean v1, p0, Landroid/support/v7/internal/view/menu/i;->y:Z

    .line 1027
    iput-boolean v1, p0, Landroid/support/v7/internal/view/menu/i;->A:Z

    .line 9276
    :cond_b
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3d

    .line 9278
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->d()V

    .line 9279
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 9280
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/x;

    .line 9281
    if-nez v1, :cond_36

    .line 9282
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1c

    .line 9284
    :cond_36
    invoke-interface {v1, p1}, Landroid/support/v7/internal/view/menu/x;->a(Z)V

    goto :goto_1c

    .line 9287
    :cond_3a
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->e()V

    .line 1034
    :cond_3d
    :goto_3d
    return-void

    .line 1032
    :cond_3e
    iput-boolean v1, p0, Landroid/support/v7/internal/view/menu/i;->E:Z

    goto :goto_3d
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 799
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->w:Z

    return v0
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 584
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->n:Landroid/support/v7/internal/view/menu/m;

    if-eqz v0, :cond_9

    .line 585
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->n:Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->b(Landroid/support/v7/internal/view/menu/m;)Z

    .line 587
    :cond_9
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 589
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 590
    return-void
.end method

.method public clearHeader()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 1175
    iput-object v0, p0, Landroid/support/v7/internal/view/menu/i;->k:Landroid/graphics/drawable/Drawable;

    .line 1176
    iput-object v0, p0, Landroid/support/v7/internal/view/menu/i;->j:Ljava/lang/CharSequence;

    .line 1177
    iput-object v0, p0, Landroid/support/v7/internal/view/menu/i;->l:Landroid/view/View;

    .line 1179
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 1180
    return-void
.end method

.method public close()V
    .registers 2

    .prologue
    .line 1013
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 1014
    return-void
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 1042
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->D:Z

    if-nez v0, :cond_a

    .line 1043
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->D:Z

    .line 1044
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->E:Z

    .line 1046
    :cond_a
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .registers 9

    .prologue
    .line 392
    if-nez p1, :cond_3

    .line 419
    :cond_2
    :goto_2
    return-void

    .line 396
    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v2

    .line 399
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->size()I

    move-result v3

    .line 400
    const/4 v0, 0x0

    move v1, v0

    :goto_11
    if-ge v1, v3, :cond_3a

    .line 401
    invoke-virtual {p0, v1}, Landroid/support/v7/internal/view/menu/i;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 402
    invoke-static {v0}, Landroid/support/v4/view/az;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v4

    .line 403
    if-eqz v4, :cond_27

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_27

    .line 404
    invoke-virtual {v4, v2}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 406
    :cond_27
    invoke-interface {v0}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v4

    if-eqz v4, :cond_36

    .line 407
    invoke-interface {v0}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ad;

    .line 408
    invoke-virtual {v0, p1}, Landroid/support/v7/internal/view/menu/ad;->d(Landroid/os/Bundle;)V

    .line 400
    :cond_36
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_11

    .line 412
    :cond_3a
    const-string v0, "android:menu:expandedactionview"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 413
    if-lez v0, :cond_2

    .line 414
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 415
    if-eqz v0, :cond_2

    .line 416
    invoke-static {v0}, Landroid/support/v4/view/az;->b(Landroid/view/MenuItem;)Z

    goto :goto_2
.end method

.method public final e()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1049
    iput-boolean v1, p0, Landroid/support/v7/internal/view/menu/i;->D:Z

    .line 1051
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->E:Z

    if-eqz v0, :cond_d

    .line 1052
    iput-boolean v1, p0, Landroid/support/v7/internal/view/menu/i;->E:Z

    .line 1053
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 1055
    :cond_d
    return-void
.end method

.method final f()V
    .registers 2

    .prologue
    const/4 v0, 0x1

    .line 1064
    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->y:Z

    .line 1065
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 1066
    return-void
.end method

.method public findItem(I)Landroid/view/MenuItem;
    .registers 6

    .prologue
    .line 671
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->size()I

    move-result v2

    .line 672
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v2, :cond_2b

    .line 673
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 674
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_17

    .line 685
    :cond_16
    :goto_16
    return-object v0

    .line 676
    :cond_17
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->hasSubMenu()Z

    move-result v3

    if-eqz v3, :cond_27

    .line 677
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 679
    if-nez v0, :cond_16

    .line 672
    :cond_27
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 685
    :cond_2b
    const/4 v0, 0x0

    goto :goto_16
.end method

.method final g()V
    .registers 2

    .prologue
    const/4 v0, 0x1

    .line 1075
    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->A:Z

    .line 1076
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 1077
    return-void
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .registers 3

    .prologue
    .line 730
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    return-object v0
.end method

.method public final h()Ljava/util/ArrayList;
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 1080
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->y:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->x:Ljava/util/ArrayList;

    .line 1095
    :goto_7
    return-object v0

    .line 1083
    :cond_8
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1085
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 1087
    :goto_14
    if-ge v1, v3, :cond_2d

    .line 1088
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 1089
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_29

    iget-object v4, p0, Landroid/support/v7/internal/view/menu/i;->x:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1087
    :cond_29
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_14

    .line 1092
    :cond_2d
    iput-boolean v2, p0, Landroid/support/v7/internal/view/menu/i;->y:Z

    .line 1093
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->A:Z

    .line 1095
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->x:Ljava/util/ArrayList;

    goto :goto_7
.end method

.method public hasVisibleItems()Z
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 653
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->o:Z

    if-eqz v0, :cond_8

    move v0, v1

    .line 666
    :goto_7
    return v0

    .line 657
    :cond_8
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->size()I

    move-result v4

    move v3, v2

    .line 659
    :goto_d
    if-ge v3, v4, :cond_23

    .line 660
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 661
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1f

    move v0, v1

    .line 662
    goto :goto_7

    .line 659
    :cond_1f
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_d

    :cond_23
    move v0, v2

    .line 666
    goto :goto_7
.end method

.method public final i()V
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 1125
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->h()Ljava/util/ArrayList;

    move-result-object v4

    .line 1127
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/i;->A:Z

    if-nez v0, :cond_a

    .line 1162
    :goto_9
    return-void

    .line 1133
    :cond_a
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_11
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1134
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/x;

    .line 1135
    if-nez v1, :cond_2b

    .line 1136
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/i;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_11

    .line 1138
    :cond_2b
    invoke-interface {v1}, Landroid/support/v7/internal/view/menu/x;->a()Z

    move-result v0

    or-int/2addr v0, v2

    move v2, v0

    .line 1140
    goto :goto_11

    .line 1142
    :cond_32
    if-eqz v2, :cond_60

    .line 1143
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1144
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1145
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v3

    .line 1146
    :goto_43
    if-ge v1, v2, :cond_73

    .line 1147
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 1148
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->f()Z

    move-result v5

    if-eqz v5, :cond_5a

    .line 1149
    iget-object v5, p0, Landroid/support/v7/internal/view/menu/i;->h:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1146
    :goto_56
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_43

    .line 1151
    :cond_5a
    iget-object v5, p0, Landroid/support/v7/internal/view/menu/i;->z:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_56

    .line 1157
    :cond_60
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1158
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1159
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->z:Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->h()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1161
    :cond_73
    iput-boolean v3, p0, Landroid/support/v7/internal/view/menu/i;->A:Z

    goto :goto_9
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .registers 4

    .prologue
    .line 735
    invoke-direct {p0, p1, p2}, Landroid/support/v7/internal/view/menu/i;->a(ILandroid/view/KeyEvent;)Landroid/support/v7/internal/view/menu/m;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final j()Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 1170
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->i()V

    .line 1171
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->z:Ljava/util/ArrayList;

    return-object v0
.end method

.method public k()Landroid/support/v7/internal/view/menu/i;
    .registers 1

    .prologue
    .line 1291
    return-object p0
.end method

.method public performIdentifierAction(II)Z
    .registers 5

    .prologue
    .line 944
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/view/menu/i;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 6948
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p2}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z

    move-result v0

    .line 944
    return v0
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .registers 6

    .prologue
    .line 836
    invoke-direct {p0, p1, p2}, Landroid/support/v7/internal/view/menu/i;->a(ILandroid/view/KeyEvent;)Landroid/support/v7/internal/view/menu/m;

    move-result-object v1

    .line 838
    const/4 v0, 0x0

    .line 840
    if-eqz v1, :cond_c

    .line 5948
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0, p3}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z

    move-result v0

    .line 844
    :cond_c
    and-int/lit8 v1, p3, 0x2

    if-eqz v1, :cond_14

    .line 845
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 848
    :cond_14
    return v0
.end method

.method public removeGroup(I)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 536
    .line 3706
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->size()I

    move-result v3

    move v2, v1

    .line 3712
    :goto_6
    if-ge v2, v3, :cond_3c

    .line 3713
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 3715
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v0

    if-ne v0, p1, :cond_39

    move v3, v2

    .line 538
    :goto_17
    if-ltz v3, :cond_43

    .line 539
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v4, v0, v3

    move v0, v1

    .line 541
    :goto_22
    add-int/lit8 v2, v0, 0x1

    if-ge v0, v4, :cond_3f

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v0

    if-ne v0, p1, :cond_3f

    .line 543
    invoke-direct {p0, v3, v1}, Landroid/support/v7/internal/view/menu/i;->a(IZ)V

    move v0, v2

    goto :goto_22

    .line 3712
    :cond_39
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 3720
    :cond_3c
    const/4 v0, -0x1

    move v3, v0

    goto :goto_17

    .line 547
    :cond_3f
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 549
    :cond_43
    return-void
.end method

.method public removeItem(I)V
    .registers 5

    .prologue
    .line 531
    .line 3689
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/i;->size()I

    move-result v2

    .line 3691
    const/4 v1, 0x0

    :goto_5
    if-ge v1, v2, :cond_1e

    .line 3692
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 3693
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getItemId()I

    move-result v0

    if-ne v0, p1, :cond_1b

    move v0, v1

    .line 531
    :goto_16
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Landroid/support/v7/internal/view/menu/i;->a(IZ)V

    .line 532
    return-void

    .line 3691
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 3698
    :cond_1e
    const/4 v0, -0x1

    goto :goto_16
.end method

.method public setGroupCheckable(IZZ)V
    .registers 8

    .prologue
    .line 610
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 612
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-ge v1, v2, :cond_22

    .line 613
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 614
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_1e

    .line 615
    invoke-virtual {v0, p3}, Landroid/support/v7/internal/view/menu/m;->a(Z)V

    .line 616
    invoke-virtual {v0, p2}, Landroid/support/v7/internal/view/menu/m;->setCheckable(Z)Landroid/view/MenuItem;

    .line 612
    :cond_1e
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 619
    :cond_22
    return-void
.end method

.method public setGroupEnabled(IZ)V
    .registers 7

    .prologue
    .line 641
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 643
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-ge v1, v2, :cond_1f

    .line 644
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 645
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_1b

    .line 646
    invoke-virtual {v0, p2}, Landroid/support/v7/internal/view/menu/m;->setEnabled(Z)Landroid/view/MenuItem;

    .line 643
    :cond_1b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 649
    :cond_1f
    return-void
.end method

.method public setGroupVisible(IZ)V
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 623
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    .line 629
    :goto_a
    if-ge v3, v4, :cond_26

    .line 630
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 631
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v5

    if-ne v5, p1, :cond_2c

    .line 632
    invoke-virtual {v0, p2}, Landroid/support/v7/internal/view/menu/m;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_2c

    move v0, v1

    .line 629
    :goto_21
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_a

    .line 636
    :cond_26
    if-eqz v2, :cond_2b

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 637
    :cond_2b
    return-void

    :cond_2c
    move v0, v2

    goto :goto_21
.end method

.method public setQwertyMode(Z)V
    .registers 3

    .prologue
    .line 740
    iput-boolean p1, p0, Landroid/support/v7/internal/view/menu/i;->v:Z

    .line 742
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 743
    return-void
.end method

.method public size()I
    .registers 2

    .prologue
    .line 725
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
