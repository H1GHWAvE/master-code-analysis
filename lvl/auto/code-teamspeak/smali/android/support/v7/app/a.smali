.class public abstract Landroid/support/v7/app/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x1

.field public static final e:I = 0x2

.field public static final f:I = 0x4

.field public static final g:I = 0x8

.field public static final h:I = 0x10


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1311
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public a(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;
    .registers 3

    .prologue
    .line 1037
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(F)V
    .registers 4

    .prologue
    .line 1001
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_d

    .line 1002
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Setting a non-zero elevation is not supported in this action bar configuration."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1005
    :cond_d
    return-void
.end method

.method public abstract a(I)V
.end method

.method public abstract a(II)V
.end method

.method public a(Landroid/content/res/Configuration;)V
    .registers 2

    .prologue
    .line 1029
    return-void
.end method

.method public abstract a(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract a(Landroid/support/v7/app/e;)V
.end method

.method public abstract a(Landroid/support/v7/app/g;)V
.end method

.method public abstract a(Landroid/support/v7/app/g;I)V
.end method

.method public abstract a(Landroid/support/v7/app/g;IZ)V
.end method

.method public abstract a(Landroid/support/v7/app/g;Z)V
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public abstract a(Landroid/view/View;Landroid/support/v7/app/c;)V
.end method

.method public abstract a(Landroid/widget/SpinnerAdapter;Landroid/support/v7/app/f;)V
.end method

.method public abstract a(Ljava/lang/CharSequence;)V
.end method

.method public abstract a(Z)V
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .registers 4

    .prologue
    .line 1057
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/KeyEvent;)Z
    .registers 3

    .prologue
    .line 1052
    const/4 v0, 0x0

    return v0
.end method

.method public abstract b()I
.end method

.method public abstract b(I)V
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param
.end method

.method public abstract b(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract b(Landroid/support/v7/app/e;)V
.end method

.method public abstract b(Landroid/support/v7/app/g;)V
.end method

.method public abstract b(Ljava/lang/CharSequence;)V
.end method

.method public abstract b(Z)V
.end method

.method public c()V
    .registers 1

    .prologue
    .line 529
    return-void
.end method

.method public abstract c(I)V
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param
.end method

.method public abstract c(Landroid/graphics/drawable/Drawable;)V
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
.end method

.method public abstract c(Landroid/support/v7/app/g;)V
.end method

.method public c(Ljava/lang/CharSequence;)V
    .registers 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 910
    return-void
.end method

.method public abstract c(Z)V
.end method

.method public abstract d()Landroid/view/View;
.end method

.method public abstract d(I)V
.end method

.method public d(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 519
    return-void
.end method

.method public d(Ljava/lang/CharSequence;)V
    .registers 2

    .prologue
    .line 1067
    return-void
.end method

.method public abstract d(Z)V
.end method

.method public abstract e()Ljava/lang/CharSequence;
    .annotation build Landroid/support/a/z;
    .end annotation
.end method

.method public abstract e(I)V
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param
.end method

.method public e(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 870
    return-void
.end method

.method public abstract e(Z)V
.end method

.method public abstract f()Ljava/lang/CharSequence;
    .annotation build Landroid/support/a/z;
    .end annotation
.end method

.method public abstract f(I)V
.end method

.method public f(Z)V
    .registers 2

    .prologue
    .line 1021
    return-void
.end method

.method public abstract g()I
.end method

.method public abstract g(I)V
.end method

.method public g(Z)V
    .registers 2

    .prologue
    .line 1025
    return-void
.end method

.method public abstract h()I
.end method

.method public abstract h(I)V
.end method

.method public h(Z)V
    .registers 2

    .prologue
    .line 1033
    return-void
.end method

.method public abstract i()Landroid/support/v7/app/g;
.end method

.method public abstract i(I)V
.end method

.method public abstract j(I)Landroid/support/v7/app/g;
.end method

.method public abstract j()V
.end method

.method public abstract k()Landroid/support/v7/app/g;
    .annotation build Landroid/support/a/z;
    .end annotation
.end method

.method public k(I)V
    .registers 2
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 891
    return-void
.end method

.method public abstract l()I
.end method

.method public l(I)V
    .registers 2
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param

    .prologue
    .line 930
    return-void
.end method

.method public abstract m()I
.end method

.method public m(I)V
    .registers 4

    .prologue
    .line 986
    if-eqz p1, :cond_a

    .line 987
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Setting an explicit action bar hide offset is not supported in this action bar configuration."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 990
    :cond_a
    return-void
.end method

.method public abstract n()V
.end method

.method public abstract o()V
.end method

.method public abstract p()Z
.end method

.method public q()V
    .registers 1

    .prologue
    .line 828
    return-void
.end method

.method public r()Landroid/content/Context;
    .registers 2

    .prologue
    .line 840
    const/4 v0, 0x0

    return-object v0
.end method

.method public s()Z
    .registers 2

    .prologue
    .line 850
    const/4 v0, 0x0

    return v0
.end method

.method public t()V
    .registers 3

    .prologue
    .line 947
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Hide on content scroll is not supported in this action bar configuration."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public u()Z
    .registers 2

    .prologue
    .line 960
    const/4 v0, 0x0

    return v0
.end method

.method public v()I
    .registers 2

    .prologue
    .line 973
    const/4 v0, 0x0

    return v0
.end method

.method public w()F
    .registers 2

    .prologue
    .line 1016
    const/4 v0, 0x0

    return v0
.end method

.method public x()Z
    .registers 2

    .prologue
    .line 1042
    const/4 v0, 0x0

    return v0
.end method

.method public y()Z
    .registers 2

    .prologue
    .line 1047
    const/4 v0, 0x0

    return v0
.end method

.method public z()Z
    .registers 2

    .prologue
    .line 1062
    const/4 v0, 0x0

    return v0
.end method
