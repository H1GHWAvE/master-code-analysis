.class public final Landroid/support/v7/widget/cb;
.super Landroid/widget/CompoundButton;
.source "SourceFile"


# static fields
.field private static final N:[I

.field private static final a:I = 0xfa

.field private static final b:I = 0x0

.field private static final c:I = 0x1

.field private static final d:I = 0x2

.field private static final e:Ljava/lang/String; = "android.widget.Switch"

.field private static final f:I = 0x1

.field private static final g:I = 0x2

.field private static final h:I = 0x3


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:Landroid/text/TextPaint;

.field private G:Landroid/content/res/ColorStateList;

.field private H:Landroid/text/Layout;

.field private I:Landroid/text/Layout;

.field private J:Landroid/text/method/TransformationMethod;

.field private K:Landroid/support/v7/widget/cd;

.field private final L:Landroid/graphics/Rect;

.field private final M:Landroid/support/v7/internal/widget/av;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:I

.field private l:I

.field private m:I

.field private n:Z

.field private o:Ljava/lang/CharSequence;

.field private p:Ljava/lang/CharSequence;

.field private q:Z

.field private r:I

.field private s:I

.field private t:F

.field private u:F

.field private v:Landroid/view/VelocityTracker;

.field private w:I

.field private x:F

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 150
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Landroid/support/v7/widget/cb;->N:[I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 160
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/cb;-><init>(Landroid/content/Context;B)V

    .line 161
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4

    .prologue
    .line 171
    sget v0, Landroid/support/v7/a/d;->switchStyle:I

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/cb;-><init>(Landroid/content/Context;I)V

    .line 172
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .registers 13

    .prologue
    const/4 v9, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 185
    invoke-direct {p0, p1, v4, p2}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 103
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cb;->v:Landroid/view/VelocityTracker;

    .line 145
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/cb;->L:Landroid/graphics/Rect;

    .line 187
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v2}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    .line 189
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 190
    iget-object v5, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, v5, Landroid/text/TextPaint;->density:F

    .line 192
    sget-object v0, Landroid/support/v7/a/n;->SwitchCompat:[I

    invoke-static {p1, v4, v0, p2}, Landroid/support/v7/internal/widget/ax;->a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;

    move-result-object v5

    .line 194
    sget v0, Landroid/support/v7/a/n;->SwitchCompat_android_thumb:I

    invoke-virtual {v5, v0}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    .line 195
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_41

    .line 196
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 198
    :cond_41
    sget v0, Landroid/support/v7/a/n;->SwitchCompat_track:I

    invoke-virtual {v5, v0}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    .line 199
    iget-object v0, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_52

    .line 200
    iget-object v0, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 202
    :cond_52
    sget v0, Landroid/support/v7/a/n;->SwitchCompat_android_textOn:I

    invoke-virtual {v5, v0}, Landroid/support/v7/internal/widget/ax;->c(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cb;->o:Ljava/lang/CharSequence;

    .line 203
    sget v0, Landroid/support/v7/a/n;->SwitchCompat_android_textOff:I

    invoke-virtual {v5, v0}, Landroid/support/v7/internal/widget/ax;->c(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cb;->p:Ljava/lang/CharSequence;

    .line 204
    sget v0, Landroid/support/v7/a/n;->SwitchCompat_showText:I

    invoke-virtual {v5, v0, v2}, Landroid/support/v7/internal/widget/ax;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/widget/cb;->q:Z

    .line 205
    sget v0, Landroid/support/v7/a/n;->SwitchCompat_thumbTextPadding:I

    invoke-virtual {v5, v0, v1}, Landroid/support/v7/internal/widget/ax;->c(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/cb;->k:I

    .line 207
    sget v0, Landroid/support/v7/a/n;->SwitchCompat_switchMinWidth:I

    invoke-virtual {v5, v0, v1}, Landroid/support/v7/internal/widget/ax;->c(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/cb;->l:I

    .line 209
    sget v0, Landroid/support/v7/a/n;->SwitchCompat_switchPadding:I

    invoke-virtual {v5, v0, v1}, Landroid/support/v7/internal/widget/ax;->c(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/cb;->m:I

    .line 211
    sget v0, Landroid/support/v7/a/n;->SwitchCompat_splitTrack:I

    invoke-virtual {v5, v0, v1}, Landroid/support/v7/internal/widget/ax;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/widget/cb;->n:Z

    .line 213
    sget v0, Landroid/support/v7/a/n;->SwitchCompat_switchTextAppearance:I

    invoke-virtual {v5, v0, v1}, Landroid/support/v7/internal/widget/ax;->e(II)I

    move-result v0

    .line 215
    if-eqz v0, :cond_10d

    .line 1237
    sget-object v6, Landroid/support/v7/a/n;->TextAppearance:[I

    invoke-virtual {p1, v0, v6}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 1242
    sget v0, Landroid/support/v7/a/n;->TextAppearance_android_textColor:I

    invoke-virtual {v6, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 1243
    if-eqz v0, :cond_133

    .line 1244
    iput-object v0, p0, Landroid/support/v7/widget/cb;->G:Landroid/content/res/ColorStateList;

    .line 1250
    :goto_a2
    sget v0, Landroid/support/v7/a/n;->TextAppearance_android_textSize:I

    invoke-virtual {v6, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 1251
    if-eqz v0, :cond_be

    .line 1252
    int-to-float v7, v0

    iget-object v8, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v8}, Landroid/text/TextPaint;->getTextSize()F

    move-result v8

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_be

    .line 1253
    iget-object v7, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    int-to-float v0, v0

    invoke-virtual {v7, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1254
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->requestLayout()V

    .line 1259
    :cond_be
    sget v0, Landroid/support/v7/a/n;->TextAppearance_android_typeface:I

    invoke-virtual {v6, v0, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 1260
    sget v7, Landroid/support/v7/a/n;->TextAppearance_android_textStyle:I

    invoke-virtual {v6, v7, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    .line 1276
    packed-switch v0, :pswitch_data_160

    move-object v0, v4

    .line 1300
    :goto_ce
    if-lez v7, :cond_14f

    .line 1301
    if-nez v0, :cond_144

    .line 1302
    invoke-static {v7}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 1307
    :goto_d6
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    .line 1309
    if-eqz v0, :cond_149

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    .line 1310
    :goto_df
    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v7, v0

    .line 1311
    iget-object v8, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    and-int/lit8 v0, v7, 0x1

    if-eqz v0, :cond_14b

    move v0, v2

    :goto_e9
    invoke-virtual {v8, v0}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 1312
    iget-object v2, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    and-int/lit8 v0, v7, 0x2

    if-eqz v0, :cond_14d

    const/high16 v0, -0x41800000    # -0.25f

    :goto_f4
    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 1264
    :goto_f7
    sget v0, Landroid/support/v7/a/n;->TextAppearance_textAllCaps:I

    invoke-virtual {v6, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 1265
    if-eqz v0, :cond_15d

    .line 1266
    new-instance v0, Landroid/support/v7/internal/b/a;

    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/internal/b/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/cb;->J:Landroid/text/method/TransformationMethod;

    .line 1271
    :goto_10a
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 219
    :cond_10d
    invoke-virtual {v5}, Landroid/support/v7/internal/widget/ax;->a()Landroid/support/v7/internal/widget/av;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cb;->M:Landroid/support/v7/internal/widget/av;

    .line 2183
    iget-object v0, v5, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 223
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 224
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/cb;->s:I

    .line 225
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/cb;->w:I

    .line 228
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->refreshDrawableState()V

    .line 229
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->setChecked(Z)V

    .line 230
    return-void

    .line 1247
    :cond_133
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cb;->G:Landroid/content/res/ColorStateList;

    goto/16 :goto_a2

    .line 1278
    :pswitch_13b
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    goto :goto_ce

    .line 1282
    :pswitch_13e
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    goto :goto_ce

    .line 1286
    :pswitch_141
    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    goto :goto_ce

    .line 1304
    :cond_144
    invoke-static {v0, v7}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_d6

    :cond_149
    move v0, v1

    .line 1309
    goto :goto_df

    :cond_14b
    move v0, v1

    .line 1311
    goto :goto_e9

    :cond_14d
    move v0, v3

    .line 1312
    goto :goto_f4

    .line 1314
    :cond_14f
    iget-object v2, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 1315
    iget-object v2, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 1316
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    goto :goto_f7

    .line 1268
    :cond_15d
    iput-object v4, p0, Landroid/support/v7/widget/cb;->J:Landroid/text/method/TransformationMethod;

    goto :goto_10a

    .line 1276
    :pswitch_data_160
    .packed-switch 0x1
        :pswitch_13b
        :pswitch_13e
        :pswitch_141
    .end packed-switch
.end method

.method private static a(F)F
    .registers 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 1142
    cmpg-float v2, p0, v0

    if-gez v2, :cond_9

    move p0, v0

    :cond_8
    :goto_8
    return p0

    :cond_9
    cmpl-float v0, p0, v1

    if-lez v0, :cond_8

    move p0, v1

    goto :goto_8
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/text/Layout;
    .registers 10

    .prologue
    .line 602
    iget-object v0, p0, Landroid/support/v7/widget/cb;->J:Landroid/text/method/TransformationMethod;

    if-eqz v0, :cond_26

    iget-object v0, p0, Landroid/support/v7/widget/cb;->J:Landroid/text/method/TransformationMethod;

    invoke-interface {v0, p1, p0}, Landroid/text/method/TransformationMethod;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 606
    :goto_a
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    if-eqz v1, :cond_28

    iget-object v3, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-static {v1, v3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    :goto_1c
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-object v0

    :cond_26
    move-object v1, p1

    .line 602
    goto :goto_a

    .line 606
    :cond_28
    const/4 v3, 0x0

    goto :goto_1c
.end method

.method private a()V
    .registers 2

    .prologue
    .line 757
    iget-object v0, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    if-eqz v0, :cond_a

    .line 758
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->clearAnimation()V

    .line 759
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    .line 761
    :cond_a
    return-void
.end method

.method private a(II)V
    .registers 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 275
    const/4 v0, 0x0

    .line 276
    packed-switch p1, :pswitch_data_50

    .line 3300
    :goto_6
    if-lez p2, :cond_42

    .line 3301
    if-nez v0, :cond_39

    .line 3302
    invoke-static {p2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 3307
    :goto_e
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    .line 3309
    if-eqz v0, :cond_3e

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    .line 3310
    :goto_17
    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, p2

    .line 3311
    iget-object v3, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    and-int/lit8 v4, v0, 0x1

    if-eqz v4, :cond_21

    const/4 v1, 0x1

    :cond_21
    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 3312
    iget-object v1, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_40

    const/high16 v0, -0x41800000    # -0.25f

    :goto_2c
    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 3313
    :goto_2f
    return-void

    .line 278
    :pswitch_30
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    goto :goto_6

    .line 282
    :pswitch_33
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    goto :goto_6

    .line 286
    :pswitch_36
    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    goto :goto_6

    .line 3304
    :cond_39
    invoke-static {v0, p2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_e

    :cond_3e
    move v0, v1

    .line 3309
    goto :goto_17

    :cond_40
    move v0, v2

    .line 3312
    goto :goto_2c

    .line 3314
    :cond_42
    iget-object v3, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 3315
    iget-object v1, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 3316
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    goto :goto_2f

    .line 276
    :pswitch_data_50
    .packed-switch 0x1
        :pswitch_30
        :pswitch_33
        :pswitch_36
    .end packed-switch
.end method

.method private a(Landroid/content/Context;I)V
    .registers 11

    .prologue
    const/4 v3, 0x0

    const/4 v7, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 237
    sget-object v0, Landroid/support/v7/a/n;->TextAppearance:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 242
    sget v0, Landroid/support/v7/a/n;->TextAppearance_android_textColor:I

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 243
    if-eqz v0, :cond_80

    .line 244
    iput-object v0, p0, Landroid/support/v7/widget/cb;->G:Landroid/content/res/ColorStateList;

    .line 250
    :goto_14
    sget v0, Landroid/support/v7/a/n;->TextAppearance_android_textSize:I

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 251
    if-eqz v0, :cond_30

    .line 252
    int-to-float v5, v0

    iget-object v6, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->getTextSize()F

    move-result v6

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_30

    .line 253
    iget-object v5, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 254
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->requestLayout()V

    .line 259
    :cond_30
    sget v0, Landroid/support/v7/a/n;->TextAppearance_android_typeface:I

    invoke-virtual {v4, v0, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 260
    sget v5, Landroid/support/v7/a/n;->TextAppearance_android_textStyle:I

    invoke-virtual {v4, v5, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    .line 2276
    packed-switch v0, :pswitch_data_ac

    move-object v0, v3

    .line 2300
    :goto_40
    if-lez v5, :cond_9b

    .line 2301
    if-nez v0, :cond_90

    .line 2302
    invoke-static {v5}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 2307
    :goto_48
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    .line 2309
    if-eqz v0, :cond_95

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    .line 2310
    :goto_51
    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v5, v0

    .line 2311
    iget-object v6, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    and-int/lit8 v0, v5, 0x1

    if-eqz v0, :cond_97

    const/4 v0, 0x1

    :goto_5b
    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 2312
    iget-object v6, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    and-int/lit8 v0, v5, 0x2

    if-eqz v0, :cond_99

    const/high16 v0, -0x41800000    # -0.25f

    :goto_66
    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 264
    :goto_69
    sget v0, Landroid/support/v7/a/n;->TextAppearance_textAllCaps:I

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 265
    if-eqz v0, :cond_a9

    .line 266
    new-instance v0, Landroid/support/v7/internal/b/a;

    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/internal/b/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/cb;->J:Landroid/text/method/TransformationMethod;

    .line 271
    :goto_7c
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 272
    return-void

    .line 247
    :cond_80
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cb;->G:Landroid/content/res/ColorStateList;

    goto :goto_14

    .line 2278
    :pswitch_87
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    goto :goto_40

    .line 2282
    :pswitch_8a
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    goto :goto_40

    .line 2286
    :pswitch_8d
    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    goto :goto_40

    .line 2304
    :cond_90
    invoke-static {v0, v5}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_48

    :cond_95
    move v0, v1

    .line 2309
    goto :goto_51

    :cond_97
    move v0, v1

    .line 2311
    goto :goto_5b

    :cond_99
    move v0, v2

    .line 2312
    goto :goto_66

    .line 2314
    :cond_9b
    iget-object v5, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v5, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 2315
    iget-object v5, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v5, v2}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 2316
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    goto :goto_69

    .line 268
    :cond_a9
    iput-object v3, p0, Landroid/support/v7/widget/cb;->J:Landroid/text/method/TransformationMethod;

    goto :goto_7c

    .line 2276
    :pswitch_data_ac
    .packed-switch 0x1
        :pswitch_87
        :pswitch_8a
        :pswitch_8d
    .end packed-switch
.end method

.method private a(Landroid/graphics/Typeface;I)V
    .registers 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 300
    if-lez p2, :cond_35

    .line 301
    if-nez p1, :cond_2c

    .line 302
    invoke-static {p2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 307
    :goto_a
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    .line 309
    if-eqz v0, :cond_31

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    .line 310
    :goto_13
    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, p2

    .line 311
    iget-object v3, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    and-int/lit8 v4, v0, 0x1

    if-eqz v4, :cond_1d

    const/4 v1, 0x1

    :cond_1d
    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 312
    iget-object v1, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_33

    const/high16 v0, -0x41800000    # -0.25f

    :goto_28
    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 318
    :goto_2b
    return-void

    .line 304
    :cond_2c
    invoke-static {p1, p2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_a

    :cond_31
    move v0, v1

    .line 309
    goto :goto_13

    :cond_33
    move v0, v2

    .line 312
    goto :goto_28

    .line 314
    :cond_35
    iget-object v0, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 315
    iget-object v0, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 316
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/cb;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    goto :goto_2b
.end method

.method static synthetic a(Landroid/support/v7/widget/cb;F)V
    .registers 2

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/support/v7/widget/cb;->setThumbPosition(F)V

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .registers 4

    .prologue
    .line 712
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 713
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 714
    invoke-super {p0, v0}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 715
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 716
    return-void
.end method

.method private a(Z)V
    .registers 6

    .prologue
    .line 751
    new-instance v1, Landroid/support/v7/widget/cd;

    iget v2, p0, Landroid/support/v7/widget/cb;->x:F

    if-eqz p1, :cond_1b

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_8
    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Landroid/support/v7/widget/cd;-><init>(Landroid/support/v7/widget/cb;FFB)V

    iput-object v1, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    .line 752
    iget-object v0, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/widget/cd;->setDuration(J)V

    .line 753
    iget-object v0, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->startAnimation(Landroid/view/animation/Animation;)V

    .line 754
    return-void

    .line 751
    :cond_1b
    const/4 v0, 0x0

    goto :goto_8
.end method

.method private a(FF)Z
    .registers 9

    .prologue
    const/4 v0, 0x0

    .line 616
    iget-object v1, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_6

    .line 629
    :cond_5
    :goto_5
    return v0

    .line 621
    :cond_6
    invoke-direct {p0}, Landroid/support/v7/widget/cb;->getThumbOffset()I

    move-result v1

    .line 623
    iget-object v2, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Landroid/support/v7/widget/cb;->L:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 624
    iget v2, p0, Landroid/support/v7/widget/cb;->C:I

    iget v3, p0, Landroid/support/v7/widget/cb;->s:I

    sub-int/2addr v2, v3

    .line 625
    iget v3, p0, Landroid/support/v7/widget/cb;->B:I

    add-int/2addr v1, v3

    iget v3, p0, Landroid/support/v7/widget/cb;->s:I

    sub-int/2addr v1, v3

    .line 626
    iget v3, p0, Landroid/support/v7/widget/cb;->A:I

    add-int/2addr v3, v1

    iget-object v4, p0, Landroid/support/v7/widget/cb;->L:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    iget-object v4, p0, Landroid/support/v7/widget/cb;->L:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    iget v4, p0, Landroid/support/v7/widget/cb;->s:I

    add-int/2addr v3, v4

    .line 628
    iget v4, p0, Landroid/support/v7/widget/cb;->E:I

    iget v5, p0, Landroid/support/v7/widget/cb;->s:I

    add-int/2addr v4, v5

    .line 629
    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-lez v1, :cond_5

    int-to-float v1, v3

    cmpg-float v1, p1, v1

    if-gez v1, :cond_5

    int-to-float v1, v2

    cmpl-float v1, p2, v1

    if-lez v1, :cond_5

    int-to-float v1, v4

    cmpg-float v1, p2, v1

    if-gez v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method private b(Landroid/view/MotionEvent;)V
    .registers 9

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 724
    iput v2, p0, Landroid/support/v7/widget/cb;->r:I

    .line 728
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_51

    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_51

    move v0, v1

    .line 729
    :goto_12
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->isChecked()Z

    move-result v3

    .line 731
    if-eqz v0, :cond_60

    .line 732
    iget-object v0, p0, Landroid/support/v7/widget/cb;->v:Landroid/view/VelocityTracker;

    const/16 v4, 0x3e8

    invoke-virtual {v0, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 733
    iget-object v0, p0, Landroid/support/v7/widget/cb;->v:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    .line 734
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Landroid/support/v7/widget/cb;->w:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_5b

    .line 735
    invoke-static {p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_55

    cmpg-float v0, v0, v6

    if-gez v0, :cond_53

    .line 743
    :cond_3a
    :goto_3a
    if-eq v1, v3, :cond_42

    .line 744
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/cb;->playSoundEffect(I)V

    .line 745
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/cb;->setChecked(Z)V

    .line 8712
    :cond_42
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 8713
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 8714
    invoke-super {p0, v0}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 8715
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 748
    return-void

    :cond_51
    move v0, v2

    .line 728
    goto :goto_12

    :cond_53
    move v1, v2

    .line 735
    goto :goto_3a

    :cond_55
    cmpl-float v0, v0, v6

    if-gtz v0, :cond_3a

    move v1, v2

    goto :goto_3a

    .line 737
    :cond_5b
    invoke-direct {p0}, Landroid/support/v7/widget/cb;->getTargetCheckedState()Z

    move-result v1

    goto :goto_3a

    :cond_60
    move v1, v3

    .line 740
    goto :goto_3a
.end method

.method private getTargetCheckedState()Z
    .registers 3

    .prologue
    .line 764
    iget v0, p0, Landroid/support/v7/widget/cb;->x:F

    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private getThumbOffset()I
    .registers 3

    .prologue
    .line 1017
    invoke-static {p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1018
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Landroid/support/v7/widget/cb;->x:F

    sub-float/2addr v0, v1

    .line 1022
    :goto_b
    invoke-direct {p0}, Landroid/support/v7/widget/cb;->getThumbScrollRange()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0

    .line 1020
    :cond_16
    iget v0, p0, Landroid/support/v7/widget/cb;->x:F

    goto :goto_b
.end method

.method private getThumbScrollRange()I
    .registers 5

    .prologue
    .line 1026
    iget-object v0, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2c

    .line 1027
    iget-object v1, p0, Landroid/support/v7/widget/cb;->L:Landroid/graphics/Rect;

    .line 1028
    iget-object v0, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1031
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_29

    .line 1032
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Landroid/support/v7/internal/widget/ae;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1037
    :goto_15
    iget v2, p0, Landroid/support/v7/widget/cb;->y:I

    iget v3, p0, Landroid/support/v7/widget/cb;->A:I

    sub-int/2addr v2, v3

    iget v3, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int v1, v2, v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sub-int v0, v1, v0

    .line 1040
    :goto_28
    return v0

    .line 1034
    :cond_29
    sget-object v0, Landroid/support/v7/internal/widget/ae;->a:Landroid/graphics/Rect;

    goto :goto_15

    .line 1040
    :cond_2c
    const/4 v0, 0x0

    goto :goto_28
.end method

.method private setThumbPosition(F)V
    .registers 2

    .prologue
    .line 773
    iput p1, p0, Landroid/support/v7/widget/cb;->x:F

    .line 774
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->invalidate()V

    .line 775
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .registers 12

    .prologue
    .line 857
    iget-object v7, p0, Landroid/support/v7/widget/cb;->L:Landroid/graphics/Rect;

    .line 858
    iget v1, p0, Landroid/support/v7/widget/cb;->B:I

    .line 859
    iget v3, p0, Landroid/support/v7/widget/cb;->C:I

    .line 860
    iget v4, p0, Landroid/support/v7/widget/cb;->D:I

    .line 861
    iget v5, p0, Landroid/support/v7/widget/cb;->E:I

    .line 863
    invoke-direct {p0}, Landroid/support/v7/widget/cb;->getThumbOffset()I

    move-result v0

    add-int v2, v1, v0

    .line 866
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_8a

    .line 867
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Landroid/support/v7/internal/widget/ae;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;

    move-result-object v0

    .line 873
    :goto_1a
    iget-object v6, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_94

    .line 874
    iget-object v6, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 877
    iget v6, v7, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v2

    .line 884
    if-eqz v0, :cond_91

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_91

    .line 885
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v8, v7, Landroid/graphics/Rect;->left:I

    if-le v2, v8, :cond_3a

    .line 886
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v8, v7, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v8

    add-int/2addr v1, v2

    .line 888
    :cond_3a
    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v8, v7, Landroid/graphics/Rect;->top:I

    if-le v2, v8, :cond_8f

    .line 889
    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v8, v7, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v8

    add-int/2addr v2, v3

    .line 891
    :goto_46
    iget v8, v0, Landroid/graphics/Rect;->right:I

    iget v9, v7, Landroid/graphics/Rect;->right:I

    if-le v8, v9, :cond_52

    .line 892
    iget v8, v0, Landroid/graphics/Rect;->right:I

    iget v9, v7, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v9

    sub-int/2addr v4, v8

    .line 894
    :cond_52
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    iget v9, v7, Landroid/graphics/Rect;->bottom:I

    if-le v8, v9, :cond_8d

    .line 895
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v8, v7, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v8

    sub-int v0, v5, v0

    .line 898
    :goto_5f
    iget-object v8, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, v1, v2, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move v0, v6

    .line 902
    :goto_65
    iget-object v1, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_86

    .line 903
    iget-object v1, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v7}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 905
    iget v1, v7, Landroid/graphics/Rect;->left:I

    sub-int v1, v0, v1

    .line 906
    iget v2, p0, Landroid/support/v7/widget/cb;->A:I

    add-int/2addr v0, v2

    iget v2, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    .line 907
    iget-object v2, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1, v3, v0, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 909
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 910
    if-eqz v2, :cond_86

    .line 911
    invoke-static {v2, v1, v3, v0, v5}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;IIII)V

    .line 917
    :cond_86
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->draw(Landroid/graphics/Canvas;)V

    .line 918
    return-void

    .line 869
    :cond_8a
    sget-object v0, Landroid/support/v7/internal/widget/ae;->a:Landroid/graphics/Rect;

    goto :goto_1a

    :cond_8d
    move v0, v5

    goto :goto_5f

    :cond_8f
    move v2, v3

    goto :goto_46

    :cond_91
    move v0, v5

    move v2, v3

    goto :goto_5f

    :cond_94
    move v0, v2

    goto :goto_65
.end method

.method public final drawableHotspotChanged(FF)V
    .registers 5

    .prologue
    .line 1072
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_9

    .line 1073
    invoke-super {p0, p1, p2}, Landroid/widget/CompoundButton;->drawableHotspotChanged(FF)V

    .line 1076
    :cond_9
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_12

    .line 1077
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1, p2}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 1080
    :cond_12
    iget-object v0, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1b

    .line 1081
    iget-object v0, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1, p2}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 1083
    :cond_1b
    return-void
.end method

.method protected final drawableStateChanged()V
    .registers 3

    .prologue
    .line 1055
    invoke-super {p0}, Landroid/widget/CompoundButton;->drawableStateChanged()V

    .line 1057
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getDrawableState()[I

    move-result-object v0

    .line 1059
    iget-object v1, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_10

    .line 1060
    iget-object v1, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1063
    :cond_10
    iget-object v1, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_19

    .line 1064
    iget-object v1, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1067
    :cond_19
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->invalidate()V

    .line 1068
    return-void
.end method

.method public final getCompoundPaddingLeft()I
    .registers 3

    .prologue
    .line 987
    invoke-static {p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 988
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingLeft()I

    move-result v0

    .line 994
    :cond_a
    :goto_a
    return v0

    .line 990
    :cond_b
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingLeft()I

    move-result v0

    iget v1, p0, Landroid/support/v7/widget/cb;->y:I

    add-int/2addr v0, v1

    .line 991
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 992
    iget v1, p0, Landroid/support/v7/widget/cb;->m:I

    add-int/2addr v0, v1

    goto :goto_a
.end method

.method public final getCompoundPaddingRight()I
    .registers 3

    .prologue
    .line 999
    invoke-static {p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1000
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingRight()I

    move-result v0

    .line 1006
    :cond_a
    :goto_a
    return v0

    .line 1002
    :cond_b
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingRight()I

    move-result v0

    iget v1, p0, Landroid/support/v7/widget/cb;->y:I

    add-int/2addr v0, v1

    .line 1003
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1004
    iget v1, p0, Landroid/support/v7/widget/cb;->m:I

    add-int/2addr v0, v1

    goto :goto_a
.end method

.method public final getShowText()Z
    .registers 2

    .prologue
    .line 519
    iget-boolean v0, p0, Landroid/support/v7/widget/cb;->q:Z

    return v0
.end method

.method public final getSplitTrack()Z
    .registers 2

    .prologue
    .line 470
    iget-boolean v0, p0, Landroid/support/v7/widget/cb;->n:Z

    return v0
.end method

.method public final getSwitchMinWidth()I
    .registers 2

    .prologue
    .line 373
    iget v0, p0, Landroid/support/v7/widget/cb;->l:I

    return v0
.end method

.method public final getSwitchPadding()I
    .registers 2

    .prologue
    .line 352
    iget v0, p0, Landroid/support/v7/widget/cb;->m:I

    return v0
.end method

.method public final getTextOff()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 492
    iget-object v0, p0, Landroid/support/v7/widget/cb;->p:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getTextOn()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 477
    iget-object v0, p0, Landroid/support/v7/widget/cb;->o:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getThumbDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 451
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getThumbTextPadding()I
    .registers 2

    .prologue
    .line 392
    iget v0, p0, Landroid/support/v7/widget/cb;->k:I

    return v0
.end method

.method public final getTrackDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 420
    iget-object v0, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final jumpDrawablesToCurrentState()V
    .registers 3

    .prologue
    .line 1092
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_34

    .line 1093
    invoke-super {p0}, Landroid/widget/CompoundButton;->jumpDrawablesToCurrentState()V

    .line 1095
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_12

    .line 1096
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 1099
    :cond_12
    iget-object v0, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1b

    .line 1100
    iget-object v0, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 1103
    :cond_1b
    iget-object v0, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    if-eqz v0, :cond_34

    iget-object v0, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    invoke-virtual {v0}, Landroid/support/v7/widget/cd;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_34

    .line 1104
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->clearAnimation()V

    .line 1106
    iget-object v0, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    iget v0, v0, Landroid/support/v7/widget/cd;->b:F

    invoke-direct {p0, v0}, Landroid/support/v7/widget/cb;->setThumbPosition(F)V

    .line 1107
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    .line 1110
    :cond_34
    return-void
.end method

.method protected final onCreateDrawableState(I)[I
    .registers 4

    .prologue
    .line 1046
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/CompoundButton;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 1047
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1048
    sget-object v1, Landroid/support/v7/widget/cb;->N:[I

    invoke-static {v0, v1}, Landroid/support/v7/widget/cb;->mergeDrawableStates([I[I)[I

    .line 1050
    :cond_11
    return-object v0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .registers 11

    .prologue
    .line 922
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onDraw(Landroid/graphics/Canvas;)V

    .line 924
    iget-object v0, p0, Landroid/support/v7/widget/cb;->L:Landroid/graphics/Rect;

    .line 925
    iget-object v1, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    .line 926
    if-eqz v1, :cond_9b

    .line 927
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 932
    :goto_c
    iget v2, p0, Landroid/support/v7/widget/cb;->C:I

    .line 933
    iget v3, p0, Landroid/support/v7/widget/cb;->E:I

    .line 934
    iget v4, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v4

    .line 935
    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    .line 937
    iget-object v4, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    .line 938
    if-eqz v1, :cond_45

    .line 939
    iget-boolean v5, p0, Landroid/support/v7/widget/cb;->n:Z

    if-eqz v5, :cond_a0

    if-eqz v4, :cond_a0

    .line 940
    invoke-static {v4}, Landroid/support/v7/internal/widget/ae;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;

    move-result-object v5

    .line 941
    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    .line 942
    iget v6, v0, Landroid/graphics/Rect;->left:I

    iget v7, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v7

    iput v6, v0, Landroid/graphics/Rect;->left:I

    .line 943
    iget v6, v0, Landroid/graphics/Rect;->right:I

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int v5, v6, v5

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 945
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v5

    .line 946
    sget-object v6, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v6}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 947
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 948
    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 954
    :cond_45
    :goto_45
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v5

    .line 956
    if-eqz v4, :cond_4e

    .line 957
    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 960
    :cond_4e
    invoke-direct {p0}, Landroid/support/v7/widget/cb;->getTargetCheckedState()Z

    move-result v0

    if-eqz v0, :cond_a4

    iget-object v0, p0, Landroid/support/v7/widget/cb;->H:Landroid/text/Layout;

    move-object v1, v0

    .line 961
    :goto_57
    if-eqz v1, :cond_97

    .line 962
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getDrawableState()[I

    move-result-object v0

    .line 963
    iget-object v6, p0, Landroid/support/v7/widget/cb;->G:Landroid/content/res/ColorStateList;

    if-eqz v6, :cond_6d

    .line 964
    iget-object v6, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    iget-object v7, p0, Landroid/support/v7/widget/cb;->G:Landroid/content/res/ColorStateList;

    const/4 v8, 0x0

    invoke-virtual {v7, v0, v8}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setColor(I)V

    .line 966
    :cond_6d
    iget-object v6, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    iput-object v0, v6, Landroid/text/TextPaint;->drawableState:[I

    .line 969
    if-eqz v4, :cond_a8

    .line 970
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 971
    iget v4, v0, Landroid/graphics/Rect;->left:I

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v4

    .line 976
    :goto_7c
    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v1}, Landroid/text/Layout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v0, v4

    .line 977
    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    .line 978
    int-to-float v0, v0

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 979
    invoke-virtual {v1, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 982
    :cond_97
    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 983
    return-void

    .line 929
    :cond_9b
    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_c

    .line 950
    :cond_a0
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_45

    .line 960
    :cond_a4
    iget-object v0, p0, Landroid/support/v7/widget/cb;->I:Landroid/text/Layout;

    move-object v1, v0

    goto :goto_57

    .line 973
    :cond_a8
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getWidth()I

    move-result v0

    goto :goto_7c
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1115
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1116
    const-string v0, "android.widget.Switch"

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1117
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 6

    .prologue
    .line 1121
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_29

    .line 1122
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1123
    const-string v0, "android.widget.Switch"

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1124
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2a

    iget-object v0, p0, Landroid/support/v7/widget/cb;->o:Ljava/lang/CharSequence;

    .line 1125
    :goto_16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_29

    .line 1126
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 1127
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 1128
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    .line 1136
    :cond_29
    :goto_29
    return-void

    .line 1124
    :cond_2a
    iget-object v0, p0, Landroid/support/v7/widget/cb;->p:Ljava/lang/CharSequence;

    goto :goto_16

    .line 1130
    :cond_2d
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1131
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1132
    invoke-virtual {p1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    goto :goto_29
.end method

.method protected final onLayout(ZIIII)V
    .registers 11

    .prologue
    const/4 v0, 0x0

    .line 801
    invoke-super/range {p0 .. p5}, Landroid/widget/CompoundButton;->onLayout(ZIIII)V

    .line 805
    iget-object v1, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_97

    .line 806
    iget-object v2, p0, Landroid/support/v7/widget/cb;->L:Landroid/graphics/Rect;

    .line 807
    iget-object v1, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_59

    .line 808
    iget-object v1, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 813
    :goto_13
    iget-object v1, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-static {v1}, Landroid/support/v7/internal/widget/ae;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;

    move-result-object v3

    .line 814
    iget v1, v3, Landroid/graphics/Rect;->left:I

    iget v4, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 815
    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int v2, v3, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 820
    :goto_2c
    invoke-static {p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_5d

    .line 821
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getPaddingLeft()I

    move-result v2

    add-int/2addr v2, v1

    .line 822
    iget v3, p0, Landroid/support/v7/widget/cb;->y:I

    add-int/2addr v3, v2

    sub-int v1, v3, v1

    sub-int v0, v1, v0

    move v1, v0

    move v0, v2

    .line 830
    :goto_40
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getGravity()I

    move-result v2

    and-int/lit8 v2, v2, 0x70

    sparse-switch v2, :sswitch_data_9a

    .line 833
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getPaddingTop()I

    move-result v3

    .line 834
    iget v2, p0, Landroid/support/v7/widget/cb;->z:I

    add-int/2addr v2, v3

    .line 849
    :goto_50
    iput v0, p0, Landroid/support/v7/widget/cb;->B:I

    .line 850
    iput v3, p0, Landroid/support/v7/widget/cb;->C:I

    .line 851
    iput v2, p0, Landroid/support/v7/widget/cb;->E:I

    .line 852
    iput v1, p0, Landroid/support/v7/widget/cb;->D:I

    .line 853
    return-void

    .line 810
    :cond_59
    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_13

    .line 824
    :cond_5d
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, v0

    .line 825
    iget v3, p0, Landroid/support/v7/widget/cb;->y:I

    sub-int v3, v2, v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    move v1, v2

    goto :goto_40

    .line 838
    :sswitch_6f
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Landroid/support/v7/widget/cb;->z:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v2, v3

    .line 840
    iget v2, p0, Landroid/support/v7/widget/cb;->z:I

    add-int/2addr v2, v3

    .line 841
    goto :goto_50

    .line 844
    :sswitch_89
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 845
    iget v3, p0, Landroid/support/v7/widget/cb;->z:I

    sub-int v3, v2, v3

    goto :goto_50

    :cond_97
    move v1, v0

    goto :goto_2c

    .line 830
    nop

    :sswitch_data_9a
    .sparse-switch
        0x10 -> :sswitch_6f
        0x50 -> :sswitch_89
    .end sparse-switch
.end method

.method public final onMeasure(II)V
    .registers 9

    .prologue
    const/4 v1, 0x0

    .line 524
    iget-boolean v0, p0, Landroid/support/v7/widget/cb;->q:Z

    if-eqz v0, :cond_1d

    .line 525
    iget-object v0, p0, Landroid/support/v7/widget/cb;->H:Landroid/text/Layout;

    if-nez v0, :cond_11

    .line 526
    iget-object v0, p0, Landroid/support/v7/widget/cb;->o:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Landroid/support/v7/widget/cb;->a(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cb;->H:Landroid/text/Layout;

    .line 529
    :cond_11
    iget-object v0, p0, Landroid/support/v7/widget/cb;->I:Landroid/text/Layout;

    if-nez v0, :cond_1d

    .line 530
    iget-object v0, p0, Landroid/support/v7/widget/cb;->p:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Landroid/support/v7/widget/cb;->a(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cb;->I:Landroid/text/Layout;

    .line 534
    :cond_1d
    iget-object v4, p0, Landroid/support/v7/widget/cb;->L:Landroid/graphics/Rect;

    .line 537
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a8

    .line 539
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 540
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget v2, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    iget v2, v4, Landroid/graphics/Rect;->right:I

    sub-int v2, v0, v2

    .line 541
    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 548
    :goto_3b
    iget-boolean v3, p0, Landroid/support/v7/widget/cb;->q:Z

    if-eqz v3, :cond_ab

    .line 549
    iget-object v3, p0, Landroid/support/v7/widget/cb;->H:Landroid/text/Layout;

    invoke-virtual {v3}, Landroid/text/Layout;->getWidth()I

    move-result v3

    iget-object v5, p0, Landroid/support/v7/widget/cb;->I:Landroid/text/Layout;

    invoke-virtual {v5}, Landroid/text/Layout;->getWidth()I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v5, p0, Landroid/support/v7/widget/cb;->k:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    .line 555
    :goto_54
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Landroid/support/v7/widget/cb;->A:I

    .line 558
    iget-object v2, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_ad

    .line 559
    iget-object v1, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 560
    iget-object v1, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 568
    :goto_69
    iget v2, v4, Landroid/graphics/Rect;->left:I

    .line 569
    iget v3, v4, Landroid/graphics/Rect;->right:I

    .line 570
    iget-object v4, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_83

    .line 571
    iget-object v4, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    invoke-static {v4}, Landroid/support/v7/internal/widget/ae;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;

    move-result-object v4

    .line 572
    iget v5, v4, Landroid/graphics/Rect;->left:I

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 573
    iget v4, v4, Landroid/graphics/Rect;->right:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 576
    :cond_83
    iget v4, p0, Landroid/support/v7/widget/cb;->l:I

    iget v5, p0, Landroid/support/v7/widget/cb;->A:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v2, v5

    add-int/2addr v2, v3

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 578
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 579
    iput v2, p0, Landroid/support/v7/widget/cb;->y:I

    .line 580
    iput v0, p0, Landroid/support/v7/widget/cb;->z:I

    .line 582
    invoke-super {p0, p1, p2}, Landroid/widget/CompoundButton;->onMeasure(II)V

    .line 584
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getMeasuredHeight()I

    move-result v1

    .line 585
    if-ge v1, v0, :cond_a7

    .line 586
    invoke-static {p0}, Landroid/support/v4/view/cx;->i(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p0, v1, v0}, Landroid/support/v7/widget/cb;->setMeasuredDimension(II)V

    .line 588
    :cond_a7
    return-void

    :cond_a8
    move v0, v1

    move v2, v1

    .line 544
    goto :goto_3b

    :cond_ab
    move v3, v1

    .line 552
    goto :goto_54

    .line 562
    :cond_ad
    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_69
.end method

.method public final onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 593
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 595
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Landroid/support/v7/widget/cb;->o:Ljava/lang/CharSequence;

    .line 596
    :goto_b
    if-eqz v0, :cond_14

    .line 597
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 599
    :cond_14
    return-void

    .line 595
    :cond_15
    iget-object v0, p0, Landroid/support/v7/widget/cb;->p:Ljava/lang/CharSequence;

    goto :goto_b
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 11

    .prologue
    const/4 v5, 0x2

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 634
    iget-object v0, p0, Landroid/support/v7/widget/cb;->v:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 635
    invoke-static {p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 636
    packed-switch v0, :pswitch_data_15c

    .line 708
    :cond_12
    :goto_12
    :pswitch_12
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    :cond_16
    :goto_16
    return v3

    .line 638
    :pswitch_17
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 639
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 640
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 5616
    iget-object v2, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_69

    .line 5621
    invoke-direct {p0}, Landroid/support/v7/widget/cb;->getThumbOffset()I

    move-result v2

    .line 5623
    iget-object v5, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    iget-object v6, p0, Landroid/support/v7/widget/cb;->L:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 5624
    iget v5, p0, Landroid/support/v7/widget/cb;->C:I

    iget v6, p0, Landroid/support/v7/widget/cb;->s:I

    sub-int/2addr v5, v6

    .line 5625
    iget v6, p0, Landroid/support/v7/widget/cb;->B:I

    add-int/2addr v2, v6

    iget v6, p0, Landroid/support/v7/widget/cb;->s:I

    sub-int/2addr v2, v6

    .line 5626
    iget v6, p0, Landroid/support/v7/widget/cb;->A:I

    add-int/2addr v6, v2

    iget-object v7, p0, Landroid/support/v7/widget/cb;->L:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v7

    iget-object v7, p0, Landroid/support/v7/widget/cb;->L:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v7

    iget v7, p0, Landroid/support/v7/widget/cb;->s:I

    add-int/2addr v6, v7

    .line 5628
    iget v7, p0, Landroid/support/v7/widget/cb;->E:I

    iget v8, p0, Landroid/support/v7/widget/cb;->s:I

    add-int/2addr v7, v8

    .line 5629
    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_69

    int-to-float v2, v6

    cmpg-float v2, v0, v2

    if-gez v2, :cond_69

    int-to-float v2, v5

    cmpl-float v2, v1, v2

    if-lez v2, :cond_69

    int-to-float v2, v7

    cmpg-float v2, v1, v2

    if-gez v2, :cond_69

    move v4, v3

    .line 640
    :cond_69
    if-eqz v4, :cond_12

    .line 641
    iput v3, p0, Landroid/support/v7/widget/cb;->r:I

    .line 642
    iput v0, p0, Landroid/support/v7/widget/cb;->t:F

    .line 643
    iput v1, p0, Landroid/support/v7/widget/cb;->u:F

    goto :goto_12

    .line 649
    :pswitch_72
    iget v0, p0, Landroid/support/v7/widget/cb;->r:I

    packed-switch v0, :pswitch_data_168

    goto :goto_12

    .line 655
    :pswitch_78
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 656
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 657
    iget v2, p0, Landroid/support/v7/widget/cb;->t:F

    sub-float v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v4, p0, Landroid/support/v7/widget/cb;->s:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_9e

    iget v2, p0, Landroid/support/v7/widget/cb;->u:F

    sub-float v2, v1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v4, p0, Landroid/support/v7/widget/cb;->s:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_12

    .line 659
    :cond_9e
    iput v5, p0, Landroid/support/v7/widget/cb;->r:I

    .line 660
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 661
    iput v0, p0, Landroid/support/v7/widget/cb;->t:F

    .line 662
    iput v1, p0, Landroid/support/v7/widget/cb;->u:F

    goto/16 :goto_16

    .line 669
    :pswitch_ad
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 670
    invoke-direct {p0}, Landroid/support/v7/widget/cb;->getThumbScrollRange()I

    move-result v0

    .line 671
    iget v5, p0, Landroid/support/v7/widget/cb;->t:F

    sub-float v5, v4, v5

    .line 673
    if-eqz v0, :cond_da

    .line 674
    int-to-float v0, v0

    div-float v0, v5, v0

    .line 680
    :goto_be
    invoke-static {p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_c5

    .line 681
    neg-float v0, v0

    .line 683
    :cond_c5
    iget v5, p0, Landroid/support/v7/widget/cb;->x:F

    add-float/2addr v0, v5

    .line 6142
    cmpg-float v5, v0, v2

    if-gez v5, :cond_e3

    move v1, v2

    .line 684
    :cond_cd
    :goto_cd
    iget v0, p0, Landroid/support/v7/widget/cb;->x:F

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_16

    .line 685
    iput v4, p0, Landroid/support/v7/widget/cb;->t:F

    .line 686
    invoke-direct {p0, v1}, Landroid/support/v7/widget/cb;->setThumbPosition(F)V

    goto/16 :goto_16

    .line 678
    :cond_da
    cmpl-float v0, v5, v2

    if-lez v0, :cond_e0

    move v0, v1

    goto :goto_be

    :cond_e0
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_be

    .line 6142
    :cond_e3
    cmpl-float v2, v0, v1

    if-gtz v2, :cond_cd

    move v1, v0

    goto :goto_cd

    .line 696
    :pswitch_e9
    iget v0, p0, Landroid/support/v7/widget/cb;->r:I

    if-ne v0, v5, :cond_153

    .line 6724
    iput v4, p0, Landroid/support/v7/widget/cb;->r:I

    .line 6728
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_140

    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_140

    move v0, v3

    .line 6729
    :goto_fc
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->isChecked()Z

    move-result v1

    .line 6731
    if-eqz v0, :cond_151

    .line 6732
    iget-object v0, p0, Landroid/support/v7/widget/cb;->v:Landroid/view/VelocityTracker;

    const/16 v5, 0x3e8

    invoke-virtual {v0, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 6733
    iget-object v0, p0, Landroid/support/v7/widget/cb;->v:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    .line 6734
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v6, p0, Landroid/support/v7/widget/cb;->w:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_14c

    .line 6735
    invoke-static {p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_144

    cmpg-float v0, v0, v2

    if-gez v0, :cond_142

    move v0, v3

    .line 6743
    :goto_125
    if-eq v0, v1, :cond_12d

    .line 6744
    invoke-virtual {p0, v4}, Landroid/support/v7/widget/cb;->playSoundEffect(I)V

    .line 6745
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->setChecked(Z)V

    .line 7712
    :cond_12d
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 7713
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 7714
    invoke-super {p0, v0}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 7715
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 699
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto/16 :goto_16

    :cond_140
    move v0, v4

    .line 6728
    goto :goto_fc

    :cond_142
    move v0, v4

    .line 6735
    goto :goto_125

    :cond_144
    cmpl-float v0, v0, v2

    if-lez v0, :cond_14a

    move v0, v3

    goto :goto_125

    :cond_14a
    move v0, v4

    goto :goto_125

    .line 6737
    :cond_14c
    invoke-direct {p0}, Landroid/support/v7/widget/cb;->getTargetCheckedState()Z

    move-result v0

    goto :goto_125

    :cond_151
    move v0, v1

    .line 6740
    goto :goto_125

    .line 702
    :cond_153
    iput v4, p0, Landroid/support/v7/widget/cb;->r:I

    .line 703
    iget-object v0, p0, Landroid/support/v7/widget/cb;->v:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_12

    .line 636
    :pswitch_data_15c
    .packed-switch 0x0
        :pswitch_17
        :pswitch_e9
        :pswitch_72
        :pswitch_e9
    .end packed-switch

    .line 649
    :pswitch_data_168
    .packed-switch 0x0
        :pswitch_12
        :pswitch_78
        :pswitch_ad
    .end packed-switch
.end method

.method public final setChecked(Z)V
    .registers 7

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 784
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 788
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->isChecked()Z

    move-result v2

    .line 790
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    if-eqz v3, :cond_31

    invoke-static {p0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_31

    .line 8751
    new-instance v3, Landroid/support/v7/widget/cd;

    iget v4, p0, Landroid/support/v7/widget/cb;->x:F

    if-eqz v2, :cond_2f

    :goto_1c
    const/4 v1, 0x0

    invoke-direct {v3, p0, v4, v0, v1}, Landroid/support/v7/widget/cd;-><init>(Landroid/support/v7/widget/cb;FFB)V

    iput-object v3, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    .line 8752
    iget-object v0, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/widget/cd;->setDuration(J)V

    .line 8753
    iget-object v0, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->startAnimation(Landroid/view/animation/Animation;)V

    .line 797
    :goto_2e
    return-void

    :cond_2f
    move v0, v1

    .line 8751
    goto :goto_1c

    .line 8757
    :cond_31
    iget-object v3, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    if-eqz v3, :cond_3b

    .line 8758
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->clearAnimation()V

    .line 8759
    const/4 v3, 0x0

    iput-object v3, p0, Landroid/support/v7/widget/cb;->K:Landroid/support/v7/widget/cd;

    .line 795
    :cond_3b
    if-eqz v2, :cond_41

    :goto_3d
    invoke-direct {p0, v0}, Landroid/support/v7/widget/cb;->setThumbPosition(F)V

    goto :goto_2e

    :cond_41
    move v0, v1

    goto :goto_3d
.end method

.method public final setShowText(Z)V
    .registers 3

    .prologue
    .line 509
    iget-boolean v0, p0, Landroid/support/v7/widget/cb;->q:Z

    if-eq v0, p1, :cond_9

    .line 510
    iput-boolean p1, p0, Landroid/support/v7/widget/cb;->q:Z

    .line 511
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->requestLayout()V

    .line 513
    :cond_9
    return-void
.end method

.method public final setSplitTrack(Z)V
    .registers 2

    .prologue
    .line 462
    iput-boolean p1, p0, Landroid/support/v7/widget/cb;->n:Z

    .line 463
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->invalidate()V

    .line 464
    return-void
.end method

.method public final setSwitchMinWidth(I)V
    .registers 2

    .prologue
    .line 362
    iput p1, p0, Landroid/support/v7/widget/cb;->l:I

    .line 363
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->requestLayout()V

    .line 364
    return-void
.end method

.method public final setSwitchPadding(I)V
    .registers 2

    .prologue
    .line 342
    iput p1, p0, Landroid/support/v7/widget/cb;->m:I

    .line 343
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->requestLayout()V

    .line 344
    return-void
.end method

.method public final setSwitchTypeface(Landroid/graphics/Typeface;)V
    .registers 3

    .prologue
    .line 328
    iget-object v0, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-eq v0, p1, :cond_13

    .line 329
    iget-object v0, p0, Landroid/support/v7/widget/cb;->F:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 331
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->requestLayout()V

    .line 332
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->invalidate()V

    .line 334
    :cond_13
    return-void
.end method

.method public final setTextOff(Ljava/lang/CharSequence;)V
    .registers 2

    .prologue
    .line 499
    iput-object p1, p0, Landroid/support/v7/widget/cb;->p:Ljava/lang/CharSequence;

    .line 500
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->requestLayout()V

    .line 501
    return-void
.end method

.method public final setTextOn(Ljava/lang/CharSequence;)V
    .registers 2

    .prologue
    .line 484
    iput-object p1, p0, Landroid/support/v7/widget/cb;->o:Ljava/lang/CharSequence;

    .line 485
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->requestLayout()V

    .line 486
    return-void
.end method

.method public final setThumbDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 430
    iput-object p1, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    .line 431
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->requestLayout()V

    .line 432
    return-void
.end method

.method public final setThumbResource(I)V
    .registers 4

    .prologue
    .line 441
    iget-object v0, p0, Landroid/support/v7/widget/cb;->M:Landroid/support/v7/internal/widget/av;

    .line 5167
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 441
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->setThumbDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 442
    return-void
.end method

.method public final setThumbTextPadding(I)V
    .registers 2

    .prologue
    .line 382
    iput p1, p0, Landroid/support/v7/widget/cb;->k:I

    .line 383
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->requestLayout()V

    .line 384
    return-void
.end method

.method public final setTrackDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 401
    iput-object p1, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    .line 402
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->requestLayout()V

    .line 403
    return-void
.end method

.method public final setTrackResource(I)V
    .registers 4

    .prologue
    .line 411
    iget-object v0, p0, Landroid/support/v7/widget/cb;->M:Landroid/support/v7/internal/widget/av;

    .line 4167
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 411
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->setTrackDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 412
    return-void
.end method

.method public final toggle()V
    .registers 2

    .prologue
    .line 779
    invoke-virtual {p0}, Landroid/support/v7/widget/cb;->isChecked()Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_7
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cb;->setChecked(Z)V

    .line 780
    return-void

    .line 779
    :cond_b
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3

    .prologue
    .line 1087
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/cb;->i:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/cb;->j:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method
