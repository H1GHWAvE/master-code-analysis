.class final Landroid/support/v7/widget/ad;
.super Landroid/support/v7/widget/an;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/CharSequence;

.field final synthetic b:Landroid/support/v7/widget/aa;

.field private u:Landroid/widget/ListAdapter;

.field private final v:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/aa;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6

    .prologue
    .line 688
    iput-object p1, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    .line 689
    invoke-direct {p0, p2, p3, p4}, Landroid/support/v7/widget/an;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 686
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ad;->v:Landroid/graphics/Rect;

    .line 1435
    iput-object p1, p0, Landroid/support/v7/widget/an;->l:Landroid/view/View;

    .line 692
    invoke-virtual {p0}, Landroid/support/v7/widget/ad;->c()V

    .line 2281
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/an;->k:I

    .line 695
    new-instance v0, Landroid/support/v7/widget/ae;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/widget/ae;-><init>(Landroid/support/v7/widget/ad;Landroid/support/v7/widget/aa;)V

    .line 2541
    iput-object v0, p0, Landroid/support/v7/widget/an;->m:Landroid/widget/AdapterView$OnItemClickListener;

    .line 706
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/ad;)Landroid/widget/ListAdapter;
    .registers 2

    .prologue
    .line 683
    iget-object v0, p0, Landroid/support/v7/widget/ad;->u:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;)V
    .registers 2

    .prologue
    .line 720
    iput-object p1, p0, Landroid/support/v7/widget/ad;->a:Ljava/lang/CharSequence;

    .line 721
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/ad;Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 6814
    invoke-static {p1}, Landroid/support/v4/view/cx;->D(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Landroid/support/v7/widget/ad;->v:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    .line 683
    goto :goto_f
.end method

.method private a(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 814
    invoke-static {p1}, Landroid/support/v4/view/cx;->D(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Landroid/support/v7/widget/ad;->v:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method static synthetic b(Landroid/support/v7/widget/ad;)V
    .registers 1

    .prologue
    .line 683
    invoke-super {p0}, Landroid/support/v7/widget/an;->b()V

    return-void
.end method

.method private h()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 715
    iget-object v0, p0, Landroid/support/v7/widget/ad;->a:Ljava/lang/CharSequence;

    return-object v0
.end method


# virtual methods
.method final a()V
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 724
    .line 3389
    iget-object v1, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 726
    if-eqz v1, :cond_97

    .line 727
    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-static {v0}, Landroid/support/v7/widget/aa;->b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 728
    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-static {v0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_8d

    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-static {v0}, Landroid/support/v7/widget/aa;->b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->right:I

    :goto_22
    move v1, v0

    .line 734
    :goto_23
    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-virtual {v0}, Landroid/support/v7/widget/aa;->getPaddingLeft()I

    move-result v3

    .line 735
    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-virtual {v0}, Landroid/support/v7/widget/aa;->getPaddingRight()I

    move-result v4

    .line 736
    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-virtual {v0}, Landroid/support/v7/widget/aa;->getWidth()I

    move-result v5

    .line 737
    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-static {v0}, Landroid/support/v7/widget/aa;->c(Landroid/support/v7/widget/aa;)I

    move-result v0

    const/4 v2, -0x2

    if-ne v0, v2, :cond_aa

    .line 738
    iget-object v2, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    iget-object v0, p0, Landroid/support/v7/widget/ad;->u:Landroid/widget/ListAdapter;

    check-cast v0, Landroid/widget/SpinnerAdapter;

    .line 4389
    iget-object v6, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v6}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 738
    invoke-static {v2, v0, v6}, Landroid/support/v7/widget/aa;->a(Landroid/support/v7/widget/aa;Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    move-result v2

    .line 740
    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-virtual {v0}, Landroid/support/v7/widget/aa;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v6, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-static {v6}, Landroid/support/v7/widget/aa;->b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v6

    iget-object v6, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-static {v6}, Landroid/support/v7/widget/aa;->b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v6

    .line 742
    if-le v2, v0, :cond_c7

    .line 745
    :goto_72
    sub-int v2, v5, v3

    sub-int/2addr v2, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ad;->a(I)V

    .line 752
    :goto_7c
    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-static {v0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 753
    sub-int v0, v5, v4

    .line 4488
    iget v2, p0, Landroid/support/v7/widget/an;->e:I

    .line 753
    sub-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 5451
    :goto_8a
    iput v0, p0, Landroid/support/v7/widget/an;->f:I

    .line 758
    return-void

    .line 728
    :cond_8d
    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-static {v0}, Landroid/support/v7/widget/aa;->b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    goto :goto_22

    .line 731
    :cond_97
    iget-object v1, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-static {v1}, Landroid/support/v7/widget/aa;->b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-static {v2}, Landroid/support/v7/widget/aa;->b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;

    move-result-object v2

    iput v0, v2, Landroid/graphics/Rect;->right:I

    iput v0, v1, Landroid/graphics/Rect;->left:I

    move v1, v0

    goto/16 :goto_23

    .line 747
    :cond_aa
    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-static {v0}, Landroid/support/v7/widget/aa;->c(Landroid/support/v7/widget/aa;)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_ba

    .line 748
    sub-int v0, v5, v3

    sub-int/2addr v0, v4

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ad;->a(I)V

    goto :goto_7c

    .line 750
    :cond_ba
    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-static {v0}, Landroid/support/v7/widget/aa;->c(Landroid/support/v7/widget/aa;)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ad;->a(I)V

    goto :goto_7c

    .line 755
    :cond_c4
    add-int v0, v1, v3

    goto :goto_8a

    :cond_c7
    move v0, v2

    goto :goto_72
.end method

.method public final a(Landroid/widget/ListAdapter;)V
    .registers 2

    .prologue
    .line 710
    invoke-super {p0, p1}, Landroid/support/v7/widget/an;->a(Landroid/widget/ListAdapter;)V

    .line 711
    iput-object p1, p0, Landroid/support/v7/widget/ad;->u:Landroid/widget/ListAdapter;

    .line 712
    return-void
.end method

.method public final b()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    .line 761
    .line 5760
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 763
    invoke-virtual {p0}, Landroid/support/v7/widget/ad;->a()V

    .line 765
    invoke-virtual {p0}, Landroid/support/v7/widget/ad;->e()V

    .line 766
    invoke-super {p0}, Landroid/support/v7/widget/an;->b()V

    .line 5845
    iget-object v1, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    .line 768
    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 769
    iget-object v1, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-virtual {v1}, Landroid/support/v7/widget/aa;->getSelectedItemPosition()I

    move-result v1

    .line 6729
    iget-object v2, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    .line 6760
    iget-object v3, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    .line 6730
    if-eqz v3, :cond_3d

    if-eqz v2, :cond_3d

    .line 6731
    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/support/v7/widget/ar;->a(Landroid/support/v7/widget/ar;Z)Z

    .line 6732
    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ar;->setSelection(I)V

    .line 6734
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_3d

    .line 6735
    invoke-virtual {v2}, Landroid/support/v7/widget/ar;->getChoiceMode()I

    move-result v3

    if-eqz v3, :cond_3d

    .line 6736
    invoke-virtual {v2, v1, v5}, Landroid/support/v7/widget/ar;->setItemChecked(IZ)V

    .line 771
    :cond_3d
    if-eqz v0, :cond_40

    .line 808
    :cond_3f
    :goto_3f
    return-void

    .line 780
    :cond_40
    iget-object v0, p0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-virtual {v0}, Landroid/support/v7/widget/aa;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 781
    if-eqz v0, :cond_3f

    .line 782
    new-instance v1, Landroid/support/v7/widget/af;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/af;-><init>(Landroid/support/v7/widget/ad;)V

    .line 797
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 798
    new-instance v0, Landroid/support/v7/widget/ag;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/ag;-><init>(Landroid/support/v7/widget/ad;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ad;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    goto :goto_3f
.end method
