.class public final Landroid/support/v7/widget/ba;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/view/menu/j;
.implements Landroid/support/v7/internal/view/menu/y;


# instance fields
.field a:Landroid/support/v7/internal/view/menu/v;

.field private b:Landroid/content/Context;

.field private c:Landroid/support/v7/internal/view/menu/i;

.field private d:Landroid/view/View;

.field private e:Landroid/support/v7/widget/bd;

.field private f:Landroid/support/v7/widget/bc;

.field private g:Landroid/view/View$OnTouchListener;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .registers 4

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/ba;-><init>(Landroid/content/Context;Landroid/view/View;B)V

    .line 71
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/View;B)V
    .registers 5

    .prologue
    .line 85
    sget v0, Landroid/support/v7/a/d;->popupMenuStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/ba;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    .line 86
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/View;I)V
    .registers 11

    .prologue
    const/4 v4, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Landroid/support/v7/widget/ba;->b:Landroid/content/Context;

    .line 108
    new-instance v0, Landroid/support/v7/internal/view/menu/i;

    invoke-direct {v0, p1}, Landroid/support/v7/internal/view/menu/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/ba;->c:Landroid/support/v7/internal/view/menu/i;

    .line 109
    iget-object v0, p0, Landroid/support/v7/widget/ba;->c:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/j;)V

    .line 110
    iput-object p2, p0, Landroid/support/v7/widget/ba;->d:Landroid/view/View;

    .line 111
    new-instance v0, Landroid/support/v7/internal/view/menu/v;

    iget-object v2, p0, Landroid/support/v7/widget/ba;->c:Landroid/support/v7/internal/view/menu/i;

    move-object v1, p1

    move-object v3, p2

    move v5, p3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/internal/view/menu/v;-><init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;ZIB)V

    iput-object v0, p0, Landroid/support/v7/widget/ba;->a:Landroid/support/v7/internal/view/menu/v;

    .line 112
    iget-object v0, p0, Landroid/support/v7/widget/ba;->a:Landroid/support/v7/internal/view/menu/v;

    .line 1121
    iput v4, v0, Landroid/support/v7/internal/view/menu/v;->f:I

    .line 113
    iget-object v0, p0, Landroid/support/v7/widget/ba;->a:Landroid/support/v7/internal/view/menu/v;

    .line 1271
    iput-object p0, v0, Landroid/support/v7/internal/view/menu/v;->d:Landroid/support/v7/internal/view/menu/y;

    .line 114
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, Landroid/support/v7/widget/ba;->a:Landroid/support/v7/internal/view/menu/v;

    .line 2125
    iget v0, v0, Landroid/support/v7/internal/view/menu/v;->f:I

    .line 136
    return v0
.end method

.method private static synthetic a(Landroid/support/v7/widget/ba;)Landroid/support/v7/internal/view/menu/v;
    .registers 2

    .prologue
    .line 41
    iget-object v0, p0, Landroid/support/v7/widget/ba;->a:Landroid/support/v7/internal/view/menu/v;

    return-object v0
.end method

.method private a(I)V
    .registers 3

    .prologue
    .line 127
    iget-object v0, p0, Landroid/support/v7/widget/ba;->a:Landroid/support/v7/internal/view/menu/v;

    .line 2121
    iput p1, v0, Landroid/support/v7/internal/view/menu/v;->f:I

    .line 128
    return-void
.end method

.method private a(Landroid/support/v7/widget/bc;)V
    .registers 2

    .prologue
    .line 242
    iput-object p1, p0, Landroid/support/v7/widget/ba;->f:Landroid/support/v7/widget/bc;

    .line 243
    return-void
.end method

.method private a(Landroid/support/v7/widget/bd;)V
    .registers 2

    .prologue
    .line 233
    iput-object p1, p0, Landroid/support/v7/widget/ba;->e:Landroid/support/v7/widget/bd;

    .line 234
    return-void
.end method

.method private b()Landroid/view/View$OnTouchListener;
    .registers 3

    .prologue
    .line 156
    iget-object v0, p0, Landroid/support/v7/widget/ba;->g:Landroid/view/View$OnTouchListener;

    if-nez v0, :cond_d

    .line 157
    new-instance v0, Landroid/support/v7/widget/bb;

    iget-object v1, p0, Landroid/support/v7/widget/ba;->d:Landroid/view/View;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/bb;-><init>(Landroid/support/v7/widget/ba;Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/v7/widget/ba;->g:Landroid/view/View$OnTouchListener;

    .line 178
    :cond_d
    iget-object v0, p0, Landroid/support/v7/widget/ba;->g:Landroid/view/View$OnTouchListener;

    return-object v0
.end method

.method private b(I)V
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/x;
        .end annotation
    .end param

    .prologue
    .line 208
    .line 2199
    new-instance v0, Landroid/support/v7/internal/view/f;

    iget-object v1, p0, Landroid/support/v7/widget/ba;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v7/internal/view/f;-><init>(Landroid/content/Context;)V

    .line 208
    iget-object v1, p0, Landroid/support/v7/widget/ba;->c:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, p1, v1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 209
    return-void
.end method

.method private c()Landroid/view/Menu;
    .registers 2

    .prologue
    .line 189
    iget-object v0, p0, Landroid/support/v7/widget/ba;->c:Landroid/support/v7/internal/view/menu/i;

    return-object v0
.end method

.method private d()Landroid/view/MenuInflater;
    .registers 3

    .prologue
    .line 199
    new-instance v0, Landroid/support/v7/internal/view/f;

    iget-object v1, p0, Landroid/support/v7/widget/ba;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v7/internal/view/f;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private e()V
    .registers 2

    .prologue
    .line 216
    iget-object v0, p0, Landroid/support/v7/widget/ba;->a:Landroid/support/v7/internal/view/menu/v;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/v;->d()V

    .line 217
    return-void
.end method

.method private f()V
    .registers 2

    .prologue
    .line 224
    iget-object v0, p0, Landroid/support/v7/widget/ba;->a:Landroid/support/v7/internal/view/menu/v;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/v;->f()V

    .line 225
    return-void
.end method

.method private static g()V
    .registers 0

    .prologue
    .line 283
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/internal/view/menu/i;)V
    .registers 2

    .prologue
    .line 289
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
    .registers 3

    .prologue
    .line 262
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .registers 4

    .prologue
    .line 249
    iget-object v0, p0, Landroid/support/v7/widget/ba;->e:Landroid/support/v7/widget/bd;

    if-eqz v0, :cond_b

    .line 250
    iget-object v0, p0, Landroid/support/v7/widget/ba;->e:Landroid/support/v7/widget/bd;

    invoke-interface {v0}, Landroid/support/v7/widget/bd;->a()Z

    move-result v0

    .line 252
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final a_(Landroid/support/v7/internal/view/menu/i;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 268
    if-nez p1, :cond_5

    const/4 v0, 0x0

    .line 276
    :cond_4
    :goto_4
    return v0

    .line 270
    :cond_5
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/i;->hasVisibleItems()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 275
    new-instance v1, Landroid/support/v7/internal/view/menu/v;

    iget-object v2, p0, Landroid/support/v7/widget/ba;->b:Landroid/content/Context;

    iget-object v3, p0, Landroid/support/v7/widget/ba;->d:Landroid/view/View;

    invoke-direct {v1, v2, p1, v3}, Landroid/support/v7/internal/view/menu/v;-><init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/v;->d()V

    goto :goto_4
.end method
