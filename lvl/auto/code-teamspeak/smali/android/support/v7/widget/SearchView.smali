.class public final Landroid/support/v7/widget/SearchView;
.super Landroid/support/v7/widget/aj;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/c/c;


# static fields
.field static final a:Landroid/support/v7/widget/bq;

.field private static final c:Z = false

.field private static final d:Ljava/lang/String; = "SearchView"

.field private static final e:Z

.field private static final f:Ljava/lang/String; = "nm"


# instance fields
.field private final A:Landroid/content/Intent;

.field private final B:Ljava/lang/CharSequence;

.field private C:Landroid/support/v7/widget/bs;

.field private D:Landroid/support/v7/widget/br;

.field private E:Landroid/view/View$OnFocusChangeListener;

.field private F:Landroid/support/v7/widget/bt;

.field private G:Landroid/view/View$OnClickListener;

.field private H:Z

.field private I:Z

.field private J:Landroid/support/v4/widget/r;

.field private K:Z

.field private L:Ljava/lang/CharSequence;

.field private M:Z

.field private N:Z

.field private O:I

.field private P:Z

.field private Q:Ljava/lang/CharSequence;

.field private R:Ljava/lang/CharSequence;

.field private S:Z

.field private T:I

.field private U:Landroid/app/SearchableInfo;

.field private V:Landroid/os/Bundle;

.field private final W:Landroid/support/v7/internal/widget/av;

.field private aa:Ljava/lang/Runnable;

.field private final ab:Ljava/lang/Runnable;

.field private ac:Ljava/lang/Runnable;

.field private final ad:Ljava/util/WeakHashMap;

.field private final ae:Landroid/view/View$OnClickListener;

.field private final af:Landroid/widget/TextView$OnEditorActionListener;

.field private final ag:Landroid/widget/AdapterView$OnItemClickListener;

.field private final ah:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private ai:Landroid/text/TextWatcher;

.field b:Landroid/view/View$OnKeyListener;

.field private final g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

.field private final n:Landroid/view/View;

.field private final o:Landroid/view/View;

.field private final p:Landroid/view/View;

.field private final q:Landroid/widget/ImageView;

.field private final r:Landroid/widget/ImageView;

.field private final s:Landroid/widget/ImageView;

.field private final t:Landroid/widget/ImageView;

.field private final u:Landroid/view/View;

.field private final v:Landroid/widget/ImageView;

.field private final w:Landroid/graphics/drawable/Drawable;

.field private final x:I

.field private final y:I

.field private final z:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 109
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_11

    const/4 v0, 0x1

    :goto_7
    sput-boolean v0, Landroid/support/v7/widget/SearchView;->e:Z

    .line 167
    new-instance v0, Landroid/support/v7/widget/bq;

    invoke-direct {v0}, Landroid/support/v7/widget/bq;-><init>()V

    sput-object v0, Landroid/support/v7/widget/SearchView;->a:Landroid/support/v7/widget/bq;

    return-void

    .line 109
    :cond_11
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 271
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;B)V

    .line 272
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4

    .prologue
    .line 275
    sget v0, Landroid/support/v7/a/d;->searchViewStyle:I

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;I)V

    .line 276
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .registers 10

    .prologue
    const/4 v1, 0x0

    const/high16 v6, 0x10000000

    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 279
    invoke-direct {p0, p1, v1, p2}, Landroid/support/v7/widget/aj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 173
    new-instance v0, Landroid/support/v7/widget/be;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/be;-><init>(Landroid/support/v7/widget/SearchView;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->aa:Ljava/lang/Runnable;

    .line 184
    new-instance v0, Landroid/support/v7/widget/bi;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bi;-><init>(Landroid/support/v7/widget/SearchView;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->ab:Ljava/lang/Runnable;

    .line 190
    new-instance v0, Landroid/support/v7/widget/bj;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bj;-><init>(Landroid/support/v7/widget/SearchView;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->ac:Ljava/lang/Runnable;

    .line 200
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->ad:Ljava/util/WeakHashMap;

    .line 938
    new-instance v0, Landroid/support/v7/widget/bn;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bn;-><init>(Landroid/support/v7/widget/SearchView;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->ae:Landroid/view/View$OnClickListener;

    .line 960
    new-instance v0, Landroid/support/v7/widget/bo;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bo;-><init>(Landroid/support/v7/widget/SearchView;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->b:Landroid/view/View$OnKeyListener;

    .line 1123
    new-instance v0, Landroid/support/v7/widget/bp;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bp;-><init>(Landroid/support/v7/widget/SearchView;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->af:Landroid/widget/TextView$OnEditorActionListener;

    .line 1307
    new-instance v0, Landroid/support/v7/widget/bf;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bf;-><init>(Landroid/support/v7/widget/SearchView;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->ag:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1318
    new-instance v0, Landroid/support/v7/widget/bg;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bg;-><init>(Landroid/support/v7/widget/SearchView;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->ah:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 1611
    new-instance v0, Landroid/support/v7/widget/bh;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bh;-><init>(Landroid/support/v7/widget/SearchView;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->ai:Landroid/text/TextWatcher;

    .line 281
    sget-object v0, Landroid/support/v7/a/n;->SearchView:[I

    invoke-static {p1, v1, v0, p2}, Landroid/support/v7/internal/widget/ax;->a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;

    move-result-object v1

    .line 284
    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ax;->a()Landroid/support/v7/internal/widget/av;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->W:Landroid/support/v7/internal/widget/av;

    .line 286
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 287
    sget v2, Landroid/support/v7/a/n;->SearchView_layout:I

    sget v3, Landroid/support/v7/a/k;->abc_search_view:I

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/internal/widget/ax;->e(II)I

    move-result v2

    .line 289
    invoke-virtual {v0, v2, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 291
    sget v0, Landroid/support/v7/a/i;->search_src_text:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    .line 292
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setSearchView(Landroid/support/v7/widget/SearchView;)V

    .line 294
    sget v0, Landroid/support/v7/a/i;->search_edit_frame:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->n:Landroid/view/View;

    .line 295
    sget v0, Landroid/support/v7/a/i;->search_plate:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->o:Landroid/view/View;

    .line 296
    sget v0, Landroid/support/v7/a/i;->submit_area:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->p:Landroid/view/View;

    .line 297
    sget v0, Landroid/support/v7/a/i;->search_button:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->q:Landroid/widget/ImageView;

    .line 298
    sget v0, Landroid/support/v7/a/i;->search_go_btn:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->r:Landroid/widget/ImageView;

    .line 299
    sget v0, Landroid/support/v7/a/i;->search_close_btn:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->s:Landroid/widget/ImageView;

    .line 300
    sget v0, Landroid/support/v7/a/i;->search_voice_btn:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->t:Landroid/widget/ImageView;

    .line 301
    sget v0, Landroid/support/v7/a/i;->search_mag_icon:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->v:Landroid/widget/ImageView;

    .line 304
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->o:Landroid/view/View;

    sget v2, Landroid/support/v7/a/n;->SearchView_queryBackground:I

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 305
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->p:Landroid/view/View;

    sget v2, Landroid/support/v7/a/n;->SearchView_submitBackground:I

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 306
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->q:Landroid/widget/ImageView;

    sget v2, Landroid/support/v7/a/n;->SearchView_searchIcon:I

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 307
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->r:Landroid/widget/ImageView;

    sget v2, Landroid/support/v7/a/n;->SearchView_goIcon:I

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 308
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->s:Landroid/widget/ImageView;

    sget v2, Landroid/support/v7/a/n;->SearchView_closeIcon:I

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 309
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->t:Landroid/widget/ImageView;

    sget v2, Landroid/support/v7/a/n;->SearchView_voiceIcon:I

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 310
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->v:Landroid/widget/ImageView;

    sget v2, Landroid/support/v7/a/n;->SearchView_searchIcon:I

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 312
    sget v0, Landroid/support/v7/a/n;->SearchView_searchHintIcon:I

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->w:Landroid/graphics/drawable/Drawable;

    .line 315
    sget v0, Landroid/support/v7/a/n;->SearchView_suggestionRowLayout:I

    sget v2, Landroid/support/v7/a/k;->abc_search_dropdown_item_icons_2line:I

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/internal/widget/ax;->e(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/SearchView;->x:I

    .line 317
    sget v0, Landroid/support/v7/a/n;->SearchView_commitIcon:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/internal/widget/ax;->e(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/SearchView;->y:I

    .line 319
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->q:Landroid/widget/ImageView;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->ae:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 320
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->s:Landroid/widget/ImageView;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->ae:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->r:Landroid/widget/ImageView;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->ae:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 322
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->t:Landroid/widget/ImageView;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->ae:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->ae:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->ai:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 326
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->af:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 327
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->ag:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 328
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->ah:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 329
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->b:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 332
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    new-instance v2, Landroid/support/v7/widget/bk;

    invoke-direct {v2, p0}, Landroid/support/v7/widget/bk;-><init>(Landroid/support/v7/widget/SearchView;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 340
    sget v0, Landroid/support/v7/a/n;->SearchView_iconifiedByDefault:I

    invoke-virtual {v1, v0, v5}, Landroid/support/v7/internal/widget/ax;->a(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 342
    sget v0, Landroid/support/v7/a/n;->SearchView_android_maxWidth:I

    invoke-virtual {v1, v0, v4}, Landroid/support/v7/internal/widget/ax;->c(II)I

    move-result v0

    .line 343
    if-eq v0, v4, :cond_18e

    .line 344
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->setMaxWidth(I)V

    .line 347
    :cond_18e
    sget v0, Landroid/support/v7/a/n;->SearchView_defaultQueryHint:I

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ax;->c(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->B:Ljava/lang/CharSequence;

    .line 348
    sget v0, Landroid/support/v7/a/n;->SearchView_queryHint:I

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ax;->c(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->L:Ljava/lang/CharSequence;

    .line 350
    sget v0, Landroid/support/v7/a/n;->SearchView_android_imeOptions:I

    invoke-virtual {v1, v0, v4}, Landroid/support/v7/internal/widget/ax;->a(II)I

    move-result v0

    .line 351
    if-eq v0, v4, :cond_1a9

    .line 352
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->setImeOptions(I)V

    .line 355
    :cond_1a9
    sget v0, Landroid/support/v7/a/n;->SearchView_android_inputType:I

    invoke-virtual {v1, v0, v4}, Landroid/support/v7/internal/widget/ax;->a(II)I

    move-result v0

    .line 356
    if-eq v0, v4, :cond_1b4

    .line 357
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->setInputType(I)V

    .line 361
    :cond_1b4
    sget v0, Landroid/support/v7/a/n;->SearchView_android_focusable:I

    invoke-virtual {v1, v0, v5}, Landroid/support/v7/internal/widget/ax;->a(IZ)Z

    move-result v0

    .line 362
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->setFocusable(Z)V

    .line 2183
    iget-object v0, v1, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 367
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.action.WEB_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->z:Landroid/content/Intent;

    .line 368
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->z:Landroid/content/Intent;

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 369
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->z:Landroid/content/Intent;

    const-string v1, "android.speech.extra.LANGUAGE_MODEL"

    const-string v2, "web_search"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->A:Landroid/content/Intent;

    .line 373
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->A:Landroid/content/Intent;

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 375
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getDropDownAnchor()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->u:Landroid/view/View;

    .line 376
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->u:Landroid/view/View;

    if-eqz v0, :cond_207

    .line 377
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_210

    .line 2390
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->u:Landroid/view/View;

    new-instance v1, Landroid/support/v7/widget/bl;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/bl;-><init>(Landroid/support/v7/widget/SearchView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 384
    :cond_207
    :goto_207
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->H:Z

    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 385
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->q()V

    .line 386
    return-void

    .line 2400
    :cond_210
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Landroid/support/v7/widget/bm;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/bm;-><init>(Landroid/support/v7/widget/SearchView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_207
.end method

.method private static a(Landroid/content/Intent;Landroid/app/SearchableInfo;)Landroid/content/Intent;
    .registers 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .prologue
    .line 1467
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 1468
    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 1469
    const-string v2, "calling_package"

    if-nez v0, :cond_12

    const/4 v0, 0x0

    :goto_e
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1471
    return-object v1

    .line 1469
    :cond_12
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    goto :goto_e
.end method

.method private a(Landroid/database/Cursor;)Landroid/content/Intent;
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 1558
    :try_start_1
    const-string v0, "suggest_intent_action"

    invoke-static {p1, v0}, Landroid/support/v7/widget/bz;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1560
    if-nez v0, :cond_15

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-lt v2, v3, :cond_15

    .line 1561
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestIntentAction()Ljava/lang/String;

    move-result-object v0

    .line 1563
    :cond_15
    if-nez v0, :cond_8f

    .line 1564
    const-string v0, "android.intent.action.SEARCH"

    move-object v2, v0

    .line 1568
    :goto_1a
    const-string v0, "suggest_intent_data"

    invoke-static {p1, v0}, Landroid/support/v7/widget/bz;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1569
    sget-boolean v3, Landroid/support/v7/widget/SearchView;->e:Z

    if-eqz v3, :cond_2c

    if-nez v0, :cond_2c

    .line 1570
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestIntentData()Ljava/lang/String;

    move-result-object v0

    .line 1573
    :cond_2c
    if-eqz v0, :cond_51

    .line 1574
    const-string v3, "suggest_intent_data_id"

    invoke-static {p1, v3}, Landroid/support/v7/widget/bz;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1575
    if-eqz v3, :cond_51

    .line 1576
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1579
    :cond_51
    if-nez v0, :cond_65

    move-object v0, v1

    .line 1581
    :goto_54
    const-string v3, "suggest_intent_query"

    invoke-static {p1, v3}, Landroid/support/v7/widget/bz;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1582
    const-string v4, "suggest_intent_extra_data"

    invoke-static {p1, v4}, Landroid/support/v7/widget/bz;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1584
    invoke-direct {p0, v2, v0, v4, v3}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1594
    :goto_64
    return-object v0

    .line 1579
    :cond_65
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_68
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_68} :catch_6a

    move-result-object v0

    goto :goto_54

    .line 1585
    :catch_6a
    move-exception v0

    move-object v2, v0

    .line 1588
    :try_start_6c
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I
    :try_end_6f
    .catch Ljava/lang/RuntimeException; {:try_start_6c .. :try_end_6f} :catch_8c

    move-result v0

    .line 1592
    :goto_70
    const-string v3, "SearchView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Search suggestions cursor at row "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " returned exception."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 1594
    goto :goto_64

    .line 1590
    :catch_8c
    move-exception v0

    const/4 v0, -0x1

    goto :goto_70

    :cond_8f
    move-object v2, v0

    goto :goto_1a
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 8

    .prologue
    .line 1434
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1435
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1439
    if-eqz p2, :cond_f

    .line 1440
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1442
    :cond_f
    const-string v1, "user_query"

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->R:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 1443
    if-eqz p4, :cond_1d

    .line 1444
    const-string v1, "query"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1446
    :cond_1d
    if-eqz p3, :cond_24

    .line 1447
    const-string v1, "intent_extra_data_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1449
    :cond_24
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->V:Landroid/os/Bundle;

    if-eqz v1, :cond_2f

    .line 1450
    const-string v1, "app_data"

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->V:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1456
    :cond_2f
    sget-boolean v1, Landroid/support/v7/widget/SearchView;->e:Z

    if-eqz v1, :cond_3c

    .line 1457
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1459
    :cond_3c
    return-object v0
.end method

.method private a(Landroid/content/Intent;)V
    .registers 6

    .prologue
    .line 1391
    if-nez p1, :cond_3

    .line 1401
    :goto_2
    return-void

    .line 1397
    :cond_3
    :try_start_3
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_a} :catch_b

    goto :goto_2

    .line 1398
    :catch_b
    move-exception v0

    .line 1399
    const-string v1, "SearchView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed launch activity: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method static synthetic a(Landroid/support/v7/widget/SearchView;)V
    .registers 3

    .prologue
    .line 104
    .line 17896
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->hasFocus()Z

    move-result v0

    .line 17897
    if-eqz v0, :cond_24

    sget-object v0, Landroid/support/v7/widget/SearchView;->FOCUSED_STATE_SET:[I

    .line 17898
    :goto_a
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->o:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 17899
    if-eqz v1, :cond_15

    .line 17900
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 17902
    :cond_15
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->p:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 17903
    if-eqz v1, :cond_20

    .line 17904
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 17906
    :cond_20
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->invalidate()V

    .line 104
    return-void

    .line 17897
    :cond_24
    sget-object v0, Landroid/support/v7/widget/SearchView;->EMPTY_STATE_SET:[I

    goto :goto_a
.end method

.method static synthetic a(Landroid/support/v7/widget/SearchView;Ljava/lang/CharSequence;)V
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 104
    .line 22135
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 22136
    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->R:Ljava/lang/CharSequence;

    .line 22137
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_35

    move v0, v1

    .line 22138
    :goto_11
    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->b(Z)V

    .line 22139
    if-nez v0, :cond_37

    :goto_16
    invoke-direct {p0, v1}, Landroid/support/v7/widget/SearchView;->c(Z)V

    .line 22140
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->n()V

    .line 22141
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->m()V

    .line 22142
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->C:Landroid/support/v7/widget/bs;

    if-eqz v0, :cond_2e

    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->Q:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2e

    .line 22143
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    .line 22145
    :cond_2e
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->Q:Ljava/lang/CharSequence;

    .line 104
    return-void

    :cond_35
    move v0, v2

    .line 22137
    goto :goto_11

    :cond_37
    move v1, v2

    .line 22139
    goto :goto_16
.end method

.method static synthetic a(Landroid/support/v7/widget/SearchView;Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 104
    invoke-direct {p0, p1}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .registers 2

    .prologue
    .line 935
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    .line 936
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 1413
    const-string v0, "android.intent.action.SEARCH"

    .line 1414
    invoke-direct {p0, v0, v1, v1, p1}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1415
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1416
    return-void
.end method

.method private a(Z)V
    .registers 8

    .prologue
    const/4 v4, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 823
    iput-boolean p1, p0, Landroid/support/v7/widget/SearchView;->I:Z

    .line 825
    if-eqz p1, :cond_3b

    move v0, v1

    .line 827
    :goto_9
    iget-object v3, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v3}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3d

    move v3, v4

    .line 829
    :goto_16
    iget-object v5, p0, Landroid/support/v7/widget/SearchView;->q:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 830
    invoke-direct {p0, v3}, Landroid/support/v7/widget/SearchView;->b(Z)V

    .line 831
    iget-object v5, p0, Landroid/support/v7/widget/SearchView;->n:Landroid/view/View;

    if-eqz p1, :cond_3f

    move v0, v2

    :goto_23
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 832
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->v:Landroid/widget/ImageView;

    iget-boolean v5, p0, Landroid/support/v7/widget/SearchView;->H:Z

    if-eqz v5, :cond_41

    :goto_2c
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 833
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->n()V

    .line 834
    if-nez v3, :cond_43

    :goto_34
    invoke-direct {p0, v4}, Landroid/support/v7/widget/SearchView;->c(Z)V

    .line 835
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->m()V

    .line 836
    return-void

    :cond_3b
    move v0, v2

    .line 825
    goto :goto_9

    :cond_3d
    move v3, v1

    .line 827
    goto :goto_16

    :cond_3f
    move v0, v1

    .line 831
    goto :goto_23

    :cond_41
    move v2, v1

    .line 832
    goto :goto_2c

    :cond_43
    move v4, v1

    .line 834
    goto :goto_34
.end method

.method private a(I)Z
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 1288
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->F:Landroid/support/v7/widget/bt;

    if-eqz v1, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->F:Landroid/support/v7/widget/bt;

    invoke-interface {v1}, Landroid/support/v7/widget/bt;->b()Z

    move-result v1

    if-nez v1, :cond_2f

    .line 12374
    :cond_d
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    .line 13194
    iget-object v1, v1, Landroid/support/v4/widget/r;->c:Landroid/database/Cursor;

    .line 12375
    if-eqz v1, :cond_26

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 12377
    invoke-direct {p0, v1}, Landroid/support/v7/widget/SearchView;->a(Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v2

    .line 13391
    if-eqz v2, :cond_26

    .line 13397
    :try_start_1f
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_26
    .catch Ljava/lang/RuntimeException; {:try_start_1f .. :try_end_26} :catch_30

    .line 1291
    :cond_26
    :goto_26
    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->setImeVisibility(Z)V

    .line 14163
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->dismissDropDown()V

    .line 1293
    const/4 v0, 0x1

    .line 1295
    :cond_2f
    return v0

    .line 13398
    :catch_30
    move-exception v1

    .line 13399
    const-string v3, "SearchView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed launch activity: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_26
.end method

.method private a(ILandroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/16 v2, 0x15

    const/4 v1, 0x0

    .line 1004
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    if-nez v0, :cond_8

    .line 1043
    :cond_7
    :goto_7
    return v1

    .line 1007
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    if-eqz v0, :cond_7

    .line 1010
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_7

    invoke-static {p2}, Landroid/support/v4/view/ab;->b(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1013
    const/16 v0, 0x42

    if-eq p1, v0, :cond_24

    const/16 v0, 0x54

    if-eq p1, v0, :cond_24

    const/16 v0, 0x3d

    if-ne p1, v0, :cond_2f

    .line 1015
    :cond_24
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getListSelection()I

    move-result v0

    .line 1016
    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->a(I)Z

    move-result v1

    goto :goto_7

    .line 1021
    :cond_2f
    if-eq p1, v2, :cond_35

    const/16 v0, 0x16

    if-ne p1, v0, :cond_57

    .line 1026
    :cond_35
    if-ne p1, v2, :cond_50

    move v0, v1

    .line 1028
    :goto_38
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setSelection(I)V

    .line 1029
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setListSelection(I)V

    .line 1030
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->clearListSelection()V

    .line 1031
    sget-object v0, Landroid/support/v7/widget/SearchView;->a:Landroid/support/v7/widget/bq;

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bq;->a(Landroid/widget/AutoCompleteTextView;)V

    .line 1033
    const/4 v1, 0x1

    goto :goto_7

    .line 1026
    :cond_50
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->length()I

    move-result v0

    goto :goto_38

    .line 1037
    :cond_57
    const/16 v0, 0x13

    if-ne p1, v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getListSelection()I

    move-result v0

    if-nez v0, :cond_7

    goto :goto_7
.end method

.method static a(Landroid/content/Context;)Z
    .registers 3

    .prologue
    .line 1604
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_f

    const/4 v0, 0x1

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method static synthetic a(Landroid/support/v7/widget/SearchView;I)Z
    .registers 3

    .prologue
    .line 104
    invoke-direct {p0, p1}, Landroid/support/v7/widget/SearchView;->a(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Landroid/support/v7/widget/SearchView;ILandroid/view/KeyEvent;)Z
    .registers 4

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/SearchView;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method private b(Landroid/content/Intent;Landroid/app/SearchableInfo;)Landroid/content/Intent;
    .registers 14
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1483
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v5

    .line 1488
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1489
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1490
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v1, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 1497
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1498
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->V:Landroid/os/Bundle;

    if-eqz v0, :cond_2a

    .line 1499
    const-string v0, "app_data"

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->V:Landroid/os/Bundle;

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1505
    :cond_2a
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 1508
    const-string v0, "free_form"

    .line 1511
    const/4 v4, 0x1

    .line 1513
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-lt v1, v3, :cond_a6

    .line 1514
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1515
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoiceLanguageModeId()I

    move-result v1

    if-eqz v1, :cond_4a

    .line 1516
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoiceLanguageModeId()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1518
    :cond_4a
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoicePromptTextId()I

    move-result v1

    if-eqz v1, :cond_a4

    .line 1519
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoicePromptTextId()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1521
    :goto_58
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoiceLanguageId()I

    move-result v9

    if-eqz v9, :cond_a2

    .line 1522
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoiceLanguageId()I

    move-result v9

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1524
    :goto_66
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoiceMaxResults()I

    move-result v9

    if-eqz v9, :cond_9e

    .line 1525
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoiceMaxResults()I

    move-result v4

    move v10, v4

    move-object v4, v0

    move v0, v10

    .line 1528
    :goto_73
    const-string v9, "android.speech.extra.LANGUAGE_MODEL"

    invoke-virtual {v8, v9, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1529
    const-string v4, "android.speech.extra.PROMPT"

    invoke-virtual {v8, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1530
    const-string v1, "android.speech.extra.LANGUAGE"

    invoke-virtual {v8, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1531
    const-string v1, "android.speech.extra.MAX_RESULTS"

    invoke-virtual {v8, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1532
    const-string v0, "calling_package"

    if-nez v5, :cond_99

    :goto_8b
    invoke-virtual {v8, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1536
    const-string v0, "android.speech.extra.RESULTS_PENDINGINTENT"

    invoke-virtual {v8, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1537
    const-string v0, "android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE"

    invoke-virtual {v8, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1539
    return-object v8

    .line 1532
    :cond_99
    invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    goto :goto_8b

    :cond_9e
    move v10, v4

    move-object v4, v0

    move v0, v10

    goto :goto_73

    :cond_a2
    move-object v3, v2

    goto :goto_66

    :cond_a4
    move-object v1, v2

    goto :goto_58

    :cond_a6
    move-object v3, v2

    move-object v1, v2

    move v10, v4

    move-object v4, v0

    move v0, v10

    goto :goto_73
.end method

.method static synthetic b(Landroid/support/v7/widget/SearchView;)Landroid/support/v4/widget/r;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    return-object v0
.end method

.method private b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 1049
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->H:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->w:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_a

    .line 1059
    :cond_9
    :goto_9
    return-object p1

    .line 1053
    :cond_a
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getTextSize()F

    move-result v0

    float-to-double v0, v0

    const-wide/high16 v2, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 1054
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4, v4, v0, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1056
    new-instance v0, Landroid/text/SpannableStringBuilder;

    const-string v1, "   "

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1057
    new-instance v1, Landroid/text/style/ImageSpan;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->w:Landroid/graphics/drawable/Drawable;

    invoke-direct {v1, v2}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1058
    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object p1, v0

    .line 1059
    goto :goto_9
.end method

.method private b(Z)V
    .registers 4

    .prologue
    .line 861
    const/16 v0, 0x8

    .line 862
    iget-boolean v1, p0, Landroid/support/v7/widget/SearchView;->K:Z

    if-eqz v1, :cond_19

    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->l()Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_19

    if-nez p1, :cond_18

    iget-boolean v1, p0, Landroid/support/v7/widget/SearchView;->P:Z

    if-nez v1, :cond_19

    .line 864
    :cond_18
    const/4 v0, 0x0

    .line 866
    :cond_19
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->r:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 867
    return-void
.end method

.method private b(I)Z
    .registers 5

    .prologue
    .line 1299
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->F:Landroid/support/v7/widget/bt;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->F:Landroid/support/v7/widget/bt;

    invoke-interface {v0}, Landroid/support/v7/widget/bt;->a()Z

    move-result v0

    if-nez v0, :cond_33

    .line 14341
    :cond_c
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 14342
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    .line 15194
    iget-object v1, v1, Landroid/support/v4/widget/r;->c:Landroid/database/Cursor;

    .line 14343
    if-eqz v1, :cond_29

    .line 14346
    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 14348
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/r;->c(Landroid/database/Cursor;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 14349
    if-eqz v1, :cond_2b

    .line 14352
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    .line 1302
    :cond_29
    :goto_29
    const/4 v0, 0x1

    .line 1304
    :goto_2a
    return v0

    .line 14355
    :cond_2b
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    goto :goto_29

    .line 14359
    :cond_2f
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    goto :goto_29

    .line 1304
    :cond_33
    const/4 v0, 0x0

    goto :goto_2a
.end method

.method static synthetic b(Landroid/support/v7/widget/SearchView;I)Z
    .registers 5

    .prologue
    .line 104
    .line 20299
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->F:Landroid/support/v7/widget/bt;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->F:Landroid/support/v7/widget/bt;

    invoke-interface {v0}, Landroid/support/v7/widget/bt;->a()Z

    move-result v0

    if-nez v0, :cond_33

    .line 20341
    :cond_c
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 20342
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    .line 21194
    iget-object v1, v1, Landroid/support/v4/widget/r;->c:Landroid/database/Cursor;

    .line 20343
    if-eqz v1, :cond_29

    .line 20346
    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 20348
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/r;->c(Landroid/database/Cursor;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 20349
    if-eqz v1, :cond_2b

    .line 20352
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    .line 20302
    :cond_29
    :goto_29
    const/4 v0, 0x1

    :goto_2a
    return v0

    .line 20355
    :cond_2b
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    goto :goto_29

    .line 20359
    :cond_2f
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    goto :goto_29

    .line 20304
    :cond_33
    const/4 v0, 0x0

    .line 104
    goto :goto_2a
.end method

.method static synthetic c(Landroid/support/v7/widget/SearchView;)Landroid/view/View$OnFocusChangeListener;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->E:Landroid/view/View$OnFocusChangeListener;

    return-object v0
.end method

.method private c(I)V
    .registers 5

    .prologue
    .line 1341
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1342
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    .line 16194
    iget-object v1, v1, Landroid/support/v4/widget/r;->c:Landroid/database/Cursor;

    .line 1343
    if-nez v1, :cond_d

    .line 1361
    :goto_c
    return-void

    .line 1346
    :cond_d
    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 1348
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/r;->c(Landroid/database/Cursor;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1349
    if-eqz v1, :cond_1f

    .line 1352
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    goto :goto_c

    .line 1355
    :cond_1f
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    goto :goto_c

    .line 1359
    :cond_23
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    goto :goto_c
.end method

.method private c(Ljava/lang/CharSequence;)V
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1135
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1136
    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->R:Ljava/lang/CharSequence;

    .line 1137
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_35

    move v0, v1

    .line 1138
    :goto_11
    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->b(Z)V

    .line 1139
    if-nez v0, :cond_37

    :goto_16
    invoke-direct {p0, v1}, Landroid/support/v7/widget/SearchView;->c(Z)V

    .line 1140
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->n()V

    .line 1141
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->m()V

    .line 1142
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->C:Landroid/support/v7/widget/bs;

    if-eqz v0, :cond_2e

    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->Q:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2e

    .line 1143
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    .line 1145
    :cond_2e
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->Q:Ljava/lang/CharSequence;

    .line 1146
    return-void

    :cond_35
    move v0, v2

    .line 1137
    goto :goto_11

    :cond_37
    move v1, v2

    .line 1139
    goto :goto_16
.end method

.method private c(Z)V
    .registers 5

    .prologue
    const/16 v1, 0x8

    .line 1115
    .line 1116
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->P:Z

    if-eqz v0, :cond_18

    .line 9689
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->I:Z

    .line 1116
    if-nez v0, :cond_18

    if-eqz p1, :cond_18

    .line 1117
    const/4 v0, 0x0

    .line 1118
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->r:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1120
    :goto_12
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->t:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1121
    return-void

    :cond_18
    move v0, v1

    goto :goto_12
.end method

.method static synthetic d(Landroid/support/v7/widget/SearchView;)V
    .registers 7

    .prologue
    .line 104
    .line 18264
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_57

    .line 18265
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 18266
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->o:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    .line 18267
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 18268
    invoke-static {p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v1

    .line 18269
    iget-boolean v4, p0, Landroid/support/v7/widget/SearchView;->H:Z

    if-eqz v4, :cond_58

    sget v4, Landroid/support/v7/a/g;->abc_dropdownitem_icon_width:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sget v5, Landroid/support/v7/a/g;->abc_dropdownitem_text_padding_left:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, v4

    .line 18273
    :goto_31
    iget-object v4, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v4}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getDropDownBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 18275
    if-eqz v1, :cond_5a

    .line 18276
    iget v1, v3, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    .line 18280
    :goto_3f
    iget-object v4, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v4, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setDropDownHorizontalOffset(I)V

    .line 18281
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->u:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget v4, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v4

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    sub-int/2addr v0, v2

    .line 18283
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setDropDownWidth(I)V

    .line 104
    :cond_57
    return-void

    .line 18269
    :cond_58
    const/4 v0, 0x0

    goto :goto_31

    .line 18278
    :cond_5a
    iget v1, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    sub-int v1, v2, v1

    goto :goto_3f
.end method

.method private d(I)Z
    .registers 7

    .prologue
    .line 1374
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    .line 17194
    iget-object v0, v0, Landroid/support/v4/widget/r;->c:Landroid/database/Cursor;

    .line 1375
    if-eqz v0, :cond_31

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_31

    .line 1377
    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->a(Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v1

    .line 17391
    if-eqz v1, :cond_19

    .line 17397
    :try_start_12
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_19
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_19} :catch_1b

    .line 1382
    :cond_19
    :goto_19
    const/4 v0, 0x1

    .line 1384
    :goto_1a
    return v0

    .line 17398
    :catch_1b
    move-exception v0

    .line 17399
    const-string v2, "SearchView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed launch activity: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_19

    .line 1384
    :cond_31
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method static synthetic e(Landroid/support/v7/widget/SearchView;)Landroid/widget/ImageView;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->q:Landroid/widget/ImageView;

    return-object v0
.end method

.method private e()V
    .registers 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 390
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->u:Landroid/view/View;

    new-instance v1, Landroid/support/v7/widget/bl;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/bl;-><init>(Landroid/support/v7/widget/SearchView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 397
    return-void
.end method

.method private f()V
    .registers 3

    .prologue
    .line 400
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Landroid/support/v7/widget/bm;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/bm;-><init>(Landroid/support/v7/widget/SearchView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 407
    return-void
.end method

.method static synthetic f(Landroid/support/v7/widget/SearchView;)V
    .registers 1

    .prologue
    .line 104
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->v()V

    return-void
.end method

.method static synthetic g(Landroid/support/v7/widget/SearchView;)Landroid/widget/ImageView;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->s:Landroid/widget/ImageView;

    return-object v0
.end method

.method private g()Z
    .registers 2

    .prologue
    .line 661
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->H:Z

    return v0
.end method

.method private getPreferredWidth()I
    .registers 3

    .prologue
    .line 818
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v7/a/g;->abc_search_view_preferred_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method static synthetic h(Landroid/support/v7/widget/SearchView;)V
    .registers 1

    .prologue
    .line 104
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->u()V

    return-void
.end method

.method private h()Z
    .registers 2

    .prologue
    .line 689
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->I:Z

    return v0
.end method

.method static synthetic i(Landroid/support/v7/widget/SearchView;)Landroid/widget/ImageView;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->r:Landroid/widget/ImageView;

    return-object v0
.end method

.method private i()Z
    .registers 2

    .prologue
    .line 711
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->K:Z

    return v0
.end method

.method static synthetic j(Landroid/support/v7/widget/SearchView;)V
    .registers 3

    .prologue
    .line 104
    .line 19149
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 19150
    if-eqz v0, :cond_31

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v1

    if-lez v1, :cond_31

    .line 19151
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->C:Landroid/support/v7/widget/bs;

    if-eqz v1, :cond_1d

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->C:Landroid/support/v7/widget/bs;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    invoke-interface {v1}, Landroid/support/v7/widget/bs;->a()Z

    move-result v1

    if-nez v1, :cond_31

    .line 19153
    :cond_1d
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    if-eqz v1, :cond_28

    .line 19154
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/String;)V

    .line 19156
    :cond_28
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->setImeVisibility(Z)V

    .line 19163
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->dismissDropDown()V

    .line 104
    :cond_31
    return-void
.end method

.method private j()Z
    .registers 2

    .prologue
    .line 741
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->M:Z

    return v0
.end method

.method static synthetic k(Landroid/support/v7/widget/SearchView;)Landroid/widget/ImageView;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->t:Landroid/widget/ImageView;

    return-object v0
.end method

.method private k()Z
    .registers 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 840
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    if-eqz v1, :cond_2b

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getVoiceSearchEnabled()Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 841
    const/4 v1, 0x0

    .line 842
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getVoiceSearchLaunchWebSearch()Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 843
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->z:Landroid/content/Intent;

    .line 847
    :cond_18
    :goto_18
    if-eqz v1, :cond_2b

    .line 848
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/high16 v3, 0x10000

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 850
    if-eqz v1, :cond_2b

    const/4 v0, 0x1

    .line 853
    :cond_2b
    return v0

    .line 844
    :cond_2c
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getVoiceSearchLaunchRecognizer()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 845
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->A:Landroid/content/Intent;

    goto :goto_18
.end method

.method static synthetic l(Landroid/support/v7/widget/SearchView;)V
    .registers 12

    .prologue
    const/4 v0, 0x0

    .line 104
    .line 19198
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    if-eqz v1, :cond_26

    .line 19201
    iget-object v5, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    .line 19203
    :try_start_7
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceSearchLaunchWebSearch()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 19204
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->z:Landroid/content/Intent;

    .line 19467
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 19468
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v1

    .line 19469
    const-string v3, "calling_package"

    if-nez v1, :cond_27

    :goto_1c
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 19206
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 19216
    :cond_26
    :goto_26
    return-void

    .line 19469
    :cond_27
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1c

    .line 19207
    :cond_2c
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceSearchLaunchRecognizer()Z

    move-result v1

    if-eqz v1, :cond_26

    .line 19208
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->A:Landroid/content/Intent;

    .line 19483
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v6

    .line 19488
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEARCH"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 19489
    invoke-virtual {v2, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 19490
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v3, v4, v2, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 19497
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 19498
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->V:Landroid/os/Bundle;

    if-eqz v2, :cond_5d

    .line 19499
    const-string v2, "app_data"

    iget-object v3, p0, Landroid/support/v7/widget/SearchView;->V:Landroid/os/Bundle;

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 19505
    :cond_5d
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 19508
    const-string v2, "free_form"

    .line 19511
    const/4 v1, 0x1

    .line 19513
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x8

    if-lt v3, v4, :cond_e6

    .line 19514
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 19515
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceLanguageModeId()I

    move-result v3

    if-eqz v3, :cond_e4

    .line 19516
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceLanguageModeId()I

    move-result v2

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 19518
    :goto_7d
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoicePromptTextId()I

    move-result v2

    if-eqz v2, :cond_e2

    .line 19519
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoicePromptTextId()I

    move-result v2

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 19521
    :goto_8b
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceLanguageId()I

    move-result v2

    if-eqz v2, :cond_e0

    .line 19522
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceLanguageId()I

    move-result v2

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 19524
    :goto_99
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceMaxResults()I

    move-result v10

    if-eqz v10, :cond_a3

    .line 19525
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceMaxResults()I

    move-result v1

    .line 19528
    :cond_a3
    :goto_a3
    const-string v5, "android.speech.extra.LANGUAGE_MODEL"

    invoke-virtual {v9, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 19529
    const-string v4, "android.speech.extra.PROMPT"

    invoke-virtual {v9, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 19530
    const-string v3, "android.speech.extra.LANGUAGE"

    invoke-virtual {v9, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 19531
    const-string v2, "android.speech.extra.MAX_RESULTS"

    invoke-virtual {v9, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 19532
    const-string v1, "calling_package"

    if-nez v6, :cond_db

    :goto_bb
    invoke-virtual {v9, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 19536
    const-string v0, "android.speech.extra.RESULTS_PENDINGINTENT"

    invoke-virtual {v9, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 19537
    const-string v0, "android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE"

    invoke-virtual {v9, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 19210
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_cf
    .catch Landroid/content/ActivityNotFoundException; {:try_start_7 .. :try_end_cf} :catch_d1

    goto/16 :goto_26

    .line 19215
    :catch_d1
    move-exception v0

    const-string v0, "SearchView"

    const-string v1, "Could not find voice search activity"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_26

    .line 19532
    :cond_db
    :try_start_db
    invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
    :try_end_de
    .catch Landroid/content/ActivityNotFoundException; {:try_start_db .. :try_end_de} :catch_d1

    move-result-object v0

    goto :goto_bb

    :cond_e0
    move-object v2, v0

    goto :goto_99

    :cond_e2
    move-object v3, v0

    goto :goto_8b

    :cond_e4
    move-object v4, v2

    goto :goto_7d

    :cond_e6
    move-object v3, v0

    move-object v4, v2

    move-object v2, v0

    goto :goto_a3
.end method

.method private l()Z
    .registers 2

    .prologue
    .line 857
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->K:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->P:Z

    if-eqz v0, :cond_e

    .line 8689
    :cond_8
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->I:Z

    .line 857
    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method static synthetic m(Landroid/support/v7/widget/SearchView;)Landroid/support/v7/widget/SearchView$SearchAutoComplete;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    return-object v0
.end method

.method private m()V
    .registers 3

    .prologue
    .line 870
    const/16 v0, 0x8

    .line 871
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->l()Z

    move-result v1

    if-eqz v1, :cond_19

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->r:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_18

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->t:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_19

    .line 874
    :cond_18
    const/4 v0, 0x0

    .line 876
    :cond_19
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->p:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 877
    return-void
.end method

.method private n()V
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 880
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_30

    move v2, v0

    .line 883
    :goto_f
    if-nez v2, :cond_19

    iget-boolean v3, p0, Landroid/support/v7/widget/SearchView;->H:Z

    if-eqz v3, :cond_32

    iget-boolean v3, p0, Landroid/support/v7/widget/SearchView;->S:Z

    if-nez v3, :cond_32

    .line 884
    :cond_19
    :goto_19
    iget-object v3, p0, Landroid/support/v7/widget/SearchView;->s:Landroid/widget/ImageView;

    if-eqz v0, :cond_34

    :goto_1d
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 885
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->s:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 886
    if-eqz v1, :cond_2f

    .line 887
    if-eqz v2, :cond_37

    sget-object v0, Landroid/support/v7/widget/SearchView;->ENABLED_STATE_SET:[I

    :goto_2c
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 889
    :cond_2f
    return-void

    :cond_30
    move v2, v1

    .line 880
    goto :goto_f

    :cond_32
    move v0, v1

    .line 883
    goto :goto_19

    .line 884
    :cond_34
    const/16 v1, 0x8

    goto :goto_1d

    .line 887
    :cond_37
    sget-object v0, Landroid/support/v7/widget/SearchView;->EMPTY_STATE_SET:[I

    goto :goto_2c
.end method

.method static synthetic n(Landroid/support/v7/widget/SearchView;)V
    .registers 1

    .prologue
    .line 104
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->y()V

    return-void
.end method

.method static synthetic o(Landroid/support/v7/widget/SearchView;)Landroid/app/SearchableInfo;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    return-object v0
.end method

.method private o()V
    .registers 2

    .prologue
    .line 892
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->ab:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->post(Ljava/lang/Runnable;)Z

    .line 893
    return-void
.end method

.method private p()V
    .registers 3

    .prologue
    .line 896
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->hasFocus()Z

    move-result v0

    .line 897
    if-eqz v0, :cond_24

    sget-object v0, Landroid/support/v7/widget/SearchView;->FOCUSED_STATE_SET:[I

    .line 898
    :goto_a
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->o:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 899
    if-eqz v1, :cond_15

    .line 900
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 902
    :cond_15
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->p:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 903
    if-eqz v1, :cond_20

    .line 904
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 906
    :cond_20
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->invalidate()V

    .line 907
    return-void

    .line 897
    :cond_24
    sget-object v0, Landroid/support/v7/widget/SearchView;->EMPTY_STATE_SET:[I

    goto :goto_a
.end method

.method static synthetic p(Landroid/support/v7/widget/SearchView;)V
    .registers 2

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->setImeVisibility(Z)V

    return-void
.end method

.method private q()V
    .registers 10

    .prologue
    const/4 v8, 0x0

    .line 1063
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getQueryHint()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1064
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    if-nez v0, :cond_b

    const-string v0, ""

    .line 9049
    :cond_b
    iget-boolean v1, p0, Landroid/support/v7/widget/SearchView;->H:Z

    if-eqz v1, :cond_13

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->w:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_17

    .line 1064
    :cond_13
    :goto_13
    invoke-virtual {v2, v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setHint(Ljava/lang/CharSequence;)V

    .line 1065
    return-void

    .line 9053
    :cond_17
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getTextSize()F

    move-result v1

    float-to-double v4, v1

    const-wide/high16 v6, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v4, v6

    double-to-int v1, v4

    .line 9054
    iget-object v3, p0, Landroid/support/v7/widget/SearchView;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v8, v8, v1, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 9056
    new-instance v1, Landroid/text/SpannableStringBuilder;

    const-string v3, "   "

    invoke-direct {v1, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 9057
    new-instance v3, Landroid/text/style/ImageSpan;

    iget-object v4, p0, Landroid/support/v7/widget/SearchView;->w:Landroid/graphics/drawable/Drawable;

    invoke-direct {v3, v4}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/16 v6, 0x21

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 9058
    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object v0, v1

    .line 9059
    goto :goto_13
.end method

.method private r()V
    .registers 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1072
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getSuggestThreshold()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setThreshold(I)V

    .line 1073
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getImeOptions()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setImeOptions(I)V

    .line 1074
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getInputType()I

    move-result v0

    .line 1077
    and-int/lit8 v2, v0, 0xf

    if-ne v2, v1, :cond_33

    .line 1080
    const v2, -0x10001

    and-int/2addr v0, v2

    .line 1081
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_33

    .line 1082
    const/high16 v2, 0x10000

    or-int/2addr v0, v2

    .line 1089
    const/high16 v2, 0x80000

    or-int/2addr v0, v2

    .line 1092
    :cond_33
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setInputType(I)V

    .line 1093
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    if-eqz v0, :cond_42

    .line 1094
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/r;->a(Landroid/database/Cursor;)V

    .line 1098
    :cond_42
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6b

    .line 1099
    new-instance v0, Landroid/support/v7/widget/bz;

    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    iget-object v4, p0, Landroid/support/v7/widget/SearchView;->ad:Ljava/util/WeakHashMap;

    invoke-direct {v0, v2, p0, v3, v4}, Landroid/support/v7/widget/bz;-><init>(Landroid/content/Context;Landroid/support/v7/widget/SearchView;Landroid/app/SearchableInfo;Ljava/util/WeakHashMap;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    .line 1101
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1102
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    check-cast v0, Landroid/support/v7/widget/bz;

    iget-boolean v2, p0, Landroid/support/v7/widget/SearchView;->M:Z

    if-eqz v2, :cond_69

    const/4 v1, 0x2

    .line 9118
    :cond_69
    iput v1, v0, Landroid/support/v7/widget/bz;->o:I

    .line 1106
    :cond_6b
    return-void
.end method

.method private s()V
    .registers 3

    .prologue
    .line 1149
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1150
    if-eqz v0, :cond_31

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v1

    if-lez v1, :cond_31

    .line 1151
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->C:Landroid/support/v7/widget/bs;

    if-eqz v1, :cond_1d

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->C:Landroid/support/v7/widget/bs;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    invoke-interface {v1}, Landroid/support/v7/widget/bs;->a()Z

    move-result v1

    if-nez v1, :cond_31

    .line 1153
    :cond_1d
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    if-eqz v1, :cond_28

    .line 1154
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/String;)V

    .line 1156
    :cond_28
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->setImeVisibility(Z)V

    .line 10163
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->dismissDropDown()V

    .line 1160
    :cond_31
    return-void
.end method

.method private setImeVisibility(Z)V
    .registers 5

    .prologue
    .line 917
    if-eqz p1, :cond_8

    .line 918
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->aa:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->post(Ljava/lang/Runnable;)Z

    .line 928
    :cond_7
    :goto_7
    return-void

    .line 920
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->aa:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 921
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 924
    if-eqz v0, :cond_7

    .line 925
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_7
.end method

.method private setQuery$609c24db(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 586
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 588
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setSelection(I)V

    .line 589
    iput-object p1, p0, Landroid/support/v7/widget/SearchView;->R:Ljava/lang/CharSequence;

    .line 596
    return-void
.end method

.method private t()V
    .registers 2

    .prologue
    .line 1163
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->dismissDropDown()V

    .line 1164
    return-void
.end method

.method private u()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 1167
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1168
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 1169
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->H:Z

    if-eqz v0, :cond_23

    .line 1171
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->D:Landroid/support/v7/widget/br;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->D:Landroid/support/v7/widget/br;

    invoke-interface {v0}, Landroid/support/v7/widget/br;->a()Z

    move-result v0

    if-nez v0, :cond_23

    .line 1173
    :cond_1d
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 1175
    invoke-direct {p0, v2}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 1184
    :cond_23
    :goto_23
    return-void

    .line 1179
    :cond_24
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 1180
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->requestFocus()Z

    .line 1181
    invoke-direct {p0, v2}, Landroid/support/v7/widget/SearchView;->setImeVisibility(Z)V

    goto :goto_23
.end method

.method private v()V
    .registers 2

    .prologue
    .line 1187
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 1188
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->requestFocus()Z

    .line 1189
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->setImeVisibility(Z)V

    .line 1190
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->G:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_16

    .line 1191
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->G:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1193
    :cond_16
    return-void
.end method

.method private w()V
    .registers 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1198
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    if-nez v1, :cond_6

    .line 1217
    :cond_5
    :goto_5
    return-void

    .line 1201
    :cond_6
    iget-object v5, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    .line 1203
    :try_start_8
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceSearchLaunchWebSearch()Z

    move-result v1

    if-eqz v1, :cond_36

    .line 1204
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->z:Landroid/content/Intent;

    .line 10467
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 10468
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v1

    .line 10469
    const-string v3, "calling_package"

    if-nez v1, :cond_31

    :goto_1d
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1206
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_27
    .catch Landroid/content/ActivityNotFoundException; {:try_start_8 .. :try_end_27} :catch_28

    goto :goto_5

    .line 1215
    :catch_28
    move-exception v0

    const-string v0, "SearchView"

    const-string v1, "Could not find voice search activity"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 10469
    :cond_31
    :try_start_31
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1d

    .line 1207
    :cond_36
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceSearchLaunchRecognizer()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1208
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->A:Landroid/content/Intent;

    .line 10483
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v6

    .line 10488
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEARCH"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 10489
    invoke-virtual {v2, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 10490
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v3, v4, v2, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 10497
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 10498
    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->V:Landroid/os/Bundle;

    if-eqz v2, :cond_67

    .line 10499
    const-string v2, "app_data"

    iget-object v3, p0, Landroid/support/v7/widget/SearchView;->V:Landroid/os/Bundle;

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 10505
    :cond_67
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 10508
    const-string v2, "free_form"

    .line 10511
    const/4 v1, 0x1

    .line 10513
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x8

    if-lt v3, v4, :cond_e6

    .line 10514
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 10515
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceLanguageModeId()I

    move-result v3

    if-eqz v3, :cond_e4

    .line 10516
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceLanguageModeId()I

    move-result v2

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 10518
    :goto_87
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoicePromptTextId()I

    move-result v2

    if-eqz v2, :cond_e2

    .line 10519
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoicePromptTextId()I

    move-result v2

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 10521
    :goto_95
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceLanguageId()I

    move-result v2

    if-eqz v2, :cond_e0

    .line 10522
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceLanguageId()I

    move-result v2

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 10524
    :goto_a3
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceMaxResults()I

    move-result v10

    if-eqz v10, :cond_ad

    .line 10525
    invoke-virtual {v5}, Landroid/app/SearchableInfo;->getVoiceMaxResults()I

    move-result v1

    .line 10528
    :cond_ad
    :goto_ad
    const-string v5, "android.speech.extra.LANGUAGE_MODEL"

    invoke-virtual {v9, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10529
    const-string v4, "android.speech.extra.PROMPT"

    invoke-virtual {v9, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10530
    const-string v3, "android.speech.extra.LANGUAGE"

    invoke-virtual {v9, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10531
    const-string v2, "android.speech.extra.MAX_RESULTS"

    invoke-virtual {v9, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 10532
    const-string v1, "calling_package"

    if-nez v6, :cond_db

    :goto_c5
    invoke-virtual {v9, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10536
    const-string v0, "android.speech.extra.RESULTS_PENDINGINTENT"

    invoke-virtual {v9, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 10537
    const-string v0, "android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE"

    invoke-virtual {v9, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1210
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_5

    .line 10532
    :cond_db
    invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
    :try_end_de
    .catch Landroid/content/ActivityNotFoundException; {:try_start_31 .. :try_end_de} :catch_28

    move-result-object v0

    goto :goto_c5

    :cond_e0
    move-object v2, v0

    goto :goto_a3

    :cond_e2
    move-object v3, v0

    goto :goto_95

    :cond_e4
    move-object v4, v2

    goto :goto_87

    :cond_e6
    move-object v3, v0

    move-object v4, v2

    move-object v2, v0

    goto :goto_ad
.end method

.method private x()V
    .registers 7

    .prologue
    .line 1264
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_57

    .line 1265
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1266
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->o:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    .line 1267
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1268
    invoke-static {p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v1

    .line 1269
    iget-boolean v4, p0, Landroid/support/v7/widget/SearchView;->H:Z

    if-eqz v4, :cond_58

    sget v4, Landroid/support/v7/a/g;->abc_dropdownitem_icon_width:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sget v5, Landroid/support/v7/a/g;->abc_dropdownitem_text_padding_left:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, v4

    .line 1273
    :goto_31
    iget-object v4, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v4}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getDropDownBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1275
    if-eqz v1, :cond_5a

    .line 1276
    iget v1, v3, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    .line 1280
    :goto_3f
    iget-object v4, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v4, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setDropDownHorizontalOffset(I)V

    .line 1281
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->u:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget v4, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v4

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    sub-int/2addr v0, v2

    .line 1283
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setDropDownWidth(I)V

    .line 1285
    :cond_57
    return-void

    .line 1269
    :cond_58
    const/4 v0, 0x0

    goto :goto_31

    .line 1278
    :cond_5a
    iget v1, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    sub-int v1, v2, v1

    goto :goto_3f
.end method

.method private y()V
    .registers 4

    .prologue
    .line 1599
    sget-object v0, Landroid/support/v7/widget/SearchView;->a:Landroid/support/v7/widget/bq;

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    .line 17780
    iget-object v2, v0, Landroid/support/v7/widget/bq;->a:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_10

    .line 17782
    :try_start_8
    iget-object v0, v0, Landroid/support/v7/widget/bq;->a:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_10} :catch_23

    .line 1600
    :cond_10
    :goto_10
    sget-object v0, Landroid/support/v7/widget/SearchView;->a:Landroid/support/v7/widget/bq;

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    .line 17789
    iget-object v2, v0, Landroid/support/v7/widget/bq;->b:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_20

    .line 17791
    :try_start_18
    iget-object v0, v0, Landroid/support/v7/widget/bq;->b:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_20} :catch_21

    .line 17793
    :cond_20
    :goto_20
    return-void

    :catch_21
    move-exception v0

    goto :goto_20

    :catch_23
    move-exception v0

    goto :goto_10
.end method


# virtual methods
.method public final a()V
    .registers 4

    .prologue
    .line 1253
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->S:Z

    if-eqz v0, :cond_5

    .line 1260
    :goto_4
    return-void

    .line 1255
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/SearchView;->S:Z

    .line 1256
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getImeOptions()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/SearchView;->T:I

    .line 1257
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget v1, p0, Landroid/support/v7/widget/SearchView;->T:I

    const/high16 v2, 0x2000000

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setImeOptions(I)V

    .line 1258
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 1259
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->setIconified(Z)V

    goto :goto_4
.end method

.method public final b()V
    .registers 4

    .prologue
    .line 1241
    const-string v0, ""

    .line 11586
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 11588
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v2, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setSelection(I)V

    .line 11589
    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->R:Ljava/lang/CharSequence;

    .line 1242
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 1243
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 1244
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget v1, p0, Landroid/support/v7/widget/SearchView;->T:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setImeOptions(I)V

    .line 1245
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/SearchView;->S:Z

    .line 1246
    return-void
.end method

.method public final clearFocus()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 512
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/SearchView;->N:Z

    .line 513
    invoke-direct {p0, v1}, Landroid/support/v7/widget/SearchView;->setImeVisibility(Z)V

    .line 514
    invoke-super {p0}, Landroid/support/v7/widget/aj;->clearFocus()V

    .line 515
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->clearFocus()V

    .line 516
    iput-boolean v1, p0, Landroid/support/v7/widget/SearchView;->N:Z

    .line 517
    return-void
.end method

.method final d()V
    .registers 2

    .prologue
    .line 1220
    .line 10689
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->I:Z

    .line 1220
    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 1223
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->o()V

    .line 1224
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1225
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->y()V

    .line 1227
    :cond_13
    return-void
.end method

.method public final getImeOptions()I
    .registers 2

    .prologue
    .line 469
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getImeOptions()I

    move-result v0

    return v0
.end method

.method public final getInputType()I
    .registers 2

    .prologue
    .line 487
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getInputType()I

    move-result v0

    return v0
.end method

.method public final getMaxWidth()I
    .registers 2

    .prologue
    .line 779
    iget v0, p0, Landroid/support/v7/widget/SearchView;->O:I

    return v0
.end method

.method public final getQuery()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 574
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public final getQueryHint()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 629
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->L:Ljava/lang/CharSequence;

    if-eqz v0, :cond_7

    .line 630
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->L:Ljava/lang/CharSequence;

    .line 636
    :goto_6
    return-object v0

    .line 631
    :cond_7
    sget-boolean v0, Landroid/support/v7/widget/SearchView;->e:Z

    if-eqz v0, :cond_26

    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    if-eqz v0, :cond_26

    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getHintId()I

    move-result v0

    if-eqz v0, :cond_26

    .line 632
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getHintId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_6

    .line 634
    :cond_26
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->B:Ljava/lang/CharSequence;

    goto :goto_6
.end method

.method final getSuggestionCommitIconResId()I
    .registers 2

    .prologue
    .line 414
    iget v0, p0, Landroid/support/v7/widget/SearchView;->y:I

    return v0
.end method

.method final getSuggestionRowLayout()I
    .registers 2

    .prologue
    .line 410
    iget v0, p0, Landroid/support/v7/widget/SearchView;->x:I

    return v0
.end method

.method public final getSuggestionsAdapter()Landroid/support/v4/widget/r;
    .registers 2

    .prologue
    .line 761
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    return-object v0
.end method

.method protected final onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 911
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->ab:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 912
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->ac:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/SearchView;->post(Ljava/lang/Runnable;)Z

    .line 913
    invoke-super {p0}, Landroid/support/v7/widget/aj;->onDetachedFromWindow()V

    .line 914
    return-void
.end method

.method protected final onMeasure(II)V
    .registers 5

    .prologue
    .line 785
    .line 7689
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->I:Z

    .line 785
    if-eqz v0, :cond_8

    .line 786
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/aj;->onMeasure(II)V

    .line 815
    :goto_7
    return-void

    .line 790
    :cond_8
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 791
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 793
    sparse-switch v1, :sswitch_data_48

    .line 814
    :cond_13
    :goto_13
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v0, p2}, Landroid/support/v7/widget/aj;->onMeasure(II)V

    goto :goto_7

    .line 796
    :sswitch_1d
    iget v1, p0, Landroid/support/v7/widget/SearchView;->O:I

    if-lez v1, :cond_28

    .line 797
    iget v1, p0, Landroid/support/v7/widget/SearchView;->O:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_13

    .line 799
    :cond_28
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->getPreferredWidth()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_13

    .line 804
    :sswitch_31
    iget v1, p0, Landroid/support/v7/widget/SearchView;->O:I

    if-lez v1, :cond_13

    .line 805
    iget v1, p0, Landroid/support/v7/widget/SearchView;->O:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_13

    .line 810
    :sswitch_3c
    iget v0, p0, Landroid/support/v7/widget/SearchView;->O:I

    if-lez v0, :cond_43

    iget v0, p0, Landroid/support/v7/widget/SearchView;->O:I

    goto :goto_13

    :cond_43
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->getPreferredWidth()I

    move-result v0

    goto :goto_13

    .line 793
    :sswitch_data_48
    .sparse-switch
        -0x80000000 -> :sswitch_1d
        0x0 -> :sswitch_3c
        0x40000000 -> :sswitch_31
    .end sparse-switch
.end method

.method public final onWindowFocusChanged(Z)V
    .registers 2

    .prologue
    .line 1231
    invoke-super {p0, p1}, Landroid/support/v7/widget/aj;->onWindowFocusChanged(Z)V

    .line 1233
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->o()V

    .line 1234
    return-void
.end method

.method public final requestFocus(ILandroid/graphics/Rect;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 494
    iget-boolean v1, p0, Landroid/support/v7/widget/SearchView;->N:Z

    if-eqz v1, :cond_6

    .line 505
    :cond_5
    :goto_5
    return v0

    .line 496
    :cond_6
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->isFocusable()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 5689
    iget-boolean v1, p0, Landroid/support/v7/widget/SearchView;->I:Z

    .line 498
    if-nez v1, :cond_1d

    .line 499
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v1, p1, p2}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v1

    .line 500
    if-eqz v1, :cond_1b

    .line 501
    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->a(Z)V

    :cond_1b
    move v0, v1

    .line 503
    goto :goto_5

    .line 505
    :cond_1d
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/aj;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_5
.end method

.method public final setAppSearchData(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 450
    iput-object p1, p0, Landroid/support/v7/widget/SearchView;->V:Landroid/os/Bundle;

    .line 451
    return-void
.end method

.method public final setIconified(Z)V
    .registers 2

    .prologue
    .line 675
    if-eqz p1, :cond_6

    .line 676
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->u()V

    .line 680
    :goto_5
    return-void

    .line 678
    :cond_6
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->v()V

    goto :goto_5
.end method

.method public final setIconifiedByDefault(Z)V
    .registers 3

    .prologue
    .line 650
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->H:Z

    if-ne v0, p1, :cond_5

    .line 654
    :goto_4
    return-void

    .line 651
    :cond_5
    iput-boolean p1, p0, Landroid/support/v7/widget/SearchView;->H:Z

    .line 652
    invoke-direct {p0, p1}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 653
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->q()V

    goto :goto_4
.end method

.method public final setImeOptions(I)V
    .registers 3

    .prologue
    .line 460
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setImeOptions(I)V

    .line 461
    return-void
.end method

.method public final setInputType(I)V
    .registers 3

    .prologue
    .line 479
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setInputType(I)V

    .line 480
    return-void
.end method

.method public final setMaxWidth(I)V
    .registers 2

    .prologue
    .line 768
    iput p1, p0, Landroid/support/v7/widget/SearchView;->O:I

    .line 770
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->requestLayout()V

    .line 771
    return-void
.end method

.method public final setOnCloseListener(Landroid/support/v7/widget/br;)V
    .registers 2

    .prologue
    .line 535
    iput-object p1, p0, Landroid/support/v7/widget/SearchView;->D:Landroid/support/v7/widget/br;

    .line 536
    return-void
.end method

.method public final setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .registers 2

    .prologue
    .line 544
    iput-object p1, p0, Landroid/support/v7/widget/SearchView;->E:Landroid/view/View$OnFocusChangeListener;

    .line 545
    return-void
.end method

.method public final setOnQueryTextListener(Landroid/support/v7/widget/bs;)V
    .registers 2

    .prologue
    .line 526
    iput-object p1, p0, Landroid/support/v7/widget/SearchView;->C:Landroid/support/v7/widget/bs;

    .line 527
    return-void
.end method

.method public final setOnSearchClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2

    .prologue
    .line 565
    iput-object p1, p0, Landroid/support/v7/widget/SearchView;->G:Landroid/view/View$OnClickListener;

    .line 566
    return-void
.end method

.method public final setOnSuggestionListener(Landroid/support/v7/widget/bt;)V
    .registers 2

    .prologue
    .line 553
    iput-object p1, p0, Landroid/support/v7/widget/SearchView;->F:Landroid/support/v7/widget/bt;

    .line 554
    return-void
.end method

.method final setQuery(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 1407
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 1409
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x0

    :goto_e
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setSelection(I)V

    .line 1410
    return-void

    .line 1409
    :cond_12
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    goto :goto_e
.end method

.method public final setQueryHint(Ljava/lang/CharSequence;)V
    .registers 2

    .prologue
    .line 608
    iput-object p1, p0, Landroid/support/v7/widget/SearchView;->L:Ljava/lang/CharSequence;

    .line 609
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->q()V

    .line 610
    return-void
.end method

.method public final setQueryRefinementEnabled(Z)V
    .registers 4

    .prologue
    .line 729
    iput-boolean p1, p0, Landroid/support/v7/widget/SearchView;->M:Z

    .line 730
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    instance-of v0, v0, Landroid/support/v7/widget/bz;

    if-eqz v0, :cond_11

    .line 731
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    check-cast v0, Landroid/support/v7/widget/bz;

    if-eqz p1, :cond_12

    const/4 v1, 0x2

    .line 7118
    :goto_f
    iput v1, v0, Landroid/support/v7/widget/bz;->o:I

    .line 734
    :cond_11
    return-void

    .line 731
    :cond_12
    const/4 v1, 0x1

    goto :goto_f
.end method

.method public final setSearchableInfo(Landroid/app/SearchableInfo;)V
    .registers 10

    .prologue
    const/4 v4, 0x0

    const/high16 v7, 0x10000

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 426
    iput-object p1, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    .line 427
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    if-eqz v0, :cond_79

    .line 428
    sget-boolean v0, Landroid/support/v7/widget/SearchView;->e:Z

    if-eqz v0, :cond_76

    .line 3072
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestThreshold()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setThreshold(I)V

    .line 3073
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getImeOptions()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setImeOptions(I)V

    .line 3074
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getInputType()I

    move-result v0

    .line 3077
    and-int/lit8 v1, v0, 0xf

    if-ne v1, v2, :cond_3f

    .line 3080
    const v1, -0x10001

    and-int/2addr v0, v1

    .line 3081
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3f

    .line 3082
    or-int/2addr v0, v7

    .line 3089
    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    .line 3092
    :cond_3f
    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setInputType(I)V

    .line 3093
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    if-eqz v0, :cond_4d

    .line 3094
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/r;->a(Landroid/database/Cursor;)V

    .line 3098
    :cond_4d
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_76

    .line 3099
    new-instance v0, Landroid/support/v7/widget/bz;

    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v5, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    iget-object v6, p0, Landroid/support/v7/widget/SearchView;->ad:Ljava/util/WeakHashMap;

    invoke-direct {v0, v1, p0, v5, v6}, Landroid/support/v7/widget/bz;-><init>(Landroid/content/Context;Landroid/support/v7/widget/SearchView;Landroid/app/SearchableInfo;Ljava/util/WeakHashMap;)V

    iput-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    .line 3101
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 3102
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    check-cast v0, Landroid/support/v7/widget/bz;

    iget-boolean v1, p0, Landroid/support/v7/widget/SearchView;->M:Z

    if-eqz v1, :cond_b9

    const/4 v1, 0x2

    .line 3118
    :goto_74
    iput v1, v0, Landroid/support/v7/widget/bz;->o:I

    .line 431
    :cond_76
    invoke-direct {p0}, Landroid/support/v7/widget/SearchView;->q()V

    .line 434
    :cond_79
    sget-boolean v0, Landroid/support/v7/widget/SearchView;->e:Z

    if-eqz v0, :cond_ca

    .line 3840
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    if-eqz v0, :cond_c8

    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getVoiceSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_c8

    .line 3842
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getVoiceSearchLaunchWebSearch()Z

    move-result v0

    if-eqz v0, :cond_bb

    .line 3843
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->z:Landroid/content/Intent;

    .line 3847
    :goto_93
    if-eqz v0, :cond_c8

    .line 3848
    invoke-virtual {p0}, Landroid/support/v7/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v7}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 3850
    if-eqz v0, :cond_c6

    move v0, v2

    .line 434
    :goto_a4
    if-eqz v0, :cond_ca

    :goto_a6
    iput-boolean v2, p0, Landroid/support/v7/widget/SearchView;->P:Z

    .line 436
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->P:Z

    if-eqz v0, :cond_b3

    .line 439
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    const-string v1, "nm"

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 4689
    :cond_b3
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->I:Z

    .line 441
    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 442
    return-void

    :cond_b9
    move v1, v2

    .line 3102
    goto :goto_74

    .line 3844
    :cond_bb
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->U:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getVoiceSearchLaunchRecognizer()Z

    move-result v0

    if-eqz v0, :cond_cc

    .line 3845
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->A:Landroid/content/Intent;

    goto :goto_93

    :cond_c6
    move v0, v3

    .line 3850
    goto :goto_a4

    :cond_c8
    move v0, v3

    .line 3853
    goto :goto_a4

    :cond_ca
    move v2, v3

    .line 434
    goto :goto_a6

    :cond_cc
    move-object v0, v4

    goto :goto_93
.end method

.method public final setSubmitButtonEnabled(Z)V
    .registers 3

    .prologue
    .line 701
    iput-boolean p1, p0, Landroid/support/v7/widget/SearchView;->K:Z

    .line 6689
    iget-boolean v0, p0, Landroid/support/v7/widget/SearchView;->I:Z

    .line 702
    invoke-direct {p0, v0}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 703
    return-void
.end method

.method public final setSuggestionsAdapter(Landroid/support/v4/widget/r;)V
    .registers 4

    .prologue
    .line 751
    iput-object p1, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    .line 753
    iget-object v0, p0, Landroid/support/v7/widget/SearchView;->g:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    iget-object v1, p0, Landroid/support/v7/widget/SearchView;->J:Landroid/support/v4/widget/r;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView$SearchAutoComplete;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 754
    return-void
.end method
