.class public final Landroid/support/v7/widget/aa;
.super Landroid/widget/Spinner;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/cr;


# static fields
.field private static final a:Z

.field private static final b:Z

.field private static final c:[I

.field private static final d:I = 0xf

.field private static final e:Ljava/lang/String; = "AppCompatSpinner"

.field private static final f:I = 0x0

.field private static final g:I = 0x1

.field private static final h:I = -0x1


# instance fields
.field private i:Landroid/support/v7/internal/widget/av;

.field private j:Landroid/support/v7/widget/q;

.field private k:Landroid/content/Context;

.field private l:Landroid/support/v7/widget/as;

.field private m:Landroid/widget/SpinnerAdapter;

.field private n:Z

.field private o:Landroid/support/v7/widget/ad;

.field private p:I

.field private final q:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 67
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v0, v3, :cond_1e

    move v0, v1

    :goto_9
    sput-boolean v0, Landroid/support/v7/widget/aa;->a:Z

    .line 68
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_20

    move v0, v1

    :goto_12
    sput-boolean v0, Landroid/support/v7/widget/aa;->b:Z

    .line 70
    new-array v0, v1, [I

    const v1, 0x10102f1

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/widget/aa;->c:[I

    return-void

    :cond_1e
    move v0, v2

    .line 67
    goto :goto_9

    :cond_20
    move v0, v2

    .line 68
    goto :goto_12
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 109
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .registers 5

    .prologue
    .line 123
    const/4 v0, 0x0

    sget v1, Landroid/support/v7/a/d;->spinnerStyle:I

    invoke-direct {p0, p1, v0, v1, p2}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 134
    sget v0, Landroid/support/v7/a/d;->spinnerStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5

    .prologue
    .line 149
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 150
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 11

    .prologue
    .line 168
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIB)V

    .line 169
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IIB)V
    .registers 13

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 197
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 99
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/aa;->q:Landroid/graphics/Rect;

    .line 199
    sget-object v0, Landroid/support/v7/a/n;->Spinner:[I

    invoke-static {p1, p2, v0, p3}, Landroid/support/v7/internal/widget/ax;->a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;

    move-result-object v4

    .line 202
    invoke-virtual {v4}, Landroid/support/v7/internal/widget/ax;->a()Landroid/support/v7/internal/widget/av;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/aa;->i:Landroid/support/v7/internal/widget/av;

    .line 203
    new-instance v0, Landroid/support/v7/widget/q;

    iget-object v2, p0, Landroid/support/v7/widget/aa;->i:Landroid/support/v7/internal/widget/av;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/widget/q;-><init>(Landroid/view/View;Landroid/support/v7/internal/widget/av;)V

    iput-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    .line 208
    sget v0, Landroid/support/v7/a/n;->Spinner_popupTheme:I

    invoke-virtual {v4, v0, v5}, Landroid/support/v7/internal/widget/ax;->e(II)I

    move-result v2

    .line 209
    if-eqz v2, :cond_ab

    .line 210
    new-instance v0, Landroid/support/v7/internal/view/b;

    invoke-direct {v0, p1, v2}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;I)V

    move-object v2, p0

    .line 214
    :goto_30
    iput-object v0, v2, Landroid/support/v7/widget/aa;->k:Landroid/content/Context;

    .line 218
    iget-object v0, p0, Landroid/support/v7/widget/aa;->k:Landroid/content/Context;

    if-eqz v0, :cond_93

    .line 219
    const/4 v0, -0x1

    if-ne p4, v0, :cond_58

    .line 220
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_ce

    .line 224
    :try_start_3f
    sget-object v0, Landroid/support/v7/widget/aa;->c:[I

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
    :try_end_45
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_45} :catch_b7
    .catchall {:try_start_3f .. :try_end_45} :catchall_c6

    move-result-object v2

    .line 226
    const/4 v0, 0x0

    :try_start_47
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 227
    const/4 v0, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_47 .. :try_end_52} :catch_d2
    .catchall {:try_start_47 .. :try_end_52} :catchall_d0

    move-result p4

    .line 232
    :cond_53
    if-eqz v2, :cond_58

    .line 233
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 242
    :cond_58
    :goto_58
    if-ne p4, v3, :cond_93

    .line 243
    new-instance v0, Landroid/support/v7/widget/ad;

    iget-object v2, p0, Landroid/support/v7/widget/aa;->k:Landroid/content/Context;

    invoke-direct {v0, p0, v2, p2, p3}, Landroid/support/v7/widget/ad;-><init>(Landroid/support/v7/widget/aa;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 244
    iget-object v2, p0, Landroid/support/v7/widget/aa;->k:Landroid/content/Context;

    sget-object v5, Landroid/support/v7/a/n;->Spinner:[I

    invoke-static {v2, p2, v5, p3}, Landroid/support/v7/internal/widget/ax;->a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;

    move-result-object v2

    .line 246
    sget v5, Landroid/support/v7/a/n;->Spinner_android_dropDownWidth:I

    const/4 v6, -0x2

    invoke-virtual {v2, v5, v6}, Landroid/support/v7/internal/widget/ax;->d(II)I

    move-result v5

    iput v5, p0, Landroid/support/v7/widget/aa;->p:I

    .line 248
    sget v5, Landroid/support/v7/a/n;->Spinner_android_popupBackground:I

    invoke-virtual {v2, v5}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/ad;->a(Landroid/graphics/drawable/Drawable;)V

    .line 250
    sget v5, Landroid/support/v7/a/n;->Spinner_android_prompt:I

    .line 1099
    iget-object v6, v4, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v6, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1720
    iput-object v5, v0, Landroid/support/v7/widget/ad;->a:Ljava/lang/CharSequence;

    .line 2183
    iget-object v2, v2, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 253
    iput-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    .line 254
    new-instance v2, Landroid/support/v7/widget/ab;

    invoke-direct {v2, p0, p0, v0}, Landroid/support/v7/widget/ab;-><init>(Landroid/support/v7/widget/aa;Landroid/view/View;Landroid/support/v7/widget/ad;)V

    iput-object v2, p0, Landroid/support/v7/widget/aa;->l:Landroid/support/v7/widget/as;

    .line 3183
    :cond_93
    iget-object v0, v4, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 272
    iput-boolean v3, p0, Landroid/support/v7/widget/aa;->n:Z

    .line 276
    iget-object v0, p0, Landroid/support/v7/widget/aa;->m:Landroid/widget/SpinnerAdapter;

    if-eqz v0, :cond_a5

    .line 277
    iget-object v0, p0, Landroid/support/v7/widget/aa;->m:Landroid/widget/SpinnerAdapter;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aa;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 278
    iput-object v1, p0, Landroid/support/v7/widget/aa;->m:Landroid/widget/SpinnerAdapter;

    .line 281
    :cond_a5
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/q;->a(Landroid/util/AttributeSet;I)V

    .line 282
    return-void

    .line 214
    :cond_ab
    sget-boolean v0, Landroid/support/v7/widget/aa;->a:Z

    if-nez v0, :cond_b3

    move-object v0, p1

    move-object v2, p0

    goto/16 :goto_30

    :cond_b3
    move-object v0, v1

    move-object v2, p0

    goto/16 :goto_30

    .line 229
    :catch_b7
    move-exception v0

    move-object v2, v1

    .line 230
    :goto_b9
    :try_start_b9
    const-string v5, "AppCompatSpinner"

    const-string v6, "Could not read android:spinnerMode"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c0
    .catchall {:try_start_b9 .. :try_end_c0} :catchall_d0

    .line 232
    if-eqz v2, :cond_58

    .line 233
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_58

    .line 232
    :catchall_c6
    move-exception v0

    move-object v2, v1

    :goto_c8
    if-eqz v2, :cond_cd

    .line 233
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    :cond_cd
    throw v0

    :cond_ce
    move p4, v3

    .line 238
    goto :goto_58

    .line 232
    :catchall_d0
    move-exception v0

    goto :goto_c8

    .line 229
    :catch_d2
    move-exception v0

    goto :goto_b9
.end method

.method static synthetic a(Landroid/support/v7/widget/aa;Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I
    .registers 4

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/aa;->a(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    move-result v0

    return v0
.end method

.method private a(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I
    .registers 13

    .prologue
    const/4 v2, 0x0

    const/4 v9, -0x2

    const/4 v0, 0x0

    .line 523
    if-nez p1, :cond_6

    .line 563
    :goto_5
    return v0

    .line 530
    :cond_6
    invoke-virtual {p0}, Landroid/support/v7/widget/aa;->getMeasuredWidth()I

    move-result v1

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 532
    invoke-virtual {p0}, Landroid/support/v7/widget/aa;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 537
    invoke-virtual {p0}, Landroid/support/v7/widget/aa;->getSelectedItemPosition()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 538
    invoke-interface {p1}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v3

    add-int/lit8 v4, v1, 0xf

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 539
    sub-int v3, v8, v1

    .line 540
    rsub-int/lit8 v3, v3, 0xf

    sub-int/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v5, v1

    move-object v3, v2

    move v4, v0

    move v1, v0

    .line 541
    :goto_35
    if-ge v5, v8, :cond_60

    .line 542
    invoke-interface {p1, v5}, Landroid/widget/SpinnerAdapter;->getItemViewType(I)I

    move-result v0

    .line 543
    if-eq v0, v1, :cond_74

    move-object v1, v2

    .line 547
    :goto_3e
    invoke-interface {p1, v5, v1, p0}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 548
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-nez v1, :cond_50

    .line 549
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v9, v9}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 553
    :cond_50
    invoke-virtual {v3, v6, v7}, Landroid/view/View;->measure(II)V

    .line 554
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 541
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v0

    goto :goto_35

    .line 558
    :cond_60
    if-eqz p2, :cond_72

    .line 559
    iget-object v0, p0, Landroid/support/v7/widget/aa;->q:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 560
    iget-object v0, p0, Landroid/support/v7/widget/aa;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Landroid/support/v7/widget/aa;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, v4

    goto :goto_5

    :cond_72
    move v0, v4

    goto :goto_5

    :cond_74
    move v0, v1

    move-object v1, v3

    goto :goto_3e
.end method

.method static synthetic a(Landroid/support/v7/widget/aa;)Landroid/support/v7/widget/ad;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    return-object v0
.end method

.method static synthetic a()Z
    .registers 1

    .prologue
    .line 65
    sget-boolean v0, Landroid/support/v7/widget/aa;->a:Z

    return v0
.end method

.method static synthetic b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v7/widget/aa;->q:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic c(Landroid/support/v7/widget/aa;)I
    .registers 2

    .prologue
    .line 65
    iget v0, p0, Landroid/support/v7/widget/aa;->p:I

    return v0
.end method


# virtual methods
.method protected final drawableStateChanged()V
    .registers 2

    .prologue
    .line 516
    invoke-super {p0}, Landroid/widget/Spinner;->drawableStateChanged()V

    .line 517
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_c

    .line 518
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->c()V

    .line 520
    :cond_c
    return-void
.end method

.method public final getDropDownHorizontalOffset()I
    .registers 2

    .prologue
    .line 349
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_9

    .line 350
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    .line 6442
    iget v0, v0, Landroid/support/v7/widget/an;->f:I

    .line 354
    :goto_8
    return v0

    .line 351
    :cond_9
    sget-boolean v0, Landroid/support/v7/widget/aa;->b:Z

    if-eqz v0, :cond_12

    .line 352
    invoke-super {p0}, Landroid/widget/Spinner;->getDropDownHorizontalOffset()I

    move-result v0

    goto :goto_8

    .line 354
    :cond_12
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final getDropDownVerticalOffset()I
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 326
    iget-object v1, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v1, :cond_f

    .line 327
    iget-object v1, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    .line 4458
    iget-boolean v2, v1, Landroid/support/v7/widget/an;->h:Z

    if-nez v2, :cond_c

    .line 4459
    :cond_b
    :goto_b
    return v0

    .line 4461
    :cond_c
    iget v0, v1, Landroid/support/v7/widget/an;->g:I

    goto :goto_b

    .line 328
    :cond_f
    sget-boolean v1, Landroid/support/v7/widget/aa;->b:Z

    if-eqz v1, :cond_b

    .line 329
    invoke-super {p0}, Landroid/widget/Spinner;->getDropDownVerticalOffset()I

    move-result v0

    goto :goto_b
.end method

.method public final getDropDownWidth()I
    .registers 2

    .prologue
    .line 366
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_7

    .line 367
    iget v0, p0, Landroid/support/v7/widget/aa;->p:I

    .line 371
    :goto_6
    return v0

    .line 368
    :cond_7
    sget-boolean v0, Landroid/support/v7/widget/aa;->b:Z

    if-eqz v0, :cond_10

    .line 369
    invoke-super {p0}, Landroid/widget/Spinner;->getDropDownWidth()I

    move-result v0

    goto :goto_6

    .line 371
    :cond_10
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final getPopupBackground()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 309
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_d

    .line 310
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    .line 3389
    iget-object v0, v0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 314
    :goto_c
    return-object v0

    .line 311
    :cond_d
    sget-boolean v0, Landroid/support/v7/widget/aa;->b:Z

    if-eqz v0, :cond_16

    .line 312
    invoke-super {p0}, Landroid/widget/Spinner;->getPopupBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_c

    .line 314
    :cond_16
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public final getPopupContext()Landroid/content/Context;
    .registers 2

    .prologue
    .line 288
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_7

    .line 289
    iget-object v0, p0, Landroid/support/v7/widget/aa;->k:Landroid/content/Context;

    .line 293
    :goto_6
    return-object v0

    .line 290
    :cond_7
    sget-boolean v0, Landroid/support/v7/widget/aa;->a:Z

    if-eqz v0, :cond_10

    .line 291
    invoke-super {p0}, Landroid/widget/Spinner;->getPopupContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_6

    .line 293
    :cond_10
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final getPrompt()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 441
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    .line 9715
    iget-object v0, v0, Landroid/support/v7/widget/ad;->a:Ljava/lang/CharSequence;

    .line 441
    :goto_8
    return-object v0

    :cond_9
    invoke-super {p0}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_8
.end method

.method public final getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 483
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->a()Landroid/content/res/ColorStateList;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 510
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->b()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method protected final onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 393
    invoke-super {p0}, Landroid/widget/Spinner;->onDetachedFromWindow()V

    .line 395
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_16

    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    .line 6760
    iget-object v0, v0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 395
    if-eqz v0, :cond_16

    .line 396
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    invoke-virtual {v0}, Landroid/support/v7/widget/ad;->d()V

    .line 398
    :cond_16
    return-void
.end method

.method protected final onMeasure(II)V
    .registers 6

    .prologue
    .line 410
    invoke-super {p0, p1, p2}, Landroid/widget/Spinner;->onMeasure(II)V

    .line 412
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_32

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_32

    .line 413
    invoke-virtual {p0}, Landroid/support/v7/widget/aa;->getMeasuredWidth()I

    move-result v0

    .line 414
    invoke-virtual {p0}, Landroid/support/v7/widget/aa;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/widget/aa;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Landroid/support/v7/widget/aa;->a(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/aa;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/aa;->setMeasuredDimension(II)V

    .line 419
    :cond_32
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3

    .prologue
    .line 402
    iget-object v0, p0, Landroid/support/v7/widget/aa;->l:Landroid/support/v7/widget/as;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/aa;->l:Landroid/support/v7/widget/as;

    invoke-virtual {v0, p0, p1}, Landroid/support/v7/widget/as;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 403
    const/4 v0, 0x1

    .line 405
    :goto_d
    return v0

    :cond_e
    invoke-super {p0, p1}, Landroid/widget/Spinner;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_d
.end method

.method public final performClick()Z
    .registers 2

    .prologue
    .line 423
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_15

    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    .line 7760
    iget-object v0, v0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 423
    if-nez v0, :cond_15

    .line 424
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    invoke-virtual {v0}, Landroid/support/v7/widget/ad;->b()V

    .line 425
    const/4 v0, 0x1

    .line 427
    :goto_14
    return v0

    :cond_15
    invoke-super {p0}, Landroid/widget/Spinner;->performClick()Z

    move-result v0

    goto :goto_14
.end method

.method public final bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .registers 2

    .prologue
    .line 65
    check-cast p1, Landroid/widget/SpinnerAdapter;

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public final setAdapter(Landroid/widget/SpinnerAdapter;)V
    .registers 5

    .prologue
    .line 378
    iget-boolean v0, p0, Landroid/support/v7/widget/aa;->n:Z

    if-nez v0, :cond_7

    .line 379
    iput-object p1, p0, Landroid/support/v7/widget/aa;->m:Landroid/widget/SpinnerAdapter;

    .line 389
    :cond_6
    :goto_6
    return-void

    .line 383
    :cond_7
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 385
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_6

    .line 386
    iget-object v0, p0, Landroid/support/v7/widget/aa;->k:Landroid/content/Context;

    if-nez v0, :cond_25

    invoke-virtual {p0}, Landroid/support/v7/widget/aa;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 387
    :goto_16
    iget-object v1, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    new-instance v2, Landroid/support/v7/widget/ac;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    invoke-direct {v2, p1, v0}, Landroid/support/v7/widget/ac;-><init>(Landroid/widget/SpinnerAdapter;Landroid/content/res/Resources$Theme;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ad;->a(Landroid/widget/ListAdapter;)V

    goto :goto_6

    .line 386
    :cond_25
    iget-object v0, p0, Landroid/support/v7/widget/aa;->k:Landroid/content/Context;

    goto :goto_16
.end method

.method public final setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 4

    .prologue
    .line 454
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 455
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_d

    .line 456
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    .line 10077
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/q;->b(Landroid/content/res/ColorStateList;)V

    .line 458
    :cond_d
    return-void
.end method

.method public final setBackgroundResource(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 446
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setBackgroundResource(I)V

    .line 447
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_c

    .line 448
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/q;->a(I)V

    .line 450
    :cond_c
    return-void
.end method

.method public final setDropDownHorizontalOffset(I)V
    .registers 3

    .prologue
    .line 335
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_9

    .line 336
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    .line 5451
    iput p1, v0, Landroid/support/v7/widget/an;->f:I

    .line 340
    :cond_8
    :goto_8
    return-void

    .line 337
    :cond_9
    sget-boolean v0, Landroid/support/v7/widget/aa;->b:Z

    if-eqz v0, :cond_8

    .line 338
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setDropDownHorizontalOffset(I)V

    goto :goto_8
.end method

.method public final setDropDownVerticalOffset(I)V
    .registers 4

    .prologue
    .line 318
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_c

    .line 319
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    .line 3470
    iput p1, v0, Landroid/support/v7/widget/an;->g:I

    .line 3471
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/an;->h:Z

    .line 323
    :cond_b
    :goto_b
    return-void

    .line 320
    :cond_c
    sget-boolean v0, Landroid/support/v7/widget/aa;->b:Z

    if-eqz v0, :cond_b

    .line 321
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setDropDownVerticalOffset(I)V

    goto :goto_b
.end method

.method public final setDropDownWidth(I)V
    .registers 3

    .prologue
    .line 358
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_7

    .line 359
    iput p1, p0, Landroid/support/v7/widget/aa;->p:I

    .line 363
    :cond_6
    :goto_6
    return-void

    .line 360
    :cond_7
    sget-boolean v0, Landroid/support/v7/widget/aa;->b:Z

    if-eqz v0, :cond_6

    .line 361
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setDropDownWidth(I)V

    goto :goto_6
.end method

.method public final setPopupBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 297
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_a

    .line 298
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ad;->a(Landroid/graphics/drawable/Drawable;)V

    .line 302
    :cond_9
    :goto_9
    return-void

    .line 299
    :cond_a
    sget-boolean v0, Landroid/support/v7/widget/aa;->b:Z

    if-eqz v0, :cond_9

    .line 300
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setPopupBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_9
.end method

.method public final setPopupBackgroundResource(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 305
    invoke-virtual {p0}, Landroid/support/v7/widget/aa;->getPopupContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aa;->setPopupBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 306
    return-void
.end method

.method public final setPrompt(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 432
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    if-eqz v0, :cond_9

    .line 433
    iget-object v0, p0, Landroid/support/v7/widget/aa;->o:Landroid/support/v7/widget/ad;

    .line 8720
    iput-object p1, v0, Landroid/support/v7/widget/ad;->a:Ljava/lang/CharSequence;

    .line 437
    :goto_8
    return-void

    .line 435
    :cond_9
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    goto :goto_8
.end method

.method public final setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .registers 3
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 469
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_9

    .line 470
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/q;->a(Landroid/content/res/ColorStateList;)V

    .line 472
    :cond_9
    return-void
.end method

.method public final setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .registers 3
    .param p1    # Landroid/graphics/PorterDuff$Mode;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 496
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_9

    .line 497
    iget-object v0, p0, Landroid/support/v7/widget/aa;->j:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/q;->a(Landroid/graphics/PorterDuff$Mode;)V

    .line 499
    :cond_9
    return-void
.end method
