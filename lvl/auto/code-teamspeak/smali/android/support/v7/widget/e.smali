.class final Landroid/support/v7/widget/e;
.super Landroid/support/v7/internal/widget/TintImageView;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/widget/k;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/ActionMenuPresenter;

.field private final b:[F


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;)V
    .registers 6

    .prologue
    const/4 v2, 0x1

    .line 612
    iput-object p1, p0, Landroid/support/v7/widget/e;->a:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 613
    const/4 v0, 0x0

    sget v1, Landroid/support/v7/a/d;->actionOverflowButtonStyle:I

    invoke-direct {p0, p2, v0, v1}, Landroid/support/v7/internal/widget/TintImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 610
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Landroid/support/v7/widget/e;->b:[F

    .line 615
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/e;->setClickable(Z)V

    .line 616
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/e;->setFocusable(Z)V

    .line 617
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/e;->setVisibility(I)V

    .line 618
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/e;->setEnabled(Z)V

    .line 620
    new-instance v0, Landroid/support/v7/widget/f;

    invoke-direct {v0, p0, p0, p1}, Landroid/support/v7/widget/f;-><init>(Landroid/support/v7/widget/e;Landroid/view/View;Landroid/support/v7/widget/ActionMenuPresenter;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/e;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 649
    return-void
.end method


# virtual methods
.method public final d()Z
    .registers 2

    .prologue
    .line 664
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .registers 2

    .prologue
    .line 669
    const/4 v0, 0x0

    return v0
.end method

.method public final performClick()Z
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 653
    invoke-super {p0}, Landroid/support/v7/internal/widget/TintImageView;->performClick()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 659
    :goto_7
    return v1

    .line 657
    :cond_8
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/e;->playSoundEffect(I)V

    .line 658
    iget-object v0, p0, Landroid/support/v7/widget/e;->a:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->e()Z

    goto :goto_7
.end method

.method protected final setFrame(IIII)Z
    .registers 13

    .prologue
    .line 674
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/internal/widget/TintImageView;->setFrame(IIII)Z

    move-result v0

    .line 677
    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 678
    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 679
    if-eqz v1, :cond_3f

    if-eqz v2, :cond_3f

    .line 680
    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getWidth()I

    move-result v1

    .line 681
    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getHeight()I

    move-result v3

    .line 682
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    .line 683
    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    .line 684
    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    .line 685
    add-int/2addr v1, v5

    div-int/lit8 v1, v1, 0x2

    .line 686
    add-int/2addr v3, v6

    div-int/lit8 v3, v3, 0x2

    .line 687
    sub-int v5, v1, v4

    sub-int v6, v3, v4

    add-int/2addr v1, v4

    add-int/2addr v3, v4

    invoke-static {v2, v5, v6, v1, v3}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;IIII)V

    .line 691
    :cond_3f
    return v0
.end method
