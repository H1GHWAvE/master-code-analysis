.class public Landroid/support/v7/widget/aj;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# static fields
.field public static final h:I = 0x0

.field public static final i:I = 0x1

.field public static final j:I = 0x0

.field public static final k:I = 0x1

.field public static final l:I = 0x2

.field public static final m:I = 0x4

.field private static final q:I = 0x4

.field private static final r:I = 0x0

.field private static final s:I = 0x1

.field private static final t:I = 0x2

.field private static final u:I = 0x3


# instance fields
.field private a:Z

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:F

.field private n:Z

.field private o:[I

.field private p:[I

.field private v:Landroid/graphics/drawable/Drawable;

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 142
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/aj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 143
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/aj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 147
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 10

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 150
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 97
    iput-boolean v2, p0, Landroid/support/v7/widget/aj;->a:Z

    .line 106
    iput v4, p0, Landroid/support/v7/widget/aj;->b:I

    .line 113
    iput v5, p0, Landroid/support/v7/widget/aj;->c:I

    .line 117
    const v0, 0x800033

    iput v0, p0, Landroid/support/v7/widget/aj;->e:I

    .line 152
    sget-object v0, Landroid/support/v7/a/n;->LinearLayoutCompat:[I

    invoke-static {p1, p2, v0, p3}, Landroid/support/v7/internal/widget/ax;->a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;

    move-result-object v0

    .line 155
    sget v1, Landroid/support/v7/a/n;->LinearLayoutCompat_android_orientation:I

    invoke-virtual {v0, v1, v4}, Landroid/support/v7/internal/widget/ax;->a(II)I

    move-result v1

    .line 156
    if-ltz v1, :cond_22

    .line 157
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/aj;->setOrientation(I)V

    .line 160
    :cond_22
    sget v1, Landroid/support/v7/a/n;->LinearLayoutCompat_android_gravity:I

    invoke-virtual {v0, v1, v4}, Landroid/support/v7/internal/widget/ax;->a(II)I

    move-result v1

    .line 161
    if-ltz v1, :cond_2d

    .line 162
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/aj;->setGravity(I)V

    .line 165
    :cond_2d
    sget v1, Landroid/support/v7/a/n;->LinearLayoutCompat_android_baselineAligned:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/internal/widget/ax;->a(IZ)Z

    move-result v1

    .line 166
    if-nez v1, :cond_38

    .line 167
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/aj;->setBaselineAligned(Z)V

    .line 170
    :cond_38
    sget v1, Landroid/support/v7/a/n;->LinearLayoutCompat_android_weightSum:I

    .line 2115
    iget-object v2, v0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v2, v1, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    .line 170
    iput v1, p0, Landroid/support/v7/widget/aj;->g:F

    .line 172
    sget v1, Landroid/support/v7/a/n;->LinearLayoutCompat_android_baselineAlignedChildIndex:I

    invoke-virtual {v0, v1, v4}, Landroid/support/v7/internal/widget/ax;->a(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/aj;->b:I

    .line 175
    sget v1, Landroid/support/v7/a/n;->LinearLayoutCompat_measureWithLargestChild:I

    invoke-virtual {v0, v1, v5}, Landroid/support/v7/internal/widget/ax;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/widget/aj;->n:Z

    .line 177
    sget v1, Landroid/support/v7/a/n;->LinearLayoutCompat_divider:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/aj;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 178
    sget v1, Landroid/support/v7/a/n;->LinearLayoutCompat_showDividers:I

    invoke-virtual {v0, v1, v5}, Landroid/support/v7/internal/widget/ax;->a(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/aj;->y:I

    .line 179
    sget v1, Landroid/support/v7/a/n;->LinearLayoutCompat_dividerPadding:I

    invoke-virtual {v0, v1, v5}, Landroid/support/v7/internal/widget/ax;->c(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/aj;->z:I

    .line 2183
    iget-object v0, v0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 182
    return-void
.end method

.method private a(I)Landroid/view/View;
    .registers 3

    .prologue
    .line 501
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(II)V
    .registers 30

    .prologue
    .line 590
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 591
    const/16 v18, 0x0

    .line 592
    const/16 v17, 0x0

    .line 593
    const/16 v16, 0x0

    .line 594
    const/4 v15, 0x0

    .line 595
    const/4 v14, 0x1

    .line 596
    const/4 v5, 0x0

    .line 598
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getVirtualChildCount()I

    move-result v21

    .line 600
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v22

    .line 601
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v23

    .line 603
    const/4 v10, 0x0

    .line 604
    const/4 v12, 0x0

    .line 606
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/aj;->b:I

    move/from16 v24, v0

    .line 607
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v7/widget/aj;->n:Z

    move/from16 v25, v0

    .line 609
    const/high16 v11, -0x80000000

    .line 612
    const/16 v19, 0x0

    :goto_2c
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_185

    .line 14501
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 615
    if-nez v4, :cond_4b

    .line 616
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    add-int/lit8 v3, v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    move/from16 v3, v19

    .line 612
    :goto_48
    add-int/lit8 v19, v3, 0x1

    goto :goto_2c

    .line 620
    :cond_4b
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v6, 0x8

    if-eq v3, v6, :cond_3bf

    .line 625
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v3

    if-eqz v3, :cond_6a

    .line 626
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/aj;->x:I

    add-int/2addr v3, v6

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 629
    :cond_6a
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Landroid/support/v7/widget/al;

    .line 631
    iget v3, v9, Landroid/support/v7/widget/al;->g:F

    add-float v13, v5, v3

    .line 633
    const/high16 v3, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-ne v0, v3, :cond_c0

    iget v3, v9, Landroid/support/v7/widget/al;->height:I

    if-nez v3, :cond_c0

    iget v3, v9, Landroid/support/v7/widget/al;->g:F

    const/4 v5, 0x0

    cmpl-float v3, v3, v5

    if-lez v3, :cond_c0

    .line 637
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 638
    iget v5, v9, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v5, v3

    iget v6, v9, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v5, v6

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 639
    const/4 v12, 0x1

    move v8, v11

    move v11, v12

    .line 678
    :goto_9b
    if-ltz v24, :cond_ab

    add-int/lit8 v3, v19, 0x1

    move/from16 v0, v24

    if-ne v0, v3, :cond_ab

    .line 679
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->c:I

    .line 685
    :cond_ab
    move/from16 v0, v19

    move/from16 v1, v24

    if-ge v0, v1, :cond_115

    iget v3, v9, Landroid/support/v7/widget/al;->g:F

    const/4 v5, 0x0

    cmpl-float v3, v3, v5

    if-lez v3, :cond_115

    .line 686
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won\'t work.  Either remove the weight, or don\'t set mBaselineAlignedChildIndex."

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 641
    :cond_c0
    const/high16 v3, -0x80000000

    .line 643
    iget v5, v9, Landroid/support/v7/widget/al;->height:I

    if-nez v5, :cond_d1

    iget v5, v9, Landroid/support/v7/widget/al;->g:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_d1

    .line 648
    const/4 v3, 0x0

    .line 649
    const/4 v5, -0x2

    iput v5, v9, Landroid/support/v7/widget/al;->height:I

    :cond_d1
    move/from16 v20, v3

    .line 656
    const/4 v6, 0x0

    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-nez v3, :cond_113

    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/v7/widget/aj;->f:I

    :goto_dd
    move-object/from16 v3, p0

    move/from16 v5, p1

    move/from16 v7, p2

    invoke-direct/range {v3 .. v8}, Landroid/support/v7/widget/aj;->a(Landroid/view/View;IIII)V

    .line 660
    const/high16 v3, -0x80000000

    move/from16 v0, v20

    if-eq v0, v3, :cond_f0

    .line 661
    move/from16 v0, v20

    iput v0, v9, Landroid/support/v7/widget/al;->height:I

    .line 664
    :cond_f0
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 665
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/aj;->f:I

    .line 666
    add-int v6, v5, v3

    iget v7, v9, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v6, v7

    iget v7, v9, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x0

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Landroid/support/v7/widget/aj;->f:I

    .line 669
    if-eqz v25, :cond_3bb

    .line 670
    invoke-static {v3, v11}, Ljava/lang/Math;->max(II)I

    move-result v11

    move v8, v11

    move v11, v12

    goto :goto_9b

    .line 656
    :cond_113
    const/4 v8, 0x0

    goto :goto_dd

    .line 692
    :cond_115
    const/4 v3, 0x0

    .line 693
    const/high16 v5, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v5, :cond_3b8

    iget v5, v9, Landroid/support/v7/widget/al;->width:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_3b8

    .line 698
    const/4 v5, 0x1

    .line 699
    const/4 v3, 0x1

    .line 702
    :goto_123
    iget v6, v9, Landroid/support/v7/widget/al;->leftMargin:I

    iget v7, v9, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v6, v7

    .line 703
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v6

    .line 704
    move/from16 v0, v18

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 705
    invoke-static {v4}, Landroid/support/v4/view/cx;->j(Landroid/view/View;)I

    move-result v4

    move/from16 v0, v17

    invoke-static {v0, v4}, Landroid/support/v7/internal/widget/bd;->a(II)I

    move-result v10

    .line 708
    if-eqz v14, :cond_16f

    iget v4, v9, Landroid/support/v7/widget/al;->width:I

    const/4 v14, -0x1

    if-ne v4, v14, :cond_16f

    const/4 v4, 0x1

    .line 709
    :goto_145
    iget v9, v9, Landroid/support/v7/widget/al;->g:F

    const/4 v14, 0x0

    cmpl-float v9, v9, v14

    if-lez v9, :cond_173

    .line 714
    if-eqz v3, :cond_171

    move v3, v6

    :goto_14f
    invoke-static {v15, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v6, v13

    move v7, v4

    move/from16 v9, v16

    move v4, v11

    move v11, v12

    move/from16 v26, v8

    move v8, v3

    move/from16 v3, v26

    .line 721
    :goto_15e
    add-int/lit8 v12, v19, 0x0

    move v14, v7

    move v15, v8

    move/from16 v16, v9

    move/from16 v17, v10

    move/from16 v18, v11

    move v11, v3

    move v10, v5

    move v3, v12

    move v5, v6

    move v12, v4

    goto/16 :goto_48

    .line 708
    :cond_16f
    const/4 v4, 0x0

    goto :goto_145

    :cond_171
    move v3, v7

    .line 714
    goto :goto_14f

    .line 717
    :cond_173
    if-eqz v3, :cond_183

    :goto_175
    move/from16 v0, v16

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v6, v13

    move v7, v4

    move v9, v3

    move v4, v11

    move v3, v8

    move v8, v15

    move v11, v12

    goto :goto_15e

    :cond_183
    move v6, v7

    goto :goto_175

    .line 724
    :cond_185
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    if-lez v3, :cond_1a2

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v3

    if-eqz v3, :cond_1a2

    .line 725
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->x:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 728
    :cond_1a2
    if-eqz v25, :cond_1f5

    const/high16 v3, -0x80000000

    move/from16 v0, v23

    if-eq v0, v3, :cond_1ac

    if-nez v23, :cond_1f5

    .line 730
    :cond_1ac
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 732
    const/4 v4, 0x0

    :goto_1b2
    move/from16 v0, v21

    if-ge v4, v0, :cond_1f5

    .line 15501
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 735
    if-nez v3, :cond_1cc

    .line 736
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    add-int/lit8 v3, v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    move v3, v4

    .line 732
    :goto_1c9
    add-int/lit8 v4, v3, 0x1

    goto :goto_1b2

    .line 740
    :cond_1cc
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-ne v6, v7, :cond_1d7

    .line 741
    add-int/lit8 v3, v4, 0x0

    .line 742
    goto :goto_1c9

    .line 745
    :cond_1d7
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/al;

    .line 748
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/aj;->f:I

    .line 749
    add-int v7, v6, v11

    iget v8, v3, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v7, v8

    iget v3, v3, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v3, v7

    add-int/lit8 v3, v3, 0x0

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    move v3, v4

    goto :goto_1c9

    .line 755
    :cond_1f5
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v6

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 757
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 760
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getSuggestedMinimumHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 763
    const/4 v4, 0x0

    move/from16 v0, p2

    invoke-static {v3, v0, v4}, Landroid/support/v4/view/cx;->a(III)I

    move-result v19

    .line 764
    const v3, 0xffffff

    and-int v3, v3, v19

    .line 769
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->f:I

    sub-int v6, v3, v4

    .line 770
    if-nez v12, :cond_22e

    if-eqz v6, :cond_35c

    const/4 v3, 0x0

    cmpl-float v3, v5, v3

    if-lez v3, :cond_35c

    .line 771
    :cond_22e
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->g:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_23b

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/aj;->g:F

    .line 773
    :cond_23b
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 775
    const/4 v3, 0x0

    move v15, v3

    move v12, v14

    move/from16 v13, v16

    move/from16 v11, v17

    move/from16 v14, v18

    :goto_249
    move/from16 v0, v21

    if-ge v15, v0, :cond_310

    .line 16501
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 778
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_3b0

    .line 782
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/al;

    .line 784
    iget v8, v3, Landroid/support/v7/widget/al;->g:F

    .line 785
    const/4 v4, 0x0

    cmpl-float v4, v8, v4

    if-lez v4, :cond_3ab

    .line 787
    int-to-float v4, v6

    mul-float/2addr v4, v8

    div-float/2addr v4, v5

    float-to-int v4, v4

    .line 788
    sub-float v8, v5, v8

    .line 789
    sub-int v9, v6, v4

    .line 791
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, v3, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v5, v6

    iget v6, v3, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v5, v6

    iget v6, v3, Landroid/support/v7/widget/al;->width:I

    move/from16 v0, p1

    invoke-static {v0, v5, v6}, Landroid/support/v7/widget/aj;->getChildMeasureSpec(III)I

    move-result v5

    .line 797
    iget v6, v3, Landroid/support/v7/widget/al;->height:I

    if-nez v6, :cond_291

    const/high16 v6, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v6, :cond_303

    .line 800
    :cond_291
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v4, v6

    .line 801
    if-gez v4, :cond_299

    .line 802
    const/4 v4, 0x0

    :cond_299
    move-object v6, v7

    .line 810
    :goto_29a
    const/high16 v16, 0x40000000    # 2.0f

    move/from16 v0, v16

    invoke-static {v4, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v6, v5, v4}, Landroid/view/View;->measure(II)V

    .line 816
    invoke-static {v7}, Landroid/support/v4/view/cx;->j(Landroid/view/View;)I

    move-result v4

    and-int/lit16 v4, v4, -0x100

    invoke-static {v11, v4}, Landroid/support/v7/internal/widget/bd;->a(II)I

    move-result v4

    move v5, v9

    move v6, v4

    move v4, v8

    .line 821
    :goto_2b2
    iget v8, v3, Landroid/support/v7/widget/al;->leftMargin:I

    iget v9, v3, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v8, v9

    .line 822
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v8

    .line 823
    invoke-static {v14, v9}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 825
    const/high16 v14, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v14, :cond_30a

    iget v14, v3, Landroid/support/v7/widget/al;->width:I

    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v14, v0, :cond_30a

    const/4 v14, 0x1

    .line 828
    :goto_2cf
    if-eqz v14, :cond_30c

    :goto_2d1
    invoke-static {v13, v8}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 831
    if-eqz v12, :cond_30e

    iget v8, v3, Landroid/support/v7/widget/al;->width:I

    const/4 v12, -0x1

    if-ne v8, v12, :cond_30e

    const/4 v8, 0x1

    .line 833
    :goto_2dd
    move-object/from16 v0, p0

    iget v12, v0, Landroid/support/v7/widget/aj;->f:I

    .line 834
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v12

    iget v13, v3, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v7, v13

    iget v3, v3, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v3, v7

    add-int/lit8 v3, v3, 0x0

    invoke-static {v12, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    move v3, v9

    move v7, v11

    .line 775
    :goto_2f8
    add-int/lit8 v9, v15, 0x1

    move v15, v9

    move v12, v8

    move v13, v3

    move v11, v6

    move v14, v7

    move v6, v5

    move v5, v4

    goto/16 :goto_249

    .line 810
    :cond_303
    if-lez v4, :cond_307

    move-object v6, v7

    goto :goto_29a

    :cond_307
    const/4 v4, 0x0

    move-object v6, v7

    goto :goto_29a

    .line 825
    :cond_30a
    const/4 v14, 0x0

    goto :goto_2cf

    :cond_30c
    move v8, v9

    .line 828
    goto :goto_2d1

    .line 831
    :cond_30e
    const/4 v8, 0x0

    goto :goto_2dd

    .line 839
    :cond_310
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    move v3, v13

    move/from16 v17, v11

    move v4, v14

    move v14, v12

    .line 871
    :goto_327
    if-nez v14, :cond_3a4

    const/high16 v5, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v5, :cond_3a4

    .line 875
    :goto_32f
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 878
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getSuggestedMinimumWidth()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 880
    move/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v3, v0, v1}, Landroid/support/v4/view/cx;->a(III)I

    move-result v3

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Landroid/support/v7/widget/aj;->setMeasuredDimension(II)V

    .line 883
    if-eqz v10, :cond_35b

    .line 884
    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, p2

    invoke-direct {v0, v1, v2}, Landroid/support/v7/widget/aj;->b(II)V

    .line 886
    :cond_35b
    return-void

    .line 842
    :cond_35c
    move/from16 v0, v16

    invoke-static {v0, v15}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 848
    if-eqz v25, :cond_3a6

    const/high16 v3, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v3, :cond_3a6

    .line 849
    const/4 v3, 0x0

    move v4, v3

    :goto_36c
    move/from16 v0, v21

    if-ge v4, v0, :cond_3a6

    .line 17501
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 852
    if-eqz v5, :cond_3a0

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v6, 0x8

    if-eq v3, v6, :cond_3a0

    .line 856
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/al;

    .line 859
    iget v3, v3, Landroid/support/v7/widget/al;->g:F

    .line 860
    const/4 v6, 0x0

    cmpl-float v3, v3, v6

    if-lez v3, :cond_3a0

    .line 861
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v11, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v3, v6}, Landroid/view/View;->measure(II)V

    .line 849
    :cond_3a0
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_36c

    :cond_3a4
    move v3, v4

    goto :goto_32f

    :cond_3a6
    move v3, v13

    move/from16 v4, v18

    goto/16 :goto_327

    :cond_3ab
    move v4, v5

    move v5, v6

    move v6, v11

    goto/16 :goto_2b2

    :cond_3b0
    move v4, v5

    move v8, v12

    move v3, v13

    move v7, v14

    move v5, v6

    move v6, v11

    goto/16 :goto_2f8

    :cond_3b8
    move v5, v10

    goto/16 :goto_123

    :cond_3bb
    move v8, v11

    move v11, v12

    goto/16 :goto_9b

    :cond_3bf
    move v3, v11

    move v4, v12

    move v6, v5

    move v7, v14

    move v8, v15

    move/from16 v9, v16

    move v5, v10

    move/from16 v11, v18

    move/from16 v10, v17

    goto/16 :goto_15e
.end method

.method private a(IIII)V
    .registers 18

    .prologue
    .line 1422
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v5

    .line 1428
    sub-int v0, p3, p1

    .line 1429
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v1

    sub-int v6, v0, v1

    .line 1432
    sub-int/2addr v0, v5

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v1

    sub-int v7, v0, v1

    .line 1434
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getVirtualChildCount()I

    move-result v8

    .line 1436
    iget v0, p0, Landroid/support/v7/widget/aj;->e:I

    and-int/lit8 v0, v0, 0x70

    .line 1437
    iget v1, p0, Landroid/support/v7/widget/aj;->e:I

    const v2, 0x800007

    and-int/2addr v2, v1

    .line 1439
    sparse-switch v0, :sswitch_data_aa

    .line 1452
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v0

    .line 1456
    :goto_28
    const/4 v4, 0x0

    move v3, v0

    :goto_2a
    if-ge v4, v8, :cond_a7

    .line 28501
    invoke-virtual {p0, v4}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 1458
    if-nez v9, :cond_50

    .line 1459
    add-int/lit8 v3, v3, 0x0

    move v0, v4

    .line 1456
    :goto_35
    add-int/lit8 v4, v0, 0x1

    goto :goto_2a

    .line 1442
    :sswitch_38
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v0

    add-int v0, v0, p4

    sub-int/2addr v0, p2

    iget v1, p0, Landroid/support/v7/widget/aj;->f:I

    sub-int/2addr v0, v1

    .line 1443
    goto :goto_28

    .line 1447
    :sswitch_43
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v0

    sub-int v1, p4, p2

    iget v3, p0, Landroid/support/v7/widget/aj;->f:I

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1448
    goto :goto_28

    .line 1460
    :cond_50
    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_a8

    .line 1461
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    .line 1462
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    .line 1464
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/al;

    .line 1467
    iget v1, v0, Landroid/support/v7/widget/al;->h:I

    .line 1468
    if-gez v1, :cond_6b

    move v1, v2

    .line 1471
    :cond_6b
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v12

    .line 1472
    invoke-static {v1, v12}, Landroid/support/v4/view/v;->a(II)I

    move-result v1

    .line 1474
    and-int/lit8 v1, v1, 0x7

    sparse-switch v1, :sswitch_data_b4

    .line 1486
    iget v1, v0, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v1, v5

    .line 1490
    :goto_7b
    invoke-direct {p0, v4}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v12

    if-eqz v12, :cond_84

    .line 1491
    iget v12, p0, Landroid/support/v7/widget/aj;->x:I

    add-int/2addr v3, v12

    .line 1494
    :cond_84
    iget v12, v0, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v3, v12

    .line 1495
    add-int/lit8 v12, v3, 0x0

    invoke-static {v9, v1, v12, v10, v11}, Landroid/support/v7/widget/aj;->b(Landroid/view/View;IIII)V

    .line 1497
    iget v0, v0, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v0, v11

    add-int/lit8 v0, v0, 0x0

    add-int/2addr v3, v0

    .line 1499
    add-int/lit8 v0, v4, 0x0

    goto :goto_35

    .line 1476
    :sswitch_95
    sub-int v1, v7, v10

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v5

    iget v12, v0, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v1, v12

    iget v12, v0, Landroid/support/v7/widget/al;->rightMargin:I

    sub-int/2addr v1, v12

    .line 1478
    goto :goto_7b

    .line 1481
    :sswitch_a1
    sub-int v1, v6, v10

    iget v12, v0, Landroid/support/v7/widget/al;->rightMargin:I

    sub-int/2addr v1, v12

    .line 1482
    goto :goto_7b

    .line 1502
    :cond_a7
    return-void

    :cond_a8
    move v0, v4

    goto :goto_35

    .line 1439
    :sswitch_data_aa
    .sparse-switch
        0x10 -> :sswitch_43
        0x50 -> :sswitch_38
    .end sparse-switch

    .line 1474
    :sswitch_data_b4
    .sparse-switch
        0x1 -> :sswitch_95
        0x5 -> :sswitch_a1
    .end sparse-switch
.end method

.method private a(Landroid/graphics/Canvas;)V
    .registers 7

    .prologue
    .line 291
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getVirtualChildCount()I

    move-result v2

    .line 292
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v2, :cond_34

    .line 6501
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 295
    if-eqz v3, :cond_30

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-eq v0, v4, :cond_30

    .line 296
    invoke-direct {p0, v1}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 297
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/al;

    .line 298
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    iget v0, v0, Landroid/support/v7/widget/al;->topMargin:I

    sub-int v0, v3, v0

    iget v3, p0, Landroid/support/v7/widget/aj;->x:I

    sub-int/2addr v0, v3

    .line 299
    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/aj;->a(Landroid/graphics/Canvas;I)V

    .line 292
    :cond_30
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 304
    :cond_34
    invoke-direct {p0, v2}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 305
    add-int/lit8 v0, v2, -0x1

    .line 7501
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 307
    if-nez v1, :cond_52

    .line 308
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/support/v7/widget/aj;->x:I

    sub-int/2addr v0, v1

    .line 313
    :goto_4e
    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/aj;->a(Landroid/graphics/Canvas;I)V

    .line 315
    :cond_51
    return-void

    .line 310
    :cond_52
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/al;

    .line 311
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v0, v1

    goto :goto_4e
.end method

.method private a(Landroid/graphics/Canvas;I)V
    .registers 7

    .prologue
    .line 359
    iget-object v0, p0, Landroid/support/v7/widget/aj;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Landroid/support/v7/widget/aj;->z:I

    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Landroid/support/v7/widget/aj;->z:I

    sub-int/2addr v2, v3

    iget v3, p0, Landroid/support/v7/widget/aj;->x:I

    add-int/2addr v3, p2

    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 361
    iget-object v0, p0, Landroid/support/v7/widget/aj;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 362
    return-void
.end method

.method private a(Landroid/view/View;IIII)V
    .registers 6

    .prologue
    .line 1373
    invoke-virtual/range {p0 .. p5}, Landroid/support/v7/widget/aj;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 1375
    return-void
.end method

.method private a()Z
    .registers 2

    .prologue
    .line 377
    iget-boolean v0, p0, Landroid/support/v7/widget/aj;->a:Z

    return v0
.end method

.method private b(II)V
    .registers 12

    .prologue
    const/4 v3, 0x0

    .line 890
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getMeasuredWidth()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move v7, v3

    .line 892
    :goto_c
    if-ge v7, p1, :cond_3a

    .line 18501
    invoke-virtual {p0, v7}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 894
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-eq v0, v4, :cond_36

    .line 895
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/support/v7/widget/al;

    .line 897
    iget v0, v6, Landroid/support/v7/widget/al;->width:I

    const/4 v4, -0x1

    if-ne v0, v4, :cond_36

    .line 900
    iget v8, v6, Landroid/support/v7/widget/al;->height:I

    .line 901
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, v6, Landroid/support/v7/widget/al;->height:I

    move-object v0, p0

    move v4, p2

    move v5, v3

    .line 904
    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/aj;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 905
    iput v8, v6, Landroid/support/v7/widget/al;->height:I

    .line 892
    :cond_36
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_c

    .line 909
    :cond_3a
    return-void
.end method

.method private b(IIII)V
    .registers 28

    .prologue
    .line 1517
    invoke-static/range {p0 .. p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v5

    .line 1518
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v8

    .line 1524
    sub-int v3, p4, p2

    .line 1525
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v4

    sub-int v12, v3, v4

    .line 1528
    sub-int/2addr v3, v8

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v4

    sub-int v13, v3, v4

    .line 1530
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getVirtualChildCount()I

    move-result v14

    .line 1532
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->e:I

    const v4, 0x800007

    and-int/2addr v3, v4

    .line 1533
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->e:I

    and-int/lit8 v11, v4, 0x70

    .line 1535
    move-object/from16 v0, p0

    iget-boolean v15, v0, Landroid/support/v7/widget/aj;->a:Z

    .line 1537
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v7/widget/aj;->o:[I

    move-object/from16 v16, v0

    .line 1538
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v7/widget/aj;->p:[I

    move-object/from16 v17, v0

    .line 1540
    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v4

    .line 1541
    invoke-static {v3, v4}, Landroid/support/v4/view/v;->a(II)I

    move-result v3

    sparse-switch v3, :sswitch_data_126

    .line 1554
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v9

    .line 1558
    :goto_48
    const/4 v4, 0x0

    .line 1559
    const/4 v3, 0x1

    .line 1561
    if-eqz v5, :cond_121

    .line 1562
    add-int/lit8 v4, v14, -0x1

    .line 1563
    const/4 v3, -0x1

    move v5, v4

    move v4, v3

    .line 1566
    :goto_51
    const/4 v10, 0x0

    :goto_52
    if-ge v10, v14, :cond_119

    .line 1567
    mul-int v3, v4, v10

    add-int v18, v5, v3

    .line 29501
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    .line 1570
    if-nez v19, :cond_87

    .line 1571
    add-int/lit8 v9, v9, 0x0

    move v3, v10

    .line 1566
    :goto_65
    add-int/lit8 v10, v3, 0x1

    goto :goto_52

    .line 1544
    :sswitch_68
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v3

    add-int v3, v3, p3

    sub-int v3, v3, p1

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->f:I

    sub-int v9, v3, v4

    .line 1545
    goto :goto_48

    .line 1549
    :sswitch_77
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v3

    sub-int v4, p3, p1

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/aj;->f:I

    sub-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x2

    add-int v9, v3, v4

    .line 1550
    goto :goto_48

    .line 1572
    :cond_87
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v6, 0x8

    if-eq v3, v6, :cond_11e

    .line 1573
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredWidth()I

    move-result v20

    .line 1574
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    .line 1575
    const/4 v6, -0x1

    .line 1577
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/al;

    .line 1580
    if-eqz v15, :cond_ac

    iget v7, v3, Landroid/support/v7/widget/al;->height:I

    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v7, v0, :cond_ac

    .line 1581
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getBaseline()I

    move-result v6

    .line 1584
    :cond_ac
    iget v7, v3, Landroid/support/v7/widget/al;->h:I

    .line 1585
    if-gez v7, :cond_b1

    move v7, v11

    .line 1589
    :cond_b1
    and-int/lit8 v7, v7, 0x70

    sparse-switch v7, :sswitch_data_130

    move v6, v8

    .line 1625
    :goto_b7
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v7

    if-eqz v7, :cond_11a

    .line 1626
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/aj;->w:I

    add-int/2addr v7, v9

    .line 1629
    :goto_c6
    iget v9, v3, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v7, v9

    .line 1630
    add-int/lit8 v9, v7, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v0, v9, v6, v1, v2}, Landroid/support/v7/widget/aj;->b(Landroid/view/View;IIII)V

    .line 1632
    iget v3, v3, Landroid/support/v7/widget/al;->rightMargin:I

    add-int v3, v3, v20

    add-int/lit8 v3, v3, 0x0

    add-int v9, v7, v3

    .line 1635
    add-int/lit8 v3, v10, 0x0

    goto :goto_65

    .line 1591
    :sswitch_df
    iget v7, v3, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v7, v8

    .line 1592
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v6, v0, :cond_11c

    .line 1593
    const/16 v22, 0x1

    aget v22, v16, v22

    sub-int v6, v22, v6

    add-int/2addr v6, v7

    goto :goto_b7

    .line 1609
    :sswitch_f0
    sub-int v6, v13, v21

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, v8

    iget v7, v3, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v6, v7

    iget v7, v3, Landroid/support/v7/widget/al;->bottomMargin:I

    sub-int/2addr v6, v7

    .line 1611
    goto :goto_b7

    .line 1614
    :sswitch_fc
    sub-int v7, v12, v21

    iget v0, v3, Landroid/support/v7/widget/al;->bottomMargin:I

    move/from16 v22, v0

    sub-int v7, v7, v22

    .line 1615
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v6, v0, :cond_11c

    .line 1616
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredHeight()I

    move-result v22

    sub-int v6, v22, v6

    .line 1617
    const/16 v22, 0x2

    aget v22, v17, v22

    sub-int v6, v22, v6

    sub-int v6, v7, v6

    .line 1618
    goto :goto_b7

    .line 1638
    :cond_119
    return-void

    :cond_11a
    move v7, v9

    goto :goto_c6

    :cond_11c
    move v6, v7

    goto :goto_b7

    :cond_11e
    move v3, v10

    goto/16 :goto_65

    :cond_121
    move v5, v4

    move v4, v3

    goto/16 :goto_51

    .line 1541
    nop

    :sswitch_data_126
    .sparse-switch
        0x1 -> :sswitch_77
        0x5 -> :sswitch_68
    .end sparse-switch

    .line 1589
    :sswitch_data_130
    .sparse-switch
        0x10 -> :sswitch_f0
        0x30 -> :sswitch_df
        0x50 -> :sswitch_fc
    .end sparse-switch
.end method

.method private b(Landroid/graphics/Canvas;)V
    .registers 8

    .prologue
    .line 318
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getVirtualChildCount()I

    move-result v2

    .line 319
    invoke-static {p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v3

    .line 320
    const/4 v0, 0x0

    move v1, v0

    :goto_a
    if-ge v1, v2, :cond_42

    .line 8501
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 323
    if-eqz v4, :cond_32

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v5, 0x8

    if-eq v0, v5, :cond_32

    .line 324
    invoke-direct {p0, v1}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 325
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/al;

    .line 327
    if-eqz v3, :cond_36

    .line 328
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    iget v0, v0, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v0, v4

    .line 332
    :goto_2f
    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/aj;->b(Landroid/graphics/Canvas;I)V

    .line 320
    :cond_32
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 330
    :cond_36
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    iget v0, v0, Landroid/support/v7/widget/al;->leftMargin:I

    sub-int v0, v4, v0

    iget v4, p0, Landroid/support/v7/widget/aj;->w:I

    sub-int/2addr v0, v4

    goto :goto_2f

    .line 337
    :cond_42
    invoke-direct {p0, v2}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 338
    add-int/lit8 v0, v2, -0x1

    .line 9501
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 340
    if-nez v1, :cond_67

    .line 341
    if-eqz v3, :cond_5a

    .line 342
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v0

    .line 354
    :goto_56
    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/aj;->b(Landroid/graphics/Canvas;I)V

    .line 356
    :cond_59
    return-void

    .line 344
    :cond_5a
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/support/v7/widget/aj;->w:I

    sub-int/2addr v0, v1

    goto :goto_56

    .line 347
    :cond_67
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/al;

    .line 348
    if-eqz v3, :cond_7b

    .line 349
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/al;->leftMargin:I

    sub-int v0, v1, v0

    iget v1, p0, Landroid/support/v7/widget/aj;->w:I

    sub-int/2addr v0, v1

    goto :goto_56

    .line 351
    :cond_7b
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v0, v1

    goto :goto_56
.end method

.method private b(Landroid/graphics/Canvas;I)V
    .registers 8

    .prologue
    .line 365
    iget-object v0, p0, Landroid/support/v7/widget/aj;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v1

    iget v2, p0, Landroid/support/v7/widget/aj;->z:I

    add-int/2addr v1, v2

    iget v2, p0, Landroid/support/v7/widget/aj;->w:I

    add-int/2addr v2, p2

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Landroid/support/v7/widget/aj;->z:I

    sub-int/2addr v3, v4

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 367
    iget-object v0, p0, Landroid/support/v7/widget/aj;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 368
    return-void
.end method

.method private static b(Landroid/view/View;IIII)V
    .registers 7

    .prologue
    .line 1641
    add-int v0, p1, p3

    add-int v1, p2, p4

    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 1642
    return-void
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 400
    iget-boolean v0, p0, Landroid/support/v7/widget/aj;->n:Z

    return v0
.end method

.method private b(I)Z
    .registers 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 561
    if-nez p1, :cond_d

    .line 562
    iget v2, p0, Landroid/support/v7/widget/aj;->y:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_b

    .line 575
    :cond_a
    :goto_a
    return v0

    :cond_b
    move v0, v1

    .line 562
    goto :goto_a

    .line 563
    :cond_d
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getChildCount()I

    move-result v2

    if-ne p1, v2, :cond_1b

    .line 564
    iget v2, p0, Landroid/support/v7/widget/aj;->y:I

    and-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_a

    move v0, v1

    goto :goto_a

    .line 565
    :cond_1b
    iget v2, p0, Landroid/support/v7/widget/aj;->y:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_34

    .line 567
    add-int/lit8 v2, p1, -0x1

    :goto_23
    if-ltz v2, :cond_36

    .line 568
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_a

    .line 567
    add-int/lit8 v2, v2, -0x1

    goto :goto_23

    :cond_34
    move v0, v1

    .line 575
    goto :goto_a

    :cond_36
    move v0, v1

    goto :goto_a
.end method

.method private c(II)V
    .registers 32

    .prologue
    .line 923
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    .line 924
    const/16 v18, 0x0

    .line 925
    const/16 v17, 0x0

    .line 926
    const/16 v16, 0x0

    .line 927
    const/4 v15, 0x0

    .line 928
    const/4 v14, 0x1

    .line 929
    const/4 v4, 0x0

    .line 931
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getVirtualChildCount()I

    move-result v21

    .line 933
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v22

    .line 934
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v23

    .line 936
    const/4 v10, 0x0

    .line 937
    const/4 v12, 0x0

    .line 939
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/aj;->o:[I

    if-eqz v2, :cond_28

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/aj;->p:[I

    if-nez v2, :cond_36

    .line 940
    :cond_28
    const/4 v2, 0x4

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Landroid/support/v7/widget/aj;->o:[I

    .line 941
    const/4 v2, 0x4

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Landroid/support/v7/widget/aj;->p:[I

    .line 944
    :cond_36
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v7/widget/aj;->o:[I

    move-object/from16 v24, v0

    .line 945
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v7/widget/aj;->p:[I

    move-object/from16 v25, v0

    .line 947
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, -0x1

    aput v7, v24, v6

    aput v7, v24, v5

    aput v7, v24, v3

    aput v7, v24, v2

    .line 948
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, -0x1

    aput v7, v25, v6

    aput v7, v25, v5

    aput v7, v25, v3

    aput v7, v25, v2

    .line 950
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v7/widget/aj;->a:Z

    move/from16 v26, v0

    .line 951
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v7/widget/aj;->n:Z

    move/from16 v27, v0

    .line 953
    const/high16 v2, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-ne v0, v2, :cond_93

    const/4 v2, 0x1

    move v9, v2

    .line 955
    :goto_70
    const/high16 v11, -0x80000000

    .line 958
    const/16 v19, 0x0

    :goto_74
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_217

    .line 19501
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 961
    if-nez v3, :cond_96

    .line 962
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/aj;->f:I

    add-int/lit8 v2, v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    move/from16 v2, v19

    .line 958
    :goto_90
    add-int/lit8 v19, v2, 0x1

    goto :goto_74

    .line 953
    :cond_93
    const/4 v2, 0x0

    move v9, v2

    goto :goto_70

    .line 966
    :cond_96
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v5, 0x8

    if-eq v2, v5, :cond_5a8

    .line 971
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v2

    if-eqz v2, :cond_b5

    .line 972
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/aj;->f:I

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/aj;->w:I

    add-int/2addr v2, v5

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    .line 975
    :cond_b5
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Landroid/support/v7/widget/al;

    .line 978
    iget v2, v8, Landroid/support/v7/widget/al;->g:F

    add-float v13, v4, v2

    .line 980
    const/high16 v2, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-ne v0, v2, :cond_190

    iget v2, v8, Landroid/support/v7/widget/al;->width:I

    if-nez v2, :cond_190

    iget v2, v8, Landroid/support/v7/widget/al;->g:F

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-lez v2, :cond_190

    .line 984
    if-eqz v9, :cond_177

    .line 985
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/aj;->f:I

    iget v4, v8, Landroid/support/v7/widget/al;->leftMargin:I

    iget v5, v8, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    .line 997
    :goto_e1
    if-eqz v26, :cond_18b

    .line 998
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 999
    invoke-virtual {v3, v2, v2}, Landroid/view/View;->measure(II)V

    move v7, v11

    move v11, v12

    .line 1042
    :goto_ee
    const/4 v2, 0x0

    .line 1043
    const/high16 v4, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v4, :cond_5a1

    iget v4, v8, Landroid/support/v7/widget/al;->height:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_5a1

    .line 1047
    const/4 v4, 0x1

    .line 1048
    const/4 v2, 0x1

    .line 1051
    :goto_fc
    iget v5, v8, Landroid/support/v7/widget/al;->topMargin:I

    iget v6, v8, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v5, v6

    .line 1052
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v5

    .line 1053
    invoke-static {v3}, Landroid/support/v4/view/cx;->j(Landroid/view/View;)I

    move-result v10

    move/from16 v0, v17

    invoke-static {v0, v10}, Landroid/support/v7/internal/widget/bd;->a(II)I

    move-result v10

    .line 1056
    if-eqz v26, :cond_13f

    .line 1057
    invoke-virtual {v3}, Landroid/view/View;->getBaseline()I

    move-result v12

    .line 1058
    const/4 v3, -0x1

    if-eq v12, v3, :cond_13f

    .line 1061
    iget v3, v8, Landroid/support/v7/widget/al;->h:I

    if-gez v3, :cond_1fa

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->e:I

    :goto_121
    and-int/lit8 v3, v3, 0x70

    .line 1063
    shr-int/lit8 v3, v3, 0x4

    and-int/lit8 v3, v3, -0x2

    shr-int/lit8 v3, v3, 0x1

    .line 1066
    aget v17, v24, v3

    move/from16 v0, v17

    invoke-static {v0, v12}, Ljava/lang/Math;->max(II)I

    move-result v17

    aput v17, v24, v3

    .line 1067
    aget v17, v25, v3

    sub-int v12, v6, v12

    move/from16 v0, v17

    invoke-static {v0, v12}, Ljava/lang/Math;->max(II)I

    move-result v12

    aput v12, v25, v3

    .line 1071
    :cond_13f
    move/from16 v0, v18

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 1073
    if-eqz v14, :cond_1fe

    iget v3, v8, Landroid/support/v7/widget/al;->height:I

    const/4 v14, -0x1

    if-ne v3, v14, :cond_1fe

    const/4 v3, 0x1

    .line 1074
    :goto_14d
    iget v8, v8, Landroid/support/v7/widget/al;->g:F

    const/4 v14, 0x0

    cmpl-float v8, v8, v14

    if-lez v8, :cond_204

    .line 1079
    if-eqz v2, :cond_201

    move v2, v5

    :goto_157
    invoke-static {v15, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v5, v13

    move v6, v3

    move/from16 v8, v16

    move v3, v11

    move v11, v12

    move/from16 v28, v7

    move v7, v2

    move/from16 v2, v28

    .line 1086
    :goto_166
    add-int/lit8 v12, v19, 0x0

    move v14, v6

    move v15, v7

    move/from16 v16, v8

    move/from16 v17, v10

    move/from16 v18, v11

    move v11, v2

    move v10, v4

    move v2, v12

    move v4, v5

    move v12, v3

    goto/16 :goto_90

    .line 987
    :cond_177
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/aj;->f:I

    .line 988
    iget v4, v8, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v4, v2

    iget v5, v8, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    goto/16 :goto_e1

    .line 1001
    :cond_18b
    const/4 v12, 0x1

    move v7, v11

    move v11, v12

    goto/16 :goto_ee

    .line 1004
    :cond_190
    const/high16 v2, -0x80000000

    .line 1006
    iget v4, v8, Landroid/support/v7/widget/al;->width:I

    if-nez v4, :cond_1a1

    iget v4, v8, Landroid/support/v7/widget/al;->g:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1a1

    .line 1011
    const/4 v2, 0x0

    .line 1012
    const/4 v4, -0x2

    iput v4, v8, Landroid/support/v7/widget/al;->width:I

    :cond_1a1
    move/from16 v20, v2

    .line 1019
    const/4 v2, 0x0

    cmpl-float v2, v13, v2

    if-nez v2, :cond_1e1

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/aj;->f:I

    :goto_1ac
    const/4 v7, 0x0

    move-object/from16 v2, p0

    move/from16 v4, p1

    move/from16 v6, p2

    invoke-direct/range {v2 .. v7}, Landroid/support/v7/widget/aj;->a(Landroid/view/View;IIII)V

    .line 1023
    const/high16 v2, -0x80000000

    move/from16 v0, v20

    if-eq v0, v2, :cond_1c0

    .line 1024
    move/from16 v0, v20

    iput v0, v8, Landroid/support/v7/widget/al;->width:I

    .line 1027
    :cond_1c0
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 1028
    if-eqz v9, :cond_1e3

    .line 1029
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->f:I

    iget v5, v8, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v5, v2

    iget v6, v8, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x0

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Landroid/support/v7/widget/aj;->f:I

    .line 1037
    :goto_1d7
    if-eqz v27, :cond_5a4

    .line 1038
    invoke-static {v2, v11}, Ljava/lang/Math;->max(II)I

    move-result v11

    move v7, v11

    move v11, v12

    goto/16 :goto_ee

    .line 1019
    :cond_1e1
    const/4 v5, 0x0

    goto :goto_1ac

    .line 1032
    :cond_1e3
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->f:I

    .line 1033
    add-int v5, v4, v2

    iget v6, v8, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v5, v6

    iget v6, v8, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Landroid/support/v7/widget/aj;->f:I

    goto :goto_1d7

    .line 1061
    :cond_1fa
    iget v3, v8, Landroid/support/v7/widget/al;->h:I

    goto/16 :goto_121

    .line 1073
    :cond_1fe
    const/4 v3, 0x0

    goto/16 :goto_14d

    :cond_201
    move v2, v6

    .line 1079
    goto/16 :goto_157

    .line 1082
    :cond_204
    if-eqz v2, :cond_215

    :goto_206
    move/from16 v0, v16

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v5, v13

    move v6, v3

    move v8, v2

    move v3, v11

    move v2, v7

    move v7, v15

    move v11, v12

    goto/16 :goto_166

    :cond_215
    move v5, v6

    goto :goto_206

    .line 1089
    :cond_217
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/aj;->f:I

    if-lez v2, :cond_234

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v2

    if-eqz v2, :cond_234

    .line 1090
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/aj;->f:I

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->w:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    .line 1095
    :cond_234
    const/4 v2, 0x1

    aget v2, v24, v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_24c

    const/4 v2, 0x0

    aget v2, v24, v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_24c

    const/4 v2, 0x2

    aget v2, v24, v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_24c

    const/4 v2, 0x3

    aget v2, v24, v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_59d

    .line 1099
    :cond_24c
    const/4 v2, 0x3

    aget v2, v24, v2

    const/4 v3, 0x0

    aget v3, v24, v3

    const/4 v5, 0x1

    aget v5, v24, v5

    const/4 v6, 0x2

    aget v6, v24, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1102
    const/4 v3, 0x3

    aget v3, v25, v3

    const/4 v5, 0x0

    aget v5, v25, v5

    const/4 v6, 0x1

    aget v6, v25, v6

    const/4 v7, 0x2

    aget v7, v25, v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1105
    add-int/2addr v2, v3

    move/from16 v0, v18

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1108
    :goto_283
    if-eqz v27, :cond_2eb

    const/high16 v2, -0x80000000

    move/from16 v0, v22

    if-eq v0, v2, :cond_28d

    if-nez v22, :cond_2eb

    .line 1110
    :cond_28d
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    .line 1112
    const/4 v3, 0x0

    :goto_293
    move/from16 v0, v21

    if-ge v3, v0, :cond_2eb

    .line 20501
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1115
    if-nez v2, :cond_2ad

    .line 1116
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/aj;->f:I

    add-int/lit8 v2, v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    move v2, v3

    .line 1112
    :goto_2aa
    add-int/lit8 v3, v2, 0x1

    goto :goto_293

    .line 1120
    :cond_2ad
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-ne v6, v7, :cond_2b8

    .line 1121
    add-int/lit8 v2, v3, 0x0

    .line 1122
    goto :goto_2aa

    .line 1125
    :cond_2b8
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/al;

    .line 1127
    if-eqz v9, :cond_2d3

    .line 1128
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/aj;->f:I

    iget v7, v2, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v7, v11

    iget v2, v2, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v2, v7

    add-int/lit8 v2, v2, 0x0

    add-int/2addr v2, v6

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    move v2, v3

    goto :goto_2aa

    .line 1131
    :cond_2d3
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/aj;->f:I

    .line 1132
    add-int v7, v6, v11

    iget v8, v2, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v7, v8

    iget v2, v2, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v2, v7

    add-int/lit8 v2, v2, 0x0

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    move v2, v3

    goto :goto_2aa

    .line 1139
    :cond_2eb
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/aj;->f:I

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v6

    add-int/2addr v3, v6

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    .line 1141
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/aj;->f:I

    .line 1144
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getSuggestedMinimumWidth()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1147
    const/4 v3, 0x0

    move/from16 v0, p1

    invoke-static {v2, v0, v3}, Landroid/support/v4/view/cx;->a(III)I

    move-result v18

    .line 1148
    const v2, 0xffffff

    and-int v2, v2, v18

    .line 1153
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    sub-int v6, v2, v3

    .line 1154
    if-nez v12, :cond_324

    if-eqz v6, :cond_540

    const/4 v2, 0x0

    cmpl-float v2, v4, v2

    if-lez v2, :cond_540

    .line 1155
    :cond_324
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/aj;->g:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_331

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->g:F

    .line 1157
    :cond_331
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v7, 0x3

    const/4 v8, -0x1

    aput v8, v24, v7

    aput v8, v24, v5

    aput v8, v24, v3

    aput v8, v24, v2

    .line 1158
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v7, 0x3

    const/4 v8, -0x1

    aput v8, v25, v7

    aput v8, v25, v5

    aput v8, v25, v3

    aput v8, v25, v2

    .line 1159
    const/4 v7, -0x1

    .line 1161
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    .line 1163
    const/4 v2, 0x0

    move v15, v2

    move v11, v14

    move/from16 v12, v16

    move v14, v7

    move/from16 v7, v17

    :goto_359
    move/from16 v0, v21

    if-ge v15, v0, :cond_46c

    .line 21501
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1166
    if-eqz v5, :cond_595

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_595

    .line 1170
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/al;

    .line 1173
    iget v8, v2, Landroid/support/v7/widget/al;->g:F

    .line 1174
    const/4 v3, 0x0

    cmpl-float v3, v8, v3

    if-lez v3, :cond_590

    .line 1176
    int-to-float v3, v6

    mul-float/2addr v3, v8

    div-float/2addr v3, v4

    float-to-int v3, v3

    .line 1177
    sub-float v8, v4, v8

    .line 1178
    sub-int/2addr v6, v3

    .line 1180
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v13

    add-int/2addr v4, v13

    iget v13, v2, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v4, v13

    iget v13, v2, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v4, v13

    iget v13, v2, Landroid/support/v7/widget/al;->height:I

    move/from16 v0, p2

    invoke-static {v0, v4, v13}, Landroid/support/v7/widget/aj;->getChildMeasureSpec(III)I

    move-result v13

    .line 1187
    iget v4, v2, Landroid/support/v7/widget/al;->width:I

    if-nez v4, :cond_3a2

    const/high16 v4, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v4, :cond_43f

    .line 1190
    :cond_3a2
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    .line 1191
    if-gez v3, :cond_3aa

    .line 1192
    const/4 v3, 0x0

    :cond_3aa
    move-object v4, v5

    .line 1200
    :goto_3ab
    const/high16 v16, 0x40000000    # 2.0f

    move/from16 v0, v16

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v4, v3, v13}, Landroid/view/View;->measure(II)V

    .line 1206
    invoke-static {v5}, Landroid/support/v4/view/cx;->j(Landroid/view/View;)I

    move-result v3

    const/high16 v4, -0x1000000

    and-int/2addr v3, v4

    invoke-static {v7, v3}, Landroid/support/v7/internal/widget/bd;->a(II)I

    move-result v13

    move v7, v8

    move v8, v6

    .line 1210
    :goto_3c3
    if-eqz v9, :cond_448

    .line 1211
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    iget v6, v2, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v4, v6

    iget v6, v2, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v4, v6

    add-int/lit8 v4, v4, 0x0

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 1219
    :goto_3da
    const/high16 v3, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v3, :cond_463

    iget v3, v2, Landroid/support/v7/widget/al;->height:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_463

    const/4 v3, 0x1

    .line 1222
    :goto_3e6
    iget v4, v2, Landroid/support/v7/widget/al;->topMargin:I

    iget v6, v2, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v4, v6

    .line 1223
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v4

    .line 1224
    invoke-static {v14, v6}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 1225
    if-eqz v3, :cond_465

    move v3, v4

    :goto_3f7
    invoke-static {v12, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1228
    if-eqz v11, :cond_467

    iget v3, v2, Landroid/support/v7/widget/al;->height:I

    const/4 v11, -0x1

    if-ne v3, v11, :cond_467

    const/4 v3, 0x1

    .line 1230
    :goto_403
    if-eqz v26, :cond_42e

    .line 1231
    invoke-virtual {v5}, Landroid/view/View;->getBaseline()I

    move-result v5

    .line 1232
    const/4 v11, -0x1

    if-eq v5, v11, :cond_42e

    .line 1234
    iget v11, v2, Landroid/support/v7/widget/al;->h:I

    if-gez v11, :cond_469

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/aj;->e:I

    :goto_414
    and-int/lit8 v2, v2, 0x70

    .line 1236
    shr-int/lit8 v2, v2, 0x4

    and-int/lit8 v2, v2, -0x2

    shr-int/lit8 v2, v2, 0x1

    .line 1239
    aget v11, v24, v2

    invoke-static {v11, v5}, Ljava/lang/Math;->max(II)I

    move-result v11

    aput v11, v24, v2

    .line 1240
    aget v11, v25, v2

    sub-int v5, v6, v5

    invoke-static {v11, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    aput v5, v25, v2

    :cond_42e
    move v2, v7

    move v5, v4

    move v6, v13

    move v4, v3

    move v7, v14

    move v3, v8

    .line 1163
    :goto_434
    add-int/lit8 v8, v15, 0x1

    move v15, v8

    move v11, v4

    move v12, v5

    move v14, v7

    move v7, v6

    move v4, v2

    move v6, v3

    goto/16 :goto_359

    .line 1200
    :cond_43f
    if-lez v3, :cond_444

    move-object v4, v5

    goto/16 :goto_3ab

    :cond_444
    const/4 v3, 0x0

    move-object v4, v5

    goto/16 :goto_3ab

    .line 1214
    :cond_448
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 1215
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v3

    iget v6, v2, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v4, v6

    iget v6, v2, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v4, v6

    add-int/lit8 v4, v4, 0x0

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    goto/16 :goto_3da

    .line 1219
    :cond_463
    const/4 v3, 0x0

    goto :goto_3e6

    :cond_465
    move v3, v6

    .line 1225
    goto :goto_3f7

    .line 1228
    :cond_467
    const/4 v3, 0x0

    goto :goto_403

    .line 1234
    :cond_469
    iget v2, v2, Landroid/support/v7/widget/al;->h:I

    goto :goto_414

    .line 1247
    :cond_46c
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/aj;->f:I

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/aj;->f:I

    .line 1252
    const/4 v2, 0x1

    aget v2, v24, v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_496

    const/4 v2, 0x0

    aget v2, v24, v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_496

    const/4 v2, 0x2

    aget v2, v24, v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_496

    const/4 v2, 0x3

    aget v2, v24, v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4cb

    .line 1256
    :cond_496
    const/4 v2, 0x3

    aget v2, v24, v2

    const/4 v3, 0x0

    aget v3, v24, v3

    const/4 v4, 0x1

    aget v4, v24, v4

    const/4 v5, 0x2

    aget v5, v24, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1259
    const/4 v3, 0x3

    aget v3, v25, v3

    const/4 v4, 0x0

    aget v4, v25, v4

    const/4 v5, 0x1

    aget v5, v25, v5

    const/4 v6, 0x2

    aget v6, v25, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1262
    add-int/2addr v2, v3

    invoke-static {v14, v2}, Ljava/lang/Math;->max(II)I

    move-result v14

    :cond_4cb
    move v2, v12

    move/from16 v17, v7

    move v3, v14

    move v14, v11

    .line 1291
    :goto_4d0
    if-nez v14, :cond_589

    const/high16 v4, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v4, :cond_589

    .line 1295
    :goto_4d8
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 1298
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getSuggestedMinimumHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1300
    const/high16 v3, -0x1000000

    and-int v3, v3, v17

    or-int v3, v3, v18

    shl-int/lit8 v4, v17, 0x10

    move/from16 v0, p2

    invoke-static {v2, v0, v4}, Landroid/support/v4/view/cx;->a(III)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Landroid/support/v7/widget/aj;->setMeasuredDimension(II)V

    .line 1304
    if-eqz v10, :cond_588

    .line 23313
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getMeasuredHeight()I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 23315
    const/4 v2, 0x0

    move v9, v2

    :goto_50b
    move/from16 v0, v21

    if-ge v9, v0, :cond_588

    .line 23501
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 23317
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v4, 0x8

    if-eq v2, v4, :cond_53c

    .line 23318
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Landroid/support/v7/widget/al;

    .line 23320
    iget v2, v8, Landroid/support/v7/widget/al;->height:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_53c

    .line 23323
    iget v10, v8, Landroid/support/v7/widget/al;->width:I

    .line 23324
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iput v2, v8, Landroid/support/v7/widget/al;->width:I

    .line 23327
    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move/from16 v4, p1

    invoke-virtual/range {v2 .. v7}, Landroid/support/v7/widget/aj;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 23328
    iput v10, v8, Landroid/support/v7/widget/al;->width:I

    .line 23315
    :cond_53c
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_50b

    .line 1265
    :cond_540
    move/from16 v0, v16

    invoke-static {v0, v15}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 1269
    if-eqz v27, :cond_58c

    const/high16 v2, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v2, :cond_58c

    .line 1270
    const/4 v2, 0x0

    move v3, v2

    :goto_550
    move/from16 v0, v21

    if-ge v3, v0, :cond_58c

    .line 22501
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1273
    if-eqz v4, :cond_584

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v6, 0x8

    if-eq v2, v6, :cond_584

    .line 1277
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/al;

    .line 1280
    iget v2, v2, Landroid/support/v7/widget/al;->g:F

    .line 1281
    const/4 v6, 0x0

    cmpl-float v2, v2, v6

    if-lez v2, :cond_584

    .line 1282
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v11, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v2, v6}, Landroid/view/View;->measure(II)V

    .line 1270
    :cond_584
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_550

    .line 1307
    :cond_588
    return-void

    :cond_589
    move v2, v3

    goto/16 :goto_4d8

    :cond_58c
    move v2, v12

    move v3, v5

    goto/16 :goto_4d0

    :cond_590
    move v8, v6

    move v13, v7

    move v7, v4

    goto/16 :goto_3c3

    :cond_595
    move v2, v4

    move v3, v6

    move v5, v12

    move v4, v11

    move v6, v7

    move v7, v14

    goto/16 :goto_434

    :cond_59d
    move/from16 v5, v18

    goto/16 :goto_283

    :cond_5a1
    move v4, v10

    goto/16 :goto_fc

    :cond_5a4
    move v7, v11

    move v11, v12

    goto/16 :goto_ee

    :cond_5a8
    move v2, v11

    move v3, v12

    move v5, v4

    move v6, v14

    move v7, v15

    move/from16 v8, v16

    move v4, v10

    move/from16 v11, v18

    move/from16 v10, v17

    goto/16 :goto_166
.end method

.method private static d()I
    .registers 1

    .prologue
    .line 1354
    const/4 v0, 0x0

    return v0
.end method

.method private d(II)V
    .registers 12

    .prologue
    const/4 v3, 0x0

    .line 1313
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getMeasuredHeight()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    move v7, v3

    .line 1315
    :goto_c
    if-ge v7, p1, :cond_3a

    .line 24501
    invoke-virtual {p0, v7}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1317
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_36

    .line 1318
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/support/v7/widget/al;

    .line 1320
    iget v0, v6, Landroid/support/v7/widget/al;->height:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_36

    .line 1323
    iget v8, v6, Landroid/support/v7/widget/al;->width:I

    .line 1324
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, v6, Landroid/support/v7/widget/al;->width:I

    move-object v0, p0

    move v2, p2

    move v5, v3

    .line 1327
    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/aj;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 1328
    iput v8, v6, Landroid/support/v7/widget/al;->width:I

    .line 1315
    :cond_36
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_c

    .line 1332
    :cond_3a
    return-void
.end method

.method private static getChildrenSkipCount$5359dca7()I
    .registers 1

    .prologue
    .line 1343
    const/4 v0, 0x0

    return v0
.end method

.method private static getLocationOffset$3c7ec8d0()I
    .registers 1

    .prologue
    .line 1385
    const/4 v0, 0x0

    return v0
.end method

.method private static getNextLocationOffset$3c7ec8d0()I
    .registers 1

    .prologue
    .line 1397
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/al;
    .registers 4

    .prologue
    .line 1707
    new-instance v0, Landroid/support/v7/widget/al;

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v7/widget/al;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected b(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/al;
    .registers 3

    .prologue
    .line 1730
    new-instance v0, Landroid/support/v7/widget/al;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/al;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected c()Landroid/support/v7/widget/al;
    .registers 4

    .prologue
    const/4 v2, -0x2

    .line 1720
    iget v0, p0, Landroid/support/v7/widget/aj;->d:I

    if-nez v0, :cond_b

    .line 1721
    new-instance v0, Landroid/support/v7/widget/al;

    invoke-direct {v0, v2, v2}, Landroid/support/v7/widget/al;-><init>(II)V

    .line 1725
    :goto_a
    return-object v0

    .line 1722
    :cond_b
    iget v0, p0, Landroid/support/v7/widget/aj;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_17

    .line 1723
    new-instance v0, Landroid/support/v7/widget/al;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2}, Landroid/support/v7/widget/al;-><init>(II)V

    goto :goto_a

    .line 1725
    :cond_17
    const/4 v0, 0x0

    goto :goto_a
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3

    .prologue
    .line 1737
    instance-of v0, p1, Landroid/support/v7/widget/al;

    return v0
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    .prologue
    .line 56
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->c()Landroid/support/v7/widget/al;

    move-result-object v0

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aj;->a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/al;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aj;->b(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/al;

    move-result-object v0

    return-object v0
.end method

.method public getBaseline()I
    .registers 6

    .prologue
    const/4 v0, -0x1

    .line 419
    iget v1, p0, Landroid/support/v7/widget/aj;->b:I

    if-gez v1, :cond_a

    .line 420
    invoke-super {p0}, Landroid/view/ViewGroup;->getBaseline()I

    move-result v0

    .line 467
    :cond_9
    :goto_9
    return v0

    .line 423
    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getChildCount()I

    move-result v1

    iget v2, p0, Landroid/support/v7/widget/aj;->b:I

    if-gt v1, v2, :cond_1a

    .line 424
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 428
    :cond_1a
    iget v1, p0, Landroid/support/v7/widget/aj;->b:I

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 429
    invoke-virtual {v2}, Landroid/view/View;->getBaseline()I

    move-result v3

    .line 431
    if-ne v3, v0, :cond_32

    .line 432
    iget v1, p0, Landroid/support/v7/widget/aj;->b:I

    if-eqz v1, :cond_9

    .line 438
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mBaselineAlignedChildIndex of LinearLayout points to a View that doesn\'t know how to get its baseline."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 448
    :cond_32
    iget v0, p0, Landroid/support/v7/widget/aj;->c:I

    .line 450
    iget v1, p0, Landroid/support/v7/widget/aj;->d:I

    const/4 v4, 0x1

    if-ne v1, v4, :cond_44

    .line 451
    iget v1, p0, Landroid/support/v7/widget/aj;->e:I

    and-int/lit8 v1, v1, 0x70

    .line 452
    const/16 v4, 0x30

    if-eq v1, v4, :cond_44

    .line 453
    sparse-switch v1, :sswitch_data_7e

    :cond_44
    move v1, v0

    .line 466
    :goto_45
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/al;

    .line 467
    iget v0, v0, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    goto :goto_9

    .line 455
    :sswitch_50
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/support/v7/widget/aj;->f:I

    sub-int/2addr v0, v1

    move v1, v0

    .line 456
    goto :goto_45

    .line 459
    :sswitch_63
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getBottom()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getTop()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v1, v4

    iget v4, p0, Landroid/support/v7/widget/aj;->f:I

    sub-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_45

    .line 453
    :sswitch_data_7e
    .sparse-switch
        0x10 -> :sswitch_63
        0x50 -> :sswitch_50
    .end sparse-switch
.end method

.method public getBaselineAlignedChildIndex()I
    .registers 2

    .prologue
    .line 476
    iget v0, p0, Landroid/support/v7/widget/aj;->b:I

    return v0
.end method

.method public getDividerDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 218
    iget-object v0, p0, Landroid/support/v7/widget/aj;->v:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getDividerPadding()I
    .registers 2

    .prologue
    .line 265
    iget v0, p0, Landroid/support/v7/widget/aj;->z:I

    return v0
.end method

.method public getDividerWidth()I
    .registers 2

    .prologue
    .line 274
    iget v0, p0, Landroid/support/v7/widget/aj;->w:I

    return v0
.end method

.method public getOrientation()I
    .registers 2

    .prologue
    .line 1663
    iget v0, p0, Landroid/support/v7/widget/aj;->d:I

    return v0
.end method

.method public getShowDividers()I
    .registers 2

    .prologue
    .line 209
    iget v0, p0, Landroid/support/v7/widget/aj;->y:I

    return v0
.end method

.method getVirtualChildCount()I
    .registers 2

    .prologue
    .line 514
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getWeightSum()F
    .registers 2

    .prologue
    .line 525
    iget v0, p0, Landroid/support/v7/widget/aj;->g:F

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 8

    .prologue
    const/16 v5, 0x8

    const/4 v0, 0x0

    .line 279
    iget-object v1, p0, Landroid/support/v7/widget/aj;->v:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_8

    .line 288
    :cond_7
    :goto_7
    return-void

    .line 283
    :cond_8
    iget v1, p0, Landroid/support/v7/widget/aj;->d:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6a

    .line 2291
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getVirtualChildCount()I

    move-result v2

    move v1, v0

    .line 2292
    :goto_12
    if-ge v1, v2, :cond_3e

    .line 2501
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2295
    if-eqz v3, :cond_3a

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v5, :cond_3a

    .line 2296
    invoke-direct {p0, v1}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 2297
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/al;

    .line 2298
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    iget v0, v0, Landroid/support/v7/widget/al;->topMargin:I

    sub-int v0, v3, v0

    iget v3, p0, Landroid/support/v7/widget/aj;->x:I

    sub-int/2addr v0, v3

    .line 2299
    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/aj;->a(Landroid/graphics/Canvas;I)V

    .line 2292
    :cond_3a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12

    .line 2304
    :cond_3e
    invoke-direct {p0, v2}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2305
    add-int/lit8 v0, v2, -0x1

    .line 3501
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2307
    if-nez v1, :cond_5c

    .line 2308
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/support/v7/widget/aj;->x:I

    sub-int/2addr v0, v1

    .line 2313
    :goto_58
    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/aj;->a(Landroid/graphics/Canvas;I)V

    goto :goto_7

    .line 2310
    :cond_5c
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/al;

    .line 2311
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v0, v1

    goto :goto_58

    .line 4318
    :cond_6a
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getVirtualChildCount()I

    move-result v2

    .line 4319
    invoke-static {p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v3

    move v1, v0

    .line 4320
    :goto_73
    if-ge v1, v2, :cond_a9

    .line 4501
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 4323
    if-eqz v4, :cond_99

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v5, :cond_99

    .line 4324
    invoke-direct {p0, v1}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v0

    if-eqz v0, :cond_99

    .line 4325
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/al;

    .line 4327
    if-eqz v3, :cond_9d

    .line 4328
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    iget v0, v0, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v0, v4

    .line 4332
    :goto_96
    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/aj;->b(Landroid/graphics/Canvas;I)V

    .line 4320
    :cond_99
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_73

    .line 4330
    :cond_9d
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    iget v0, v0, Landroid/support/v7/widget/al;->leftMargin:I

    sub-int v0, v4, v0

    iget v4, p0, Landroid/support/v7/widget/aj;->w:I

    sub-int/2addr v0, v4

    goto :goto_96

    .line 4337
    :cond_a9
    invoke-direct {p0, v2}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4338
    add-int/lit8 v0, v2, -0x1

    .line 5501
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 4340
    if-nez v1, :cond_cf

    .line 4341
    if-eqz v3, :cond_c2

    .line 4342
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v0

    .line 4354
    :goto_bd
    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/aj;->b(Landroid/graphics/Canvas;I)V

    goto/16 :goto_7

    .line 4344
    :cond_c2
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/support/v7/widget/aj;->w:I

    sub-int/2addr v0, v1

    goto :goto_bd

    .line 4347
    :cond_cf
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/al;

    .line 4348
    if-eqz v3, :cond_e3

    .line 4349
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/al;->leftMargin:I

    sub-int v0, v1, v0

    iget v1, p0, Landroid/support/v7/widget/aj;->w:I

    sub-int/2addr v0, v1

    goto :goto_bd

    .line 4351
    :cond_e3
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v0, v1

    goto :goto_bd
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4

    .prologue
    .line 1741
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_12

    .line 1742
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1743
    const-class v0, Landroid/support/v7/widget/aj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1745
    :cond_12
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 4

    .prologue
    .line 1748
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_12

    .line 1749
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1750
    const-class v0, Landroid/support/v7/widget/aj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1752
    :cond_12
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 29

    .prologue
    .line 1402
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->d:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_bd

    .line 25422
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v8

    .line 25428
    sub-int v3, p4, p2

    .line 25429
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v4

    sub-int v9, v3, v4

    .line 25432
    sub-int/2addr v3, v8

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v4

    sub-int v10, v3, v4

    .line 25434
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getVirtualChildCount()I

    move-result v11

    .line 25436
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->e:I

    and-int/lit8 v3, v3, 0x70

    .line 25437
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->e:I

    const v5, 0x800007

    and-int/2addr v5, v4

    .line 25439
    sparse-switch v3, :sswitch_data_1e6

    .line 25452
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v3

    .line 25456
    :goto_33
    const/4 v7, 0x0

    move v6, v3

    :goto_35
    if-ge v7, v11, :cond_1d6

    .line 26501
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 25458
    if-nez v12, :cond_62

    .line 25459
    add-int/lit8 v6, v6, 0x0

    move v3, v7

    .line 25456
    :goto_42
    add-int/lit8 v7, v3, 0x1

    goto :goto_35

    .line 25442
    :sswitch_45
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v3

    add-int v3, v3, p5

    sub-int v3, v3, p3

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->f:I

    sub-int/2addr v3, v4

    .line 25443
    goto :goto_33

    .line 25447
    :sswitch_53
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v3

    sub-int v4, p5, p3

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/aj;->f:I

    sub-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 25448
    goto :goto_33

    .line 25460
    :cond_62
    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_1e2

    .line 25461
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    .line 25462
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    .line 25464
    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/al;

    .line 25467
    iget v4, v3, Landroid/support/v7/widget/al;->h:I

    .line 25468
    if-gez v4, :cond_7d

    move v4, v5

    .line 25471
    :cond_7d
    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v15

    .line 25472
    invoke-static {v4, v15}, Landroid/support/v4/view/v;->a(II)I

    move-result v4

    .line 25474
    and-int/lit8 v4, v4, 0x7

    sparse-switch v4, :sswitch_data_1f0

    .line 25486
    iget v4, v3, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v4, v8

    .line 25490
    :goto_8d
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v15

    if-eqz v15, :cond_9a

    .line 25491
    move-object/from16 v0, p0

    iget v15, v0, Landroid/support/v7/widget/aj;->x:I

    add-int/2addr v6, v15

    .line 25494
    :cond_9a
    iget v15, v3, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v6, v15

    .line 25495
    add-int/lit8 v15, v6, 0x0

    invoke-static {v12, v4, v15, v13, v14}, Landroid/support/v7/widget/aj;->b(Landroid/view/View;IIII)V

    .line 25497
    iget v3, v3, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v3, v14

    add-int/lit8 v3, v3, 0x0

    add-int/2addr v6, v3

    .line 25499
    add-int/lit8 v3, v7, 0x0

    goto :goto_42

    .line 25476
    :sswitch_ab
    sub-int v4, v10, v13

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v8

    iget v15, v3, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v4, v15

    iget v15, v3, Landroid/support/v7/widget/al;->rightMargin:I

    sub-int/2addr v4, v15

    .line 25478
    goto :goto_8d

    .line 25481
    :sswitch_b7
    sub-int v4, v9, v13

    iget v15, v3, Landroid/support/v7/widget/al;->rightMargin:I

    sub-int/2addr v4, v15

    .line 25482
    goto :goto_8d

    .line 26517
    :cond_bd
    invoke-static/range {p0 .. p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v5

    .line 26518
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v8

    .line 26524
    sub-int v3, p5, p3

    .line 26525
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v4

    sub-int v12, v3, v4

    .line 26528
    sub-int/2addr v3, v8

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v4

    sub-int v13, v3, v4

    .line 26530
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getVirtualChildCount()I

    move-result v14

    .line 26532
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->e:I

    const v4, 0x800007

    and-int/2addr v3, v4

    .line 26533
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->e:I

    and-int/lit8 v11, v4, 0x70

    .line 26535
    move-object/from16 v0, p0

    iget-boolean v15, v0, Landroid/support/v7/widget/aj;->a:Z

    .line 26537
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v7/widget/aj;->o:[I

    move-object/from16 v16, v0

    .line 26538
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v7/widget/aj;->p:[I

    move-object/from16 v17, v0

    .line 26540
    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v4

    .line 26541
    invoke-static {v3, v4}, Landroid/support/v4/view/v;->a(II)I

    move-result v3

    sparse-switch v3, :sswitch_data_1fa

    .line 26554
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v9

    .line 26558
    :goto_105
    const/4 v4, 0x0

    .line 26559
    const/4 v3, 0x1

    .line 26561
    if-eqz v5, :cond_1de

    .line 26562
    add-int/lit8 v4, v14, -0x1

    .line 26563
    const/4 v3, -0x1

    move v5, v4

    move v4, v3

    .line 26566
    :goto_10e
    const/4 v10, 0x0

    :goto_10f
    if-ge v10, v14, :cond_1d6

    .line 26567
    mul-int v3, v4, v10

    add-int v18, v5, v3

    .line 27501
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    .line 26570
    if-nez v19, :cond_144

    .line 26571
    add-int/lit8 v9, v9, 0x0

    move v3, v10

    .line 26566
    :goto_122
    add-int/lit8 v10, v3, 0x1

    goto :goto_10f

    .line 26544
    :sswitch_125
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v3

    add-int v3, v3, p4

    sub-int v3, v3, p2

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->f:I

    sub-int v9, v3, v4

    .line 26545
    goto :goto_105

    .line 26549
    :sswitch_134
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v3

    sub-int v4, p4, p2

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/aj;->f:I

    sub-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x2

    add-int v9, v3, v4

    .line 26550
    goto :goto_105

    .line 26572
    :cond_144
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v6, 0x8

    if-eq v3, v6, :cond_1db

    .line 26573
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredWidth()I

    move-result v20

    .line 26574
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    .line 26575
    const/4 v6, -0x1

    .line 26577
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/al;

    .line 26580
    if-eqz v15, :cond_169

    iget v7, v3, Landroid/support/v7/widget/al;->height:I

    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v7, v0, :cond_169

    .line 26581
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getBaseline()I

    move-result v6

    .line 26584
    :cond_169
    iget v7, v3, Landroid/support/v7/widget/al;->h:I

    .line 26585
    if-gez v7, :cond_16e

    move v7, v11

    .line 26589
    :cond_16e
    and-int/lit8 v7, v7, 0x70

    sparse-switch v7, :sswitch_data_204

    move v6, v8

    .line 26625
    :goto_174
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v7

    if-eqz v7, :cond_1d7

    .line 26626
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/aj;->w:I

    add-int/2addr v7, v9

    .line 26629
    :goto_183
    iget v9, v3, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v7, v9

    .line 26630
    add-int/lit8 v9, v7, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v0, v9, v6, v1, v2}, Landroid/support/v7/widget/aj;->b(Landroid/view/View;IIII)V

    .line 26632
    iget v3, v3, Landroid/support/v7/widget/al;->rightMargin:I

    add-int v3, v3, v20

    add-int/lit8 v3, v3, 0x0

    add-int v9, v7, v3

    .line 26635
    add-int/lit8 v3, v10, 0x0

    goto :goto_122

    .line 26591
    :sswitch_19c
    iget v7, v3, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v7, v8

    .line 26592
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v6, v0, :cond_1d9

    .line 26593
    const/16 v22, 0x1

    aget v22, v16, v22

    sub-int v6, v22, v6

    add-int/2addr v6, v7

    goto :goto_174

    .line 26609
    :sswitch_1ad
    sub-int v6, v13, v21

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, v8

    iget v7, v3, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v6, v7

    iget v7, v3, Landroid/support/v7/widget/al;->bottomMargin:I

    sub-int/2addr v6, v7

    .line 26611
    goto :goto_174

    .line 26614
    :sswitch_1b9
    sub-int v7, v12, v21

    iget v0, v3, Landroid/support/v7/widget/al;->bottomMargin:I

    move/from16 v22, v0

    sub-int v7, v7, v22

    .line 26615
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v6, v0, :cond_1d9

    .line 26616
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredHeight()I

    move-result v22

    sub-int v6, v22, v6

    .line 26617
    const/16 v22, 0x2

    aget v22, v17, v22

    sub-int v6, v22, v6

    sub-int v6, v7, v6

    .line 26618
    goto :goto_174

    .line 1407
    :cond_1d6
    return-void

    :cond_1d7
    move v7, v9

    goto :goto_183

    :cond_1d9
    move v6, v7

    goto :goto_174

    :cond_1db
    move v3, v10

    goto/16 :goto_122

    :cond_1de
    move v5, v4

    move v4, v3

    goto/16 :goto_10e

    :cond_1e2
    move v3, v7

    goto/16 :goto_42

    .line 25439
    nop

    :sswitch_data_1e6
    .sparse-switch
        0x10 -> :sswitch_53
        0x50 -> :sswitch_45
    .end sparse-switch

    .line 25474
    :sswitch_data_1f0
    .sparse-switch
        0x1 -> :sswitch_ab
        0x5 -> :sswitch_b7
    .end sparse-switch

    .line 26541
    :sswitch_data_1fa
    .sparse-switch
        0x1 -> :sswitch_134
        0x5 -> :sswitch_125
    .end sparse-switch

    .line 26589
    :sswitch_data_204
    .sparse-switch
        0x10 -> :sswitch_1ad
        0x30 -> :sswitch_19c
        0x50 -> :sswitch_1b9
    .end sparse-switch
.end method

.method public onMeasure(II)V
    .registers 30

    .prologue
    .line 546
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->d:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3ab

    .line 9590
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9591
    const/16 v18, 0x0

    .line 9592
    const/16 v17, 0x0

    .line 9593
    const/16 v16, 0x0

    .line 9594
    const/4 v15, 0x0

    .line 9595
    const/4 v14, 0x1

    .line 9596
    const/4 v5, 0x0

    .line 9598
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getVirtualChildCount()I

    move-result v21

    .line 9600
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v22

    .line 9601
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v23

    .line 9603
    const/4 v10, 0x0

    .line 9604
    const/4 v12, 0x0

    .line 9606
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/aj;->b:I

    move/from16 v24, v0

    .line 9607
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v7/widget/aj;->n:Z

    move/from16 v25, v0

    .line 9609
    const/high16 v11, -0x80000000

    .line 9612
    const/16 v19, 0x0

    :goto_33
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_18c

    .line 10501
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 9615
    if-nez v4, :cond_52

    .line 9616
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    add-int/lit8 v3, v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    move/from16 v3, v19

    .line 9612
    :goto_4f
    add-int/lit8 v19, v3, 0x1

    goto :goto_33

    .line 9620
    :cond_52
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v6, 0x8

    if-eq v3, v6, :cond_3ca

    .line 9625
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v3

    if-eqz v3, :cond_71

    .line 9626
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/aj;->x:I

    add-int/2addr v3, v6

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9629
    :cond_71
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Landroid/support/v7/widget/al;

    .line 9631
    iget v3, v9, Landroid/support/v7/widget/al;->g:F

    add-float v13, v5, v3

    .line 9633
    const/high16 v3, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-ne v0, v3, :cond_c7

    iget v3, v9, Landroid/support/v7/widget/al;->height:I

    if-nez v3, :cond_c7

    iget v3, v9, Landroid/support/v7/widget/al;->g:F

    const/4 v5, 0x0

    cmpl-float v3, v3, v5

    if-lez v3, :cond_c7

    .line 9637
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9638
    iget v5, v9, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v5, v3

    iget v6, v9, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v5, v6

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9639
    const/4 v12, 0x1

    move v8, v11

    move v11, v12

    .line 9678
    :goto_a2
    if-ltz v24, :cond_b2

    add-int/lit8 v3, v19, 0x1

    move/from16 v0, v24

    if-ne v0, v3, :cond_b2

    .line 9679
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->c:I

    .line 9685
    :cond_b2
    move/from16 v0, v19

    move/from16 v1, v24

    if-ge v0, v1, :cond_11c

    iget v3, v9, Landroid/support/v7/widget/al;->g:F

    const/4 v5, 0x0

    cmpl-float v3, v3, v5

    if-lez v3, :cond_11c

    .line 9686
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won\'t work.  Either remove the weight, or don\'t set mBaselineAlignedChildIndex."

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 9641
    :cond_c7
    const/high16 v3, -0x80000000

    .line 9643
    iget v5, v9, Landroid/support/v7/widget/al;->height:I

    if-nez v5, :cond_d8

    iget v5, v9, Landroid/support/v7/widget/al;->g:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_d8

    .line 9648
    const/4 v3, 0x0

    .line 9649
    const/4 v5, -0x2

    iput v5, v9, Landroid/support/v7/widget/al;->height:I

    :cond_d8
    move/from16 v20, v3

    .line 9656
    const/4 v6, 0x0

    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-nez v3, :cond_11a

    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/v7/widget/aj;->f:I

    :goto_e4
    move-object/from16 v3, p0

    move/from16 v5, p1

    move/from16 v7, p2

    invoke-direct/range {v3 .. v8}, Landroid/support/v7/widget/aj;->a(Landroid/view/View;IIII)V

    .line 9660
    const/high16 v3, -0x80000000

    move/from16 v0, v20

    if-eq v0, v3, :cond_f7

    .line 9661
    move/from16 v0, v20

    iput v0, v9, Landroid/support/v7/widget/al;->height:I

    .line 9664
    :cond_f7
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 9665
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9666
    add-int v6, v5, v3

    iget v7, v9, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v6, v7

    iget v7, v9, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x0

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9669
    if-eqz v25, :cond_3c6

    .line 9670
    invoke-static {v3, v11}, Ljava/lang/Math;->max(II)I

    move-result v11

    move v8, v11

    move v11, v12

    goto :goto_a2

    .line 9656
    :cond_11a
    const/4 v8, 0x0

    goto :goto_e4

    .line 9692
    :cond_11c
    const/4 v3, 0x0

    .line 9693
    const/high16 v5, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v5, :cond_3c3

    iget v5, v9, Landroid/support/v7/widget/al;->width:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_3c3

    .line 9698
    const/4 v5, 0x1

    .line 9699
    const/4 v3, 0x1

    .line 9702
    :goto_12a
    iget v6, v9, Landroid/support/v7/widget/al;->leftMargin:I

    iget v7, v9, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v6, v7

    .line 9703
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v6

    .line 9704
    move/from16 v0, v18

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 9705
    invoke-static {v4}, Landroid/support/v4/view/cx;->j(Landroid/view/View;)I

    move-result v4

    move/from16 v0, v17

    invoke-static {v0, v4}, Landroid/support/v7/internal/widget/bd;->a(II)I

    move-result v10

    .line 9708
    if-eqz v14, :cond_176

    iget v4, v9, Landroid/support/v7/widget/al;->width:I

    const/4 v14, -0x1

    if-ne v4, v14, :cond_176

    const/4 v4, 0x1

    .line 9709
    :goto_14c
    iget v9, v9, Landroid/support/v7/widget/al;->g:F

    const/4 v14, 0x0

    cmpl-float v9, v9, v14

    if-lez v9, :cond_17a

    .line 9714
    if-eqz v3, :cond_178

    move v3, v6

    :goto_156
    invoke-static {v15, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v6, v13

    move v7, v4

    move/from16 v9, v16

    move v4, v11

    move v11, v12

    move/from16 v26, v8

    move v8, v3

    move/from16 v3, v26

    .line 9721
    :goto_165
    add-int/lit8 v12, v19, 0x0

    move v14, v7

    move v15, v8

    move/from16 v16, v9

    move/from16 v17, v10

    move/from16 v18, v11

    move v11, v3

    move v10, v5

    move v3, v12

    move v5, v6

    move v12, v4

    goto/16 :goto_4f

    .line 9708
    :cond_176
    const/4 v4, 0x0

    goto :goto_14c

    :cond_178
    move v3, v7

    .line 9714
    goto :goto_156

    .line 9717
    :cond_17a
    if-eqz v3, :cond_18a

    :goto_17c
    move/from16 v0, v16

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v6, v13

    move v7, v4

    move v9, v3

    move v4, v11

    move v3, v8

    move v8, v15

    move v11, v12

    goto :goto_165

    :cond_18a
    move v6, v7

    goto :goto_17c

    .line 9724
    :cond_18c
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    if-lez v3, :cond_1a9

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Landroid/support/v7/widget/aj;->b(I)Z

    move-result v3

    if-eqz v3, :cond_1a9

    .line 9725
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->x:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9728
    :cond_1a9
    if-eqz v25, :cond_1fc

    const/high16 v3, -0x80000000

    move/from16 v0, v23

    if-eq v0, v3, :cond_1b3

    if-nez v23, :cond_1fc

    .line 9730
    :cond_1b3
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9732
    const/4 v4, 0x0

    :goto_1b9
    move/from16 v0, v21

    if-ge v4, v0, :cond_1fc

    .line 11501
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 9735
    if-nez v3, :cond_1d3

    .line 9736
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    add-int/lit8 v3, v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    move v3, v4

    .line 9732
    :goto_1d0
    add-int/lit8 v4, v3, 0x1

    goto :goto_1b9

    .line 9740
    :cond_1d3
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-ne v6, v7, :cond_1de

    .line 9741
    add-int/lit8 v3, v4, 0x0

    .line 9742
    goto :goto_1d0

    .line 9745
    :cond_1de
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/al;

    .line 9748
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9749
    add-int v7, v6, v11

    iget v8, v3, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v7, v8

    iget v3, v3, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v3, v7

    add-int/lit8 v3, v3, 0x0

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    move v3, v4

    goto :goto_1d0

    .line 9755
    :cond_1fc
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v6

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9757
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9760
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getSuggestedMinimumHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 9763
    const/4 v4, 0x0

    move/from16 v0, p2

    invoke-static {v3, v0, v4}, Landroid/support/v4/view/cx;->a(III)I

    move-result v19

    .line 9764
    const v3, 0xffffff

    and-int v3, v3, v19

    .line 9769
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/aj;->f:I

    sub-int v6, v3, v4

    .line 9770
    if-nez v12, :cond_235

    if-eqz v6, :cond_363

    const/4 v3, 0x0

    cmpl-float v3, v5, v3

    if-lez v3, :cond_363

    .line 9771
    :cond_235
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->g:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_242

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/aj;->g:F

    .line 9773
    :cond_242
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9775
    const/4 v3, 0x0

    move v15, v3

    move v12, v14

    move/from16 v13, v16

    move/from16 v11, v17

    move/from16 v14, v18

    :goto_250
    move/from16 v0, v21

    if-ge v15, v0, :cond_317

    .line 12501
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 9778
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_3bb

    .line 9782
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/al;

    .line 9784
    iget v8, v3, Landroid/support/v7/widget/al;->g:F

    .line 9785
    const/4 v4, 0x0

    cmpl-float v4, v8, v4

    if-lez v4, :cond_3b6

    .line 9787
    int-to-float v4, v6

    mul-float/2addr v4, v8

    div-float/2addr v4, v5

    float-to-int v4, v4

    .line 9788
    sub-float v8, v5, v8

    .line 9789
    sub-int v9, v6, v4

    .line 9791
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, v3, Landroid/support/v7/widget/al;->leftMargin:I

    add-int/2addr v5, v6

    iget v6, v3, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v5, v6

    iget v6, v3, Landroid/support/v7/widget/al;->width:I

    move/from16 v0, p1

    invoke-static {v0, v5, v6}, Landroid/support/v7/widget/aj;->getChildMeasureSpec(III)I

    move-result v5

    .line 9797
    iget v6, v3, Landroid/support/v7/widget/al;->height:I

    if-nez v6, :cond_298

    const/high16 v6, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v6, :cond_30a

    .line 9800
    :cond_298
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v4, v6

    .line 9801
    if-gez v4, :cond_2a0

    .line 9802
    const/4 v4, 0x0

    :cond_2a0
    move-object v6, v7

    .line 9810
    :goto_2a1
    const/high16 v16, 0x40000000    # 2.0f

    move/from16 v0, v16

    invoke-static {v4, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v6, v5, v4}, Landroid/view/View;->measure(II)V

    .line 9816
    invoke-static {v7}, Landroid/support/v4/view/cx;->j(Landroid/view/View;)I

    move-result v4

    and-int/lit16 v4, v4, -0x100

    invoke-static {v11, v4}, Landroid/support/v7/internal/widget/bd;->a(II)I

    move-result v4

    move v5, v9

    move v6, v4

    move v4, v8

    .line 9821
    :goto_2b9
    iget v8, v3, Landroid/support/v7/widget/al;->leftMargin:I

    iget v9, v3, Landroid/support/v7/widget/al;->rightMargin:I

    add-int/2addr v8, v9

    .line 9822
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v8

    .line 9823
    invoke-static {v14, v9}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 9825
    const/high16 v14, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v14, :cond_311

    iget v14, v3, Landroid/support/v7/widget/al;->width:I

    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v14, v0, :cond_311

    const/4 v14, 0x1

    .line 9828
    :goto_2d6
    if-eqz v14, :cond_313

    :goto_2d8
    invoke-static {v13, v8}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 9831
    if-eqz v12, :cond_315

    iget v8, v3, Landroid/support/v7/widget/al;->width:I

    const/4 v12, -0x1

    if-ne v8, v12, :cond_315

    const/4 v8, 0x1

    .line 9833
    :goto_2e4
    move-object/from16 v0, p0

    iget v12, v0, Landroid/support/v7/widget/aj;->f:I

    .line 9834
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v12

    iget v13, v3, Landroid/support/v7/widget/al;->topMargin:I

    add-int/2addr v7, v13

    iget v3, v3, Landroid/support/v7/widget/al;->bottomMargin:I

    add-int/2addr v3, v7

    add-int/lit8 v3, v3, 0x0

    invoke-static {v12, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    move v3, v9

    move v7, v11

    .line 9775
    :goto_2ff
    add-int/lit8 v9, v15, 0x1

    move v15, v9

    move v12, v8

    move v13, v3

    move v11, v6

    move v14, v7

    move v6, v5

    move v5, v4

    goto/16 :goto_250

    .line 9810
    :cond_30a
    if-lez v4, :cond_30e

    move-object v6, v7

    goto :goto_2a1

    :cond_30e
    const/4 v4, 0x0

    move-object v6, v7

    goto :goto_2a1

    .line 9825
    :cond_311
    const/4 v14, 0x0

    goto :goto_2d6

    :cond_313
    move v8, v9

    .line 9828
    goto :goto_2d8

    .line 9831
    :cond_315
    const/4 v8, 0x0

    goto :goto_2e4

    .line 9839
    :cond_317
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/aj;->f:I

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingTop()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/aj;->f:I

    move v3, v13

    move/from16 v17, v11

    move v4, v14

    move v14, v12

    .line 9871
    :goto_32e
    if-nez v14, :cond_3af

    const/high16 v5, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v5, :cond_3af

    .line 9875
    :goto_336
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingLeft()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 9878
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/aj;->getSuggestedMinimumWidth()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 9880
    move/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v3, v0, v1}, Landroid/support/v4/view/cx;->a(III)I

    move-result v3

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Landroid/support/v7/widget/aj;->setMeasuredDimension(II)V

    .line 9883
    if-eqz v10, :cond_362

    .line 9884
    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, p2

    invoke-direct {v0, v1, v2}, Landroid/support/v7/widget/aj;->b(II)V

    .line 551
    :cond_362
    :goto_362
    return-void

    .line 9842
    :cond_363
    move/from16 v0, v16

    invoke-static {v0, v15}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 9848
    if-eqz v25, :cond_3b1

    const/high16 v3, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v3, :cond_3b1

    .line 9849
    const/4 v3, 0x0

    move v4, v3

    :goto_373
    move/from16 v0, v21

    if-ge v4, v0, :cond_3b1

    .line 13501
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 9852
    if-eqz v5, :cond_3a7

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v6, 0x8

    if-eq v3, v6, :cond_3a7

    .line 9856
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/al;

    .line 9859
    iget v3, v3, Landroid/support/v7/widget/al;->g:F

    .line 9860
    const/4 v6, 0x0

    cmpl-float v3, v3, v6

    if-lez v3, :cond_3a7

    .line 9861
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v11, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v3, v6}, Landroid/view/View;->measure(II)V

    .line 9849
    :cond_3a7
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_373

    .line 549
    :cond_3ab
    invoke-direct/range {p0 .. p2}, Landroid/support/v7/widget/aj;->c(II)V

    goto :goto_362

    :cond_3af
    move v3, v4

    goto :goto_336

    :cond_3b1
    move v3, v13

    move/from16 v4, v18

    goto/16 :goto_32e

    :cond_3b6
    move v4, v5

    move v5, v6

    move v6, v11

    goto/16 :goto_2b9

    :cond_3bb
    move v4, v5

    move v8, v12

    move v3, v13

    move v7, v14

    move v5, v6

    move v6, v11

    goto/16 :goto_2ff

    :cond_3c3
    move v5, v10

    goto/16 :goto_12a

    :cond_3c6
    move v8, v11

    move v11, v12

    goto/16 :goto_a2

    :cond_3ca
    move v3, v11

    move v4, v12

    move v6, v5

    move v7, v14

    move v8, v15

    move/from16 v9, v16

    move v5, v10

    move/from16 v11, v18

    move/from16 v10, v17

    goto/16 :goto_165
.end method

.method public setBaselineAligned(Z)V
    .registers 2

    .prologue
    .line 388
    iput-boolean p1, p0, Landroid/support/v7/widget/aj;->a:Z

    .line 389
    return-void
.end method

.method public setBaselineAlignedChildIndex(I)V
    .registers 5

    .prologue
    .line 484
    if-ltz p1, :cond_8

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getChildCount()I

    move-result v0

    if-lt p1, v0, :cond_27

    .line 485
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "base aligned child index out of range (0, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->getChildCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 488
    :cond_27
    iput p1, p0, Landroid/support/v7/widget/aj;->b:I

    .line 489
    return-void
.end method

.method public setDividerDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 229
    iget-object v1, p0, Landroid/support/v7/widget/aj;->v:Landroid/graphics/drawable/Drawable;

    if-ne p1, v1, :cond_6

    .line 242
    :goto_5
    return-void

    .line 232
    :cond_6
    iput-object p1, p0, Landroid/support/v7/widget/aj;->v:Landroid/graphics/drawable/Drawable;

    .line 233
    if-eqz p1, :cond_20

    .line 234
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/aj;->w:I

    .line 235
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/aj;->x:I

    .line 240
    :goto_16
    if-nez p1, :cond_19

    const/4 v0, 0x1

    :cond_19
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aj;->setWillNotDraw(Z)V

    .line 241
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->requestLayout()V

    goto :goto_5

    .line 237
    :cond_20
    iput v0, p0, Landroid/support/v7/widget/aj;->w:I

    .line 238
    iput v0, p0, Landroid/support/v7/widget/aj;->x:I

    goto :goto_16
.end method

.method public setDividerPadding(I)V
    .registers 2

    .prologue
    .line 254
    iput p1, p0, Landroid/support/v7/widget/aj;->z:I

    .line 255
    return-void
.end method

.method public setGravity(I)V
    .registers 4

    .prologue
    .line 1675
    iget v0, p0, Landroid/support/v7/widget/aj;->e:I

    if-eq v0, p1, :cond_19

    .line 1676
    const v0, 0x800007

    and-int/2addr v0, p1

    if-nez v0, :cond_1a

    .line 1677
    const v0, 0x800003

    or-int/2addr v0, p1

    .line 1680
    :goto_e
    and-int/lit8 v1, v0, 0x70

    if-nez v1, :cond_14

    .line 1681
    or-int/lit8 v0, v0, 0x30

    .line 1684
    :cond_14
    iput v0, p0, Landroid/support/v7/widget/aj;->e:I

    .line 1685
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->requestLayout()V

    .line 1687
    :cond_19
    return-void

    :cond_1a
    move v0, p1

    goto :goto_e
.end method

.method public setHorizontalGravity(I)V
    .registers 5

    .prologue
    const v2, 0x800007

    .line 1690
    and-int v0, p1, v2

    .line 1691
    iget v1, p0, Landroid/support/v7/widget/aj;->e:I

    and-int/2addr v1, v2

    if-eq v1, v0, :cond_16

    .line 1692
    iget v1, p0, Landroid/support/v7/widget/aj;->e:I

    const v2, -0x800008

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/aj;->e:I

    .line 1693
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->requestLayout()V

    .line 1695
    :cond_16
    return-void
.end method

.method public setMeasureWithLargestChildEnabled(Z)V
    .registers 2

    .prologue
    .line 414
    iput-boolean p1, p0, Landroid/support/v7/widget/aj;->n:Z

    .line 415
    return-void
.end method

.method public setOrientation(I)V
    .registers 3

    .prologue
    .line 1650
    iget v0, p0, Landroid/support/v7/widget/aj;->d:I

    if-eq v0, p1, :cond_9

    .line 1651
    iput p1, p0, Landroid/support/v7/widget/aj;->d:I

    .line 1652
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->requestLayout()V

    .line 1654
    :cond_9
    return-void
.end method

.method public setShowDividers(I)V
    .registers 3

    .prologue
    .line 192
    iget v0, p0, Landroid/support/v7/widget/aj;->y:I

    if-eq p1, v0, :cond_7

    .line 193
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->requestLayout()V

    .line 195
    :cond_7
    iput p1, p0, Landroid/support/v7/widget/aj;->y:I

    .line 196
    return-void
.end method

.method public setVerticalGravity(I)V
    .registers 4

    .prologue
    .line 1698
    and-int/lit8 v0, p1, 0x70

    .line 1699
    iget v1, p0, Landroid/support/v7/widget/aj;->e:I

    and-int/lit8 v1, v1, 0x70

    if-eq v1, v0, :cond_12

    .line 1700
    iget v1, p0, Landroid/support/v7/widget/aj;->e:I

    and-int/lit8 v1, v1, -0x71

    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/aj;->e:I

    .line 1701
    invoke-virtual {p0}, Landroid/support/v7/widget/aj;->requestLayout()V

    .line 1703
    :cond_12
    return-void
.end method

.method public setWeightSum(F)V
    .registers 3

    .prologue
    .line 541
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/aj;->g:F

    .line 542
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2

    .prologue
    .line 200
    const/4 v0, 0x0

    return v0
.end method
