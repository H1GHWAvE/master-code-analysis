.class public final Landroid/support/v7/widget/t;
.super Landroid/widget/CheckedTextView;
.source "SourceFile"


# static fields
.field private static final a:[I


# instance fields
.field private b:Landroid/support/v7/internal/widget/av;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 34
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010108

    aput v2, v0, v1

    sput-object v0, Landroid/support/v7/widget/t;->a:[I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/t;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/t;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;B)V

    .line 46
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;B)V
    .registers 7

    .prologue
    const v2, 0x10103c8

    .line 49
    invoke-direct {p0, p1, p2, v2}, Landroid/widget/CheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    sget-boolean v0, Landroid/support/v7/internal/widget/av;->a:Z

    if-eqz v0, :cond_27

    .line 52
    invoke-virtual {p0}, Landroid/support/v7/widget/t;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/widget/t;->a:[I

    invoke-static {v0, p2, v1, v2}, Landroid/support/v7/internal/widget/ax;->a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;

    move-result-object v0

    .line 54
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/t;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1183
    iget-object v1, v0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 57
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ax;->a()Landroid/support/v7/internal/widget/av;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/internal/widget/av;

    .line 59
    :cond_27
    return-void
.end method


# virtual methods
.method public final setCheckMarkDrawable(I)V
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/internal/widget/av;

    if-eqz v0, :cond_f

    .line 64
    iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/internal/widget/av;

    .line 2167
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 64
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/t;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 68
    :goto_e
    return-void

    .line 66
    :cond_f
    invoke-super {p0, p1}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(I)V

    goto :goto_e
.end method
