.class public Lorg/xbill/DNS/DNSSEC$UnsupportedAlgorithmException;
.super Lorg/xbill/DNS/DNSSEC$DNSSECException;
.source "SourceFile"


# direct methods
.method constructor <init>(I)V
    .registers 4

    .prologue
    .line 214
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "Unsupported algorithm: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/xbill/DNS/DNSSEC$DNSSECException;-><init>(Ljava/lang/String;)V

    .line 215
    return-void
.end method
