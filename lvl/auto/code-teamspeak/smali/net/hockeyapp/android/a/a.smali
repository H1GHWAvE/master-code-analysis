.class public final Lnet/hockeyapp/android/a/a;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/ArrayList;

.field private b:Landroid/content/Context;

.field private c:Ljava/text/SimpleDateFormat;

.field private d:Ljava/text/SimpleDateFormat;

.field private e:Ljava/util/Date;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Lnet/hockeyapp/android/f/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .registers 5

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 60
    iput-object p1, p0, Lnet/hockeyapp/android/a/a;->b:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lnet/hockeyapp/android/a/a;->a:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/hockeyapp/android/a/a;->c:Ljava/text/SimpleDateFormat;

    .line 64
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "d MMM h:mm a"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/hockeyapp/android/a/a;->d:Ljava/text/SimpleDateFormat;

    .line 65
    return-void
.end method

.method private a()V
    .registers 2

    .prologue
    .line 123
    iget-object v0, p0, Lnet/hockeyapp/android/a/a;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_9

    .line 124
    iget-object v0, p0, Lnet/hockeyapp/android/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 126
    :cond_9
    return-void
.end method

.method private a(Lnet/hockeyapp/android/c/g;)V
    .registers 3

    .prologue
    .line 129
    if-eqz p1, :cond_b

    iget-object v0, p0, Lnet/hockeyapp/android/a/a;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    .line 130
    iget-object v0, p0, Lnet/hockeyapp/android/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    :cond_b
    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Lnet/hockeyapp/android/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 114
    iget-object v0, p0, Lnet/hockeyapp/android/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    .prologue
    .line 119
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11

    .prologue
    const/4 v2, 0x0

    .line 73
    iget-object v0, p0, Lnet/hockeyapp/android/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/c/g;

    .line 76
    if-nez p2, :cond_96

    .line 77
    new-instance p2, Lnet/hockeyapp/android/f/h;

    iget-object v1, p0, Lnet/hockeyapp/android/a/a;->b:Landroid/content/Context;

    invoke-direct {p2, v1}, Lnet/hockeyapp/android/f/h;-><init>(Landroid/content/Context;)V

    .line 83
    :goto_12
    if-eqz v0, :cond_9f

    .line 84
    const/16 v1, 0x3001

    invoke-virtual {p2, v1}, Lnet/hockeyapp/android/f/h;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/hockeyapp/android/a/a;->f:Landroid/widget/TextView;

    .line 85
    const/16 v1, 0x3002

    invoke-virtual {p2, v1}, Lnet/hockeyapp/android/f/h;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/hockeyapp/android/a/a;->g:Landroid/widget/TextView;

    .line 86
    const/16 v1, 0x3003

    invoke-virtual {p2, v1}, Lnet/hockeyapp/android/f/h;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lnet/hockeyapp/android/a/a;->h:Landroid/widget/TextView;

    .line 87
    const/16 v1, 0x3004

    invoke-virtual {p2, v1}, Lnet/hockeyapp/android/f/h;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lnet/hockeyapp/android/f/a;

    iput-object v1, p0, Lnet/hockeyapp/android/a/a;->i:Lnet/hockeyapp/android/f/a;

    .line 90
    :try_start_3c
    iget-object v1, p0, Lnet/hockeyapp/android/a/a;->c:Ljava/text/SimpleDateFormat;

    .line 1099
    iget-object v3, v0, Lnet/hockeyapp/android/c/g;->f:Ljava/lang/String;

    .line 90
    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lnet/hockeyapp/android/a/a;->e:Ljava/util/Date;

    .line 91
    iget-object v1, p0, Lnet/hockeyapp/android/a/a;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lnet/hockeyapp/android/a/a;->d:Ljava/text/SimpleDateFormat;

    iget-object v4, p0, Lnet/hockeyapp/android/a/a;->e:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_53
    .catch Ljava/text/ParseException; {:try_start_3c .. :try_end_53} :catch_9a

    .line 96
    :goto_53
    iget-object v1, p0, Lnet/hockeyapp/android/a/a;->f:Landroid/widget/TextView;

    .line 1147
    iget-object v3, v0, Lnet/hockeyapp/android/c/g;->l:Ljava/lang/String;

    .line 96
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v1, p0, Lnet/hockeyapp/android/a/a;->h:Landroid/widget/TextView;

    .line 2067
    iget-object v3, v0, Lnet/hockeyapp/android/c/g;->b:Ljava/lang/String;

    .line 97
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v1, p0, Lnet/hockeyapp/android/a/a;->i:Lnet/hockeyapp/android/f/a;

    invoke-virtual {v1}, Lnet/hockeyapp/android/f/a;->removeAllViews()V

    .line 2162
    iget-object v0, v0, Lnet/hockeyapp/android/c/g;->n:Ljava/util/List;

    .line 100
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/c/e;

    .line 101
    new-instance v3, Lnet/hockeyapp/android/f/b;

    iget-object v4, p0, Lnet/hockeyapp/android/a/a;->b:Landroid/content/Context;

    iget-object v5, p0, Lnet/hockeyapp/android/a/a;->i:Lnet/hockeyapp/android/f/a;

    invoke-direct {v3, v4, v5, v0}, Lnet/hockeyapp/android/f/b;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lnet/hockeyapp/android/c/e;)V

    .line 3067
    sget-object v4, Lnet/hockeyapp/android/d/d;->a:Lnet/hockeyapp/android/d/a;

    .line 3080
    iget-object v5, v4, Lnet/hockeyapp/android/d/a;->a:Ljava/util/Queue;

    new-instance v6, Lnet/hockeyapp/android/d/e;

    invoke-direct {v6, v0, v3, v2}, Lnet/hockeyapp/android/d/e;-><init>(Lnet/hockeyapp/android/c/e;Lnet/hockeyapp/android/f/b;B)V

    invoke-interface {v5, v6}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 3081
    invoke-virtual {v4}, Lnet/hockeyapp/android/d/a;->a()V

    .line 103
    iget-object v0, p0, Lnet/hockeyapp/android/a/a;->i:Lnet/hockeyapp/android/f/a;

    invoke-virtual {v0, v3}, Lnet/hockeyapp/android/f/a;->addView(Landroid/view/View;)V

    goto :goto_6c

    .line 80
    :cond_96
    check-cast p2, Lnet/hockeyapp/android/f/h;

    goto/16 :goto_12

    .line 93
    :catch_9a
    move-exception v1

    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_53

    .line 107
    :cond_9f
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_a8

    move v0, v2

    :goto_a4
    invoke-virtual {p2, v0}, Lnet/hockeyapp/android/f/h;->setFeedbackMessageViewBgAndTextColor(I)V

    .line 109
    return-object p2

    .line 107
    :cond_a8
    const/4 v0, 0x1

    goto :goto_a4
.end method
