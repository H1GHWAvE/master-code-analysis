.class public final Lnet/hockeyapp/android/e/f;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .prologue
    .line 44
    invoke-direct {p0}, Lnet/hockeyapp/android/e/f;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 78
    if-nez p0, :cond_5

    .line 79
    const-string v0, ""

    .line 95
    :goto_4
    return-object v0

    .line 83
    :cond_5
    :try_start_5
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 84
    if-nez v0, :cond_e

    .line 85
    const-string v0, ""

    goto :goto_4

    .line 88
    :cond_e
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 90
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_1e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_1e} :catch_20

    move-result-object v0

    goto :goto_4

    .line 92
    :catch_20
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 95
    const-string v0, ""

    goto :goto_4
.end method

.method private static a()Lnet/hockeyapp/android/e/f;
    .registers 1

    .prologue
    .line 58
    sget-object v0, Lnet/hockeyapp/android/e/h;->a:Lnet/hockeyapp/android/e/f;

    return-object v0
.end method

.method private static b()I
    .registers 1

    .prologue
    .line 68
    sget-object v0, Lnet/hockeyapp/android/a;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
