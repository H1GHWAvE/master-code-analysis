.class public final Lnet/hockeyapp/android/ay;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lnet/hockeyapp/android/d/g;

.field private static b:Lnet/hockeyapp/android/az;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const/4 v0, 0x0

    .line 57
    sput-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    .line 62
    sput-object v0, Lnet/hockeyapp/android/ay;->b:Lnet/hockeyapp/android/az;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lnet/hockeyapp/android/az;
    .registers 1

    .prologue
    .line 308
    sget-object v0, Lnet/hockeyapp/android/ay;->b:Lnet/hockeyapp/android/az;

    return-object v0
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 71
    .line 1105
    const-string v0, "https://sdk.hockeyapp.net/"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v0, p1, v1, v2}, Lnet/hockeyapp/android/ay;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V

    .line 72
    return-void
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
    .registers 5

    .prologue
    .line 117
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lnet/hockeyapp/android/ay;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V

    .line 118
    return-void
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
    .registers 11

    .prologue
    const/4 v2, 0x0

    .line 130
    invoke-static {p2}, Lnet/hockeyapp/android/e/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 132
    sput-object p3, Lnet/hockeyapp/android/ay;->b:Lnet/hockeyapp/android/az;

    .line 134
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 135
    invoke-static {}, Lnet/hockeyapp/android/e/w;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 2292
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2293
    if-eqz v0, :cond_30

    .line 2294
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v4, "hockey_update_dialog"

    invoke-virtual {v0, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 2295
    if-eqz v0, :cond_2e

    const/4 v0, 0x1

    .line 135
    :goto_2b
    if-eqz v0, :cond_32

    .line 3265
    :cond_2d
    :goto_2d
    return-void

    :cond_2e
    move v0, v2

    .line 2295
    goto :goto_2b

    :cond_30
    move v0, v2

    .line 2299
    goto :goto_2b

    .line 139
    :cond_32
    invoke-static {v1}, Lnet/hockeyapp/android/ay;->a(Ljava/lang/ref/WeakReference;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 3263
    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    if-eqz v0, :cond_46

    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-virtual {v0}, Lnet/hockeyapp/android/d/g;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v2, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v2, :cond_54

    .line 3264
    :cond_46
    new-instance v0, Lnet/hockeyapp/android/d/h;

    move-object v2, p1

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lnet/hockeyapp/android/d/h;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V

    .line 3265
    sput-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    goto :goto_2d

    .line 3268
    :cond_54
    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/d/g;->a(Ljava/lang/ref/WeakReference;)V

    goto :goto_2d
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
    .registers 5

    .prologue
    .line 93
    const-string v0, "https://sdk.hockeyapp.net/"

    const/4 v1, 0x1

    invoke-static {p0, v0, p1, p2, v1}, Lnet/hockeyapp/android/ay;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V

    .line 94
    return-void
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;Z)V
    .registers 5

    .prologue
    .line 105
    const-string v0, "https://sdk.hockeyapp.net/"

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1, p2}, Lnet/hockeyapp/android/ay;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V

    .line 106
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
    .registers 8

    .prologue
    .line 164
    invoke-static {p2}, Lnet/hockeyapp/android/e/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 166
    sput-object p3, Lnet/hockeyapp/android/ay;->b:Lnet/hockeyapp/android/az;

    .line 168
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 170
    invoke-static {v1}, Lnet/hockeyapp/android/ay;->a(Ljava/lang/ref/WeakReference;)Z

    move-result v2

    if-nez v2, :cond_29

    .line 5277
    sget-object v2, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    if-eqz v2, :cond_1f

    sget-object v2, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-virtual {v2}, Lnet/hockeyapp/android/d/g;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    sget-object v3, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v2, v3, :cond_2a

    .line 5278
    :cond_1f
    new-instance v2, Lnet/hockeyapp/android/d/g;

    invoke-direct {v2, v1, p1, v0, p3}, Lnet/hockeyapp/android/d/g;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V

    .line 5279
    sput-object v2, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-static {v2}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    :cond_29
    :goto_29
    return-void

    .line 5282
    :cond_2a
    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/d/g;->a(Ljava/lang/ref/WeakReference;)V

    goto :goto_29
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
    .registers 8

    .prologue
    .line 152
    const-string v0, "https://sdk.hockeyapp.net/"

    .line 4164
    invoke-static {p1}, Lnet/hockeyapp/android/e/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4166
    sput-object p2, Lnet/hockeyapp/android/ay;->b:Lnet/hockeyapp/android/az;

    .line 4168
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 4170
    invoke-static {v2}, Lnet/hockeyapp/android/ay;->a(Ljava/lang/ref/WeakReference;)Z

    move-result v3

    if-nez v3, :cond_2b

    .line 4277
    sget-object v3, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    if-eqz v3, :cond_21

    sget-object v3, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-virtual {v3}, Lnet/hockeyapp/android/d/g;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v3

    sget-object v4, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v3, v4, :cond_2c

    .line 4278
    :cond_21
    new-instance v3, Lnet/hockeyapp/android/d/g;

    invoke-direct {v3, v2, v0, v1, p2}, Lnet/hockeyapp/android/d/g;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V

    .line 4279
    sput-object v3, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-static {v3}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    :cond_2b
    :goto_2b
    return-void

    .line 4282
    :cond_2c
    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-virtual {v0, v2}, Lnet/hockeyapp/android/d/g;->a(Ljava/lang/ref/WeakReference;)V

    goto :goto_2b
.end method

.method private static a(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
    .registers 6

    .prologue
    .line 277
    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    if-eqz v0, :cond_e

    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-virtual {v0}, Lnet/hockeyapp/android/d/g;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_19

    .line 278
    :cond_e
    new-instance v0, Lnet/hockeyapp/android/d/g;

    invoke-direct {v0, p0, p1, p2, p3}, Lnet/hockeyapp/android/d/g;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V

    .line 279
    sput-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    .line 284
    :goto_18
    return-void

    .line 282
    :cond_19
    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-virtual {v0, p0}, Lnet/hockeyapp/android/d/g;->a(Ljava/lang/ref/WeakReference;)V

    goto :goto_18
.end method

.method private static a(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
    .registers 11

    .prologue
    .line 263
    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    if-eqz v0, :cond_e

    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-virtual {v0}, Lnet/hockeyapp/android/d/g;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1e

    .line 264
    :cond_e
    new-instance v0, Lnet/hockeyapp/android/d/h;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lnet/hockeyapp/android/d/h;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V

    .line 265
    sput-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    .line 270
    :goto_1d
    return-void

    .line 268
    :cond_1e
    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-virtual {v0, p0}, Lnet/hockeyapp/android/d/g;->a(Ljava/lang/ref/WeakReference;)V

    goto :goto_1d
.end method

.method private static a(Ljava/lang/ref/WeakReference;)Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 226
    .line 228
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 229
    if-eqz v0, :cond_1d

    .line 231
    :try_start_9
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 232
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_18
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_18} :catch_20

    move-result v0

    if-nez v0, :cond_1e

    const/4 v0, 0x1

    :goto_1c
    move v1, v0

    .line 238
    :cond_1d
    :goto_1d
    return v1

    :cond_1e
    move v0, v1

    .line 232
    goto :goto_1c

    :catch_20
    move-exception v0

    goto :goto_1d
.end method

.method private static b()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 179
    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    if-eqz v0, :cond_12

    .line 180
    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/d/g;->cancel(Z)Z

    .line 181
    sget-object v0, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    invoke-virtual {v0}, Lnet/hockeyapp/android/d/g;->a()V

    .line 182
    sput-object v2, Lnet/hockeyapp/android/ay;->a:Lnet/hockeyapp/android/d/g;

    .line 185
    :cond_12
    sput-object v2, Lnet/hockeyapp/android/ay;->b:Lnet/hockeyapp/android/az;

    .line 186
    return-void
.end method

.method private static b(Landroid/app/Activity;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 82
    .line 2105
    const-string v0, "https://sdk.hockeyapp.net/"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v0, p1, v1, v2}, Lnet/hockeyapp/android/ay;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V

    .line 83
    return-void
.end method

.method private static b(Ljava/lang/ref/WeakReference;)V
    .registers 4

    .prologue
    .line 246
    if-eqz p0, :cond_1c

    .line 247
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 248
    if-eqz v0, :cond_1c

    .line 249
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 251
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lnet/hockeyapp/android/k;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 252
    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 253
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 256
    :cond_1c
    return-void
.end method

.method private static c()Z
    .registers 1

    .prologue
    .line 204
    const/4 v0, 0x0

    return v0
.end method

.method private static c(Ljava/lang/ref/WeakReference;)Z
    .registers 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 292
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 293
    if-eqz v0, :cond_19

    .line 294
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "hockey_update_dialog"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 295
    if-eqz v0, :cond_17

    const/4 v0, 0x1

    .line 299
    :goto_16
    return v0

    :cond_17
    move v0, v1

    .line 295
    goto :goto_16

    :cond_19
    move v0, v1

    .line 299
    goto :goto_16
.end method

.method private static d()Z
    .registers 1

    .prologue
    .line 219
    const/4 v0, 0x0

    return v0
.end method
