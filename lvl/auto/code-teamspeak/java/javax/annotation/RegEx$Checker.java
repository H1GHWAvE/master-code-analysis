package javax.annotation;
public final class RegEx$Checker implements javax.annotation.meta.TypeQualifierValidator {

    public RegEx$Checker()
    {
        return;
    }

    private static javax.annotation.meta.When forConstantValue$78dac8c8(Object p1)
    {
        javax.annotation.meta.When v0_1;
        if ((p1 instanceof String)) {
            try {
                java.util.regex.Pattern.compile(((String) p1));
                v0_1 = javax.annotation.meta.When.ALWAYS;
            } catch (javax.annotation.meta.When v0) {
                v0_1 = javax.annotation.meta.When.NEVER;
            }
        } else {
            v0_1 = javax.annotation.meta.When.NEVER;
        }
        return v0_1;
    }

    public final synthetic javax.annotation.meta.When forConstantValue(otation.Annotation p2, Object p3)
    {
        return javax.annotation.RegEx$Checker.forConstantValue$78dac8c8(p3);
    }
}
