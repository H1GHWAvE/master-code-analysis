package com.a.d.a;
final enum class b extends java.lang.Enum {
    public static final enum com.a.d.a.b a;
    public static final enum com.a.d.a.b b;
    private static final synthetic com.a.d.a.b[] e;
    private final char c;
    private final char d;

    static b()
    {
        com.a.d.a.b.a = new com.a.d.a.b("PRIVATE", 0, 58, 44);
        com.a.d.a.b.b = new com.a.d.a.b("ICANN", 1, 33, 63);
        com.a.d.a.b[] v0_5 = new com.a.d.a.b[2];
        v0_5[0] = com.a.d.a.b.a;
        v0_5[1] = com.a.d.a.b.b;
        com.a.d.a.b.e = v0_5;
        return;
    }

    private b(String p1, int p2, char p3, char p4)
    {
        this(p1, p2);
        this.c = p3;
        this.d = p4;
        return;
    }

    private char a()
    {
        return this.d;
    }

    static com.a.d.a.b a(char p5)
    {
        String v1_0 = com.a.d.a.b.values();
        int v0_0 = 0;
        while (v0_0 < v1_0.length) {
            com.a.d.a.b v3 = v1_0[v0_0];
            if ((v3.c != p5) && (v3.d != p5)) {
                v0_0++;
            } else {
                return v3;
            }
        }
        throw new IllegalArgumentException(new StringBuilder(38).append("No enum corresponding to given code: ").append(p5).toString());
    }

    private static com.a.d.a.b a(boolean p1)
    {
        com.a.d.a.b v0;
        if (!p1) {
            v0 = com.a.d.a.b.b;
        } else {
            v0 = com.a.d.a.b.a;
        }
        return v0;
    }

    private char b()
    {
        return this.c;
    }

    public static com.a.d.a.b valueOf(String p1)
    {
        return ((com.a.d.a.b) Enum.valueOf(com.a.d.a.b, p1));
    }

    public static com.a.d.a.b[] values()
    {
        return ((com.a.d.a.b[]) com.a.d.a.b.e.clone());
    }
}
