package com.a.c;
public abstract enum class d extends java.lang.Enum implements com.a.c.j {
    public static final enum com.a.c.d a;
    public static final enum com.a.c.d b;
    public static final enum com.a.c.d c;
    public static final enum com.a.c.d d;
    public static final enum com.a.c.d e;
    private static final synthetic com.a.c.d[] f;

    static d()
    {
        com.a.c.d.a = new com.a.c.e("IDENTITY");
        com.a.c.d.b = new com.a.c.f("UPPER_CAMEL_CASE");
        com.a.c.d.c = new com.a.c.g("UPPER_CAMEL_CASE_WITH_SPACES");
        com.a.c.d.d = new com.a.c.h("LOWER_CASE_WITH_UNDERSCORES");
        com.a.c.d.e = new com.a.c.i("LOWER_CASE_WITH_DASHES");
        com.a.c.d[] v0_11 = new com.a.c.d[5];
        v0_11[0] = com.a.c.d.a;
        v0_11[1] = com.a.c.d.b;
        v0_11[2] = com.a.c.d.c;
        v0_11[3] = com.a.c.d.d;
        v0_11[4] = com.a.c.d.e;
        com.a.c.d.f = v0_11;
        return;
    }

    private d(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic d(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    private static String a(char p2, String p3, int p4)
    {
        String v0_1;
        if (p4 >= p3.length()) {
            v0_1 = String.valueOf(p2);
        } else {
            v0_1 = new StringBuilder().append(p2).append(p3.substring(p4)).toString();
        }
        return v0_1;
    }

    static synthetic String a(String p4)
    {
        String v1_0 = 0;
        StringBuilder v2_1 = new StringBuilder();
        StringBuilder v0_0 = p4.charAt(0);
        while ((v1_0 < (p4.length() - 1)) && (!Character.isLetter(v0_0))) {
            v2_1.append(v0_0);
            v1_0++;
            v0_0 = p4.charAt(v1_0);
        }
        if (v1_0 != p4.length()) {
            if (!Character.isUpperCase(v0_0)) {
                StringBuilder v0_2;
                StringBuilder v0_1 = Character.toUpperCase(v0_0);
                String v1_1 = (v1_0 + 1);
                if (v1_1 >= p4.length()) {
                    v0_2 = String.valueOf(v0_1);
                } else {
                    v0_2 = new StringBuilder().append(v0_1).append(p4.substring(v1_1)).toString();
                }
                p4 = v2_1.append(v0_2).toString();
            }
        } else {
            p4 = v2_1.toString();
        }
        return p4;
    }

    static synthetic String a(String p4, String p5)
    {
        StringBuilder v1_1 = new StringBuilder();
        int v0_0 = 0;
        while (v0_0 < p4.length()) {
            char v2_1 = p4.charAt(v0_0);
            if ((Character.isUpperCase(v2_1)) && (v1_1.length() != 0)) {
                v1_1.append(p5);
            }
            v1_1.append(v2_1);
            v0_0++;
        }
        return v1_1.toString();
    }

    private static String b(String p4)
    {
        String v1_0 = 0;
        StringBuilder v2_1 = new StringBuilder();
        StringBuilder v0_0 = p4.charAt(0);
        while ((v1_0 < (p4.length() - 1)) && (!Character.isLetter(v0_0))) {
            v2_1.append(v0_0);
            v1_0++;
            v0_0 = p4.charAt(v1_0);
        }
        if (v1_0 != p4.length()) {
            if (!Character.isUpperCase(v0_0)) {
                StringBuilder v0_2;
                StringBuilder v0_1 = Character.toUpperCase(v0_0);
                String v1_1 = (v1_0 + 1);
                if (v1_1 >= p4.length()) {
                    v0_2 = String.valueOf(v0_1);
                } else {
                    v0_2 = new StringBuilder().append(v0_1).append(p4.substring(v1_1)).toString();
                }
                p4 = v2_1.append(v0_2).toString();
            }
        } else {
            p4 = v2_1.toString();
        }
        return p4;
    }

    private static String b(String p4, String p5)
    {
        StringBuilder v1_1 = new StringBuilder();
        int v0_0 = 0;
        while (v0_0 < p4.length()) {
            char v2_1 = p4.charAt(v0_0);
            if ((Character.isUpperCase(v2_1)) && (v1_1.length() != 0)) {
                v1_1.append(p5);
            }
            v1_1.append(v2_1);
            v0_0++;
        }
        return v1_1.toString();
    }

    public static com.a.c.d valueOf(String p1)
    {
        return ((com.a.c.d) Enum.valueOf(com.a.c.d, p1));
    }

    public static com.a.c.d[] values()
    {
        return ((com.a.c.d[]) com.a.c.d.f.clone());
    }
}
