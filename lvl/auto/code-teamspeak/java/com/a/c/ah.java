package com.a.c;
public abstract enum class ah extends java.lang.Enum {
    public static final enum com.a.c.ah a;
    public static final enum com.a.c.ah b;
    private static final synthetic com.a.c.ah[] c;

    static ah()
    {
        com.a.c.ah.a = new com.a.c.ai("DEFAULT");
        com.a.c.ah.b = new com.a.c.aj("STRING");
        com.a.c.ah[] v0_5 = new com.a.c.ah[2];
        v0_5[0] = com.a.c.ah.a;
        v0_5[1] = com.a.c.ah.b;
        com.a.c.ah.c = v0_5;
        return;
    }

    private ah(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic ah(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.c.ah valueOf(String p1)
    {
        return ((com.a.c.ah) Enum.valueOf(com.a.c.ah, p1));
    }

    public static com.a.c.ah[] values()
    {
        return ((com.a.c.ah[]) com.a.c.ah.c.clone());
    }

    public abstract com.a.c.w a();
}
