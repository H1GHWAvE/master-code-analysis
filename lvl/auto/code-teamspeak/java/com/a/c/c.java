package com.a.c;
public final class c {
    private final reflect.Field a;

    public c(reflect.Field p1)
    {
        com.a.c.b.a.a(p1);
        this.a = p1;
        return;
    }

    private Class a()
    {
        return this.a.getDeclaringClass();
    }

    private Object a(Object p2)
    {
        return this.a.get(p2);
    }

    private otation.Annotation a(Class p2)
    {
        return this.a.getAnnotation(p2);
    }

    private boolean a(int p2)
    {
        int v0_3;
        if ((this.a.getModifiers() & p2) == 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private String b()
    {
        return this.a.getName();
    }

    private reflect.Type c()
    {
        return this.a.getGenericType();
    }

    private Class d()
    {
        return this.a.getType();
    }

    private java.util.Collection e()
    {
        return java.util.Arrays.asList(this.a.getAnnotations());
    }

    private boolean f()
    {
        return this.a.isSynthetic();
    }
}
