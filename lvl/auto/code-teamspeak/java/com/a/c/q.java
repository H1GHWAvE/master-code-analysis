package com.a.c;
final class q extends com.a.c.an {
    com.a.c.an a;

    q()
    {
        return;
    }

    private void a(com.a.c.an p2)
    {
        if (this.a == null) {
            this.a = p2;
            return;
        } else {
            throw new AssertionError();
        }
    }

    public final Object a(com.a.c.d.a p2)
    {
        if (this.a != null) {
            return this.a.a(p2);
        } else {
            throw new IllegalStateException();
        }
    }

    public final void a(com.a.c.d.e p2, Object p3)
    {
        if (this.a != null) {
            this.a.a(p2, p3);
            return;
        } else {
            throw new IllegalStateException();
        }
    }
}
