package com.a.c.b;
public final class ag extends java.util.AbstractMap implements java.io.Serializable {
    static final synthetic boolean f;
    private static final java.util.Comparator g;
    java.util.Comparator a;
    com.a.c.b.an b;
    int c;
    int d;
    final com.a.c.b.an e;
    private com.a.c.b.ai h;
    private com.a.c.b.ak i;

    static ag()
    {
        com.a.c.b.ah v0_2;
        if (com.a.c.b.ag.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.a.c.b.ag.f = v0_2;
        com.a.c.b.ag.g = new com.a.c.b.ah();
        return;
    }

    public ag()
    {
        this(com.a.c.b.ag.g);
        return;
    }

    private ag(java.util.Comparator p2)
    {
        this.c = 0;
        this.d = 0;
        this.e = new com.a.c.b.an();
        if (p2 == null) {
            p2 = com.a.c.b.ag.g;
        }
        this.a = p2;
        return;
    }

    private com.a.c.b.an a(Object p8, boolean p9)
    {
        int v2_1;
        com.a.c.b.an v3_0;
        int v1_0 = 0;
        java.util.Comparator v5 = this.a;
        int v2_0 = this.b;
        if (v2_0 == 0) {
            v3_0 = v2_0;
            v2_1 = 0;
            if (p9) {
                ClassCastException v0_5;
                int v1_1 = this.e;
                if (v3_0 != null) {
                    v0_5 = new com.a.c.b.an(v3_0, p8, v1_1, v1_1.e);
                    if (v2_1 >= 0) {
                        v3_0.c = v0_5;
                    } else {
                        v3_0.b = v0_5;
                    }
                    this.b(v3_0, 1);
                } else {
                    if ((v5 != com.a.c.b.ag.g) || ((p8 instanceof Comparable))) {
                        v0_5 = new com.a.c.b.an(v3_0, p8, v1_1, v1_1.e);
                        this.b = v0_5;
                    } else {
                        throw new ClassCastException(new StringBuilder().append(p8.getClass().getName()).append(" is not Comparable").toString());
                    }
                }
                this.c = (this.c + 1);
                this.d = (this.d + 1);
                v1_0 = v0_5;
            }
        } else {
            ClassCastException v0_2;
            if (v5 != com.a.c.b.ag.g) {
                v0_2 = 0;
            } else {
                v0_2 = ((Comparable) p8);
            }
            while(true) {
                com.a.c.b.an v3_2;
                if (v0_2 == null) {
                    v3_2 = v5.compare(p8, v2_0.f);
                } else {
                    v3_2 = v0_2.compareTo(v2_0.f);
                }
                if (v3_2 != null) {
                    com.a.c.b.an v4_0;
                    if (v3_2 >= null) {
                        v4_0 = v2_0.c;
                    } else {
                        v4_0 = v2_0.b;
                    }
                    if (v4_0 == null) {
                        break;
                    }
                    v2_0 = v4_0;
                } else {
                    v1_0 = v2_0;
                }
            }
            v3_0 = v2_0;
            v2_1 = v3_2;
        }
        return v1_0;
    }

    private Object a()
    {
        return new java.util.LinkedHashMap(this);
    }

    private void a(com.a.c.b.an p7)
    {
        int v1 = 0;
        int v0_0 = p7.b;
        com.a.c.b.an v3 = p7.c;
        com.a.c.b.an v4 = v3.b;
        com.a.c.b.an v5 = v3.c;
        p7.c = v4;
        if (v4 != null) {
            v4.a = p7;
        }
        int v2;
        this.a(p7, v3);
        v3.b = p7;
        p7.a = v3;
        if (v0_0 == 0) {
            v2 = 0;
        } else {
            v2 = v0_0.h;
        }
        int v0_2;
        if (v4 == null) {
            v0_2 = 0;
        } else {
            v0_2 = v4.h;
        }
        p7.h = (Math.max(v2, v0_2) + 1);
        if (v5 != null) {
            v1 = v5.h;
        }
        v3.h = (Math.max(p7.h, v1) + 1);
        return;
    }

    private void a(com.a.c.b.an p3, com.a.c.b.an p4)
    {
        AssertionError v0_0 = p3.a;
        p3.a = 0;
        if (p4 != null) {
            p4.a = v0_0;
        }
        if (v0_0 == null) {
            this.b = p4;
        } else {
            if (v0_0.b != p3) {
                if ((com.a.c.b.ag.f) || (v0_0.c == p3)) {
                    v0_0.c = p4;
                } else {
                    throw new AssertionError();
                }
            } else {
                v0_0.b = p4;
            }
        }
        return;
    }

    private static boolean a(Object p1, Object p2)
    {
        if ((p1 != p2) && ((p1 == null) || (!p1.equals(p2)))) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private com.a.c.b.an b(Object p3)
    {
        com.a.c.b.an v0 = 0;
        if (p3 != null) {
            try {
                v0 = this.a(p3, 0);
            } catch (ClassCastException v1) {
            }
        }
        return v0;
    }

    private void b(com.a.c.b.an p7)
    {
        int v1 = 0;
        com.a.c.b.an v3 = p7.b;
        int v0_0 = p7.c;
        com.a.c.b.an v4 = v3.b;
        com.a.c.b.an v5 = v3.c;
        p7.b = v5;
        if (v5 != null) {
            v5.a = p7;
        }
        int v2;
        this.a(p7, v3);
        v3.c = p7;
        p7.a = v3;
        if (v0_0 == 0) {
            v2 = 0;
        } else {
            v2 = v0_0.h;
        }
        int v0_2;
        if (v5 == null) {
            v0_2 = 0;
        } else {
            v0_2 = v5.h;
        }
        p7.h = (Math.max(v2, v0_2) + 1);
        if (v4 != null) {
            v1 = v4.h;
        }
        v3.h = (Math.max(p7.h, v1) + 1);
        return;
    }

    private void b(com.a.c.b.an p10, boolean p11)
    {
        while (p10 != null) {
            boolean v2_0;
            boolean v3_0 = p10.b;
            com.a.c.b.an v4_0 = p10.c;
            if (!v3_0) {
                v2_0 = 0;
            } else {
                v2_0 = v3_0.h;
            }
            AssertionError v0_1;
            if (v4_0 == null) {
                v0_1 = 0;
            } else {
                v0_1 = v4_0.h;
            }
            int v5 = (v2_0 - v0_1);
            if (v5 != -2) {
                if (v5 != 2) {
                    if (v5 != 0) {
                        if ((com.a.c.b.ag.f) || ((v5 == -1) || (v5 == 1))) {
                            p10.h = (Math.max(v2_0, v0_1) + 1);
                            if (!p11) {
                                break;
                            }
                        } else {
                            throw new AssertionError();
                        }
                    } else {
                        p10.h = (v2_0 + 1);
                        if (p11) {
                            break;
                        }
                    }
                } else {
                    boolean v2_1;
                    com.a.c.b.an v4_2 = v3_0.b;
                    AssertionError v0_7 = v3_0.c;
                    if (v0_7 == null) {
                        v2_1 = 0;
                    } else {
                        v2_1 = v0_7.h;
                    }
                    AssertionError v0_9;
                    if (v4_2 == null) {
                        v0_9 = 0;
                    } else {
                        v0_9 = v4_2.h;
                    }
                    AssertionError v0_10 = (v0_9 - v2_1);
                    if ((v0_10 != 1) && ((v0_10 != null) || (p11))) {
                        if ((com.a.c.b.ag.f) || (v0_10 == -1)) {
                            this.a(v3_0);
                            this.b(p10);
                        } else {
                            throw new AssertionError();
                        }
                    } else {
                        this.b(p10);
                    }
                    if (p11) {
                        break;
                    }
                }
            } else {
                boolean v2_3;
                boolean v3_2 = v4_0.b;
                AssertionError v0_13 = v4_0.c;
                if (v0_13 == null) {
                    v2_3 = 0;
                } else {
                    v2_3 = v0_13.h;
                }
                AssertionError v0_15;
                if (!v3_2) {
                    v0_15 = 0;
                } else {
                    v0_15 = v3_2.h;
                }
                AssertionError v0_16 = (v0_15 - v2_3);
                if ((v0_16 != -1) && ((v0_16 != null) || (p11))) {
                    if ((com.a.c.b.ag.f) || (v0_16 == 1)) {
                        this.b(v4_0);
                        this.a(p10);
                    } else {
                        throw new AssertionError();
                    }
                } else {
                    this.a(p10);
                }
                if (p11) {
                    break;
                }
            }
            p10 = p10.a;
        }
        return;
    }

    final com.a.c.b.an a(Object p3)
    {
        com.a.c.b.an v0 = this.b(p3);
        if (v0 != null) {
            this.a(v0, 1);
        }
        return v0;
    }

    final com.a.c.b.an a(java.util.Map$Entry p6)
    {
        int v1 = 1;
        int v0_1 = this.b(p6.getKey());
        if (v0_1 == 0) {
            v1 = 0;
        } else {
            int v3_2;
            int v3_0 = v0_1.g;
            Object v4 = p6.getValue();
            if ((v3_0 != v4) && ((v3_0 == 0) || (!v3_0.equals(v4)))) {
                v3_2 = 0;
            } else {
                v3_2 = 1;
            }
            if (v3_2 == 0) {
            }
        }
        if (v1 == 0) {
            v0_1 = 0;
        }
        return v0_1;
    }

    final void a(com.a.c.b.an p8, boolean p9)
    {
        int v2 = 0;
        if (p9) {
            p8.e.d = p8.d;
            p8.d.e = p8.e;
        }
        int v1_2 = p8.b;
        com.a.c.b.an v0_2 = p8.c;
        com.a.c.b.an v3_0 = p8.a;
        if ((v1_2 == 0) || (v0_2 == null)) {
            if (v1_2 == 0) {
                if (v0_2 == null) {
                    this.a(p8, 0);
                } else {
                    this.a(p8, v0_2);
                    p8.c = 0;
                }
            } else {
                this.a(p8, v1_2);
                p8.b = 0;
            }
            this.b(v3_0, 0);
            this.c = (this.c - 1);
            this.d = (this.d + 1);
        } else {
            if (v1_2.h <= v0_2.h) {
                while(true) {
                    int v1_3 = v0_2.b;
                    if (v1_3 == 0) {
                        break;
                    }
                    v0_2 = v1_3;
                }
            } else {
                v0_2 = v1_2;
                int v1_4 = v1_2.c;
                while (v1_4 != 0) {
                    v0_2 = v1_4;
                    v1_4 = v1_4.c;
                }
            }
            int v1_5;
            this.a(v0_2, 0);
            com.a.c.b.an v3_2 = p8.b;
            if (v3_2 == null) {
                v1_5 = 0;
            } else {
                v1_5 = v3_2.h;
                v0_2.b = v3_2;
                v3_2.a = v0_2;
                p8.b = 0;
            }
            com.a.c.b.an v3_3 = p8.c;
            if (v3_3 != null) {
                v2 = v3_3.h;
                v0_2.c = v3_3;
                v3_3.a = v0_2;
                p8.c = 0;
            }
            v0_2.h = (Math.max(v1_5, v2) + 1);
            this.a(p8, v0_2);
        }
        return;
    }

    public final void clear()
    {
        this.b = 0;
        this.c = 0;
        this.d = (this.d + 1);
        com.a.c.b.an v0_4 = this.e;
        v0_4.e = v0_4;
        v0_4.d = v0_4;
        return;
    }

    public final boolean containsKey(Object p2)
    {
        int v0_1;
        if (this.b(p2) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final java.util.Set entrySet()
    {
        com.a.c.b.ai v0_0 = this.h;
        if (v0_0 == null) {
            v0_0 = new com.a.c.b.ai(this);
            this.h = v0_0;
        }
        return v0_0;
    }

    public final Object get(Object p2)
    {
        int v0_1;
        int v0_0 = this.b(p2);
        if (v0_0 == 0) {
            v0_1 = 0;
        } else {
            v0_1 = v0_0.g;
        }
        return v0_1;
    }

    public final java.util.Set keySet()
    {
        com.a.c.b.ak v0_0 = this.i;
        if (v0_0 == null) {
            v0_0 = new com.a.c.b.ak(this);
            this.i = v0_0;
        }
        return v0_0;
    }

    public final Object put(Object p3, Object p4)
    {
        if (p3 != null) {
            com.a.c.b.an v0_1 = this.a(p3, 1);
            Object v1_0 = v0_1.g;
            v0_1.g = p4;
            return v1_0;
        } else {
            throw new NullPointerException("key == null");
        }
    }

    public final Object remove(Object p2)
    {
        int v0_1;
        int v0_0 = this.a(p2);
        if (v0_0 == 0) {
            v0_1 = 0;
        } else {
            v0_1 = v0_0.g;
        }
        return v0_1;
    }

    public final int size()
    {
        return this.c;
    }
}
