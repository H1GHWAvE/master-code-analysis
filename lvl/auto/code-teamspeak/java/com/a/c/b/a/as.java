package com.a.c.b.a;
final class as extends com.a.c.an {

    as()
    {
        return;
    }

    private void a(com.a.c.d.e p4, com.a.c.w p5)
    {
        if ((p5 != null) && (!(p5 instanceof com.a.c.y))) {
            if (!(p5 instanceof com.a.c.ac)) {
                if (!(p5 instanceof com.a.c.t)) {
                    if (!(p5 instanceof com.a.c.z)) {
                        throw new IllegalArgumentException(new StringBuilder("Couldn\'t write ").append(p5.getClass()).toString());
                    } else {
                        p4.d();
                        if (!(p5 instanceof com.a.c.z)) {
                            throw new IllegalStateException(new StringBuilder("Not a JSON Object: ").append(p5).toString());
                        } else {
                            String v2_3 = ((com.a.c.z) p5).a.entrySet().iterator();
                            while (v2_3.hasNext()) {
                                com.a.c.w v0_13 = ((java.util.Map$Entry) v2_3.next());
                                p4.a(((String) v0_13.getKey()));
                                this.a(p4, ((com.a.c.w) v0_13.getValue()));
                            }
                            p4.e();
                        }
                    }
                } else {
                    p4.b();
                    if (!(p5 instanceof com.a.c.t)) {
                        throw new IllegalStateException("This is not a JSON Array.");
                    } else {
                        String v1_11 = ((com.a.c.t) p5).iterator();
                        while (v1_11.hasNext()) {
                            this.a(p4, ((com.a.c.w) v1_11.next()));
                        }
                        p4.c();
                    }
                }
            } else {
                com.a.c.w v0_22 = p5.n();
                if (!(v0_22.a instanceof Number)) {
                    if (!(v0_22.a instanceof Boolean)) {
                        p4.b(v0_22.b());
                    } else {
                        p4.a(v0_22.l());
                    }
                } else {
                    p4.a(v0_22.a());
                }
            }
        } else {
            p4.f();
        }
        return;
    }

    private com.a.c.w b(com.a.c.d.a p4)
    {
        com.a.c.z v0_3;
        switch (com.a.c.b.a.ba.a[p4.f().ordinal()]) {
            case 1:
                v0_3 = new com.a.c.ac(new com.a.c.b.v(p4.i()));
                break;
            case 2:
                v0_3 = new com.a.c.ac(Boolean.valueOf(p4.j()));
                break;
            case 3:
                v0_3 = new com.a.c.ac(p4.i());
                break;
            case 4:
                p4.k();
                v0_3 = com.a.c.y.a;
                break;
            case 5:
                v0_3 = new com.a.c.t();
                p4.a();
                while (p4.e()) {
                    v0_3.a(this.b(p4));
                }
                p4.b();
                break;
            case 6:
                v0_3 = new com.a.c.z();
                p4.c();
                while (p4.e()) {
                    v0_3.a(p4.h(), this.b(p4));
                }
                p4.d();
                break;
            default:
                throw new IllegalArgumentException();
        }
        return v0_3;
    }

    public final synthetic Object a(com.a.c.d.a p2)
    {
        return this.b(p2);
    }

    public final bridge synthetic void a(com.a.c.d.e p1, Object p2)
    {
        this.a(p1, ((com.a.c.w) p2));
        return;
    }
}
