package com.a.c.b;
final class aa extends java.util.AbstractSet {
    final synthetic com.a.c.b.w a;

    aa(com.a.c.b.w p1)
    {
        this.a = p1;
        return;
    }

    public final void clear()
    {
        this.a.clear();
        return;
    }

    public final boolean contains(Object p2)
    {
        if ((!(p2 instanceof java.util.Map$Entry)) || (this.a.a(((java.util.Map$Entry) p2)) == null)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.c.b.ab(this);
    }

    public final boolean remove(Object p4)
    {
        int v0_0 = 0;
        if ((p4 instanceof java.util.Map$Entry)) {
            com.a.c.b.af v2_2 = this.a.a(((java.util.Map$Entry) p4));
            if (v2_2 != null) {
                this.a.a(v2_2, 1);
                v0_0 = 1;
            }
        }
        return v0_0;
    }

    public final int size()
    {
        return this.a.d;
    }
}
