package com.a.c.b.a;
public final class e extends com.a.c.an {
    public static final com.a.c.ap a;
    private final java.text.DateFormat b;
    private final java.text.DateFormat c;
    private final java.text.DateFormat d;

    static e()
    {
        com.a.c.b.a.e.a = new com.a.c.b.a.f();
        return;
    }

    public e()
    {
        this.b = java.text.DateFormat.getDateTimeInstance(2, 2, java.util.Locale.US);
        this.c = java.text.DateFormat.getDateTimeInstance(2, 2);
        java.text.SimpleDateFormat v0_4 = new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss\'Z\'", java.util.Locale.US);
        v0_4.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
        this.d = v0_4;
        return;
    }

    private static java.text.DateFormat a()
    {
        java.text.SimpleDateFormat v0_1 = new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss\'Z\'", java.util.Locale.US);
        v0_1.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
        return v0_1;
    }

    private declared_synchronized java.util.Date a(String p3)
    {
        try {
            java.text.ParseException v0_1 = this.c.parse(p3);
        } catch (java.text.ParseException v0_5) {
            throw v0_5;
        } catch (java.text.ParseException v0) {
            try {
                v0_1 = this.b.parse(p3);
            } catch (java.text.ParseException v0) {
                try {
                    v0_1 = this.d.parse(p3);
                } catch (java.text.ParseException v0_4) {
                    throw new com.a.c.ag(p3, v0_4);
                }
            }
        }
        return v0_1;
    }

    private declared_synchronized void a(com.a.c.d.e p2, java.util.Date p3)
    {
        try {
            if (p3 != null) {
                p2.b(this.b.format(p3));
            } else {
                p2.f();
            }
        } catch (Throwable v0_2) {
            throw v0_2;
        }
        return;
    }

    private java.util.Date b(com.a.c.d.a p3)
    {
        java.util.Date v0_2;
        if (p3.f() != com.a.c.d.d.i) {
            v0_2 = this.a(p3.i());
        } else {
            p3.k();
            v0_2 = 0;
        }
        return v0_2;
    }

    public final synthetic Object a(com.a.c.d.a p3)
    {
        java.util.Date v0_2;
        if (p3.f() != com.a.c.d.d.i) {
            v0_2 = this.a(p3.i());
        } else {
            p3.k();
            v0_2 = 0;
        }
        return v0_2;
    }

    public final bridge synthetic void a(com.a.c.d.e p1, Object p2)
    {
        this.a(p1, ((java.util.Date) p2));
        return;
    }
}
