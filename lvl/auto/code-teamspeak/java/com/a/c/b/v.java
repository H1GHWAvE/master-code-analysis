package com.a.c.b;
public final class v extends java.lang.Number {
    private final String a;

    public v(String p1)
    {
        this.a = p1;
        return;
    }

    private Object a()
    {
        return new java.math.BigDecimal(this.a);
    }

    public final double doubleValue()
    {
        return Double.parseDouble(this.a);
    }

    public final float floatValue()
    {
        return Float.parseFloat(this.a);
    }

    public final int intValue()
    {
        try {
            int v0_1 = Integer.parseInt(this.a);
        } catch (int v0) {
            try {
                v0_1 = ((int) Long.parseLong(this.a));
            } catch (int v0) {
                v0_1 = new java.math.BigDecimal(this.a).intValue();
            }
        }
        return v0_1;
    }

    public final long longValue()
    {
        try {
            long v0_1 = Long.parseLong(this.a);
        } catch (long v0) {
            v0_1 = new java.math.BigDecimal(this.a).longValue();
        }
        return v0_1;
    }

    public final String toString()
    {
        return this.a;
    }
}
