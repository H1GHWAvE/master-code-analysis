package com.a.c.b;
final class e implements java.io.Serializable, java.lang.reflect.WildcardType {
    private static final long c;
    private final reflect.Type a;
    private final reflect.Type b;

    public e(reflect.Type[] p5, reflect.Type[] p6)
    {
        Class v0_1;
        int v1 = 1;
        if (p6.length > 1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Class v0_3;
        com.a.c.b.a.a(v0_1);
        if (p5.length != 1) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        com.a.c.b.a.a(v0_3);
        if (p6.length != 1) {
            com.a.c.b.a.a(p5[0]);
            com.a.c.b.b.e(p5[0]);
            this.b = 0;
            this.a = com.a.c.b.b.a(p5[0]);
        } else {
            com.a.c.b.a.a(p6[0]);
            com.a.c.b.b.e(p6[0]);
            if (p5[0] != Object) {
                v1 = 0;
            }
            com.a.c.b.a.a(v1);
            this.b = com.a.c.b.b.a(p6[0]);
            this.a = Object;
        }
        return;
    }

    public final boolean equals(Object p2)
    {
        if ((!(p2 instanceof reflect.WildcardType)) || (!com.a.c.b.b.a(this, ((reflect.WildcardType) p2)))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final reflect.Type[] getLowerBounds()
    {
        reflect.Type[] v0_1;
        if (this.b == null) {
            v0_1 = com.a.c.b.b.a;
        } else {
            v0_1 = new reflect.Type[1];
            v0_1[0] = this.b;
        }
        return v0_1;
    }

    public final reflect.Type[] getUpperBounds()
    {
        reflect.Type[] v0_1 = new reflect.Type[1];
        v0_1[0] = this.a;
        return v0_1;
    }

    public final int hashCode()
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 1;
        } else {
            v0_1 = (this.b.hashCode() + 31);
        }
        return (v0_1 ^ (this.a.hashCode() + 31));
    }

    public final String toString()
    {
        String v0_5;
        if (this.b == null) {
            if (this.a != Object) {
                v0_5 = new StringBuilder("? extends ").append(com.a.c.b.b.c(this.a)).toString();
            } else {
                v0_5 = "?";
            }
        } else {
            v0_5 = new StringBuilder("? super ").append(com.a.c.b.b.c(this.b)).toString();
        }
        return v0_5;
    }
}
