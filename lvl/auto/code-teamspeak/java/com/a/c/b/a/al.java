package com.a.c.b.a;
final class al extends com.a.c.an {

    al()
    {
        return;
    }

    private static void a(com.a.c.d.e p4, java.util.BitSet p5)
    {
        if (p5 != null) {
            p4.b();
            int v0 = 0;
            while (v0 < p5.length()) {
                long v2_2;
                if (!p5.get(v0)) {
                    v2_2 = 0;
                } else {
                    v2_2 = 1;
                }
                p4.a(((long) v2_2));
                v0++;
            }
            p4.c();
        } else {
            p4.f();
        }
        return;
    }

    private static java.util.BitSet b(com.a.c.d.a p7)
    {
        StringBuilder v0_2;
        if (p7.f() != com.a.c.d.d.i) {
            java.util.BitSet v4_1 = new java.util.BitSet();
            p7.a();
            StringBuilder v0_1 = p7.f();
            com.a.c.ag v1_1 = 0;
            while (v0_1 != com.a.c.d.d.b) {
                StringBuilder v0_7;
                switch (com.a.c.b.a.ba.a[v0_1.ordinal()]) {
                    case 1:
                        if (p7.n() == 0) {
                            v0_7 = 0;
                        } else {
                            v0_7 = 1;
                        }
                        break;
                    case 2:
                        v0_7 = p7.j();
                        break;
                    case 3:
                        try {
                            StringBuilder v0_4 = Integer.parseInt(p7.i());
                        } catch (com.a.c.ag v1) {
                            throw new com.a.c.ag(new StringBuilder("Error: Expecting: bitset number value (1, 0), Found: ").append(v0_4).toString());
                        }
                        if (v0_4 == null) {
                            v0_7 = 0;
                        } else {
                            v0_7 = 1;
                        }
                        break;
                    default:
                        throw new com.a.c.ag(new StringBuilder("Invalid bitset value type: ").append(v0_1).toString());
                }
                if (v0_7 != null) {
                    v4_1.set(v1_1);
                }
                v1_1++;
                v0_1 = p7.f();
            }
            p7.b();
            v0_2 = v4_1;
        } else {
            p7.k();
            v0_2 = 0;
        }
        return v0_2;
    }

    public final synthetic Object a(com.a.c.d.a p2)
    {
        return com.a.c.b.a.al.b(p2);
    }

    public final synthetic void a(com.a.c.d.e p5, Object p6)
    {
        if (((java.util.BitSet) p6) != null) {
            p5.b();
            int v0 = 0;
            while (v0 < ((java.util.BitSet) p6).length()) {
                long v2_2;
                if (!((java.util.BitSet) p6).get(v0)) {
                    v2_2 = 0;
                } else {
                    v2_2 = 1;
                }
                p5.a(((long) v2_2));
                v0++;
            }
            p5.c();
        } else {
            p5.f();
        }
        return;
    }
}
