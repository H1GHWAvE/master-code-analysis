package com.a.c.b;
public final class s implements com.a.c.ap, java.lang.Cloneable {
    public static final com.a.c.b.s a = None;
    private static final double h = 49136;
    public double b;
    public int c;
    public boolean d;
    public boolean e;
    public java.util.List f;
    public java.util.List g;

    static s()
    {
        com.a.c.b.s.a = new com.a.c.b.s();
        return;
    }

    public s()
    {
        this.b = -1.0;
        this.c = 136;
        this.d = 1;
        this.f = java.util.Collections.emptyList();
        this.g = java.util.Collections.emptyList();
        return;
    }

    private com.a.c.b.s a(double p2)
    {
        com.a.c.b.s v0 = this.a();
        v0.b = p2;
        return v0;
    }

    private varargs com.a.c.b.s a(int[] p6)
    {
        int v0 = 0;
        com.a.c.b.s v1 = this.a();
        v1.c = 0;
        int v2 = p6.length;
        while (v0 < v2) {
            v1.c = (p6[v0] | v1.c);
            v0++;
        }
        return v1;
    }

    private boolean a(com.a.c.a.d p5)
    {
        if ((p5 == null) || (p5.a() <= this.b)) {
            int v0_2 = 1;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private boolean a(com.a.c.a.e p5)
    {
        if ((p5 == null) || (p5.a() > this.b)) {
            int v0_2 = 1;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public static boolean a(Class p1)
    {
        if ((Enum.isAssignableFrom(p1)) || ((!p1.isAnonymousClass()) && (!p1.isLocalClass()))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    private boolean a(reflect.Field p7, boolean p8)
    {
        int v0_22;
        if ((this.c & p7.getModifiers()) == 0) {
            if ((this.b == -1.0) || (this.a(((com.a.c.a.d) p7.getAnnotation(com.a.c.a.d)), ((com.a.c.a.e) p7.getAnnotation(com.a.c.a.e))))) {
                if (!p7.isSynthetic()) {
                    if (this.e) {
                        int v0_12 = ((com.a.c.a.a) p7.getAnnotation(com.a.c.a.a));
                        if (v0_12 != 0) {
                            if (!p8) {
                                if (v0_12.b()) {
                                    if ((this.d) || (!com.a.c.b.s.b(p7.getType()))) {
                                        if (!com.a.c.b.s.a(p7.getType())) {
                                            int v0_20;
                                            if (!p8) {
                                                v0_20 = this.g;
                                            } else {
                                                v0_20 = this.f;
                                            }
                                            if (!v0_20.isEmpty()) {
                                                new com.a.c.c(p7);
                                                java.util.Iterator v1_6 = v0_20.iterator();
                                                while (v1_6.hasNext()) {
                                                    if (((com.a.c.b) v1_6.next()).a()) {
                                                        v0_22 = 1;
                                                        return v0_22;
                                                    }
                                                    v0_22 = 1;
                                                    return v0_22;
                                                }
                                            }
                                            v0_22 = 0;
                                            return v0_22;
                                        } else {
                                            v0_22 = 1;
                                            return v0_22;
                                        }
                                    } else {
                                        v0_22 = 1;
                                        return v0_22;
                                    }
                                }
                            } else {
                                if (v0_12.a()) {
                                }
                            }
                        }
                    }
                } else {
                    v0_22 = 1;
                }
            } else {
                v0_22 = 1;
            }
        } else {
            v0_22 = 1;
        }
        return v0_22;
    }

    private com.a.c.b.s b()
    {
        com.a.c.b.s v0 = this.a();
        v0.d = 0;
        return v0;
    }

    public static boolean b(Class p3)
    {
        int v0 = 1;
        if (!p3.isMemberClass()) {
            v0 = 0;
        } else {
            int v2_3;
            if ((p3.getModifiers() & 8) == 0) {
                v2_3 = 0;
            } else {
                v2_3 = 1;
            }
            if (v2_3 != 0) {
            }
        }
        return v0;
    }

    private com.a.c.b.s c()
    {
        com.a.c.b.s v0 = this.a();
        v0.e = 1;
        return v0;
    }

    private static boolean c(Class p1)
    {
        int v0_2;
        if ((p1.getModifiers() & 8) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final com.a.c.an a(com.a.c.k p7, com.a.c.c.a p8)
    {
        int v0_2;
        int v0_0 = p8.a;
        boolean v3 = this.a(v0_0, 1);
        boolean v2 = this.a(v0_0, 0);
        if ((v3) || (v2)) {
            v0_2 = new com.a.c.b.t(this, v2, v3, p7, p8);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final com.a.c.b.s a()
    {
        try {
            return ((com.a.c.b.s) super.clone());
        } catch (AssertionError v0) {
            throw new AssertionError();
        }
    }

    public final com.a.c.b.s a(com.a.c.b p4, boolean p5, boolean p6)
    {
        com.a.c.b.s v0 = this.a();
        if (p5) {
            v0.f = new java.util.ArrayList(this.f);
            v0.f.add(p4);
        }
        if (p6) {
            v0.g = new java.util.ArrayList(this.g);
            v0.g.add(p4);
        }
        return v0;
    }

    public final boolean a(com.a.c.a.d p7, com.a.c.a.e p8)
    {
        int v2_2;
        int v0 = 1;
        if ((p7 == null) || (p7.a() <= this.b)) {
            v2_2 = 1;
        } else {
            v2_2 = 0;
        }
        if (v2_2 == 0) {
            v0 = 0;
        } else {
            if ((p8 == null) || (p8.a() > this.b)) {
                int v2_5 = 1;
            } else {
                v2_5 = 0;
            }
            if (v2_5 == 0) {
            }
        }
        return v0;
    }

    public final boolean a(Class p7, boolean p8)
    {
        if ((this.b == -1.0) || (this.a(((com.a.c.a.d) p7.getAnnotation(com.a.c.a.d)), ((com.a.c.a.e) p7.getAnnotation(com.a.c.a.e))))) {
            if ((this.d) || (!com.a.c.b.s.b(p7))) {
                if (!com.a.c.b.s.a(p7)) {
                    int v0_9;
                    if (!p8) {
                        v0_9 = this.g;
                    } else {
                        v0_9 = this.f;
                    }
                    java.util.Iterator v1_3 = v0_9.iterator();
                    while (v1_3.hasNext()) {
                        if (((com.a.c.b) v1_3.next()).b()) {
                            int v0_11 = 1;
                        }
                    }
                    v0_11 = 0;
                } else {
                    v0_11 = 1;
                }
            } else {
                v0_11 = 1;
            }
        } else {
            v0_11 = 1;
        }
        return v0_11;
    }

    protected final synthetic Object clone()
    {
        return this.a();
    }
}
