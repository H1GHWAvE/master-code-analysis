package com.a.c.b.a;
public final class u extends com.a.c.an {
    public static final com.a.c.ap a;
    private final java.text.DateFormat b;

    static u()
    {
        com.a.c.b.a.u.a = new com.a.c.b.a.v();
        return;
    }

    public u()
    {
        this.b = new java.text.SimpleDateFormat("MMM d, yyyy");
        return;
    }

    private declared_synchronized void a(com.a.c.d.e p2, java.sql.Date p3)
    {
        try {
            Throwable v0_1;
            if (p3 != null) {
                v0_1 = this.b.format(p3);
            } else {
                v0_1 = 0;
            }
        } catch (Throwable v0_2) {
            throw v0_2;
        }
        p2.b(v0_1);
        return;
    }

    private declared_synchronized java.sql.Date b(com.a.c.d.a p5)
    {
        try {
            java.text.ParseException v0_4;
            if (p5.f() != com.a.c.d.d.i) {
                try {
                    v0_4 = new java.sql.Date(this.b.parse(p5.i()).getTime());
                } catch (java.text.ParseException v0_5) {
                    throw new com.a.c.ag(v0_5);
                }
            } else {
                p5.k();
                v0_4 = 0;
            }
        } catch (java.text.ParseException v0_6) {
            throw v0_6;
        }
        return v0_4;
    }

    public final synthetic Object a(com.a.c.d.a p2)
    {
        return this.b(p2);
    }

    public final bridge synthetic void a(com.a.c.d.e p1, Object p2)
    {
        this.a(p1, ((java.sql.Date) p2));
        return;
    }
}
