package com.a.c.b.a;
public final class q implements com.a.c.ap {
    private final com.a.c.b.f a;
    private final com.a.c.j b;
    private final com.a.c.b.s c;

    public q(com.a.c.b.f p1, com.a.c.j p2, com.a.c.b.s p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    static synthetic com.a.c.an a(com.a.c.b.a.q p2, com.a.c.k p3, reflect.Field p4, com.a.c.c.a p5)
    {
        com.a.c.an v0_3;
        com.a.c.an v0_2 = ((com.a.c.a.b) p4.getAnnotation(com.a.c.a.b));
        if (v0_2 == null) {
            v0_3 = p3.a(p5);
        } else {
            v0_3 = com.a.c.b.a.g.a(p2.a, p3, p5, v0_2);
            if (v0_3 == null) {
            }
        }
        return v0_3;
    }

    private com.a.c.an a(com.a.c.k p3, reflect.Field p4, com.a.c.c.a p5)
    {
        com.a.c.an v0_3;
        com.a.c.an v0_2 = ((com.a.c.a.b) p4.getAnnotation(com.a.c.a.b));
        if (v0_2 == null) {
            v0_3 = p3.a(p5);
        } else {
            v0_3 = com.a.c.b.a.g.a(this.a, p3, p5, v0_2);
            if (v0_3 == null) {
            }
        }
        return v0_3;
    }

    private com.a.c.b.a.t a(com.a.c.k p10, reflect.Field p11, String p12, com.a.c.c.a p13, boolean p14, boolean p15)
    {
        return new com.a.c.b.a.r(this, p12, p14, p15, p10, p11, p13, com.a.c.b.ap.a(p13.a));
    }

    private String a(reflect.Field p2)
    {
        String v0_3;
        String v0_2 = ((com.a.c.a.c) p2.getAnnotation(com.a.c.a.c));
        if (v0_2 != null) {
            v0_3 = v0_2.a();
        } else {
            v0_3 = this.b.a(p2);
        }
        return v0_3;
    }

    private java.util.Map a(com.a.c.k p16, com.a.c.c.a p17, Class p18)
    {
        String v1_2;
        java.util.LinkedHashMap v10_1 = new java.util.LinkedHashMap();
        if (!p18.isInterface()) {
            reflect.Type v12 = p17.b;
            while (p18 != Object) {
                reflect.Field[] v13 = p18.getDeclaredFields();
                int v14 = v13.length;
                int v11 = 0;
                while (v11 < v14) {
                    reflect.Field v7 = v13[v11];
                    String v4_0 = this.a(v7, 1);
                    boolean v5 = this.a(v7, 0);
                    if ((v4_0 != null) || (v5)) {
                        StringBuilder v3_0;
                        v7.setAccessible(1);
                        IllegalArgumentException v2_2 = com.a.c.b.b.a(p17.b, p18, v7.getGenericType());
                        String v1_12 = ((com.a.c.a.c) v7.getAnnotation(com.a.c.a.c));
                        if (v1_12 != null) {
                            v3_0 = v1_12.a();
                        } else {
                            v3_0 = this.b.a(v7);
                        }
                        com.a.c.c.a v8 = com.a.c.c.a.a(v2_2);
                        String v1_16 = new com.a.c.b.a.r(this, v3_0, v4_0, v5, p16, v7, v8, com.a.c.b.ap.a(v8.a));
                        String v1_18 = ((com.a.c.b.a.t) v10_1.put(v1_16.g, v1_16));
                        if (v1_18 != null) {
                            throw new IllegalArgumentException(new StringBuilder().append(v12).append(" declares multiple JSON fields named ").append(v1_18.g).toString());
                        }
                    }
                    v11++;
                }
                p17 = com.a.c.c.a.a(com.a.c.b.b.a(p17.b, p18, p18.getGenericSuperclass()));
                p18 = p17.a;
            }
            v1_2 = v10_1;
        } else {
            v1_2 = v10_1;
        }
        return v1_2;
    }

    private boolean a(reflect.Field p9, boolean p10)
    {
        int v0_28;
        if (this.c.a(p9.getType(), p10)) {
            v0_28 = 0;
        } else {
            int v0_24;
            com.a.c.b.s v4 = this.c;
            if ((v4.c & p9.getModifiers()) == 0) {
                if ((v4.b == -1.0) || (v4.a(((com.a.c.a.d) p9.getAnnotation(com.a.c.a.d)), ((com.a.c.a.e) p9.getAnnotation(com.a.c.a.e))))) {
                    if (!p9.isSynthetic()) {
                        if (v4.e) {
                            int v0_14 = ((com.a.c.a.a) p9.getAnnotation(com.a.c.a.a));
                            if (v0_14 != 0) {
                                if (!p10) {
                                    if (v0_14.b()) {
                                        if ((v4.d) || (!com.a.c.b.s.b(p9.getType()))) {
                                            if (!com.a.c.b.s.a(p9.getType())) {
                                                int v0_22;
                                                if (!p10) {
                                                    v0_22 = v4.g;
                                                } else {
                                                    v0_22 = v4.f;
                                                }
                                                if (!v0_22.isEmpty()) {
                                                    new com.a.c.c(p9);
                                                    java.util.Iterator v1_7 = v0_22.iterator();
                                                    while (v1_7.hasNext()) {
                                                        if (((com.a.c.b) v1_7.next()).a()) {
                                                            v0_24 = 1;
                                                            if (v0_24 != 0) {
                                                            } else {
                                                                v0_28 = 1;
                                                            }
                                                            return v0_28;
                                                        }
                                                        v0_24 = 1;
                                                    }
                                                }
                                                v0_24 = 0;
                                            } else {
                                                v0_24 = 1;
                                            }
                                        } else {
                                            v0_24 = 1;
                                        }
                                    }
                                } else {
                                    if (v0_14.a()) {
                                    }
                                }
                            }
                        }
                    } else {
                        v0_24 = 1;
                    }
                } else {
                    v0_24 = 1;
                }
            } else {
                v0_24 = 1;
            }
        }
        return v0_28;
    }

    public final com.a.c.an a(com.a.c.k p5, com.a.c.c.a p6)
    {
        com.a.c.b.a.s v0_4;
        java.util.Map v1_0 = p6.a;
        if (Object.isAssignableFrom(v1_0)) {
            v0_4 = new com.a.c.b.a.s(this.a.a(p6), this.a(p5, p6, v1_0), 0);
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }
}
