package com.a.c.b.a;
public final class a extends com.a.c.an {
    public static final com.a.c.ap a;
    private final Class b;
    private final com.a.c.an c;

    static a()
    {
        com.a.c.b.a.a.a = new com.a.c.b.a.b();
        return;
    }

    public a(com.a.c.k p2, com.a.c.an p3, Class p4)
    {
        this.c = new com.a.c.b.a.y(p2, p3, p4);
        this.b = p4;
        return;
    }

    public final Object a(com.a.c.d.a p5)
    {
        Object v0_4;
        if (p5.f() != com.a.c.d.d.i) {
            java.util.ArrayList v2_1 = new java.util.ArrayList();
            p5.a();
            while (p5.e()) {
                v2_1.add(this.c.a(p5));
            }
            p5.b();
            Object v1_2 = reflect.Array.newInstance(this.b, v2_1.size());
            Object v0_3 = 0;
            while (v0_3 < v2_1.size()) {
                reflect.Array.set(v1_2, v0_3, v2_1.get(v0_3));
                v0_3++;
            }
            v0_4 = v1_2;
        } else {
            p5.k();
            v0_4 = 0;
        }
        return v0_4;
    }

    public final void a(com.a.c.d.e p5, Object p6)
    {
        if (p6 != null) {
            p5.b();
            int v0 = 0;
            int v1 = reflect.Array.getLength(p6);
            while (v0 < v1) {
                this.c.a(p5, reflect.Array.get(p6, v0));
                v0++;
            }
            p5.c();
        } else {
            p5.f();
        }
        return;
    }
}
