package com.a.c.b;
final class af implements java.util.Map$Entry {
    com.a.c.b.af a;
    com.a.c.b.af b;
    com.a.c.b.af c;
    com.a.c.b.af d;
    com.a.c.b.af e;
    final Object f;
    final int g;
    Object h;
    int i;

    af()
    {
        this.f = 0;
        this.g = -1;
        this.e = this;
        this.d = this;
        return;
    }

    af(com.a.c.b.af p2, Object p3, int p4, com.a.c.b.af p5, com.a.c.b.af p6)
    {
        this.a = p2;
        this.f = p3;
        this.g = p4;
        this.i = 1;
        this.d = p5;
        this.e = p6;
        p6.d = this;
        p5.e = this;
        return;
    }

    private com.a.c.b.af a()
    {
        com.a.c.b.af v0 = this.b;
        while (v0 != null) {
            this = v0;
            v0 = v0.b;
        }
        return this;
    }

    private com.a.c.b.af b()
    {
        com.a.c.b.af v0 = this.c;
        while (v0 != null) {
            this = v0;
            v0 = v0.c;
        }
        return this;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if ((p4 instanceof java.util.Map$Entry)) {
            if (this.f != null) {
                if (!this.f.equals(((java.util.Map$Entry) p4).getKey())) {
                    return v0;
                }
            } else {
                if (((java.util.Map$Entry) p4).getKey() != null) {
                    return v0;
                }
            }
            if (this.h != null) {
                if (!this.h.equals(((java.util.Map$Entry) p4).getValue())) {
                    return v0;
                }
            } else {
                if (((java.util.Map$Entry) p4).getValue() != null) {
                    return v0;
                }
            }
            v0 = 1;
        }
        return v0;
    }

    public final Object getKey()
    {
        return this.f;
    }

    public final Object getValue()
    {
        return this.h;
    }

    public final int hashCode()
    {
        int v0_2;
        int v1_0 = 0;
        if (this.f != null) {
            v0_2 = this.f.hashCode();
        } else {
            v0_2 = 0;
        }
        if (this.h != null) {
            v1_0 = this.h.hashCode();
        }
        return (v0_2 ^ v1_0);
    }

    public final Object setValue(Object p2)
    {
        Object v0 = this.h;
        this.h = p2;
        return v0;
    }

    public final String toString()
    {
        return new StringBuilder().append(this.f).append("=").append(this.h).toString();
    }
}
