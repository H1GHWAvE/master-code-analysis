package com.a.b.b;
final class eb implements com.a.b.b.dz, java.io.Serializable {
    private static final long e;
    final com.a.b.b.dz a;
    final long b;
    transient volatile Object c;
    transient volatile long d;

    eb(com.a.b.b.dz p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        int v0_5;
        this.a = ((com.a.b.b.dz) com.a.b.b.cn.a(p3));
        this.b = p6.toNanos(p4);
        if (p4 <= 0) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        com.a.b.b.cn.a(v0_5);
        return;
    }

    public final Object a()
    {
        Object v0_2;
        Object v0_0 = this.d;
        long v2_0 = System.nanoTime();
        if ((v0_0 != 0) && ((v2_0 - v0_0) < 0)) {
            v0_2 = this.c;
        } else {
            if (v0_0 != this.d) {
            } else {
                v0_2 = this.a.a();
                this.c = v0_2;
                long v2_1 = (v2_0 + this.b);
                if (v2_1 == 0) {
                    v2_1 = 1;
                }
                this.d = v2_1;
            }
        }
        return v0_2;
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 62)).append("Suppliers.memoizeWithExpiration(").append(v0_2).append(", ").append(this.b).append(", NANOS)").toString();
    }
}
