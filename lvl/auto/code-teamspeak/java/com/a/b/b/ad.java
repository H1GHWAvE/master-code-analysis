package com.a.b.b;
final class ad extends com.a.b.b.ae {
    private final java.util.BitSet s;

    private ad(java.util.BitSet p3, String p4)
    {
        java.util.BitSet v0_2;
        this(p4);
        if ((p3.length() + 64) >= p3.size()) {
            v0_2 = p3;
        } else {
            v0_2 = ((java.util.BitSet) p3.clone());
        }
        this.s = v0_2;
        return;
    }

    synthetic ad(java.util.BitSet p1, String p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    final void a(java.util.BitSet p2)
    {
        p2.or(this.s);
        return;
    }

    public final boolean c(char p2)
    {
        return this.s.get(p2);
    }
}
