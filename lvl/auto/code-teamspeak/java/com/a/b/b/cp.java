package com.a.b.b;
public final class cp {
    private static final com.a.b.b.bv a;

    static cp()
    {
        com.a.b.b.cp.a = com.a.b.b.bv.a(44);
        return;
    }

    private cp()
    {
        return;
    }

    public static com.a.b.b.co a()
    {
        return com.a.b.b.da.a;
    }

    public static com.a.b.b.co a(com.a.b.b.co p1)
    {
        return new com.a.b.b.cz(p1);
    }

    public static com.a.b.b.co a(com.a.b.b.co p2, com.a.b.b.bj p3)
    {
        return new com.a.b.b.ct(p2, p3, 0);
    }

    public static com.a.b.b.co a(com.a.b.b.co p3, com.a.b.b.co p4)
    {
        return new com.a.b.b.cr(com.a.b.b.cp.c(((com.a.b.b.co) com.a.b.b.cn.a(p3)), ((com.a.b.b.co) com.a.b.b.cn.a(p4))), 0);
    }

    public static com.a.b.b.co a(Class p2)
    {
        return new com.a.b.b.cx(p2, 0);
    }

    private static com.a.b.b.co a(Iterable p3)
    {
        return new com.a.b.b.cr(com.a.b.b.cp.c(p3), 0);
    }

    public static com.a.b.b.co a(Object p2)
    {
        com.a.b.b.cy v0_1;
        if (p2 != null) {
            v0_1 = new com.a.b.b.cy(p2, 0);
        } else {
            v0_1 = com.a.b.b.da.c;
        }
        return v0_1;
    }

    private static com.a.b.b.co a(String p1)
    {
        return new com.a.b.b.cu(p1);
    }

    public static com.a.b.b.co a(java.util.Collection p2)
    {
        return new com.a.b.b.cw(p2, 0);
    }

    private static com.a.b.b.co a(java.util.regex.Pattern p1)
    {
        return new com.a.b.b.cv(p1);
    }

    private static varargs com.a.b.b.co a(com.a.b.b.co[] p3)
    {
        return new com.a.b.b.cr(com.a.b.b.cp.a(p3), 0);
    }

    private static varargs java.util.List a(Object[] p1)
    {
        return com.a.b.b.cp.c(java.util.Arrays.asList(p1));
    }

    static synthetic com.a.b.b.bv b()
    {
        return com.a.b.b.cp.a;
    }

    private static com.a.b.b.co b(com.a.b.b.co p3, com.a.b.b.co p4)
    {
        return new com.a.b.b.df(com.a.b.b.cp.c(((com.a.b.b.co) com.a.b.b.cn.a(p3)), ((com.a.b.b.co) com.a.b.b.cn.a(p4))), 0);
    }

    private static com.a.b.b.co b(Class p2)
    {
        return new com.a.b.b.cs(p2, 0);
    }

    private static com.a.b.b.co b(Iterable p3)
    {
        return new com.a.b.b.df(com.a.b.b.cp.c(p3), 0);
    }

    private static varargs com.a.b.b.co b(com.a.b.b.co[] p3)
    {
        return new com.a.b.b.df(com.a.b.b.cp.a(p3), 0);
    }

    private static com.a.b.b.co c()
    {
        return com.a.b.b.da.b;
    }

    private static java.util.List c(com.a.b.b.co p2, com.a.b.b.co p3)
    {
        java.util.List v0_1 = new com.a.b.b.co[2];
        v0_1[0] = p2;
        v0_1[1] = p3;
        return java.util.Arrays.asList(v0_1);
    }

    private static java.util.List c(Iterable p3)
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        java.util.Iterator v1 = p3.iterator();
        while (v1.hasNext()) {
            v0_1.add(com.a.b.b.cn.a(v1.next()));
        }
        return v0_1;
    }

    private static com.a.b.b.co d()
    {
        return com.a.b.b.da.c;
    }

    private static com.a.b.b.co e()
    {
        return com.a.b.b.da.d;
    }
}
