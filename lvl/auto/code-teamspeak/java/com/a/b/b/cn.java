package com.a.b.b;
public final class cn {

    private cn()
    {
        return;
    }

    public static int a(int p7, int p8)
    {
        if ((p7 >= 0) && (p7 < p8)) {
            return p7;
        } else {
            String v0_3;
            if (p7 >= 0) {
                if (p8 >= 0) {
                    Object[] v3_1 = new Object[3];
                    v3_1[0] = "index";
                    v3_1[1] = Integer.valueOf(p7);
                    v3_1[2] = Integer.valueOf(p8);
                    v0_3 = com.a.b.b.cn.a("%s (%s) must be less than size (%s)", v3_1);
                } else {
                    throw new IllegalArgumentException(new StringBuilder(26).append("negative size: ").append(p8).toString());
                }
            } else {
                Object[] v3_2 = new Object[2];
                v3_2[0] = "index";
                v3_2[1] = Integer.valueOf(p7);
                v0_3 = com.a.b.b.cn.a("%s (%s) must not be negative", v3_2);
            }
            throw new IndexOutOfBoundsException(v0_3);
        }
    }

    private static int a(int p6, int p7, String p8)
    {
        if ((p6 >= 0) && (p6 < p7)) {
            return p6;
        } else {
            String v0_1;
            if (p6 >= 0) {
                if (p7 >= 0) {
                    Object[] v2_1 = new Object[3];
                    v2_1[0] = p8;
                    v2_1[1] = Integer.valueOf(p6);
                    v2_1[2] = Integer.valueOf(p7);
                    v0_1 = com.a.b.b.cn.a("%s (%s) must be less than size (%s)", v2_1);
                } else {
                    throw new IllegalArgumentException(new StringBuilder(26).append("negative size: ").append(p7).toString());
                }
            } else {
                Object[] v2_4 = new Object[2];
                v2_4[0] = p8;
                v2_4[1] = Integer.valueOf(p6);
                v0_1 = com.a.b.b.cn.a("%s (%s) must not be negative", v2_4);
            }
            throw new IndexOutOfBoundsException(v0_1);
        }
    }

    public static Object a(Object p1)
    {
        if (p1 != null) {
            return p1;
        } else {
            throw new NullPointerException();
        }
    }

    public static Object a(Object p2, Object p3)
    {
        if (p2 != null) {
            return p2;
        } else {
            throw new NullPointerException(String.valueOf(p3));
        }
    }

    public static varargs Object a(Object p2, String p3, Object[] p4)
    {
        if (p2 != null) {
            return p2;
        } else {
            throw new NullPointerException(com.a.b.b.cn.a(p3, p4));
        }
    }

    static varargs String a(String p7, Object[] p8)
    {
        int v0_0 = 0;
        String v2 = String.valueOf(p7);
        StringBuilder v3_1 = new StringBuilder((v2.length() + (p8.length * 16)));
        int v1_2 = 0;
        while (v0_0 < p8.length) {
            int v4_4 = v2.indexOf("%s", v1_2);
            if (v4_4 == -1) {
                break;
            }
            v3_1.append(v2.substring(v1_2, v4_4));
            int v1_11 = (v0_0 + 1);
            v3_1.append(p8[v0_0]);
            v1_2 = (v4_4 + 2);
            v0_0 = v1_11;
        }
        v3_1.append(v2.substring(v1_2));
        if (v0_0 < p8.length) {
            v3_1.append(" [");
            int v1_6 = (v0_0 + 1);
            v3_1.append(p8[v0_0]);
            int v0_2 = v1_6;
            while (v0_2 < p8.length) {
                v3_1.append(", ");
                int v1_9 = (v0_2 + 1);
                v3_1.append(p8[v0_2]);
                v0_2 = v1_9;
            }
            v3_1.append(93);
        }
        return v3_1.toString();
    }

    public static void a(int p5, int p6, int p7)
    {
        if ((p5 >= 0) && ((p6 >= p5) && (p6 <= p7))) {
            return;
        } else {
            if ((p5 >= 0) && (p5 <= p7)) {
                if ((p6 >= 0) && (p6 <= p7)) {
                    Object[] v2_1 = new Object[2];
                    v2_1[0] = Integer.valueOf(p6);
                    v2_1[1] = Integer.valueOf(p5);
                    String v0_1 = com.a.b.b.cn.a("end index (%s) must not be less than start index (%s)", v2_1);
                } else {
                    v0_1 = com.a.b.b.cn.d(p6, p7, "end index");
                }
            } else {
                v0_1 = com.a.b.b.cn.d(p5, p7, "start index");
            }
            throw new IndexOutOfBoundsException(v0_1);
        }
    }

    public static void a(boolean p1)
    {
        if (p1) {
            return;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public static void a(boolean p2, Object p3)
    {
        if (p2) {
            return;
        } else {
            throw new IllegalArgumentException(String.valueOf(p3));
        }
    }

    public static varargs void a(boolean p2, String p3, Object[] p4)
    {
        if (p2) {
            return;
        } else {
            throw new IllegalArgumentException(com.a.b.b.cn.a(p3, p4));
        }
    }

    public static int b(int p2, int p3)
    {
        if ((p2 >= 0) && (p2 <= p3)) {
            return p2;
        } else {
            throw new IndexOutOfBoundsException(com.a.b.b.cn.d(p2, p3, "index"));
        }
    }

    private static String b(int p4, int p5, int p6)
    {
        if ((p4 >= 0) && (p4 <= p6)) {
            if ((p5 >= 0) && (p5 <= p6)) {
                Object[] v1_1 = new Object[2];
                v1_1[0] = Integer.valueOf(p5);
                v1_1[1] = Integer.valueOf(p4);
                String v0_1 = com.a.b.b.cn.a("end index (%s) must not be less than start index (%s)", v1_1);
            } else {
                v0_1 = com.a.b.b.cn.d(p5, p6, "end index");
            }
        } else {
            v0_1 = com.a.b.b.cn.d(p4, p6, "start index");
        }
        return v0_1;
    }

    private static String b(int p5, int p6, String p7)
    {
        String v0_1;
        if (p5 >= 0) {
            if (p6 >= 0) {
                Object[] v1_1 = new Object[3];
                v1_1[0] = p7;
                v1_1[1] = Integer.valueOf(p5);
                v1_1[2] = Integer.valueOf(p6);
                v0_1 = com.a.b.b.cn.a("%s (%s) must be less than size (%s)", v1_1);
            } else {
                throw new IllegalArgumentException(new StringBuilder(26).append("negative size: ").append(p6).toString());
            }
        } else {
            Object[] v1_7 = new Object[2];
            v1_7[0] = p7;
            v1_7[1] = Integer.valueOf(p5);
            v0_1 = com.a.b.b.cn.a("%s (%s) must not be negative", v1_7);
        }
        return v0_1;
    }

    public static void b(boolean p1)
    {
        if (p1) {
            return;
        } else {
            throw new IllegalStateException();
        }
    }

    public static void b(boolean p2, Object p3)
    {
        if (p2) {
            return;
        } else {
            throw new IllegalStateException(String.valueOf(p3));
        }
    }

    public static varargs void b(boolean p2, String p3, Object[] p4)
    {
        if (p2) {
            return;
        } else {
            throw new IllegalStateException(com.a.b.b.cn.a(p3, p4));
        }
    }

    private static int c(int p2, int p3, String p4)
    {
        if ((p2 >= 0) && (p2 <= p3)) {
            return p2;
        } else {
            throw new IndexOutOfBoundsException(com.a.b.b.cn.d(p2, p3, p4));
        }
    }

    private static String d(int p5, int p6, String p7)
    {
        String v0_1;
        if (p5 >= 0) {
            if (p6 >= 0) {
                Object[] v1_1 = new Object[3];
                v1_1[0] = p7;
                v1_1[1] = Integer.valueOf(p5);
                v1_1[2] = Integer.valueOf(p6);
                v0_1 = com.a.b.b.cn.a("%s (%s) must not be greater than size (%s)", v1_1);
            } else {
                throw new IllegalArgumentException(new StringBuilder(26).append("negative size: ").append(p6).toString());
            }
        } else {
            Object[] v1_7 = new Object[2];
            v1_7[0] = p7;
            v1_7[1] = Integer.valueOf(p5);
            v0_1 = com.a.b.b.cn.a("%s (%s) must not be negative", v1_7);
        }
        return v0_1;
    }
}
