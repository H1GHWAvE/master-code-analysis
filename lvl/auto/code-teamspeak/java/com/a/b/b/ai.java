package com.a.b.b;
final class ai extends com.a.b.b.m {
    private final char[] s;
    private final char[] t;

    ai(String p6, char[] p7, char[] p8)
    {
        int v0_1;
        this(p6);
        this.s = p7;
        this.t = p8;
        if (p7.length != p8.length) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        int v0_2 = 0;
        while (v0_2 < p7.length) {
            int v3_3;
            if (p7[v0_2] > p8[v0_2]) {
                v3_3 = 0;
            } else {
                v3_3 = 1;
            }
            com.a.b.b.cn.a(v3_3);
            if ((v0_2 + 1) < p7.length) {
                int v3_6;
                if (p8[v0_2] >= p7[(v0_2 + 1)]) {
                    v3_6 = 0;
                } else {
                    v3_6 = 1;
                }
                com.a.b.b.cn.a(v3_6);
            }
            v0_2++;
        }
        return;
    }

    public final bridge synthetic boolean a(Object p2)
    {
        return super.a(((Character) p2));
    }

    public final boolean c(char p4)
    {
        int v0 = 1;
        char v1_1 = java.util.Arrays.binarySearch(this.s, p4);
        if (v1_1 < 0) {
            char v1_3 = ((v1_1 ^ -1) - 1);
            if ((v1_3 < 0) || (p4 > this.t[v1_3])) {
                v0 = 0;
            }
        }
        return v0;
    }
}
