package com.a.b.b;
public class bv {
    final String a;

    private bv(com.a.b.b.bv p2)
    {
        this.a = p2.a;
        return;
    }

    synthetic bv(com.a.b.b.bv p1, byte p2)
    {
        this(p1);
        return;
    }

    private bv(String p2)
    {
        this.a = ((String) com.a.b.b.cn.a(p2));
        return;
    }

    public static com.a.b.b.bv a(char p2)
    {
        return new com.a.b.b.bv(String.valueOf(p2));
    }

    public static com.a.b.b.bv a(String p1)
    {
        return new com.a.b.b.bv(p1);
    }

    private Appendable a(Appendable p2, Iterable p3)
    {
        return this.a(p2, p3.iterator());
    }

    private varargs Appendable a(Appendable p2, Object p3, Object p4, Object[] p5)
    {
        return this.a(p2, com.a.b.b.bv.b(p3, p4, p5));
    }

    private Appendable a(Appendable p2, Object[] p3)
    {
        return this.a(p2, java.util.Arrays.asList(p3));
    }

    private static synthetic String a(com.a.b.b.bv p1)
    {
        return p1.a;
    }

    private varargs String a(Object p2, Object p3, Object[] p4)
    {
        return this.a(com.a.b.b.bv.b(p2, p3, p4));
    }

    private String a(java.util.Iterator p2)
    {
        return this.a(new StringBuilder(), p2).toString();
    }

    private varargs StringBuilder a(StringBuilder p2, Object p3, Object p4, Object[] p5)
    {
        return this.a(p2, com.a.b.b.bv.b(p3, p4, p5));
    }

    private StringBuilder a(StringBuilder p2, Object[] p3)
    {
        return this.a(p2, java.util.Arrays.asList(p3));
    }

    private static Iterable b(Object p1, Object p2, Object[] p3)
    {
        com.a.b.b.cn.a(p3);
        return new com.a.b.b.by(p3, p1, p2);
    }

    public com.a.b.b.bv a()
    {
        return new com.a.b.b.bx(this, this);
    }

    public Appendable a(Appendable p2, java.util.Iterator p3)
    {
        com.a.b.b.cn.a(p2);
        if (p3.hasNext()) {
            p2.append(this.a(p3.next()));
            while (p3.hasNext()) {
                p2.append(this.a);
                p2.append(this.a(p3.next()));
            }
        }
        return p2;
    }

    CharSequence a(Object p2)
    {
        String v2_1;
        com.a.b.b.cn.a(p2);
        if (!(p2 instanceof CharSequence)) {
            v2_1 = p2.toString();
        } else {
            v2_1 = ((CharSequence) p2);
        }
        return v2_1;
    }

    public final String a(Iterable p3)
    {
        return this.a(new StringBuilder(), p3.iterator()).toString();
    }

    public final String a(Object[] p2)
    {
        return this.a(java.util.Arrays.asList(p2));
    }

    public final StringBuilder a(StringBuilder p2, Iterable p3)
    {
        return this.a(p2, p3.iterator());
    }

    public final StringBuilder a(StringBuilder p3, java.util.Iterator p4)
    {
        try {
            this.a(p3, p4);
            return p3;
        } catch (java.io.IOException v0) {
            throw new AssertionError(v0);
        }
    }

    public com.a.b.b.bv b(String p2)
    {
        com.a.b.b.cn.a(p2);
        return new com.a.b.b.bw(this, this, p2);
    }

    public com.a.b.b.bz c(String p3)
    {
        return new com.a.b.b.bz(this, p3, 0);
    }
}
