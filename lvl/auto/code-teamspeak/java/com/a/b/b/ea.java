package com.a.b.b;
public final class ea {

    private ea()
    {
        return;
    }

    private static com.a.b.b.bj a()
    {
        return com.a.b.b.ef.a;
    }

    private static com.a.b.b.dz a(com.a.b.b.bj p1, com.a.b.b.dz p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.b.ed(p1, p2);
    }

    private static com.a.b.b.dz a(com.a.b.b.dz p2)
    {
        if (!(p2 instanceof com.a.b.b.ec)) {
            p2 = new com.a.b.b.ec(((com.a.b.b.dz) com.a.b.b.cn.a(p2)));
        }
        return p2;
    }

    private static com.a.b.b.dz a(com.a.b.b.dz p1, long p2, java.util.concurrent.TimeUnit p4)
    {
        return new com.a.b.b.eb(p1, p2, p4);
    }

    private static com.a.b.b.dz a(Object p1)
    {
        return new com.a.b.b.eg(p1);
    }

    private static com.a.b.b.dz b(com.a.b.b.dz p2)
    {
        return new com.a.b.b.eh(((com.a.b.b.dz) com.a.b.b.cn.a(p2)));
    }
}
