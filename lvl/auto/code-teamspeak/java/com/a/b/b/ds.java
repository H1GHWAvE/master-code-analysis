package com.a.b.b;
public final class ds {
    private static final String a = "Chunk [%s] is not a valid entry";
    private final com.a.b.b.di b;
    private final com.a.b.b.di c;

    private ds(com.a.b.b.di p2, com.a.b.b.di p3)
    {
        this.b = p2;
        this.c = ((com.a.b.b.di) com.a.b.b.cn.a(p3));
        return;
    }

    synthetic ds(com.a.b.b.di p1, com.a.b.b.di p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    private java.util.Map a(CharSequence p11)
    {
        java.util.LinkedHashMap v5_1 = new java.util.LinkedHashMap();
        java.util.Iterator v6 = this.b.a(p11).iterator();
        while (v6.hasNext()) {
            String v2_2;
            java.util.Map v0_5 = ((String) v6.next());
            Object[] v7_0 = this.c.b(v0_5);
            int v1_1 = v7_0.hasNext();
            String v8_0 = new Object[1];
            v8_0[0] = v0_5;
            com.a.b.b.cn.a(v1_1, "Chunk [%s] is not a valid entry", v8_0);
            int v1_3 = ((String) v7_0.next());
            if (v5_1.containsKey(v1_3)) {
                v2_2 = 0;
            } else {
                v2_2 = 1;
            }
            int v1_5;
            Object[] v9_0 = new Object[1];
            v9_0[0] = v1_3;
            com.a.b.b.cn.a(v2_2, "Duplicate key [%s] found.", v9_0);
            String v2_3 = v7_0.hasNext();
            Object[] v9_1 = new Object[1];
            v9_1[0] = v0_5;
            com.a.b.b.cn.a(v2_3, "Chunk [%s] is not a valid entry", v9_1);
            v5_1.put(v1_3, ((String) v7_0.next()));
            if (v7_0.hasNext()) {
                v1_5 = 0;
            } else {
                v1_5 = 1;
            }
            Object[] v7_1 = new Object[1];
            v7_1[0] = v0_5;
            com.a.b.b.cn.a(v1_5, "Chunk [%s] is not a valid entry", v7_1);
        }
        return java.util.Collections.unmodifiableMap(v5_1);
    }
}
