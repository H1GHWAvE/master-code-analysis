package com.a.b.b;
final class dr implements java.lang.Iterable {
    final synthetic CharSequence a;
    final synthetic com.a.b.b.di b;

    dr(com.a.b.b.di p1, CharSequence p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final java.util.Iterator iterator()
    {
        return this.b.b(this.a);
    }

    public final String toString()
    {
        return com.a.b.b.bv.a(", ").a(new StringBuilder("["), this).append(93).toString();
    }
}
