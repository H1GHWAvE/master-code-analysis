package com.a.b.b;
public final class di {
    final com.a.b.b.m a;
    final boolean b;
    final int c;
    private final com.a.b.b.du d;

    private di(com.a.b.b.du p4)
    {
        this(p4, 0, com.a.b.b.m.m, 2147483647);
        return;
    }

    private di(com.a.b.b.du p1, boolean p2, com.a.b.b.m p3, int p4)
    {
        this.d = p1;
        this.b = p2;
        this.a = p3;
        this.c = p4;
        return;
    }

    public static com.a.b.b.di a(char p3)
    {
        com.a.b.b.m v0 = com.a.b.b.m.a(p3);
        com.a.b.b.cn.a(v0);
        return new com.a.b.b.di(new com.a.b.b.dj(v0));
    }

    private static com.a.b.b.di a(int p2)
    {
        com.a.b.b.di v0_0;
        if (p2 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0, "The length may not be less than 1");
        return new com.a.b.b.di(new com.a.b.b.dp(p2));
    }

    private static com.a.b.b.di a(com.a.b.b.m p2)
    {
        com.a.b.b.cn.a(p2);
        return new com.a.b.b.di(new com.a.b.b.dj(p2));
    }

    public static com.a.b.b.di a(String p2)
    {
        com.a.b.b.di v0_1;
        if (p2.length() == 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1, "The separator may not be the empty string.");
        return new com.a.b.b.di(new com.a.b.b.dl(p2));
    }

    public static com.a.b.b.di a(java.util.regex.Pattern p4)
    {
        com.a.b.b.di v0_3;
        com.a.b.b.cn.a(p4);
        if (p4.matcher("").matches()) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        com.a.b.b.dn v1_1 = new Object[1];
        v1_1[0] = p4;
        com.a.b.b.cn.a(v0_3, "The pattern may not match the empty string: %s", v1_1);
        return new com.a.b.b.di(new com.a.b.b.dn(p4));
    }

    private com.a.b.b.ds a(com.a.b.b.di p3)
    {
        return new com.a.b.b.ds(this, p3, 0);
    }

    private static synthetic java.util.Iterator a(com.a.b.b.di p1, CharSequence p2)
    {
        return p1.b(p2);
    }

    private com.a.b.b.di b(int p6)
    {
        com.a.b.b.di v0_0;
        if (p6 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.du v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p6);
        com.a.b.b.cn.a(v0_0, "must be greater than zero: %s", v1_1);
        return new com.a.b.b.di(this.d, this.b, this.a, p6);
    }

    private com.a.b.b.di b(com.a.b.b.m p5)
    {
        com.a.b.b.cn.a(p5);
        return new com.a.b.b.di(this.d, this.b, p5, this.c);
    }

    private static com.a.b.b.di b(String p1)
    {
        return com.a.b.b.di.a(java.util.regex.Pattern.compile(p1));
    }

    private com.a.b.b.ds b(char p2)
    {
        return this.a(com.a.b.b.di.a(p2));
    }

    private static synthetic com.a.b.b.m b(com.a.b.b.di p1)
    {
        return p1.a;
    }

    private com.a.b.b.ds c(String p2)
    {
        return this.a(com.a.b.b.di.a(p2));
    }

    private java.util.List c(CharSequence p4)
    {
        com.a.b.b.cn.a(p4);
        java.util.List v0_0 = this.b(p4);
        java.util.ArrayList v1_1 = new java.util.ArrayList();
        while (v0_0.hasNext()) {
            v1_1.add(v0_0.next());
        }
        return java.util.Collections.unmodifiableList(v1_1);
    }

    private static synthetic boolean c(com.a.b.b.di p1)
    {
        return p1.b;
    }

    private static synthetic int d(com.a.b.b.di p1)
    {
        return p1.c;
    }

    public final com.a.b.b.di a()
    {
        return new com.a.b.b.di(this.d, 1, this.a, this.c);
    }

    public final Iterable a(CharSequence p2)
    {
        com.a.b.b.cn.a(p2);
        return new com.a.b.b.dr(this, p2);
    }

    public final com.a.b.b.di b()
    {
        com.a.b.b.m v0 = com.a.b.b.m.r;
        com.a.b.b.cn.a(v0);
        return new com.a.b.b.di(this.d, this.b, v0, this.c);
    }

    final java.util.Iterator b(CharSequence p2)
    {
        return this.d.a(this, p2);
    }
}
