package com.a.b.b;
final class dg extends com.a.b.b.ci {
    private static final long b;
    private final Object a;

    dg(Object p1)
    {
        this.a = p1;
        return;
    }

    public final com.a.b.b.ci a(com.a.b.b.bj p4)
    {
        return new com.a.b.b.dg(com.a.b.b.cn.a(p4.e(this.a), "the Function passed to Optional.transform() must not return null."));
    }

    public final com.a.b.b.ci a(com.a.b.b.ci p1)
    {
        com.a.b.b.cn.a(p1);
        return this;
    }

    public final Object a(com.a.b.b.dz p2)
    {
        com.a.b.b.cn.a(p2);
        return this.a;
    }

    public final Object a(Object p2)
    {
        com.a.b.b.cn.a(p2, "use Optional.orNull() instead of Optional.or(null)");
        return this.a;
    }

    public final boolean b()
    {
        return 1;
    }

    public final Object c()
    {
        return this.a;
    }

    public final Object d()
    {
        return this.a;
    }

    public final java.util.Set e()
    {
        return java.util.Collections.singleton(this.a);
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.dg)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.b.dg) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return (1502476572 + this.a.hashCode());
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 13)).append("Optional.of(").append(v0_2).append(")").toString();
    }
}
