package com.a.b.i;
public final class ar implements java.io.Closeable {
    private static final com.a.b.i.au b;
    final com.a.b.i.au a;
    private final java.util.Deque c;
    private Throwable d;

    static ar()
    {
        com.a.b.i.as v0_1;
        if (!com.a.b.i.at.a()) {
            v0_1 = com.a.b.i.as.a;
        } else {
            v0_1 = com.a.b.i.at.a;
        }
        com.a.b.i.ar.b = v0_1;
        return;
    }

    private ar(com.a.b.i.au p3)
    {
        this.c = new java.util.ArrayDeque(4);
        this.a = ((com.a.b.i.au) com.a.b.b.cn.a(p3));
        return;
    }

    public static com.a.b.i.ar a()
    {
        return new com.a.b.i.ar(com.a.b.i.ar.b);
    }

    private RuntimeException a(Throwable p2, Class p3)
    {
        com.a.b.b.cn.a(p2);
        this.d = p2;
        com.a.b.b.ei.b(p2, java.io.IOException);
        com.a.b.b.ei.b(p2, p3);
        throw new RuntimeException(p2);
    }

    private RuntimeException a(Throwable p2, Class p3, Class p4)
    {
        com.a.b.b.cn.a(p2);
        this.d = p2;
        com.a.b.b.ei.b(p2, java.io.IOException);
        com.a.b.b.cn.a(p4);
        com.a.b.b.ei.a(p2, p3);
        com.a.b.b.ei.b(p2, p4);
        throw new RuntimeException(p2);
    }

    public final java.io.Closeable a(java.io.Closeable p2)
    {
        if (p2 != null) {
            this.c.addFirst(p2);
        }
        return p2;
    }

    public final RuntimeException a(Throwable p2)
    {
        com.a.b.b.cn.a(p2);
        this.d = p2;
        com.a.b.b.ei.b(p2, java.io.IOException);
        throw new RuntimeException(p2);
    }

    public final void close()
    {
        Throwable v1 = this.d;
        while (!this.c.isEmpty()) {
            AssertionError v0_9 = ((java.io.Closeable) this.c.removeFirst());
            try {
                v0_9.close();
            } catch (Throwable v2) {
                if (v1 != null) {
                    this.a.a(v0_9, v1, v2);
                } else {
                    v1 = v2;
                }
            }
        }
        if ((this.d != null) || (v1 == null)) {
            return;
        } else {
            com.a.b.b.ei.b(v1, java.io.IOException);
            throw new AssertionError(v1);
        }
    }
}
