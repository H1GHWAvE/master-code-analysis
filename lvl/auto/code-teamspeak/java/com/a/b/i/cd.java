package com.a.b.i;
final class cd extends java.io.InputStream {
    private java.util.Iterator a;
    private java.io.InputStream b;

    public cd(java.util.Iterator p2)
    {
        this.a = ((java.util.Iterator) com.a.b.b.cn.a(p2));
        this.a();
        return;
    }

    private void a()
    {
        this.close();
        if (this.a.hasNext()) {
            this.b = ((com.a.b.i.s) this.a.next()).a();
        }
        return;
    }

    public final int available()
    {
        int v0_2;
        if (this.b != null) {
            v0_2 = this.b.available();
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final void close()
    {
        if (this.b != null) {
            try {
                this.b.close();
                this.b = 0;
            } catch (Throwable v0_2) {
                this.b = 0;
                throw v0_2;
            }
        }
        return;
    }

    public final boolean markSupported()
    {
        return 0;
    }

    public final int read()
    {
        int v0 = -1;
        while (this.b != null) {
            int v1_2 = this.b.read();
            if (v1_2 != -1) {
                v0 = v1_2;
                break;
            } else {
                this.a();
            }
        }
        return v0;
    }

    public final int read(byte[] p3, int p4, int p5)
    {
        int v0 = -1;
        while (this.b != null) {
            int v1_2 = this.b.read(p3, p4, p5);
            if (v1_2 != -1) {
                v0 = v1_2;
                break;
            } else {
                this.a();
            }
        }
        return v0;
    }

    public final long skip(long p10)
    {
        if ((this.b != null) && (p10 > 0)) {
            long v0_3 = this.b.skip(p10);
            if (v0_3 == 0) {
                if (this.read() != -1) {
                    v0_3 = (this.b.skip((p10 - 1)) + 1);
                } else {
                    v0_3 = 0;
                }
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }
}
