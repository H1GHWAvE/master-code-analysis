package com.a.b.i;
final class ce extends java.io.Reader {
    private final java.util.Iterator a;
    private java.io.Reader b;

    ce(java.util.Iterator p1)
    {
        this.a = p1;
        this.a();
        return;
    }

    private void a()
    {
        this.close();
        if (this.a.hasNext()) {
            this.b = ((com.a.b.i.ah) this.a.next()).a();
        }
        return;
    }

    public final void close()
    {
        if (this.b != null) {
            try {
                this.b.close();
                this.b = 0;
            } catch (Throwable v0_2) {
                this.b = 0;
                throw v0_2;
            }
        }
        return;
    }

    public final int read(char[] p3, int p4, int p5)
    {
        int v0 = -1;
        while (this.b != null) {
            int v1_2 = this.b.read(p3, p4, p5);
            if (v1_2 != -1) {
                v0 = v1_2;
                break;
            } else {
                this.a();
            }
        }
        return v0;
    }

    public final boolean ready()
    {
        if ((this.b == null) || (!this.b.ready())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final long skip(long p6)
    {
        long v0_1;
        if (p6 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        long v0_4;
        com.a.b.b.cn.a(v0_1, "n is negative");
        if (p6 <= 0) {
            v0_4 = 0;
        } else {
            while (this.b != null) {
                v0_4 = this.b.skip(p6);
                if (v0_4 <= 0) {
                    this.a();
                }
            }
        }
        return v0_4;
    }
}
