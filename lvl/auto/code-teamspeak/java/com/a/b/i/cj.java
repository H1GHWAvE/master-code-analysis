package com.a.b.i;
final class cj extends com.a.b.i.s {
    private final java.net.URL a;

    private cj(java.net.URL p2)
    {
        this.a = ((java.net.URL) com.a.b.b.cn.a(p2));
        return;
    }

    synthetic cj(java.net.URL p1, byte p2)
    {
        this(p1);
        return;
    }

    public final java.io.InputStream a()
    {
        return this.a.openStream();
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 24)).append("Resources.asByteSource(").append(v0_2).append(")").toString();
    }
}
