package com.a.b.i;
public final class cg implements java.io.FilenameFilter {
    private final java.util.regex.Pattern a;

    private cg(String p2)
    {
        this(java.util.regex.Pattern.compile(p2));
        return;
    }

    private cg(java.util.regex.Pattern p2)
    {
        this.a = ((java.util.regex.Pattern) com.a.b.b.cn.a(p2));
        return;
    }

    public final boolean accept(java.io.File p2, String p3)
    {
        return this.a.matcher(p3).matches();
    }
}
