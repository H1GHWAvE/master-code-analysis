package com.a.b.i;
public final class bk {
    private static final java.util.logging.Logger a;

    static bk()
    {
        com.a.b.i.bk.a = java.util.logging.Logger.getLogger(com.a.b.i.bk.getName());
        return;
    }

    private bk()
    {
        return;
    }

    private static void a(java.io.Flushable p4)
    {
        try {
            p4.flush();
        } catch (java.io.IOException v0) {
            com.a.b.i.bk.a.log(java.util.logging.Level.WARNING, "IOException thrown while flushing Flushable.", v0);
        }
        return;
    }

    private static void b(java.io.Flushable p4)
    {
        try {
            p4.flush();
        } catch (java.io.IOException v0_0) {
            try {
                com.a.b.i.bk.a.log(java.util.logging.Level.WARNING, "IOException thrown while flushing Flushable.", v0_0);
            } catch (java.io.IOException v0_1) {
                com.a.b.i.bk.a.log(java.util.logging.Level.SEVERE, "IOException should not have been thrown.", v0_1);
            }
        }
        return;
    }
}
