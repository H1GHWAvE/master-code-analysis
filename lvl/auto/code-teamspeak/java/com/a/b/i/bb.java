package com.a.b.i;
public final enum class bb extends java.lang.Enum {
    public static final enum com.a.b.i.bb a;
    private static final synthetic com.a.b.i.bb[] b;

    static bb()
    {
        com.a.b.i.bb.a = new com.a.b.i.bb("APPEND");
        com.a.b.i.bb[] v0_3 = new com.a.b.i.bb[1];
        v0_3[0] = com.a.b.i.bb.a;
        com.a.b.i.bb.b = v0_3;
        return;
    }

    private bb(String p2)
    {
        this(p2, 0);
        return;
    }

    public static com.a.b.i.bb valueOf(String p1)
    {
        return ((com.a.b.i.bb) Enum.valueOf(com.a.b.i.bb, p1));
    }

    public static com.a.b.i.bb[] values()
    {
        return ((com.a.b.i.bb[]) com.a.b.i.bb.b.clone());
    }
}
