package com.a.b.i;
public final class an {
    private static final int a = 2048;

    private an()
    {
        return;
    }

    public static long a(Readable p6, Appendable p7)
    {
        com.a.b.b.cn.a(p6);
        com.a.b.b.cn.a(p7);
        java.nio.CharBuffer v2 = java.nio.CharBuffer.allocate(2048);
        long v0_1 = 0;
        while (p6.read(v2) != -1) {
            v2.flip();
            p7.append(v2);
            v0_1 += ((long) v2.remaining());
            v2.clear();
        }
        return v0_1;
    }

    private static java.io.Writer a()
    {
        return com.a.b.i.ap.a();
    }

    private static java.io.Writer a(Appendable p1)
    {
        com.a.b.i.a v1_1;
        if (!(p1 instanceof java.io.Writer)) {
            v1_1 = new com.a.b.i.a(p1);
        } else {
            v1_1 = ((java.io.Writer) p1);
        }
        return v1_1;
    }

    private static Object a(Readable p2, com.a.b.i.by p3)
    {
        com.a.b.b.cn.a(p2);
        com.a.b.b.cn.a(p3);
        Object v0_1 = new com.a.b.i.bz(p2);
        while(true) {
            String v1 = v0_1.a();
            if (v1 == null) {
                break;
            }
            p3.a(v1);
        }
        return p3.a();
    }

    private static String a(Readable p1)
    {
        String v0_1 = new StringBuilder();
        com.a.b.i.an.a(p1, v0_1);
        return v0_1.toString();
    }

    private static void a(java.io.Reader p7, long p8)
    {
        com.a.b.b.cn.a(p7);
        while (p8 > 0) {
            long v0_1 = p7.skip(p8);
            if (v0_1 != 0) {
                p8 -= v0_1;
            } else {
                if (p7.read() != -1) {
                    p8--;
                } else {
                    throw new java.io.EOFException();
                }
            }
        }
        return;
    }

    private static StringBuilder b(Readable p1)
    {
        StringBuilder v0_1 = new StringBuilder();
        com.a.b.i.an.a(p1, v0_1);
        return v0_1;
    }

    private static java.util.List c(Readable p3)
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        com.a.b.i.bz v1_1 = new com.a.b.i.bz(p3);
        while(true) {
            String v2 = v1_1.a();
            if (v2 == null) {
                break;
            }
            v0_1.add(v2);
        }
        return v0_1;
    }

    private static java.io.Reader d(Readable p1)
    {
        com.a.b.i.ao v1_1;
        com.a.b.b.cn.a(p1);
        if (!(p1 instanceof java.io.Reader)) {
            v1_1 = new com.a.b.i.ao(p1);
        } else {
            v1_1 = ((java.io.Reader) p1);
        }
        return v1_1;
    }
}
