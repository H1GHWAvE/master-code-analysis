package com.a.b.i;
public final class aw extends java.io.FilterOutputStream {
    private long a;

    private aw(java.io.OutputStream p1)
    {
        this(p1);
        return;
    }

    private long a()
    {
        return this.a;
    }

    public final void close()
    {
        this.out.close();
        return;
    }

    public final void write(int p5)
    {
        this.out.write(p5);
        this.a = (this.a + 1);
        return;
    }

    public final void write(byte[] p5, int p6, int p7)
    {
        this.out.write(p5, p6, p7);
        this.a = (this.a + ((long) p7));
        return;
    }
}
