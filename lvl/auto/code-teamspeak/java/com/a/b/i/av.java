package com.a.b.i;
public final class av extends java.io.FilterInputStream {
    private long a;
    private long b;

    private av(java.io.InputStream p3)
    {
        this(p3);
        this.b = -1;
        return;
    }

    private long a()
    {
        return this.a;
    }

    public final declared_synchronized void mark(int p3)
    {
        try {
            this.in.mark(p3);
            this.b = this.a;
            return;
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final int read()
    {
        int v0_1 = this.in.read();
        if (v0_1 != -1) {
            this.a = (this.a + 1);
        }
        return v0_1;
    }

    public final int read(byte[] p7, int p8, int p9)
    {
        int v0_1 = this.in.read(p7, p8, p9);
        if (v0_1 != -1) {
            this.a = (this.a + ((long) v0_1));
        }
        return v0_1;
    }

    public final declared_synchronized void reset()
    {
        try {
            if (this.in.markSupported()) {
                if (this.b != -1) {
                    this.in.reset();
                    this.a = this.b;
                    return;
                } else {
                    throw new java.io.IOException("Mark not set");
                }
            } else {
                throw new java.io.IOException("Mark not supported");
            }
        } catch (long v0_10) {
            throw v0_10;
        }
    }

    public final long skip(long p6)
    {
        long v0_1 = this.in.skip(p6);
        this.a = (this.a + v0_1);
        return v0_1;
    }
}
