package com.a.b.c;
public final class l {
    private static final com.a.b.b.di n;
    private static final com.a.b.b.di o;
    private static final com.a.b.d.jt p;
    Integer a;
    Long b;
    Long c;
    Integer d;
    com.a.b.c.bw e;
    com.a.b.c.bw f;
    Boolean g;
    long h;
    java.util.concurrent.TimeUnit i;
    long j;
    java.util.concurrent.TimeUnit k;
    long l;
    java.util.concurrent.TimeUnit m;
    private final String q;

    static l()
    {
        com.a.b.c.l.n = com.a.b.b.di.a(44).b();
        com.a.b.c.l.o = com.a.b.b.di.a(61).b();
        com.a.b.c.l.p = com.a.b.d.jt.l().a("initialCapacity", new com.a.b.c.q()).a("maximumSize", new com.a.b.c.u()).a("maximumWeight", new com.a.b.c.v()).a("concurrencyLevel", new com.a.b.c.o()).a("weakKeys", new com.a.b.c.s(com.a.b.c.bw.c)).a("softValues", new com.a.b.c.z(com.a.b.c.bw.b)).a("weakValues", new com.a.b.c.z(com.a.b.c.bw.c)).a("recordStats", new com.a.b.c.w()).a("expireAfterAccess", new com.a.b.c.n()).a("expireAfterWrite", new com.a.b.c.aa()).a("refreshAfterWrite", new com.a.b.c.x()).a("refreshInterval", new com.a.b.c.x()).a();
        return;
    }

    private l(String p1)
    {
        this.q = p1;
        return;
    }

    private static com.a.b.c.l a()
    {
        return com.a.b.c.l.a("maximumSize=0");
    }

    public static com.a.b.c.l a(String p10)
    {
        com.a.b.c.l v5_1 = new com.a.b.c.l(p10);
        if (!p10.isEmpty()) {
            java.util.Iterator v6 = com.a.b.c.l.n.a(p10).iterator();
            while (v6.hasNext()) {
                com.a.b.c.y v1_3;
                String v0_5 = ((String) v6.next());
                com.a.b.d.jl v7 = com.a.b.d.jl.a(com.a.b.c.l.o.a(v0_5));
                if (v7.isEmpty()) {
                    v1_3 = 0;
                } else {
                    v1_3 = 1;
                }
                com.a.b.c.y v1_5;
                com.a.b.b.cn.a(v1_3, "blank key-value pair");
                if (v7.size() > 2) {
                    v1_5 = 0;
                } else {
                    v1_5 = 1;
                }
                String v2_3;
                String v8_0 = new Object[1];
                v8_0[0] = v0_5;
                com.a.b.b.cn.a(v1_5, "key-value pair %s with more than one equals sign", v8_0);
                String v0_7 = ((String) v7.get(0));
                com.a.b.c.y v1_8 = ((com.a.b.c.y) com.a.b.c.l.p.get(v0_7));
                if (v1_8 == null) {
                    v2_3 = 0;
                } else {
                    v2_3 = 1;
                }
                String v2_6;
                Object[] v9 = new Object[1];
                v9[0] = v0_7;
                com.a.b.b.cn.a(v2_3, "unknown key %s", v9);
                if (v7.size() != 1) {
                    v2_6 = ((String) v7.get(1));
                } else {
                    v2_6 = 0;
                }
                v1_8.a(v5_1, v0_7, v2_6);
            }
        }
        return v5_1;
    }

    private static Long a(long p2, java.util.concurrent.TimeUnit p4)
    {
        Long v0_1;
        if (p4 != null) {
            v0_1 = Long.valueOf(p4.toNanos(p2));
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private com.a.b.c.f b()
    {
        com.a.b.c.f v3 = com.a.b.c.f.a();
        if (this.a != null) {
            long v0_3;
            long v4_0 = this.a.intValue();
            if (v3.g != -1) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            long v0_4;
            java.util.concurrent.TimeUnit v6_0 = new Object[1];
            v6_0[0] = Integer.valueOf(v3.g);
            com.a.b.b.cn.b(v0_3, "initial capacity was already set to %s", v6_0);
            if (v4_0 < 0) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            com.a.b.b.cn.a(v0_4);
            v3.g = v4_0;
        }
        if (this.b != null) {
            v3.a(this.b.longValue());
        }
        if (this.c != null) {
            v3.b(this.c.longValue());
        }
        if (this.d != null) {
            v3.a(this.d.intValue());
        }
        if (this.e != null) {
            switch (com.a.b.c.m.a[this.e.ordinal()]) {
                case 1:
                    v3.a(com.a.b.c.bw.c);
                    break;
                default:
                    throw new AssertionError();
            }
        }
        if (this.f != null) {
            switch (com.a.b.c.m.a[this.f.ordinal()]) {
                case 1:
                    v3.b(com.a.b.c.bw.c);
                    break;
                case 2:
                    v3.b(com.a.b.c.bw.b);
                    break;
                default:
                    throw new AssertionError();
            }
        }
        if ((this.g != null) && (this.g.booleanValue())) {
            v3.u = com.a.b.c.f.c;
        }
        if (this.i != null) {
            v3.a(this.h, this.i);
        }
        if (this.k != null) {
            v3.b(this.j, this.k);
        }
        if (this.m != null) {
            long v0_31;
            long v4_9 = this.l;
            java.util.concurrent.TimeUnit v6_1 = this.m;
            com.a.b.b.cn.a(v6_1);
            if (v3.p != -1) {
                v0_31 = 0;
            } else {
                v0_31 = 1;
            }
            long v0_33;
            Object[] v8_1 = new Object[1];
            v8_1[0] = Long.valueOf(v3.p);
            com.a.b.b.cn.b(v0_31, "refresh was already set to %s ns", v8_1);
            if (v4_9 <= 0) {
                v0_33 = 0;
            } else {
                v0_33 = 1;
            }
            Object[] v8_4 = new Object[2];
            v8_4[0] = Long.valueOf(v4_9);
            v8_4[1] = v6_1;
            com.a.b.b.cn.a(v0_33, "duration must be positive: %s %s", v8_4);
            v3.p = v6_1.toNanos(v4_9);
        }
        return v3;
    }

    private String c()
    {
        return this.q;
    }

    public final boolean equals(Object p7)
    {
        int v0 = 1;
        if (this != p7) {
            if ((p7 instanceof com.a.b.c.l)) {
                if ((!com.a.b.b.ce.a(this.a, ((com.a.b.c.l) p7).a)) || ((!com.a.b.b.ce.a(this.b, ((com.a.b.c.l) p7).b)) || ((!com.a.b.b.ce.a(this.c, ((com.a.b.c.l) p7).c)) || ((!com.a.b.b.ce.a(this.d, ((com.a.b.c.l) p7).d)) || ((!com.a.b.b.ce.a(this.e, ((com.a.b.c.l) p7).e)) || ((!com.a.b.b.ce.a(this.f, ((com.a.b.c.l) p7).f)) || ((!com.a.b.b.ce.a(this.g, ((com.a.b.c.l) p7).g)) || ((!com.a.b.b.ce.a(com.a.b.c.l.a(this.h, this.i), com.a.b.c.l.a(((com.a.b.c.l) p7).h, ((com.a.b.c.l) p7).i))) || ((!com.a.b.b.ce.a(com.a.b.c.l.a(this.j, this.k), com.a.b.c.l.a(((com.a.b.c.l) p7).j, ((com.a.b.c.l) p7).k))) || (!com.a.b.b.ce.a(com.a.b.c.l.a(this.l, this.m), com.a.b.c.l.a(((com.a.b.c.l) p7).l, ((com.a.b.c.l) p7).m)))))))))))) {
                    v0 = 0;
                }
            } else {
                v0 = 0;
            }
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[10];
        v0_1[0] = this.a;
        v0_1[1] = this.b;
        v0_1[2] = this.c;
        v0_1[3] = this.d;
        v0_1[4] = this.e;
        v0_1[5] = this.f;
        v0_1[6] = this.g;
        v0_1[7] = com.a.b.c.l.a(this.h, this.i);
        v0_1[8] = com.a.b.c.l.a(this.j, this.k);
        v0_1[9] = com.a.b.c.l.a(this.l, this.m);
        return java.util.Arrays.hashCode(v0_1);
    }

    public final String toString()
    {
        return com.a.b.b.ca.a(this).a(this.q).toString();
    }
}
