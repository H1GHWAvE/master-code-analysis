package com.a.b.c;
final class bl implements com.a.b.c.cg {
    volatile com.a.b.c.cg a;
    final com.a.b.n.a.fq b;
    final com.a.b.b.dw c;

    public bl()
    {
        this(com.a.b.c.ao.j());
        return;
    }

    public bl(com.a.b.c.cg p2)
    {
        this.b = com.a.b.n.a.fq.a();
        this.c = new com.a.b.b.dw();
        this.a = p2;
        return;
    }

    private static com.a.b.n.a.dp b(Throwable p1)
    {
        return com.a.b.n.a.ci.a(p1);
    }

    private com.a.b.c.cg g()
    {
        return this.a;
    }

    public final int a()
    {
        return this.a.a();
    }

    public final com.a.b.c.cg a(ref.ReferenceQueue p1, Object p2, com.a.b.c.bs p3)
    {
        return this;
    }

    public final com.a.b.n.a.dp a(Object p3, com.a.b.c.ab p4)
    {
        com.a.b.n.a.dp v0_4;
        this.c.b();
        com.a.b.n.a.dp v0_2 = this.a.get();
        try {
            if (v0_2 != null) {
                com.a.b.n.a.dp v0_3 = p4.a(p3, v0_2);
                if (v0_3 != null) {
                    v0_4 = com.a.b.n.a.ci.a(v0_3, new com.a.b.c.bm(this));
                } else {
                    v0_4 = com.a.b.n.a.ci.a(0);
                }
            } else {
                com.a.b.n.a.dp v0_6 = p4.a(p3);
                if (!this.b(v0_6)) {
                    v0_4 = com.a.b.n.a.ci.a(v0_6);
                } else {
                    v0_4 = this.b;
                }
            }
        } catch (com.a.b.n.a.dp v0_7) {
            if ((v0_7 instanceof InterruptedException)) {
                Thread.currentThread().interrupt();
            }
            if (!this.a(v0_7)) {
                v0_4 = com.a.b.n.a.ci.a(v0_7);
            } else {
                v0_4 = this.b;
            }
        }
        return v0_4;
    }

    public final void a(Object p2)
    {
        if (p2 == null) {
            this.a = com.a.b.c.ao.j();
        } else {
            this.b(p2);
        }
        return;
    }

    public final boolean a(Throwable p2)
    {
        return this.b.a(p2);
    }

    public final com.a.b.c.bs b()
    {
        return 0;
    }

    public final boolean b(Object p2)
    {
        return this.b.a(p2);
    }

    public final boolean c()
    {
        return 1;
    }

    public final boolean d()
    {
        return this.a.d();
    }

    public final Object e()
    {
        return com.a.b.n.a.gs.a(this.b);
    }

    public final long f()
    {
        return this.c.a(java.util.concurrent.TimeUnit.NANOSECONDS);
    }

    public final Object get()
    {
        return this.a.get();
    }
}
