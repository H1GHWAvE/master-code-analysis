package com.a.b.c;
abstract class p implements com.a.b.c.y {

    p()
    {
        return;
    }

    protected abstract void a();

    public final void a(com.a.b.c.l p8, String p9, String p10)
    {
        if ((p10 == null) || (p10.isEmpty())) {
            java.util.concurrent.TimeUnit v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        long v4_0 = new Object[1];
        v4_0[0] = p9;
        com.a.b.b.cn.a(v0_1, "value of key %s omitted", v4_0);
        try {
            java.util.concurrent.TimeUnit v0_5;
            switch (p10.charAt((p10.length() - 1))) {
                case 100:
                    v0_5 = java.util.concurrent.TimeUnit.DAYS;
                    this.a(p8, Long.parseLong(p10.substring(0, (p10.length() - 1))), v0_5);
                    return;
                case 104:
                    v0_5 = java.util.concurrent.TimeUnit.HOURS;
                    this.a(p8, Long.parseLong(p10.substring(0, (p10.length() - 1))), v0_5);
                    return;
                case 109:
                    v0_5 = java.util.concurrent.TimeUnit.MINUTES;
                    this.a(p8, Long.parseLong(p10.substring(0, (p10.length() - 1))), v0_5);
                    return;
                case 115:
                    v0_5 = java.util.concurrent.TimeUnit.SECONDS;
                    this.a(p8, Long.parseLong(p10.substring(0, (p10.length() - 1))), v0_5);
                    return;
                default:
                    long v4_5 = new Object[2];
                    v4_5[0] = p9;
                    v4_5[1] = p10;
                    throw new IllegalArgumentException(String.format("key %s invalid format.  was %s, must end with one of [dDhHmMsS]", v4_5));
            }
        } catch (java.util.concurrent.TimeUnit v0) {
        }
        long v4_6 = new Object[2];
        v4_6[0] = p9;
        v4_6[1] = p10;
        throw new IllegalArgumentException(String.format("key %s value set to %s, must be integer", v4_6));
    }
}
