package com.a.b.c;
abstract class dl extends java.lang.Number {
    static final ThreadLocal a;
    static final java.util.Random b;
    static final int c;
    private static final sun.misc.Unsafe g;
    private static final long h;
    private static final long i;
    transient volatile com.a.b.c.dn[] d;
    transient volatile long e;
    transient volatile int f;

    static dl()
    {
        com.a.b.c.dl.a = new ThreadLocal();
        com.a.b.c.dl.b = new java.util.Random();
        com.a.b.c.dl.c = Runtime.getRuntime().availableProcessors();
        try {
            com.a.b.c.dl.g = com.a.b.c.dl.b();
            com.a.b.c.dl.h = com.a.b.c.dl.g.objectFieldOffset(com.a.b.c.dl.getDeclaredField("base"));
            com.a.b.c.dl.i = com.a.b.c.dl.g.objectFieldOffset(com.a.b.c.dl.getDeclaredField("busy"));
            return;
        } catch (Exception v0_10) {
            throw new Error(v0_10);
        }
    }

    dl()
    {
        return;
    }

    private void a()
    {
        com.a.b.c.dn[] v1 = this.d;
        this.e = 0;
        if (v1 != null) {
            int v2 = v1.length;
            int v0 = 0;
            while (v0 < v2) {
                com.a.b.c.dn v3 = v1[v0];
                if (v3 != null) {
                    v3.h = 0;
                }
                v0++;
            }
        }
        return;
    }

    private void a(long p12, int[] p14, boolean p15)
    {
        int v0_1;
        if (p14 != null) {
            v0_1 = p14[0];
        } else {
            p14 = new int[1];
            com.a.b.c.dl.a.set(p14);
            v0_1 = com.a.b.c.dl.b.nextInt();
            if (v0_1 == 0) {
                v0_1 = 1;
            }
            p14[0] = v0_1;
        }
        int v1_3 = v0_1;
        int v0_4 = 0;
        while(true) {
            int v3_0 = this.d;
            if (v3_0 == 0) {
                break;
            }
            int v2_0 = v3_0.length;
            if (v2_0 <= 0) {
                break;
            }
            com.a.b.c.dn[] v4_6 = v3_0[((v2_0 - 1) & v1_3)];
            if (v4_6 != null) {
                if (p15 != 0) {
                    int v6_0 = v4_6.h;
                    if (v4_6.a(v6_0, this.a(v6_0, p12))) {
                        return;
                    } else {
                        if ((v2_0 < com.a.b.c.dl.c) && (this.d == v3_0)) {
                            if (v0_4 != 0) {
                                if ((this.f == 0) && (this.c())) {
                                    try {
                                        if (this.d == v3_0) {
                                            com.a.b.c.dn[] v4_12 = new com.a.b.c.dn[(v2_0 << 1)];
                                            int v0_8 = 0;
                                            while (v0_8 < v2_0) {
                                                v4_12[v0_8] = v3_0[v0_8];
                                                v0_8++;
                                            }
                                            this.d = v4_12;
                                        }
                                        this.f = 0;
                                        v0_4 = 0;
                                    } catch (int v0_10) {
                                        this.f = 0;
                                        throw v0_10;
                                    }
                                }
                            } else {
                                v0_4 = 1;
                            }
                        } else {
                            v0_4 = 0;
                        }
                    }
                } else {
                    p15 = 1;
                }
            } else {
                if (this.f == 0) {
                    int v3_4 = new com.a.b.c.dn(p12);
                    if ((this.f == 0) && (this.c())) {
                        int v2_16 = 0;
                        try {
                            com.a.b.c.dn[] v4_13 = this.d;
                        } catch (int v0_11) {
                            this.f = 0;
                            throw v0_11;
                        }
                        if (v4_13 != null) {
                            int v5_1 = v4_13.length;
                            if (v5_1 > 0) {
                                int v5_3 = ((v5_1 - 1) & v1_3);
                                if (v4_13[v5_3] == null) {
                                    v4_13[v5_3] = v3_4;
                                    v2_16 = 1;
                                }
                            }
                        }
                        this.f = 0;
                        if (v2_16 != 0) {
                            return;
                        }
                    }
                }
                v0_4 = 0;
            }
            int v1_6 = (v1_3 ^ (v1_3 << 13));
            int v1_7 = (v1_6 ^ (v1_6 >> 17));
            v1_3 = (v1_7 ^ (v1_7 << 5));
            p14[0] = v1_3;
        }
        if ((this.f != 0) || ((this.d != v3_0) || (!this.c()))) {
            int v2_4 = this.e;
            if (this.b(v2_4, this.a(v2_4, p12))) {
            }
        } else {
            int v2_6 = 0;
            try {
                if (this.d == v3_0) {
                    int v2_8 = new com.a.b.c.dn[2];
                    v2_8[(v1_3 & 1)] = new com.a.b.c.dn(p12);
                    this.d = v2_8;
                    v2_6 = 1;
                }
            } catch (int v0_5) {
                this.f = 0;
                throw v0_5;
            }
            this.f = 0;
            if (v2_6 == 0) {
            }
        }
        return;
    }

    private static sun.misc.Unsafe b()
    {
        try {
            Throwable v0_0 = sun.misc.Unsafe.getUnsafe();
        } catch (Throwable v0) {
            try {
                v0_0 = ((sun.misc.Unsafe) java.security.AccessController.doPrivileged(new com.a.b.c.dm()));
            } catch (Throwable v0_4) {
                throw new RuntimeException("Could not initialize intrinsics", v0_4.getCause());
            }
        }
        return v0_0;
    }

    static synthetic sun.misc.Unsafe d()
    {
        return com.a.b.c.dl.b();
    }

    abstract long a();

    final boolean b(long p10, long p12)
    {
        return com.a.b.c.dl.g.compareAndSwapLong(this, com.a.b.c.dl.h, p10, p12);
    }

    final boolean c()
    {
        return com.a.b.c.dl.g.compareAndSwapInt(this, com.a.b.c.dl.i, 0, 1);
    }
}
