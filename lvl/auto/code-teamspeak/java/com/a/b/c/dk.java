package com.a.b.c;
public final class dk implements java.util.Map$Entry {
    private static final long d;
    private final Object a;
    private final Object b;
    private final com.a.b.c.da c;

    dk(Object p2, Object p3, com.a.b.c.da p4)
    {
        this.a = p2;
        this.b = p3;
        this.c = ((com.a.b.c.da) com.a.b.b.cn.a(p4));
        return;
    }

    private com.a.b.c.da a()
    {
        return this.c;
    }

    private boolean b()
    {
        return this.c.a();
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof java.util.Map$Entry)) && ((com.a.b.b.ce.a(this.getKey(), ((java.util.Map$Entry) p4).getKey())) && (com.a.b.b.ce.a(this.getValue(), ((java.util.Map$Entry) p4).getValue())))) {
            v0 = 1;
        }
        return v0;
    }

    public final Object getKey()
    {
        return this.a;
    }

    public final Object getValue()
    {
        return this.b;
    }

    public final int hashCode()
    {
        int v1_1;
        int v0_0 = 0;
        int v1_0 = this.getKey();
        Object v2 = this.getValue();
        if (v1_0 != 0) {
            v1_1 = v1_0.hashCode();
        } else {
            v1_1 = 0;
        }
        if (v2 != null) {
            v0_0 = v2.hashCode();
        }
        return (v0_0 ^ v1_1);
    }

    public final Object setValue(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.getKey()));
        String v1_2 = String.valueOf(String.valueOf(this.getValue()));
        return new StringBuilder(((v0_2.length() + 1) + v1_2.length())).append(v0_2).append("=").append(v1_2).toString();
    }
}
