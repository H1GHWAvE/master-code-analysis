package com.a.b.c;
final class bn extends com.a.b.c.bo implements com.a.b.c.an {
    private static final long b = 1;

    bn(com.a.b.c.f p3, com.a.b.c.ab p4)
    {
        this(new com.a.b.c.ao(p3, ((com.a.b.c.ab) com.a.b.b.cn.a(p4))), 0);
        return;
    }

    public final Object b(Object p3)
    {
        try {
            return this.f(p3);
        } catch (Throwable v0_1) {
            throw new com.a.b.n.a.gq(v0_1.getCause());
        }
    }

    public final com.a.b.d.jt c(Iterable p2)
    {
        return this.a.a(p2);
    }

    public final void c(Object p5)
    {
        com.a.b.c.ab v0_0 = this.a;
        int v1_1 = v0_0.a(com.a.b.b.cn.a(p5));
        v0_0.a(v1_1).a(p5, v1_1, v0_0.y, 0);
        return;
    }

    public final Object e(Object p2)
    {
        return this.b(p2);
    }

    final Object f()
    {
        return new com.a.b.c.bk(this.a);
    }

    public final Object f(Object p3)
    {
        Object v0_0 = this.a;
        return v0_0.a(p3, v0_0.y);
    }
}
