package com.a.b.c;
final enum class j extends java.lang.Enum implements com.a.b.c.dg {
    public static final enum com.a.b.c.j a;
    private static final synthetic com.a.b.c.j[] b;

    static j()
    {
        com.a.b.c.j.a = new com.a.b.c.j("INSTANCE");
        com.a.b.c.j[] v0_3 = new com.a.b.c.j[1];
        v0_3[0] = com.a.b.c.j.a;
        com.a.b.c.j.b = v0_3;
        return;
    }

    private j(String p2)
    {
        this(p2, 0);
        return;
    }

    public static com.a.b.c.j valueOf(String p1)
    {
        return ((com.a.b.c.j) Enum.valueOf(com.a.b.c.j, p1));
    }

    public static com.a.b.c.j[] values()
    {
        return ((com.a.b.c.j[]) com.a.b.c.j.b.clone());
    }

    public final void a(com.a.b.c.dk p1)
    {
        return;
    }
}
