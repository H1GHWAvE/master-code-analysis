package com.a.b.c;
abstract class r implements com.a.b.c.y {

    r()
    {
        return;
    }

    protected abstract void a();

    public final void a(com.a.b.c.l p7, String p8, String p9)
    {
        if ((p9 == null) || (p9.isEmpty())) {
            NumberFormatException v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        String v4_0 = new Object[1];
        v4_0[0] = p8;
        com.a.b.b.cn.a(v0_1, "value of key %s omitted", v4_0);
        try {
            this.a(p7, Integer.parseInt(p9));
            return;
        } catch (NumberFormatException v0_3) {
            Object[] v5_1 = new Object[2];
            v5_1[0] = p8;
            v5_1[1] = p9;
            throw new IllegalArgumentException(String.format("key %s value set to %s, must be integer", v5_1), v0_3);
        }
    }
}
