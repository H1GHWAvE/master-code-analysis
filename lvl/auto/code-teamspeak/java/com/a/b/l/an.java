package com.a.b.l;
final enum class an extends java.lang.Enum implements java.util.Comparator {
    public static final enum com.a.b.l.an a;
    private static final synthetic com.a.b.l.an[] b;

    static an()
    {
        com.a.b.l.an.a = new com.a.b.l.an("INSTANCE");
        com.a.b.l.an[] v0_3 = new com.a.b.l.an[1];
        v0_3[0] = com.a.b.l.an.a;
        com.a.b.l.an.b = v0_3;
        return;
    }

    private an(String p2)
    {
        this(p2, 0);
        return;
    }

    private static int a(int[] p4, int[] p5)
    {
        int v1_1 = Math.min(p4.length, p5.length);
        int v0_1 = 0;
        while (v0_1 < v1_1) {
            if (p4[v0_1] == p5[v0_1]) {
                v0_1++;
            } else {
                int v0_3 = com.a.b.l.am.a(p4[v0_1], p5[v0_1]);
            }
            return v0_3;
        }
        v0_3 = (p4.length - p5.length);
        return v0_3;
    }

    public static com.a.b.l.an valueOf(String p1)
    {
        return ((com.a.b.l.an) Enum.valueOf(com.a.b.l.an, p1));
    }

    public static com.a.b.l.an[] values()
    {
        return ((com.a.b.l.an[]) com.a.b.l.an.b.clone());
    }

    public final synthetic int compare(Object p5, Object p6)
    {
        int v1_1 = Math.min(((int[]) p5).length, ((int[]) p6).length);
        int v0_1 = 0;
        while (v0_1 < v1_1) {
            if (((int[]) p5)[v0_1] == ((int[]) p6)[v0_1]) {
                v0_1++;
            } else {
                int v0_3 = com.a.b.l.am.a(((int[]) p5)[v0_1], ((int[]) p6)[v0_1]);
            }
            return v0_3;
        }
        v0_3 = (((int[]) p5).length - ((int[]) p6).length);
        return v0_3;
    }
}
