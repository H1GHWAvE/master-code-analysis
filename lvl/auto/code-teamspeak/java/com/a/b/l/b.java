package com.a.b.l;
final class b extends java.util.AbstractList implements java.io.Serializable, java.util.RandomAccess {
    private static final long d;
    final boolean[] a;
    final int b;
    final int c;

    b(boolean[] p3)
    {
        this(p3, 0, p3.length);
        return;
    }

    private b(boolean[] p1, int p2, int p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    private Boolean a(int p3)
    {
        com.a.b.b.cn.a(p3, this.size());
        return Boolean.valueOf(this.a[(this.b + p3)]);
    }

    private Boolean a(int p5, Boolean p6)
    {
        com.a.b.b.cn.a(p5, this.size());
        boolean v1_2 = this.a[(this.b + p5)];
        this.a[(this.b + p5)] = ((Boolean) com.a.b.b.cn.a(p6)).booleanValue();
        return Boolean.valueOf(v1_2);
    }

    private boolean[] a()
    {
        int v0 = this.size();
        boolean[] v1 = new boolean[v0];
        System.arraycopy(this.a, this.b, v1, 0, v0);
        return v1;
    }

    public final boolean contains(Object p5)
    {
        if ((!(p5 instanceof Boolean)) || (com.a.b.l.a.a(this.a, ((Boolean) p5).booleanValue(), this.b, this.c) == -1)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean equals(Object p8)
    {
        int v0 = 1;
        if (p8 != this) {
            if (!(p8 instanceof com.a.b.l.b)) {
                v0 = super.equals(p8);
            } else {
                int v3 = this.size();
                if (((com.a.b.l.b) p8).size() == v3) {
                    int v2_2 = 0;
                    while (v2_2 < v3) {
                        if (this.a[(this.b + v2_2)] == ((com.a.b.l.b) p8).a[(((com.a.b.l.b) p8).b + v2_2)]) {
                            v2_2++;
                        } else {
                            v0 = 0;
                            break;
                        }
                    }
                } else {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public final synthetic Object get(int p3)
    {
        com.a.b.b.cn.a(p3, this.size());
        return Boolean.valueOf(this.a[(this.b + p3)]);
    }

    public final int hashCode()
    {
        int v1_0 = 1;
        int v0 = this.b;
        while (v0 < this.c) {
            int v1_3;
            if (!this.a[v0]) {
                v1_3 = 1237;
            } else {
                v1_3 = 1231;
            }
            v1_0 = (v1_3 + (v1_0 * 31));
            v0++;
        }
        return v1_0;
    }

    public final int indexOf(Object p5)
    {
        int v0_3;
        if (!(p5 instanceof Boolean)) {
            v0_3 = -1;
        } else {
            int v0_2 = com.a.b.l.a.a(this.a, ((Boolean) p5).booleanValue(), this.b, this.c);
            if (v0_2 < 0) {
            } else {
                v0_3 = (v0_2 - this.b);
            }
        }
        return v0_3;
    }

    public final boolean isEmpty()
    {
        return 0;
    }

    public final int lastIndexOf(Object p5)
    {
        int v0_3;
        if (!(p5 instanceof Boolean)) {
            v0_3 = -1;
        } else {
            int v0_2 = com.a.b.l.a.b(this.a, ((Boolean) p5).booleanValue(), this.b, this.c);
            if (v0_2 < 0) {
            } else {
                v0_3 = (v0_2 - this.b);
            }
        }
        return v0_3;
    }

    public final synthetic Object set(int p5, Object p6)
    {
        com.a.b.b.cn.a(p5, this.size());
        boolean v1_2 = this.a[(this.b + p5)];
        this.a[(this.b + p5)] = ((Boolean) com.a.b.b.cn.a(((Boolean) p6))).booleanValue();
        return Boolean.valueOf(v1_2);
    }

    public final int size()
    {
        return (this.c - this.b);
    }

    public final java.util.List subList(int p5, int p6)
    {
        com.a.b.l.b v0_2;
        com.a.b.b.cn.a(p5, p6, this.size());
        if (p5 != p6) {
            v0_2 = new com.a.b.l.b(this.a, (this.b + p5), (this.b + p6));
        } else {
            v0_2 = java.util.Collections.emptyList();
        }
        return v0_2;
    }

    public final String toString()
    {
        int v0_4;
        StringBuilder v2_1 = new StringBuilder((this.size() * 7));
        if (!this.a[this.b]) {
            v0_4 = "[false";
        } else {
            v0_4 = "[true";
        }
        v2_1.append(v0_4);
        int v0_6 = (this.b + 1);
        while (v0_6 < this.c) {
            String v1_4;
            if (!this.a[v0_6]) {
                v1_4 = ", false";
            } else {
                v1_4 = ", true";
            }
            v2_1.append(v1_4);
            v0_6++;
        }
        return v2_1.append(93).toString();
    }
}
