package com.a.b.l;
final class w extends java.util.AbstractList implements java.io.Serializable, java.util.RandomAccess {
    private static final long d;
    final long[] a;
    final int b;
    final int c;

    w(long[] p3)
    {
        this(p3, 0, p3.length);
        return;
    }

    private w(long[] p1, int p2, int p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    private Long a(int p3)
    {
        com.a.b.b.cn.a(p3, this.size());
        return Long.valueOf(this.a[(this.b + p3)]);
    }

    private Long a(int p9, Long p10)
    {
        com.a.b.b.cn.a(p9, this.size());
        long v2 = this.a[(this.b + p9)];
        this.a[(this.b + p9)] = ((Long) com.a.b.b.cn.a(p10)).longValue();
        return Long.valueOf(v2);
    }

    private long[] a()
    {
        int v0 = this.size();
        long[] v1 = new long[v0];
        System.arraycopy(this.a, this.b, v1, 0, v0);
        return v1;
    }

    public final boolean contains(Object p6)
    {
        if ((!(p6 instanceof Long)) || (com.a.b.l.u.b(this.a, ((Long) p6).longValue(), this.b, this.c) == -1)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean equals(Object p9)
    {
        int v0 = 1;
        if (p9 != this) {
            if (!(p9 instanceof com.a.b.l.w)) {
                v0 = super.equals(p9);
            } else {
                int v3 = this.size();
                if (((com.a.b.l.w) p9).size() == v3) {
                    int v2_2 = 0;
                    while (v2_2 < v3) {
                        if (this.a[(this.b + v2_2)] == ((com.a.b.l.w) p9).a[(((com.a.b.l.w) p9).b + v2_2)]) {
                            v2_2++;
                        } else {
                            v0 = 0;
                            break;
                        }
                    }
                } else {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public final synthetic Object get(int p3)
    {
        com.a.b.b.cn.a(p3, this.size());
        return Long.valueOf(this.a[(this.b + p3)]);
    }

    public final int hashCode()
    {
        int v1_0 = 1;
        int v0 = this.b;
        while (v0 < this.c) {
            int v2_2 = this.a[v0];
            v1_0 = ((v1_0 * 31) + ((int) (v2_2 ^ (v2_2 >> 32))));
            v0++;
        }
        return v1_0;
    }

    public final int indexOf(Object p6)
    {
        int v0_3;
        if (!(p6 instanceof Long)) {
            v0_3 = -1;
        } else {
            int v0_2 = com.a.b.l.u.b(this.a, ((Long) p6).longValue(), this.b, this.c);
            if (v0_2 < 0) {
            } else {
                v0_3 = (v0_2 - this.b);
            }
        }
        return v0_3;
    }

    public final boolean isEmpty()
    {
        return 0;
    }

    public final int lastIndexOf(Object p6)
    {
        int v0_3;
        if (!(p6 instanceof Long)) {
            v0_3 = -1;
        } else {
            int v0_2 = com.a.b.l.u.a(this.a, ((Long) p6).longValue(), this.b, this.c);
            if (v0_2 < 0) {
            } else {
                v0_3 = (v0_2 - this.b);
            }
        }
        return v0_3;
    }

    public final synthetic Object set(int p9, Object p10)
    {
        com.a.b.b.cn.a(p9, this.size());
        long v2 = this.a[(this.b + p9)];
        this.a[(this.b + p9)] = ((Long) com.a.b.b.cn.a(((Long) p10))).longValue();
        return Long.valueOf(v2);
    }

    public final int size()
    {
        return (this.c - this.b);
    }

    public final java.util.List subList(int p5, int p6)
    {
        com.a.b.l.w v0_2;
        com.a.b.b.cn.a(p5, p6, this.size());
        if (p5 != p6) {
            v0_2 = new com.a.b.l.w(this.a, (this.b + p5), (this.b + p6));
        } else {
            v0_2 = java.util.Collections.emptyList();
        }
        return v0_2;
    }

    public final String toString()
    {
        StringBuilder v1_1 = new StringBuilder((this.size() * 10));
        v1_1.append(91).append(this.a[this.b]);
        String v0_5 = (this.b + 1);
        while (v0_5 < this.c) {
            v1_1.append(", ").append(this.a[v0_5]);
            v0_5++;
        }
        return v1_1.append(93).toString();
    }
}
