package com.a.b.n.a;
final class y extends com.a.b.n.a.cb implements java.util.concurrent.Callable {
    final synthetic com.a.b.n.a.x a;
    private final Runnable b;
    private final java.util.concurrent.ScheduledExecutorService c;
    private final com.a.b.n.a.ad d;
    private final java.util.concurrent.locks.ReentrantLock e;
    private java.util.concurrent.Future f;

    y(com.a.b.n.a.x p2, com.a.b.n.a.ad p3, java.util.concurrent.ScheduledExecutorService p4, Runnable p5)
    {
        this.a = p2;
        this.e = new java.util.concurrent.locks.ReentrantLock();
        this.b = p5;
        this.c = p4;
        this.d = p3;
        return;
    }

    private Void c()
    {
        this.b.run();
        this.a();
        return 0;
    }

    public final void a()
    {
        this.e.lock();
        try {
            if ((this.f != null) && (this.f.isCancelled())) {
                this.e.unlock();
            } else {
                java.util.concurrent.locks.ReentrantLock v0_5 = this.a.a();
                this.f = this.c.schedule(this, v0_5.a, v0_5.b);
            }
        } catch (java.util.concurrent.locks.ReentrantLock v0_10) {
            this.e.unlock();
            throw v0_10;
        } catch (java.util.concurrent.locks.ReentrantLock v0_8) {
            this.d.a(v0_8);
            this.e.unlock();
        }
        return;
    }

    protected final java.util.concurrent.Future b()
    {
        throw new UnsupportedOperationException("Only cancel is supported by this future");
    }

    public final synthetic Object call()
    {
        return this.c();
    }

    public final boolean cancel(boolean p3)
    {
        this.e.lock();
        try {
            Throwable v0_2 = this.f.cancel(p3);
            this.e.unlock();
            return v0_2;
        } catch (Throwable v0_3) {
            this.e.unlock();
            throw v0_3;
        }
    }

    protected final synthetic Object k_()
    {
        return this.b();
    }
}
