package com.a.b.n.a;
public final class gs {

    private gs()
    {
        return;
    }

    private static Object a(java.util.concurrent.BlockingQueue p2)
    {
        Thread v0_0 = 0;
        try {
            while(true) {
                Throwable v1_0 = p2.take();
                v0_0 = 1;
            }
        } catch (Thread v0) {
        } catch (Throwable v1_1) {
            if (v0_0 != null) {
                Thread.currentThread().interrupt();
            }
            throw v1_1;
        }
        if (v0_0 != null) {
            Thread.currentThread().interrupt();
        }
        return v1_0;
    }

    public static Object a(java.util.concurrent.Future p2)
    {
        Thread v0_0 = 0;
        try {
            while(true) {
                Throwable v1_0 = p2.get();
                v0_0 = 1;
            }
        } catch (Thread v0) {
        } catch (Throwable v1_1) {
            if (v0_0 != null) {
                Thread.currentThread().interrupt();
            }
            throw v1_1;
        }
        if (v0_0 != null) {
            Thread.currentThread().interrupt();
        }
        return v1_0;
    }

    public static Object a(java.util.concurrent.Future p7, long p8, java.util.concurrent.TimeUnit p10)
    {
        Thread v1_0 = 0;
        try {
            long v2_0 = p10.toNanos(p8);
            long v4_1 = (System.nanoTime() + v2_0);
        } catch (InterruptedException v0_2) {
            if (v1_0 != null) {
                Thread.currentThread().interrupt();
            }
            throw v0_2;
        }
        try {
            while(true) {
                InterruptedException v0_1 = p7.get(v2_0, java.util.concurrent.TimeUnit.NANOSECONDS);
                v2_0 = (v4_1 - System.nanoTime());
            }
        } catch (InterruptedException v0) {
            v1_0 = 1;
        }
        if (v1_0 != null) {
            Thread.currentThread().interrupt();
        }
        return v0_1;
    }

    private static void a(long p6, java.util.concurrent.TimeUnit p8)
    {
        int v1_0 = 0;
        try {
            long v2_0 = p8.toNanos(p6);
            long v4_1 = (System.nanoTime() + v2_0);
            try {
                while(true) {
                    java.util.concurrent.TimeUnit.NANOSECONDS.sleep(v2_0);
                    v2_0 = (v4_1 - System.nanoTime());
                }
            } catch (Thread v0) {
                v1_0 = 1;
            }
            if (v1_0 != 0) {
                Thread.currentThread().interrupt();
            }
            return;
        } catch (Thread v0_1) {
            if (v1_0 != 0) {
                Thread.currentThread().interrupt();
            }
            throw v0_1;
        }
    }

    private static void a(Thread p2)
    {
        Thread v0_0 = 0;
        try {
            while(true) {
                p2.join();
                v0_0 = 1;
            }
        } catch (Throwable v1) {
            if (v0_0 != null) {
                Thread.currentThread().interrupt();
            }
            throw v1;
        } catch (Thread v0) {
        }
        if (v0_0 != null) {
            Thread.currentThread().interrupt();
        }
        return;
    }

    private static void a(Thread p7, long p8, java.util.concurrent.TimeUnit p10)
    {
        com.a.b.b.cn.a(p7);
        int v1_0 = 0;
        try {
            long v2_0 = p10.toNanos(p8);
            long v4_1 = (System.nanoTime() + v2_0);
            try {
                while(true) {
                    java.util.concurrent.TimeUnit.NANOSECONDS.timedJoin(p7, v2_0);
                    v2_0 = (v4_1 - System.nanoTime());
                }
            } catch (Thread v0) {
                v1_0 = 1;
            }
            if (v1_0 != 0) {
                Thread.currentThread().interrupt();
            }
            return;
        } catch (Thread v0_1) {
            if (v1_0 != 0) {
                Thread.currentThread().interrupt();
            }
            throw v0_1;
        }
    }

    private static void a(java.util.concurrent.BlockingQueue p2, Object p3)
    {
        Thread v0_0 = 0;
        try {
            while(true) {
                p2.put(p3);
                v0_0 = 1;
            }
        } catch (Throwable v1) {
            if (v0_0 != null) {
                Thread.currentThread().interrupt();
            }
            throw v1;
        } catch (Thread v0) {
        }
        if (v0_0 != null) {
            Thread.currentThread().interrupt();
        }
        return;
    }

    private static void a(java.util.concurrent.CountDownLatch p2)
    {
        Thread v0_0 = 0;
        try {
            while(true) {
                p2.await();
                v0_0 = 1;
            }
        } catch (Thread v0) {
        } catch (Throwable v1) {
            if (v0_0 != null) {
                Thread.currentThread().interrupt();
            }
            throw v1;
        }
        if (v0_0 != null) {
            Thread.currentThread().interrupt();
        }
        return;
    }

    private static boolean a(java.util.concurrent.CountDownLatch p7, long p8, java.util.concurrent.TimeUnit p10)
    {
        Thread v1_0 = 0;
        try {
            long v2_0 = p10.toNanos(p8);
            long v4_1 = (System.nanoTime() + v2_0);
            try {
                while(true) {
                    InterruptedException v0_1 = p7.await(v2_0, java.util.concurrent.TimeUnit.NANOSECONDS);
                    v2_0 = (v4_1 - System.nanoTime());
                }
            } catch (InterruptedException v0) {
                v1_0 = 1;
            }
            if (v1_0 != null) {
                Thread.currentThread().interrupt();
            }
            return v0_1;
        } catch (InterruptedException v0_2) {
            if (v1_0 != null) {
                Thread.currentThread().interrupt();
            }
            throw v0_2;
        }
    }

    private static boolean a(java.util.concurrent.Semaphore p1, long p2, java.util.concurrent.TimeUnit p4)
    {
        return com.a.b.n.a.gs.b(p1, p2, p4);
    }

    private static boolean b(java.util.concurrent.Semaphore p9, long p10, java.util.concurrent.TimeUnit p12)
    {
        int v1_0 = 0;
        try {
            long v4 = p12.toNanos(p10);
            long v6_1 = (System.nanoTime() + v4);
        } catch (Throwable v0_4) {
            if (v1_0 != 0) {
                Thread.currentThread().interrupt();
            }
            throw v0_4;
        }
        try {
            while(true) {
                Throwable v0_1 = p9.tryAcquire(1, v4, java.util.concurrent.TimeUnit.NANOSECONDS);
                try {
                    v4 = (v6_1 - System.nanoTime());
                    v1_0 = 1;
                } catch (Throwable v0_4) {
                    v1_0 = 1;
                }
            }
        } catch (Throwable v0) {
        }
        if (v1_0 != 0) {
            Thread.currentThread().interrupt();
        }
        return v0_1;
    }
}
