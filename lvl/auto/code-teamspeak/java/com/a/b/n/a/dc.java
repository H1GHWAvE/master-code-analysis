package com.a.b.n.a;
final class dc extends com.a.b.n.a.df {
    private final java.util.concurrent.CancellationException a;

    dc()
    {
        this(0);
        this.a = new java.util.concurrent.CancellationException("Immediate cancelled future.");
        return;
    }

    public final Object get()
    {
        throw com.a.b.n.a.g.a("Task was cancelled.", this.a);
    }

    public final boolean isCancelled()
    {
        return 1;
    }
}
