package com.a.b.n.a;
final class cj implements java.lang.Runnable {
    final synthetic java.util.concurrent.Executor a;
    final synthetic Runnable b;
    final synthetic com.a.b.n.a.g c;

    cj(java.util.concurrent.Executor p1, Runnable p2, com.a.b.n.a.g p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    public final void run()
    {
        com.a.b.n.a.g v1_1 = new java.util.concurrent.atomic.AtomicBoolean(1);
        try {
            this.a.execute(new com.a.b.n.a.ck(this, v1_1));
        } catch (java.util.concurrent.RejectedExecutionException v0_2) {
            if (!v1_1.get()) {
            } else {
                this.c.a(v0_2);
            }
        }
        return;
    }
}
