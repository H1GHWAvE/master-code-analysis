package com.a.b.n.a;
final class eq implements java.util.concurrent.Executor {
    private static final java.util.logging.Logger a;
    private final java.util.concurrent.Executor b;
    private final java.util.Queue c;
    private boolean d;
    private final com.a.b.n.a.es e;
    private final Object f;

    static eq()
    {
        com.a.b.n.a.eq.a = java.util.logging.Logger.getLogger(com.a.b.n.a.eq.getName());
        return;
    }

    public eq(java.util.concurrent.Executor p3)
    {
        this.c = new java.util.ArrayDeque();
        this.d = 0;
        this.e = new com.a.b.n.a.es(this, 0);
        this.f = new com.a.b.n.a.er(this);
        com.a.b.b.cn.a(p3, "\'executor\' must not be null.");
        this.b = p3;
        return;
    }

    static synthetic java.util.logging.Logger a()
    {
        return com.a.b.n.a.eq.a;
    }

    static synthetic boolean a(com.a.b.n.a.eq p1)
    {
        return p1.d;
    }

    static synthetic Object b(com.a.b.n.a.eq p1)
    {
        return p1.f;
    }

    static synthetic java.util.Queue c(com.a.b.n.a.eq p1)
    {
        return p1.c;
    }

    static synthetic boolean d(com.a.b.n.a.eq p1)
    {
        p1.d = 0;
        return 0;
    }

    public final void execute(Runnable p5)
    {
        Throwable v0_0 = 1;
        com.a.b.b.cn.a(p5, "\'r\' must not be null.");
        this.c.add(p5);
        if (this.d) {
            v0_0 = 0;
        } else {
            this.d = 1;
        }
        if (v0_0 != null) {
            try {
                this.b.execute(this.e);
            } catch (Throwable v0_3) {
                this.d = 0;
                throw v0_3;
            }
        }
        return;
    }
}
