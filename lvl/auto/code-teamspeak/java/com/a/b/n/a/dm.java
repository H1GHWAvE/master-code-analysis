package com.a.b.n.a;
public final class dm {

    private dm()
    {
        return;
    }

    private static com.a.b.n.a.dp a(java.util.concurrent.Future p1)
    {
        com.a.b.n.a.dn v1_1;
        if (!(p1 instanceof com.a.b.n.a.dp)) {
            v1_1 = new com.a.b.n.a.dn(p1);
        } else {
            v1_1 = ((com.a.b.n.a.dp) p1);
        }
        return v1_1;
    }

    private static com.a.b.n.a.dp a(java.util.concurrent.Future p1, java.util.concurrent.Executor p2)
    {
        com.a.b.n.a.dn v1_1;
        com.a.b.b.cn.a(p2);
        if (!(p1 instanceof com.a.b.n.a.dp)) {
            v1_1 = new com.a.b.n.a.dn(p1, p2);
        } else {
            v1_1 = ((com.a.b.n.a.dp) p1);
        }
        return v1_1;
    }
}
