package com.a.b.n.a;
final class cs extends com.a.b.n.a.g implements java.lang.Runnable {
    private com.a.b.n.a.ap b;
    private com.a.b.n.a.dp c;
    private volatile com.a.b.n.a.dp d;

    private cs(com.a.b.n.a.ap p2, com.a.b.n.a.dp p3)
    {
        this.b = ((com.a.b.n.a.ap) com.a.b.b.cn.a(p2));
        this.c = ((com.a.b.n.a.dp) com.a.b.b.cn.a(p3));
        return;
    }

    synthetic cs(com.a.b.n.a.ap p1, com.a.b.n.a.dp p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    static synthetic com.a.b.n.a.dp a(com.a.b.n.a.cs p1)
    {
        p1.d = 0;
        return 0;
    }

    private static void a(java.util.concurrent.Future p0, boolean p1)
    {
        if (p0 != null) {
            p0.cancel(p1);
        }
        return;
    }

    public final boolean cancel(boolean p2)
    {
        int v0_1;
        if (!super.cancel(p2)) {
            v0_1 = 0;
        } else {
            com.a.b.n.a.cs.a(this.c, p2);
            com.a.b.n.a.cs.a(this.d, p2);
            v0_1 = 1;
        }
        return v0_1;
    }

    public final void run()
    {
        try {
        } catch (int v0_8) {
            this.a(v0_8);
            this.b = 0;
            this.c = 0;
            return;
        } catch (int v0_11) {
            this.b = 0;
            this.c = 0;
            throw v0_11;
        } catch (int v0_9) {
            this.a(v0_9.getCause());
            this.b = 0;
            this.c = 0;
            return;
        } catch (int v0_2) {
            this.a(v0_2.getCause());
            this.b = 0;
            this.c = 0;
            return;
        } catch (int v0) {
            this.cancel(0);
            this.b = 0;
            this.c = 0;
            return;
        }
        int v0_7 = ((com.a.b.n.a.dp) com.a.b.b.cn.a(this.b.a(com.a.b.n.a.gs.a(this.c)), "AsyncFunction may not return null."));
        this.d = v0_7;
        if (!this.isCancelled()) {
            v0_7.a(new com.a.b.n.a.ct(this, v0_7), com.a.b.n.a.ef.a);
            this.b = 0;
            this.c = 0;
            return;
        } else {
            v0_7.cancel(this.a.d());
            this.d = 0;
            this.b = 0;
            this.c = 0;
            return;
        }
    }
}
