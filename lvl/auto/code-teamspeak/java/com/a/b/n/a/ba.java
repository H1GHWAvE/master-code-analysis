package com.a.b.n.a;
final class ba implements java.util.concurrent.Callable {
    final synthetic com.a.b.b.dz a;
    final synthetic java.util.concurrent.Callable b;

    ba(com.a.b.b.dz p1, java.util.concurrent.Callable p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final Object call()
    {
        Thread v1 = Thread.currentThread();
        String v2 = v1.getName();
        boolean v3 = com.a.b.n.a.ay.a(((String) this.a.a()), v1);
        try {
            Throwable v0_4 = this.b.call();
        } catch (Throwable v0_5) {
            if (v3) {
                com.a.b.n.a.ay.a(v2, v1);
            }
            throw v0_5;
        }
        if (v3) {
            com.a.b.n.a.ay.a(v2, v1);
        }
        return v0_4;
    }
}
