package com.a.b.n.a;
public class bd {
    private static final java.util.concurrent.ConcurrentMap b;
    private static final java.util.logging.Logger c;
    private static final ThreadLocal d;
    final com.a.b.n.a.bq a;

    static bd()
    {
        com.a.b.n.a.bd.b = new com.a.b.d.ql().a(com.a.b.d.sh.c).e();
        com.a.b.n.a.bd.c = java.util.logging.Logger.getLogger(com.a.b.n.a.bd.getName());
        com.a.b.n.a.bd.d = new com.a.b.n.a.be();
        return;
    }

    private bd(com.a.b.n.a.bq p2)
    {
        this.a = ((com.a.b.n.a.bq) com.a.b.b.cn.a(p2));
        return;
    }

    synthetic bd(com.a.b.n.a.bq p1, byte p2)
    {
        this(p1);
        return;
    }

    private static com.a.b.n.a.bd a(com.a.b.n.a.bq p1)
    {
        return new com.a.b.n.a.bd(p1);
    }

    private static com.a.b.n.a.bs a(Class p14, com.a.b.n.a.bq p15)
    {
        java.util.Map v1_0 = 0;
        com.a.b.b.cn.a(p14);
        com.a.b.b.cn.a(p15);
        java.util.Map v0_2 = ((java.util.Map) com.a.b.n.a.bd.b.get(p14));
        if (v0_2 == null) {
            java.util.EnumMap v3 = com.a.b.d.sz.a(p14);
            java.util.Map v0_4 = ((Enum[]) p14.getEnumConstants());
            int v4 = v0_4.length;
            java.util.ArrayList v5 = com.a.b.d.ov.a(v4);
            java.util.List v6_0 = v0_4.length;
            com.a.b.n.a.bm v2_0 = 0;
            while (v2_0 < v6_0) {
                java.util.List v7_1 = v0_4[v2_0];
                String v9_3 = String.valueOf(String.valueOf(v7_1.getDeclaringClass().getSimpleName()));
                String v10_2 = String.valueOf(String.valueOf(v7_1.name()));
                com.a.b.n.a.bl v8_1 = new com.a.b.n.a.bl(new StringBuilder(((v9_3.length() + 1) + v10_2.length())).append(v9_3).append(".").append(v10_2).toString());
                v5.add(v8_1);
                v3.put(v7_1, v8_1);
                v2_0++;
            }
            com.a.b.n.a.bm v2_1 = 1;
            while (v2_1 < v4) {
                ((com.a.b.n.a.bl) v5.get(v2_1)).a(com.a.b.n.a.bm.a, v5.subList(0, v2_1));
                v2_1++;
            }
            while (v1_0 < (v4 - 1)) {
                ((com.a.b.n.a.bl) v5.get(v1_0)).a(com.a.b.n.a.bm.c, v5.subList((v1_0 + 1), v4));
                v1_0++;
            }
            java.util.Map v1_1 = java.util.Collections.unmodifiableMap(v3);
            v0_2 = ((java.util.Map) com.a.b.b.ca.a(((java.util.Map) com.a.b.n.a.bd.b.putIfAbsent(p14, v1_1)), v1_1));
        }
        return new com.a.b.n.a.bs(p15, v0_2);
    }

    private static String a(Enum p5)
    {
        String v0_3 = String.valueOf(String.valueOf(p5.getDeclaringClass().getSimpleName()));
        String v1_2 = String.valueOf(String.valueOf(p5.name()));
        return new StringBuilder(((v0_3.length() + 1) + v1_2.length())).append(v0_3).append(".").append(v1_2).toString();
    }

    private static java.util.Map a(Class p14)
    {
        java.util.Map v1_0 = 0;
        java.util.Map v0_2 = ((java.util.Map) com.a.b.n.a.bd.b.get(p14));
        if (v0_2 == null) {
            java.util.EnumMap v3 = com.a.b.d.sz.a(p14);
            java.util.Map v0_4 = ((Enum[]) p14.getEnumConstants());
            int v4 = v0_4.length;
            java.util.ArrayList v5 = com.a.b.d.ov.a(v4);
            java.util.List v6_0 = v0_4.length;
            com.a.b.n.a.bm v2_0 = 0;
            while (v2_0 < v6_0) {
                java.util.List v7_1 = v0_4[v2_0];
                String v9_3 = String.valueOf(String.valueOf(v7_1.getDeclaringClass().getSimpleName()));
                String v10_2 = String.valueOf(String.valueOf(v7_1.name()));
                com.a.b.n.a.bl v8_1 = new com.a.b.n.a.bl(new StringBuilder(((v9_3.length() + 1) + v10_2.length())).append(v9_3).append(".").append(v10_2).toString());
                v5.add(v8_1);
                v3.put(v7_1, v8_1);
                v2_0++;
            }
            com.a.b.n.a.bm v2_1 = 1;
            while (v2_1 < v4) {
                ((com.a.b.n.a.bl) v5.get(v2_1)).a(com.a.b.n.a.bm.a, v5.subList(0, v2_1));
                v2_1++;
            }
            while (v1_0 < (v4 - 1)) {
                ((com.a.b.n.a.bl) v5.get(v1_0)).a(com.a.b.n.a.bm.c, v5.subList((v1_0 + 1), v4));
                v1_0++;
            }
            java.util.Map v1_1 = java.util.Collections.unmodifiableMap(v3);
            v0_2 = ((java.util.Map) com.a.b.b.ca.a(((java.util.Map) com.a.b.n.a.bd.b.putIfAbsent(p14, v1_1)), v1_1));
        }
        return v0_2;
    }

    private java.util.concurrent.locks.ReentrantLock a(String p4)
    {
        com.a.b.n.a.bg v0_2;
        if (this.a != com.a.b.n.a.bm.c) {
            v0_2 = new com.a.b.n.a.bg(this, new com.a.b.n.a.bl(p4), 0);
        } else {
            v0_2 = new java.util.concurrent.locks.ReentrantLock(0);
        }
        return v0_2;
    }

    static synthetic java.util.logging.Logger a()
    {
        return com.a.b.n.a.bd.c;
    }

    static synthetic void a(com.a.b.n.a.bd p3, com.a.b.n.a.bf p4)
    {
        if (!p4.b()) {
            java.util.ArrayList v0_3 = ((java.util.ArrayList) com.a.b.n.a.bd.d.get());
            com.a.b.n.a.bl v1 = p4.a();
            v1.a(p3.a, v0_3);
            v0_3.add(v1);
        }
        return;
    }

    static synthetic void a(com.a.b.n.a.bf p4)
    {
        if (!p4.b()) {
            java.util.ArrayList v0_3 = ((java.util.ArrayList) com.a.b.n.a.bd.d.get());
            com.a.b.n.a.bl v2 = p4.a();
            int v1_1 = (v0_3.size() - 1);
            while (v1_1 >= 0) {
                if (v0_3.get(v1_1) != v2) {
                    v1_1--;
                } else {
                    v0_3.remove(v1_1);
                    break;
                }
            }
        }
        return;
    }

    private static java.util.Map b(Class p14)
    {
        int v1 = 0;
        java.util.EnumMap v3 = com.a.b.d.sz.a(p14);
        java.util.Map v0_1 = ((Enum[]) p14.getEnumConstants());
        int v4 = v0_1.length;
        java.util.ArrayList v5 = com.a.b.d.ov.a(v4);
        java.util.List v6_0 = v0_1.length;
        com.a.b.n.a.bm v2_0 = 0;
        while (v2_0 < v6_0) {
            java.util.List v7_1 = v0_1[v2_0];
            String v9_3 = String.valueOf(String.valueOf(v7_1.getDeclaringClass().getSimpleName()));
            String v10_2 = String.valueOf(String.valueOf(v7_1.name()));
            com.a.b.n.a.bl v8_1 = new com.a.b.n.a.bl(new StringBuilder(((v9_3.length() + 1) + v10_2.length())).append(v9_3).append(".").append(v10_2).toString());
            v5.add(v8_1);
            v3.put(v7_1, v8_1);
            v2_0++;
        }
        com.a.b.n.a.bm v2_1 = 1;
        while (v2_1 < v4) {
            ((com.a.b.n.a.bl) v5.get(v2_1)).a(com.a.b.n.a.bm.a, v5.subList(0, v2_1));
            v2_1++;
        }
        while (v1 < (v4 - 1)) {
            ((com.a.b.n.a.bl) v5.get(v1)).a(com.a.b.n.a.bm.c, v5.subList((v1 + 1), v4));
            v1++;
        }
        return java.util.Collections.unmodifiableMap(v3);
    }

    private java.util.concurrent.locks.ReentrantLock b(String p4)
    {
        com.a.b.n.a.bg v0_2;
        if (this.a != com.a.b.n.a.bm.c) {
            v0_2 = new com.a.b.n.a.bg(this, new com.a.b.n.a.bl(p4), 0);
        } else {
            v0_2 = new java.util.concurrent.locks.ReentrantLock(0);
        }
        return v0_2;
    }

    private void b(com.a.b.n.a.bf p4)
    {
        if (!p4.b()) {
            java.util.ArrayList v0_3 = ((java.util.ArrayList) com.a.b.n.a.bd.d.get());
            com.a.b.n.a.bl v1 = p4.a();
            v1.a(this.a, v0_3);
            v0_3.add(v1);
        }
        return;
    }

    private java.util.concurrent.locks.ReentrantReadWriteLock c(String p4)
    {
        com.a.b.n.a.bi v0_2;
        if (this.a != com.a.b.n.a.bm.c) {
            v0_2 = new com.a.b.n.a.bi(this, new com.a.b.n.a.bl(p4), 0);
        } else {
            v0_2 = new java.util.concurrent.locks.ReentrantReadWriteLock(0);
        }
        return v0_2;
    }

    private static void c(com.a.b.n.a.bf p4)
    {
        if (!p4.b()) {
            java.util.ArrayList v0_3 = ((java.util.ArrayList) com.a.b.n.a.bd.d.get());
            com.a.b.n.a.bl v2 = p4.a();
            int v1_1 = (v0_3.size() - 1);
            while (v1_1 >= 0) {
                if (v0_3.get(v1_1) != v2) {
                    v1_1--;
                } else {
                    v0_3.remove(v1_1);
                    break;
                }
            }
        }
        return;
    }

    private java.util.concurrent.locks.ReentrantReadWriteLock d(String p4)
    {
        com.a.b.n.a.bi v0_2;
        if (this.a != com.a.b.n.a.bm.c) {
            v0_2 = new com.a.b.n.a.bi(this, new com.a.b.n.a.bl(p4), 0);
        } else {
            v0_2 = new java.util.concurrent.locks.ReentrantReadWriteLock(0);
        }
        return v0_2;
    }
}
