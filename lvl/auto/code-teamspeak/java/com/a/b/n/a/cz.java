package com.a.b.n.a;
final class cz implements com.a.b.n.a.cg {
    final synthetic com.a.b.n.a.ch a;
    final synthetic com.a.b.n.a.cy b;

    cz(com.a.b.n.a.cy p1, com.a.b.n.a.ch p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void a(Object p2)
    {
        this.b.a(p2);
        return;
    }

    public final void a(Throwable p4)
    {
        if (!this.b.isCancelled()) {
            try {
                com.a.b.n.a.cy.a(this.b, this.a.a());
            } catch (com.a.b.n.a.dp v0_7) {
                this.b.a(v0_7);
            }
            if (!this.b.isCancelled()) {
                com.a.b.n.a.ci.a(com.a.b.n.a.cy.a(this.b), new com.a.b.n.a.da(this), com.a.b.n.a.ef.a);
            } else {
                com.a.b.n.a.cy.a(this.b).cancel(this.b.a.d());
            }
        }
        return;
    }
}
