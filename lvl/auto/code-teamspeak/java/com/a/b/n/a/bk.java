package com.a.b.n.a;
 class bk extends java.lang.IllegalStateException {
    static final StackTraceElement[] a;
    static java.util.Set b;

    static bk()
    {
        com.a.b.d.lo v0_1 = new StackTraceElement[0];
        com.a.b.n.a.bk.a = v0_1;
        com.a.b.n.a.bk.b = com.a.b.d.lo.a(com.a.b.n.a.bd.getName(), com.a.b.n.a.bk.getName(), com.a.b.n.a.bl.getName());
        return;
    }

    bk(com.a.b.n.a.bl p6, com.a.b.n.a.bl p7)
    {
        int v0_2 = String.valueOf(String.valueOf(p6.c));
        StackTraceElement[] v1_2 = String.valueOf(String.valueOf(p7.c));
        this(new StringBuilder(((v0_2.length() + 4) + v1_2.length())).append(v0_2).append(" -> ").append(v1_2).toString());
        StackTraceElement[] v1_3 = this.getStackTrace();
        int v0_7 = 0;
        int v2_3 = v1_3.length;
        while (v0_7 < v2_3) {
            if (!com.a.b.n.a.bs.getName().equals(v1_3[v0_7].getClassName())) {
                if (com.a.b.n.a.bk.b.contains(v1_3[v0_7].getClassName())) {
                    v0_7++;
                } else {
                    this.setStackTrace(((StackTraceElement[]) java.util.Arrays.copyOfRange(v1_3, v0_7, v2_3)));
                    break;
                }
            } else {
                this.setStackTrace(com.a.b.n.a.bk.a);
                break;
            }
        }
        return;
    }
}
