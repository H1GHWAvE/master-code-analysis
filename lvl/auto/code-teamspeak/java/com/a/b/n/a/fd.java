package com.a.b.n.a;
public final class fd {
    private static final java.util.logging.Logger a;
    private static final com.a.b.n.a.dt b;
    private static final com.a.b.n.a.dt c;
    private final com.a.b.n.a.fk d;
    private final com.a.b.d.jl e;

    static fd()
    {
        com.a.b.n.a.fd.a = java.util.logging.Logger.getLogger(com.a.b.n.a.fd.getName());
        com.a.b.n.a.fd.b = new com.a.b.n.a.fe("healthy()");
        com.a.b.n.a.fd.c = new com.a.b.n.a.ff("stopped()");
        return;
    }

    private fd(Iterable p9)
    {
        com.a.b.n.a.fk v0_0 = com.a.b.d.jl.a(p9);
        if (v0_0.isEmpty()) {
            com.a.b.n.a.fd.a.log(java.util.logging.Level.WARNING, "ServiceManager configured with no services.  Is your application configured properly?", new com.a.b.n.a.fg(0));
            v0_0 = com.a.b.d.jl.a(new com.a.b.n.a.fi(0));
        }
        this.d = new com.a.b.n.a.fk(v0_0);
        this.e = v0_0;
        ref.WeakReference v4_2 = new ref.WeakReference(this.d);
        java.util.Iterator v5_2 = v0_0.iterator();
        while (v5_2.hasNext()) {
            int v1_8;
            com.a.b.n.a.fk v0_7 = ((com.a.b.n.a.et) v5_2.next());
            v0_7.a(new com.a.b.n.a.fj(v0_7, v4_2), com.a.b.n.a.ef.a);
            if (v0_7.f() != com.a.b.n.a.ew.a) {
                v1_8 = 0;
            } else {
                v1_8 = 1;
            }
            Object[] v7 = new Object[1];
            v7[0] = v0_7;
            com.a.b.b.cn.a(v1_8, "Can only manage NEW services, %s", v7);
        }
        this.d.a();
        return;
    }

    static synthetic java.util.logging.Logger a()
    {
        return com.a.b.n.a.fd.a;
    }

    private void a(long p8, java.util.concurrent.TimeUnit p10)
    {
        com.a.b.n.a.dw v1_0 = this.d;
        v1_0.a.a.lock();
        try {
            if (v1_0.a.b(v1_0.h, p8, p10)) {
                v1_0.d();
                v1_0.a.a();
                return;
            } else {
                String v2_3 = String.valueOf(String.valueOf("Timeout waiting for the services to become healthy. The following services have not started: "));
                String v3_3 = String.valueOf(String.valueOf(com.a.b.d.we.a(v1_0.b, com.a.b.b.cp.a(com.a.b.d.lo.b(com.a.b.n.a.ew.a, com.a.b.n.a.ew.b)))));
                throw new java.util.concurrent.TimeoutException(new StringBuilder(((v2_3.length() + 0) + v3_3.length())).append(v2_3).append(v3_3).toString());
            }
        } catch (com.a.b.n.a.dw v0_7) {
            v1_0.a.a();
            throw v0_7;
        }
    }

    private void a(com.a.b.n.a.fh p3)
    {
        this.d.a(p3, com.a.b.n.a.ef.a);
        return;
    }

    private void a(com.a.b.n.a.fh p2, java.util.concurrent.Executor p3)
    {
        this.d.a(p2, p3);
        return;
    }

    static synthetic com.a.b.n.a.dt b()
    {
        return com.a.b.n.a.fd.c;
    }

    private void b(long p8, java.util.concurrent.TimeUnit p10)
    {
        com.a.b.n.a.dw v1_0 = this.d;
        v1_0.a.a.lock();
        try {
            if (v1_0.a.b(v1_0.i, p8, p10)) {
                v1_0.a.a();
                return;
            } else {
                String v2_3 = String.valueOf(String.valueOf("Timeout waiting for the services to stop. The following services have not stopped: "));
                String v3_3 = String.valueOf(String.valueOf(com.a.b.d.we.a(v1_0.b, com.a.b.b.cp.a(com.a.b.b.cp.a(com.a.b.d.lo.b(com.a.b.n.a.ew.e, com.a.b.n.a.ew.f))))));
                throw new java.util.concurrent.TimeoutException(new StringBuilder(((v2_3.length() + 0) + v3_3.length())).append(v2_3).append(v3_3).toString());
            }
        } catch (com.a.b.n.a.dw v0_7) {
            v1_0.a.a();
            throw v0_7;
        }
    }

    static synthetic com.a.b.n.a.dt c()
    {
        return com.a.b.n.a.fd.b;
    }

    private com.a.b.n.a.fd d()
    {
        com.a.b.b.dw v4_0 = this.e.iterator();
        while (v4_0.hasNext()) {
            com.a.b.n.a.dw v1_10;
            String v0_11 = ((com.a.b.n.a.et) v4_0.next());
            StringBuilder v5_3 = v0_11.f();
            if (v5_3 != com.a.b.n.a.ew.a) {
                v1_10 = 0;
            } else {
                v1_10 = 1;
            }
            Object[] v7_1 = new Object[2];
            v7_1[0] = v0_11;
            v7_1[1] = v5_3;
            com.a.b.b.cn.b(v1_10, "Service %s is %s, cannot start it.", v7_1);
        }
        java.util.Iterator v2_1 = this.e.iterator();
        while (v2_1.hasNext()) {
            String v0_5 = ((com.a.b.n.a.et) v2_1.next());
            try {
                com.a.b.n.a.dw v3_1 = this.d;
                v3_1.a.a.lock();
            } catch (com.a.b.n.a.dw v1_8) {
                String v0_7 = String.valueOf(String.valueOf(v0_5));
                com.a.b.n.a.fd.a.log(java.util.logging.Level.WARNING, new StringBuilder((v0_7.length() + 24)).append("Unable to start Service ").append(v0_7).toString(), v1_8);
            }
            try {
                if (((com.a.b.b.dw) v3_1.d.get(v0_5)) == null) {
                    v3_1.d.put(v0_5, com.a.b.b.dw.a());
                }
                v3_1.a.a();
                v0_5.h();
            } catch (com.a.b.n.a.dw v1_6) {
                v3_1.a.a();
                throw v1_6;
            }
        }
        return this;
    }

    private void e()
    {
        com.a.b.n.a.dw v0_0 = this.d;
        v0_0.a.a(v0_0.h);
        try {
            v0_0.d();
            com.a.b.n.a.dw v0_1 = v0_0.a;
            v0_1.a();
            return;
        } catch (Throwable v1_1) {
            v0_1.a.a();
            throw v1_1;
        }
    }

    private com.a.b.n.a.fd f()
    {
        java.util.Iterator v1 = this.e.iterator();
        while (v1.hasNext()) {
            ((com.a.b.n.a.et) v1.next()).i();
        }
        return this;
    }

    private void g()
    {
        com.a.b.n.a.dw v0_0 = this.d;
        v0_0.a.a(v0_0.i);
        v0_0.a.a();
        return;
    }

    private boolean h()
    {
        java.util.Iterator v1 = this.e.iterator();
        while (v1.hasNext()) {
            if (!((com.a.b.n.a.et) v1.next()).e()) {
                int v0_2 = 0;
            }
            return v0_2;
        }
        v0_2 = 1;
        return v0_2;
    }

    private com.a.b.d.kk i()
    {
        return this.d.b();
    }

    private com.a.b.d.jt j()
    {
        return this.d.c();
    }

    public final String toString()
    {
        return new com.a.b.b.cc(com.a.b.b.ca.a(com.a.b.n.a.fd), 0).a("services", com.a.b.d.cm.a(this.e, com.a.b.b.cp.a(com.a.b.b.cp.a(com.a.b.n.a.fi)))).toString();
    }
}
