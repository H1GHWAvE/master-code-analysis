package com.a.b.n.a;
final class q extends com.a.b.n.a.ad {
    final synthetic com.a.b.n.a.p a;
    private volatile java.util.concurrent.Future b;
    private volatile java.util.concurrent.ScheduledExecutorService c;
    private final java.util.concurrent.locks.ReentrantLock d;
    private final Runnable e;

    q(com.a.b.n.a.p p2)
    {
        this.a = p2;
        this.d = new java.util.concurrent.locks.ReentrantLock();
        this.e = new com.a.b.n.a.r(this);
        return;
    }

    static synthetic java.util.concurrent.Future a(com.a.b.n.a.q p0, java.util.concurrent.Future p1)
    {
        p0.b = p1;
        return p1;
    }

    static synthetic java.util.concurrent.locks.ReentrantLock a(com.a.b.n.a.q p1)
    {
        return p1.d;
    }

    static synthetic java.util.concurrent.ScheduledExecutorService b(com.a.b.n.a.q p1)
    {
        return p1.c;
    }

    static synthetic Runnable c(com.a.b.n.a.q p1)
    {
        return p1.e;
    }

    protected final void a()
    {
        com.a.b.n.a.ec v0_1 = this.a.l();
        com.a.b.n.a.s v2_1 = new com.a.b.n.a.s(this);
        com.a.b.b.cn.a(v0_1);
        com.a.b.b.cn.a(v2_1);
        if (!com.a.b.n.a.dy.a()) {
            v0_1 = new com.a.b.n.a.ec(v0_1, v2_1);
        }
        this.c = v0_1;
        this.c.execute(new com.a.b.n.a.t(this));
        return;
    }

    protected final void b()
    {
        this.b.cancel(0);
        this.c.execute(new com.a.b.n.a.u(this));
        return;
    }
}
