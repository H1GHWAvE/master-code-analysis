package com.a.b.n.a;
public abstract class i implements com.a.b.n.a.et {
    final com.a.b.b.dz a;
    private final com.a.b.n.a.et b;

    protected i()
    {
        this.a = new com.a.b.n.a.j(this);
        this.b = new com.a.b.n.a.k(this);
        return;
    }

    private static synthetic com.a.b.b.dz a(com.a.b.n.a.i p1)
    {
        return p1.a;
    }

    private String d()
    {
        return this.getClass().getSimpleName();
    }

    protected abstract void a();

    public final void a(long p2, java.util.concurrent.TimeUnit p4)
    {
        this.b.a(p2, p4);
        return;
    }

    public final void a(com.a.b.n.a.ev p2, java.util.concurrent.Executor p3)
    {
        this.b.a(p2, p3);
        return;
    }

    protected abstract void b();

    public final void b(long p2, java.util.concurrent.TimeUnit p4)
    {
        this.b.b(p2, p4);
        return;
    }

    protected final java.util.concurrent.Executor c()
    {
        return new com.a.b.n.a.n(this);
    }

    public final boolean e()
    {
        return this.b.e();
    }

    public final com.a.b.n.a.ew f()
    {
        return this.b.f();
    }

    public final Throwable g()
    {
        return this.b.g();
    }

    public final com.a.b.n.a.et h()
    {
        this.b.h();
        return this;
    }

    public final com.a.b.n.a.et i()
    {
        this.b.i();
        return this;
    }

    public final void j()
    {
        this.b.j();
        return;
    }

    public final void k()
    {
        this.b.k();
        return;
    }

    public String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.getClass().getSimpleName()));
        String v1_2 = String.valueOf(String.valueOf(this.f()));
        return new StringBuilder(((v0_3.length() + 3) + v1_2.length())).append(v0_3).append(" [").append(v1_2).append("]").toString();
    }
}
