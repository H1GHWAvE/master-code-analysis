package com.a.b.g;
final class ai extends com.a.b.g.ag implements java.io.Serializable {
    private static final long b;
    final int a;

    ai(int p1)
    {
        this.a = p1;
        return;
    }

    public final int a()
    {
        return 32;
    }

    final void a(byte[] p5, int p6, int p7)
    {
        int v0 = 0;
        while (v0 < p7) {
            p5[(p6 + v0)] = ((byte) (this.a >> (v0 * 8)));
            v0++;
        }
        return;
    }

    final boolean a(com.a.b.g.ag p3)
    {
        int v0_1;
        if (this.a != p3.b()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int b()
    {
        return this.a;
    }

    public final long c()
    {
        throw new IllegalStateException("this HashCode only has 32 bits; cannot create a long");
    }

    public final long d()
    {
        return (((long) this.a) & 2.1219957905e-314);
    }

    public final byte[] e()
    {
        byte[] v0_1 = new byte[4];
        v0_1[0] = ((byte) this.a);
        v0_1[1] = ((byte) (this.a >> 8));
        v0_1[2] = ((byte) (this.a >> 16));
        v0_1[3] = ((byte) (this.a >> 24));
        return v0_1;
    }
}
