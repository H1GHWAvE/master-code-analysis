package com.a.b.g;
public final class be extends java.io.FilterOutputStream {
    private final com.a.b.g.al a;

    private be(com.a.b.g.ak p2, java.io.OutputStream p3)
    {
        this(((java.io.OutputStream) com.a.b.b.cn.a(p3)));
        this.a = ((com.a.b.g.al) com.a.b.b.cn.a(p2.a()));
        return;
    }

    private com.a.b.g.ag a()
    {
        return this.a.a();
    }

    public final void close()
    {
        this.out.close();
        return;
    }

    public final void write(int p3)
    {
        this.a.b(((byte) p3));
        this.out.write(p3);
        return;
    }

    public final void write(byte[] p2, int p3, int p4)
    {
        this.a.b(p2, p3, p4);
        this.out.write(p2, p3, p4);
        return;
    }
}
