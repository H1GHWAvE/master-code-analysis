package com.a.b.g;
final enum class p extends com.a.b.g.n {

    p(String p3)
    {
        this(p3, 1, 0);
        return;
    }

    private static long a(byte[] p8)
    {
        return com.a.b.l.u.a(p8[7], p8[6], p8[5], p8[4], p8[3], p8[2], p8[1], p8[0]);
    }

    private static long b(byte[] p8)
    {
        return com.a.b.l.u.a(p8[15], p8[14], p8[13], p8[12], p8[11], p8[10], p8[9], p8[8]);
    }

    public final boolean a(Object p11, com.a.b.g.w p12, int p13, com.a.b.g.q p14)
    {
        int v2 = 0;
        long v4 = p14.a();
        int v3_0 = com.a.b.g.am.b().a(p11, p12).f();
        long v0_2 = com.a.b.g.p.a(v3_0);
        long v6 = com.a.b.g.p.b(v3_0);
        int v3_1 = 0;
        while (v2 < p13) {
            v3_1 |= p14.a(((nan & v0_2) % v4));
            v0_2 += v6;
            v2++;
        }
        return v3_1;
    }

    public final boolean b(Object p11, com.a.b.g.w p12, int p13, com.a.b.g.q p14)
    {
        int v0 = 0;
        long v4 = p14.a();
        int v1_2 = com.a.b.g.am.b().a(p11, p12).f();
        long v2 = com.a.b.g.p.a(v1_2);
        long v6 = com.a.b.g.p.b(v1_2);
        int v1_3 = 0;
        while (v1_3 < p13) {
            if (p14.b(((nan & v2) % v4))) {
                v2 += v6;
                v1_3++;
            }
            return v0;
        }
        v0 = 1;
        return v0;
    }
}
