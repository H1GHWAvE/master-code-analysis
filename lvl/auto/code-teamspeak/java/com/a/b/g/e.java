package com.a.b.g;
abstract class e implements com.a.b.g.ak {

    e()
    {
        return;
    }

    public final com.a.b.g.ag a(long p2)
    {
        return this.a(8).a(p2).a();
    }

    public final com.a.b.g.ag a(CharSequence p5)
    {
        int v1 = p5.length();
        com.a.b.g.al v2 = this.a((v1 * 2));
        com.a.b.g.ag v0_1 = 0;
        while (v0_1 < v1) {
            v2.a(p5.charAt(v0_1));
            v0_1++;
        }
        return v2.a();
    }

    public final com.a.b.g.ag a(CharSequence p3, java.nio.charset.Charset p4)
    {
        com.a.b.g.ag v0_1 = p3.toString().getBytes(p4);
        return this.a(v0_1, v0_1.length);
    }

    public final com.a.b.g.ag a(Object p2, com.a.b.g.w p3)
    {
        return this.a().a(p2, p3).a();
    }

    public final com.a.b.g.ag a(byte[] p2)
    {
        return this.a(p2, p2.length);
    }

    public final com.a.b.g.al a()
    {
        return new com.a.b.g.f(this, 32);
    }

    public final com.a.b.g.al a(int p2)
    {
        com.a.b.g.f v0_0;
        if (p2 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0);
        return new com.a.b.g.f(this, p2);
    }

    public final com.a.b.g.ag b(int p2)
    {
        return this.a(4).a(p2).a();
    }
}
