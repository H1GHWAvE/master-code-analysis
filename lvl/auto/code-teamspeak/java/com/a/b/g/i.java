package com.a.b.g;
public abstract class i extends com.a.b.g.d {
    private final java.nio.ByteBuffer a;
    private final int b;
    private final int c;

    protected i(int p1)
    {
        this(p1, p1);
        return;
    }

    private i(int p3, int p4)
    {
        java.nio.ByteBuffer v0_1;
        if ((p4 % p3) != 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        this.a = java.nio.ByteBuffer.allocate((p4 + 7)).order(java.nio.ByteOrder.LITTLE_ENDIAN);
        this.b = p4;
        this.c = p3;
        return;
    }

    private com.a.b.g.al c(java.nio.ByteBuffer p5)
    {
        if (p5.remaining() > this.a.remaining()) {
            int v1_4 = (this.b - this.a.position());
            java.nio.ByteBuffer v0_2 = 0;
            while (v0_2 < v1_4) {
                this.a.put(p5.get());
                v0_2++;
            }
            this.d();
            while (p5.remaining() >= this.c) {
                this.a(p5);
            }
            this.a.put(p5);
        } else {
            this.a.put(p5);
            this.c();
        }
        return this;
    }

    private void c()
    {
        if (this.a.remaining() < 8) {
            this.d();
        }
        return;
    }

    private void d()
    {
        this.a.flip();
        while (this.a.remaining() >= this.c) {
            this.a(this.a);
        }
        this.a.compact();
        return;
    }

    public final com.a.b.g.ag a()
    {
        this.d();
        this.a.flip();
        if (this.a.remaining() > 0) {
            this.b(this.a);
        }
        return this.b();
    }

    public final com.a.b.g.al a(char p2)
    {
        this.a.putChar(p2);
        this.c();
        return this;
    }

    public final com.a.b.g.al a(int p2)
    {
        this.a.putInt(p2);
        this.c();
        return this;
    }

    public final com.a.b.g.al a(long p2)
    {
        this.a.putLong(p2);
        this.c();
        return this;
    }

    public final com.a.b.g.al a(CharSequence p3)
    {
        int v0 = 0;
        while (v0 < p3.length()) {
            this.a(p3.charAt(v0));
            v0++;
        }
        return this;
    }

    public final com.a.b.g.al a(Object p1, com.a.b.g.w p2)
    {
        p2.a(p1, this);
        return this;
    }

    public final com.a.b.g.al a(short p2)
    {
        this.a.putShort(p2);
        this.c();
        return this;
    }

    protected abstract void a();

    abstract com.a.b.g.ag b();

    public final com.a.b.g.al b(byte p2)
    {
        this.a.put(p2);
        this.c();
        return this;
    }

    public final com.a.b.g.al b(byte[] p3)
    {
        return this.b(p3, 0, p3.length);
    }

    public final com.a.b.g.al b(byte[] p6, int p7, int p8)
    {
        java.nio.ByteBuffer v1_1 = java.nio.ByteBuffer.wrap(p6, p7, p8).order(java.nio.ByteOrder.LITTLE_ENDIAN);
        if (v1_1.remaining() > this.a.remaining()) {
            int v2_4 = (this.b - this.a.position());
            java.nio.ByteBuffer v0_3 = 0;
            while (v0_3 < v2_4) {
                this.a.put(v1_1.get());
                v0_3++;
            }
            this.d();
            while (v1_1.remaining() >= this.c) {
                this.a(v1_1);
            }
            this.a.put(v1_1);
        } else {
            this.a.put(v1_1);
            this.c();
        }
        return this;
    }

    public final synthetic com.a.b.g.bn b(char p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(int p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(long p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(CharSequence p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(short p2)
    {
        return this.a(p2);
    }

    protected void b(java.nio.ByteBuffer p3)
    {
        p3.position(p3.limit());
        p3.limit((this.c + 7));
        while (p3.position() < this.c) {
            p3.putLong(0);
        }
        p3.limit(this.c);
        p3.flip();
        this.a(p3);
        return;
    }

    public final synthetic com.a.b.g.bn c(byte p2)
    {
        return this.b(p2);
    }

    public final synthetic com.a.b.g.bn c(byte[] p2)
    {
        return this.b(p2);
    }

    public final synthetic com.a.b.g.bn c(byte[] p2, int p3, int p4)
    {
        return this.b(p2, p3, p4);
    }
}
