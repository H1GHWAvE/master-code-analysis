package com.a.b.m;
abstract enum class at extends java.lang.Enum implements com.a.b.b.co {
    public static final enum com.a.b.m.at a;
    public static final enum com.a.b.m.at b;
    private static final synthetic com.a.b.m.at[] c;

    static at()
    {
        com.a.b.m.at.a = new com.a.b.m.au("IGNORE_TYPE_VARIABLE_OR_WILDCARD");
        com.a.b.m.at.b = new com.a.b.m.av("INTERFACE_ONLY");
        com.a.b.m.at[] v0_5 = new com.a.b.m.at[2];
        v0_5[0] = com.a.b.m.at.a;
        v0_5[1] = com.a.b.m.at.b;
        com.a.b.m.at.c = v0_5;
        return;
    }

    private at(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic at(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.m.at valueOf(String p1)
    {
        return ((com.a.b.m.at) Enum.valueOf(com.a.b.m.at, p1));
    }

    public static com.a.b.m.at[] values()
    {
        return ((com.a.b.m.at[]) com.a.b.m.at.c.clone());
    }
}
