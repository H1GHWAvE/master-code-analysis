package com.a.b.m;
final class ah extends com.a.b.m.ax {
    final synthetic com.a.b.m.ae a;

    ah(com.a.b.m.ae p1)
    {
        this.a = p1;
        return;
    }

    final void a(reflect.GenericArrayType p4)
    {
        reflect.Type[] v0_1 = new reflect.Type[1];
        v0_1[0] = p4.getGenericComponentType();
        this.a(v0_1);
        return;
    }

    final void a(reflect.ParameterizedType p4)
    {
        this.a(p4.getActualTypeArguments());
        reflect.Type[] v0_2 = new reflect.Type[1];
        v0_2[0] = p4.getOwnerType();
        this.a(v0_2);
        return;
    }

    final void a(reflect.TypeVariable p5)
    {
        String v1_3 = String.valueOf(String.valueOf(com.a.b.m.ae.b(this.a)));
        throw new IllegalArgumentException(new StringBuilder((v1_3.length() + 58)).append(v1_3).append("contains a type variable and is not safe for the operation").toString());
    }

    final void a(reflect.WildcardType p2)
    {
        this.a(p2.getLowerBounds());
        this.a(p2.getUpperBounds());
        return;
    }
}
