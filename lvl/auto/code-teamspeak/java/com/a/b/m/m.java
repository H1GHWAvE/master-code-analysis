package com.a.b.m;
 class m extends com.a.b.m.k {
    final reflect.Method a;

    m(reflect.Method p1)
    {
        this(p1);
        this.a = p1;
        return;
    }

    final Object a(Object p2, Object[] p3)
    {
        return this.a.invoke(p2, p3);
    }

    public final boolean b()
    {
        if ((reflect.Modifier.isFinal(this.getModifiers())) || ((reflect.Modifier.isPrivate(this.getModifiers())) || ((reflect.Modifier.isStatic(this.getModifiers())) || (reflect.Modifier.isFinal(this.getDeclaringClass().getModifiers()))))) {
            int v0_9 = 0;
        } else {
            v0_9 = 1;
        }
        return v0_9;
    }

    public final boolean c()
    {
        return this.a.isVarArgs();
    }

    reflect.Type[] d()
    {
        return this.a.getGenericParameterTypes();
    }

    reflect.Type[] e()
    {
        return this.a.getGenericExceptionTypes();
    }

    final otation.Annotation[][] f()
    {
        return this.a.getParameterAnnotations();
    }

    reflect.Type g()
    {
        return this.a.getGenericReturnType();
    }

    public final reflect.TypeVariable[] getTypeParameters()
    {
        return this.a.getTypeParameters();
    }
}
