package com.a.b.m;
abstract class an {
    static final com.a.b.m.an a;
    static final com.a.b.m.an b;

    static an()
    {
        com.a.b.m.an.a = new com.a.b.m.ao();
        com.a.b.m.an.b = new com.a.b.m.ap();
        return;
    }

    private an()
    {
        return;
    }

    synthetic an(byte p1)
    {
        return;
    }

    private int a(Object p4, java.util.Map p5)
    {
        int v0_5;
        int v0_1 = ((Integer) p5.get(this));
        if (v0_1 == 0) {
            int v0_4;
            if (!this.b(p4).isInterface()) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            Integer v1_1 = this.c(p4).iterator();
            while (v1_1.hasNext()) {
                v0_4 = Math.max(v0_4, this.a(v1_1.next(), p5));
            }
            Integer v1_2 = this.d(p4);
            if (v1_2 != null) {
                v0_4 = Math.max(v0_4, this.a(v1_2, p5));
            }
            p5.put(p4, Integer.valueOf((v0_4 + 1)));
            v0_5 = (v0_4 + 1);
        } else {
            v0_5 = v0_1.intValue();
        }
        return v0_5;
    }

    private static com.a.b.d.jl a(java.util.Map p2, java.util.Comparator p3)
    {
        return new com.a.b.m.ar(p3, p2).b(p2.keySet());
    }

    com.a.b.d.jl a(Iterable p4)
    {
        com.a.b.d.jl v0_0 = com.a.b.d.sz.c();
        com.a.b.d.yd v1_0 = p4.iterator();
        while (v1_0.hasNext()) {
            this.a(v1_0.next(), v0_0);
        }
        return new com.a.b.m.ar(com.a.b.d.yd.d().a(), v0_0).b(v0_0.keySet());
    }

    final com.a.b.d.jl a(Object p2)
    {
        return this.a(com.a.b.d.jl.a(p2));
    }

    final com.a.b.m.an a()
    {
        return new com.a.b.m.aq(this, this);
    }

    abstract Class b();

    abstract Iterable c();

    abstract Object d();
}
