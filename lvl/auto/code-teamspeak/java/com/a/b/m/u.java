package com.a.b.m;
abstract class u {

    u()
    {
        return;
    }

    final reflect.Type a()
    {
        reflect.Type v0_1 = this.getClass().getGenericSuperclass();
        boolean v1 = (v0_1 instanceof reflect.ParameterizedType);
        Object[] v3_1 = new Object[1];
        v3_1[0] = v0_1;
        com.a.b.b.cn.a(v1, "%s isn\'t parameterized", v3_1);
        return ((reflect.ParameterizedType) v0_1).getActualTypeArguments()[0];
    }
}
