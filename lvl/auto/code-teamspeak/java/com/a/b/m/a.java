package com.a.b.m;
public abstract class a implements java.lang.reflect.InvocationHandler {
    private static final Object[] a;

    static a()
    {
        Object[] v0_1 = new Object[0];
        com.a.b.m.a.a = v0_1;
        return;
    }

    public a()
    {
        return;
    }

    private static boolean a(Object p2, Class p3)
    {
        if ((!p3.isInstance(p2)) && ((!reflect.Proxy.isProxyClass(p2.getClass())) || (!java.util.Arrays.equals(p2.getClass().getInterfaces(), p3.getInterfaces())))) {
            int v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    protected abstract Object a();

    public boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public int hashCode()
    {
        return super.hashCode();
    }

    public final Object invoke(Object p6, reflect.Method p7, Object[] p8)
    {
        Boolean v0_0 = 1;
        if (p8 == null) {
            p8 = com.a.b.m.a.a;
        }
        if ((p8.length != 0) || (!p7.getName().equals("hashCode"))) {
            if ((p8.length != 1) || ((!p7.getName().equals("equals")) || (p7.getParameterTypes()[0] != Object))) {
                if ((p8.length != 0) || (!p7.getName().equals("toString"))) {
                    Boolean v0_4 = this.a();
                } else {
                    v0_4 = this.toString();
                }
            } else {
                Object v3_3 = p8[0];
                if (v3_3 != null) {
                    if (p6 != v3_3) {
                        boolean v2_11;
                        boolean v2_8 = p6.getClass();
                        if ((!v2_8.isInstance(v3_3)) && ((!reflect.Proxy.isProxyClass(v3_3.getClass())) || (!java.util.Arrays.equals(v3_3.getClass().getInterfaces(), v2_8.getInterfaces())))) {
                            v2_11 = 0;
                        } else {
                            v2_11 = 1;
                        }
                        if ((!v2_11) || (!this.equals(reflect.Proxy.getInvocationHandler(v3_3)))) {
                            v0_0 = 0;
                        }
                        v0_4 = Boolean.valueOf(v0_0);
                    } else {
                        v0_4 = Boolean.valueOf(1);
                    }
                } else {
                    v0_4 = Boolean.valueOf(0);
                }
            }
        } else {
            v0_4 = Integer.valueOf(this.hashCode());
        }
        return v0_4;
    }

    public String toString()
    {
        return super.toString();
    }
}
