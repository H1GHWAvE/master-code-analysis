package com.a.b.m;
final class f {
    final com.a.b.d.mf a;
    private final java.util.Set b;

    f()
    {
        this.a = new com.a.b.d.mf(com.a.b.d.yd.e());
        this.b = new java.util.HashSet();
        return;
    }

    private static com.a.b.d.lo a(java.io.File p7, java.util.jar.Manifest p8)
    {
        String v0_4;
        if (p8 != null) {
            com.a.b.d.lp v2 = com.a.b.d.lo.i();
            String v0_1 = p8.getMainAttributes().getValue(java.util.jar.Attributes$Name.CLASS_PATH.toString());
            if (v0_1 != null) {
                java.util.Iterator v3 = com.a.b.m.b.b().a(v0_1).iterator();
                while (v3.hasNext()) {
                    String v0_6 = ((String) v3.next());
                    try {
                        java.io.File v1_4 = new java.net.URI(v0_6);
                    } catch (java.io.File v1) {
                        java.io.File v1_7 = com.a.b.m.b.a();
                        String v0_7 = String.valueOf(v0_6);
                        if (v0_7.length() == 0) {
                            String v0_9 = new String("Invalid Class-Path entry: ");
                        } else {
                            v0_9 = "Invalid Class-Path entry: ".concat(v0_7);
                        }
                        v1_7.warning(v0_9);
                    }
                    if (!v1_4.isAbsolute()) {
                        v0_6 = new java.io.File(p7.getParentFile(), v0_6.replace(47, java.io.File.separatorChar)).toURI();
                    } else {
                        v0_6 = v1_4;
                    }
                    v2.c(v0_6);
                }
            }
            v0_4 = v2.b();
        } else {
            v0_4 = com.a.b.d.lo.h();
        }
        return v0_4;
    }

    private com.a.b.d.me a()
    {
        return this.a.c();
    }

    private static java.net.URI a(java.io.File p4, String p5)
    {
        java.net.URI v0_1 = new java.net.URI(p5);
        if (!v0_1.isAbsolute()) {
            v0_1 = new java.io.File(p4.getParentFile(), p5.replace(47, java.io.File.separatorChar)).toURI();
        }
        return v0_1;
    }

    private void a(java.io.File p6, ClassLoader p7)
    {
        if (p6.exists()) {
            if (!p6.isDirectory()) {
                try {
                    java.io.IOException v1_1 = new java.util.jar.JarFile(p6);
                } catch (com.a.b.m.e v0) {
                }
                try {
                    java.util.Enumeration v2_0 = com.a.b.m.f.a(p6, v1_1.getManifest()).iterator();
                } catch (com.a.b.m.e v0_8) {
                    try {
                        v1_1.close();
                    } catch (java.io.IOException v1) {
                    }
                    throw v0_8;
                }
                while (v2_0.hasNext()) {
                    this.a(((java.net.URI) v2_0.next()), p7);
                }
                java.util.Enumeration v2_1 = v1_1.entries();
                while (v2_1.hasMoreElements()) {
                    com.a.b.m.e v0_7 = ((java.util.jar.JarEntry) v2_1.nextElement());
                    if ((!v0_7.isDirectory()) && (!v0_7.getName().equals("META-INF/MANIFEST.MF"))) {
                        this.a.d(com.a.b.m.e.a(v0_7.getName(), p7));
                    }
                }
                try {
                    v1_1.close();
                } catch (com.a.b.m.e v0) {
                }
            } else {
                this.a(p6, p7, "", com.a.b.d.lo.h());
            }
        }
        return;
    }

    private void a(java.io.File p11, ClassLoader p12, String p13, com.a.b.d.lo p14)
    {
        com.a.b.m.e v0_0 = p11.getCanonicalFile();
        if (!p14.contains(v0_0)) {
            StringBuilder v2_0 = p11.listFiles();
            if (v2_0 != null) {
                com.a.b.d.lo v3_0 = com.a.b.d.lo.i().b(p14).c(v0_0).b();
                int v4 = v2_0.length;
                int v1_3 = 0;
                while (v1_3 < v4) {
                    com.a.b.m.e v0_3 = v2_0[v1_3];
                    com.a.b.d.mf v5_0 = v0_3.getName();
                    if (!v0_3.isDirectory()) {
                        com.a.b.m.e v0_6;
                        String v6_1 = String.valueOf(p13);
                        com.a.b.m.e v0_4 = String.valueOf(v5_0);
                        if (v0_4.length() == 0) {
                            v0_6 = new String(v6_1);
                        } else {
                            v0_6 = v6_1.concat(v0_4);
                        }
                        if (!v0_6.equals("META-INF/MANIFEST.MF")) {
                            this.a.d(com.a.b.m.e.a(v0_6, p12));
                        }
                    } else {
                        String v6_3 = String.valueOf(String.valueOf(p13));
                        com.a.b.d.mf v5_6 = String.valueOf(String.valueOf(v5_0));
                        this.a(v0_3, p12, new StringBuilder(((v6_3.length() + 1) + v5_6.length())).append(v6_3).append(v5_6).append("/").toString(), v3_0);
                    }
                    v1_3++;
                }
            } else {
                com.a.b.m.e v0_9 = com.a.b.m.b.a();
                int v1_5 = String.valueOf(String.valueOf(p11));
                v0_9.warning(new StringBuilder((v1_5.length() + 22)).append("Cannot read directory ").append(v1_5).toString());
            }
        }
        return;
    }

    private void b(java.io.File p3, ClassLoader p4)
    {
        this.a(p3, p4, "", com.a.b.d.lo.h());
        return;
    }

    private void c(java.io.File p6, ClassLoader p7)
    {
        try {
            java.io.IOException v1_1 = new java.util.jar.JarFile(p6);
            try {
                java.util.Enumeration v2_0 = com.a.b.m.f.a(p6, v1_1.getManifest()).iterator();
            } catch (com.a.b.m.e v0_6) {
                try {
                    v1_1.close();
                } catch (java.io.IOException v1) {
                }
                throw v0_6;
            }
            while (v2_0.hasNext()) {
                this.a(((java.net.URI) v2_0.next()), p7);
            }
            java.util.Enumeration v2_1 = v1_1.entries();
            while (v2_1.hasMoreElements()) {
                com.a.b.m.e v0_5 = ((java.util.jar.JarEntry) v2_1.nextElement());
                if ((!v0_5.isDirectory()) && (!v0_5.getName().equals("META-INF/MANIFEST.MF"))) {
                    this.a.d(com.a.b.m.e.a(v0_5.getName(), p7));
                }
            }
            try {
                v1_1.close();
            } catch (com.a.b.m.e v0) {
            }
            return;
        } catch (com.a.b.m.e v0) {
            return;
        }
    }

    final void a(java.net.URI p4, ClassLoader p5)
    {
        if ((p4.getScheme().equals("file")) && (this.b.add(p4))) {
            java.io.File v0_5 = new java.io.File(p4);
            if (v0_5.exists()) {
                if (!v0_5.isDirectory()) {
                    this.c(v0_5, p5);
                } else {
                    this.a(v0_5, p5, "", com.a.b.d.lo.h());
                }
            }
        }
        return;
    }
}
