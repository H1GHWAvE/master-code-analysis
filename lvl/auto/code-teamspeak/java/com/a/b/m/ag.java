package com.a.b.m;
final class ag extends com.a.b.m.l {
    final synthetic com.a.b.m.ae b;

    ag(com.a.b.m.ae p1, reflect.Constructor p2)
    {
        this.b = p1;
        this(p2);
        return;
    }

    public final com.a.b.m.ae a()
    {
        return this.b;
    }

    final reflect.Type[] d()
    {
        return com.a.b.m.ae.a(this.b, super.d());
    }

    final reflect.Type[] e()
    {
        return com.a.b.m.ae.a(this.b, super.e());
    }

    final reflect.Type g()
    {
        return this.b.b(super.g()).a;
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.b));
        String v1_4 = String.valueOf(String.valueOf(com.a.b.b.bv.a(", ").a(this.d())));
        return new StringBuilder(((v0_2.length() + 2) + v1_4.length())).append(v0_2).append("(").append(v1_4).append(")").toString();
    }
}
