package com.a.b.m;
final class bo implements java.lang.reflect.TypeVariable {
    private final reflect.GenericDeclaration a;
    private final String b;
    private final com.a.b.d.jl c;

    bo(reflect.GenericDeclaration p2, String p3, reflect.Type[] p4)
    {
        com.a.b.m.ay.a(p4, "bound for type variable");
        this.a = ((reflect.GenericDeclaration) com.a.b.b.cn.a(p2));
        this.b = ((String) com.a.b.b.cn.a(p3));
        this.c = com.a.b.d.jl.a(p4);
        return;
    }

    public final boolean equals(Object p5)
    {
        int v0 = 1;
        if (!com.a.b.m.bm.a) {
            if (!(p5 instanceof reflect.TypeVariable)) {
                v0 = 0;
            } else {
                if ((!this.b.equals(((reflect.TypeVariable) p5).getName())) || (!this.a.equals(((reflect.TypeVariable) p5).getGenericDeclaration()))) {
                    v0 = 0;
                }
            }
        } else {
            if (!(p5 instanceof com.a.b.m.bo)) {
                v0 = 0;
            } else {
                if ((!this.b.equals(((com.a.b.m.bo) p5).getName())) || ((!this.a.equals(((com.a.b.m.bo) p5).getGenericDeclaration())) || (!this.c.equals(((com.a.b.m.bo) p5).c)))) {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public final reflect.Type[] getBounds()
    {
        return com.a.b.m.ay.a(this.c);
    }

    public final reflect.GenericDeclaration getGenericDeclaration()
    {
        return this.a;
    }

    public final String getName()
    {
        return this.b;
    }

    public final int hashCode()
    {
        return (this.a.hashCode() ^ this.b.hashCode());
    }

    public final String toString()
    {
        return this.b;
    }
}
