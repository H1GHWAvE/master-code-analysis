package com.a.b.j;
public final class g {
    static final int a = 3037000499;
    static final byte[] b = None;
    static final int[] c = None;
    static final int[] d = None;
    static final int e = 46340;
    static int[] f;
    private static final int[] g;

    static g()
    {
        int[] v0_1 = new byte[33];
        v0_1 = {9, 9, 9, 8, 8, 8, 7, 7, 7, 6, 6, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 1, 1, 1, 0, 0, 0, 0};
        com.a.b.j.g.b = v0_1;
        int[] v0_2 = new int[10];
        v0_2 = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000};
        com.a.b.j.g.c = v0_2;
        int[] v0_3 = new int[10];
        v0_3 = {3, 31, 316, 3162, 31622, 316227, 3162277, 31622776, 316227766, 2147483647};
        com.a.b.j.g.d = v0_3;
        int[] v0_5 = new int[13];
        v0_5 = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600};
        com.a.b.j.g.g = v0_5;
        int[] v0_7 = new int[17];
        v0_7 = {2147483647, 2147483647, 65536, 2345, 477, 193, 110, 75, 58, 49, 43, 39, 37, 35, 34, 34, 33};
        com.a.b.j.g.f = v0_7;
        return;
    }

    private g()
    {
        return;
    }

    public static int a(int p1)
    {
        int v0_3;
        com.a.b.j.k.b("n", p1);
        if (p1 >= com.a.b.j.g.g.length) {
            v0_3 = 2147483647;
        } else {
            v0_3 = com.a.b.j.g.g[p1];
        }
        return v0_3;
    }

    public static int a(int p4, int p5)
    {
        int v0_4;
        long v2_1 = (((long) p5) + ((long) p4));
        if (v2_1 != ((long) ((int) v2_1))) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        com.a.b.j.k.c(v0_4);
        return ((int) v2_1);
    }

    public static int a(int p7, int p8, java.math.RoundingMode p9)
    {
        String v0_0 = 1;
        String v1_0 = 0;
        com.a.b.b.cn.a(p9);
        if (p8 != 0) {
            String v0_1;
            int v2 = (p7 / p8);
            int v3_1 = (p7 - (p8 * v2));
            if (v3_1 != 0) {
                int v5 = (((p7 ^ p8) >> 31) | 1);
                switch (com.a.b.j.h.a[p9.ordinal()]) {
                    case 1:
                        if (v3_1 != 0) {
                            v0_0 = 0;
                        }
                        com.a.b.j.k.a(v0_0);
                    case 2:
                        v0_0 = 0;
                        break;
                    case 3:
                        if (v5 >= 0) {
                            v0_0 = 0;
                        } else {
                        }
                    case 4:
                        break;
                    case 5:
                        if (v5 > 0) {
                        } else {
                            v0_0 = 0;
                        }
                        break;
                    case 6:
                    case 7:
                    case 8:
                        int v3_2 = Math.abs(v3_1);
                        int v3_3 = (v3_2 - (Math.abs(p8) - v3_2));
                        if (v3_3 != 0) {
                            if (v3_3 > 0) {
                            } else {
                                v0_0 = 0;
                            }
                        } else {
                            if (p9 == java.math.RoundingMode.HALF_UP) {
                                v1_0 = 1;
                            } else {
                                String v4_6;
                                if (p9 != java.math.RoundingMode.HALF_EVEN) {
                                    v4_6 = 0;
                                } else {
                                    v4_6 = 1;
                                }
                                int v3_7;
                                if ((v2 & 1) == 0) {
                                    v3_7 = 0;
                                } else {
                                    v3_7 = 1;
                                }
                                if ((v3_7 & v4_6) != 0) {
                                }
                            }
                            v0_0 = v1_0;
                        }
                        break;
                    default:
                        throw new AssertionError();
                }
                if (v0_0 == null) {
                    v0_1 = v2;
                } else {
                    v0_1 = (v2 + v5);
                }
            } else {
                v0_1 = v2;
            }
            return v0_1;
        } else {
            throw new ArithmeticException("/ by zero");
        }
    }

    public static int a(int p4, java.math.RoundingMode p5)
    {
        int v0_3;
        int v0_0 = 1;
        com.a.b.j.k.a("x", p4);
        switch (com.a.b.j.h.a[p5.ordinal()]) {
            case 1:
                int v2_3;
                if (p4 <= 0) {
                    v2_3 = 0;
                } else {
                    v2_3 = 1;
                }
                if (((p4 - 1) & p4) != 0) {
                    v0_0 = 0;
                }
                com.a.b.j.k.a((v0_0 & v2_3));
            case 2:
            case 3:
                v0_3 = (31 - Integer.numberOfLeadingZeros(p4));
                break;
            case 4:
            case 5:
                v0_3 = (32 - Integer.numberOfLeadingZeros((p4 - 1)));
                break;
            case 6:
            case 7:
            case 8:
                int v0_1 = Integer.numberOfLeadingZeros(p4);
                v0_3 = ((31 - v0_1) + com.a.b.j.g.c((-1257966797 >> v0_1), p4));
                break;
            default:
                throw new AssertionError();
        }
        return v0_3;
    }

    public static int b(int p4, int p5)
    {
        int v0_4;
        long v2_1 = (((long) p5) * ((long) p4));
        if (v2_1 != ((long) ((int) v2_1))) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        com.a.b.j.k.c(v0_4);
        return ((int) v2_1);
    }

    public static int b(int p3, java.math.RoundingMode p4)
    {
        int v0_8;
        com.a.b.j.k.b("x", p3);
        int v1 = ((int) Math.sqrt(((double) p3)));
        switch (com.a.b.j.h.a[p4.ordinal()]) {
            case 1:
                int v0_12;
                if ((v1 * v1) != p3) {
                    v0_12 = 0;
                } else {
                    v0_12 = 1;
                }
                com.a.b.j.k.a(v0_12);
            case 2:
            case 3:
                v0_8 = v1;
                break;
            case 4:
            case 5:
                v0_8 = (com.a.b.j.g.c((v1 * v1), p3) + v1);
                break;
            case 6:
            case 7:
            case 8:
                v0_8 = (com.a.b.j.g.c(((v1 * v1) + v1), p3) + v1);
                break;
            default:
                throw new AssertionError();
        }
        return v0_8;
    }

    private static boolean b(int p4)
    {
        int v2;
        int v0_0 = 1;
        if (p4 <= 0) {
            v2 = 0;
        } else {
            v2 = 1;
        }
        if (((p4 - 1) & p4) != 0) {
            v0_0 = 0;
        }
        return (v0_0 & v2);
    }

    private static int c(int p2)
    {
        int v0_1 = com.a.b.j.g.b[Integer.numberOfLeadingZeros(p2)];
        return (v0_1 - com.a.b.j.g.c(p2, com.a.b.j.g.c[v0_1]));
    }

    private static int c(int p1, int p2)
    {
        return ((((p1 - p2) ^ -1) ^ -1) >> 31);
    }

    private static int c(int p4, java.math.RoundingMode p5)
    {
        int v0_8;
        com.a.b.j.k.a("x", p4);
        int v0_2 = com.a.b.j.g.b[Integer.numberOfLeadingZeros(p4)];
        int v1_4 = (v0_2 - com.a.b.j.g.c(p4, com.a.b.j.g.c[v0_2]));
        int v0_4 = com.a.b.j.g.c[v1_4];
        switch (com.a.b.j.h.a[p5.ordinal()]) {
            case 1:
                int v0_10;
                if (p4 != v0_4) {
                    v0_10 = 0;
                } else {
                    v0_10 = 1;
                }
                com.a.b.j.k.a(v0_10);
            case 2:
            case 3:
                v0_8 = v1_4;
                break;
            case 4:
            case 5:
                v0_8 = (com.a.b.j.g.c(v0_4, p4) + v1_4);
                break;
            case 6:
            case 7:
            case 8:
                v0_8 = (com.a.b.j.g.c(com.a.b.j.g.d[v1_4], p4) + v1_4);
                break;
            default:
                throw new AssertionError();
        }
        return v0_8;
    }

    private static int d(int p2)
    {
        return ((int) Math.sqrt(((double) p2)));
    }

    private static int d(int p4, int p5)
    {
        int v1 = 1;
        com.a.b.j.k.b("exponent", p5);
        switch (p4) {
            case -2:
                if (p5 >= 32) {
                    v1 = 0;
                } else {
                    if ((p5 & 1) != 0) {
                        v1 = (- (1 << p5));
                    } else {
                        v1 = (1 << p5);
                    }
                }
                break;
            case -1:
                if ((p5 & 1) == 0) {
                } else {
                    v1 = -1;
                }
                break;
            case 0:
                if (p5 != 0) {
                    v1 = 0;
                } else {
                }
            case 1:
                break;
            case 2:
                if (p5 >= 32) {
                    v1 = 0;
                } else {
                    v1 = (1 << p5);
                }
                break;
            default:
                int v3_1 = 1;
                int v2_1 = p4;
                while(true) {
                    switch (p5) {
                        case 0:
                            v1 = v3_1;
                            break;
                        case 1:
                            v1 = (v2_1 * v3_1);
                            break;
                        default:
                            int v0_5;
                            if ((p5 & 1) != 0) {
                                v0_5 = v2_1;
                            } else {
                                v0_5 = 1;
                            }
                            v3_1 *= v0_5;
                            v2_1 *= v2_1;
                            p5 >>= 1;
                    }
                }
        }
        return v1;
    }

    private static int e(int p3, int p4)
    {
        if (p4 > 0) {
            int v0_0 = (p3 % p4);
            if (v0_0 < 0) {
                v0_0 += p4;
            }
            return v0_0;
        } else {
            throw new ArithmeticException(new StringBuilder(31).append("Modulus ").append(p4).append(" must be > 0").toString());
        }
    }

    private static int f(int p5, int p6)
    {
        com.a.b.j.k.b("a", p5);
        com.a.b.j.k.b("b", p6);
        if (p5 != 0) {
            if (p6 != 0) {
                int v2 = Integer.numberOfTrailingZeros(p5);
                int v1_0 = (p5 >> v2);
                int v3 = Integer.numberOfTrailingZeros(p6);
                int v0_2 = (p6 >> v3);
                while (v1_0 != v0_2) {
                    int v1_1 = (v1_0 - v0_2);
                    int v4_1 = ((v1_1 >> 31) & v1_1);
                    int v1_3 = ((v1_1 - v4_1) - v4_1);
                    v0_2 += v4_1;
                    v1_0 = (v1_3 >> Integer.numberOfTrailingZeros(v1_3));
                }
                p6 = (v1_0 << Math.min(v2, v3));
            } else {
                p6 = p5;
            }
        }
        return p6;
    }

    private static int g(int p4, int p5)
    {
        int v0_4;
        long v2_1 = (((long) p4) - ((long) p5));
        if (v2_1 != ((long) ((int) v2_1))) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        com.a.b.j.k.c(v0_4);
        return ((int) v2_1);
    }

    private static int h(int p5, int p6)
    {
        int v0_0 = 0;
        int v1 = 1;
        com.a.b.j.k.b("exponent", p6);
        switch (p5) {
            case -2:
                if (p6 < 32) {
                    v0_0 = 1;
                }
                com.a.b.j.k.c(v0_0);
                if ((p6 & 1) != 0) {
                    v1 = (-1 << p6);
                } else {
                    v1 = (1 << p6);
                }
                break;
            case -1:
                if ((p6 & 1) == 0) {
                } else {
                    v1 = -1;
                }
                break;
            case 0:
                if (p6 != 0) {
                    v1 = 0;
                } else {
                }
            case 1:
                break;
            case 2:
                if (p6 < 31) {
                    v0_0 = 1;
                }
                com.a.b.j.k.c(v0_0);
                v1 = (1 << p6);
                break;
            default:
                int v2_2 = 1;
                while(true) {
                    switch (p6) {
                        case 0:
                            v1 = v2_2;
                            break;
                        case 1:
                            v1 = com.a.b.j.g.b(v2_2, p5);
                            break;
                        default:
                            int v4;
                            if ((p6 & 1) == 0) {
                                v4 = v2_2;
                            } else {
                                v4 = com.a.b.j.g.b(v2_2, p5);
                            }
                            p6 >>= 1;
                            if (p6 <= 0) {
                                v2_2 = v4;
                            } else {
                                int v3_3;
                                if (-46340 > p5) {
                                    v3_3 = 0;
                                } else {
                                    v3_3 = 1;
                                }
                                int v2_6;
                                if (p5 > 46340) {
                                    v2_6 = 0;
                                } else {
                                    v2_6 = 1;
                                }
                                com.a.b.j.k.c((v2_6 & v3_3));
                                p5 *= p5;
                                v2_2 = v4;
                            }
                    }
                }
        }
        return v1;
    }

    private static int i(int p6, int p7)
    {
        long v0_2;
        int v1 = 1;
        int v2 = 0;
        com.a.b.j.k.b("n", p6);
        com.a.b.j.k.b("k", p7);
        if (p7 > p6) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        long v4_1 = new Object[2];
        v4_1[0] = Integer.valueOf(p7);
        v4_1[1] = Integer.valueOf(p6);
        com.a.b.b.cn.a(v0_2, "k (%s) > n (%s)", v4_1);
        if (p7 > (p6 >> 1)) {
            p7 = (p6 - p7);
        }
        if ((p7 < com.a.b.j.g.f.length) && (p6 <= com.a.b.j.g.f[p7])) {
            switch (p7) {
                case 0:
                    break;
                case 1:
                    v1 = p6;
                    break;
                default:
                    long v0_8 = 1;
                    while (v2 < p7) {
                        v0_8 = ((v0_8 * ((long) (p6 - v2))) / ((long) (v2 + 1)));
                        v2++;
                    }
                    v1 = ((int) v0_8);
            }
        } else {
            v1 = 2147483647;
        }
        return v1;
    }

    private static int j(int p2, int p3)
    {
        return ((p2 & p3) + ((p2 ^ p3) >> 1));
    }
}
