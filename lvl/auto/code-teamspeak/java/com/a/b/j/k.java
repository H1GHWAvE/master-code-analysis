package com.a.b.j;
final class k {

    private k()
    {
        return;
    }

    private static double a(String p5, double p6)
    {
        if (p6 >= 0) {
            return p6;
        } else {
            String v1_1 = String.valueOf(String.valueOf(p5));
            throw new IllegalArgumentException(new StringBuilder((v1_1.length() + 40)).append(v1_1).append(" (").append(p6).append(") must be >= 0").toString());
        }
    }

    static int a(String p4, int p5)
    {
        if (p5 > 0) {
            return p5;
        } else {
            String v1_1 = String.valueOf(String.valueOf(p4));
            throw new IllegalArgumentException(new StringBuilder((v1_1.length() + 26)).append(v1_1).append(" (").append(p5).append(") must be > 0").toString());
        }
    }

    static long a(String p5, long p6)
    {
        if (p6 > 0) {
            return p6;
        } else {
            String v1_1 = String.valueOf(String.valueOf(p5));
            throw new IllegalArgumentException(new StringBuilder((v1_1.length() + 35)).append(v1_1).append(" (").append(p6).append(") must be > 0").toString());
        }
    }

    static java.math.BigInteger a(String p6, java.math.BigInteger p7)
    {
        if (p7.signum() > 0) {
            return p7;
        } else {
            String v1_1 = String.valueOf(String.valueOf(p6));
            String v2_1 = String.valueOf(String.valueOf(p7));
            throw new IllegalArgumentException(new StringBuilder(((v1_1.length() + 15) + v2_1.length())).append(v1_1).append(" (").append(v2_1).append(") must be > 0").toString());
        }
    }

    static void a(boolean p2)
    {
        if (p2) {
            return;
        } else {
            throw new ArithmeticException("mode was UNNECESSARY, but rounding was necessary");
        }
    }

    static int b(String p4, int p5)
    {
        if (p5 >= 0) {
            return p5;
        } else {
            String v1_1 = String.valueOf(String.valueOf(p4));
            throw new IllegalArgumentException(new StringBuilder((v1_1.length() + 27)).append(v1_1).append(" (").append(p5).append(") must be >= 0").toString());
        }
    }

    static long b(String p5, long p6)
    {
        if (p6 >= 0) {
            return p6;
        } else {
            String v1_1 = String.valueOf(String.valueOf(p5));
            throw new IllegalArgumentException(new StringBuilder((v1_1.length() + 36)).append(v1_1).append(" (").append(p6).append(") must be >= 0").toString());
        }
    }

    private static java.math.BigInteger b(String p6, java.math.BigInteger p7)
    {
        if (p7.signum() >= 0) {
            return p7;
        } else {
            String v1_1 = String.valueOf(String.valueOf(p6));
            String v2_1 = String.valueOf(String.valueOf(p7));
            throw new IllegalArgumentException(new StringBuilder(((v1_1.length() + 16) + v2_1.length())).append(v1_1).append(" (").append(v2_1).append(") must be >= 0").toString());
        }
    }

    static void b(boolean p2)
    {
        if (p2) {
            return;
        } else {
            throw new ArithmeticException("not in range");
        }
    }

    static void c(boolean p2)
    {
        if (p2) {
            return;
        } else {
            throw new ArithmeticException("overflow");
        }
    }
}
