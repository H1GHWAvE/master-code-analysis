package com.a.b.e;
final class f extends com.a.b.e.d {
    private final char[][] a;
    private final int b;

    f(char[][] p2)
    {
        this.a = p2;
        this.b = p2.length;
        return;
    }

    public final String a(String p5)
    {
        int v1 = p5.length();
        int v0 = 0;
        while (v0 < v1) {
            char[] v2_0 = p5.charAt(v0);
            if ((v2_0 >= this.a.length) || (this.a[v2_0] == null)) {
                v0++;
            } else {
                p5 = this.a(p5, v0);
                break;
            }
        }
        return p5;
    }

    protected final char[] a(char p2)
    {
        int v0_1;
        if (p2 >= this.b) {
            v0_1 = 0;
        } else {
            v0_1 = this.a[p2];
        }
        return v0_1;
    }
}
