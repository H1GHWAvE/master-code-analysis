package com.a.b.e;
public abstract class c extends com.a.b.e.p {
    private final char[][] a;
    private final int b;
    private final int c;
    private final int d;
    private final char e;
    private final char f;

    private c(com.a.b.e.b p2, int p3, int p4)
    {
        com.a.b.b.cn.a(p2);
        this.a = p2.a;
        this.b = this.a.length;
        if (p4 < p3) {
            p4 = -1;
            p3 = 2147483647;
        }
        this.c = p3;
        this.d = p4;
        if (p3 < 55296) {
            this.e = ((char) p3);
            this.f = ((char) Math.min(p4, 55295));
        } else {
            this.e = 65535;
            this.f = 0;
        }
        return;
    }

    private c(java.util.Map p2, int p3, int p4)
    {
        this(com.a.b.e.b.a(p2), p3, p4);
        return;
    }

    protected final int a(CharSequence p3, int p4, int p5)
    {
        while (p4 < p5) {
            char v0 = p3.charAt(p4);
            if (((v0 < this.b) && (this.a[v0] != null)) || ((v0 > this.f) || (v0 < this.e))) {
                break;
            }
            p4++;
        }
        return p4;
    }

    public final String a(String p4)
    {
        com.a.b.b.cn.a(p4);
        int v0 = 0;
        while (v0 < p4.length()) {
            char v1_1 = p4.charAt(v0);
            if (((v1_1 >= this.b) || (this.a[v1_1] == null)) && ((v1_1 <= this.f) && (v1_1 >= this.e))) {
                v0++;
            } else {
                p4 = this.a(p4, v0);
                break;
            }
        }
        return p4;
    }

    protected abstract char[] a();

    protected final char[] a(int p2)
    {
        int v0_2;
        if (p2 >= this.b) {
            if ((p2 < this.c) || (p2 > this.d)) {
                v0_2 = this.a();
            } else {
                v0_2 = 0;
            }
        } else {
            v0_2 = this.a[p2];
            if (v0_2 == 0) {
            }
        }
        return v0_2;
    }
}
