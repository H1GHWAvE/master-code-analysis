package com.a.b.e;
final class k extends com.a.b.e.p {
    final synthetic com.a.b.e.d a;

    k(com.a.b.e.d p1)
    {
        this.a = p1;
        return;
    }

    protected final char[] a(int p10)
    {
        char[] v0_11;
        int v2 = 0;
        if (p10 >= 65536) {
            char[] v5 = new char[2];
            Character.toChars(p10, v5, 0);
            char[] v6 = this.a.a(v5[0]);
            char[] v7 = this.a.a(v5[1]);
            if ((v6 != null) || (v7 != null)) {
                char v4;
                if (v6 == null) {
                    v4 = 1;
                } else {
                    v4 = v6.length;
                }
                char[] v0_5;
                if (v7 == null) {
                    v0_5 = 1;
                } else {
                    v0_5 = v7.length;
                }
                char[] v3_2 = new char[(v0_5 + v4)];
                if (v6 == null) {
                    v3_2[0] = v5[0];
                } else {
                    char[] v0_8 = 0;
                    while (v0_8 < v6.length) {
                        v3_2[v0_8] = v6[v0_8];
                        v0_8++;
                    }
                }
                if (v7 == null) {
                    v3_2[v4] = v5[1];
                } else {
                    while (v2 < v7.length) {
                        v3_2[(v4 + v2)] = v7[v2];
                        v2++;
                    }
                }
                v0_11 = v3_2;
            } else {
                v0_11 = 0;
            }
        } else {
            v0_11 = this.a.a(((char) p10));
        }
        return v0_11;
    }
}
