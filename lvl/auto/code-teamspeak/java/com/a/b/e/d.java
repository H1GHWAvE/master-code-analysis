package com.a.b.e;
public abstract class d extends com.a.b.e.g {
    private static final int a = 2;

    protected d()
    {
        return;
    }

    private static char[] a(char[] p2, int p3, int p4)
    {
        char[] v0 = new char[p4];
        if (p3 > 0) {
            System.arraycopy(p2, 0, v0, 0, p3);
        }
        return v0;
    }

    public String a(String p4)
    {
        com.a.b.b.cn.a(p4);
        int v1 = p4.length();
        int v0 = 0;
        while (v0 < v1) {
            if (this.a(p4.charAt(v0)) == null) {
                v0++;
            } else {
                p4 = this.a(p4, v0);
                break;
            }
        }
        return p4;
    }

    protected final String a(String p12, int p13)
    {
        int v6 = p12.length();
        char[] v3 = com.a.b.e.n.a();
        int v2_0 = v3.length;
        int v0_0 = 0;
        int v1_0 = 0;
        while (p13 < v6) {
            int v4_3 = this.a(p12.charAt(p13));
            if (v4_3 != 0) {
                int v7 = v4_3.length;
                int v8 = (p13 - v0_0);
                int v9_1 = ((v1_0 + v8) + v7);
                if (v2_0 < v9_1) {
                    v2_0 = (((v6 - p13) * 2) + v9_1);
                    v3 = com.a.b.e.d.a(v3, v1_0, v2_0);
                }
                int v0_3;
                if (v8 <= 0) {
                    v0_3 = v1_0;
                } else {
                    p12.getChars(v0_0, p13, v3, v1_0);
                    v0_3 = (v1_0 + v8);
                }
                if (v7 > 0) {
                    System.arraycopy(v4_3, 0, v3, v0_3, v7);
                    v0_3 += v7;
                }
                v1_0 = v0_3;
                v0_0 = (p13 + 1);
            }
            p13++;
        }
        int v4_0 = (v6 - v0_0);
        if (v4_0 > 0) {
            int v4_1 = (v4_0 + v1_0);
            if (v2_0 < v4_1) {
                v3 = com.a.b.e.d.a(v3, v1_0, v4_1);
            }
            p12.getChars(v0_0, v6, v3, v1_0);
            v1_0 = v4_1;
        }
        return new String(v3, 0, v1_0);
    }

    protected abstract char[] a();
}
