package com.a.b.d;
 class adq extends com.a.b.d.add implements java.util.Set {
    private static final long a;

    adq(java.util.Set p2, Object p3)
    {
        this(p2, p3, 0);
        return;
    }

    synthetic java.util.Collection b()
    {
        return this.c();
    }

    java.util.Set c()
    {
        return ((java.util.Set) super.b());
    }

    synthetic Object d()
    {
        return this.c();
    }

    public boolean equals(Object p3)
    {
        Throwable v0_1;
        if (p3 != this) {
            try {
                v0_1 = this.c().equals(p3);
            } catch (Throwable v0_2) {
                throw v0_2;
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public int hashCode()
    {
        try {
            return this.c().hashCode();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }
}
