package com.a.b.d;
final class wr extends com.a.b.d.an implements com.a.b.d.aac, java.io.Serializable {
    private static final long b = 7845222491160860175;
    final java.util.Map a;

    wr(java.util.Map p2)
    {
        this.a = ((java.util.Map) com.a.b.b.cn.a(p2));
        return;
    }

    public final java.util.Set a(Object p2)
    {
        return new com.a.b.d.ws(this, p2);
    }

    public final java.util.Set a(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean a(com.a.b.d.vi p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean a(Object p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        return this.a(p2, p3);
    }

    public final java.util.Set b(Object p3)
    {
        java.util.HashSet v0_1 = new java.util.HashSet(2);
        if (this.a.containsKey(p3)) {
            v0_1.add(this.a.remove(p3));
        }
        return v0_1;
    }

    public final boolean b(Object p3, Object p4)
    {
        return this.a.entrySet().contains(com.a.b.d.sz.a(p3, p4));
    }

    public final synthetic java.util.Collection c(Object p2)
    {
        return this.a(p2);
    }

    public final boolean c(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean c(Object p3, Object p4)
    {
        return this.a.entrySet().remove(com.a.b.d.sz.a(p3, p4));
    }

    public final synthetic java.util.Collection d(Object p2)
    {
        return this.b(p2);
    }

    public final int f()
    {
        return this.a.size();
    }

    public final boolean f(Object p2)
    {
        return this.a.containsKey(p2);
    }

    public final void g()
    {
        this.a.clear();
        return;
    }

    public final boolean g(Object p2)
    {
        return this.a.containsValue(p2);
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final java.util.Collection i()
    {
        return this.a.values();
    }

    public final synthetic java.util.Collection k()
    {
        return this.u();
    }

    final java.util.Iterator l()
    {
        return this.a.entrySet().iterator();
    }

    final java.util.Map m()
    {
        return new com.a.b.d.wf(this);
    }

    public final java.util.Set p()
    {
        return this.a.keySet();
    }

    public final java.util.Set u()
    {
        return this.a.entrySet();
    }
}
