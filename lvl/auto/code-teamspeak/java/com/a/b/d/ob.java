package com.a.b.d;
final class ob extends com.a.b.d.yd implements java.io.Serializable {
    private static final long b;
    final com.a.b.d.yd a;

    ob(com.a.b.d.yd p1)
    {
        this.a = p1;
        return;
    }

    private int a(Iterable p6, Iterable p7)
    {
        java.util.Iterator v1 = p6.iterator();
        java.util.Iterator v2 = p7.iterator();
        while (v1.hasNext()) {
            if (v2.hasNext()) {
                int v0_2 = this.a.compare(v1.next(), v2.next());
                if (v0_2 != 0) {
                }
            } else {
                v0_2 = 1;
            }
            return v0_2;
        }
        if (!v2.hasNext()) {
            v0_2 = 0;
        } else {
            v0_2 = -1;
        }
        return v0_2;
    }

    public final synthetic int compare(Object p6, Object p7)
    {
        java.util.Iterator v1 = ((Iterable) p6).iterator();
        java.util.Iterator v2 = ((Iterable) p7).iterator();
        while (v1.hasNext()) {
            if (v2.hasNext()) {
                int v0_2 = this.a.compare(v1.next(), v2.next());
                if (v0_2 != 0) {
                }
            } else {
                v0_2 = 1;
            }
            return v0_2;
        }
        if (!v2.hasNext()) {
            v0_2 = 0;
        } else {
            v0_2 = -1;
        }
        return v0_2;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (p3 != this) {
            if (!(p3 instanceof com.a.b.d.ob)) {
                v0_1 = 0;
            } else {
                v0_1 = this.a.equals(((com.a.b.d.ob) p3).a);
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return (this.a.hashCode() ^ 2075626741);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 18)).append(v0_2).append(".lexicographical()").toString();
    }
}
