package com.a.b.d;
final class lh extends com.a.b.d.me {
    final synthetic com.a.b.d.lf a;
    private final com.a.b.d.ep f;
    private transient Integer g;

    lh(com.a.b.d.lf p2, com.a.b.d.ep p3)
    {
        this.a = p2;
        this(com.a.b.d.yd.d());
        this.f = p3;
        return;
    }

    static synthetic com.a.b.d.ep a(com.a.b.d.lh p1)
    {
        return p1.f;
    }

    private com.a.b.d.me a(com.a.b.d.yl p3)
    {
        return this.a.d(p3).a(this.f);
    }

    private com.a.b.d.me a(Comparable p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.a(p2, com.a.b.d.ce.a(p3)));
    }

    private com.a.b.d.me a(Comparable p3, boolean p4, Comparable p5, boolean p6)
    {
        if ((p4) || ((p6) || (com.a.b.d.yl.b(p3, p5) != 0))) {
            com.a.b.d.me v0_3 = this.a(com.a.b.d.yl.a(p3, com.a.b.d.ce.a(p4), p5, com.a.b.d.ce.a(p6)));
        } else {
            v0_3 = com.a.b.d.me.c;
        }
        return v0_3;
    }

    private com.a.b.d.me b(Comparable p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.b(p2, com.a.b.d.ce.a(p3)));
    }

    final synthetic com.a.b.d.me a(Object p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.b(((Comparable) p2), com.a.b.d.ce.a(p3)));
    }

    final synthetic com.a.b.d.me a(Object p3, boolean p4, Object p5, boolean p6)
    {
        if ((p4) || ((p6) || (com.a.b.d.yl.b(((Comparable) p3), ((Comparable) p5)) != 0))) {
            com.a.b.d.me v0_3 = this.a(com.a.b.d.yl.a(((Comparable) p3), com.a.b.d.ce.a(p4), ((Comparable) p5), com.a.b.d.ce.a(p6)));
        } else {
            v0_3 = com.a.b.d.me.c;
        }
        return v0_3;
    }

    final synthetic com.a.b.d.me b(Object p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.a(((Comparable) p2), com.a.b.d.ce.a(p3)));
    }

    final int c(Object p6)
    {
        long v0_1;
        if (!this.contains(p6)) {
            v0_1 = -1;
        } else {
            java.util.Iterator v4 = com.a.b.d.lf.a(this.a).iterator();
            long v2_2 = 0;
            while (v4.hasNext()) {
                long v0_7 = ((com.a.b.d.yl) v4.next());
                if (!v0_7.c(((Comparable) p6))) {
                    v2_2 = (((long) com.a.b.d.du.a(v0_7, this.f).size()) + v2_2);
                } else {
                    v0_1 = com.a.b.l.q.b((((long) com.a.b.d.du.a(v0_7, this.f).c(((Comparable) p6))) + v2_2));
                }
            }
            throw new AssertionError("impossible");
        }
        return v0_1;
    }

    public final com.a.b.d.agi c()
    {
        return new com.a.b.d.li(this);
    }

    public final boolean contains(Object p3)
    {
        boolean v0 = 0;
        if (p3 != null) {
            try {
                v0 = this.a.a(((Comparable) p3));
            } catch (ClassCastException v1) {
            }
        }
        return v0;
    }

    public final com.a.b.d.agi d()
    {
        return new com.a.b.d.lj(this);
    }

    public final synthetic java.util.Iterator descendingIterator()
    {
        return this.d();
    }

    final Object g()
    {
        return new com.a.b.d.lk(com.a.b.d.lf.a(this.a), this.f);
    }

    final boolean h_()
    {
        return com.a.b.d.lf.a(this.a).h_();
    }

    public final synthetic java.util.Iterator iterator()
    {
        return this.c();
    }

    public final int size()
    {
        Integer v0_0 = this.g;
        if (v0_0 == null) {
            java.util.Iterator v4 = com.a.b.d.lf.a(this.a).iterator();
            Integer v2_2 = 0;
            while (v4.hasNext()) {
                Integer v0_3 = (((long) com.a.b.d.du.a(((com.a.b.d.yl) v4.next()), this.f).size()) + v2_2);
                if (v0_3 < 2147483647) {
                    v2_2 = v0_3;
                }
                v0_0 = Integer.valueOf(com.a.b.l.q.b(v0_3));
                this.g = v0_0;
            }
            v0_3 = v2_2;
        }
        return v0_0.intValue();
    }

    public final String toString()
    {
        return com.a.b.d.lf.a(this.a).toString();
    }
}
