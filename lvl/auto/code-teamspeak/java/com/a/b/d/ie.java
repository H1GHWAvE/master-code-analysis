package com.a.b.d;
final class ie extends java.util.AbstractMap implements com.a.b.d.bw, java.io.Serializable {
    final synthetic com.a.b.d.hy a;

    private ie(com.a.b.d.hy p1)
    {
        this.a = p1;
        return;
    }

    synthetic ie(com.a.b.d.hy p1, byte p2)
    {
        this(p1);
        return;
    }

    private com.a.b.d.bw a()
    {
        return this.a;
    }

    private Object d()
    {
        return new com.a.b.d.ik(this.a);
    }

    public final Object a(Object p3, Object p4)
    {
        return com.a.b.d.hy.a(this.a, p3, p4, 1);
    }

    public final com.a.b.d.bw b()
    {
        return this.a;
    }

    public final void clear()
    {
        this.a.clear();
        return;
    }

    public final boolean containsKey(Object p2)
    {
        return this.a.containsValue(p2);
    }

    public final java.util.Set entrySet()
    {
        return new com.a.b.d.if(this);
    }

    public final Object get(Object p3)
    {
        Object v0_2;
        Object v0_1 = this.a.a(p3, com.a.b.d.hy.a(p3));
        if (v0_1 != null) {
            v0_2 = v0_1.e;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final java.util.Set j_()
    {
        return this.a.keySet();
    }

    public final java.util.Set keySet()
    {
        return new com.a.b.d.ii(this);
    }

    public final Object put(Object p3, Object p4)
    {
        return com.a.b.d.hy.a(this.a, p3, p4, 0);
    }

    public final Object remove(Object p3)
    {
        Object v0_2;
        Object v0_1 = this.a.a(p3, com.a.b.d.hy.a(p3));
        if (v0_1 != null) {
            this.a.a(v0_1);
            v0_2 = v0_1.e;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final int size()
    {
        return this.a.a;
    }

    public final synthetic java.util.Collection values()
    {
        return this.j_();
    }
}
