package com.a.b.d;
final class abz implements java.util.Iterator {
    final java.util.Iterator a;
    java.util.Map$Entry b;
    java.util.Iterator c;
    final synthetic com.a.b.d.abx d;

    private abz(com.a.b.d.abx p2)
    {
        this.d = p2;
        this.a = this.d.a.entrySet().iterator();
        this.c = com.a.b.d.nj.b();
        return;
    }

    synthetic abz(com.a.b.d.abx p1, byte p2)
    {
        this(p1);
        return;
    }

    private com.a.b.d.adw a()
    {
        if (!this.c.hasNext()) {
            this.b = ((java.util.Map$Entry) this.a.next());
            this.c = ((java.util.Map) this.b.getValue()).entrySet().iterator();
        }
        com.a.b.d.adw v0_12 = ((java.util.Map$Entry) this.c.next());
        return com.a.b.d.adx.a(this.b.getKey(), v0_12.getKey(), v0_12.getValue());
    }

    public final boolean hasNext()
    {
        if ((!this.a.hasNext()) && (!this.c.hasNext())) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final synthetic Object next()
    {
        if (!this.c.hasNext()) {
            this.b = ((java.util.Map$Entry) this.a.next());
            this.c = ((java.util.Map) this.b.getValue()).entrySet().iterator();
        }
        com.a.b.d.adw v0_12 = ((java.util.Map$Entry) this.c.next());
        return com.a.b.d.adx.a(this.b.getKey(), v0_12.getKey(), v0_12.getValue());
    }

    public final void remove()
    {
        this.c.remove();
        if (((java.util.Map) this.b.getValue()).isEmpty()) {
            this.a.remove();
        }
        return;
    }
}
