package com.a.b.d;
final class aae extends com.a.b.d.aaq {
    final synthetic java.util.Set a;
    final synthetic java.util.Set b;
    final synthetic java.util.Set c;

    aae(java.util.Set p2, java.util.Set p3, java.util.Set p4)
    {
        this.a = p2;
        this.b = p3;
        this.c = p4;
        this(0);
        return;
    }

    public final com.a.b.d.lo a()
    {
        return new com.a.b.d.lp().b(this.a).b(this.c).b();
    }

    public final java.util.Set a(java.util.Set p2)
    {
        p2.addAll(this.a);
        p2.addAll(this.c);
        return p2;
    }

    public final boolean contains(Object p2)
    {
        if ((!this.a.contains(p2)) && (!this.c.contains(p2))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final boolean isEmpty()
    {
        if ((!this.a.isEmpty()) || (!this.c.isEmpty())) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final java.util.Iterator iterator()
    {
        return com.a.b.d.nj.a(com.a.b.d.nj.b(this.a.iterator(), this.b.iterator()));
    }

    public final int size()
    {
        return (this.a.size() + this.b.size());
    }
}
