package com.a.b.d;
public final class nj {
    static final com.a.b.d.agj a;
    private static final java.util.Iterator b;

    static nj()
    {
        com.a.b.d.nj.a = new com.a.b.d.nk();
        com.a.b.d.nj.b = new com.a.b.d.nq();
        return;
    }

    private nj()
    {
        return;
    }

    public static com.a.b.d.agi a()
    {
        return com.a.b.d.nj.a;
    }

    private static com.a.b.d.agi a(com.a.b.d.agi p1)
    {
        return ((com.a.b.d.agi) com.a.b.b.cn.a(p1));
    }

    public static com.a.b.d.agi a(Iterable p1, java.util.Comparator p2)
    {
        com.a.b.b.cn.a(p1, "iterators");
        com.a.b.b.cn.a(p2, "comparator");
        return new com.a.b.d.ny(p1, p2);
    }

    public static com.a.b.d.agi a(Object p1)
    {
        return new com.a.b.d.nn(p1);
    }

    private static com.a.b.d.agi a(java.util.Enumeration p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.d.no(p1);
    }

    public static com.a.b.d.agi a(java.util.Iterator p1)
    {
        com.a.b.d.nr v1_1;
        com.a.b.b.cn.a(p1);
        if (!(p1 instanceof com.a.b.d.agi)) {
            v1_1 = new com.a.b.d.nr(p1);
        } else {
            v1_1 = ((com.a.b.d.agi) p1);
        }
        return v1_1;
    }

    public static com.a.b.d.agi a(java.util.Iterator p1, int p2)
    {
        return com.a.b.d.nj.a(p1, p2, 0);
    }

    private static com.a.b.d.agi a(java.util.Iterator p1, int p2, boolean p3)
    {
        com.a.b.d.nu v0_0;
        com.a.b.b.cn.a(p1);
        if (p2 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0);
        return new com.a.b.d.nu(p1, p2, p3);
    }

    public static com.a.b.d.agi a(java.util.Iterator p1, Class p2)
    {
        return com.a.b.d.nj.b(p1, com.a.b.b.cp.a(p2));
    }

    public static varargs com.a.b.d.agi a(Object[] p2)
    {
        return com.a.b.d.nj.a(p2, 0, p2.length, 0);
    }

    static com.a.b.d.agj a(Object[] p2, int p3, int p4, int p5)
    {
        com.a.b.d.nm v0_0;
        if (p4 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.d.nm v0_3;
        com.a.b.b.cn.a(v0_0);
        com.a.b.b.cn.a(p3, (p3 + p4), p2.length);
        com.a.b.b.cn.b(p5, p4);
        if (p4 != 0) {
            v0_3 = new com.a.b.d.nm(p4, p5, p2, p3);
        } else {
            v0_3 = com.a.b.d.nj.a;
        }
        return v0_3;
    }

    private static com.a.b.d.yi a(com.a.b.d.yi p1)
    {
        return ((com.a.b.d.yi) com.a.b.b.cn.a(p1));
    }

    private static Object a(java.util.Iterator p1, int p2, Object p3)
    {
        com.a.b.d.nj.a(p2);
        com.a.b.d.nj.d(p1, p2);
        return com.a.b.d.nj.d(p1, p3);
    }

    public static Object a(java.util.Iterator p1, com.a.b.b.co p2, Object p3)
    {
        return com.a.b.d.nj.d(com.a.b.d.nj.b(p1, p2), p3);
    }

    public static java.util.Iterator a(Iterable p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.d.ns(p1);
    }

    public static java.util.Iterator a(java.util.Iterator p1, com.a.b.b.bj p2)
    {
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.nw(p1, p2);
    }

    private static java.util.Iterator a(java.util.Iterator p1, java.util.Iterator p2, java.util.Iterator p3)
    {
        return com.a.b.d.nj.e(com.a.b.d.jl.a(p1, p2, p3).c());
    }

    private static java.util.Iterator a(java.util.Iterator p1, java.util.Iterator p2, java.util.Iterator p3, java.util.Iterator p4)
    {
        return com.a.b.d.nj.e(com.a.b.d.jl.a(p1, p2, p3, p4).c());
    }

    private static varargs java.util.Iterator a(java.util.Iterator[] p1)
    {
        return com.a.b.d.nj.e(com.a.b.d.jl.a(p1).c());
    }

    static void a(int p3)
    {
        if (p3 >= 0) {
            return;
        } else {
            throw new IndexOutOfBoundsException(new StringBuilder(43).append("position (").append(p3).append(") must not be negative").toString());
        }
    }

    public static boolean a(java.util.Collection p2, java.util.Iterator p3)
    {
        com.a.b.b.cn.a(p2);
        com.a.b.b.cn.a(p3);
        int v0 = 0;
        while (p3.hasNext()) {
            v0 |= p2.add(p3.next());
        }
        return v0;
    }

    public static boolean a(java.util.Iterator p2, com.a.b.b.co p3)
    {
        com.a.b.b.cn.a(p3);
        int v0 = 0;
        while (p2.hasNext()) {
            if (p3.a(p2.next())) {
                p2.remove();
                v0 = 1;
            }
        }
        return v0;
    }

    public static boolean a(java.util.Iterator p1, Object p2)
    {
        return com.a.b.d.nj.c(p1, com.a.b.b.cp.a(p2));
    }

    public static boolean a(java.util.Iterator p1, java.util.Collection p2)
    {
        return com.a.b.d.nj.a(p1, com.a.b.b.cp.a(p2));
    }

    public static boolean a(java.util.Iterator p3, java.util.Iterator p4)
    {
        int v0 = 0;
        while (p3.hasNext()) {
            if ((!p4.hasNext()) || (!com.a.b.b.ce.a(p3.next(), p4.next()))) {
            }
            return v0;
        }
        if (!p4.hasNext()) {
            v0 = 1;
        }
        return v0;
    }

    public static int b(java.util.Iterator p2)
    {
        int v0 = 0;
        while (p2.hasNext()) {
            p2.next();
            v0++;
        }
        return v0;
    }

    public static com.a.b.d.agi b(java.util.Iterator p1, int p2)
    {
        return com.a.b.d.nj.a(p1, p2, 1);
    }

    public static com.a.b.d.agi b(java.util.Iterator p1, com.a.b.b.co p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.nv(p1, p2);
    }

    public static Object b(java.util.Iterator p1, Object p2)
    {
        if (p1.hasNext()) {
            p2 = com.a.b.d.nj.d(p1);
        }
        return p2;
    }

    static java.util.Iterator b()
    {
        return com.a.b.d.nj.b;
    }

    public static java.util.Iterator b(java.util.Iterator p1, java.util.Iterator p2)
    {
        return com.a.b.d.nj.e(com.a.b.d.jl.a(p1, p2).c());
    }

    private static varargs java.util.Iterator b(Object[] p1)
    {
        return com.a.b.d.nj.a(com.a.b.d.ov.a(p1));
    }

    public static boolean b(java.util.Iterator p1, java.util.Collection p2)
    {
        return com.a.b.d.nj.a(p1, com.a.b.b.cp.a(com.a.b.b.cp.a(p2)));
    }

    private static Object[] b(java.util.Iterator p1, Class p2)
    {
        return com.a.b.d.mq.a(com.a.b.d.ov.a(p1), p2);
    }

    public static int c(java.util.Iterator p1, Object p2)
    {
        return com.a.b.d.nj.b(com.a.b.d.nj.b(p1, com.a.b.b.cp.a(p2)));
    }

    private static com.a.b.d.agj c()
    {
        return com.a.b.d.nj.a;
    }

    public static Object c(java.util.Iterator p4, int p5)
    {
        com.a.b.d.nj.a(p5);
        Object v0_0 = com.a.b.d.nj.d(p4, p5);
        if (p4.hasNext()) {
            return p4.next();
        } else {
            throw new IndexOutOfBoundsException(new StringBuilder(91).append("position (").append(p5).append(") must be less than the number of elements that remained (").append(v0_0).append(")").toString());
        }
    }

    public static String c(java.util.Iterator p3)
    {
        return com.a.b.d.cm.a.a(new StringBuilder("["), p3).append(93).toString();
    }

    public static boolean c(java.util.Iterator p2, com.a.b.b.co p3)
    {
        int v0_1;
        if (com.a.b.d.nj.g(p2, p3) == -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public static int d(java.util.Iterator p3, int p4)
    {
        boolean v0_0;
        int v1 = 0;
        com.a.b.b.cn.a(p3);
        if (p4 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0, "numberToAdvance must be nonnegative");
        while ((v1 < p4) && (p3.hasNext())) {
            p3.next();
            v1++;
        }
        return v1;
    }

    public static Object d(java.util.Iterator p5)
    {
        IllegalArgumentException v0_0 = p5.next();
        if (p5.hasNext()) {
            String v1_2 = new StringBuilder();
            IllegalArgumentException v0_2 = String.valueOf(String.valueOf(v0_0));
            v1_2.append(new StringBuilder((v0_2.length() + 31)).append("expected one element but was: <").append(v0_2).toString());
            IllegalArgumentException v0_5 = 0;
            while ((v0_5 < 4) && (p5.hasNext())) {
                String v2_7 = String.valueOf(String.valueOf(p5.next()));
                v1_2.append(new StringBuilder((v2_7.length() + 2)).append(", ").append(v2_7).toString());
                v0_5++;
            }
            if (p5.hasNext()) {
                v1_2.append(", ...");
            }
            v1_2.append(62);
            throw new IllegalArgumentException(v1_2.toString());
        } else {
            return v0_0;
        }
    }

    public static Object d(java.util.Iterator p1, Object p2)
    {
        if (p1.hasNext()) {
            p2 = p1.next();
        }
        return p2;
    }

    public static boolean d(java.util.Iterator p1, com.a.b.b.co p2)
    {
        com.a.b.b.cn.a(p2);
        while (p1.hasNext()) {
            if (!p2.a(p1.next())) {
                int v0_1 = 0;
            }
            return v0_1;
        }
        v0_1 = 1;
        return v0_1;
    }

    public static Object e(java.util.Iterator p1, com.a.b.b.co p2)
    {
        return com.a.b.d.nj.b(p1, p2).next();
    }

    public static Object e(java.util.Iterator p1, Object p2)
    {
        if (p1.hasNext()) {
            p2 = com.a.b.d.nj.f(p1);
        }
        return p2;
    }

    public static java.util.Iterator e(java.util.Iterator p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.d.nt(p1);
    }

    public static java.util.Iterator e(java.util.Iterator p2, int p3)
    {
        com.a.b.d.nx v0_0;
        com.a.b.b.cn.a(p2);
        if (p3 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0, "limit is negative");
        return new com.a.b.d.nx(p3, p2);
    }

    public static com.a.b.b.ci f(java.util.Iterator p2, com.a.b.b.co p3)
    {
        com.a.b.b.ci v0_1;
        com.a.b.b.ci v0_0 = com.a.b.d.nj.b(p2, p3);
        if (!v0_0.hasNext()) {
            v0_1 = com.a.b.b.ci.f();
        } else {
            v0_1 = com.a.b.b.ci.b(v0_0.next());
        }
        return v0_1;
    }

    public static Object f(java.util.Iterator p2)
    {
        do {
            Object v0 = p2.next();
        } while(p2.hasNext());
        return v0;
    }

    public static int g(java.util.Iterator p2, com.a.b.b.co p3)
    {
        com.a.b.b.cn.a(p3, "predicate");
        int v0_1 = 0;
        while (p2.hasNext()) {
            if (!p3.a(p2.next())) {
                v0_1++;
            }
            return v0_1;
        }
        v0_1 = -1;
        return v0_1;
    }

    public static java.util.Iterator g(java.util.Iterator p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.d.nl(p1);
    }

    static Object h(java.util.Iterator p1)
    {
        int v0_1;
        if (!p1.hasNext()) {
            v0_1 = 0;
        } else {
            v0_1 = p1.next();
            p1.remove();
        }
        return v0_1;
    }

    static void i(java.util.Iterator p1)
    {
        com.a.b.b.cn.a(p1);
        while (p1.hasNext()) {
            p1.next();
            p1.remove();
        }
        return;
    }

    public static com.a.b.d.yi j(java.util.Iterator p1)
    {
        com.a.b.d.oa v1_1;
        if (!(p1 instanceof com.a.b.d.oa)) {
            v1_1 = new com.a.b.d.oa(p1);
        } else {
            v1_1 = ((com.a.b.d.oa) p1);
        }
        return v1_1;
    }

    static java.util.ListIterator k(java.util.Iterator p0)
    {
        return ((java.util.ListIterator) p0);
    }

    private static java.util.Enumeration l(java.util.Iterator p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.d.np(p1);
    }
}
