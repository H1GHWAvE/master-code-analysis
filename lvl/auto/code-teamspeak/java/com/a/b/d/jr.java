package com.a.b.d;
public class jr extends com.a.b.d.kk implements com.a.b.d.ou {
    private static final long d;
    private transient com.a.b.d.jr a;

    jr(com.a.b.d.jt p1, int p2)
    {
        this(p1, p2);
        return;
    }

    private static com.a.b.d.jl A()
    {
        throw new UnsupportedOperationException();
    }

    private static com.a.b.d.jl B()
    {
        throw new UnsupportedOperationException();
    }

    public static com.a.b.d.jr a()
    {
        return com.a.b.d.ex.a;
    }

    private static com.a.b.d.jr a(Object p1, Object p2, Object p3, Object p4)
    {
        com.a.b.d.jr v0_1 = new com.a.b.d.js();
        v0_1.a(p1, p2);
        v0_1.a(p3, p4);
        return v0_1.a();
    }

    private static com.a.b.d.jr a(Object p1, Object p2, Object p3, Object p4, Object p5, Object p6)
    {
        com.a.b.d.jr v0_1 = new com.a.b.d.js();
        v0_1.a(p1, p2);
        v0_1.a(p3, p4);
        v0_1.a(p5, p6);
        return v0_1.a();
    }

    private static com.a.b.d.jr a(Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8)
    {
        com.a.b.d.jr v0_1 = new com.a.b.d.js();
        v0_1.a(p1, p2);
        v0_1.a(p3, p4);
        v0_1.a(p5, p6);
        v0_1.a(p7, p8);
        return v0_1.a();
    }

    private static com.a.b.d.jr a(Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8, Object p9, Object p10)
    {
        com.a.b.d.jr v0_1 = new com.a.b.d.js();
        v0_1.a(p1, p2);
        v0_1.a(p3, p4);
        v0_1.a(p5, p6);
        v0_1.a(p7, p8);
        v0_1.a(p9, p10);
        return v0_1.a();
    }

    private void a(java.io.ObjectInputStream p11)
    {
        p11.defaultReadObject();
        int v4 = p11.readInt();
        if (v4 >= 0) {
            com.a.b.d.ju v5 = com.a.b.d.jt.l();
            int v2_0 = 0;
            int v3 = 0;
            while (v2_0 < v4) {
                Object v6 = p11.readObject();
                int v7 = p11.readInt();
                if (v7 > 0) {
                    Object[] v8 = new Object[v7];
                    int v0_5 = 0;
                    while (v0_5 < v7) {
                        v8[v0_5] = p11.readObject();
                        v0_5++;
                    }
                    v5.a(v6, com.a.b.d.jl.a(v8));
                    v3 += v7;
                    v2_0++;
                } else {
                    throw new java.io.InvalidObjectException(new StringBuilder(31).append("Invalid value count ").append(v7).toString());
                }
            }
            try {
                com.a.b.d.kq.a.a(this, v5.a());
                com.a.b.d.kq.b.a(this, v3);
                return;
            } catch (int v0_2) {
                throw ((java.io.InvalidObjectException) new java.io.InvalidObjectException(v0_2.getMessage()).initCause(v0_2));
            }
        } else {
            throw new java.io.InvalidObjectException(new StringBuilder(29).append("Invalid key count ").append(v4).toString());
        }
    }

    private void a(java.io.ObjectOutputStream p1)
    {
        p1.defaultWriteObject();
        com.a.b.d.zz.a(this, p1);
        return;
    }

    private static com.a.b.d.jr c(com.a.b.d.vi p6)
    {
        int v0_3;
        if (!p6.n()) {
            if ((p6 instanceof com.a.b.d.jr)) {
                v0_3 = ((com.a.b.d.jr) p6);
                if (!((com.a.b.d.jr) p6).b.i_()) {
                    return v0_3;
                }
            }
            com.a.b.d.ju v3 = com.a.b.d.jt.l();
            java.util.Iterator v4 = p6.b().entrySet().iterator();
            int v2 = 0;
            while (v4.hasNext()) {
                int v0_9;
                int v0_8 = ((java.util.Map$Entry) v4.next());
                com.a.b.d.jt v1_7 = com.a.b.d.jl.a(((java.util.Collection) v0_8.getValue()));
                if (v1_7.isEmpty()) {
                    v0_9 = v2;
                } else {
                    v3.a(v0_8.getKey(), v1_7);
                    v0_9 = (v1_7.size() + v2);
                }
                v2 = v0_9;
            }
            v0_3 = new com.a.b.d.jr(v3.a(), v2);
        } else {
            v0_3 = com.a.b.d.ex.a;
        }
        return v0_3;
    }

    public static com.a.b.d.js c()
    {
        return new com.a.b.d.js();
    }

    public static com.a.b.d.jr d(Object p1, Object p2)
    {
        com.a.b.d.jr v0_1 = new com.a.b.d.js();
        v0_1.a(p1, p2);
        return v0_1.a();
    }

    private com.a.b.d.jr u()
    {
        com.a.b.d.jr v0_0 = this.a;
        if (v0_0 == null) {
            com.a.b.d.js v1_1 = new com.a.b.d.js();
            java.util.Iterator v2 = this.v().iterator();
            while (v2.hasNext()) {
                com.a.b.d.jr v0_4 = ((java.util.Map$Entry) v2.next());
                v1_1.a(v0_4.getValue(), v0_4.getKey());
            }
            v0_0 = v1_1.a();
            v0_0.a = this;
            this.a = v0_0;
        }
        return v0_0;
    }

    private com.a.b.d.jr z()
    {
        com.a.b.d.js v1_1 = new com.a.b.d.js();
        java.util.Iterator v2 = this.v().iterator();
        while (v2.hasNext()) {
            com.a.b.d.jr v0_4 = ((java.util.Map$Entry) v2.next());
            v1_1.a(v0_4.getValue(), v0_4.getKey());
        }
        com.a.b.d.jr v0_2 = v1_1.a();
        v0_2.a = this;
        return v0_2;
    }

    public final synthetic java.util.List a(Object p2)
    {
        return this.e(p2);
    }

    public final synthetic java.util.List a(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic java.util.List b(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic java.util.Collection c(Object p2)
    {
        return this.e(p2);
    }

    public final synthetic com.a.b.d.kk d()
    {
        com.a.b.d.jr v0_0 = this.a;
        if (v0_0 == null) {
            com.a.b.d.js v1_1 = new com.a.b.d.js();
            java.util.Iterator v2 = this.v().iterator();
            while (v2.hasNext()) {
                com.a.b.d.jr v0_4 = ((java.util.Map$Entry) v2.next());
                v1_1.a(v0_4.getValue(), v0_4.getKey());
            }
            v0_0 = v1_1.a();
            v0_0.a = this;
            this.a = v0_0;
        }
        return v0_0;
    }

    public final synthetic java.util.Collection d(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic com.a.b.d.iz e()
    {
        throw new UnsupportedOperationException();
    }

    public final com.a.b.d.jl e(Object p2)
    {
        com.a.b.d.jl v0_2 = ((com.a.b.d.jl) this.b.get(p2));
        if (v0_2 == null) {
            v0_2 = com.a.b.d.jl.d();
        }
        return v0_2;
    }

    public final synthetic com.a.b.d.iz h(Object p2)
    {
        return this.e(p2);
    }

    public final synthetic com.a.b.d.iz t()
    {
        throw new UnsupportedOperationException();
    }
}
