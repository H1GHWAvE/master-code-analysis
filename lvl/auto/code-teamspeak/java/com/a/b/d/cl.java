package com.a.b.d;
final class cl {

    cl()
    {
        return;
    }

    static int a(int p4, String p5)
    {
        if (p4 >= 0) {
            return p4;
        } else {
            String v1_1 = String.valueOf(String.valueOf(p5));
            throw new IllegalArgumentException(new StringBuilder((v1_1.length() + 40)).append(v1_1).append(" cannot be negative but was: ").append(p4).toString());
        }
    }

    static void a(Object p4, Object p5)
    {
        if (p4 != null) {
            if (p5 != null) {
                return;
            } else {
                String v1_1 = String.valueOf(String.valueOf(p4));
                throw new NullPointerException(new StringBuilder((v1_1.length() + 26)).append("null value in entry: ").append(v1_1).append("=null").toString());
            }
        } else {
            String v1_6 = String.valueOf(String.valueOf(p5));
            throw new NullPointerException(new StringBuilder((v1_6.length() + 24)).append("null key in entry: null=").append(v1_6).toString());
        }
    }

    private static void a(boolean p1)
    {
        com.a.b.b.cn.b(p1, "no calls to next() since the last call to remove()");
        return;
    }
}
