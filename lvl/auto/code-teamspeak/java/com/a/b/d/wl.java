package com.a.b.d;
final class wl extends com.a.b.d.be {
    private static final long c;
    transient com.a.b.b.dz a;
    transient java.util.Comparator b;

    wl(java.util.Map p2, com.a.b.b.dz p3)
    {
        this(p2);
        this.a = ((com.a.b.b.dz) com.a.b.b.cn.a(p3));
        this.b = ((java.util.SortedSet) p3.a()).comparator();
        return;
    }

    private void a(java.io.ObjectInputStream p2)
    {
        p2.defaultReadObject();
        this.a = ((com.a.b.b.dz) p2.readObject());
        this.b = ((java.util.SortedSet) this.a.a()).comparator();
        this.a(((java.util.Map) p2.readObject()));
        return;
    }

    private void a(java.io.ObjectOutputStream p2)
    {
        p2.defaultWriteObject();
        p2.writeObject(this.a);
        p2.writeObject(this.e());
        return;
    }

    protected final synthetic java.util.Set a()
    {
        return this.y();
    }

    protected final synthetic java.util.Collection c()
    {
        return this.y();
    }

    public final java.util.Comparator d_()
    {
        return this.b;
    }

    protected final java.util.SortedSet y()
    {
        return ((java.util.SortedSet) this.a.a());
    }
}
