package com.a.b.d;
final class zk extends com.a.b.d.lo {
    final transient Object[] a;
    private final Object[] c;
    private final transient int d;
    private final transient int e;

    zk(Object[] p1, int p2, Object[] p3, int p4)
    {
        this.c = p1;
        this.a = p3;
        this.d = p4;
        this.e = p2;
        return;
    }

    final int a(Object[] p4, int p5)
    {
        System.arraycopy(this.c, 0, p4, p5, this.c.length);
        return (this.c.length + p5);
    }

    public final com.a.b.d.agi c()
    {
        return com.a.b.d.nj.a(this.c);
    }

    public final boolean contains(Object p5)
    {
        int v0_2;
        if (p5 != null) {
            int v0_1 = com.a.b.d.iq.a(p5.hashCode());
            while(true) {
                boolean v2_1 = this.a[(this.d & v0_1)];
                if (v2_1) {
                    if (v2_1.equals(p5)) {
                        break;
                    }
                    v0_1++;
                } else {
                    v0_2 = 0;
                }
            }
            v0_2 = 1;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    final boolean g_()
    {
        return 1;
    }

    final boolean h_()
    {
        return 0;
    }

    public final int hashCode()
    {
        return this.e;
    }

    public final synthetic java.util.Iterator iterator()
    {
        return com.a.b.d.nj.a(this.c);
    }

    final com.a.b.d.jl l()
    {
        return new com.a.b.d.yw(this, this.c);
    }

    public final int size()
    {
        return this.c.length;
    }
}
