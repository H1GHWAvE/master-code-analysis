package com.a.b.d;
public class lr extends com.a.b.d.kk implements com.a.b.d.aac {
    private static final long f;
    private final transient com.a.b.d.lo a;
    private transient com.a.b.d.lr d;
    private transient com.a.b.d.lo e;

    lr(com.a.b.d.jt p2, int p3, java.util.Comparator p4)
    {
        this(p2, p3);
        this.a = com.a.b.d.lr.a(p4);
        return;
    }

    private com.a.b.d.lr A()
    {
        com.a.b.d.ls v1_1 = new com.a.b.d.ls();
        java.util.Iterator v2 = this.D().iterator();
        while (v2.hasNext()) {
            com.a.b.d.lr v0_4 = ((java.util.Map$Entry) v2.next());
            v1_1.a(v0_4.getValue(), v0_4.getKey());
        }
        com.a.b.d.lr v0_2 = v1_1.a();
        v0_2.d = this;
        return v0_2;
    }

    private static com.a.b.d.lo B()
    {
        throw new UnsupportedOperationException();
    }

    private static com.a.b.d.lo C()
    {
        throw new UnsupportedOperationException();
    }

    private com.a.b.d.lo D()
    {
        com.a.b.d.lu v0_0 = this.e;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.lu(this);
            this.e = v0_0;
        }
        return v0_0;
    }

    private java.util.Comparator E()
    {
        int v0_2;
        if (!(this.a instanceof com.a.b.d.me)) {
            v0_2 = 0;
        } else {
            v0_2 = ((com.a.b.d.me) this.a).comparator();
        }
        return v0_2;
    }

    private static com.a.b.d.lo a(java.util.Comparator p1)
    {
        com.a.b.d.me v0;
        if (p1 != null) {
            v0 = com.a.b.d.me.a(p1);
        } else {
            v0 = com.a.b.d.lo.h();
        }
        return v0;
    }

    private static com.a.b.d.lo a(java.util.Comparator p1, java.util.Collection p2)
    {
        com.a.b.d.me v0;
        if (p1 != null) {
            v0 = com.a.b.d.me.a(p1, p2);
        } else {
            v0 = com.a.b.d.lo.a(p2);
        }
        return v0;
    }

    public static com.a.b.d.lr a()
    {
        return com.a.b.d.ez.a;
    }

    static synthetic com.a.b.d.lr a(com.a.b.d.vi p1, java.util.Comparator p2)
    {
        return com.a.b.d.lr.b(p1, p2);
    }

    private static com.a.b.d.lr a(Object p1, Object p2, Object p3, Object p4)
    {
        com.a.b.d.lr v0_1 = new com.a.b.d.ls();
        v0_1.a(p1, p2);
        v0_1.a(p3, p4);
        return v0_1.a();
    }

    private static com.a.b.d.lr a(Object p1, Object p2, Object p3, Object p4, Object p5, Object p6)
    {
        com.a.b.d.lr v0_1 = new com.a.b.d.ls();
        v0_1.a(p1, p2);
        v0_1.a(p3, p4);
        v0_1.a(p5, p6);
        return v0_1.a();
    }

    private static com.a.b.d.lr a(Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8)
    {
        com.a.b.d.lr v0_1 = new com.a.b.d.ls();
        v0_1.a(p1, p2);
        v0_1.a(p3, p4);
        v0_1.a(p5, p6);
        v0_1.a(p7, p8);
        return v0_1.a();
    }

    private static com.a.b.d.lr a(Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8, Object p9, Object p10)
    {
        com.a.b.d.lr v0_1 = new com.a.b.d.ls();
        v0_1.a(p1, p2);
        v0_1.a(p3, p4);
        v0_1.a(p5, p6);
        v0_1.a(p7, p8);
        v0_1.a(p9, p10);
        return v0_1.a();
    }

    private void a(java.io.ObjectInputStream p12)
    {
        p12.defaultReadObject();
        java.io.InvalidObjectException v0_1 = ((java.util.Comparator) p12.readObject());
        int v5 = p12.readInt();
        if (v5 >= 0) {
            com.a.b.d.ju v6 = com.a.b.d.jt.l();
            int v3_0 = 0;
            int v4 = 0;
            while (v3_0 < v5) {
                Object v7 = p12.readObject();
                int v8 = p12.readInt();
                if (v8 > 0) {
                    int v9_0 = new Object[v8];
                    int v1_5 = 0;
                    while (v1_5 < v8) {
                        v9_0[v1_5] = p12.readObject();
                        v1_5++;
                    }
                    int v1_7 = com.a.b.d.lr.a(v0_1, java.util.Arrays.asList(v9_0));
                    if (v1_7.size() == v9_0.length) {
                        v6.a(v7, v1_7);
                        v4 += v8;
                        v3_0++;
                    } else {
                        int v1_10 = String.valueOf(String.valueOf(v7));
                        throw new java.io.InvalidObjectException(new StringBuilder((v1_10.length() + 40)).append("Duplicate key-value pairs exist for key ").append(v1_10).toString());
                    }
                } else {
                    throw new java.io.InvalidObjectException(new StringBuilder(31).append("Invalid value count ").append(v8).toString());
                }
            }
            try {
                com.a.b.d.kq.a.a(this, v6.a());
                com.a.b.d.kq.b.a(this, v4);
                com.a.b.d.kq.c.a(this, com.a.b.d.lr.a(v0_1));
                return;
            } catch (java.io.InvalidObjectException v0_3) {
                throw ((java.io.InvalidObjectException) new java.io.InvalidObjectException(v0_3.getMessage()).initCause(v0_3));
            }
        } else {
            throw new java.io.InvalidObjectException(new StringBuilder(29).append("Invalid key count ").append(v5).toString());
        }
    }

    private void a(java.io.ObjectOutputStream p2)
    {
        int v0_2;
        p2.defaultWriteObject();
        if (!(this.a instanceof com.a.b.d.me)) {
            v0_2 = 0;
        } else {
            v0_2 = ((com.a.b.d.me) this.a).comparator();
        }
        p2.writeObject(v0_2);
        com.a.b.d.zz.a(this, p2);
        return;
    }

    private static com.a.b.d.lr b(com.a.b.d.vi p6, java.util.Comparator p7)
    {
        int v0_3;
        com.a.b.b.cn.a(p6);
        if ((!p6.n()) || (p7 != null)) {
            if ((p6 instanceof com.a.b.d.lr)) {
                v0_3 = ((com.a.b.d.lr) p6);
                if (!((com.a.b.d.lr) p6).b.i_()) {
                    return v0_3;
                }
            }
            com.a.b.d.jt v2_0 = com.a.b.d.jt.l();
            java.util.Iterator v3 = p6.b().entrySet().iterator();
            int v1_4 = 0;
            while (v3.hasNext()) {
                int v0_12;
                int v0_8 = ((java.util.Map$Entry) v3.next());
                Object v4 = v0_8.getKey();
                int v0_11 = com.a.b.d.lr.a(p7, ((java.util.Collection) v0_8.getValue()));
                if (v0_11.isEmpty()) {
                    v0_12 = v1_4;
                } else {
                    v2_0.a(v4, v0_11);
                    v0_12 = (v0_11.size() + v1_4);
                }
                v1_4 = v0_12;
            }
            v0_3 = new com.a.b.d.lr(v2_0.a(), v1_4, p7);
        } else {
            v0_3 = com.a.b.d.ez.a;
        }
        return v0_3;
    }

    private static com.a.b.d.lr c(com.a.b.d.vi p1)
    {
        return com.a.b.d.lr.b(p1, 0);
    }

    public static com.a.b.d.ls c()
    {
        return new com.a.b.d.ls();
    }

    private static com.a.b.d.lr d(Object p1, Object p2)
    {
        com.a.b.d.lr v0_1 = new com.a.b.d.ls();
        v0_1.a(p1, p2);
        return v0_1.a();
    }

    private com.a.b.d.lo e(Object p3)
    {
        return ((com.a.b.d.lo) com.a.b.b.ca.a(((com.a.b.d.lo) this.b.get(p3)), this.a));
    }

    private com.a.b.d.lr z()
    {
        com.a.b.d.lr v0_0 = this.d;
        if (v0_0 == null) {
            com.a.b.d.ls v1_1 = new com.a.b.d.ls();
            java.util.Iterator v2 = this.D().iterator();
            while (v2.hasNext()) {
                com.a.b.d.lr v0_4 = ((java.util.Map$Entry) v2.next());
                v1_1.a(v0_4.getValue(), v0_4.getKey());
            }
            v0_0 = v1_1.a();
            v0_0.d = this;
            this.d = v0_0;
        }
        return v0_0;
    }

    public final synthetic java.util.Set a(Object p2)
    {
        return this.e(p2);
    }

    public final synthetic java.util.Set a(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic java.util.Set b(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic java.util.Collection c(Object p2)
    {
        return this.e(p2);
    }

    public final synthetic com.a.b.d.kk d()
    {
        com.a.b.d.lr v0_0 = this.d;
        if (v0_0 == null) {
            com.a.b.d.ls v1_1 = new com.a.b.d.ls();
            java.util.Iterator v2 = this.D().iterator();
            while (v2.hasNext()) {
                com.a.b.d.lr v0_4 = ((java.util.Map$Entry) v2.next());
                v1_1.a(v0_4.getValue(), v0_4.getKey());
            }
            v0_0 = v1_1.a();
            v0_0.d = this;
            this.d = v0_0;
        }
        return v0_0;
    }

    public final synthetic java.util.Collection d(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic com.a.b.d.iz e()
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic com.a.b.d.iz h(Object p2)
    {
        return this.e(p2);
    }

    public final synthetic java.util.Collection k()
    {
        return this.D();
    }

    public final synthetic com.a.b.d.iz t()
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic java.util.Set u()
    {
        return this.D();
    }

    public final synthetic com.a.b.d.iz v()
    {
        return this.D();
    }
}
