package com.a.b.d;
public class afm extends com.a.b.d.ay {
    final java.util.NavigableMap a;
    private transient java.util.Set b;
    private transient com.a.b.d.yr c;

    private afm(java.util.NavigableMap p1)
    {
        this.a = p1;
        return;
    }

    synthetic afm(java.util.NavigableMap p1, byte p2)
    {
        this(p1);
        return;
    }

    private static synthetic com.a.b.d.yl a(com.a.b.d.afm p2, com.a.b.d.yl p3)
    {
        com.a.b.d.yl v0_4;
        com.a.b.b.cn.a(p3);
        java.util.Map$Entry v1_1 = p2.a.floorEntry(p3.b);
        if ((v1_1 == null) || (!((com.a.b.d.yl) v1_1.getValue()).a(p3))) {
            v0_4 = 0;
        } else {
            v0_4 = ((com.a.b.d.yl) v1_1.getValue());
        }
        return v0_4;
    }

    public static com.a.b.d.afm c()
    {
        return new com.a.b.d.afm(new java.util.TreeMap());
    }

    private static com.a.b.d.afm d(com.a.b.d.yr p1)
    {
        com.a.b.d.afm v0 = com.a.b.d.afm.c();
        super.b(p1);
        return v0;
    }

    private com.a.b.d.yl d(com.a.b.d.yl p3)
    {
        com.a.b.d.yl v0_4;
        com.a.b.b.cn.a(p3);
        java.util.Map$Entry v1_1 = this.a.floorEntry(p3.b);
        if ((v1_1 == null) || (!((com.a.b.d.yl) v1_1.getValue()).a(p3))) {
            v0_4 = 0;
        } else {
            v0_4 = ((com.a.b.d.yl) v1_1.getValue());
        }
        return v0_4;
    }

    private void f(com.a.b.d.yl p3)
    {
        if (!p3.f()) {
            this.a.put(p3.b, p3);
        } else {
            this.a.remove(p3.b);
        }
        return;
    }

    public void a(com.a.b.d.yl p6)
    {
        com.a.b.b.cn.a(p6);
        if (!p6.f()) {
            com.a.b.d.dw v1_1;
            com.a.b.d.dw v2_1;
            com.a.b.d.dw v2_0 = p6.b;
            com.a.b.d.dw v1_0 = p6.c;
            com.a.b.d.yl v0_2 = this.a.lowerEntry(v2_0);
            if (v0_2 == null) {
                v2_1 = v1_0;
                v1_1 = v2_0;
            } else {
                com.a.b.d.yl v0_4 = ((com.a.b.d.yl) v0_2.getValue());
                if (v0_4.c.a(v2_0) < 0) {
                } else {
                    if (v0_4.c.a(v1_0) >= 0) {
                        v1_0 = v0_4.c;
                    }
                    v2_1 = v1_0;
                    v1_1 = v0_4.b;
                }
            }
            com.a.b.d.yl v0_7 = this.a.floorEntry(v2_1);
            if (v0_7 != null) {
                com.a.b.d.yl v0_9 = ((com.a.b.d.yl) v0_7.getValue());
                if (v0_9.c.a(v2_1) >= 0) {
                    v2_1 = v0_9.c;
                }
            }
            this.a.subMap(v1_1, v2_1).clear();
            this.f(com.a.b.d.yl.a(v1_1, v2_1));
        }
        return;
    }

    public final bridge synthetic boolean a()
    {
        return super.a();
    }

    public final bridge synthetic boolean a(com.a.b.d.yr p2)
    {
        return super.a(p2);
    }

    public bridge synthetic boolean a(Comparable p2)
    {
        return super.a(p2);
    }

    public com.a.b.d.yl b(Comparable p3)
    {
        com.a.b.d.yl v0_4;
        com.a.b.b.cn.a(p3);
        java.util.Map$Entry v1_1 = this.a.floorEntry(com.a.b.d.dw.b(p3));
        if ((v1_1 == null) || (!((com.a.b.d.yl) v1_1.getValue()).c(p3))) {
            v0_4 = 0;
        } else {
            v0_4 = ((com.a.b.d.yl) v1_1.getValue());
        }
        return v0_4;
    }

    public bridge synthetic void b()
    {
        super.b();
        return;
    }

    public void b(com.a.b.d.yl p4)
    {
        com.a.b.b.cn.a(p4);
        if (!p4.f()) {
            com.a.b.d.yl v0_2 = this.a.lowerEntry(p4.b);
            if (v0_2 != null) {
                com.a.b.d.yl v0_4 = ((com.a.b.d.yl) v0_2.getValue());
                if (v0_4.c.a(p4.b) >= 0) {
                    if ((p4.e()) && (v0_4.c.a(p4.c) >= 0)) {
                        this.f(com.a.b.d.yl.a(p4.c, v0_4.c));
                    }
                    this.f(com.a.b.d.yl.a(v0_4.b, p4.b));
                }
            }
            com.a.b.d.yl v0_8 = this.a.floorEntry(p4.c);
            if (v0_8 != null) {
                com.a.b.d.yl v0_10 = ((com.a.b.d.yl) v0_8.getValue());
                if ((p4.e()) && (v0_10.c.a(p4.c) >= 0)) {
                    this.f(com.a.b.d.yl.a(p4.c, v0_10.c));
                }
            }
            this.a.subMap(p4.b, p4.c).clear();
        }
        return;
    }

    public final bridge synthetic void b(com.a.b.d.yr p1)
    {
        super.b(p1);
        return;
    }

    public final bridge synthetic void c(com.a.b.d.yr p1)
    {
        super.c(p1);
        return;
    }

    public boolean c(com.a.b.d.yl p3)
    {
        int v0_5;
        com.a.b.b.cn.a(p3);
        int v0_1 = this.a.floorEntry(p3.b);
        if ((v0_1 == 0) || (!((com.a.b.d.yl) v0_1.getValue()).a(p3))) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public final com.a.b.d.yl e()
    {
        com.a.b.d.yl v0_1 = this.a.firstEntry();
        java.util.Map$Entry v1_1 = this.a.lastEntry();
        if (v0_1 != null) {
            return com.a.b.d.yl.a(((com.a.b.d.yl) v0_1.getValue()).b, ((com.a.b.d.yl) v1_1.getValue()).c);
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public com.a.b.d.yr e(com.a.b.d.yl p2)
    {
        if (!p2.equals(com.a.b.d.yl.c())) {
            this = new com.a.b.d.afw(this, p2);
        }
        return this;
    }

    public bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public com.a.b.d.yr f()
    {
        com.a.b.d.afp v0_0 = this.c;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.afp(this);
            this.c = v0_0;
        }
        return v0_0;
    }

    public final java.util.Set g()
    {
        com.a.b.d.afo v0_0 = this.b;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.afo(this);
            this.b = v0_0;
        }
        return v0_0;
    }
}
