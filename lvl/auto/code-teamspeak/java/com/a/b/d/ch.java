package com.a.b.d;
final class ch extends com.a.b.d.yd implements java.io.Serializable {
    private static final long e;
    final com.a.b.b.bj a;
    final com.a.b.d.yd b;

    ch(com.a.b.b.bj p2, com.a.b.d.yd p3)
    {
        this.a = ((com.a.b.b.bj) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.d.yd) com.a.b.b.cn.a(p3));
        return;
    }

    public final int compare(Object p4, Object p5)
    {
        return this.b.compare(this.a.e(p4), this.a.e(p5));
    }

    public final boolean equals(Object p5)
    {
        int v0 = 1;
        if (p5 != this) {
            if (!(p5 instanceof com.a.b.d.ch)) {
                v0 = 0;
            } else {
                if ((!this.a.equals(((com.a.b.d.ch) p5).a)) || (!this.b.equals(((com.a.b.d.ch) p5).b))) {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[2];
        v0_1[0] = this.a;
        v0_1[1] = this.b;
        return java.util.Arrays.hashCode(v0_1);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.b));
        String v1_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder(((v0_2.length() + 13) + v1_2.length())).append(v0_2).append(".onResultOf(").append(v1_2).append(")").toString();
    }
}
