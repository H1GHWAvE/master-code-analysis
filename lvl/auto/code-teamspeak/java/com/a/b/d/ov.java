package com.a.b.d;
public final class ov {

    private ov()
    {
        return;
    }

    private static com.a.b.d.jl a(String p2)
    {
        return new com.a.b.d.ph(((String) com.a.b.b.cn.a(p2)));
    }

    private static java.util.ArrayList a()
    {
        return new java.util.ArrayList();
    }

    public static java.util.ArrayList a(int p1)
    {
        com.a.b.d.cl.a(p1, "initialArraySize");
        return new java.util.ArrayList(p1);
    }

    public static java.util.ArrayList a(Iterable p2)
    {
        java.util.ArrayList v0_2;
        com.a.b.b.cn.a(p2);
        if (!(p2 instanceof java.util.Collection)) {
            v0_2 = com.a.b.d.ov.a(p2.iterator());
        } else {
            v0_2 = new java.util.ArrayList(com.a.b.d.cm.a(p2));
        }
        return v0_2;
    }

    public static java.util.ArrayList a(java.util.Iterator p1)
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        com.a.b.d.nj.a(v0_1, p1);
        return v0_1;
    }

    public static varargs java.util.ArrayList a(Object[] p2)
    {
        com.a.b.b.cn.a(p2);
        java.util.ArrayList v1_1 = new java.util.ArrayList(com.a.b.d.ov.c(p2.length));
        java.util.Collections.addAll(v1_1, p2);
        return v1_1;
    }

    private static java.util.List a(CharSequence p2)
    {
        return new com.a.b.d.oz(((CharSequence) com.a.b.b.cn.a(p2)));
    }

    private static java.util.List a(Object p1, Object p2, Object[] p3)
    {
        return new com.a.b.d.pm(p1, p2, p3);
    }

    private static java.util.List a(Object p1, Object[] p2)
    {
        return new com.a.b.d.pa(p1, p2);
    }

    private static java.util.List a(java.util.List p1)
    {
        return com.a.b.d.ci.a(p1);
    }

    private static java.util.List a(java.util.List p1, int p2)
    {
        com.a.b.d.pb v0_0;
        com.a.b.b.cn.a(p1);
        if (p2 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.d.pb v0_3;
        com.a.b.b.cn.a(v0_0);
        if (!(p1 instanceof java.util.RandomAccess)) {
            v0_3 = new com.a.b.d.pb(p1, p2);
        } else {
            v0_3 = new com.a.b.d.pd(p1, p2);
        }
        return v0_3;
    }

    private static java.util.List a(java.util.List p1, int p2, int p3)
    {
        java.util.List v0_2;
        if (!(p1 instanceof java.util.RandomAccess)) {
            v0_2 = new com.a.b.d.ox(p1);
        } else {
            v0_2 = new com.a.b.d.ow(p1);
        }
        return v0_2.subList(p2, p3);
    }

    public static java.util.List a(java.util.List p1, com.a.b.b.bj p2)
    {
        com.a.b.d.pk v0_2;
        if (!(p1 instanceof java.util.RandomAccess)) {
            v0_2 = new com.a.b.d.pk(p1, p2);
        } else {
            v0_2 = new com.a.b.d.pi(p1, p2);
        }
        return v0_2;
    }

    private static varargs java.util.List a(java.util.List[] p1)
    {
        return com.a.b.d.ci.a(java.util.Arrays.asList(p1));
    }

    private static boolean a(java.util.List p4, int p5, Iterable p6)
    {
        int v0_0 = 0;
        java.util.ListIterator v1 = p4.listIterator(p5);
        java.util.Iterator v2 = p6.iterator();
        while (v2.hasNext()) {
            v1.add(v2.next());
            v0_0 = 1;
        }
        return v0_0;
    }

    static boolean a(java.util.List p4, Object p5)
    {
        int v0 = 1;
        if (p5 != com.a.b.b.cn.a(p4)) {
            if ((p5 instanceof java.util.List)) {
                if ((p4.size() != ((java.util.List) p5).size()) || (!com.a.b.d.nj.a(p4.iterator(), ((java.util.List) p5).iterator()))) {
                    v0 = 0;
                }
            } else {
                v0 = 0;
            }
        }
        return v0;
    }

    static int b(java.util.List p2, Object p3)
    {
        int v0_0 = p2.listIterator();
        while (v0_0.hasNext()) {
            if (com.a.b.b.ce.a(p3, v0_0.next())) {
                int v0_1 = v0_0.previousIndex();
            }
            return v0_1;
        }
        v0_1 = -1;
        return v0_1;
    }

    public static java.util.ArrayList b(int p2)
    {
        return new java.util.ArrayList(com.a.b.d.ov.c(p2));
    }

    private static java.util.LinkedList b()
    {
        return new java.util.LinkedList();
    }

    private static java.util.LinkedList b(Iterable p1)
    {
        java.util.LinkedList v0_1 = new java.util.LinkedList();
        com.a.b.d.mq.a(v0_1, p1);
        return v0_1;
    }

    private static java.util.List b(java.util.List p1)
    {
        com.a.b.d.pf v0_4;
        if (!(p1 instanceof com.a.b.d.jl)) {
            if (!(p1 instanceof com.a.b.d.pf)) {
                if (!(p1 instanceof java.util.RandomAccess)) {
                    v0_4 = new com.a.b.d.pf(p1);
                } else {
                    v0_4 = new com.a.b.d.pe(p1);
                }
            } else {
                v0_4 = ((com.a.b.d.pf) p1).a;
            }
        } else {
            v0_4 = ((com.a.b.d.jl) p1).e();
        }
        return v0_4;
    }

    private static java.util.ListIterator b(java.util.List p1, int p2)
    {
        return new com.a.b.d.oy(p1).listIterator(p2);
    }

    private static int c(int p4)
    {
        com.a.b.d.cl.a(p4, "arraySize");
        return com.a.b.l.q.b(((5 + ((long) p4)) + ((long) (p4 / 10))));
    }

    private static int c(java.util.List p4)
    {
        int v0_0 = 1;
        java.util.Iterator v1 = p4.iterator();
        while (v1.hasNext()) {
            int v0_1;
            Object v2_1 = v1.next();
            int v3 = (v0_0 * 31);
            if (v2_1 != null) {
                v0_1 = v2_1.hashCode();
            } else {
                v0_1 = 0;
            }
            v0_0 = (((v0_1 + v3) ^ -1) ^ -1);
        }
        return v0_0;
    }

    static int c(java.util.List p2, Object p3)
    {
        int v0_1 = p2.listIterator(p2.size());
        while (v0_1.hasPrevious()) {
            if (com.a.b.b.ce.a(p3, v0_1.previous())) {
                int v0_2 = v0_1.nextIndex();
            }
            return v0_2;
        }
        v0_2 = -1;
        return v0_2;
    }

    private static java.util.concurrent.CopyOnWriteArrayList c()
    {
        return new java.util.concurrent.CopyOnWriteArrayList();
    }

    private static java.util.concurrent.CopyOnWriteArrayList c(Iterable p2)
    {
        java.util.ArrayList v0_1;
        if (!(p2 instanceof java.util.Collection)) {
            v0_1 = com.a.b.d.ov.a(p2);
        } else {
            v0_1 = com.a.b.d.cm.a(p2);
        }
        return new java.util.concurrent.CopyOnWriteArrayList(v0_1);
    }

    private static java.util.List d(Iterable p0)
    {
        return ((java.util.List) p0);
    }
}
