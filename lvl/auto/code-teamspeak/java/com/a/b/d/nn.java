package com.a.b.d;
final class nn extends com.a.b.d.agi {
    boolean a;
    final synthetic Object b;

    nn(Object p1)
    {
        this.b = p1;
        return;
    }

    public final boolean hasNext()
    {
        int v0_1;
        if (this.a) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final Object next()
    {
        if (!this.a) {
            this.a = 1;
            return this.b;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }
}
