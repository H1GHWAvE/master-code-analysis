package com.a.b.d;
final class afd extends java.util.AbstractMap {
    final synthetic com.a.b.d.afb a;

    private afd(com.a.b.d.afb p1)
    {
        this.a = p1;
        return;
    }

    synthetic afd(com.a.b.d.afb p1, byte p2)
    {
        this(p1);
        return;
    }

    public final boolean containsKey(Object p2)
    {
        int v0_1;
        if (this.get(p2) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final java.util.Set entrySet()
    {
        return new com.a.b.d.afe(this);
    }

    public final Object get(Object p3)
    {
        Object v0_5;
        if (!(p3 instanceof com.a.b.d.yl)) {
            v0_5 = 0;
        } else {
            Object v0_4 = ((com.a.b.d.aff) com.a.b.d.afb.a(this.a).get(((com.a.b.d.yl) p3).b));
            if ((v0_4 == null) || (!v0_4.a.equals(((com.a.b.d.yl) p3)))) {
            } else {
                v0_5 = v0_4.getValue();
            }
        }
        return v0_5;
    }
}
