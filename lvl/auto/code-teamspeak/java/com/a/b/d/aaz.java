package com.a.b.d;
final class aaz {

    private aaz()
    {
        return;
    }

    public static java.util.Comparator a(java.util.SortedSet p1)
    {
        com.a.b.d.yd v0 = p1.comparator();
        if (v0 == null) {
            v0 = com.a.b.d.yd.d();
        }
        return v0;
    }

    public static boolean a(java.util.Comparator p1, Iterable p2)
    {
        int v0_2;
        int v0_3;
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        if (!(p2 instanceof java.util.SortedSet)) {
            if (!(p2 instanceof com.a.b.d.aay)) {
                v0_2 = 0;
            } else {
                v0_3 = ((com.a.b.d.aay) p2).comparator();
                v0_2 = p1.equals(v0_3);
            }
        } else {
            v0_3 = com.a.b.d.aaz.a(((java.util.SortedSet) p2));
        }
        return v0_2;
    }
}
