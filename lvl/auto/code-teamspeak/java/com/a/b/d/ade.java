package com.a.b.d;
final class ade extends com.a.b.d.ado implements java.util.Deque {
    private static final long a;

    ade(java.util.Deque p1)
    {
        this(p1);
        return;
    }

    private java.util.Deque c()
    {
        return ((java.util.Deque) super.a());
    }

    final bridge synthetic java.util.Queue a()
    {
        return ((java.util.Deque) super.a());
    }

    public final void addFirst(Object p3)
    {
        try {
            ((java.util.Deque) super.a()).addFirst(p3);
            return;
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final void addLast(Object p3)
    {
        try {
            ((java.util.Deque) super.a()).addLast(p3);
            return;
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    final synthetic java.util.Collection b()
    {
        return ((java.util.Deque) super.a());
    }

    final synthetic Object d()
    {
        return ((java.util.Deque) super.a());
    }

    public final java.util.Iterator descendingIterator()
    {
        try {
            return ((java.util.Deque) super.a()).descendingIterator();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final Object getFirst()
    {
        try {
            return ((java.util.Deque) super.a()).getFirst();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final Object getLast()
    {
        try {
            return ((java.util.Deque) super.a()).getLast();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final boolean offerFirst(Object p3)
    {
        try {
            return ((java.util.Deque) super.a()).offerFirst(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final boolean offerLast(Object p3)
    {
        try {
            return ((java.util.Deque) super.a()).offerLast(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final Object peekFirst()
    {
        try {
            return ((java.util.Deque) super.a()).peekFirst();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final Object peekLast()
    {
        try {
            return ((java.util.Deque) super.a()).peekLast();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final Object pollFirst()
    {
        try {
            return ((java.util.Deque) super.a()).pollFirst();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final Object pollLast()
    {
        try {
            return ((java.util.Deque) super.a()).pollLast();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final Object pop()
    {
        try {
            return ((java.util.Deque) super.a()).pop();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final void push(Object p3)
    {
        try {
            ((java.util.Deque) super.a()).push(p3);
            return;
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final Object removeFirst()
    {
        try {
            return ((java.util.Deque) super.a()).removeFirst();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final boolean removeFirstOccurrence(Object p3)
    {
        try {
            return ((java.util.Deque) super.a()).removeFirstOccurrence(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final Object removeLast()
    {
        try {
            return ((java.util.Deque) super.a()).removeLast();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final boolean removeLastOccurrence(Object p3)
    {
        try {
            return ((java.util.Deque) super.a()).removeLastOccurrence(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }
}
