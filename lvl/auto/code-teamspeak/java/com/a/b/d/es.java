package com.a.b.d;
final class es extends com.a.b.d.ep implements java.io.Serializable {
    private static final com.a.b.d.es a;
    private static final long b;

    static es()
    {
        com.a.b.d.es.a = new com.a.b.d.es();
        return;
    }

    private es()
    {
        return;
    }

    private static long a(Long p8, Long p9)
    {
        double v0_1 = (p9.longValue() - p8.longValue());
        if ((p9.longValue() <= p8.longValue()) || (v0_1 >= 0)) {
            if ((p9.longValue() < p8.longValue()) && (v0_1 > 0)) {
                v0_1 = -0.0;
            }
        } else {
            v0_1 = nan;
        }
        return v0_1;
    }

    private static Long a(Long p4)
    {
        Long v0_2;
        Long v0_0 = p4.longValue();
        if (v0_0 != nan) {
            v0_2 = Long.valueOf((v0_0 + 1));
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private static Long b(Long p4)
    {
        Long v0_2;
        Long v0_0 = p4.longValue();
        if (v0_0 != -0.0) {
            v0_2 = Long.valueOf((v0_0 - 1));
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    static synthetic com.a.b.d.es c()
    {
        return com.a.b.d.es.a;
    }

    private static Long d()
    {
        return Long.valueOf(-0.0);
    }

    private static Long e()
    {
        return Long.valueOf(nan);
    }

    private static Object f()
    {
        return com.a.b.d.es.a;
    }

    public final synthetic long a(Comparable p9, Comparable p10)
    {
        double v0_1 = (((Long) p10).longValue() - ((Long) p9).longValue());
        if ((((Long) p10).longValue() <= ((Long) p9).longValue()) || (v0_1 >= 0)) {
            if ((((Long) p10).longValue() < ((Long) p9).longValue()) && (v0_1 > 0)) {
                v0_1 = -0.0;
            }
        } else {
            v0_1 = nan;
        }
        return v0_1;
    }

    public final synthetic Comparable a()
    {
        return Long.valueOf(-0.0);
    }

    public final synthetic Comparable a(Comparable p5)
    {
        Long v0_2;
        Long v0_0 = ((Long) p5).longValue();
        if (v0_0 != nan) {
            v0_2 = Long.valueOf((v0_0 + 1));
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final synthetic Comparable b()
    {
        return Long.valueOf(nan);
    }

    public final synthetic Comparable b(Comparable p5)
    {
        Long v0_2;
        Long v0_0 = ((Long) p5).longValue();
        if (v0_0 != -0.0) {
            v0_2 = Long.valueOf((v0_0 - 1));
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final String toString()
    {
        return "DiscreteDomain.longs()";
    }
}
