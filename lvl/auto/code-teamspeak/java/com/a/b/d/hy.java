package com.a.b.d;
public final class hy extends java.util.AbstractMap implements com.a.b.d.bw, java.io.Serializable {
    private static final double b = 16368;
    private static final long h;
    transient int a;
    private transient com.a.b.d.ia[] c;
    private transient com.a.b.d.ia[] d;
    private transient int e;
    private transient int f;
    private transient com.a.b.d.bw g;

    private hy(int p1)
    {
        this.b(p1);
        return;
    }

    static synthetic int a(com.a.b.d.hy p1)
    {
        return p1.f;
    }

    static int a(Object p1)
    {
        int v0_0;
        if (p1 != null) {
            v0_0 = p1.hashCode();
        } else {
            v0_0 = 0;
        }
        return com.a.b.d.iq.a(v0_0);
    }

    public static com.a.b.d.hy a()
    {
        return com.a.b.d.hy.a(16);
    }

    private static com.a.b.d.hy a(int p1)
    {
        return new com.a.b.d.hy(p1);
    }

    private static com.a.b.d.hy a(java.util.Map p1)
    {
        com.a.b.d.hy v0_1 = com.a.b.d.hy.a(p1.size());
        v0_1.putAll(p1);
        return v0_1;
    }

    static synthetic com.a.b.d.ia a(com.a.b.d.hy p1, Object p2, int p3)
    {
        return p1.b(p2, p3);
    }

    static synthetic Object a(com.a.b.d.hy p4, Object p5, Object p6, boolean p7)
    {
        IllegalArgumentException v0_0 = com.a.b.d.hy.a(p5);
        String v1_0 = com.a.b.d.hy.a(p6);
        StringBuilder v2_0 = p4.a(p5, v0_0);
        if ((v2_0 == null) || ((v1_0 != v2_0.a) || (!com.a.b.b.ce.a(p6, v2_0.e)))) {
            com.a.b.d.ia v3_3 = p4.b(p6, v1_0);
            if (v3_3 != null) {
                if (!p7) {
                    String v1_2 = String.valueOf(String.valueOf(p6));
                    throw new IllegalArgumentException(new StringBuilder((v1_2.length() + 23)).append("value already present: ").append(v1_2).toString());
                } else {
                    p4.a(v3_3);
                }
            }
            if (v2_0 != null) {
                p4.a(v2_0);
            }
            p4.b(new com.a.b.d.ia(p6, v1_0, p5, v0_0)).d();
            if (v2_0 != null) {
                p6 = v2_0.e;
            } else {
                p6 = 0;
            }
        }
        return p6;
    }

    private Object a(Object p5, Object p6, boolean p7)
    {
        IllegalArgumentException v0_0 = com.a.b.d.hy.a(p5);
        String v1_0 = com.a.b.d.hy.a(p6);
        StringBuilder v2_0 = this.b(p5, v0_0);
        if ((v2_0 == null) || ((v1_0 != v2_0.b) || (!com.a.b.b.ce.a(p6, v2_0.f)))) {
            com.a.b.d.ia v3_3 = this.a(p6, v1_0);
            if (v3_3 != null) {
                if (!p7) {
                    String v1_2 = String.valueOf(String.valueOf(p6));
                    throw new IllegalArgumentException(new StringBuilder((v1_2.length() + 23)).append("value already present: ").append(v1_2).toString());
                } else {
                    this.a(v3_3);
                }
            }
            if (v2_0 != null) {
                this.a(v2_0);
            }
            this.b(new com.a.b.d.ia(p5, v0_0, p6, v1_0));
            this.d();
            if (v2_0 != null) {
                p6 = v2_0.f;
            } else {
                p6 = 0;
            }
        }
        return p6;
    }

    static synthetic void a(com.a.b.d.hy p0, com.a.b.d.ia p1)
    {
        p0.a(p1);
        return;
    }

    private void a(java.io.ObjectInputStream p2)
    {
        p2.defaultReadObject();
        int v0 = p2.readInt();
        this.b(v0);
        com.a.b.d.zz.a(this, p2, v0);
        return;
    }

    private void a(java.io.ObjectOutputStream p1)
    {
        p1.defaultWriteObject();
        com.a.b.d.zz.a(this, p1);
        return;
    }

    static synthetic int b(Object p1)
    {
        return com.a.b.d.hy.a(p1);
    }

    static synthetic com.a.b.d.ia b(com.a.b.d.hy p1, Object p2, int p3)
    {
        return p1.a(p2, p3);
    }

    private com.a.b.d.ia b(Object p3, int p4)
    {
        com.a.b.d.ia v0_1 = this.c[(this.e & p4)];
        while (v0_1 != null) {
            if ((p4 != v0_1.a) || (!com.a.b.b.ce.a(p3, v0_1.e))) {
                v0_1 = v0_1.c;
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    private Object b(Object p5, Object p6, boolean p7)
    {
        IllegalArgumentException v0_0 = com.a.b.d.hy.a(p5);
        String v1_0 = com.a.b.d.hy.a(p6);
        StringBuilder v2_0 = this.a(p5, v0_0);
        if ((v2_0 == null) || ((v1_0 != v2_0.a) || (!com.a.b.b.ce.a(p6, v2_0.e)))) {
            com.a.b.d.ia v3_3 = this.b(p6, v1_0);
            if (v3_3 != null) {
                if (!p7) {
                    String v1_2 = String.valueOf(String.valueOf(p6));
                    throw new IllegalArgumentException(new StringBuilder((v1_2.length() + 23)).append("value already present: ").append(v1_2).toString());
                } else {
                    this.a(v3_3);
                }
            }
            if (v2_0 != null) {
                this.a(v2_0);
            }
            this.b(new com.a.b.d.ia(p6, v1_0, p5, v0_0));
            this.d();
            if (v2_0 != null) {
                p6 = v2_0.e;
            } else {
                p6 = 0;
            }
        }
        return p6;
    }

    private void b(int p4)
    {
        com.a.b.d.cl.a(p4, "expectedSize");
        int v0_2 = com.a.b.d.iq.a(p4, 1.0);
        com.a.b.d.ia[] v1_0 = new com.a.b.d.ia[v0_2];
        this.c = v1_0;
        com.a.b.d.ia[] v1_1 = new com.a.b.d.ia[v0_2];
        this.d = v1_1;
        this.e = (v0_2 - 1);
        this.f = 0;
        this.a = 0;
        return;
    }

    static synthetic void b(com.a.b.d.hy p0, com.a.b.d.ia p1)
    {
        p0.b(p1);
        return;
    }

    private void b(com.a.b.d.ia p3)
    {
        int v0_1 = (p3.a & this.e);
        p3.c = this.c[v0_1];
        this.c[v0_1] = p3;
        int v0_3 = (p3.b & this.e);
        p3.d = this.d[v0_3];
        this.d[v0_3] = p3;
        this.a = (this.a + 1);
        this.f = (this.f + 1);
        return;
    }

    static synthetic com.a.b.d.ia[] b(com.a.b.d.hy p1)
    {
        return p1.c;
    }

    private static synthetic int c(com.a.b.d.hy p1)
    {
        return p1.a;
    }

    private static com.a.b.d.ia[] c(int p1)
    {
        com.a.b.d.ia[] v0 = new com.a.b.d.ia[p1];
        return v0;
    }

    private void d()
    {
        int v0_0 = 0;
        com.a.b.d.ia[] v3 = this.c;
        if (com.a.b.d.iq.a(this.a, v3.length)) {
            com.a.b.d.ia v1_3 = (v3.length * 2);
            com.a.b.d.ia v2_1 = new com.a.b.d.ia[v1_3];
            this.c = v2_1;
            com.a.b.d.ia v2_2 = new com.a.b.d.ia[v1_3];
            this.d = v2_2;
            this.e = (v1_3 - 1);
            this.a = 0;
            while (v0_0 < v3.length) {
                com.a.b.d.ia v1_6 = v3[v0_0];
                while (v1_6 != null) {
                    com.a.b.d.ia v2_3 = v1_6.c;
                    this.b(v1_6);
                    v1_6 = v2_3;
                }
                v0_0++;
            }
            this.f = (this.f + 1);
        }
        return;
    }

    final com.a.b.d.ia a(Object p3, int p4)
    {
        com.a.b.d.ia v0_1 = this.d[(this.e & p4)];
        while (v0_1 != null) {
            if ((p4 != v0_1.b) || (!com.a.b.b.ce.a(p3, v0_1.f))) {
                v0_1 = v0_1.d;
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    public final Object a(Object p2, Object p3)
    {
        return this.a(p2, p3, 1);
    }

    final void a(com.a.b.d.ia p6)
    {
        int v0_0 = 0;
        int v3 = (p6.a & this.e);
        com.a.b.d.ia v1_2 = this.c[v3];
        int v2_1 = 0;
        while (v1_2 != p6) {
            v2_1 = v1_2;
            v1_2 = v1_2.c;
        }
        if (v2_1 != 0) {
            v2_1.c = p6.c;
        } else {
            this.c[v3] = p6.c;
        }
        int v2_5 = (this.e & p6.b);
        com.a.b.d.ia v1_7 = this.d[v2_5];
        while (v1_7 != p6) {
            com.a.b.d.ia v4_1 = v1_7;
            v1_7 = v1_7.d;
            v0_0 = v4_1;
        }
        if (v0_0 != 0) {
            v0_0.d = p6.d;
        } else {
            this.d[v2_5] = p6.d;
        }
        this.a = (this.a - 1);
        this.f = (this.f + 1);
        return;
    }

    public final com.a.b.d.bw b()
    {
        com.a.b.d.bw v0_1;
        if (this.g != null) {
            v0_1 = this.g;
        } else {
            v0_1 = new com.a.b.d.ie(this, 0);
            this.g = v0_1;
        }
        return v0_1;
    }

    public final void clear()
    {
        this.a = 0;
        java.util.Arrays.fill(this.c, 0);
        java.util.Arrays.fill(this.d, 0);
        this.f = (this.f + 1);
        return;
    }

    public final boolean containsKey(Object p2)
    {
        int v0_2;
        if (this.b(p2, com.a.b.d.hy.a(p2)) == null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean containsValue(Object p2)
    {
        int v0_2;
        if (this.a(p2, com.a.b.d.hy.a(p2)) == null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final java.util.Set entrySet()
    {
        return new com.a.b.d.ib(this, 0);
    }

    public final Object get(Object p2)
    {
        Object v0_2;
        Object v0_1 = this.b(p2, com.a.b.d.hy.a(p2));
        if (v0_1 != null) {
            v0_2 = v0_1.f;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final java.util.Set j_()
    {
        return this.b().keySet();
    }

    public final java.util.Set keySet()
    {
        return new com.a.b.d.im(this);
    }

    public final Object put(Object p2, Object p3)
    {
        return this.a(p2, p3, 0);
    }

    public final Object remove(Object p2)
    {
        Object v0_2;
        Object v0_1 = this.b(p2, com.a.b.d.hy.a(p2));
        if (v0_1 != null) {
            this.a(v0_1);
            v0_2 = v0_1.f;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final int size()
    {
        return this.a;
    }

    public final synthetic java.util.Collection values()
    {
        return this.j_();
    }
}
