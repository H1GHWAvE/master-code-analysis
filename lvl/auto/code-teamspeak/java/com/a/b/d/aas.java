package com.a.b.d;
final class aas extends com.a.b.d.agi {
    final com.a.b.d.jl a;
    int b;
    final synthetic com.a.b.d.aar c;

    aas(com.a.b.d.aar p2)
    {
        this.c = p2;
        this.a = com.a.b.d.aar.a(this.c).g().f();
        this.b = com.a.b.d.aar.b(this.c);
        return;
    }

    public final boolean hasNext()
    {
        int v0_1;
        if (this.b == 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final Object next()
    {
        Object v0_1 = Integer.numberOfTrailingZeros(this.b);
        if (v0_1 != 32) {
            this.b = (this.b & ((1 << v0_1) ^ -1));
            return this.a.get(v0_1);
        } else {
            throw new java.util.NoSuchElementException();
        }
    }
}
