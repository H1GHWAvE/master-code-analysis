package com.a.b.d;
 class abq extends com.a.b.d.xp implements java.util.SortedSet {
    final com.a.b.d.abn b;

    abq(com.a.b.d.abn p1)
    {
        this.b = p1;
        return;
    }

    private com.a.b.d.abn b()
    {
        return this.b;
    }

    final bridge synthetic com.a.b.d.xc a()
    {
        return this.b;
    }

    public java.util.Comparator comparator()
    {
        return this.b.comparator();
    }

    public Object first()
    {
        return com.a.b.d.abp.a(this.b.h());
    }

    public java.util.SortedSet headSet(Object p3)
    {
        return this.b.d(p3, com.a.b.d.ce.a).e_();
    }

    public Object last()
    {
        return com.a.b.d.abp.a(this.b.i());
    }

    public java.util.SortedSet subSet(Object p4, Object p5)
    {
        return this.b.a(p4, com.a.b.d.ce.b, p5, com.a.b.d.ce.a).e_();
    }

    public java.util.SortedSet tailSet(Object p3)
    {
        return this.b.c(p3, com.a.b.d.ce.b).e_();
    }
}
