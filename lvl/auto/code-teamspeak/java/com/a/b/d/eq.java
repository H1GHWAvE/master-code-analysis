package com.a.b.d;
final class eq extends com.a.b.d.ep implements java.io.Serializable {
    private static final com.a.b.d.eq a;
    private static final java.math.BigInteger b;
    private static final java.math.BigInteger c;
    private static final long d;

    static eq()
    {
        com.a.b.d.eq.a = new com.a.b.d.eq();
        com.a.b.d.eq.b = java.math.BigInteger.valueOf(-0.0);
        com.a.b.d.eq.c = java.math.BigInteger.valueOf(nan);
        return;
    }

    private eq()
    {
        return;
    }

    private static long a(java.math.BigInteger p2, java.math.BigInteger p3)
    {
        return p3.subtract(p2).max(com.a.b.d.eq.b).min(com.a.b.d.eq.c).longValue();
    }

    private static java.math.BigInteger a(java.math.BigInteger p1)
    {
        return p1.add(java.math.BigInteger.ONE);
    }

    private static java.math.BigInteger b(java.math.BigInteger p1)
    {
        return p1.subtract(java.math.BigInteger.ONE);
    }

    static synthetic com.a.b.d.eq c()
    {
        return com.a.b.d.eq.a;
    }

    private static Object d()
    {
        return com.a.b.d.eq.a;
    }

    public final synthetic long a(Comparable p3, Comparable p4)
    {
        return ((java.math.BigInteger) p4).subtract(((java.math.BigInteger) p3)).max(com.a.b.d.eq.b).min(com.a.b.d.eq.c).longValue();
    }

    public final synthetic Comparable a(Comparable p2)
    {
        return ((java.math.BigInteger) p2).add(java.math.BigInteger.ONE);
    }

    public final synthetic Comparable b(Comparable p2)
    {
        return ((java.math.BigInteger) p2).subtract(java.math.BigInteger.ONE);
    }

    public final String toString()
    {
        return "DiscreteDomain.bigIntegers()";
    }
}
