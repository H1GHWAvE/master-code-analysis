package com.a.b.d;
public abstract class ma extends com.a.b.d.md implements com.a.b.d.abn {
    private static final java.util.Comparator b;
    private static final com.a.b.d.ma c;
    transient com.a.b.d.ma a;

    static ma()
    {
        com.a.b.d.ma.b = com.a.b.d.yd.d();
        com.a.b.d.ma.c = new com.a.b.d.fb(com.a.b.d.ma.b);
        return;
    }

    ma()
    {
        return;
    }

    public static com.a.b.d.ma a(com.a.b.d.abn p2)
    {
        return com.a.b.d.ma.a(p2.comparator(), com.a.b.d.ov.a(p2.a()));
    }

    private static com.a.b.d.ma a(Comparable p6)
    {
        com.a.b.d.zq v1_1 = ((com.a.b.d.zq) com.a.b.d.me.a(p6));
        int[] v2 = new int[1];
        v2[0] = 1;
        long[] v3 = new long[2];
        v3 = {0, 0};
        return new com.a.b.d.zp(v1_1, v2, v3, 0, 1);
    }

    private static com.a.b.d.ma a(Comparable p3, Comparable p4)
    {
        com.a.b.d.ma v0_0 = com.a.b.d.yd.d();
        java.util.List v1_1 = new Comparable[2];
        v1_1[0] = p3;
        v1_1[1] = p4;
        return com.a.b.d.ma.a(v0_0, java.util.Arrays.asList(v1_1));
    }

    private static com.a.b.d.ma a(Comparable p3, Comparable p4, Comparable p5)
    {
        com.a.b.d.ma v0_0 = com.a.b.d.yd.d();
        java.util.List v1_1 = new Comparable[3];
        v1_1[0] = p3;
        v1_1[1] = p4;
        v1_1[2] = p5;
        return com.a.b.d.ma.a(v0_0, java.util.Arrays.asList(v1_1));
    }

    private static com.a.b.d.ma a(Comparable p3, Comparable p4, Comparable p5, Comparable p6)
    {
        com.a.b.d.ma v0_0 = com.a.b.d.yd.d();
        java.util.List v1_1 = new Comparable[4];
        v1_1[0] = p3;
        v1_1[1] = p4;
        v1_1[2] = p5;
        v1_1[3] = p6;
        return com.a.b.d.ma.a(v0_0, java.util.Arrays.asList(v1_1));
    }

    private static com.a.b.d.ma a(Comparable p3, Comparable p4, Comparable p5, Comparable p6, Comparable p7)
    {
        com.a.b.d.ma v0_0 = com.a.b.d.yd.d();
        java.util.List v1_1 = new Comparable[5];
        v1_1[0] = p3;
        v1_1[1] = p4;
        v1_1[2] = p5;
        v1_1[3] = p6;
        v1_1[4] = p7;
        return com.a.b.d.ma.a(v0_0, java.util.Arrays.asList(v1_1));
    }

    private static varargs com.a.b.d.ma a(Comparable p3, Comparable p4, Comparable p5, Comparable p6, Comparable p7, Comparable p8, Comparable[] p9)
    {
        com.a.b.d.ma v0_2 = com.a.b.d.ov.a((p9.length + 6));
        com.a.b.d.yd v1_1 = new Comparable[6];
        v1_1[0] = p3;
        v1_1[1] = p4;
        v1_1[2] = p5;
        v1_1[3] = p6;
        v1_1[4] = p7;
        v1_1[5] = p8;
        java.util.Collections.addAll(v0_2, v1_1);
        java.util.Collections.addAll(v0_2, p9);
        return com.a.b.d.ma.a(com.a.b.d.yd.d(), v0_2);
    }

    static com.a.b.d.ma a(java.util.Comparator p1)
    {
        com.a.b.d.fb v0_3;
        if (!com.a.b.d.ma.b.equals(p1)) {
            v0_3 = new com.a.b.d.fb(p1);
        } else {
            v0_3 = com.a.b.d.ma.c;
        }
        return v0_3;
    }

    private static com.a.b.d.ma a(java.util.Comparator p2, Iterable p3)
    {
        com.a.b.d.ma v0_2;
        if (!(p3 instanceof com.a.b.d.ma)) {
            boolean v1_2 = com.a.b.d.ov.a(p3);
            com.a.b.d.ma v0_5 = com.a.b.d.aer.a(((java.util.Comparator) com.a.b.b.cn.a(p2)));
            com.a.b.d.mq.a(v0_5, v1_2);
            v0_2 = com.a.b.d.ma.a(p2, v0_5.a());
        } else {
            v0_2 = ((com.a.b.d.ma) p3);
            if (!p2.equals(((com.a.b.d.ma) p3).comparator())) {
            } else {
                if (((com.a.b.d.ma) p3).h_()) {
                    v0_2 = com.a.b.d.ma.a(p2, ((com.a.b.d.ma) p3).o().f());
                }
            }
        }
        return v0_2;
    }

    private static com.a.b.d.ma a(java.util.Comparator p12, java.util.Collection p13)
    {
        com.a.b.d.zp v0_7;
        if (!p13.isEmpty()) {
            int v5_1 = new com.a.b.d.jn(p13.size());
            int[] v2 = new int[p13.size()];
            long[] v3 = new long[(p13.size() + 1)];
            java.util.Iterator v6 = p13.iterator();
            com.a.b.d.zq v1_0 = 0;
            while (v6.hasNext()) {
                com.a.b.d.zp v0_9 = ((com.a.b.d.xd) v6.next());
                v5_1.c(v0_9.a());
                v2[v1_0] = v0_9.b();
                v3[(v1_0 + 1)] = (v3[v1_0] + ((long) v2[v1_0]));
                v1_0++;
            }
            v0_7 = new com.a.b.d.zp(new com.a.b.d.zq(v5_1.b(), p12), v2, v3, 0, p13.size());
        } else {
            v0_7 = com.a.b.d.ma.a(p12);
        }
        return v0_7;
    }

    private static com.a.b.d.ma a(java.util.Comparator p1, java.util.Iterator p2)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.d.mb(p1).c(p2).c();
    }

    private static com.a.b.d.ma a(java.util.Iterator p2)
    {
        com.a.b.d.ma v0_0 = com.a.b.d.yd.d();
        com.a.b.b.cn.a(v0_0);
        return new com.a.b.d.mb(v0_0).c(p2).c();
    }

    private static com.a.b.d.ma a(Comparable[] p2)
    {
        return com.a.b.d.ma.a(com.a.b.d.yd.d(), java.util.Arrays.asList(p2));
    }

    private static com.a.b.d.ma b(Iterable p1)
    {
        return com.a.b.d.ma.a(com.a.b.d.yd.d(), p1);
    }

    private com.a.b.d.ma b(Object p6, com.a.b.d.ce p7, Object p8, com.a.b.d.ce p9)
    {
        com.a.b.d.ma v0_2;
        if (this.comparator().compare(p6, p8) > 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        Object[] v4_1 = new Object[2];
        v4_1[0] = p6;
        v4_1[1] = p8;
        com.a.b.b.cn.a(v0_2, "Expected lowerBound <= upperBound but %s > %s", v4_1);
        return this.b(p6, p7).a(p8, p9);
    }

    private static com.a.b.d.mb b(java.util.Comparator p1)
    {
        return new com.a.b.d.mb(p1);
    }

    private static com.a.b.d.ma p()
    {
        return com.a.b.d.ma.c;
    }

    private static com.a.b.d.mb q()
    {
        return new com.a.b.d.mb(com.a.b.d.yd.d().a());
    }

    private static com.a.b.d.mb r()
    {
        return new com.a.b.d.mb(com.a.b.d.yd.d());
    }

    public final synthetic com.a.b.d.abn a(Object p6, com.a.b.d.ce p7, Object p8, com.a.b.d.ce p9)
    {
        com.a.b.d.ma v0_2;
        if (this.comparator().compare(p6, p8) > 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        Object[] v4_1 = new Object[2];
        v4_1[0] = p6;
        v4_1[1] = p8;
        com.a.b.b.cn.a(v0_2, "Expected lowerBound <= upperBound but %s > %s", v4_1);
        return this.b(p6, p7).a(p8, p9);
    }

    public abstract com.a.b.d.ma a();

    public abstract com.a.b.d.ma b();

    public abstract com.a.b.d.me b();

    public synthetic com.a.b.d.abn c(Object p2, com.a.b.d.ce p3)
    {
        return this.b(p2, p3);
    }

    public final java.util.Comparator comparator()
    {
        return this.b().comparator();
    }

    public synthetic com.a.b.d.abn d(Object p2, com.a.b.d.ce p3)
    {
        return this.a(p2, p3);
    }

    public com.a.b.d.ma e()
    {
        com.a.b.d.el v0_0 = this.a;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.el(this);
            this.a = v0_0;
        }
        return v0_0;
    }

    public synthetic java.util.NavigableSet e_()
    {
        return this.b();
    }

    final Object g()
    {
        return new com.a.b.d.mc(this);
    }

    public final com.a.b.d.xd j()
    {
        throw new UnsupportedOperationException();
    }

    public final com.a.b.d.xd k()
    {
        throw new UnsupportedOperationException();
    }

    public synthetic com.a.b.d.abn m()
    {
        return this.e();
    }

    public synthetic java.util.SortedSet n()
    {
        return this.b();
    }

    public synthetic java.util.Set n_()
    {
        return this.b();
    }
}
