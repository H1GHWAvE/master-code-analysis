package com.a.b.d;
 class xa extends com.a.b.d.wy implements com.a.b.d.aac {
    private static final long g;

    xa(com.a.b.d.aac p1)
    {
        this(p1);
        return;
    }

    public com.a.b.d.aac a()
    {
        return ((com.a.b.d.aac) super.c());
    }

    public java.util.Set a(Object p2)
    {
        return java.util.Collections.unmodifiableSet(this.a().a(p2));
    }

    public java.util.Set a(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        return this.a(p2, p3);
    }

    public java.util.Set b(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public synthetic com.a.b.d.vi c()
    {
        return this.a();
    }

    public synthetic java.util.Collection c(Object p2)
    {
        return this.a(p2);
    }

    public synthetic java.util.Collection d(Object p2)
    {
        return this.b(p2);
    }

    public final synthetic java.util.Collection k()
    {
        return this.u();
    }

    public synthetic Object k_()
    {
        return this.a();
    }

    public final java.util.Set u()
    {
        return com.a.b.d.sz.a(this.a().u());
    }
}
