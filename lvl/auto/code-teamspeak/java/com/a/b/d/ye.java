package com.a.b.d;
final class ye extends com.a.b.d.yd {
    private java.util.Map a;

    ye()
    {
        this.a = new com.a.b.d.ql().a(com.a.b.d.sh.c).a(new com.a.b.d.yf(this));
        return;
    }

    private static int a(Object p1)
    {
        return System.identityHashCode(p1);
    }

    public final int compare(Object p5, Object p6)
    {
        AssertionError v0_0 = -1;
        if (p5 != p6) {
            if (p5 != null) {
                if (p6 != null) {
                    int v2 = System.identityHashCode(p5);
                    int v3 = System.identityHashCode(p6);
                    if (v2 == v3) {
                        v0_0 = ((Integer) this.a.get(p5)).compareTo(((Integer) this.a.get(p6)));
                        if (v0_0 == null) {
                            throw new AssertionError();
                        }
                    } else {
                        if (v2 >= v3) {
                            v0_0 = 1;
                        }
                    }
                } else {
                    v0_0 = 1;
                }
            }
        } else {
            v0_0 = 0;
        }
        return v0_0;
    }

    public final String toString()
    {
        return "Ordering.arbitrary()";
    }
}
