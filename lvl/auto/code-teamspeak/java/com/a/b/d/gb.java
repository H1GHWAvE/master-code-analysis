package com.a.b.d;
final class gb extends java.util.AbstractCollection {
    private final com.a.b.d.ga a;

    gb(com.a.b.d.ga p2)
    {
        this.a = ((com.a.b.d.ga) com.a.b.b.cn.a(p2));
        return;
    }

    public final void clear()
    {
        this.a.g();
        return;
    }

    public final boolean contains(Object p2)
    {
        return this.a.g(p2);
    }

    public final java.util.Iterator iterator()
    {
        return com.a.b.d.sz.b(this.a.k().iterator());
    }

    public final boolean remove(Object p5)
    {
        com.a.b.b.co v1 = this.a.c();
        java.util.Iterator v2 = this.a.a().k().iterator();
        while (v2.hasNext()) {
            int v0_7 = ((java.util.Map$Entry) v2.next());
            if ((v1.a(v0_7)) && (com.a.b.b.ce.a(v0_7.getValue(), p5))) {
                v2.remove();
                int v0_5 = 1;
            }
            return v0_5;
        }
        v0_5 = 0;
        return v0_5;
    }

    public final boolean removeAll(java.util.Collection p4)
    {
        return com.a.b.d.mq.a(this.a.a().k(), com.a.b.b.cp.a(this.a.c(), com.a.b.d.sz.b(com.a.b.b.cp.a(p4))));
    }

    public final boolean retainAll(java.util.Collection p4)
    {
        return com.a.b.d.mq.a(this.a.a().k(), com.a.b.b.cp.a(this.a.c(), com.a.b.d.sz.b(com.a.b.b.cp.a(com.a.b.b.cp.a(p4)))));
    }

    public final int size()
    {
        return this.a.f();
    }
}
