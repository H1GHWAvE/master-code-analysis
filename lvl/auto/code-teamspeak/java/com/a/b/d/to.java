package com.a.b.d;
final class to extends com.a.b.b.ak implements java.io.Serializable {
    private static final long b;
    private final com.a.b.d.bw a;

    to(com.a.b.d.bw p2)
    {
        this.a = ((com.a.b.d.bw) com.a.b.b.cn.a(p2));
        return;
    }

    private static Object a(com.a.b.d.bw p5, Object p6)
    {
        int v0;
        Object v3 = p5.get(p6);
        if (v3 == null) {
            v0 = 0;
        } else {
            v0 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = p6;
        com.a.b.b.cn.a(v0, "No non-null mapping present for input: %s", v1_1);
        return v3;
    }

    protected final Object a(Object p2)
    {
        return com.a.b.d.to.a(this.a.b(), p2);
    }

    protected final Object b(Object p2)
    {
        return com.a.b.d.to.a(this.a, p2);
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.d.to)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.d.to) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 18)).append("Maps.asConverter(").append(v0_2).append(")").toString();
    }
}
