package com.a.b.d;
public abstract class gq extends com.a.b.d.go implements java.util.ListIterator {

    protected gq()
    {
        return;
    }

    protected synthetic java.util.Iterator a()
    {
        return this.b();
    }

    public void add(Object p2)
    {
        this.b().add(p2);
        return;
    }

    protected abstract java.util.ListIterator b();

    public boolean hasPrevious()
    {
        return this.b().hasPrevious();
    }

    protected synthetic Object k_()
    {
        return this.b();
    }

    public int nextIndex()
    {
        return this.b().nextIndex();
    }

    public Object previous()
    {
        return this.b().previous();
    }

    public int previousIndex()
    {
        return this.b().previousIndex();
    }

    public void set(Object p2)
    {
        this.b().set(p2);
        return;
    }
}
