package com.a.b.d;
public abstract class me extends com.a.b.d.mh implements com.a.b.d.aay, java.util.NavigableSet {
    private static final java.util.Comparator a;
    static final com.a.b.d.me c;
    final transient java.util.Comparator d;
    transient com.a.b.d.me e;

    static me()
    {
        com.a.b.d.me.a = com.a.b.d.yd.d();
        com.a.b.d.me.c = new com.a.b.d.fc(com.a.b.d.me.a);
        return;
    }

    me(java.util.Comparator p1)
    {
        this.d = p1;
        return;
    }

    private static int a(java.util.Comparator p1, Object p2, Object p3)
    {
        return p1.compare(p2, p3);
    }

    public static com.a.b.d.me a(Comparable p3)
    {
        return new com.a.b.d.zq(com.a.b.d.jl.a(p3), com.a.b.d.yd.d());
    }

    private static com.a.b.d.me a(Comparable p4, Comparable p5)
    {
        com.a.b.d.me v0_0 = com.a.b.d.yd.d();
        Comparable[] v1 = new Comparable[2];
        v1[0] = p4;
        v1[1] = p5;
        return com.a.b.d.me.a(v0_0, 2, v1);
    }

    private static com.a.b.d.me a(Comparable p4, Comparable p5, Comparable p6)
    {
        com.a.b.d.me v0_0 = com.a.b.d.yd.d();
        Comparable[] v1 = new Comparable[3];
        v1[0] = p4;
        v1[1] = p5;
        v1[2] = p6;
        return com.a.b.d.me.a(v0_0, 3, v1);
    }

    private static com.a.b.d.me a(Comparable p4, Comparable p5, Comparable p6, Comparable p7)
    {
        com.a.b.d.me v0_0 = com.a.b.d.yd.d();
        Comparable[] v1 = new Comparable[4];
        v1[0] = p4;
        v1[1] = p5;
        v1[2] = p6;
        v1[3] = p7;
        return com.a.b.d.me.a(v0_0, 4, v1);
    }

    private static com.a.b.d.me a(Comparable p4, Comparable p5, Comparable p6, Comparable p7, Comparable p8)
    {
        com.a.b.d.me v0_0 = com.a.b.d.yd.d();
        Comparable[] v1 = new Comparable[5];
        v1[0] = p4;
        v1[1] = p5;
        v1[2] = p6;
        v1[3] = p7;
        v1[4] = p8;
        return com.a.b.d.me.a(v0_0, 5, v1);
    }

    private static varargs com.a.b.d.me a(Comparable p4, Comparable p5, Comparable p6, Comparable p7, Comparable p8, Comparable p9, Comparable[] p10)
    {
        com.a.b.d.me v0_2 = new Comparable[(p10.length + 6)];
        v0_2[0] = p4;
        v0_2[1] = p5;
        v0_2[2] = p6;
        v0_2[3] = p7;
        v0_2[4] = p8;
        v0_2[5] = p9;
        System.arraycopy(p10, 0, v0_2, 6, p10.length);
        return com.a.b.d.me.a(com.a.b.d.yd.d(), v0_2.length, ((Comparable[]) v0_2));
    }

    static com.a.b.d.me a(java.util.Comparator p1)
    {
        com.a.b.d.fc v0_3;
        if (!com.a.b.d.me.a.equals(p1)) {
            v0_3 = new com.a.b.d.fc(p1);
        } else {
            v0_3 = com.a.b.d.me.c;
        }
        return v0_3;
    }

    static varargs com.a.b.d.me a(java.util.Comparator p4, int p5, Object[] p6)
    {
        com.a.b.d.jl v0_3;
        if (p5 != 0) {
            com.a.b.d.yc.c(p6, p5);
            java.util.Arrays.sort(p6, 0, p5, p4);
            int v2 = 1;
            com.a.b.d.jl v1_1 = 1;
            while (v2 < p5) {
                com.a.b.d.jl v0_7;
                Object v3 = p6[v2];
                if (p4.compare(v3, p6[(v1_1 - 1)]) == 0) {
                    v0_7 = v1_1;
                } else {
                    v0_7 = (v1_1 + 1);
                    p6[v1_1] = v3;
                }
                v2++;
                v1_1 = v0_7;
            }
            java.util.Arrays.fill(p6, v1_1, p5, 0);
            v0_3 = new com.a.b.d.zq(com.a.b.d.jl.b(p6, v1_1), p4);
        } else {
            v0_3 = com.a.b.d.me.a(p4);
        }
        return v0_3;
    }

    public static com.a.b.d.me a(java.util.Comparator p2, Iterable p3)
    {
        com.a.b.d.me v0_3;
        com.a.b.b.cn.a(p2);
        if ((!com.a.b.d.aaz.a(p2, p3)) || (!(p3 instanceof com.a.b.d.me))) {
            com.a.b.d.me v0_5 = ((Object[]) com.a.b.d.mq.c(p3));
            v0_3 = com.a.b.d.me.a(p2, v0_5.length, v0_5);
        } else {
            v0_3 = ((com.a.b.d.me) p3);
            if (((com.a.b.d.me) p3).h_()) {
            }
        }
        return v0_3;
    }

    public static com.a.b.d.me a(java.util.Comparator p1, java.util.Collection p2)
    {
        return com.a.b.d.me.a(p1, p2);
    }

    private static com.a.b.d.me a(java.util.Comparator p1, java.util.Iterator p2)
    {
        return new com.a.b.d.mf(p1).c(p2).c();
    }

    private static com.a.b.d.me a(java.util.Iterator p2)
    {
        return new com.a.b.d.mf(com.a.b.d.yd.d()).c(p2).c();
    }

    private static com.a.b.d.me a(java.util.SortedSet p3)
    {
        com.a.b.d.zq v0_2;
        java.util.Comparator v1 = com.a.b.d.aaz.a(p3);
        com.a.b.d.jl v2 = com.a.b.d.jl.a(p3);
        if (!v2.isEmpty()) {
            v0_2 = new com.a.b.d.zq(v2, v1);
        } else {
            v0_2 = com.a.b.d.me.a(v1);
        }
        return v0_2;
    }

    private static com.a.b.d.me a(Comparable[] p3)
    {
        return com.a.b.d.me.a(com.a.b.d.yd.d(), p3.length, ((Object[]) p3.clone()));
    }

    private static com.a.b.d.me b(Iterable p1)
    {
        return com.a.b.d.me.a(com.a.b.d.yd.d(), p1);
    }

    private static com.a.b.d.me b(java.util.Collection p1)
    {
        return com.a.b.d.me.a(com.a.b.d.yd.d(), p1);
    }

    private static com.a.b.d.mf b(java.util.Comparator p1)
    {
        return new com.a.b.d.mf(p1);
    }

    public static com.a.b.d.me j()
    {
        return com.a.b.d.me.c;
    }

    private static com.a.b.d.me k()
    {
        return com.a.b.d.me.c;
    }

    private static com.a.b.d.mf m()
    {
        return new com.a.b.d.mf(com.a.b.d.yd.d().a());
    }

    private static com.a.b.d.mf n()
    {
        return new com.a.b.d.mf(com.a.b.d.yd.d());
    }

    private static void o()
    {
        throw new java.io.InvalidObjectException("Use SerializedForm");
    }

    public com.a.b.d.me a(Object p2)
    {
        return this.c(p2, 1);
    }

    public com.a.b.d.me a(Object p3, Object p4)
    {
        return this.b(p3, 1, p4, 0);
    }

    abstract com.a.b.d.me a();

    abstract com.a.b.d.me a();

    public com.a.b.d.me b()
    {
        com.a.b.d.me v0 = this.e;
        if (v0 == null) {
            v0 = this.e();
            this.e = v0;
            v0.e = this;
        }
        return v0;
    }

    public com.a.b.d.me b(Object p2)
    {
        return this.d(p2, 0);
    }

    abstract com.a.b.d.me b();

    public com.a.b.d.me b(Object p2, boolean p3, Object p4, boolean p5)
    {
        com.a.b.d.me v0_2;
        com.a.b.b.cn.a(p2);
        com.a.b.b.cn.a(p4);
        if (this.d.compare(p2, p4) > 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.a.b.b.cn.a(v0_2);
        return this.a(p2, p3, p4, p5);
    }

    abstract int c();

    final int c(Object p2, Object p3)
    {
        return this.d.compare(p2, p3);
    }

    public abstract com.a.b.d.agi c();

    public com.a.b.d.me c(Object p2, boolean p3)
    {
        return this.a(com.a.b.b.cn.a(p2), p3);
    }

    public Object ceiling(Object p2)
    {
        return com.a.b.d.mq.f(this.c(p2, 1));
    }

    public java.util.Comparator comparator()
    {
        return this.d;
    }

    public abstract com.a.b.d.agi d();

    public com.a.b.d.me d(Object p2, boolean p3)
    {
        return this.b(com.a.b.b.cn.a(p2), p3);
    }

    public synthetic java.util.Iterator descendingIterator()
    {
        return this.d();
    }

    public synthetic java.util.NavigableSet descendingSet()
    {
        return this.b();
    }

    com.a.b.d.me e()
    {
        return new com.a.b.d.em(this);
    }

    public Object first()
    {
        return this.c().next();
    }

    public Object floor(Object p3)
    {
        return com.a.b.d.nj.d(this.d(p3, 1).d(), 0);
    }

    Object g()
    {
        return new com.a.b.d.mg(this.d, this.toArray());
    }

    public synthetic java.util.NavigableSet headSet(Object p2, boolean p3)
    {
        return this.d(p2, p3);
    }

    public synthetic java.util.SortedSet headSet(Object p2)
    {
        return this.b(p2);
    }

    public Object higher(Object p2)
    {
        return com.a.b.d.mq.f(this.c(p2, 0));
    }

    public synthetic java.util.Iterator iterator()
    {
        return this.c();
    }

    public Object last()
    {
        return this.d().next();
    }

    public Object lower(Object p3)
    {
        return com.a.b.d.nj.d(this.d(p3, 0).d(), 0);
    }

    public final Object pollFirst()
    {
        throw new UnsupportedOperationException();
    }

    public final Object pollLast()
    {
        throw new UnsupportedOperationException();
    }

    public synthetic java.util.NavigableSet subSet(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.b(p2, p3, p4, p5);
    }

    public synthetic java.util.SortedSet subSet(Object p2, Object p3)
    {
        return this.a(p2, p3);
    }

    public synthetic java.util.NavigableSet tailSet(Object p2, boolean p3)
    {
        return this.c(p2, p3);
    }

    public synthetic java.util.SortedSet tailSet(Object p2)
    {
        return this.a(p2);
    }
}
