package com.a.b.d;
 class z extends com.a.b.d.q implements java.util.SortedMap {
    java.util.SortedSet d;
    final synthetic com.a.b.d.n e;

    z(com.a.b.d.n p1, java.util.SortedMap p2)
    {
        this.e = p1;
        this(p1, p2);
        return;
    }

    java.util.SortedSet b()
    {
        return new com.a.b.d.aa(this.e, this.d());
    }

    public java.util.SortedSet c()
    {
        java.util.SortedSet v0 = this.d;
        if (v0 == null) {
            v0 = this.b();
            this.d = v0;
        }
        return v0;
    }

    public java.util.Comparator comparator()
    {
        return this.d().comparator();
    }

    java.util.SortedMap d()
    {
        return ((java.util.SortedMap) this.a);
    }

    synthetic java.util.Set e()
    {
        return this.b();
    }

    public Object firstKey()
    {
        return this.d().firstKey();
    }

    public java.util.SortedMap headMap(Object p4)
    {
        return new com.a.b.d.z(this.e, this.d().headMap(p4));
    }

    public synthetic java.util.Set keySet()
    {
        return this.c();
    }

    public Object lastKey()
    {
        return this.d().lastKey();
    }

    public java.util.SortedMap subMap(Object p4, Object p5)
    {
        return new com.a.b.d.z(this.e, this.d().subMap(p4, p5));
    }

    public java.util.SortedMap tailMap(Object p4)
    {
        return new com.a.b.d.z(this.e, this.d().tailMap(p4));
    }
}
