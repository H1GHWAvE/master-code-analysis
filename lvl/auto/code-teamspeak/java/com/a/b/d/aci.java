package com.a.b.d;
final class aci extends com.a.b.d.uj {
    final synthetic com.a.b.d.abx a;

    private aci(com.a.b.d.abx p1)
    {
        this.a = p1;
        return;
    }

    synthetic aci(com.a.b.d.abx p1, byte p2)
    {
        this(p1);
        return;
    }

    private java.util.Map b(Object p2)
    {
        int v0_2;
        if (!this.a.b(p2)) {
            v0_2 = 0;
        } else {
            v0_2 = com.a.b.d.abx.a(this.a, p2);
        }
        return v0_2;
    }

    public final java.util.Map a(Object p2)
    {
        int v0_2;
        if (!this.a.b(p2)) {
            v0_2 = 0;
        } else {
            v0_2 = this.a.d(p2);
        }
        return v0_2;
    }

    public final java.util.Set a()
    {
        return new com.a.b.d.acj(this);
    }

    final java.util.Collection c_()
    {
        return new com.a.b.d.acl(this);
    }

    public final boolean containsKey(Object p2)
    {
        return this.a.b(p2);
    }

    public final synthetic Object get(Object p2)
    {
        return this.a(p2);
    }

    public final java.util.Set keySet()
    {
        return this.a.b();
    }

    public final synthetic Object remove(Object p2)
    {
        int v0_2;
        if (!this.a.b(p2)) {
            v0_2 = 0;
        } else {
            v0_2 = com.a.b.d.abx.a(this.a, p2);
        }
        return v0_2;
    }
}
