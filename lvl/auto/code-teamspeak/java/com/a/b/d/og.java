package com.a.b.d;
final class og implements java.util.Iterator {
    com.a.b.d.oh a;
    com.a.b.d.oe b;
    int c;
    final synthetic com.a.b.d.of d;

    og(com.a.b.d.of p2)
    {
        this.d = p2;
        this.a = com.a.b.d.of.a(this.d);
        this.c = com.a.b.d.of.b(this.d);
        return;
    }

    private void a()
    {
        if (com.a.b.d.of.b(this.d) == this.c) {
            return;
        } else {
            throw new java.util.ConcurrentModificationException();
        }
    }

    public final boolean hasNext()
    {
        int v0_1;
        this.a();
        if (this.a == this.d) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final Object next()
    {
        if (this.hasNext()) {
            com.a.b.d.oh v0_2 = ((com.a.b.d.oe) this.a);
            Object v1 = v0_2.getValue();
            this.b = v0_2;
            this.a = v0_2.d;
            return v1;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final void remove()
    {
        int v0_1;
        this.a();
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        this.d.remove(this.b.getValue());
        this.c = com.a.b.d.of.b(this.d);
        this.b = 0;
        return;
    }
}
