package com.a.b.d;
 class aaj extends com.a.b.d.he {
    private final java.util.NavigableSet a;

    aaj(java.util.NavigableSet p1)
    {
        this.a = p1;
        return;
    }

    private static com.a.b.d.yd a(java.util.Comparator p1)
    {
        return com.a.b.d.yd.a(p1).a();
    }

    protected final bridge synthetic java.util.Set a()
    {
        return this.a;
    }

    protected final bridge synthetic java.util.Collection b()
    {
        return this.a;
    }

    protected final bridge synthetic java.util.SortedSet c()
    {
        return this.a;
    }

    public Object ceiling(Object p2)
    {
        return this.a.floor(p2);
    }

    public java.util.Comparator comparator()
    {
        com.a.b.d.yd v0_3;
        com.a.b.d.yd v0_1 = this.a.comparator();
        if (v0_1 != null) {
            v0_3 = com.a.b.d.yd.a(v0_1).a();
        } else {
            v0_3 = com.a.b.d.yd.d().a();
        }
        return v0_3;
    }

    protected final java.util.NavigableSet d()
    {
        return this.a;
    }

    public java.util.Iterator descendingIterator()
    {
        return this.a.iterator();
    }

    public java.util.NavigableSet descendingSet()
    {
        return this.a;
    }

    public Object first()
    {
        return this.a.last();
    }

    public Object floor(Object p2)
    {
        return this.a.ceiling(p2);
    }

    public java.util.NavigableSet headSet(Object p2, boolean p3)
    {
        return this.a.tailSet(p2, p3).descendingSet();
    }

    public java.util.SortedSet headSet(Object p2)
    {
        return this.headSet(p2, 0);
    }

    public Object higher(Object p2)
    {
        return this.a.lower(p2);
    }

    public java.util.Iterator iterator()
    {
        return this.a.descendingIterator();
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public Object last()
    {
        return this.a.first();
    }

    public Object lower(Object p2)
    {
        return this.a.higher(p2);
    }

    public Object pollFirst()
    {
        return this.a.pollLast();
    }

    public Object pollLast()
    {
        return this.a.pollFirst();
    }

    public java.util.NavigableSet subSet(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.a.subSet(p4, p5, p2, p3).descendingSet();
    }

    public java.util.SortedSet subSet(Object p3, Object p4)
    {
        return this.subSet(p3, 1, p4, 0);
    }

    public java.util.NavigableSet tailSet(Object p2, boolean p3)
    {
        return this.a.headSet(p2, p3).descendingSet();
    }

    public java.util.SortedSet tailSet(Object p2)
    {
        return this.tailSet(p2, 1);
    }

    public Object[] toArray()
    {
        return this.p();
    }

    public Object[] toArray(Object[] p2)
    {
        return com.a.b.d.yc.a(this, p2);
    }

    public String toString()
    {
        return this.o();
    }
}
