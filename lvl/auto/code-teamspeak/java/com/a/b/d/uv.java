package com.a.b.d;
final class uv extends com.a.b.d.gs implements com.a.b.d.bw, java.io.Serializable {
    private static final long e;
    final java.util.Map a;
    final com.a.b.d.bw b;
    com.a.b.d.bw c;
    transient java.util.Set d;

    uv(com.a.b.d.bw p2, com.a.b.d.bw p3)
    {
        this.a = java.util.Collections.unmodifiableMap(p2);
        this.b = p2;
        this.c = p3;
        return;
    }

    public final Object a(Object p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    protected final java.util.Map a()
    {
        return this.a;
    }

    public final com.a.b.d.bw b()
    {
        com.a.b.d.uv v0_0 = this.c;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.uv(this.b.b(), this);
            this.c = v0_0;
        }
        return v0_0;
    }

    public final java.util.Set j_()
    {
        java.util.Set v0_0 = this.d;
        if (v0_0 == null) {
            v0_0 = java.util.Collections.unmodifiableSet(this.b.j_());
            this.d = v0_0;
        }
        return v0_0;
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public final synthetic java.util.Collection values()
    {
        return this.j_();
    }
}
