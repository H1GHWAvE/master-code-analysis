package com.a.b.d;
final class aar extends java.util.AbstractSet {
    private final com.a.b.d.jt a;
    private final int b;

    aar(com.a.b.d.jt p1, int p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    static synthetic com.a.b.d.jt a(com.a.b.d.aar p1)
    {
        return p1.a;
    }

    static synthetic int b(com.a.b.d.aar p1)
    {
        return p1.b;
    }

    public final boolean contains(Object p4)
    {
        int v0_6;
        int v0_2 = ((Integer) this.a.get(p4));
        if ((v0_2 == 0) || (((1 << v0_2.intValue()) & this.b) == 0)) {
            v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.aas(this);
    }

    public final int size()
    {
        return Integer.bitCount(this.b);
    }
}
