package com.a.b.d;
final class aaf extends com.a.b.d.aaq {
    final synthetic java.util.Set a;
    final synthetic com.a.b.b.co b;
    final synthetic java.util.Set c;

    aaf(java.util.Set p2, com.a.b.b.co p3, java.util.Set p4)
    {
        this.a = p2;
        this.b = p3;
        this.c = p4;
        this(0);
        return;
    }

    public final boolean contains(Object p2)
    {
        if ((!this.a.contains(p2)) || (!this.c.contains(p2))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final boolean containsAll(java.util.Collection p2)
    {
        if ((!this.a.containsAll(p2)) || (!this.c.containsAll(p2))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final boolean isEmpty()
    {
        int v0_2;
        if (this.iterator().hasNext()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final java.util.Iterator iterator()
    {
        return com.a.b.d.nj.b(this.a.iterator(), this.b);
    }

    public final int size()
    {
        return com.a.b.d.nj.b(this.iterator());
    }
}
