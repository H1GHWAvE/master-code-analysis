package com.a.b.d;
final class jv extends com.a.b.d.jt {
    private final com.a.b.d.jt a;

    jv(com.a.b.d.jt p2)
    {
        this.a = ((com.a.b.d.jt) com.a.b.b.cn.a(p2));
        return;
    }

    static synthetic com.a.b.d.jt a(com.a.b.d.jv p1)
    {
        return p1.a;
    }

    private com.a.b.d.lo a(Object p2)
    {
        com.a.b.d.lo v0_2;
        com.a.b.d.lo v0_1 = this.a.get(p2);
        if (v0_1 != null) {
            v0_2 = com.a.b.d.lo.d(v0_1);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final boolean containsKey(Object p2)
    {
        return this.a.containsKey(p2);
    }

    final com.a.b.d.lo d()
    {
        return new com.a.b.d.jw(this);
    }

    public final synthetic java.util.Set entrySet()
    {
        return super.e();
    }

    public final synthetic Object get(Object p2)
    {
        com.a.b.d.lo v0_2;
        com.a.b.d.lo v0_1 = this.a.get(p2);
        if (v0_1 != null) {
            v0_2 = com.a.b.d.lo.d(v0_1);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    final boolean i_()
    {
        return 0;
    }

    public final synthetic java.util.Set keySet()
    {
        return super.g();
    }

    public final int size()
    {
        return this.a.size();
    }

    public final synthetic java.util.Collection values()
    {
        return super.h();
    }
}
