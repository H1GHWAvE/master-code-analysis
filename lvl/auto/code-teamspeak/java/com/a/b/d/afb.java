package com.a.b.d;
public final class afb implements com.a.b.d.yq {
    private static final com.a.b.d.yq b;
    private final java.util.NavigableMap a;

    static afb()
    {
        com.a.b.d.afb.b = new com.a.b.d.afc();
        return;
    }

    private afb()
    {
        this.a = com.a.b.d.sz.e();
        return;
    }

    static synthetic java.util.NavigableMap a(com.a.b.d.afb p1)
    {
        return p1.a;
    }

    private void a(com.a.b.d.dw p3, com.a.b.d.dw p4, Object p5)
    {
        this.a.put(p3, new com.a.b.d.aff(p3, p4, p5));
        return;
    }

    public static com.a.b.d.afb c()
    {
        return new com.a.b.d.afb();
    }

    static synthetic com.a.b.d.yq e()
    {
        return com.a.b.d.afb.b;
    }

    private static com.a.b.d.yq f()
    {
        return com.a.b.d.afb.b;
    }

    public final com.a.b.d.yl a()
    {
        com.a.b.d.yl v0_1 = this.a.firstEntry();
        java.util.Map$Entry v1_1 = this.a.lastEntry();
        if (v0_1 != null) {
            return com.a.b.d.yl.a(((com.a.b.d.aff) v0_1.getValue()).a.b, ((com.a.b.d.aff) v1_1.getValue()).a.c);
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final Object a(Comparable p2)
    {
        Object v0_1;
        Object v0_0 = this.b(p2);
        if (v0_0 != null) {
            v0_1 = v0_0.getValue();
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public final void a(com.a.b.d.yl p6)
    {
        if (!p6.f()) {
            com.a.b.d.dw v2_0 = this.a.lowerEntry(p6.b);
            if (v2_0 != null) {
                java.util.NavigableMap v0_3 = ((com.a.b.d.aff) v2_0.getValue());
                if (v0_3.a.c.a(p6.b) > 0) {
                    if (v0_3.a.c.a(p6.c) > 0) {
                        this.a(p6.c, v0_3.a.c, ((com.a.b.d.aff) v2_0.getValue()).getValue());
                    }
                    this.a(v0_3.a.b, p6.b, ((com.a.b.d.aff) v2_0.getValue()).getValue());
                }
            }
            com.a.b.d.dw v1_13 = this.a.lowerEntry(p6.c);
            if (v1_13 != null) {
                java.util.NavigableMap v0_10 = ((com.a.b.d.aff) v1_13.getValue());
                if (v0_10.a.c.a(p6.c) > 0) {
                    this.a(p6.c, v0_10.a.c, ((com.a.b.d.aff) v1_13.getValue()).getValue());
                    this.a.remove(p6.b);
                }
            }
            this.a.subMap(p6.b, p6.c).clear();
        }
        return;
    }

    public final void a(com.a.b.d.yl p4, Object p5)
    {
        if (!p4.f()) {
            com.a.b.b.cn.a(p5);
            this.a(p4);
            this.a.put(p4.b, new com.a.b.d.aff(p4, p5));
        }
        return;
    }

    public final void a(com.a.b.d.yq p4)
    {
        java.util.Iterator v2 = p4.d().entrySet().iterator();
        while (v2.hasNext()) {
            Object v0_4 = ((java.util.Map$Entry) v2.next());
            this.a(((com.a.b.d.yl) v0_4.getKey()), v0_4.getValue());
        }
        return;
    }

    public final java.util.Map$Entry b(Comparable p3)
    {
        java.util.Map$Entry v0_5;
        java.util.Map$Entry v1_1 = this.a.floorEntry(com.a.b.d.dw.b(p3));
        if ((v1_1 == null) || (!((com.a.b.d.aff) v1_1.getValue()).a.c(p3))) {
            v0_5 = 0;
        } else {
            v0_5 = ((java.util.Map$Entry) v1_1.getValue());
        }
        return v0_5;
    }

    public final void b()
    {
        this.a.clear();
        return;
    }

    public final com.a.b.d.yq c(com.a.b.d.yl p2)
    {
        if (!p2.equals(com.a.b.d.yl.c())) {
            this = new com.a.b.d.afg(this, p2);
        }
        return this;
    }

    public final java.util.Map d()
    {
        return new com.a.b.d.afd(this, 0);
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.d.yq)) {
            v0_1 = 0;
        } else {
            v0_1 = this.d().equals(((com.a.b.d.yq) p3).d());
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.d().hashCode();
    }

    public final String toString()
    {
        return this.a.values().toString();
    }
}
