package com.a.b.d;
final class ug extends com.a.b.d.uc implements java.util.SortedSet {
    final synthetic com.a.b.d.uf b;

    ug(com.a.b.d.uf p1)
    {
        this.b = p1;
        this(p1);
        return;
    }

    public final java.util.Comparator comparator()
    {
        return ((java.util.SortedMap) this.b.a).comparator();
    }

    public final Object first()
    {
        return this.b.firstKey();
    }

    public final java.util.SortedSet headSet(Object p2)
    {
        return ((java.util.SortedSet) this.b.headMap(p2).keySet());
    }

    public final Object last()
    {
        return this.b.lastKey();
    }

    public final java.util.SortedSet subSet(Object p2, Object p3)
    {
        return ((java.util.SortedSet) this.b.subMap(p2, p3).keySet());
    }

    public final java.util.SortedSet tailSet(Object p2)
    {
        return ((java.util.SortedSet) this.b.tailMap(p2).keySet());
    }
}
