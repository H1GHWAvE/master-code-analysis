package com.a.b.d;
public final class lf extends com.a.b.d.ay implements java.io.Serializable {
    private static final com.a.b.d.lf a;
    private static final com.a.b.d.lf b;
    private final transient com.a.b.d.jl c;
    private transient com.a.b.d.lf d;

    static lf()
    {
        com.a.b.d.lf.a = new com.a.b.d.lf(com.a.b.d.jl.d());
        com.a.b.d.lf.b = new com.a.b.d.lf(com.a.b.d.jl.a(com.a.b.d.yl.c()));
        return;
    }

    lf(com.a.b.d.jl p1)
    {
        this.c = p1;
        return;
    }

    private lf(com.a.b.d.jl p1, com.a.b.d.lf p2)
    {
        this.c = p1;
        this.d = p2;
        return;
    }

    static synthetic com.a.b.d.jl a(com.a.b.d.lf p1)
    {
        return p1.c;
    }

    public static com.a.b.d.lf c()
    {
        return com.a.b.d.lf.a;
    }

    static com.a.b.d.lf d()
    {
        return com.a.b.d.lf.b;
    }

    public static com.a.b.d.lf d(com.a.b.d.yr p2)
    {
        com.a.b.d.lf v0_5;
        com.a.b.b.cn.a(p2);
        if (!p2.a()) {
            if (!p2.c(com.a.b.d.yl.c())) {
                if ((p2 instanceof com.a.b.d.lf)) {
                    v0_5 = ((com.a.b.d.lf) p2);
                    if (!((com.a.b.d.lf) p2).c.h_()) {
                        return v0_5;
                    }
                }
                v0_5 = new com.a.b.d.lf(com.a.b.d.jl.a(p2.g()));
            } else {
                v0_5 = com.a.b.d.lf.b;
            }
        } else {
            v0_5 = com.a.b.d.lf.a;
        }
        return v0_5;
    }

    private static com.a.b.d.lf f(com.a.b.d.yl p2)
    {
        com.a.b.d.lf v0_4;
        com.a.b.b.cn.a(p2);
        if (!p2.f()) {
            if (!p2.equals(com.a.b.d.yl.c())) {
                v0_4 = new com.a.b.d.lf(com.a.b.d.jl.a(p2));
            } else {
                v0_4 = com.a.b.d.lf.b;
            }
        } else {
            v0_4 = com.a.b.d.lf.a;
        }
        return v0_4;
    }

    private com.a.b.d.jl g(com.a.b.d.yl p7)
    {
        if ((!this.c.isEmpty()) && (!p7.f())) {
            if (!p7.a(this.e())) {
                com.a.b.d.lg v0_6;
                if (!p7.d()) {
                    v0_6 = 0;
                } else {
                    v0_6 = com.a.b.d.aba.a(this.c, com.a.b.d.yl.b(), p7.b, com.a.b.d.abg.d, com.a.b.d.abc.b);
                }
                com.a.b.d.lg v1_3;
                if (!p7.e()) {
                    v1_3 = this.c.size();
                } else {
                    v1_3 = com.a.b.d.aba.a(this.c, com.a.b.d.yl.a(), p7.c, com.a.b.d.abg.c, com.a.b.d.abc.b);
                }
                int v2_2 = (v1_3 - v0_6);
                if (v2_2 != 0) {
                    com.a.b.d.lg v0_8 = new com.a.b.d.lg(this, v2_2, v0_6, p7);
                } else {
                    v0_8 = com.a.b.d.jl.d();
                }
            } else {
                v0_8 = this.c;
            }
        } else {
            v0_8 = com.a.b.d.jl.d();
        }
        return v0_8;
    }

    private com.a.b.d.lo h()
    {
        com.a.b.d.zq v0_3;
        if (!this.c.isEmpty()) {
            v0_3 = new com.a.b.d.zq(this.c, com.a.b.d.yl.a);
        } else {
            v0_3 = com.a.b.d.lo.h();
        }
        return v0_3;
    }

    private com.a.b.d.lf i()
    {
        com.a.b.d.lf v0_0 = this.d;
        if (v0_0 == null) {
            if (!this.c.isEmpty()) {
                if ((this.c.size() != 1) || (!((com.a.b.d.yl) this.c.get(0)).equals(com.a.b.d.yl.c()))) {
                    v0_0 = new com.a.b.d.lf(new com.a.b.d.lm(this), this);
                    this.d = v0_0;
                } else {
                    v0_0 = com.a.b.d.lf.a;
                    this.d = v0_0;
                }
            } else {
                v0_0 = com.a.b.d.lf.b;
                this.d = v0_0;
            }
        }
        return v0_0;
    }

    private boolean j()
    {
        return this.c.h_();
    }

    private static com.a.b.d.ll k()
    {
        return new com.a.b.d.ll();
    }

    private Object l()
    {
        return new com.a.b.d.ln(this.c);
    }

    public final com.a.b.d.me a(com.a.b.d.ep p5)
    {
        IllegalArgumentException v0_7;
        com.a.b.b.cn.a(p5);
        if (!this.c.isEmpty()) {
            IllegalArgumentException v0_2 = this.e();
            com.a.b.b.cn.a(p5);
            String v1_1 = v0_2.b.c(p5);
            com.a.b.d.dw v2_1 = v0_2.c.c(p5);
            if ((v1_1 != v0_2.b) || (v2_1 != v0_2.c)) {
                v0_2 = com.a.b.d.yl.a(v1_1, v2_1);
            }
            if (v0_2.d()) {
                if (!v0_2.e()) {
                    try {
                        p5.b();
                    } catch (IllegalArgumentException v0) {
                        throw new IllegalArgumentException("Neither the DiscreteDomain nor this range set are bounded above");
                    }
                }
                v0_7 = new com.a.b.d.lh(this, p5);
            } else {
                throw new IllegalArgumentException("Neither the DiscreteDomain nor this range set are bounded below");
            }
        } else {
            v0_7 = com.a.b.d.me.j();
        }
        return v0_7;
    }

    public final void a(com.a.b.d.yl p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean a()
    {
        return this.c.isEmpty();
    }

    public final bridge synthetic boolean a(com.a.b.d.yr p2)
    {
        return super.a(p2);
    }

    public final bridge synthetic boolean a(Comparable p2)
    {
        return super.a(p2);
    }

    public final com.a.b.d.yl b(Comparable p8)
    {
        int v0_2;
        int v0_1 = com.a.b.d.aba.a(this.c, com.a.b.d.yl.a(), com.a.b.d.dw.b(p8), com.a.b.d.yd.d(), com.a.b.d.abg.a, com.a.b.d.abc.a);
        if (v0_1 == -1) {
            v0_2 = 0;
        } else {
            v0_2 = ((com.a.b.d.yl) this.c.get(v0_1));
            if (!v0_2.c(p8)) {
                v0_2 = 0;
            }
        }
        return v0_2;
    }

    public final bridge synthetic void b()
    {
        super.b();
        return;
    }

    public final void b(com.a.b.d.yl p2)
    {
        throw new UnsupportedOperationException();
    }

    public final void b(com.a.b.d.yr p2)
    {
        throw new UnsupportedOperationException();
    }

    public final void c(com.a.b.d.yr p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean c(com.a.b.d.yl p7)
    {
        int v0_5;
        int v0_1 = com.a.b.d.aba.a(this.c, com.a.b.d.yl.a(), p7.b, com.a.b.d.yd.d(), com.a.b.d.abg.a, com.a.b.d.abc.a);
        if ((v0_1 == -1) || (!((com.a.b.d.yl) this.c.get(v0_1)).a(p7))) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public final com.a.b.d.lf d(com.a.b.d.yl p8)
    {
        if (this.c.isEmpty()) {
            this = com.a.b.d.lf.a;
        } else {
            com.a.b.d.lg v0_2 = this.e();
            if (!p8.a(v0_2)) {
                if (!p8.b(v0_2)) {
                } else {
                    if ((!this.c.isEmpty()) && (!p8.f())) {
                        if (!p8.a(this.e())) {
                            com.a.b.d.lg v0_10;
                            if (!p8.d()) {
                                v0_10 = 0;
                            } else {
                                v0_10 = com.a.b.d.aba.a(this.c, com.a.b.d.yl.b(), p8.b, com.a.b.d.abg.d, com.a.b.d.abc.b);
                            }
                            com.a.b.d.lg v1_4;
                            if (!p8.e()) {
                                v1_4 = this.c.size();
                            } else {
                                v1_4 = com.a.b.d.aba.a(this.c, com.a.b.d.yl.a(), p8.c, com.a.b.d.abg.c, com.a.b.d.abc.b);
                            }
                            int v3_2 = (v1_4 - v0_10);
                            if (v3_2 != 0) {
                                com.a.b.d.lg v0_12 = new com.a.b.d.lg(this, v3_2, v0_10, p8);
                            } else {
                                v0_12 = com.a.b.d.jl.d();
                            }
                        } else {
                            v0_12 = this.c;
                        }
                    } else {
                        v0_12 = com.a.b.d.jl.d();
                    }
                    this = new com.a.b.d.lf(v0_12);
                }
            }
        }
        return this;
    }

    public final com.a.b.d.yl e()
    {
        if (!this.c.isEmpty()) {
            return com.a.b.d.yl.a(((com.a.b.d.yl) this.c.get(0)).b, ((com.a.b.d.yl) this.c.get((this.c.size() - 1))).c);
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final synthetic com.a.b.d.yr e(com.a.b.d.yl p2)
    {
        return this.d(p2);
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final synthetic com.a.b.d.yr f()
    {
        com.a.b.d.lf v0_0 = this.d;
        if (v0_0 == null) {
            if (!this.c.isEmpty()) {
                if ((this.c.size() != 1) || (!((com.a.b.d.yl) this.c.get(0)).equals(com.a.b.d.yl.c()))) {
                    v0_0 = new com.a.b.d.lf(new com.a.b.d.lm(this), this);
                    this.d = v0_0;
                } else {
                    v0_0 = com.a.b.d.lf.a;
                    this.d = v0_0;
                }
            } else {
                v0_0 = com.a.b.d.lf.b;
                this.d = v0_0;
            }
        }
        return v0_0;
    }

    public final synthetic java.util.Set g()
    {
        com.a.b.d.zq v0_3;
        if (!this.c.isEmpty()) {
            v0_3 = new com.a.b.d.zq(this.c, com.a.b.d.yl.a);
        } else {
            v0_3 = com.a.b.d.lo.h();
        }
        return v0_3;
    }
}
