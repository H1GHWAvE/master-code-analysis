package com.a.b.d;
final class rx extends java.util.AbstractSet {
    final synthetic com.a.b.d.qy a;

    rx(com.a.b.d.qy p1)
    {
        this.a = p1;
        return;
    }

    public final void clear()
    {
        this.a.clear();
        return;
    }

    public final boolean contains(Object p2)
    {
        return this.a.containsKey(p2);
    }

    public final boolean isEmpty()
    {
        return this.a.isEmpty();
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.rw(this.a);
    }

    public final boolean remove(Object p2)
    {
        int v0_2;
        if (this.a.remove(p2) == null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final int size()
    {
        return this.a.size();
    }
}
