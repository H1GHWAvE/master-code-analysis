package com.a.b.d;
final class sy extends com.a.b.d.am {
    final Object a;
    Object b;
    final synthetic com.a.b.d.qy c;

    sy(com.a.b.d.qy p1, Object p2, Object p3)
    {
        this.c = p1;
        this.a = p2;
        this.b = p3;
        return;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof java.util.Map$Entry)) && ((this.a.equals(((java.util.Map$Entry) p4).getKey())) && (this.b.equals(((java.util.Map$Entry) p4).getValue())))) {
            v0 = 1;
        }
        return v0;
    }

    public final Object getKey()
    {
        return this.a;
    }

    public final Object getValue()
    {
        return this.b;
    }

    public final int hashCode()
    {
        return (this.a.hashCode() ^ this.b.hashCode());
    }

    public final Object setValue(Object p3)
    {
        Object v0_1 = this.c.put(this.a, p3);
        this.b = p3;
        return v0_1;
    }
}
