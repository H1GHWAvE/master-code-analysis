package com.a.b.d;
public abstract enum class abg extends java.lang.Enum {
    public static final enum com.a.b.d.abg a;
    public static final enum com.a.b.d.abg b;
    public static final enum com.a.b.d.abg c;
    public static final enum com.a.b.d.abg d;
    public static final enum com.a.b.d.abg e;
    private static final synthetic com.a.b.d.abg[] f;

    static abg()
    {
        com.a.b.d.abg.a = new com.a.b.d.abh("ANY_PRESENT");
        com.a.b.d.abg.b = new com.a.b.d.abi("LAST_PRESENT");
        com.a.b.d.abg.c = new com.a.b.d.abj("FIRST_PRESENT");
        com.a.b.d.abg.d = new com.a.b.d.abk("FIRST_AFTER");
        com.a.b.d.abg.e = new com.a.b.d.abl("LAST_BEFORE");
        com.a.b.d.abg[] v0_11 = new com.a.b.d.abg[5];
        v0_11[0] = com.a.b.d.abg.a;
        v0_11[1] = com.a.b.d.abg.b;
        v0_11[2] = com.a.b.d.abg.c;
        v0_11[3] = com.a.b.d.abg.d;
        v0_11[4] = com.a.b.d.abg.e;
        com.a.b.d.abg.f = v0_11;
        return;
    }

    private abg(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic abg(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.d.abg valueOf(String p1)
    {
        return ((com.a.b.d.abg) Enum.valueOf(com.a.b.d.abg, p1));
    }

    public static com.a.b.d.abg[] values()
    {
        return ((com.a.b.d.abg[]) com.a.b.d.abg.f.clone());
    }

    abstract int a();
}
