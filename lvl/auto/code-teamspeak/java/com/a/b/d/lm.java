package com.a.b.d;
final class lm extends com.a.b.d.jl {
    final synthetic com.a.b.d.lf a;
    private final boolean b;
    private final boolean d;
    private final int e;

    lm(com.a.b.d.lf p3)
    {
        int v0_6;
        this.a = p3;
        this.b = ((com.a.b.d.yl) com.a.b.d.lf.a(p3).get(0)).d();
        int v0_4 = com.a.b.d.lf.a(p3);
        if (!(v0_4 instanceof java.util.List)) {
            v0_6 = com.a.b.d.nj.f(v0_4.iterator());
        } else {
            int v0_7 = ((java.util.List) v0_4);
            if (!v0_7.isEmpty()) {
                v0_6 = com.a.b.d.mq.a(v0_7);
            } else {
                throw new java.util.NoSuchElementException();
            }
        }
        this.d = ((com.a.b.d.yl) v0_6).e();
        int v0_12 = (com.a.b.d.lf.a(p3).size() - 1);
        if (this.b) {
            v0_12++;
        }
        if (this.d) {
            v0_12++;
        }
        this.e = v0_12;
        return;
    }

    private com.a.b.d.yl b(int p4)
    {
        boolean v1_0;
        com.a.b.b.cn.a(p4, this.e);
        if (!this.b) {
            v1_0 = ((com.a.b.d.yl) com.a.b.d.lf.a(this.a).get(p4)).c;
        } else {
            com.a.b.d.dw v0_11;
            if (p4 != 0) {
                v0_11 = ((com.a.b.d.yl) com.a.b.d.lf.a(this.a).get((p4 - 1))).c;
            } else {
                v0_11 = com.a.b.d.dw.d();
            }
            v1_0 = v0_11;
        }
        if ((!this.d) || (p4 != (this.e - 1))) {
            com.a.b.d.dw v0_17;
            com.a.b.d.jl v2 = com.a.b.d.lf.a(this.a);
            if (!this.b) {
                v0_17 = 1;
            } else {
                v0_17 = 0;
            }
            com.a.b.d.dw v0_21 = ((com.a.b.d.yl) v2.get((v0_17 + p4))).b;
        } else {
            v0_21 = com.a.b.d.dw.e();
        }
        return com.a.b.d.yl.a(v1_0, v0_21);
    }

    public final synthetic Object get(int p4)
    {
        boolean v1_0;
        com.a.b.b.cn.a(p4, this.e);
        if (!this.b) {
            v1_0 = ((com.a.b.d.yl) com.a.b.d.lf.a(this.a).get(p4)).c;
        } else {
            com.a.b.d.dw v0_11;
            if (p4 != 0) {
                v0_11 = ((com.a.b.d.yl) com.a.b.d.lf.a(this.a).get((p4 - 1))).c;
            } else {
                v0_11 = com.a.b.d.dw.d();
            }
            v1_0 = v0_11;
        }
        if ((!this.d) || (p4 != (this.e - 1))) {
            com.a.b.d.dw v0_17;
            com.a.b.d.jl v2 = com.a.b.d.lf.a(this.a);
            if (!this.b) {
                v0_17 = 1;
            } else {
                v0_17 = 0;
            }
            com.a.b.d.dw v0_21 = ((com.a.b.d.yl) v2.get((v0_17 + p4))).b;
        } else {
            v0_21 = com.a.b.d.dw.e();
        }
        return com.a.b.d.yl.a(v1_0, v0_21);
    }

    final boolean h_()
    {
        return 1;
    }

    public final int size()
    {
        return this.e;
    }
}
