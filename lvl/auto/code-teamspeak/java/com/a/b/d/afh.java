package com.a.b.d;
final class afh extends java.util.AbstractMap {
    final synthetic com.a.b.d.afg a;

    afh(com.a.b.d.afg p1)
    {
        this.a = p1;
        return;
    }

    private boolean a(com.a.b.b.co p5)
    {
        java.util.ArrayList v1_1 = new java.util.ArrayList();
        java.util.Iterator v2_0 = this.entrySet().iterator();
        while (v2_0.hasNext()) {
            int v0_8 = ((java.util.Map$Entry) v2_0.next());
            if (p5.a(v0_8)) {
                v1_1.add(v0_8.getKey());
            }
        }
        java.util.Iterator v2_1 = v1_1.iterator();
        while (v2_1.hasNext()) {
            this.a.b.a(((com.a.b.d.yl) v2_1.next()));
        }
        int v0_4;
        if (v1_1.isEmpty()) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    static synthetic boolean a(com.a.b.d.afh p4, com.a.b.b.co p5)
    {
        java.util.ArrayList v1_1 = new java.util.ArrayList();
        java.util.Iterator v2_0 = p4.entrySet().iterator();
        while (v2_0.hasNext()) {
            int v0_8 = ((java.util.Map$Entry) v2_0.next());
            if (p5.a(v0_8)) {
                v1_1.add(v0_8.getKey());
            }
        }
        java.util.Iterator v2_1 = v1_1.iterator();
        while (v2_1.hasNext()) {
            p4.a.b.a(((com.a.b.d.yl) v2_1.next()));
        }
        int v0_4;
        if (v1_1.isEmpty()) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final void clear()
    {
        com.a.b.d.yl v0_0 = this.a;
        v0_0.b.a(v0_0.a);
        return;
    }

    public final boolean containsKey(Object p2)
    {
        int v0_1;
        if (this.get(p2) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final java.util.Set entrySet()
    {
        return new com.a.b.d.afj(this);
    }

    public final Object get(Object p5)
    {
        Object v1 = 0;
        try {
            if (((p5 instanceof com.a.b.d.yl)) && ((this.a.a.a(((com.a.b.d.yl) p5))) && (!((com.a.b.d.yl) p5).f()))) {
                int v0_11;
                if (((com.a.b.d.yl) p5).b.a(this.a.a.b) != 0) {
                    v0_11 = ((com.a.b.d.aff) com.a.b.d.afb.a(this.a.b).get(((com.a.b.d.yl) p5).b));
                } else {
                    int v0_15 = com.a.b.d.afb.a(this.a.b).floorEntry(((com.a.b.d.yl) p5).b);
                    if (v0_15 == 0) {
                        v0_11 = 0;
                    } else {
                        v0_11 = ((com.a.b.d.aff) v0_15.getValue());
                    }
                }
                if ((v0_11 != 0) && ((v0_11.a.b(this.a.a)) && (v0_11.a.c(this.a.a).equals(((com.a.b.d.yl) p5))))) {
                    v1 = v0_11.getValue();
                }
            }
        } catch (int v0) {
        }
        return v1;
    }

    public final java.util.Set keySet()
    {
        return new com.a.b.d.afi(this, this);
    }

    public final Object remove(Object p3)
    {
        int v0 = this.get(p3);
        if (v0 == 0) {
            v0 = 0;
        } else {
            this.a.b.a(((com.a.b.d.yl) p3));
        }
        return v0;
    }

    public final java.util.Collection values()
    {
        return new com.a.b.d.afl(this, this);
    }
}
