package com.a.b.d;
final class xs extends com.a.b.d.as {
    final com.a.b.d.xc a;
    final com.a.b.b.co b;

    xs(com.a.b.d.xc p2, com.a.b.b.co p3)
    {
        this.a = ((com.a.b.d.xc) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.b.co) com.a.b.b.cn.a(p3));
        return;
    }

    private com.a.b.d.agi g()
    {
        return com.a.b.d.nj.b(this.a.iterator(), this.b);
    }

    public final int a(Object p4)
    {
        int v0_1 = this.a.a(p4);
        if (v0_1 <= 0) {
            v0_1 = 0;
        } else {
            if (!this.b.a(p4)) {
                v0_1 = 0;
            }
        }
        return v0_1;
    }

    public final int a(Object p6, int p7)
    {
        int v0_1 = this.b.a(p6);
        Object[] v2_1 = new Object[2];
        v2_1[0] = p6;
        v2_1[1] = this.b;
        com.a.b.b.cn.a(v0_1, "Element %s does not match predicate %s", v2_1);
        return this.a.a(p6, p7);
    }

    public final int b(Object p2, int p3)
    {
        int v0_2;
        com.a.b.d.cl.a(p3, "occurrences");
        if (p3 != 0) {
            if (!this.contains(p2)) {
                v0_2 = 0;
            } else {
                v0_2 = this.a.b(p2, p3);
            }
        } else {
            v0_2 = this.a(p2);
        }
        return v0_2;
    }

    final java.util.Iterator b()
    {
        throw new AssertionError("should never be called");
    }

    final int c()
    {
        return this.n_().size();
    }

    public final void clear()
    {
        this.n_().clear();
        return;
    }

    final java.util.Set e()
    {
        return com.a.b.d.aad.a(this.a.n_(), this.b);
    }

    final java.util.Set f()
    {
        return com.a.b.d.aad.a(this.a.a(), new com.a.b.d.xt(this));
    }

    public final synthetic java.util.Iterator iterator()
    {
        return com.a.b.d.nj.b(this.a.iterator(), this.b);
    }
}
