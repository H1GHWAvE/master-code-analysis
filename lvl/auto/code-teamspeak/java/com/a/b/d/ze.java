package com.a.b.d;
final class ze extends com.a.b.d.jl {
    private final transient int a;
    private final transient int b;
    private final transient Object[] d;

    ze(Object[] p3)
    {
        this(p3, 0, p3.length);
        return;
    }

    private ze(Object[] p1, int p2, int p3)
    {
        this.a = p2;
        this.b = p3;
        this.d = p1;
        return;
    }

    final int a(Object[] p4, int p5)
    {
        System.arraycopy(this.d, this.a, p4, p5, this.b);
        return (this.b + p5);
    }

    public final com.a.b.d.agj a(int p4)
    {
        return com.a.b.d.nj.a(this.d, this.a, this.b, p4);
    }

    final com.a.b.d.jl b(int p5, int p6)
    {
        return new com.a.b.d.ze(this.d, (this.a + p5), (p6 - p5));
    }

    public final Object get(int p3)
    {
        com.a.b.b.cn.a(p3, this.b);
        return this.d[(this.a + p3)];
    }

    final boolean h_()
    {
        int v0_1;
        if (this.b == this.d.length) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int indexOf(Object p5)
    {
        int v0;
        if (p5 != null) {
            v0 = 0;
            while (v0 < this.b) {
                if (!this.d[(this.a + v0)].equals(p5)) {
                    v0++;
                }
            }
            v0 = -1;
        } else {
            v0 = -1;
        }
        return v0;
    }

    public final int lastIndexOf(Object p5)
    {
        int v0 = -1;
        if (p5 != null) {
            int v1_1 = (this.b - 1);
            while (v1_1 >= 0) {
                if (!this.d[(this.a + v1_1)].equals(p5)) {
                    v1_1--;
                } else {
                    v0 = v1_1;
                    break;
                }
            }
        }
        return v0;
    }

    public final synthetic java.util.ListIterator listIterator(int p2)
    {
        return this.a(p2);
    }

    public final int size()
    {
        return this.b;
    }
}
