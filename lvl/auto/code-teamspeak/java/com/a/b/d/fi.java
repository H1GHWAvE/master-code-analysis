package com.a.b.d;
 class fi extends com.a.b.d.an implements com.a.b.d.ga {
    final com.a.b.d.vi a;
    final com.a.b.b.co b;

    fi(com.a.b.d.vi p2, com.a.b.b.co p3)
    {
        this.a = ((com.a.b.d.vi) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.b.co) com.a.b.b.cn.a(p3));
        return;
    }

    static java.util.Collection a(java.util.Collection p1, com.a.b.b.co p2)
    {
        java.util.Collection v0_1;
        if (!(p1 instanceof java.util.Set)) {
            v0_1 = com.a.b.d.cm.a(p1, p2);
        } else {
            v0_1 = com.a.b.d.aad.a(((java.util.Set) p1), p2);
        }
        return v0_1;
    }

    static synthetic boolean a(com.a.b.d.fi p2, Object p3, Object p4)
    {
        return p2.b.a(com.a.b.d.sz.a(p3, p4));
    }

    private java.util.Collection d()
    {
        java.util.List v0_2;
        if (!(this.a instanceof com.a.b.d.aac)) {
            v0_2 = java.util.Collections.emptyList();
        } else {
            v0_2 = java.util.Collections.emptySet();
        }
        return v0_2;
    }

    private boolean d(Object p3, Object p4)
    {
        return this.b.a(com.a.b.d.sz.a(p3, p4));
    }

    public com.a.b.d.vi a()
    {
        return this.a;
    }

    final boolean a(com.a.b.b.co p7)
    {
        java.util.Iterator v3 = this.a.b().entrySet().iterator();
        int v2_0 = 0;
        while (v3.hasNext()) {
            int v0_7;
            int v0_6 = ((java.util.Map$Entry) v3.next());
            boolean v4_0 = v0_6.getKey();
            java.util.Collection v1_2 = com.a.b.d.fi.a(((java.util.Collection) v0_6.getValue()), new com.a.b.d.fr(this, v4_0));
            if ((v1_2.isEmpty()) || (!p7.a(com.a.b.d.sz.a(v4_0, v1_2)))) {
                v0_7 = v2_0;
            } else {
                if (v1_2.size() != ((java.util.Collection) v0_6.getValue()).size()) {
                    v1_2.clear();
                } else {
                    v3.remove();
                }
                v0_7 = 1;
            }
            v2_0 = v0_7;
        }
        return v2_0;
    }

    public final com.a.b.b.co c()
    {
        return this.b;
    }

    public java.util.Collection c(Object p3)
    {
        return com.a.b.d.fi.a(this.a.c(p3), new com.a.b.d.fr(this, p3));
    }

    public java.util.Collection d(Object p3)
    {
        java.util.Collection v0_3;
        Object v1 = this.b().remove(p3);
        if (!(this.a instanceof com.a.b.d.aac)) {
            v0_3 = java.util.Collections.emptyList();
        } else {
            v0_3 = java.util.Collections.emptySet();
        }
        return ((java.util.Collection) com.a.b.b.ca.a(v1, v0_3));
    }

    public final int f()
    {
        return this.k().size();
    }

    public final boolean f(Object p2)
    {
        int v0_2;
        if (this.b().get(p2) == null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final void g()
    {
        this.k().clear();
        return;
    }

    final java.util.Iterator l()
    {
        throw new AssertionError("should never be called");
    }

    final java.util.Map m()
    {
        return new com.a.b.d.fj(this);
    }

    java.util.Collection o()
    {
        return com.a.b.d.fi.a(this.a.k(), this.b);
    }

    public final java.util.Set p()
    {
        return this.b().keySet();
    }

    final com.a.b.d.xc r()
    {
        return new com.a.b.d.fo(this);
    }

    final java.util.Collection s()
    {
        return new com.a.b.d.gb(this);
    }
}
