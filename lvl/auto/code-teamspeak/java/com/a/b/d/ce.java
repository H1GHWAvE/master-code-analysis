package com.a.b.d;
public abstract enum class ce extends java.lang.Enum {
    public static final enum com.a.b.d.ce a;
    public static final enum com.a.b.d.ce b;
    private static final synthetic com.a.b.d.ce[] c;

    static ce()
    {
        com.a.b.d.ce.a = new com.a.b.d.cf("OPEN");
        com.a.b.d.ce.b = new com.a.b.d.cg("CLOSED");
        com.a.b.d.ce[] v0_5 = new com.a.b.d.ce[2];
        v0_5[0] = com.a.b.d.ce.a;
        v0_5[1] = com.a.b.d.ce.b;
        com.a.b.d.ce.c = v0_5;
        return;
    }

    private ce(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic ce(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    static com.a.b.d.ce a(boolean p1)
    {
        com.a.b.d.ce v0;
        if (!p1) {
            v0 = com.a.b.d.ce.a;
        } else {
            v0 = com.a.b.d.ce.b;
        }
        return v0;
    }

    public static com.a.b.d.ce valueOf(String p1)
    {
        return ((com.a.b.d.ce) Enum.valueOf(com.a.b.d.ce, p1));
    }

    public static com.a.b.d.ce[] values()
    {
        return ((com.a.b.d.ce[]) com.a.b.d.ce.c.clone());
    }

    abstract com.a.b.d.ce a();
}
