package com.a.b.d;
public class ju {
    com.a.b.d.kb[] a;
    int b;

    public ju()
    {
        this(0);
        return;
    }

    private ju(byte p2)
    {
        int v0_1 = new com.a.b.d.kb[4];
        this.a = v0_1;
        this.b = 0;
        return;
    }

    private void a(int p3)
    {
        if (p3 > this.a.length) {
            this.a = ((com.a.b.d.kb[]) com.a.b.d.yc.b(this.a, com.a.b.d.jb.a(this.a.length, p3)));
        }
        return;
    }

    public com.a.b.d.jt a()
    {
        com.a.b.d.jt v0_4;
        switch (this.b) {
            case 0:
                v0_4 = com.a.b.d.jt.k();
                break;
            case 1:
                v0_4 = com.a.b.d.jt.c(this.a[0].getKey(), this.a[0].getValue());
                break;
            default:
                v0_4 = new com.a.b.d.zf(this.b, this.a);
        }
        return v0_4;
    }

    public com.a.b.d.ju a(Object p5, Object p6)
    {
        this.a((this.b + 1));
        com.a.b.d.kb v0_2 = com.a.b.d.jt.d(p5, p6);
        com.a.b.d.kb[] v1 = this.a;
        int v2 = this.b;
        this.b = (v2 + 1);
        v1[v2] = v0_2;
        return this;
    }

    public com.a.b.d.ju a(java.util.Map$Entry p3)
    {
        return this.a(p3.getKey(), p3.getValue());
    }

    public com.a.b.d.ju a(java.util.Map p3)
    {
        this.a((this.b + p3.size()));
        java.util.Iterator v1_1 = p3.entrySet().iterator();
        while (v1_1.hasNext()) {
            this.a(((java.util.Map$Entry) v1_1.next()));
        }
        return this;
    }
}
