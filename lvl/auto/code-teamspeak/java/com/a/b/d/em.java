package com.a.b.d;
final class em extends com.a.b.d.me {
    private final com.a.b.d.me a;

    em(com.a.b.d.me p2)
    {
        this(com.a.b.d.yd.a(p2.comparator()).a());
        this.a = p2;
        return;
    }

    final com.a.b.d.me a(Object p2, boolean p3)
    {
        return this.a.d(p2, p3).b();
    }

    final com.a.b.d.me a(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.a.b(p4, p5, p2, p3).b();
    }

    public final com.a.b.d.me b()
    {
        return this.a;
    }

    final com.a.b.d.me b(Object p2, boolean p3)
    {
        return this.a.c(p2, p3).b();
    }

    final int c(Object p3)
    {
        int v0_1 = this.a.c(p3);
        if (v0_1 != -1) {
            v0_1 = ((this.size() - 1) - v0_1);
        }
        return v0_1;
    }

    public final com.a.b.d.agi c()
    {
        return this.a.d();
    }

    public final Object ceiling(Object p2)
    {
        return this.a.floor(p2);
    }

    public final com.a.b.d.agi d()
    {
        return this.a.c();
    }

    public final synthetic java.util.Iterator descendingIterator()
    {
        return this.a.c();
    }

    public final bridge synthetic java.util.NavigableSet descendingSet()
    {
        return this.a;
    }

    final com.a.b.d.me e()
    {
        throw new AssertionError("should never be called");
    }

    public final Object floor(Object p2)
    {
        return this.a.ceiling(p2);
    }

    final boolean h_()
    {
        return this.a.h_();
    }

    public final Object higher(Object p2)
    {
        return this.a.lower(p2);
    }

    public final synthetic java.util.Iterator iterator()
    {
        return this.a.d();
    }

    public final Object lower(Object p2)
    {
        return this.a.higher(p2);
    }

    public final int size()
    {
        return this.a.size();
    }
}
