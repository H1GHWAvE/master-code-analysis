package com.a.b.d;
final class aff extends com.a.b.d.am {
    final com.a.b.d.yl a;
    private final Object b;

    aff(com.a.b.d.dw p2, com.a.b.d.dw p3, Object p4)
    {
        this(com.a.b.d.yl.a(p2, p3), p4);
        return;
    }

    aff(com.a.b.d.yl p1, Object p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private com.a.b.d.yl a()
    {
        return this.a;
    }

    private boolean a(Comparable p2)
    {
        return this.a.c(p2);
    }

    private com.a.b.d.dw b()
    {
        return this.a.b;
    }

    private com.a.b.d.dw c()
    {
        return this.a.c;
    }

    public final bridge synthetic Object getKey()
    {
        return this.a;
    }

    public final Object getValue()
    {
        return this.b;
    }
}
