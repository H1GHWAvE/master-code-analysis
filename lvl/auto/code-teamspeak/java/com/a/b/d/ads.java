package com.a.b.d;
 class ads extends com.a.b.d.adi implements java.util.SortedMap {
    private static final long a;

    ads(java.util.SortedMap p1, Object p2)
    {
        this(p1, p2);
        return;
    }

    synthetic java.util.Map a()
    {
        return this.b();
    }

    java.util.SortedMap b()
    {
        return ((java.util.SortedMap) super.a());
    }

    public java.util.Comparator comparator()
    {
        try {
            return this.b().comparator();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    synthetic Object d()
    {
        return this.b();
    }

    public Object firstKey()
    {
        try {
            return this.b().firstKey();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public java.util.SortedMap headMap(Object p4)
    {
        try {
            return com.a.b.d.acu.a(this.b().headMap(p4), this.h);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public Object lastKey()
    {
        try {
            return this.b().lastKey();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public java.util.SortedMap subMap(Object p4, Object p5)
    {
        try {
            return com.a.b.d.acu.a(this.b().subMap(p4, p5), this.h);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public java.util.SortedMap tailMap(Object p4)
    {
        try {
            return com.a.b.d.acu.a(this.b().tailMap(p4), this.h);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }
}
