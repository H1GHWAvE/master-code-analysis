package com.a.b.d;
final class pa extends java.util.AbstractList implements java.io.Serializable, java.util.RandomAccess {
    private static final long c;
    final Object a;
    final Object[] b;

    pa(Object p2, Object[] p3)
    {
        this.a = p2;
        this.b = ((Object[]) com.a.b.b.cn.a(p3));
        return;
    }

    public final Object get(int p3)
    {
        Object v0_2;
        com.a.b.b.cn.a(p3, this.size());
        if (p3 != 0) {
            v0_2 = this.b[(p3 - 1)];
        } else {
            v0_2 = this.a;
        }
        return v0_2;
    }

    public final int size()
    {
        return (this.b.length + 1);
    }
}
