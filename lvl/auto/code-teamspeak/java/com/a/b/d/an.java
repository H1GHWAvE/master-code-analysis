package com.a.b.d;
abstract class an implements com.a.b.d.vi {
    private transient java.util.Collection a;
    private transient java.util.Set b;
    private transient com.a.b.d.xc c;
    private transient java.util.Collection d;
    private transient java.util.Map e;

    an()
    {
        return;
    }

    public boolean a(com.a.b.d.vi p5)
    {
        java.util.Iterator v2 = p5.k().iterator();
        int v1_1 = 0;
        while (v2.hasNext()) {
            int v0_3 = ((java.util.Map$Entry) v2.next());
            v1_1 = (this.a(v0_3.getKey(), v0_3.getValue()) | v1_1);
        }
        return v1_1;
    }

    public boolean a(Object p2, Object p3)
    {
        return this.c(p2).add(p3);
    }

    public java.util.Collection b(Object p2, Iterable p3)
    {
        com.a.b.b.cn.a(p3);
        java.util.Collection v0 = this.d(p2);
        this.c(p2, p3);
        return v0;
    }

    public java.util.Map b()
    {
        java.util.Map v0 = this.e;
        if (v0 == null) {
            v0 = this.m();
            this.e = v0;
        }
        return v0;
    }

    public boolean b(Object p2, Object p3)
    {
        int v0_4;
        int v0_2 = ((java.util.Collection) this.b().get(p2));
        if ((v0_2 == 0) || (!v0_2.contains(p3))) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public boolean c(Object p5, Iterable p6)
    {
        int v0 = 1;
        com.a.b.b.cn.a(p6);
        if (!(p6 instanceof java.util.Collection)) {
            boolean v2_1 = p6.iterator();
            if ((!v2_1.hasNext()) || (!com.a.b.d.nj.a(this.c(p5), v2_1))) {
                v0 = 0;
            }
        } else {
            if ((((java.util.Collection) p6).isEmpty()) || (!this.c(p5).addAll(((java.util.Collection) p6)))) {
                v0 = 0;
            }
        }
        return v0;
    }

    public boolean c(Object p2, Object p3)
    {
        int v0_4;
        int v0_2 = ((java.util.Collection) this.b().get(p2));
        if ((v0_2 == 0) || (!v0_2.remove(p3))) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public boolean equals(Object p3)
    {
        int v0_1;
        if (p3 != this) {
            if (!(p3 instanceof com.a.b.d.vi)) {
                v0_1 = 0;
            } else {
                v0_1 = this.b().equals(((com.a.b.d.vi) p3).b());
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean g(Object p3)
    {
        java.util.Iterator v1 = this.b().values().iterator();
        while (v1.hasNext()) {
            if (((java.util.Collection) v1.next()).contains(p3)) {
                int v0_3 = 1;
            }
            return v0_3;
        }
        v0_3 = 0;
        return v0_3;
    }

    java.util.Set h()
    {
        return new com.a.b.d.uk(this.b());
    }

    public int hashCode()
    {
        return this.b().hashCode();
    }

    public java.util.Collection i()
    {
        java.util.Collection v0 = this.d;
        if (v0 == null) {
            v0 = this.s();
            this.d = v0;
        }
        return v0;
    }

    java.util.Iterator j()
    {
        return com.a.b.d.sz.b(this.k().iterator());
    }

    public java.util.Collection k()
    {
        java.util.Collection v0 = this.a;
        if (v0 == null) {
            v0 = this.o();
            this.a = v0;
        }
        return v0;
    }

    abstract java.util.Iterator l();

    abstract java.util.Map m();

    public boolean n()
    {
        int v0_1;
        if (this.f() != 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    java.util.Collection o()
    {
        com.a.b.d.ap v0_2;
        if (!(this instanceof com.a.b.d.aac)) {
            v0_2 = new com.a.b.d.ap(this, 0);
        } else {
            v0_2 = new com.a.b.d.aq(this, 0);
        }
        return v0_2;
    }

    public java.util.Set p()
    {
        java.util.Set v0 = this.b;
        if (v0 == null) {
            v0 = this.h();
            this.b = v0;
        }
        return v0;
    }

    public com.a.b.d.xc q()
    {
        com.a.b.d.xc v0 = this.c;
        if (v0 == null) {
            v0 = this.r();
            this.c = v0;
        }
        return v0;
    }

    com.a.b.d.xc r()
    {
        return new com.a.b.d.wn(this);
    }

    java.util.Collection s()
    {
        return new com.a.b.d.ar(this);
    }

    public String toString()
    {
        return this.b().toString();
    }
}
