package com.a.b.d;
public abstract class az extends com.a.b.d.agi {
    private Object a;

    public az(Object p1)
    {
        this.a = p1;
        return;
    }

    public abstract Object a();

    public final boolean hasNext()
    {
        int v0_1;
        if (this.a == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final Object next()
    {
        if (this.hasNext()) {
            try {
                Throwable v0_1 = this.a;
                this.a = this.a(this.a);
                return v0_1;
            } catch (Throwable v0_2) {
                this.a = this.a(this.a);
                throw v0_2;
            }
        } else {
            throw new java.util.NoSuchElementException();
        }
    }
}
