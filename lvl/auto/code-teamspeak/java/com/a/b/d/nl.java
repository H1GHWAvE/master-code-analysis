package com.a.b.d;
final class nl extends com.a.b.d.agi {
    final synthetic java.util.Iterator a;

    nl(java.util.Iterator p1)
    {
        this.a = p1;
        return;
    }

    public final boolean hasNext()
    {
        return this.a.hasNext();
    }

    public final Object next()
    {
        Object v0_1 = this.a.next();
        this.a.remove();
        return v0_1;
    }

    public final String toString()
    {
        return "Iterators.consumingIterator(...)";
    }
}
