package com.a.b.d;
final class sg extends java.lang.ref.SoftReference implements com.a.b.d.sr {
    final com.a.b.d.rz a;

    sg(ref.ReferenceQueue p1, Object p2, com.a.b.d.rz p3)
    {
        this(p2, p1);
        this.a = p3;
        return;
    }

    public final com.a.b.d.rz a()
    {
        return this.a;
    }

    public final com.a.b.d.sr a(ref.ReferenceQueue p2, Object p3, com.a.b.d.rz p4)
    {
        return new com.a.b.d.sg(p2, p3, p4);
    }

    public final void a(com.a.b.d.sr p1)
    {
        this.clear();
        return;
    }

    public final boolean b()
    {
        return 0;
    }

    public final Object c()
    {
        return this.get();
    }
}
