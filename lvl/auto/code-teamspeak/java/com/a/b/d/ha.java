package com.a.b.d;
public abstract class ha extends com.a.b.d.hk implements java.util.NavigableMap {

    protected ha()
    {
        return;
    }

    private java.util.Map$Entry a(Object p2)
    {
        return this.headMap(p2, 0).lastEntry();
    }

    private Object b(Object p2)
    {
        return com.a.b.d.sz.b(this.lowerEntry(p2));
    }

    private java.util.Map$Entry d()
    {
        return ((java.util.Map$Entry) com.a.b.d.mq.f(this.entrySet()));
    }

    private java.util.Map$Entry d(Object p2)
    {
        return this.headMap(p2, 1).lastEntry();
    }

    private Object e()
    {
        Object v0_0 = this.firstEntry();
        if (v0_0 != null) {
            return v0_0.getKey();
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    private Object e(Object p2)
    {
        return com.a.b.d.sz.b(this.floorEntry(p2));
    }

    private java.util.Map$Entry f()
    {
        return ((java.util.Map$Entry) com.a.b.d.mq.f(this.descendingMap().entrySet()));
    }

    private java.util.Map$Entry f(Object p2)
    {
        return this.tailMap(p2, 1).firstEntry();
    }

    private Object g(Object p2)
    {
        return com.a.b.d.sz.b(this.ceilingEntry(p2));
    }

    private Object h()
    {
        Object v0_0 = this.lastEntry();
        if (v0_0 != null) {
            return v0_0.getKey();
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    private java.util.Map$Entry h(Object p2)
    {
        return this.tailMap(p2, 0).firstEntry();
    }

    private Object i(Object p2)
    {
        return com.a.b.d.sz.b(this.higherEntry(p2));
    }

    private java.util.Map$Entry i()
    {
        return ((java.util.Map$Entry) com.a.b.d.nj.h(this.entrySet().iterator()));
    }

    private java.util.Map$Entry j()
    {
        return ((java.util.Map$Entry) com.a.b.d.nj.h(this.descendingMap().entrySet().iterator()));
    }

    private java.util.SortedMap j(Object p2)
    {
        return this.headMap(p2, 0);
    }

    private java.util.NavigableSet k()
    {
        return this.descendingMap().navigableKeySet();
    }

    private java.util.SortedMap k(Object p2)
    {
        return this.tailMap(p2, 1);
    }

    protected final synthetic java.util.Map a()
    {
        return this.b();
    }

    protected final java.util.SortedMap a(Object p3, Object p4)
    {
        return this.subMap(p3, 1, p4, 0);
    }

    protected abstract java.util.NavigableMap b();

    protected final synthetic java.util.SortedMap c()
    {
        return this.b();
    }

    public java.util.Map$Entry ceilingEntry(Object p2)
    {
        return this.b().ceilingEntry(p2);
    }

    public Object ceilingKey(Object p2)
    {
        return this.b().ceilingKey(p2);
    }

    public java.util.NavigableSet descendingKeySet()
    {
        return this.b().descendingKeySet();
    }

    public java.util.NavigableMap descendingMap()
    {
        return this.b().descendingMap();
    }

    public java.util.Map$Entry firstEntry()
    {
        return this.b().firstEntry();
    }

    public java.util.Map$Entry floorEntry(Object p2)
    {
        return this.b().floorEntry(p2);
    }

    public Object floorKey(Object p2)
    {
        return this.b().floorKey(p2);
    }

    public java.util.NavigableMap headMap(Object p2, boolean p3)
    {
        return this.b().headMap(p2, p3);
    }

    public java.util.Map$Entry higherEntry(Object p2)
    {
        return this.b().higherEntry(p2);
    }

    public Object higherKey(Object p2)
    {
        return this.b().higherKey(p2);
    }

    protected final synthetic Object k_()
    {
        return this.b();
    }

    public java.util.Map$Entry lastEntry()
    {
        return this.b().lastEntry();
    }

    public java.util.Map$Entry lowerEntry(Object p2)
    {
        return this.b().lowerEntry(p2);
    }

    public Object lowerKey(Object p2)
    {
        return this.b().lowerKey(p2);
    }

    public java.util.NavigableSet navigableKeySet()
    {
        return this.b().navigableKeySet();
    }

    public java.util.Map$Entry pollFirstEntry()
    {
        return this.b().pollFirstEntry();
    }

    public java.util.Map$Entry pollLastEntry()
    {
        return this.b().pollLastEntry();
    }

    public java.util.NavigableMap subMap(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.b().subMap(p2, p3, p4, p5);
    }

    public java.util.NavigableMap tailMap(Object p2, boolean p3)
    {
        return this.b().tailMap(p2, p3);
    }
}
