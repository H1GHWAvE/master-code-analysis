package com.a.b.d;
final class xv implements java.util.Iterator {
    private final com.a.b.d.xc a;
    private final java.util.Iterator b;
    private com.a.b.d.xd c;
    private int d;
    private int e;
    private boolean f;

    xv(com.a.b.d.xc p1, java.util.Iterator p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final boolean hasNext()
    {
        if ((this.d <= 0) && (!this.b.hasNext())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final Object next()
    {
        if (this.hasNext()) {
            if (this.d == 0) {
                this.c = ((com.a.b.d.xd) this.b.next());
                Object v0_6 = this.c.b();
                this.d = v0_6;
                this.e = v0_6;
            }
            this.d = (this.d - 1);
            this.f = 1;
            return this.c.a();
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final void remove()
    {
        com.a.b.b.cn.b(this.f, "no calls to next() since the last call to remove()");
        if (this.e != 1) {
            this.a.remove(this.c.a());
        } else {
            this.b.remove();
        }
        this.e = (this.e - 1);
        this.f = 0;
        return;
    }
}
