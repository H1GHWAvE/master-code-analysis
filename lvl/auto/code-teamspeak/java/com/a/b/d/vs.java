package com.a.b.d;
final enum class vs extends java.lang.Enum implements com.a.b.b.dz {
    public static final enum com.a.b.d.vs a;
    private static final synthetic com.a.b.d.vs[] b;

    static vs()
    {
        com.a.b.d.vs.a = new com.a.b.d.vs("INSTANCE");
        com.a.b.d.vs[] v0_3 = new com.a.b.d.vs[1];
        v0_3[0] = com.a.b.d.vs.a;
        com.a.b.d.vs.b = v0_3;
        return;
    }

    private vs(String p2)
    {
        this(p2, 0);
        return;
    }

    public static com.a.b.b.dz b()
    {
        return com.a.b.d.vs.a;
    }

    private static java.util.List c()
    {
        return new java.util.LinkedList();
    }

    public static com.a.b.d.vs valueOf(String p1)
    {
        return ((com.a.b.d.vs) Enum.valueOf(com.a.b.d.vs, p1));
    }

    public static com.a.b.d.vs[] values()
    {
        return ((com.a.b.d.vs[]) com.a.b.d.vs.b.clone());
    }

    public final synthetic Object a()
    {
        return new java.util.LinkedList();
    }
}
