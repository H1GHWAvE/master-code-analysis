package com.a.b.d;
final class zp extends com.a.b.d.ma {
    private final transient com.a.b.d.zq b;
    private final transient int[] c;
    private final transient long[] d;
    private final transient int e;
    private final transient int f;

    zp(com.a.b.d.zq p1, int[] p2, long[] p3, int p4, int p5)
    {
        this.b = p1;
        this.c = p2;
        this.d = p3;
        this.e = p4;
        this.f = p5;
        return;
    }

    private com.a.b.d.ma a(int p7, int p8)
    {
        com.a.b.b.cn.a(p7, p8, this.f);
        if (p7 != p8) {
            if ((p7 != 0) || (p8 != this.f)) {
                this = new com.a.b.d.zp(((com.a.b.d.zq) this.b.a(p7, p8)), this.c, this.d, (this.e + p7), (p8 - p7));
            }
        } else {
            this = com.a.b.d.zp.a(this.comparator());
        }
        return this;
    }

    public final int a(Object p4)
    {
        int v0_3;
        int v0_1 = this.b.c(p4);
        if (v0_1 != -1) {
            v0_3 = this.c[(v0_1 + this.e)];
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final com.a.b.d.ma a(Object p5, com.a.b.d.ce p6)
    {
        com.a.b.d.ma v0_1;
        if (com.a.b.b.cn.a(p6) != com.a.b.d.ce.b) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return this.a(0, this.b.e(p5, v0_1));
    }

    final com.a.b.d.xd a(int p4)
    {
        return com.a.b.d.xe.a(this.b.f().get(p4), this.c[(this.e + p4)]);
    }

    public final com.a.b.d.ma b(Object p4, com.a.b.d.ce p5)
    {
        com.a.b.d.ma v0_1;
        if (com.a.b.b.cn.a(p5) != com.a.b.d.ce.b) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return this.a(this.b.f(p4, v0_1), this.f);
    }

    public final com.a.b.d.me b()
    {
        return this.b;
    }

    public final synthetic com.a.b.d.abn c(Object p2, com.a.b.d.ce p3)
    {
        return this.b(p2, p3);
    }

    public final synthetic com.a.b.d.abn d(Object p2, com.a.b.d.ce p3)
    {
        return this.a(p2, p3);
    }

    public final bridge synthetic java.util.NavigableSet e_()
    {
        return this.b;
    }

    public final com.a.b.d.xd h()
    {
        return this.a(0);
    }

    final boolean h_()
    {
        if ((this.e <= 0) && (this.f >= this.c.length)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final com.a.b.d.xd i()
    {
        return this.a((this.f - 1));
    }

    public final bridge synthetic java.util.SortedSet n()
    {
        return this.b;
    }

    public final bridge synthetic java.util.Set n_()
    {
        return this.b;
    }

    public final int size()
    {
        return com.a.b.l.q.b((this.d[(this.e + this.f)] - this.d[this.e]));
    }
}
