package com.a.b.d;
 class pb extends java.util.AbstractList {
    final java.util.List a;
    final int b;

    pb(java.util.List p1, int p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private java.util.List a(int p4)
    {
        com.a.b.b.cn.a(p4, this.size());
        java.util.List v0_2 = (this.b * p4);
        return this.a.subList(v0_2, Math.min((this.b + v0_2), this.a.size()));
    }

    public synthetic Object get(int p4)
    {
        com.a.b.b.cn.a(p4, this.size());
        java.util.List v0_2 = (this.b * p4);
        return this.a.subList(v0_2, Math.min((this.b + v0_2), this.a.size()));
    }

    public boolean isEmpty()
    {
        return this.a.isEmpty();
    }

    public int size()
    {
        return com.a.b.j.g.a(this.a.size(), this.b, java.math.RoundingMode.CEILING);
    }
}
