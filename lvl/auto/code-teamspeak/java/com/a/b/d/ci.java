package com.a.b.d;
final class ci extends java.util.AbstractList implements java.util.RandomAccess {
    private final transient com.a.b.d.jl a;
    private final transient int[] b;

    ci(com.a.b.d.jl p5)
    {
        this.a = p5;
        int[] v2 = new int[(p5.size() + 1)];
        v2[p5.size()] = 1;
        try {
            int v1_1 = (p5.size() - 1);
        } catch (int v0) {
            throw new IllegalArgumentException("Cartesian product too large; must have size at most Integer.MAX_VALUE");
        }
        while (v1_1 >= 0) {
            v2[v1_1] = com.a.b.j.g.b(v2[(v1_1 + 1)], ((java.util.List) p5.get(v1_1)).size());
            v1_1--;
        }
        this.b = v2;
        return;
    }

    private int a(int p3, int p4)
    {
        return ((p3 / this.b[(p4 + 1)]) % ((java.util.List) this.a.get(p4)).size());
    }

    static synthetic int a(com.a.b.d.ci p2, int p3, int p4)
    {
        return ((p3 / p2.b[(p4 + 1)]) % ((java.util.List) p2.a.get(p4)).size());
    }

    private com.a.b.d.jl a(int p2)
    {
        com.a.b.b.cn.a(p2, this.size());
        return new com.a.b.d.cj(this, p2);
    }

    static synthetic com.a.b.d.jl a(com.a.b.d.ci p1)
    {
        return p1.a;
    }

    static java.util.List a(java.util.List p4)
    {
        com.a.b.d.jl v1_1 = new com.a.b.d.jn(p4.size());
        java.util.Iterator v2 = p4.iterator();
        while (v2.hasNext()) {
            com.a.b.d.jl v0_6 = com.a.b.d.jl.a(((java.util.List) v2.next()));
            if (!v0_6.isEmpty()) {
                v1_1.c(v0_6);
            } else {
                com.a.b.d.jl v0_3 = com.a.b.d.jl.d();
            }
            return v0_3;
        }
        v0_3 = new com.a.b.d.ci(v1_1.b());
        return v0_3;
    }

    public final boolean contains(Object p5)
    {
        int v0_3;
        if ((p5 instanceof java.util.List)) {
            if (((java.util.List) p5).size() == this.a.size()) {
                java.util.ListIterator v2_2 = ((java.util.List) p5).listIterator();
                while (v2_2.hasNext()) {
                    if (!((java.util.List) this.a.get(v2_2.nextIndex())).contains(v2_2.next())) {
                        v0_3 = 0;
                    }
                }
                v0_3 = 1;
            } else {
                v0_3 = 0;
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final synthetic Object get(int p2)
    {
        com.a.b.b.cn.a(p2, this.size());
        return new com.a.b.d.cj(this, p2);
    }

    public final int size()
    {
        return this.b[0];
    }
}
