package com.a.b.d;
public abstract class gg extends com.a.b.d.gj implements java.util.concurrent.BlockingDeque {

    protected gg()
    {
        return;
    }

    protected final synthetic java.util.Queue a()
    {
        return this.c();
    }

    protected final synthetic java.util.Collection b()
    {
        return this.c();
    }

    protected abstract java.util.concurrent.BlockingDeque c();

    protected final synthetic java.util.Deque d()
    {
        return this.c();
    }

    public int drainTo(java.util.Collection p2)
    {
        return this.c().drainTo(p2);
    }

    public int drainTo(java.util.Collection p2, int p3)
    {
        return this.c().drainTo(p2, p3);
    }

    protected final synthetic Object k_()
    {
        return this.c();
    }

    public boolean offer(Object p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        return this.c().offer(p3, p4, p6);
    }

    public boolean offerFirst(Object p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        return this.c().offerFirst(p3, p4, p6);
    }

    public boolean offerLast(Object p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        return this.c().offerLast(p3, p4, p6);
    }

    public Object poll(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.c().poll(p2, p4);
    }

    public Object pollFirst(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.c().pollFirst(p2, p4);
    }

    public Object pollLast(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.c().pollLast(p2, p4);
    }

    public void put(Object p2)
    {
        this.c().put(p2);
        return;
    }

    public void putFirst(Object p2)
    {
        this.c().putFirst(p2);
        return;
    }

    public void putLast(Object p2)
    {
        this.c().putLast(p2);
        return;
    }

    public int remainingCapacity()
    {
        return this.c().remainingCapacity();
    }

    public Object take()
    {
        return this.c().take();
    }

    public Object takeFirst()
    {
        return this.c().takeFirst();
    }

    public Object takeLast()
    {
        return this.c().takeLast();
    }
}
