package com.a.b.d;
final class op implements java.util.Iterator {
    final java.util.Set a;
    com.a.b.d.or b;
    com.a.b.d.or c;
    int d;
    final synthetic com.a.b.d.oj e;

    private op(com.a.b.d.oj p2)
    {
        this.e = p2;
        this.a = com.a.b.d.aad.a(this.e.p().size());
        this.b = com.a.b.d.oj.c(this.e);
        this.d = com.a.b.d.oj.a(this.e);
        return;
    }

    synthetic op(com.a.b.d.oj p1, byte p2)
    {
        this(p1);
        return;
    }

    private void a()
    {
        if (com.a.b.d.oj.a(this.e) == this.d) {
            return;
        } else {
            throw new java.util.ConcurrentModificationException();
        }
    }

    public final boolean hasNext()
    {
        int v0_1;
        this.a();
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final Object next()
    {
        this.a();
        com.a.b.d.oj.e(this.b);
        this.c = this.b;
        this.a.add(this.c.a);
        do {
            this.b = this.b.c;
        } while((this.b != null) && (!this.a.add(this.b.a)));
        return this.c.a;
    }

    public final void remove()
    {
        int v0_1;
        this.a();
        if (this.c == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        com.a.b.d.oj.a(this.e, this.c.a);
        this.c = 0;
        this.d = com.a.b.d.oj.a(this.e);
        return;
    }
}
