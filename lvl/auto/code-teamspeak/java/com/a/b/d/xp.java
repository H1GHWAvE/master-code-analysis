package com.a.b.d;
abstract class xp extends com.a.b.d.aan {

    xp()
    {
        return;
    }

    abstract com.a.b.d.xc a();

    public void clear()
    {
        this.a().clear();
        return;
    }

    public boolean contains(Object p2)
    {
        return this.a().contains(p2);
    }

    public boolean containsAll(java.util.Collection p2)
    {
        return this.a().containsAll(p2);
    }

    public boolean isEmpty()
    {
        return this.a().isEmpty();
    }

    public java.util.Iterator iterator()
    {
        return new com.a.b.d.xq(this, this.a().a().iterator());
    }

    public boolean remove(Object p3)
    {
        int v0_2;
        int v0_1 = this.a().a(p3);
        if (v0_1 <= 0) {
            v0_2 = 0;
        } else {
            this.a().b(p3, v0_1);
            v0_2 = 1;
        }
        return v0_2;
    }

    public int size()
    {
        return this.a().a().size();
    }
}
