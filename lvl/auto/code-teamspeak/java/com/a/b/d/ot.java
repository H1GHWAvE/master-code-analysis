package com.a.b.d;
final class ot implements java.util.ListIterator {
    final Object a;
    int b;
    com.a.b.d.or c;
    com.a.b.d.or d;
    com.a.b.d.or e;
    final synthetic com.a.b.d.oj f;

    ot(com.a.b.d.oj p2, Object p3)
    {
        com.a.b.d.or v0_3;
        this.f = p2;
        this.a = p3;
        com.a.b.d.or v0_2 = ((com.a.b.d.oq) com.a.b.d.oj.d(p2).get(p3));
        if (v0_2 != null) {
            v0_3 = v0_2.a;
        } else {
            v0_3 = 0;
        }
        this.c = v0_3;
        return;
    }

    public ot(com.a.b.d.oj p5, Object p6, int p7)
    {
        int v1;
        this.f = p5;
        int v0_2 = ((com.a.b.d.oq) com.a.b.d.oj.d(p5).get(p6));
        if (v0_2 != 0) {
            v1 = v0_2.c;
        } else {
            v1 = 0;
        }
        com.a.b.b.cn.b(p7, v1);
        if (p7 < (v1 / 2)) {
            int v0_3;
            if (v0_2 != 0) {
                v0_3 = v0_2.a;
            } else {
                v0_3 = 0;
            }
            this.c = v0_3;
            while(true) {
                int v0_4 = (p7 - 1);
                if (p7 <= 0) {
                    break;
                }
                this.next();
                p7 = v0_4;
            }
        } else {
            int v0_5;
            if (v0_2 != 0) {
                v0_5 = v0_2.b;
            } else {
                v0_5 = 0;
            }
            this.e = v0_5;
            this.b = v1;
            while(true) {
                int v0_6 = (p7 + 1);
                if (p7 >= v1) {
                    break;
                }
                this.previous();
                p7 = v0_6;
            }
        }
        this.a = p6;
        this.d = 0;
        return;
    }

    public final void add(Object p4)
    {
        this.e = com.a.b.d.oj.a(this.f, this.a, p4, this.c);
        this.b = (this.b + 1);
        this.d = 0;
        return;
    }

    public final boolean hasNext()
    {
        int v0_1;
        if (this.c == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean hasPrevious()
    {
        int v0_1;
        if (this.e == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final Object next()
    {
        com.a.b.d.oj.e(this.c);
        Object v0_1 = this.c;
        this.d = v0_1;
        this.e = v0_1;
        this.c = this.c.e;
        this.b = (this.b + 1);
        return this.d.b;
    }

    public final int nextIndex()
    {
        return this.b;
    }

    public final Object previous()
    {
        com.a.b.d.oj.e(this.e);
        Object v0_1 = this.e;
        this.d = v0_1;
        this.c = v0_1;
        this.e = this.e.f;
        this.b = (this.b - 1);
        return this.d.b;
    }

    public final int previousIndex()
    {
        return (this.b - 1);
    }

    public final void remove()
    {
        int v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        if (this.d == this.c) {
            this.c = this.d.e;
        } else {
            this.e = this.d.f;
            this.b = (this.b - 1);
        }
        com.a.b.d.oj.a(this.f, this.d);
        this.d = 0;
        return;
    }

    public final void set(Object p2)
    {
        com.a.b.d.or v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1);
        this.d.b = p2;
        return;
    }
}
