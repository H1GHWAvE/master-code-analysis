package com.a.b.d;
public final class adx {
    private static final com.a.b.b.bj a;

    static adx()
    {
        com.a.b.d.adx.a = new com.a.b.d.ady();
        return;
    }

    private adx()
    {
        return;
    }

    static synthetic com.a.b.b.bj a()
    {
        return com.a.b.d.adx.a;
    }

    public static com.a.b.d.adv a(com.a.b.d.adv p1)
    {
        com.a.b.d.aef v0_2;
        if (!(p1 instanceof com.a.b.d.aef)) {
            v0_2 = new com.a.b.d.aef(p1);
        } else {
            v0_2 = ((com.a.b.d.aef) p1).a;
        }
        return v0_2;
    }

    private static com.a.b.d.adv a(com.a.b.d.adv p1, com.a.b.b.bj p2)
    {
        return new com.a.b.d.aeb(p1, p2);
    }

    private static com.a.b.d.adv a(java.util.Map p1, com.a.b.b.dz p2)
    {
        com.a.b.b.cn.a(p1.isEmpty());
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.abx(p1, p2);
    }

    public static com.a.b.d.adw a(Object p1, Object p2, Object p3)
    {
        return new com.a.b.d.aea(p1, p2, p3);
    }

    private static com.a.b.d.zy a(com.a.b.d.zy p1)
    {
        return new com.a.b.d.aeh(p1);
    }

    static boolean a(com.a.b.d.adv p2, Object p3)
    {
        int v0_1;
        if (p3 != p2) {
            if (!(p3 instanceof com.a.b.d.adv)) {
                v0_1 = 0;
            } else {
                v0_1 = p2.e().equals(((com.a.b.d.adv) p3).e());
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static com.a.b.b.bj b()
    {
        return com.a.b.d.adx.a;
    }

    private static com.a.b.d.adv b(com.a.b.d.adv p1)
    {
        return new com.a.b.d.aei(p1);
    }
}
