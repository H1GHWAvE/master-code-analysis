package com.a.b.d;
 class pf extends java.util.AbstractList {
    final java.util.List a;

    pf(java.util.List p2)
    {
        this.a = ((java.util.List) com.a.b.b.cn.a(p2));
        return;
    }

    private int a(int p2)
    {
        int v0_0 = this.size();
        com.a.b.b.cn.a(p2, v0_0);
        return ((v0_0 - 1) - p2);
    }

    static synthetic int a(com.a.b.d.pf p1, int p2)
    {
        return p1.b(p2);
    }

    private java.util.List a()
    {
        return this.a;
    }

    private int b(int p2)
    {
        int v0_0 = this.size();
        com.a.b.b.cn.b(p2, v0_0);
        return (v0_0 - p2);
    }

    public void add(int p3, Object p4)
    {
        this.a.add(this.b(p3), p4);
        return;
    }

    public void clear()
    {
        this.a.clear();
        return;
    }

    public Object get(int p3)
    {
        return this.a.get(this.a(p3));
    }

    public java.util.Iterator iterator()
    {
        return this.listIterator();
    }

    public java.util.ListIterator listIterator(int p3)
    {
        return new com.a.b.d.pg(this, this.a.listIterator(this.b(p3)));
    }

    public Object remove(int p3)
    {
        return this.a.remove(this.a(p3));
    }

    protected void removeRange(int p2, int p3)
    {
        this.subList(p2, p3).clear();
        return;
    }

    public Object set(int p3, Object p4)
    {
        return this.a.set(this.a(p3), p4);
    }

    public int size()
    {
        return this.a.size();
    }

    public java.util.List subList(int p4, int p5)
    {
        com.a.b.d.pf v0_3;
        com.a.b.b.cn.a(p4, p5, this.size());
        com.a.b.d.pf v0_2 = this.a.subList(this.b(p5), this.b(p4));
        if (!(v0_2 instanceof com.a.b.d.jl)) {
            if (!(v0_2 instanceof com.a.b.d.pf)) {
                if (!(v0_2 instanceof java.util.RandomAccess)) {
                    v0_3 = new com.a.b.d.pf(v0_2);
                } else {
                    v0_3 = new com.a.b.d.pe(v0_2);
                }
            } else {
                v0_3 = ((com.a.b.d.pf) v0_2).a;
            }
        } else {
            v0_3 = ((com.a.b.d.jl) v0_2).e();
        }
        return v0_3;
    }
}
