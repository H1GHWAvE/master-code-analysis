package com.a.b.d;
public final class fe extends com.a.b.d.a {
    private static final long c;
    transient Class b;

    private fe(Class p3)
    {
        this(com.a.b.d.agm.a(new java.util.EnumMap(p3)), com.a.b.d.sz.a(((Enum[]) p3.getEnumConstants()).length));
        this.b = p3;
        return;
    }

    private static com.a.b.d.fe a(Class p1)
    {
        return new com.a.b.d.fe(p1);
    }

    private static com.a.b.d.fe a(java.util.Map p2)
    {
        com.a.b.d.fe v1_1 = new com.a.b.d.fe(com.a.b.d.fd.a(p2));
        v1_1.putAll(p2);
        return v1_1;
    }

    private static Enum a(Enum p1)
    {
        return ((Enum) com.a.b.b.cn.a(p1));
    }

    private Object a(Enum p2, Object p3)
    {
        return super.put(p2, p3);
    }

    private void a(java.io.ObjectInputStream p4)
    {
        p4.defaultReadObject();
        this.b = ((Class) p4.readObject());
        this.a(com.a.b.d.agm.a(new java.util.EnumMap(this.b)), new java.util.HashMap(((((Enum[]) this.b.getEnumConstants()).length * 3) / 2)));
        com.a.b.d.zz.a(this, p4);
        return;
    }

    private void a(java.io.ObjectOutputStream p2)
    {
        p2.defaultWriteObject();
        p2.writeObject(this.b);
        com.a.b.d.zz.a(this, p2);
        return;
    }

    private Object b(Enum p2, Object p3)
    {
        return super.a(p2, p3);
    }

    private Class d()
    {
        return this.b;
    }

    final bridge synthetic Object a(Object p2)
    {
        return ((Enum) com.a.b.b.cn.a(((Enum) p2)));
    }

    public final bridge synthetic Object a(Object p2, Object p3)
    {
        return super.a(((Enum) p2), p3);
    }

    public final bridge synthetic com.a.b.d.bw b()
    {
        return super.b();
    }

    public final bridge synthetic void clear()
    {
        super.clear();
        return;
    }

    public final bridge synthetic boolean containsValue(Object p2)
    {
        return super.containsValue(p2);
    }

    public final bridge synthetic java.util.Set entrySet()
    {
        return super.entrySet();
    }

    public final bridge synthetic java.util.Set j_()
    {
        return super.j_();
    }

    public final bridge synthetic java.util.Set keySet()
    {
        return super.keySet();
    }

    public final bridge synthetic Object put(Object p2, Object p3)
    {
        return super.put(((Enum) p2), p3);
    }

    public final bridge synthetic void putAll(java.util.Map p1)
    {
        super.putAll(p1);
        return;
    }

    public final bridge synthetic Object remove(Object p2)
    {
        return super.remove(p2);
    }
}
