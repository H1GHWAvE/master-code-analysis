package com.a.b.d;
final enum class ry extends java.lang.Enum implements com.a.b.d.rz {
    public static final enum com.a.b.d.ry a;
    private static final synthetic com.a.b.d.ry[] b;

    static ry()
    {
        com.a.b.d.ry.a = new com.a.b.d.ry("INSTANCE");
        com.a.b.d.ry[] v0_3 = new com.a.b.d.ry[1];
        v0_3[0] = com.a.b.d.ry.a;
        com.a.b.d.ry.b = v0_3;
        return;
    }

    private ry(String p2)
    {
        this(p2, 0);
        return;
    }

    public static com.a.b.d.ry valueOf(String p1)
    {
        return ((com.a.b.d.ry) Enum.valueOf(com.a.b.d.ry, p1));
    }

    public static com.a.b.d.ry[] values()
    {
        return ((com.a.b.d.ry[]) com.a.b.d.ry.b.clone());
    }

    public final com.a.b.d.sr a()
    {
        return 0;
    }

    public final void a(long p1)
    {
        return;
    }

    public final void a(com.a.b.d.rz p1)
    {
        return;
    }

    public final void a(com.a.b.d.sr p1)
    {
        return;
    }

    public final com.a.b.d.rz b()
    {
        return 0;
    }

    public final void b(com.a.b.d.rz p1)
    {
        return;
    }

    public final int c()
    {
        return 0;
    }

    public final void c(com.a.b.d.rz p1)
    {
        return;
    }

    public final Object d()
    {
        return 0;
    }

    public final void d(com.a.b.d.rz p1)
    {
        return;
    }

    public final long e()
    {
        return 0;
    }

    public final com.a.b.d.rz f()
    {
        return this;
    }

    public final com.a.b.d.rz g()
    {
        return this;
    }

    public final com.a.b.d.rz h()
    {
        return this;
    }

    public final com.a.b.d.rz i()
    {
        return this;
    }
}
