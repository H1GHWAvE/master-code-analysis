package com.a.b.d;
final class fn extends com.a.b.d.vb {
    final synthetic com.a.b.d.fj a;

    fn(com.a.b.d.fj p1, java.util.Map p2)
    {
        this.a = p1;
        this(p2);
        return;
    }

    public final boolean remove(Object p7)
    {
        int v0_7;
        if (!(p7 instanceof java.util.Collection)) {
            v0_7 = 0;
        } else {
            java.util.Iterator v2 = this.a.a.a.b().entrySet().iterator();
            while (v2.hasNext()) {
                int v0_9 = ((java.util.Map$Entry) v2.next());
                java.util.Collection v1_2 = com.a.b.d.fi.a(((java.util.Collection) v0_9.getValue()), new com.a.b.d.fr(this.a.a, v0_9.getKey()));
                if ((!v1_2.isEmpty()) && (((java.util.Collection) p7).equals(v1_2))) {
                    if (v1_2.size() != ((java.util.Collection) v0_9.getValue()).size()) {
                        v1_2.clear();
                    } else {
                        v2.remove();
                    }
                    v0_7 = 1;
                }
            }
        }
        return v0_7;
    }

    public final boolean removeAll(java.util.Collection p3)
    {
        return this.a.a.a(com.a.b.d.sz.b(com.a.b.b.cp.a(p3)));
    }

    public final boolean retainAll(java.util.Collection p3)
    {
        return this.a.a.a(com.a.b.d.sz.b(com.a.b.b.cp.a(com.a.b.b.cp.a(p3))));
    }
}
