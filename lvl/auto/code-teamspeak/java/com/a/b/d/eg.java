package com.a.b.d;
abstract class eg extends com.a.b.d.jt {
    private final int a;

    eg(int p1)
    {
        this.a = p1;
        return;
    }

    private Object b(int p2)
    {
        return this.a().g().f().get(p2);
    }

    private boolean i()
    {
        int v0_1;
        if (this.a != this.a().size()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    abstract com.a.b.d.jt a();

    abstract Object a();

    final com.a.b.d.lo c()
    {
        com.a.b.d.lo v0_1;
        if (this.a != this.a().size()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.d.lo v0_2;
        if (v0_1 == null) {
            v0_2 = super.c();
        } else {
            v0_2 = this.a().g();
        }
        return v0_2;
    }

    final com.a.b.d.lo d()
    {
        return new com.a.b.d.eh(this);
    }

    public Object get(Object p2)
    {
        Object v0_4;
        Object v0_2 = ((Integer) this.a().get(p2));
        if (v0_2 != null) {
            v0_4 = this.a(v0_2.intValue());
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    public int size()
    {
        return this.a;
    }
}
