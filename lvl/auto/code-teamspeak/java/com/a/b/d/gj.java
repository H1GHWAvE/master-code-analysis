package com.a.b.d;
public abstract class gj extends com.a.b.d.hh implements java.util.Deque {

    protected gj()
    {
        return;
    }

    protected synthetic java.util.Queue a()
    {
        return this.d();
    }

    public void addFirst(Object p2)
    {
        this.d().addFirst(p2);
        return;
    }

    public void addLast(Object p2)
    {
        this.d().addLast(p2);
        return;
    }

    protected synthetic java.util.Collection b()
    {
        return this.d();
    }

    protected abstract java.util.Deque d();

    public java.util.Iterator descendingIterator()
    {
        return this.d().descendingIterator();
    }

    public Object getFirst()
    {
        return this.d().getFirst();
    }

    public Object getLast()
    {
        return this.d().getLast();
    }

    protected synthetic Object k_()
    {
        return this.d();
    }

    public boolean offerFirst(Object p2)
    {
        return this.d().offerFirst(p2);
    }

    public boolean offerLast(Object p2)
    {
        return this.d().offerLast(p2);
    }

    public Object peekFirst()
    {
        return this.d().peekFirst();
    }

    public Object peekLast()
    {
        return this.d().peekLast();
    }

    public Object pollFirst()
    {
        return this.d().pollFirst();
    }

    public Object pollLast()
    {
        return this.d().pollLast();
    }

    public Object pop()
    {
        return this.d().pop();
    }

    public void push(Object p2)
    {
        this.d().push(p2);
        return;
    }

    public Object removeFirst()
    {
        return this.d().removeFirst();
    }

    public boolean removeFirstOccurrence(Object p2)
    {
        return this.d().removeFirstOccurrence(p2);
    }

    public Object removeLast()
    {
        return this.d().removeLast();
    }

    public boolean removeLastOccurrence(Object p2)
    {
        return this.d().removeLastOccurrence(p2);
    }
}
