package com.a.b.d;
final class aab {
    private final reflect.Field a;

    private aab(reflect.Field p2)
    {
        this.a = p2;
        p2.setAccessible(1);
        return;
    }

    synthetic aab(reflect.Field p1, byte p2)
    {
        this(p1);
        return;
    }

    final void a(Object p3, int p4)
    {
        try {
            this.a.set(p3, Integer.valueOf(p4));
            return;
        } catch (IllegalAccessException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    final void a(Object p3, Object p4)
    {
        try {
            this.a.set(p3, p4);
            return;
        } catch (IllegalAccessException v0_1) {
            throw new AssertionError(v0_1);
        }
    }
}
