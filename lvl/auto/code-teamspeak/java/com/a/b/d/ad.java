package com.a.b.d;
 class ad extends com.a.b.d.ab implements java.util.List {
    final synthetic com.a.b.d.n g;

    ad(com.a.b.d.n p1, Object p2, java.util.List p3, com.a.b.d.ab p4)
    {
        this.g = p1;
        this(p1, p2, p3, p4);
        return;
    }

    private java.util.List d()
    {
        return ((java.util.List) this.c);
    }

    public void add(int p3, Object p4)
    {
        this.a();
        boolean v1 = this.c.isEmpty();
        ((java.util.List) this.c).add(p3, p4);
        com.a.b.d.n.c(this.g);
        if (v1) {
            this.c();
        }
        return;
    }

    public boolean addAll(int p5, java.util.Collection p6)
    {
        boolean v0_3;
        if (!p6.isEmpty()) {
            int v1 = this.size();
            v0_3 = ((java.util.List) this.c).addAll(p5, p6);
            if (v0_3) {
                com.a.b.d.n.a(this.g, (this.c.size() - v1));
                if (v1 == 0) {
                    this.c();
                }
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public Object get(int p2)
    {
        this.a();
        return ((java.util.List) this.c).get(p2);
    }

    public int indexOf(Object p2)
    {
        this.a();
        return ((java.util.List) this.c).indexOf(p2);
    }

    public int lastIndexOf(Object p2)
    {
        this.a();
        return ((java.util.List) this.c).lastIndexOf(p2);
    }

    public java.util.ListIterator listIterator()
    {
        this.a();
        return new com.a.b.d.ae(this);
    }

    public java.util.ListIterator listIterator(int p2)
    {
        this.a();
        return new com.a.b.d.ae(this, p2);
    }

    public Object remove(int p3)
    {
        this.a();
        Object v0_2 = ((java.util.List) this.c).remove(p3);
        com.a.b.d.n.b(this.g);
        this.b();
        return v0_2;
    }

    public Object set(int p2, Object p3)
    {
        this.a();
        return ((java.util.List) this.c).set(p2, p3);
    }

    public java.util.List subList(int p5, int p6)
    {
        this.a();
        com.a.b.d.n v1 = this.g;
        Object v2 = this.b;
        java.util.List v0_2 = ((java.util.List) this.c).subList(p5, p6);
        if (this.d != null) {
            this = this.d;
        }
        return com.a.b.d.n.a(v1, v2, v0_2, this);
    }
}
