package com.a.b.d;
final class al implements java.util.Iterator {
    final java.util.Iterator a;
    java.util.Map$Entry b;
    int c;
    boolean d;
    final synthetic com.a.b.d.ai e;

    al(com.a.b.d.ai p2)
    {
        this.e = p2;
        this.a = com.a.b.d.ai.a(p2).entrySet().iterator();
        return;
    }

    public final boolean hasNext()
    {
        if ((this.c <= 0) && (!this.a.hasNext())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final Object next()
    {
        if (this.c == 0) {
            this.b = ((java.util.Map$Entry) this.a.next());
            this.c = ((com.a.b.d.dv) this.b.getValue()).a;
        }
        this.c = (this.c - 1);
        this.d = 1;
        return this.b.getKey();
    }

    public final void remove()
    {
        com.a.b.b.cn.b(this.d, "no calls to next() since the last call to remove()");
        if (((com.a.b.d.dv) this.b.getValue()).a > 0) {
            if (((com.a.b.d.dv) this.b.getValue()).a(-1) == 0) {
                this.a.remove();
            }
            com.a.b.d.ai.b(this.e);
            this.d = 0;
            return;
        } else {
            throw new java.util.ConcurrentModificationException();
        }
    }
}
