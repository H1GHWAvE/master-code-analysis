package com.a.b.d;
final class dn {

    private dn()
    {
        return;
    }

    static java.util.Collection a(java.util.Collection p1, com.a.b.d.dm p2)
    {
        com.a.b.d.do v0_4;
        if (!(p1 instanceof java.util.SortedSet)) {
            if (!(p1 instanceof java.util.Set)) {
                if (!(p1 instanceof java.util.List)) {
                    v0_4 = new com.a.b.d.do(p1, p2);
                } else {
                    v0_4 = com.a.b.d.dn.a(((java.util.List) p1), p2);
                }
            } else {
                v0_4 = new com.a.b.d.ds(((java.util.Set) p1), p2);
            }
        } else {
            v0_4 = com.a.b.d.dn.a(((java.util.SortedSet) p1), p2);
        }
        return v0_4;
    }

    public static java.util.List a(java.util.List p1, com.a.b.d.dm p2)
    {
        com.a.b.d.dp v0_2;
        if (!(p1 instanceof java.util.RandomAccess)) {
            v0_2 = new com.a.b.d.dp(p1, p2);
        } else {
            v0_2 = new com.a.b.d.dr(p1, p2);
        }
        return v0_2;
    }

    static synthetic java.util.ListIterator a(java.util.ListIterator p1, com.a.b.d.dm p2)
    {
        return new com.a.b.d.dq(p1, p2);
    }

    private static java.util.Set a(java.util.Set p1, com.a.b.d.dm p2)
    {
        return new com.a.b.d.ds(p1, p2);
    }

    public static java.util.SortedSet a(java.util.SortedSet p1, com.a.b.d.dm p2)
    {
        return new com.a.b.d.dt(p1, p2);
    }

    static synthetic java.util.Collection b(java.util.Collection p3, com.a.b.d.dm p4)
    {
        java.util.ArrayList v0 = com.a.b.d.ov.a(p3);
        java.util.Iterator v1 = v0.iterator();
        while (v1.hasNext()) {
            p4.a(v1.next());
        }
        return v0;
    }

    private static java.util.ListIterator b(java.util.ListIterator p1, com.a.b.d.dm p2)
    {
        return new com.a.b.d.dq(p1, p2);
    }

    private static java.util.Collection c(java.util.Collection p1, com.a.b.d.dm p2)
    {
        return new com.a.b.d.do(p1, p2);
    }

    private static java.util.Collection d(java.util.Collection p3, com.a.b.d.dm p4)
    {
        java.util.ArrayList v0 = com.a.b.d.ov.a(p3);
        java.util.Iterator v1 = v0.iterator();
        while (v1.hasNext()) {
            p4.a(v1.next());
        }
        return v0;
    }
}
