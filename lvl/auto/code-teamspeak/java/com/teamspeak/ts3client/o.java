package com.teamspeak.ts3client;
final class o implements android.view.View$OnTouchListener {
    final synthetic com.teamspeak.ts3client.ConnectionBackground a;
    private int b;
    private int c;
    private float d;
    private float e;
    private long f;
    private long g;

    o(com.teamspeak.ts3client.ConnectionBackground p1)
    {
        this.a = p1;
        return;
    }

    public final boolean onTouch(android.view.View p9, android.view.MotionEvent p10)
    {
        int v0 = 1;
        switch (p10.getAction()) {
            case 0:
                if ((this.a.a <= 0) || ((System.nanoTime() - this.a.a) >= 500000000)) {
                    this.f = System.nanoTime();
                    com.teamspeak.ts3client.ConnectionBackground.d(this.a).a.a.a(com.teamspeak.ts3client.ConnectionBackground.d(this.a).a.e, com.teamspeak.ts3client.jni.d.k, 0);
                    com.teamspeak.ts3client.ConnectionBackground.d(this.a).a.b.d();
                    this.a.c = 1;
                    this.a.d = this.a.getResources().getConfiguration().orientation;
                } else {
                    this.a.b = 1;
                    this.b = com.teamspeak.ts3client.ConnectionBackground.a(this.a).x;
                    this.c = com.teamspeak.ts3client.ConnectionBackground.a(this.a).y;
                    this.d = p10.getRawX();
                    this.e = p10.getRawY();
                    com.teamspeak.ts3client.ConnectionBackground.b(this.a).performHapticFeedback(0);
                    com.teamspeak.ts3client.ConnectionBackground.b(this.a).setAlpha(125);
                }
                break;
            case 1:
                if (!this.a.b) {
                    this.g = System.nanoTime();
                    com.teamspeak.ts3client.ConnectionBackground.d(this.a).a.a.a(com.teamspeak.ts3client.ConnectionBackground.d(this.a).a.e, com.teamspeak.ts3client.jni.d.k, 1);
                    com.teamspeak.ts3client.ConnectionBackground.d(this.a).a.b.f();
                    this.a.c = 0;
                    if ((this.g - this.f) >= 500000000) {
                    } else {
                        this.a.a = System.nanoTime();
                    }
                } else {
                    this.a.b = 0;
                    if (this.a.getResources().getConfiguration().orientation != 2) {
                        if (this.a.getResources().getConfiguration().orientation != 1) {
                            com.teamspeak.ts3client.ConnectionBackground.d(this.a).e.edit().putInt("overlay_last_x_u", com.teamspeak.ts3client.ConnectionBackground.a(this.a).x).putInt("overlay_last_y_u", com.teamspeak.ts3client.ConnectionBackground.a(this.a).y).commit();
                        } else {
                            com.teamspeak.ts3client.ConnectionBackground.d(this.a).e.edit().putInt("overlay_last_x_p", com.teamspeak.ts3client.ConnectionBackground.a(this.a).x).putInt("overlay_last_y_p", com.teamspeak.ts3client.ConnectionBackground.a(this.a).y).commit();
                        }
                    } else {
                        com.teamspeak.ts3client.ConnectionBackground.d(this.a).e.edit().putInt("overlay_last_x_l", com.teamspeak.ts3client.ConnectionBackground.a(this.a).x).putInt("overlay_last_y_l", com.teamspeak.ts3client.ConnectionBackground.a(this.a).y).commit();
                    }
                    com.teamspeak.ts3client.ConnectionBackground.b(this.a).setAlpha(255);
                }
                break;
            case 2:
                if (!this.a.b) {
                } else {
                    com.teamspeak.ts3client.ConnectionBackground.a(this.a).x = (this.b + ((int) (p10.getRawX() - this.d)));
                    com.teamspeak.ts3client.ConnectionBackground.a(this.a).y = (this.c + ((int) (p10.getRawY() - this.e)));
                    com.teamspeak.ts3client.ConnectionBackground.c(this.a).updateViewLayout(com.teamspeak.ts3client.ConnectionBackground.b(this.a), com.teamspeak.ts3client.ConnectionBackground.a(this.a));
                }
                break;
            default:
                v0 = 0;
        }
        return v0;
    }
}
