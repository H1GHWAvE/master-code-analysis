package com.teamspeak.ts3client.e;
final class f implements android.view.View$OnClickListener {
    final synthetic int a;
    final synthetic com.teamspeak.ts3client.e.e b;

    f(com.teamspeak.ts3client.e.e p1, int p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onClick(android.view.View p5)
    {
        android.app.AlertDialog v0_2 = new android.app.AlertDialog$Builder(com.teamspeak.ts3client.e.e.a(this.b)).create();
        com.teamspeak.ts3client.data.d.q.a(v0_2);
        v0_2.setTitle(com.teamspeak.ts3client.data.e.a.a("identity.info"));
        v0_2.setMessage(com.teamspeak.ts3client.data.e.a.a("identity.text"));
        v0_2.setButton(-1, com.teamspeak.ts3client.data.e.a.a("button.delete"), new com.teamspeak.ts3client.e.g(this));
        v0_2.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.e.h(this, v0_2));
        v0_2.setCancelable(0);
        v0_2.show();
        return;
    }
}
