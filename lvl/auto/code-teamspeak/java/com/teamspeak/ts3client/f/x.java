package com.teamspeak.ts3client.f;
final class x extends android.support.v4.app.ax {
    final synthetic com.teamspeak.ts3client.f.v at;

    private x(com.teamspeak.ts3client.f.v p1)
    {
        this.at = p1;
        return;
    }

    synthetic x(com.teamspeak.ts3client.f.v p1, byte p2)
    {
        this(p1);
        return;
    }

    private android.view.View a(String p10, String p11)
    {
        android.widget.RelativeLayout v1_1 = new android.widget.RelativeLayout(this.i());
        v1_1.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        v1_1.setPadding(8, 18, 0, 18);
        v1_1.setBackgroundResource(2130837686);
        com.teamspeak.ts3client.f.aa v2_2 = new android.widget.TextView(this.i());
        android.view.View v3_3 = ((android.view.LayoutInflater) this.h().getSystemService("layout_inflater")).inflate(2130903109, 0);
        android.widget.CheckBox v0_11 = ((android.widget.CheckBox) v3_3.findViewById(2131493263));
        v2_2.setText(p11);
        v2_2.setTextSize(1101004800);
        v2_2.setTypeface(0, 1);
        v2_2.setId(1);
        v0_11.setChecked(Boolean.valueOf(com.teamspeak.ts3client.Ts3Application.a().e.getBoolean(p10, 0)).booleanValue());
        v0_11.setContentDescription(new StringBuilder().append(com.teamspeak.ts3client.f.v.c(this.at)).append(" Checkbox").toString());
        v0_11.setOnCheckedChangeListener(new com.teamspeak.ts3client.f.z(this, p10));
        v1_1.addView(v2_2, new android.widget.RelativeLayout$LayoutParams(-2, -2));
        com.teamspeak.ts3client.f.aa v2_4 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_4.addRule(11);
        v1_1.addView(v3_3, v2_4);
        v1_1.setClickable(1);
        v1_1.setOnClickListener(new com.teamspeak.ts3client.f.aa(this, v0_11));
        return v1_1;
    }

    public final android.view.View a(android.view.LayoutInflater p11, android.view.ViewGroup p12)
    {
        android.widget.ScrollView v3_1 = new android.widget.ScrollView(this.i());
        v3_1.setScrollbarFadingEnabled(0);
        android.widget.LinearLayout v4_1 = new android.widget.LinearLayout(this.i());
        v4_1.setOrientation(1);
        android.widget.Button v0_5 = new android.widget.TextView(this.i());
        v0_5.setPadding(8, 18, 0, 18);
        v0_5.setText(com.teamspeak.ts3client.f.v.a(this.at));
        v4_1.addView(v0_5);
        java.util.Iterator v5_1 = com.teamspeak.ts3client.f.v.b(this.at).entrySet().iterator();
        while (v5_1.hasNext()) {
            android.widget.Button v0_14 = ((java.util.Map$Entry) v5_1.next());
            com.teamspeak.ts3client.f.y v1_12 = ((String) v0_14.getKey());
            android.widget.Button v0_16 = ((String) v0_14.getValue());
            android.widget.RelativeLayout v6_2 = new android.widget.RelativeLayout(this.i());
            v6_2.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
            v6_2.setPadding(8, 18, 0, 18);
            v6_2.setBackgroundResource(2130837686);
            int v7_3 = new android.widget.TextView(this.i());
            android.view.View v8_4 = ((android.view.LayoutInflater) this.h().getSystemService("layout_inflater")).inflate(2130903109, 0);
            android.widget.CheckBox v2_12 = ((android.widget.CheckBox) v8_4.findViewById(2131493263));
            v7_3.setText(v0_16);
            v7_3.setTextSize(1101004800);
            v7_3.setTypeface(0, 1);
            v7_3.setId(1);
            v2_12.setChecked(Boolean.valueOf(com.teamspeak.ts3client.Ts3Application.a().e.getBoolean(v1_12, 0)).booleanValue());
            v2_12.setContentDescription(new StringBuilder().append(com.teamspeak.ts3client.f.v.c(this.at)).append(" Checkbox").toString());
            v2_12.setOnCheckedChangeListener(new com.teamspeak.ts3client.f.z(this, v1_12));
            v6_2.addView(v7_3, new android.widget.RelativeLayout$LayoutParams(-2, -2));
            android.widget.Button v0_35 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
            v0_35.addRule(11);
            v6_2.addView(v8_4, v0_35);
            v6_2.setClickable(1);
            v6_2.setOnClickListener(new com.teamspeak.ts3client.f.aa(this, v2_12));
            v4_1.addView(v6_2);
        }
        this.j.setTitle(com.teamspeak.ts3client.f.v.c(this.at));
        android.widget.Button v0_12 = new android.widget.Button(this.i());
        v0_12.setText(com.teamspeak.ts3client.data.e.a.a("button.close"));
        v0_12.setOnClickListener(new com.teamspeak.ts3client.f.y(this));
        v4_1.addView(v0_12);
        v3_1.addView(v4_1);
        return v3_1;
    }
}
