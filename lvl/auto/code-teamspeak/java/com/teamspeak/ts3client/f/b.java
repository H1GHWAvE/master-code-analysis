package com.teamspeak.ts3client.f;
public final class b extends android.widget.RelativeLayout implements android.view.View$OnClickListener {
    private String a;
    private android.support.v4.app.bi b;
    private String c;

    public b(android.content.Context p8, String p9, String p10, android.support.v4.app.bi p11)
    {
        this(p8);
        this.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.setPadding(0, 18, 0, 18);
        this.setBackgroundResource(2130837686);
        this.b = p11;
        this.a = p9;
        this.c = p10;
        int v0_4 = new android.widget.TextView(p8);
        android.widget.TextView v1_2 = new android.widget.TextView(p8);
        v0_4.setText(this.a);
        v0_4.setTextSize(1101004800);
        v0_4.setTypeface(0, 1);
        v0_4.setId(1);
        v1_2.setText(this.c);
        v1_2.setTextSize(1092616192);
        v1_2.setTypeface(0, 2);
        v1_2.setId(2);
        android.widget.RelativeLayout$LayoutParams v2_8 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_8.addRule(10);
        this.addView(v0_4, v2_8);
        android.widget.RelativeLayout$LayoutParams v2_10 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_10.addRule(3, v0_4.getId());
        this.addView(v1_2, v2_10);
        this.setClickable(1);
        this.setOnClickListener(this);
        return;
    }

    public final void onClick(android.view.View p4)
    {
        int v0_0 = com.teamspeak.ts3client.a.a.y();
        if (!v0_0.at) {
            v0_0.a(this.b, "AudioSettings");
            p4.performHapticFeedback(1);
        }
        return;
    }
}
