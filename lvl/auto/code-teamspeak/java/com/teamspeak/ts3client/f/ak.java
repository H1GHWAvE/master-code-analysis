package com.teamspeak.ts3client.f;
public final class ak extends android.widget.RelativeLayout implements android.view.View$OnClickListener {
    private long a;
    private boolean b;
    private android.content.BroadcastReceiver c;
    private android.widget.SeekBar d;
    private android.widget.TextView e;
    private String f;
    private com.teamspeak.ts3client.f.am g;
    private android.support.v4.app.bi h;
    private String i;
    private String j;

    public ak(android.content.Context p8, String p9, String p10, String p11, android.support.v4.app.bi p12)
    {
        this(p8);
        this.c = new com.teamspeak.ts3client.f.al(this);
        this.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.setPadding(0, 18, 0, 18);
        this.h = p12;
        this.f = p10;
        this.j = p11;
        this.i = p9;
        this.setBackgroundResource(2130837686);
        int v0_6 = new android.widget.TextView(p8);
        android.widget.TextView v1_2 = new android.widget.TextView(p8);
        v0_6.setText(this.f);
        v0_6.setTextSize(1101004800);
        v0_6.setTypeface(0, 1);
        v0_6.setId(1);
        v1_2.setText(this.j);
        v1_2.setTextSize(1092616192);
        v1_2.setTypeface(0, 2);
        v1_2.setId(2);
        android.widget.RelativeLayout$LayoutParams v2_8 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_8.addRule(10);
        this.addView(v0_6, v2_8);
        android.widget.RelativeLayout$LayoutParams v2_10 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_10.addRule(3, v0_6.getId());
        this.addView(v1_2, v2_10);
        this.setClickable(1);
        this.setOnClickListener(this);
        return;
    }

    static synthetic long a(com.teamspeak.ts3client.f.ak p1, long p2)
    {
        p1.a = p2;
        return p2;
    }

    static synthetic android.widget.SeekBar a(com.teamspeak.ts3client.f.ak p0, android.widget.SeekBar p1)
    {
        p0.d = p1;
        return p1;
    }

    static synthetic android.widget.TextView a(com.teamspeak.ts3client.f.ak p0, android.widget.TextView p1)
    {
        p0.e = p1;
        return p1;
    }

    static synthetic boolean a(com.teamspeak.ts3client.f.ak p1)
    {
        return p1.b;
    }

    static synthetic boolean a(com.teamspeak.ts3client.f.ak p0, boolean p1)
    {
        p0.b = p1;
        return p1;
    }

    static synthetic long b(com.teamspeak.ts3client.f.ak p2)
    {
        return p2.a;
    }

    static synthetic android.widget.SeekBar c(com.teamspeak.ts3client.f.ak p1)
    {
        return p1.d;
    }

    static synthetic String d(com.teamspeak.ts3client.f.ak p1)
    {
        return p1.i;
    }

    static synthetic android.widget.TextView e(com.teamspeak.ts3client.f.ak p1)
    {
        return p1.e;
    }

    static synthetic com.teamspeak.ts3client.f.am f(com.teamspeak.ts3client.f.ak p1)
    {
        return p1.g;
    }

    static synthetic String g(com.teamspeak.ts3client.f.ak p1)
    {
        return p1.f;
    }

    static synthetic android.content.BroadcastReceiver h(com.teamspeak.ts3client.f.ak p1)
    {
        return p1.c;
    }

    public final void onClick(android.view.View p4)
    {
        this.g = new com.teamspeak.ts3client.f.am(this, 0);
        this.g.a(this.h, this.f);
        p4.performHapticFeedback(1);
        return;
    }
}
