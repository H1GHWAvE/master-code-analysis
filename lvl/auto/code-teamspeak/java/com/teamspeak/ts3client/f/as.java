package com.teamspeak.ts3client.f;
public final class as extends android.support.v4.app.Fragment {
    private com.teamspeak.ts3client.Ts3Application a;

    public as()
    {
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p13, android.view.ViewGroup p14)
    {
        this.n();
        this.a = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        this.a.o.b(com.teamspeak.ts3client.data.e.a.a("menu.settings"));
        this.a.c = this;
        android.widget.ScrollView v9_1 = new android.widget.ScrollView(p13.getContext());
        android.widget.LinearLayout v10_1 = new android.widget.LinearLayout(p13.getContext());
        v10_1.setLayoutParams(new android.widget.LinearLayout$LayoutParams(-1, -1));
        v10_1.setOrientation(1);
        java.util.TreeMap v11_1 = new java.util.TreeMap();
        v11_1.put("vibrate_chat", com.teamspeak.ts3client.data.e.a.a("settings.vibrate.chat"));
        v11_1.put("vibrate_chatnew", com.teamspeak.ts3client.data.e.a.a("settings.vibrate.chatnew"));
        v11_1.put("vibrate_poke", com.teamspeak.ts3client.data.e.a.a("settings.vibrate.poke"));
        v10_1.addView(new com.teamspeak.ts3client.f.a(p13.getContext(), com.teamspeak.ts3client.data.e.a.a("settings.spacer.audio")));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "audio_ptt", Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("settings.ptt"), com.teamspeak.ts3client.data.e.a.a("settings.ptt.text")));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "android_overlay_setting", Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("settings.android_overlay"), com.teamspeak.ts3client.data.e.a.a("settings.android_overlay.text"), com.teamspeak.ts3client.data.e.a.a("settings.android_overlay.info")));
        v10_1.addView(new com.teamspeak.ts3client.f.p(p13.getContext(), "ptt_key_setting", com.teamspeak.ts3client.data.e.a.a("settings.pttkey"), com.teamspeak.ts3client.data.e.a.a("settings.pttkey.text"), com.teamspeak.ts3client.data.e.a.a("settings.pttkey.intercept"), com.teamspeak.ts3client.data.e.a.a("settings.pttkey.info"), this.M));
        v10_1.addView(new com.teamspeak.ts3client.f.ak(p13.getContext(), "voiceactivation_level", com.teamspeak.ts3client.data.e.a.a("settings.val"), com.teamspeak.ts3client.data.e.a.a("settings.val.text"), this.M));
        v10_1.addView(new com.teamspeak.ts3client.f.k(p13.getContext(), "volume_modifier", com.teamspeak.ts3client.data.e.a.a("settings.volume_modifier"), com.teamspeak.ts3client.data.e.a.a("settings.volume_modifier.text"), this.M));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "audio_handfree", Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("settings.handfree"), com.teamspeak.ts3client.data.e.a.a("settings.handfree.text"), com.teamspeak.ts3client.data.e.a.a("settings.handfree.warn"), 0));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "audio_bt", Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("settings.bt"), com.teamspeak.ts3client.data.e.a.a("settings.bt.text")));
        v10_1.addView(new com.teamspeak.ts3client.f.ag(p13.getContext(), "whisper", com.teamspeak.ts3client.data.e.a.a("settings.whisper"), com.teamspeak.ts3client.data.e.a.a("settings.whisper.text"), -2, this.M));
        v10_1.addView(new com.teamspeak.ts3client.f.ag(p13.getContext(), "sound_pack", com.teamspeak.ts3client.data.e.a.a("settings.soundpack"), com.teamspeak.ts3client.data.e.a.a("settings.soundpack.text"), 2131361796, this.M));
        v10_1.addView(new com.teamspeak.ts3client.f.b(p13.getContext(), com.teamspeak.ts3client.data.e.a.a("settings.audiosettings"), com.teamspeak.ts3client.data.e.a.a("settings.audiosettings.text"), this.M));
        v10_1.addView(new com.teamspeak.ts3client.f.a(p13.getContext(), com.teamspeak.ts3client.data.e.a.a("settings.spacer.call")));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "call_setaway", Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("settings.callstatus"), com.teamspeak.ts3client.data.e.a.a("settings.callstatus.text")));
        v10_1.addView(new com.teamspeak.ts3client.f.f(p13.getContext(), "call_awaymessage", com.teamspeak.ts3client.data.e.a.a("settings.callmessage.edittext"), com.teamspeak.ts3client.data.e.a.a("settings.callmessage"), com.teamspeak.ts3client.data.e.a.a("settings.callmessage.text"), this.M, 524289, 0));
        v10_1.addView(new com.teamspeak.ts3client.f.a(p13.getContext(), com.teamspeak.ts3client.data.e.a.a("settings.spacer.gui")));
        v10_1.addView(new com.teamspeak.ts3client.f.ag(p13.getContext(), "lang_tag", com.teamspeak.ts3client.data.e.a.a("settings.language"), com.teamspeak.ts3client.data.e.a.a("settings.language.text"), -1, this.M));
        v10_1.addView(new com.teamspeak.ts3client.f.v(p13.getContext(), com.teamspeak.ts3client.data.e.a.a("settings.vibrate"), com.teamspeak.ts3client.data.e.a.a("settings.vibrate.text"), com.teamspeak.ts3client.data.e.a.a("settings.vibrate.text2"), v11_1, this.M));
        v10_1.addView(new com.teamspeak.ts3client.f.f(p13.getContext(), "avatarlimit", "500", com.teamspeak.ts3client.data.e.a.a("settings.avatarlimit"), com.teamspeak.ts3client.data.e.a.a("settings.avatarlimit.text"), this.M, 7));
        v10_1.addView(new com.teamspeak.ts3client.f.f(p13.getContext(), "reconnect.limit", "15", com.teamspeak.ts3client.data.e.a.a("settings.reconnect.limit"), com.teamspeak.ts3client.data.e.a.a("settings.reconnect.limit.text"), this.M, 4));
        v10_1.addView(new com.teamspeak.ts3client.f.ab(p13.getContext(), "channel_height", com.teamspeak.ts3client.data.e.a.a("settings.channelheight"), com.teamspeak.ts3client.data.e.a.a("settings.channelheight.text"), this.M));
        v10_1.addView(new com.teamspeak.ts3client.f.ab(p13.getContext(), "client_height", com.teamspeak.ts3client.data.e.a.a("settings.clientheight"), com.teamspeak.ts3client.data.e.a.a("settings.clientheight.text"), this.M));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "longclick_client", Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("settings.clientclick"), com.teamspeak.ts3client.data.e.a.a("settings.clientclick.text")));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "trenn_linie", Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("settings.seperator"), com.teamspeak.ts3client.data.e.a.a("settings.seperator.text")));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "show_squeryclient", Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("settings.queryclient"), com.teamspeak.ts3client.data.e.a.a("settings.queryclient.text")));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "show_flags", Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("settings.flags"), com.teamspeak.ts3client.data.e.a.a("settings.flags.text")));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "use_proximity", Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("settings.proximity"), com.teamspeak.ts3client.data.e.a.a("settings.proximity.text")));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "enable_fullscreen", Boolean.valueOf(1), com.teamspeak.ts3client.data.e.a.a("settings.fullscreen"), com.teamspeak.ts3client.data.e.a.a("settings.fullscreen.text")));
        v10_1.addView(new com.teamspeak.ts3client.f.ag(p13.getContext(), "screen_rotation", com.teamspeak.ts3client.data.e.a.a("settings.screenrotation"), com.teamspeak.ts3client.data.e.a.a("settings.screenrotation.text"), -3, this.M));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "talk_notification", Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("settings.talknotification"), com.teamspeak.ts3client.data.e.a.a("settings.talknotification.text")));
        v10_1.addView(new com.teamspeak.ts3client.f.a(p13.getContext(), com.teamspeak.ts3client.data.e.a.a("settings.spacer.system")));
        v10_1.addView(new com.teamspeak.ts3client.f.c(p13.getContext(), "create_debuglog", Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("settings.debuglog"), com.teamspeak.ts3client.data.e.a.a("settings.debuglog.text")));
        v9_1.addView(v10_1);
        return v9_1;
    }

    public final void a(android.os.Bundle p1)
    {
        super.a(p1);
        return;
    }

    public final void a(android.view.Menu p1, android.view.MenuInflater p2)
    {
        p1.clear();
        super.a(p1, p2);
        return;
    }

    public final void c(android.os.Bundle p1)
    {
        super.c(p1);
        return;
    }
}
