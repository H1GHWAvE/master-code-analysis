package com.teamspeak.ts3client.f;
final class i implements android.text.TextWatcher {
    final synthetic android.widget.Button a;
    final synthetic com.teamspeak.ts3client.f.h b;

    i(com.teamspeak.ts3client.f.h p1, android.widget.Button p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void afterTextChanged(android.text.Editable p3)
    {
        if (p3.length() <= 0) {
            this.a.setEnabled(0);
        } else {
            this.a.setEnabled(1);
        }
        return;
    }

    public final void beforeTextChanged(CharSequence p1, int p2, int p3, int p4)
    {
        return;
    }

    public final void onTextChanged(CharSequence p1, int p2, int p3, int p4)
    {
        return;
    }
}
