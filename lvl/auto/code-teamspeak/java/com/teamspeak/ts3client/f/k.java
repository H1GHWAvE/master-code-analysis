package com.teamspeak.ts3client.f;
public final class k extends android.widget.RelativeLayout implements android.view.View$OnClickListener {
    private android.widget.SeekBar a;
    private android.widget.TextView b;
    private String c;
    private com.teamspeak.ts3client.f.m d;
    private android.support.v4.app.bi e;
    private String f;
    private String g;

    public k(android.content.Context p8, String p9, String p10, String p11, android.support.v4.app.bi p12)
    {
        this(p8);
        this.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.setPadding(0, 18, 0, 18);
        this.e = p12;
        this.c = p10;
        this.g = p11;
        this.f = p9;
        this.setBackgroundResource(2130837686);
        int v0_4 = new android.widget.TextView(p8);
        android.widget.TextView v1_2 = new android.widget.TextView(p8);
        v0_4.setText(this.c);
        v0_4.setTextSize(1101004800);
        v0_4.setTypeface(0, 1);
        v0_4.setId(1);
        v1_2.setText(this.g);
        v1_2.setTextSize(1092616192);
        v1_2.setTypeface(0, 2);
        v1_2.setId(2);
        android.widget.RelativeLayout$LayoutParams v2_8 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_8.addRule(10);
        this.addView(v0_4, v2_8);
        android.widget.RelativeLayout$LayoutParams v2_10 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_10.addRule(3, v0_4.getId());
        this.addView(v1_2, v2_10);
        this.setClickable(1);
        this.setOnClickListener(this);
        return;
    }

    static synthetic android.widget.SeekBar a(com.teamspeak.ts3client.f.k p0, android.widget.SeekBar p1)
    {
        p0.a = p1;
        return p1;
    }

    static synthetic android.widget.TextView a(com.teamspeak.ts3client.f.k p0, android.widget.TextView p1)
    {
        p0.b = p1;
        return p1;
    }

    static synthetic String a(com.teamspeak.ts3client.f.k p1)
    {
        return p1.f;
    }

    static synthetic android.widget.SeekBar b(com.teamspeak.ts3client.f.k p1)
    {
        return p1.a;
    }

    static synthetic android.widget.TextView c(com.teamspeak.ts3client.f.k p1)
    {
        return p1.b;
    }

    static synthetic com.teamspeak.ts3client.f.m d(com.teamspeak.ts3client.f.k p1)
    {
        return p1.d;
    }

    static synthetic String e(com.teamspeak.ts3client.f.k p1)
    {
        return p1.c;
    }

    public final void onClick(android.view.View p4)
    {
        this.d = new com.teamspeak.ts3client.f.m(this, 0);
        this.d.a(this.e, this.c);
        p4.performHapticFeedback(1);
        return;
    }
}
