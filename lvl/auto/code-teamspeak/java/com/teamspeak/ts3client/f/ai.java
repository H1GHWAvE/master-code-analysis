package com.teamspeak.ts3client.f;
final class ai extends android.support.v4.app.ax {
    android.widget.ArrayAdapter at;
    java.util.HashMap au;
    final synthetic com.teamspeak.ts3client.f.ag av;
    private android.widget.Spinner aw;
    private android.view.View ax;

    private ai(com.teamspeak.ts3client.f.ag p2)
    {
        this.av = p2;
        this.au = new java.util.HashMap();
        return;
    }

    synthetic ai(com.teamspeak.ts3client.f.ag p1, byte p2)
    {
        this(p1);
        return;
    }

    static synthetic android.widget.Spinner a(com.teamspeak.ts3client.f.ai p1)
    {
        return p1.aw;
    }

    public final android.view.View a(android.view.LayoutInflater p14, android.view.ViewGroup p15)
    {
        android.app.Dialog v0_2 = ((com.teamspeak.ts3client.Ts3Application) p14.getContext().getApplicationContext());
        android.widget.RelativeLayout v3_1 = new android.widget.RelativeLayout(p14.getContext());
        v3_1.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.ax = p14.inflate(2130903110, 0);
        this.aw = ((android.widget.Spinner) this.ax.findViewById(2131493264));
        switch (com.teamspeak.ts3client.f.ag.a(this.av)) {
            case -3:
                this.aw.setAdapter(com.teamspeak.ts3client.data.e.a.b("settings.screenrotation.array", this.i(), 3));
                this.aw.setSelection(v0_2.e.getInt(com.teamspeak.ts3client.f.ag.b(this.av), 0));
                break;
            case -2:
                this.aw.setAdapter(com.teamspeak.ts3client.data.e.a.b("settings.whisper.array", this.i(), 3));
                this.aw.setSelection(v0_2.e.getInt(com.teamspeak.ts3client.f.ag.b(this.av), 0));
                break;
            case -1:
                android.widget.Spinner v1_16;
                int v5_3 = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/content/lang/").toString());
                if (!v5_3.exists()) {
                    v1_16 = new String[0];
                } else {
                    v1_16 = v5_3.list(new com.teamspeak.ts3client.data.e.b(v5_3));
                }
                com.teamspeak.ts3client.f.aj v4_3 = new java.util.ArrayList(java.util.Arrays.asList(v1_16));
                int v5_4 = v4_3.iterator();
                while (v5_4.hasNext()) {
                    this.au.put(((String) v5_4.next()), ((String) v5_4.next()));
                }
                int v5_5 = com.teamspeak.ts3client.data.e.c.values();
                int v6_0 = v5_5.length;
                android.widget.Spinner v1_21 = 0;
                while (v1_21 < v6_0) {
                    String v7_0 = v5_5[v1_21];
                    v4_3.add(0, v7_0.name());
                    this.au.put(v7_0.name(), v7_0.i);
                    v1_21++;
                }
                this.at = new android.widget.ArrayAdapter(this.h(), 17367049, v4_3);
                this.aw.setAdapter(this.at);
                break;
            default:
                this.aw.setAdapter(android.widget.ArrayAdapter.createFromResource(this.h(), com.teamspeak.ts3client.f.ag.a(this.av), 17367048));
                this.aw.setSelection(v0_2.e.getInt(com.teamspeak.ts3client.f.ag.b(this.av), 0));
        }
        this.ax.setId(4);
        android.widget.Spinner v1_38 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_38.addRule(10);
        v3_1.addView(this.ax, v1_38);
        android.widget.Spinner v1_40 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_40.addRule(3, this.ax.getId());
        android.widget.ArrayAdapter v2_12 = new android.widget.Button(p14.getContext());
        v2_12.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        v2_12.setOnClickListener(new com.teamspeak.ts3client.f.aj(this, v0_2, v3_1));
        v3_1.addView(v2_12, v1_40);
        this.j.setTitle(com.teamspeak.ts3client.f.ag.d(this.av));
        return v3_1;
    }
}
