package com.teamspeak.ts3client.bookmark;
public class CustomEditText extends android.widget.EditText {
    boolean a;
    android.graphics.Paint b;

    public CustomEditText(android.content.Context p3, android.util.AttributeSet p4)
    {
        this(p3, p4);
        this.a = 0;
        this.b = new android.graphics.Paint();
        this.b.setAntiAlias(1);
        this.b.setColor(-65536);
        this.b.setAlpha(100);
        this.addTextChangedListener(new com.teamspeak.ts3client.bookmark.d(this));
        return;
    }

    private boolean a()
    {
        return this.a;
    }

    public void draw(android.graphics.Canvas p7)
    {
        super.draw(p7);
        if (this.a) {
            p7.drawRect(1084227584, 1084227584, ((float) (this.getWidth() - 5)), ((float) (this.getHeight() - 5)), this.b);
        }
        return;
    }

    public void setInputerror(boolean p1)
    {
        this.a = p1;
        this.requestLayout();
        return;
    }
}
