package com.teamspeak.ts3client.jni.events;
public class ConnectStatusChange implements com.teamspeak.ts3client.jni.k {
    private long a;
    private int b;
    private int c;

    public ConnectStatusChange()
    {
        return;
    }

    private ConnectStatusChange(long p2, int p4, int p5)
    {
        this.a = p2;
        this.b = p4;
        this.c = p5;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long c()
    {
        return this.a;
    }

    public final int a()
    {
        return this.c;
    }

    public final int b()
    {
        return this.b;
    }

    public String toString()
    {
        return new StringBuilder("ConnectStatusChange [serverConnectionHandlerID=").append(this.a).append(", newStatus=").append(this.b).append(", errorNumber=").append(this.c).append("]").toString();
    }
}
