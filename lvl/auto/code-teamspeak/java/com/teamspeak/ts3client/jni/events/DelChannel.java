package com.teamspeak.ts3client.jni.events;
public class DelChannel implements com.teamspeak.ts3client.jni.k {
    private long a;
    private long b;
    private int c;
    private String d;
    private String e;

    public DelChannel()
    {
        return;
    }

    private DelChannel(long p2, long p4, int p6, String p7, String p8)
    {
        this.a = p2;
        this.b = p4;
        this.c = p6;
        this.d = p7;
        this.e = p8;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private String d()
    {
        return this.e;
    }

    private long e()
    {
        return this.a;
    }

    public final long a()
    {
        return this.b;
    }

    public final int b()
    {
        return this.c;
    }

    public final String c()
    {
        return this.d;
    }

    public String toString()
    {
        return new StringBuilder("DelChannel [serverConnectionHandlerID=").append(this.a).append(", channelID=").append(this.b).append(", invokerID=").append(this.c).append(", invokerName=").append(this.d).append(", invokerUniqueIdentifier=").append(this.e).append("]").toString();
    }
}
