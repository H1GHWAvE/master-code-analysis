package com.teamspeak.ts3client.jni.events.rare;
public class ComplainList implements com.teamspeak.ts3client.jni.k {
    private long a;
    private long b;
    private String c;
    private long d;
    private String e;
    private String f;
    private long g;

    public ComplainList()
    {
        return;
    }

    private ComplainList(long p1, long p3, String p5, long p6, String p8, String p9, long p10)
    {
        this.a = p1;
        this.b = p3;
        this.c = p5;
        this.d = p6;
        this.e = p8;
        this.f = p9;
        this.g = p10;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private String a()
    {
        return this.f;
    }

    private long b()
    {
        return this.d;
    }

    private String c()
    {
        return this.e;
    }

    private long d()
    {
        return this.a;
    }

    private long e()
    {
        return this.b;
    }

    private String f()
    {
        return this.c;
    }

    private long g()
    {
        return this.g;
    }

    public String toString()
    {
        return new StringBuilder("ComplainList [serverConnectionHandlerID=").append(this.a).append(", targetClientDatabaseID=").append(this.b).append(", targetClientNickName=").append(this.c).append(", fromClientDatabaseID=").append(this.d).append(", fromClientNickName=").append(this.e).append(", complainReason=").append(this.f).append(", timestamp=").append(this.g).append("]").toString();
    }
}
