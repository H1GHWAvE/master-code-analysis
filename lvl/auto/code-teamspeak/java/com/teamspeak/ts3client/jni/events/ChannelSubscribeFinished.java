package com.teamspeak.ts3client.jni.events;
public class ChannelSubscribeFinished implements com.teamspeak.ts3client.jni.k {
    private long a;

    public ChannelSubscribeFinished()
    {
        return;
    }

    private ChannelSubscribeFinished(long p2)
    {
        this.a = p2;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    public final long a()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ChannelSubscribeFinished [serverConnectionHandlerID=").append(this.a).append("]").toString();
    }
}
