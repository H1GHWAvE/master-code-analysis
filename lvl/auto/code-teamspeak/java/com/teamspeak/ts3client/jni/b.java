package com.teamspeak.ts3client.jni;
public final enum class b extends java.lang.Enum {
    public static final enum com.teamspeak.ts3client.jni.b A;
    private static final synthetic com.teamspeak.ts3client.jni.b[] C;
    public static final enum com.teamspeak.ts3client.jni.b a;
    public static final enum com.teamspeak.ts3client.jni.b b;
    public static final enum com.teamspeak.ts3client.jni.b c;
    public static final enum com.teamspeak.ts3client.jni.b d;
    public static final enum com.teamspeak.ts3client.jni.b e;
    public static final enum com.teamspeak.ts3client.jni.b f;
    public static final enum com.teamspeak.ts3client.jni.b g;
    public static final enum com.teamspeak.ts3client.jni.b h;
    public static final enum com.teamspeak.ts3client.jni.b i;
    public static final enum com.teamspeak.ts3client.jni.b j;
    public static final enum com.teamspeak.ts3client.jni.b k;
    public static final enum com.teamspeak.ts3client.jni.b l;
    public static final enum com.teamspeak.ts3client.jni.b m;
    public static final enum com.teamspeak.ts3client.jni.b n;
    public static final enum com.teamspeak.ts3client.jni.b o;
    public static final enum com.teamspeak.ts3client.jni.b p;
    public static final enum com.teamspeak.ts3client.jni.b q;
    public static final enum com.teamspeak.ts3client.jni.b r;
    public static final enum com.teamspeak.ts3client.jni.b s;
    public static final enum com.teamspeak.ts3client.jni.b t;
    public static final enum com.teamspeak.ts3client.jni.b u;
    public static final enum com.teamspeak.ts3client.jni.b v;
    public static final enum com.teamspeak.ts3client.jni.b w;
    public static final enum com.teamspeak.ts3client.jni.b x;
    public static final enum com.teamspeak.ts3client.jni.b y;
    public static final enum com.teamspeak.ts3client.jni.b z;
    public int B;

    static b()
    {
        com.teamspeak.ts3client.jni.b.a = new com.teamspeak.ts3client.jni.b("BBCodeTag_B", 0, 1);
        com.teamspeak.ts3client.jni.b.b = new com.teamspeak.ts3client.jni.b("BBCodeTag_I", 1, 2);
        com.teamspeak.ts3client.jni.b.c = new com.teamspeak.ts3client.jni.b("BBCodeTag_U", 2, 4);
        com.teamspeak.ts3client.jni.b.d = new com.teamspeak.ts3client.jni.b("BBCodeTag_S", 3, 8);
        com.teamspeak.ts3client.jni.b.e = new com.teamspeak.ts3client.jni.b("BBCodeTag_SUP", 4, 16);
        com.teamspeak.ts3client.jni.b.f = new com.teamspeak.ts3client.jni.b("BBCodeTag_SUB", 5, 32);
        com.teamspeak.ts3client.jni.b.g = new com.teamspeak.ts3client.jni.b("BBCodeTag_COLOR", 6, 64);
        com.teamspeak.ts3client.jni.b.h = new com.teamspeak.ts3client.jni.b("BBCodeTag_SIZE", 7, 128);
        com.teamspeak.ts3client.jni.b.i = new com.teamspeak.ts3client.jni.b("BBCodeTag_group_text", 8, 255);
        com.teamspeak.ts3client.jni.b.j = new com.teamspeak.ts3client.jni.b("BBCodeTag_LEFT", 9, 4096);
        com.teamspeak.ts3client.jni.b.k = new com.teamspeak.ts3client.jni.b("BBCodeTag_RIGHT", 10, 8192);
        com.teamspeak.ts3client.jni.b.l = new com.teamspeak.ts3client.jni.b("BBCodeTag_CENTER", 11, 16384);
        com.teamspeak.ts3client.jni.b.m = new com.teamspeak.ts3client.jni.b("BBCodeTag_group_align", 12, 28672);
        com.teamspeak.ts3client.jni.b.n = new com.teamspeak.ts3client.jni.b("BBCodeTag_URL", 13, 65536);
        com.teamspeak.ts3client.jni.b.o = new com.teamspeak.ts3client.jni.b("BBCodeTag_IMAGE", 14, 131072);
        com.teamspeak.ts3client.jni.b.p = new com.teamspeak.ts3client.jni.b("BBCodeTag_HR", 15, 262144);
        com.teamspeak.ts3client.jni.b.q = new com.teamspeak.ts3client.jni.b("BBCodeTag_LIST", 16, 1048576);
        com.teamspeak.ts3client.jni.b.r = new com.teamspeak.ts3client.jni.b("BBCodeTag_LISTITEM", 17, 2097152);
        com.teamspeak.ts3client.jni.b.s = new com.teamspeak.ts3client.jni.b("BBCodeTag_group_list", 18, 3145728);
        com.teamspeak.ts3client.jni.b.t = new com.teamspeak.ts3client.jni.b("BBCodeTag_TABLE", 19, 4194304);
        com.teamspeak.ts3client.jni.b.u = new com.teamspeak.ts3client.jni.b("BBCodeTag_TR", 20, 8388608);
        com.teamspeak.ts3client.jni.b.v = new com.teamspeak.ts3client.jni.b("BBCodeTag_TH", 21, 16777216);
        com.teamspeak.ts3client.jni.b.w = new com.teamspeak.ts3client.jni.b("BBCodeTag_TD", 22, 33554432);
        com.teamspeak.ts3client.jni.b.x = new com.teamspeak.ts3client.jni.b("BBCodeTag_group_table", 23, 62914560);
        com.teamspeak.ts3client.jni.b.y = new com.teamspeak.ts3client.jni.b("BBCodeTag_def_simple", 24, (((((((com.teamspeak.ts3client.jni.b.a.B | com.teamspeak.ts3client.jni.b.b.B) | com.teamspeak.ts3client.jni.b.c.B) | com.teamspeak.ts3client.jni.b.d.B) | com.teamspeak.ts3client.jni.b.e.B) | com.teamspeak.ts3client.jni.b.f.B) | com.teamspeak.ts3client.jni.b.g.B) | com.teamspeak.ts3client.jni.b.n.B));
        com.teamspeak.ts3client.jni.b.z = new com.teamspeak.ts3client.jni.b("BBCodeTag_def_simple_Img", 25, (com.teamspeak.ts3client.jni.b.y.B | com.teamspeak.ts3client.jni.b.o.B));
        com.teamspeak.ts3client.jni.b.A = new com.teamspeak.ts3client.jni.b("BBCodeTag_def_extended", 26, ((((((com.teamspeak.ts3client.jni.b.i.B | com.teamspeak.ts3client.jni.b.m.B) | com.teamspeak.ts3client.jni.b.n.B) | com.teamspeak.ts3client.jni.b.o.B) | com.teamspeak.ts3client.jni.b.p.B) | com.teamspeak.ts3client.jni.b.s.B) | com.teamspeak.ts3client.jni.b.x.B));
        com.teamspeak.ts3client.jni.b[] v0_55 = new com.teamspeak.ts3client.jni.b[27];
        v0_55[0] = com.teamspeak.ts3client.jni.b.a;
        v0_55[1] = com.teamspeak.ts3client.jni.b.b;
        v0_55[2] = com.teamspeak.ts3client.jni.b.c;
        v0_55[3] = com.teamspeak.ts3client.jni.b.d;
        v0_55[4] = com.teamspeak.ts3client.jni.b.e;
        v0_55[5] = com.teamspeak.ts3client.jni.b.f;
        v0_55[6] = com.teamspeak.ts3client.jni.b.g;
        v0_55[7] = com.teamspeak.ts3client.jni.b.h;
        v0_55[8] = com.teamspeak.ts3client.jni.b.i;
        v0_55[9] = com.teamspeak.ts3client.jni.b.j;
        v0_55[10] = com.teamspeak.ts3client.jni.b.k;
        v0_55[11] = com.teamspeak.ts3client.jni.b.l;
        v0_55[12] = com.teamspeak.ts3client.jni.b.m;
        v0_55[13] = com.teamspeak.ts3client.jni.b.n;
        v0_55[14] = com.teamspeak.ts3client.jni.b.o;
        v0_55[15] = com.teamspeak.ts3client.jni.b.p;
        v0_55[16] = com.teamspeak.ts3client.jni.b.q;
        v0_55[17] = com.teamspeak.ts3client.jni.b.r;
        v0_55[18] = com.teamspeak.ts3client.jni.b.s;
        v0_55[19] = com.teamspeak.ts3client.jni.b.t;
        v0_55[20] = com.teamspeak.ts3client.jni.b.u;
        v0_55[21] = com.teamspeak.ts3client.jni.b.v;
        v0_55[22] = com.teamspeak.ts3client.jni.b.w;
        v0_55[23] = com.teamspeak.ts3client.jni.b.x;
        v0_55[24] = com.teamspeak.ts3client.jni.b.y;
        v0_55[25] = com.teamspeak.ts3client.jni.b.z;
        v0_55[26] = com.teamspeak.ts3client.jni.b.A;
        com.teamspeak.ts3client.jni.b.C = v0_55;
        return;
    }

    private b(String p1, int p2, int p3)
    {
        this(p1, p2);
        this.B = p3;
        return;
    }

    private int a()
    {
        return this.B;
    }

    private static int a(int p0)
    {
        return p0;
    }

    public static com.teamspeak.ts3client.jni.b valueOf(String p1)
    {
        return ((com.teamspeak.ts3client.jni.b) Enum.valueOf(com.teamspeak.ts3client.jni.b, p1));
    }

    public static com.teamspeak.ts3client.jni.b[] values()
    {
        return ((com.teamspeak.ts3client.jni.b[]) com.teamspeak.ts3client.jni.b.C.clone());
    }
}
