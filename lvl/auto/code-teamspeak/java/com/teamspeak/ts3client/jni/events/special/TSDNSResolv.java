package com.teamspeak.ts3client.jni.events.special;
public class TSDNSResolv implements com.teamspeak.ts3client.jni.k {
    private String a;
    private int b;
    private String c;
    private boolean d;
    private int e;

    public TSDNSResolv()
    {
        return;
    }

    private TSDNSResolv(String p1, int p2, String p3, boolean p4, int p5)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        this.d = p4;
        this.e = p5;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private String d()
    {
        return this.c;
    }

    private boolean e()
    {
        return this.d;
    }

    public final int a()
    {
        return this.e;
    }

    public final String b()
    {
        return this.a;
    }

    public final int c()
    {
        return this.b;
    }

    public String toString()
    {
        return new StringBuilder("TSDNSResolv [ip=").append(this.a).append(", port=").append(this.b).append(", query=").append(this.c).append(", wasTSDNS=").append(this.d).append(", error=").append(this.e).append("]").toString();
    }
}
