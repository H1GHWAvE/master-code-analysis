package com.teamspeak.ts3client.d;
public final class m extends android.app.Dialog {
    private java.io.File a;
    private android.content.Context b;

    public m(android.content.Context p4)
    {
        this(p4);
        this.a = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/").append(android.os.Environment.DIRECTORY_DOWNLOADS).toString());
        return;
    }

    private int a(float p3)
    {
        return ((int) ((this.getContext().getResources().getDisplayMetrics().density * p3) + 1056964608));
    }

    static synthetic com.teamspeak.ts3client.e.a a(com.teamspeak.ts3client.d.m p1, String p2)
    {
        return p1.a(p2);
    }

    private com.teamspeak.ts3client.e.a a(String p9)
    {
        com.teamspeak.ts3client.e.a v0_38;
        com.teamspeak.ts3client.e.a v0_1 = new java.io.File(new StringBuilder().append(this.a).append("/").append(p9).toString());
        if (v0_1.exists()) {
            String v3_3 = new java.util.ArrayList();
            try {
                com.teamspeak.ts3client.e.a v2_8 = new java.io.FileInputStream(v0_1);
                com.teamspeak.ts3client.e.a v0_3 = new java.io.BufferedReader(new java.io.InputStreamReader(v2_8));
            } catch (com.teamspeak.ts3client.e.a v0_5) {
                v0_5.printStackTrace();
            } catch (com.teamspeak.ts3client.e.a v0_4) {
                v0_4.printStackTrace();
            }
            while(true) {
                String v4_2 = v0_3.readLine();
                if (v4_2 == null) {
                    break;
                }
                v3_3.add(com.teamspeak.ts3client.data.d.x.b(v4_2));
            }
            v0_3.close();
            v2_8.close();
            if ((v3_3.size() > 0) && ((((String) v3_3.get(0)).equals("[Identity]")) && ((((String) v3_3.get(1)).startsWith("id")) && ((((String) v3_3.get(2)).startsWith("identity=")) && (((String) v3_3.get(3)).startsWith("nickname=")))))) {
                com.teamspeak.ts3client.e.a v2_14 = new com.teamspeak.ts3client.e.a();
                v2_14.b = ((String) v3_3.get(1)).replace("id=", "").toString();
                com.teamspeak.ts3client.e.a v0_28 = ((String) v3_3.get(2)).replace("identity=", "").toString();
                v2_14.c = v0_28.substring(1, (v0_28.length() - 1));
                if ((!v2_14.c.equals("")) && (!v2_14.b.equals(""))) {
                    v2_14.d = ((String) v3_3.get(1)).replace("nickname=", "").toString();
                    v0_38 = v2_14;
                } else {
                    this.a();
                    v0_38 = 0;
                }
            } else {
                this.a();
                v0_38 = 0;
            }
        } else {
            com.teamspeak.ts3client.e.a v0_41 = new android.app.AlertDialog$Builder(this.b).create();
            com.teamspeak.ts3client.data.d.q.a(v0_41);
            v0_41.setTitle(com.teamspeak.ts3client.data.e.a.a("ident.file.error.info"));
            v0_41.setMessage(com.teamspeak.ts3client.data.e.a.a("ident.file.error.text2"));
            v0_41.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.d.p(this, v0_41));
            v0_41.show();
            v0_38 = 0;
        }
        return v0_38;
    }

    static synthetic java.io.File a(com.teamspeak.ts3client.d.m p1)
    {
        return p1.a;
    }

    private void a()
    {
        android.app.AlertDialog v0_2 = new android.app.AlertDialog$Builder(this.b).create();
        com.teamspeak.ts3client.data.d.q.a(v0_2);
        v0_2.setTitle(com.teamspeak.ts3client.data.e.a.a("ident.file.error.info"));
        v0_2.setMessage(com.teamspeak.ts3client.data.e.a.a("ident.file.error.text1"));
        v0_2.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.d.n(this, v0_2));
        v0_2.show();
        return;
    }

    private String[] b()
    {
        String[] v0_3;
        if (!this.a.exists()) {
            v0_3 = new String[0];
        } else {
            v0_3 = this.a.list(new com.teamspeak.ts3client.d.o(this));
        }
        return v0_3;
    }

    protected final void onCreate(android.os.Bundle p8)
    {
        com.teamspeak.ts3client.d.q v1_0 = 0;
        super.onCreate(p8);
        this.b = this.getContext();
        com.teamspeak.ts3client.data.d.q.a(this);
        android.widget.ScrollView v2_1 = new android.widget.ScrollView(this.getContext());
        if (android.os.Environment.getExternalStorageState().equals("mounted")) {
            android.widget.TextView v0_8;
            this.setTitle(com.teamspeak.ts3client.data.e.a.a("ident.file.error.dialog.info"));
            if (!this.a.exists()) {
                v0_8 = new String[0];
            } else {
                v0_8 = this.a.list(new com.teamspeak.ts3client.d.o(this));
            }
            if (v0_8.length > 0) {
                android.widget.RadioGroup v3_4 = new android.widget.RadioGroup(this.getContext());
                while (v1_0 < v0_8.length) {
                    android.widget.RadioButton v4_3 = new android.widget.RadioButton(this.getContext());
                    v4_3.setText(v0_8[v1_0]);
                    v4_3.setId(v1_0);
                    v3_4.addView(v4_3);
                    v1_0++;
                }
                v2_1.addView(v3_4);
                v2_1.setId(1);
                v3_4.setOnCheckedChangeListener(new com.teamspeak.ts3client.d.q(this, v0_8));
                this.setContentView(v2_1);
            } else {
                this.setTitle(com.teamspeak.ts3client.data.e.a.a("ident.file.error.info"));
                android.widget.TextView v0_14 = new android.widget.TextView(this.b);
                android.widget.RadioButton v4_4 = new Object[1];
                v4_4[0] = this.a;
                v0_14.setText(com.teamspeak.ts3client.data.e.a.a("ident.file.error.dialog.text2", v4_4));
                v2_1.addView(v0_14);
                this.setContentView(v2_1);
            }
        } else {
            this.setTitle(com.teamspeak.ts3client.data.e.a.a("ident.file.error.info"));
            android.widget.TextView v0_18 = new android.widget.TextView(this.b);
            v0_18.setText(com.teamspeak.ts3client.data.e.a.a("ident.file.error.dialog.text1"));
            v2_1.addView(v0_18);
            this.setContentView(v2_1);
        }
        return;
    }
}
