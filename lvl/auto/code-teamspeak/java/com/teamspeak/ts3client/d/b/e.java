package com.teamspeak.ts3client.d.b;
final class e implements android.view.View$OnClickListener {
    final synthetic com.teamspeak.ts3client.Ts3Application a;
    final synthetic com.teamspeak.ts3client.d.b.a b;

    e(com.teamspeak.ts3client.d.b.a p1, com.teamspeak.ts3client.Ts3Application p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onClick(android.view.View p14)
    {
        com.teamspeak.ts3client.d.b.a v0_1 = new android.app.Dialog(p14.getContext());
        com.teamspeak.ts3client.data.d.q.a(v0_1);
        String v1_2 = new android.widget.LinearLayout(p14.getContext());
        android.widget.RelativeLayout v2_2 = new android.widget.RelativeLayout(p14.getContext());
        v2_2.setLayoutParams(new android.view.ViewGroup$LayoutParams(-1, -2));
        android.widget.RelativeLayout$LayoutParams v3_4 = new android.widget.TextView(p14.getContext());
        v3_4.setId(1);
        v3_4.setText(new StringBuilder().append(com.teamspeak.ts3client.d.b.a.a(this.b).a).append(":").toString());
        android.widget.SeekBar v4_8 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v4_8.addRule(10);
        v2_2.addView(v3_4, v4_8);
        android.widget.SeekBar v4_10 = new android.widget.SeekBar(p14.getContext());
        v4_10.setId(3);
        v4_10.setMax(120);
        v4_10.setProgress(60);
        if ((com.teamspeak.ts3client.d.b.a.a(this.b).w != null) && (com.teamspeak.ts3client.d.b.a.a(this.b).w.m != 0)) {
            v4_10.setProgress((Math.round((com.teamspeak.ts3client.d.b.a.a(this.b).w.m / 1056964608)) + 60));
        }
        int v5_24 = new android.widget.TextView(p14.getContext());
        v5_24.setTextSize(1090519040);
        v5_24.setTypeface(0, 2);
        v5_24.setText(new StringBuilder().append((((float) (v4_10.getProgress() - 60)) * 1056964608)).append(" / 30").toString());
        v5_24.setPadding(0, 0, 10, 0);
        v5_24.setId(4);
        v4_10.setOnSeekBarChangeListener(new com.teamspeak.ts3client.d.b.f(this, v5_24));
        v4_10.setProgressDrawable(this.b.j().getDrawable(2130837676));
        com.teamspeak.ts3client.d.b.g v6_17 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v6_17.addRule(3, v3_4.getId());
        v2_2.addView(v4_10, v6_17);
        android.widget.RelativeLayout$LayoutParams v3_7 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v3_7.addRule(3, v4_10.getId());
        v3_7.addRule(11);
        v2_2.addView(v5_24, v3_7);
        android.widget.RelativeLayout$LayoutParams v3_9 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v3_9.addRule(3, v5_24.getId());
        int v5_27 = new android.widget.Button(p14.getContext());
        v5_27.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        v5_27.setOnClickListener(new com.teamspeak.ts3client.d.b.g(this, v4_10, v2_2, v0_1));
        v2_2.addView(v5_27, v3_9);
        v1_2.addView(v2_2);
        v0_1.setContentView(v1_2);
        v0_1.setTitle(com.teamspeak.ts3client.data.e.a.a("dialog.client.volumemodifier.text"));
        v0_1.show();
        this.b.b();
        return;
    }
}
