package com.teamspeak.ts3client.d;
final class y extends android.os.AsyncTask {
    final synthetic com.teamspeak.ts3client.d.t a;

    private y(com.teamspeak.ts3client.d.t p1)
    {
        this.a = p1;
        return;
    }

    synthetic y(com.teamspeak.ts3client.d.t p1, byte p2)
    {
        this(p1);
        return;
    }

    private static varargs String a(String[] p5)
    {
        int v1 = 0;
        try {
            java.io.BufferedReader v2_1 = new java.net.URL(p5[0]);
            String v0_3 = ((java.net.HttpURLConnection) v2_1.openConnection());
            StringBuilder v3_0 = v0_3.getHeaderFields();
        } catch (String v0) {
            String v0_4 = v1;
            v1 = v0_4;
            return v1;
        }
        if ((!v2_1.getHost().equals(v0_3.getURL().getHost())) || ((!v3_0.containsKey("X-TS3-SIGNATURE")) || ((v0_3.getResponseCode() != 200) && (v0_3.getResponseCode() != 304)))) {
            return v1;
        } else {
            v0_3.setConnectTimeout(1000);
            java.io.BufferedReader v2_10 = new java.io.BufferedReader(new java.io.InputStreamReader(v0_3.getInputStream()));
            StringBuilder v3_6 = new StringBuilder();
            while(true) {
                String v4_3 = v2_10.readLine();
                if (v4_3 == null) {
                    break;
                }
                v3_6.append(v4_3);
            }
            v0_3.disconnect();
            v0_4 = v3_6.toString();
        }
    }

    private void a(String p4)
    {
        if (p4 == null) {
            if (!com.teamspeak.ts3client.d.t.e(this.a).equals("en")) {
                com.teamspeak.ts3client.d.t.b(this.a, "en");
                com.teamspeak.ts3client.d.t.g(this.a);
            } else {
                String v1_3 = new Object[0];
                com.teamspeak.ts3client.d.t.f(this.a).a(v1_3);
            }
        } else {
            com.teamspeak.ts3client.d.t.a(this.a, p4);
            com.teamspeak.ts3client.Ts3Application.a().e.edit().putString("la_url", com.teamspeak.ts3client.d.t.b(this.a)).commit();
        }
        return;
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return com.teamspeak.ts3client.d.y.a(((String[]) p2));
    }

    protected final synthetic void onPostExecute(Object p4)
    {
        if (((String) p4) == null) {
            if (!com.teamspeak.ts3client.d.t.e(this.a).equals("en")) {
                com.teamspeak.ts3client.d.t.b(this.a, "en");
                com.teamspeak.ts3client.d.t.g(this.a);
            } else {
                String v1_3 = new Object[0];
                com.teamspeak.ts3client.d.t.f(this.a).a(v1_3);
            }
        } else {
            com.teamspeak.ts3client.d.t.a(this.a, ((String) p4));
            com.teamspeak.ts3client.Ts3Application.a().e.edit().putString("la_url", com.teamspeak.ts3client.d.t.b(this.a)).commit();
        }
        return;
    }
}
