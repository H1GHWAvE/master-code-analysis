package com.teamspeak.ts3client.d.b;
final class m implements android.view.View$OnClickListener {
    final synthetic com.teamspeak.ts3client.Ts3Application a;
    final synthetic com.teamspeak.ts3client.d.b.a b;

    m(com.teamspeak.ts3client.d.b.a p1, com.teamspeak.ts3client.Ts3Application p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onClick(android.view.View p9)
    {
        if (this.a.a.i.containsKey(Integer.valueOf(com.teamspeak.ts3client.d.b.a.a(this.b).c))) {
            com.teamspeak.ts3client.d.b.a v0_7 = this.a.a.f.a;
            String v1_5 = new java.util.ArrayList();
            android.widget.LinearLayout v2_0 = v0_7.iterator();
            while (v2_0.hasNext()) {
                com.teamspeak.ts3client.d.b.a v0_13 = ((com.teamspeak.ts3client.data.a) v2_0.next());
                if (v0_13.b != 0) {
                    v1_5.add(v0_13);
                }
            }
            com.teamspeak.ts3client.d.b.a v0_10 = new android.app.Dialog(p9.getContext());
            com.teamspeak.ts3client.data.d.q.a(v0_10);
            android.widget.LinearLayout v2_3 = new android.widget.LinearLayout(p9.getContext());
            v2_3.setOrientation(1);
            android.widget.Spinner v3_3 = new android.widget.Spinner(p9.getContext());
            v3_3.setLayoutParams(new android.view.ViewGroup$LayoutParams(-1, -2));
            android.widget.ArrayAdapter v4_4 = new android.widget.ArrayAdapter(p9.getContext(), 17367049, v1_5);
            v3_3.setAdapter(v4_4);
            v2_3.addView(v3_3);
            String v1_7 = new android.widget.Button(p9.getContext());
            v1_7.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.move.dialog.info"));
            v1_7.setOnClickListener(new com.teamspeak.ts3client.d.b.n(this, v4_4, v3_3, v0_10));
            v2_3.addView(v1_7);
            v0_10.setContentView(v2_3);
            v0_10.setTitle(com.teamspeak.ts3client.data.e.a.a("dialog.client.move.dialog.text"));
            v0_10.show();
            this.b.b();
        } else {
            this.b.b();
        }
        return;
    }
}
