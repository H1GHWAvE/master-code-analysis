package com.teamspeak.ts3client.d;
final class c implements java.lang.Runnable {
    final synthetic java.text.DecimalFormat a;
    final synthetic java.text.DecimalFormat b;
    final synthetic com.teamspeak.ts3client.d.a c;

    c(com.teamspeak.ts3client.d.a p1, java.text.DecimalFormat p2, java.text.DecimalFormat p3)
    {
        this.c = p1;
        this.a = p2;
        this.b = p3;
        return;
    }

    public final void run()
    {
        com.teamspeak.ts3client.d.a.e(this.c).setText(com.teamspeak.ts3client.d.a.b(this.c).a);
        com.teamspeak.ts3client.d.a.f(this.c).setText(com.teamspeak.ts3client.data.d.w.a((com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.c) / 1000)));
        com.teamspeak.ts3client.d.a.g(this.c).setText(com.teamspeak.ts3client.data.d.w.a((com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.d) / 1000)));
        com.teamspeak.ts3client.d.a.h(this.c).setText(new StringBuilder().append(com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.a)).append("ms \u00b1 ").append(this.a.format(com.teamspeak.ts3client.d.a.c(this.c).a.a.b(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.b))).toString());
        if (com.teamspeak.ts3client.d.a.b(this.c).c == com.teamspeak.ts3client.d.a.c(this.c).a.h) {
            android.widget.TextView v0_16;
            android.widget.TextView v0_15 = com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.h);
            String v2_22 = com.teamspeak.ts3client.d.a.i(this.c);
            int v3_13 = new StringBuilder().append(com.teamspeak.ts3client.d.a.c(this.c).a.a.c(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.g)).append(":");
            if (v0_15 != 0) {
                v0_16 = Long.valueOf(v0_15);
            } else {
                v0_16 = "9987";
            }
            v2_22.setText(v3_13.append(v0_16).toString());
        }
        android.widget.TextView v0_23 = com.teamspeak.ts3client.d.a.c(this.c).a.a.c(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.e);
        if (v0_23 == null) {
            com.teamspeak.ts3client.d.a.j(this.c).setText(com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.iphidden"));
        } else {
            com.teamspeak.ts3client.d.a.j(this.c).setText(new StringBuilder().append(v0_23).append(":").append(com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.f)).toString());
        }
        com.teamspeak.ts3client.d.a.k(this.c).setText(this.b.format(com.teamspeak.ts3client.d.a.c(this.c).a.a.b(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.F)));
        com.teamspeak.ts3client.d.a.l(this.c).setText(this.b.format(com.teamspeak.ts3client.d.a.c(this.c).a.a.b(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.J)));
        com.teamspeak.ts3client.d.a.m(this.c).setText(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.t), 0));
        com.teamspeak.ts3client.d.a.n(this.c).setText(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.l), 0));
        com.teamspeak.ts3client.d.a.o(this.c).setText(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.x), 1));
        com.teamspeak.ts3client.d.a.p(this.c).setText(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.p), 1));
        com.teamspeak.ts3client.d.a.q(this.c).setText(new StringBuilder().append(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.V), 1)).append("/s").toString());
        com.teamspeak.ts3client.d.a.r(this.c).setText(new StringBuilder().append(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.N), 1)).append("/s").toString());
        com.teamspeak.ts3client.d.a.s(this.c).setText(new StringBuilder().append(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.Z), 1)).append("/s").toString());
        com.teamspeak.ts3client.d.a.t(this.c).setText(new StringBuilder().append(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.R), 1)).append("/s").toString());
        com.teamspeak.ts3client.d.a.u(this.c).setText(new StringBuilder().append(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.al), 1)).append("/s").toString());
        com.teamspeak.ts3client.d.a.v(this.c).setText(new StringBuilder().append(com.teamspeak.ts3client.data.d.w.a(com.teamspeak.ts3client.d.a.c(this.c).a.a.a(com.teamspeak.ts3client.d.a.c(this.c).a.e, com.teamspeak.ts3client.d.a.b(this.c).c, com.teamspeak.ts3client.jni.f.ak), 1)).append("/s").toString());
        return;
    }
}
