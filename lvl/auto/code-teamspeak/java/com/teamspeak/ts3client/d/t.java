package com.teamspeak.ts3client.d;
public final class t extends android.app.Dialog implements com.teamspeak.ts3client.customs.f, com.teamspeak.ts3client.d.j {
    public com.teamspeak.ts3client.d.k a;
    public com.teamspeak.ts3client.d.k b;
    public int c;
    public com.teamspeak.ts3client.d.k d;
    public com.teamspeak.ts3client.d.l e;
    public String f;
    private com.teamspeak.ts3client.customs.CustomLicenseAgreementWebView g;
    private String h;
    private android.widget.Button i;
    private String j;

    public t(android.content.Context p2)
    {
        this(p2, 2131165364);
        this.a(p2);
        return;
    }

    private t(android.content.Context p1, byte p2)
    {
        this(p1);
        this.a(p1);
        return;
    }

    private t(android.content.Context p1, boolean p2, android.content.DialogInterface$OnCancelListener p3)
    {
        this(p1, p2, p3);
        this.a(p1);
        return;
    }

    static synthetic com.teamspeak.ts3client.d.k a(com.teamspeak.ts3client.d.t p1)
    {
        return p1.b;
    }

    private void a(int p1)
    {
        this.c = p1;
        return;
    }

    private void a(android.content.Context p6)
    {
        this.setCancelable(0);
        this.setTitle("License Agreement");
        android.view.View v1_1 = android.view.LayoutInflater.from(p6).inflate(2130903107, 0, 0);
        this.g = ((com.teamspeak.ts3client.customs.CustomLicenseAgreementWebView) v1_1.findViewById(2131493262));
        this.g.setWebViewClient(new com.teamspeak.ts3client.d.u(this));
        android.view.ViewGroup$LayoutParams v0_8 = ((android.widget.Button) v1_1.findViewById(2131493259));
        v0_8.setText(com.teamspeak.ts3client.data.e.a.a("button.reject"));
        v0_8.setOnClickListener(new com.teamspeak.ts3client.d.v(this));
        android.view.ViewGroup$LayoutParams v0_11 = ((android.widget.Button) v1_1.findViewById(2131493260));
        v0_11.setText(com.teamspeak.ts3client.data.e.a.a("button.browser"));
        v0_11.setOnClickListener(new com.teamspeak.ts3client.d.w(this));
        this.i = ((android.widget.Button) v1_1.findViewById(2131493261));
        this.i.setText(com.teamspeak.ts3client.data.e.a.a("button.accept"));
        this.i.setOnClickListener(new com.teamspeak.ts3client.d.x(this));
        this.g.setOnBottomReachedListener(this);
        this.addContentView(v1_1, new android.view.ViewGroup$LayoutParams(-1, -1));
        return;
    }

    static synthetic void a(com.teamspeak.ts3client.d.t p3, String p4)
    {
        p3.h = p4;
        p3.g.loadData(p4, "text/html; charset=UTF-8", 0);
        p3.e.n();
        return;
    }

    private void a(String p4)
    {
        this.h = p4;
        this.g.loadData(p4, "text/html; charset=UTF-8", 0);
        this.e.n();
        return;
    }

    static synthetic String b(com.teamspeak.ts3client.d.t p1)
    {
        return p1.j;
    }

    static synthetic String b(com.teamspeak.ts3client.d.t p0, String p1)
    {
        p0.f = p1;
        return p1;
    }

    private void b(String p1)
    {
        this.j = p1;
        return;
    }

    static synthetic com.teamspeak.ts3client.d.k c(com.teamspeak.ts3client.d.t p1)
    {
        return p1.a;
    }

    private String c()
    {
        return this.j;
    }

    static synthetic int d(com.teamspeak.ts3client.d.t p1)
    {
        return p1.c;
    }

    static synthetic String e(com.teamspeak.ts3client.d.t p1)
    {
        return p1.f;
    }

    static synthetic com.teamspeak.ts3client.d.k f(com.teamspeak.ts3client.d.t p1)
    {
        return p1.d;
    }

    static synthetic void g(com.teamspeak.ts3client.d.t p0)
    {
        p0.b();
        return;
    }

    public final void a()
    {
        if (this.i != null) {
            this.i.setEnabled(1);
        }
        return;
    }

    public final void a(com.teamspeak.ts3client.d.k p1)
    {
        this.a = p1;
        return;
    }

    public final void a(com.teamspeak.ts3client.d.l p4)
    {
        this.f = com.teamspeak.ts3client.Ts3Application.a().e.getString("lang_tag", java.util.Locale.getDefault().getLanguage());
        this.e = p4;
        this.b();
        return;
    }

    public final void b()
    {
        this.j = new StringBuilder("http://la.teamspeak.com/").append(this.c).append("/").append(this.f).append("/la.html").toString();
        com.teamspeak.ts3client.d.y v0_8 = new com.teamspeak.ts3client.d.y(this, 0);
        String[] v1_6 = new String[1];
        v1_6[0] = this.j;
        v0_8.execute(v1_6);
        return;
    }

    public final void b(com.teamspeak.ts3client.d.k p1)
    {
        this.b = p1;
        return;
    }

    public final void c(com.teamspeak.ts3client.d.k p1)
    {
        this.d = p1;
        return;
    }
}
