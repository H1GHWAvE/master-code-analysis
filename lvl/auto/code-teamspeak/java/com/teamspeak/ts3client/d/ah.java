package com.teamspeak.ts3client.d;
final class ah implements android.widget.AdapterView$OnItemSelectedListener {
    final synthetic com.teamspeak.ts3client.d.ae a;

    ah(com.teamspeak.ts3client.d.ae p1)
    {
        this.a = p1;
        return;
    }

    public final void onItemSelected(android.widget.AdapterView p5, android.view.View p6, int p7, long p8)
    {
        if ((p7 == 3) && (!com.teamspeak.ts3client.d.ae.l(this.a))) {
            android.app.AlertDialog v0_5 = new android.app.AlertDialog$Builder(p6.getContext()).create();
            com.teamspeak.ts3client.data.d.q.a(v0_5);
            v0_5.setTitle(com.teamspeak.ts3client.data.e.a.a("virtualservereditdialog.dialog.info"));
            v0_5.setMessage(com.teamspeak.ts3client.data.e.a.a("virtualservereditdialog.dialog.text"));
            v0_5.setButton(-1, com.teamspeak.ts3client.data.e.a.a("button.ok"), new com.teamspeak.ts3client.d.ai(this, v0_5));
            v0_5.setCancelable(0);
            v0_5.show();
        }
        return;
    }

    public final void onNothingSelected(android.widget.AdapterView p1)
    {
        return;
    }
}
