package com.teamspeak.ts3client.d.d;
final class p implements android.view.View$OnClickListener {
    final synthetic com.teamspeak.ts3client.d.d.n a;

    p(com.teamspeak.ts3client.d.d.n p1)
    {
        this.a = p1;
        return;
    }

    public final void onClick(android.view.View p5)
    {
        IllegalStateException v0_1 = new android.content.Intent("android.intent.action.SEND");
        v0_1.setType("text/plain");
        v0_1.putExtra("android.intent.extra.SUBJECT", "TeamSpeak 3 Temporary Password");
        v0_1.putExtra("android.intent.extra.TEXT", new StringBuilder().append(new StringBuilder("You are Invited to join ").append(com.teamspeak.ts3client.d.d.n.a(this.a).a.m()).append("!\n\nts3server://").append(com.teamspeak.ts3client.d.d.n.a(this.a).a.t).append("?password=").append(this.a.at.c).toString()).append("\n\nhttp://www.teamspeak.com/invite/").append(com.teamspeak.ts3client.d.d.n.a(this.a).a.t).append("/?password=").append(this.a.at.c).append("\n\nYou can Download TeamSpeak 3 for free on http://www.teamspeak.com/").toString());
        String v1_18 = this.a;
        if (v1_18.N != null) {
            v1_18.N.a(v1_18, v0_1, -1);
            return;
        } else {
            throw new IllegalStateException(new StringBuilder("Fragment ").append(v1_18).append(" not attached to Activity").toString());
        }
    }
}
