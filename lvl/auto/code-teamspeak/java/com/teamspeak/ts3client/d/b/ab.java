package com.teamspeak.ts3client.d.b;
public final class ab extends android.support.v4.app.ax {
    private com.teamspeak.ts3client.data.c at;
    private android.widget.Button au;
    private android.widget.TextView av;
    private android.widget.TextView aw;
    private android.widget.TextView ax;
    private android.widget.Spinner ay;

    public ab(com.teamspeak.ts3client.data.c p1)
    {
        this.at = p1;
        return;
    }

    static synthetic android.widget.TextView a(com.teamspeak.ts3client.d.b.ab p1)
    {
        return p1.ax;
    }

    static synthetic android.widget.Spinner b(com.teamspeak.ts3client.d.b.ab p1)
    {
        return p1.ay;
    }

    static synthetic com.teamspeak.ts3client.data.c c(com.teamspeak.ts3client.d.b.ab p1)
    {
        return p1.at;
    }

    static synthetic android.widget.TextView d(com.teamspeak.ts3client.d.b.ab p1)
    {
        return p1.aw;
    }

    public final android.view.View a(android.view.LayoutInflater p8, android.view.ViewGroup p9)
    {
        com.teamspeak.ts3client.Ts3Application v0_2 = ((com.teamspeak.ts3client.Ts3Application) p8.getContext().getApplicationContext());
        android.widget.LinearLayout v1_2 = ((android.widget.LinearLayout) p8.inflate(2130903085, p9, 0));
        this.j.setTitle(com.teamspeak.ts3client.data.e.a.a("clientdialog.ban.info"));
        com.teamspeak.ts3client.data.e.a.a("clientdialog.ban.name", v1_2, 2131493186);
        com.teamspeak.ts3client.data.e.a.a("clientdialog.ban.reason", v1_2, 2131493188);
        com.teamspeak.ts3client.data.e.a.a("clientdialog.ban.duration", v1_2, 2131493190);
        ((android.widget.Spinner) v1_2.findViewById(2131493193)).setAdapter(com.teamspeak.ts3client.data.e.a.a("clientdialog.ban.duration.array", this.i(), 4));
        this.au = ((android.widget.Button) v1_2.findViewById(2131493194));
        this.au.setText(com.teamspeak.ts3client.data.e.a.a("clientdialog.ban.button"));
        this.av = ((android.widget.TextView) v1_2.findViewById(2131493187));
        this.aw = ((android.widget.TextView) v1_2.findViewById(2131493189));
        this.ax = ((android.widget.TextView) v1_2.findViewById(2131493192));
        this.ay = ((android.widget.Spinner) v1_2.findViewById(2131493193));
        this.av.setText(new StringBuilder().append(this.at.a).append(" / ").append(this.at.c).toString());
        this.au.setOnClickListener(new com.teamspeak.ts3client.d.b.ac(this, v0_2));
        return v1_2;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
