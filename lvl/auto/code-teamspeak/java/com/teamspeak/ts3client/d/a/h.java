package com.teamspeak.ts3client.d.a;
final class h implements android.view.View$OnClickListener {
    final synthetic com.teamspeak.ts3client.d.a.a a;

    h(com.teamspeak.ts3client.d.a.a p1)
    {
        this.a = p1;
        return;
    }

    public final void onClick(android.view.View p11)
    {
        com.teamspeak.ts3client.d.a.a v0_1 = new java.util.ArrayList();
        v0_1.addAll(com.teamspeak.ts3client.d.a.a.y().a.f.a);
        Object[] v1_5 = new java.util.ArrayList();
        android.app.Dialog v2_0 = v0_1.iterator();
        while (v2_0.hasNext()) {
            com.teamspeak.ts3client.d.a.a v0_13 = ((com.teamspeak.ts3client.data.a) v2_0.next());
            if (v0_13.b != com.teamspeak.ts3client.d.a.a.a(this.a).b) {
                v1_5.add(v0_13);
            }
        }
        android.app.Dialog v2_2 = new android.app.Dialog(p11.getContext());
        com.teamspeak.ts3client.data.d.q.a(v2_2);
        String v3_1 = new android.widget.LinearLayout(p11.getContext());
        v3_1.setOrientation(1);
        com.teamspeak.ts3client.d.a.k v4_1 = android.view.View.inflate(p11.getContext(), 2130903110, 0);
        com.teamspeak.ts3client.d.a.a v0_8 = ((android.widget.Spinner) v4_1.findViewById(2131493264));
        v4_1.setLayoutParams(new android.view.ViewGroup$LayoutParams(-1, -2));
        android.widget.ArrayAdapter v5_4 = new android.widget.ArrayAdapter(p11.getContext(), 17367049, v1_5);
        Object[] v1_7 = new android.widget.Button(this.a.i());
        v1_7.setEnabled(0);
        android.widget.Button v6_5 = new android.widget.Button(this.a.i());
        v0_8.setAdapter(v5_4);
        v0_8.setOnItemSelectedListener(new com.teamspeak.ts3client.d.a.i(this, v1_7));
        v3_1.addView(v4_1);
        v6_5.setText(com.teamspeak.ts3client.data.e.a.a("dialog.channel.move.info1"));
        v6_5.setOnClickListener(new com.teamspeak.ts3client.d.a.j(this, v5_4, v0_8, v2_2));
        v3_1.addView(v6_5);
        v1_7.setText(com.teamspeak.ts3client.data.e.a.a("dialog.channel.move.info2"));
        v1_7.setOnClickListener(new com.teamspeak.ts3client.d.a.k(this, v5_4, v0_8, v2_2));
        v3_1.addView(v1_7);
        v2_2.setContentView(v3_1);
        Object[] v1_8 = new Object[1];
        v1_8[0] = com.teamspeak.ts3client.d.a.a.a(this.a).a;
        v2_2.setTitle(com.teamspeak.ts3client.data.e.a.a("dialog.channel.move.info", v1_8));
        v2_2.show();
        this.a.b();
        return;
    }
}
