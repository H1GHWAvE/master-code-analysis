package com.teamspeak.ts3client.d.d;
public final class i extends android.support.v4.app.Fragment implements com.teamspeak.ts3client.data.w {
    private com.teamspeak.ts3client.Ts3Application a;
    private android.widget.ListView b;
    private com.teamspeak.ts3client.d.d.a c;
    private android.widget.ImageButton d;
    private android.widget.ImageButton e;
    private com.teamspeak.ts3client.d.d.i f;

    public i()
    {
        return;
    }

    static synthetic com.teamspeak.ts3client.d.d.a a(com.teamspeak.ts3client.d.d.i p1)
    {
        return p1.c;
    }

    public final android.view.View a(android.view.LayoutInflater p4, android.view.ViewGroup p5)
    {
        this.a = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        this.a.c = this;
        android.view.View v1_1 = p4.inflate(2130903128, p5, 0);
        this.b = ((android.widget.ListView) v1_1.findViewById(2131493347));
        this.d = ((android.widget.ImageButton) v1_1.findViewById(2131493349));
        this.d.setOnClickListener(new com.teamspeak.ts3client.d.d.l(this));
        this.e = ((android.widget.ImageButton) v1_1.findViewById(2131493350));
        this.e.setOnClickListener(new com.teamspeak.ts3client.d.d.m(this));
        this.n();
        this.a.o.b(com.teamspeak.ts3client.data.e.a.a("menu.temporarypasswords"));
        return v1_1;
    }

    public final void a()
    {
        this.a.a.c.a(this);
        if (this.c != null) {
            this.c.a.clear();
        } else {
            this.c = new com.teamspeak.ts3client.d.d.a(this.i().getApplicationContext(), this.M);
        }
        this.b.setAdapter(this.c);
        this.a.a.a.ts3client_requestServerTemporaryPasswordList(this.a.a.e, "request temppasslist");
        return;
    }

    public final void a(android.app.Activity p1)
    {
        super.a(p1);
        return;
    }

    public final void a(android.os.Bundle p2)
    {
        super.a(p2);
        this.a = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        this.f = this;
        return;
    }

    public final void a(android.view.Menu p1, android.view.MenuInflater p2)
    {
        p1.clear();
        super.a(p1, p2);
        return;
    }

    public final void a(com.teamspeak.ts3client.jni.k p18)
    {
        if (((p18 instanceof com.teamspeak.ts3client.jni.events.ServerError)) && ((((com.teamspeak.ts3client.jni.events.ServerError) p18).b == 0) && ((((com.teamspeak.ts3client.jni.events.ServerError) p18).c.equals("CreatedNewTempPw")) || (((com.teamspeak.ts3client.jni.events.ServerError) p18).c.equals("DeletedOldTempPw"))))) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.d.d.j(this));
        }
        if ((p18 instanceof com.teamspeak.ts3client.jni.events.rare.ServerTemporaryPasswordList)) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.d.d.k(this, new com.teamspeak.ts3client.d.d.c(((com.teamspeak.ts3client.jni.events.rare.ServerTemporaryPasswordList) p18).a, ((com.teamspeak.ts3client.jni.events.rare.ServerTemporaryPasswordList) p18).b, ((com.teamspeak.ts3client.jni.events.rare.ServerTemporaryPasswordList) p18).c, ((com.teamspeak.ts3client.jni.events.rare.ServerTemporaryPasswordList) p18).d, ((com.teamspeak.ts3client.jni.events.rare.ServerTemporaryPasswordList) p18).e, ((com.teamspeak.ts3client.jni.events.rare.ServerTemporaryPasswordList) p18).f, ((com.teamspeak.ts3client.jni.events.rare.ServerTemporaryPasswordList) p18).g, ((com.teamspeak.ts3client.jni.events.rare.ServerTemporaryPasswordList) p18).h, ((com.teamspeak.ts3client.jni.events.rare.ServerTemporaryPasswordList) p18).i)));
        }
        return;
    }

    public final void c(android.os.Bundle p1)
    {
        super.c(p1);
        this.a();
        return;
    }

    public final void g()
    {
        super.g();
        this.a.a.c.b(this);
        return;
    }
}
