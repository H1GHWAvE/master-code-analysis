package com.teamspeak.ts3client.d.b;
public final class ah extends android.support.v4.app.ax {
    private com.teamspeak.ts3client.data.c at;
    private android.widget.EditText au;
    private android.widget.TextView av;

    public ah(com.teamspeak.ts3client.data.c p1)
    {
        this.at = p1;
        return;
    }

    static synthetic com.teamspeak.ts3client.data.c a(com.teamspeak.ts3client.d.b.ah p1)
    {
        return p1.at;
    }

    static synthetic android.widget.EditText b(com.teamspeak.ts3client.d.b.ah p1)
    {
        return p1.au;
    }

    public final android.view.View a(android.view.LayoutInflater p12, android.view.ViewGroup p13)
    {
        android.app.Dialog v0_2 = ((com.teamspeak.ts3client.Ts3Application) p12.getContext().getApplicationContext());
        android.widget.RelativeLayout v1_1 = new android.widget.RelativeLayout(p12.getContext());
        v1_1.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.av = new android.widget.TextView(p12.getContext());
        this.av.setId(1);
        String v2_6 = this.av;
        com.teamspeak.ts3client.d.b.ai v4_0 = new Object[1];
        v4_0[0] = this.at.a;
        v2_6.setText(com.teamspeak.ts3client.data.e.a.a("clientdialog.poke.text", v4_0));
        String v2_8 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v2_8.addRule(10);
        v1_1.addView(this.av, v2_8);
        this.au = new android.widget.EditText(p12.getContext());
        this.au.setId(2);
        String v2_12 = new android.text.InputFilter[1];
        v2_12[0] = new android.text.InputFilter$LengthFilter(100);
        this.au.setFilters(v2_12);
        String v2_14 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v2_14.addRule(3, this.av.getId());
        v1_1.addView(this.au, v2_14);
        String v2_16 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v2_16.addRule(3, this.au.getId());
        android.widget.Button v3_16 = new android.widget.Button(p12.getContext());
        v3_16.setText(com.teamspeak.ts3client.data.e.a.a("clientdialog.poke.info"));
        v3_16.setOnClickListener(new com.teamspeak.ts3client.d.b.ai(this, v0_2, v1_1));
        v1_1.addView(v3_16, v2_16);
        this.j.setTitle(this.at.a);
        return v1_1;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
