package com.teamspeak.ts3client.data.d;
public final class i extends android.os.AsyncTask {
    final synthetic com.teamspeak.ts3client.data.d.h a;

    public i(com.teamspeak.ts3client.data.d.h p1)
    {
        this.a = p1;
        return;
    }

    private varargs Boolean a()
    {
        Boolean v0_0 = new byte[1];
        try {
            Boolean v0_4;
            String v1_1 = new java.net.DatagramSocket();
            v1_1.setSoTimeout(10000);
            int v2_2 = java.net.InetAddress.getByName(com.teamspeak.ts3client.data.d.h.b);
            byte[] v3_4 = new StringBuilder("ip4:").append(java.net.InetAddress.getByName(com.teamspeak.ts3client.data.d.h.a(this.a)).getHostAddress()).toString().getBytes();
            v1_1.send(new java.net.DatagramPacket(v3_4, v3_4.length, v2_2, com.teamspeak.ts3client.data.d.h.a));
            int v2_4 = new java.net.DatagramPacket(v0_0, 1);
            v1_1.receive(v2_4);
            Boolean v0_2 = new String(v2_4.getData(), 0, v2_4.getLength());
            v1_1.close();
        } catch (Boolean v0) {
            v0_4 = Boolean.valueOf(1);
            return v0_4;
        } catch (Boolean v0) {
            v0_4 = Boolean.valueOf(1);
            return v0_4;
        } catch (Boolean v0) {
            v0_4 = Boolean.valueOf(1);
            return v0_4;
        }
        if (!v0_2.equals("0")) {
            v0_4 = Boolean.valueOf(1);
            return v0_4;
        } else {
            v0_4 = Boolean.valueOf(0);
            return v0_4;
        }
    }

    private void a(Boolean p8)
    {
        super.onPostExecute(p8);
        if ((!p8.booleanValue()) && (com.teamspeak.ts3client.data.d.h.b(this.a).a != null)) {
            com.teamspeak.ts3client.data.d.h.b(this.a).d.log(java.util.logging.Level.INFO, "Server is blacklisted!");
            com.teamspeak.ts3client.data.d.h.b(this.a).a.k.a(com.teamspeak.ts3client.data.e.a.a("blacklist.warning"), com.teamspeak.ts3client.data.e.a.a("blacklist.text"), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.exit"));
            com.teamspeak.ts3client.data.d.h.b(this.a).a.k.a();
        }
        return;
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return this.a();
    }

    protected final synthetic void onPostExecute(Object p8)
    {
        super.onPostExecute(((Boolean) p8));
        if ((!((Boolean) p8).booleanValue()) && (com.teamspeak.ts3client.data.d.h.b(this.a).a != null)) {
            com.teamspeak.ts3client.data.d.h.b(this.a).d.log(java.util.logging.Level.INFO, "Server is blacklisted!");
            com.teamspeak.ts3client.data.d.h.b(this.a).a.k.a(com.teamspeak.ts3client.data.e.a.a("blacklist.warning"), com.teamspeak.ts3client.data.e.a.a("blacklist.text"), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), com.teamspeak.ts3client.data.e.a.a("button.exit"));
            com.teamspeak.ts3client.data.d.h.b(this.a).a.k.a();
        }
        return;
    }
}
