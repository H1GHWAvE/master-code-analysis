package com.teamspeak.ts3client.data.b;
final class g extends android.database.sqlite.SQLiteOpenHelper {
    final synthetic com.teamspeak.ts3client.data.b.f a;

    public g(com.teamspeak.ts3client.data.b.f p4, android.content.Context p5)
    {
        this.a = p4;
        this(p5, "Teamspeak-Ident", 0, 1);
        return;
    }

    public final android.database.sqlite.SQLiteDatabase a(android.content.Context p2)
    {
        p2.getDatabasePath("Teamspeak-Ident").delete();
        return super.getWritableDatabase();
    }

    public final void onCreate(android.database.sqlite.SQLiteDatabase p5)
    {
        p5.execSQL("create table ident (ident_id integer primary key autoincrement, name text not null, identity text not null, nickname text not null, defaultid int not null);");
        Exception v0_1 = com.teamspeak.ts3client.jni.Ts3Jni.b();
        v0_1.ts3client_startInit();
        if (com.teamspeak.ts3client.jni.Ts3Jni.a() != 5) {
            Exception v0_2 = v0_1.ts3client_createIdentity();
            android.content.ContentValues v1_2 = new android.content.ContentValues();
            v1_2.put("name", "Android Default");
            v1_2.put("identity", v0_2);
            v1_2.put("nickname", "TeamSpeakAndroidUser");
            v1_2.put("defaultid", "1");
            try {
                p5.insert("ident", 0, v1_2);
            } catch (Exception v0) {
            }
        }
        return;
    }

    public final void onUpgrade(android.database.sqlite.SQLiteDatabase p1, int p2, int p3)
    {
        return;
    }
}
