package com.teamspeak.ts3client.data.d;
public final enum class aa extends java.lang.Enum {
    public static final enum com.teamspeak.ts3client.data.d.aa a;
    public static final enum com.teamspeak.ts3client.data.d.aa b;
    public static final enum com.teamspeak.ts3client.data.d.aa c;
    public static final enum com.teamspeak.ts3client.data.d.aa d;
    private static final synthetic com.teamspeak.ts3client.data.d.aa[] e;

    static aa()
    {
        com.teamspeak.ts3client.data.d.aa.a = new com.teamspeak.ts3client.data.d.aa("PRESET_CUSTOM", 0);
        com.teamspeak.ts3client.data.d.aa.b = new com.teamspeak.ts3client.data.d.aa("PRESET_VOICE_MOBILE", 1);
        com.teamspeak.ts3client.data.d.aa.c = new com.teamspeak.ts3client.data.d.aa("PRESET_VOICE_DESKTOP", 2);
        com.teamspeak.ts3client.data.d.aa.d = new com.teamspeak.ts3client.data.d.aa("PRESET_MUSIC", 3);
        com.teamspeak.ts3client.data.d.aa[] v0_9 = new com.teamspeak.ts3client.data.d.aa[4];
        v0_9[0] = com.teamspeak.ts3client.data.d.aa.a;
        v0_9[1] = com.teamspeak.ts3client.data.d.aa.b;
        v0_9[2] = com.teamspeak.ts3client.data.d.aa.c;
        v0_9[3] = com.teamspeak.ts3client.data.d.aa.d;
        com.teamspeak.ts3client.data.d.aa.e = v0_9;
        return;
    }

    private aa(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    public static com.teamspeak.ts3client.data.d.aa valueOf(String p1)
    {
        return ((com.teamspeak.ts3client.data.d.aa) Enum.valueOf(com.teamspeak.ts3client.data.d.aa, p1));
    }

    public static com.teamspeak.ts3client.data.d.aa[] values()
    {
        return ((com.teamspeak.ts3client.data.d.aa[]) com.teamspeak.ts3client.data.d.aa.e.clone());
    }
}
