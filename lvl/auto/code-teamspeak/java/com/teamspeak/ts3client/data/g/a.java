package com.teamspeak.ts3client.data.g;
public final class a implements java.lang.Comparable {
    public long a;
    public String b;
    public int c;
    public long d;
    public int e;
    public int f;
    private long g;
    private int h;
    private int i;
    private int j;
    private int k;

    public a(long p6, long p8, String p10, int p11, long p12, int p14, int p15, int p16, int p17, int p18, int p19)
    {
        this.g = p6;
        this.a = p8;
        this.b = p10;
        this.c = p11;
        this.d = (2.1219957905e-314 & p12);
        this.h = p14;
        this.i = p15;
        this.j = p16;
        this.k = p17;
        this.e = p18;
        this.f = p19;
        return;
    }

    private int a(com.teamspeak.ts3client.data.g.a p9)
    {
        int v0_1;
        if (this.i == 0) {
            v0_1 = ((int) this.a);
        } else {
            v0_1 = this.i;
        }
        int v3_1;
        if (p9.i == 0) {
            v3_1 = ((int) p9.a);
        } else {
            v3_1 = p9.i;
        }
        int v0_2;
        if (v0_1 != v3_1) {
            if (v0_1 >= v3_1) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
        } else {
            if (this.a >= p9.a) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
        }
        int v0_4;
        if (v0_2 == 0) {
            v0_4 = 1;
        } else {
            v0_4 = -1;
        }
        return v0_4;
    }

    private long b()
    {
        return this.d;
    }

    private int c()
    {
        return this.j;
    }

    private int d()
    {
        return this.e;
    }

    private int e()
    {
        return this.f;
    }

    private int f()
    {
        return this.k;
    }

    private int g()
    {
        return this.h;
    }

    private long h()
    {
        return this.g;
    }

    private long i()
    {
        return this.a;
    }

    private int j()
    {
        return this.i;
    }

    private int k()
    {
        return this.c;
    }

    public final String a()
    {
        return this.b;
    }

    public final bridge synthetic int compareTo(Object p9)
    {
        int v0_1;
        if (this.i == 0) {
            v0_1 = ((int) this.a);
        } else {
            v0_1 = this.i;
        }
        int v3_1;
        if (((com.teamspeak.ts3client.data.g.a) p9).i == 0) {
            v3_1 = ((int) ((com.teamspeak.ts3client.data.g.a) p9).a);
        } else {
            v3_1 = ((com.teamspeak.ts3client.data.g.a) p9).i;
        }
        int v0_2;
        if (v0_1 != v3_1) {
            if (v0_1 >= v3_1) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
        } else {
            if (this.a >= ((com.teamspeak.ts3client.data.g.a) p9).a) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
        }
        int v0_4;
        if (v0_2 == 0) {
            v0_4 = 1;
        } else {
            v0_4 = -1;
        }
        return v0_4;
    }

    public final String toString()
    {
        return this.b;
    }
}
