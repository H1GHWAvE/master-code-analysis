package com.teamspeak.ts3client.data.b;
final class b extends android.database.sqlite.SQLiteOpenHelper {
    final synthetic com.teamspeak.ts3client.data.b.a a;

    public b(com.teamspeak.ts3client.data.b.a p4, android.content.Context p5)
    {
        this.a = p4;
        this(p5, "Teamspeak-Bookmark", 0, 5);
        return;
    }

    public final void onCreate(android.database.sqlite.SQLiteDatabase p4)
    {
        p4.execSQL("create table server (server_id integer primary key autoincrement, label text not null, address text not null, serverpassword blob null, nickname text not null, defaultchannel text not null, defaultchannelpassword text not null, ident int not null, subscribeall text not null,subscriptionlist int not null);");
        Exception v0_2 = new android.content.ContentValues();
        v0_2.put("label", "TeamSpeak Public");
        v0_2.put("address", "voice.teamspeak.com");
        v0_2.put("serverpassword", com.teamspeak.ts3client.data.b.a.a(""));
        v0_2.put("nickname", "Android_Client");
        v0_2.put("defaultchannel", "Default Channel (Mobile)");
        v0_2.put("defaultchannelpassword", "");
        v0_2.put("ident", Integer.valueOf(1));
        v0_2.put("subscribeall", Integer.valueOf(0));
        v0_2.put("subscriptionlist", "");
        try {
            p4.insert("server", 0, v0_2);
        } catch (Exception v0) {
        }
        return;
    }

    public final void onUpgrade(android.database.sqlite.SQLiteDatabase p2, int p3, int p4)
    {
        if (p3 < p4) {
            while ((p3 - 1) < p4) {
                switch (p3) {
                    case 5:
                        p2.execSQL("DROP TABLE IF EXISTS server");
                        this.onCreate(p2);
                        break;
                }
                p3++;
            }
        }
        return;
    }
}
