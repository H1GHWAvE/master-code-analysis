package com.teamspeak.ts3client.data;
public final class d {
    private java.util.SortedMap a;

    public d()
    {
        this.a = new java.util.TreeMap();
        return;
    }

    private int b()
    {
        return this.a.size();
    }

    public final declared_synchronized java.util.SortedMap a()
    {
        try {
            return this.a;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final declared_synchronized void a(com.teamspeak.ts3client.data.c p3)
    {
        try {
            this.a.put(Integer.valueOf(p3.c), p3);
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final declared_synchronized boolean a(int p3)
    {
        try {
            return this.a.containsKey(Integer.valueOf(p3));
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final declared_synchronized com.teamspeak.ts3client.data.c b(int p3)
    {
        try {
            return ((com.teamspeak.ts3client.data.c) this.a.get(Integer.valueOf(p3)));
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final declared_synchronized void c(int p3)
    {
        try {
            this.a.remove(Integer.valueOf(p3));
            com.teamspeak.ts3client.data.d.q.a(new StringBuilder("cid_").append(p3).toString());
            return;
        } catch (Throwable v0_5) {
            throw v0_5;
        }
    }
}
