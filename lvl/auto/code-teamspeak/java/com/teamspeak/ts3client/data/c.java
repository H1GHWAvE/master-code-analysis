package com.teamspeak.ts3client.data;
public final class c implements com.teamspeak.ts3client.data.w, java.lang.Comparable {
    public int A;
    private com.teamspeak.ts3client.Ts3Application B;
    private String C;
    private java.util.ArrayList D;
    public String a;
    public String b;
    public int c;
    public int d;
    public int e;
    public boolean f;
    boolean g;
    public boolean h;
    public boolean i;
    public boolean j;
    public boolean k;
    boolean l;
    public String m;
    public boolean n;
    public int o;
    public String p;
    public String q;
    public boolean r;
    public boolean s;
    public String t;
    public String u;
    public int v;
    public com.teamspeak.ts3client.c.a w;
    String x;
    public android.graphics.Bitmap y;
    long z;

    public c(com.teamspeak.ts3client.c.a p4)
    {
        this.b = "";
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.l = 0;
        this.m = "";
        this.n = 0;
        this.o = 0;
        this.p = "0";
        this.q = "";
        this.r = 0;
        this.s = 0;
        this.C = "";
        this.D = new java.util.ArrayList();
        this.z = 0;
        this.w = p4;
        this.a = p4.a;
        this.b = p4.c.replace("\'", "");
        return;
    }

    public c(String p2, int p3, com.teamspeak.ts3client.Ts3Application p4)
    {
        this(p2, p3, p4, 0);
        return;
    }

    public c(String p11, int p12, com.teamspeak.ts3client.Ts3Application p13, boolean p14)
    {
        int v1 = 0;
        this.b = "";
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.l = 0;
        this.m = "";
        this.n = 0;
        this.o = 0;
        this.p = "0";
        this.q = "";
        this.r = 0;
        this.s = 0;
        this.C = "";
        this.D = new java.util.ArrayList();
        this.z = 0;
        this.a = com.teamspeak.ts3client.data.d.x.a(p11);
        this.c = p12;
        this.B = p13;
        if (!p14) {
            this.x = p13.a.a.b(p13.a.e, p12, com.teamspeak.ts3client.jni.d.ag);
        }
        long v0_14;
        if (p13.a.a.c(p13.a.e, p12, com.teamspeak.ts3client.jni.d.f) != 0) {
            v0_14 = 1;
        } else {
            v0_14 = 0;
        }
        long v0_18;
        this.f = v0_14;
        if (p13.a.a.c(p13.a.e, p12, com.teamspeak.ts3client.jni.d.g) != 0) {
            v0_18 = 1;
        } else {
            v0_18 = 0;
        }
        long v0_22;
        this.a(v0_18);
        if (p13.a.a.c(p13.a.e, p12, com.teamspeak.ts3client.jni.d.j) != 1) {
            v0_22 = 1;
        } else {
            v0_22 = 0;
        }
        long v0_26;
        this.i = v0_22;
        if (p13.a.a.c(p13.a.e, p12, com.teamspeak.ts3client.jni.d.i) != 1) {
            v0_26 = 1;
        } else {
            v0_26 = 0;
        }
        long v0_39;
        this.h = v0_26;
        this.t = p13.a.a.b(p13.a.e, p12, com.teamspeak.ts3client.jni.d.I);
        this.u = p13.a.a.b(p13.a.e, p12, com.teamspeak.ts3client.jni.d.H);
        this.v = p13.a.a.a(p13.a.e, p12, com.teamspeak.ts3client.jni.d.O);
        if (p13.a.a.a(p13.a.e, p12, com.teamspeak.ts3client.jni.d.M) != 1) {
            v0_39 = 0;
        } else {
            v0_39 = 1;
        }
        long v0_46;
        this.j = v0_39;
        this.m = p13.a.a.b(p13.a.e, p12, com.teamspeak.ts3client.jni.d.N);
        if (p13.a.a.a(p13.a.e, p12, com.teamspeak.ts3client.jni.d.af) != 1) {
            v0_46 = 0;
        } else {
            v0_46 = 1;
        }
        long v0_53;
        this.n = v0_46;
        this.b(p13.a.a.b(p13.a.e, p12, com.teamspeak.ts3client.jni.d.a));
        if (p13.a.a.c(p13.a.e, p12, com.teamspeak.ts3client.jni.d.Z) != 0) {
            v0_53 = 1;
        } else {
            v0_53 = 0;
        }
        this.k = v0_53;
        this.o = p13.a.a.a(p13.a.e, p12, com.teamspeak.ts3client.jni.d.Q);
        this.p = p13.a.a.b(p13.a.e, p12, com.teamspeak.ts3client.jni.d.R);
        this.q = p13.a.a.b(p13.a.e, p12, com.teamspeak.ts3client.jni.d.S);
        this.a(p13.a.a.c(p13.a.e, p12, com.teamspeak.ts3client.jni.d.ae));
        if (p13.a.a.c(p13.a.e, p12, com.teamspeak.ts3client.jni.d.r) != 0) {
            v1 = 1;
        }
        this.s = v1;
        return;
    }

    private void A()
    {
        if (this.w.m != 0) {
            this.B.a.a.ts3client_setClientVolumeModifier(this.B.a.e, this.c, this.w.m);
        }
        if ((!this.w.f) || (this.B.a.h == this.c)) {
            com.teamspeak.ts3client.jni.Ts3Jni v0_13 = this.B.a.a;
            long v2_1 = this.B.a.e;
            int[] v1_7 = new int[2];
            v1_7[0] = this.c;
            v1_7[1] = 0;
            v0_13.ts3client_requestUnmuteClients(v2_1, v1_7, new StringBuilder("UnMute: ").append(this.c).toString());
            this.l = 0;
        } else {
            com.teamspeak.ts3client.jni.Ts3Jni v0_16 = this.B.a.a;
            long v2_2 = this.B.a.e;
            int[] v1_10 = new int[2];
            v1_10[0] = this.c;
            v1_10[1] = 0;
            v0_16.ts3client_requestMuteClients(v2_2, v1_10, new StringBuilder("Mute: ").append(this.c).toString());
            this.l = 1;
        }
        return;
    }

    private int B()
    {
        return this.e;
    }

    private long C()
    {
        return this.z;
    }

    private boolean D()
    {
        return this.l;
    }

    private int E()
    {
        return this.A;
    }

    private boolean F()
    {
        return this.s;
    }

    private int a(com.teamspeak.ts3client.data.c p3)
    {
        int v0_3;
        if ((p3.o - this.o) != 0) {
            v0_3 = (p3.o - this.o);
        } else {
            if ((!this.r) || (!p3.r)) {
                if (!this.r) {
                    if (!p3.r) {
                        v0_3 = this.a.toLowerCase().compareTo(p3.a.toLowerCase());
                    } else {
                        v0_3 = 1;
                    }
                } else {
                    v0_3 = -1;
                }
            } else {
                if (!this.a.toLowerCase().equals(p3.a.toLowerCase())) {
                    v0_3 = this.a.toLowerCase().compareTo(p3.a.toLowerCase());
                } else {
                    v0_3 = this.a.compareTo(p3.a);
                }
            }
        }
        return v0_3;
    }

    private void b(boolean p1)
    {
        this.j = p1;
        return;
    }

    private void c(int p1)
    {
        this.o = p1;
        return;
    }

    private void c(boolean p1)
    {
        this.h = p1;
        return;
    }

    private void d(int p1)
    {
        this.v = p1;
        return;
    }

    private void d(String p1)
    {
        this.m = p1;
        return;
    }

    private void d(boolean p1)
    {
        this.f = p1;
        return;
    }

    private void e(int p1)
    {
        this.c = p1;
        return;
    }

    private void e(String p1)
    {
        this.p = p1;
        return;
    }

    private void e(boolean p1)
    {
        this.n = p1;
        return;
    }

    private void f(int p1)
    {
        this.e = p1;
        return;
    }

    private void f(String p1)
    {
        this.q = p1;
        return;
    }

    private void f(boolean p1)
    {
        this.k = p1;
        return;
    }

    private void g(String p2)
    {
        this.a = com.teamspeak.ts3client.data.d.x.a(p2);
        return;
    }

    private void g(boolean p1)
    {
        this.r = p1;
        return;
    }

    private android.graphics.Bitmap h()
    {
        return this.y;
    }

    private void h(boolean p1)
    {
        this.i = p1;
        return;
    }

    private String i()
    {
        return this.u;
    }

    private void i(boolean p1)
    {
        this.l = p1;
        return;
    }

    private String j()
    {
        return this.m;
    }

    private void j(boolean p1)
    {
        this.s = p1;
        return;
    }

    private int k()
    {
        return this.o;
    }

    private String l()
    {
        return this.p;
    }

    private String m()
    {
        return this.q;
    }

    private String n()
    {
        return this.b;
    }

    private String o()
    {
        return this.x.toLowerCase();
    }

    private int p()
    {
        return this.d;
    }

    private String q()
    {
        return this.t;
    }

    private boolean r()
    {
        return this.j;
    }

    private boolean s()
    {
        return this.h;
    }

    private boolean t()
    {
        return this.f;
    }

    private boolean u()
    {
        return this.n;
    }

    private boolean v()
    {
        return this.k;
    }

    private boolean w()
    {
        return this.r;
    }

    private boolean x()
    {
        return this.i;
    }

    private boolean y()
    {
        return this.g;
    }

    private void z()
    {
        java.util.Iterator v1 = this.D.iterator();
        while (v1.hasNext()) {
            ((com.teamspeak.ts3client.data.d.s) v1.next()).y();
        }
        this.D.clear();
        return;
    }

    public final void a()
    {
        com.teamspeak.ts3client.jni.Ts3Jni v0_1;
        if (this.w == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        this.w = com.teamspeak.ts3client.data.b.c.a.a(this.b);
        if (this.B.a != null) {
            if (this.w == null) {
                if (v0_1 != null) {
                    this.B.a.a.ts3client_setClientVolumeModifier(this.B.a.e, this.c, 0);
                }
                if (this.l) {
                    this.l = 0;
                    com.teamspeak.ts3client.jni.Ts3Jni v0_8 = this.B.a.a;
                    long v4_2 = this.B.a.e;
                    int[] v3_11 = new int[2];
                    v3_11[0] = this.c;
                    v3_11[1] = 0;
                    v0_8.ts3client_requestUnmuteClients(v4_2, v3_11, new StringBuilder("Unmute: ").append(this.c).toString());
                }
            } else {
                this.A();
            }
        }
        return;
    }

    public final void a(int p1)
    {
        this.d = p1;
        return;
    }

    public final void a(long p4)
    {
        this.z = (2.1219957905e-314 & p4);
        return;
    }

    public final void a(android.graphics.Bitmap p3)
    {
        this.y = p3;
        java.util.Iterator v1 = this.D.iterator();
        while (v1.hasNext()) {
            ((com.teamspeak.ts3client.data.d.s) v1.next()).y();
        }
        this.D.clear();
        return;
    }

    public final void a(com.teamspeak.ts3client.data.d.s p3)
    {
        if (this.y == null) {
            if (!this.D.contains(p3)) {
                this.D.add(p3);
            }
            if (this.D.size() == 1) {
                new com.teamspeak.ts3client.data.c.a(this);
            }
        }
        return;
    }

    public final void a(com.teamspeak.ts3client.jni.k p3)
    {
        if (((p3 instanceof com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID)) && (((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p3).a == this.c)) {
            this.b = ((com.teamspeak.ts3client.jni.events.rare.ClientUIDfromClientID) p3).b;
            this.B.a.c.b(this);
        }
        return;
    }

    public final void a(String p1)
    {
        this.u = p1;
        return;
    }

    public final void a(boolean p6)
    {
        if ((!p6) && ((this.w != null) && (this.w.m != 0))) {
            this.B.a.a.ts3client_setClientVolumeModifier(this.B.a.e, this.c, this.w.m);
        }
        this.g = p6;
        return;
    }

    public final String b()
    {
        if (this.C != null) {
            com.teamspeak.ts3client.chat.a v0_1 = this.C;
            this.C = this.B.a.a.b(this.B.a.e, this.c, com.teamspeak.ts3client.jni.d.P);
            if (!v0_1.equals(this.C)) {
                this.y = 0;
                if (com.teamspeak.ts3client.chat.d.a().c(this.b).booleanValue()) {
                    com.teamspeak.ts3client.chat.d.a().d(this.b).a();
                }
            }
        }
        return this.C;
    }

    public final void b(int p1)
    {
        this.A = p1;
        return;
    }

    public final void b(String p2)
    {
        this.b = p2;
        this.w = com.teamspeak.ts3client.data.b.c.a.a(p2);
        try {
            if ((this.B.a != null) && (this.w != null)) {
                this.A();
            }
        } catch (com.teamspeak.ts3client.c.a v0) {
        }
        return;
    }

    public final int c()
    {
        return this.v;
    }

    public final void c(String p1)
    {
        this.t = p1;
        return;
    }

    public final synthetic int compareTo(Object p3)
    {
        int v0_3;
        if ((((com.teamspeak.ts3client.data.c) p3).o - this.o) != 0) {
            v0_3 = (((com.teamspeak.ts3client.data.c) p3).o - this.o);
        } else {
            if ((!this.r) || (!((com.teamspeak.ts3client.data.c) p3).r)) {
                if (!this.r) {
                    if (!((com.teamspeak.ts3client.data.c) p3).r) {
                        v0_3 = this.a.toLowerCase().compareTo(((com.teamspeak.ts3client.data.c) p3).a.toLowerCase());
                    } else {
                        v0_3 = 1;
                    }
                } else {
                    v0_3 = -1;
                }
            } else {
                if (!this.a.toLowerCase().equals(((com.teamspeak.ts3client.data.c) p3).a.toLowerCase())) {
                    v0_3 = this.a.toLowerCase().compareTo(((com.teamspeak.ts3client.data.c) p3).a.toLowerCase());
                } else {
                    v0_3 = this.a.compareTo(((com.teamspeak.ts3client.data.c) p3).a);
                }
            }
        }
        return v0_3;
    }

    public final String d()
    {
        return this.a;
    }

    public final com.teamspeak.ts3client.c.a e()
    {
        return this.w;
    }

    public final int f()
    {
        return this.c;
    }

    public final void g()
    {
        this.B.a.c.a(this);
        this.B.a.a.ts3client_requestClientUIDfromClientID(this.B.a.e, this.c, new StringBuilder("Request UID ").append(this.c).toString());
        return;
    }
}
