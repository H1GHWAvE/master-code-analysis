package com.teamspeak.ts3client.data;
public class customExpandableListView extends android.widget.ExpandableListView {
    int a;
    private boolean b;
    private android.os.Handler c;
    private Runnable d;

    public customExpandableListView(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3);
        this.a = 0;
        this.b = 1;
        this.c = new android.os.Handler();
        this.d = new com.teamspeak.ts3client.data.ak(this);
        this.setContentDescription("Server View");
        return;
    }

    protected void onDraw(android.graphics.Canvas p5)
    {
        super.onDraw(p5);
        com.teamspeak.ts3client.t v0_0 = this.getWidth();
        if ((v0_0 != this.a) && (v0_0 > null)) {
            this.a = v0_0;
            if (this.b) {
                this.c.postDelayed(this.d, 800);
                this.b = 0;
            }
            if ((com.teamspeak.ts3client.Ts3Application.a() != null) && (com.teamspeak.ts3client.Ts3Application.a().a != null)) {
                com.teamspeak.ts3client.Ts3Application.a().a.k.y();
            }
        }
        return;
    }
}
