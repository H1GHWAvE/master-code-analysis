package com.teamspeak.ts3client.data.d;
public final class j {
    java.io.File a;
    String b;
    int c;
    String d;
    com.teamspeak.ts3client.Ts3Application e;
    android.app.ProgressDialog f;
    String g;
    int h;
    private com.teamspeak.ts3client.data.d.k i;

    private j(java.io.File p2, String p3, int p4, String p5, com.teamspeak.ts3client.Ts3Application p6, android.content.Context p7, com.teamspeak.ts3client.data.d.k p8, String p9, int p10)
    {
        this.a = p2;
        this.b = p3;
        this.c = p4;
        this.d = p5;
        this.e = p6;
        this.i = p8;
        this.g = p9;
        this.h = p10;
        this.f = new android.app.ProgressDialog(p7);
        return;
    }

    private String a()
    {
        return this.g;
    }

    private static synthetic String a(com.teamspeak.ts3client.data.d.j p1)
    {
        return p1.b;
    }

    private static synthetic int b(com.teamspeak.ts3client.data.d.j p1)
    {
        return p1.h;
    }

    private void b()
    {
        this.e.d.log(java.util.logging.Level.INFO, new StringBuilder("Updating Content_").append(this.d).toString());
        if (android.os.Build$VERSION.SDK_INT < 11) {
            Void[] v1_2 = new Void[0];
            new com.teamspeak.ts3client.data.d.l(this).execute(v1_2);
        } else {
            Void[] v2_4 = new Void[0];
            new com.teamspeak.ts3client.data.d.l(this).executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR, v2_4);
        }
        return;
    }

    private static synthetic String c(com.teamspeak.ts3client.data.d.j p1)
    {
        return p1.d;
    }

    private static synthetic int d(com.teamspeak.ts3client.data.d.j p1)
    {
        return p1.c;
    }

    private static synthetic com.teamspeak.ts3client.Ts3Application e(com.teamspeak.ts3client.data.d.j p1)
    {
        return p1.e;
    }

    private static synthetic java.io.File f(com.teamspeak.ts3client.data.d.j p1)
    {
        return p1.a;
    }

    private static synthetic android.app.ProgressDialog g(com.teamspeak.ts3client.data.d.j p1)
    {
        return p1.f;
    }

    private static synthetic String h(com.teamspeak.ts3client.data.d.j p1)
    {
        return p1.g;
    }

    private static synthetic com.teamspeak.ts3client.data.d.k i(com.teamspeak.ts3client.data.d.j p1)
    {
        return p1.i;
    }

    public final String toString()
    {
        return new StringBuilder("ContentDownloader [targetPath=").append(this.a).append(", NewVersion=").append(this.c).append(", Setting=").append(this.d).append(", app=").append(this.e).append(", event=").append(this.i).append(", dialog=").append(this.f).append(", description=").append(this.g).append(", size=").append(this.h).append("]").toString();
    }
}
