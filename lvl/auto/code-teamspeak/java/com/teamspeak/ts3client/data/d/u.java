package com.teamspeak.ts3client.data.d;
public final class u {

    public u()
    {
        return;
    }

    public static boolean a(android.content.Context p4)
    {
        int v0_5;
        int v0_2 = ((android.net.ConnectivityManager) p4.getSystemService("connectivity"));
        boolean v3_0 = v0_2.getNetworkInfo(1);
        if ((!v3_0) || (!v3_0.isConnected())) {
            boolean v3_2 = v0_2.getNetworkInfo(0);
            if ((!v3_2) || (!v3_2.isConnected())) {
                int v0_3 = v0_2.getActiveNetworkInfo();
                if ((v0_3 == 0) || (!v0_3.isConnected())) {
                    v0_5 = 0;
                } else {
                    v0_5 = 1;
                }
            } else {
                v0_5 = 1;
            }
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public static boolean b(android.content.Context p4)
    {
        int v0_5;
        int v0_2 = ((android.net.ConnectivityManager) p4.getSystemService("connectivity"));
        if ((v0_2.getNetworkInfo(1) == null) || (!v0_2.getNetworkInfo(1).isConnected())) {
            if (v0_2.getNetworkInfo(0) != null) {
                if (v0_2.getNetworkInfo(0).getType() != 0) {
                    v0_5 = 0;
                } else {
                    v0_5 = 1;
                }
            } else {
                v0_5 = 0;
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    private static android.net.NetworkInfo c(android.content.Context p1)
    {
        return ((android.net.ConnectivityManager) p1.getSystemService("connectivity")).getActiveNetworkInfo();
    }

    private static int d(android.content.Context p2)
    {
        int v0_4;
        int v0_2 = ((android.net.ConnectivityManager) p2.getSystemService("connectivity"));
        if (v0_2.getActiveNetworkInfo() != null) {
            v0_4 = v0_2.getActiveNetworkInfo().getType();
        } else {
            v0_4 = -1;
        }
        return v0_4;
    }

    private static boolean e(android.content.Context p4)
    {
        int v0_5;
        int v0_2 = ((android.net.ConnectivityManager) p4.getSystemService("connectivity"));
        if (v0_2.getNetworkInfo(1) != null) {
            if (v0_2.getNetworkInfo(1).getType() != 1) {
                v0_5 = 0;
            } else {
                v0_5 = 1;
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    private static boolean f(android.content.Context p2)
    {
        boolean v0_4;
        boolean v0_2 = ((android.net.ConnectivityManager) p2.getSystemService("connectivity"));
        if (v0_2.getActiveNetworkInfo() != null) {
            v0_4 = v0_2.getActiveNetworkInfo().isAvailable();
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    private static boolean g(android.content.Context p4)
    {
        int v0 = 1;
        if (android.os.Build$VERSION.SDK_INT < 17) {
            if (android.provider.Settings$System.getInt(p4.getContentResolver(), "airplane_mode_on", 0) == 0) {
                v0 = 0;
            }
        } else {
            if (android.provider.Settings$System.getInt(p4.getContentResolver(), "airplane_mode_on", 0) == 0) {
                v0 = 0;
            }
        }
        return v0;
    }
}
