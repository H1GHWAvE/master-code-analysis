package com.teamspeak.ts3client.data.b;
public final class a {
    public static com.teamspeak.ts3client.data.b.a a = None;
    private static final String b = "create table server (server_id integer primary key autoincrement, label text not null, address text not null, serverpassword blob null, nickname text not null, defaultchannel text not null, defaultchannelpassword text not null, ident int not null, subscribeall text not null,subscriptionlist int not null);";
    private static final String c = "server";
    private static final String d = "Teamspeak-Bookmark";
    private static final int e = 5;
    private android.database.sqlite.SQLiteDatabase f;

    public a(android.content.Context p2)
    {
        this.f = new com.teamspeak.ts3client.data.b.b(this, p2).getWritableDatabase();
        com.teamspeak.ts3client.data.b.a.a = this;
        return;
    }

    private static String a(byte[] p4)
    {
        String v0_5;
        if (p4 != null) {
            java.util.logging.Level v2_1 = new javax.crypto.spec.IvParameterSpec("mnd783bnsd03bs72".getBytes());
            String v0_3 = new javax.crypto.spec.SecretKeySpec("abd732ns0Sg37s3S".getBytes(), "AES");
            try {
                java.util.logging.Logger v1_3 = javax.crypto.Cipher.getInstance("AES/CBC/PKCS5Padding");
                v1_3.init(2, v0_3, v2_1);
                v0_5 = new String(v1_3.doFinal(p4));
            } catch (String v0_16) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v0_16.getMessage());
                v0_5 = "";
            } catch (String v0_10) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v0_10.getMessage());
            } catch (String v0_14) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v0_14.getMessage());
            } catch (String v0_12) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v0_12.getMessage());
            } catch (String v0_6) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v0_6.getMessage());
            } catch (String v0_8) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v0_8.getMessage());
            }
        } else {
            v0_5 = "";
        }
        return v0_5;
    }

    static byte[] a(String p5)
    {
        byte[] v0 = 0;
        if (!p5.equals("")) {
            java.util.logging.Level v3_1 = new javax.crypto.spec.IvParameterSpec("mnd783bnsd03bs72".getBytes());
            String v1_5 = new javax.crypto.spec.SecretKeySpec("abd732ns0Sg37s3S".getBytes(), "AES");
            try {
                java.util.logging.Logger v2_3 = javax.crypto.Cipher.getInstance("AES/CBC/PKCS5Padding");
                v2_3.init(1, v1_5, v3_1);
                v0 = v2_3.doFinal(p5.getBytes());
            } catch (String v1_17) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v1_17.getMessage());
            } catch (String v1_9) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v1_9.getMessage());
            } catch (String v1_13) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v1_13.getMessage());
            } catch (String v1_15) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v1_15.getMessage());
            } catch (String v1_7) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v1_7.getMessage());
            } catch (String v1_11) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, v1_11.getMessage());
            }
        }
        return v0;
    }

    private com.teamspeak.ts3client.data.ab b(long p14)
    {
        com.teamspeak.ts3client.data.ab v9_1 = new com.teamspeak.ts3client.data.ab();
        try {
            String v0_0 = this.f;
            long v2_1 = new String[10];
            v2_1[0] = "server_id";
            v2_1[1] = "label";
            v2_1[2] = "address";
            v2_1[3] = "serverpassword";
            v2_1[4] = "nickname";
            v2_1[5] = "defaultchannel";
            v2_1[6] = "defaultchannelpassword";
            v2_1[7] = "ident";
            v2_1[8] = "subscribeall";
            v2_1[9] = "subscriptionlist";
            android.database.Cursor v1_1 = v0_0.query("server", v2_1, new StringBuilder("server_id=").append(p14).toString(), 0, 0, 0, 0, 0);
        } catch (String v0) {
            String v0_32 = 0;
            return v0_32;
        }
        if (v1_1.moveToFirst()) {
            String v0_28;
            v9_1.a = v1_1.getString(v1_1.getColumnIndex("label"));
            v9_1.a(v1_1.getString(v1_1.getColumnIndex("address")).replaceAll("(.*://)+", ""));
            v9_1.e = new String(com.teamspeak.ts3client.data.b.a.a(v1_1.getBlob(v1_1.getColumnIndex("serverpassword"))));
            v9_1.f = v1_1.getString(v1_1.getColumnIndex("nickname"));
            v9_1.g = v1_1.getString(v1_1.getColumnIndex("defaultchannel"));
            v9_1.h = v1_1.getString(v1_1.getColumnIndex("defaultchannelpassword"));
            v9_1.l = v1_1.getLong(v1_1.getColumnIndex("server_id"));
            v9_1.d = v1_1.getInt(v1_1.getColumnIndex("ident"));
            if (v1_1.getInt(v1_1.getColumnIndex("subscribeall")) != 0) {
                v0_28 = 1;
            } else {
                v0_28 = 0;
            }
            v9_1.j = v0_28;
            v9_1.b(v1_1.getString(v1_1.getColumnIndex("subscriptionlist")));
        }
        v1_1.close();
        v0_32 = v9_1;
        return v0_32;
    }

    private static com.teamspeak.ts3client.data.b.a b()
    {
        return com.teamspeak.ts3client.data.b.a.a;
    }

    private static synthetic byte[] b(String p1)
    {
        return com.teamspeak.ts3client.data.b.a.a(p1);
    }

    private void c()
    {
        this.f.close();
        return;
    }

    public final long a(com.teamspeak.ts3client.data.ab p5)
    {
        long v0_9;
        android.content.ContentValues v1_1 = new android.content.ContentValues();
        v1_1.put("label", p5.a);
        v1_1.put("address", p5.c.replaceAll("(.*://)+", ""));
        v1_1.put("serverpassword", com.teamspeak.ts3client.data.b.a.a(p5.e));
        v1_1.put("nickname", p5.f);
        v1_1.put("defaultchannel", p5.g);
        v1_1.put("defaultchannelpassword", p5.h);
        v1_1.put("ident", Integer.valueOf(p5.d));
        if (!p5.j) {
            v0_9 = 0;
        } else {
            v0_9 = 1;
        }
        v1_1.put("subscribeall", Integer.valueOf(v0_9));
        v1_1.put("subscriptionlist", p5.h());
        try {
            long v0_13 = this.f.insert("server", 0, v1_1);
        } catch (long v0_14) {
            v0_14.printStackTrace();
            v0_13 = -1;
        }
        return v0_13;
    }

    public final java.util.ArrayList a()
    {
        java.util.ArrayList v8_1 = new java.util.ArrayList();
        try {
            boolean v0_0 = this.f;
            com.teamspeak.ts3client.data.ab v2_1 = new String[10];
            v2_1[0] = "server_id";
            v2_1[1] = "label";
            v2_1[2] = "address";
            v2_1[3] = "serverpassword";
            v2_1[4] = "nickname";
            v2_1[5] = "defaultchannel";
            v2_1[6] = "defaultchannelpassword";
            v2_1[7] = "ident";
            v2_1[8] = "subscribeall";
            v2_1[9] = "subscriptionlist";
            android.database.Cursor v1_1 = v0_0.query("server", v2_1, 0, 0, 0, 0, 0);
            v1_1.moveToFirst();
        } catch (boolean v0_33) {
            v0_33.printStackTrace();
            boolean v0_34 = 0;
            return v0_34;
        }
        if (!v1_1.isAfterLast()) {
            do {
                boolean v0_28;
                com.teamspeak.ts3client.data.ab v2_3 = new com.teamspeak.ts3client.data.ab();
                v2_3.a = v1_1.getString(v1_1.getColumnIndex("label"));
                v2_3.a(v1_1.getString(v1_1.getColumnIndex("address")).replaceAll("(.*://)+", ""));
                v2_3.e = new String(com.teamspeak.ts3client.data.b.a.a(v1_1.getBlob(v1_1.getColumnIndex("serverpassword"))));
                v2_3.f = v1_1.getString(v1_1.getColumnIndex("nickname"));
                v2_3.g = v1_1.getString(v1_1.getColumnIndex("defaultchannel"));
                v2_3.h = v1_1.getString(v1_1.getColumnIndex("defaultchannelpassword"));
                v2_3.l = v1_1.getLong(v1_1.getColumnIndex("server_id"));
                v2_3.d = v1_1.getInt(v1_1.getColumnIndex("ident"));
                if (v1_1.getInt(v1_1.getColumnIndex("subscribeall")) != 0) {
                    v0_28 = 1;
                } else {
                    v0_28 = 0;
                }
                v2_3.j = v0_28;
                v2_3.b(v1_1.getString(v1_1.getColumnIndex("subscriptionlist")));
                v8_1.add(v2_3);
            } while(v1_1.moveToNext());
        }
        v1_1.close();
        v0_34 = v8_1;
        return v0_34;
    }

    public final boolean a(long p6)
    {
        try {
            this.f.delete("server", new StringBuilder("server_id=").append(p6).toString(), 0);
            int v0_1 = 1;
        } catch (int v0_2) {
            v0_2.printStackTrace();
            v0_1 = 0;
        }
        return v0_1;
    }

    public final boolean a(long p8, com.teamspeak.ts3client.data.ab p10)
    {
        Exception v0_9;
        int v1 = 1;
        android.content.ContentValues v3_1 = new android.content.ContentValues();
        v3_1.put("label", p10.a);
        v3_1.put("address", p10.c.replaceAll("(.*://)+", ""));
        v3_1.put("serverpassword", com.teamspeak.ts3client.data.b.a.a(p10.e));
        v3_1.put("nickname", p10.f);
        v3_1.put("defaultchannel", p10.g);
        v3_1.put("defaultchannelpassword", p10.h);
        v3_1.put("ident", Integer.valueOf(p10.d));
        if (!p10.j) {
            v0_9 = 0;
        } else {
            v0_9 = 1;
        }
        v3_1.put("subscribeall", Integer.valueOf(v0_9));
        v3_1.put("subscriptionlist", p10.h());
        try {
            this.f.update("server", v3_1, new StringBuilder("server_id=").append(p8).toString(), 0);
        } catch (Exception v0_13) {
            v0_13.printStackTrace();
            v1 = 0;
        }
        return v1;
    }
}
