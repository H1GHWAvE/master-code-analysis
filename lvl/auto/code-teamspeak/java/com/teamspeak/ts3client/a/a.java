package com.teamspeak.ts3client.a;
public final class a extends android.support.v4.app.ax {
    private static com.teamspeak.ts3client.a.a au;
    private int aA;
    private android.widget.Spinner aB;
    private android.widget.Spinner aC;
    private boolean aD;
    private android.widget.Spinner aE;
    private android.widget.Spinner aF;
    private android.widget.Button aG;
    private android.widget.Button aH;
    private boolean aI;
    private android.widget.Button aJ;
    private android.widget.TextView aK;
    private android.widget.TextView aL;
    private android.widget.TextView aM;
    private android.widget.TextView aN;
    private boolean aO;
    private android.content.BroadcastReceiver aP;
    private android.widget.TextView aQ;
    public boolean at;
    private com.teamspeak.ts3client.Ts3Application av;
    private com.teamspeak.ts3client.jni.Ts3Jni aw;
    private com.teamspeak.ts3client.a.k ax;
    private long ay;
    private int az;

    private a()
    {
        this.aP = new com.teamspeak.ts3client.a.b(this);
        return;
    }

    private void A()
    {
        this.aG.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.button.start"));
        this.aH.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.button.stop"));
        this.aJ.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        this.aN.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.text.play"));
        this.aM.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.text.rec"));
        this.aK.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.text.playbackstream"));
        this.aL.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.text.recordstream"));
        this.aQ.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.error"));
        return;
    }

    private void B()
    {
        if (this.aD) {
            this.aD = 0;
            if (this.av.a == null) {
                if ((this.ax != null) && (this.aw != null)) {
                    this.ax.e();
                    this.ax.f();
                    this.ax.b();
                    this.ax = 0;
                    this.aw.ts3client_closeCaptureDevice(this.ay);
                    this.aw.ts3client_closePlaybackDevice(this.ay);
                    this.aw.ts3client_destroyServerConnectionHandler(this.ay);
                    this.aw = 0;
                }
            } else {
                if (this.aw == null) {
                    this.aw = com.teamspeak.ts3client.jni.Ts3Jni.b();
                }
                this.aw.ts3client_closeCaptureDevice(this.ay);
                this.aw.ts3client_closePlaybackDevice(this.ay);
                this.aw.ts3client_setLocalTestMode(this.ay, 0);
                this.aw.ts3client_destroyServerConnectionHandler(this.ay);
                this.ax = 0;
                this.aw = 0;
            }
        }
        return;
    }

    private void C()
    {
        this.a(this.aB, this.aC);
        this.av.e.edit().putInt("recordStream", this.aA).commit();
        this.av.e.edit().putInt("playbackStream", this.az).commit();
        this.av.e.edit().putInt("sampleRec", Integer.parseInt(((String) this.aE.getSelectedItem()))).commit();
        this.av.e.edit().putInt("samplePlay", Integer.parseInt(((String) this.aF.getSelectedItem()))).commit();
        this.b();
        return;
    }

    private void D()
    {
        this.b();
        return;
    }

    private boolean E()
    {
        return this.at;
    }

    static synthetic long a(com.teamspeak.ts3client.a.a p1, long p2)
    {
        p1.ay = p2;
        return p2;
    }

    static synthetic com.teamspeak.ts3client.jni.Ts3Jni a(com.teamspeak.ts3client.a.a p0, com.teamspeak.ts3client.jni.Ts3Jni p1)
    {
        p0.aw = p1;
        return p1;
    }

    private void a(android.widget.Spinner p9, android.widget.Spinner p10)
    {
        this.az = 0;
        if (p9.getSelectedItemId() == 0) {
            this.az = 0;
        }
        if (p9.getSelectedItemId() == 1) {
            this.az = 3;
        }
        this.aA = 0;
        if (p10.getSelectedItemId() == 0) {
            this.aA = 0;
        }
        if (p10.getSelectedItemId() == 1) {
            this.aA = 1;
        }
        if (p10.getSelectedItemId() == 2) {
            this.aA = 7;
        }
        return;
    }

    static synthetic void a(com.teamspeak.ts3client.a.a p8, android.widget.Spinner p9, android.widget.Spinner p10)
    {
        if (p8.aD) {
            void v8_1 = p8.a(p8.aB, p8.aC);
            v8_1.aw.ts3client_closeCaptureDevice(v8_1.ay);
            v8_1.aw.ts3client_closePlaybackDevice(v8_1.ay);
            v8_1.aw.ts3client_prepareAudioDevice(Integer.parseInt(((String) p10.getSelectedItem())), Integer.parseInt(((String) p9.getSelectedItem())));
            v8_1.ax = com.teamspeak.ts3client.a.k.a();
            v8_1.ax.d = v8_1.aw;
            if (v8_1.ax.a(Integer.parseInt(((String) p10.getSelectedItem())), Integer.parseInt(((String) p9.getSelectedItem())), v8_1.az, v8_1.aA)) {
                v8_1.ax.a(Boolean.valueOf(1));
                if (!v8_1.aO) {
                    com.teamspeak.ts3client.a.j.a().b(1);
                } else {
                    com.teamspeak.ts3client.a.j.a().a(1);
                }
                v8_1.aw.ts3client_activateCaptureDevice(v8_1.ay);
                v8_1.aw.ts3client_openCaptureDevice(v8_1.ay, "", "");
                v8_1.aw.ts3client_openPlaybackDevice(v8_1.ay, "", "");
                v8_1.aw.a(v8_1.ay, com.teamspeak.ts3client.jni.d.k, 0);
                v8_1.aw.ts3client_setPreProcessorConfigValue(v8_1.ay, "vad", "true");
                v8_1.aw.ts3client_setPreProcessorConfigValue(v8_1.ay, "voiceactivation_level", "-50");
                v8_1.aw.ts3client_setLocalTestMode(v8_1.ay, 1);
                v8_1.ax.d();
                v8_1.ax.c();
            } else {
                v8_1.ax.b();
                v8_1.ax = 0;
                v8_1.aw.ts3client_destroyServerConnectionHandler(v8_1.ay);
                v8_1.aw = 0;
                v8_1.aH.setEnabled(0);
                v8_1.aG.setEnabled(1);
                v8_1.aQ.setVisibility(0);
            }
        }
        return;
    }

    static synthetic boolean a(com.teamspeak.ts3client.a.a p1)
    {
        return p1.aO;
    }

    static synthetic boolean a(com.teamspeak.ts3client.a.a p0, boolean p1)
    {
        p0.aO = p1;
        return p1;
    }

    static synthetic String[] a(com.teamspeak.ts3client.a.a p1, java.util.Vector p2)
    {
        return p1.a(p2);
    }

    private String[] a(java.util.Vector p5)
    {
        String[] v0_6;
        if (p5.size() > 0) {
            String v1_1 = new java.util.Vector();
            java.util.Iterator v2 = p5.iterator();
            while (v2.hasNext()) {
                String[] v0_13 = ((Integer) v2.next()).intValue();
                if (v0_13 != null) {
                    v1_1.add(String.valueOf(v0_13));
                }
            }
            if (v1_1.size() != 0) {
                String[] v0_4 = new String[v1_1.size()];
                v0_6 = ((String[]) v1_1.toArray(v0_4));
            } else {
                this.aG.setEnabled(0);
                this.aJ.setEnabled(0);
                this.aQ.setVisibility(0);
                v0_6 = new String[1];
                v0_6[0] = "ERROR";
            }
        } else {
            v0_6 = new String[0];
        }
        return v0_6;
    }

    static synthetic android.widget.TextView b(com.teamspeak.ts3client.a.a p1)
    {
        return p1.aQ;
    }

    private void b(android.widget.Spinner p9, android.widget.Spinner p10)
    {
        if (this.aD) {
            this.a(this.aB, this.aC);
            this.aw.ts3client_closeCaptureDevice(this.ay);
            this.aw.ts3client_closePlaybackDevice(this.ay);
            this.aw.ts3client_prepareAudioDevice(Integer.parseInt(((String) p10.getSelectedItem())), Integer.parseInt(((String) p9.getSelectedItem())));
            this.ax = com.teamspeak.ts3client.a.k.a();
            this.ax.d = this.aw;
            if (this.ax.a(Integer.parseInt(((String) p10.getSelectedItem())), Integer.parseInt(((String) p9.getSelectedItem())), this.az, this.aA)) {
                this.ax.a(Boolean.valueOf(1));
                if (!this.aO) {
                    com.teamspeak.ts3client.a.j.a().b(1);
                } else {
                    com.teamspeak.ts3client.a.j.a().a(1);
                }
                this.aw.ts3client_activateCaptureDevice(this.ay);
                this.aw.ts3client_openCaptureDevice(this.ay, "", "");
                this.aw.ts3client_openPlaybackDevice(this.ay, "", "");
                this.aw.a(this.ay, com.teamspeak.ts3client.jni.d.k, 0);
                this.aw.ts3client_setPreProcessorConfigValue(this.ay, "vad", "true");
                this.aw.ts3client_setPreProcessorConfigValue(this.ay, "voiceactivation_level", "-50");
                this.aw.ts3client_setLocalTestMode(this.ay, 1);
                this.ax.d();
                this.ax.c();
            } else {
                this.ax.b();
                this.ax = 0;
                this.aw.ts3client_destroyServerConnectionHandler(this.ay);
                this.aw = 0;
                this.aH.setEnabled(0);
                this.aG.setEnabled(1);
                this.aQ.setVisibility(0);
            }
        }
        return;
    }

    static synthetic void b(com.teamspeak.ts3client.a.a p0, android.widget.Spinner p1, android.widget.Spinner p2)
    {
        p0.a(p1, p2);
        return;
    }

    static synthetic boolean c(com.teamspeak.ts3client.a.a p1)
    {
        p1.aI = 0;
        return 0;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application d(com.teamspeak.ts3client.a.a p1)
    {
        return p1.av;
    }

    static synthetic com.teamspeak.ts3client.jni.Ts3Jni e(com.teamspeak.ts3client.a.a p1)
    {
        return p1.aw;
    }

    static synthetic boolean f(com.teamspeak.ts3client.a.a p1)
    {
        p1.aD = 1;
        return 1;
    }

    static synthetic android.widget.Spinner g(com.teamspeak.ts3client.a.a p1)
    {
        return p1.aF;
    }

    static synthetic android.widget.Spinner h(com.teamspeak.ts3client.a.a p1)
    {
        return p1.aE;
    }

    static synthetic android.widget.Button i(com.teamspeak.ts3client.a.a p1)
    {
        return p1.aH;
    }

    static synthetic android.widget.Button j(com.teamspeak.ts3client.a.a p1)
    {
        return p1.aG;
    }

    static synthetic void k(com.teamspeak.ts3client.a.a p3)
    {
        void v3_1 = p3.a(p3.aB, p3.aC);
        v3_1.av.e.edit().putInt("recordStream", v3_1.aA).commit();
        v3_1.av.e.edit().putInt("playbackStream", v3_1.az).commit();
        v3_1.av.e.edit().putInt("sampleRec", Integer.parseInt(((String) v3_1.aE.getSelectedItem()))).commit();
        v3_1.av.e.edit().putInt("samplePlay", Integer.parseInt(((String) v3_1.aF.getSelectedItem()))).commit();
        v3_1.b();
        return;
    }

    static synthetic android.widget.Spinner l(com.teamspeak.ts3client.a.a p1)
    {
        return p1.aB;
    }

    static synthetic android.widget.Spinner m(com.teamspeak.ts3client.a.a p1)
    {
        return p1.aC;
    }

    static synthetic boolean n(com.teamspeak.ts3client.a.a p1)
    {
        return p1.aI;
    }

    static synthetic com.teamspeak.ts3client.a.k o(com.teamspeak.ts3client.a.a p1)
    {
        return p1.ax;
    }

    static synthetic com.teamspeak.ts3client.a.k p(com.teamspeak.ts3client.a.a p1)
    {
        p1.ax = 0;
        return 0;
    }

    static synthetic int q(com.teamspeak.ts3client.a.a p1)
    {
        return p1.az;
    }

    static synthetic int r(com.teamspeak.ts3client.a.a p1)
    {
        return p1.aA;
    }

    static synthetic void s(com.teamspeak.ts3client.a.a p0)
    {
        p0.B();
        return;
    }

    public static com.teamspeak.ts3client.a.a y()
    {
        if (com.teamspeak.ts3client.a.a.au == null) {
            com.teamspeak.ts3client.a.a.au = new com.teamspeak.ts3client.a.a();
        }
        return com.teamspeak.ts3client.a.a.au;
    }

    private void z()
    {
        if (this.av.e.contains("playbackStream")) {
            if (this.av.e.getInt("playbackStream", 0) != 0) {
                this.aB.setSelection(1);
            } else {
                this.aB.setSelection(0);
            }
            if (this.av.e.getInt("recordStream", 0) != 0) {
                if (this.av.e.getInt("recordStream", 0) != 1) {
                    if (this.av.e.getInt("recordStream", 0) == 7) {
                        this.aC.setSelection(2);
                    }
                } else {
                    this.aC.setSelection(1);
                }
            } else {
                this.aC.setSelection(0);
            }
            this.a(this.aB, this.aC);
            java.util.Vector v3_2 = com.teamspeak.ts3client.a.k.a(this.i(), this.az, this.aA);
            int v4_0 = com.teamspeak.ts3client.a.k.a(this.i(), this.az);
            this.aF.setAdapter(new android.widget.ArrayAdapter(this.i(), 17367049, this.a(v4_0)));
            this.aE.setAdapter(new android.widget.ArrayAdapter(this.i(), 17367049, this.a(v3_2)));
            int v1_14 = 0;
            while (v1_14 < v4_0.size()) {
                if (((Integer) v4_0.get(Integer.valueOf(v1_14).intValue())).intValue() != this.av.e.getInt("samplePlay", 0)) {
                    v1_14++;
                } else {
                    this.aF.setSelection(v1_14);
                    break;
                }
            }
            int v1_15 = 0;
            while (v1_15 < v3_2.size()) {
                if (((Integer) v3_2.get(Integer.valueOf(v1_15).intValue())).intValue() != this.av.e.getInt("sampleRec", 0)) {
                    v1_15++;
                } else {
                    this.aE.setSelection(v1_15);
                    break;
                }
            }
        }
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p8, android.view.ViewGroup p9)
    {
        this.at = 1;
        this.j.setCanceledOnTouchOutside(0);
        this.j.setTitle(com.teamspeak.ts3client.data.e.a.a("audiosettings.text"));
        if (this.av.a != null) {
            this.av.a.a.ts3client_closeCaptureDevice(this.av.a.e);
            this.av.a.a.ts3client_closePlaybackDevice(this.av.a.e);
        }
        this.aI = 1;
        android.view.View v1_6 = p8.inflate(2130903064, p9, 0);
        this.aQ = ((android.widget.TextView) v1_6.findViewById(2131492981));
        this.aQ.setVisibility(8);
        this.aG = ((android.widget.Button) v1_6.findViewById(2131492967));
        this.aG.setOnClickListener(new com.teamspeak.ts3client.a.c(this));
        this.aJ = ((android.widget.Button) v1_6.findViewById(2131492982));
        this.aJ.setOnClickListener(new com.teamspeak.ts3client.a.d(this));
        this.aK = ((android.widget.TextView) v1_6.findViewById(2131492970));
        this.aL = ((android.widget.TextView) v1_6.findViewById(2131492971));
        this.aM = ((android.widget.TextView) v1_6.findViewById(2131492977));
        this.aN = ((android.widget.TextView) v1_6.findViewById(2131492976));
        this.aF = ((android.widget.Spinner) v1_6.findViewById(2131492979));
        this.aF.setAdapter(new android.widget.ArrayAdapter(this.i(), 17367049, this.a(com.teamspeak.ts3client.a.k.a(this.i(), 0, 0))));
        this.aF.setOnItemSelectedListener(new com.teamspeak.ts3client.a.e(this));
        this.aE = ((android.widget.Spinner) v1_6.findViewById(2131492980));
        this.aE.setAdapter(new android.widget.ArrayAdapter(this.i(), 17367049, this.a(com.teamspeak.ts3client.a.k.a(this.i(), 0))));
        this.aE.setOnItemSelectedListener(new com.teamspeak.ts3client.a.f(this));
        this.aB = ((android.widget.Spinner) v1_6.findViewById(2131492973));
        this.aB.setAdapter(com.teamspeak.ts3client.data.e.a.a("audiosettings.array.play", this.i(), 2));
        this.aB.setOnItemSelectedListener(new com.teamspeak.ts3client.a.g(this));
        this.aC = ((android.widget.Spinner) v1_6.findViewById(2131492974));
        this.aC.setAdapter(com.teamspeak.ts3client.data.e.a.a("audiosettings.array.rec", this.i(), 3));
        this.aC.setOnItemSelectedListener(new com.teamspeak.ts3client.a.h(this));
        this.aH = ((android.widget.Button) v1_6.findViewById(2131492968));
        this.aH.setOnClickListener(new com.teamspeak.ts3client.a.i(this));
        this.aH.setEnabled(0);
        this.aG.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.button.start"));
        this.aH.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.button.stop"));
        this.aJ.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        this.aN.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.text.play"));
        this.aM.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.text.rec"));
        this.aK.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.text.playbackstream"));
        this.aL.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.text.recordstream"));
        this.aQ.setText(com.teamspeak.ts3client.data.e.a.a("audiosettings.error"));
        this.z();
        return v1_6;
    }

    public final void a(android.os.Bundle p2)
    {
        super.a(p2);
        this.av = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        return;
    }

    public final void c(android.os.Bundle p1)
    {
        super.c(p1);
        return;
    }

    public final void e()
    {
        super.e();
        return;
    }

    public final void onDismiss(android.content.DialogInterface p2)
    {
        this.B();
        if (this.av.a != null) {
            this.av.a.r();
        }
        this.at = 0;
        com.teamspeak.ts3client.a.a.au = 0;
        super.onDismiss(p2);
        return;
    }

    public final void s()
    {
        android.content.IntentFilter v0_1 = new android.content.IntentFilter();
        v0_1.addAction("android.intent.action.HEADSET_PLUG");
        this.i().registerReceiver(this.aP, v0_1);
        super.s();
        return;
    }

    public final void t()
    {
        this.i().unregisterReceiver(this.aP);
        super.t();
        return;
    }
}
