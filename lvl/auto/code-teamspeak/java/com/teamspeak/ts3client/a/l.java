package com.teamspeak.ts3client.a;
final class l extends java.lang.Thread {
    final synthetic com.teamspeak.ts3client.a.k a;
    private short[] b;

    l(com.teamspeak.ts3client.a.k p1)
    {
        this.a = p1;
        return;
    }

    public final void run()
    {
        android.os.Process.setThreadPriority(-19);
        int v0_1 = 1;
        try {
            while(true) {
                if (v0_1 == 0) {
                    int v0_2 = 1;
                    while (this.a.e) {
                        this.b = this.a.d.requestAudioData();
                        if (this.b == null) {
                            this.a.f.stop();
                            this.a.f.flush();
                            try {
                                Thread.sleep(100);
                                v0_2 = 0;
                            } catch (int v0) {
                                v0_2 = 0;
                            }
                        } else {
                            if (v0_2 == 0) {
                                this.a.f.play();
                                v0_2 = 1;
                            }
                            if (this.b != null) {
                                this.a.f.flush();
                                this.a.f.write(this.b, 0, ((this.a.j / 100) * 2));
                            }
                        }
                    }
                } else {
                    if (this.a.f != null) {
                        break;
                    }
                }
                return;
                v0_1 = 0;
            }
        } catch (int v0) {
            try {
                this.a.h.d.log(java.util.logging.Level.SEVERE, "Audio Play IllegalStateException, retry");
                Thread.sleep(100);
                v0_1 = 1;
            } catch (int v0) {
            }
        }
        this.a.f.play();
        v0_1 = 0;
    }
}
