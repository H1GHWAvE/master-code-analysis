package com.teamspeak.ts3client.b;
public final class b extends android.support.v4.app.Fragment implements com.teamspeak.ts3client.data.w {
    private com.teamspeak.ts3client.b.f a;
    private com.teamspeak.ts3client.Ts3Application b;
    private java.util.Vector c;

    public b()
    {
        return;
    }

    static synthetic com.teamspeak.ts3client.b.f a(com.teamspeak.ts3client.b.b p1)
    {
        return p1.a;
    }

    private void a()
    {
        this.c = new java.util.Vector();
        this.a.clear();
        this.b.a.c.a(this);
        this.b.a.a.ts3client_requestBanList(this.b.a.e, "requestbannlist");
        return;
    }

    static synthetic java.util.Vector b(com.teamspeak.ts3client.b.b p1)
    {
        return p1.c;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application c(com.teamspeak.ts3client.b.b p1)
    {
        return p1.b;
    }

    static synthetic void d(com.teamspeak.ts3client.b.b p0)
    {
        p0.a();
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p5, android.view.ViewGroup p6)
    {
        android.view.View v2 = p5.inflate(2130903068, p6, 0);
        android.support.v7.app.a v0_3 = ((android.widget.ListView) v2.findViewById(2131493002));
        ((android.widget.EditText) v2.findViewById(2131493003)).addTextChangedListener(new com.teamspeak.ts3client.b.c(this));
        this.a = new com.teamspeak.ts3client.b.f(this, this.i());
        v0_3.setAdapter(this.a);
        this.b = com.teamspeak.ts3client.Ts3Application.a();
        this.b.c = this;
        this.n();
        this.b.o.b(com.teamspeak.ts3client.data.e.a.a("menu.banlist"));
        return v2;
    }

    public final void a(android.view.Menu p1, android.view.MenuInflater p2)
    {
        p1.clear();
        super.a(p1, p2);
        return;
    }

    public final void a(com.teamspeak.ts3client.jni.k p4)
    {
        if ((p4 instanceof com.teamspeak.ts3client.jni.events.rare.BanList)) {
            com.teamspeak.ts3client.Ts3Application.a().c.i().runOnUiThread(new com.teamspeak.ts3client.b.e(this, ((com.teamspeak.ts3client.jni.events.rare.BanList) p4)));
        }
        if (((p4 instanceof com.teamspeak.ts3client.jni.events.ServerError)) && (((com.teamspeak.ts3client.jni.events.ServerError) p4).c.equals("requestbannlist"))) {
            this.b.a.c.b(this);
        }
        return;
    }

    public final void e()
    {
        super.e();
        this.c = new java.util.Vector();
        if (this.a != null) {
            this.a.clear();
        }
        return;
    }

    public final void f()
    {
        super.f();
        return;
    }

    public final void s()
    {
        super.s();
        this.a();
        return;
    }
}
