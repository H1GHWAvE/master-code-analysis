package android.support.v4.media.session;
final class bj implements android.os.Parcelable$Creator {

    bj()
    {
        return;
    }

    private static android.support.v4.media.session.PlaybackStateCompat a(android.os.Parcel p2)
    {
        return new android.support.v4.media.session.PlaybackStateCompat(p2, 0);
    }

    private static android.support.v4.media.session.PlaybackStateCompat[] a(int p1)
    {
        android.support.v4.media.session.PlaybackStateCompat[] v0 = new android.support.v4.media.session.PlaybackStateCompat[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p3)
    {
        return new android.support.v4.media.session.PlaybackStateCompat(p3, 0);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.media.session.PlaybackStateCompat[] v0 = new android.support.v4.media.session.PlaybackStateCompat[p2];
        return v0;
    }
}
