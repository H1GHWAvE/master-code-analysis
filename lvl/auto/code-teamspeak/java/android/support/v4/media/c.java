package android.support.v4.media;
public class c {

    public c()
    {
        return;
    }

    private static Object a(android.os.Parcel p1)
    {
        return android.media.MediaDescription.CREATOR.createFromParcel(p1);
    }

    private static String a(Object p1)
    {
        return ((android.media.MediaDescription) p1).getMediaId();
    }

    private static void a(Object p0, android.os.Parcel p1, int p2)
    {
        ((android.media.MediaDescription) p0).writeToParcel(p1, p2);
        return;
    }

    private static CharSequence b(Object p1)
    {
        return ((android.media.MediaDescription) p1).getTitle();
    }

    private static CharSequence c(Object p1)
    {
        return ((android.media.MediaDescription) p1).getSubtitle();
    }

    private static CharSequence d(Object p1)
    {
        return ((android.media.MediaDescription) p1).getDescription();
    }

    private static android.graphics.Bitmap e(Object p1)
    {
        return ((android.media.MediaDescription) p1).getIconBitmap();
    }

    private static android.net.Uri f(Object p1)
    {
        return ((android.media.MediaDescription) p1).getIconUri();
    }

    private static android.os.Bundle g(Object p1)
    {
        return ((android.media.MediaDescription) p1).getExtras();
    }
}
