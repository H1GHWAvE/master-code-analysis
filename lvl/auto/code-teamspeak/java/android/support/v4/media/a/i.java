package android.support.v4.media.a;
public final class i {

    public i()
    {
        return;
    }

    private static void a(Object p0, int p1)
    {
        ((android.media.MediaRouter$UserRouteInfo) p0).setPlaybackType(p1);
        return;
    }

    private static void a(Object p0, android.graphics.drawable.Drawable p1)
    {
        ((android.media.MediaRouter$UserRouteInfo) p0).setIconDrawable(p1);
        return;
    }

    private static void a(Object p0, CharSequence p1)
    {
        ((android.media.MediaRouter$UserRouteInfo) p0).setName(p1);
        return;
    }

    private static void a(Object p0, Object p1)
    {
        ((android.media.MediaRouter$UserRouteInfo) p0).setVolumeCallback(((android.media.MediaRouter$VolumeCallback) p1));
        return;
    }

    private static void b(Object p0, int p1)
    {
        ((android.media.MediaRouter$UserRouteInfo) p0).setPlaybackStream(p1);
        return;
    }

    private static void b(Object p0, CharSequence p1)
    {
        ((android.media.MediaRouter$UserRouteInfo) p0).setStatus(p1);
        return;
    }

    private static void b(Object p0, Object p1)
    {
        ((android.media.MediaRouter$UserRouteInfo) p0).setRemoteControlClient(((android.media.RemoteControlClient) p1));
        return;
    }

    private static void c(Object p0, int p1)
    {
        ((android.media.MediaRouter$UserRouteInfo) p0).setVolume(p1);
        return;
    }

    private static void d(Object p0, int p1)
    {
        ((android.media.MediaRouter$UserRouteInfo) p0).setVolumeMax(p1);
        return;
    }

    private static void e(Object p0, int p1)
    {
        ((android.media.MediaRouter$UserRouteInfo) p0).setVolumeHandling(p1);
        return;
    }
}
