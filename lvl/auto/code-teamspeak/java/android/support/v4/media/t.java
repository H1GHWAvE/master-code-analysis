package android.support.v4.media;
public final class t extends android.support.v4.media.s {
    public static final int i = 126;
    public static final int j = 127;
    public static final int k = 130;
    public static final int l = 1;
    public static final int m = 2;
    public static final int n = 4;
    public static final int o = 8;
    public static final int p = 16;
    public static final int q = 32;
    public static final int r = 64;
    public static final int s = 128;
    final android.content.Context a;
    final android.support.v4.media.ac b;
    final android.media.AudioManager c;
    final android.view.View d;
    final Object e;
    final android.support.v4.media.x f;
    final java.util.ArrayList g;
    final android.support.v4.media.w h;
    final android.view.KeyEvent$Callback t;

    private t(android.app.Activity p2, android.support.v4.media.ac p3)
    {
        this(p2, 0, p3);
        return;
    }

    private t(android.app.Activity p6, android.view.View p7, android.support.v4.media.ac p8)
    {
        int v0_6;
        this.g = new java.util.ArrayList();
        this.h = new android.support.v4.media.u(this);
        this.t = new android.support.v4.media.v(this);
        if (p6 == null) {
            v0_6 = p7.getContext();
        } else {
            v0_6 = p6;
        }
        this.a = v0_6;
        this.b = p8;
        this.c = ((android.media.AudioManager) this.a.getSystemService("audio"));
        if (p6 != null) {
            p7 = p6.getWindow().getDecorView();
        }
        this.d = p7;
        this.e = android.support.v4.view.ab.a(this.d);
        if (android.os.Build$VERSION.SDK_INT < 18) {
            this.f = 0;
        } else {
            this.f = new android.support.v4.media.x(this.a, this.c, this.d, this.h);
        }
        return;
    }

    private t(android.view.View p2, android.support.v4.media.ac p3)
    {
        this(0, p2, p3);
        return;
    }

    static boolean a(int p1)
    {
        int v0;
        switch (p1) {
            case 79:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 126:
            case 127:
            case 130:
                v0 = 1;
                break;
            default:
                v0 = 0;
        }
        return v0;
    }

    private boolean a(android.view.KeyEvent p3)
    {
        return android.support.v4.view.ab.a(p3, this.t, this.e, this);
    }

    private Object j()
    {
        int v0_1;
        if (this.f == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.f.m;
        }
        return v0_1;
    }

    private android.support.v4.media.ad[] k()
    {
        android.support.v4.media.ad[] v0_4;
        if (this.g.size() > 0) {
            v0_4 = new android.support.v4.media.ad[this.g.size()];
            this.g.toArray(v0_4);
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    private void l()
    {
        this.k();
        return;
    }

    private void m()
    {
        this.k();
        return;
    }

    private void n()
    {
        if (this.f != null) {
            android.support.v4.media.x v2 = this.f;
            boolean v3 = this.b.g();
            long v4 = this.b.e();
            if (v2.m != null) {
                int v1_0;
                if (!v3) {
                    v1_0 = 1;
                } else {
                    v1_0 = 3;
                }
                android.media.RemoteControlClient v0_6;
                if (!v3) {
                    v0_6 = 0;
                } else {
                    v0_6 = 1065353216;
                }
                v2.m.setPlaybackState(v1_0, v4, v0_6);
                v2.m.setTransportControlFlags(60);
            }
        }
        return;
    }

    private void o()
    {
        this.n();
        this.k();
        this.k();
        return;
    }

    private void p()
    {
        android.view.ViewTreeObserver$OnWindowFocusChangeListener v0_0 = this.f;
        v0_0.d();
        v0_0.c.getViewTreeObserver().removeOnWindowAttachListener(v0_0.h);
        v0_0.c.getViewTreeObserver().removeOnWindowFocusChangeListener(v0_0.i);
        return;
    }

    public final void a()
    {
        if (this.f != null) {
            android.support.v4.media.x v0_1 = this.f;
            if (v0_1.o != 3) {
                v0_1.o = 3;
                v0_1.m.setPlaybackState(3);
            }
            if (v0_1.n) {
                v0_1.a();
            }
        }
        this.n();
        this.k();
        return;
    }

    public final void a(android.support.v4.media.ad p2)
    {
        this.g.add(p2);
        return;
    }

    public final void b()
    {
        if (this.f != null) {
            android.support.v4.media.x v0_1 = this.f;
            if (v0_1.o == 3) {
                v0_1.o = 2;
                v0_1.m.setPlaybackState(2);
            }
            v0_1.b();
        }
        this.n();
        this.k();
        return;
    }

    public final void b(android.support.v4.media.ad p2)
    {
        this.g.remove(p2);
        return;
    }

    public final void c()
    {
        if (this.f != null) {
            android.support.v4.media.x v0_1 = this.f;
            if (v0_1.o != 1) {
                v0_1.o = 1;
                v0_1.m.setPlaybackState(1);
            }
            v0_1.b();
        }
        this.n();
        this.k();
        return;
    }

    public final long d()
    {
        return this.b.d();
    }

    public final long e()
    {
        return this.b.e();
    }

    public final void f()
    {
        return;
    }

    public final boolean g()
    {
        return this.b.g();
    }

    public final int h()
    {
        return 100;
    }

    public final int i()
    {
        return 60;
    }
}
