package android.support.v4.media.session;
final class bq {

    bq()
    {
        return;
    }

    private static Object a(String p1, CharSequence p2, int p3, android.os.Bundle p4)
    {
        android.media.session.PlaybackState$CustomAction v0_1 = new android.media.session.PlaybackState$CustomAction$Builder(p1, p2, p3);
        v0_1.setExtras(p4);
        return v0_1.build();
    }

    private static String a(Object p1)
    {
        return ((android.media.session.PlaybackState$CustomAction) p1).getAction();
    }

    private static CharSequence b(Object p1)
    {
        return ((android.media.session.PlaybackState$CustomAction) p1).getName();
    }

    private static int c(Object p1)
    {
        return ((android.media.session.PlaybackState$CustomAction) p1).getIcon();
    }

    private static android.os.Bundle d(Object p1)
    {
        return ((android.media.session.PlaybackState$CustomAction) p1).getExtras();
    }
}
