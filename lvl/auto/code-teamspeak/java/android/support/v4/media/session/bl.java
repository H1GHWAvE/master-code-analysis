package android.support.v4.media.session;
public final class bl {
    final java.util.List a;
    int b;
    long c;
    long d;
    float e;
    long f;
    CharSequence g;
    long h;
    long i;
    android.os.Bundle j;

    public bl()
    {
        this.a = new java.util.ArrayList();
        this.i = -1;
        return;
    }

    public bl(android.support.v4.media.session.PlaybackStateCompat p3)
    {
        this.a = new java.util.ArrayList();
        this.i = -1;
        this.b = android.support.v4.media.session.PlaybackStateCompat.a(p3);
        this.c = android.support.v4.media.session.PlaybackStateCompat.b(p3);
        this.e = android.support.v4.media.session.PlaybackStateCompat.c(p3);
        this.h = android.support.v4.media.session.PlaybackStateCompat.d(p3);
        this.d = android.support.v4.media.session.PlaybackStateCompat.e(p3);
        this.f = android.support.v4.media.session.PlaybackStateCompat.f(p3);
        this.g = android.support.v4.media.session.PlaybackStateCompat.g(p3);
        if (android.support.v4.media.session.PlaybackStateCompat.h(p3) != null) {
            this.a.addAll(android.support.v4.media.session.PlaybackStateCompat.h(p3));
        }
        this.i = android.support.v4.media.session.PlaybackStateCompat.i(p3);
        this.j = android.support.v4.media.session.PlaybackStateCompat.j(p3);
        return;
    }

    private android.support.v4.media.session.PlaybackStateCompat a()
    {
        return new android.support.v4.media.session.PlaybackStateCompat(this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.a, this.i, this.j, 0);
    }

    private android.support.v4.media.session.bl a(int p9, long p10, float p12)
    {
        return this.a(p9, p10, p12, android.os.SystemClock.elapsedRealtime());
    }

    private android.support.v4.media.session.bl a(long p2)
    {
        this.d = p2;
        return this;
    }

    private android.support.v4.media.session.bl a(android.os.Bundle p1)
    {
        this.j = p1;
        return this;
    }

    private android.support.v4.media.session.bl a(android.support.v4.media.session.PlaybackStateCompat$CustomAction p2)
    {
        this.a.add(p2);
        return this;
    }

    private android.support.v4.media.session.bl a(CharSequence p1)
    {
        this.g = p1;
        return this;
    }

    private android.support.v4.media.session.bl a(String p7, String p8, int p9)
    {
        this.a.add(new android.support.v4.media.session.PlaybackStateCompat$CustomAction(p7, p8, p9, 0, 0));
        return this;
    }

    private android.support.v4.media.session.bl b(long p2)
    {
        this.f = p2;
        return this;
    }

    private android.support.v4.media.session.bl c(long p2)
    {
        this.i = p2;
        return this;
    }

    public final android.support.v4.media.session.bl a(int p1, long p2, float p4, long p5)
    {
        this.b = p1;
        this.c = p2;
        this.h = p5;
        this.e = p4;
        return this;
    }
}
