package android.support.v4.media.session;
public class ParcelableVolumeInfo implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR;
    public int a;
    public int b;
    public int c;
    public int d;
    public int e;

    static ParcelableVolumeInfo()
    {
        android.support.v4.media.session.ParcelableVolumeInfo.CREATOR = new android.support.v4.media.session.bi();
        return;
    }

    public ParcelableVolumeInfo(int p1, int p2, int p3, int p4, int p5)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        this.d = p4;
        this.e = p5;
        return;
    }

    public ParcelableVolumeInfo(android.os.Parcel p2)
    {
        this.a = p2.readInt();
        this.c = p2.readInt();
        this.d = p2.readInt();
        this.e = p2.readInt();
        this.b = p2.readInt();
        return;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(android.os.Parcel p2, int p3)
    {
        p2.writeInt(this.a);
        p2.writeInt(this.c);
        p2.writeInt(this.d);
        p2.writeInt(this.e);
        p2.writeInt(this.b);
        return;
    }
}
