package android.support.v4.media.session;
final class MediaSessionCompat$ResultReceiverWrapper implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR;
    private android.os.ResultReceiver a;

    static MediaSessionCompat$ResultReceiverWrapper()
    {
        android.support.v4.media.session.MediaSessionCompat$ResultReceiverWrapper.CREATOR = new android.support.v4.media.session.aq();
        return;
    }

    MediaSessionCompat$ResultReceiverWrapper(android.os.Parcel p2)
    {
        this.a = ((android.os.ResultReceiver) android.os.ResultReceiver.CREATOR.createFromParcel(p2));
        return;
    }

    public MediaSessionCompat$ResultReceiverWrapper(android.os.ResultReceiver p1)
    {
        this.a = p1;
        return;
    }

    static synthetic android.os.ResultReceiver a(android.support.v4.media.session.MediaSessionCompat$ResultReceiverWrapper p1)
    {
        return p1.a;
    }

    public final int describeContents()
    {
        return 0;
    }

    public final void writeToParcel(android.os.Parcel p2, int p3)
    {
        this.a.writeToParcel(p2, p3);
        return;
    }
}
