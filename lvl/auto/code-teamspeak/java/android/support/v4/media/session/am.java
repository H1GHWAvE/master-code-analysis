package android.support.v4.media.session;
final class am extends android.support.v4.media.session.e {
    final synthetic android.support.v4.media.session.ai G;

    am(android.support.v4.media.session.ai p1)
    {
        this.G = p1;
        return;
    }

    public final void a(int p2, int p3, String p4)
    {
        android.support.v4.media.session.ai.a(this.G, p2, p3);
        return;
    }

    public final void a(long p4)
    {
        this.G.a.a(4, Long.valueOf(p4));
        return;
    }

    public final void a(android.net.Uri p3, android.os.Bundle p4)
    {
        this.G.a.a(18, p3, p4);
        return;
    }

    public final void a(android.support.v4.media.RatingCompat p3)
    {
        this.G.a.a(12, p3);
        return;
    }

    public final void a(android.support.v4.media.session.a p2)
    {
        if (!this.G.g) {
            this.G.f.register(p2);
        } else {
            try {
                p2.a();
            } catch (Exception v0) {
            }
        }
        return;
    }

    public final void a(String p3, android.os.Bundle p4)
    {
        this.G.a.a(2, p3, p4);
        return;
    }

    public final void a(String p5, android.os.Bundle p6, android.support.v4.media.session.MediaSessionCompat$ResultReceiverWrapper p7)
    {
        this.G.a.a(15, new android.support.v4.media.session.al(p5, p6, android.support.v4.media.session.MediaSessionCompat$ResultReceiverWrapper.a(p7)));
        return;
    }

    public final boolean a()
    {
        int v0_3;
        if ((this.G.i & 2) == 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean a(android.view.KeyEvent p4)
    {
        int v0_3;
        if ((this.G.i & 1) == 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        if (v0_3 != 0) {
            this.G.a.a(14, p4);
        }
        return v0_3;
    }

    public final String b()
    {
        return this.G.b;
    }

    public final void b(int p2, int p3, String p4)
    {
        android.support.v4.media.session.ai.b(this.G, p2, p3);
        return;
    }

    public final void b(long p4)
    {
        this.G.a.a(11, Long.valueOf(p4));
        return;
    }

    public final void b(android.support.v4.media.session.a p2)
    {
        this.G.f.unregister(p2);
        return;
    }

    public final void b(String p3, android.os.Bundle p4)
    {
        this.G.a.a(3, p3, p4);
        return;
    }

    public final String c()
    {
        return this.G.c;
    }

    public final void c(String p3, android.os.Bundle p4)
    {
        this.G.a.a(13, p3, p4);
        return;
    }

    public final android.app.PendingIntent d()
    {
        try {
            return this.G.l;
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final long e()
    {
        try {
            return ((long) this.G.i);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final android.support.v4.media.session.ParcelableVolumeInfo f()
    {
        int v3 = 2;
        try {
            int v4;
            int v5;
            int v1 = this.G.q;
            int v2 = this.G.r;
            android.support.v4.media.session.ParcelableVolumeInfo v0_4 = this.G.s;
        } catch (android.support.v4.media.session.ParcelableVolumeInfo v0_9) {
            throw v0_9;
        }
        if (v1 != 2) {
            v4 = this.G.d.getStreamMaxVolume(v2);
            v5 = this.G.d.getStreamVolume(v2);
        } else {
            v3 = v0_4.d;
            v4 = v0_4.e;
            v5 = v0_4.f;
        }
        return new android.support.v4.media.session.ParcelableVolumeInfo(v1, v2, v3, v4, v5);
    }

    public final void g()
    {
        this.G.a.a(1, 0);
        return;
    }

    public final void h()
    {
        this.G.a.a(5, 0);
        return;
    }

    public final void i()
    {
        this.G.a.a(6, 0);
        return;
    }

    public final void j()
    {
        this.G.a.a(7, 0);
        return;
    }

    public final void k()
    {
        this.G.a.a(8, 0);
        return;
    }

    public final void l()
    {
        this.G.a.a(9, 0);
        return;
    }

    public final void m()
    {
        this.G.a.a(10, 0);
        return;
    }

    public final android.support.v4.media.MediaMetadataCompat n()
    {
        return this.G.j;
    }

    public final android.support.v4.media.session.PlaybackStateCompat o()
    {
        return this.G.f();
    }

    public final java.util.List p()
    {
        try {
            return this.G.m;
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final CharSequence q()
    {
        return this.G.n;
    }

    public final android.os.Bundle r()
    {
        try {
            return this.G.p;
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final int s()
    {
        return this.G.o;
    }
}
