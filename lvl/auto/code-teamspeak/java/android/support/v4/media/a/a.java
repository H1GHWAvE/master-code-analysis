package android.support.v4.media.a;
 class a {
    public static final int a = 1;
    public static final int b = 2;
    public static final int c = 8388608;
    public static final int d = 8388611;
    private static final String e = "MediaRouterJellybean";

    a()
    {
        return;
    }

    private static Object a(android.content.Context p1)
    {
        return p1.getSystemService("media_router");
    }

    private static Object a(android.support.v4.media.a.b p1)
    {
        return new android.support.v4.media.a.c(p1);
    }

    private static Object a(android.support.v4.media.a.j p1)
    {
        return new android.support.v4.media.a.k(p1);
    }

    private static Object a(Object p1, int p2)
    {
        return ((android.media.MediaRouter) p1).getSelectedRoute(p2);
    }

    private static Object a(Object p1, String p2, boolean p3)
    {
        return ((android.media.MediaRouter) p1).createRouteCategory(p2, p3);
    }

    private static java.util.List a(Object p4)
    {
        int v1 = ((android.media.MediaRouter) p4).getRouteCount();
        java.util.ArrayList v2_1 = new java.util.ArrayList(v1);
        int v0 = 0;
        while (v0 < v1) {
            v2_1.add(((android.media.MediaRouter) p4).getRouteAt(v0));
            v0++;
        }
        return v2_1;
    }

    private static void a(Object p0, int p1, Object p2)
    {
        ((android.media.MediaRouter) p0).selectRoute(p1, ((android.media.MediaRouter$RouteInfo) p2));
        return;
    }

    private static void a(Object p0, Object p1)
    {
        ((android.media.MediaRouter) p0).removeCallback(((android.media.MediaRouter$Callback) p1));
        return;
    }

    private static Object b(Object p1, Object p2)
    {
        return ((android.media.MediaRouter) p1).createUserRoute(((android.media.MediaRouter$RouteCategory) p2));
    }

    private static java.util.List b(Object p4)
    {
        int v1 = ((android.media.MediaRouter) p4).getCategoryCount();
        java.util.ArrayList v2_1 = new java.util.ArrayList(v1);
        int v0 = 0;
        while (v0 < v1) {
            v2_1.add(((android.media.MediaRouter) p4).getCategoryAt(v0));
            v0++;
        }
        return v2_1;
    }

    private static void b(Object p0, int p1, Object p2)
    {
        ((android.media.MediaRouter) p0).addCallback(p1, ((android.media.MediaRouter$Callback) p2));
        return;
    }

    private static void c(Object p0, Object p1)
    {
        ((android.media.MediaRouter) p0).addUserRoute(((android.media.MediaRouter$UserRouteInfo) p1));
        return;
    }

    private static void d(Object p0, Object p1)
    {
        ((android.media.MediaRouter) p0).removeUserRoute(((android.media.MediaRouter$UserRouteInfo) p1));
        return;
    }
}
