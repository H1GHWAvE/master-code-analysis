package android.support.v4.media.session;
final class az {

    az()
    {
        return;
    }

    private static Object a(android.content.Context p1, String p2)
    {
        return new android.media.session.MediaSession(p1, p2);
    }

    private static Object a(android.support.v4.media.session.ba p1)
    {
        return new android.support.v4.media.session.bb(p1);
    }

    private static Object a(Object p2)
    {
        if (!(p2 instanceof android.media.session.MediaSession)) {
            throw new IllegalArgumentException("mediaSession is not a valid MediaSession object");
        } else {
            return p2;
        }
    }

    private static void a(Object p0, int p1)
    {
        ((android.media.session.MediaSession) p0).setFlags(p1);
        return;
    }

    private static void a(Object p0, android.app.PendingIntent p1)
    {
        ((android.media.session.MediaSession) p0).setSessionActivity(p1);
        return;
    }

    private static void a(Object p0, android.os.Bundle p1)
    {
        ((android.media.session.MediaSession) p0).setExtras(p1);
        return;
    }

    private static void a(Object p0, CharSequence p1)
    {
        ((android.media.session.MediaSession) p0).setQueueTitle(p1);
        return;
    }

    private static void a(Object p0, Object p1)
    {
        ((android.media.session.MediaSession) p0).setPlaybackToRemote(((android.media.VolumeProvider) p1));
        return;
    }

    private static void a(Object p0, Object p1, android.os.Handler p2)
    {
        ((android.media.session.MediaSession) p0).setCallback(((android.media.session.MediaSession$Callback) p1), p2);
        return;
    }

    private static void a(Object p0, String p1, android.os.Bundle p2)
    {
        ((android.media.session.MediaSession) p0).sendSessionEvent(p1, p2);
        return;
    }

    public static void a(Object p3, java.util.List p4)
    {
        if (p4 != null) {
            java.util.ArrayList v1_1 = new java.util.ArrayList();
            java.util.Iterator v2 = p4.iterator();
            while (v2.hasNext()) {
                v1_1.add(((android.media.session.MediaSession$QueueItem) v2.next()));
            }
            ((android.media.session.MediaSession) p3).setQueue(v1_1);
        } else {
            ((android.media.session.MediaSession) p3).setQueue(0);
        }
        return;
    }

    private static void a(Object p0, boolean p1)
    {
        ((android.media.session.MediaSession) p0).setActive(p1);
        return;
    }

    private static Object b(Object p2)
    {
        if (!(p2 instanceof android.media.session.MediaSession$Token)) {
            throw new IllegalArgumentException("token is not a valid MediaSession.Token object");
        } else {
            return p2;
        }
    }

    private static void b(Object p1, int p2)
    {
        android.media.AudioAttributes v0_1 = new android.media.AudioAttributes$Builder();
        v0_1.setLegacyStreamType(p2);
        ((android.media.session.MediaSession) p1).setPlaybackToLocal(v0_1.build());
        return;
    }

    private static void b(Object p0, android.app.PendingIntent p1)
    {
        ((android.media.session.MediaSession) p0).setMediaButtonReceiver(p1);
        return;
    }

    private static void b(Object p0, Object p1)
    {
        ((android.media.session.MediaSession) p0).setPlaybackState(((android.media.session.PlaybackState) p1));
        return;
    }

    private static void c(Object p0, Object p1)
    {
        ((android.media.session.MediaSession) p0).setMetadata(((android.media.MediaMetadata) p1));
        return;
    }

    private static boolean c(Object p1)
    {
        return ((android.media.session.MediaSession) p1).isActive();
    }

    private static void d(Object p0)
    {
        ((android.media.session.MediaSession) p0).release();
        return;
    }

    private static android.os.Parcelable e(Object p1)
    {
        return ((android.media.session.MediaSession) p1).getSessionToken();
    }
}
