package android.support.v4.media.session;
 class s extends android.support.v4.media.session.r {
    protected final Object a;

    public s(Object p1)
    {
        this.a = p1;
        return;
    }

    public final void a()
    {
        ((android.media.session.MediaController$TransportControls) this.a).play();
        return;
    }

    public final void a(long p2)
    {
        ((android.media.session.MediaController$TransportControls) this.a).skipToQueueItem(p2);
        return;
    }

    public void a(android.net.Uri p1, android.os.Bundle p2)
    {
        return;
    }

    public final void a(android.support.v4.media.RatingCompat p3)
    {
        android.media.Rating v1_0;
        android.media.session.MediaController$TransportControls v0_0 = this.a;
        if (p3 == null) {
            v1_0 = 0;
        } else {
            v1_0 = p3.a();
        }
        ((android.media.session.MediaController$TransportControls) v0_0).setRating(((android.media.Rating) v1_0));
        return;
    }

    public final void a(android.support.v4.media.session.PlaybackStateCompat$CustomAction p3, android.os.Bundle p4)
    {
        android.support.v4.media.session.z.a(this.a, p3.a, p4);
        return;
    }

    public final void a(String p2, android.os.Bundle p3)
    {
        ((android.media.session.MediaController$TransportControls) this.a).playFromMediaId(p2, p3);
        return;
    }

    public final void b()
    {
        ((android.media.session.MediaController$TransportControls) this.a).pause();
        return;
    }

    public final void b(long p2)
    {
        ((android.media.session.MediaController$TransportControls) this.a).seekTo(p2);
        return;
    }

    public final void b(String p2, android.os.Bundle p3)
    {
        ((android.media.session.MediaController$TransportControls) this.a).playFromSearch(p2, p3);
        return;
    }

    public final void c()
    {
        ((android.media.session.MediaController$TransportControls) this.a).stop();
        return;
    }

    public final void c(String p2, android.os.Bundle p3)
    {
        android.support.v4.media.session.z.a(this.a, p2, p3);
        return;
    }

    public final void d()
    {
        ((android.media.session.MediaController$TransportControls) this.a).fastForward();
        return;
    }

    public final void e()
    {
        ((android.media.session.MediaController$TransportControls) this.a).skipToNext();
        return;
    }

    public final void f()
    {
        ((android.media.session.MediaController$TransportControls) this.a).rewind();
        return;
    }

    public final void g()
    {
        ((android.media.session.MediaController$TransportControls) this.a).skipToPrevious();
        return;
    }
}
