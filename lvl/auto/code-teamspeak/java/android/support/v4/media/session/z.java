package android.support.v4.media.session;
public class z {

    public z()
    {
        return;
    }

    private static void a(Object p0)
    {
        ((android.media.session.MediaController$TransportControls) p0).play();
        return;
    }

    private static void a(Object p1, long p2)
    {
        ((android.media.session.MediaController$TransportControls) p1).seekTo(p2);
        return;
    }

    private static void a(Object p0, Object p1)
    {
        ((android.media.session.MediaController$TransportControls) p0).setRating(((android.media.Rating) p1));
        return;
    }

    public static void a(Object p0, String p1, android.os.Bundle p2)
    {
        ((android.media.session.MediaController$TransportControls) p0).sendCustomAction(p1, p2);
        return;
    }

    private static void b(Object p0)
    {
        ((android.media.session.MediaController$TransportControls) p0).pause();
        return;
    }

    private static void b(Object p1, long p2)
    {
        ((android.media.session.MediaController$TransportControls) p1).skipToQueueItem(p2);
        return;
    }

    private static void b(Object p0, String p1, android.os.Bundle p2)
    {
        ((android.media.session.MediaController$TransportControls) p0).playFromMediaId(p1, p2);
        return;
    }

    private static void c(Object p0)
    {
        ((android.media.session.MediaController$TransportControls) p0).stop();
        return;
    }

    private static void c(Object p0, String p1, android.os.Bundle p2)
    {
        ((android.media.session.MediaController$TransportControls) p0).playFromSearch(p1, p2);
        return;
    }

    private static void d(Object p0)
    {
        ((android.media.session.MediaController$TransportControls) p0).fastForward();
        return;
    }

    private static void e(Object p0)
    {
        ((android.media.session.MediaController$TransportControls) p0).rewind();
        return;
    }

    private static void f(Object p0)
    {
        ((android.media.session.MediaController$TransportControls) p0).skipToNext();
        return;
    }

    private static void g(Object p0)
    {
        ((android.media.session.MediaController$TransportControls) p0).skipToPrevious();
        return;
    }
}
