package android.support.v4.media.session;
public final class PlaybackStateCompat implements android.os.Parcelable {
    public static final long A = 255;
    public static final android.os.Parcelable$Creator CREATOR = None;
    public static final long a = 1;
    public static final long b = 2;
    public static final long c = 4;
    public static final long d = 8;
    public static final long e = 16;
    public static final long f = 32;
    public static final long g = 64;
    public static final long h = 128;
    public static final long i = 256;
    public static final long j = 512;
    public static final long k = 1024;
    public static final long l = 2048;
    public static final long m = 4096;
    public static final long n = 8192;
    public static final int o = 0;
    public static final int p = 1;
    public static final int q = 2;
    public static final int r = 3;
    public static final int s = 4;
    public static final int t = 5;
    public static final int u = 6;
    public static final int v = 7;
    public static final int w = 8;
    public static final int x = 9;
    public static final int y = 10;
    public static final int z = 11;
    final int B;
    final long C;
    final long D;
    final float E;
    final long F;
    final CharSequence G;
    final long H;
    java.util.List I;
    final long J;
    final android.os.Bundle K;
    Object L;

    static PlaybackStateCompat()
    {
        android.support.v4.media.session.PlaybackStateCompat.CREATOR = new android.support.v4.media.session.bj();
        return;
    }

    private PlaybackStateCompat(int p3, long p4, long p6, float p8, long p9, CharSequence p11, long p12, java.util.List p14, long p15, android.os.Bundle p17)
    {
        this.B = p3;
        this.C = p4;
        this.D = p6;
        this.E = p8;
        this.F = p9;
        this.G = p11;
        this.H = p12;
        this.I = new java.util.ArrayList(p14);
        this.J = p15;
        this.K = p17;
        return;
    }

    synthetic PlaybackStateCompat(int p1, long p2, long p4, float p6, long p7, CharSequence p9, long p10, java.util.List p12, long p13, android.os.Bundle p15, byte p16)
    {
        this(p1, p2, p4, p6, p7, p9, p10, p12, p13, p15);
        return;
    }

    private PlaybackStateCompat(android.os.Parcel p3)
    {
        this.B = p3.readInt();
        this.C = p3.readLong();
        this.E = p3.readFloat();
        this.H = p3.readLong();
        this.D = p3.readLong();
        this.F = p3.readLong();
        this.G = ((CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(p3));
        this.I = p3.createTypedArrayList(android.support.v4.media.session.PlaybackStateCompat$CustomAction.CREATOR);
        this.J = p3.readLong();
        this.K = p3.readBundle();
        return;
    }

    synthetic PlaybackStateCompat(android.os.Parcel p1, byte p2)
    {
        this(p1);
        return;
    }

    private int a()
    {
        return this.B;
    }

    static synthetic int a(android.support.v4.media.session.PlaybackStateCompat p1)
    {
        return p1.B;
    }

    public static android.support.v4.media.session.PlaybackStateCompat a(Object p18)
    {
        if ((p18 != null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            android.support.v4.media.session.PlaybackStateCompat v2_3 = ((android.media.session.PlaybackState) p18).getCustomActions();
            java.util.ArrayList v14_0 = 0;
            if (v2_3 != null) {
                v14_0 = new java.util.ArrayList(v2_3.size());
                android.support.v4.media.session.PlaybackStateCompat v2_4 = v2_3.iterator();
                while (v2_4.hasNext()) {
                    v14_0.add(android.support.v4.media.session.PlaybackStateCompat$CustomAction.a(v2_4.next()));
                }
            }
            int v17;
            if (android.os.Build$VERSION.SDK_INT < 22) {
                v17 = 0;
            } else {
                v17 = ((android.media.session.PlaybackState) p18).getExtras();
            }
            android.support.v4.media.session.PlaybackStateCompat v2_9 = new android.support.v4.media.session.PlaybackStateCompat(((android.media.session.PlaybackState) p18).getState(), ((android.media.session.PlaybackState) p18).getPosition(), ((android.media.session.PlaybackState) p18).getBufferedPosition(), ((android.media.session.PlaybackState) p18).getPlaybackSpeed(), ((android.media.session.PlaybackState) p18).getActions(), ((android.media.session.PlaybackState) p18).getErrorMessage(), ((android.media.session.PlaybackState) p18).getLastPositionUpdateTime(), v14_0, ((android.media.session.PlaybackState) p18).getActiveQueueItemId(), v17);
            v2_9.L = p18;
        } else {
            v2_9 = 0;
        }
        return v2_9;
    }

    private long b()
    {
        return this.C;
    }

    static synthetic long b(android.support.v4.media.session.PlaybackStateCompat p2)
    {
        return p2.C;
    }

    static synthetic float c(android.support.v4.media.session.PlaybackStateCompat p1)
    {
        return p1.E;
    }

    private long c()
    {
        return this.D;
    }

    private float d()
    {
        return this.E;
    }

    static synthetic long d(android.support.v4.media.session.PlaybackStateCompat p2)
    {
        return p2.H;
    }

    private long e()
    {
        return this.F;
    }

    static synthetic long e(android.support.v4.media.session.PlaybackStateCompat p2)
    {
        return p2.D;
    }

    static synthetic long f(android.support.v4.media.session.PlaybackStateCompat p2)
    {
        return p2.F;
    }

    private java.util.List f()
    {
        return this.I;
    }

    private CharSequence g()
    {
        return this.G;
    }

    static synthetic CharSequence g(android.support.v4.media.session.PlaybackStateCompat p1)
    {
        return p1.G;
    }

    private long h()
    {
        return this.H;
    }

    static synthetic java.util.List h(android.support.v4.media.session.PlaybackStateCompat p1)
    {
        return p1.I;
    }

    private long i()
    {
        return this.J;
    }

    static synthetic long i(android.support.v4.media.session.PlaybackStateCompat p2)
    {
        return p2.J;
    }

    private android.os.Bundle j()
    {
        return this.K;
    }

    static synthetic android.os.Bundle j(android.support.v4.media.session.PlaybackStateCompat p1)
    {
        return p1.K;
    }

    private Object k()
    {
        if ((this.L == null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            java.util.ArrayList v14_0 = 0;
            if (this.I != null) {
                v14_0 = new java.util.ArrayList(this.I.size());
                int v3_1 = this.I.iterator();
                while (v3_1.hasNext()) {
                    Object v2_13;
                    Object v2_12 = ((android.support.v4.media.session.PlaybackStateCompat$CustomAction) v3_1.next());
                    if ((v2_12.e == null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
                        android.os.Bundle v7 = v2_12.d;
                        android.media.session.PlaybackState$CustomAction$Builder v8_3 = new android.media.session.PlaybackState$CustomAction$Builder(v2_12.a, v2_12.b, v2_12.c);
                        v8_3.setExtras(v7);
                        v2_12.e = v8_3.build();
                        v2_13 = v2_12.e;
                    } else {
                        v2_13 = v2_12.e;
                    }
                    v14_0.add(v2_13);
                }
            }
            if (android.os.Build$VERSION.SDK_INT < 22) {
                this.L = android.support.v4.media.session.bp.a(this.B, this.C, this.D, this.E, this.F, this.G, this.H, v14_0, this.J);
            } else {
                this.L = android.support.v4.media.session.br.a(this.B, this.C, this.D, this.E, this.F, this.G, this.H, v14_0, this.J, this.K);
            }
            Object v2_10 = this.L;
        } else {
            v2_10 = this.L;
        }
        return v2_10;
    }

    public final int describeContents()
    {
        return 0;
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder("PlaybackState {");
        v0_1.append("state=").append(this.B);
        v0_1.append(", position=").append(this.C);
        v0_1.append(", buffered position=").append(this.D);
        v0_1.append(", speed=").append(this.E);
        v0_1.append(", updated=").append(this.H);
        v0_1.append(", actions=").append(this.F);
        v0_1.append(", error=").append(this.G);
        v0_1.append(", custom actions=").append(this.I);
        v0_1.append(", active item id=").append(this.J);
        v0_1.append("}");
        return v0_1.toString();
    }

    public final void writeToParcel(android.os.Parcel p3, int p4)
    {
        p3.writeInt(this.B);
        p3.writeLong(this.C);
        p3.writeFloat(this.E);
        p3.writeLong(this.H);
        p3.writeLong(this.D);
        p3.writeLong(this.F);
        android.text.TextUtils.writeToParcel(this.G, p3, p4);
        p3.writeTypedList(this.I);
        p3.writeLong(this.J);
        p3.writeBundle(this.K);
        return;
    }
}
