package android.support.v4.media.session;
final class x extends android.media.session.MediaController$Callback {
    protected final android.support.v4.media.session.w a;

    public x(android.support.v4.media.session.w p1)
    {
        this.a = p1;
        return;
    }

    public final void onMetadataChanged(android.media.MediaMetadata p2)
    {
        this.a.b(p2);
        return;
    }

    public final void onPlaybackStateChanged(android.media.session.PlaybackState p2)
    {
        this.a.a(p2);
        return;
    }

    public final void onSessionDestroyed()
    {
        return;
    }

    public final void onSessionEvent(String p1, android.os.Bundle p2)
    {
        return;
    }
}
