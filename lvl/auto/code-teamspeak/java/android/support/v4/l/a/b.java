package android.support.v4.l.a;
final class b {
    public static final String a = "embeddedTts";
    public static final String b = "networkTts";

    b()
    {
        return;
    }

    private static java.util.Set a(android.speech.tts.TextToSpeech p2, java.util.Locale p3)
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 15) {
            v0_1 = 0;
        } else {
            v0_1 = p2.getFeatures(p3);
        }
        return v0_1;
    }

    private static void a(android.speech.tts.TextToSpeech p2, android.support.v4.l.a.e p3)
    {
        if (android.os.Build$VERSION.SDK_INT < 15) {
            p2.setOnUtteranceCompletedListener(new android.support.v4.l.a.d(p3));
        } else {
            p2.setOnUtteranceProgressListener(new android.support.v4.l.a.c(p3));
        }
        return;
    }
}
