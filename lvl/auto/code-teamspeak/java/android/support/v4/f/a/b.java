package android.support.v4.f.a;
final class b extends android.support.v4.f.a.a {
    private final Object b;

    public b(android.content.Context p2)
    {
        this.b = p2.getSystemService("display");
        return;
    }

    public final android.view.Display a(int p2)
    {
        return ((android.hardware.display.DisplayManager) this.b).getDisplay(p2);
    }

    public final android.view.Display[] a()
    {
        return ((android.hardware.display.DisplayManager) this.b).getDisplays();
    }

    public final android.view.Display[] a(String p2)
    {
        return ((android.hardware.display.DisplayManager) this.b).getDisplays(p2);
    }
}
