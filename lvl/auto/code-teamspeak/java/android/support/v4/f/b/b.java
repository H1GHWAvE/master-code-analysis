package android.support.v4.f.b;
final class b implements android.support.v4.f.b.g {

    public b()
    {
        return;
    }

    private static android.support.v4.f.b.f a(android.support.v4.f.b.m p2)
    {
        android.support.v4.f.b.f v0_0 = 0;
        if (p2 != null) {
            if (p2.b == null) {
                if (p2.a == null) {
                    if (p2.c != null) {
                        v0_0 = new android.support.v4.f.b.f(p2.c);
                    }
                } else {
                    v0_0 = new android.support.v4.f.b.f(p2.a);
                }
            } else {
                v0_0 = new android.support.v4.f.b.f(p2.b);
            }
        }
        return v0_0;
    }

    private static android.support.v4.f.b.k a(android.support.v4.f.b.d p1)
    {
        return new android.support.v4.f.b.c(p1);
    }

    private static android.support.v4.f.b.m a(android.support.v4.f.b.f p2)
    {
        android.support.v4.f.b.m v0_0 = 0;
        if (p2 != null) {
            if (p2.b == null) {
                if (p2.a == null) {
                    if (p2.c != null) {
                        v0_0 = new android.support.v4.f.b.m(p2.c);
                    }
                } else {
                    v0_0 = new android.support.v4.f.b.m(p2.a);
                }
            } else {
                v0_0 = new android.support.v4.f.b.m(p2.b);
            }
        }
        return v0_0;
    }

    private static synthetic android.support.v4.f.b.f b(android.support.v4.f.b.m p2)
    {
        android.support.v4.f.b.f v0_3;
        if (p2 == null) {
            v0_3 = 0;
        } else {
            if (p2.b == null) {
                if (p2.a == null) {
                    if (p2.c == null) {
                    } else {
                        v0_3 = new android.support.v4.f.b.f(p2.c);
                    }
                } else {
                    v0_3 = new android.support.v4.f.b.f(p2.a);
                }
            } else {
                v0_3 = new android.support.v4.f.b.f(p2.b);
            }
        }
        return v0_3;
    }

    public final void a(android.content.Context p7, android.support.v4.f.b.f p8, int p9, android.support.v4.i.c p10, android.support.v4.f.b.d p11, android.os.Handler p12)
    {
        javax.crypto.Mac v3_0;
        android.hardware.fingerprint.FingerprintManager$CryptoObject v1_0 = 0;
        if (p8 == null) {
            v3_0 = 0;
        } else {
            if (p8.b == null) {
                if (p8.a == null) {
                    if (p8.c == null) {
                    } else {
                        v3_0 = new android.support.v4.f.b.m(p8.c);
                    }
                } else {
                    v3_0 = new android.support.v4.f.b.m(p8.a);
                }
            } else {
                v3_0 = new android.support.v4.f.b.m(p8.b);
            }
        }
        android.os.CancellationSignal v2_3;
        if (p10 == null) {
            v2_3 = 0;
        } else {
            v2_3 = p10.b();
        }
        android.os.Handler v5_1 = new android.support.v4.f.b.c(p11);
        android.hardware.fingerprint.FingerprintManager v0_9 = android.support.v4.f.b.i.a(p7);
        if (v3_0 != null) {
            if (v3_0.b == null) {
                if (v3_0.a == null) {
                    if (v3_0.c != null) {
                        v1_0 = new android.hardware.fingerprint.FingerprintManager$CryptoObject(v3_0.c);
                    }
                } else {
                    v1_0 = new android.hardware.fingerprint.FingerprintManager$CryptoObject(v3_0.a);
                }
            } else {
                v1_0 = new android.hardware.fingerprint.FingerprintManager$CryptoObject(v3_0.b);
            }
        }
        v0_9.authenticate(v1_0, ((android.os.CancellationSignal) v2_3), p9, new android.support.v4.f.b.j(v5_1), p12);
        return;
    }

    public final boolean a(android.content.Context p2)
    {
        return android.support.v4.f.b.i.a(p2).hasEnrolledFingerprints();
    }

    public final boolean b(android.content.Context p2)
    {
        return android.support.v4.f.b.i.a(p2).isHardwareDetected();
    }
}
