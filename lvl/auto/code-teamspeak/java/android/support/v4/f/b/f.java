package android.support.v4.f.b;
public final class f {
    final java.security.Signature a;
    final javax.crypto.Cipher b;
    final javax.crypto.Mac c;

    public f(java.security.Signature p2)
    {
        this.a = p2;
        this.b = 0;
        this.c = 0;
        return;
    }

    public f(javax.crypto.Cipher p2)
    {
        this.b = p2;
        this.a = 0;
        this.c = 0;
        return;
    }

    public f(javax.crypto.Mac p2)
    {
        this.c = p2;
        this.b = 0;
        this.a = 0;
        return;
    }

    private java.security.Signature a()
    {
        return this.a;
    }

    private javax.crypto.Cipher b()
    {
        return this.b;
    }

    private javax.crypto.Mac c()
    {
        return this.c;
    }
}
