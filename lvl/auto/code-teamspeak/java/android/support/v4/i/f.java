package android.support.v4.i;
public final class f {
    public static final String a = "unknown";
    private static final String b = "EnvironmentCompat";

    public f()
    {
        return;
    }

    private static String a(java.io.File p4)
    {
        String v0_6;
        if (android.os.Build$VERSION.SDK_INT < 19) {
            try {
                if (p4.getCanonicalPath().startsWith(android.os.Environment.getExternalStorageDirectory().getCanonicalPath())) {
                    v0_6 = android.os.Environment.getExternalStorageState();
                    return v0_6;
                }
            } catch (String v0_3) {
                android.util.Log.w("EnvironmentCompat", new StringBuilder("Failed to resolve canonical path: ").append(v0_3).toString());
            }
            v0_6 = "unknown";
        } else {
            v0_6 = android.os.Environment.getStorageState(p4);
        }
        return v0_6;
    }
}
