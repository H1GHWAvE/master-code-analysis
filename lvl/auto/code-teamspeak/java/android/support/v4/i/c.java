package android.support.v4.i;
public final class c {
    public boolean a;
    public Object b;
    public boolean c;
    private android.support.v4.i.d d;

    public c()
    {
        return;
    }

    private void a(android.support.v4.i.d p2)
    {
        try {
            while (this.c) {
                try {
                    this.wait();
                } catch (boolean v0) {
                }
            }
        } catch (boolean v0_3) {
            throw v0_3;
        }
        if (this.d != p2) {
            this.d = p2;
            if ((this.a) && (p2 != null)) {
            } else {
            }
        } else {
        }
        return;
    }

    private void c()
    {
        if (!this.a()) {
            return;
        } else {
            throw new android.support.v4.i.h();
        }
    }

    private void d()
    {
        if (!this.a) {
            this.a = 1;
            this.c = 1;
            Throwable v0_3 = this.b;
            if (v0_3 != null) {
                try {
                    ((android.os.CancellationSignal) v0_3).cancel();
                } catch (Throwable v0_5) {
                    this.c = 0;
                    this.notifyAll();
                    throw v0_5;
                }
            }
            try {
                this.c = 0;
                this.notifyAll();
            } catch (Throwable v0_8) {
                throw v0_8;
            }
        } else {
        }
        return;
    }

    private void e()
    {
        while (this.c) {
            try {
                this.wait();
            } catch (InterruptedException v0) {
            }
        }
        return;
    }

    public final boolean a()
    {
        try {
            return this.a;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final Object b()
    {
        android.os.CancellationSignal v0_7;
        if (android.os.Build$VERSION.SDK_INT >= 16) {
            try {
                if (this.b == null) {
                    this.b = new android.os.CancellationSignal();
                    if (this.a) {
                        ((android.os.CancellationSignal) this.b).cancel();
                    }
                }
            } catch (android.os.CancellationSignal v0_8) {
                throw v0_8;
            }
            v0_7 = this.b;
        } else {
            v0_7 = 0;
        }
        return v0_7;
    }
}
