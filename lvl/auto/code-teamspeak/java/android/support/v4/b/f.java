package android.support.v4.b;
final class f implements android.support.v4.b.l {
    java.util.List a;
    java.util.List b;
    android.view.View c;
    long d;
    long e;
    float f;
    Runnable g;
    private boolean h;
    private boolean i;

    public f()
    {
        this.a = new java.util.ArrayList();
        this.b = new java.util.ArrayList();
        this.e = 200;
        this.f = 0;
        this.h = 0;
        this.i = 0;
        this.g = new android.support.v4.b.g(this);
        return;
    }

    private static synthetic float a(android.support.v4.b.f p0, float p1)
    {
        p0.f = p1;
        return p1;
    }

    private static synthetic long a(android.support.v4.b.f p2)
    {
        return p2.c.getDrawingTime();
    }

    private static synthetic long b(android.support.v4.b.f p2)
    {
        return p2.d;
    }

    private static synthetic long c(android.support.v4.b.f p2)
    {
        return p2.e;
    }

    private static synthetic void d(android.support.v4.b.f p2)
    {
        int v0_2 = (p2.b.size() - 1);
        while (v0_2 >= 0) {
            p2.b.get(v0_2);
            v0_2--;
        }
        return;
    }

    private static synthetic float e(android.support.v4.b.f p1)
    {
        return p1.f;
    }

    private void e()
    {
        int v0_2 = (this.b.size() - 1);
        while (v0_2 >= 0) {
            this.b.get(v0_2);
            v0_2--;
        }
        return;
    }

    private long f()
    {
        return this.c.getDrawingTime();
    }

    private static synthetic void f(android.support.v4.b.f p0)
    {
        p0.b();
        return;
    }

    private static synthetic Runnable g(android.support.v4.b.f p1)
    {
        return p1.g;
    }

    private void g()
    {
        int v0_2 = (this.a.size() - 1);
        while (v0_2 >= 0) {
            this.a.get(v0_2);
            v0_2--;
        }
        return;
    }

    private void h()
    {
        int v0_2 = (this.a.size() - 1);
        while (v0_2 >= 0) {
            this.a.get(v0_2);
            v0_2--;
        }
        return;
    }

    public final void a()
    {
        if (!this.h) {
            this.h = 1;
            android.view.View v0_4 = (this.a.size() - 1);
            while (v0_4 >= null) {
                this.a.get(v0_4);
                v0_4--;
            }
            this.f = 0;
            this.d = this.c.getDrawingTime();
            this.c.postDelayed(this.g, 16);
        }
        return;
    }

    public final void a(long p2)
    {
        if (!this.h) {
            this.e = p2;
        }
        return;
    }

    public final void a(android.support.v4.b.b p2)
    {
        this.a.add(p2);
        return;
    }

    public final void a(android.support.v4.b.d p2)
    {
        this.b.add(p2);
        return;
    }

    public final void a(android.view.View p1)
    {
        this.c = p1;
        return;
    }

    final void b()
    {
        int v0_2 = (this.a.size() - 1);
        while (v0_2 >= 0) {
            this.a.get(v0_2);
            v0_2--;
        }
        return;
    }

    public final void c()
    {
        if (!this.i) {
            this.i = 1;
            if (this.h) {
                int v0_5 = (this.a.size() - 1);
                while (v0_5 >= 0) {
                    this.a.get(v0_5);
                    v0_5--;
                }
            }
            this.b();
        }
        return;
    }

    public final float d()
    {
        return this.f;
    }
}
