package android.support.v4.view.a;
final class i extends android.support.v4.view.a.k {

    i()
    {
        return;
    }

    public final Object a(android.support.v4.view.a.m p3)
    {
        return new android.support.v4.view.a.o(new android.support.v4.view.a.j(this, p3));
    }

    public final java.util.List a(android.view.accessibility.AccessibilityManager p2)
    {
        return p2.getInstalledAccessibilityServiceList();
    }

    public final java.util.List a(android.view.accessibility.AccessibilityManager p2, int p3)
    {
        return p2.getEnabledAccessibilityServiceList(p3);
    }

    public final boolean a(android.view.accessibility.AccessibilityManager p2, android.support.v4.view.a.m p3)
    {
        return p2.addAccessibilityStateChangeListener(((android.view.accessibility.AccessibilityManager$AccessibilityStateChangeListener) p3.a));
    }

    public final boolean b(android.view.accessibility.AccessibilityManager p2)
    {
        return p2.isTouchExplorationEnabled();
    }

    public final boolean b(android.view.accessibility.AccessibilityManager p2, android.support.v4.view.a.m p3)
    {
        return p2.removeAccessibilityStateChangeListener(((android.view.accessibility.AccessibilityManager$AccessibilityStateChangeListener) p3.a));
    }
}
