package android.support.v4.view;
final class fv {

    fv()
    {
        return;
    }

    private static long a(android.view.View p2)
    {
        return p2.animate().getDuration();
    }

    private static void a(android.view.View p1, float p2)
    {
        p1.animate().alpha(p2);
        return;
    }

    private static void a(android.view.View p1, long p2)
    {
        p1.animate().setDuration(p2);
        return;
    }

    public static void a(android.view.View p2, android.support.v4.view.gd p3)
    {
        p2.animate().setListener(new android.support.v4.view.fw(p3, p2));
        return;
    }

    private static void a(android.view.View p1, android.view.animation.Interpolator p2)
    {
        p1.animate().setInterpolator(p2);
        return;
    }

    private static long b(android.view.View p2)
    {
        return p2.animate().getStartDelay();
    }

    private static void b(android.view.View p1, float p2)
    {
        p1.animate().translationX(p2);
        return;
    }

    private static void b(android.view.View p1, long p2)
    {
        p1.animate().setStartDelay(p2);
        return;
    }

    private static void c(android.view.View p1)
    {
        p1.animate().cancel();
        return;
    }

    private static void c(android.view.View p1, float p2)
    {
        p1.animate().translationY(p2);
        return;
    }

    private static void d(android.view.View p1)
    {
        p1.animate().start();
        return;
    }

    private static void d(android.view.View p1, float p2)
    {
        p1.animate().alphaBy(p2);
        return;
    }

    private static void e(android.view.View p1, float p2)
    {
        p1.animate().rotation(p2);
        return;
    }

    private static void f(android.view.View p1, float p2)
    {
        p1.animate().rotationBy(p2);
        return;
    }

    private static void g(android.view.View p1, float p2)
    {
        p1.animate().rotationX(p2);
        return;
    }

    private static void h(android.view.View p1, float p2)
    {
        p1.animate().rotationXBy(p2);
        return;
    }

    private static void i(android.view.View p1, float p2)
    {
        p1.animate().rotationY(p2);
        return;
    }

    private static void j(android.view.View p1, float p2)
    {
        p1.animate().rotationYBy(p2);
        return;
    }

    private static void k(android.view.View p1, float p2)
    {
        p1.animate().scaleX(p2);
        return;
    }

    private static void l(android.view.View p1, float p2)
    {
        p1.animate().scaleXBy(p2);
        return;
    }

    private static void m(android.view.View p1, float p2)
    {
        p1.animate().scaleY(p2);
        return;
    }

    private static void n(android.view.View p1, float p2)
    {
        p1.animate().scaleYBy(p2);
        return;
    }

    private static void o(android.view.View p1, float p2)
    {
        p1.animate().x(p2);
        return;
    }

    private static void p(android.view.View p1, float p2)
    {
        p1.animate().xBy(p2);
        return;
    }

    private static void q(android.view.View p1, float p2)
    {
        p1.animate().y(p2);
        return;
    }

    private static void r(android.view.View p1, float p2)
    {
        p1.animate().yBy(p2);
        return;
    }

    private static void s(android.view.View p1, float p2)
    {
        p1.animate().translationXBy(p2);
        return;
    }

    private static void t(android.view.View p1, float p2)
    {
        p1.animate().translationYBy(p2);
        return;
    }
}
