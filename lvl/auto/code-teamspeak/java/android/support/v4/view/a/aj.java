package android.support.v4.view.a;
final class aj {

    aj()
    {
        return;
    }

    private static Object a(android.view.View p1, int p2)
    {
        return android.view.accessibility.AccessibilityNodeInfo.obtain(p1, p2);
    }

    private static void a(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setMovementGranularities(p1);
        return;
    }

    private static void a(Object p0, android.view.View p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).addChild(p1, p2);
        return;
    }

    private static void a(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setVisibleToUser(p1);
        return;
    }

    private static boolean a(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isVisibleToUser();
    }

    private static boolean a(Object p1, int p2, android.os.Bundle p3)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).performAction(p2, p3);
    }

    private static int b(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getMovementGranularities();
    }

    private static Object b(Object p1, int p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).findFocus(p2);
    }

    private static void b(Object p0, android.view.View p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setSource(p1, p2);
        return;
    }

    private static void b(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setAccessibilityFocused(p1);
        return;
    }

    private static Object c(Object p1, int p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).focusSearch(p2);
    }

    private static void c(Object p0, android.view.View p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setParent(p1, p2);
        return;
    }

    private static boolean c(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isAccessibilityFocused();
    }
}
