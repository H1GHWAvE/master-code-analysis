package android.support.v4.view;
final class av implements android.support.v4.view.au {

    av()
    {
        return;
    }

    public final int a(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return p2.leftMargin;
    }

    public final void a(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        p1.leftMargin = p2;
        return;
    }

    public final int b(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return p2.rightMargin;
    }

    public final void b(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        p1.rightMargin = p2;
        return;
    }

    public final void c(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        return;
    }

    public final boolean c(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return 0;
    }

    public final int d(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return 0;
    }

    public final void d(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        return;
    }
}
