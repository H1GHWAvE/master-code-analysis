package android.support.v4.view;
public final class ck {
    static final android.support.v4.view.co a;

    static ck()
    {
        if (android.os.Build$VERSION.SDK_INT < 19) {
            android.support.v4.view.ck.a = new android.support.v4.view.cm(0);
        } else {
            android.support.v4.view.ck.a = new android.support.v4.view.cn(0);
        }
        return;
    }

    private ck()
    {
        return;
    }

    private static void a(Object p1, boolean p2)
    {
        android.support.v4.view.ck.a.a(p1, p2);
        return;
    }

    private static boolean a(Object p1)
    {
        return android.support.v4.view.ck.a.a(p1);
    }
}
