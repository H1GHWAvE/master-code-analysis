package android.support.v4.view.a;
public final class bm {
    public static final int a = 1;
    public static final int b = 2;
    public static final int c = 3;
    public static final int d = 4;
    private static final android.support.v4.view.a.bp e = None;
    private static final int g = 255;
    private Object f;

    static bm()
    {
        if (android.os.Build$VERSION.SDK_INT < 21) {
            android.support.v4.view.a.bm.e = new android.support.v4.view.a.bq(0);
        } else {
            android.support.v4.view.a.bm.e = new android.support.v4.view.a.bo(0);
        }
        return;
    }

    private bm(Object p1)
    {
        this.f = p1;
        return;
    }

    private int a()
    {
        return android.support.v4.view.a.bm.e.b(this.f);
    }

    private android.support.v4.view.a.bm a(int p3)
    {
        return android.support.v4.view.a.bm.a(android.support.v4.view.a.bm.e.a(this.f, p3));
    }

    private static android.support.v4.view.a.bm a(android.support.v4.view.a.bm p2)
    {
        return android.support.v4.view.a.bm.a(android.support.v4.view.a.bm.e.a(p2.f));
    }

    static android.support.v4.view.a.bm a(Object p1)
    {
        int v0_0;
        if (p1 == null) {
            v0_0 = 0;
        } else {
            v0_0 = new android.support.v4.view.a.bm(p1);
        }
        return v0_0;
    }

    private void a(android.graphics.Rect p3)
    {
        android.support.v4.view.a.bm.e.a(this.f, p3);
        return;
    }

    private int b()
    {
        return android.support.v4.view.a.bm.e.c(this.f);
    }

    private static String b(int p1)
    {
        String v0;
        switch (p1) {
            case 1:
                v0 = "TYPE_APPLICATION";
                break;
            case 2:
                v0 = "TYPE_INPUT_METHOD";
                break;
            case 3:
                v0 = "TYPE_SYSTEM";
                break;
            case 4:
                v0 = "TYPE_ACCESSIBILITY_OVERLAY";
                break;
            default:
                v0 = "<UNKNOWN>";
        }
        return v0;
    }

    private android.support.v4.view.a.q c()
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.bm.e.d(this.f));
    }

    private android.support.v4.view.a.bm d()
    {
        return android.support.v4.view.a.bm.a(android.support.v4.view.a.bm.e.e(this.f));
    }

    private int e()
    {
        return android.support.v4.view.a.bm.e.f(this.f);
    }

    private boolean f()
    {
        return android.support.v4.view.a.bm.e.g(this.f);
    }

    private boolean g()
    {
        return android.support.v4.view.a.bm.e.h(this.f);
    }

    private boolean h()
    {
        return android.support.v4.view.a.bm.e.i(this.f);
    }

    private int i()
    {
        return android.support.v4.view.a.bm.e.j(this.f);
    }

    private static android.support.v4.view.a.bm j()
    {
        return android.support.v4.view.a.bm.a(android.support.v4.view.a.bm.e.a());
    }

    private void k()
    {
        android.support.v4.view.a.bm.e.k(this.f);
        return;
    }

    public final boolean equals(Object p5)
    {
        int v0 = 1;
        if (this != p5) {
            if (p5 != null) {
                if (this.getClass() == p5.getClass()) {
                    if (this.f != null) {
                        if (!this.f.equals(((android.support.v4.view.a.bm) p5).f)) {
                            v0 = 0;
                        }
                    } else {
                        if (((android.support.v4.view.a.bm) p5).f != null) {
                            v0 = 0;
                        }
                    }
                } else {
                    v0 = 0;
                }
            } else {
                v0 = 0;
            }
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_2;
        if (this.f != null) {
            v0_2 = this.f.hashCode();
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final String toString()
    {
        String v0_7;
        int v1 = 1;
        StringBuilder v3_1 = new StringBuilder();
        int v4_1 = new android.graphics.Rect();
        android.support.v4.view.a.bm.e.a(this.f, v4_1);
        v3_1.append("AccessibilityWindowInfo[");
        v3_1.append("id=").append(android.support.v4.view.a.bm.e.f(this.f));
        Object v5_3 = v3_1.append(", type=");
        switch (android.support.v4.view.a.bm.e.b(this.f)) {
            case 1:
                v0_7 = "TYPE_APPLICATION";
                break;
            case 2:
                v0_7 = "TYPE_INPUT_METHOD";
                break;
            case 3:
                v0_7 = "TYPE_SYSTEM";
                break;
            case 4:
                v0_7 = "TYPE_ACCESSIBILITY_OVERLAY";
                break;
            default:
                v0_7 = "<UNKNOWN>";
        }
        String v0_20;
        v5_3.append(v0_7);
        v3_1.append(", layer=").append(android.support.v4.view.a.bm.e.c(this.f));
        v3_1.append(", bounds=").append(v4_1);
        v3_1.append(", focused=").append(android.support.v4.view.a.bm.e.h(this.f));
        v3_1.append(", active=").append(android.support.v4.view.a.bm.e.g(this.f));
        int v4_6 = v3_1.append(", hasParent=");
        if (android.support.v4.view.a.bm.a(android.support.v4.view.a.bm.e.e(this.f)) == null) {
            v0_20 = 0;
        } else {
            v0_20 = 1;
        }
        v4_6.append(v0_20);
        String v0_22 = v3_1.append(", hasChildren=");
        if (android.support.v4.view.a.bm.e.j(this.f) <= 0) {
            v1 = 0;
        }
        v0_22.append(v1);
        v3_1.append(93);
        return v3_1.toString();
    }
}
