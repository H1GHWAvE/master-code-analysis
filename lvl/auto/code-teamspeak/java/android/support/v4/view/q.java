package android.support.v4.view;
public final class q {
    private final android.support.v4.view.r a;

    private q(android.content.Context p2, android.view.GestureDetector$OnGestureListener p3)
    {
        this(p2, p3, 0);
        return;
    }

    private q(android.content.Context p3, android.view.GestureDetector$OnGestureListener p4, byte p5)
    {
        if (android.os.Build$VERSION.SDK_INT <= 17) {
            this.a = new android.support.v4.view.s(p3, p4);
        } else {
            this.a = new android.support.v4.view.u(p3, p4);
        }
        return;
    }

    private void a(android.view.GestureDetector$OnDoubleTapListener p2)
    {
        this.a.a(p2);
        return;
    }

    private void a(boolean p2)
    {
        this.a.a(p2);
        return;
    }

    private boolean a()
    {
        return this.a.a();
    }

    private boolean a(android.view.MotionEvent p2)
    {
        return this.a.a(p2);
    }
}
