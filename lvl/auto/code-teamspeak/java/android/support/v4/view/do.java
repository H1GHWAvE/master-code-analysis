package android.support.v4.view;
final class do {

    do()
    {
        return;
    }

    private static void a(android.view.View p0, int p1)
    {
        p0.setImportantForAccessibility(p1);
        return;
    }

    private static void a(android.view.View p0, int p1, int p2, int p3, int p4)
    {
        p0.postInvalidate(p1, p2, p3, p4);
        return;
    }

    private static void a(android.view.View p0, Runnable p1)
    {
        p0.postOnAnimation(p1);
        return;
    }

    private static void a(android.view.View p0, Runnable p1, long p2)
    {
        p0.postOnAnimationDelayed(p1, p2);
        return;
    }

    private static void a(android.view.View p0, boolean p1)
    {
        p0.setHasTransientState(p1);
        return;
    }

    private static boolean a(android.view.View p1)
    {
        return p1.hasTransientState();
    }

    private static boolean a(android.view.View p1, int p2, android.os.Bundle p3)
    {
        return p1.performAccessibilityAction(p2, p3);
    }

    private static void b(android.view.View p0)
    {
        p0.postInvalidateOnAnimation();
        return;
    }

    private static int c(android.view.View p1)
    {
        return p1.getImportantForAccessibility();
    }

    private static Object d(android.view.View p1)
    {
        return p1.getAccessibilityNodeProvider();
    }

    private static android.view.ViewParent e(android.view.View p1)
    {
        return p1.getParentForAccessibility();
    }

    private static int f(android.view.View p1)
    {
        return p1.getMinimumWidth();
    }

    private static int g(android.view.View p1)
    {
        return p1.getMinimumHeight();
    }

    private static void h(android.view.View p0)
    {
        p0.requestFitSystemWindows();
        return;
    }

    private static boolean i(android.view.View p1)
    {
        return p1.getFitsSystemWindows();
    }

    private static boolean j(android.view.View p1)
    {
        return p1.hasOverlappingRendering();
    }
}
