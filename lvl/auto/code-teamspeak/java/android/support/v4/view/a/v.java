package android.support.v4.view.a;
 class v extends android.support.v4.view.a.ab {

    v()
    {
        return;
    }

    public final boolean A(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isScrollable();
    }

    public final boolean B(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isSelected();
    }

    public final void C(Object p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).recycle();
        return;
    }

    public final Object a()
    {
        return android.view.accessibility.AccessibilityNodeInfo.obtain();
    }

    public final Object a(android.view.View p2)
    {
        return android.view.accessibility.AccessibilityNodeInfo.obtain(p2);
    }

    public final java.util.List a(Object p2, String p3)
    {
        return ((java.util.List) ((android.view.accessibility.AccessibilityNodeInfo) p2).findAccessibilityNodeInfosByText(p3));
    }

    public final void a(Object p1, android.graphics.Rect p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).getBoundsInParent(p2);
        return;
    }

    public final void a(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setCheckable(p2);
        return;
    }

    public final void b(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).addAction(p2);
        return;
    }

    public final void b(Object p1, android.graphics.Rect p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).getBoundsInScreen(p2);
        return;
    }

    public final void b(Object p1, CharSequence p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setClassName(p2);
        return;
    }

    public final void b(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setChecked(p2);
        return;
    }

    public final Object c(Object p2, int p3)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getChild(p3);
    }

    public final void c(Object p1, android.graphics.Rect p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setBoundsInParent(p2);
        return;
    }

    public final void c(Object p1, CharSequence p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setContentDescription(p2);
        return;
    }

    public final void c(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setClickable(p2);
        return;
    }

    public final void d(Object p1, android.graphics.Rect p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setBoundsInScreen(p2);
        return;
    }

    public final void d(Object p1, android.view.View p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).addChild(p2);
        return;
    }

    public final void d(Object p1, CharSequence p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setPackageName(p2);
        return;
    }

    public final void d(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setEnabled(p2);
        return;
    }

    public final boolean d(Object p2, int p3)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).performAction(p3);
    }

    public final void e(Object p1, android.view.View p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setParent(p2);
        return;
    }

    public final void e(Object p1, CharSequence p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setText(p2);
        return;
    }

    public final void e(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setFocusable(p2);
        return;
    }

    public final void f(Object p1, android.view.View p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setSource(p2);
        return;
    }

    public final void f(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setFocused(p2);
        return;
    }

    public final void g(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setLongClickable(p2);
        return;
    }

    public final void h(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setPassword(p2);
        return;
    }

    public final void i(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setScrollable(p2);
        return;
    }

    public final Object j(Object p2)
    {
        return android.view.accessibility.AccessibilityNodeInfo.obtain(((android.view.accessibility.AccessibilityNodeInfo) p2));
    }

    public final void j(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setSelected(p2);
        return;
    }

    public final int k(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getActions();
    }

    public final int l(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getChildCount();
    }

    public final CharSequence m(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getClassName();
    }

    public final CharSequence n(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getContentDescription();
    }

    public final CharSequence o(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getPackageName();
    }

    public final Object p(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getParent();
    }

    public final CharSequence q(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getText();
    }

    public final int r(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getWindowId();
    }

    public final boolean s(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isCheckable();
    }

    public final boolean t(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isChecked();
    }

    public final boolean u(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isClickable();
    }

    public final boolean v(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isEnabled();
    }

    public final boolean w(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isFocusable();
    }

    public final boolean x(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isFocused();
    }

    public final boolean y(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isLongClickable();
    }

    public final boolean z(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isPassword();
    }
}
