package android.support.v4.view.a;
 class x extends android.support.v4.view.a.v {

    x()
    {
        return;
    }

    public final int D(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getMovementGranularities();
    }

    public final boolean E(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isVisibleToUser();
    }

    public final boolean F(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isAccessibilityFocused();
    }

    public final Object a(android.view.View p2, int p3)
    {
        return android.view.accessibility.AccessibilityNodeInfo.obtain(p2, p3);
    }

    public final boolean a(Object p2, int p3, android.os.Bundle p4)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).performAction(p3, p4);
    }

    public final void d(Object p1, android.view.View p2, int p3)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setSource(p2, p3);
        return;
    }

    public final Object e(Object p2, int p3)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).findFocus(p3);
    }

    public final void e(Object p1, android.view.View p2, int p3)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).addChild(p2, p3);
        return;
    }

    public final Object f(Object p2, int p3)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).focusSearch(p3);
    }

    public final void f(Object p1, android.view.View p2, int p3)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setParent(p2, p3);
        return;
    }

    public final void g(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setMovementGranularities(p2);
        return;
    }

    public final void k(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setVisibleToUser(p2);
        return;
    }

    public final void l(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setAccessibilityFocused(p2);
        return;
    }
}
