package android.support.v4.view;
final class fj {
    private static final String a = "ViewParentCompat";

    fj()
    {
        return;
    }

    private static void a(android.view.ViewParent p4, android.view.View p5)
    {
        try {
            p4.onStopNestedScroll(p5);
        } catch (AbstractMethodError v0) {
            android.util.Log.e("ViewParentCompat", new StringBuilder("ViewParent ").append(p4).append(" does not implement interface method onStopNestedScroll").toString(), v0);
        }
        return;
    }

    private static void a(android.view.ViewParent p4, android.view.View p5, int p6, int p7, int p8, int p9)
    {
        try {
            p4.onNestedScroll(p5, p6, p7, p8, p9);
        } catch (AbstractMethodError v0) {
            android.util.Log.e("ViewParentCompat", new StringBuilder("ViewParent ").append(p4).append(" does not implement interface method onNestedScroll").toString(), v0);
        }
        return;
    }

    private static void a(android.view.ViewParent p4, android.view.View p5, int p6, int p7, int[] p8)
    {
        try {
            p4.onNestedPreScroll(p5, p6, p7, p8);
        } catch (AbstractMethodError v0) {
            android.util.Log.e("ViewParentCompat", new StringBuilder("ViewParent ").append(p4).append(" does not implement interface method onNestedPreScroll").toString(), v0);
        }
        return;
    }

    public static boolean a(android.view.ViewParent p4, android.view.View p5, float p6, float p7)
    {
        try {
            int v0_0 = p4.onNestedPreFling(p5, p6, p7);
        } catch (int v0_1) {
            android.util.Log.e("ViewParentCompat", new StringBuilder("ViewParent ").append(p4).append(" does not implement interface method onNestedPreFling").toString(), v0_1);
            v0_0 = 0;
        }
        return v0_0;
    }

    public static boolean a(android.view.ViewParent p4, android.view.View p5, float p6, float p7, boolean p8)
    {
        try {
            int v0_0 = p4.onNestedFling(p5, p6, p7, p8);
        } catch (int v0_1) {
            android.util.Log.e("ViewParentCompat", new StringBuilder("ViewParent ").append(p4).append(" does not implement interface method onNestedFling").toString(), v0_1);
            v0_0 = 0;
        }
        return v0_0;
    }

    public static boolean a(android.view.ViewParent p4, android.view.View p5, android.view.View p6, int p7)
    {
        try {
            int v0_0 = p4.onStartNestedScroll(p5, p6, p7);
        } catch (int v0_1) {
            android.util.Log.e("ViewParentCompat", new StringBuilder("ViewParent ").append(p4).append(" does not implement interface method onStartNestedScroll").toString(), v0_1);
            v0_0 = 0;
        }
        return v0_0;
    }

    private static void b(android.view.ViewParent p4, android.view.View p5, android.view.View p6, int p7)
    {
        try {
            p4.onNestedScrollAccepted(p5, p6, p7);
        } catch (AbstractMethodError v0) {
            android.util.Log.e("ViewParentCompat", new StringBuilder("ViewParent ").append(p4).append(" does not implement interface method onNestedScrollAccepted").toString(), v0);
        }
        return;
    }
}
