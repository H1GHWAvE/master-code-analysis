package android.support.v4.view;
public interface bt {

    public abstract boolean dispatchNestedFling();

    public abstract boolean dispatchNestedPreFling();

    public abstract boolean dispatchNestedPreScroll();

    public abstract boolean dispatchNestedScroll();

    public abstract boolean hasNestedScrollingParent();

    public abstract boolean isNestedScrollingEnabled();

    public abstract void setNestedScrollingEnabled();

    public abstract boolean startNestedScroll();

    public abstract void stopNestedScroll();
}
