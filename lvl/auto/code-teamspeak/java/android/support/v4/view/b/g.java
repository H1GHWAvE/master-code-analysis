package android.support.v4.view.b;
final class g {

    private g()
    {
        return;
    }

    private static android.view.animation.Interpolator a(float p1, float p2)
    {
        return new android.support.v4.view.b.h(p1, p2);
    }

    private static android.view.animation.Interpolator a(float p1, float p2, float p3, float p4)
    {
        return new android.support.v4.view.b.h(p1, p2, p3, p4);
    }

    private static android.view.animation.Interpolator a(android.graphics.Path p1)
    {
        return new android.support.v4.view.b.h(p1);
    }
}
