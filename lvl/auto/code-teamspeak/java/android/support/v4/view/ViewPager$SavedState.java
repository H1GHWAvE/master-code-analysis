package android.support.v4.view;
public class ViewPager$SavedState extends android.view.View$BaseSavedState {
    public static final android.os.Parcelable$Creator CREATOR;
    int a;
    android.os.Parcelable b;
    ClassLoader c;

    static ViewPager$SavedState()
    {
        android.support.v4.i.j v0_2;
        android.support.v4.view.ey v1_1 = new android.support.v4.view.ey();
        if (android.os.Build$VERSION.SDK_INT < 13) {
            v0_2 = new android.support.v4.i.j(v1_1);
        } else {
            v0_2 = new android.support.v4.i.l(v1_1);
        }
        android.support.v4.view.ViewPager$SavedState.CREATOR = v0_2;
        return;
    }

    ViewPager$SavedState(android.os.Parcel p2, ClassLoader p3)
    {
        this(p2);
        if (p3 == null) {
            p3 = this.getClass().getClassLoader();
        }
        this.a = p2.readInt();
        this.b = p2.readParcelable(p3);
        this.c = p3;
        return;
    }

    public ViewPager$SavedState(android.os.Parcelable p1)
    {
        this(p1);
        return;
    }

    public String toString()
    {
        return new StringBuilder("FragmentPager.SavedState{").append(Integer.toHexString(System.identityHashCode(this))).append(" position=").append(this.a).append("}").toString();
    }

    public void writeToParcel(android.os.Parcel p2, int p3)
    {
        super.writeToParcel(p2, p3);
        p2.writeInt(this.a);
        p2.writeParcelable(this.b, p3);
        return;
    }
}
