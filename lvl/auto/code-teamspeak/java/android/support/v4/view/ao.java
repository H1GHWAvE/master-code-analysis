package android.support.v4.view;
 class ao implements android.view.LayoutInflater$Factory {
    final android.support.v4.view.as a;

    ao(android.support.v4.view.as p1)
    {
        this.a = p1;
        return;
    }

    public android.view.View onCreateView(String p3, android.content.Context p4, android.util.AttributeSet p5)
    {
        return this.a.a(0, p3, p4, p5);
    }

    public String toString()
    {
        return new StringBuilder().append(this.getClass().getName()).append("{").append(this.a).append("}").toString();
    }
}
