package android.support.v4.view;
final class cn implements android.support.v4.view.co {

    private cn()
    {
        return;
    }

    synthetic cn(byte p1)
    {
        return;
    }

    public final void a(Object p1, boolean p2)
    {
        ((android.view.ScaleGestureDetector) p1).setQuickScaleEnabled(p2);
        return;
    }

    public final boolean a(Object p2)
    {
        return ((android.view.ScaleGestureDetector) p2).isQuickScaleEnabled();
    }
}
