package android.support.v4.view;
 class cz extends android.support.v4.view.cy {

    cz()
    {
        return;
    }

    public final void a(android.view.ViewGroup p7)
    {
        if (android.support.v4.view.dk.b == null) {
            try {
                String v2_1 = new Class[1];
                v2_1[0] = Boolean.TYPE;
                android.support.v4.view.dk.b = android.view.ViewGroup.getDeclaredMethod("setChildrenDrawingOrderEnabled", v2_1);
            } catch (reflect.InvocationTargetException v0_3) {
                android.util.Log.e("ViewCompat", "Unable to find childrenDrawingOrderEnabled", v0_3);
            }
            android.support.v4.view.dk.b.setAccessible(1);
        }
        try {
            String v1_3 = new Object[1];
            v1_3[0] = Boolean.valueOf(1);
            android.support.v4.view.dk.b.invoke(p7, v1_3);
        } catch (reflect.InvocationTargetException v0_6) {
            android.util.Log.e("ViewCompat", "Unable to invoke childrenDrawingOrderEnabled", v0_6);
        } catch (reflect.InvocationTargetException v0_8) {
            android.util.Log.e("ViewCompat", "Unable to invoke childrenDrawingOrderEnabled", v0_8);
        } catch (reflect.InvocationTargetException v0_7) {
            android.util.Log.e("ViewCompat", "Unable to invoke childrenDrawingOrderEnabled", v0_7);
        }
        return;
    }

    public final boolean m(android.view.View p2)
    {
        return p2.isOpaque();
    }
}
