package android.support.v4.view.a;
 class y extends android.support.v4.view.a.x {

    y()
    {
        return;
    }

    public final Object V(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getLabelFor();
    }

    public final Object W(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getLabeledBy();
    }

    public final void g(Object p1, android.view.View p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setLabelFor(p2);
        return;
    }

    public final void g(Object p1, android.view.View p2, int p3)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setLabelFor(p2, p3);
        return;
    }

    public final void h(Object p1, android.view.View p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setLabeledBy(p2);
        return;
    }

    public final void h(Object p1, android.view.View p2, int p3)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setLabeledBy(p2, p3);
        return;
    }
}
