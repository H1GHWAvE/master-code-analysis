package android.support.v4.view.a;
 class t extends android.support.v4.view.a.aa {

    t()
    {
        return;
    }

    public final Object a(int p3)
    {
        return new android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction(p3, 0);
    }

    public final Object a(int p2, int p3, int p4, int p5, boolean p6, boolean p7)
    {
        return android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo.obtain(p2, p3, p4, p5, p6, p7);
    }

    public final Object a(int p2, int p3, boolean p4, int p5)
    {
        return android.view.accessibility.AccessibilityNodeInfo$CollectionInfo.obtain(p2, p3, p4, p5);
    }

    public final java.util.List a(Object p2)
    {
        return ((java.util.List) ((android.view.accessibility.AccessibilityNodeInfo) p2).getActionList());
    }

    public final void a(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setMaxTextLength(p2);
        return;
    }

    public final void a(Object p1, CharSequence p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setError(p2);
        return;
    }

    public final void a(Object p1, Object p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).addAction(((android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction) p2));
        return;
    }

    public final boolean a(Object p2, android.view.View p3)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).removeChild(p3);
    }

    public final boolean a(Object p2, android.view.View p3, int p4)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).removeChild(p3, p4);
    }

    public final int b(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction) p2).getId();
    }

    public final boolean b(Object p2, Object p3)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).removeAction(((android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction) p3));
    }

    public final CharSequence c(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction) p2).getLabel();
    }

    public final boolean d(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo) p2).isSelected();
    }

    public final CharSequence e(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getError();
    }

    public final int f(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getMaxTextLength();
    }

    public final Object g(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getWindow();
    }
}
