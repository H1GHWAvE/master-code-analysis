package android.support.v4.view;
public abstract class by {
    public static final int a = 255;
    public static final int b = 254;
    private android.database.DataSetObservable c;

    public by()
    {
        this.c = new android.database.DataSetObservable();
        return;
    }

    private static void a()
    {
        return;
    }

    private static Object f()
    {
        throw new UnsupportedOperationException("Required method instantiateItem was not overridden");
    }

    private static void g()
    {
        throw new UnsupportedOperationException("Required method destroyItem was not overridden");
    }

    private static void h()
    {
        return;
    }

    private static void i()
    {
        return;
    }

    private static int j()
    {
        return -1;
    }

    private void k()
    {
        this.c.notifyChanged();
        return;
    }

    private static CharSequence l()
    {
        return 0;
    }

    private static float m()
    {
        return 1065353216;
    }

    public Object a(android.view.ViewGroup p3, int p4)
    {
        throw new UnsupportedOperationException("Required method instantiateItem was not overridden");
    }

    public void a(int p3, Object p4)
    {
        throw new UnsupportedOperationException("Required method destroyItem was not overridden");
    }

    public final void a(android.database.DataSetObserver p2)
    {
        this.c.registerObserver(p2);
        return;
    }

    public void a(android.os.Parcelable p1, ClassLoader p2)
    {
        return;
    }

    public void a(Object p1)
    {
        return;
    }

    public abstract boolean a();

    public void b()
    {
        return;
    }

    public final void b(android.database.DataSetObserver p2)
    {
        this.c.unregisterObserver(p2);
        return;
    }

    public void c()
    {
        return;
    }

    public android.os.Parcelable d()
    {
        return 0;
    }

    public abstract int e();
}
