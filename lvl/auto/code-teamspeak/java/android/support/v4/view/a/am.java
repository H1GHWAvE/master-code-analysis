package android.support.v4.view.a;
final class am {

    am()
    {
        return;
    }

    private static int a(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getLiveRegion();
    }

    private static Object a(int p1, int p2, int p3, int p4, boolean p5)
    {
        return android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo.obtain(p1, p2, p3, p4, p5);
    }

    private static Object a(int p1, int p2, boolean p3)
    {
        return android.view.accessibility.AccessibilityNodeInfo$CollectionInfo.obtain(p1, p2, p3);
    }

    private static void a(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setLiveRegion(p1);
        return;
    }

    private static void a(Object p0, Object p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setCollectionInfo(((android.view.accessibility.AccessibilityNodeInfo$CollectionInfo) p1));
        return;
    }

    private static void a(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setContentInvalid(p1);
        return;
    }

    private static Object b(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getCollectionInfo();
    }

    private static void b(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setInputType(p1);
        return;
    }

    private static void b(Object p0, Object p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setCollectionItemInfo(((android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo) p1));
        return;
    }

    private static void b(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setCanOpenPopup(p1);
        return;
    }

    private static Object c(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getCollectionItemInfo();
    }

    private static void c(Object p0, Object p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setRangeInfo(((android.view.accessibility.AccessibilityNodeInfo$RangeInfo) p1));
        return;
    }

    private static void c(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setDismissable(p1);
        return;
    }

    private static Object d(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getRangeInfo();
    }

    private static void d(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setMultiLine(p1);
        return;
    }

    private static boolean e(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isContentInvalid();
    }

    private static boolean f(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).canOpenPopup();
    }

    private static android.os.Bundle g(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getExtras();
    }

    private static int h(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getInputType();
    }

    private static boolean i(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isDismissable();
    }

    private static boolean j(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).isMultiLine();
    }
}
