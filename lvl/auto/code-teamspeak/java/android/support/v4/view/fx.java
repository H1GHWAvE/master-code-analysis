package android.support.v4.view;
final class fx {

    fx()
    {
        return;
    }

    private static void a(android.view.View p1)
    {
        p1.animate().withLayer();
        return;
    }

    private static void a(android.view.View p2, android.support.v4.view.gd p3)
    {
        if (p3 == null) {
            p2.animate().setListener(0);
        } else {
            p2.animate().setListener(new android.support.v4.view.fy(p3, p2));
        }
        return;
    }

    private static void a(android.view.View p1, Runnable p2)
    {
        p1.animate().withStartAction(p2);
        return;
    }

    private static void b(android.view.View p1, Runnable p2)
    {
        p1.animate().withEndAction(p2);
        return;
    }
}
