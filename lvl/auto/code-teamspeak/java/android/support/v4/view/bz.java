package android.support.v4.view;
public final class bz extends android.support.v4.view.cc {
    private static final String f = "PagerTabStrip";
    private static final int g = 3;
    private static final int h = 6;
    private static final int i = 16;
    private static final int j = 32;
    private static final int k = 64;
    private static final int l = 1;
    private static final int m = 32;
    private float A;
    private float B;
    private int C;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private final android.graphics.Paint t;
    private final android.graphics.Rect u;
    private int v;
    private boolean w;
    private boolean x;
    private int y;
    private boolean z;

    private bz(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private bz(android.content.Context p7, byte p8)
    {
        this(p7, 0);
        this.t = new android.graphics.Paint();
        this.u = new android.graphics.Rect();
        this.v = 255;
        this.w = 0;
        this.x = 0;
        this.n = this.e;
        this.t.setColor(this.n);
        android.graphics.drawable.Drawable v0_9 = p7.getResources().getDisplayMetrics().density;
        this.o = ((int) ((1077936128 * v0_9) + 1056964608));
        this.p = ((int) ((1086324736 * v0_9) + 1056964608));
        this.q = ((int) (1115684864 * v0_9));
        this.s = ((int) ((1098907648 * v0_9) + 1056964608));
        this.y = ((int) ((1065353216 * v0_9) + 1056964608));
        this.r = ((int) ((v0_9 * 1107296256) + 1056964608));
        this.C = android.view.ViewConfiguration.get(p7).getScaledTouchSlop();
        this.setPadding(this.getPaddingLeft(), this.getPaddingTop(), this.getPaddingRight(), this.getPaddingBottom());
        this.setTextSpacing(this.getTextSpacing());
        this.setWillNotDraw(0);
        this.b.setFocusable(1);
        this.b.setOnClickListener(new android.support.v4.view.ca(this));
        this.d.setFocusable(1);
        this.d.setOnClickListener(new android.support.v4.view.cb(this));
        if (this.getBackground() == null) {
            this.w = 1;
        }
        return;
    }

    final void a(int p7, float p8, boolean p9)
    {
        android.graphics.Rect v0 = this.u;
        int v1 = this.getHeight();
        int v4_2 = (v1 - this.o);
        v0.set((this.c.getLeft() - this.s), v4_2, (this.c.getRight() + this.s), v1);
        super.a(p7, p8, p9);
        this.v = ((int) ((Math.abs((p8 - 1056964608)) * 1073741824) * 1132396544));
        v0.union((this.c.getLeft() - this.s), v4_2, (this.c.getRight() + this.s), v1);
        this.invalidate(v0);
        return;
    }

    public final boolean getDrawFullUnderline()
    {
        return this.w;
    }

    final int getMinHeight()
    {
        return Math.max(super.getMinHeight(), this.r);
    }

    public final int getTabIndicatorColor()
    {
        return this.n;
    }

    protected final void onDraw(android.graphics.Canvas p9)
    {
        super.onDraw(p9);
        int v6 = this.getHeight();
        android.graphics.Canvas v0_2 = (this.c.getLeft() - this.s);
        float v3_0 = (this.c.getRight() + this.s);
        float v2_1 = (v6 - this.o);
        this.t.setColor(((this.v << 24) | (this.n & 16777215)));
        p9.drawRect(((float) v0_2), ((float) v2_1), ((float) v3_0), ((float) v6), this.t);
        if (this.w) {
            this.t.setColor((-16777216 | (this.n & 16777215)));
            p9.drawRect(((float) this.getPaddingLeft()), ((float) (v6 - this.y)), ((float) (this.getWidth() - this.getPaddingRight())), ((float) v6), this.t);
        }
        return;
    }

    public final boolean onTouchEvent(android.view.MotionEvent p6)
    {
        android.support.v4.view.ViewPager v0_0 = 0;
        int v2_0 = p6.getAction();
        if ((v2_0 == 0) || (!this.z)) {
            float v3_1 = p6.getX();
            float v4 = p6.getY();
            switch (v2_0) {
                case 0:
                    this.A = v3_1;
                    this.B = v4;
                    this.z = 0;
                    break;
                case 1:
                    if (v3_1 >= ((float) (this.c.getLeft() - this.s))) {
                        if (v3_1 <= ((float) (this.c.getRight() + this.s))) {
                        } else {
                            this.a.setCurrentItem((this.a.getCurrentItem() + 1));
                        }
                    } else {
                        this.a.setCurrentItem((this.a.getCurrentItem() - 1));
                    }
                    break;
                case 2:
                    if ((Math.abs((v3_1 - this.A)) <= ((float) this.C)) && (Math.abs((v4 - this.B)) <= ((float) this.C))) {
                    } else {
                        this.z = 1;
                    }
                    break;
            }
            v0_0 = 1;
        }
        return v0_0;
    }

    public final void setBackgroundColor(int p2)
    {
        super.setBackgroundColor(p2);
        if (!this.x) {
            int v0_3;
            if ((-16777216 & p2) != 0) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            this.w = v0_3;
        }
        return;
    }

    public final void setBackgroundDrawable(android.graphics.drawable.Drawable p2)
    {
        super.setBackgroundDrawable(p2);
        if (!this.x) {
            int v0_1;
            if (p2 != null) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
            }
            this.w = v0_1;
        }
        return;
    }

    public final void setBackgroundResource(int p2)
    {
        super.setBackgroundResource(p2);
        if (!this.x) {
            int v0_1;
            if (p2 != 0) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
            }
            this.w = v0_1;
        }
        return;
    }

    public final void setDrawFullUnderline(boolean p2)
    {
        this.w = p2;
        this.x = 1;
        this.invalidate();
        return;
    }

    public final void setPadding(int p2, int p3, int p4, int p5)
    {
        if (p5 < this.p) {
            p5 = this.p;
        }
        super.setPadding(p2, p3, p4, p5);
        return;
    }

    public final void setTabIndicatorColor(int p3)
    {
        this.n = p3;
        this.t.setColor(this.n);
        this.invalidate();
        return;
    }

    public final void setTabIndicatorColorResource(int p2)
    {
        this.setTabIndicatorColor(this.getContext().getResources().getColor(p2));
        return;
    }

    public final void setTextSpacing(int p2)
    {
        if (p2 < this.q) {
            p2 = this.q;
        }
        super.setTextSpacing(p2);
        return;
    }
}
