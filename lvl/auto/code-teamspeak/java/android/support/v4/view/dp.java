package android.support.v4.view;
final class dp {

    dp()
    {
        return;
    }

    private static int a(android.view.View p1)
    {
        return p1.getLabelFor();
    }

    private static void a(android.view.View p0, int p1)
    {
        p0.setLabelFor(p1);
        return;
    }

    private static void a(android.view.View p0, int p1, int p2, int p3, int p4)
    {
        p0.setPaddingRelative(p1, p2, p3, p4);
        return;
    }

    private static void a(android.view.View p0, android.graphics.Paint p1)
    {
        p0.setLayerPaint(p1);
        return;
    }

    private static int b(android.view.View p1)
    {
        return p1.getLayoutDirection();
    }

    private static void b(android.view.View p0, int p1)
    {
        p0.setLayoutDirection(p1);
        return;
    }

    private static int c(android.view.View p1)
    {
        return p1.getPaddingStart();
    }

    private static int d(android.view.View p1)
    {
        return p1.getPaddingEnd();
    }

    private static int e(android.view.View p1)
    {
        return p1.getWindowSystemUiVisibility();
    }

    private static boolean f(android.view.View p1)
    {
        return p1.isPaddingRelative();
    }
}
