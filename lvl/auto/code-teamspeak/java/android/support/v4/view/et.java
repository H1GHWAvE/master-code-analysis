package android.support.v4.view;
final class et extends android.support.v4.view.a {
    final synthetic android.support.v4.view.ViewPager a;

    et(android.support.v4.view.ViewPager p1)
    {
        this.a = p1;
        return;
    }

    private boolean a()
    {
        int v0 = 1;
        if ((android.support.v4.view.ViewPager.b(this.a) == null) || (android.support.v4.view.ViewPager.b(this.a).e() <= 1)) {
            v0 = 0;
        }
        return v0;
    }

    public final void a(android.view.View p3, android.support.v4.view.a.q p4)
    {
        super.a(p3, p4);
        p4.b(android.support.v4.view.ViewPager.getName());
        p4.i(this.a());
        if (this.a.canScrollHorizontally(1)) {
            p4.a(4096);
        }
        if (this.a.canScrollHorizontally(-1)) {
            p4.a(8192);
        }
        return;
    }

    public final void a(android.view.View p5, android.view.accessibility.AccessibilityEvent p6)
    {
        super.a(p5, p6);
        p6.setClassName(android.support.v4.view.ViewPager.getName());
        Object v0_2 = android.support.v4.view.a.bd.a();
        v0_2.a(this.a());
        if ((p6.getEventType() == 4096) && (android.support.v4.view.ViewPager.b(this.a) != null)) {
            android.support.v4.view.a.bd.a.d(v0_2.b, android.support.v4.view.ViewPager.b(this.a).e());
            android.support.v4.view.a.bd.a.c(v0_2.b, android.support.v4.view.ViewPager.c(this.a));
            android.support.v4.view.a.bd.a.h(v0_2.b, android.support.v4.view.ViewPager.c(this.a));
        }
        return;
    }

    public final boolean a(android.view.View p5, int p6, android.os.Bundle p7)
    {
        android.support.v4.view.ViewPager v0 = 1;
        if (!super.a(p5, p6, p7)) {
            switch (p6) {
                case 4096:
                    if (!this.a.canScrollHorizontally(1)) {
                        v0 = 0;
                    } else {
                        this.a.setCurrentItem((android.support.v4.view.ViewPager.c(this.a) + 1));
                    }
                    break;
                case 8192:
                    if (!this.a.canScrollHorizontally(-1)) {
                        v0 = 0;
                    } else {
                        this.a.setCurrentItem((android.support.v4.view.ViewPager.c(this.a) - 1));
                    }
                    break;
                default:
                    v0 = 0;
            }
        }
        return v0;
    }
}
