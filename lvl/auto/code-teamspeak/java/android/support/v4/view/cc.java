package android.support.v4.view;
public class cc extends android.view.ViewGroup implements android.support.v4.view.eq {
    private static final String f = "PagerTitleStrip";
    private static final int[] o = None;
    private static final int[] p = None;
    private static final float q = 1058642330;
    private static final int r = 16;
    private static final android.support.v4.view.cf t;
    android.support.v4.view.ViewPager a;
    android.widget.TextView b;
    android.widget.TextView c;
    android.widget.TextView d;
    int e;
    private int g;
    private float h;
    private int i;
    private int j;
    private boolean k;
    private boolean l;
    private final android.support.v4.view.ce m;
    private ref.WeakReference n;
    private int s;

    static cc()
    {
        android.support.v4.view.cg v0_1 = new int[4];
        v0_1 = {16842804, 16842901, 16842904, 16842927};
        android.support.v4.view.cc.o = v0_1;
        android.support.v4.view.cg v0_3 = new int[1];
        v0_3[0] = 16843660;
        android.support.v4.view.cc.p = v0_3;
        if (android.os.Build$VERSION.SDK_INT < 14) {
            android.support.v4.view.cc.t = new android.support.v4.view.cg();
        } else {
            android.support.v4.view.cc.t = new android.support.v4.view.ch();
        }
        return;
    }

    private cc(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public cc(android.content.Context p7, byte p8)
    {
        int v0_0 = 0;
        this(p7, 0);
        this.g = -1;
        this.h = -1082130432;
        this.m = new android.support.v4.view.ce(this, 0);
        int v1_5 = new android.widget.TextView(p7);
        this.b = v1_5;
        this.addView(v1_5);
        int v1_7 = new android.widget.TextView(p7);
        this.c = v1_7;
        this.addView(v1_7);
        int v1_9 = new android.widget.TextView(p7);
        this.d = v1_9;
        this.addView(v1_9);
        int v1_11 = p7.obtainStyledAttributes(0, android.support.v4.view.cc.o);
        int v2_1 = v1_11.getResourceId(0, 0);
        if (v2_1 != 0) {
            this.b.setTextAppearance(p7, v2_1);
            this.c.setTextAppearance(p7, v2_1);
            this.d.setTextAppearance(p7, v2_1);
        }
        android.text.TextUtils$TruncateAt v3_4 = v1_11.getDimensionPixelSize(1, 0);
        if (v3_4 != null) {
            android.text.TextUtils$TruncateAt v3_5 = ((float) v3_4);
            this.b.setTextSize(0, v3_5);
            this.c.setTextSize(0, v3_5);
            this.d.setTextSize(0, v3_5);
        }
        if (v1_11.hasValue(2)) {
            android.text.TextUtils$TruncateAt v3_7 = v1_11.getColor(2, 0);
            this.b.setTextColor(v3_7);
            this.c.setTextColor(v3_7);
            this.d.setTextColor(v3_7);
        }
        this.j = v1_11.getInteger(3, 80);
        v1_11.recycle();
        this.e = this.c.getTextColors().getDefaultColor();
        this.setNonPrimaryAlpha(1058642330);
        this.b.setEllipsize(android.text.TextUtils$TruncateAt.END);
        this.c.setEllipsize(android.text.TextUtils$TruncateAt.END);
        this.d.setEllipsize(android.text.TextUtils$TruncateAt.END);
        if (v2_1 != 0) {
            int v1_20 = p7.obtainStyledAttributes(v2_1, android.support.v4.view.cc.p);
            v0_0 = v1_20.getBoolean(0, 0);
            v1_20.recycle();
        }
        if (v0_0 == 0) {
            this.b.setSingleLine();
            this.c.setSingleLine();
            this.d.setSingleLine();
        } else {
            android.support.v4.view.cc.setSingleLineAllCaps(this.b);
            android.support.v4.view.cc.setSingleLineAllCaps(this.c);
            android.support.v4.view.cc.setSingleLineAllCaps(this.d);
        }
        this.i = ((int) (p7.getResources().getDisplayMetrics().density * 1098907648));
        return;
    }

    static synthetic float a(android.support.v4.view.cc p1)
    {
        return p1.h;
    }

    private static void setSingleLineAllCaps(android.widget.TextView p1)
    {
        android.support.v4.view.cc.t.a(p1);
        return;
    }

    private void setTextSize$255e752(float p3)
    {
        this.b.setTextSize(0, p3);
        this.c.setTextSize(0, p3);
        this.d.setTextSize(0, p3);
        return;
    }

    final void a(int p6)
    {
        this.k = 1;
        this.b.setText(0);
        this.c.setText(0);
        this.d.setText(0);
        float v0_11 = android.view.View$MeasureSpec.makeMeasureSpec(Math.max(0, ((int) (((float) ((this.getWidth() - this.getPaddingLeft()) - this.getPaddingRight())) * 1061997773))), -2147483648);
        int v1_8 = android.view.View$MeasureSpec.makeMeasureSpec(Math.max(0, ((this.getHeight() - this.getPaddingTop()) - this.getPaddingBottom())), -2147483648);
        this.b.measure(v0_11, v1_8);
        this.c.measure(v0_11, v1_8);
        this.d.measure(v0_11, v1_8);
        this.g = p6;
        if (!this.l) {
            this.a(p6, this.h, 0);
        }
        this.k = 0;
        return;
    }

    void a(int p19, float p20, boolean p21)
    {
        if (p19 == this.g) {
            if ((p21) || (p20 != this.h)) {
                this.l = 1;
                int v5_0 = this.b.getMeasuredWidth();
                int v3_0 = this.c.getMeasuredWidth();
                int v6_0 = this.d.getMeasuredWidth();
                android.widget.TextView v4_0 = (v3_0 / 2);
                int v7 = this.getWidth();
                android.widget.TextView v8_0 = this.getHeight();
                int v9_0 = this.getPaddingLeft();
                int v10 = this.getPaddingRight();
                int v11_0 = this.getPaddingTop();
                int v12 = this.getPaddingBottom();
                int v13_0 = (v10 + v4_0);
                int v2_11 = (1056964608 + p20);
                if (v2_11 > 1065353216) {
                    v2_11 -= 1065353216;
                }
                int v2_18;
                android.widget.TextView v4_8;
                int v3_4;
                int v13_2 = (((v7 - v13_0) - ((int) (v2_11 * ((float) ((v7 - (v9_0 + v4_0)) - v13_0))))) - v4_0);
                int v14_2 = (v13_2 + v3_0);
                int v2_16 = this.b.getBaseline();
                int v3_2 = this.c.getBaseline();
                android.widget.TextView v4_2 = this.d.getBaseline();
                int v15_4 = Math.max(Math.max(v2_16, v3_2), v4_2);
                int v2_17 = (v15_4 - v2_16);
                int v3_3 = (v15_4 - v3_2);
                int v15_5 = (v15_4 - v4_2);
                android.widget.TextView v4_7 = Math.max(Math.max((this.b.getMeasuredHeight() + v2_17), (this.c.getMeasuredHeight() + v3_3)), (this.d.getMeasuredHeight() + v15_5));
                switch ((this.j & 112)) {
                    case 16:
                        android.widget.TextView v8_5 = ((((v8_0 - v11_0) - v12) - v4_7) / 2);
                        v4_8 = (v8_5 + v2_17);
                        v2_18 = (v8_5 + v3_3);
                        v3_4 = (v8_5 + v15_5);
                        break;
                    case 80:
                        android.widget.TextView v8_2 = ((v8_0 - v12) - v4_7);
                        v4_8 = (v8_2 + v2_17);
                        v2_18 = (v8_2 + v3_3);
                        v3_4 = (v8_2 + v15_5);
                        break;
                    default:
                        v4_8 = (v11_0 + v2_17);
                        v2_18 = (v11_0 + v3_3);
                        v3_4 = (v11_0 + v15_5);
                }
                this.c.layout(v13_2, v2_18, v14_2, (this.c.getMeasuredHeight() + v2_18));
                int v2_22 = Math.min(v9_0, ((v13_2 - this.i) - v5_0));
                this.b.layout(v2_22, v4_8, (v5_0 + v2_22), (this.b.getMeasuredHeight() + v4_8));
                int v2_25 = Math.max(((v7 - v10) - v6_0), (this.i + v14_2));
                this.d.layout(v2_25, v3_4, (v2_25 + v6_0), (this.d.getMeasuredHeight() + v3_4));
                this.h = p20;
                this.l = 0;
            }
        } else {
            this.a.getAdapter();
            this.a(p19);
        }
        return;
    }

    final void a(android.support.v4.view.by p2, android.support.v4.view.by p3)
    {
        if (p2 != null) {
            p2.b(this.m);
            this.n = 0;
        }
        if (p3 != null) {
            p3.a(this.m);
            this.n = new ref.WeakReference(p3);
        }
        if (this.a != null) {
            this.g = -1;
            this.h = -1082130432;
            this.a(this.a.getCurrentItem());
            this.requestLayout();
        }
        return;
    }

    int getMinHeight()
    {
        int v0 = 0;
        android.graphics.drawable.Drawable v1 = this.getBackground();
        if (v1 != null) {
            v0 = v1.getIntrinsicHeight();
        }
        return v0;
    }

    public int getTextSpacing()
    {
        return this.i;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        int v0_0 = this.getParent();
        if ((v0_0 instanceof android.support.v4.view.ViewPager)) {
            int v0_3;
            int v0_1 = ((android.support.v4.view.ViewPager) v0_0);
            android.support.v4.view.by v1_1 = v0_1.getAdapter();
            v0_1.a(this.m);
            v0_1.setOnAdapterChangeListener(this.m);
            this.a = v0_1;
            if (this.n == null) {
                v0_3 = 0;
            } else {
                v0_3 = ((android.support.v4.view.by) this.n.get());
            }
            this.a(v0_3, v1_1);
            return;
        } else {
            throw new IllegalStateException("PagerTitleStrip must be a direct child of a ViewPager.");
        }
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if (this.a != null) {
            this.a(this.a.getAdapter(), 0);
            this.a.a(0);
            this.a.setOnAdapterChangeListener(0);
            this.a = 0;
        }
        return;
    }

    protected void onLayout(boolean p4, int p5, int p6, int p7, int p8)
    {
        float v0 = 0;
        if (this.a != null) {
            if (this.h >= 0) {
                v0 = this.h;
            }
            this.a(this.g, v0, 1);
        }
        return;
    }

    protected void onMeasure(int p12, int p13)
    {
        int v0_0 = android.view.View$MeasureSpec.getMode(p12);
        int v1_0 = android.view.View$MeasureSpec.getMode(p13);
        int v2 = android.view.View$MeasureSpec.getSize(p12);
        int v3 = android.view.View$MeasureSpec.getSize(p13);
        if (v0_0 == 1073741824) {
            int v0_1 = this.getMinHeight();
            int v4_1 = (this.getPaddingTop() + this.getPaddingBottom());
            int v5_1 = (v3 - v4_1);
            int v6_4 = android.view.View$MeasureSpec.makeMeasureSpec(Math.max(0, ((int) (((float) v2) * 1061997773))), -2147483648);
            int v5_3 = android.view.View$MeasureSpec.makeMeasureSpec(Math.min(0, v5_1), -2147483648);
            this.b.measure(v6_4, v5_3);
            this.c.measure(v6_4, v5_3);
            this.d.measure(v6_4, v5_3);
            if (v1_0 != 1073741824) {
                this.setMeasuredDimension(v2, Math.max(v0_1, (this.c.getMeasuredHeight() + v4_1)));
            } else {
                this.setMeasuredDimension(v2, v3);
            }
            return;
        } else {
            throw new IllegalStateException("Must measure with an exact width");
        }
    }

    public void requestLayout()
    {
        if (!this.k) {
            super.requestLayout();
        }
        return;
    }

    public void setGravity(int p1)
    {
        this.j = p1;
        this.requestLayout();
        return;
    }

    public void setNonPrimaryAlpha(float p4)
    {
        this.s = (((int) (1132396544 * p4)) & 255);
        int v0_6 = ((this.s << 24) | (this.e & 16777215));
        this.b.setTextColor(v0_6);
        this.d.setTextColor(v0_6);
        return;
    }

    public void setTextColor(int p4)
    {
        this.e = p4;
        this.c.setTextColor(p4);
        int v0_3 = ((this.s << 24) | (this.e & 16777215));
        this.b.setTextColor(v0_3);
        this.d.setTextColor(v0_3);
        return;
    }

    public void setTextSpacing(int p1)
    {
        this.i = p1;
        this.requestLayout();
        return;
    }
}
