package android.support.v4.view;
public final class fk {
    static final int b = 2113929216;
    public static final android.support.v4.view.fu c = None;
    private static final String d = "ViewAnimatorCompat";
    public ref.WeakReference a;
    private Runnable e;
    private Runnable f;
    private int g;

    static fk()
    {
        android.support.v4.view.fm v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 21) {
            if (v0_0 < 19) {
                if (v0_0 < 18) {
                    if (v0_0 < 16) {
                        if (v0_0 < 14) {
                            android.support.v4.view.fk.c = new android.support.v4.view.fm();
                        } else {
                            android.support.v4.view.fk.c = new android.support.v4.view.fo();
                        }
                    } else {
                        android.support.v4.view.fk.c = new android.support.v4.view.fr();
                    }
                } else {
                    android.support.v4.view.fk.c = new android.support.v4.view.fq();
                }
            } else {
                android.support.v4.view.fk.c = new android.support.v4.view.fs();
            }
        } else {
            android.support.v4.view.fk.c = new android.support.v4.view.ft();
        }
        return;
    }

    fk(android.view.View p2)
    {
        this.e = 0;
        this.f = 0;
        this.g = -1;
        this.a = new ref.WeakReference(p2);
        return;
    }

    static synthetic int a(android.support.v4.view.fk p0, int p1)
    {
        p0.g = p1;
        return p1;
    }

    private android.support.v4.view.fk a(Runnable p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.a(this, v0_2, p3);
        }
        return this;
    }

    static synthetic Runnable a(android.support.v4.view.fk p1)
    {
        return p1.e;
    }

    static synthetic Runnable a(android.support.v4.view.fk p0, Runnable p1)
    {
        p0.f = p1;
        return p1;
    }

    private android.support.v4.view.fk b(Runnable p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.b(this, v0_2, p3);
        }
        return this;
    }

    static synthetic Runnable b(android.support.v4.view.fk p1)
    {
        return p1.f;
    }

    static synthetic Runnable b(android.support.v4.view.fk p0, Runnable p1)
    {
        p0.e = p1;
        return p1;
    }

    static synthetic int c(android.support.v4.view.fk p1)
    {
        return p1.g;
    }

    private long c()
    {
        long v0_3;
        long v0_2 = ((android.view.View) this.a.get());
        if (v0_2 == 0) {
            v0_3 = 0;
        } else {
            v0_3 = android.support.v4.view.fk.c.a(v0_2);
        }
        return v0_3;
    }

    private android.support.v4.view.fk c(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.d(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk d(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.b(this, v0_2, p3);
        }
        return this;
    }

    private android.view.animation.Interpolator d()
    {
        int v0_3;
        int v0_2 = ((android.view.View) this.a.get());
        if (v0_2 == 0) {
            v0_3 = 0;
        } else {
            v0_3 = android.support.v4.view.fk.c.b(v0_2);
        }
        return v0_3;
    }

    private long e()
    {
        long v0_3;
        long v0_2 = ((android.view.View) this.a.get());
        if (v0_2 == 0) {
            v0_3 = 0;
        } else {
            v0_3 = android.support.v4.view.fk.c.c(v0_2);
        }
        return v0_3;
    }

    private android.support.v4.view.fk e(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.e(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk f()
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.a(this, v0_2);
        }
        return this;
    }

    private android.support.v4.view.fk f(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.f(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk g()
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.b(this, v0_2);
        }
        return this;
    }

    private android.support.v4.view.fk g(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.g(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk h()
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.e(this, v0_2);
        }
        return this;
    }

    private android.support.v4.view.fk h(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.h(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk i(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.i(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk j(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.j(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk k(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.k(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk l(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.l(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk m(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.m(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk n(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.n(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk o(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.o(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk p(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.p(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk q(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.q(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk r(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.r(this, v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk s(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.d(v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk t(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.c(v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk u(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.a(v0_2, p3);
        }
        return this;
    }

    private android.support.v4.view.fk v(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.b(v0_2, p3);
        }
        return this;
    }

    public final android.support.v4.view.fk a(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.a(this, v0_2, p3);
        }
        return this;
    }

    public final android.support.v4.view.fk a(long p4)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.a(v0_2, p4);
        }
        return this;
    }

    public final android.support.v4.view.fk a(android.support.v4.view.gd p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.a(this, v0_2, p3);
        }
        return this;
    }

    public final android.support.v4.view.fk a(android.support.v4.view.gf p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.a(v0_2, p3);
        }
        return this;
    }

    public final android.support.v4.view.fk a(android.view.animation.Interpolator p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.a(v0_2, p3);
        }
        return this;
    }

    public final void a()
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.c(this, v0_2);
        }
        return;
    }

    public final android.support.v4.view.fk b(float p3)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.c(this, v0_2, p3);
        }
        return this;
    }

    public final android.support.v4.view.fk b(long p4)
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.b(v0_2, p4);
        }
        return this;
    }

    public final void b()
    {
        android.view.View v0_2 = ((android.view.View) this.a.get());
        if (v0_2 != null) {
            android.support.v4.view.fk.c.d(this, v0_2);
        }
        return;
    }
}
