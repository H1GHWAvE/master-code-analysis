package android.support.v4.widget;
public class SlidingPaneLayout extends android.view.ViewGroup {
    static final android.support.v4.widget.di a = None;
    private static final String b = "SlidingPaneLayout";
    private static final int c = 32;
    private static final int d = 3435973836;
    private static final int f = 400;
    private int e;
    private int g;
    private android.graphics.drawable.Drawable h;
    private android.graphics.drawable.Drawable i;
    private final int j;
    private boolean k;
    private android.view.View l;
    private float m;
    private float n;
    private int o;
    private boolean p;
    private int q;
    private float r;
    private float s;
    private android.support.v4.widget.df t;
    private final android.support.v4.widget.eg u;
    private boolean v;
    private boolean w;
    private final android.graphics.Rect x;
    private final java.util.ArrayList y;

    static SlidingPaneLayout()
    {
        android.support.v4.widget.dj v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 17) {
            if (v0_0 < 16) {
                android.support.v4.widget.SlidingPaneLayout.a = new android.support.v4.widget.dj();
            } else {
                android.support.v4.widget.SlidingPaneLayout.a = new android.support.v4.widget.dk();
            }
        } else {
            android.support.v4.widget.SlidingPaneLayout.a = new android.support.v4.widget.dl();
        }
        return;
    }

    private SlidingPaneLayout(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private SlidingPaneLayout(android.content.Context p2, byte p3)
    {
        this(p2, 0);
        return;
    }

    private SlidingPaneLayout(android.content.Context p6, char p7)
    {
        this(p6, 0, 0);
        this.e = -858993460;
        this.w = 1;
        this.x = new android.graphics.Rect();
        this.y = new java.util.ArrayList();
        float v0_8 = p6.getResources().getDisplayMetrics().density;
        this.j = ((int) ((1107296256 * v0_8) + 1056964608));
        android.view.ViewConfiguration.get(p6);
        this.setWillNotDraw(0);
        android.support.v4.view.cx.a(this, new android.support.v4.widget.db(this));
        android.support.v4.view.cx.c(this, 1);
        this.u = android.support.v4.widget.eg.a(this, 1056964608, new android.support.v4.widget.dd(this, 0));
        this.u.s = (v0_8 * 1137180672);
        return;
    }

    private void a(int p5)
    {
        if (this.l != null) {
            boolean v3 = this.n();
            android.view.View v0_3 = ((android.support.v4.widget.de) this.l.getLayoutParams());
            float v1_1 = this.l.getWidth();
            if (v3) {
                p5 = ((this.getWidth() - p5) - v1_1);
            }
            int v2_2;
            if (!v3) {
                v2_2 = this.getPaddingLeft();
            } else {
                v2_2 = this.getPaddingRight();
            }
            float v1_4;
            if (!v3) {
                v1_4 = v0_3.leftMargin;
            } else {
                v1_4 = v0_3.rightMargin;
            }
            this.m = (((float) (p5 - (v1_4 + v2_2))) / ((float) this.o));
            if (this.q != 0) {
                this.b(this.m);
            }
            if (v0_3.c) {
                this.a(this.l, this.m, this.e);
            }
        } else {
            this.m = 0;
        }
        return;
    }

    static synthetic void a(android.support.v4.widget.SlidingPaneLayout p4, int p5)
    {
        if (p4.l != null) {
            boolean v3 = p4.n();
            android.view.View v0_3 = ((android.support.v4.widget.de) p4.l.getLayoutParams());
            float v1_1 = p4.l.getWidth();
            if (v3) {
                p5 = ((p4.getWidth() - p5) - v1_1);
            }
            int v2_2;
            if (!v3) {
                v2_2 = p4.getPaddingLeft();
            } else {
                v2_2 = p4.getPaddingRight();
            }
            float v1_4;
            if (!v3) {
                v1_4 = v0_3.leftMargin;
            } else {
                v1_4 = v0_3.rightMargin;
            }
            p4.m = (((float) (p5 - (v1_4 + v2_2))) / ((float) p4.o));
            if (p4.q != 0) {
                p4 = p4.b(p4.m);
            }
            if (v0_3.c) {
                p4.a(p4.l, p4.m, p4.e);
            }
        } else {
            p4.m = 0;
        }
        return;
    }

    static synthetic void a(android.support.v4.widget.SlidingPaneLayout p0, android.view.View p1)
    {
        p0.d(p1);
        return;
    }

    private void a(android.view.View p7, float p8, int p9)
    {
        android.graphics.Paint v0_1 = ((android.support.v4.widget.de) p7.getLayoutParams());
        if ((p8 <= 0) || (p9 == 0)) {
            if (android.support.v4.view.cx.e(p7) != 0) {
                if (v0_1.d != null) {
                    v0_1.d.setColorFilter(0);
                }
                android.graphics.Paint v0_4 = new android.support.v4.widget.dc(this, p7);
                this.y.add(v0_4);
                android.support.v4.view.cx.a(this, v0_4);
            }
        } else {
            java.util.ArrayList v1_13 = ((((int) (((float) ((-16777216 & p9) >> 24)) * p8)) << 24) | (16777215 & p9));
            if (v0_1.d == null) {
                v0_1.d = new android.graphics.Paint();
            }
            v0_1.d.setColorFilter(new android.graphics.PorterDuffColorFilter(v1_13, android.graphics.PorterDuff$Mode.SRC_OVER));
            if (android.support.v4.view.cx.e(p7) != 2) {
                android.support.v4.view.cx.a(p7, 2, v0_1.d);
            }
            this.d(p7);
        }
        return;
    }

    private boolean a(float p6)
    {
        int v0_16;
        if (this.k) {
            int v0_8;
            int v0_3 = ((android.support.v4.widget.de) this.l.getLayoutParams());
            if (!this.n()) {
                v0_8 = ((int) (((float) (v0_3.leftMargin + this.getPaddingLeft())) + (((float) this.o) * p6)));
            } else {
                v0_8 = ((int) (((float) this.getWidth()) - ((((float) (v0_3.rightMargin + this.getPaddingRight())) + (((float) this.o) * p6)) + ((float) this.l.getWidth()))));
            }
            if (!this.u.a(this.l, v0_8, this.l.getTop())) {
                v0_16 = 0;
            } else {
                this.a();
                android.support.v4.view.cx.b(this);
                v0_16 = 1;
            }
        } else {
            v0_16 = 0;
        }
        return v0_16;
    }

    static synthetic boolean a(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.p;
    }

    static synthetic boolean a(android.support.v4.widget.SlidingPaneLayout p0, boolean p1)
    {
        p0.v = p1;
        return p1;
    }

    private boolean a(android.view.View p10, int p11, int p12, int p13)
    {
        int v0_5;
        if (!(p10 instanceof android.view.ViewGroup)) {
            if (!this.n()) {
                p11 = (- p11);
            }
            if (!android.support.v4.view.cx.a(p10, p11)) {
                v0_5 = 0;
            } else {
                v0_5 = 1;
            }
        } else {
            int v3 = p10.getScrollX();
            int v4 = p10.getScrollY();
            int v2_1 = (((android.view.ViewGroup) p10).getChildCount() - 1);
            while (v2_1 >= 0) {
                boolean v5_0 = ((android.view.ViewGroup) p10).getChildAt(v2_1);
                if (((p12 + v3) < v5_0.getLeft()) || (((p12 + v3) >= v5_0.getRight()) || (((p13 + v4) < v5_0.getTop()) || (((p13 + v4) >= v5_0.getBottom()) || (!this.a(v5_0, p11, ((p12 + v3) - v5_0.getLeft()), ((p13 + v4) - v5_0.getTop()))))))) {
                    v2_1--;
                } else {
                    v0_5 = 1;
                }
            }
        }
        return v0_5;
    }

    static synthetic android.support.v4.widget.eg b(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.u;
    }

    private static void b()
    {
        return;
    }

    private void b(float p10)
    {
        int v0_4;
        boolean v3 = this.n();
        int v0_2 = ((android.support.v4.widget.de) this.l.getLayoutParams());
        if (!v0_2.c) {
            v0_4 = 0;
        } else {
            int v0_3;
            if (!v3) {
                v0_3 = v0_2.leftMargin;
            } else {
                v0_3 = v0_2.rightMargin;
            }
            if (v0_3 > 0) {
            } else {
                v0_4 = 1;
            }
        }
        int v4 = this.getChildCount();
        int v2_1 = 0;
        while (v2_1 < v4) {
            android.view.View v5 = this.getChildAt(v2_1);
            if (v5 != this.l) {
                float v1_5 = ((int) ((1065353216 - this.n) * ((float) this.q)));
                this.n = p10;
                float v1_6 = (v1_5 - ((int) ((1065353216 - p10) * ((float) this.q))));
                if (v3) {
                    v1_6 = (- v1_6);
                }
                v5.offsetLeftAndRight(v1_6);
                if (v0_4 != 0) {
                    float v1_8;
                    if (!v3) {
                        v1_8 = (1065353216 - this.n);
                    } else {
                        v1_8 = (this.n - 1065353216);
                    }
                    this.a(v5, v1_8, this.g);
                }
            }
            v2_1++;
        }
        return;
    }

    static synthetic float c(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.m;
    }

    private void c()
    {
        this.sendAccessibilityEvent(32);
        return;
    }

    private static boolean c(android.view.View p4)
    {
        int v0 = 1;
        if (!android.support.v4.view.cx.h(p4)) {
            if (android.os.Build$VERSION.SDK_INT < 18) {
                int v2_2 = p4.getBackground();
                if (v2_2 == 0) {
                    v0 = 0;
                } else {
                    if (v2_2.getOpacity() != -1) {
                        v0 = 0;
                    }
                }
            } else {
                v0 = 0;
            }
        }
        return v0;
    }

    static synthetic android.view.View d(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.l;
    }

    private void d()
    {
        this.sendAccessibilityEvent(32);
        return;
    }

    private void d(android.view.View p2)
    {
        android.support.v4.widget.SlidingPaneLayout.a.a(this, p2);
        return;
    }

    private boolean e()
    {
        int v0 = 0;
        if ((this.w) || (this.a(0))) {
            this.v = 0;
            v0 = 1;
        }
        return v0;
    }

    static synthetic boolean e(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.n();
    }

    static synthetic int f(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.o;
    }

    private boolean f()
    {
        int v0 = 1;
        if ((!this.w) && (!this.a(1065353216))) {
            v0 = 0;
        } else {
            this.v = 1;
        }
        return v0;
    }

    static synthetic java.util.ArrayList g(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.y;
    }

    private void g()
    {
        this.h();
        return;
    }

    private boolean h()
    {
        int v0 = 1;
        if ((!this.w) && (!this.a(1065353216))) {
            v0 = 0;
        } else {
            this.v = 1;
        }
        return v0;
    }

    private void i()
    {
        this.e();
        return;
    }

    private boolean j()
    {
        return this.e();
    }

    private boolean k()
    {
        if ((this.k) && (this.m != 1065353216)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private boolean l()
    {
        return this.k;
    }

    private boolean m()
    {
        return this.k;
    }

    private boolean n()
    {
        int v0 = 1;
        if (android.support.v4.view.cx.f(this) != 1) {
            v0 = 0;
        }
        return v0;
    }

    final void a()
    {
        int v2 = this.getChildCount();
        int v0 = 0;
        while (v0 < v2) {
            android.view.View v3 = this.getChildAt(v0);
            if (v3.getVisibility() == 4) {
                v3.setVisibility(0);
            }
            v0++;
        }
        return;
    }

    final void a(android.view.View p18)
    {
        int v7;
        boolean v9 = this.n();
        if (!v9) {
            v7 = this.getPaddingLeft();
        } else {
            v7 = (this.getWidth() - this.getPaddingRight());
        }
        int v1_2;
        if (!v9) {
            v1_2 = (this.getWidth() - this.getPaddingRight());
        } else {
            v1_2 = this.getPaddingLeft();
        }
        int v3_3;
        int v2_8;
        int v4;
        int v5;
        int v10 = this.getPaddingTop();
        int v11 = (this.getHeight() - this.getPaddingBottom());
        if (p18 == null) {
            v2_8 = 0;
            v3_3 = 0;
            v4 = 0;
            v5 = 0;
        } else {
            int v2_6;
            if (!android.support.v4.view.cx.h(p18)) {
                if (android.os.Build$VERSION.SDK_INT < 18) {
                    int v2_5 = p18.getBackground();
                    if (v2_5 != 0) {
                        if (v2_5.getOpacity() != -1) {
                            v2_6 = 0;
                            if (v2_6 == 0) {
                            } else {
                                v5 = p18.getLeft();
                                v4 = p18.getRight();
                                v3_3 = p18.getTop();
                                v2_8 = p18.getBottom();
                            }
                            int v12 = this.getChildCount();
                            int v8 = 0;
                            while (v8 < v12) {
                                android.view.View v13 = this.getChildAt(v8);
                                if (v13 == p18) {
                                    break;
                                }
                                int v6_1;
                                if (!v9) {
                                    v6_1 = v7;
                                } else {
                                    v6_1 = v1_2;
                                }
                                int v6_3;
                                int v14_1 = Math.max(v6_1, v13.getLeft());
                                int v15 = Math.max(v10, v13.getTop());
                                if (!v9) {
                                    v6_3 = v1_2;
                                } else {
                                    v6_3 = v7;
                                }
                                int v6_5;
                                int v6_4 = Math.min(v6_3, v13.getRight());
                                int v16_2 = Math.min(v11, v13.getBottom());
                                if ((v14_1 < v5) || ((v15 < v3_3) || ((v6_4 > v4) || (v16_2 > v2_8)))) {
                                    v6_5 = 0;
                                } else {
                                    v6_5 = 4;
                                }
                                v13.setVisibility(v6_5);
                                v8++;
                            }
                            return;
                        } else {
                            v2_6 = 1;
                        }
                    }
                }
                v2_6 = 0;
            } else {
                v2_6 = 1;
            }
        }
    }

    final boolean b(android.view.View p4)
    {
        int v0_5;
        if (p4 != null) {
            if ((!this.k) || ((!((android.support.v4.widget.de) p4.getLayoutParams()).c) || (this.m <= 0))) {
                v0_5 = 0;
            } else {
                v0_5 = 1;
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    protected boolean checkLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        if ((!(p2 instanceof android.support.v4.widget.de)) || (!super.checkLayoutParams(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public void computeScroll()
    {
        if (this.u.c()) {
            if (this.k) {
                android.support.v4.view.cx.b(this);
            } else {
                this.u.b();
            }
        }
        return;
    }

    public void draw(android.graphics.Canvas p7)
    {
        android.graphics.drawable.Drawable v0_1;
        super.draw(p7);
        if (!this.n()) {
            v0_1 = this.h;
        } else {
            v0_1 = this.i;
        }
        int v1_1;
        if (this.getChildCount() <= 1) {
            v1_1 = 0;
        } else {
            v1_1 = this.getChildAt(1);
        }
        if ((v1_1 != 0) && (v0_1 != null)) {
            int v2_2;
            int v1_2;
            int v3 = v1_1.getTop();
            int v4 = v1_1.getBottom();
            int v5 = v0_1.getIntrinsicWidth();
            if (!this.n()) {
                v1_2 = v1_1.getLeft();
                v2_2 = (v1_2 - v5);
            } else {
                v2_2 = v1_1.getRight();
                v1_2 = (v2_2 + v5);
            }
            v0_1.setBounds(v2_2, v3, v1_2, v4);
            v0_1.draw(p7);
        }
        return;
    }

    protected boolean drawChild(android.graphics.Canvas p8, android.view.View p9, long p10)
    {
        String v0_1 = ((android.support.v4.widget.de) p9.getLayoutParams());
        int v2_1 = p8.save(2);
        if ((this.k) && ((!v0_1.b) && (this.l != null))) {
            p8.getClipBounds(this.x);
            if (!this.n()) {
                this.x.right = Math.min(this.x.right, this.l.getLeft());
            } else {
                this.x.left = Math.max(this.x.left, this.l.getRight());
            }
            p8.clipRect(this.x);
        }
        String v0_4;
        if (android.os.Build$VERSION.SDK_INT >= 11) {
            v0_4 = super.drawChild(p8, p9, p10);
        } else {
            if ((!v0_1.c) || (this.m <= 0)) {
                if (!p9.isDrawingCacheEnabled()) {
                } else {
                    p9.setDrawingCacheEnabled(0);
                }
            } else {
                if (!p9.isDrawingCacheEnabled()) {
                    p9.setDrawingCacheEnabled(1);
                }
                String v3_14 = p9.getDrawingCache();
                if (v3_14 == null) {
                    android.util.Log.e("SlidingPaneLayout", new StringBuilder("drawChild: child view ").append(p9).append(" returned null drawing cache").toString());
                } else {
                    p8.drawBitmap(v3_14, ((float) p9.getLeft()), ((float) p9.getTop()), v0_1.d);
                    v0_4 = 0;
                }
            }
        }
        p8.restoreToCount(v2_1);
        return v0_4;
    }

    protected android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return new android.support.v4.widget.de();
    }

    public android.view.ViewGroup$LayoutParams generateLayoutParams(android.util.AttributeSet p3)
    {
        return new android.support.v4.widget.de(this.getContext(), p3);
    }

    protected android.view.ViewGroup$LayoutParams generateLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        android.support.v4.widget.de v0_2;
        if (!(p2 instanceof android.view.ViewGroup$MarginLayoutParams)) {
            v0_2 = new android.support.v4.widget.de(p2);
        } else {
            v0_2 = new android.support.v4.widget.de(((android.view.ViewGroup$MarginLayoutParams) p2));
        }
        return v0_2;
    }

    public int getCoveredFadeColor()
    {
        return this.g;
    }

    public int getParallaxDistance()
    {
        return this.q;
    }

    public int getSliderFadeColor()
    {
        return this.e;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        this.w = 1;
        return;
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.w = 1;
        int v2 = this.y.size();
        int v1_1 = 0;
        while (v1_1 < v2) {
            ((android.support.v4.widget.dc) this.y.get(v1_1)).run();
            v1_1++;
        }
        this.y.clear();
        return;
    }

    public boolean onInterceptTouchEvent(android.view.MotionEvent p7)
    {
        int v2 = 0;
        boolean v3_0 = android.support.v4.view.bk.a(p7);
        if ((!this.k) && ((!v3_0) && (this.getChildCount() > 1))) {
            android.support.v4.widget.eg v0_2 = this.getChildAt(1);
            if (v0_2 != null) {
                android.support.v4.widget.eg v0_4;
                if (android.support.v4.widget.eg.b(v0_2, ((int) p7.getX()), ((int) p7.getY()))) {
                    v0_4 = 0;
                } else {
                    v0_4 = 1;
                }
                this.v = v0_4;
            }
        }
        if ((this.k) && ((!this.p) || (!v3_0))) {
            if ((v3_0 != 3) && (v3_0 != 1)) {
                android.support.v4.widget.eg v0_18;
                switch (v3_0) {
                    case 0:
                        this.p = 0;
                        android.support.v4.widget.eg v0_13 = p7.getX();
                        boolean v3_4 = p7.getY();
                        this.r = v0_13;
                        this.s = v3_4;
                        if ((android.support.v4.widget.eg.b(this.l, ((int) v0_13), ((int) v3_4))) && (this.b(this.l))) {
                            v0_18 = 1;
                        } else {
                            v0_18 = 0;
                        }
                        break;
                    case 2:
                        android.support.v4.widget.eg v0_8 = p7.getX();
                        boolean v3_1 = p7.getY();
                        android.support.v4.widget.eg v0_10 = Math.abs((v0_8 - this.r));
                        boolean v3_3 = Math.abs((v3_1 - this.s));
                        if ((v0_10 <= ((float) this.u.n)) || (v3_3 <= v0_10)) {
                        } else {
                            this.u.a();
                            this.p = 1;
                        }
                        break;
                    default:
                }
                if ((this.u.a(p7)) || (v0_18 != null)) {
                    v2 = 1;
                }
            } else {
                this.u.a();
            }
        } else {
            this.u.a();
            v2 = super.onInterceptTouchEvent(p7);
        }
        return v2;
    }

    protected void onLayout(boolean p18, int p19, int p20, int p21, int p22)
    {
        boolean v9 = this.n();
        if (!v9) {
            this.u.u = 1;
        } else {
            this.u.u = 2;
        }
        boolean v6;
        int v10 = (p21 - p19);
        if (!v9) {
            v6 = this.getPaddingLeft();
        } else {
            v6 = this.getPaddingRight();
        }
        android.view.View v2_2;
        if (!v9) {
            v2_2 = this.getPaddingRight();
        } else {
            v2_2 = this.getPaddingLeft();
        }
        int v11 = this.getPaddingTop();
        int v12 = this.getChildCount();
        if (this.w) {
            if ((!this.k) || (!this.v)) {
                int v1_7 = 0;
            } else {
                v1_7 = 1065353216;
            }
            this.m = v1_7;
        }
        int v8 = 0;
        int v7 = v6;
        while (v8 < v12) {
            int v1_22;
            int v3_3;
            android.view.View v13 = this.getChildAt(v8);
            if (v13.getVisibility() == 8) {
                v1_22 = v6;
                v3_3 = v7;
            } else {
                int v4_1;
                int v1_24 = ((android.support.v4.widget.de) v13.getLayoutParams());
                int v14 = v13.getMeasuredWidth();
                int v5_0 = 0;
                if (!v1_24.b) {
                    if ((!this.k) || (this.q == 0)) {
                        int v1_27 = 0;
                    } else {
                        v1_27 = ((int) ((1065353216 - this.m) * ((float) this.q)));
                    }
                    v5_0 = v1_27;
                    v4_1 = v6;
                } else {
                    int v3_10;
                    int v15_1 = ((Math.min(v6, ((v10 - v2_2) - this.j)) - v7) - (v1_24.leftMargin + v1_24.rightMargin));
                    this.o = v15_1;
                    if (!v9) {
                        v3_10 = v1_24.leftMargin;
                    } else {
                        v3_10 = v1_24.rightMargin;
                    }
                    int v4_10;
                    if ((((v7 + v3_10) + v15_1) + (v14 / 2)) <= (v10 - v2_2)) {
                        v4_10 = 0;
                    } else {
                        v4_10 = 1;
                    }
                    v1_24.c = v4_10;
                    int v1_33 = ((int) (((float) v15_1) * this.m));
                    v4_1 = (v7 + (v3_10 + v1_33));
                    this.m = (((float) v1_33) / ((float) this.o));
                }
                int v3_14;
                int v1_36;
                if (!v9) {
                    v1_36 = (v4_1 - v5_0);
                    v3_14 = (v1_36 + v14);
                } else {
                    v3_14 = ((v10 - v4_1) + v5_0);
                    v1_36 = (v3_14 - v14);
                }
                v13.layout(v1_36, v11, v3_14, (v13.getMeasuredHeight() + v11));
                v1_22 = (v13.getWidth() + v6);
                v3_3 = v4_1;
            }
            v8++;
            v6 = v1_22;
            v7 = v3_3;
        }
        if (this.w) {
            if (!this.k) {
                int v1_11 = 0;
                while (v1_11 < v12) {
                    this.a(this.getChildAt(v1_11), 0, this.e);
                    v1_11++;
                }
            } else {
                if (this.q != 0) {
                    this.b(this.m);
                }
                if (((android.support.v4.widget.de) this.l.getLayoutParams()).c) {
                    this.a(this.l, this.m, this.e);
                }
            }
            this.a(this.l);
        }
        this.w = 0;
        return;
    }

    protected void onMeasure(int p18, int p19)
    {
        int v10_0;
        int v12;
        int v3_1;
        int v4_0 = android.view.View$MeasureSpec.getMode(p18);
        int v3_0 = android.view.View$MeasureSpec.getSize(p18);
        int v1_0 = android.view.View$MeasureSpec.getMode(p19);
        int v2_0 = android.view.View$MeasureSpec.getSize(p19);
        if (v4_0 == 1073741824) {
            if (v1_0 != 0) {
                v10_0 = v1_0;
                v12 = v3_0;
                v3_1 = v2_0;
            } else {
                if (!this.isInEditMode()) {
                    throw new IllegalStateException("Height must not be UNSPECIFIED");
                } else {
                    if (v1_0 != 0) {
                    } else {
                        v10_0 = -2147483648;
                        v12 = v3_0;
                        v3_1 = 300;
                    }
                }
            }
        } else {
            if (!this.isInEditMode()) {
                throw new IllegalStateException("Width must have an exact value or MATCH_PARENT");
            } else {
                if ((v4_0 == -2147483648) || (v4_0 != 0)) {
                } else {
                    v10_0 = v1_0;
                    v12 = 300;
                    v3_1 = v2_0;
                }
            }
        }
        int v1_9;
        int v2_6;
        switch (v10_0) {
            case -2147483648:
                v1_9 = 0;
                v2_6 = ((v3_1 - this.getPaddingTop()) - this.getPaddingBottom());
                break;
            case 1073741824:
                v1_9 = ((v3_1 - this.getPaddingTop()) - this.getPaddingBottom());
                v2_6 = v1_9;
                break;
            default:
                v1_9 = 0;
                v2_6 = -1;
        }
        int v7 = 0;
        int v11 = ((v12 - this.getPaddingLeft()) - this.getPaddingRight());
        int v13 = this.getChildCount();
        if (v13 > 2) {
            android.util.Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
        }
        this.l = 0;
        float v9_0 = 0;
        int v6_0 = v11;
        int v8 = v1_9;
        int v3_10 = 0;
        while (v9_0 < v13) {
            int v1_43;
            int v4_38;
            int v5_13;
            int v3_11;
            int v14_1 = this.getChildAt(v9_0);
            int v1_42 = ((android.support.v4.widget.de) v14_1.getLayoutParams());
            if (v14_1.getVisibility() != 8) {
                if (v1_42.a > 0) {
                    v3_10 += v1_42.a;
                    if (v1_42.width == 0) {
                        v1_43 = v6_0;
                        v4_38 = v8;
                        v5_13 = v3_10;
                        v3_11 = v7;
                        v9_0++;
                        v7 = v3_11;
                        v8 = v4_38;
                        v6_0 = v1_43;
                        v3_10 = v5_13;
                    }
                }
                int v4_42;
                int v4_40 = (v1_42.leftMargin + v1_42.rightMargin);
                if (v1_42.width != -2) {
                    if (v1_42.width != -1) {
                        v4_42 = android.view.View$MeasureSpec.makeMeasureSpec(v1_42.width, 1073741824);
                    } else {
                        v4_42 = android.view.View$MeasureSpec.makeMeasureSpec((v11 - v4_40), 1073741824);
                    }
                } else {
                    v4_42 = android.view.View$MeasureSpec.makeMeasureSpec((v11 - v4_40), -2147483648);
                }
                int v5_23;
                if (v1_42.height != -2) {
                    if (v1_42.height != -1) {
                        v5_23 = android.view.View$MeasureSpec.makeMeasureSpec(v1_42.height, 1073741824);
                    } else {
                        v5_23 = android.view.View$MeasureSpec.makeMeasureSpec(v2_6, 1073741824);
                    }
                } else {
                    v5_23 = android.view.View$MeasureSpec.makeMeasureSpec(v2_6, -2147483648);
                }
                v14_1.measure(v4_42, v5_23);
                int v4_45 = v14_1.getMeasuredWidth();
                int v5_26 = v14_1.getMeasuredHeight();
                if ((v10_0 == -2147483648) && (v5_26 > v8)) {
                    v8 = Math.min(v5_26, v2_6);
                }
                int v4_46;
                int v5_27 = (v6_0 - v4_45);
                if (v5_27 >= 0) {
                    v4_46 = 0;
                } else {
                    v4_46 = 1;
                }
                v1_42.b = v4_46;
                int v4_47 = (v4_46 | v7);
                if (v1_42.b) {
                    this.l = v14_1;
                }
                v1_43 = v5_27;
                v5_13 = v3_10;
                v3_11 = v4_47;
                v4_38 = v8;
            } else {
                v1_42.c = 0;
                v1_43 = v6_0;
                v4_38 = v8;
                v5_13 = v3_10;
                v3_11 = v7;
            }
        }
        if ((v7 != 0) || (v3_10 > 0)) {
            int v14_0 = (v11 - this.j);
            int v10_1 = 0;
            while (v10_1 < v13) {
                int v15_0 = this.getChildAt(v10_1);
                if (v15_0.getVisibility() != 8) {
                    int v1_25 = ((android.support.v4.widget.de) v15_0.getLayoutParams());
                    if (v15_0.getVisibility() != 8) {
                        if ((v1_25.width != 0) || (v1_25.a <= 0)) {
                            float v9_1 = 0;
                        } else {
                            v9_1 = 1;
                        }
                        int v5_6;
                        if (v9_1 == 0) {
                            v5_6 = v15_0.getMeasuredWidth();
                        } else {
                            v5_6 = 0;
                        }
                        if ((v7 == 0) || (v15_0 == this.l)) {
                            if (v1_25.a > 0) {
                                int v4_18;
                                if (v1_25.width != 0) {
                                    v4_18 = android.view.View$MeasureSpec.makeMeasureSpec(v15_0.getMeasuredHeight(), 1073741824);
                                } else {
                                    if (v1_25.height != -2) {
                                        if (v1_25.height != -1) {
                                            v4_18 = android.view.View$MeasureSpec.makeMeasureSpec(v1_25.height, 1073741824);
                                        } else {
                                            v4_18 = android.view.View$MeasureSpec.makeMeasureSpec(v2_6, 1073741824);
                                        }
                                    } else {
                                        v4_18 = android.view.View$MeasureSpec.makeMeasureSpec(v2_6, -2147483648);
                                    }
                                }
                                if (v7 == 0) {
                                    v15_0.measure(android.view.View$MeasureSpec.makeMeasureSpec((((int) ((v1_25.a * ((float) Math.max(0, v6_0))) / v3_10)) + v5_6), 1073741824), v4_18);
                                } else {
                                    int v1_34 = (v11 - (v1_25.rightMargin + v1_25.leftMargin));
                                    float v9_12 = android.view.View$MeasureSpec.makeMeasureSpec(v1_34, 1073741824);
                                    if (v5_6 != v1_34) {
                                        v15_0.measure(v9_12, v4_18);
                                    }
                                }
                            }
                        } else {
                            if ((v1_25.width < 0) && ((v5_6 > v14_0) || (v1_25.a > 0))) {
                                int v1_36;
                                if (v9_1 == 0) {
                                    v1_36 = android.view.View$MeasureSpec.makeMeasureSpec(v15_0.getMeasuredHeight(), 1073741824);
                                } else {
                                    if (v1_25.height != -2) {
                                        if (v1_25.height != -1) {
                                            v1_36 = android.view.View$MeasureSpec.makeMeasureSpec(v1_25.height, 1073741824);
                                        } else {
                                            v1_36 = android.view.View$MeasureSpec.makeMeasureSpec(v2_6, 1073741824);
                                        }
                                    } else {
                                        v1_36 = android.view.View$MeasureSpec.makeMeasureSpec(v2_6, -2147483648);
                                    }
                                }
                                v15_0.measure(android.view.View$MeasureSpec.makeMeasureSpec(v14_0, 1073741824), v1_36);
                            }
                        }
                    }
                }
                v10_1++;
            }
        }
        this.setMeasuredDimension(v12, ((this.getPaddingTop() + v8) + this.getPaddingBottom()));
        this.k = v7;
        if ((this.u.m != 0) && (v7 == 0)) {
            this.u.b();
        }
        return;
    }

    protected void onRestoreInstanceState(android.os.Parcelable p2)
    {
        super.onRestoreInstanceState(((android.support.v4.widget.SlidingPaneLayout$SavedState) p2).getSuperState());
        if (!((android.support.v4.widget.SlidingPaneLayout$SavedState) p2).a) {
            this.e();
        } else {
            this.h();
        }
        this.v = ((android.support.v4.widget.SlidingPaneLayout$SavedState) p2).a;
        return;
    }

    protected android.os.Parcelable onSaveInstanceState()
    {
        int v0_2;
        android.support.v4.widget.SlidingPaneLayout$SavedState v1_1 = new android.support.v4.widget.SlidingPaneLayout$SavedState(super.onSaveInstanceState());
        if (!this.k) {
            v0_2 = this.v;
        } else {
            if ((this.k) && (this.m != 1065353216)) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
        }
        v1_1.a = v0_2;
        return v1_1;
    }

    protected void onSizeChanged(int p2, int p3, int p4, int p5)
    {
        super.onSizeChanged(p2, p3, p4, p5);
        if (p2 != p4) {
            this.w = 1;
        }
        return;
    }

    public boolean onTouchEvent(android.view.MotionEvent p6)
    {
        boolean v0_10;
        if (this.k) {
            this.u.b(p6);
            switch ((p6.getAction() & 255)) {
                case 0:
                    boolean v0_9 = p6.getX();
                    int v1_2 = p6.getY();
                    this.r = v0_9;
                    this.s = v1_2;
                    break;
                case 1:
                    if (!this.b(this.l)) {
                    } else {
                        boolean v0_6 = p6.getX();
                        int v1_0 = p6.getY();
                        if (((((v0_6 - this.r) * (v0_6 - this.r)) + ((v1_0 - this.s) * (v1_0 - this.s))) >= ((float) (this.u.n * this.u.n))) || (!android.support.v4.widget.eg.b(this.l, ((int) v0_6), ((int) v1_0)))) {
                        } else {
                            this.e();
                        }
                    }
                    break;
            }
            v0_10 = 1;
        } else {
            v0_10 = super.onTouchEvent(p6);
        }
        return v0_10;
    }

    public void requestChildFocus(android.view.View p2, android.view.View p3)
    {
        super.requestChildFocus(p2, p3);
        if ((!this.isInTouchMode()) && (!this.k)) {
            int v0_3;
            if (p2 != this.l) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            this.v = v0_3;
        }
        return;
    }

    public void setCoveredFadeColor(int p1)
    {
        this.g = p1;
        return;
    }

    public void setPanelSlideListener(android.support.v4.widget.df p1)
    {
        this.t = p1;
        return;
    }

    public void setParallaxDistance(int p1)
    {
        this.q = p1;
        this.requestLayout();
        return;
    }

    public void setShadowDrawable(android.graphics.drawable.Drawable p1)
    {
        this.setShadowDrawableLeft(p1);
        return;
    }

    public void setShadowDrawableLeft(android.graphics.drawable.Drawable p1)
    {
        this.h = p1;
        return;
    }

    public void setShadowDrawableRight(android.graphics.drawable.Drawable p1)
    {
        this.i = p1;
        return;
    }

    public void setShadowResource(int p2)
    {
        this.setShadowDrawable(this.getResources().getDrawable(p2));
        return;
    }

    public void setShadowResourceLeft(int p2)
    {
        this.setShadowDrawableLeft(this.getResources().getDrawable(p2));
        return;
    }

    public void setShadowResourceRight(int p2)
    {
        this.setShadowDrawableRight(this.getResources().getDrawable(p2));
        return;
    }

    public void setSliderFadeColor(int p1)
    {
        this.e = p1;
        return;
    }
}
