package android.support.v4.widget;
final class bb extends android.graphics.drawable.Drawable implements android.graphics.drawable.Animatable {
    private static final float A = 1061997773;
    static final int a = 0;
    static final int b = 1;
    private static final android.view.animation.Interpolator e = None;
    private static final android.view.animation.Interpolator f = None;
    private static final float g = 17543;
    private static final int h = 40;
    private static final float i = 16652;
    private static final float j = 16416;
    private static final int k = 56;
    private static final float l = 16712;
    private static final float m = 16448;
    private static final float o = 16192;
    private static final float p = 63;
    private static final float q = 63;
    private static final int r = 1332;
    private static final float s = 16544;
    private static final int v = 10;
    private static final int w = 5;
    private static final float x = 16544;
    private static final int y = 12;
    private static final int z = 6;
    private android.content.res.Resources B;
    private android.view.View C;
    private android.view.animation.Animation D;
    private float E;
    private double F;
    private double G;
    private final android.graphics.drawable.Drawable$Callback H;
    final android.support.v4.widget.bg c;
    boolean d;
    private final int[] n;
    private final java.util.ArrayList t;
    private float u;

    static bb()
    {
        android.support.v4.widget.bb.e = new android.view.animation.LinearInterpolator();
        android.support.v4.widget.bb.f = new android.support.v4.view.b.b();
        return;
    }

    public bb(android.content.Context p5, android.view.View p6)
    {
        android.support.v4.widget.bg v0_0 = new int[1];
        v0_0[0] = -16777216;
        this.n = v0_0;
        this.t = new java.util.ArrayList();
        this.H = new android.support.v4.widget.be(this);
        this.C = p6;
        this.B = p5.getResources();
        this.c = new android.support.v4.widget.bg(this.H);
        this.c.a(this.n);
        this.a(1);
        android.support.v4.widget.bg v0_9 = this.c;
        android.support.v4.widget.bc v1_4 = new android.support.v4.widget.bc(this, v0_9);
        v1_4.setRepeatCount(-1);
        v1_4.setRepeatMode(1);
        v1_4.setInterpolator(android.support.v4.widget.bb.e);
        v1_4.setAnimationListener(new android.support.v4.widget.bd(this, v0_9));
        this.D = v1_4;
        return;
    }

    static synthetic float a(android.support.v4.widget.bb p1)
    {
        return p1.E;
    }

    static synthetic float a(android.support.v4.widget.bb p0, float p1)
    {
        p0.E = p1;
        return p1;
    }

    static synthetic float a(android.support.v4.widget.bg p1)
    {
        return android.support.v4.widget.bb.b(p1);
    }

    private static int a(float p8, int p9, int p10)
    {
        int v0_1 = Integer.valueOf(p9).intValue();
        int v1_1 = ((v0_1 >> 24) & 255);
        int v2_1 = ((v0_1 >> 16) & 255);
        int v3_1 = ((v0_1 >> 8) & 255);
        int v0_2 = (v0_1 & 255);
        int v4_1 = Integer.valueOf(p10).intValue();
        return ((v0_2 + ((int) (((float) ((v4_1 & 255) - v0_2)) * p8))) | ((((v1_1 + ((int) (((float) (((v4_1 >> 24) & 255) - v1_1)) * p8))) << 24) | ((v2_1 + ((int) (((float) (((v4_1 >> 16) & 255) - v2_1)) * p8))) << 16)) | ((((int) (((float) (((v4_1 >> 8) & 255) - v3_1)) * p8)) + v3_1) << 8)));
    }

    static synthetic android.view.animation.Interpolator a()
    {
        return android.support.v4.widget.bb.f;
    }

    private void a(double p8, double p10, double p12, double p14, float p16, float p17)
    {
        float v0_9;
        android.support.v4.widget.bg v1 = this.c;
        float v0_2 = this.B.getDisplayMetrics().density;
        this.F = (((double) v0_2) * p8);
        this.G = (((double) v0_2) * p10);
        double v2_5 = (((float) p14) * v0_2);
        v1.g = v2_5;
        v1.b.setStrokeWidth(v2_5);
        v1.d();
        v1.q = (((double) v0_2) * p12);
        v1.a(0);
        float v0_3 = (v0_2 * p17);
        v1.r = ((int) (p16 * v0_2));
        v1.s = ((int) v0_3);
        float v0_7 = ((float) Math.min(((int) this.F), ((int) this.G)));
        if ((v1.q > 0) && (v0_7 >= 0)) {
            v0_9 = ((float) (((double) (v0_7 / 1073741824)) - v1.q));
        } else {
            v0_9 = ((float) Math.ceil(((double) (v1.g / 1073741824))));
        }
        v1.h = v0_9;
        return;
    }

    static synthetic void a(float p4, android.support.v4.widget.bg p5)
    {
        android.support.v4.widget.bb.c(p4, p5);
        float v0_5 = ((float) (Math.floor(((double) (p5.m / 1061997773))) + 1.0));
        p5.a(((((p5.l - android.support.v4.widget.bb.b(p5)) - p5.k) * p4) + p5.k));
        p5.b(p5.l);
        p5.c((((v0_5 - p5.m) * p4) + p5.m));
        return;
    }

    private varargs void a(int[] p3)
    {
        this.c.a(p3);
        this.c.a(0);
        return;
    }

    private float b()
    {
        return this.u;
    }

    private static float b(android.support.v4.widget.bg p6)
    {
        return ((float) Math.toRadians((((double) p6.g) / (6.283185307179586 * p6.q))));
    }

    static synthetic void b(float p0, android.support.v4.widget.bg p1)
    {
        android.support.v4.widget.bb.c(p0, p1);
        return;
    }

    private void c()
    {
        android.support.v4.widget.bg v0 = this.c;
        android.support.v4.widget.bc v1_1 = new android.support.v4.widget.bc(this, v0);
        v1_1.setRepeatCount(-1);
        v1_1.setRepeatMode(1);
        v1_1.setInterpolator(android.support.v4.widget.bb.e);
        v1_1.setAnimationListener(new android.support.v4.widget.bd(this, v0));
        this.D = v1_1;
        return;
    }

    private static void c(float p9, android.support.v4.widget.bg p10)
    {
        if (p9 > 1061158912) {
            int v0_2 = ((p9 - 1061158912) / 1048576000);
            float v2_2 = p10.i[p10.a()];
            int v1_5 = Integer.valueOf(p10.i[p10.j]).intValue();
            int v3_2 = ((v1_5 >> 24) & 255);
            int v4_1 = ((v1_5 >> 16) & 255);
            int v5_1 = ((v1_5 >> 8) & 255);
            int v1_6 = (v1_5 & 255);
            float v2_4 = Integer.valueOf(v2_2).intValue();
            p10.w = ((((int) (v0_2 * ((float) ((v2_4 & 255) - v1_6)))) + v1_6) | ((((v3_2 + ((int) (((float) (((v2_4 >> 24) & 255) - v3_2)) * v0_2))) << 24) | ((v4_1 + ((int) (((float) (((v2_4 >> 16) & 255) - v4_1)) * v0_2))) << 16)) | ((((int) (((float) (((v2_4 >> 8) & 255) - v5_1)) * v0_2)) + v5_1) << 8)));
        }
        return;
    }

    private void d(float p2)
    {
        this.c.c(p2);
        return;
    }

    private static void d(float p4, android.support.v4.widget.bg p5)
    {
        android.support.v4.widget.bb.c(p4, p5);
        float v0_5 = ((float) (Math.floor(((double) (p5.m / 1061997773))) + 1.0));
        p5.a(((((p5.l - android.support.v4.widget.bb.b(p5)) - p5.k) * p4) + p5.k));
        p5.b(p5.l);
        p5.c((((v0_5 - p5.m) * p4) + p5.m));
        return;
    }

    public final void a(float p3)
    {
        android.support.v4.widget.bg v0 = this.c;
        if (p3 != v0.p) {
            v0.p = p3;
            v0.d();
        }
        return;
    }

    public final void a(int p15)
    {
        if (p15 != 0) {
            this.a(40.0, 40.0, 8.75, 2.5, 1092616192, 1084227584);
        } else {
            this.a(56.0, 56.0, 12.5, 3.0, 1094713344, 1086324736);
        }
        return;
    }

    public final void a(boolean p2)
    {
        this.c.a(p2);
        return;
    }

    public final void b(float p3)
    {
        this.c.a(0);
        this.c.b(p3);
        return;
    }

    public final void b(int p2)
    {
        this.c.v = p2;
        return;
    }

    final void c(float p1)
    {
        this.u = p1;
        this.invalidateSelf();
        return;
    }

    public final void draw(android.graphics.Canvas p13)
    {
        android.graphics.Rect v6 = this.getBounds();
        int v7 = p13.save();
        p13.rotate(this.u, v6.exactCenterX(), v6.exactCenterY());
        android.support.v4.widget.bg v8 = this.c;
        android.graphics.Paint v1_1 = v8.a;
        v1_1.set(v6);
        v1_1.inset(v8.h, v8.h);
        float v2_4 = (1135869952 * (v8.d + v8.f));
        android.graphics.Paint v3_2 = (((v8.e + v8.f) * 1135869952) - v2_4);
        v8.b.setColor(v8.w);
        p13.drawArc(v1_1, v2_4, v3_2, 0, v8.b);
        if (v8.n) {
            if (v8.o != null) {
                v8.o.reset();
            } else {
                v8.o = new android.graphics.Path();
                v8.o.setFillType(android.graphics.Path$FillType.EVEN_ODD);
            }
            android.graphics.Path v0_19 = (((float) (((int) v8.h) / 2)) * v8.p);
            android.graphics.Paint v1_5 = ((float) ((v8.q * Math.cos(0)) + ((double) v6.exactCenterX())));
            float v4_8 = ((float) ((v8.q * Math.sin(0)) + ((double) v6.exactCenterY())));
            v8.o.moveTo(0, 0);
            v8.o.lineTo((((float) v8.r) * v8.p), 0);
            v8.o.lineTo(((((float) v8.r) * v8.p) / 1073741824), (((float) v8.s) * v8.p));
            v8.o.offset((v1_5 - v0_19), v4_8);
            v8.o.close();
            v8.c.setColor(v8.w);
            p13.rotate(((v2_4 + v3_2) - 1084227584), v6.exactCenterX(), v6.exactCenterY());
            p13.drawPath(v8.o, v8.c);
        }
        if (v8.t < 255) {
            v8.u.setColor(v8.v);
            v8.u.setAlpha((255 - v8.t));
            p13.drawCircle(v6.exactCenterX(), v6.exactCenterY(), ((float) (v6.width() / 2)), v8.u);
        }
        p13.restoreToCount(v7);
        return;
    }

    public final int getAlpha()
    {
        return this.c.t;
    }

    public final int getIntrinsicHeight()
    {
        return ((int) this.G);
    }

    public final int getIntrinsicWidth()
    {
        return ((int) this.F);
    }

    public final int getOpacity()
    {
        return -3;
    }

    public final boolean isRunning()
    {
        java.util.ArrayList v3 = this.t;
        int v4 = v3.size();
        int v2 = 0;
        while (v2 < v4) {
            int v0_2 = ((android.view.animation.Animation) v3.get(v2));
            if ((!v0_2.hasStarted()) || (v0_2.hasEnded())) {
                v2++;
            } else {
                int v0_0 = 1;
            }
            return v0_0;
        }
        v0_0 = 0;
        return v0_0;
    }

    public final void setAlpha(int p2)
    {
        this.c.t = p2;
        return;
    }

    public final void setColorFilter(android.graphics.ColorFilter p3)
    {
        android.support.v4.widget.bg v0 = this.c;
        v0.b.setColorFilter(p3);
        v0.d();
        return;
    }

    public final void start()
    {
        this.D.reset();
        this.c.b();
        if (this.c.e == this.c.d) {
            this.c.a(0);
            this.c.c();
            this.D.setDuration(1332);
            this.C.startAnimation(this.D);
        } else {
            this.d = 1;
            this.D.setDuration(666);
            this.C.startAnimation(this.D);
        }
        return;
    }

    public final void stop()
    {
        this.C.clearAnimation();
        this.c(0);
        this.c.a(0);
        this.c.a(0);
        this.c.c();
        return;
    }
}
