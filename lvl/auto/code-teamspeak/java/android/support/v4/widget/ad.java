package android.support.v4.widget;
public final class ad extends android.view.ViewGroup$MarginLayoutParams {
    public int a;
    float b;
    boolean c;
    boolean d;

    public ad(int p2, int p3)
    {
        this(p2, p3);
        this.a = 0;
        return;
    }

    private ad(int p1, int p2, int p3)
    {
        this(p1, p2);
        this.a = p3;
        return;
    }

    public ad(android.content.Context p3, android.util.AttributeSet p4)
    {
        this(p3, p4);
        this.a = 0;
        android.content.res.TypedArray v0_1 = p3.obtainStyledAttributes(p4, android.support.v4.widget.DrawerLayout.e());
        this.a = v0_1.getInt(0, 0);
        v0_1.recycle();
        return;
    }

    public ad(android.support.v4.widget.ad p2)
    {
        this(p2);
        this.a = 0;
        this.a = p2.a;
        return;
    }

    public ad(android.view.ViewGroup$LayoutParams p2)
    {
        this(p2);
        this.a = 0;
        return;
    }

    public ad(android.view.ViewGroup$MarginLayoutParams p2)
    {
        this(p2);
        this.a = 0;
        return;
    }
}
