package android.support.v4.widget;
final class bv {
    private static final String a = "PopupWindowCompatApi21";
    private static reflect.Field b;

    static bv()
    {
        try {
            NoSuchFieldException v0_1 = android.widget.PopupWindow.getDeclaredField("mOverlapAnchor");
            android.support.v4.widget.bv.b = v0_1;
            v0_1.setAccessible(1);
        } catch (NoSuchFieldException v0_2) {
            android.util.Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", v0_2);
        }
        return;
    }

    bv()
    {
        return;
    }

    static void a(android.widget.PopupWindow p3, boolean p4)
    {
        if (android.support.v4.widget.bv.b != null) {
            try {
                android.support.v4.widget.bv.b.set(p3, Boolean.valueOf(p4));
            } catch (IllegalAccessException v0_2) {
                android.util.Log.i("PopupWindowCompatApi21", "Could not set overlap anchor field in PopupWindow", v0_2);
            }
        }
        return;
    }

    static boolean a(android.widget.PopupWindow p3)
    {
        IllegalAccessException v0_4;
        if (android.support.v4.widget.bv.b == null) {
            v0_4 = 0;
        } else {
            try {
                v0_4 = ((Boolean) android.support.v4.widget.bv.b.get(p3)).booleanValue();
            } catch (IllegalAccessException v0_5) {
                android.util.Log.i("PopupWindowCompatApi21", "Could not get overlap anchor field in PopupWindow", v0_5);
            }
        }
        return v0_4;
    }
}
