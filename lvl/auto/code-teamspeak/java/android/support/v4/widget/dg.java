package android.support.v4.widget;
final class dg implements android.os.Parcelable$Creator {

    dg()
    {
        return;
    }

    private static android.support.v4.widget.SlidingPaneLayout$SavedState a(android.os.Parcel p2)
    {
        return new android.support.v4.widget.SlidingPaneLayout$SavedState(p2, 0);
    }

    private static android.support.v4.widget.SlidingPaneLayout$SavedState[] a(int p1)
    {
        android.support.v4.widget.SlidingPaneLayout$SavedState[] v0 = new android.support.v4.widget.SlidingPaneLayout$SavedState[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p3)
    {
        return new android.support.v4.widget.SlidingPaneLayout$SavedState(p3, 0);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.widget.SlidingPaneLayout$SavedState[] v0 = new android.support.v4.widget.SlidingPaneLayout$SavedState[p2];
        return v0;
    }
}
