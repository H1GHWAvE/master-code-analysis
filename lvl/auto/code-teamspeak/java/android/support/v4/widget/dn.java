package android.support.v4.widget;
public class dn extends android.view.ViewGroup implements android.support.v4.view.bt, android.support.v4.view.bv {
    private static final int[] M = None;
    public static final int a = 0;
    public static final int b = 1;
    private static final String e = "None";
    private static final int f = 255;
    private static final int g = 76;
    private static final int h = 40;
    private static final int i = 56;
    private static final float j = 64;
    private static final int k = 255;
    private static final float l = 63;
    private static final float m = 1061997773;
    private static final int n = 150;
    private static final int o = 300;
    private static final int p = 200;
    private static final int q = 200;
    private static final int r = 16448250;
    private static final int s = 64;
    private final android.support.v4.view.bu A;
    private final int[] B;
    private int C;
    private int D;
    private boolean E;
    private float F;
    private float G;
    private boolean H;
    private int I;
    private boolean J;
    private boolean K;
    private final android.view.animation.DecelerateInterpolator L;
    private android.support.v4.widget.e N;
    private int O;
    private float P;
    private android.support.v4.widget.bb Q;
    private android.view.animation.Animation R;
    private android.view.animation.Animation S;
    private android.view.animation.Animation T;
    private android.view.animation.Animation U;
    private android.view.animation.Animation V;
    private float W;
    private boolean aa;
    private int ab;
    private int ac;
    private boolean ad;
    private android.view.animation.Animation$AnimationListener ae;
    private final android.view.animation.Animation af;
    private final android.view.animation.Animation ag;
    private final android.view.animation.Animation ah;
    protected int c;
    protected int d;
    private android.view.View t;
    private android.support.v4.widget.dx u;
    private boolean v;
    private int w;
    private float x;
    private float y;
    private final android.support.v4.view.bw z;

    static dn()
    {
        android.support.v4.widget.dn.e = android.support.v4.widget.dn.getSimpleName();
        int[] v0_3 = new int[1];
        v0_3[0] = 16842766;
        android.support.v4.widget.dn.M = v0_3;
        return;
    }

    private dn(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private dn(android.content.Context p7, byte p8)
    {
        this(p7, 0);
        this.v = 0;
        this.x = -1082130432;
        android.support.v4.view.bu v0_2 = new int[2];
        this.B = v0_2;
        this.E = 0;
        this.I = -1;
        this.O = -1;
        this.ae = new android.support.v4.widget.do(this);
        this.af = new android.support.v4.widget.dt(this);
        this.ag = new android.support.v4.widget.du(this);
        this.ah = new android.support.v4.widget.dv(this);
        this.w = android.view.ViewConfiguration.get(p7).getScaledTouchSlop();
        this.C = this.getResources().getInteger(17694721);
        this.setWillNotDraw(0);
        this.L = new android.view.animation.DecelerateInterpolator(1073741824);
        android.support.v4.view.bu v0_18 = p7.obtainStyledAttributes(0, android.support.v4.widget.dn.M);
        this.setEnabled(v0_18.getBoolean(0, 1));
        v0_18.recycle();
        android.support.v4.view.bu v0_20 = this.getResources().getDisplayMetrics();
        this.ab = ((int) (v0_20.density * 1109393408));
        this.ac = ((int) (v0_20.density * 1109393408));
        this.N = new android.support.v4.widget.e(this.getContext());
        this.Q = new android.support.v4.widget.bb(this.getContext(), this);
        this.Q.b(-328966);
        this.N.setImageDrawable(this.Q);
        this.N.setVisibility(8);
        this.addView(this.N);
        android.support.v4.view.cx.a(this);
        this.W = (v0_20.density * 1115684864);
        this.x = this.W;
        this.z = new android.support.v4.view.bw(this);
        this.A = new android.support.v4.view.bu(this);
        this.setNestedScrollingEnabled(1);
        return;
    }

    private static float a(android.view.MotionEvent p1, int p2)
    {
        float v0_1;
        float v0_0 = android.support.v4.view.bk.a(p1, p2);
        if (v0_0 >= 0) {
            v0_1 = android.support.v4.view.bk.d(p1, v0_0);
        } else {
            v0_1 = -1082130432;
        }
        return v0_1;
    }

    static synthetic int a(android.support.v4.widget.dn p0, int p1)
    {
        p0.D = p1;
        return p1;
    }

    private android.view.animation.Animation a(int p5, int p6)
    {
        android.support.v4.widget.dr v0_0 = 0;
        if ((!this.J) || (!android.support.v4.widget.dn.b())) {
            android.support.v4.widget.dr v1_3 = new android.support.v4.widget.dr(this, p5, p6);
            v1_3.setDuration(300);
            this.N.a = 0;
            this.N.clearAnimation();
            this.N.startAnimation(v1_3);
            v0_0 = v1_3;
        }
        return v0_0;
    }

    private void a()
    {
        this.N = new android.support.v4.widget.e(this.getContext());
        this.Q = new android.support.v4.widget.bb(this.getContext(), this);
        this.Q.b(-328966);
        this.N.setImageDrawable(this.Q);
        this.N.setVisibility(8);
        this.addView(this.N);
        return;
    }

    private void a(float p15)
    {
        int v0_8;
        this.Q.a(1);
        android.view.animation.Animation v1_0 = Math.min(1065353216, Math.abs((p15 / this.x)));
        android.support.v4.widget.bg v2_5 = ((((float) Math.max((((double) v1_0) - 0.4), 0)) * 1084227584) / 1077936128);
        float v3_1 = (Math.abs(p15) - this.x);
        if (!this.ad) {
            v0_8 = this.W;
        } else {
            v0_8 = (this.W - ((float) this.d));
        }
        float v3_4 = Math.max(0, (Math.min(v3_1, (v0_8 * 1073741824)) / v0_8));
        float v3_7 = (((float) (((double) (v3_4 / 1082130432)) - Math.pow(((double) (v3_4 / 1082130432)), 2.0))) * 1073741824);
        int v0_13 = (((int) ((v0_8 * v1_0) + ((v0_8 * v3_7) * 1073741824))) + this.d);
        if (this.N.getVisibility() != 0) {
            this.N.setVisibility(0);
        }
        if (!this.J) {
            android.support.v4.view.cx.d(this.N, 1065353216);
            android.support.v4.view.cx.e(this.N, 1065353216);
        }
        if (p15 >= this.x) {
            if ((this.Q.getAlpha() < 255) && (!android.support.v4.widget.dn.a(this.U))) {
                this.U = this.a(this.Q.getAlpha(), 255);
            }
        } else {
            if (this.J) {
                this.setAnimationProgress((p15 / this.x));
            }
            if ((this.Q.getAlpha() > 76) && (!android.support.v4.widget.dn.a(this.T))) {
                this.T = this.a(this.Q.getAlpha(), 76);
            }
            this.Q.b(Math.min(1061997773, (v2_5 * 1061997773)));
            this.Q.a(Math.min(1065353216, v2_5));
        }
        this.Q.c.c((((-1098907648 + (v2_5 * 1053609165)) + (v3_7 * 1073741824)) * 1056964608));
        this.a((v0_13 - this.D), 1);
        return;
    }

    private void a(int p5, android.view.animation.Animation$AnimationListener p6)
    {
        this.c = p5;
        this.af.reset();
        this.af.setDuration(200);
        this.af.setInterpolator(this.L);
        if (p6 != null) {
            this.N.a = p6;
        }
        this.N.clearAnimation();
        this.N.startAnimation(this.af);
        return;
    }

    private void a(int p3, boolean p4)
    {
        this.N.bringToFront();
        this.N.offsetTopAndBottom(p3);
        this.D = this.N.getTop();
        if ((p4) && (android.os.Build$VERSION.SDK_INT < 11)) {
            this.invalidate();
        }
        return;
    }

    static synthetic void a(android.support.v4.widget.dn p0, float p1)
    {
        p0.setAnimationProgress(p1);
        return;
    }

    static synthetic void a(android.support.v4.widget.dn p0, int p1, boolean p2)
    {
        p0.a(p1, p2);
        return;
    }

    private void a(android.view.MotionEvent p4)
    {
        int v0_0 = android.support.v4.view.bk.b(p4);
        if (android.support.v4.view.bk.b(p4, v0_0) == this.I) {
            int v0_1;
            if (v0_0 != 0) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
            }
            this.I = android.support.v4.view.bk.b(p4, v0_1);
        }
        return;
    }

    private void a(android.view.animation.Animation$AnimationListener p5)
    {
        this.N.setVisibility(0);
        if (android.os.Build$VERSION.SDK_INT >= 11) {
            this.Q.setAlpha(255);
        }
        this.R = new android.support.v4.widget.dp(this);
        this.R.setDuration(((long) this.C));
        if (p5 != null) {
            this.N.a = p5;
        }
        this.N.clearAnimation();
        this.N.startAnimation(this.R);
        return;
    }

    private void a(boolean p2, int p3)
    {
        this.W = ((float) p3);
        this.J = p2;
        this.N.invalidate();
        return;
    }

    private void a(boolean p3, int p4, int p5)
    {
        this.J = p3;
        this.N.setVisibility(8);
        this.D = p4;
        this.d = p4;
        this.W = ((float) p5);
        this.ad = 1;
        this.N.invalidate();
        return;
    }

    private void a(boolean p5, boolean p6)
    {
        if (this.v != p5) {
            this.aa = p6;
            this.f();
            this.v = p5;
            if (!this.v) {
                this.b(this.ae);
            } else {
                android.view.animation.Animation v1_0 = this.ae;
                this.c = this.D;
                this.af.reset();
                this.af.setDuration(200);
                this.af.setInterpolator(this.L);
                if (v1_0 != null) {
                    this.N.a = v1_0;
                }
                this.N.clearAnimation();
                this.N.startAnimation(this.af);
            }
        }
        return;
    }

    static synthetic boolean a(android.support.v4.widget.dn p1)
    {
        return p1.v;
    }

    private static boolean a(android.view.animation.Animation p1)
    {
        if ((p1 == null) || ((!p1.hasStarted()) || (p1.hasEnded()))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    static synthetic android.support.v4.widget.bb b(android.support.v4.widget.dn p1)
    {
        return p1.Q;
    }

    private void b(float p6)
    {
        if (p6 <= this.x) {
            this.v = 0;
            this.Q.b(0);
            android.support.v4.widget.e v0_3 = 0;
            if (!this.J) {
                v0_3 = new android.support.v4.widget.ds(this);
            }
            android.view.animation.Animation v1_3 = this.D;
            if (!this.J) {
                this.c = v1_3;
                this.ah.reset();
                this.ah.setDuration(200);
                this.ah.setInterpolator(this.L);
                if (v0_3 != null) {
                    this.N.a = v0_3;
                }
                this.N.clearAnimation();
                this.N.startAnimation(this.ah);
            } else {
                this.c = v1_3;
                if (!android.support.v4.widget.dn.b()) {
                    this.P = android.support.v4.view.cx.q(this.N);
                } else {
                    this.P = ((float) this.Q.getAlpha());
                }
                this.V = new android.support.v4.widget.dw(this);
                this.V.setDuration(150);
                if (v0_3 != null) {
                    this.N.a = v0_3;
                }
                this.N.clearAnimation();
                this.N.startAnimation(this.V);
            }
            this.Q.a(0);
        } else {
            this.a(1, 1);
        }
        return;
    }

    private void b(int p5, android.view.animation.Animation$AnimationListener p6)
    {
        this.c = p5;
        this.ag.reset();
        this.ag.setDuration(500);
        this.ag.setInterpolator(this.L);
        if (p6 != null) {
            this.N.a = p6;
        }
        this.N.clearAnimation();
        this.N.startAnimation(this.ag);
        return;
    }

    static synthetic void b(android.support.v4.widget.dn p3, float p4)
    {
        p3.a(((p3.c + ((int) (((float) (p3.d - p3.c)) * p4))) - p3.N.getTop()), 0);
        return;
    }

    private void b(android.view.animation.Animation$AnimationListener p5)
    {
        this.S = new android.support.v4.widget.dq(this);
        this.S.setDuration(150);
        this.N.a = p5;
        this.N.clearAnimation();
        this.N.startAnimation(this.S);
        return;
    }

    private static boolean b()
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT >= 11) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private void c()
    {
        this.T = this.a(this.Q.getAlpha(), 76);
        return;
    }

    private void c(float p4)
    {
        this.a(((this.c + ((int) (((float) (this.d - this.c)) * p4))) - this.N.getTop()), 0);
        return;
    }

    private void c(int p5, android.view.animation.Animation$AnimationListener p6)
    {
        if (!this.J) {
            this.c = p5;
            this.ah.reset();
            this.ah.setDuration(200);
            this.ah.setInterpolator(this.L);
            if (p6 != null) {
                this.N.a = p6;
            }
            this.N.clearAnimation();
            this.N.startAnimation(this.ah);
        } else {
            this.c = p5;
            if (!android.support.v4.widget.dn.b()) {
                this.P = android.support.v4.view.cx.q(this.N);
            } else {
                this.P = ((float) this.Q.getAlpha());
            }
            this.V = new android.support.v4.widget.dw(this);
            this.V.setDuration(150);
            if (p6 != null) {
                this.N.a = p6;
            }
            this.N.clearAnimation();
            this.N.startAnimation(this.V);
        }
        return;
    }

    static synthetic boolean c(android.support.v4.widget.dn p1)
    {
        return p1.aa;
    }

    static synthetic android.support.v4.widget.dx d(android.support.v4.widget.dn p1)
    {
        return p1.u;
    }

    private void d()
    {
        this.U = this.a(this.Q.getAlpha(), 255);
        return;
    }

    private void d(int p5, android.view.animation.Animation$AnimationListener p6)
    {
        this.c = p5;
        if (!android.support.v4.widget.dn.b()) {
            this.P = android.support.v4.view.cx.q(this.N);
        } else {
            this.P = ((float) this.Q.getAlpha());
        }
        this.V = new android.support.v4.widget.dw(this);
        this.V.setDuration(150);
        if (p6 != null) {
            this.N.a = p6;
        }
        this.N.clearAnimation();
        this.N.startAnimation(this.V);
        return;
    }

    static synthetic android.support.v4.widget.e e(android.support.v4.widget.dn p1)
    {
        return p1.N;
    }

    private boolean e()
    {
        return this.v;
    }

    private void f()
    {
        if (this.t == null) {
            int v0_1 = 0;
            while (v0_1 < this.getChildCount()) {
                android.view.View v1_1 = this.getChildAt(v0_1);
                if (v1_1.equals(this.N)) {
                    v0_1++;
                } else {
                    this.t = v1_1;
                    break;
                }
            }
        }
        return;
    }

    static synthetic void f(android.support.v4.widget.dn p1)
    {
        p1.setColorViewAlpha(255);
        return;
    }

    private boolean g()
    {
        int v0_2;
        if (android.os.Build$VERSION.SDK_INT >= 14) {
            v0_2 = android.support.v4.view.cx.b(this.t, -1);
        } else {
            if (!(this.t instanceof android.widget.AbsListView)) {
                if ((!android.support.v4.view.cx.b(this.t, -1)) && (this.t.getScrollY() <= 0)) {
                    v0_2 = 0;
                } else {
                    v0_2 = 1;
                }
            } else {
                int v0_10 = ((android.widget.AbsListView) this.t);
                if ((v0_10.getChildCount() <= 0) || ((v0_10.getFirstVisiblePosition() <= 0) && (v0_10.getChildAt(0).getTop() >= v0_10.getPaddingTop()))) {
                    v0_2 = 0;
                } else {
                    v0_2 = 1;
                }
            }
        }
        return v0_2;
    }

    static synthetic boolean g(android.support.v4.widget.dn p1)
    {
        return p1.J;
    }

    static synthetic int h(android.support.v4.widget.dn p1)
    {
        return p1.D;
    }

    static synthetic void i(android.support.v4.widget.dn p1)
    {
        p1.b(0);
        return;
    }

    static synthetic boolean j(android.support.v4.widget.dn p1)
    {
        return p1.ad;
    }

    static synthetic float k(android.support.v4.widget.dn p1)
    {
        return p1.W;
    }

    static synthetic float l(android.support.v4.widget.dn p1)
    {
        return p1.P;
    }

    private void setAnimationProgress(float p2)
    {
        if (!android.support.v4.widget.dn.b()) {
            android.support.v4.view.cx.d(this.N, p2);
            android.support.v4.view.cx.e(this.N, p2);
        } else {
            this.setColorViewAlpha(((int) (1132396544 * p2)));
        }
        return;
    }

    private void setColorViewAlpha(int p2)
    {
        this.N.getBackground().setAlpha(p2);
        this.Q.setAlpha(p2);
        return;
    }

    public boolean dispatchNestedFling(float p2, float p3, boolean p4)
    {
        return this.A.a(p2, p3, p4);
    }

    public boolean dispatchNestedPreFling(float p2, float p3)
    {
        return this.A.a(p2, p3);
    }

    public boolean dispatchNestedPreScroll(int p2, int p3, int[] p4, int[] p5)
    {
        return this.A.a(p2, p3, p4, p5);
    }

    public boolean dispatchNestedScroll(int p7, int p8, int p9, int p10, int[] p11)
    {
        return this.A.a(p7, p8, p9, p10, p11);
    }

    protected int getChildDrawingOrder(int p2, int p3)
    {
        if (this.O >= 0) {
            if (p3 != (p2 - 1)) {
                if (p3 >= this.O) {
                    p3++;
                }
            } else {
                p3 = this.O;
            }
        }
        return p3;
    }

    public int getNestedScrollAxes()
    {
        return this.z.a;
    }

    public int getProgressCircleDiameter()
    {
        int v0_1;
        if (this.N == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.N.getMeasuredHeight();
        }
        return v0_1;
    }

    public boolean hasNestedScrollingParent()
    {
        return this.A.a();
    }

    public boolean isNestedScrollingEnabled()
    {
        return this.A.a;
    }

    public boolean onInterceptTouchEvent(android.view.MotionEvent p7)
    {
        android.support.v4.widget.bb v0_0 = 0;
        this.f();
        int v1_0 = android.support.v4.view.bk.a(p7);
        if ((this.K) && (v1_0 == 0)) {
            this.K = 0;
        }
        if ((this.isEnabled()) && ((!this.K) && ((!this.g()) && (!this.v)))) {
            switch (v1_0) {
                case 0:
                    this.a((this.d - this.N.getTop()), 1);
                    this.I = android.support.v4.view.bk.b(p7, 0);
                    this.H = 0;
                    int v1_14 = android.support.v4.widget.dn.a(p7, this.I);
                    if (v1_14 != -1082130432) {
                        this.G = v1_14;
                    }
                    break;
                case 1:
                case 3:
                    this.H = 0;
                    this.I = -1;
                    break;
                case 2:
                    if (this.I != -1) {
                        int v1_3 = android.support.v4.widget.dn.a(p7, this.I);
                        if (v1_3 != -1082130432) {
                            if (((v1_3 - this.G) <= ((float) this.w)) || (this.H)) {
                            } else {
                                this.F = (this.G + ((float) this.w));
                                this.H = 1;
                                this.Q.setAlpha(76);
                            }
                        }
                    } else {
                        android.util.Log.e(android.support.v4.widget.dn.e, "Got ACTION_MOVE event but don\'t have an active pointer id.");
                    }
                    break;
                case 4:
                case 5:
                default:
                    break;
                case 4:
                case 5:
                    break;
                case 6:
                    this.a(p7);
                    break;
            }
            v0_0 = this.H;
        }
        return v0_0;
    }

    protected void onLayout(boolean p8, int p9, int p10, int p11, int p12)
    {
        int v0_0 = this.getMeasuredWidth();
        int v1_0 = this.getMeasuredHeight();
        if (this.getChildCount() != 0) {
            if (this.t == null) {
                this.f();
            }
            if (this.t != null) {
                int v2_3 = this.t;
                android.support.v4.widget.e v3_0 = this.getPaddingLeft();
                int v4_0 = this.getPaddingTop();
                v2_3.layout(v3_0, v4_0, (((v0_0 - this.getPaddingLeft()) - this.getPaddingRight()) + v3_0), (((v1_0 - this.getPaddingTop()) - this.getPaddingBottom()) + v4_0));
                int v1_5 = this.N.getMeasuredWidth();
                this.N.layout(((v0_0 / 2) - (v1_5 / 2)), this.D, ((v0_0 / 2) + (v1_5 / 2)), (this.D + this.N.getMeasuredHeight()));
            }
        }
        return;
    }

    public void onMeasure(int p6, int p7)
    {
        super.onMeasure(p6, p7);
        if (this.t == null) {
            this.f();
        }
        if (this.t != null) {
            this.t.measure(android.view.View$MeasureSpec.makeMeasureSpec(((this.getMeasuredWidth() - this.getPaddingLeft()) - this.getPaddingRight()), 1073741824), android.view.View$MeasureSpec.makeMeasureSpec(((this.getMeasuredHeight() - this.getPaddingTop()) - this.getPaddingBottom()), 1073741824));
            this.N.measure(android.view.View$MeasureSpec.makeMeasureSpec(this.ab, 1073741824), android.view.View$MeasureSpec.makeMeasureSpec(this.ac, 1073741824));
            if ((!this.ad) && (!this.E)) {
                this.E = 1;
                int v0_9 = (- this.N.getMeasuredHeight());
                this.d = v0_9;
                this.D = v0_9;
            }
            this.O = -1;
            int v0_11 = 0;
            while (v0_11 < this.getChildCount()) {
                if (this.getChildAt(v0_11) != this.N) {
                    v0_11++;
                } else {
                    this.O = v0_11;
                    break;
                }
            }
        }
        return;
    }

    public boolean onNestedFling(android.view.View p2, float p3, float p4, boolean p5)
    {
        return 0;
    }

    public boolean onNestedPreFling(android.view.View p2, float p3, float p4)
    {
        return 0;
    }

    public void onNestedPreScroll(android.view.View p7, int p8, int p9, int[] p10)
    {
        if ((p9 > 0) && (this.y > 0)) {
            if (((float) p9) <= this.y) {
                this.y = (this.y - ((float) p9));
                p10[1] = p9;
            } else {
                p10[1] = (p9 - ((int) this.y));
                this.y = 0;
            }
            this.a(this.y);
        }
        float v0_10 = this.B;
        if (this.dispatchNestedPreScroll((p8 - p10[0]), (p9 - p10[1]), v0_10, 0)) {
            p10[0] = (p10[0] + v0_10[0]);
            p10[1] = (v0_10[1] + p10[1]);
        }
        return;
    }

    public void onNestedScroll(android.view.View p7, int p8, int p9, int p10, int p11)
    {
        if (p11 < 0) {
            this.y = (((float) Math.abs(p11)) + this.y);
            this.a(this.y);
        }
        this.dispatchNestedScroll(p8, p9, p10, p8, 0);
        return;
    }

    public void onNestedScrollAccepted(android.view.View p2, android.view.View p3, int p4)
    {
        this.z.a = p4;
        this.y = 0;
        return;
    }

    public boolean onStartNestedScroll(android.view.View p2, android.view.View p3, int p4)
    {
        if ((!this.isEnabled()) || ((p4 & 2) == 0)) {
            int v0_2 = 0;
        } else {
            this.startNestedScroll((p4 & 2));
            v0_2 = 1;
        }
        return v0_2;
    }

    public void onStopNestedScroll(android.view.View p4)
    {
        this.z.a = 0;
        if (this.y > 0) {
            this.b(this.y);
            this.y = 0;
        }
        this.stopNestedScroll();
        return;
    }

    public boolean onTouchEvent(android.view.MotionEvent p7)
    {
        int v0_0 = 0;
        String v2_0 = android.support.v4.view.bk.a(p7);
        if ((this.K) && (v2_0 == null)) {
            this.K = 0;
        }
        if ((this.isEnabled()) && ((!this.K) && (!this.g()))) {
            switch (v2_0) {
                case 0:
                    this.I = android.support.v4.view.bk.b(p7, 0);
                    this.H = 0;
                    break;
                case 1:
                case 3:
                    if (this.I != -1) {
                        String v1_6 = ((android.support.v4.view.bk.d(p7, android.support.v4.view.bk.a(p7, this.I)) - this.F) * 1056964608);
                        this.H = 0;
                        this.b(v1_6);
                        this.I = -1;
                    } else {
                        if (v2_0 == 1) {
                            android.util.Log.e(android.support.v4.widget.dn.e, "Got ACTION_UP event but don\'t have an active pointer id.");
                        }
                    }
                    break;
                case 2:
                    String v2_2 = android.support.v4.view.bk.a(p7, this.I);
                    if (v2_2 >= null) {
                        String v2_5 = ((android.support.v4.view.bk.d(p7, v2_2) - this.F) * 1056964608);
                        if (!this.H) {
                        } else {
                            if (v2_5 > 0) {
                                this.a(v2_5);
                            }
                        }
                    } else {
                        android.util.Log.e(android.support.v4.widget.dn.e, "Got ACTION_MOVE event but have an invalid active pointer id.");
                    }
                    break;
                case 4:
                default:
                    break;
                case 5:
                    this.I = android.support.v4.view.bk.b(p7, android.support.v4.view.bk.b(p7));
                    break;
                case 6:
                    this.a(p7);
                    break;
            }
            v0_0 = 1;
        }
        return v0_0;
    }

    public void requestDisallowInterceptTouchEvent(boolean p3)
    {
        if (((android.os.Build$VERSION.SDK_INT >= 21) || (!(this.t instanceof android.widget.AbsListView))) && ((this.t == null) || (android.support.v4.view.cx.z(this.t)))) {
            super.requestDisallowInterceptTouchEvent(p3);
        }
        return;
    }

    public varargs void setColorScheme(int[] p1)
    {
        this.setColorSchemeResources(p1);
        return;
    }

    public varargs void setColorSchemeColors(int[] p3)
    {
        this.f();
        android.support.v4.widget.bg v0_0 = this.Q;
        v0_0.c.a(p3);
        v0_0.c.a(0);
        return;
    }

    public varargs void setColorSchemeResources(int[] p5)
    {
        android.content.res.Resources v1 = this.getResources();
        int[] v2 = new int[p5.length];
        int v0_1 = 0;
        while (v0_1 < p5.length) {
            v2[v0_1] = v1.getColor(p5[v0_1]);
            v0_1++;
        }
        this.setColorSchemeColors(v2);
        return;
    }

    public void setDistanceToTriggerSync(int p2)
    {
        this.x = ((float) p2);
        return;
    }

    public void setNestedScrollingEnabled(boolean p2)
    {
        this.A.a(p2);
        return;
    }

    public void setOnRefreshListener(android.support.v4.widget.dx p1)
    {
        this.u = p1;
        return;
    }

    public void setProgressBackgroundColor(int p1)
    {
        this.setProgressBackgroundColorSchemeResource(p1);
        return;
    }

    public void setProgressBackgroundColorSchemeColor(int p2)
    {
        this.N.setBackgroundColor(p2);
        this.Q.b(p2);
        return;
    }

    public void setProgressBackgroundColorSchemeResource(int p2)
    {
        this.setProgressBackgroundColorSchemeColor(this.getResources().getColor(p2));
        return;
    }

    public void setRefreshing(boolean p5)
    {
        if ((!p5) || (this.v == p5)) {
            this.a(p5, 0);
        } else {
            android.support.v4.widget.e v0_3;
            this.v = p5;
            if (this.ad) {
                v0_3 = ((int) this.W);
            } else {
                v0_3 = ((int) (this.W + ((float) this.d)));
            }
            this.a((v0_3 - this.D), 1);
            this.aa = 0;
            android.support.v4.widget.e v0_7 = this.ae;
            this.N.setVisibility(0);
            if (android.os.Build$VERSION.SDK_INT >= 11) {
                this.Q.setAlpha(255);
            }
            this.R = new android.support.v4.widget.dp(this);
            this.R.setDuration(((long) this.C));
            if (v0_7 != null) {
                this.N.a = v0_7;
            }
            this.N.clearAnimation();
            this.N.startAnimation(this.R);
        }
        return;
    }

    public void setSize(int p3)
    {
        if ((p3 == 0) || (p3 == 1)) {
            android.support.v4.widget.e v0_2 = this.getResources().getDisplayMetrics();
            if (p3 != 0) {
                android.support.v4.widget.e v0_5 = ((int) (v0_2.density * 1109393408));
                this.ab = v0_5;
                this.ac = v0_5;
            } else {
                android.support.v4.widget.e v0_8 = ((int) (v0_2.density * 1113587712));
                this.ab = v0_8;
                this.ac = v0_8;
            }
            this.N.setImageDrawable(0);
            this.Q.a(p3);
            this.N.setImageDrawable(this.Q);
        }
        return;
    }

    public boolean startNestedScroll(int p2)
    {
        return this.A.a(p2);
    }

    public void stopNestedScroll()
    {
        this.A.b();
        return;
    }
}
