package android.support.v4.widget;
final class bg {
    final android.graphics.RectF a;
    final android.graphics.Paint b;
    final android.graphics.Paint c;
    float d;
    float e;
    float f;
    float g;
    float h;
    int[] i;
    int j;
    float k;
    float l;
    float m;
    boolean n;
    android.graphics.Path o;
    float p;
    double q;
    int r;
    int s;
    int t;
    final android.graphics.Paint u;
    int v;
    int w;
    private final android.graphics.drawable.Drawable$Callback x;

    public bg(android.graphics.drawable.Drawable$Callback p4)
    {
        this.a = new android.graphics.RectF();
        this.b = new android.graphics.Paint();
        this.c = new android.graphics.Paint();
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = 1084227584;
        this.h = 1075838976;
        this.u = new android.graphics.Paint(1);
        this.x = p4;
        this.b.setStrokeCap(android.graphics.Paint$Cap.SQUARE);
        this.b.setAntiAlias(1);
        this.b.setStyle(android.graphics.Paint$Style.STROKE);
        this.c.setStyle(android.graphics.Paint$Style.FILL);
        this.c.setAntiAlias(1);
        return;
    }

    private void a(double p2)
    {
        this.q = p2;
        return;
    }

    private void a(float p2, float p3)
    {
        this.r = ((int) p2);
        this.s = ((int) p3);
        return;
    }

    private void a(int p8, int p9)
    {
        float v0_5;
        float v0_1 = ((float) Math.min(p8, p9));
        if ((this.q > 0) && (v0_1 >= 0)) {
            v0_5 = ((float) (((double) (v0_1 / 1073741824)) - this.q));
        } else {
            v0_5 = ((float) Math.ceil(((double) (this.g / 1073741824))));
        }
        this.h = v0_5;
        return;
    }

    private void a(android.graphics.Canvas p11, float p12, float p13, android.graphics.Rect p14)
    {
        if (this.n) {
            if (this.o != null) {
                this.o.reset();
            } else {
                this.o = new android.graphics.Path();
                this.o.setFillType(android.graphics.Path$FillType.EVEN_ODD);
            }
            android.graphics.Path v0_10 = (((float) (((int) this.h) / 2)) * this.p);
            android.graphics.Paint v1_3 = ((float) ((this.q * Math.cos(0)) + ((double) p14.exactCenterX())));
            float v2_6 = ((float) ((this.q * Math.sin(0)) + ((double) p14.exactCenterY())));
            this.o.moveTo(0, 0);
            this.o.lineTo((((float) this.r) * this.p), 0);
            this.o.lineTo(((((float) this.r) * this.p) / 1073741824), (((float) this.s) * this.p));
            this.o.offset((v1_3 - v0_10), v2_6);
            this.o.close();
            this.c.setColor(this.w);
            p11.rotate(((p12 + p13) - 1084227584), p14.exactCenterX(), p14.exactCenterY());
            p11.drawPath(this.o, this.c);
        }
        return;
    }

    private void a(android.graphics.Canvas p13, android.graphics.Rect p14)
    {
        android.graphics.Paint v1_0 = this.a;
        v1_0.set(p14);
        v1_0.inset(this.h, this.h);
        float v2_2 = ((this.d + this.f) * 1135869952);
        android.graphics.Paint v3_1 = (((this.e + this.f) * 1135869952) - v2_2);
        this.b.setColor(this.w);
        p13.drawArc(v1_0, v2_2, v3_1, 0, this.b);
        if (this.n) {
            if (this.o != null) {
                this.o.reset();
            } else {
                this.o = new android.graphics.Path();
                this.o.setFillType(android.graphics.Path$FillType.EVEN_ODD);
            }
            android.graphics.Path v0_18 = (((float) (((int) this.h) / 2)) * this.p);
            android.graphics.Paint v1_4 = ((float) ((this.q * Math.cos(0)) + ((double) p14.exactCenterX())));
            float v4_9 = ((float) ((this.q * Math.sin(0)) + ((double) p14.exactCenterY())));
            this.o.moveTo(0, 0);
            this.o.lineTo((((float) this.r) * this.p), 0);
            this.o.lineTo(((((float) this.r) * this.p) / 1073741824), (((float) this.s) * this.p));
            this.o.offset((v1_4 - v0_18), v4_9);
            this.o.close();
            this.c.setColor(this.w);
            p13.rotate(((v2_2 + v3_1) - 1084227584), p14.exactCenterX(), p14.exactCenterY());
            p13.drawPath(this.o, this.c);
        }
        if (this.t < 255) {
            this.u.setColor(this.v);
            this.u.setAlpha((255 - this.t));
            p13.drawCircle(p14.exactCenterX(), p14.exactCenterY(), ((float) (p14.width() / 2)), this.u);
        }
        return;
    }

    private void a(android.graphics.ColorFilter p2)
    {
        this.b.setColorFilter(p2);
        this.d();
        return;
    }

    private void b(int p1)
    {
        this.v = p1;
        return;
    }

    private void c(int p1)
    {
        this.w = p1;
        return;
    }

    private void d(float p2)
    {
        this.g = p2;
        this.b.setStrokeWidth(p2);
        this.d();
        return;
    }

    private void d(int p1)
    {
        this.t = p1;
        return;
    }

    private int e()
    {
        return this.i[this.a()];
    }

    private void e(float p2)
    {
        if (p2 != this.p) {
            this.p = p2;
            this.d();
        }
        return;
    }

    private void f()
    {
        this.a(this.a());
        return;
    }

    private int g()
    {
        return this.t;
    }

    private float h()
    {
        return this.g;
    }

    private float i()
    {
        return this.d;
    }

    private float j()
    {
        return this.k;
    }

    private float k()
    {
        return this.l;
    }

    private int l()
    {
        return this.i[this.j];
    }

    private float m()
    {
        return this.e;
    }

    private float n()
    {
        return this.f;
    }

    private float o()
    {
        return this.h;
    }

    private double p()
    {
        return this.q;
    }

    private float q()
    {
        return this.m;
    }

    final int a()
    {
        return ((this.j + 1) % this.i.length);
    }

    public final void a(float p1)
    {
        this.d = p1;
        this.d();
        return;
    }

    public final void a(int p3)
    {
        this.j = p3;
        this.w = this.i[this.j];
        return;
    }

    public final void a(boolean p2)
    {
        if (this.n != p2) {
            this.n = p2;
            this.d();
        }
        return;
    }

    public final void a(int[] p2)
    {
        this.i = p2;
        this.a(0);
        return;
    }

    public final void b()
    {
        this.k = this.d;
        this.l = this.e;
        this.m = this.f;
        return;
    }

    public final void b(float p1)
    {
        this.e = p1;
        this.d();
        return;
    }

    public final void c()
    {
        this.k = 0;
        this.l = 0;
        this.m = 0;
        this.a(0);
        this.b(0);
        this.c(0);
        return;
    }

    public final void c(float p1)
    {
        this.f = p1;
        this.d();
        return;
    }

    final void d()
    {
        this.x.invalidateDrawable(0);
        return;
    }
}
