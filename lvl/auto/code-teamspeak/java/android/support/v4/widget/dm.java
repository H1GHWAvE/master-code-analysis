package android.support.v4.widget;
final class dm {
    private static final int a = 3003121664;
    private static final int b = 2147483648;
    private static final int c = 1291845632;
    private static final int d = 436207616;
    private static final int e = 2000;
    private static final int f = 1000;
    private static final android.view.animation.Interpolator g;
    private final android.graphics.Paint h;
    private final android.graphics.RectF i;
    private float j;
    private long k;
    private long l;
    private boolean m;
    private int n;
    private int o;
    private int p;
    private int q;
    private android.view.View r;
    private android.graphics.Rect s;

    static dm()
    {
        android.support.v4.widget.dm.g = new android.support.v4.view.b.b();
        return;
    }

    private dm(android.view.View p2)
    {
        this.h = new android.graphics.Paint();
        this.i = new android.graphics.RectF();
        this.s = new android.graphics.Rect();
        this.r = p2;
        this.n = -1291845632;
        this.o = -2147483648;
        this.p = 1291845632;
        this.q = 436207616;
        return;
    }

    private void a()
    {
        if (!this.m) {
            this.j = 0;
            this.k = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
            this.m = 1;
            this.r.postInvalidate();
        }
        return;
    }

    private void a(float p6)
    {
        this.j = p6;
        this.k = 0;
        android.support.v4.view.cx.a(this.r, this.s.left, this.s.top, this.s.right, this.s.bottom);
        return;
    }

    private void a(int p1, int p2, int p3, int p4)
    {
        this.n = p1;
        this.o = p2;
        this.p = p3;
        this.q = p4;
        return;
    }

    private void a(android.graphics.Canvas p19)
    {
        int v2_1 = this.s.width();
        android.view.View v3_1 = this.s.height();
        int v10 = (v2_1 / 2);
        int v11 = (v3_1 / 2);
        int v8 = p19.save();
        p19.clipRect(this.s);
        if ((!this.m) && (this.l <= 0)) {
            if ((this.j <= 0) || (((double) this.j) > 1.0)) {
                p19.restoreToCount(v8);
            } else {
                this.a(p19, v10, v11);
            }
        } else {
            int v9;
            int v4_5 = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
            long v12_3 = ((v4_5 - this.k) / 2000);
            float v14_1 = (((float) ((v4_5 - this.k) % 2000)) / 1101004800);
            if (this.m) {
                v9 = 0;
            } else {
                if ((v4_5 - this.l) < 1000) {
                    int v2_13 = (android.support.v4.widget.dm.g.getInterpolation(((((float) ((v4_5 - this.l) % 1000)) / 1092616192) / 1120403456)) * ((float) v10));
                    this.i.set((((float) v10) - v2_13), 0, (v2_13 + ((float) v10)), ((float) v3_1));
                    p19.saveLayerAlpha(this.i, 0, 0);
                    v9 = 1;
                } else {
                    this.l = 0;
                    return;
                }
            }
            if (v12_3 != 0) {
                if ((v14_1 < 0) || (v14_1 >= 1103626240)) {
                    if ((v14_1 < 1103626240) || (v14_1 >= 1112014848)) {
                        if ((v14_1 < 1112014848) || (v14_1 >= 1117126656)) {
                            p19.drawColor(this.p);
                        } else {
                            p19.drawColor(this.o);
                        }
                    } else {
                        p19.drawColor(this.n);
                    }
                } else {
                    p19.drawColor(this.q);
                }
            } else {
                p19.drawColor(this.n);
            }
            if ((v14_1 >= 0) && (v14_1 <= 1103626240)) {
                this.a(p19, ((float) v10), ((float) v11), this.n, (((1103626240 + v14_1) * 1073741824) / 1120403456));
            }
            if ((v14_1 >= 0) && (v14_1 <= 1112014848)) {
                this.a(p19, ((float) v10), ((float) v11), this.o, ((1073741824 * v14_1) / 1120403456));
            }
            if ((v14_1 >= 1103626240) && (v14_1 <= 1117126656)) {
                this.a(p19, ((float) v10), ((float) v11), this.p, (((v14_1 - 1103626240) * 1073741824) / 1120403456));
            }
            if ((v14_1 >= 1112014848) && (v14_1 <= 1120403456)) {
                this.a(p19, ((float) v10), ((float) v11), this.q, (((v14_1 - 1112014848) * 1073741824) / 1120403456));
            }
            if ((v14_1 >= 1117126656) && (v14_1 <= 1120403456)) {
                this.a(p19, ((float) v10), ((float) v11), this.n, (((v14_1 - 1117126656) * 1073741824) / 1120403456));
            }
            if ((this.j <= 0) || (v9 == 0)) {
                int v2_77 = v8;
            } else {
                p19.restoreToCount(v8);
                v2_77 = p19.save();
                p19.clipRect(this.s);
                this.a(p19, v10, v11);
            }
            android.support.v4.view.cx.a(this.r, this.s.left, this.s.top, this.s.right, this.s.bottom);
            v8 = v2_77;
        }
        return;
    }

    private void a(android.graphics.Canvas p3, float p4, float p5, int p6, float p7)
    {
        this.h.setColor(p6);
        p3.save();
        p3.translate(p4, p5);
        p3.scale(android.support.v4.widget.dm.g.getInterpolation(p7), android.support.v4.widget.dm.g.getInterpolation(p7));
        p3.drawCircle(0, 0, p4, this.h);
        p3.restore();
        return;
    }

    private void a(android.graphics.Canvas p5, int p6, int p7)
    {
        this.h.setColor(this.n);
        p5.drawCircle(((float) p6), ((float) p7), (((float) p6) * this.j), this.h);
        return;
    }

    private void b()
    {
        if (this.m) {
            this.j = 0;
            this.l = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
            this.m = 0;
            this.r.postInvalidate();
        }
        return;
    }

    private void b(int p2, int p3, int p4, int p5)
    {
        this.s.left = p2;
        this.s.top = p3;
        this.s.right = p4;
        this.s.bottom = p5;
        return;
    }

    private boolean c()
    {
        if ((!this.m) && (this.l <= 0)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }
}
