package android.support.v4.m;
final class p implements android.support.v4.m.q {
    public static final android.support.v4.m.p a;

    static p()
    {
        android.support.v4.m.p.a = new android.support.v4.m.p();
        return;
    }

    private p()
    {
        return;
    }

    public final int a(CharSequence p4, int p5, int p6)
    {
        int v2 = (p5 + p6);
        int v0_0 = 2;
        while ((p5 < v2) && (v0_0 == 2)) {
            v0_0 = android.support.v4.m.m.a(Character.getDirectionality(p4.charAt(p5)));
            p5++;
        }
        return v0_0;
    }
}
