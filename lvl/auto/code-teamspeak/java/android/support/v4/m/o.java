package android.support.v4.m;
final class o implements android.support.v4.m.q {
    public static final android.support.v4.m.o a;
    public static final android.support.v4.m.o b;
    private final boolean c;

    static o()
    {
        android.support.v4.m.o.a = new android.support.v4.m.o(1);
        android.support.v4.m.o.b = new android.support.v4.m.o(0);
        return;
    }

    private o(boolean p1)
    {
        this.c = p1;
        return;
    }

    public final int a(CharSequence p6, int p7, int p8)
    {
        int v1 = 1;
        int v3 = (p7 + p8);
        int v0_0 = 0;
        while (p7 < v3) {
            switch (android.support.v4.m.m.b(Character.getDirectionality(p6.charAt(p7)))) {
                case 0:
                    if (!this.c) {
                        v0_0 = 1;
                    } else {
                        v1 = 0;
                    }
                    return v1;
                case 1:
                    if (this.c) {
                        v0_0 = 1;
                    }
                    return v1;
            }
            p7++;
        }
        if (v0_0 == 0) {
            v1 = 2;
        } else {
            if (!this.c) {
                v1 = 0;
            }
        }
        return v1;
    }
}
