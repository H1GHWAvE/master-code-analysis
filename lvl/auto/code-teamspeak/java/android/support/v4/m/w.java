package android.support.v4.m;
 class w {

    private w()
    {
        return;
    }

    synthetic w(byte p1)
    {
        return;
    }

    private static int b(java.util.Locale p2)
    {
        int v0 = 0;
        switch (Character.getDirectionality(p2.getDisplayName(p2).charAt(0))) {
            case 1:
            case 2:
                v0 = 1;
                break;
        }
        return v0;
    }

    public int a(java.util.Locale p5)
    {
        int v0 = 1;
        if ((p5 == null) || (p5.equals(android.support.v4.m.u.a))) {
            v0 = 0;
        } else {
            boolean v2_2 = android.support.v4.m.e.a(p5);
            if (v2_2) {
                if ((!v2_2.equalsIgnoreCase(android.support.v4.m.u.a())) && (!v2_2.equalsIgnoreCase(android.support.v4.m.u.b()))) {
                }
            } else {
                switch (Character.getDirectionality(p5.getDisplayName(p5).charAt(0))) {
                    case 1:
                    case 2:
                        break;
                    case 1:
                    case 2:
                        break;
                    default:
                        v0 = 0;
                }
            }
        }
        return v0;
    }

    public String a(String p4)
    {
        StringBuilder v1_1 = new StringBuilder();
        int v0_0 = 0;
        while (v0_0 < p4.length()) {
            String v2_1 = p4.charAt(v0_0);
            switch (v2_1) {
                case 34:
                    v1_1.append("&quot;");
                    break;
                case 38:
                    v1_1.append("&amp;");
                    break;
                case 39:
                    v1_1.append("&#39;");
                    break;
                case 60:
                    v1_1.append("&lt;");
                    break;
                case 62:
                    v1_1.append("&gt;");
                    break;
                default:
                    v1_1.append(v2_1);
            }
            v0_0++;
        }
        return v1_1.toString();
    }
}
