package android.support.v4.n;
public final class e {
    private int[] a;
    private int b;
    private int c;
    private int d;

    public e()
    {
        this(0);
        return;
    }

    private e(byte p4)
    {
        int[] v0_0 = 8;
        if (Integer.bitCount(8) != 1) {
            v0_0 = (1 << (Integer.highestOneBit(8) + 1));
        }
        this.d = (v0_0 - 1);
        int[] v0_3 = new int[v0_0];
        this.a = v0_3;
        return;
    }

    private void a()
    {
        int v0_1 = this.a.length;
        String v1_1 = (v0_1 - this.b);
        int v2 = (v0_1 << 1);
        if (v2 >= 0) {
            int[] v3 = new int[v2];
            System.arraycopy(this.a, this.b, v3, 0, v1_1);
            System.arraycopy(this.a, 0, v3, v1_1, this.b);
            this.a = v3;
            this.b = 0;
            this.c = v0_1;
            this.d = (v2 - 1);
            return;
        } else {
            throw new RuntimeException("Max array capacity exceeded");
        }
    }

    private void a(int p3)
    {
        this.b = ((this.b - 1) & this.d);
        this.a[this.b] = p3;
        if (this.b == this.c) {
            this.a();
        }
        return;
    }

    private int b()
    {
        if (this.b != this.c) {
            int v0_2 = this.a[this.b];
            this.b = ((this.b + 1) & this.d);
            return v0_2;
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private void b(int p3)
    {
        this.a[this.c] = p3;
        this.c = ((this.c + 1) & this.d);
        if (this.c == this.b) {
            this.a();
        }
        return;
    }

    private int c()
    {
        if (this.b != this.c) {
            int v0_3 = ((this.c - 1) & this.d);
            int v1_3 = this.a[v0_3];
            this.c = v0_3;
            return v1_3;
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private void c(int p3)
    {
        if (p3 > 0) {
            if (p3 <= this.g()) {
                this.b = ((this.b + p3) & this.d);
            } else {
                throw new ArrayIndexOutOfBoundsException();
            }
        }
        return;
    }

    private void d()
    {
        this.c = this.b;
        return;
    }

    private void d(int p3)
    {
        if (p3 > 0) {
            if (p3 <= this.g()) {
                this.c = ((this.c - p3) & this.d);
            } else {
                throw new ArrayIndexOutOfBoundsException();
            }
        }
        return;
    }

    private int e()
    {
        if (this.b != this.c) {
            return this.a[this.b];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private int e(int p4)
    {
        if ((p4 >= 0) && (p4 < this.g())) {
            return this.a[((this.b + p4) & this.d)];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private int f()
    {
        if (this.b != this.c) {
            return this.a[((this.c - 1) & this.d)];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private int g()
    {
        return ((this.c - this.b) & this.d);
    }

    private boolean h()
    {
        int v0_1;
        if (this.b != this.c) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }
}
