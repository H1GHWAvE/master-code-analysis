package android.support.v4.n;
public final class x {
    public static final int a = 19;
    private static final int b = 60;
    private static final int c = 3600;
    private static final int d = 86400;
    private static final Object e;
    private static char[] f;

    static x()
    {
        android.support.v4.n.x.e = new Object();
        char[] v0_3 = new char[24];
        android.support.v4.n.x.f = v0_3;
        return;
    }

    public x()
    {
        return;
    }

    private static int a(int p1, int p2, boolean p3, int p4)
    {
        if ((p1 <= 99) && ((!p3) || (p4 < 3))) {
            if ((p1 <= 9) && ((!p3) || (p4 < 2))) {
                if ((!p3) && (p1 <= 0)) {
                    int v0_4 = 0;
                } else {
                    v0_4 = (p2 + 1);
                }
            } else {
                v0_4 = (p2 + 2);
            }
        } else {
            v0_4 = (p2 + 3);
        }
        return v0_4;
    }

    private static int a(long p14)
    {
        int v4 = 1;
        int v2 = 0;
        if (android.support.v4.n.x.f.length < 0) {
            int v0_2 = new char[0];
            android.support.v4.n.x.f = v0_2;
        }
        char[] v7 = android.support.v4.n.x.f;
        if (p14 != 0) {
            int v0_7;
            if (p14 <= 0) {
                v0_7 = 45;
                p14 = (- p14);
            } else {
                v0_7 = 43;
            }
            int v6_0;
            int v8_2 = ((int) (p14 % 1000));
            int v3 = ((int) Math.floor(((double) (p14 / 1000))));
            if (v3 <= 86400) {
                v6_0 = 0;
            } else {
                int v1_0 = (v3 / 86400);
                v3 -= (86400 * v1_0);
                v6_0 = v1_0;
            }
            int v5_2;
            if (v3 <= 3600) {
                v5_2 = 0;
            } else {
                int v1_2 = (v3 / 3600);
                v3 -= (v1_2 * 3600);
                v5_2 = v1_2;
            }
            int v1_4;
            if (v3 <= 60) {
                v1_4 = 0;
            } else {
                v1_4 = (v3 / 60);
                v3 -= (v1_4 * 60);
            }
            int v0_9;
            v7[0] = v0_7;
            int v6_1 = android.support.v4.n.x.a(v7, v6_0, 100, 1, 0);
            if (v6_1 == 1) {
                v0_9 = 0;
            } else {
                v0_9 = 1;
            }
            int v0_10;
            int v5_4 = android.support.v4.n.x.a(v7, v5_2, 104, v6_1, v0_9);
            if (v5_4 == 1) {
                v0_10 = 0;
            } else {
                v0_10 = 1;
            }
            int v0_11 = android.support.v4.n.x.a(v7, v1_4, 109, v5_4, v0_10);
            if (v0_11 != 1) {
                v2 = 1;
            }
            int v0_13 = android.support.v4.n.x.a(v7, v8_2, 109, android.support.v4.n.x.a(v7, v3, 115, v0_11, v2), 1);
            v7[v0_13] = 115;
            v4 = (v0_13 + 1);
        } else {
            v7[0] = 48;
        }
        return v4;
    }

    private static int a(char[] p4, int p5, char p6, int p7, boolean p8)
    {
        if ((p8) || (p5 > 0)) {
            int v0_1;
            char v1_0;
            if (p5 <= 99) {
                v0_1 = p7;
                v1_0 = p5;
            } else {
                char v1_1 = (p5 / 100);
                p4[p7] = ((char) (v1_1 + 48));
                v0_1 = (p7 + 1);
                v1_0 = (p5 - (v1_1 * 100));
            }
            if ((v1_0 > 9) || (p7 != v0_1)) {
                int v2_1 = (v1_0 / 10);
                p4[v0_1] = ((char) (v2_1 + 48));
                v0_1++;
                v1_0 -= (v2_1 * 10);
            }
            p4[v0_1] = ((char) (v1_0 + 48));
            int v0_4 = (v0_1 + 1);
            p4[v0_4] = p6;
            p7 = (v0_4 + 1);
        }
        return p7;
    }

    public static void a(long p2, long p4, java.io.PrintWriter p6)
    {
        if (p2 != 0) {
            android.support.v4.n.x.b((p2 - p4), p6);
        } else {
            p6.print("--");
        }
        return;
    }

    public static void a(long p0, java.io.PrintWriter p2)
    {
        android.support.v4.n.x.b(p0, p2);
        return;
    }

    private static void a(long p4, StringBuilder p6)
    {
        try {
            p6.append(android.support.v4.n.x.f, 0, android.support.v4.n.x.a(p4));
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    private static void b(long p6, java.io.PrintWriter p8)
    {
        try {
            p8.print(new String(android.support.v4.n.x.f, 0, android.support.v4.n.x.a(p6)));
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }
}
