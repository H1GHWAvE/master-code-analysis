package android.support.v4.app;
public abstract class cm extends android.os.Binder implements android.support.v4.app.cl {
    static final int a = 1;
    static final int b = 2;
    static final int c = 3;
    private static final String d = "android.support.v4.app.INotificationSideChannel";

    public cm()
    {
        this.attachInterface(this, "android.support.v4.app.INotificationSideChannel");
        return;
    }

    public static android.support.v4.app.cl a(android.os.IBinder p2)
    {
        android.support.v4.app.cl v0_3;
        if (p2 != null) {
            android.support.v4.app.cl v0_1 = p2.queryLocalInterface("android.support.v4.app.INotificationSideChannel");
            if ((v0_1 == null) || (!(v0_1 instanceof android.support.v4.app.cl))) {
                v0_3 = new android.support.v4.app.cn(p2);
            } else {
                v0_3 = ((android.support.v4.app.cl) v0_1);
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p6, android.os.Parcel p7, android.os.Parcel p8, int p9)
    {
        int v0_1;
        switch (p6) {
            case 1:
                int v0_8;
                p7.enforceInterface("android.support.v4.app.INotificationSideChannel");
                int v2_1 = p7.readString();
                String v3_1 = p7.readInt();
                String v4 = p7.readString();
                if (p7.readInt() == 0) {
                    v0_8 = 0;
                } else {
                    v0_8 = ((android.app.Notification) android.app.Notification.CREATOR.createFromParcel(p7));
                }
                this.a(v2_1, v3_1, v4, v0_8);
                v0_1 = 1;
                break;
            case 2:
                p7.enforceInterface("android.support.v4.app.INotificationSideChannel");
                this.a(p7.readString(), p7.readInt(), p7.readString());
                v0_1 = 1;
                break;
            case 3:
                p7.enforceInterface("android.support.v4.app.INotificationSideChannel");
                this.a(p7.readString());
                v0_1 = 1;
                break;
            case 1598968902:
                p8.writeString("android.support.v4.app.INotificationSideChannel");
                v0_1 = 1;
                break;
            default:
                v0_1 = super.onTransact(p6, p7, p8, p9);
        }
        return v0_1;
    }
}
