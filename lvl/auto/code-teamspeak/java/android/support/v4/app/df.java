package android.support.v4.app;
public final class df extends android.support.v4.app.ek {
    public static final android.support.v4.app.el e;
    final android.os.Bundle a;
    public int b;
    public CharSequence c;
    public android.app.PendingIntent d;
    private final android.support.v4.app.fn[] f;

    static df()
    {
        android.support.v4.app.df.e = new android.support.v4.app.dg();
        return;
    }

    public df(int p7, CharSequence p8, android.app.PendingIntent p9)
    {
        this(p7, p8, p9, new android.os.Bundle(), 0);
        return;
    }

    private df(int p2, CharSequence p3, android.app.PendingIntent p4, android.os.Bundle p5, android.support.v4.app.fn[] p6)
    {
        this.b = p2;
        this.c = android.support.v4.app.dm.d(p3);
        this.d = p4;
        if (p5 == null) {
            p5 = new android.os.Bundle();
        }
        this.a = p5;
        this.f = p6;
        return;
    }

    synthetic df(int p1, CharSequence p2, android.app.PendingIntent p3, android.os.Bundle p4, android.support.v4.app.fn[] p5, byte p6)
    {
        this(p1, p2, p3, p4, p5);
        return;
    }

    static synthetic android.os.Bundle a(android.support.v4.app.df p1)
    {
        return p1.a;
    }

    private android.support.v4.app.fn[] f()
    {
        return this.f;
    }

    public final int a()
    {
        return this.b;
    }

    public final CharSequence b()
    {
        return this.c;
    }

    public final android.app.PendingIntent c()
    {
        return this.d;
    }

    public final android.os.Bundle d()
    {
        return this.a;
    }

    public final bridge synthetic android.support.v4.app.fw[] e()
    {
        return this.f;
    }
}
