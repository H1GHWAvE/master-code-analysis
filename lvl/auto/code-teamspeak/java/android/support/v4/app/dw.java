package android.support.v4.app;
final class dw extends android.support.v4.app.dv {

    dw()
    {
        return;
    }

    public final android.app.Notification a(android.support.v4.app.dm p31, android.support.v4.app.dn p32)
    {
        android.app.Notification v2_1 = new android.support.v4.app.ei(p31.a, p31.B, p31.b, p31.c, p31.h, p31.f, p31.i, p31.d, p31.e, p31.g, p31.o, p31.p, p31.q, p31.k, p31.l, p31.j, p31.n, p31.v, p31.w, p31.C, p31.x, p31.y, p31.z, p31.A, p31.r, p31.s, p31.t);
        android.support.v4.app.dd.a(v2_1, p31.u);
        android.support.v4.app.dd.a(v2_1, p31.m);
        return p32.a(p31, v2_1);
    }

    public final android.os.Bundle a(android.support.v4.app.em p8)
    {
        android.os.Bundle v0_0 = 0;
        String[] v1_0 = 0;
        if (p8 != null) {
            android.os.Bundle v2_1 = new android.os.Bundle();
            if ((p8.d() != null) && (p8.d().length > 1)) {
                v0_0 = p8.d()[0];
            }
            android.os.Parcelable[] v3_5 = new android.os.Parcelable[p8.a().length];
            while (v1_0 < v3_5.length) {
                long v4_4 = new android.os.Bundle();
                v4_4.putString("text", p8.a()[v1_0]);
                v4_4.putString("author", v0_0);
                v3_5[v1_0] = v4_4;
                v1_0++;
            }
            v2_1.putParcelableArray("messages", v3_5);
            android.os.Bundle v0_3 = p8.g();
            if (v0_3 != null) {
                v2_1.putParcelable("remote_input", android.support.v4.app.eh.a(v0_3));
            }
            v2_1.putParcelable("on_reply", p8.b());
            v2_1.putParcelable("on_read", p8.c());
            v2_1.putStringArray("participants", p8.d());
            v2_1.putLong("timestamp", p8.f());
            v0_0 = v2_1;
        }
        return v0_0;
    }

    public final android.support.v4.app.em a(android.os.Bundle p13, android.support.v4.app.en p14, android.support.v4.app.fx p15)
    {
        boolean v2_0 = 0;
        android.support.v4.app.em v4_0 = 0;
        if (p13 != null) {
            String[] v10;
            long v6_0 = p13.getParcelableArray("messages");
            if (v6_0 == 0) {
                v10 = 0;
            } else {
                android.app.PendingIntent v3_0 = new String[v6_0.length];
                String[] v1_0 = 0;
                while (v1_0 < v3_0.length) {
                    if ((v6_0[v1_0] instanceof android.os.Bundle)) {
                        v3_0[v1_0] = ((android.os.Bundle) v6_0[v1_0]).getString("text");
                        if (v3_0[v1_0] != null) {
                            v1_0++;
                        }
                    }
                    if (!v2_0) {
                        return v4_0;
                    } else {
                        v10 = v3_0;
                    }
                }
                v2_0 = 1;
            }
            android.app.PendingIntent v8_1 = ((android.app.PendingIntent) p13.getParcelable("on_read"));
            android.app.PendingIntent v9_1 = ((android.app.PendingIntent) p13.getParcelable("on_reply"));
            android.support.v4.app.en v0_16 = ((android.app.RemoteInput) p13.getParcelable("remote_input"));
            String[] v11 = p13.getStringArray("participants");
            if ((v11 != null) && (v11.length == 1)) {
                boolean v2_1;
                if (v0_16 == null) {
                    v2_1 = 0;
                } else {
                    v2_1 = p15.a(v0_16.getResultKey(), v0_16.getLabel(), v0_16.getChoices(), v0_16.getAllowFreeFormInput(), v0_16.getExtras());
                }
                v4_0 = p14.a(v10, v2_1, v9_1, v8_1, v11, p13.getLong("timestamp"));
            }
        }
        return v4_0;
    }

    public final String c(android.app.Notification p2)
    {
        return p2.category;
    }
}
