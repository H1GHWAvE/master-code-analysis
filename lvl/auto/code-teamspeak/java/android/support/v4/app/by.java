package android.support.v4.app;
public abstract class by extends android.support.v4.view.by {
    private static final String c = "FragmentStatePagerAdapter";
    private static final boolean d;
    private final android.support.v4.app.bi e;
    private android.support.v4.app.cd f;
    private java.util.ArrayList g;
    private java.util.ArrayList h;
    private android.support.v4.app.Fragment i;

    private by(android.support.v4.app.bi p3)
    {
        this.f = 0;
        this.g = new java.util.ArrayList();
        this.h = new java.util.ArrayList();
        this.i = 0;
        this.e = p3;
        return;
    }

    public abstract android.support.v4.app.Fragment a();

    public final Object a(android.view.ViewGroup p6, int p7)
    {
        android.os.Bundle v0_4;
        if (this.h.size() <= p7) {
            if (this.f == null) {
                this.f = this.e.a();
            }
            android.support.v4.app.Fragment v2 = this.a();
            if (this.g.size() > p7) {
                android.os.Bundle v0_12 = ((android.support.v4.app.Fragment$SavedState) this.g.get(p7));
                if (v0_12 != null) {
                    if (v2.z < 0) {
                        if ((v0_12 == null) || (v0_12.a == null)) {
                            android.os.Bundle v0_13 = 0;
                        } else {
                            v0_13 = v0_12.a;
                        }
                        v2.x = v0_13;
                    } else {
                        throw new IllegalStateException("Fragment already active");
                    }
                }
            }
            while (this.h.size() <= p7) {
                this.h.add(0);
            }
            v2.b(0);
            v2.c(0);
            this.h.set(p7, v2);
            this.f.a(p6.getId(), v2);
            v0_4 = v2;
        } else {
            v0_4 = ((android.support.v4.app.Fragment) this.h.get(p7));
            if (v0_4 == null) {
            }
        }
        return v0_4;
    }

    public final void a(int p4, Object p5)
    {
        if (this.f == null) {
            this.f = this.e.a();
        }
        while (this.g.size() <= p4) {
            this.g.add(0);
        }
        this.g.set(p4, this.e.a(((android.support.v4.app.Fragment) p5)));
        this.h.set(p4, 0);
        this.f.b(((android.support.v4.app.Fragment) p5));
        return;
    }

    public final void a(android.os.Parcelable p7, ClassLoader p8)
    {
        if (p7 != null) {
            ((android.os.Bundle) p7).setClassLoader(p8);
            String v3_0 = ((android.os.Bundle) p7).getParcelableArray("states");
            this.g.clear();
            this.h.clear();
            if (v3_0 != null) {
                int v1_0 = 0;
                while (v1_0 < v3_0.length) {
                    this.g.add(((android.support.v4.app.Fragment$SavedState) v3_0[v1_0]));
                    v1_0++;
                }
            }
            int v1_1 = ((android.os.Bundle) p7).keySet().iterator();
            while (v1_1.hasNext()) {
                java.util.ArrayList v0_7 = ((String) v1_1.next());
                if (v0_7.startsWith("f")) {
                    String v3_5 = Integer.parseInt(v0_7.substring(1));
                    StringBuilder v4_1 = this.e.a(((android.os.Bundle) p7), v0_7);
                    if (v4_1 == null) {
                        android.util.Log.w("FragmentStatePagerAdapter", new StringBuilder("Bad fragment at key ").append(v0_7).toString());
                    }
                    while (this.h.size() <= v3_5) {
                        this.h.add(0);
                    }
                    v4_1.b(0);
                    this.h.set(v3_5, v4_1);
                }
            }
        }
        return;
    }

    public final void a(Object p4)
    {
        if (((android.support.v4.app.Fragment) p4) != this.i) {
            if (this.i != null) {
                this.i.b(0);
                this.i.c(0);
            }
            if (((android.support.v4.app.Fragment) p4) != null) {
                ((android.support.v4.app.Fragment) p4).b(1);
                ((android.support.v4.app.Fragment) p4).c(1);
            }
            this.i = ((android.support.v4.app.Fragment) p4);
        }
        return;
    }

    public final boolean a(android.view.View p2, Object p3)
    {
        int v0_1;
        if (((android.support.v4.app.Fragment) p3).ac != p2) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final void b()
    {
        return;
    }

    public final void c()
    {
        if (this.f != null) {
            this.f.j();
            this.f = 0;
            this.e.b();
        }
        return;
    }

    public final android.os.Parcelable d()
    {
        int v0_0 = 0;
        if (this.g.size() > 0) {
            v0_0 = new android.os.Bundle();
            int v1_4 = new android.support.v4.app.Fragment$SavedState[this.g.size()];
            this.g.toArray(v1_4);
            v0_0.putParcelableArray("states", v1_4);
        }
        int v1_5 = 0;
        android.os.Bundle v2_2 = v0_0;
        while (v1_5 < this.h.size()) {
            int v0_6 = ((android.support.v4.app.Fragment) this.h.get(v1_5));
            if ((v0_6 != 0) && (v0_6.k())) {
                if (v2_2 == null) {
                    v2_2 = new android.os.Bundle();
                }
                this.e.a(v2_2, new StringBuilder("f").append(v1_5).toString(), v0_6);
            }
            v1_5++;
        }
        return v2_2;
    }
}
