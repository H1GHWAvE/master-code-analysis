package android.support.v4.app;
final class j {
    private static final String a = "ActionBarDrawerToggleHoneycomb";
    private static final int[] b;

    static j()
    {
        int[] v0_1 = new int[1];
        v0_1[0] = 16843531;
        android.support.v4.app.j.b = v0_1;
        return;
    }

    j()
    {
        return;
    }

    public static android.graphics.drawable.Drawable a(android.app.Activity p2)
    {
        android.content.res.TypedArray v0_1 = p2.obtainStyledAttributes(android.support.v4.app.j.b);
        android.graphics.drawable.Drawable v1_1 = v0_1.getDrawable(0);
        v0_1.recycle();
        return v1_1;
    }

    public static Object a(Object p6, android.app.Activity p7, int p8)
    {
        Object v1_0;
        if (p6 != null) {
            v1_0 = p6;
        } else {
            v1_0 = new android.support.v4.app.k(p7);
        }
        CharSequence v0_1 = ((android.support.v4.app.k) v1_0);
        if (v0_1.a != null) {
            try {
                String v2_1 = p7.getActionBar();
                CharSequence v0_2 = v0_1.b;
                String v3_1 = new Object[1];
                v3_1[0] = Integer.valueOf(p8);
                v0_2.invoke(v2_1, v3_1);
            } catch (CharSequence v0_5) {
                android.util.Log.w("ActionBarDrawerToggleHoneycomb", "Couldn\'t set content description via JB-MR2 API", v0_5);
            }
            if (android.os.Build$VERSION.SDK_INT <= 19) {
                v2_1.setSubtitle(v2_1.getSubtitle());
            }
        }
        return v1_0;
    }

    public static Object a(Object p6, android.app.Activity p7, android.graphics.drawable.Drawable p8, int p9)
    {
        Object v1_0;
        if (p6 != null) {
            v1_0 = p6;
        } else {
            v1_0 = new android.support.v4.app.k(p7);
        }
        String v0_1 = ((android.support.v4.app.k) v1_0);
        if (v0_1.a == null) {
            if (v0_1.c == null) {
                android.util.Log.w("ActionBarDrawerToggleHoneycomb", "Couldn\'t set home-as-up indicator");
            } else {
                v0_1.c.setImageDrawable(p8);
            }
        } else {
            try {
                String v2_3 = p7.getActionBar();
                String v3_0 = v0_1.a;
                int v4_1 = new Object[1];
                v4_1[0] = p8;
                v3_0.invoke(v2_3, v4_1);
                String v0_4 = v0_1.b;
                String v3_2 = new Object[1];
                v3_2[0] = Integer.valueOf(p9);
                v0_4.invoke(v2_3, v3_2);
            } catch (String v0_5) {
                android.util.Log.w("ActionBarDrawerToggleHoneycomb", "Couldn\'t set home-as-up indicator via JB-MR2 API", v0_5);
            }
        }
        return v1_0;
    }
}
