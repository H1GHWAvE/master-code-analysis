package android.support.v4.app;
public final class do implements android.support.v4.app.ds {
    private static final String a = "CarExtender";
    private static final String b = "android.car.EXTENSIONS";
    private static final String c = "large_icon";
    private static final String d = "car_conversation";
    private static final String e = "app_color";
    private android.graphics.Bitmap f;
    private android.support.v4.app.dp g;
    private int h;

    public do()
    {
        this.h = 0;
        return;
    }

    private do(android.app.Notification p5)
    {
        this.h = 0;
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            android.support.v4.app.du v1_2;
            if (android.support.v4.app.dd.a(p5) != null) {
                v1_2 = android.support.v4.app.dd.a(p5).getBundle("android.car.EXTENSIONS");
            } else {
                v1_2 = 0;
            }
            if (v1_2 != null) {
                this.f = ((android.graphics.Bitmap) v1_2.getParcelable("large_icon"));
                this.h = v1_2.getInt("app_color", 0);
                this.g = ((android.support.v4.app.dp) android.support.v4.app.dd.a().a(v1_2.getBundle("car_conversation"), android.support.v4.app.dp.a, android.support.v4.app.fn.c));
            }
        }
        return;
    }

    private int a()
    {
        return this.h;
    }

    private android.support.v4.app.do a(int p1)
    {
        this.h = p1;
        return this;
    }

    private android.support.v4.app.do a(android.graphics.Bitmap p1)
    {
        this.f = p1;
        return this;
    }

    private android.support.v4.app.do a(android.support.v4.app.dp p1)
    {
        this.g = p1;
        return this;
    }

    private android.graphics.Bitmap b()
    {
        return this.f;
    }

    private android.support.v4.app.dp c()
    {
        return this.g;
    }

    public final android.support.v4.app.dm a(android.support.v4.app.dm p4)
    {
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            android.os.Bundle v0_2 = new android.os.Bundle();
            if (this.f != null) {
                v0_2.putParcelable("large_icon", this.f);
            }
            if (this.h != 0) {
                v0_2.putInt("app_color", this.h);
            }
            if (this.g != null) {
                v0_2.putBundle("car_conversation", android.support.v4.app.dd.a().a(this.g));
            }
            p4.b().putBundle("android.car.EXTENSIONS", v0_2);
        }
        return p4;
    }
}
