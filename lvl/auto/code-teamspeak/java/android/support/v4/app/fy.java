package android.support.v4.app;
final class fy {
    public static final String a = "android.remoteinput.results";
    public static final String b = "android.remoteinput.resultsData";
    private static final String c = "resultKey";
    private static final String d = "label";
    private static final String e = "choices";
    private static final String f = "allowFreeFormInput";
    private static final String g = "extras";

    fy()
    {
        return;
    }

    private static android.os.Bundle a(android.content.Intent p4)
    {
        android.os.Bundle v0_0 = 0;
        String v1_0 = p4.getClipData();
        if (v1_0 != null) {
            boolean v2_0 = v1_0.getDescription();
            if ((v2_0.hasMimeType("text/vnd.android.intent")) && (v2_0.getLabel().equals("android.remoteinput.results"))) {
                v0_0 = ((android.os.Bundle) v1_0.getItemAt(0).getIntent().getExtras().getParcelable("android.remoteinput.resultsData"));
            }
        }
        return v0_0;
    }

    private static android.os.Bundle a(android.support.v4.app.fw p3)
    {
        android.os.Bundle v0_1 = new android.os.Bundle();
        v0_1.putString("resultKey", p3.a());
        v0_1.putCharSequence("label", p3.b());
        v0_1.putCharSequenceArray("choices", p3.c());
        v0_1.putBoolean("allowFreeFormInput", p3.d());
        v0_1.putBundle("extras", p3.e());
        return v0_1;
    }

    private static android.support.v4.app.fw a(android.os.Bundle p6, android.support.v4.app.fx p7)
    {
        return p7.a(p6.getString("resultKey"), p6.getCharSequence("label"), p6.getCharSequenceArray("choices"), p6.getBoolean("allowFreeFormInput"), p6.getBundle("extras"));
    }

    static void a(android.support.v4.app.fw[] p6, android.content.Intent p7, android.os.Bundle p8)
    {
        android.os.Bundle v2_1 = new android.os.Bundle();
        int v3 = p6.length;
        int v1_0 = 0;
        while (v1_0 < v3) {
            String v4_0 = p6[v1_0];
            int v0_5 = p8.get(v4_0.a());
            if ((v0_5 instanceof CharSequence)) {
                v2_1.putCharSequence(v4_0.a(), ((CharSequence) v0_5));
            }
            v1_0++;
        }
        int v0_2 = new android.content.Intent();
        v0_2.putExtra("android.remoteinput.resultsData", v2_1);
        p7.setClipData(android.content.ClipData.newIntent("android.remoteinput.results", v0_2));
        return;
    }

    static android.os.Bundle[] a(android.support.v4.app.fw[] p6)
    {
        android.os.Bundle[] v0_2;
        if (p6 != null) {
            android.os.Bundle[] v1 = new android.os.Bundle[p6.length];
            android.os.Bundle[] v0_1 = 0;
            while (v0_1 < p6.length) {
                android.os.Bundle v2_1 = p6[v0_1];
                android.os.Bundle v3_1 = new android.os.Bundle();
                v3_1.putString("resultKey", v2_1.a());
                v3_1.putCharSequence("label", v2_1.b());
                v3_1.putCharSequenceArray("choices", v2_1.c());
                v3_1.putBoolean("allowFreeFormInput", v2_1.d());
                v3_1.putBundle("extras", v2_1.e());
                v1[v0_1] = v3_1;
                v0_1++;
            }
            v0_2 = v1;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    static android.support.v4.app.fw[] a(android.os.Bundle[] p8, android.support.v4.app.fx p9)
    {
        android.support.v4.app.fw[] v0_3;
        if (p8 != null) {
            android.support.v4.app.fw[] v7 = p9.a(p8.length);
            int v6 = 0;
            while (v6 < p8.length) {
                android.support.v4.app.fw[] v0_4 = p8[v6];
                v7[v6] = p9.a(v0_4.getString("resultKey"), v0_4.getCharSequence("label"), v0_4.getCharSequenceArray("choices"), v0_4.getBoolean("allowFreeFormInput"), v0_4.getBundle("extras"));
                v6++;
            }
            v0_3 = v7;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }
}
