package android.support.v4.app;
abstract class ar extends android.app.Activity {

    ar()
    {
        return;
    }

    abstract android.view.View a();

    public void onCreate(android.os.Bundle p3)
    {
        if ((android.os.Build$VERSION.SDK_INT < 11) && (this.getLayoutInflater().getFactory() == null)) {
            this.getLayoutInflater().setFactory(this);
        }
        super.onCreate(p3);
        return;
    }

    public android.view.View onCreateView(String p2, android.content.Context p3, android.util.AttributeSet p4)
    {
        android.view.View v0_1 = this.a(0, p2, p3, p4);
        if (v0_1 == null) {
            v0_1 = super.onCreateView(p2, p3, p4);
        }
        return v0_1;
    }
}
