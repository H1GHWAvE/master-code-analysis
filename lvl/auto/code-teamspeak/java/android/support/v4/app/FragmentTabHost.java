package android.support.v4.app;
public final class FragmentTabHost extends android.widget.TabHost implements android.widget.TabHost$OnTabChangeListener {
    private final java.util.ArrayList a;
    private android.widget.FrameLayout b;
    private android.content.Context c;
    private android.support.v4.app.bi d;
    private int e;
    private android.widget.TabHost$OnTabChangeListener f;
    private android.support.v4.app.cc g;
    private boolean h;

    private FragmentTabHost(android.content.Context p3)
    {
        this(p3, 0);
        this.a = new java.util.ArrayList();
        this.a(p3, 0);
        return;
    }

    private FragmentTabHost(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3);
        this.a = new java.util.ArrayList();
        this.a(p2, p3);
        return;
    }

    private android.support.v4.app.cd a(String p5, android.support.v4.app.cd p6)
    {
        String v1_0 = 0;
        android.support.v4.app.Fragment v2_0 = 0;
        while (v2_0 < this.a.size()) {
            android.support.v4.app.Fragment v0_19 = ((android.support.v4.app.cc) this.a.get(v2_0));
            if (!v0_19.a.equals(p5)) {
                v0_19 = v1_0;
            }
            v2_0++;
            v1_0 = v0_19;
        }
        if (v1_0 != null) {
            if (this.g != v1_0) {
                if (p6 == null) {
                    p6 = this.d.a();
                }
                if ((this.g != null) && (this.g.d != null)) {
                    p6.e(this.g.d);
                }
                if (v1_0 != null) {
                    if (v1_0.d != null) {
                        p6.f(v1_0.d);
                    } else {
                        v1_0.d = android.support.v4.app.Fragment.a(this.c, v1_0.b.getName(), v1_0.c);
                        p6.a(this.e, v1_0.d, v1_0.a);
                    }
                }
                this.g = v1_0;
            }
            return p6;
        } else {
            throw new IllegalStateException(new StringBuilder("No tab known for tag ").append(p5).toString());
        }
    }

    private void a()
    {
        if (this.b == null) {
            this.b = ((android.widget.FrameLayout) this.findViewById(this.e));
            if (this.b == null) {
                throw new IllegalStateException(new StringBuilder("No tab content FrameLayout found for id ").append(this.e).toString());
            }
        }
        return;
    }

    private void a(android.content.Context p8)
    {
        if (this.findViewById(16908307) == null) {
            android.widget.LinearLayout v0_2 = new android.widget.LinearLayout(p8);
            v0_2.setOrientation(1);
            this.addView(v0_2, new android.widget.FrameLayout$LayoutParams(-1, -1));
            android.widget.FrameLayout v1_4 = new android.widget.TabWidget(p8);
            v1_4.setId(16908307);
            v1_4.setOrientation(0);
            v0_2.addView(v1_4, new android.widget.LinearLayout$LayoutParams(-1, -2, 0));
            android.widget.FrameLayout v1_6 = new android.widget.FrameLayout(p8);
            v1_6.setId(16908305);
            v0_2.addView(v1_6, new android.widget.LinearLayout$LayoutParams(0, 0, 0));
            android.widget.FrameLayout v1_8 = new android.widget.FrameLayout(p8);
            this.b = v1_8;
            this.b.setId(this.e);
            v0_2.addView(v1_8, new android.widget.LinearLayout$LayoutParams(-1, 0, 1065353216));
        }
        return;
    }

    private void a(android.content.Context p1, android.support.v4.app.bi p2)
    {
        this.a(p1);
        super.setup();
        this.c = p1;
        this.d = p2;
        this.a();
        return;
    }

    private void a(android.content.Context p3, android.support.v4.app.bi p4, int p5)
    {
        this.a(p3);
        super.setup();
        this.c = p3;
        this.d = p4;
        this.e = p5;
        this.a();
        this.b.setId(p5);
        if (this.getId() == -1) {
            this.setId(16908306);
        }
        return;
    }

    private void a(android.content.Context p4, android.util.AttributeSet p5)
    {
        android.content.res.TypedArray v0_1 = new int[1];
        v0_1[0] = 16842995;
        android.content.res.TypedArray v0_2 = p4.obtainStyledAttributes(p5, v0_1, 0, 0);
        this.e = v0_2.getResourceId(0, 0);
        v0_2.recycle();
        super.setOnTabChangedListener(this);
        return;
    }

    private void a(android.widget.TabHost$TabSpec p4, Class p5, android.os.Bundle p6)
    {
        p4.setContent(new android.support.v4.app.ca(this.c));
        android.support.v4.app.cd v0_2 = p4.getTag();
        android.support.v4.app.cc v1_2 = new android.support.v4.app.cc(v0_2, p5, p6);
        if (this.h) {
            v1_2.d = this.d.a(v0_2);
            if ((v1_2.d != null) && (!v1_2.d.U)) {
                android.support.v4.app.cd v0_8 = this.d.a();
                v0_8.e(v1_2.d);
                v0_8.i();
            }
        }
        this.a.add(v1_2);
        this.addTab(p4);
        return;
    }

    protected final void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        String v3 = this.getCurrentTabTag();
        android.support.v4.app.cd v1_0 = 0;
        int v2 = 0;
        while (v2 < this.a.size()) {
            android.support.v4.app.Fragment v0_8 = ((android.support.v4.app.cc) this.a.get(v2));
            v0_8.d = this.d.a(v0_8.a);
            if ((v0_8.d != null) && (!v0_8.d.U)) {
                if (!v0_8.a.equals(v3)) {
                    if (v1_0 == null) {
                        v1_0 = this.d.a();
                    }
                    v1_0.e(v0_8.d);
                } else {
                    this.g = v0_8;
                }
            }
            v2++;
        }
        this.h = 1;
        android.support.v4.app.Fragment v0_4 = this.a(v3, v1_0);
        if (v0_4 != null) {
            v0_4.i();
            this.d.b();
        }
        return;
    }

    protected final void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.h = 0;
        return;
    }

    protected final void onRestoreInstanceState(android.os.Parcelable p2)
    {
        super.onRestoreInstanceState(((android.support.v4.app.FragmentTabHost$SavedState) p2).getSuperState());
        this.setCurrentTabByTag(((android.support.v4.app.FragmentTabHost$SavedState) p2).a);
        return;
    }

    protected final android.os.Parcelable onSaveInstanceState()
    {
        android.support.v4.app.FragmentTabHost$SavedState v1_1 = new android.support.v4.app.FragmentTabHost$SavedState(super.onSaveInstanceState());
        v1_1.a = this.getCurrentTabTag();
        return v1_1;
    }

    public final void onTabChanged(String p2)
    {
        if (this.h) {
            android.widget.TabHost$OnTabChangeListener v0_2 = this.a(p2, 0);
            if (v0_2 != null) {
                v0_2.i();
            }
        }
        if (this.f != null) {
            this.f.onTabChanged(p2);
        }
        return;
    }

    public final void setOnTabChangedListener(android.widget.TabHost$OnTabChangeListener p1)
    {
        this.f = p1;
        return;
    }

    public final void setup()
    {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }
}
