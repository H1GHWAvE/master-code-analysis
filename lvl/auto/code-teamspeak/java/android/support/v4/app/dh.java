package android.support.v4.app;
public final class dh {
    final android.os.Bundle a;
    private final int b;
    private final CharSequence c;
    private final android.app.PendingIntent d;
    private java.util.ArrayList e;

    private dh(int p2, CharSequence p3, android.app.PendingIntent p4)
    {
        this(p2, p3, p4, new android.os.Bundle());
        return;
    }

    private dh(int p2, CharSequence p3, android.app.PendingIntent p4, android.os.Bundle p5)
    {
        this.b = p2;
        this.c = android.support.v4.app.dm.d(p3);
        this.d = p4;
        this.a = p5;
        return;
    }

    private dh(android.support.v4.app.df p6)
    {
        this(p6.b, p6.c, p6.d, new android.os.Bundle(android.support.v4.app.df.a(p6)));
        return;
    }

    private android.os.Bundle a()
    {
        return this.a;
    }

    private android.support.v4.app.dh a(android.os.Bundle p2)
    {
        if (p2 != null) {
            this.a.putAll(p2);
        }
        return this;
    }

    private android.support.v4.app.dh a(android.support.v4.app.di p1)
    {
        p1.a(this);
        return this;
    }

    private android.support.v4.app.dh a(android.support.v4.app.fn p2)
    {
        if (this.e == null) {
            this.e = new java.util.ArrayList();
        }
        this.e.add(p2);
        return this;
    }

    private android.support.v4.app.df b()
    {
        int v5;
        if (this.e == null) {
            v5 = 0;
        } else {
            int v1_2 = new android.support.v4.app.fn[this.e.size()];
            v5 = ((android.support.v4.app.fn[]) this.e.toArray(v1_2));
        }
        return new android.support.v4.app.df(this.b, this.c, this.d, this.a, v5, 0);
    }
}
