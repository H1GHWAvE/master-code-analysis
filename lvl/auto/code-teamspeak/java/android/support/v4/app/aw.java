package android.support.v4.app;
final class aw {

    aw()
    {
        return;
    }

    public static android.os.Bundle[] a(android.os.Bundle p3, String p4)
    {
        android.os.Bundle[] v0_2;
        android.os.Bundle[] v0_0 = p3.getParcelableArray(p4);
        if ((!(v0_0 instanceof android.os.Bundle[])) && (v0_0 != null)) {
            v0_2 = ((android.os.Bundle[]) java.util.Arrays.copyOf(v0_0, v0_0.length, android.os.Bundle[]));
            p3.putParcelableArray(p4, v0_2);
        } else {
            v0_2 = ((android.os.Bundle[]) ((android.os.Bundle[]) v0_0));
        }
        return v0_2;
    }
}
