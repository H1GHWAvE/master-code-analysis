package android.support.v4.app;
final class cu implements android.support.v4.c.ac, android.support.v4.c.ad {
    final int a;
    final android.os.Bundle b;
    android.support.v4.app.cs c;
    android.support.v4.c.aa d;
    boolean e;
    boolean f;
    Object g;
    boolean h;
    boolean i;
    boolean j;
    boolean k;
    boolean l;
    boolean m;
    android.support.v4.app.cu n;
    final synthetic android.support.v4.app.ct o;

    public cu(android.support.v4.app.ct p1, int p2, android.os.Bundle p3, android.support.v4.app.cs p4)
    {
        this.o = p1;
        this.a = p2;
        this.b = p3;
        this.c = p4;
        return;
    }

    private void e()
    {
        if (android.support.v4.app.ct.b) {
            android.util.Log.v("LoaderManager", new StringBuilder("  Retaining: ").append(this).toString());
        }
        this.i = 1;
        this.j = this.h;
        this.h = 0;
        this.c = 0;
        return;
    }

    private void f()
    {
        if (this.i) {
            if (android.support.v4.app.ct.b) {
                android.util.Log.v("LoaderManager", new StringBuilder("  Finished Retaining: ").append(this).toString());
            }
            this.i = 0;
            if ((this.h != this.j) && (!this.h)) {
                this.b();
            }
        }
        if ((this.h) && ((this.e) && (!this.k))) {
            this.b(this.d, this.g);
        }
        return;
    }

    private void g()
    {
        if ((this.h) && (this.k)) {
            this.k = 0;
            if (this.e) {
                this.b(this.d, this.g);
            }
        }
        return;
    }

    private void h()
    {
        if (android.support.v4.app.ct.b) {
            android.util.Log.v("LoaderManager", new StringBuilder("  Canceling: ").append(this).toString());
        }
        if ((this.h) && ((this.d != null) && ((this.m) && (!this.d.j())))) {
            this.d();
        }
        return;
    }

    final void a()
    {
        if ((!this.i) || (!this.j)) {
            if (!this.h) {
                this.h = 1;
                if (android.support.v4.app.ct.b) {
                    android.util.Log.v("LoaderManager", new StringBuilder("  Starting: ").append(this).toString());
                }
                if ((this.d == null) && (this.c != null)) {
                    this.d = this.c.a();
                }
                if (this.d != null) {
                    if ((!this.d.getClass().isMemberClass()) || (reflect.Modifier.isStatic(this.d.getClass().getModifiers()))) {
                        if (!this.m) {
                            IllegalStateException v0_18 = this.d;
                            String v1_4 = this.a;
                            if (v0_18.q == null) {
                                v0_18.q = this;
                                v0_18.p = v1_4;
                                IllegalStateException v0_19 = this.d;
                                if (v0_19.r == null) {
                                    v0_19.r = this;
                                    this.m = 1;
                                } else {
                                    throw new IllegalStateException("There is already a listener registered");
                                }
                            } else {
                                throw new IllegalStateException("There is already a listener registered");
                            }
                        }
                        this.d.i();
                    } else {
                        throw new IllegalArgumentException(new StringBuilder("Object returned from onCreateLoader must not be a non-static inner member class: ").append(this.d).toString());
                    }
                }
            }
        } else {
            this.h = 1;
        }
        return;
    }

    public final void a(android.support.v4.c.aa p7, Object p8)
    {
        if (android.support.v4.app.ct.b) {
            android.util.Log.v("LoaderManager", new StringBuilder("onLoadComplete: ").append(this).toString());
        }
        if (!this.l) {
            if (this.o.c.a(this.a) == this) {
                android.support.v4.app.bl v0_6 = this.n;
                if (v0_6 == null) {
                    if ((this.g != p8) || (!this.e)) {
                        this.g = p8;
                        this.e = 1;
                        if (this.h) {
                            this.b(p7, p8);
                        }
                    }
                    android.support.v4.app.bl v0_13 = ((android.support.v4.app.cu) this.o.d.a(this.a));
                    if ((v0_13 != null) && (v0_13 != this)) {
                        v0_13.f = 0;
                        v0_13.c();
                        android.support.v4.app.bl v0_15 = this.o.d;
                        int v1_8 = android.support.v4.n.f.a(v0_15.c, v0_15.e, this.a);
                        if ((v1_8 >= 0) && (v0_15.d[v1_8] != android.support.v4.n.w.a)) {
                            v0_15.d[v1_8] = android.support.v4.n.w.a;
                            v0_15.b = 1;
                        }
                    }
                    if ((android.support.v4.app.ct.a(this.o) != null) && (!this.o.a())) {
                        android.support.v4.app.ct.a(this.o).f.j();
                    }
                } else {
                    if (android.support.v4.app.ct.b) {
                        android.util.Log.v("LoaderManager", new StringBuilder("  Switching to pending loader: ").append(v0_6).toString());
                    }
                    this.n = 0;
                    this.o.c.a(this.a, 0);
                    this.c();
                    this.o.a(v0_6);
                }
            } else {
                if (android.support.v4.app.ct.b) {
                    android.util.Log.v("LoaderManager", "  Ignoring load complete -- not active");
                }
            }
        } else {
            if (android.support.v4.app.ct.b) {
                android.util.Log.v("LoaderManager", "  Ignoring load complete -- destroyed");
            }
        }
        return;
    }

    public final void a(String p4, java.io.FileDescriptor p5, java.io.PrintWriter p6, String[] p7)
    {
        while(true) {
            p6.print(p4);
            p6.print("mId=");
            p6.print(this.a);
            p6.print(" mArgs=");
            p6.println(this.b);
            p6.print(p4);
            p6.print("mCallbacks=");
            p6.println(this.c);
            p6.print(p4);
            p6.print("mLoader=");
            p6.println(this.d);
            if (this.d != null) {
                this.d.a(new StringBuilder().append(p4).append("  ").toString(), p5, p6, p7);
            }
            if ((this.e) || (this.f)) {
                p6.print(p4);
                p6.print("mHaveData=");
                p6.print(this.e);
                p6.print("  mDeliveredData=");
                p6.println(this.f);
                p6.print(p4);
                p6.print("mData=");
                p6.println(this.g);
            }
            p6.print(p4);
            p6.print("mStarted=");
            p6.print(this.h);
            p6.print(" mReportNextStart=");
            p6.print(this.k);
            p6.print(" mDestroyed=");
            p6.println(this.l);
            p6.print(p4);
            p6.print("mRetaining=");
            p6.print(this.i);
            p6.print(" mRetainingStarted=");
            p6.print(this.j);
            p6.print(" mListenerRegistered=");
            p6.println(this.m);
            if (this.n == null) {
                break;
            }
            p6.print(p4);
            p6.println("Pending Loader ");
            p6.print(this.n);
            p6.println(":");
            this = this.n;
            p4 = new StringBuilder().append(p4).append("  ").toString();
        }
        return;
    }

    final void b()
    {
        if (android.support.v4.app.ct.b) {
            android.util.Log.v("LoaderManager", new StringBuilder("  Stopping: ").append(this).toString());
        }
        this.h = 0;
        if ((!this.i) && ((this.d != null) && (this.m))) {
            this.m = 0;
            this.d.a(this);
            this.d.a(this);
            this.d.l();
        }
        return;
    }

    final void b(android.support.v4.c.aa p6, Object p7)
    {
        if (this.c != null) {
            String v1_2;
            if (android.support.v4.app.ct.a(this.o) == null) {
                v1_2 = 0;
            } else {
                int v0_5 = android.support.v4.app.ct.a(this.o).f.B;
                android.support.v4.app.ct.a(this.o).f.B = "onLoadFinished";
                v1_2 = v0_5;
            }
            try {
                if (android.support.v4.app.ct.b) {
                    android.support.v4.app.bl v2_4 = new StringBuilder("  onLoadFinished in ").append(p6).append(": ");
                    String v3_3 = new StringBuilder(64);
                    android.support.v4.n.g.a(p7, v3_3);
                    v3_3.append("}");
                    android.util.Log.v("LoaderManager", v2_4.append(v3_3.toString()).toString());
                }
            } catch (int v0_8) {
                if (android.support.v4.app.ct.a(this.o) != null) {
                    android.support.v4.app.ct.a(this.o).f.B = v1_2;
                }
                throw v0_8;
            }
            if (android.support.v4.app.ct.a(this.o) != null) {
                android.support.v4.app.ct.a(this.o).f.B = v1_2;
            }
            this.f = 1;
        }
        return;
    }

    final void c()
    {
        while(true) {
            if (android.support.v4.app.ct.b) {
                android.util.Log.v("LoaderManager", new StringBuilder("  Destroying: ").append(this).toString());
            }
            this.l = 1;
            int v0_3 = this.f;
            this.f = 0;
            if ((this.c != null) && ((this.d != null) && ((this.e) && (v0_3 != 0)))) {
                if (android.support.v4.app.ct.b) {
                    android.util.Log.v("LoaderManager", new StringBuilder("  Reseting: ").append(this).toString());
                }
                int v0_8;
                if (android.support.v4.app.ct.a(this.o) == null) {
                    v0_8 = 0;
                } else {
                    v0_8 = android.support.v4.app.ct.a(this.o).f.B;
                    android.support.v4.app.ct.a(this.o).f.B = "onLoaderReset";
                }
                if (android.support.v4.app.ct.a(this.o) != null) {
                    android.support.v4.app.ct.a(this.o).f.B = v0_8;
                }
            }
            this.c = 0;
            this.g = 0;
            this.e = 0;
            if (this.d != null) {
                if (this.m) {
                    this.m = 0;
                    this.d.a(this);
                    this.d.a(this);
                }
                this.d.m();
            }
            if (this.n == null) {
                break;
            }
            this = this.n;
        }
        return;
    }

    public final void d()
    {
        if (android.support.v4.app.ct.b) {
            android.util.Log.v("LoaderManager", new StringBuilder("onLoadCanceled: ").append(this).toString());
        }
        if (!this.l) {
            if (this.o.c.a(this.a) == this) {
                String v0_6 = this.n;
                if (v0_6 != null) {
                    if (android.support.v4.app.ct.b) {
                        android.util.Log.v("LoaderManager", new StringBuilder("  Switching to pending loader: ").append(v0_6).toString());
                    }
                    this.n = 0;
                    this.o.c.a(this.a, 0);
                    this.c();
                    this.o.a(v0_6);
                }
            } else {
                if (android.support.v4.app.ct.b) {
                    android.util.Log.v("LoaderManager", "  Ignoring load canceled -- not active");
                }
            }
        } else {
            if (android.support.v4.app.ct.b) {
                android.util.Log.v("LoaderManager", "  Ignoring load canceled -- destroyed");
            }
        }
        return;
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder(64);
        v0_1.append("LoaderInfo{");
        v0_1.append(Integer.toHexString(System.identityHashCode(this)));
        v0_1.append(" #");
        v0_1.append(this.a);
        v0_1.append(" : ");
        android.support.v4.n.g.a(this.d, v0_1);
        v0_1.append("}}");
        return v0_1.toString();
    }
}
