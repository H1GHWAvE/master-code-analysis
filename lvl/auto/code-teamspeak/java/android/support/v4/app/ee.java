package android.support.v4.app;
public final class ee implements android.support.v4.app.ds {
    private static final int A = 16;
    private static final int B = 1;
    private static final int C = 8388613;
    private static final int D = 80;
    public static final int a = 255;
    public static final int b = 0;
    public static final int c = 1;
    public static final int d = 2;
    public static final int e = 3;
    public static final int f = 4;
    public static final int g = 5;
    public static final int h = 0;
    public static final int i = 255;
    private static final String j = "android.wearable.EXTENSIONS";
    private static final String k = "actions";
    private static final String l = "flags";
    private static final String m = "displayIntent";
    private static final String n = "pages";
    private static final String o = "background";
    private static final String p = "contentIcon";
    private static final String q = "contentIconGravity";
    private static final String r = "contentActionIndex";
    private static final String s = "customSizePreset";
    private static final String t = "customContentHeight";
    private static final String u = "gravity";
    private static final String v = "hintScreenTimeout";
    private static final int w = 1;
    private static final int x = 2;
    private static final int y = 4;
    private static final int z = 8;
    private java.util.ArrayList E;
    private int F;
    private android.app.PendingIntent G;
    private java.util.ArrayList H;
    private android.graphics.Bitmap I;
    private int J;
    private int K;
    private int L;
    private int M;
    private int N;
    private int O;
    private int P;

    public ee()
    {
        this.E = new java.util.ArrayList();
        this.F = 1;
        this.H = new java.util.ArrayList();
        this.K = 8388613;
        this.L = -1;
        this.M = 0;
        this.O = 80;
        return;
    }

    private ee(android.app.Notification p9)
    {
        int v1_0;
        this.E = new java.util.ArrayList();
        this.F = 1;
        this.H = new java.util.ArrayList();
        this.K = 8388613;
        this.L = -1;
        this.M = 0;
        this.O = 80;
        int v0_4 = android.support.v4.app.dd.a(p9);
        if (v0_4 == 0) {
            v1_0 = 0;
        } else {
            v1_0 = v0_4.getBundle("android.wearable.EXTENSIONS");
        }
        if (v1_0 != 0) {
            int v0_8 = android.support.v4.app.dd.a().a(v1_0.getParcelableArrayList("actions"));
            if (v0_8 != 0) {
                java.util.Collections.addAll(this.E, v0_8);
            }
            this.F = v1_0.getInt("flags", 1);
            this.G = ((android.app.PendingIntent) v1_0.getParcelable("displayIntent"));
            int v0_15 = android.support.v4.app.dd.a(v1_0, "pages");
            if (v0_15 != 0) {
                java.util.Collections.addAll(this.H, v0_15);
            }
            this.I = ((android.graphics.Bitmap) v1_0.getParcelable("background"));
            this.J = v1_0.getInt("contentIcon");
            this.K = v1_0.getInt("contentIconGravity", 8388613);
            this.L = v1_0.getInt("contentActionIndex", -1);
            this.M = v1_0.getInt("customSizePreset", 0);
            this.N = v1_0.getInt("customContentHeight");
            this.O = v1_0.getInt("gravity", 80);
            this.P = v1_0.getInt("hintScreenTimeout");
        }
        return;
    }

    private android.support.v4.app.ee a()
    {
        android.support.v4.app.ee v0_1 = new android.support.v4.app.ee();
        v0_1.E = new java.util.ArrayList(this.E);
        v0_1.F = this.F;
        v0_1.G = this.G;
        v0_1.H = new java.util.ArrayList(this.H);
        v0_1.I = this.I;
        v0_1.J = this.J;
        v0_1.K = this.K;
        v0_1.L = this.L;
        v0_1.M = this.M;
        v0_1.N = this.N;
        v0_1.O = this.O;
        v0_1.P = this.P;
        return v0_1;
    }

    private android.support.v4.app.ee a(int p1)
    {
        this.J = p1;
        return this;
    }

    private android.support.v4.app.ee a(android.app.Notification p2)
    {
        this.H.add(p2);
        return this;
    }

    private android.support.v4.app.ee a(android.app.PendingIntent p1)
    {
        this.G = p1;
        return this;
    }

    private android.support.v4.app.ee a(android.graphics.Bitmap p1)
    {
        this.I = p1;
        return this;
    }

    private android.support.v4.app.ee a(android.support.v4.app.df p2)
    {
        this.E.add(p2);
        return this;
    }

    private android.support.v4.app.ee a(java.util.List p2)
    {
        this.E.addAll(p2);
        return this;
    }

    private android.support.v4.app.ee a(boolean p2)
    {
        this.a(8, p2);
        return this;
    }

    private void a(int p3, boolean p4)
    {
        if (!p4) {
            this.F = (this.F & (p3 ^ -1));
        } else {
            this.F = (this.F | p3);
        }
        return;
    }

    private android.support.v4.app.ee b()
    {
        this.E.clear();
        return this;
    }

    private android.support.v4.app.ee b(int p1)
    {
        this.K = p1;
        return this;
    }

    private android.support.v4.app.ee b(java.util.List p2)
    {
        this.H.addAll(p2);
        return this;
    }

    private android.support.v4.app.ee b(boolean p2)
    {
        this.a(1, p2);
        return this;
    }

    private android.support.v4.app.ee c(int p1)
    {
        this.L = p1;
        return this;
    }

    private android.support.v4.app.ee c(boolean p2)
    {
        this.a(2, p2);
        return this;
    }

    private java.util.List c()
    {
        return this.E;
    }

    private android.app.PendingIntent d()
    {
        return this.G;
    }

    private android.support.v4.app.ee d(int p1)
    {
        this.O = p1;
        return this;
    }

    private android.support.v4.app.ee d(boolean p2)
    {
        this.a(4, p2);
        return this;
    }

    private android.support.v4.app.ee e()
    {
        this.H.clear();
        return this;
    }

    private android.support.v4.app.ee e(int p1)
    {
        this.M = p1;
        return this;
    }

    private android.support.v4.app.ee e(boolean p2)
    {
        this.a(16, p2);
        return this;
    }

    private android.support.v4.app.ee f(int p1)
    {
        this.N = p1;
        return this;
    }

    private java.util.List f()
    {
        return this.H;
    }

    private android.graphics.Bitmap g()
    {
        return this.I;
    }

    private android.support.v4.app.ee g(int p1)
    {
        this.P = p1;
        return this;
    }

    private int h()
    {
        return this.J;
    }

    private int i()
    {
        return this.K;
    }

    private int j()
    {
        return this.L;
    }

    private int k()
    {
        return this.O;
    }

    private int l()
    {
        return this.M;
    }

    private int m()
    {
        return this.N;
    }

    private boolean n()
    {
        int v0_2;
        if ((this.F & 8) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private boolean o()
    {
        int v0_2;
        if ((this.F & 1) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private boolean p()
    {
        int v0_2;
        if ((this.F & 2) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private boolean q()
    {
        int v0_2;
        if ((this.F & 4) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private boolean r()
    {
        int v0_2;
        if ((this.F & 16) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private int s()
    {
        return this.P;
    }

    public final android.support.v4.app.dm a(android.support.v4.app.dm p6)
    {
        android.os.Bundle v1_1 = new android.os.Bundle();
        if (!this.E.isEmpty()) {
            android.support.v4.app.df[] v4_2 = new android.support.v4.app.df[this.E.size()];
            v1_1.putParcelableArrayList("actions", android.support.v4.app.dd.a().a(((android.support.v4.app.df[]) this.E.toArray(v4_2))));
        }
        if (this.F != 1) {
            v1_1.putInt("flags", this.F);
        }
        if (this.G != null) {
            v1_1.putParcelable("displayIntent", this.G);
        }
        if (!this.H.isEmpty()) {
            android.app.Notification[] v3_3 = new android.app.Notification[this.H.size()];
            v1_1.putParcelableArray("pages", ((android.os.Parcelable[]) this.H.toArray(v3_3)));
        }
        if (this.I != null) {
            v1_1.putParcelable("background", this.I);
        }
        if (this.J != 0) {
            v1_1.putInt("contentIcon", this.J);
        }
        if (this.K != 8388613) {
            v1_1.putInt("contentIconGravity", this.K);
        }
        if (this.L != -1) {
            v1_1.putInt("contentActionIndex", this.L);
        }
        if (this.M != 0) {
            v1_1.putInt("customSizePreset", this.M);
        }
        if (this.N != 0) {
            v1_1.putInt("customContentHeight", this.N);
        }
        if (this.O != 80) {
            v1_1.putInt("gravity", this.O);
        }
        if (this.P != 0) {
            v1_1.putInt("hintScreenTimeout", this.P);
        }
        p6.b().putBundle("android.wearable.EXTENSIONS", v1_1);
        return p6;
    }

    public final synthetic Object clone()
    {
        android.support.v4.app.ee v0_1 = new android.support.v4.app.ee();
        v0_1.E = new java.util.ArrayList(this.E);
        v0_1.F = this.F;
        v0_1.G = this.G;
        v0_1.H = new java.util.ArrayList(this.H);
        v0_1.I = this.I;
        v0_1.J = this.J;
        v0_1.K = this.K;
        v0_1.L = this.L;
        v0_1.M = this.M;
        v0_1.N = this.N;
        v0_1.O = this.O;
        v0_1.P = this.P;
        return v0_1;
    }
}
