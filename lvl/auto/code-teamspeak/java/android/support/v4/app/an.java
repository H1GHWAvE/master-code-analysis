package android.support.v4.app;
final class an implements android.view.ViewTreeObserver$OnPreDrawListener {
    final synthetic android.view.View a;
    final synthetic android.support.v4.app.ap b;
    final synthetic int c;
    final synthetic Object d;
    final synthetic android.support.v4.app.ak e;

    an(android.support.v4.app.ak p1, android.view.View p2, android.support.v4.app.ap p3, int p4, Object p5)
    {
        this.e = p1;
        this.a = p2;
        this.b = p3;
        this.c = p4;
        this.d = p5;
        return;
    }

    public final boolean onPreDraw()
    {
        this.a.getViewTreeObserver().removeOnPreDrawListener(this);
        android.support.v4.app.ak.a(this.e, this.b, this.c, this.d);
        return 1;
    }
}
