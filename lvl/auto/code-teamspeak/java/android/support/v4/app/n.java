package android.support.v4.app;
final class n implements java.lang.Runnable {
    final synthetic String[] a;
    final synthetic android.app.Activity b;
    final synthetic int c;

    n(String[] p1, android.app.Activity p2, int p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    public final void run()
    {
        int[] v1 = new int[this.a.length];
        int v2_0 = this.b.getPackageManager();
        String[] v3_0 = this.b.getPackageName();
        int v4 = this.a.length;
        android.support.v4.app.o v0_5 = 0;
        while (v0_5 < v4) {
            v1[v0_5] = v2_0.checkPermission(this.a[v0_5], v3_0);
            v0_5++;
        }
        ((android.support.v4.app.o) this.b).onRequestPermissionsResult(this.c, this.a, v1);
        return;
    }
}
