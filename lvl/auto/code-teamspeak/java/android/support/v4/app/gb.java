package android.support.v4.app;
public final class gb {
    android.app.Activity a;
    private android.content.Intent b;
    private CharSequence c;
    private java.util.ArrayList d;
    private java.util.ArrayList e;
    private java.util.ArrayList f;
    private java.util.ArrayList g;

    private gb(android.app.Activity p4)
    {
        this.a = p4;
        this.b = new android.content.Intent().setAction("android.intent.action.SEND");
        this.b.putExtra("android.support.v4.app.EXTRA_CALLING_PACKAGE", p4.getPackageName());
        this.b.putExtra("android.support.v4.app.EXTRA_CALLING_ACTIVITY", p4.getComponentName());
        this.b.addFlags(524288);
        return;
    }

    private android.support.v4.app.gb a(int p2)
    {
        this.c = this.a.getText(p2);
        return this;
    }

    private static android.support.v4.app.gb a(android.app.Activity p1)
    {
        return new android.support.v4.app.gb(p1);
    }

    private android.support.v4.app.gb a(android.net.Uri p3)
    {
        if (!this.b.getAction().equals("android.intent.action.SEND")) {
            this.b.setAction("android.intent.action.SEND");
        }
        this.g = 0;
        this.b.putExtra("android.intent.extra.STREAM", p3);
        return this;
    }

    private android.support.v4.app.gb a(CharSequence p1)
    {
        this.c = p1;
        return this;
    }

    private android.support.v4.app.gb a(String p2)
    {
        this.b.setType(p2);
        return this;
    }

    private android.support.v4.app.gb a(String[] p3)
    {
        if (this.d != null) {
            this.d = 0;
        }
        this.b.putExtra("android.intent.extra.EMAIL", p3);
        return this;
    }

    private void a(String p6, java.util.ArrayList p7)
    {
        android.content.Intent v0_1;
        String[] v2 = this.b.getStringArrayExtra(p6);
        if (v2 == null) {
            v0_1 = 0;
        } else {
            v0_1 = v2.length;
        }
        String[] v3_2 = new String[(p7.size() + v0_1)];
        p7.toArray(v3_2);
        if (v2 != null) {
            System.arraycopy(v2, 0, v3_2, p7.size(), v0_1);
        }
        this.b.putExtra(p6, v3_2);
        return;
    }

    private void a(String p6, String[] p7)
    {
        int v0;
        android.content.Intent v2 = this.a();
        int v3_0 = v2.getStringArrayExtra(p6);
        if (v3_0 == 0) {
            v0 = 0;
        } else {
            v0 = v3_0.length;
        }
        String[] v4_2 = new String[(p7.length + v0)];
        if (v3_0 != 0) {
            System.arraycopy(v3_0, 0, v4_2, 0, v0);
        }
        System.arraycopy(p7, 0, v4_2, v0, p7.length);
        v2.putExtra(p6, v4_2);
        return;
    }

    private android.support.v4.app.gb b(android.net.Uri p4)
    {
        java.util.ArrayList v0_2 = ((android.net.Uri) this.b.getParcelableExtra("android.intent.extra.STREAM"));
        if (v0_2 != null) {
            if (this.g == null) {
                this.g = new java.util.ArrayList();
            }
            if (v0_2 != null) {
                this.b.removeExtra("android.intent.extra.STREAM");
                this.g.add(v0_2);
            }
            this.g.add(p4);
        } else {
            if (!this.b.getAction().equals("android.intent.action.SEND")) {
                this.b.setAction("android.intent.action.SEND");
            }
            this.g = 0;
            this.b.putExtra("android.intent.extra.STREAM", p4);
        }
        return this;
    }

    private android.support.v4.app.gb b(CharSequence p3)
    {
        this.b.putExtra("android.intent.extra.TEXT", p3);
        return this;
    }

    private android.support.v4.app.gb b(String p4)
    {
        this.b.putExtra("android.intent.extra.HTML_TEXT", p4);
        if (!this.b.hasExtra("android.intent.extra.TEXT")) {
            this.b.putExtra("android.intent.extra.TEXT", android.text.Html.fromHtml(p4));
        }
        return this;
    }

    private android.support.v4.app.gb b(String[] p2)
    {
        this.a("android.intent.extra.EMAIL", p2);
        return this;
    }

    private android.app.Activity c()
    {
        return this.a;
    }

    private android.support.v4.app.gb c(String p2)
    {
        if (this.d == null) {
            this.d = new java.util.ArrayList();
        }
        this.d.add(p2);
        return this;
    }

    private android.support.v4.app.gb c(String[] p3)
    {
        this.b.putExtra("android.intent.extra.CC", p3);
        return this;
    }

    private android.support.v4.app.gb d(String p2)
    {
        if (this.e == null) {
            this.e = new java.util.ArrayList();
        }
        this.e.add(p2);
        return this;
    }

    private android.support.v4.app.gb d(String[] p2)
    {
        this.a("android.intent.extra.CC", p2);
        return this;
    }

    private void d()
    {
        this.a.startActivity(this.b());
        return;
    }

    private android.support.v4.app.gb e(String p2)
    {
        if (this.f == null) {
            this.f = new java.util.ArrayList();
        }
        this.f.add(p2);
        return this;
    }

    private android.support.v4.app.gb e(String[] p3)
    {
        this.b.putExtra("android.intent.extra.BCC", p3);
        return this;
    }

    private android.support.v4.app.gb f(String p3)
    {
        this.b.putExtra("android.intent.extra.SUBJECT", p3);
        return this;
    }

    private android.support.v4.app.gb f(String[] p2)
    {
        this.a("android.intent.extra.BCC", p2);
        return this;
    }

    public final android.content.Intent a()
    {
        if (this.d != null) {
            this.a("android.intent.extra.EMAIL", this.d);
            this.d = 0;
        }
        if (this.e != null) {
            this.a("android.intent.extra.CC", this.e);
            this.e = 0;
        }
        if (this.f != null) {
            this.a("android.intent.extra.BCC", this.f);
            this.f = 0;
        }
        if ((this.g == null) || (this.g.size() <= 1)) {
            String v1_9 = 0;
        } else {
            v1_9 = 1;
        }
        boolean v3_4 = this.b.getAction().equals("android.intent.action.SEND_MULTIPLE");
        if ((v1_9 == null) && (v3_4)) {
            this.b.setAction("android.intent.action.SEND");
            if ((this.g == null) || (this.g.isEmpty())) {
                this.b.removeExtra("android.intent.extra.STREAM");
            } else {
                this.b.putExtra("android.intent.extra.STREAM", ((android.os.Parcelable) this.g.get(0)));
            }
            this.g = 0;
        }
        if ((v1_9 != null) && (!v3_4)) {
            this.b.setAction("android.intent.action.SEND_MULTIPLE");
            if ((this.g == null) || (this.g.isEmpty())) {
                this.b.removeExtra("android.intent.extra.STREAM");
            } else {
                this.b.putParcelableArrayListExtra("android.intent.extra.STREAM", this.g);
            }
        }
        return this.b;
    }

    public final android.content.Intent b()
    {
        return android.content.Intent.createChooser(this.a(), this.c);
    }
}
