package android.support.v4.a;
public final class a {
    public static final int a = 1;
    public static final int b = 2;
    public static final int c = 4;
    public static final int d = 8;
    public static final int e = 32;
    public static final int f = 255;
    public static final int g = 1;
    public static final int h = 2;
    public static final int i = 4;
    public static final int j = 8;
    public static final int k = 16;
    public static final int l = 32;
    private static final android.support.v4.a.e m;

    static a()
    {
        if (android.os.Build$VERSION.SDK_INT < 18) {
            if (android.os.Build$VERSION.SDK_INT < 14) {
                android.support.v4.a.a.m = new android.support.v4.a.d();
            } else {
                android.support.v4.a.a.m = new android.support.v4.a.b();
            }
        } else {
            android.support.v4.a.a.m = new android.support.v4.a.c();
        }
        return;
    }

    private a()
    {
        return;
    }

    private static String a(int p4)
    {
        String v0_1 = new StringBuilder();
        v0_1.append("[");
        while (p4 > 0) {
            String v1_3 = (1 << Integer.numberOfTrailingZeros(p4));
            p4 &= (v1_3 ^ -1);
            if (v0_1.length() > 1) {
                v0_1.append(", ");
            }
            switch (v1_3) {
                case 1:
                    v0_1.append("FEEDBACK_SPOKEN");
                    break;
                case 2:
                    v0_1.append("FEEDBACK_HAPTIC");
                    break;
                case 4:
                    v0_1.append("FEEDBACK_AUDIBLE");
                    break;
                case 8:
                    v0_1.append("FEEDBACK_VISUAL");
                    break;
                case 16:
                    v0_1.append("FEEDBACK_GENERIC");
                    break;
                default:
            }
        }
        v0_1.append("]");
        return v0_1.toString();
    }

    private static String a(android.accessibilityservice.AccessibilityServiceInfo p1)
    {
        return android.support.v4.a.a.m.c(p1);
    }

    private static android.content.pm.ResolveInfo b(android.accessibilityservice.AccessibilityServiceInfo p1)
    {
        return android.support.v4.a.a.m.d(p1);
    }

    private static String b(int p1)
    {
        String v0;
        switch (p1) {
            case 1:
                v0 = "DEFAULT";
                break;
            case 2:
                v0 = "FLAG_INCLUDE_NOT_IMPORTANT_VIEWS";
                break;
            case 4:
                v0 = "FLAG_REQUEST_TOUCH_EXPLORATION_MODE";
                break;
            case 8:
                v0 = "FLAG_REQUEST_ENHANCED_WEB_ACCESSIBILITY";
                break;
            case 16:
                v0 = "FLAG_REPORT_VIEW_IDS";
                break;
            case 32:
                v0 = "FLAG_REQUEST_FILTER_KEY_EVENTS";
                break;
            default:
                v0 = 0;
        }
        return v0;
    }

    private static String c(int p1)
    {
        String v0;
        switch (p1) {
            case 1:
                v0 = "CAPABILITY_CAN_RETRIEVE_WINDOW_CONTENT";
                break;
            case 2:
                v0 = "CAPABILITY_CAN_REQUEST_TOUCH_EXPLORATION";
                break;
            case 3:
            case 5:
            case 6:
            case 7:
            default:
                v0 = "UNKNOWN";
                break;
            case 4:
                v0 = "CAPABILITY_CAN_REQUEST_ENHANCED_WEB_ACCESSIBILITY";
                break;
            case 8:
                v0 = "CAPABILITY_CAN_FILTER_KEY_EVENTS";
                break;
        }
        return v0;
    }

    private static String c(android.accessibilityservice.AccessibilityServiceInfo p1)
    {
        return android.support.v4.a.a.m.e(p1);
    }

    private static boolean d(android.accessibilityservice.AccessibilityServiceInfo p1)
    {
        return android.support.v4.a.a.m.a(p1);
    }

    private static String e(android.accessibilityservice.AccessibilityServiceInfo p1)
    {
        return android.support.v4.a.a.m.b(p1);
    }

    private static int f(android.accessibilityservice.AccessibilityServiceInfo p1)
    {
        return android.support.v4.a.a.m.f(p1);
    }
}
