package android.support.v4.a;
 class b extends android.support.v4.a.d {

    b()
    {
        return;
    }

    public final boolean a(android.accessibilityservice.AccessibilityServiceInfo p2)
    {
        return p2.getCanRetrieveWindowContent();
    }

    public final String b(android.accessibilityservice.AccessibilityServiceInfo p2)
    {
        return p2.getDescription();
    }

    public final String c(android.accessibilityservice.AccessibilityServiceInfo p2)
    {
        return p2.getId();
    }

    public final android.content.pm.ResolveInfo d(android.accessibilityservice.AccessibilityServiceInfo p2)
    {
        return p2.getResolveInfo();
    }

    public final String e(android.accessibilityservice.AccessibilityServiceInfo p2)
    {
        return p2.getSettingsActivityName();
    }

    public int f(android.accessibilityservice.AccessibilityServiceInfo p2)
    {
        int v0_1;
        if (!p2.getCanRetrieveWindowContent()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }
}
