package android.support.v4.j;
final class k extends android.print.PrintDocumentAdapter {
    android.os.AsyncTask a;
    android.graphics.Bitmap b;
    final synthetic String c;
    final synthetic android.net.Uri d;
    final synthetic android.support.v4.j.n e;
    final synthetic int f;
    final synthetic android.support.v4.j.i g;
    private android.print.PrintAttributes h;

    k(android.support.v4.j.i p2, String p3, android.net.Uri p4, android.support.v4.j.n p5, int p6)
    {
        this.g = p2;
        this.c = p3;
        this.d = p4;
        this.e = p5;
        this.f = p6;
        this.b = 0;
        return;
    }

    private void a()
    {
        try {
            if (this.g.b != null) {
                this.g.b.requestCancelDecode();
                this.g.b = 0;
            }
        } catch (android.support.v4.j.i v0_6) {
            throw v0_6;
        }
        return;
    }

    static synthetic void a(android.support.v4.j.k p0)
    {
        p0.a();
        return;
    }

    public final void onFinish()
    {
        super.onFinish();
        this.a();
        if (this.a != null) {
            this.a.cancel(1);
        }
        if (this.b != null) {
            this.b.recycle();
            this.b = 0;
        }
        return;
    }

    public final void onLayout(android.print.PrintAttributes p8, android.print.PrintAttributes p9, android.os.CancellationSignal p10, android.print.PrintDocumentAdapter$LayoutResultCallback p11, android.os.Bundle p12)
    {
        int v0_0 = 1;
        this.h = p9;
        if (!p10.isCanceled()) {
            if (this.b == null) {
                android.net.Uri[] v1_3 = new android.net.Uri[0];
                this.a = new android.support.v4.j.l(this, p10, p9, p8, p11).execute(v1_3);
            } else {
                android.net.Uri[] v1_8 = new android.print.PrintDocumentInfo$Builder(this.c).setContentType(1).setPageCount(1).build();
                if (p9.equals(p8)) {
                    v0_0 = 0;
                }
                p11.onLayoutFinished(v1_8, v0_0);
            }
        } else {
            p11.onLayoutCancelled();
        }
        return;
    }

    public final void onWrite(android.print.PageRange[] p8, android.os.ParcelFileDescriptor p9, android.os.CancellationSignal p10, android.print.PrintDocumentAdapter$WriteResultCallback p11)
    {
        java.io.IOException v1_1 = new android.print.pdf.PrintedPdfDocument(this.g.a, this.h);
        android.graphics.Bitmap v2_3 = android.support.v4.j.i.a(this.b, this.h.getColorMode());
        try {
            java.io.IOException v0_4 = v1_1.startPage(1);
            v0_4.getCanvas().drawBitmap(v2_3, android.support.v4.j.i.a(this.b.getWidth(), this.b.getHeight(), new android.graphics.RectF(v0_4.getInfo().getContentRect()), this.f), 0);
            v1_1.finishPage(v0_4);
            try {
                v1_1.writeTo(new java.io.FileOutputStream(p9.getFileDescriptor()));
                java.io.IOException v0_8 = new android.print.PageRange[1];
                v0_8[0] = android.print.PageRange.ALL_PAGES;
                p11.onWriteFinished(v0_8);
            } catch (java.io.IOException v0_9) {
                android.util.Log.e("PrintHelperKitkat", "Error writing printed content", v0_9);
                p11.onWriteFailed(0);
            }
            v1_1.close();
            if (p9 != null) {
                try {
                    p9.close();
                } catch (java.io.IOException v0) {
                }
            }
            if (v2_3 != this.b) {
                v2_3.recycle();
            }
            return;
        } catch (java.io.IOException v0_11) {
            v1_1.close();
            if (p9 != null) {
                try {
                    p9.close();
                } catch (java.io.IOException v1) {
                }
            }
            if (v2_3 != this.b) {
                v2_3.recycle();
            }
            throw v0_11;
        }
    }
}
