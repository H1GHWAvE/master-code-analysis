package android.support.v4.j;
final class d implements android.support.v4.j.h {
    private final android.support.v4.j.i a;

    d(android.content.Context p2)
    {
        this.a = new android.support.v4.j.i(p2);
        return;
    }

    public final int a()
    {
        return this.a.j;
    }

    public final void a(int p2)
    {
        this.a.j = p2;
        return;
    }

    public final void a(String p9, android.graphics.Bitmap p10, android.support.v4.j.c p11)
    {
        android.support.v4.j.e v5_0 = 0;
        if (p11 != null) {
            v5_0 = new android.support.v4.j.e(this, p11);
        }
        android.support.v4.j.i v1 = this.a;
        if (p10 != null) {
            android.support.v4.j.j v0_2 = android.print.PrintAttributes$MediaSize.UNKNOWN_PORTRAIT;
            if (p10.getWidth() > p10.getHeight()) {
                v0_2 = android.print.PrintAttributes$MediaSize.UNKNOWN_LANDSCAPE;
            }
            ((android.print.PrintManager) v1.a.getSystemService("print")).print(p9, new android.support.v4.j.j(v1, p9, p10, v1.j, v5_0), new android.print.PrintAttributes$Builder().setMediaSize(v0_2).setColorMode(v1.k).build());
        }
        return;
    }

    public final void a(String p7, android.net.Uri p8, android.support.v4.j.c p9)
    {
        int v4_0 = 0;
        if (p9 != null) {
            v4_0 = new android.support.v4.j.f(this, p9);
        }
        android.print.PrintAttributes$MediaSize v1_0 = this.a;
        android.support.v4.j.k v0_1 = new android.support.v4.j.k(v1_0, p7, p8, v4_0, v1_0.j);
        android.print.PrintManager v2_3 = ((android.print.PrintManager) v1_0.a.getSystemService("print"));
        android.print.PrintAttributes$Builder v3_3 = new android.print.PrintAttributes$Builder();
        v3_3.setColorMode(v1_0.k);
        if (v1_0.l != 1) {
            if (v1_0.l == 2) {
                v3_3.setMediaSize(android.print.PrintAttributes$MediaSize.UNKNOWN_PORTRAIT);
            }
        } else {
            v3_3.setMediaSize(android.print.PrintAttributes$MediaSize.UNKNOWN_LANDSCAPE);
        }
        v2_3.print(p7, v0_1, v3_3.build());
        return;
    }

    public final int b()
    {
        return this.a.k;
    }

    public final void b(int p2)
    {
        this.a.k = p2;
        return;
    }

    public final int c()
    {
        return this.a.l;
    }

    public final void c(int p2)
    {
        this.a.l = p2;
        return;
    }
}
