package android.support.v4.c;
abstract class ai {
    private static final String a = "AsyncTask";
    private static final int b = 5;
    private static final int c = 128;
    public static final java.util.concurrent.Executor d = None;
    private static final int f = 1;
    private static final java.util.concurrent.ThreadFactory g = None;
    private static final java.util.concurrent.BlockingQueue h = None;
    private static final int i = 1;
    private static final int j = 2;
    private static android.support.v4.c.ao k;
    private static volatile java.util.concurrent.Executor l;
    final java.util.concurrent.FutureTask e;
    private final android.support.v4.c.aq m;
    private volatile int n;
    private final java.util.concurrent.atomic.AtomicBoolean o;

    static ai()
    {
        android.support.v4.c.ai.g = new android.support.v4.c.aj();
        android.support.v4.c.ai.h = new java.util.concurrent.LinkedBlockingQueue(10);
        java.util.concurrent.ThreadPoolExecutor v1_2 = new java.util.concurrent.ThreadPoolExecutor(5, 128, 1, java.util.concurrent.TimeUnit.SECONDS, android.support.v4.c.ai.h, android.support.v4.c.ai.g);
        android.support.v4.c.ai.d = v1_2;
        android.support.v4.c.ai.l = v1_2;
        return;
    }

    public ai()
    {
        this.n = android.support.v4.c.ap.a;
        this.o = new java.util.concurrent.atomic.AtomicBoolean();
        this.m = new android.support.v4.c.ak(this);
        this.e = new android.support.v4.c.al(this, this.m);
        return;
    }

    private varargs android.support.v4.c.ai a(Object[] p2)
    {
        return this.a(android.support.v4.c.ai.l, p2);
    }

    private Object a(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.e.get(p2, p4);
    }

    static synthetic Object a(android.support.v4.c.ai p1, Object p2)
    {
        return p1.d(p2);
    }

    static synthetic java.util.concurrent.atomic.AtomicBoolean a(android.support.v4.c.ai p1)
    {
        return p1.o;
    }

    private static void a(Runnable p1)
    {
        android.support.v4.c.ai.l.execute(p1);
        return;
    }

    private static void a(java.util.concurrent.Executor p0)
    {
        android.support.v4.c.ai.l = p0;
        return;
    }

    protected static varargs void b()
    {
        return;
    }

    static synthetic void b(android.support.v4.c.ai p1, Object p2)
    {
        if (!p1.o.get()) {
            p1.d(p2);
        }
        return;
    }

    private varargs void b(Object[] p4)
    {
        if (!this.e.isCancelled()) {
            android.support.v4.c.ai.c().obtainMessage(2, new android.support.v4.c.an(this, p4)).sendToTarget();
        }
        return;
    }

    private static android.os.Handler c()
    {
        try {
            if (android.support.v4.c.ai.k == null) {
                android.support.v4.c.ai.k = new android.support.v4.c.ao();
            }
        } catch (android.support.v4.c.ao v0_4) {
            throw v0_4;
        }
        return android.support.v4.c.ai.k;
    }

    static synthetic void c(android.support.v4.c.ai p1, Object p2)
    {
        if (!p1.e.isCancelled()) {
            p1.a(p2);
        } else {
            p1.b(p2);
        }
        p1.n = android.support.v4.c.ap.c;
        return;
    }

    private void c(Object p2)
    {
        if (!this.o.get()) {
            this.d(p2);
        }
        return;
    }

    private int d()
    {
        return this.n;
    }

    private Object d(Object p6)
    {
        android.os.Message v0_0 = android.support.v4.c.ai.c();
        Object[] v2 = new Object[1];
        v2[0] = p6;
        v0_0.obtainMessage(1, new android.support.v4.c.an(this, v2)).sendToTarget();
        return p6;
    }

    private static void e()
    {
        return;
    }

    private void e(Object p2)
    {
        if (!this.e.isCancelled()) {
            this.a(p2);
        } else {
            this.b(p2);
        }
        this.n = android.support.v4.c.ap.c;
        return;
    }

    private static void f()
    {
        return;
    }

    private boolean g()
    {
        return this.e.isCancelled();
    }

    private boolean h()
    {
        return this.e.cancel(0);
    }

    private Object i()
    {
        return this.e.get();
    }

    public final varargs android.support.v4.c.ai a(java.util.concurrent.Executor p3, Object[] p4)
    {
        if (this.n != android.support.v4.c.ap.a) {
            switch (android.support.v4.c.am.a[(this.n - 1)]) {
                case 1:
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                    break;
                case 2:
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
                    break;
            }
        }
        this.n = android.support.v4.c.ap.b;
        this.m.b = p4;
        p3.execute(this.e);
        return this;
    }

    protected abstract varargs Object a();

    protected void a(Object p1)
    {
        return;
    }

    protected void b(Object p1)
    {
        return;
    }
}
