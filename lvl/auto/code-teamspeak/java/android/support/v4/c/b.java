package android.support.v4.c;
final class b extends android.support.v4.c.ai implements java.lang.Runnable {
    final java.util.concurrent.CountDownLatch a;
    boolean b;
    final synthetic android.support.v4.c.a c;

    b(android.support.v4.c.a p3)
    {
        this.c = p3;
        this.a = new java.util.concurrent.CountDownLatch(1);
        return;
    }

    private varargs Object c()
    {
        try {
            int v0_1 = this.c.d();
        } catch (int v0_2) {
            if (this.e.isCancelled()) {
                v0_1 = 0;
            } else {
                throw v0_2;
            }
        }
        return v0_1;
    }

    private void d()
    {
        try {
            this.a.await();
        } catch (InterruptedException v0) {
        }
        return;
    }

    protected final synthetic Object a()
    {
        return this.c();
    }

    protected final void a(Object p5)
    {
        try {
            java.util.concurrent.CountDownLatch v0_0 = this.c;
        } catch (java.util.concurrent.CountDownLatch v0_1) {
            this.a.countDown();
            throw v0_1;
        }
        if (v0_0.c == this) {
            if (!v0_0.u) {
                v0_0.x = 0;
                v0_0.f = android.os.SystemClock.uptimeMillis();
                v0_0.c = 0;
                v0_0.b(p5);
            } else {
                v0_0.a(p5);
            }
        } else {
            v0_0.a(this, p5);
        }
        this.a.countDown();
        return;
    }

    protected final void b(Object p3)
    {
        try {
            this.c.a(this, p3);
            this.a.countDown();
            return;
        } catch (Throwable v0_2) {
            this.a.countDown();
            throw v0_2;
        }
    }

    public final void run()
    {
        this.b = 0;
        this.c.c();
        return;
    }
}
