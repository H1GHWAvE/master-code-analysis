package android.support.v4.c;
public class h {
    private static final String a = "ContextCompat";
    private static final String b = "Android";
    private static final String c = "data";
    private static final String d = "obb";
    private static final String e = "files";
    private static final String f = "cache";

    public h()
    {
        return;
    }

    public static int a(android.content.Context p2, String p3)
    {
        return p2.checkPermission(p3, android.os.Process.myPid(), android.os.Process.myUid());
    }

    public static final android.graphics.drawable.Drawable a(android.content.Context p2, int p3)
    {
        android.graphics.drawable.Drawable v0_2;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_2 = p2.getResources().getDrawable(p3);
        } else {
            v0_2 = p2.getDrawable(p3);
        }
        return v0_2;
    }

    private static declared_synchronized java.io.File a(java.io.File p4)
    {
        try {
            if ((!p4.exists()) && ((!p4.mkdirs()) && (!p4.exists()))) {
                android.util.Log.w("ContextCompat", new StringBuilder("Unable to create files subdir ").append(p4.getPath()).toString());
                p4 = 0;
            }
        } catch (String v0_4) {
            throw v0_4;
        }
        return p4;
    }

    private static varargs java.io.File a(java.io.File p5, String[] p6)
    {
        int v3 = p6.length;
        int v2 = 0;
        java.io.File v1_0 = p5;
        while (v2 < v3) {
            java.io.File v0_1;
            String v4 = p6[v2];
            if (v1_0 != null) {
                if (v4 == null) {
                    v0_1 = v1_0;
                } else {
                    v0_1 = new java.io.File(v1_0, v4);
                }
            } else {
                v0_1 = new java.io.File(v4);
            }
            v2++;
            v1_0 = v0_1;
        }
        return v1_0;
    }

    public static boolean a(android.content.Context p3, android.content.Intent[] p4)
    {
        int v0 = 1;
        int v1_0 = android.os.Build$VERSION.SDK_INT;
        if (v1_0 < 16) {
            if (v1_0 < 11) {
                v0 = 0;
            } else {
                p3.startActivities(p4);
            }
        } else {
            p3.startActivities(p4, 0);
        }
        return v0;
    }

    private static java.io.File[] a(android.content.Context p6)
    {
        java.io.File[] v0_3;
        java.io.File[] v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 19) {
            java.io.File[] v0_2;
            if (v0_0 < 11) {
                java.io.File[] v0_1 = android.os.Environment.getExternalStorageDirectory();
                java.io.File[] v1_3 = new String[3];
                v1_3[0] = "Android";
                v1_3[1] = "obb";
                v1_3[2] = p6.getPackageName();
                v0_2 = android.support.v4.c.h.a(v0_1, v1_3);
            } else {
                v0_2 = p6.getObbDir();
            }
            java.io.File[] v1_4 = new java.io.File[1];
            v1_4[0] = v0_2;
            v0_3 = v1_4;
        } else {
            v0_3 = p6.getObbDirs();
        }
        return v0_3;
    }

    private static android.content.res.ColorStateList b(android.content.Context p2, int p3)
    {
        android.content.res.ColorStateList v0_2;
        if (android.os.Build$VERSION.SDK_INT < 23) {
            v0_2 = p2.getResources().getColorStateList(p3);
        } else {
            v0_2 = p2.getColorStateList(p3);
        }
        return v0_2;
    }

    private static boolean b(android.content.Context p1, android.content.Intent[] p2)
    {
        return android.support.v4.c.h.a(p1, p2);
    }

    private static java.io.File[] b(android.content.Context p6)
    {
        java.io.File[] v0_3;
        java.io.File[] v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 19) {
            java.io.File[] v0_2;
            if (v0_0 < 8) {
                java.io.File[] v0_1 = android.os.Environment.getExternalStorageDirectory();
                java.io.File[] v1_3 = new String[4];
                v1_3[0] = "Android";
                v1_3[1] = "data";
                v1_3[2] = p6.getPackageName();
                v1_3[3] = "cache";
                v0_2 = android.support.v4.c.h.a(v0_1, v1_3);
            } else {
                v0_2 = p6.getExternalCacheDir();
            }
            java.io.File[] v1_4 = new java.io.File[1];
            v1_4[0] = v0_2;
            v0_3 = v1_4;
        } else {
            v0_3 = p6.getExternalCacheDirs();
        }
        return v0_3;
    }

    private static java.io.File[] b(android.content.Context p6, String p7)
    {
        java.io.File[] v0_3;
        java.io.File[] v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 19) {
            java.io.File[] v0_2;
            if (v0_0 < 8) {
                java.io.File[] v0_1 = android.os.Environment.getExternalStorageDirectory();
                java.io.File[] v1_3 = new String[5];
                v1_3[0] = "Android";
                v1_3[1] = "data";
                v1_3[2] = p6.getPackageName();
                v1_3[3] = "files";
                v1_3[4] = p7;
                v0_2 = android.support.v4.c.h.a(v0_1, v1_3);
            } else {
                v0_2 = p6.getExternalFilesDir(p7);
            }
            java.io.File[] v1_4 = new java.io.File[1];
            v1_4[0] = v0_2;
            v0_3 = v1_4;
        } else {
            v0_3 = p6.getExternalFilesDirs(p7);
        }
        return v0_3;
    }

    private static int c(android.content.Context p2, int p3)
    {
        int v0_2;
        if (android.os.Build$VERSION.SDK_INT < 23) {
            v0_2 = p2.getResources().getColor(p3);
        } else {
            v0_2 = p2.getColor(p3);
        }
        return v0_2;
    }

    private static java.io.File c(android.content.Context p3)
    {
        java.io.File v0_3;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_3 = android.support.v4.c.h.a(new java.io.File(p3.getApplicationInfo().dataDir, "no_backup"));
        } else {
            v0_3 = p3.getNoBackupFilesDir();
        }
        return v0_3;
    }

    private static java.io.File d(android.content.Context p3)
    {
        java.io.File v0_3;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_3 = android.support.v4.c.h.a(new java.io.File(p3.getApplicationInfo().dataDir, "code_cache"));
        } else {
            v0_3 = p3.getCodeCacheDir();
        }
        return v0_3;
    }
}
