package android.support.v4.c;
public final class ae {
    static final int a = 1;
    private static final String b = "LocalBroadcastManager";
    private static final boolean c;
    private static final Object i;
    private static android.support.v4.c.ae j;
    private final android.content.Context d;
    private final java.util.HashMap e;
    private final java.util.HashMap f;
    private final java.util.ArrayList g;
    private final android.os.Handler h;

    static ae()
    {
        android.support.v4.c.ae.i = new Object();
        return;
    }

    private ae(android.content.Context p3)
    {
        this.e = new java.util.HashMap();
        this.f = new java.util.HashMap();
        this.g = new java.util.ArrayList();
        this.d = p3;
        this.h = new android.support.v4.c.af(this, p3.getMainLooper());
        return;
    }

    private static android.support.v4.c.ae a(android.content.Context p3)
    {
        try {
            if (android.support.v4.c.ae.j == null) {
                android.support.v4.c.ae.j = new android.support.v4.c.ae(p3.getApplicationContext());
            }
        } catch (android.support.v4.c.ae v0_4) {
            throw v0_4;
        }
        return android.support.v4.c.ae.j;
    }

    private void a()
    {
        while(true) {
            int v0_1 = this.g.size();
            if (v0_1 <= 0) {
                break;
            }
            android.support.v4.c.ag[] v4 = new android.support.v4.c.ag[v0_1];
            this.g.toArray(v4);
            this.g.clear();
            int v3 = 0;
            while (v3 < v4.length) {
                android.support.v4.c.ag v5 = v4[v3];
                int v1 = 0;
                while (v1 < v5.b.size()) {
                    ((android.support.v4.c.ah) v5.b.get(v1)).b.onReceive(this.d, v5.a);
                    v1++;
                }
                v3++;
            }
        }
        return;
    }

    private void a(android.content.BroadcastReceiver p11)
    {
        try {
            Throwable v0_2 = ((java.util.ArrayList) this.e.remove(p11));
        } catch (Throwable v0_3) {
            throw v0_3;
        }
        if (v0_2 != null) {
            int v7 = 0;
            while (v7 < v0_2.size()) {
                int v1_2 = ((android.content.IntentFilter) v0_2.get(v7));
                int v6 = 0;
                while (v6 < v1_2.countActions()) {
                    String v9 = v1_2.getAction(v6);
                    java.util.HashMap v2_3 = ((java.util.ArrayList) this.f.get(v9));
                    if (v2_3 != null) {
                        int v4 = 0;
                        while (v4 < v2_3.size()) {
                            int v3_4;
                            if (((android.support.v4.c.ah) v2_3.get(v4)).b != p11) {
                                v3_4 = v4;
                            } else {
                                v2_3.remove(v4);
                                v3_4 = (v4 - 1);
                            }
                            v4 = (v3_4 + 1);
                        }
                        if (v2_3.size() <= 0) {
                            this.f.remove(v9);
                        }
                    }
                    v6++;
                }
                v7++;
            }
        } else {
        }
        return;
    }

    private void a(android.content.BroadcastReceiver p7, android.content.IntentFilter p8)
    {
        try {
            android.support.v4.c.ah v3_1 = new android.support.v4.c.ah(p8, p7);
            int v0_2 = ((java.util.ArrayList) this.e.get(p7));
        } catch (int v0_11) {
            throw v0_11;
        }
        if (v0_2 == 0) {
            v0_2 = new java.util.ArrayList(1);
            this.e.put(p7, v0_2);
        }
        v0_2.add(p8);
        int v1_2 = 0;
        while (v1_2 < p8.countActions()) {
            String v4 = p8.getAction(v1_2);
            int v0_8 = ((java.util.ArrayList) this.f.get(v4));
            if (v0_8 == 0) {
                v0_8 = new java.util.ArrayList(1);
                this.f.put(v4, v0_8);
            }
            v0_8.add(v3_1);
            v1_2++;
        }
        return;
    }

    static synthetic void a(android.support.v4.c.ae p0)
    {
        p0.a();
        return;
    }

    private boolean a(android.content.Intent p17)
    {
        try {
            int v12;
            int v2_0 = p17.getAction();
            int v3_0 = p17.resolveTypeIfNeeded(this.d.getContentResolver());
            android.net.Uri v5 = p17.getData();
            String v4 = p17.getScheme();
            java.util.Set v6 = p17.getCategories();
        } catch (String v1_36) {
            throw v1_36;
        }
        if ((p17.getFlags() & 8) == 0) {
            v12 = 0;
        } else {
            v12 = 1;
        }
        if (v12 != 0) {
            android.util.Log.v("LocalBroadcastManager", new StringBuilder("Resolving type ").append(v3_0).append(" scheme ").append(v4).append(" of intent ").append(p17).toString());
        }
        String v1_12;
        java.util.ArrayList v8_3 = ((java.util.ArrayList) this.f.get(p17.getAction()));
        if (v8_3 == null) {
            v1_12 = 0;
        } else {
            if (v12 != 0) {
                android.util.Log.v("LocalBroadcastManager", new StringBuilder("Action list: ").append(v8_3).toString());
            }
            boolean v10 = 0;
            int v11 = 0;
            while (v11 < v8_3.size()) {
                StringBuilder v9_1 = ((android.support.v4.c.ah) v8_3.get(v11));
                if (v12 != 0) {
                    android.util.Log.v("LocalBroadcastManager", new StringBuilder("Matching against filter ").append(v9_1.a).toString());
                }
                String v1_33;
                if (!v9_1.c) {
                    String v1_26 = v9_1.a.match(v2_0, v3_0, v4, v5, v6, "LocalBroadcastManager");
                    if (v1_26 < null) {
                        if (v12 == 0) {
                            v1_33 = v10;
                        } else {
                            String v1_27;
                            switch (v1_26) {
                                case -4:
                                    v1_27 = "category";
                                    break;
                                case -3:
                                    v1_27 = "action";
                                    break;
                                case -2:
                                    v1_27 = "data";
                                    break;
                                case -1:
                                    v1_27 = "type";
                                    break;
                                default:
                                    v1_27 = "unknown reason";
                            }
                            android.util.Log.v("LocalBroadcastManager", new StringBuilder("  Filter did not match: ").append(v1_27).toString());
                        }
                    } else {
                        if (v12 != 0) {
                            android.util.Log.v("LocalBroadcastManager", new StringBuilder("  Filter matched!  match=0x").append(Integer.toHexString(v1_26)).toString());
                        }
                        if (v10) {
                            v1_33 = v10;
                        } else {
                            v1_33 = new java.util.ArrayList();
                        }
                        v1_33.add(v9_1);
                        v9_1.c = 1;
                    }
                } else {
                    if (v12 == 0) {
                    } else {
                        android.util.Log.v("LocalBroadcastManager", "  Filter\'s target already added");
                        v1_33 = v10;
                    }
                }
                v11++;
                v10 = v1_33;
            }
            if (!v10) {
            } else {
                int v2_1 = 0;
                while (v2_1 < v10.size()) {
                    ((android.support.v4.c.ah) v10.get(v2_1)).c = 0;
                    v2_1++;
                }
                this.g.add(new android.support.v4.c.ag(p17, v10));
                if (!this.h.hasMessages(1)) {
                    this.h.sendEmptyMessage(1);
                }
                v1_12 = 1;
            }
        }
        return v1_12;
    }

    private void b(android.content.Intent p2)
    {
        if (this.a(p2)) {
            this.a();
        }
        return;
    }
}
