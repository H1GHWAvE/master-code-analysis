package android.support.v4.e.a;
final class o {

    o()
    {
        return;
    }

    private static void a(android.graphics.drawable.Drawable p0, boolean p1)
    {
        p0.setAutoMirrored(p1);
        return;
    }

    private static boolean a(android.graphics.drawable.Drawable p1)
    {
        return p1.isAutoMirrored();
    }

    private static android.graphics.drawable.Drawable b(android.graphics.drawable.Drawable p1)
    {
        if (!(p1 instanceof android.support.v4.e.a.t)) {
            p1 = new android.support.v4.e.a.t(p1);
        }
        return p1;
    }
}
