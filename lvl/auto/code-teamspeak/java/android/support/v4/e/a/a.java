package android.support.v4.e.a;
public final class a {
    static final android.support.v4.e.a.c a;

    static a()
    {
        android.support.v4.e.a.b v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 23) {
            if (v0_0 < 22) {
                if (v0_0 < 21) {
                    if (v0_0 < 19) {
                        if (v0_0 < 17) {
                            if (v0_0 < 11) {
                                android.support.v4.e.a.a.a = new android.support.v4.e.a.b();
                            } else {
                                android.support.v4.e.a.a.a = new android.support.v4.e.a.d();
                            }
                        } else {
                            android.support.v4.e.a.a.a = new android.support.v4.e.a.e();
                        }
                    } else {
                        android.support.v4.e.a.a.a = new android.support.v4.e.a.f();
                    }
                } else {
                    android.support.v4.e.a.a.a = new android.support.v4.e.a.g();
                }
            } else {
                android.support.v4.e.a.a.a = new android.support.v4.e.a.h();
            }
        } else {
            android.support.v4.e.a.a.a = new android.support.v4.e.a.i();
        }
        return;
    }

    public a()
    {
        return;
    }

    public static void a(android.graphics.drawable.Drawable p1)
    {
        android.support.v4.e.a.a.a.a(p1);
        return;
    }

    public static void a(android.graphics.drawable.Drawable p1, float p2, float p3)
    {
        android.support.v4.e.a.a.a.a(p1, p2, p3);
        return;
    }

    public static void a(android.graphics.drawable.Drawable p1, int p2)
    {
        android.support.v4.e.a.a.a.a(p1, p2);
        return;
    }

    public static void a(android.graphics.drawable.Drawable p6, int p7, int p8, int p9, int p10)
    {
        android.support.v4.e.a.a.a.a(p6, p7, p8, p9, p10);
        return;
    }

    public static void a(android.graphics.drawable.Drawable p1, android.content.res.ColorStateList p2)
    {
        android.support.v4.e.a.a.a.a(p1, p2);
        return;
    }

    public static void a(android.graphics.drawable.Drawable p1, android.graphics.PorterDuff$Mode p2)
    {
        android.support.v4.e.a.a.a.a(p1, p2);
        return;
    }

    public static void a(android.graphics.drawable.Drawable p1, boolean p2)
    {
        android.support.v4.e.a.a.a.a(p1, p2);
        return;
    }

    public static void b(android.graphics.drawable.Drawable p1, int p2)
    {
        android.support.v4.e.a.a.a.b(p1, p2);
        return;
    }

    public static boolean b(android.graphics.drawable.Drawable p1)
    {
        return android.support.v4.e.a.a.a.b(p1);
    }

    public static android.graphics.drawable.Drawable c(android.graphics.drawable.Drawable p1)
    {
        return android.support.v4.e.a.a.a.c(p1);
    }

    public static android.graphics.drawable.Drawable d(android.graphics.drawable.Drawable p1)
    {
        if ((p1 instanceof android.support.v4.e.a.q)) {
            p1 = ((android.support.v4.e.a.q) p1).a();
        }
        return p1;
    }

    public static int e(android.graphics.drawable.Drawable p1)
    {
        return android.support.v4.e.a.a.a.d(p1);
    }
}
