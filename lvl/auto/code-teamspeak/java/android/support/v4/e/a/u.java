package android.support.v4.e.a;
final class u extends android.support.v4.e.a.t {

    u(android.graphics.drawable.Drawable p1)
    {
        this(p1);
        return;
    }

    public final void applyTheme(android.content.res.Resources$Theme p2)
    {
        this.b.applyTheme(p2);
        return;
    }

    public final boolean canApplyTheme()
    {
        return this.b.canApplyTheme();
    }

    public final android.graphics.Rect getDirtyBounds()
    {
        return this.b.getDirtyBounds();
    }

    public final void getOutline(android.graphics.Outline p2)
    {
        this.b.getOutline(p2);
        return;
    }

    public final void setHotspot(float p2, float p3)
    {
        this.b.setHotspot(p2, p3);
        return;
    }

    public final void setHotspotBounds(int p2, int p3, int p4, int p5)
    {
        this.b.setHotspotBounds(p2, p3, p4, p5);
        return;
    }
}
