package android.support.v4.e.a;
 class f extends android.support.v4.e.a.e {

    f()
    {
        return;
    }

    public final void a(android.graphics.drawable.Drawable p1, boolean p2)
    {
        p1.setAutoMirrored(p2);
        return;
    }

    public final boolean b(android.graphics.drawable.Drawable p2)
    {
        return p2.isAutoMirrored();
    }

    public android.graphics.drawable.Drawable c(android.graphics.drawable.Drawable p2)
    {
        if (!(p2 instanceof android.support.v4.e.a.t)) {
            p2 = new android.support.v4.e.a.t(p2);
        }
        return p2;
    }
}
