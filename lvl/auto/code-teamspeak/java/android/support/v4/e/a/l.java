package android.support.v4.e.a;
final class l {

    l()
    {
        return;
    }

    private static android.graphics.drawable.Drawable a(android.graphics.drawable.Drawable p1)
    {
        if (!(p1 instanceof android.support.v4.e.a.r)) {
            p1 = new android.support.v4.e.a.r(p1);
        }
        return p1;
    }

    public static void a(android.graphics.drawable.Drawable p1, int p2)
    {
        if ((p1 instanceof android.support.v4.e.a.q)) {
            ((android.support.v4.e.a.q) p1).setTint(p2);
        }
        return;
    }

    public static void a(android.graphics.drawable.Drawable p1, android.content.res.ColorStateList p2)
    {
        if ((p1 instanceof android.support.v4.e.a.q)) {
            ((android.support.v4.e.a.q) p1).setTintList(p2);
        }
        return;
    }

    public static void a(android.graphics.drawable.Drawable p1, android.graphics.PorterDuff$Mode p2)
    {
        if ((p1 instanceof android.support.v4.e.a.q)) {
            ((android.support.v4.e.a.q) p1).setTintMode(p2);
        }
        return;
    }
}
