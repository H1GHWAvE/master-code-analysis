package android.support.v4.e;
public final class j {
    private static final int a = 10;
    private static final int b = 10;

    private j()
    {
        return;
    }

    private static double a(int p8)
    {
        double v0_5;
        double v0_2 = (((double) android.graphics.Color.red(p8)) / 255.0);
        if (v0_2 >= 0.03928) {
            v0_5 = Math.pow(((v0_2 + 0.055) / 1.055), 2.4);
        } else {
            v0_5 = (v0_2 / 12.92);
        }
        double v2_12;
        double v2_9 = (((double) android.graphics.Color.green(p8)) / 255.0);
        if (v2_9 >= 0.03928) {
            v2_12 = Math.pow(((v2_9 + 0.055) / 1.055), 2.4);
        } else {
            v2_12 = (v2_9 / 12.92);
        }
        double v4_12;
        double v4_9 = (((double) android.graphics.Color.blue(p8)) / 255.0);
        if (v4_9 >= 0.03928) {
            v4_12 = Math.pow(((v4_9 + 0.055) / 1.055), 2.4);
        } else {
            v4_12 = (v4_9 / 12.92);
        }
        return (((v0_5 * 0.2126) + (v2_12 * 0.7152)) + (0.0722 * v4_12));
    }

    private static float a(float p2, float p3)
    {
        if (p2 >= 0) {
            if (p2 <= p3) {
                p3 = p2;
            }
        } else {
            p3 = 0;
        }
        return p3;
    }

    public static int a(int p7, int p8)
    {
        int v0_0 = android.graphics.Color.alpha(p8);
        int v1 = android.graphics.Color.alpha(p7);
        int v2_3 = (255 - (((255 - v0_0) * (255 - v1)) / 255));
        return android.graphics.Color.argb(v2_3, android.support.v4.e.j.a(android.graphics.Color.red(p7), v1, android.graphics.Color.red(p8), v0_0, v2_3), android.support.v4.e.j.a(android.graphics.Color.green(p7), v1, android.graphics.Color.green(p8), v0_0, v2_3), android.support.v4.e.j.a(android.graphics.Color.blue(p7), v1, android.graphics.Color.blue(p8), v0_0, v2_3));
    }

    private static int a(int p9, int p10, float p11)
    {
        int v2_0 = 0;
        int v0_0 = 255;
        if (android.graphics.Color.alpha(p10) == 255) {
            if (android.support.v4.e.j.d(android.support.v4.e.j.b(p9, 255), p10) >= ((double) p11)) {
                int v3 = 0;
                while ((v3 <= 10) && ((v0_0 - v2_0) > 10)) {
                    int v1_5 = ((v2_0 + v0_0) / 2);
                    if (android.support.v4.e.j.d(android.support.v4.e.j.b(p9, v1_5), p10) >= ((double) p11)) {
                        v0_0 = v1_5;
                        v1_5 = v2_0;
                    }
                    v3++;
                    v2_0 = v1_5;
                }
            } else {
                v0_0 = -1;
            }
            return v0_0;
        } else {
            throw new IllegalArgumentException("background can not be translucent");
        }
    }

    private static int a(int p3, int p4, int p5, int p6, int p7)
    {
        int v0_3;
        if (p7 != 0) {
            v0_3 = ((((p3 * 255) * p4) + ((p5 * p6) * (255 - p4))) / (p7 * 255));
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private static int a(float[] p8)
    {
        int v2_10;
        int v1_3;
        int v0_0 = 0;
        int v1_0 = p8[0];
        float v3_1 = p8[2];
        float v4_4 = ((1065353216 - Math.abs(((1073741824 * v3_1) - 1065353216))) * p8[1]);
        float v3_2 = (v3_1 - (1056964608 * v4_4));
        float v5_1 = (v4_4 * (1065353216 - Math.abs((((v1_0 / 1114636288) % 1073741824) - 1065353216))));
        switch ((((int) v1_0) / 60)) {
            case 0:
                v2_10 = Math.round(((v4_4 + v3_2) * 1132396544));
                v1_3 = Math.round(((v5_1 + v3_2) * 1132396544));
                v0_0 = Math.round((1132396544 * v3_2));
                break;
            case 1:
                v2_10 = Math.round(((v5_1 + v3_2) * 1132396544));
                v1_3 = Math.round(((v4_4 + v3_2) * 1132396544));
                v0_0 = Math.round((1132396544 * v3_2));
                break;
            case 2:
                v2_10 = Math.round((1132396544 * v3_2));
                v1_3 = Math.round(((v4_4 + v3_2) * 1132396544));
                v0_0 = Math.round(((v5_1 + v3_2) * 1132396544));
                break;
            case 3:
                v2_10 = Math.round((1132396544 * v3_2));
                v1_3 = Math.round(((v5_1 + v3_2) * 1132396544));
                v0_0 = Math.round(((v4_4 + v3_2) * 1132396544));
                break;
            case 4:
                v2_10 = Math.round(((v5_1 + v3_2) * 1132396544));
                v1_3 = Math.round((1132396544 * v3_2));
                v0_0 = Math.round(((v4_4 + v3_2) * 1132396544));
                break;
            case 5:
            case 6:
                v2_10 = Math.round(((v4_4 + v3_2) * 1132396544));
                v1_3 = Math.round((1132396544 * v3_2));
                v0_0 = Math.round(((v5_1 + v3_2) * 1132396544));
                break;
            default:
                v1_3 = 0;
                v2_10 = 0;
        }
        return android.graphics.Color.rgb(android.support.v4.e.j.b(v2_10), android.support.v4.e.j.b(v1_3), android.support.v4.e.j.b(v0_0));
    }

    private static void a(int p11, int p12, int p13, float[] p14)
    {
        float v0_4;
        int v1_8;
        float v0_1 = (((float) p11) / 1132396544);
        int v1_1 = (((float) p12) / 1132396544);
        int v3_1 = (((float) p13) / 1132396544);
        float v4_2 = Math.max(v0_1, Math.max(v1_1, v3_1));
        float v5_1 = Math.min(v0_1, Math.min(v1_1, v3_1));
        float v6 = (v4_2 - v5_1);
        float v7_1 = ((v4_2 + v5_1) / 1073741824);
        if (v4_2 != v5_1) {
            if (v4_2 != v0_1) {
                if (v4_2 != v1_1) {
                    v0_4 = (((v0_1 - v1_1) / v6) + 1082130432);
                } else {
                    v0_4 = (((v3_1 - v0_1) / v6) + 1073741824);
                }
            } else {
                v0_4 = (((v1_1 - v3_1) / v6) % 1086324736);
            }
            v1_8 = (v6 / (1065353216 - Math.abs(((1073741824 * v7_1) - 1065353216))));
        } else {
            v1_8 = 0;
            v0_4 = 0;
        }
        float v0_10 = ((v0_4 * 1114636288) % 1135869952);
        if (v0_10 < 0) {
            v0_10 += 1135869952;
        }
        p14[0] = android.support.v4.e.j.a(v0_10, 1135869952);
        p14[1] = android.support.v4.e.j.a(v1_8, 1065353216);
        p14[2] = android.support.v4.e.j.a(v7_1, 1065353216);
        return;
    }

    private static void a(int p11, float[] p12)
    {
        int v1_9;
        float v0_5;
        float v0_2 = (((float) android.graphics.Color.red(p11)) / 1132396544);
        int v1_2 = (((float) android.graphics.Color.green(p11)) / 1132396544);
        int v3_2 = (((float) android.graphics.Color.blue(p11)) / 1132396544);
        float v4_2 = Math.max(v0_2, Math.max(v1_2, v3_2));
        float v5_1 = Math.min(v0_2, Math.min(v1_2, v3_2));
        float v6 = (v4_2 - v5_1);
        float v7_1 = ((v4_2 + v5_1) / 1073741824);
        if (v4_2 != v5_1) {
            if (v4_2 != v0_2) {
                if (v4_2 != v1_2) {
                    v0_5 = (((v0_2 - v1_2) / v6) + 1082130432);
                } else {
                    v0_5 = (((v3_2 - v0_2) / v6) + 1073741824);
                }
            } else {
                v0_5 = (((v1_2 - v3_2) / v6) % 1086324736);
            }
            v1_9 = (v6 / (1065353216 - Math.abs(((1073741824 * v7_1) - 1065353216))));
        } else {
            v1_9 = 0;
            v0_5 = 0;
        }
        float v0_11 = ((v0_5 * 1114636288) % 1135869952);
        if (v0_11 < 0) {
            v0_11 += 1135869952;
        }
        p12[0] = android.support.v4.e.j.a(v0_11, 1135869952);
        p12[1] = android.support.v4.e.j.a(v1_9, 1065353216);
        p12[2] = android.support.v4.e.j.a(v7_1, 1065353216);
        return;
    }

    private static int b(int p1)
    {
        if (p1 >= 0) {
            if (p1 > 255) {
                p1 = 255;
            }
        } else {
            p1 = 0;
        }
        return p1;
    }

    public static int b(int p2, int p3)
    {
        if ((p3 >= 0) && (p3 <= 255)) {
            return ((16777215 & p2) | (p3 << 24));
        } else {
            throw new IllegalArgumentException("alpha must be between 0 and 255.");
        }
    }

    private static int c(int p2, int p3)
    {
        return (255 - (((255 - p3) * (255 - p2)) / 255));
    }

    private static double d(int p6, int p7)
    {
        if (android.graphics.Color.alpha(p7) == 255) {
            if (android.graphics.Color.alpha(p6) < 255) {
                p6 = android.support.v4.e.j.a(p6, p7);
            }
            double v0_3 = (android.support.v4.e.j.a(p6) + 0.05);
            double v2_1 = (android.support.v4.e.j.a(p7) + 0.05);
            return (Math.max(v0_3, v2_1) / Math.min(v0_3, v2_1));
        } else {
            throw new IllegalArgumentException("background can not be translucent");
        }
    }
}
