package android.support.v4.k;
final class b {
    private static final String a = "DocumentFile";

    b()
    {
        return;
    }

    public static String a(android.content.Context p2, android.net.Uri p3)
    {
        int v0_1 = android.support.v4.k.b.a(p2, p3, "mime_type");
        if ("vnd.android.document/directory".equals(v0_1)) {
            v0_1 = 0;
        }
        return v0_1;
    }

    static String a(android.content.Context p7, android.net.Uri p8, String p9)
    {
        String v0_0 = p7.getContentResolver();
        try {
            String v2_0 = new String[1];
            v2_0[0] = p9;
            int v1_3 = v0_0.query(p8, v2_0, 0, 0, 0);
            try {
                if ((!v1_3.moveToFirst()) || (v1_3.isNull(0))) {
                    android.support.v4.k.b.a(v1_3);
                    String v0_8 = 0;
                    return v0_8;
                } else {
                    v0_8 = v1_3.getString(0);
                    android.support.v4.k.b.a(v1_3);
                    return v0_8;
                }
            } catch (String v0_1) {
                android.util.Log.w("DocumentFile", new StringBuilder("Failed query: ").append(v0_1).toString());
                android.support.v4.k.b.a(v1_3);
                v0_8 = 0;
                return v0_8;
            }
        } catch (String v0_2) {
            v1_3 = 0;
            android.support.v4.k.b.a(v1_3);
            throw v0_2;
        } catch (String v0_1) {
            v1_3 = 0;
        } catch (String v0_2) {
        }
    }

    private static void a(AutoCloseable p1)
    {
        if (p1 != null) {
            try {
                p1.close();
            } catch (Exception v0) {
                throw v0;
            } catch (Exception v0) {
            }
        }
        return;
    }

    static long b(android.content.Context p9, android.net.Uri p10, String p11)
    {
        long v0_0 = p9.getContentResolver();
        try {
            String v2_0 = new String[1];
            v2_0[0] = p11;
            String v2_1 = v0_0.query(p10, v2_0, 0, 0, 0);
            try {
                if ((!v2_1.moveToFirst()) || (v2_1.isNull(0))) {
                    android.support.v4.k.b.a(v2_1);
                    long v0_8 = 0;
                    return v0_8;
                } else {
                    v0_8 = v2_1.getLong(0);
                    android.support.v4.k.b.a(v2_1);
                    return v0_8;
                }
            } catch (long v0_1) {
                String v1_3 = v2_1;
                try {
                    android.util.Log.w("DocumentFile", new StringBuilder("Failed query: ").append(v0_1).toString());
                    android.support.v4.k.b.a(v1_3);
                    v0_8 = 0;
                } catch (long v0_2) {
                    android.support.v4.k.b.a(v1_3);
                    throw v0_2;
                }
                return v0_8;
            } catch (long v0_2) {
                v1_3 = v2_1;
            }
        } catch (long v0_1) {
            v1_3 = 0;
        } catch (long v0_2) {
            v1_3 = 0;
        }
    }

    public static boolean b(android.content.Context p2, android.net.Uri p3)
    {
        return "vnd.android.document/directory".equals(android.support.v4.k.b.a(p2, p3, "mime_type"));
    }

    private static int c(android.content.Context p2, android.net.Uri p3, String p4)
    {
        return ((int) android.support.v4.k.b.b(p2, p3, p4));
    }

    public static boolean c(android.content.Context p2, android.net.Uri p3)
    {
        int v0_3;
        int v0_1 = android.support.v4.k.b.a(p2, p3, "mime_type");
        if ((!"vnd.android.document/directory".equals(v0_1)) && (!android.text.TextUtils.isEmpty(v0_1))) {
            v0_3 = 1;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public static boolean d(android.content.Context p3, android.net.Uri p4)
    {
        int v0 = 0;
        if ((p3.checkCallingOrSelfUriPermission(p4, 1) == 0) && (!android.text.TextUtils.isEmpty(android.support.v4.k.b.a(p3, p4, "mime_type")))) {
            v0 = 1;
        }
        return v0;
    }

    public static boolean e(android.content.Context p6, android.net.Uri p7)
    {
        int v0 = 0;
        if (p6.checkCallingOrSelfUriPermission(p7, 2) == 0) {
            int v2_3 = android.support.v4.k.b.a(p6, p7, "mime_type");
            int v3_1 = ((int) android.support.v4.k.b.b(p6, p7, "flags"));
            if (!android.text.TextUtils.isEmpty(v2_3)) {
                if ((v3_1 & 4) == 0) {
                    if ((!"vnd.android.document/directory".equals(v2_3)) || ((v3_1 & 8) == 0)) {
                        if ((!android.text.TextUtils.isEmpty(v2_3)) && ((v3_1 & 2) != 0)) {
                            v0 = 1;
                        }
                    } else {
                        v0 = 1;
                    }
                } else {
                    v0 = 1;
                }
            }
        }
        return v0;
    }

    public static boolean f(android.content.Context p1, android.net.Uri p2)
    {
        return android.provider.DocumentsContract.deleteDocument(p1.getContentResolver(), p2);
    }

    public static boolean g(android.content.Context p9, android.net.Uri p10)
    {
        int v0_0 = p9.getContentResolver();
        try {
            String v2_0 = new String[1];
            v2_0[0] = "document_id";
            int v1_3 = v0_0.query(p10, v2_0, 0, 0, 0);
            try {
                int v0_6;
                if (v1_3.getCount() <= 0) {
                    v0_6 = 0;
                } else {
                    v0_6 = 1;
                }
            } catch (int v0_1) {
                android.util.Log.w("DocumentFile", new StringBuilder("Failed query: ").append(v0_1).toString());
                android.support.v4.k.b.a(v1_3);
                v0_6 = 0;
                return v0_6;
            }
            android.support.v4.k.b.a(v1_3);
            return v0_6;
        } catch (int v0_1) {
            v1_3 = 0;
        } catch (int v0_2) {
            v1_3 = 0;
            android.support.v4.k.b.a(v1_3);
            throw v0_2;
        } catch (int v0_2) {
        }
    }

    private static boolean h(android.content.Context p1, android.net.Uri p2)
    {
        return android.provider.DocumentsContract.isDocumentUri(p1, p2);
    }

    private static String i(android.content.Context p1, android.net.Uri p2)
    {
        return android.support.v4.k.b.a(p1, p2, "_display_name");
    }

    private static String j(android.content.Context p1, android.net.Uri p2)
    {
        return android.support.v4.k.b.a(p1, p2, "mime_type");
    }

    private static long k(android.content.Context p2, android.net.Uri p3)
    {
        return android.support.v4.k.b.b(p2, p3, "last_modified");
    }

    private static long l(android.content.Context p2, android.net.Uri p3)
    {
        return android.support.v4.k.b.b(p2, p3, "_size");
    }
}
