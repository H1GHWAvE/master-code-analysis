package android.support.v4.k;
final class f extends android.support.v4.k.a {
    private android.content.Context b;
    private android.net.Uri c;

    f(android.support.v4.k.a p1, android.content.Context p2, android.net.Uri p3)
    {
        this(p1);
        this.b = p2;
        this.c = p3;
        return;
    }

    public final android.net.Uri a()
    {
        return this.c;
    }

    public final android.support.v4.k.a a(String p4)
    {
        int v0_1;
        android.net.Uri v1_1 = android.support.v4.k.c.a(this.b, this.c, "vnd.android.document/directory", p4);
        if (v1_1 == null) {
            v0_1 = 0;
        } else {
            v0_1 = new android.support.v4.k.f(this, this.b, v1_1);
        }
        return v0_1;
    }

    public final android.support.v4.k.a a(String p4, String p5)
    {
        int v0_1;
        android.net.Uri v1_1 = android.support.v4.k.c.a(this.b, this.c, p4, p5);
        if (v1_1 == null) {
            v0_1 = 0;
        } else {
            v0_1 = new android.support.v4.k.f(this, this.b, v1_1);
        }
        return v0_1;
    }

    public final String b()
    {
        return android.support.v4.k.b.a(this.b, this.c, "_display_name");
    }

    public final boolean b(String p3)
    {
        int v0_3;
        int v0_2 = android.provider.DocumentsContract.renameDocument(this.b.getContentResolver(), this.c, p3);
        if (v0_2 == 0) {
            v0_3 = 0;
        } else {
            this.c = v0_2;
            v0_3 = 1;
        }
        return v0_3;
    }

    public final String c()
    {
        return android.support.v4.k.b.a(this.b, this.c);
    }

    public final boolean d()
    {
        return android.support.v4.k.b.b(this.b, this.c);
    }

    public final boolean e()
    {
        return android.support.v4.k.b.c(this.b, this.c);
    }

    public final long f()
    {
        return android.support.v4.k.b.b(this.b, this.c, "last_modified");
    }

    public final long g()
    {
        return android.support.v4.k.b.b(this.b, this.c, "_size");
    }

    public final boolean h()
    {
        return android.support.v4.k.b.d(this.b, this.c);
    }

    public final boolean i()
    {
        return android.support.v4.k.b.e(this.b, this.c);
    }

    public final boolean j()
    {
        return android.support.v4.k.b.f(this.b, this.c);
    }

    public final boolean k()
    {
        return android.support.v4.k.b.g(this.b, this.c);
    }

    public final android.support.v4.k.a[] l()
    {
        android.net.Uri[] v1_1 = android.support.v4.k.c.a(this.b, this.c);
        android.support.v4.k.a[] v2 = new android.support.v4.k.a[v1_1.length];
        int v0_2 = 0;
        while (v0_2 < v1_1.length) {
            v2[v0_2] = new android.support.v4.k.f(this, this.b, v1_1[v0_2]);
            v0_2++;
        }
        return v2;
    }
}
