package android.support.v7.b.a;
public class b extends android.graphics.drawable.Drawable implements android.graphics.drawable.Drawable$Callback {
    public android.graphics.drawable.Drawable p;

    public b(android.graphics.drawable.Drawable p3)
    {
        if (this.p != null) {
            this.p.setCallback(0);
        }
        this.p = p3;
        if (p3 != null) {
            p3.setCallback(this);
        }
        return;
    }

    private android.graphics.drawable.Drawable a()
    {
        return this.p;
    }

    private void a(android.graphics.drawable.Drawable p3)
    {
        if (this.p != null) {
            this.p.setCallback(0);
        }
        this.p = p3;
        if (p3 != null) {
            p3.setCallback(this);
        }
        return;
    }

    public void draw(android.graphics.Canvas p2)
    {
        this.p.draw(p2);
        return;
    }

    public int getChangingConfigurations()
    {
        return this.p.getChangingConfigurations();
    }

    public android.graphics.drawable.Drawable getCurrent()
    {
        return this.p.getCurrent();
    }

    public int getIntrinsicHeight()
    {
        return this.p.getIntrinsicHeight();
    }

    public int getIntrinsicWidth()
    {
        return this.p.getIntrinsicWidth();
    }

    public int getMinimumHeight()
    {
        return this.p.getMinimumHeight();
    }

    public int getMinimumWidth()
    {
        return this.p.getMinimumWidth();
    }

    public int getOpacity()
    {
        return this.p.getOpacity();
    }

    public boolean getPadding(android.graphics.Rect p2)
    {
        return this.p.getPadding(p2);
    }

    public int[] getState()
    {
        return this.p.getState();
    }

    public android.graphics.Region getTransparentRegion()
    {
        return this.p.getTransparentRegion();
    }

    public void invalidateDrawable(android.graphics.drawable.Drawable p1)
    {
        this.invalidateSelf();
        return;
    }

    public boolean isAutoMirrored()
    {
        return android.support.v4.e.a.a.b(this.p);
    }

    public boolean isStateful()
    {
        return this.p.isStateful();
    }

    public void jumpToCurrentState()
    {
        android.support.v4.e.a.a.a(this.p);
        return;
    }

    public void onBoundsChange(android.graphics.Rect p2)
    {
        this.p.setBounds(p2);
        return;
    }

    protected boolean onLevelChange(int p2)
    {
        return this.p.setLevel(p2);
    }

    public void scheduleDrawable(android.graphics.drawable.Drawable p2, Runnable p3, long p4)
    {
        this.scheduleSelf(p3, p4);
        return;
    }

    public void setAlpha(int p2)
    {
        this.p.setAlpha(p2);
        return;
    }

    public void setAutoMirrored(boolean p2)
    {
        android.support.v4.e.a.a.a(this.p, p2);
        return;
    }

    public void setChangingConfigurations(int p2)
    {
        this.p.setChangingConfigurations(p2);
        return;
    }

    public void setColorFilter(android.graphics.ColorFilter p2)
    {
        this.p.setColorFilter(p2);
        return;
    }

    public void setDither(boolean p2)
    {
        this.p.setDither(p2);
        return;
    }

    public void setFilterBitmap(boolean p2)
    {
        this.p.setFilterBitmap(p2);
        return;
    }

    public void setHotspot(float p2, float p3)
    {
        android.support.v4.e.a.a.a(this.p, p2, p3);
        return;
    }

    public void setHotspotBounds(int p2, int p3, int p4, int p5)
    {
        android.support.v4.e.a.a.a(this.p, p2, p3, p4, p5);
        return;
    }

    public boolean setState(int[] p2)
    {
        return this.p.setState(p2);
    }

    public void setTint(int p2)
    {
        android.support.v4.e.a.a.a(this.p, p2);
        return;
    }

    public void setTintList(android.content.res.ColorStateList p2)
    {
        android.support.v4.e.a.a.a(this.p, p2);
        return;
    }

    public void setTintMode(android.graphics.PorterDuff$Mode p2)
    {
        android.support.v4.e.a.a.a(this.p, p2);
        return;
    }

    public boolean setVisible(boolean p2, boolean p3)
    {
        if ((!super.setVisible(p2, p3)) && (!this.p.setVisible(p2, p3))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public void unscheduleDrawable(android.graphics.drawable.Drawable p1, Runnable p2)
    {
        this.unscheduleSelf(p2);
        return;
    }
}
