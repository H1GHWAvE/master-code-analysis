package android.support.v7.internal.view;
public final class i {
    public final java.util.ArrayList a;
    android.support.v4.view.gd b;
    boolean c;
    private long d;
    private android.view.animation.Interpolator e;
    private final android.support.v4.view.ge f;

    public i()
    {
        this.d = -1;
        this.f = new android.support.v7.internal.view.j(this);
        this.a = new java.util.ArrayList();
        return;
    }

    private static synthetic android.support.v4.view.gd a(android.support.v7.internal.view.i p1)
    {
        return p1.b;
    }

    private android.support.v7.internal.view.i a(android.support.v4.view.fk p3, android.support.v4.view.fk p4)
    {
        java.util.ArrayList v0_4;
        this.a.add(p3);
        java.util.ArrayList v0_3 = ((android.view.View) p3.a.get());
        if (v0_3 == null) {
            v0_4 = 0;
        } else {
            v0_4 = android.support.v4.view.fk.c.a(v0_3);
        }
        p4.b(v0_4);
        this.a.add(p4);
        return this;
    }

    private static synthetic void b(android.support.v7.internal.view.i p1)
    {
        p1.c = 0;
        return;
    }

    private static synthetic java.util.ArrayList c(android.support.v7.internal.view.i p1)
    {
        return p1.a;
    }

    private void d()
    {
        this.c = 0;
        return;
    }

    public final android.support.v7.internal.view.i a(android.support.v4.view.fk p2)
    {
        if (!this.c) {
            this.a.add(p2);
        }
        return this;
    }

    public final android.support.v7.internal.view.i a(android.support.v4.view.gd p2)
    {
        if (!this.c) {
            this.b = p2;
        }
        return this;
    }

    public final android.support.v7.internal.view.i a(android.view.animation.Interpolator p2)
    {
        if (!this.c) {
            this.e = p2;
        }
        return this;
    }

    public final void a()
    {
        if (!this.c) {
            java.util.Iterator v1 = this.a.iterator();
            while (v1.hasNext()) {
                int v0_5 = ((android.support.v4.view.fk) v1.next());
                if (this.d >= 0) {
                    v0_5.a(this.d);
                }
                if (this.e != null) {
                    v0_5.a(this.e);
                }
                if (this.b != null) {
                    v0_5.a(this.f);
                }
                v0_5.b();
            }
            this.c = 1;
        }
        return;
    }

    public final void b()
    {
        if (this.c) {
            java.util.Iterator v1 = this.a.iterator();
            while (v1.hasNext()) {
                ((android.support.v4.view.fk) v1.next()).a();
            }
            this.c = 0;
        }
        return;
    }

    public final android.support.v7.internal.view.i c()
    {
        if (!this.c) {
            this.d = 250;
        }
        return this;
    }
}
