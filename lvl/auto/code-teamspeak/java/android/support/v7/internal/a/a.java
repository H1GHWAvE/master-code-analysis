package android.support.v7.internal.a;
public final class a {
    static final Class[] a = None;
    private static final String b = "AppCompatViewInflater";
    private static final java.util.Map c;
    private final Object[] d;

    static a()
    {
        android.support.v4.n.a v0_1 = new Class[2];
        v0_1[0] = android.content.Context;
        v0_1[1] = android.util.AttributeSet;
        android.support.v7.internal.a.a.a = v0_1;
        android.support.v7.internal.a.a.c = new android.support.v4.n.a();
        return;
    }

    public a()
    {
        Object[] v0_1 = new Object[2];
        this.d = v0_1;
        return;
    }

    public static android.content.Context a(android.content.Context p4, android.util.AttributeSet p5, boolean p6)
    {
        android.support.v7.internal.view.b v0_1;
        android.content.res.TypedArray v2 = p4.obtainStyledAttributes(p5, android.support.v7.a.n.View, 0, 0);
        if (!p6) {
            v0_1 = 0;
        } else {
            v0_1 = v2.getResourceId(android.support.v7.a.n.View_android_theme, 0);
        }
        if (v0_1 == null) {
            v0_1 = v2.getResourceId(android.support.v7.a.n.View_theme, 0);
            if (v0_1 != null) {
                android.util.Log.i("AppCompatViewInflater", "app:theme is now deprecated. Please move to using android:theme instead.");
            }
        }
        String v1_2 = v0_1;
        v2.recycle();
        if ((v1_2 != null) && ((!(p4 instanceof android.support.v7.internal.view.b)) || (((android.support.v7.internal.view.b) p4).a != v1_2))) {
            p4 = new android.support.v7.internal.view.b(p4, v1_2);
        }
        return p4;
    }

    private android.view.View a(android.content.Context p3, String p4, String p5)
    {
        reflect.Constructor v0_2 = ((reflect.Constructor) android.support.v7.internal.a.a.c.get(p4));
        try {
            if (v0_2 != null) {
                v0_2.setAccessible(1);
                reflect.Constructor v0_11 = ((android.view.View) v0_2.newInstance(this.d));
            } else {
                reflect.Constructor v0_3;
                java.util.Map v1_0 = p3.getClassLoader();
                if (p5 == null) {
                    v0_3 = p4;
                } else {
                    v0_3 = new StringBuilder().append(p5).append(p4).toString();
                }
                v0_2 = v1_0.loadClass(v0_3).asSubclass(android.view.View).getConstructor(android.support.v7.internal.a.a.a);
                android.support.v7.internal.a.a.c.put(p4, v0_2);
            }
        } catch (reflect.Constructor v0) {
            v0_11 = 0;
        }
        return v0_11;
    }

    private android.view.View a(android.view.View p7, String p8, android.content.Context p9, android.util.AttributeSet p10, boolean p11, boolean p12)
    {
        if ((!p11) || (p7 == null)) {
            android.support.v7.internal.view.b v1 = p9;
        } else {
            v1 = p7.getContext();
        }
        int v0_1;
        android.content.res.TypedArray v4 = v1.obtainStyledAttributes(p10, android.support.v7.a.n.View, 0, 0);
        if (!p12) {
            v0_1 = 0;
        } else {
            v0_1 = v4.getResourceId(android.support.v7.a.n.View_android_theme, 0);
        }
        if (v0_1 == 0) {
            v0_1 = v4.getResourceId(android.support.v7.a.n.View_theme, 0);
            if (v0_1 != 0) {
                android.util.Log.i("AppCompatViewInflater", "app:theme is now deprecated. Please move to using android:theme instead.");
            }
        }
        boolean v2_1 = v0_1;
        v4.recycle();
        if ((v2_1) && ((!(v1 instanceof android.support.v7.internal.view.b)) || (((android.support.v7.internal.view.b) v1).a != v2_1))) {
            v1 = new android.support.v7.internal.view.b(v1, v2_1);
        }
        int v0_10 = -1;
        switch (p8.hashCode()) {
            case -1946472170:
                if (!p8.equals("RatingBar")) {
                } else {
                    v0_10 = 7;
                }
                break;
            case -1455429095:
                if (!p8.equals("CheckedTextView")) {
                } else {
                    v0_10 = 4;
                }
                break;
            case -1346021293:
                if (!p8.equals("MultiAutoCompleteTextView")) {
                } else {
                    v0_10 = 6;
                }
                break;
            case -938935918:
                if (!p8.equals("TextView")) {
                } else {
                    v0_10 = 9;
                }
                break;
            case -339785223:
                if (!p8.equals("Spinner")) {
                } else {
                    v0_10 = 1;
                }
                break;
            case 776382189:
                if (!p8.equals("RadioButton")) {
                } else {
                    v0_10 = 3;
                }
                break;
            case 1413872058:
                if (!p8.equals("AutoCompleteTextView")) {
                } else {
                    v0_10 = 5;
                }
                break;
            case 1601505219:
                if (!p8.equals("CheckBox")) {
                } else {
                    v0_10 = 2;
                }
                break;
            case 1666676343:
                if (!p8.equals("EditText")) {
                } else {
                    v0_10 = 0;
                }
                break;
            case 2001146706:
                if (!p8.equals("Button")) {
                } else {
                    v0_10 = 8;
                }
                break;
        }
        int v0_12;
        switch (v0_10) {
            case 0:
                v0_12 = new android.support.v7.widget.w(v1, p10);
                break;
            case 1:
                v0_12 = new android.support.v7.widget.aa(v1, p10);
                break;
            case 2:
                v0_12 = new android.support.v7.widget.s(v1, p10);
                break;
            case 3:
                v0_12 = new android.support.v7.widget.y(v1, p10);
                break;
            case 4:
                v0_12 = new android.support.v7.widget.t(v1, p10);
                break;
            case 5:
                v0_12 = new android.support.v7.widget.p(v1, p10);
                break;
            case 6:
                v0_12 = new android.support.v7.widget.x(v1, p10);
                break;
            case 7:
                v0_12 = new android.support.v7.widget.z(v1, p10);
                break;
            case 8:
                v0_12 = new android.support.v7.widget.r(v1, p10);
                break;
            case 9:
                v0_12 = new android.support.v7.widget.ai(v1, p10);
                break;
            default:
                if (p9 == v1) {
                    v0_12 = 0;
                } else {
                    v0_12 = this.a(v1, p8, p10);
                }
        }
        return v0_12;
    }

    public final android.view.View a(android.content.Context p6, String p7, android.util.AttributeSet p8)
    {
        if (p7.equals("view")) {
            p7 = p8.getAttributeValue(0, "class");
        }
        try {
            android.view.View v0_7;
            this.d[0] = p6;
            this.d[1] = p8;
        } catch (android.view.View v0_10) {
            this.d[0] = 0;
            this.d[1] = 0;
            throw v0_10;
        } catch (android.view.View v0) {
            this.d[0] = 0;
            this.d[1] = 0;
            v0_7 = 0;
            return v0_7;
        }
        if (-1 != p7.indexOf(46)) {
            v0_7 = this.a(p6, p7, 0);
            this.d[0] = 0;
            this.d[1] = 0;
            return v0_7;
        } else {
            v0_7 = this.a(p6, p7, "android.widget.");
            this.d[0] = 0;
            this.d[1] = 0;
            return v0_7;
        }
    }
}
