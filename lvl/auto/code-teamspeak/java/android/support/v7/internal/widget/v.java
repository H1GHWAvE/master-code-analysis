package android.support.v7.internal.widget;
final class v implements android.view.ViewTreeObserver$OnGlobalLayoutListener {
    final synthetic android.support.v7.internal.widget.ActivityChooserView a;

    v(android.support.v7.internal.widget.ActivityChooserView p1)
    {
        this.a = p1;
        return;
    }

    public final void onGlobalLayout()
    {
        if (this.a.c()) {
            if (this.a.isShown()) {
                android.support.v7.internal.widget.ActivityChooserView.b(this.a).b();
                if (this.a.a != null) {
                    this.a.a.a(1);
                }
            } else {
                android.support.v7.internal.widget.ActivityChooserView.b(this.a).d();
            }
        }
        return;
    }
}
