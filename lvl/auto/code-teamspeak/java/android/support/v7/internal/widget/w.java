package android.support.v7.internal.widget;
final class w extends android.support.v7.widget.as {
    final synthetic android.support.v7.internal.widget.ActivityChooserView a;

    w(android.support.v7.internal.widget.ActivityChooserView p1, android.view.View p2)
    {
        this.a = p1;
        this(p2);
        return;
    }

    public final android.support.v7.widget.an a()
    {
        return android.support.v7.internal.widget.ActivityChooserView.b(this.a);
    }

    protected final boolean b()
    {
        this.a.a();
        return 1;
    }

    protected final boolean c()
    {
        this.a.b();
        return 1;
    }
}
