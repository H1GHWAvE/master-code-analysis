package android.support.v7.internal.widget;
public class ActionBarContextView extends android.support.v7.internal.widget.a {
    private static final String i = "ActionBarContextView";
    public android.view.View g;
    public boolean h;
    private CharSequence j;
    private CharSequence k;
    private android.view.View l;
    private android.widget.LinearLayout m;
    private android.widget.TextView n;
    private android.widget.TextView o;
    private int p;
    private int q;
    private int r;

    public ActionBarContextView(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public ActionBarContextView(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, android.support.v7.a.d.actionModeStyle);
        return;
    }

    public ActionBarContextView(android.content.Context p4, android.util.AttributeSet p5, int p6)
    {
        this(p4, p5, p6);
        android.content.res.TypedArray v0_1 = android.support.v7.internal.widget.ax.a(p4, p5, android.support.v7.a.n.ActionMode, p6);
        this.setBackgroundDrawable(v0_1.a(android.support.v7.a.n.ActionMode_background));
        this.p = v0_1.e(android.support.v7.a.n.ActionMode_titleTextStyle, 0);
        this.q = v0_1.e(android.support.v7.a.n.ActionMode_subtitleTextStyle, 0);
        this.e = v0_1.d(android.support.v7.a.n.ActionMode_height, 0);
        this.r = v0_1.e(android.support.v7.a.n.ActionMode_closeItemLayout, android.support.v7.a.k.abc_action_mode_close_item_material);
        v0_1.a.recycle();
        return;
    }

    private void j()
    {
        int v4 = 8;
        int v1 = 1;
        if (this.m == null) {
            android.view.LayoutInflater.from(this.getContext()).inflate(android.support.v7.a.k.abc_action_bar_title_item, this);
            this.m = ((android.widget.LinearLayout) this.getChildAt((this.getChildCount() - 1)));
            this.n = ((android.widget.TextView) this.m.findViewById(android.support.v7.a.i.action_bar_title));
            this.o = ((android.widget.TextView) this.m.findViewById(android.support.v7.a.i.action_bar_subtitle));
            if (this.p != 0) {
                this.n.setTextAppearance(this.getContext(), this.p);
            }
            if (this.q != 0) {
                this.o.setTextAppearance(this.getContext(), this.q);
            }
        }
        android.widget.LinearLayout v0_21;
        this.n.setText(this.j);
        this.o.setText(this.k);
        if (android.text.TextUtils.isEmpty(this.j)) {
            v0_21 = 0;
        } else {
            v0_21 = 1;
        }
        if (android.text.TextUtils.isEmpty(this.k)) {
            v1 = 0;
        }
        android.widget.LinearLayout v3_9;
        if (v1 == 0) {
            v3_9 = 8;
        } else {
            v3_9 = 0;
        }
        this.o.setVisibility(v3_9);
        if ((v0_21 != null) || (v1 != 0)) {
            v4 = 0;
        }
        this.m.setVisibility(v4);
        if (this.m.getParent() == null) {
            this.addView(this.m);
        }
        return;
    }

    private void k()
    {
        if (this.g == null) {
            this.i();
        }
        return;
    }

    private boolean l()
    {
        return this.h;
    }

    public final bridge synthetic android.support.v4.view.fk a(int p3, long p4)
    {
        return super.a(p3, p4);
    }

    public final bridge synthetic void a(int p1)
    {
        super.a(p1);
        return;
    }

    public final void a(android.support.v7.c.a p5)
    {
        if (this.g != null) {
            if (this.g.getParent() == null) {
                this.addView(this.g);
            }
        } else {
            this.g = android.view.LayoutInflater.from(this.getContext()).inflate(this.r, this, 0);
            this.addView(this.g);
        }
        this.g.findViewById(android.support.v7.a.i.action_mode_close_button).setOnClickListener(new android.support.v7.internal.widget.f(this, p5));
        android.support.v7.widget.ActionMenuView v0_11 = ((android.support.v7.internal.view.menu.i) p5.b());
        if (this.d != null) {
            this.d.g();
        }
        this.d = new android.support.v7.widget.ActionMenuPresenter(this.getContext());
        this.d.d();
        android.view.ViewGroup$LayoutParams v1_10 = new android.view.ViewGroup$LayoutParams(-2, -1);
        v0_11.a(this.d, this.b);
        this.c = ((android.support.v7.widget.ActionMenuView) this.d.a(this));
        this.c.setBackgroundDrawable(0);
        this.addView(this.c, v1_10);
        return;
    }

    public final boolean a()
    {
        int v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.d.e();
        }
        return v0_1;
    }

    public final bridge synthetic void b()
    {
        super.b();
        return;
    }

    public final boolean c()
    {
        int v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.d.f();
        }
        return v0_1;
    }

    public final boolean d()
    {
        int v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.d.i();
        }
        return v0_1;
    }

    public final bridge synthetic boolean e()
    {
        return super.e();
    }

    public final bridge synthetic boolean f()
    {
        return super.f();
    }

    public final bridge synthetic boolean g()
    {
        return super.g();
    }

    protected android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return new android.view.ViewGroup$MarginLayoutParams(-1, -2);
    }

    public android.view.ViewGroup$LayoutParams generateLayoutParams(android.util.AttributeSet p3)
    {
        return new android.view.ViewGroup$MarginLayoutParams(this.getContext(), p3);
    }

    public bridge synthetic int getAnimatedVisibility()
    {
        return super.getAnimatedVisibility();
    }

    public bridge synthetic int getContentHeight()
    {
        return super.getContentHeight();
    }

    public CharSequence getSubtitle()
    {
        return this.k;
    }

    public CharSequence getTitle()
    {
        return this.j;
    }

    public final bridge synthetic void h()
    {
        super.h();
        return;
    }

    public final void i()
    {
        this.removeAllViews();
        this.l = 0;
        this.c = 0;
        return;
    }

    public void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if (this.d != null) {
            this.d.f();
            this.d.h();
        }
        return;
    }

    public bridge synthetic boolean onHoverEvent(android.view.MotionEvent p2)
    {
        return super.onHoverEvent(p2);
    }

    public void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 14) {
            if (p3.getEventType() != 32) {
                super.onInitializeAccessibilityEvent(p3);
            } else {
                p3.setSource(this);
                p3.setClassName(this.getClass().getName());
                p3.setPackageName(this.getContext().getPackageName());
                p3.setContentDescription(this.j);
            }
        }
        return;
    }

    protected void onLayout(boolean p8, int p9, int p10, int p11, int p12)
    {
        int v1_0;
        boolean v3 = android.support.v7.internal.widget.bd.a(this);
        if (!v3) {
            v1_0 = this.getPaddingLeft();
        } else {
            v1_0 = ((p11 - p9) - this.getPaddingRight());
        }
        int v4 = this.getPaddingTop();
        int v5 = (((p12 - p10) - this.getPaddingTop()) - this.getPaddingBottom());
        if ((this.g != null) && (this.g.getVisibility() != 8)) {
            android.support.v7.widget.ActionMenuView v2_2;
            int v0_10 = ((android.view.ViewGroup$MarginLayoutParams) this.g.getLayoutParams());
            if (!v3) {
                v2_2 = v0_10.leftMargin;
            } else {
                v2_2 = v0_10.rightMargin;
            }
            int v0_11;
            if (!v3) {
                v0_11 = v0_10.rightMargin;
            } else {
                v0_11 = v0_10.leftMargin;
            }
            int v1_2 = android.support.v7.internal.widget.ActionBarContextView.a(v1_0, v2_2, v3);
            v1_0 = android.support.v7.internal.widget.ActionBarContextView.a((v1_2 + android.support.v7.internal.widget.ActionBarContextView.a(this.g, v1_2, v4, v5, v3)), v0_11, v3);
        }
        if ((this.m != null) && ((this.l == null) && (this.m.getVisibility() != 8))) {
            v1_0 += android.support.v7.internal.widget.ActionBarContextView.a(this.m, v1_0, v4, v5, v3);
        }
        if (this.l != null) {
            android.support.v7.internal.widget.ActionBarContextView.a(this.l, v1_0, v4, v5, v3);
        }
        int v0_21;
        if (!v3) {
            v0_21 = ((p11 - p9) - this.getPaddingRight());
        } else {
            v0_21 = this.getPaddingLeft();
        }
        if (this.c != null) {
            int v1_6;
            if (v3) {
                v1_6 = 0;
            } else {
                v1_6 = 1;
            }
            android.support.v7.internal.widget.ActionBarContextView.a(this.c, v0_21, v4, v5, v1_6);
        }
        return;
    }

    protected void onMeasure(int p13, int p14)
    {
        int v4 = 1073741824;
        int v3 = 0;
        if (android.view.View$MeasureSpec.getMode(p13) == 1073741824) {
            if (android.view.View$MeasureSpec.getMode(p14) != 0) {
                int v1_0;
                int v7 = android.view.View$MeasureSpec.getSize(p13);
                if (this.e <= 0) {
                    v1_0 = android.view.View$MeasureSpec.getSize(p14);
                } else {
                    v1_0 = this.e;
                }
                int v8 = (this.getPaddingTop() + this.getPaddingBottom());
                int v0_8 = ((v7 - this.getPaddingLeft()) - this.getPaddingRight());
                android.view.View v6_0 = (v1_0 - v8);
                int v2_2 = android.view.View$MeasureSpec.makeMeasureSpec(v6_0, -2147483648);
                if (this.g != null) {
                    int v0_11 = ((android.view.ViewGroup$MarginLayoutParams) this.g.getLayoutParams());
                    v0_8 = (android.support.v7.internal.widget.ActionBarContextView.a(this.g, v0_8, v2_2) - (v0_11.rightMargin + v0_11.leftMargin));
                }
                if ((this.c != null) && (this.c.getParent() == this)) {
                    v0_8 = android.support.v7.internal.widget.ActionBarContextView.a(this.c, v0_8, v2_2);
                }
                if ((this.m != null) && (this.l == null)) {
                    if (!this.h) {
                        v0_8 = android.support.v7.internal.widget.ActionBarContextView.a(this.m, v0_8, v2_2);
                    } else {
                        int v2_4;
                        this.m.measure(android.view.View$MeasureSpec.makeMeasureSpec(0, 0), v2_2);
                        android.widget.LinearLayout v9_12 = this.m.getMeasuredWidth();
                        if (v9_12 > v0_8) {
                            v2_4 = 0;
                        } else {
                            v2_4 = 1;
                        }
                        if (v2_4 != 0) {
                            v0_8 -= v9_12;
                        }
                        int v2_5;
                        if (v2_4 == 0) {
                            v2_5 = 8;
                        } else {
                            v2_5 = 0;
                        }
                        this.m.setVisibility(v2_5);
                    }
                }
                if (this.l != null) {
                    int v2_9;
                    android.widget.LinearLayout v9_14 = this.l.getLayoutParams();
                    if (v9_14.width == -2) {
                        v2_9 = -2147483648;
                    } else {
                        v2_9 = 1073741824;
                    }
                    if (v9_14.width >= 0) {
                        v0_8 = Math.min(v9_14.width, v0_8);
                    }
                    if (v9_14.height == -2) {
                        v4 = -2147483648;
                    }
                    int v5_2;
                    if (v9_14.height < 0) {
                        v5_2 = v6_0;
                    } else {
                        v5_2 = Math.min(v9_14.height, v6_0);
                    }
                    this.l.measure(android.view.View$MeasureSpec.makeMeasureSpec(v0_8, v2_9), android.view.View$MeasureSpec.makeMeasureSpec(v5_2, v4));
                }
                if (this.e > 0) {
                    this.setMeasuredDimension(v7, v1_0);
                } else {
                    int v2_11 = this.getChildCount();
                    int v1_1 = 0;
                    while (v3 < v2_11) {
                        int v0_18 = (this.getChildAt(v3).getMeasuredHeight() + v8);
                        if (v0_18 <= v1_1) {
                            v0_18 = v1_1;
                        }
                        v3++;
                        v1_1 = v0_18;
                    }
                    this.setMeasuredDimension(v7, v1_1);
                }
                return;
            } else {
                throw new IllegalStateException(new StringBuilder().append(this.getClass().getSimpleName()).append(" can only be used with android:layout_height=\"wrap_content\"").toString());
            }
        } else {
            throw new IllegalStateException(new StringBuilder().append(this.getClass().getSimpleName()).append(" can only be used with android:layout_width=\"match_parent\" (or fill_parent)").toString());
        }
    }

    public bridge synthetic boolean onTouchEvent(android.view.MotionEvent p2)
    {
        return super.onTouchEvent(p2);
    }

    public void setContentHeight(int p1)
    {
        this.e = p1;
        return;
    }

    public void setCustomView(android.view.View p2)
    {
        if (this.l != null) {
            this.removeView(this.l);
        }
        this.l = p2;
        if ((p2 != null) && (this.m != null)) {
            this.removeView(this.m);
            this.m = 0;
        }
        if (p2 != null) {
            this.addView(p2);
        }
        this.requestLayout();
        return;
    }

    public void setSubtitle(CharSequence p1)
    {
        this.k = p1;
        this.j();
        return;
    }

    public void setTitle(CharSequence p1)
    {
        this.j = p1;
        this.j();
        return;
    }

    public void setTitleOptional(boolean p2)
    {
        if (p2 != this.h) {
            this.requestLayout();
        }
        this.h = p2;
        return;
    }

    public bridge synthetic void setVisibility(int p1)
    {
        super.setVisibility(p1);
        return;
    }

    public boolean shouldDelayChildPressedState()
    {
        return 0;
    }
}
