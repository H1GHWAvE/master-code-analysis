package android.support.v7.internal.view.menu;
final class u extends android.support.v7.internal.view.menu.p implements android.view.ActionProvider$VisibilityListener {
    android.support.v4.view.p e;
    final synthetic android.support.v7.internal.view.menu.t f;

    public u(android.support.v7.internal.view.menu.t p1, android.content.Context p2, android.view.ActionProvider p3)
    {
        this.f = p1;
        this(p1, p2, p3);
        return;
    }

    public final android.view.View a(android.view.MenuItem p2)
    {
        return this.c.onCreateActionView(p2);
    }

    public final void a(android.support.v4.view.p p2)
    {
        this.e = p2;
        this.c.setVisibilityListener(this);
        return;
    }

    public final boolean b()
    {
        return this.c.overridesItemVisibility();
    }

    public final boolean c()
    {
        return this.c.isVisible();
    }

    public final void d()
    {
        this.c.refreshVisibility();
        return;
    }

    public final void onActionProviderVisibilityChanged(boolean p2)
    {
        if (this.e != null) {
            this.e.a();
        }
        return;
    }
}
