package android.support.v7.internal;
public final class a {

    private a()
    {
        return;
    }

    private static boolean a()
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }
}
