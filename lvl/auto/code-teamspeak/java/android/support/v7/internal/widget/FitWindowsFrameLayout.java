package android.support.v7.internal.widget;
public class FitWindowsFrameLayout extends android.widget.FrameLayout implements android.support.v7.internal.widget.af {
    private android.support.v7.internal.widget.ag a;

    public FitWindowsFrameLayout(android.content.Context p1)
    {
        this(p1);
        return;
    }

    public FitWindowsFrameLayout(android.content.Context p1, android.util.AttributeSet p2)
    {
        this(p1, p2);
        return;
    }

    protected boolean fitSystemWindows(android.graphics.Rect p2)
    {
        if (this.a != null) {
            this.a.a(p2);
        }
        return super.fitSystemWindows(p2);
    }

    public void setOnFitSystemWindowsListener(android.support.v7.internal.widget.ag p1)
    {
        this.a = p1;
        return;
    }
}
