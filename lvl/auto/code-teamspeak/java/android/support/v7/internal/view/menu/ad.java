package android.support.v7.internal.view.menu;
public class ad extends android.support.v7.internal.view.menu.i implements android.view.SubMenu {
    public android.support.v7.internal.view.menu.i p;
    private android.support.v7.internal.view.menu.m q;

    public ad(android.content.Context p1, android.support.v7.internal.view.menu.i p2, android.support.v7.internal.view.menu.m p3)
    {
        this(p1);
        this.p = p2;
        this.q = p3;
        return;
    }

    private android.view.Menu l()
    {
        return this.p;
    }

    public final String a()
    {
        String v0_1;
        if (this.q == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.q.getItemId();
        }
        String v0_4;
        if (v0_1 != null) {
            v0_4 = new StringBuilder().append(super.a()).append(":").append(v0_1).toString();
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    public final void a(android.support.v7.internal.view.menu.j p2)
    {
        this.p.a(p2);
        return;
    }

    public final void a(boolean p2)
    {
        this.p.a(p2);
        return;
    }

    final boolean a(android.support.v7.internal.view.menu.i p2, android.view.MenuItem p3)
    {
        if ((!super.a(p2, p3)) && (!this.p.a(p2, p3))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean a(android.support.v7.internal.view.menu.m p2)
    {
        return this.p.a(p2);
    }

    public final boolean b()
    {
        return this.p.b();
    }

    public final boolean b(android.support.v7.internal.view.menu.m p2)
    {
        return this.p.b(p2);
    }

    public final boolean c()
    {
        return this.p.c();
    }

    public android.view.MenuItem getItem()
    {
        return this.q;
    }

    public final android.support.v7.internal.view.menu.i k()
    {
        return this.p;
    }

    public android.view.SubMenu setHeaderIcon(int p2)
    {
        super.a(android.support.v4.c.h.a(this.e, p2));
        return this;
    }

    public android.view.SubMenu setHeaderIcon(android.graphics.drawable.Drawable p1)
    {
        super.a(p1);
        return this;
    }

    public android.view.SubMenu setHeaderTitle(int p2)
    {
        super.a(this.e.getResources().getString(p2));
        return this;
    }

    public android.view.SubMenu setHeaderTitle(CharSequence p1)
    {
        super.a(p1);
        return this;
    }

    public android.view.SubMenu setHeaderView(android.view.View p7)
    {
        super.a(0, 0, 0, 0, p7);
        return this;
    }

    public android.view.SubMenu setIcon(int p2)
    {
        this.q.setIcon(p2);
        return this;
    }

    public android.view.SubMenu setIcon(android.graphics.drawable.Drawable p2)
    {
        this.q.setIcon(p2);
        return this;
    }

    public void setQwertyMode(boolean p2)
    {
        this.p.setQwertyMode(p2);
        return;
    }
}
