package android.support.v7.internal.widget;
public final class as extends android.content.ContextWrapper {
    private android.content.res.Resources a;

    private as(android.content.Context p1)
    {
        this(p1);
        return;
    }

    public static android.content.Context a(android.content.Context p1)
    {
        if (!(p1 instanceof android.support.v7.internal.widget.as)) {
            p1 = new android.support.v7.internal.widget.as(p1);
        }
        return p1;
    }

    public final android.content.res.Resources getResources()
    {
        if (this.a == null) {
            this.a = new android.support.v7.internal.widget.at(super.getResources(), android.support.v7.internal.widget.av.a(this));
        }
        return this.a;
    }
}
