package android.support.v7.internal.widget;
public final class c implements android.support.v4.view.gd {
    int a;
    final synthetic android.support.v7.internal.widget.a b;
    private boolean c;

    protected c(android.support.v7.internal.widget.a p2)
    {
        this.b = p2;
        this.c = 0;
        return;
    }

    public final android.support.v7.internal.widget.c a(android.support.v4.view.fk p2, int p3)
    {
        this.b.f = p2;
        this.a = p3;
        return this;
    }

    public final void a(android.view.View p2)
    {
        android.support.v7.internal.widget.a.a(this.b);
        this.c = 0;
        return;
    }

    public final void b(android.view.View p3)
    {
        if (!this.c) {
            this.b.f = 0;
            android.support.v7.internal.widget.a.a(this.b, this.a);
        }
        return;
    }

    public final void c(android.view.View p2)
    {
        this.c = 1;
        return;
    }
}
