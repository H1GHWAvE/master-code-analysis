package android.support.v7.internal.view;
final class h {
    private static final int A = 0;
    private static final int B = 0;
    private static final int C = 0;
    private static final int D = 0;
    private static final int E = 0;
    private static final boolean F = False;
    private static final boolean G = True;
    private static final boolean H = True;
    android.view.Menu a;
    int b;
    int c;
    int d;
    int e;
    boolean f;
    boolean g;
    boolean h;
    int i;
    int j;
    CharSequence k;
    CharSequence l;
    int m;
    char n;
    char o;
    int p;
    boolean q;
    boolean r;
    boolean s;
    int t;
    int u;
    String v;
    String w;
    String x;
    android.support.v4.view.n y;
    final synthetic android.support.v7.internal.view.f z;

    public h(android.support.v7.internal.view.f p1, android.view.Menu p2)
    {
        this.z = p1;
        this.a = p2;
        this.a();
        return;
    }

    static char a(String p1)
    {
        char v0 = 0;
        if (p1 != null) {
            v0 = p1.charAt(0);
        }
        return v0;
    }

    private static synthetic android.support.v4.view.n a(android.support.v7.internal.view.h p1)
    {
        return p1.y;
    }

    private void a(android.util.AttributeSet p5)
    {
        android.content.res.TypedArray v0_2 = android.support.v7.internal.view.f.a(this.z).obtainStyledAttributes(p5, android.support.v7.a.n.MenuGroup);
        this.b = v0_2.getResourceId(android.support.v7.a.n.MenuGroup_android_id, 0);
        this.c = v0_2.getInt(android.support.v7.a.n.MenuGroup_android_menuCategory, 0);
        this.d = v0_2.getInt(android.support.v7.a.n.MenuGroup_android_orderInCategory, 0);
        this.e = v0_2.getInt(android.support.v7.a.n.MenuGroup_android_checkableBehavior, 0);
        this.f = v0_2.getBoolean(android.support.v7.a.n.MenuGroup_android_visible, 1);
        this.g = v0_2.getBoolean(android.support.v7.a.n.MenuGroup_android_enabled, 1);
        v0_2.recycle();
        return;
    }

    private void b(android.util.AttributeSet p7)
    {
        Class[] v1_0 = 1;
        android.content.res.TypedArray v3_1 = android.support.v7.internal.view.f.a(this.z).obtainStyledAttributes(p7, android.support.v7.a.n.MenuItem);
        this.i = v3_1.getResourceId(android.support.v7.a.n.MenuItem_android_id, 0);
        this.j = ((v3_1.getInt(android.support.v7.a.n.MenuItem_android_menuCategory, this.c) & -65536) | (v3_1.getInt(android.support.v7.a.n.MenuItem_android_orderInCategory, this.d) & 65535));
        this.k = v3_1.getText(android.support.v7.a.n.MenuItem_android_title);
        this.l = v3_1.getText(android.support.v7.a.n.MenuItem_android_titleCondensed);
        this.m = v3_1.getResourceId(android.support.v7.a.n.MenuItem_android_icon, 0);
        this.n = android.support.v7.internal.view.h.a(v3_1.getString(android.support.v7.a.n.MenuItem_android_alphabeticShortcut));
        this.o = android.support.v7.internal.view.h.a(v3_1.getString(android.support.v7.a.n.MenuItem_android_numericShortcut));
        if (!v3_1.hasValue(android.support.v7.a.n.MenuItem_android_checkable)) {
            this.p = this.e;
        } else {
            android.support.v4.view.n v0_25;
            if (!v3_1.getBoolean(android.support.v7.a.n.MenuItem_android_checkable, 0)) {
                v0_25 = 0;
            } else {
                v0_25 = 1;
            }
            this.p = v0_25;
        }
        this.q = v3_1.getBoolean(android.support.v7.a.n.MenuItem_android_checked, 0);
        this.r = v3_1.getBoolean(android.support.v7.a.n.MenuItem_android_visible, this.f);
        this.s = v3_1.getBoolean(android.support.v7.a.n.MenuItem_android_enabled, this.g);
        this.t = v3_1.getInt(android.support.v7.a.n.MenuItem_showAsAction, -1);
        this.x = v3_1.getString(android.support.v7.a.n.MenuItem_android_onClick);
        this.u = v3_1.getResourceId(android.support.v7.a.n.MenuItem_actionLayout, 0);
        this.v = v3_1.getString(android.support.v7.a.n.MenuItem_actionViewClass);
        this.w = v3_1.getString(android.support.v7.a.n.MenuItem_actionProviderClass);
        if (this.w == null) {
            v1_0 = 0;
        }
        if ((v1_0 == null) || ((this.u != 0) || (this.v != null))) {
            if (v1_0 != null) {
                android.util.Log.w("SupportMenuInflater", "Ignoring attribute \'actionProviderClass\'. Action view already specified.");
            }
            this.y = 0;
        } else {
            this.y = ((android.support.v4.view.n) this.a(this.w, android.support.v7.internal.view.f.a(), android.support.v7.internal.view.f.b(this.z)));
        }
        v3_1.recycle();
        this.h = 0;
        return;
    }

    private void c()
    {
        this.h = 1;
        this.a(this.a.add(this.b, this.i, this.j, this.k));
        return;
    }

    private boolean d()
    {
        return this.h;
    }

    final Object a(String p5, Class[] p6, Object[] p7)
    {
        try {
            int v0_4 = android.support.v7.internal.view.f.a(this.z).getClassLoader().loadClass(p5).getConstructor(p6);
            v0_4.setAccessible(1);
            int v0_5 = v0_4.newInstance(p7);
        } catch (int v0_6) {
            android.util.Log.w("SupportMenuInflater", new StringBuilder("Cannot instantiate class: ").append(p5).toString(), v0_6);
            v0_5 = 0;
        }
        return v0_5;
    }

    public final void a()
    {
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = 1;
        this.g = 1;
        return;
    }

    final void a(android.view.MenuItem p9)
    {
        android.support.v4.view.n v0_4;
        Object[] v2 = 1;
        String v1_2 = p9.setChecked(this.q).setVisible(this.r).setEnabled(this.s);
        if (this.p <= 0) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        v1_2.setCheckable(v0_4).setTitleCondensed(this.l).setIcon(this.m).setAlphabeticShortcut(this.n).setNumericShortcut(this.o);
        if (this.t >= 0) {
            android.support.v4.view.az.a(p9, this.t);
        }
        if (this.x != null) {
            if (!android.support.v7.internal.view.f.a(this.z).isRestricted()) {
                p9.setOnMenuItemClickListener(new android.support.v7.internal.view.g(android.support.v7.internal.view.f.c(this.z), this.x));
            } else {
                throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
            }
        }
        if (this.p >= 2) {
            if (!(p9 instanceof android.support.v7.internal.view.menu.m)) {
                if ((p9 instanceof android.support.v7.internal.view.menu.o)) {
                    try {
                        if (((android.support.v7.internal.view.menu.o) p9).f == null) {
                            String v1_13 = ((android.support.v4.g.a.b) ((android.support.v7.internal.view.menu.o) p9).d).getClass();
                            int v5_1 = new Class[1];
                            v5_1[0] = Boolean.TYPE;
                            ((android.support.v7.internal.view.menu.o) p9).f = v1_13.getDeclaredMethod("setExclusiveCheckable", v5_1);
                        }
                    } catch (android.support.v4.view.n v0_22) {
                        android.util.Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", v0_22);
                    }
                    String v1_16 = ((android.support.v7.internal.view.menu.o) p9).f;
                    android.support.v4.view.n v0_23 = ((android.support.v7.internal.view.menu.o) p9).d;
                    Object[] v4_4 = new Object[1];
                    v4_4[0] = Boolean.valueOf(1);
                    v1_16.invoke(v0_23, v4_4);
                }
            } else {
                ((android.support.v7.internal.view.menu.m) p9).a(1);
            }
        }
        if (this.v == null) {
            v2 = 0;
        } else {
            android.support.v4.view.az.a(p9, ((android.view.View) this.a(this.v, android.support.v7.internal.view.f.b(), android.support.v7.internal.view.f.d(this.z))));
        }
        if (this.u > 0) {
            if (v2 != null) {
                android.util.Log.w("SupportMenuInflater", "Ignoring attribute \'itemActionViewLayout\'. Action view already specified.");
            } else {
                android.support.v4.view.az.b(p9, this.u);
            }
        }
        if (this.y != null) {
            android.support.v4.view.az.a(p9, this.y);
        }
        return;
    }

    public final android.view.SubMenu b()
    {
        this.h = 1;
        android.view.SubMenu v0_2 = this.a.addSubMenu(this.b, this.i, this.j, this.k);
        this.a(v0_2.getItem());
        return v0_2;
    }
}
