package android.support.v7.internal.view.menu;
public final class l implements android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnDismissListener, android.content.DialogInterface$OnKeyListener, android.support.v7.internal.view.menu.y {
    android.support.v7.internal.view.menu.i a;
    android.support.v7.app.af b;
    android.support.v7.internal.view.menu.g c;
    private android.support.v7.internal.view.menu.y d;

    public l(android.support.v7.internal.view.menu.i p1)
    {
        this.a = p1;
        return;
    }

    private void a()
    {
        android.support.v7.app.af v0_0 = this.a;
        int v1_1 = new android.support.v7.app.ag(v0_0.e);
        this.c = new android.support.v7.internal.view.menu.g(v1_1.a.a, android.support.v7.a.k.abc_list_menu_item_layout);
        this.c.g = this;
        this.a.a(this.c);
        v1_1.a.t = this.c.d();
        v1_1.a.u = this;
        int v2_8 = v0_0.l;
        if (v2_8 == 0) {
            v1_1.a.d = v0_0.k;
            v1_1.a.f = v0_0.j;
        } else {
            v1_1.a.g = v2_8;
        }
        v1_1.a.r = this;
        this.b = v1_1.a();
        this.b.setOnDismissListener(this);
        android.support.v7.app.af v0_8 = this.b.getWindow().getAttributes();
        v0_8.type = 1003;
        v0_8.flags = (v0_8.flags | 131072);
        this.b.show();
        return;
    }

    private void a(android.support.v7.internal.view.menu.y p1)
    {
        this.d = p1;
        return;
    }

    private void b()
    {
        if (this.b != null) {
            this.b.dismiss();
        }
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p2, boolean p3)
    {
        if (((p3) || (p2 == this.a)) && (this.b != null)) {
            this.b.dismiss();
        }
        if (this.d != null) {
            this.d.a(p2, p3);
        }
        return;
    }

    public final boolean a_(android.support.v7.internal.view.menu.i p2)
    {
        int v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.d.a_(p2);
        }
        return v0_1;
    }

    public final void onClick(android.content.DialogInterface p5, int p6)
    {
        this.a.a(((android.support.v7.internal.view.menu.m) this.c.d().getItem(p6)), 0, 0);
        return;
    }

    public final void onDismiss(android.content.DialogInterface p4)
    {
        this.c.a(this.a, 1);
        return;
    }

    public final boolean onKey(android.content.DialogInterface p3, int p4, android.view.KeyEvent p5)
    {
        boolean v0_0 = 1;
        if ((p4 != 82) && (p4 != 4)) {
            v0_0 = this.a.performShortcut(p4, p5, 0);
        } else {
            if ((p5.getAction() != 0) || (p5.getRepeatCount() != 0)) {
                if ((p5.getAction() != 1) || (p5.isCanceled())) {
                } else {
                    android.support.v7.internal.view.menu.i v1_7 = this.b.getWindow();
                    if (v1_7 == null) {
                    } else {
                        android.support.v7.internal.view.menu.i v1_8 = v1_7.getDecorView();
                        if (v1_8 == null) {
                        } else {
                            android.support.v7.internal.view.menu.i v1_9 = v1_8.getKeyDispatcherState();
                            if ((v1_9 == null) || (!v1_9.isTracking(p5))) {
                            } else {
                                this.a.b(1);
                                p3.dismiss();
                            }
                        }
                    }
                }
            } else {
                android.support.v7.internal.view.menu.i v1_13 = this.b.getWindow();
                if (v1_13 == null) {
                } else {
                    android.support.v7.internal.view.menu.i v1_14 = v1_13.getDecorView();
                    if (v1_14 == null) {
                    } else {
                        android.support.v7.internal.view.menu.i v1_15 = v1_14.getKeyDispatcherState();
                        if (v1_15 == null) {
                        } else {
                            v1_15.startTracking(p5, this);
                        }
                    }
                }
            }
        }
        return v0_0;
    }
}
