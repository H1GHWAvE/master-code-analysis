package android.support.v7.internal.widget;
public final class aq implements android.support.v4.view.gd {
    final synthetic android.support.v7.internal.widget.al a;
    private boolean b;
    private int c;

    protected aq(android.support.v7.internal.widget.al p2)
    {
        this.a = p2;
        this.b = 0;
        return;
    }

    public final android.support.v7.internal.widget.aq a(android.support.v4.view.fk p2, int p3)
    {
        this.c = p3;
        this.a.g = p2;
        return this;
    }

    public final void a(android.view.View p3)
    {
        this.a.setVisibility(0);
        this.b = 0;
        return;
    }

    public final void b(android.view.View p3)
    {
        if (!this.b) {
            this.a.g = 0;
            this.a.setVisibility(this.c);
        }
        return;
    }

    public final void c(android.view.View p2)
    {
        this.b = 1;
        return;
    }
}
