package android.support.v7.app;
public final class j implements android.support.v4.widget.ac {
    boolean a;
    android.view.View$OnClickListener b;
    private final android.support.v7.app.l c;
    private final android.support.v4.widget.DrawerLayout d;
    private android.support.v7.app.o e;
    private android.graphics.drawable.Drawable f;
    private boolean g;
    private final int h;
    private final int i;
    private boolean j;

    private j(android.app.Activity p7, android.support.v4.widget.DrawerLayout p8, int p9, int p10)
    {
        this(p7, 0, p8, p9, p10);
        return;
    }

    private j(android.app.Activity p7, android.support.v4.widget.DrawerLayout p8, android.support.v7.widget.Toolbar p9, int p10, int p11)
    {
        this(p7, p9, p8, p10, p11);
        return;
    }

    private j(android.app.Activity p4, android.support.v7.widget.Toolbar p5, android.support.v4.widget.DrawerLayout p6, int p7, int p8)
    {
        this.a = 1;
        this.j = 0;
        if (p5 == null) {
            if (!(p4 instanceof android.support.v7.app.m)) {
                if (android.os.Build$VERSION.SDK_INT < 18) {
                    if (android.os.Build$VERSION.SDK_INT < 11) {
                        this.c = new android.support.v7.app.p(p4);
                    } else {
                        this.c = new android.support.v7.app.q(p4, 0);
                    }
                } else {
                    this.c = new android.support.v7.app.r(p4, 0);
                }
            } else {
                this.c = ((android.support.v7.app.m) p4).d();
            }
        } else {
            this.c = new android.support.v7.app.s(p5);
            p5.setNavigationOnClickListener(new android.support.v7.app.k(this));
        }
        this.d = p6;
        this.h = p7;
        this.i = p8;
        this.e = new android.support.v7.app.n(p4, this.c.b());
        this.f = this.i();
        return;
    }

    private void a(int p3)
    {
        android.graphics.drawable.Drawable v0_0 = 0;
        if (p3 != 0) {
            v0_0 = this.d.getResources().getDrawable(p3);
        }
        if (v0_0 != null) {
            this.f = v0_0;
            this.g = 1;
        } else {
            this.f = this.i();
            this.g = 0;
        }
        if (!this.a) {
            this.a(this.f, 0);
        }
        return;
    }

    private void a(android.graphics.drawable.Drawable p3)
    {
        if (p3 != null) {
            this.f = p3;
            this.g = 1;
        } else {
            this.f = this.i();
            this.g = 0;
        }
        if (!this.a) {
            this.a(this.f, 0);
        }
        return;
    }

    private void a(android.graphics.drawable.Drawable p3, int p4)
    {
        if ((!this.j) && (!this.c.c())) {
            android.util.Log.w("ActionBarDrawerToggle", "DrawerToggle may not show up because NavigationIcon is not visible. You may need to call actionbar.setDisplayHomeAsUpEnabled(true);");
            this.j = 1;
        }
        this.c.a(p3, p4);
        return;
    }

    private void a(android.view.View$OnClickListener p1)
    {
        this.b = p1;
        return;
    }

    private void a(boolean p3)
    {
        if (p3 != this.a) {
            if (!p3) {
                this.a(this.f, 0);
            } else {
                int v1_3;
                if (!this.d.c()) {
                    v1_3 = this.h;
                } else {
                    v1_3 = this.i;
                }
                this.a(((android.graphics.drawable.Drawable) this.e), v1_3);
            }
            this.a = p3;
        }
        return;
    }

    private static synthetic boolean a(android.support.v7.app.j p1)
    {
        return p1.a;
    }

    private boolean a(android.view.MenuItem p3)
    {
        if ((p3 == null) || ((p3.getItemId() != 16908332) || (!this.a))) {
            int v0_2 = 0;
        } else {
            this.d();
            v0_2 = 1;
        }
        return v0_2;
    }

    private void b(int p2)
    {
        this.c.a(p2);
        return;
    }

    private static synthetic void b(android.support.v7.app.j p0)
    {
        p0.d();
        return;
    }

    private static synthetic android.view.View$OnClickListener c(android.support.v7.app.j p1)
    {
        return p1.b;
    }

    private void e()
    {
        if (!this.d.c()) {
            this.e.a(0);
        } else {
            this.e.a(1065353216);
        }
        if (this.a) {
            int v1_4;
            if (!this.d.c()) {
                v1_4 = this.h;
            } else {
                v1_4 = this.i;
            }
            this.a(((android.graphics.drawable.Drawable) this.e), v1_4);
        }
        return;
    }

    private void f()
    {
        if (!this.g) {
            this.f = this.i();
        }
        if (!this.d.c()) {
            this.e.a(0);
        } else {
            this.e.a(1065353216);
        }
        if (this.a) {
            int v1_4;
            if (!this.d.c()) {
                v1_4 = this.h;
            } else {
                v1_4 = this.i;
            }
            this.a(((android.graphics.drawable.Drawable) this.e), v1_4);
        }
        return;
    }

    private boolean g()
    {
        return this.a;
    }

    private android.view.View$OnClickListener h()
    {
        return this.b;
    }

    private android.graphics.drawable.Drawable i()
    {
        return this.c.a();
    }

    public final void a()
    {
        this.e.a(1065353216);
        if (this.a) {
            this.b(this.i);
        }
        return;
    }

    public final void a(float p4)
    {
        this.e.a(Math.min(1065353216, Math.max(0, p4)));
        return;
    }

    public final void b()
    {
        this.e.a(0);
        if (this.a) {
            this.b(this.h);
        }
        return;
    }

    public final void c()
    {
        return;
    }

    final void d()
    {
        if (!this.d.d()) {
            this.d.a();
        } else {
            this.d.b();
        }
        return;
    }
}
