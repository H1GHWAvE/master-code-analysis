package android.support.v7.app;
public final class bj extends android.support.v4.app.dm {

    private bj(android.content.Context p1)
    {
        this(p1);
        return;
    }

    protected final android.support.v4.app.dn d()
    {
        android.support.v4.app.dn v0_3;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            if (android.os.Build$VERSION.SDK_INT < 16) {
                if (android.os.Build$VERSION.SDK_INT < 14) {
                    v0_3 = super.d();
                } else {
                    v0_3 = new android.support.v7.app.bk(0);
                }
            } else {
                v0_3 = new android.support.v7.app.bl(0);
            }
        } else {
            v0_3 = new android.support.v7.app.bm(0);
        }
        return v0_3;
    }
}
