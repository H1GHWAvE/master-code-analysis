package android.support.v7.app;
 class AppCompatDelegateImplV7$PanelFeatureState$SavedState implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR;
    int a;
    boolean b;
    android.os.Bundle c;

    static AppCompatDelegateImplV7$PanelFeatureState$SavedState()
    {
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState.CREATOR = new android.support.v7.app.bd();
        return;
    }

    private AppCompatDelegateImplV7$PanelFeatureState$SavedState()
    {
        return;
    }

    synthetic AppCompatDelegateImplV7$PanelFeatureState$SavedState(byte p1)
    {
        return;
    }

    static synthetic android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState a(android.os.Parcel p3)
    {
        android.os.Bundle v0_0 = 1;
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState v1_1 = new android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState();
        v1_1.a = p3.readInt();
        if (p3.readInt() != 1) {
            v0_0 = 0;
        }
        v1_1.b = v0_0;
        if (v1_1.b) {
            v1_1.c = p3.readBundle();
        }
        return v1_1;
    }

    private static android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState b(android.os.Parcel p3)
    {
        android.os.Bundle v0_0 = 1;
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState v1_1 = new android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState();
        v1_1.a = p3.readInt();
        if (p3.readInt() != 1) {
            v0_0 = 0;
        }
        v1_1.b = v0_0;
        if (v1_1.b) {
            v1_1.c = p3.readBundle();
        }
        return v1_1;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(android.os.Parcel p2, int p3)
    {
        android.os.Bundle v0_2;
        p2.writeInt(this.a);
        if (!this.b) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        p2.writeInt(v0_2);
        if (this.b) {
            p2.writeBundle(this.c);
        }
        return;
    }
}
