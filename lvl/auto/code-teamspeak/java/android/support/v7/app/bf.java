package android.support.v7.app;
public class bf extends android.app.Dialog implements android.support.v7.app.ai {
    private android.support.v7.app.aj a;

    private bf(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public bf(android.content.Context p5, int p6)
    {
        if (p6 == 0) {
            android.support.v7.app.aj v0_1 = new android.util.TypedValue();
            p5.getTheme().resolveAttribute(android.support.v7.a.d.dialogTheme, v0_1, 1);
            p6 = v0_1.resourceId;
        }
        this(p5, p6);
        this.c().c();
        return;
    }

    private bf(android.content.Context p1, boolean p2, android.content.DialogInterface$OnCancelListener p3)
    {
        this(p1, p2, p3);
        return;
    }

    private static int a(android.content.Context p4, int p5)
    {
        if (p5 == 0) {
            android.util.TypedValue v0_1 = new android.util.TypedValue();
            p4.getTheme().resolveAttribute(android.support.v7.a.d.dialogTheme, v0_1, 1);
            p5 = v0_1.resourceId;
        }
        return p5;
    }

    private android.support.v7.app.a b()
    {
        return this.c().a();
    }

    private android.support.v7.app.aj c()
    {
        if (this.a == null) {
            this.a = android.support.v7.app.aj.a(this.getContext(), this.getWindow(), this);
        }
        return this.a;
    }

    public final boolean a()
    {
        return this.c().b(1);
    }

    public void addContentView(android.view.View p2, android.view.ViewGroup$LayoutParams p3)
    {
        this.c().b(p2, p3);
        return;
    }

    public final void e()
    {
        return;
    }

    public final void f()
    {
        return;
    }

    public final android.support.v7.c.a g()
    {
        return 0;
    }

    public void invalidateOptionsMenu()
    {
        this.c().g();
        return;
    }

    protected void onCreate(android.os.Bundle p2)
    {
        this.c().j();
        super.onCreate(p2);
        this.c().c();
        return;
    }

    protected void onStop()
    {
        super.onStop();
        this.c().e();
        return;
    }

    public void setContentView(int p2)
    {
        this.c().a(p2);
        return;
    }

    public void setContentView(android.view.View p2)
    {
        this.c().a(p2);
        return;
    }

    public void setContentView(android.view.View p2, android.view.ViewGroup$LayoutParams p3)
    {
        this.c().a(p2, p3);
        return;
    }

    public void setTitle(int p3)
    {
        super.setTitle(p3);
        this.c().a(this.getContext().getString(p3));
        return;
    }

    public void setTitle(CharSequence p2)
    {
        super.setTitle(p2);
        this.c().a(p2);
        return;
    }
}
