package android.support.v7.app;
final class ad extends android.os.Handler {
    private static final int a = 1;
    private ref.WeakReference b;

    public ad(android.content.DialogInterface p2)
    {
        this.b = new ref.WeakReference(p2);
        return;
    }

    public final void handleMessage(android.os.Message p4)
    {
        switch (p4.what) {
            case -3:
            case -2:
            case -1:
                ((android.content.DialogInterface$OnClickListener) p4.obj).onClick(((android.content.DialogInterface) this.b.get()), p4.what);
                break;
            case 0:
            default:
                break;
            case 1:
                ((android.content.DialogInterface) p4.obj).dismiss();
                break;
        }
        return;
    }
}
