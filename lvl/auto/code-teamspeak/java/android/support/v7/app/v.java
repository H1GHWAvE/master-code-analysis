package android.support.v7.app;
final class v {
    android.widget.TextView A;
    android.widget.TextView B;
    android.view.View C;
    android.widget.ListAdapter D;
    int E;
    int F;
    int G;
    int H;
    int I;
    int J;
    int K;
    int L;
    android.os.Handler M;
    final android.view.View$OnClickListener N;
    final android.content.Context a;
    final android.support.v7.app.bf b;
    final android.view.Window c;
    CharSequence d;
    CharSequence e;
    android.widget.ListView f;
    android.view.View g;
    int h;
    int i;
    int j;
    int k;
    int l;
    boolean m;
    android.widget.Button n;
    CharSequence o;
    android.os.Message p;
    android.widget.Button q;
    CharSequence r;
    android.os.Message s;
    android.widget.Button t;
    CharSequence u;
    android.os.Message v;
    android.widget.ScrollView w;
    int x;
    android.graphics.drawable.Drawable y;
    android.widget.ImageView z;

    public v(android.content.Context p5, android.support.v7.app.bf p6, android.view.Window p7)
    {
        this.m = 0;
        this.x = 0;
        this.E = -1;
        this.L = 0;
        this.N = new android.support.v7.app.w(this);
        this.a = p5;
        this.b = p6;
        this.c = p7;
        this.M = new android.support.v7.app.ad(p6);
        android.content.res.TypedArray v0_6 = p5.obtainStyledAttributes(0, android.support.v7.a.n.AlertDialog, android.support.v7.a.d.alertDialogStyle, 0);
        this.F = v0_6.getResourceId(android.support.v7.a.n.AlertDialog_android_layout, 0);
        this.G = v0_6.getResourceId(android.support.v7.a.n.AlertDialog_buttonPanelSideLayout, 0);
        this.H = v0_6.getResourceId(android.support.v7.a.n.AlertDialog_listLayout, 0);
        this.I = v0_6.getResourceId(android.support.v7.a.n.AlertDialog_multiChoiceItemLayout, 0);
        this.J = v0_6.getResourceId(android.support.v7.a.n.AlertDialog_singleChoiceItemLayout, 0);
        this.K = v0_6.getResourceId(android.support.v7.a.n.AlertDialog_listItemLayout, 0);
        v0_6.recycle();
        return;
    }

    private static synthetic int a(android.support.v7.app.v p0, int p1)
    {
        p0.E = p1;
        return p1;
    }

    private static synthetic android.widget.Button a(android.support.v7.app.v p1)
    {
        return p1.n;
    }

    private static synthetic android.widget.ListAdapter a(android.support.v7.app.v p0, android.widget.ListAdapter p1)
    {
        p0.D = p1;
        return p1;
    }

    private static synthetic android.widget.ListView a(android.support.v7.app.v p0, android.widget.ListView p1)
    {
        p0.f = p1;
        return p1;
    }

    private void a()
    {
        android.content.res.TypedArray v0_3;
        int v2_0 = 0;
        this.b.a();
        if ((this.G == 0) || (this.L != 1)) {
            v0_3 = this.F;
        } else {
            v0_3 = this.G;
        }
        this.b.setContentView(v0_3);
        android.content.res.TypedArray v0_6 = ((android.view.ViewGroup) this.c.findViewById(android.support.v7.a.i.contentPanel));
        this.w = ((android.widget.ScrollView) this.c.findViewById(android.support.v7.a.i.scrollView));
        this.w.setFocusable(0);
        this.B = ((android.widget.TextView) this.c.findViewById(16908299));
        if (this.B != null) {
            if (this.e == null) {
                this.B.setVisibility(8);
                this.w.removeView(this.B);
                if (this.f == null) {
                    v0_6.setVisibility(8);
                } else {
                    android.content.res.TypedArray v0_9 = ((android.view.ViewGroup) this.w.getParent());
                    int v1_15 = v0_9.indexOfChild(this.w);
                    v0_9.removeViewAt(v1_15);
                    v0_9.addView(this.f, v1_15, new android.view.ViewGroup$LayoutParams(-1, -1));
                }
            } else {
                this.B.setText(this.e);
            }
        }
        int v1_20;
        this.n = ((android.widget.Button) this.c.findViewById(16908313));
        this.n.setOnClickListener(this.N);
        if (!android.text.TextUtils.isEmpty(this.o)) {
            this.n.setText(this.o);
            this.n.setVisibility(0);
            v1_20 = 1;
        } else {
            this.n.setVisibility(8);
            v1_20 = 0;
        }
        this.q = ((android.widget.Button) this.c.findViewById(16908314));
        this.q.setOnClickListener(this.N);
        if (!android.text.TextUtils.isEmpty(this.r)) {
            this.q.setText(this.r);
            this.q.setVisibility(0);
            v1_20 |= 2;
        } else {
            this.q.setVisibility(8);
        }
        this.t = ((android.widget.Button) this.c.findViewById(16908315));
        this.t.setOnClickListener(this.N);
        if (!android.text.TextUtils.isEmpty(this.u)) {
            this.t.setText(this.u);
            this.t.setVisibility(0);
            v1_20 |= 4;
        } else {
            this.t.setVisibility(8);
        }
        android.content.res.TypedArray v0_41;
        android.content.res.TypedArray v0_38 = this.a;
        int v4_11 = new android.util.TypedValue();
        v0_38.getTheme().resolveAttribute(android.support.v7.a.d.alertDialogCenterButtons, v4_11, 1);
        if (v4_11.data == 0) {
            v0_41 = 0;
        } else {
            v0_41 = 1;
        }
        if (v0_41 != null) {
            if (v1_20 != 1) {
                if (v1_20 != 2) {
                    if (v1_20 == 4) {
                        android.support.v7.app.v.a(this.t);
                    }
                } else {
                    android.support.v7.app.v.a(this.q);
                }
            } else {
                android.support.v7.app.v.a(this.n);
            }
        }
        int v4_12;
        if (v1_20 == 0) {
            v4_12 = 0;
        } else {
            v4_12 = 1;
        }
        android.content.res.TypedArray v0_49 = ((android.view.ViewGroup) this.c.findViewById(android.support.v7.a.i.topPanel));
        android.support.v7.internal.widget.ax v5_4 = android.support.v7.internal.widget.ax.a(this.a, 0, android.support.v7.a.n.AlertDialog, android.support.v7.a.d.alertDialogStyle);
        if (this.C == null) {
            int v1_29;
            this.z = ((android.widget.ImageView) this.c.findViewById(16908294));
            if (android.text.TextUtils.isEmpty(this.d)) {
                v1_29 = 0;
            } else {
                v1_29 = 1;
            }
            if (v1_29 == 0) {
                this.c.findViewById(android.support.v7.a.i.title_template).setVisibility(8);
                this.z.setVisibility(8);
                v0_49.setVisibility(8);
            } else {
                this.A = ((android.widget.TextView) this.c.findViewById(android.support.v7.a.i.alertTitle));
                this.A.setText(this.d);
                if (this.x == 0) {
                    if (this.y == null) {
                        this.A.setPadding(this.z.getPaddingLeft(), this.z.getPaddingTop(), this.z.getPaddingRight(), this.z.getPaddingBottom());
                        this.z.setVisibility(8);
                    } else {
                        this.z.setImageDrawable(this.y);
                    }
                } else {
                    this.z.setImageResource(this.x);
                }
            }
        } else {
            v0_49.addView(this.C, 0, new android.view.ViewGroup$LayoutParams(-1, -2));
            this.c.findViewById(android.support.v7.a.i.title_template).setVisibility(8);
        }
        android.content.res.TypedArray v0_63 = this.c.findViewById(android.support.v7.a.i.buttonPanel);
        if (v4_12 == 0) {
            v0_63.setVisibility(8);
            android.content.res.TypedArray v0_65 = this.c.findViewById(android.support.v7.a.i.textSpacerNoButtons);
            if (v0_65 != null) {
                v0_65.setVisibility(0);
            }
        }
        int v4_13;
        android.content.res.TypedArray v0_68 = ((android.widget.FrameLayout) this.c.findViewById(android.support.v7.a.i.customPanel));
        if (this.g == null) {
            if (this.h == 0) {
                v4_13 = 0;
            } else {
                v4_13 = android.view.LayoutInflater.from(this.a).inflate(this.h, v0_68, 0);
            }
        } else {
            v4_13 = this.g;
        }
        if (v4_13 != 0) {
            v2_0 = 1;
        }
        if ((v2_0 == 0) || (!android.support.v7.app.v.a(v4_13))) {
            this.c.setFlags(131072, 131072);
        }
        if (v2_0 == 0) {
            v0_68.setVisibility(8);
        } else {
            int v1_56 = ((android.widget.FrameLayout) this.c.findViewById(android.support.v7.a.i.custom));
            v1_56.addView(v4_13, new android.view.ViewGroup$LayoutParams(-1, -1));
            if (this.m) {
                v1_56.setPadding(this.i, this.j, this.k, this.l);
            }
            if (this.f != null) {
                ((android.widget.LinearLayout$LayoutParams) v0_68.getLayoutParams()).weight = 0;
            }
        }
        android.content.res.TypedArray v0_71 = this.f;
        if ((v0_71 != null) && (this.D != null)) {
            v0_71.setAdapter(this.D);
            int v1_61 = this.E;
            if (v1_61 >= 0) {
                v0_71.setItemChecked(v1_61, 1);
                v0_71.setSelection(v1_61);
            }
        }
        v5_4.a.recycle();
        return;
    }

    static void a(android.widget.Button p2)
    {
        android.widget.LinearLayout$LayoutParams v0_1 = ((android.widget.LinearLayout$LayoutParams) p2.getLayoutParams());
        v0_1.gravity = 1;
        v0_1.weight = 1056964608;
        p2.setLayoutParams(v0_1);
        return;
    }

    private static boolean a(android.content.Context p4)
    {
        int v0 = 1;
        int v1_1 = new android.util.TypedValue();
        p4.getTheme().resolveAttribute(android.support.v7.a.d.alertDialogCenterButtons, v1_1, 1);
        if (v1_1.data == 0) {
            v0 = 0;
        }
        return v0;
    }

    private boolean a(android.view.KeyEvent p2)
    {
        if ((this.w == null) || (!this.w.executeKeyEvent(p2))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    static boolean a(android.view.View p4)
    {
        int v0 = 1;
        if (!p4.onCheckIsTextEditor()) {
            if ((p4 instanceof android.view.ViewGroup)) {
                int v2_2 = ((android.view.ViewGroup) p4).getChildCount();
                while (v2_2 > 0) {
                    v2_2--;
                    if (android.support.v7.app.v.a(((android.view.ViewGroup) p4).getChildAt(v2_2))) {
                    }
                }
                v0 = 0;
            } else {
                v0 = 0;
            }
        }
        return v0;
    }

    private boolean a(android.view.ViewGroup p8)
    {
        int v0_10;
        if (this.C == null) {
            int v0_6;
            this.z = ((android.widget.ImageView) this.c.findViewById(16908294));
            if (android.text.TextUtils.isEmpty(this.d)) {
                v0_6 = 0;
            } else {
                v0_6 = 1;
            }
            if (v0_6 == 0) {
                this.c.findViewById(android.support.v7.a.i.title_template).setVisibility(8);
                this.z.setVisibility(8);
                p8.setVisibility(8);
                v0_10 = 0;
            } else {
                this.A = ((android.widget.TextView) this.c.findViewById(android.support.v7.a.i.alertTitle));
                this.A.setText(this.d);
                if (this.x == 0) {
                    if (this.y == null) {
                        this.A.setPadding(this.z.getPaddingLeft(), this.z.getPaddingTop(), this.z.getPaddingRight(), this.z.getPaddingBottom());
                        this.z.setVisibility(8);
                        v0_10 = 1;
                    } else {
                        this.z.setImageDrawable(this.y);
                        v0_10 = 1;
                    }
                } else {
                    this.z.setImageResource(this.x);
                    v0_10 = 1;
                }
            }
        } else {
            p8.addView(this.C, 0, new android.view.ViewGroup$LayoutParams(-1, -2));
            this.c.findViewById(android.support.v7.a.i.title_template).setVisibility(8);
            v0_10 = 1;
        }
        return v0_10;
    }

    private int b()
    {
        int v0_2;
        if (this.G != 0) {
            if (this.L != 1) {
                v0_2 = this.F;
            } else {
                v0_2 = this.G;
            }
        } else {
            v0_2 = this.F;
        }
        return v0_2;
    }

    private static synthetic android.os.Message b(android.support.v7.app.v p1)
    {
        return p1.p;
    }

    private void b(int p2)
    {
        this.g = 0;
        this.h = p2;
        this.m = 0;
        return;
    }

    private void b(android.view.ViewGroup p6)
    {
        this.w = ((android.widget.ScrollView) this.c.findViewById(android.support.v7.a.i.scrollView));
        this.w.setFocusable(0);
        this.B = ((android.widget.TextView) this.c.findViewById(16908299));
        if (this.B != null) {
            if (this.e == null) {
                this.B.setVisibility(8);
                this.w.removeView(this.B);
                if (this.f == null) {
                    p6.setVisibility(8);
                } else {
                    android.view.ViewGroup v0_14 = ((android.view.ViewGroup) this.w.getParent());
                    int v1_5 = v0_14.indexOfChild(this.w);
                    v0_14.removeViewAt(v1_5);
                    v0_14.addView(this.f, v1_5, new android.view.ViewGroup$LayoutParams(-1, -1));
                }
            } else {
                this.B.setText(this.e);
            }
        }
        return;
    }

    private boolean b(android.view.KeyEvent p2)
    {
        if ((this.w == null) || (!this.w.executeKeyEvent(p2))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private static synthetic android.widget.Button c(android.support.v7.app.v p1)
    {
        return p1.q;
    }

    private android.widget.ListView c()
    {
        return this.f;
    }

    private void c(int p1)
    {
        this.L = p1;
        return;
    }

    private void c(android.view.View p1)
    {
        this.C = p1;
        return;
    }

    private int d(int p4)
    {
        int v0_1 = new android.util.TypedValue();
        this.a.getTheme().resolveAttribute(p4, v0_1, 1);
        return v0_1.resourceId;
    }

    private static synthetic android.os.Message d(android.support.v7.app.v p1)
    {
        return p1.s;
    }

    private void d()
    {
        int v2_0 = 0;
        android.content.res.TypedArray v0_2 = ((android.view.ViewGroup) this.c.findViewById(android.support.v7.a.i.contentPanel));
        this.w = ((android.widget.ScrollView) this.c.findViewById(android.support.v7.a.i.scrollView));
        this.w.setFocusable(0);
        this.B = ((android.widget.TextView) this.c.findViewById(16908299));
        if (this.B != null) {
            if (this.e == null) {
                this.B.setVisibility(8);
                this.w.removeView(this.B);
                if (this.f == null) {
                    v0_2.setVisibility(8);
                } else {
                    android.content.res.TypedArray v0_5 = ((android.view.ViewGroup) this.w.getParent());
                    int v1_14 = v0_5.indexOfChild(this.w);
                    v0_5.removeViewAt(v1_14);
                    v0_5.addView(this.f, v1_14, new android.view.ViewGroup$LayoutParams(-1, -1));
                }
            } else {
                this.B.setText(this.e);
            }
        }
        int v1_19;
        this.n = ((android.widget.Button) this.c.findViewById(16908313));
        this.n.setOnClickListener(this.N);
        if (!android.text.TextUtils.isEmpty(this.o)) {
            this.n.setText(this.o);
            this.n.setVisibility(0);
            v1_19 = 1;
        } else {
            this.n.setVisibility(8);
            v1_19 = 0;
        }
        this.q = ((android.widget.Button) this.c.findViewById(16908314));
        this.q.setOnClickListener(this.N);
        if (!android.text.TextUtils.isEmpty(this.r)) {
            this.q.setText(this.r);
            this.q.setVisibility(0);
            v1_19 |= 2;
        } else {
            this.q.setVisibility(8);
        }
        this.t = ((android.widget.Button) this.c.findViewById(16908315));
        this.t.setOnClickListener(this.N);
        if (!android.text.TextUtils.isEmpty(this.u)) {
            this.t.setText(this.u);
            this.t.setVisibility(0);
            v1_19 |= 4;
        } else {
            this.t.setVisibility(8);
        }
        android.content.res.TypedArray v0_37;
        android.content.res.TypedArray v0_34 = this.a;
        int v4_11 = new android.util.TypedValue();
        v0_34.getTheme().resolveAttribute(android.support.v7.a.d.alertDialogCenterButtons, v4_11, 1);
        if (v4_11.data == 0) {
            v0_37 = 0;
        } else {
            v0_37 = 1;
        }
        if (v0_37 != null) {
            if (v1_19 != 1) {
                if (v1_19 != 2) {
                    if (v1_19 == 4) {
                        android.support.v7.app.v.a(this.t);
                    }
                } else {
                    android.support.v7.app.v.a(this.q);
                }
            } else {
                android.support.v7.app.v.a(this.n);
            }
        }
        int v4_12;
        if (v1_19 == 0) {
            v4_12 = 0;
        } else {
            v4_12 = 1;
        }
        android.content.res.TypedArray v0_45 = ((android.view.ViewGroup) this.c.findViewById(android.support.v7.a.i.topPanel));
        android.support.v7.internal.widget.ax v5_4 = android.support.v7.internal.widget.ax.a(this.a, 0, android.support.v7.a.n.AlertDialog, android.support.v7.a.d.alertDialogStyle);
        if (this.C == null) {
            int v1_28;
            this.z = ((android.widget.ImageView) this.c.findViewById(16908294));
            if (android.text.TextUtils.isEmpty(this.d)) {
                v1_28 = 0;
            } else {
                v1_28 = 1;
            }
            if (v1_28 == 0) {
                this.c.findViewById(android.support.v7.a.i.title_template).setVisibility(8);
                this.z.setVisibility(8);
                v0_45.setVisibility(8);
            } else {
                this.A = ((android.widget.TextView) this.c.findViewById(android.support.v7.a.i.alertTitle));
                this.A.setText(this.d);
                if (this.x == 0) {
                    if (this.y == null) {
                        this.A.setPadding(this.z.getPaddingLeft(), this.z.getPaddingTop(), this.z.getPaddingRight(), this.z.getPaddingBottom());
                        this.z.setVisibility(8);
                    } else {
                        this.z.setImageDrawable(this.y);
                    }
                } else {
                    this.z.setImageResource(this.x);
                }
            }
        } else {
            v0_45.addView(this.C, 0, new android.view.ViewGroup$LayoutParams(-1, -2));
            this.c.findViewById(android.support.v7.a.i.title_template).setVisibility(8);
        }
        android.content.res.TypedArray v0_59 = this.c.findViewById(android.support.v7.a.i.buttonPanel);
        if (v4_12 == 0) {
            v0_59.setVisibility(8);
            android.content.res.TypedArray v0_61 = this.c.findViewById(android.support.v7.a.i.textSpacerNoButtons);
            if (v0_61 != null) {
                v0_61.setVisibility(0);
            }
        }
        int v4_13;
        android.content.res.TypedArray v0_64 = ((android.widget.FrameLayout) this.c.findViewById(android.support.v7.a.i.customPanel));
        if (this.g == null) {
            if (this.h == 0) {
                v4_13 = 0;
            } else {
                v4_13 = android.view.LayoutInflater.from(this.a).inflate(this.h, v0_64, 0);
            }
        } else {
            v4_13 = this.g;
        }
        if (v4_13 != 0) {
            v2_0 = 1;
        }
        if ((v2_0 == 0) || (!android.support.v7.app.v.a(v4_13))) {
            this.c.setFlags(131072, 131072);
        }
        if (v2_0 == 0) {
            v0_64.setVisibility(8);
        } else {
            int v1_55 = ((android.widget.FrameLayout) this.c.findViewById(android.support.v7.a.i.custom));
            v1_55.addView(v4_13, new android.view.ViewGroup$LayoutParams(-1, -1));
            if (this.m) {
                v1_55.setPadding(this.i, this.j, this.k, this.l);
            }
            if (this.f != null) {
                ((android.widget.LinearLayout$LayoutParams) v0_64.getLayoutParams()).weight = 0;
            }
        }
        android.content.res.TypedArray v0_67 = this.f;
        if ((v0_67 != null) && (this.D != null)) {
            v0_67.setAdapter(this.D);
            int v1_60 = this.E;
            if (v1_60 >= 0) {
                v0_67.setItemChecked(v1_60, 1);
                v0_67.setSelection(v1_60);
            }
        }
        v5_4.a.recycle();
        return;
    }

    private android.widget.Button e(int p2)
    {
        android.widget.Button v0;
        switch (p2) {
            case -3:
                v0 = this.t;
                break;
            case -2:
                v0 = this.q;
                break;
            case -1:
                v0 = this.n;
                break;
            default:
                v0 = 0;
        }
        return v0;
    }

    private static synthetic android.widget.Button e(android.support.v7.app.v p1)
    {
        return p1.t;
    }

    private boolean e()
    {
        int v1_3;
        int v3 = 1;
        this.n = ((android.widget.Button) this.c.findViewById(16908313));
        this.n.setOnClickListener(this.N);
        if (!android.text.TextUtils.isEmpty(this.o)) {
            this.n.setText(this.o);
            this.n.setVisibility(0);
            v1_3 = 1;
        } else {
            this.n.setVisibility(8);
            v1_3 = 0;
        }
        this.q = ((android.widget.Button) this.c.findViewById(16908314));
        this.q.setOnClickListener(this.N);
        if (!android.text.TextUtils.isEmpty(this.r)) {
            this.q.setText(this.r);
            this.q.setVisibility(0);
            v1_3 |= 2;
        } else {
            this.q.setVisibility(8);
        }
        this.t = ((android.widget.Button) this.c.findViewById(16908315));
        this.t.setOnClickListener(this.N);
        if (!android.text.TextUtils.isEmpty(this.u)) {
            this.t.setText(this.u);
            this.t.setVisibility(0);
            v1_3 |= 4;
        } else {
            this.t.setVisibility(8);
        }
        android.widget.Button v0_30;
        android.widget.Button v0_27 = this.a;
        android.util.TypedValue v4_7 = new android.util.TypedValue();
        v0_27.getTheme().resolveAttribute(android.support.v7.a.d.alertDialogCenterButtons, v4_7, 1);
        if (v4_7.data == 0) {
            v0_30 = 0;
        } else {
            v0_30 = 1;
        }
        if (v0_30 != null) {
            if (v1_3 != 1) {
                if (v1_3 != 2) {
                    if (v1_3 == 4) {
                        android.support.v7.app.v.a(this.t);
                    }
                } else {
                    android.support.v7.app.v.a(this.q);
                }
            } else {
                android.support.v7.app.v.a(this.n);
            }
        }
        if (v1_3 == 0) {
            v3 = 0;
        }
        return v3;
    }

    private static synthetic android.os.Message f(android.support.v7.app.v p1)
    {
        return p1.v;
    }

    private static synthetic android.support.v7.app.bf g(android.support.v7.app.v p1)
    {
        return p1.b;
    }

    private static synthetic android.os.Handler h(android.support.v7.app.v p1)
    {
        return p1.M;
    }

    private static synthetic int i(android.support.v7.app.v p1)
    {
        return p1.H;
    }

    private static synthetic int j(android.support.v7.app.v p1)
    {
        return p1.I;
    }

    private static synthetic int k(android.support.v7.app.v p1)
    {
        return p1.J;
    }

    private static synthetic int l(android.support.v7.app.v p1)
    {
        return p1.K;
    }

    public final void a(int p3)
    {
        this.y = 0;
        this.x = p3;
        if (this.z != null) {
            if (p3 == 0) {
                this.z.setVisibility(8);
            } else {
                this.z.setImageResource(this.x);
            }
        }
        return;
    }

    public final void a(int p3, CharSequence p4, android.content.DialogInterface$OnClickListener p5, android.os.Message p6)
    {
        if ((p6 == null) && (p5 != null)) {
            p6 = this.M.obtainMessage(p3, p5);
        }
        switch (p3) {
            case -3:
                this.u = p4;
                this.v = p6;
                break;
            case -2:
                this.r = p4;
                this.s = p6;
                break;
            case -1:
                this.o = p4;
                this.p = p6;
                break;
            default:
                throw new IllegalArgumentException("Button does not exist");
        }
        return;
    }

    public final void a(android.graphics.drawable.Drawable p3)
    {
        this.y = p3;
        this.x = 0;
        if (this.z != null) {
            if (p3 == null) {
                this.z.setVisibility(8);
            } else {
                this.z.setImageDrawable(p3);
            }
        }
        return;
    }

    public final void a(android.view.View p2, int p3, int p4, int p5, int p6)
    {
        this.g = p2;
        this.h = 0;
        this.m = 1;
        this.i = p3;
        this.j = p4;
        this.k = p5;
        this.l = p6;
        return;
    }

    public final void a(CharSequence p2)
    {
        this.d = p2;
        if (this.A != null) {
            this.A.setText(p2);
        }
        return;
    }

    public final void b(android.view.View p2)
    {
        this.g = p2;
        this.h = 0;
        this.m = 0;
        return;
    }

    public final void b(CharSequence p2)
    {
        this.e = p2;
        if (this.B != null) {
            this.B.setText(p2);
        }
        return;
    }
}
