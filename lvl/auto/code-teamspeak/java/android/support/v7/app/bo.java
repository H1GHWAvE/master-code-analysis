package android.support.v7.app;
final class bo {
    static android.support.v7.app.bo a = None;
    public static final int b = 0;
    public static final int c = 1;
    private static final float g = 1016003125;
    private static final float h = 980151802;
    private static final float i = 3184949072;
    private static final float j = 1023992574;
    private static final float k = 968295128;
    private static final float l = 917483750;
    private static final float m = 1053920540;
    private static final long n = 946728000000;
    public long d;
    public long e;
    public int f;

    bo()
    {
        return;
    }

    private static android.support.v7.app.bo a()
    {
        if (android.support.v7.app.bo.a == null) {
            android.support.v7.app.bo.a = new android.support.v7.app.bo();
        }
        return android.support.v7.app.bo.a;
    }

    public final void a(long p16, double p18, double p20)
    {
        int v2_3 = (((float) (p16 - 4.67745780756e-312)) / 1285868416);
        float v3_2 = (1086828178 + (1015868197 * v2_3));
        double v4_7 = (((((((double) v3_2) + (0.03341960161924362 * Math.sin(((double) v3_2)))) + (0.00034906598739326 * Math.sin(((double) (1073741824 * v3_2))))) + (5.236000106378924e-06 * Math.sin(((double) (1077936128 * v3_2))))) + 1.796593063) + 3.141592653589793);
        long v6_9 = ((- p20) / 360.0);
        int v2_11 = (((Math.sin(((double) v3_2)) * 0.0053) + (v6_9 + ((double) (((float) Math.round((((double) (v2_3 - 980151802)) - v6_9))) + 980151802)))) + (-0.0069 * Math.sin((2.0 * v4_7))));
        double v4_10 = Math.asin((Math.sin(v4_7) * Math.sin(0.4092797040939331)));
        long v6_16 = (0.01745329238474369 * p18);
        double v4_13 = ((Math.sin(-0.10471975803375244) - (Math.sin(v6_16) * Math.sin(v4_10))) / (Math.cos(v4_10) * Math.cos(v6_16)));
        if (v4_13 < 1.0) {
            if (v4_13 > -1.0) {
                double v4_16 = ((float) (Math.acos(v4_13) / 6.283185307179586));
                this.d = (Math.round(((((double) v4_16) + v2_11) * 86400000.0)) + 4.67745780756e-312);
                this.e = (Math.round(((v2_11 - ((double) v4_16)) * 86400000.0)) + 4.67745780756e-312);
                if ((this.e >= p16) || (this.d <= p16)) {
                    this.f = 1;
                } else {
                    this.f = 0;
                }
            } else {
                this.f = 0;
                this.d = -1;
                this.e = -1;
            }
        } else {
            this.f = 1;
            this.d = -1;
            this.e = -1;
        }
        return;
    }
}
