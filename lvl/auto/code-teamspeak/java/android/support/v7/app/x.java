package android.support.v7.app;
public final class x {
    public int A;
    public boolean B;
    public boolean[] C;
    public boolean D;
    public boolean E;
    public int F;
    public android.content.DialogInterface$OnMultiChoiceClickListener G;
    public android.database.Cursor H;
    public String I;
    public String J;
    public boolean K;
    public android.widget.AdapterView$OnItemSelectedListener L;
    public android.support.v7.app.ac M;
    public boolean N;
    public final android.content.Context a;
    public final android.view.LayoutInflater b;
    public int c;
    public android.graphics.drawable.Drawable d;
    public int e;
    public CharSequence f;
    public android.view.View g;
    public CharSequence h;
    public CharSequence i;
    public android.content.DialogInterface$OnClickListener j;
    public CharSequence k;
    public android.content.DialogInterface$OnClickListener l;
    public CharSequence m;
    public android.content.DialogInterface$OnClickListener n;
    public boolean o;
    public android.content.DialogInterface$OnCancelListener p;
    public android.content.DialogInterface$OnDismissListener q;
    public android.content.DialogInterface$OnKeyListener r;
    public CharSequence[] s;
    public android.widget.ListAdapter t;
    public android.content.DialogInterface$OnClickListener u;
    public int v;
    public android.view.View w;
    public int x;
    public int y;
    public int z;

    public x(android.content.Context p3)
    {
        this.c = 0;
        this.e = 0;
        this.B = 0;
        this.F = -1;
        this.N = 1;
        this.a = p3;
        this.o = 1;
        this.b = ((android.view.LayoutInflater) p3.getSystemService("layout_inflater"));
        return;
    }

    private void a(android.support.v7.app.v p13)
    {
        if (this.g == null) {
            if (this.f != null) {
                p13.a(this.f);
            }
            if (this.d != null) {
                p13.a(this.d);
            }
            if (this.c != 0) {
                p13.a(this.c);
            }
            if (this.e != 0) {
                int v0_8 = this.e;
                android.content.Context v1_1 = new android.util.TypedValue();
                p13.a.getTheme().resolveAttribute(v0_8, v1_1, 1);
                p13.a(v1_1.resourceId);
            }
        } else {
            p13.C = this.g;
        }
        if (this.h != null) {
            p13.b(this.h);
        }
        if (this.i != null) {
            p13.a(-1, this.i, this.j, 0);
        }
        if (this.k != null) {
            p13.a(-2, this.k, this.l, 0);
        }
        if (this.m != null) {
            p13.a(-3, this.m, this.n, 0);
        }
        if ((this.s != null) || ((this.H != null) || (this.t != null))) {
            int v0_30;
            int v5_1 = ((android.widget.ListView) this.b.inflate(p13.H, 0));
            if (!this.D) {
                int v8;
                if (!this.E) {
                    v8 = p13.K;
                } else {
                    v8 = p13.J;
                }
                if (this.H != null) {
                    android.content.Context v7 = this.a;
                    android.database.Cursor v9 = this.H;
                    String[] v10 = new String[1];
                    v10[0] = this.I;
                    int[] v11 = new int[1];
                    v11[0] = 16908308;
                    v0_30 = new android.widget.SimpleCursorAdapter(v7, v8, v9, v10, v11);
                } else {
                    if (this.t == null) {
                        v0_30 = new android.support.v7.app.ae(this.a, v8, this.s);
                    } else {
                        v0_30 = this.t;
                    }
                }
            } else {
                if (this.H != null) {
                    v0_30 = new android.support.v7.app.z(this, this.a, this.H, v5_1, p13);
                } else {
                    v0_30 = new android.support.v7.app.y(this, this.a, p13.I, this.s, v5_1);
                }
            }
            p13.D = v0_30;
            p13.E = this.F;
            if (this.u == null) {
                if (this.G != null) {
                    v5_1.setOnItemClickListener(new android.support.v7.app.ab(this, v5_1, p13));
                }
            } else {
                v5_1.setOnItemClickListener(new android.support.v7.app.aa(this, p13));
            }
            if (this.L != null) {
                v5_1.setOnItemSelectedListener(this.L);
            }
            if (!this.E) {
                if (this.D) {
                    v5_1.setChoiceMode(2);
                }
            } else {
                v5_1.setChoiceMode(1);
            }
            p13.f = v5_1;
        }
        if (this.w == null) {
            if (this.v != 0) {
                int v0_50 = this.v;
                p13.g = 0;
                p13.h = v0_50;
                p13.m = 0;
            }
        } else {
            if (!this.B) {
                p13.b(this.w);
            } else {
                p13.a(this.w, this.x, this.y, this.z, this.A);
            }
        }
        return;
    }

    private void b(android.support.v7.app.v p14)
    {
        int v0_6;
        android.widget.ListView v5_1 = ((android.widget.ListView) this.b.inflate(p14.H, 0));
        if (!this.D) {
            int v8;
            if (!this.E) {
                v8 = p14.K;
            } else {
                v8 = p14.J;
            }
            if (this.H != null) {
                android.content.Context v7 = this.a;
                android.database.Cursor v9 = this.H;
                String[] v10 = new String[1];
                v10[0] = this.I;
                int[] v11 = new int[1];
                v11[0] = 16908308;
                v0_6 = new android.widget.SimpleCursorAdapter(v7, v8, v9, v10, v11);
            } else {
                if (this.t == null) {
                    v0_6 = new android.support.v7.app.ae(this.a, v8, this.s);
                } else {
                    v0_6 = this.t;
                }
            }
        } else {
            if (this.H != null) {
                v0_6 = new android.support.v7.app.z(this, this.a, this.H, v5_1, p14);
            } else {
                v0_6 = new android.support.v7.app.y(this, this.a, p14.I, this.s, v5_1);
            }
        }
        p14.D = v0_6;
        p14.E = this.F;
        if (this.u == null) {
            if (this.G != null) {
                v5_1.setOnItemClickListener(new android.support.v7.app.ab(this, v5_1, p14));
            }
        } else {
            v5_1.setOnItemClickListener(new android.support.v7.app.aa(this, p14));
        }
        if (this.L != null) {
            v5_1.setOnItemSelectedListener(this.L);
        }
        if (!this.E) {
            if (this.D) {
                v5_1.setChoiceMode(2);
            }
        } else {
            v5_1.setChoiceMode(1);
        }
        p14.f = v5_1;
        return;
    }
}
