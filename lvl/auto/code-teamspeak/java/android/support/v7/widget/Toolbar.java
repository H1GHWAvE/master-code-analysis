package android.support.v7.widget;
public class Toolbar extends android.view.ViewGroup {
    private static final String o = "Toolbar";
    private int A;
    private CharSequence B;
    private CharSequence C;
    private int D;
    private int E;
    private boolean F;
    private boolean G;
    private final java.util.ArrayList H;
    private final int[] I;
    private android.support.v7.widget.cl J;
    private final android.support.v7.widget.o K;
    private android.support.v7.internal.widget.ay L;
    private boolean M;
    private final Runnable N;
    private final android.support.v7.internal.widget.av O;
    public android.support.v7.widget.ActionMenuView a;
    public android.widget.TextView b;
    public android.widget.TextView c;
    android.view.View d;
    public android.content.Context e;
    public int f;
    public int g;
    public int h;
    public final android.support.v7.internal.widget.ak i;
    final java.util.ArrayList j;
    public android.support.v7.widget.ActionMenuPresenter k;
    public android.support.v7.widget.cj l;
    public android.support.v7.internal.view.menu.y m;
    public android.support.v7.internal.view.menu.j n;
    private android.widget.ImageButton p;
    private android.widget.ImageView q;
    private android.graphics.drawable.Drawable r;
    private CharSequence s;
    private android.widget.ImageButton t;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    public Toolbar(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public Toolbar(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, android.support.v7.a.d.toolbarStyle);
        return;
    }

    public Toolbar(android.content.Context p9, android.util.AttributeSet p10, int p11)
    {
        this(p9, p10, p11);
        this.i = new android.support.v7.internal.widget.ak();
        this.A = 8388627;
        this.H = new java.util.ArrayList();
        this.j = new java.util.ArrayList();
        android.support.v7.internal.widget.av v0_8 = new int[2];
        this.I = v0_8;
        this.K = new android.support.v7.widget.cg(this);
        this.N = new android.support.v7.widget.ch(this);
        android.support.v7.internal.widget.av v0_14 = android.support.v7.internal.widget.ax.a(this.getContext(), p10, android.support.v7.a.n.Toolbar, p11);
        this.g = v0_14.e(android.support.v7.a.n.Toolbar_titleTextAppearance, 0);
        this.h = v0_14.e(android.support.v7.a.n.Toolbar_subtitleTextAppearance, 0);
        this.A = v0_14.a.getInteger(android.support.v7.a.n.Toolbar_android_gravity, this.A);
        this.u = 48;
        android.content.res.TypedArray v1_9 = v0_14.b(android.support.v7.a.n.Toolbar_titleMargins, 0);
        this.z = v1_9;
        this.y = v1_9;
        this.x = v1_9;
        this.w = v1_9;
        android.content.res.TypedArray v1_11 = v0_14.b(android.support.v7.a.n.Toolbar_titleMarginStart, -1);
        if (v1_11 >= null) {
            this.w = v1_11;
        }
        android.content.res.TypedArray v1_13 = v0_14.b(android.support.v7.a.n.Toolbar_titleMarginEnd, -1);
        if (v1_13 >= null) {
            this.x = v1_13;
        }
        android.content.res.TypedArray v1_15 = v0_14.b(android.support.v7.a.n.Toolbar_titleMarginTop, -1);
        if (v1_15 >= null) {
            this.y = v1_15;
        }
        android.content.res.TypedArray v1_17 = v0_14.b(android.support.v7.a.n.Toolbar_titleMarginBottom, -1);
        if (v1_17 >= null) {
            this.z = v1_17;
        }
        this.v = v0_14.c(android.support.v7.a.n.Toolbar_maxButtonHeight, -1);
        android.content.res.TypedArray v1_21 = v0_14.b(android.support.v7.a.n.Toolbar_contentInsetStart, -2147483648);
        boolean v2_2 = v0_14.b(android.support.v7.a.n.Toolbar_contentInsetEnd, -2147483648);
        this.i.b(v0_14.c(android.support.v7.a.n.Toolbar_contentInsetLeft, 0), v0_14.c(android.support.v7.a.n.Toolbar_contentInsetRight, 0));
        if ((v1_21 != -2147483648) || (v2_2 != -2147483648)) {
            this.i.a(v1_21, v2_2);
        }
        this.r = v0_14.a(android.support.v7.a.n.Toolbar_collapseIcon);
        this.s = v0_14.c(android.support.v7.a.n.Toolbar_collapseContentDescription);
        android.content.res.TypedArray v1_27 = v0_14.c(android.support.v7.a.n.Toolbar_title);
        if (!android.text.TextUtils.isEmpty(v1_27)) {
            this.setTitle(v1_27);
        }
        android.content.res.TypedArray v1_29 = v0_14.c(android.support.v7.a.n.Toolbar_subtitle);
        if (!android.text.TextUtils.isEmpty(v1_29)) {
            this.setSubtitle(v1_29);
        }
        this.e = this.getContext();
        this.setPopupTheme(v0_14.e(android.support.v7.a.n.Toolbar_popupTheme, 0));
        android.content.res.TypedArray v1_34 = v0_14.a(android.support.v7.a.n.Toolbar_navigationIcon);
        if (v1_34 != null) {
            this.setNavigationIcon(v1_34);
        }
        android.content.res.TypedArray v1_36 = v0_14.c(android.support.v7.a.n.Toolbar_navigationContentDescription);
        if (!android.text.TextUtils.isEmpty(v1_36)) {
            this.setNavigationContentDescription(v1_36);
        }
        android.content.res.TypedArray v1_38 = v0_14.a(android.support.v7.a.n.Toolbar_logo);
        if (v1_38 != null) {
            this.setLogo(v1_38);
        }
        android.content.res.TypedArray v1_40 = v0_14.c(android.support.v7.a.n.Toolbar_logoDescription);
        if (!android.text.TextUtils.isEmpty(v1_40)) {
            this.setLogoDescription(v1_40);
        }
        if (v0_14.e(android.support.v7.a.n.Toolbar_titleTextColor)) {
            this.setTitleTextColor(v0_14.d(android.support.v7.a.n.Toolbar_titleTextColor));
        }
        if (v0_14.e(android.support.v7.a.n.Toolbar_subtitleTextColor)) {
            this.setSubtitleTextColor(v0_14.d(android.support.v7.a.n.Toolbar_subtitleTextColor));
        }
        v0_14.a.recycle();
        this.O = v0_14.a();
        return;
    }

    private int a(android.view.View p9, int p10)
    {
        int v1_0;
        int v0_1 = ((android.support.v7.widget.ck) p9.getLayoutParams());
        int v4_0 = p9.getMeasuredHeight();
        if (p10 <= 0) {
            v1_0 = 0;
        } else {
            v1_0 = ((v4_0 - p10) / 2);
        }
        int v3_1 = (v0_1.a & 112);
        switch (v3_1) {
            case 16:
            case 48:
            case 80:
                break;
            case 16:
            case 48:
            case 80:
                break;
            case 16:
            case 48:
            case 80:
                break;
            default:
                v3_1 = (this.A & 112);
        }
        int v0_4;
        switch (v3_1) {
            case 48:
                v0_4 = (this.getPaddingTop() - v1_0);
                break;
            case 80:
                v0_4 = ((((this.getHeight() - this.getPaddingBottom()) - v4_0) - v0_1.bottomMargin) - v1_0);
                break;
            default:
                int v0_6;
                int v3_4 = this.getPaddingTop();
                int v5_0 = this.getPaddingBottom();
                int v6 = this.getHeight();
                int v1_5 = ((((v6 - v3_4) - v5_0) - v4_0) / 2);
                if (v1_5 >= v0_1.topMargin) {
                    int v4_3 = ((((v6 - v5_0) - v4_0) - v1_5) - v3_4);
                    if (v4_3 >= v0_1.bottomMargin) {
                        v0_6 = v1_5;
                    } else {
                        v0_6 = Math.max(0, (v1_5 - (v0_1.bottomMargin - v4_3)));
                    }
                } else {
                    v0_6 = v0_1.topMargin;
                }
                v0_4 = (v0_6 + v3_4);
        }
        return v0_4;
    }

    private int a(android.view.View p8, int p9, int p10, int p11, int p12, int[] p13)
    {
        int v0_1 = ((android.view.ViewGroup$MarginLayoutParams) p8.getLayoutParams());
        int v1_1 = (v0_1.leftMargin - p13[0]);
        int v2_2 = (v0_1.rightMargin - p13[1]);
        int v3_2 = (Math.max(0, v1_1) + Math.max(0, v2_2));
        p13[0] = Math.max(0, (- v1_1));
        p13[1] = Math.max(0, (- v2_2));
        p8.measure(android.support.v7.widget.Toolbar.getChildMeasureSpec(p9, (((this.getPaddingLeft() + this.getPaddingRight()) + v3_2) + p10), v0_1.width), android.support.v7.widget.Toolbar.getChildMeasureSpec(p11, ((((this.getPaddingTop() + this.getPaddingBottom()) + v0_1.topMargin) + v0_1.bottomMargin) + p12), v0_1.height));
        return (p8.getMeasuredWidth() + v3_2);
    }

    private int a(android.view.View p7, int p8, int[] p9, int p10)
    {
        int v0_1 = ((android.support.v7.widget.ck) p7.getLayoutParams());
        int v1_1 = (v0_1.leftMargin - p9[0]);
        int v2_2 = (Math.max(0, v1_1) + p8);
        p9[0] = Math.max(0, (- v1_1));
        int v1_4 = this.a(p7, p10);
        int v3_1 = p7.getMeasuredWidth();
        p7.layout(v2_2, v1_4, (v2_2 + v3_1), (p7.getMeasuredHeight() + v1_4));
        return ((v0_1.rightMargin + v3_1) + v2_2);
    }

    private static int a(java.util.List p10, int[] p11)
    {
        int v0_0 = p11[0];
        int v1_1 = p11[1];
        int v7 = p10.size();
        int v2 = 0;
        int v4 = 0;
        int v5_0 = v0_0;
        int v6 = v1_1;
        while (v2 < v7) {
            int v0_2 = ((android.view.View) p10.get(v2));
            int v1_3 = ((android.support.v7.widget.ck) v0_2.getLayoutParams());
            int v5_1 = (v1_3.leftMargin - v5_0);
            int v1_5 = (v1_3.rightMargin - v6);
            int v8_1 = Math.max(0, v5_1);
            int v9 = Math.max(0, v1_5);
            v5_0 = Math.max(0, (- v5_1));
            v6 = Math.max(0, (- v1_5));
            v2++;
            v4 += ((v0_2.getMeasuredWidth() + v8_1) + v9);
        }
        return v4;
    }

    private android.support.v7.widget.ck a(android.util.AttributeSet p3)
    {
        return new android.support.v7.widget.ck(this.getContext(), p3);
    }

    private static android.support.v7.widget.ck a(android.view.ViewGroup$LayoutParams p1)
    {
        android.support.v7.widget.ck v0_4;
        if (!(p1 instanceof android.support.v7.widget.ck)) {
            if (!(p1 instanceof android.support.v7.app.c)) {
                if (!(p1 instanceof android.view.ViewGroup$MarginLayoutParams)) {
                    v0_4 = new android.support.v7.widget.ck(p1);
                } else {
                    v0_4 = new android.support.v7.widget.ck(((android.view.ViewGroup$MarginLayoutParams) p1));
                }
            } else {
                v0_4 = new android.support.v7.widget.ck(((android.support.v7.app.c) p1));
            }
        } else {
            v0_4 = new android.support.v7.widget.ck(((android.support.v7.widget.ck) p1));
        }
        return v0_4;
    }

    static synthetic android.support.v7.widget.cl a(android.support.v7.widget.Toolbar p1)
    {
        return p1.J;
    }

    private void a(int p3)
    {
        this.getMenuInflater().inflate(p3, this.getMenu());
        return;
    }

    private void a(int p2, int p3)
    {
        this.i.a(p2, p3);
        return;
    }

    private void a(android.content.Context p2, int p3)
    {
        this.g = p3;
        if (this.b != null) {
            this.b.setTextAppearance(p2, p3);
        }
        return;
    }

    private void a(android.support.v7.internal.view.menu.i p5, android.support.v7.widget.ActionMenuPresenter p6)
    {
        if ((p5 != null) || (this.a != null)) {
            this.d();
            android.support.v7.widget.ActionMenuView v0_2 = this.a.c;
            if (v0_2 != p5) {
                if (v0_2 != null) {
                    v0_2.b(this.k);
                    v0_2.b(this.l);
                }
                if (this.l == null) {
                    this.l = new android.support.v7.widget.cj(this, 0);
                }
                p6.o = 1;
                if (p5 == null) {
                    p6.a(this.e, 0);
                    this.l.a(this.e, 0);
                    p6.a(1);
                    this.l.a(1);
                } else {
                    p5.a(p6, this.e);
                    p5.a(this.l, this.e);
                }
                this.a.setPopupTheme(this.f);
                this.a.setPresenter(p6);
                this.k = p6;
            }
        }
        return;
    }

    private void a(android.support.v7.internal.view.menu.y p1, android.support.v7.internal.view.menu.j p2)
    {
        this.m = p1;
        this.n = p2;
        return;
    }

    private void a(android.view.View p6, int p7, int p8, int p9, int p10)
    {
        int v0_1 = ((android.view.ViewGroup$MarginLayoutParams) p6.getLayoutParams());
        int v1_5 = android.support.v7.widget.Toolbar.getChildMeasureSpec(p7, ((((this.getPaddingLeft() + this.getPaddingRight()) + v0_1.leftMargin) + v0_1.rightMargin) + p8), v0_1.width);
        int v0_3 = android.support.v7.widget.Toolbar.getChildMeasureSpec(p9, ((((this.getPaddingTop() + this.getPaddingBottom()) + v0_1.topMargin) + v0_1.bottomMargin) + 0), v0_1.height);
        int v2_9 = android.view.View$MeasureSpec.getMode(v0_3);
        if ((v2_9 != 1073741824) && (p10 >= 0)) {
            if (v2_9 != 0) {
                p10 = Math.min(android.view.View$MeasureSpec.getSize(v0_3), p10);
            }
            v0_3 = android.view.View$MeasureSpec.makeMeasureSpec(p10, 1073741824);
        }
        p6.measure(v1_5, v0_3);
        return;
    }

    private void a(android.view.View p3, boolean p4)
    {
        java.util.ArrayList v0_1;
        java.util.ArrayList v0_0 = p3.getLayoutParams();
        if (v0_0 != null) {
            if (this.checkLayoutParams(v0_0)) {
                v0_1 = ((android.support.v7.widget.ck) v0_0);
            } else {
                v0_1 = android.support.v7.widget.Toolbar.a(v0_0);
            }
        } else {
            v0_1 = new android.support.v7.widget.ck();
        }
        v0_1.e = 1;
        if ((!p4) || (this.d == null)) {
            this.addView(p3, v0_1);
        } else {
            p3.setLayoutParams(v0_1);
            this.j.add(p3);
        }
        return;
    }

    private void a(java.util.List p7, int p8)
    {
        int v0_0 = 1;
        int v1_0 = 0;
        if (android.support.v4.view.cx.f(this) != 1) {
            v0_0 = 0;
        }
        android.view.View v2_1 = this.getChildCount();
        int v3_1 = android.support.v4.view.v.a(p8, android.support.v4.view.cx.f(this));
        p7.clear();
        if (v0_0 == 0) {
            while (v1_0 < v2_1) {
                boolean v4_0 = this.getChildAt(v1_0);
                int v0_2 = ((android.support.v7.widget.ck) v4_0.getLayoutParams());
                if ((v0_2.e == 0) && ((this.a(v4_0)) && (this.c(v0_2.a) == v3_1))) {
                    p7.add(v4_0);
                }
                v1_0++;
            }
        } else {
            int v1_1 = (v2_1 - 1);
            while (v1_1 >= 0) {
                android.view.View v2_2 = this.getChildAt(v1_1);
                int v0_7 = ((android.support.v7.widget.ck) v2_2.getLayoutParams());
                if ((v0_7.e == 0) && ((this.a(v2_2)) && (this.c(v0_7.a) == v3_1))) {
                    p7.add(v2_2);
                }
                v1_1--;
            }
        }
        return;
    }

    private boolean a(android.view.View p3)
    {
        if ((p3 == null) || ((p3.getParent() != this) || (p3.getVisibility() == 8))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private int b(int p2)
    {
        int v0_0 = (p2 & 112);
        switch (v0_0) {
            case 16:
            case 48:
            case 80:
                break;
            case 16:
            case 48:
            case 80:
                break;
            case 16:
            case 48:
            case 80:
                break;
            default:
                v0_0 = (this.A & 112);
        }
        return v0_0;
    }

    private static int b(android.view.View p2)
    {
        int v0_1 = ((android.view.ViewGroup$MarginLayoutParams) p2.getLayoutParams());
        return (android.support.v4.view.at.b(v0_1) + android.support.v4.view.at.a(v0_1));
    }

    private int b(android.view.View p7, int p8, int[] p9, int p10)
    {
        int v0_1 = ((android.support.v7.widget.ck) p7.getLayoutParams());
        int v1_1 = (v0_1.rightMargin - p9[1]);
        int v2_2 = (p8 - Math.max(0, v1_1));
        p9[1] = Math.max(0, (- v1_1));
        int v1_4 = this.a(p7, p10);
        int v3_1 = p7.getMeasuredWidth();
        p7.layout((v2_2 - v3_1), v1_4, v2_2, (p7.getMeasuredHeight() + v1_4));
        return (v2_2 - (v0_1.leftMargin + v3_1));
    }

    private void b(int p2, int p3)
    {
        this.i.b(p2, p3);
        return;
    }

    private void b(android.content.Context p2, int p3)
    {
        this.h = p3;
        if (this.c != null) {
            this.c.setTextAppearance(p2, p3);
        }
        return;
    }

    static synthetic void b(android.support.v7.widget.Toolbar p4)
    {
        if (p4.t == null) {
            p4.t = new android.widget.ImageButton(p4.getContext(), 0, android.support.v7.a.d.toolbarNavigationButtonStyle);
            p4.t.setImageDrawable(p4.r);
            p4.t.setContentDescription(p4.s);
            android.widget.ImageButton v0_6 = new android.support.v7.widget.ck();
            v0_6.a = (8388611 | (p4.u & 112));
            v0_6.e = 2;
            p4.t.setLayoutParams(v0_6);
            p4.t.setOnClickListener(new android.support.v7.widget.ci(p4));
        }
        return;
    }

    private int c(int p3)
    {
        int v1 = android.support.v4.view.cx.f(this);
        int v0_1 = (android.support.v4.view.v.a(p3, v1) & 7);
        switch (v0_1) {
            case 1:
            case 3:
            case 5:
                break;
            case 2:
            case 4:
            default:
                if (v1 != 1) {
                    v0_1 = 3;
                } else {
                    v0_1 = 5;
                }
            case 1:
            case 3:
            case 5:
                break;
            case 1:
            case 3:
            case 5:
                break;
        }
        return v0_1;
    }

    private static int c(android.view.View p2)
    {
        int v0_1 = ((android.view.ViewGroup$MarginLayoutParams) p2.getLayoutParams());
        return (v0_1.bottomMargin + v0_1.topMargin);
    }

    static synthetic android.widget.ImageButton c(android.support.v7.widget.Toolbar p1)
    {
        return p1.t;
    }

    static synthetic int d(android.support.v7.widget.Toolbar p1)
    {
        return p1.u;
    }

    private static boolean d(android.view.View p1)
    {
        int v0_3;
        if (((android.support.v7.widget.ck) p1.getLayoutParams()).e != 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    protected static android.support.v7.widget.ck e()
    {
        return new android.support.v7.widget.ck();
    }

    private boolean e(android.view.View p2)
    {
        if ((p2.getParent() != this) && (!this.j.contains(p2))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private boolean f()
    {
        if ((this.getVisibility() != 0) || ((this.a == null) || (!this.a.d))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    private boolean g()
    {
        int v0 = 1;
        if (this.a == null) {
            v0 = 0;
        } else {
            int v2_4;
            int v2_1 = this.a;
            if ((v2_1.e == null) || (!v2_1.e.j())) {
                v2_4 = 0;
            } else {
                v2_4 = 1;
            }
            if (v2_4 == 0) {
            }
        }
        return v0;
    }

    private android.view.MenuInflater getMenuInflater()
    {
        return new android.support.v7.internal.view.f(this.getContext());
    }

    private boolean h()
    {
        int v0 = 1;
        if (this.a == null) {
            v0 = 0;
        } else {
            int v2_4;
            int v2_1 = this.a;
            if ((v2_1.e == null) || (!v2_1.e.f())) {
                v2_4 = 0;
            } else {
                v2_4 = 1;
            }
            if (v2_4 == 0) {
            }
        }
        return v0;
    }

    private void i()
    {
        if (this.a != null) {
            this.a.b();
        }
        return;
    }

    private boolean j()
    {
        int v0 = 0;
        if (this.b != null) {
            android.text.Layout v2 = this.b.getLayout();
            if (v2 != null) {
                int v3 = v2.getLineCount();
                int v1_2 = 0;
                while (v1_2 < v3) {
                    if (v2.getEllipsisCount(v1_2) <= 0) {
                        v1_2++;
                    } else {
                        v0 = 1;
                        break;
                    }
                }
            }
        }
        return v0;
    }

    private void k()
    {
        if (this.q == null) {
            this.q = new android.widget.ImageView(this.getContext());
        }
        return;
    }

    private boolean l()
    {
        if ((this.l == null) || (this.l.b == null)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private void m()
    {
        this.d();
        if (this.a.c == null) {
            android.support.v7.internal.view.menu.i v0_4 = ((android.support.v7.internal.view.menu.i) this.a.getMenu());
            if (this.l == null) {
                this.l = new android.support.v7.widget.cj(this, 0);
            }
            this.a.setExpandedActionViewsExclusive(1);
            v0_4.a(this.l, this.e);
        }
        return;
    }

    private void n()
    {
        if (this.p == null) {
            this.p = new android.widget.ImageButton(this.getContext(), 0, android.support.v7.a.d.toolbarNavigationButtonStyle);
            android.support.v7.widget.ck v0_4 = new android.support.v7.widget.ck();
            v0_4.a = (8388611 | (this.u & 112));
            this.p.setLayoutParams(v0_4);
        }
        return;
    }

    private void o()
    {
        if (this.t == null) {
            this.t = new android.widget.ImageButton(this.getContext(), 0, android.support.v7.a.d.toolbarNavigationButtonStyle);
            this.t.setImageDrawable(this.r);
            this.t.setContentDescription(this.s);
            android.widget.ImageButton v0_6 = new android.support.v7.widget.ck();
            v0_6.a = (8388611 | (this.u & 112));
            v0_6.e = 2;
            this.t.setLayoutParams(v0_6);
            this.t.setOnClickListener(new android.support.v7.widget.ci(this));
        }
        return;
    }

    private void p()
    {
        this.removeCallbacks(this.N);
        this.post(this.N);
        return;
    }

    private boolean q()
    {
        int v0 = 0;
        if (this.M) {
            int v2 = this.getChildCount();
            int v1_1 = 0;
            while (v1_1 < v2) {
                int v3_0 = this.getChildAt(v1_1);
                if ((!this.a(v3_0)) || ((v3_0.getMeasuredWidth() <= 0) || (v3_0.getMeasuredHeight() <= 0))) {
                    v1_1++;
                }
            }
            v0 = 1;
        }
        return v0;
    }

    private void r()
    {
        int v1 = (this.getChildCount() - 1);
        while (v1 >= 0) {
            android.view.View v2 = this.getChildAt(v1);
            if ((((android.support.v7.widget.ck) v2.getLayoutParams()).e != 2) && (v2 != this.a)) {
                this.removeViewAt(v1);
                this.j.add(v2);
            }
            v1--;
        }
        return;
    }

    private void s()
    {
        int v1 = (this.j.size() - 1);
        while (v1 >= 0) {
            this.addView(((android.view.View) this.j.get(v1)));
            v1--;
        }
        this.j.clear();
        return;
    }

    public final boolean a()
    {
        int v0 = 1;
        if (this.a == null) {
            v0 = 0;
        } else {
            int v2_4;
            int v2_1 = this.a;
            if ((v2_1.e == null) || (!v2_1.e.i())) {
                v2_4 = 0;
            } else {
                v2_4 = 1;
            }
            if (v2_4 == 0) {
            }
        }
        return v0;
    }

    public final boolean b()
    {
        int v0 = 1;
        if (this.a == null) {
            v0 = 0;
        } else {
            int v2_4;
            int v2_1 = this.a;
            if ((v2_1.e == null) || (!v2_1.e.e())) {
                v2_4 = 0;
            } else {
                v2_4 = 1;
            }
            if (v2_4 == 0) {
            }
        }
        return v0;
    }

    public final void c()
    {
        android.support.v7.internal.view.menu.m v0_2;
        if (this.l != null) {
            v0_2 = this.l.b;
        } else {
            v0_2 = 0;
        }
        if (v0_2 != null) {
            v0_2.collapseActionView();
        }
        return;
    }

    protected boolean checkLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        if ((!super.checkLayoutParams(p2)) || (!(p2 instanceof android.support.v7.widget.ck))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final void d()
    {
        if (this.a == null) {
            this.a = new android.support.v7.widget.ActionMenuView(this.getContext());
            this.a.setPopupTheme(this.f);
            this.a.setOnMenuItemClickListener(this.K);
            android.support.v7.widget.ActionMenuView v0_5 = this.a;
            int v2_0 = this.n;
            v0_5.f = this.m;
            v0_5.g = v2_0;
            android.support.v7.widget.ActionMenuView v0_7 = new android.support.v7.widget.ck();
            v0_7.a = (8388613 | (this.u & 112));
            this.a.setLayoutParams(v0_7);
            this.a(this.a, 0);
        }
        return;
    }

    protected synthetic android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return new android.support.v7.widget.ck();
    }

    public synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.util.AttributeSet p3)
    {
        return new android.support.v7.widget.ck(this.getContext(), p3);
    }

    protected synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return android.support.v7.widget.Toolbar.a(p2);
    }

    public int getContentInsetEnd()
    {
        int v0_1;
        int v0_0 = this.i;
        if (!v0_0.h) {
            v0_1 = v0_0.c;
        } else {
            v0_1 = v0_0.b;
        }
        return v0_1;
    }

    public int getContentInsetLeft()
    {
        return this.i.b;
    }

    public int getContentInsetRight()
    {
        return this.i.c;
    }

    public int getContentInsetStart()
    {
        int v0_1;
        int v0_0 = this.i;
        if (!v0_0.h) {
            v0_1 = v0_0.b;
        } else {
            v0_1 = v0_0.c;
        }
        return v0_1;
    }

    public android.graphics.drawable.Drawable getLogo()
    {
        int v0_1;
        if (this.q == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.q.getDrawable();
        }
        return v0_1;
    }

    public CharSequence getLogoDescription()
    {
        int v0_1;
        if (this.q == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.q.getContentDescription();
        }
        return v0_1;
    }

    public android.view.Menu getMenu()
    {
        this.m();
        return this.a.getMenu();
    }

    public CharSequence getNavigationContentDescription()
    {
        int v0_1;
        if (this.p == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.p.getContentDescription();
        }
        return v0_1;
    }

    public android.graphics.drawable.Drawable getNavigationIcon()
    {
        int v0_1;
        if (this.p == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.p.getDrawable();
        }
        return v0_1;
    }

    public android.graphics.drawable.Drawable getOverflowIcon()
    {
        this.m();
        return this.a.getOverflowIcon();
    }

    public int getPopupTheme()
    {
        return this.f;
    }

    public CharSequence getSubtitle()
    {
        return this.C;
    }

    public CharSequence getTitle()
    {
        return this.B;
    }

    public android.support.v7.internal.widget.ad getWrapper()
    {
        if (this.L == null) {
            this.L = new android.support.v7.internal.widget.ay(this, 1);
        }
        return this.L;
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.removeCallbacks(this.N);
        return;
    }

    public boolean onHoverEvent(android.view.MotionEvent p6)
    {
        int v0 = android.support.v4.view.bk.a(p6);
        if (v0 == 9) {
            this.G = 0;
        }
        if (!this.G) {
            int v1_1 = super.onHoverEvent(p6);
            if ((v0 == 9) && (v1_1 == 0)) {
                this.G = 1;
            }
        }
        if ((v0 == 10) || (v0 == 3)) {
            this.G = 0;
        }
        return 1;
    }

    protected void onLayout(boolean p24, int p25, int p26, int p27, int p28)
    {
        int v5_0;
        if (android.support.v4.view.cx.f(this) != 1) {
            v5_0 = 0;
        } else {
            v5_0 = 1;
        }
        int v4_4;
        int v12 = this.getWidth();
        android.widget.TextView v13_0 = this.getHeight();
        int v6_0 = this.getPaddingLeft();
        int v14 = this.getPaddingRight();
        int v15_0 = this.getPaddingTop();
        int v16_0 = this.getPaddingBottom();
        int v3_3 = (v12 - v14);
        int[] v17 = this.I;
        v17[1] = 0;
        v17[0] = 0;
        int v18 = android.support.v4.view.cx.o(this);
        if (!this.a(this.p)) {
            v4_4 = v6_0;
        } else {
            if (v5_0 == 0) {
                v4_4 = this.a(this.p, v6_0, v17, v18);
            } else {
                v3_3 = this.b(this.p, v3_3, v17, v18);
                v4_4 = v6_0;
            }
        }
        if (this.a(this.t)) {
            if (v5_0 == 0) {
                v4_4 = this.a(this.t, v4_4, v17, v18);
            } else {
                v3_3 = this.b(this.t, v3_3, v17, v18);
            }
        }
        if (this.a(this.a)) {
            if (v5_0 == 0) {
                v3_3 = this.b(this.a, v3_3, v17, v18);
            } else {
                v4_4 = this.a(this.a, v4_4, v17, v18);
            }
        }
        v17[0] = Math.max(0, (this.getContentInsetLeft() - v4_4));
        v17[1] = Math.max(0, (this.getContentInsetRight() - ((v12 - v14) - v3_3)));
        int v4_7 = Math.max(v4_4, this.getContentInsetLeft());
        int v3_4 = Math.min(v3_3, ((v12 - v14) - this.getContentInsetRight()));
        if (this.a(this.d)) {
            if (v5_0 == 0) {
                v4_7 = this.a(this.d, v4_7, v17, v18);
            } else {
                v3_4 = this.b(this.d, v3_4, v17, v18);
            }
        }
        int v7_20;
        int v8_6;
        if (!this.a(this.q)) {
            v7_20 = v3_4;
            v8_6 = v4_7;
        } else {
            if (v5_0 == 0) {
                v7_20 = v3_4;
                v8_6 = this.a(this.q, v4_7, v17, v18);
            } else {
                v7_20 = this.b(this.q, v3_4, v17, v18);
                v8_6 = v4_7;
            }
        }
        int v19_0 = this.a(this.b);
        boolean v20 = this.a(this.c);
        int v4_9 = 0;
        if (v19_0 != 0) {
            int v3_10 = ((android.support.v7.widget.ck) this.b.getLayoutParams());
            v4_9 = ((v3_10.bottomMargin + (v3_10.topMargin + this.b.getMeasuredHeight())) + 0);
        }
        int v11_0;
        if (!v20) {
            v11_0 = v4_9;
        } else {
            int v3_15 = ((android.support.v7.widget.ck) this.c.getLayoutParams());
            v11_0 = ((v3_15.bottomMargin + (v3_15.topMargin + this.c.getMeasuredHeight())) + v4_9);
        }
        if ((v19_0 != 0) || (v20)) {
            int v9_8;
            if (v19_0 == 0) {
                v9_8 = this.c;
            } else {
                v9_8 = this.b;
            }
            int v4_12;
            if (!v20) {
                v4_12 = this.b;
            } else {
                v4_12 = this.c;
            }
            int v9_13;
            int v3_24 = ((android.support.v7.widget.ck) v9_8.getLayoutParams());
            int v4_14 = ((android.support.v7.widget.ck) v4_12.getLayoutParams());
            if (((v19_0 == 0) || (this.b.getMeasuredWidth() <= 0)) && ((!v20) || (this.c.getMeasuredWidth() <= 0))) {
                v9_13 = 0;
            } else {
                v9_13 = 1;
            }
            int v10_6;
            switch ((this.A & 112)) {
                case 48:
                    v10_6 = ((v3_24.topMargin + this.getPaddingTop()) + this.y);
                    break;
                case 80:
                    v10_6 = ((((v13_0 - v16_0) - v4_14.bottomMargin) - this.z) - v11_0);
                    break;
                default:
                    int v3_32;
                    int v10_10 = ((((v13_0 - v15_0) - v16_0) - v11_0) / 2);
                    if (v10_10 >= (v3_24.topMargin + this.y)) {
                        int v11_3 = ((((v13_0 - v16_0) - v11_0) - v10_10) - v15_0);
                        if (v11_3 >= (v3_24.bottomMargin + this.z)) {
                            v3_32 = v10_10;
                        } else {
                            v3_32 = Math.max(0, (v10_10 - ((v4_14.bottomMargin + this.z) - v11_3)));
                        }
                    } else {
                        v3_32 = (v3_24.topMargin + this.y);
                    }
                    v10_6 = (v15_0 + v3_32);
            }
            if (v5_0 == 0) {
                int v3_35;
                if (v9_13 == 0) {
                    v3_35 = 0;
                } else {
                    v3_35 = this.w;
                }
                int v5_2;
                int v4_29;
                int v3_36 = (v3_35 - v17[0]);
                v8_6 += Math.max(0, v3_36);
                v17[0] = Math.max(0, (- v3_36));
                if (v19_0 == 0) {
                    v5_2 = v8_6;
                    v4_29 = v10_6;
                } else {
                    int v3_41 = ((android.support.v7.widget.ck) this.b.getLayoutParams());
                    int v4_32 = (this.b.getMeasuredWidth() + v8_6);
                    int v5_5 = (this.b.getMeasuredHeight() + v10_6);
                    this.b.layout(v8_6, v10_6, v4_32, v5_5);
                    v5_2 = (v4_32 + this.x);
                    v4_29 = (v3_41.bottomMargin + v5_5);
                }
                int v3_44;
                if (!v20) {
                    v3_44 = v8_6;
                } else {
                    int v4_34 = (v4_29 + ((android.support.v7.widget.ck) this.c.getLayoutParams()).topMargin);
                    int v10_15 = (this.c.getMeasuredWidth() + v8_6);
                    this.c.layout(v8_6, v4_34, v10_15, (this.c.getMeasuredHeight() + v4_34));
                    v3_44 = (this.x + v10_15);
                }
                if (v9_13 != 0) {
                    v8_6 = Math.max(v5_2, v3_44);
                }
            } else {
                int v3_48;
                if (v9_13 == 0) {
                    v3_48 = 0;
                } else {
                    v3_48 = this.w;
                }
                int v7_24;
                int v3_49 = (v3_48 - v17[1]);
                int v4_41 = (v7_20 - Math.max(0, v3_49));
                v17[1] = Math.max(0, (- v3_49));
                if (v19_0 == 0) {
                    v7_24 = v4_41;
                } else {
                    int v3_54 = ((android.support.v7.widget.ck) this.b.getLayoutParams());
                    int v5_9 = (v4_41 - this.b.getMeasuredWidth());
                    int v7_27 = (this.b.getMeasuredHeight() + v10_6);
                    this.b.layout(v5_9, v10_6, v4_41, v7_27);
                    v10_6 = (v7_27 + v3_54.bottomMargin);
                    v7_24 = (v5_9 - this.x);
                }
                int v3_56;
                if (!v20) {
                    v3_56 = v4_41;
                } else {
                    int v5_12 = (((android.support.v7.widget.ck) this.c.getLayoutParams()).topMargin + v10_6);
                    this.c.layout((v4_41 - this.c.getMeasuredWidth()), v5_12, v4_41, (this.c.getMeasuredHeight() + v5_12));
                    v3_56 = (v4_41 - this.x);
                }
                int v3_60;
                if (v9_13 == 0) {
                    v3_60 = v4_41;
                } else {
                    v3_60 = Math.min(v7_24, v3_56);
                }
                v7_20 = v3_60;
            }
        }
        this.a(this.H, 3);
        int v9_14 = this.H.size();
        int v4_43 = 0;
        int v5_15 = v8_6;
        while (v4_43 < v9_14) {
            v5_15 = this.a(((android.view.View) this.H.get(v4_43)), v5_15, v17, v18);
            v4_43++;
        }
        this.a(this.H, 5);
        int v8_7 = this.H.size();
        int v4_45 = 0;
        int v11_12 = v7_20;
        while (v4_45 < v8_7) {
            int v7_30 = this.b(((android.view.View) this.H.get(v4_45)), v11_12, v17, v18);
            v4_45++;
            v11_12 = v7_30;
        }
        this.a(this.H, 1);
        android.widget.TextView v13_6 = this.H;
        int v7_28 = v17[0];
        int v8_8 = v17[1];
        int v15_1 = v13_6.size();
        int v9_15 = v7_28;
        int v10_20 = v8_8;
        int v7_29 = 0;
        int v8_9 = 0;
        while (v7_29 < v15_1) {
            int v4_56 = ((android.view.View) v13_6.get(v7_29));
            int v3_83 = ((android.support.v7.widget.ck) v4_56.getLayoutParams());
            int v9_16 = (v3_83.leftMargin - v9_15);
            int v3_85 = (v3_83.rightMargin - v10_20);
            int v16_2 = Math.max(0, v9_16);
            int v19_1 = Math.max(0, v3_85);
            v9_15 = Math.max(0, (- v9_16));
            v10_20 = Math.max(0, (- v3_85));
            v7_29++;
            v8_9 += ((v4_56.getMeasuredWidth() + v16_2) + v19_1);
        }
        int v3_75 = (((((v12 - v6_0) - v14) / 2) + v6_0) - (v8_9 / 2));
        int v4_49 = (v3_75 + v8_9);
        if (v3_75 >= v5_15) {
            if (v4_49 > v11_12) {
                v3_75 -= (v4_49 - v11_12);
            }
        } else {
            v3_75 = v5_15;
        }
        int v6_1 = this.H.size();
        int v5_16 = 0;
        int v4_53 = v3_75;
        while (v5_16 < v6_1) {
            int v3_80 = this.a(((android.view.View) this.H.get(v5_16)), v4_53, v17, v18);
            v5_16++;
            v4_53 = v3_80;
        }
        this.H.clear();
        return;
    }

    protected void onMeasure(int p18, int p19)
    {
        android.support.v7.widget.Toolbar v8_0;
        android.widget.TextView v9_0;
        int[] v7 = this.I;
        if (!android.support.v7.internal.widget.bd.a(this)) {
            v8_0 = 1;
            v9_0 = 0;
        } else {
            v8_0 = 0;
            v9_0 = 1;
        }
        int v10_0;
        int v11_0;
        int v1_3 = 0;
        if (!this.a(this.p)) {
            v10_0 = 0;
            v11_0 = 0;
        } else {
            this.a(this.p, p18, 0, p19, this.v);
            v1_3 = (this.p.getMeasuredWidth() + android.support.v7.widget.Toolbar.b(this.p));
            v10_0 = android.support.v7.internal.widget.bd.a(0, android.support.v4.view.cx.j(this.p));
            v11_0 = Math.max(0, (this.p.getMeasuredHeight() + android.support.v7.widget.Toolbar.c(this.p)));
        }
        if (this.a(this.t)) {
            this.a(this.t, p18, 0, p19, this.v);
            v1_3 = (this.t.getMeasuredWidth() + android.support.v7.widget.Toolbar.b(this.t));
            v11_0 = Math.max(v11_0, (this.t.getMeasuredHeight() + android.support.v7.widget.Toolbar.c(this.t)));
            v10_0 = android.support.v7.internal.widget.bd.a(v10_0, android.support.v4.view.cx.j(this.t));
        }
        int v2_20 = this.getContentInsetStart();
        int v4_7 = (Math.max(v2_20, v1_3) + 0);
        v7[v9_0] = Math.max(0, (v2_20 - v1_3));
        int v1_12 = 0;
        if (this.a(this.a)) {
            this.a(this.a, p18, v4_7, p19, this.v);
            v1_12 = (this.a.getMeasuredWidth() + android.support.v7.widget.Toolbar.b(this.a));
            v11_0 = Math.max(v11_0, (this.a.getMeasuredHeight() + android.support.v7.widget.Toolbar.c(this.a)));
            v10_0 = android.support.v7.internal.widget.bd.a(v10_0, android.support.v4.view.cx.j(this.a));
        }
        int v2_31 = this.getContentInsetEnd();
        int v4_8 = (v4_7 + Math.max(v2_31, v1_12));
        v7[v8_0] = Math.max(0, (v2_31 - v1_12));
        if (this.a(this.d)) {
            v4_8 += this.a(this.d, p18, v4_8, p19, 0, v7);
            v11_0 = Math.max(v11_0, (this.d.getMeasuredHeight() + android.support.v7.widget.Toolbar.c(this.d)));
            v10_0 = android.support.v7.internal.widget.bd.a(v10_0, android.support.v4.view.cx.j(this.d));
        }
        if (this.a(this.q)) {
            v4_8 += this.a(this.q, p18, v4_8, p19, 0, v7);
            v11_0 = Math.max(v11_0, (this.q.getMeasuredHeight() + android.support.v7.widget.Toolbar.c(this.q)));
            v10_0 = android.support.v7.internal.widget.bd.a(v10_0, android.support.v4.view.cx.j(this.q));
        }
        android.widget.TextView v9_1 = this.getChildCount();
        android.support.v7.widget.Toolbar v8_1 = 0;
        int v15 = v10_0;
        int v16 = v11_0;
        while (v8_1 < v9_1) {
            int v1_50;
            int v2_50;
            int v2_49 = this.getChildAt(v8_1);
            if ((((android.support.v7.widget.ck) v2_49.getLayoutParams()).e != 0) || (!this.a(v2_49))) {
                v1_50 = v15;
                v2_50 = v16;
            } else {
                v4_8 += this.a(v2_49, p18, v4_8, p19, 0, v7);
                int v3_37 = Math.max(v16, (v2_49.getMeasuredHeight() + android.support.v7.widget.Toolbar.c(v2_49)));
                v1_50 = android.support.v7.internal.widget.bd.a(v15, android.support.v4.view.cx.j(v2_49));
                v2_50 = v3_37;
            }
            v8_1++;
            v15 = v1_50;
            v16 = v2_50;
        }
        int v2_38 = 0;
        int v1_37 = 0;
        int v13_0 = (this.y + this.z);
        int v3_20 = (this.w + this.x);
        if (this.a(this.b)) {
            this.a(this.b, p18, (v4_8 + v3_20), p19, v13_0, v7);
            v2_38 = (android.support.v7.widget.Toolbar.b(this.b) + this.b.getMeasuredWidth());
            v1_37 = (this.b.getMeasuredHeight() + android.support.v7.widget.Toolbar.c(this.b));
            v15 = android.support.v7.internal.widget.bd.a(v15, android.support.v4.view.cx.j(this.b));
        }
        if (this.a(this.c)) {
            v2_38 = Math.max(v2_38, this.a(this.c, p18, (v4_8 + v3_20), p19, (v13_0 + v1_37), v7));
            v1_37 += (this.c.getMeasuredHeight() + android.support.v7.widget.Toolbar.c(this.c));
            v15 = android.support.v7.internal.widget.bd.a(v15, android.support.v4.view.cx.j(this.c));
        }
        int v2_48;
        int v2_41 = (v2_38 + v4_8);
        int v1_42 = Math.max(v16, v1_37);
        int v2_42 = (v2_41 + (this.getPaddingLeft() + this.getPaddingRight()));
        int v1_43 = (v1_42 + (this.getPaddingTop() + this.getPaddingBottom()));
        int v3_34 = android.support.v4.view.cx.a(Math.max(v2_42, this.getSuggestedMinimumWidth()), p18, (-16777216 & v15));
        int v1_45 = android.support.v4.view.cx.a(Math.max(v1_43, this.getSuggestedMinimumHeight()), p19, (v15 << 16));
        if (this.M) {
            int v4_11 = this.getChildCount();
            int v2_47 = 0;
            while (v2_47 < v4_11) {
                int v5_17 = this.getChildAt(v2_47);
                if ((!this.a(v5_17)) || ((v5_17.getMeasuredWidth() <= 0) || (v5_17.getMeasuredHeight() <= 0))) {
                    v2_47++;
                } else {
                    v2_48 = 0;
                }
            }
            v2_48 = 1;
        } else {
            v2_48 = 0;
        }
        if (v2_48 != 0) {
            v1_45 = 0;
        }
        this.setMeasuredDimension(v3_34, v1_45);
        return;
    }

    protected void onRestoreInstanceState(android.os.Parcelable p3)
    {
        android.view.MenuItem v0_2;
        super.onRestoreInstanceState(((android.support.v7.widget.Toolbar$SavedState) p3).getSuperState());
        if (this.a == null) {
            v0_2 = 0;
        } else {
            v0_2 = this.a.c;
        }
        if ((((android.support.v7.widget.Toolbar$SavedState) p3).a != 0) && ((this.l != null) && (v0_2 != null))) {
            android.view.MenuItem v0_4 = v0_2.findItem(((android.support.v7.widget.Toolbar$SavedState) p3).a);
            if (v0_4 != null) {
                android.support.v4.view.az.b(v0_4);
            }
        }
        if (((android.support.v7.widget.Toolbar$SavedState) p3).b) {
            this.removeCallbacks(this.N);
            this.post(this.N);
        }
        return;
    }

    public void onRtlPropertiesChanged(int p5)
    {
        int v0_0 = 1;
        if (android.os.Build$VERSION.SDK_INT >= 17) {
            super.onRtlPropertiesChanged(p5);
        }
        android.support.v7.internal.widget.ak v1_1 = this.i;
        if (p5 != 1) {
            v0_0 = 0;
        }
        if (v0_0 != v1_1.h) {
            v1_1.h = v0_0;
            if (!v1_1.i) {
                v1_1.b = v1_1.f;
                v1_1.c = v1_1.g;
            } else {
                if (v0_0 == 0) {
                    int v0_4;
                    if (v1_1.d == -2147483648) {
                        v0_4 = v1_1.f;
                    } else {
                        v0_4 = v1_1.d;
                    }
                    int v0_6;
                    v1_1.b = v0_4;
                    if (v1_1.e == -2147483648) {
                        v0_6 = v1_1.g;
                    } else {
                        v0_6 = v1_1.e;
                    }
                    v1_1.c = v0_6;
                } else {
                    int v0_8;
                    if (v1_1.e == -2147483648) {
                        v0_8 = v1_1.f;
                    } else {
                        v0_8 = v1_1.e;
                    }
                    int v0_10;
                    v1_1.b = v0_8;
                    if (v1_1.d == -2147483648) {
                        v0_10 = v1_1.g;
                    } else {
                        v0_10 = v1_1.d;
                    }
                    v1_1.c = v0_10;
                }
            }
        }
        return;
    }

    protected android.os.Parcelable onSaveInstanceState()
    {
        android.support.v7.widget.Toolbar$SavedState v0_1 = new android.support.v7.widget.Toolbar$SavedState(super.onSaveInstanceState());
        if ((this.l != null) && (this.l.b != null)) {
            v0_1.a = this.l.b.getItemId();
        }
        v0_1.b = this.a();
        return v0_1;
    }

    public boolean onTouchEvent(android.view.MotionEvent p5)
    {
        int v0 = android.support.v4.view.bk.a(p5);
        if (v0 == 0) {
            this.F = 0;
        }
        if (!this.F) {
            int v1_1 = super.onTouchEvent(p5);
            if ((v0 == 0) && (v1_1 == 0)) {
                this.F = 1;
            }
        }
        if ((v0 == 1) || (v0 == 3)) {
            this.F = 0;
        }
        return 1;
    }

    public void setCollapsible(boolean p1)
    {
        this.M = p1;
        this.requestLayout();
        return;
    }

    public void setLogo(int p3)
    {
        this.setLogo(this.O.a(p3, 0));
        return;
    }

    public void setLogo(android.graphics.drawable.Drawable p3)
    {
        if (p3 == null) {
            if ((this.q != null) && (this.e(this.q))) {
                this.removeView(this.q);
                this.j.remove(this.q);
            }
        } else {
            this.k();
            if (!this.e(this.q)) {
                this.a(this.q, 1);
            }
        }
        if (this.q != null) {
            this.q.setImageDrawable(p3);
        }
        return;
    }

    public void setLogoDescription(int p2)
    {
        this.setLogoDescription(this.getContext().getText(p2));
        return;
    }

    public void setLogoDescription(CharSequence p2)
    {
        if (!android.text.TextUtils.isEmpty(p2)) {
            this.k();
        }
        if (this.q != null) {
            this.q.setContentDescription(p2);
        }
        return;
    }

    public void setNavigationContentDescription(int p2)
    {
        int v0_0;
        if (p2 == 0) {
            v0_0 = 0;
        } else {
            v0_0 = this.getContext().getText(p2);
        }
        this.setNavigationContentDescription(v0_0);
        return;
    }

    public void setNavigationContentDescription(CharSequence p2)
    {
        if (!android.text.TextUtils.isEmpty(p2)) {
            this.n();
        }
        if (this.p != null) {
            this.p.setContentDescription(p2);
        }
        return;
    }

    public void setNavigationIcon(int p3)
    {
        this.setNavigationIcon(this.O.a(p3, 0));
        return;
    }

    public void setNavigationIcon(android.graphics.drawable.Drawable p3)
    {
        if (p3 == null) {
            if ((this.p != null) && (this.e(this.p))) {
                this.removeView(this.p);
                this.j.remove(this.p);
            }
        } else {
            this.n();
            if (!this.e(this.p)) {
                this.a(this.p, 1);
            }
        }
        if (this.p != null) {
            this.p.setImageDrawable(p3);
        }
        return;
    }

    public void setNavigationOnClickListener(android.view.View$OnClickListener p2)
    {
        this.n();
        this.p.setOnClickListener(p2);
        return;
    }

    public void setOnMenuItemClickListener(android.support.v7.widget.cl p1)
    {
        this.J = p1;
        return;
    }

    public void setOverflowIcon(android.graphics.drawable.Drawable p2)
    {
        this.m();
        this.a.setOverflowIcon(p2);
        return;
    }

    public void setPopupTheme(int p3)
    {
        if (this.f != p3) {
            this.f = p3;
            if (p3 != 0) {
                this.e = new android.view.ContextThemeWrapper(this.getContext(), p3);
            } else {
                this.e = this.getContext();
            }
        }
        return;
    }

    public void setSubtitle(int p2)
    {
        this.setSubtitle(this.getContext().getText(p2));
        return;
    }

    public void setSubtitle(CharSequence p4)
    {
        if (android.text.TextUtils.isEmpty(p4)) {
            if ((this.c != null) && (this.e(this.c))) {
                this.removeView(this.c);
                this.j.remove(this.c);
            }
        } else {
            if (this.c == null) {
                android.widget.TextView v0_7 = this.getContext();
                this.c = new android.widget.TextView(v0_7);
                this.c.setSingleLine();
                this.c.setEllipsize(android.text.TextUtils$TruncateAt.END);
                if (this.h != 0) {
                    this.c.setTextAppearance(v0_7, this.h);
                }
                if (this.E != 0) {
                    this.c.setTextColor(this.E);
                }
            }
            if (!this.e(this.c)) {
                this.a(this.c, 1);
            }
        }
        if (this.c != null) {
            this.c.setText(p4);
        }
        this.C = p4;
        return;
    }

    public void setSubtitleTextColor(int p2)
    {
        this.E = p2;
        if (this.c != null) {
            this.c.setTextColor(p2);
        }
        return;
    }

    public void setTitle(int p2)
    {
        this.setTitle(this.getContext().getText(p2));
        return;
    }

    public void setTitle(CharSequence p4)
    {
        if (android.text.TextUtils.isEmpty(p4)) {
            if ((this.b != null) && (this.e(this.b))) {
                this.removeView(this.b);
                this.j.remove(this.b);
            }
        } else {
            if (this.b == null) {
                android.widget.TextView v0_7 = this.getContext();
                this.b = new android.widget.TextView(v0_7);
                this.b.setSingleLine();
                this.b.setEllipsize(android.text.TextUtils$TruncateAt.END);
                if (this.g != 0) {
                    this.b.setTextAppearance(v0_7, this.g);
                }
                if (this.D != 0) {
                    this.b.setTextColor(this.D);
                }
            }
            if (!this.e(this.b)) {
                this.a(this.b, 1);
            }
        }
        if (this.b != null) {
            this.b.setText(p4);
        }
        this.B = p4;
        return;
    }

    public void setTitleTextColor(int p2)
    {
        this.D = p2;
        if (this.b != null) {
            this.b.setTextColor(p2);
        }
        return;
    }
}
