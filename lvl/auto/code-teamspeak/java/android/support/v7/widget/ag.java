package android.support.v7.widget;
final class ag implements android.widget.PopupWindow$OnDismissListener {
    final synthetic android.view.ViewTreeObserver$OnGlobalLayoutListener a;
    final synthetic android.support.v7.widget.ad b;

    ag(android.support.v7.widget.ad p1, android.view.ViewTreeObserver$OnGlobalLayoutListener p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onDismiss()
    {
        android.view.ViewTreeObserver v0_2 = this.b.b.getViewTreeObserver();
        if (v0_2 != null) {
            v0_2.removeGlobalOnLayoutListener(this.a);
        }
        return;
    }
}
