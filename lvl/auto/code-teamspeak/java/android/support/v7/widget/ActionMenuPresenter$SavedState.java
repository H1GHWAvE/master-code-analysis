package android.support.v7.widget;
 class ActionMenuPresenter$SavedState implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR;
    public int a;

    static ActionMenuPresenter$SavedState()
    {
        android.support.v7.widget.ActionMenuPresenter$SavedState.CREATOR = new android.support.v7.widget.i();
        return;
    }

    ActionMenuPresenter$SavedState()
    {
        return;
    }

    ActionMenuPresenter$SavedState(android.os.Parcel p2)
    {
        this.a = p2.readInt();
        return;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(android.os.Parcel p2, int p3)
    {
        p2.writeInt(this.a);
        return;
    }
}
