package android.support.v7.widget;
public final class SearchView extends android.support.v7.widget.aj implements android.support.v7.c.c {
    static final android.support.v7.widget.bq a = None;
    private static final boolean c = False;
    private static final String d = "SearchView";
    private static final boolean e = False;
    private static final String f = "nm";
    private final android.content.Intent A;
    private final CharSequence B;
    private android.support.v7.widget.bs C;
    private android.support.v7.widget.br D;
    private android.view.View$OnFocusChangeListener E;
    private android.support.v7.widget.bt F;
    private android.view.View$OnClickListener G;
    private boolean H;
    private boolean I;
    private android.support.v4.widget.r J;
    private boolean K;
    private CharSequence L;
    private boolean M;
    private boolean N;
    private int O;
    private boolean P;
    private CharSequence Q;
    private CharSequence R;
    private boolean S;
    private int T;
    private android.app.SearchableInfo U;
    private android.os.Bundle V;
    private final android.support.v7.internal.widget.av W;
    private Runnable aa;
    private final Runnable ab;
    private Runnable ac;
    private final java.util.WeakHashMap ad;
    private final android.view.View$OnClickListener ae;
    private final android.widget.TextView$OnEditorActionListener af;
    private final android.widget.AdapterView$OnItemClickListener ag;
    private final android.widget.AdapterView$OnItemSelectedListener ah;
    private android.text.TextWatcher ai;
    android.view.View$OnKeyListener b;
    private final android.support.v7.widget.SearchView$SearchAutoComplete g;
    private final android.view.View n;
    private final android.view.View o;
    private final android.view.View p;
    private final android.widget.ImageView q;
    private final android.widget.ImageView r;
    private final android.widget.ImageView s;
    private final android.widget.ImageView t;
    private final android.view.View u;
    private final android.widget.ImageView v;
    private final android.graphics.drawable.Drawable w;
    private final int x;
    private final int y;
    private final android.content.Intent z;

    static SearchView()
    {
        android.support.v7.widget.bq v0_1;
        if (android.os.Build$VERSION.SDK_INT < 8) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        android.support.v7.widget.SearchView.e = v0_1;
        android.support.v7.widget.SearchView.a = new android.support.v7.widget.bq();
        return;
    }

    private SearchView(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private SearchView(android.content.Context p2, byte p3)
    {
        this(p2, android.support.v7.a.d.searchViewStyle);
        return;
    }

    private SearchView(android.content.Context p8, int p9)
    {
        this(p8, 0, p9);
        this.aa = new android.support.v7.widget.be(this);
        this.ab = new android.support.v7.widget.bi(this);
        this.ac = new android.support.v7.widget.bj(this);
        this.ad = new java.util.WeakHashMap();
        this.ae = new android.support.v7.widget.bn(this);
        this.b = new android.support.v7.widget.bo(this);
        this.af = new android.support.v7.widget.bp(this);
        this.ag = new android.support.v7.widget.bf(this);
        this.ah = new android.support.v7.widget.bg(this);
        this.ai = new android.support.v7.widget.bh(this);
        android.support.v7.widget.bm v1_1 = android.support.v7.internal.widget.ax.a(p8, 0, android.support.v7.a.n.SearchView, p9);
        this.W = v1_1.a();
        android.view.LayoutInflater.from(p8).inflate(v1_1.e(android.support.v7.a.n.SearchView_layout, android.support.v7.a.k.abc_search_view), this, 1);
        this.g = ((android.support.v7.widget.SearchView$SearchAutoComplete) this.findViewById(android.support.v7.a.i.search_src_text));
        this.g.setSearchView(this);
        this.n = this.findViewById(android.support.v7.a.i.search_edit_frame);
        this.o = this.findViewById(android.support.v7.a.i.search_plate);
        this.p = this.findViewById(android.support.v7.a.i.submit_area);
        this.q = ((android.widget.ImageView) this.findViewById(android.support.v7.a.i.search_button));
        this.r = ((android.widget.ImageView) this.findViewById(android.support.v7.a.i.search_go_btn));
        this.s = ((android.widget.ImageView) this.findViewById(android.support.v7.a.i.search_close_btn));
        this.t = ((android.widget.ImageView) this.findViewById(android.support.v7.a.i.search_voice_btn));
        this.v = ((android.widget.ImageView) this.findViewById(android.support.v7.a.i.search_mag_icon));
        this.o.setBackgroundDrawable(v1_1.a(android.support.v7.a.n.SearchView_queryBackground));
        this.p.setBackgroundDrawable(v1_1.a(android.support.v7.a.n.SearchView_submitBackground));
        this.q.setImageDrawable(v1_1.a(android.support.v7.a.n.SearchView_searchIcon));
        this.r.setImageDrawable(v1_1.a(android.support.v7.a.n.SearchView_goIcon));
        this.s.setImageDrawable(v1_1.a(android.support.v7.a.n.SearchView_closeIcon));
        this.t.setImageDrawable(v1_1.a(android.support.v7.a.n.SearchView_voiceIcon));
        this.v.setImageDrawable(v1_1.a(android.support.v7.a.n.SearchView_searchIcon));
        this.w = v1_1.a(android.support.v7.a.n.SearchView_searchHintIcon);
        this.x = v1_1.e(android.support.v7.a.n.SearchView_suggestionRowLayout, android.support.v7.a.k.abc_search_dropdown_item_icons_2line);
        this.y = v1_1.e(android.support.v7.a.n.SearchView_commitIcon, 0);
        this.q.setOnClickListener(this.ae);
        this.s.setOnClickListener(this.ae);
        this.r.setOnClickListener(this.ae);
        this.t.setOnClickListener(this.ae);
        this.g.setOnClickListener(this.ae);
        this.g.addTextChangedListener(this.ai);
        this.g.setOnEditorActionListener(this.af);
        this.g.setOnItemClickListener(this.ag);
        this.g.setOnItemSelectedListener(this.ah);
        this.g.setOnKeyListener(this.b);
        this.g.setOnFocusChangeListener(new android.support.v7.widget.bk(this));
        this.setIconifiedByDefault(v1_1.a(android.support.v7.a.n.SearchView_iconifiedByDefault, 1));
        android.view.ViewTreeObserver v0_75 = v1_1.c(android.support.v7.a.n.SearchView_android_maxWidth, -1);
        if (v0_75 != -1) {
            this.setMaxWidth(v0_75);
        }
        this.B = v1_1.c(android.support.v7.a.n.SearchView_defaultQueryHint);
        this.L = v1_1.c(android.support.v7.a.n.SearchView_queryHint);
        android.view.ViewTreeObserver v0_81 = v1_1.a(android.support.v7.a.n.SearchView_android_imeOptions, -1);
        if (v0_81 != -1) {
            this.setImeOptions(v0_81);
        }
        android.view.ViewTreeObserver v0_83 = v1_1.a(android.support.v7.a.n.SearchView_android_inputType, -1);
        if (v0_83 != -1) {
            this.setInputType(v0_83);
        }
        this.setFocusable(v1_1.a(android.support.v7.a.n.SearchView_android_focusable, 1));
        v1_1.a.recycle();
        this.z = new android.content.Intent("android.speech.action.WEB_SEARCH");
        this.z.addFlags(268435456);
        this.z.putExtra("android.speech.extra.LANGUAGE_MODEL", "web_search");
        this.A = new android.content.Intent("android.speech.action.RECOGNIZE_SPEECH");
        this.A.addFlags(268435456);
        this.u = this.findViewById(this.g.getDropDownAnchor());
        if (this.u != null) {
            if (android.os.Build$VERSION.SDK_INT < 11) {
                this.u.getViewTreeObserver().addOnGlobalLayoutListener(new android.support.v7.widget.bm(this));
            } else {
                this.u.addOnLayoutChangeListener(new android.support.v7.widget.bl(this));
            }
        }
        this.a(this.H);
        this.q();
        return;
    }

    private static android.content.Intent a(android.content.Intent p3, android.app.SearchableInfo p4)
    {
        String v0_1;
        android.content.Intent v1_1 = new android.content.Intent(p3);
        String v0_0 = p4.getSearchActivity();
        if (v0_0 != null) {
            v0_1 = v0_0.flattenToShortString();
        } else {
            v0_1 = 0;
        }
        v1_1.putExtra("calling_package", v0_1);
        return v1_1;
    }

    private android.content.Intent a(android.database.Cursor p7)
    {
        try {
            android.content.Intent v0_1 = android.support.v7.widget.bz.a(p7, "suggest_intent_action");
        } catch (android.content.Intent v0_12) {
            String v2_2 = v0_12;
            try {
                android.content.Intent v0_13 = p7.getPosition();
            } catch (android.content.Intent v0) {
                v0_13 = -1;
            }
            android.util.Log.w("SearchView", new StringBuilder("Search suggestions cursor at row ").append(v0_13).append(" returned exception.").toString(), v2_2);
            android.content.Intent v0_11 = 0;
            return v0_11;
        }
        if ((v0_1 == null) && (android.os.Build$VERSION.SDK_INT >= 8)) {
            v0_1 = this.U.getSuggestIntentAction();
        }
        String v2_1;
        if (v0_1 != null) {
            v2_1 = v0_1;
        } else {
            v2_1 = "android.intent.action.SEARCH";
        }
        android.content.Intent v0_5 = android.support.v7.widget.bz.a(p7, "suggest_intent_data");
        if ((android.support.v7.widget.SearchView.e) && (v0_5 == null)) {
            v0_5 = this.U.getSuggestIntentData();
        }
        if (v0_5 != null) {
            String v3_3 = android.support.v7.widget.bz.a(p7, "suggest_intent_data_id");
            if (v3_3 != null) {
                v0_5 = new StringBuilder().append(v0_5).append("/").append(android.net.Uri.encode(v3_3)).toString();
            }
        }
        android.content.Intent v0_10;
        if (v0_5 != null) {
            v0_10 = android.net.Uri.parse(v0_5);
        } else {
            v0_10 = 0;
        }
        v0_11 = this.a(v2_1, v0_10, android.support.v7.widget.bz.a(p7, "suggest_intent_extra_data"), android.support.v7.widget.bz.a(p7, "suggest_intent_query"));
        return v0_11;
    }

    private android.content.Intent a(String p4, android.net.Uri p5, String p6, String p7)
    {
        android.content.Intent v0_1 = new android.content.Intent(p4);
        v0_1.addFlags(268435456);
        if (p5 != null) {
            v0_1.setData(p5);
        }
        v0_1.putExtra("user_query", this.R);
        if (p7 != null) {
            v0_1.putExtra("query", p7);
        }
        if (p6 != null) {
            v0_1.putExtra("intent_extra_data_key", p6);
        }
        if (this.V != null) {
            v0_1.putExtra("app_data", this.V);
        }
        if (android.support.v7.widget.SearchView.e) {
            v0_1.setComponent(this.U.getSearchActivity());
        }
        return v0_1;
    }

    private void a(android.content.Intent p5)
    {
        if (p5 != null) {
            try {
                this.getContext().startActivity(p5);
            } catch (RuntimeException v0_1) {
                android.util.Log.e("SearchView", new StringBuilder("Failed launch activity: ").append(p5).toString(), v0_1);
            }
        }
        return;
    }

    static synthetic void a(android.support.v7.widget.SearchView p2)
    {
        int[] v0_2;
        if (!p2.g.hasFocus()) {
            v0_2 = android.support.v7.widget.SearchView.EMPTY_STATE_SET;
        } else {
            v0_2 = android.support.v7.widget.SearchView.FOCUSED_STATE_SET;
        }
        android.graphics.drawable.Drawable v1_1 = p2.o.getBackground();
        if (v1_1 != null) {
            v1_1.setState(v0_2);
        }
        android.graphics.drawable.Drawable v1_3 = p2.p.getBackground();
        if (v1_3 != null) {
            v1_3.setState(v0_2);
        }
        p2.invalidate();
        return;
    }

    static synthetic void a(android.support.v7.widget.SearchView p3, CharSequence p4)
    {
        String v0_3;
        int v1 = 1;
        String v0_1 = p3.g.getText();
        p3.R = v0_1;
        if (android.text.TextUtils.isEmpty(v0_1)) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        void v3_1 = p3.b(v0_3);
        if (v0_3 != null) {
            v1 = 0;
        }
        void v3_4 = v3_1.c(v1).n().m();
        if ((v3_4.C != null) && (!android.text.TextUtils.equals(p4, v3_4.Q))) {
            p4.toString();
        }
        v3_4.Q = p4.toString();
        return;
    }

    static synthetic void a(android.support.v7.widget.SearchView p0, String p1)
    {
        p0.a(p1);
        return;
    }

    private void a(CharSequence p1)
    {
        this.setQuery(p1);
        return;
    }

    private void a(String p3)
    {
        this.getContext().startActivity(this.a("android.intent.action.SEARCH", 0, 0, p3));
        return;
    }

    private void a(boolean p7)
    {
        android.widget.ImageView v0_0;
        int v4 = 1;
        int v2 = 8;
        this.I = p7;
        if (!p7) {
            v0_0 = 8;
        } else {
            v0_0 = 0;
        }
        int v3_3;
        if (android.text.TextUtils.isEmpty(this.g.getText())) {
            v3_3 = 0;
        } else {
            v3_3 = 1;
        }
        android.widget.ImageView v0_1;
        this.q.setVisibility(v0_0);
        this.b(v3_3);
        if (!p7) {
            v0_1 = 0;
        } else {
            v0_1 = 8;
        }
        this.n.setVisibility(v0_1);
        if (!this.H) {
            v2 = 0;
        }
        this.v.setVisibility(v2);
        this.n();
        if (v3_3 != 0) {
            v4 = 0;
        }
        this.c(v4);
        this.m();
        return;
    }

    private boolean a(int p7)
    {
        int v0_0 = 0;
        if ((this.F == null) || (!this.F.b())) {
            RuntimeException v1_4 = this.J.c;
            if ((v1_4 != null) && (v1_4.moveToPosition(p7))) {
                String v2_1 = this.a(v1_4);
                if (v2_1 != null) {
                    try {
                        this.getContext().startActivity(v2_1);
                    } catch (RuntimeException v1_6) {
                        android.util.Log.e("SearchView", new StringBuilder("Failed launch activity: ").append(v2_1).toString(), v1_6);
                    }
                }
            }
            this.setImeVisibility(0);
            this.g.dismissDropDown();
            v0_0 = 1;
        }
        return v0_0;
    }

    private boolean a(int p4, android.view.KeyEvent p5)
    {
        int v1_0 = 0;
        if ((this.U != null) && ((this.J != null) && ((p5.getAction() == 0) && (android.support.v4.view.ab.b(p5))))) {
            if ((p4 != 66) && ((p4 != 84) && (p4 != 61))) {
                if ((p4 != 21) && (p4 != 22)) {
                    // Both branches of the condition point to the same code.
                    // if ((p4 == 19) && (this.g.getListSelection() != 0)) {
                    // }
                } else {
                    android.support.v7.widget.bq v0_12;
                    if (p4 != 21) {
                        v0_12 = this.g.length();
                    } else {
                        v0_12 = 0;
                    }
                    this.g.setSelection(v0_12);
                    this.g.setListSelection(0);
                    this.g.clearListSelection();
                    android.support.v7.widget.SearchView.a.a(this.g);
                    v1_0 = 1;
                }
            } else {
                v1_0 = this.a(this.g.getListSelection());
            }
        }
        return v1_0;
    }

    static boolean a(android.content.Context p2)
    {
        int v0_3;
        if (p2.getResources().getConfiguration().orientation != 2) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    static synthetic boolean a(android.support.v7.widget.SearchView p1, int p2)
    {
        return p1.a(p2);
    }

    static synthetic boolean a(android.support.v7.widget.SearchView p1, int p2, android.view.KeyEvent p3)
    {
        return p1.a(p2, p3);
    }

    private android.content.Intent b(android.content.Intent p12, android.app.SearchableInfo p13)
    {
        String v2 = 0;
        android.content.ComponentName v5 = p13.getSearchActivity();
        String v0_1 = new android.content.Intent("android.intent.action.SEARCH");
        v0_1.setComponent(v5);
        android.app.PendingIntent v6 = android.app.PendingIntent.getActivity(this.getContext(), 0, v0_1, 1073741824);
        android.os.Bundle v7_1 = new android.os.Bundle();
        if (this.V != null) {
            v7_1.putParcelable("app_data", this.V);
        }
        int v1_4;
        String v3_2;
        String v0_5;
        int v4_2;
        android.content.Intent v8_1 = new android.content.Intent(p12);
        String v0_4 = "free_form";
        if (android.os.Build$VERSION.SDK_INT < 8) {
            v3_2 = 0;
            v1_4 = 0;
            v4_2 = "free_form";
            v0_5 = 1;
        } else {
            String v3_3 = this.getResources();
            if (p13.getVoiceLanguageModeId() != 0) {
                v0_4 = v3_3.getString(p13.getVoiceLanguageModeId());
            }
            if (p13.getVoicePromptTextId() == 0) {
                v1_4 = 0;
            } else {
                v1_4 = v3_3.getString(p13.getVoicePromptTextId());
            }
            if (p13.getVoiceLanguageId() == 0) {
                v3_2 = 0;
            } else {
                v3_2 = v3_3.getString(p13.getVoiceLanguageId());
            }
            if (p13.getVoiceMaxResults() == 0) {
                v4_2 = v0_4;
                v0_5 = 1;
            } else {
                v4_2 = v0_4;
                v0_5 = p13.getVoiceMaxResults();
            }
        }
        v8_1.putExtra("android.speech.extra.LANGUAGE_MODEL", v4_2);
        v8_1.putExtra("android.speech.extra.PROMPT", v1_4);
        v8_1.putExtra("android.speech.extra.LANGUAGE", v3_2);
        v8_1.putExtra("android.speech.extra.MAX_RESULTS", v0_5);
        if (v5 != null) {
            v2 = v5.flattenToShortString();
        }
        v8_1.putExtra("calling_package", v2);
        v8_1.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", v6);
        v8_1.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", v7_1);
        return v8_1;
    }

    static synthetic android.support.v4.widget.r b(android.support.v7.widget.SearchView p1)
    {
        return p1.J;
    }

    private CharSequence b(CharSequence p6)
    {
        if ((this.H) && (this.w != null)) {
            this.w.setBounds(0, 0, ((int) (((double) this.g.getTextSize()) * 1.25)), ((int) (((double) this.g.getTextSize()) * 1.25)));
            android.text.SpannableStringBuilder v0_8 = new android.text.SpannableStringBuilder("   ");
            v0_8.setSpan(new android.text.style.ImageSpan(this.w), 1, 2, 33);
            v0_8.append(p6);
            p6 = v0_8;
        }
        return p6;
    }

    private void b(boolean p3)
    {
        int v0 = 8;
        if ((this.K) && ((this.l()) && ((this.hasFocus()) && ((p3) || (!this.P))))) {
            v0 = 0;
        }
        this.r.setVisibility(v0);
        return;
    }

    private boolean b(int p4)
    {
        if ((this.F != null) && (this.F.a())) {
            int v0_3 = 0;
        } else {
            int v0_5 = this.g.getText();
            CharSequence v1_1 = this.J.c;
            if (v1_1 != null) {
                if (!v1_1.moveToPosition(p4)) {
                    this.setQuery(v0_5);
                } else {
                    CharSequence v1_2 = this.J.c(v1_1);
                    if (v1_2 == null) {
                        this.setQuery(v0_5);
                    } else {
                        this.setQuery(v1_2);
                    }
                }
            }
            v0_3 = 1;
        }
        return v0_3;
    }

    static synthetic boolean b(android.support.v7.widget.SearchView p3, int p4)
    {
        if ((p3.F != null) && (p3.F.a())) {
            int v0_3 = 0;
        } else {
            int v0_5 = p3.g.getText();
            CharSequence v1_1 = p3.J.c;
            if (v1_1 != null) {
                if (!v1_1.moveToPosition(p4)) {
                    p3.setQuery(v0_5);
                } else {
                    CharSequence v1_2 = p3.J.c(v1_1);
                    if (v1_2 == null) {
                        p3.setQuery(v0_5);
                    } else {
                        p3.setQuery(v1_2);
                    }
                }
            }
            v0_3 = 1;
        }
        return v0_3;
    }

    static synthetic android.view.View$OnFocusChangeListener c(android.support.v7.widget.SearchView p1)
    {
        return p1.E;
    }

    private void c(int p4)
    {
        android.text.Editable v0_1 = this.g.getText();
        CharSequence v1_1 = this.J.c;
        if (v1_1 != null) {
            if (!v1_1.moveToPosition(p4)) {
                this.setQuery(v0_1);
            } else {
                CharSequence v1_2 = this.J.c(v1_1);
                if (v1_2 == null) {
                    this.setQuery(v0_1);
                } else {
                    this.setQuery(v1_2);
                }
            }
        }
        return;
    }

    private void c(CharSequence p4)
    {
        String v0_3;
        int v1 = 1;
        String v0_1 = this.g.getText();
        this.R = v0_1;
        if (android.text.TextUtils.isEmpty(v0_1)) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        this.b(v0_3);
        if (v0_3 != null) {
            v1 = 0;
        }
        this.c(v1);
        this.n();
        this.m();
        if ((this.C != null) && (!android.text.TextUtils.equals(p4, this.Q))) {
            p4.toString();
        }
        this.Q = p4.toString();
        return;
    }

    private void c(boolean p4)
    {
        if ((!this.P) || ((this.I) || (!p4))) {
            int v0_2 = 8;
        } else {
            v0_2 = 0;
            this.r.setVisibility(8);
        }
        this.t.setVisibility(v0_2);
        return;
    }

    static synthetic void d(android.support.v7.widget.SearchView p6)
    {
        if (p6.u.getWidth() > 1) {
            int v0_4;
            int v0_3 = p6.getContext().getResources();
            int v2 = p6.o.getPaddingLeft();
            int v3_1 = new android.graphics.Rect();
            android.support.v7.widget.SearchView$SearchAutoComplete v1_2 = android.support.v7.internal.widget.bd.a(p6);
            if (!p6.H) {
                v0_4 = 0;
            } else {
                v0_4 = (v0_3.getDimensionPixelSize(android.support.v7.a.g.abc_dropdownitem_text_padding_left) + v0_3.getDimensionPixelSize(android.support.v7.a.g.abc_dropdownitem_icon_width));
            }
            android.support.v7.widget.SearchView$SearchAutoComplete v1_5;
            p6.g.getDropDownBackground().getPadding(v3_1);
            if (v1_2 == null) {
                v1_5 = (v2 - (v3_1.left + v0_4));
            } else {
                v1_5 = (- v3_1.left);
            }
            p6.g.setDropDownHorizontalOffset(v1_5);
            p6.g.setDropDownWidth(((v0_4 + ((p6.u.getWidth() + v3_1.left) + v3_1.right)) - v2));
        }
        return;
    }

    private boolean d(int p6)
    {
        RuntimeException v0_2;
        RuntimeException v0_1 = this.J.c;
        if ((v0_1 == null) || (!v0_1.moveToPosition(p6))) {
            v0_2 = 0;
        } else {
            String v1_1 = this.a(v0_1);
            if (v1_1 != null) {
                try {
                    this.getContext().startActivity(v1_1);
                } catch (RuntimeException v0_4) {
                    android.util.Log.e("SearchView", new StringBuilder("Failed launch activity: ").append(v1_1).toString(), v0_4);
                }
            }
            v0_2 = 1;
        }
        return v0_2;
    }

    static synthetic android.widget.ImageView e(android.support.v7.widget.SearchView p1)
    {
        return p1.q;
    }

    private void e()
    {
        this.u.addOnLayoutChangeListener(new android.support.v7.widget.bl(this));
        return;
    }

    private void f()
    {
        this.u.getViewTreeObserver().addOnGlobalLayoutListener(new android.support.v7.widget.bm(this));
        return;
    }

    static synthetic void f(android.support.v7.widget.SearchView p0)
    {
        p0.v();
        return;
    }

    static synthetic android.widget.ImageView g(android.support.v7.widget.SearchView p1)
    {
        return p1.s;
    }

    private boolean g()
    {
        return this.H;
    }

    private int getPreferredWidth()
    {
        return this.getContext().getResources().getDimensionPixelSize(android.support.v7.a.g.abc_search_view_preferred_width);
    }

    static synthetic void h(android.support.v7.widget.SearchView p0)
    {
        p0.u();
        return;
    }

    private boolean h()
    {
        return this.I;
    }

    static synthetic android.widget.ImageView i(android.support.v7.widget.SearchView p1)
    {
        return p1.r;
    }

    private boolean i()
    {
        return this.K;
    }

    static synthetic void j(android.support.v7.widget.SearchView p2)
    {
        android.support.v7.widget.SearchView$SearchAutoComplete v0_1 = p2.g.getText();
        if ((v0_1 != null) && (android.text.TextUtils.getTrimmedLength(v0_1) > 0)) {
            if (p2.C != null) {
                android.app.SearchableInfo v1_2 = p2.C;
                v0_1.toString();
                if (v1_2.a()) {
                    return;
                }
            }
            if (p2.U != null) {
                p2 = p2.a(v0_1.toString());
            }
            p2.setImeVisibility(0).g.dismissDropDown();
        }
        return;
    }

    private boolean j()
    {
        return this.M;
    }

    static synthetic android.widget.ImageView k(android.support.v7.widget.SearchView p1)
    {
        return p1.t;
    }

    private boolean k()
    {
        int v0 = 0;
        if ((this.U != null) && (this.U.getVoiceSearchEnabled())) {
            android.content.pm.ResolveInfo v1_3 = 0;
            if (!this.U.getVoiceSearchLaunchWebSearch()) {
                if (this.U.getVoiceSearchLaunchRecognizer()) {
                    v1_3 = this.A;
                }
            } else {
                v1_3 = this.z;
            }
            if ((v1_3 != null) && (this.getContext().getPackageManager().resolveActivity(v1_3, 65536) != null)) {
                v0 = 1;
            }
        }
        return v0;
    }

    static synthetic void l(android.support.v7.widget.SearchView p11)
    {
        String v0_0 = 0;
        try {
            if (p11.U != null) {
                String v5_0 = p11.U;
                if (!v5_0.getVoiceSearchLaunchWebSearch()) {
                    if (v5_0.getVoiceSearchLaunchRecognizer()) {
                        int v1_3 = p11.A;
                        android.content.ComponentName v6 = v5_0.getSearchActivity();
                        String v2_1 = new android.content.Intent("android.intent.action.SEARCH");
                        v2_1.setComponent(v6);
                        android.app.PendingIntent v7_1 = android.app.PendingIntent.getActivity(p11.getContext(), 0, v2_1, 1073741824);
                        android.os.Bundle v8_1 = new android.os.Bundle();
                        if (p11.V != null) {
                            v8_1.putParcelable("app_data", p11.V);
                        }
                        String v4_2;
                        String v2_5;
                        String v3_4;
                        android.content.Intent v9_1 = new android.content.Intent(v1_3);
                        int v1_4 = 1;
                        if (android.os.Build$VERSION.SDK_INT < 8) {
                            v3_4 = 0;
                            v4_2 = "free_form";
                            v2_5 = 0;
                        } else {
                            int v10_0 = p11.getResources();
                            if (v5_0.getVoiceLanguageModeId() == 0) {
                                v4_2 = "free_form";
                            } else {
                                v4_2 = v10_0.getString(v5_0.getVoiceLanguageModeId());
                            }
                            if (v5_0.getVoicePromptTextId() == 0) {
                                v3_4 = 0;
                            } else {
                                v3_4 = v10_0.getString(v5_0.getVoicePromptTextId());
                            }
                            if (v5_0.getVoiceLanguageId() == 0) {
                                v2_5 = 0;
                            } else {
                                v2_5 = v10_0.getString(v5_0.getVoiceLanguageId());
                            }
                            if (v5_0.getVoiceMaxResults() != 0) {
                                v1_4 = v5_0.getVoiceMaxResults();
                            }
                        }
                        v9_1.putExtra("android.speech.extra.LANGUAGE_MODEL", v4_2);
                        v9_1.putExtra("android.speech.extra.PROMPT", v3_4);
                        v9_1.putExtra("android.speech.extra.LANGUAGE", v2_5);
                        v9_1.putExtra("android.speech.extra.MAX_RESULTS", v1_4);
                        if (v6 != null) {
                            v0_0 = v6.flattenToShortString();
                        }
                        v9_1.putExtra("calling_package", v0_0);
                        v9_1.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", v7_1);
                        v9_1.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", v8_1);
                        p11.getContext().startActivity(v9_1);
                    }
                } else {
                    String v2_13 = new android.content.Intent(p11.z);
                    int v1_7 = v5_0.getSearchActivity();
                    if (v1_7 != 0) {
                        v0_0 = v1_7.flattenToShortString();
                    }
                    v2_13.putExtra("calling_package", v0_0);
                    p11.getContext().startActivity(v2_13);
                }
            }
        } catch (String v0) {
            android.util.Log.w("SearchView", "Could not find voice search activity");
        }
        return;
    }

    private boolean l()
    {
        if (((!this.K) && (!this.P)) || (this.I)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    static synthetic android.support.v7.widget.SearchView$SearchAutoComplete m(android.support.v7.widget.SearchView p1)
    {
        return p1.g;
    }

    private void m()
    {
        int v0 = 8;
        if ((this.l()) && ((this.r.getVisibility() == 0) || (this.t.getVisibility() == 0))) {
            v0 = 0;
        }
        this.p.setVisibility(v0);
        return;
    }

    private void n()
    {
        int v2_3;
        int[] v0_0 = 1;
        int v1_0 = 0;
        if (android.text.TextUtils.isEmpty(this.g.getText())) {
            v2_3 = 0;
        } else {
            v2_3 = 1;
        }
        if ((v2_3 == 0) && ((!this.H) || (this.S))) {
            v0_0 = 0;
        }
        if (v0_0 == null) {
            v1_0 = 8;
        }
        this.s.setVisibility(v1_0);
        int v1_1 = this.s.getDrawable();
        if (v1_1 != 0) {
            int[] v0_2;
            if (v2_3 == 0) {
                v0_2 = android.support.v7.widget.SearchView.EMPTY_STATE_SET;
            } else {
                v0_2 = android.support.v7.widget.SearchView.ENABLED_STATE_SET;
            }
            v1_1.setState(v0_2);
        }
        return;
    }

    static synthetic void n(android.support.v7.widget.SearchView p0)
    {
        p0.y();
        return;
    }

    static synthetic android.app.SearchableInfo o(android.support.v7.widget.SearchView p1)
    {
        return p1.U;
    }

    private void o()
    {
        this.post(this.ab);
        return;
    }

    private void p()
    {
        int[] v0_2;
        if (!this.g.hasFocus()) {
            v0_2 = android.support.v7.widget.SearchView.EMPTY_STATE_SET;
        } else {
            v0_2 = android.support.v7.widget.SearchView.FOCUSED_STATE_SET;
        }
        android.graphics.drawable.Drawable v1_1 = this.o.getBackground();
        if (v1_1 != null) {
            v1_1.setState(v0_2);
        }
        android.graphics.drawable.Drawable v1_3 = this.p.getBackground();
        if (v1_3 != null) {
            v1_3.setState(v0_2);
        }
        this.invalidate();
        return;
    }

    static synthetic void p(android.support.v7.widget.SearchView p1)
    {
        p1.setImeVisibility(0);
        return;
    }

    private void q()
    {
        android.text.SpannableStringBuilder v0 = this.getQueryHint();
        android.support.v7.widget.SearchView$SearchAutoComplete v2 = this.g;
        if (v0 == null) {
            v0 = "";
        }
        if ((this.H) && (this.w != null)) {
            this.w.setBounds(0, 0, ((int) (((double) this.g.getTextSize()) * 1.25)), ((int) (((double) this.g.getTextSize()) * 1.25)));
            android.text.SpannableStringBuilder v1_6 = new android.text.SpannableStringBuilder("   ");
            v1_6.setSpan(new android.text.style.ImageSpan(this.w), 1, 2, 33);
            v1_6.append(v0);
            v0 = v1_6;
        }
        v2.setHint(v0);
        return;
    }

    private void r()
    {
        int v1 = 1;
        this.g.setThreshold(this.U.getSuggestThreshold());
        this.g.setImeOptions(this.U.getImeOptions());
        android.support.v7.widget.bz v0_3 = this.U.getInputType();
        if ((v0_3 & 15) == 1) {
            v0_3 &= -65537;
            if (this.U.getSuggestAuthority() != null) {
                v0_3 = ((v0_3 | 65536) | 524288);
            }
        }
        this.g.setInputType(v0_3);
        if (this.J != null) {
            this.J.a(0);
        }
        if (this.U.getSuggestAuthority() != null) {
            this.J = new android.support.v7.widget.bz(this.getContext(), this, this.U, this.ad);
            this.g.setAdapter(this.J);
            if (this.M) {
                v1 = 2;
            }
            ((android.support.v7.widget.bz) this.J).o = v1;
        }
        return;
    }

    private void s()
    {
        android.support.v7.widget.SearchView$SearchAutoComplete v0_1 = this.g.getText();
        if ((v0_1 != null) && (android.text.TextUtils.getTrimmedLength(v0_1) > 0)) {
            if (this.C != null) {
                android.app.SearchableInfo v1_2 = this.C;
                v0_1.toString();
                if (v1_2.a()) {
                    return;
                }
            }
            if (this.U != null) {
                this.a(v0_1.toString());
            }
            this.setImeVisibility(0);
            this.g.dismissDropDown();
        }
        return;
    }

    private void setImeVisibility(boolean p4)
    {
        if (!p4) {
            this.removeCallbacks(this.aa);
            android.view.inputmethod.InputMethodManager v0_3 = ((android.view.inputmethod.InputMethodManager) this.getContext().getSystemService("input_method"));
            if (v0_3 != null) {
                v0_3.hideSoftInputFromWindow(this.getWindowToken(), 0);
            }
        } else {
            this.post(this.aa);
        }
        return;
    }

    private void setQuery$609c24db(CharSequence p3)
    {
        this.g.setText(p3);
        this.g.setSelection(this.g.length());
        this.R = p3;
        return;
    }

    private void t()
    {
        this.g.dismissDropDown();
        return;
    }

    private void u()
    {
        if (!android.text.TextUtils.isEmpty(this.g.getText())) {
            this.g.setText("");
            this.g.requestFocus();
            this.setImeVisibility(1);
        } else {
            if ((this.H) && ((this.D == null) || (!this.D.a()))) {
                this.clearFocus();
                this.a(1);
            }
        }
        return;
    }

    private void v()
    {
        this.a(0);
        this.g.requestFocus();
        this.setImeVisibility(1);
        if (this.G != null) {
            this.G.onClick(this);
        }
        return;
    }

    private void w()
    {
        String v0_0 = 0;
        if (this.U != null) {
            String v5_0 = this.U;
            try {
                if (!v5_0.getVoiceSearchLaunchWebSearch()) {
                    if (v5_0.getVoiceSearchLaunchRecognizer()) {
                        int v1_3 = this.A;
                        android.content.ComponentName v6 = v5_0.getSearchActivity();
                        String v2_1 = new android.content.Intent("android.intent.action.SEARCH");
                        v2_1.setComponent(v6);
                        android.app.PendingIntent v7_1 = android.app.PendingIntent.getActivity(this.getContext(), 0, v2_1, 1073741824);
                        android.os.Bundle v8_1 = new android.os.Bundle();
                        if (this.V != null) {
                            v8_1.putParcelable("app_data", this.V);
                        }
                        String v4_2;
                        String v2_5;
                        String v3_4;
                        android.content.Intent v9_1 = new android.content.Intent(v1_3);
                        int v1_4 = 1;
                        if (android.os.Build$VERSION.SDK_INT < 8) {
                            v3_4 = 0;
                            v4_2 = "free_form";
                            v2_5 = 0;
                        } else {
                            int v10_0 = this.getResources();
                            if (v5_0.getVoiceLanguageModeId() == 0) {
                                v4_2 = "free_form";
                            } else {
                                v4_2 = v10_0.getString(v5_0.getVoiceLanguageModeId());
                            }
                            if (v5_0.getVoicePromptTextId() == 0) {
                                v3_4 = 0;
                            } else {
                                v3_4 = v10_0.getString(v5_0.getVoicePromptTextId());
                            }
                            if (v5_0.getVoiceLanguageId() == 0) {
                                v2_5 = 0;
                            } else {
                                v2_5 = v10_0.getString(v5_0.getVoiceLanguageId());
                            }
                            if (v5_0.getVoiceMaxResults() != 0) {
                                v1_4 = v5_0.getVoiceMaxResults();
                            }
                        }
                        v9_1.putExtra("android.speech.extra.LANGUAGE_MODEL", v4_2);
                        v9_1.putExtra("android.speech.extra.PROMPT", v3_4);
                        v9_1.putExtra("android.speech.extra.LANGUAGE", v2_5);
                        v9_1.putExtra("android.speech.extra.MAX_RESULTS", v1_4);
                        if (v6 != null) {
                            v0_0 = v6.flattenToShortString();
                        }
                        v9_1.putExtra("calling_package", v0_0);
                        v9_1.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", v7_1);
                        v9_1.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", v8_1);
                        this.getContext().startActivity(v9_1);
                    }
                } else {
                    String v2_13 = new android.content.Intent(this.z);
                    int v1_7 = v5_0.getSearchActivity();
                    if (v1_7 != 0) {
                        v0_0 = v1_7.flattenToShortString();
                    }
                    v2_13.putExtra("calling_package", v0_0);
                    this.getContext().startActivity(v2_13);
                }
            } catch (String v0) {
                android.util.Log.w("SearchView", "Could not find voice search activity");
            }
        }
        return;
    }

    private void x()
    {
        if (this.u.getWidth() > 1) {
            int v0_4;
            int v0_3 = this.getContext().getResources();
            int v2 = this.o.getPaddingLeft();
            int v3_1 = new android.graphics.Rect();
            android.support.v7.widget.SearchView$SearchAutoComplete v1_2 = android.support.v7.internal.widget.bd.a(this);
            if (!this.H) {
                v0_4 = 0;
            } else {
                v0_4 = (v0_3.getDimensionPixelSize(android.support.v7.a.g.abc_dropdownitem_text_padding_left) + v0_3.getDimensionPixelSize(android.support.v7.a.g.abc_dropdownitem_icon_width));
            }
            android.support.v7.widget.SearchView$SearchAutoComplete v1_5;
            this.g.getDropDownBackground().getPadding(v3_1);
            if (v1_2 == null) {
                v1_5 = (v2 - (v3_1.left + v0_4));
            } else {
                v1_5 = (- v3_1.left);
            }
            this.g.setDropDownHorizontalOffset(v1_5);
            this.g.setDropDownWidth(((v0_4 + ((this.u.getWidth() + v3_1.left) + v3_1.right)) - v2));
        }
        return;
    }

    private void y()
    {
        Exception v0_0 = android.support.v7.widget.SearchView.a;
        if (v0_0.a != null) {
            try {
                Object[] v2_2 = new Object[0];
                v0_0.a.invoke(this.g, v2_2);
            } catch (Exception v0) {
            }
        }
        Exception v0_2 = android.support.v7.widget.SearchView.a;
        if (v0_2.b != null) {
            try {
                Object[] v2_5 = new Object[0];
                v0_2.b.invoke(this.g, v2_5);
            } catch (Exception v0) {
            }
        }
        return;
    }

    public final void a()
    {
        if (!this.S) {
            this.S = 1;
            this.T = this.g.getImeOptions();
            this.g.setImeOptions((this.T | 33554432));
            this.g.setText("");
            this.setIconified(0);
        }
        return;
    }

    public final void b()
    {
        this.g.setText("");
        this.g.setSelection(this.g.length());
        this.R = "";
        this.clearFocus();
        this.a(1);
        this.g.setImeOptions(this.T);
        this.S = 0;
        return;
    }

    public final void clearFocus()
    {
        this.N = 1;
        this.setImeVisibility(0);
        super.clearFocus();
        this.g.clearFocus();
        this.N = 0;
        return;
    }

    final void d()
    {
        this.a(this.I);
        this.o();
        if (this.g.hasFocus()) {
            this.y();
        }
        return;
    }

    public final int getImeOptions()
    {
        return this.g.getImeOptions();
    }

    public final int getInputType()
    {
        return this.g.getInputType();
    }

    public final int getMaxWidth()
    {
        return this.O;
    }

    public final CharSequence getQuery()
    {
        return this.g.getText();
    }

    public final CharSequence getQueryHint()
    {
        CharSequence v0_5;
        if (this.L == null) {
            if ((!android.support.v7.widget.SearchView.e) || ((this.U == null) || (this.U.getHintId() == 0))) {
                v0_5 = this.B;
            } else {
                v0_5 = this.getContext().getText(this.U.getHintId());
            }
        } else {
            v0_5 = this.L;
        }
        return v0_5;
    }

    final int getSuggestionCommitIconResId()
    {
        return this.y;
    }

    final int getSuggestionRowLayout()
    {
        return this.x;
    }

    public final android.support.v4.widget.r getSuggestionsAdapter()
    {
        return this.J;
    }

    protected final void onDetachedFromWindow()
    {
        this.removeCallbacks(this.ab);
        this.post(this.ac);
        super.onDetachedFromWindow();
        return;
    }

    protected final void onMeasure(int p3, int p4)
    {
        if (!this.I) {
            int v1_0 = android.view.View$MeasureSpec.getMode(p3);
            int v0_1 = android.view.View$MeasureSpec.getSize(p3);
            switch (v1_0) {
                case -2147483648:
                    if (this.O <= 0) {
                        v0_1 = Math.min(this.getPreferredWidth(), v0_1);
                    } else {
                        v0_1 = Math.min(this.O, v0_1);
                    }
                    break;
                case 0:
                    if (this.O <= 0) {
                        v0_1 = this.getPreferredWidth();
                    } else {
                        v0_1 = this.O;
                    }
                    break;
                case 1073741824:
                    if (this.O <= 0) {
                    } else {
                        v0_1 = Math.min(this.O, v0_1);
                    }
                    break;
            }
            super.onMeasure(android.view.View$MeasureSpec.makeMeasureSpec(v0_1, 1073741824), p4);
        } else {
            super.onMeasure(p3, p4);
        }
        return;
    }

    public final void onWindowFocusChanged(boolean p1)
    {
        super.onWindowFocusChanged(p1);
        this.o();
        return;
    }

    public final boolean requestFocus(int p3, android.graphics.Rect p4)
    {
        boolean v0 = 0;
        if ((!this.N) && (this.isFocusable())) {
            if (this.I) {
                v0 = super.requestFocus(p3, p4);
            } else {
                boolean v1_4 = this.g.requestFocus(p3, p4);
                if (v1_4) {
                    this.a(0);
                }
                v0 = v1_4;
            }
        }
        return v0;
    }

    public final void setAppSearchData(android.os.Bundle p1)
    {
        this.V = p1;
        return;
    }

    public final void setIconified(boolean p1)
    {
        if (!p1) {
            this.v();
        } else {
            this.u();
        }
        return;
    }

    public final void setIconifiedByDefault(boolean p2)
    {
        if (this.H != p2) {
            this.H = p2;
            this.a(p2);
            this.q();
        }
        return;
    }

    public final void setImeOptions(int p2)
    {
        this.g.setImeOptions(p2);
        return;
    }

    public final void setInputType(int p2)
    {
        this.g.setInputType(p2);
        return;
    }

    public final void setMaxWidth(int p1)
    {
        this.O = p1;
        this.requestLayout();
        return;
    }

    public final void setOnCloseListener(android.support.v7.widget.br p1)
    {
        this.D = p1;
        return;
    }

    public final void setOnQueryTextFocusChangeListener(android.view.View$OnFocusChangeListener p1)
    {
        this.E = p1;
        return;
    }

    public final void setOnQueryTextListener(android.support.v7.widget.bs p1)
    {
        this.C = p1;
        return;
    }

    public final void setOnSearchClickListener(android.view.View$OnClickListener p1)
    {
        this.G = p1;
        return;
    }

    public final void setOnSuggestionListener(android.support.v7.widget.bt p1)
    {
        this.F = p1;
        return;
    }

    final void setQuery(CharSequence p3)
    {
        int v0_2;
        this.g.setText(p3);
        android.support.v7.widget.SearchView$SearchAutoComplete v1 = this.g;
        if (!android.text.TextUtils.isEmpty(p3)) {
            v0_2 = p3.length();
        } else {
            v0_2 = 0;
        }
        v1.setSelection(v0_2);
        return;
    }

    public final void setQueryHint(CharSequence p1)
    {
        this.L = p1;
        this.q();
        return;
    }

    public final void setQueryRefinementEnabled(boolean p3)
    {
        this.M = p3;
        if ((this.J instanceof android.support.v7.widget.bz)) {
            int v1;
            if (!p3) {
                v1 = 1;
            } else {
                v1 = 2;
            }
            ((android.support.v7.widget.bz) this.J).o = v1;
        }
        return;
    }

    public final void setSearchableInfo(android.app.SearchableInfo p9)
    {
        int v2 = 1;
        this.U = p9;
        if (this.U != null) {
            if (android.support.v7.widget.SearchView.e) {
                this.g.setThreshold(this.U.getSuggestThreshold());
                this.g.setImeOptions(this.U.getImeOptions());
                int v0_5 = this.U.getInputType();
                if ((v0_5 & 15) == 1) {
                    v0_5 &= -65537;
                    if (this.U.getSuggestAuthority() != null) {
                        v0_5 = ((v0_5 | 65536) | 524288);
                    }
                }
                this.g.setInputType(v0_5);
                if (this.J != null) {
                    this.J.a(0);
                }
                if (this.U.getSuggestAuthority() != null) {
                    android.content.pm.PackageManager v1_13;
                    this.J = new android.support.v7.widget.bz(this.getContext(), this, this.U, this.ad);
                    this.g.setAdapter(this.J);
                    if (!this.M) {
                        v1_13 = 1;
                    } else {
                        v1_13 = 2;
                    }
                    ((android.support.v7.widget.bz) this.J).o = v1_13;
                }
            }
            this.q();
        }
        if (!android.support.v7.widget.SearchView.e) {
            v2 = 0;
        } else {
            if ((this.U == null) || (!this.U.getVoiceSearchEnabled())) {
                int v0_25 = 0;
            } else {
                int v0_24;
                if (!this.U.getVoiceSearchLaunchWebSearch()) {
                    if (!this.U.getVoiceSearchLaunchRecognizer()) {
                        v0_24 = 0;
                    } else {
                        v0_24 = this.A;
                    }
                } else {
                    v0_24 = this.z;
                }
                if (v0_24 == 0) {
                } else {
                    if (this.getContext().getPackageManager().resolveActivity(v0_24, 65536) == null) {
                        v0_25 = 0;
                    } else {
                        v0_25 = 1;
                    }
                }
            }
            if (v0_25 == 0) {
            }
        }
        this.P = v2;
        if (this.P) {
            this.g.setPrivateImeOptions("nm");
        }
        this.a(this.I);
        return;
    }

    public final void setSubmitButtonEnabled(boolean p2)
    {
        this.K = p2;
        this.a(this.I);
        return;
    }

    public final void setSuggestionsAdapter(android.support.v4.widget.r p3)
    {
        this.J = p3;
        this.g.setAdapter(this.J);
        return;
    }
}
