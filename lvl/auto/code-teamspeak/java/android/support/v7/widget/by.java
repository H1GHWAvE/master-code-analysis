package android.support.v7.widget;
final class by implements android.view.MenuItem$OnMenuItemClickListener {
    final synthetic android.support.v7.widget.bu a;

    private by(android.support.v7.widget.bu p1)
    {
        this.a = p1;
        return;
    }

    synthetic by(android.support.v7.widget.bu p1, byte p2)
    {
        this(p1);
        return;
    }

    public final boolean onMenuItemClick(android.view.MenuItem p4)
    {
        int v0_3 = android.support.v7.internal.widget.l.a(this.a.d, this.a.e).b(p4.getItemId());
        if (v0_3 != 0) {
            android.content.Context v1_3 = v0_3.getAction();
            if (("android.intent.action.SEND".equals(v1_3)) || ("android.intent.action.SEND_MULTIPLE".equals(v1_3))) {
                android.support.v7.widget.bu.a(v0_3);
            }
            this.a.d.startActivity(v0_3);
        }
        return 1;
    }
}
