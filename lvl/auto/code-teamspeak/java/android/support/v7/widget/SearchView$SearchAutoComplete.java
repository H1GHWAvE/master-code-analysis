package android.support.v7.widget;
public class SearchView$SearchAutoComplete extends android.support.v7.widget.p {
    private int a;
    private android.support.v7.widget.SearchView b;

    public SearchView$SearchAutoComplete(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public SearchView$SearchAutoComplete(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, android.support.v7.a.d.autoCompleteTextViewStyle);
        return;
    }

    public SearchView$SearchAutoComplete(android.content.Context p2, android.util.AttributeSet p3, int p4)
    {
        this(p2, p3, p4);
        this.a = this.getThreshold();
        return;
    }

    private boolean a()
    {
        int v0_2;
        if (android.text.TextUtils.getTrimmedLength(this.getText()) != 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    static synthetic boolean a(android.support.v7.widget.SearchView$SearchAutoComplete p1)
    {
        int v0_2;
        if (android.text.TextUtils.getTrimmedLength(p1.getText()) != 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public boolean enoughToFilter()
    {
        if ((this.a > 0) && (!super.enoughToFilter())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    protected void onFocusChanged(boolean p2, int p3, android.graphics.Rect p4)
    {
        super.onFocusChanged(p2, p3, p4);
        this.b.d();
        return;
    }

    public boolean onKeyPreIme(int p3, android.view.KeyEvent p4)
    {
        boolean v0 = 1;
        if (p3 != 4) {
            v0 = super.onKeyPreIme(p3, p4);
        } else {
            if ((p4.getAction() != 0) || (p4.getRepeatCount() != 0)) {
                if (p4.getAction() != 1) {
                } else {
                    android.support.v7.widget.SearchView v1_4 = this.getKeyDispatcherState();
                    if (v1_4 != null) {
                        v1_4.handleUpEvent(p4);
                    }
                    if ((!p4.isTracking()) || (p4.isCanceled())) {
                    } else {
                        this.b.clearFocus();
                        android.support.v7.widget.SearchView.p(this.b);
                    }
                }
            } else {
                android.support.v7.widget.SearchView v1_9 = this.getKeyDispatcherState();
                if (v1_9 != null) {
                    v1_9.startTracking(p4, this);
                }
            }
        }
        return v0;
    }

    public void onWindowFocusChanged(boolean p3)
    {
        super.onWindowFocusChanged(p3);
        if ((p3) && ((this.b.hasFocus()) && (this.getVisibility() == 0))) {
            ((android.view.inputmethod.InputMethodManager) this.getContext().getSystemService("input_method")).showSoftInput(this, 0);
            if (android.support.v7.widget.SearchView.a(this.getContext())) {
                android.support.v7.widget.SearchView.a.a(this);
            }
        }
        return;
    }

    public void performCompletion()
    {
        return;
    }

    protected void replaceText(CharSequence p1)
    {
        return;
    }

    void setSearchView(android.support.v7.widget.SearchView p1)
    {
        this.b = p1;
        return;
    }

    public void setThreshold(int p1)
    {
        super.setThreshold(p1);
        this.a = p1;
        return;
    }
}
