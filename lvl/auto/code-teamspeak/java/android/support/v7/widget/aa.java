package android.support.v7.widget;
public final class aa extends android.widget.Spinner implements android.support.v4.view.cr {
    private static final boolean a = False;
    private static final boolean b = False;
    private static final int[] c = None;
    private static final int d = 15;
    private static final String e = "AppCompatSpinner";
    private static final int f = 0;
    private static final int g = 1;
    private static final int h = 255;
    private android.support.v7.internal.widget.av i;
    private android.support.v7.widget.q j;
    private android.content.Context k;
    private android.support.v7.widget.as l;
    private android.widget.SpinnerAdapter m;
    private boolean n;
    private android.support.v7.widget.ad o;
    private int p;
    private final android.graphics.Rect q;

    static aa()
    {
        int[] v0_1;
        if (android.os.Build$VERSION.SDK_INT < 23) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int[] v0_3;
        android.support.v7.widget.aa.a = v0_1;
        if (android.os.Build$VERSION.SDK_INT < 16) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        android.support.v7.widget.aa.b = v0_3;
        int[] v0_4 = new int[1];
        v0_4[0] = 16843505;
        android.support.v7.widget.aa.c = v0_4;
        return;
    }

    private aa(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private aa(android.content.Context p3, int p4)
    {
        this(p3, 0, android.support.v7.a.d.spinnerStyle, p4);
        return;
    }

    public aa(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, android.support.v7.a.d.spinnerStyle);
        return;
    }

    public aa(android.content.Context p2, android.util.AttributeSet p3, int p4)
    {
        this(p2, p3, p4, -1);
        return;
    }

    private aa(android.content.Context p7, android.util.AttributeSet p8, int p9, int p10)
    {
        this(p7, p8, p9, p10, 0);
        return;
    }

    private aa(android.content.Context p8, android.util.AttributeSet p9, int p10, int p11, byte p12)
    {
        int v0_8;
        int v2_2;
        this(p8, p9, p10);
        this.q = new android.graphics.Rect();
        android.support.v7.internal.widget.ax v4 = android.support.v7.internal.widget.ax.a(p8, p9, android.support.v7.a.n.Spinner, p10);
        this.i = v4.a();
        this.j = new android.support.v7.widget.q(this, this.i);
        int v2_1 = v4.e(android.support.v7.a.n.Spinner_popupTheme, 0);
        if (v2_1 == 0) {
            if (android.support.v7.widget.aa.a) {
                v0_8 = 0;
                v2_2 = this;
            } else {
                v0_8 = p8;
                v2_2 = this;
            }
        } else {
            v0_8 = new android.support.v7.internal.view.b(p8, v2_1);
            v2_2 = this;
        }
        v2_2.k = v0_8;
        if (this.k != null) {
            if (p11 == -1) {
                if (android.os.Build$VERSION.SDK_INT < 11) {
                    p11 = 1;
                } else {
                    try {
                        int v2_5 = p8.obtainStyledAttributes(p9, android.support.v7.widget.aa.c, p10, 0);
                        try {
                            if (v2_5.hasValue(0)) {
                                p11 = v2_5.getInt(0, 0);
                            }
                        } catch (int v0_15) {
                            android.util.Log.i("AppCompatSpinner", "Could not read android:spinnerMode", v0_15);
                            if (v2_5 == 0) {
                                if (p11 == 1) {
                                    int v0_20 = new android.support.v7.widget.ad(this, this.k, p9, p10);
                                    int v2_8 = android.support.v7.internal.widget.ax.a(this.k, p9, android.support.v7.a.n.Spinner, p10);
                                    this.p = v2_8.d(android.support.v7.a.n.Spinner_android_dropDownWidth, -2);
                                    v0_20.a(v2_8.a(android.support.v7.a.n.Spinner_android_popupBackground));
                                    v0_20.a = v4.a.getString(android.support.v7.a.n.Spinner_android_prompt);
                                    v2_8.a.recycle();
                                    this.o = v0_20;
                                    this.l = new android.support.v7.widget.ab(this, this, v0_20);
                                }
                                v4.a.recycle();
                                this.n = 1;
                                if (this.m != null) {
                                    this.setAdapter(this.m);
                                    this.m = 0;
                                }
                                this.j.a(p9, p10);
                                return;
                            } else {
                                v2_5.recycle();
                            }
                        }
                        if (v2_5 != 0) {
                            v2_5.recycle();
                        }
                    } catch (int v0_16) {
                        v2_5 = 0;
                        if (v2_5 != 0) {
                            v2_5.recycle();
                        }
                        throw v0_16;
                    } catch (int v0_15) {
                        v2_5 = 0;
                    } catch (int v0_16) {
                    }
                }
            }
        }
    }

    static synthetic int a(android.support.v7.widget.aa p1, android.widget.SpinnerAdapter p2, android.graphics.drawable.Drawable p3)
    {
        return p1.a(p2, p3);
    }

    private int a(android.widget.SpinnerAdapter p11, android.graphics.drawable.Drawable p12)
    {
        int v0_0 = 0;
        if (p11 != null) {
            int v6 = android.view.View$MeasureSpec.makeMeasureSpec(this.getMeasuredWidth(), 0);
            int v7 = android.view.View$MeasureSpec.makeMeasureSpec(this.getMeasuredHeight(), 0);
            int v1_3 = Math.max(0, this.getSelectedItemPosition());
            int v8 = Math.min(p11.getCount(), (v1_3 + 15));
            int v5 = Math.max(0, (v1_3 - (15 - (v8 - v1_3))));
            android.view.View v3_3 = 0;
            int v4_1 = 0;
            int v1_6 = 0;
            while (v5 < v8) {
                int v1_9;
                int v0_5 = p11.getItemViewType(v5);
                if (v0_5 == v1_6) {
                    v0_5 = v1_6;
                    v1_9 = v3_3;
                } else {
                    v1_9 = 0;
                }
                v3_3 = p11.getView(v5, v1_9, this);
                if (v3_3.getLayoutParams() == null) {
                    v3_3.setLayoutParams(new android.view.ViewGroup$LayoutParams(-2, -2));
                }
                v3_3.measure(v6, v7);
                v4_1 = Math.max(v4_1, v3_3.getMeasuredWidth());
                v5++;
                v1_6 = v0_5;
            }
            if (p12 == null) {
                v0_0 = v4_1;
            } else {
                p12.getPadding(this.q);
                v0_0 = ((this.q.left + this.q.right) + v4_1);
            }
        }
        return v0_0;
    }

    static synthetic android.support.v7.widget.ad a(android.support.v7.widget.aa p1)
    {
        return p1.o;
    }

    static synthetic boolean a()
    {
        return android.support.v7.widget.aa.a;
    }

    static synthetic android.graphics.Rect b(android.support.v7.widget.aa p1)
    {
        return p1.q;
    }

    static synthetic int c(android.support.v7.widget.aa p1)
    {
        return p1.p;
    }

    protected final void drawableStateChanged()
    {
        super.drawableStateChanged();
        if (this.j != null) {
            this.j.c();
        }
        return;
    }

    public final int getDropDownHorizontalOffset()
    {
        int v0_2;
        if (this.o == null) {
            if (!android.support.v7.widget.aa.b) {
                v0_2 = 0;
            } else {
                v0_2 = super.getDropDownHorizontalOffset();
            }
        } else {
            v0_2 = this.o.f;
        }
        return v0_2;
    }

    public final int getDropDownVerticalOffset()
    {
        int v0 = 0;
        if (this.o == null) {
            if (android.support.v7.widget.aa.b) {
                v0 = super.getDropDownVerticalOffset();
            }
        } else {
            boolean v1_2 = this.o;
            if (v1_2.h) {
                v0 = v1_2.g;
            }
        }
        return v0;
    }

    public final int getDropDownWidth()
    {
        int v0_2;
        if (this.o == null) {
            if (!android.support.v7.widget.aa.b) {
                v0_2 = 0;
            } else {
                v0_2 = super.getDropDownWidth();
            }
        } else {
            v0_2 = this.p;
        }
        return v0_2;
    }

    public final android.graphics.drawable.Drawable getPopupBackground()
    {
        int v0_2;
        if (this.o == null) {
            if (!android.support.v7.widget.aa.b) {
                v0_2 = 0;
            } else {
                v0_2 = super.getPopupBackground();
            }
        } else {
            v0_2 = this.o.c.getBackground();
        }
        return v0_2;
    }

    public final android.content.Context getPopupContext()
    {
        int v0_2;
        if (this.o == null) {
            if (!android.support.v7.widget.aa.a) {
                v0_2 = 0;
            } else {
                v0_2 = super.getPopupContext();
            }
        } else {
            v0_2 = this.k;
        }
        return v0_2;
    }

    public final CharSequence getPrompt()
    {
        CharSequence v0_1;
        if (this.o == null) {
            v0_1 = super.getPrompt();
        } else {
            v0_1 = this.o.a;
        }
        return v0_1;
    }

    public final android.content.res.ColorStateList getSupportBackgroundTintList()
    {
        int v0_1;
        if (this.j == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.j.a();
        }
        return v0_1;
    }

    public final android.graphics.PorterDuff$Mode getSupportBackgroundTintMode()
    {
        int v0_1;
        if (this.j == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.j.b();
        }
        return v0_1;
    }

    protected final void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if ((this.o != null) && (this.o.c.isShowing())) {
            this.o.d();
        }
        return;
    }

    protected final void onMeasure(int p4, int p5)
    {
        super.onMeasure(p4, p5);
        if ((this.o != null) && (android.view.View$MeasureSpec.getMode(p4) == -2147483648)) {
            this.setMeasuredDimension(Math.min(Math.max(this.getMeasuredWidth(), this.a(this.getAdapter(), this.getBackground())), android.view.View$MeasureSpec.getSize(p4)), this.getMeasuredHeight());
        }
        return;
    }

    public final boolean onTouchEvent(android.view.MotionEvent p2)
    {
        if ((this.l == null) || (!this.l.onTouch(this, p2))) {
            int v0_3 = super.onTouchEvent(p2);
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean performClick()
    {
        if ((this.o == null) || (this.o.c.isShowing())) {
            int v0_4 = super.performClick();
        } else {
            this.o.b();
            v0_4 = 1;
        }
        return v0_4;
    }

    public final bridge synthetic void setAdapter(android.widget.Adapter p1)
    {
        this.setAdapter(((android.widget.SpinnerAdapter) p1));
        return;
    }

    public final void setAdapter(android.widget.SpinnerAdapter p4)
    {
        if (this.n) {
            super.setAdapter(p4);
            if (this.o != null) {
                android.content.res.Resources$Theme v0_3;
                if (this.k != null) {
                    v0_3 = this.k;
                } else {
                    v0_3 = this.getContext();
                }
                this.o.a(new android.support.v7.widget.ac(p4, v0_3.getTheme()));
            }
        } else {
            this.m = p4;
        }
        return;
    }

    public final void setBackgroundDrawable(android.graphics.drawable.Drawable p3)
    {
        super.setBackgroundDrawable(p3);
        if (this.j != null) {
            this.j.b(0);
        }
        return;
    }

    public final void setBackgroundResource(int p2)
    {
        super.setBackgroundResource(p2);
        if (this.j != null) {
            this.j.a(p2);
        }
        return;
    }

    public final void setDropDownHorizontalOffset(int p2)
    {
        if (this.o == null) {
            if (android.support.v7.widget.aa.b) {
                super.setDropDownHorizontalOffset(p2);
            }
        } else {
            this.o.f = p2;
        }
        return;
    }

    public final void setDropDownVerticalOffset(int p3)
    {
        if (this.o == null) {
            if (android.support.v7.widget.aa.b) {
                super.setDropDownVerticalOffset(p3);
            }
        } else {
            boolean v0_2 = this.o;
            v0_2.g = p3;
            v0_2.h = 1;
        }
        return;
    }

    public final void setDropDownWidth(int p2)
    {
        if (this.o == null) {
            if (android.support.v7.widget.aa.b) {
                super.setDropDownWidth(p2);
            }
        } else {
            this.p = p2;
        }
        return;
    }

    public final void setPopupBackgroundDrawable(android.graphics.drawable.Drawable p2)
    {
        if (this.o == null) {
            if (android.support.v7.widget.aa.b) {
                super.setPopupBackgroundDrawable(p2);
            }
        } else {
            this.o.a(p2);
        }
        return;
    }

    public final void setPopupBackgroundResource(int p2)
    {
        this.setPopupBackgroundDrawable(this.getPopupContext().getDrawable(p2));
        return;
    }

    public final void setPrompt(CharSequence p2)
    {
        if (this.o == null) {
            super.setPrompt(p2);
        } else {
            this.o.a = p2;
        }
        return;
    }

    public final void setSupportBackgroundTintList(android.content.res.ColorStateList p2)
    {
        if (this.j != null) {
            this.j.a(p2);
        }
        return;
    }

    public final void setSupportBackgroundTintMode(android.graphics.PorterDuff$Mode p2)
    {
        if (this.j != null) {
            this.j.a(p2);
        }
        return;
    }
}
