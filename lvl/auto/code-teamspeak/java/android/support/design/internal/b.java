package android.support.design.internal;
public final class b implements android.support.v7.internal.view.menu.x, android.widget.AdapterView$OnItemClickListener {
    private static final String j = "android:menu:list";
    private static final String k = "android:menu:adapter";
    public android.support.design.internal.NavigationMenuView a;
    public android.widget.LinearLayout b;
    public int c;
    public android.support.design.internal.c d;
    public android.view.LayoutInflater e;
    public android.content.res.ColorStateList f;
    public android.content.res.ColorStateList g;
    public android.graphics.drawable.Drawable h;
    public int i;
    private android.support.v7.internal.view.menu.y l;
    private android.support.v7.internal.view.menu.i m;
    private int n;
    private boolean o;
    private int p;

    public b()
    {
        return;
    }

    static synthetic android.view.LayoutInflater a(android.support.design.internal.b p1)
    {
        return p1.e;
    }

    private void a(android.graphics.drawable.Drawable p1)
    {
        this.h = p1;
        return;
    }

    static synthetic android.content.res.ColorStateList b(android.support.design.internal.b p1)
    {
        return p1.g;
    }

    private android.view.View b(int p4)
    {
        android.view.View v0_1 = this.e.inflate(p4, this.b, 0);
        this.a(v0_1);
        return v0_1;
    }

    private void b(android.view.View p5)
    {
        this.b.removeView(p5);
        if (this.b.getChildCount() == 0) {
            this.a.setPadding(0, this.i, 0, this.a.getPaddingBottom());
        }
        return;
    }

    private void c(android.support.v7.internal.view.menu.m p2)
    {
        this.d.a(p2);
        return;
    }

    static synthetic boolean c(android.support.design.internal.b p1)
    {
        return p1.o;
    }

    static synthetic int d(android.support.design.internal.b p1)
    {
        return p1.n;
    }

    private void d()
    {
        this.c = 1;
        return;
    }

    private android.content.res.ColorStateList e()
    {
        return this.g;
    }

    static synthetic android.content.res.ColorStateList e(android.support.design.internal.b p1)
    {
        return p1.f;
    }

    private android.content.res.ColorStateList f()
    {
        return this.f;
    }

    static synthetic android.graphics.drawable.Drawable f(android.support.design.internal.b p1)
    {
        return p1.h;
    }

    private android.graphics.drawable.Drawable g()
    {
        return this.h;
    }

    static synthetic android.support.v7.internal.view.menu.i g(android.support.design.internal.b p1)
    {
        return p1.m;
    }

    static synthetic int h(android.support.design.internal.b p1)
    {
        return p1.p;
    }

    public final android.support.v7.internal.view.menu.z a(android.view.ViewGroup p5)
    {
        if (this.a == null) {
            this.a = ((android.support.design.internal.NavigationMenuView) this.e.inflate(android.support.design.k.design_navigation_menu, p5, 0));
            if (this.d == null) {
                this.d = new android.support.design.internal.c(this);
            }
            this.b = ((android.widget.LinearLayout) this.e.inflate(android.support.design.k.design_navigation_item_header, this.a, 0));
            this.a.addHeaderView(this.b, 0, 0);
            this.a.setAdapter(this.d);
            this.a.setOnItemClickListener(this);
        }
        return this.a;
    }

    public final void a(int p2)
    {
        this.n = p2;
        this.o = 1;
        this.a(0);
        return;
    }

    public final void a(android.content.Context p3, android.support.v7.internal.view.menu.i p4)
    {
        this.e = android.view.LayoutInflater.from(p3);
        this.m = p4;
        int v0_1 = p3.getResources();
        this.i = v0_1.getDimensionPixelOffset(android.support.design.g.design_navigation_padding_top_default);
        this.p = v0_1.getDimensionPixelOffset(android.support.design.g.design_navigation_separator_vertical_padding);
        return;
    }

    public final void a(android.content.res.ColorStateList p2)
    {
        this.g = p2;
        this.a(0);
        return;
    }

    public final void a(android.os.Parcelable p7)
    {
        android.support.v7.internal.view.menu.m v0_1 = ((android.os.Bundle) p7).getSparseParcelableArray("android:menu:list");
        if (v0_1 != null) {
            this.a.restoreHierarchyState(v0_1);
        }
        android.support.v7.internal.view.menu.m v0_3 = ((android.os.Bundle) p7).getBundle("android:menu:adapter");
        if (v0_3 != null) {
            android.support.design.internal.c v1_1 = this.d;
            int v2_1 = v0_3.getInt("android:menu:checked", 0);
            if (v2_1 != 0) {
                v1_1.c = 1;
                java.util.Iterator v3 = v1_1.a.iterator();
                while (v3.hasNext()) {
                    android.support.v7.internal.view.menu.m v0_9 = ((android.support.design.internal.d) v3.next()).a;
                    if ((v0_9 != null) && (v0_9.getItemId() == v2_1)) {
                        v1_1.a(v0_9);
                        break;
                    }
                }
                v1_1.c = 0;
                v1_1.a();
            }
        }
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p2, boolean p3)
    {
        if (this.l != null) {
            this.l.a(p2, p3);
        }
        return;
    }

    public final void a(android.support.v7.internal.view.menu.y p1)
    {
        this.l = p1;
        return;
    }

    public final void a(android.view.View p4)
    {
        this.b.addView(p4);
        this.a.setPadding(0, 0, 0, this.a.getPaddingBottom());
        return;
    }

    public final void a(boolean p2)
    {
        if (this.d != null) {
            this.d.notifyDataSetChanged();
        }
        return;
    }

    public final boolean a()
    {
        return 0;
    }

    public final boolean a(android.support.v7.internal.view.menu.ad p2)
    {
        return 0;
    }

    public final boolean a(android.support.v7.internal.view.menu.m p2)
    {
        return 0;
    }

    public final int b()
    {
        return this.c;
    }

    public final void b(android.content.res.ColorStateList p2)
    {
        this.f = p2;
        this.a(0);
        return;
    }

    public final void b(boolean p2)
    {
        if (this.d != null) {
            this.d.c = p2;
        }
        return;
    }

    public final boolean b(android.support.v7.internal.view.menu.m p2)
    {
        return 0;
    }

    public final android.os.Parcelable c()
    {
        android.os.Bundle v0_1 = new android.os.Bundle();
        if (this.a != null) {
            String v1_2 = new android.util.SparseArray();
            this.a.saveHierarchyState(v1_2);
            v0_1.putSparseParcelableArray("android:menu:list", v1_2);
        }
        if (this.d != null) {
            int v2_2 = this.d;
            android.os.Bundle v3_1 = new android.os.Bundle();
            if (v2_2.b != null) {
                v3_1.putInt("android:menu:checked", v2_2.b.getItemId());
            }
            v0_1.putBundle("android:menu:adapter", v3_1);
        }
        return v0_1;
    }

    public final void onItemClick(android.widget.AdapterView p4, android.view.View p5, int p6, long p7)
    {
        android.support.v7.internal.view.menu.m v0_2 = (p6 - this.a.getHeaderViewsCount());
        if (v0_2 >= null) {
            this.b(1);
            android.support.v7.internal.view.menu.m v0_4 = this.d.a(v0_2).a;
            if ((v0_4 != null) && (v0_4.isCheckable())) {
                this.d.a(v0_4);
            }
            this.m.a(v0_4, this, 0);
            this.b(0);
            this.a(0);
        }
        return;
    }
}
