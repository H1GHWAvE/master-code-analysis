package android.support.design.widget;
public class FloatingActionButton$Behavior extends android.support.design.widget.t {
    private static final boolean a;
    private android.graphics.Rect b;

    static FloatingActionButton$Behavior()
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 11) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        android.support.design.widget.FloatingActionButton$Behavior.a = v0_1;
        return;
    }

    public FloatingActionButton$Behavior()
    {
        return;
    }

    private static void a(android.support.design.widget.CoordinatorLayout p11, android.support.design.widget.FloatingActionButton p12)
    {
        if (p12.getVisibility() == 0) {
            int v4 = 0;
            java.util.List v6 = p11.a(p12);
            int v7 = v6.size();
            int v5 = 0;
            while (v5 < v7) {
                float v0_3;
                float v0_2 = ((android.view.View) v6.get(v5));
                if (!(v0_2 instanceof android.support.design.widget.Snackbar$SnackbarLayout)) {
                    v0_3 = v4;
                } else {
                    if ((p12.getVisibility() != 0) || (v0_2.getVisibility() != 0)) {
                        int v1_3 = 0;
                    } else {
                        int v1_5;
                        int v8_0 = p11.i;
                        if (p12.getParent() == p11) {
                            v1_5 = 0;
                        } else {
                            v1_5 = 1;
                        }
                        int v1_7;
                        p11.a(p12, v1_5, v8_0);
                        android.graphics.Rect v9 = p11.j;
                        if (v0_2.getParent() == p11) {
                            v1_7 = 0;
                        } else {
                            v1_7 = 1;
                        }
                        p11.a(v0_2, v1_7, v9);
                        if ((v8_0.left > v9.right) || ((v8_0.top > v9.bottom) || ((v8_0.right < v9.left) || (v8_0.bottom < v9.top)))) {
                            v1_3 = 0;
                        } else {
                            v1_3 = 1;
                        }
                    }
                    if (v1_3 == 0) {
                    } else {
                        v0_3 = Math.min(v4, (android.support.v4.view.cx.n(v0_2) - ((float) v0_2.getHeight())));
                    }
                }
                v5++;
                v4 = v0_3;
            }
            android.support.v4.view.cx.b(p12, v4);
        }
        return;
    }

    private static void a(android.support.design.widget.FloatingActionButton p3, android.view.View p4)
    {
        if (((p4 instanceof android.support.design.widget.Snackbar$SnackbarLayout)) && (android.support.v4.view.cx.n(p3) != 0)) {
            int v1_1 = android.support.v4.view.cx.p(p3).b(0);
            android.support.v4.view.fk v0_6 = ((android.view.View) v1_1.a.get());
            if (v0_6 != null) {
                android.support.v4.view.fk.c.a(v1_1, v0_6);
            }
            android.support.v4.view.fk v0_9 = ((android.view.View) v1_1.a.get());
            if (v0_9 != null) {
                android.support.v4.view.fk.c.b(v1_1, v0_9);
            }
            v1_1.a(1065353216).a(android.support.design.widget.a.b).a(0);
        }
        return;
    }

    private boolean a(android.support.design.widget.CoordinatorLayout p3, android.support.design.widget.AppBarLayout p4, android.support.design.widget.FloatingActionButton p5)
    {
        int v0_10;
        if (((android.support.design.widget.w) p5.getLayoutParams()).f == p4.getId()) {
            if (this.b == null) {
                this.b = new android.graphics.Rect();
            }
            int v0_6 = this.b;
            android.support.design.widget.cz.a(p3, p4, v0_6);
            if (v0_6.bottom > p4.getMinimumHeightForVisibleOverlappingContent()) {
                p5.a.c();
            } else {
                p5.a.b();
            }
            v0_10 = 1;
        } else {
            v0_10 = 0;
        }
        return v0_10;
    }

    private boolean a(android.support.design.widget.CoordinatorLayout p8, android.support.design.widget.FloatingActionButton p9, int p10)
    {
        int v2 = 0;
        android.graphics.Rect v3_0 = p8.a(p9);
        int v4_0 = v3_0.size();
        int v1_0 = 0;
        while (v1_0 < v4_0) {
            int v0_1 = ((android.view.View) v3_0.get(v1_0));
            if (((v0_1 instanceof android.support.design.widget.AppBarLayout)) && (this.a(p8, ((android.support.design.widget.AppBarLayout) v0_1), p9))) {
                break;
            }
            v1_0++;
        }
        p8.a(p9, p10);
        android.graphics.Rect v3_1 = android.support.design.widget.FloatingActionButton.a(p9);
        if ((v3_1 != null) && ((v3_1.centerX() > 0) && (v3_1.centerY() > 0))) {
            int v1_3;
            int v0_7 = ((android.support.design.widget.w) p9.getLayoutParams());
            if (p9.getRight() < (p8.getWidth() - v0_7.rightMargin)) {
                if (p9.getLeft() > v0_7.leftMargin) {
                    v1_3 = 0;
                } else {
                    v1_3 = (- v3_1.left);
                }
            } else {
                v1_3 = v3_1.right;
            }
            if (p9.getBottom() < (p8.getBottom() - v0_7.bottomMargin)) {
                if (p9.getTop() <= v0_7.topMargin) {
                    v2 = (- v3_1.top);
                }
            } else {
                v2 = v3_1.bottom;
            }
            p9.offsetTopAndBottom(v2);
            p9.offsetLeftAndRight(v1_3);
        }
        return 1;
    }

    private boolean a(android.support.design.widget.CoordinatorLayout p12, android.support.design.widget.FloatingActionButton p13, android.view.View p14)
    {
        if (!(p14 instanceof android.support.design.widget.Snackbar$SnackbarLayout)) {
            if ((p14 instanceof android.support.design.widget.AppBarLayout)) {
                this.a(p12, ((android.support.design.widget.AppBarLayout) p14), p13);
            }
        } else {
            if (p13.getVisibility() == 0) {
                int v4 = 0;
                java.util.List v6 = p12.a(p13);
                int v7 = v6.size();
                int v5 = 0;
                while (v5 < v7) {
                    float v0_5;
                    float v0_4 = ((android.view.View) v6.get(v5));
                    if (!(v0_4 instanceof android.support.design.widget.Snackbar$SnackbarLayout)) {
                        v0_5 = v4;
                    } else {
                        if ((p13.getVisibility() != 0) || (v0_4.getVisibility() != 0)) {
                            int v1_3 = 0;
                        } else {
                            int v1_5;
                            int v8_0 = p12.i;
                            if (p13.getParent() == p12) {
                                v1_5 = 0;
                            } else {
                                v1_5 = 1;
                            }
                            int v1_7;
                            p12.a(p13, v1_5, v8_0);
                            android.graphics.Rect v9 = p12.j;
                            if (v0_4.getParent() == p12) {
                                v1_7 = 0;
                            } else {
                                v1_7 = 1;
                            }
                            p12.a(v0_4, v1_7, v9);
                            if ((v8_0.left > v9.right) || ((v8_0.top > v9.bottom) || ((v8_0.right < v9.left) || (v8_0.bottom < v9.top)))) {
                                v1_3 = 0;
                            } else {
                                v1_3 = 1;
                            }
                        }
                        if (v1_3 == 0) {
                        } else {
                            v0_5 = Math.min(v4, (android.support.v4.view.cx.n(v0_4) - ((float) v0_4.getHeight())));
                        }
                    }
                    v5++;
                    v4 = v0_5;
                }
                android.support.v4.view.cx.b(p13, v4);
            }
        }
        return 0;
    }

    private static float b(android.support.design.widget.CoordinatorLayout p11, android.support.design.widget.FloatingActionButton p12)
    {
        int v4 = 0;
        java.util.List v6 = p11.a(p12);
        int v7 = v6.size();
        int v5 = 0;
        while (v5 < v7) {
            float v0_2;
            float v0_1 = ((android.view.View) v6.get(v5));
            if (!(v0_1 instanceof android.support.design.widget.Snackbar$SnackbarLayout)) {
                v0_2 = v4;
            } else {
                if ((p12.getVisibility() != 0) || (v0_1.getVisibility() != 0)) {
                    int v1_3 = 0;
                } else {
                    int v1_5;
                    int v8_0 = p11.i;
                    if (p12.getParent() == p11) {
                        v1_5 = 0;
                    } else {
                        v1_5 = 1;
                    }
                    int v1_7;
                    p11.a(p12, v1_5, v8_0);
                    android.graphics.Rect v9 = p11.j;
                    if (v0_1.getParent() == p11) {
                        v1_7 = 0;
                    } else {
                        v1_7 = 1;
                    }
                    p11.a(v0_1, v1_7, v9);
                    if ((v8_0.left > v9.right) || ((v8_0.top > v9.bottom) || ((v8_0.right < v9.left) || (v8_0.bottom < v9.top)))) {
                        v1_3 = 0;
                    } else {
                        v1_3 = 1;
                    }
                }
                if (v1_3 == 0) {
                } else {
                    v0_2 = Math.min(v4, (android.support.v4.view.cx.n(v0_1) - ((float) v0_1.getHeight())));
                }
            }
            v5++;
            v4 = v0_2;
        }
        return v4;
    }

    private static void c(android.support.design.widget.CoordinatorLayout p7, android.support.design.widget.FloatingActionButton p8)
    {
        int v2 = 0;
        android.graphics.Rect v3 = android.support.design.widget.FloatingActionButton.a(p8);
        if ((v3 != null) && ((v3.centerX() > 0) && (v3.centerY() > 0))) {
            int v1_2;
            int v0_3 = ((android.support.design.widget.w) p8.getLayoutParams());
            if (p8.getRight() < (p7.getWidth() - v0_3.rightMargin)) {
                if (p8.getLeft() > v0_3.leftMargin) {
                    v1_2 = 0;
                } else {
                    v1_2 = (- v3.left);
                }
            } else {
                v1_2 = v3.right;
            }
            if (p8.getBottom() < (p7.getBottom() - v0_3.bottomMargin)) {
                if (p8.getTop() <= v0_3.topMargin) {
                    v2 = (- v3.top);
                }
            } else {
                v2 = v3.bottom;
            }
            p8.offsetTopAndBottom(v2);
            p8.offsetLeftAndRight(v1_2);
        }
        return;
    }

    private static boolean c(android.view.View p1)
    {
        if ((!android.support.design.widget.FloatingActionButton$Behavior.a) || (!(p1 instanceof android.support.design.widget.Snackbar$SnackbarLayout))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final synthetic void a(android.view.View p4, android.view.View p5)
    {
        if (((p5 instanceof android.support.design.widget.Snackbar$SnackbarLayout)) && (android.support.v4.view.cx.n(((android.support.design.widget.FloatingActionButton) p4)) != 0)) {
            int v1_1 = android.support.v4.view.cx.p(((android.support.design.widget.FloatingActionButton) p4)).b(0);
            android.support.v4.view.fk v0_6 = ((android.view.View) v1_1.a.get());
            if (v0_6 != null) {
                android.support.v4.view.fk.c.a(v1_1, v0_6);
            }
            android.support.v4.view.fk v0_9 = ((android.view.View) v1_1.a.get());
            if (v0_9 != null) {
                android.support.v4.view.fk.c.b(v1_1, v0_9);
            }
            v1_1.a(1065353216).a(android.support.design.widget.a.b).a(0);
        }
        return;
    }

    public final synthetic boolean a(android.support.design.widget.CoordinatorLayout p8, android.view.View p9, int p10)
    {
        int v2 = 0;
        android.graphics.Rect v3_0 = p8.a(((android.support.design.widget.FloatingActionButton) p9));
        int v4_0 = v3_0.size();
        int v1_0 = 0;
        while (v1_0 < v4_0) {
            int v0_1 = ((android.view.View) v3_0.get(v1_0));
            if (((v0_1 instanceof android.support.design.widget.AppBarLayout)) && (this.a(p8, ((android.support.design.widget.AppBarLayout) v0_1), ((android.support.design.widget.FloatingActionButton) p9)))) {
                break;
            }
            v1_0++;
        }
        p8.a(((android.support.design.widget.FloatingActionButton) p9), p10);
        android.graphics.Rect v3_1 = android.support.design.widget.FloatingActionButton.a(((android.support.design.widget.FloatingActionButton) p9));
        if ((v3_1 != null) && ((v3_1.centerX() > 0) && (v3_1.centerY() > 0))) {
            int v1_3;
            int v0_7 = ((android.support.design.widget.w) ((android.support.design.widget.FloatingActionButton) p9).getLayoutParams());
            if (((android.support.design.widget.FloatingActionButton) p9).getRight() < (p8.getWidth() - v0_7.rightMargin)) {
                if (((android.support.design.widget.FloatingActionButton) p9).getLeft() > v0_7.leftMargin) {
                    v1_3 = 0;
                } else {
                    v1_3 = (- v3_1.left);
                }
            } else {
                v1_3 = v3_1.right;
            }
            if (((android.support.design.widget.FloatingActionButton) p9).getBottom() < (p8.getBottom() - v0_7.bottomMargin)) {
                if (((android.support.design.widget.FloatingActionButton) p9).getTop() <= v0_7.topMargin) {
                    v2 = (- v3_1.top);
                }
            } else {
                v2 = v3_1.bottom;
            }
            ((android.support.design.widget.FloatingActionButton) p9).offsetTopAndBottom(v2);
            ((android.support.design.widget.FloatingActionButton) p9).offsetLeftAndRight(v1_3);
        }
        return 1;
    }

    public final synthetic boolean a(android.support.design.widget.CoordinatorLayout p12, android.view.View p13, android.view.View p14)
    {
        if (!(p14 instanceof android.support.design.widget.Snackbar$SnackbarLayout)) {
            if ((p14 instanceof android.support.design.widget.AppBarLayout)) {
                this.a(p12, ((android.support.design.widget.AppBarLayout) p14), ((android.support.design.widget.FloatingActionButton) p13));
            }
        } else {
            if (((android.support.design.widget.FloatingActionButton) p13).getVisibility() == 0) {
                int v4 = 0;
                java.util.List v6 = p12.a(((android.support.design.widget.FloatingActionButton) p13));
                int v7 = v6.size();
                int v5 = 0;
                while (v5 < v7) {
                    float v0_5;
                    float v0_4 = ((android.view.View) v6.get(v5));
                    if (!(v0_4 instanceof android.support.design.widget.Snackbar$SnackbarLayout)) {
                        v0_5 = v4;
                    } else {
                        if ((((android.support.design.widget.FloatingActionButton) p13).getVisibility() != 0) || (v0_4.getVisibility() != 0)) {
                            int v1_3 = 0;
                        } else {
                            int v1_5;
                            int v8_0 = p12.i;
                            if (((android.support.design.widget.FloatingActionButton) p13).getParent() == p12) {
                                v1_5 = 0;
                            } else {
                                v1_5 = 1;
                            }
                            int v1_7;
                            p12.a(((android.support.design.widget.FloatingActionButton) p13), v1_5, v8_0);
                            android.graphics.Rect v9 = p12.j;
                            if (v0_4.getParent() == p12) {
                                v1_7 = 0;
                            } else {
                                v1_7 = 1;
                            }
                            p12.a(v0_4, v1_7, v9);
                            if ((v8_0.left > v9.right) || ((v8_0.top > v9.bottom) || ((v8_0.right < v9.left) || (v8_0.bottom < v9.top)))) {
                                v1_3 = 0;
                            } else {
                                v1_3 = 1;
                            }
                        }
                        if (v1_3 == 0) {
                        } else {
                            v0_5 = Math.min(v4, (android.support.v4.view.cx.n(v0_4) - ((float) v0_4.getHeight())));
                        }
                    }
                    v5++;
                    v4 = v0_5;
                }
                android.support.v4.view.cx.b(((android.support.design.widget.FloatingActionButton) p13), v4);
            }
        }
        return 0;
    }

    public final bridge synthetic boolean b(android.view.View p2)
    {
        if ((!android.support.design.widget.FloatingActionButton$Behavior.a) || (!(p2 instanceof android.support.design.widget.Snackbar$SnackbarLayout))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }
}
