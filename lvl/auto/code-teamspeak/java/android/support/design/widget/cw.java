package android.support.design.widget;
final class cw extends android.support.design.widget.cr {
    final android.animation.ValueAnimator a;

    cw()
    {
        this.a = new android.animation.ValueAnimator();
        return;
    }

    public final void a()
    {
        this.a.start();
        return;
    }

    public final void a(float p4, float p5)
    {
        android.animation.ValueAnimator v0 = this.a;
        float[] v1_1 = new float[2];
        v1_1[0] = p4;
        v1_1[1] = p5;
        v0.setFloatValues(v1_1);
        return;
    }

    public final void a(int p5)
    {
        this.a.setDuration(((long) p5));
        return;
    }

    public final void a(int p4, int p5)
    {
        android.animation.ValueAnimator v0 = this.a;
        int[] v1_1 = new int[2];
        v1_1[0] = p4;
        v1_1[1] = p5;
        v0.setIntValues(v1_1);
        return;
    }

    public final void a(android.support.design.widget.cs p3)
    {
        this.a.addListener(new android.support.design.widget.cy(this, p3));
        return;
    }

    public final void a(android.support.design.widget.ct p3)
    {
        this.a.addUpdateListener(new android.support.design.widget.cx(this, p3));
        return;
    }

    public final void a(android.view.animation.Interpolator p2)
    {
        this.a.setInterpolator(p2);
        return;
    }

    public final boolean b()
    {
        return this.a.isRunning();
    }

    public final int c()
    {
        return ((Integer) this.a.getAnimatedValue()).intValue();
    }

    public final float d()
    {
        return ((Float) this.a.getAnimatedValue()).floatValue();
    }

    public final void e()
    {
        this.a.cancel();
        return;
    }

    public final float f()
    {
        return this.a.getAnimatedFraction();
    }

    public final void g()
    {
        this.a.end();
        return;
    }
}
