package android.support.design.widget;
final class cj extends android.support.v4.view.a {
    final synthetic android.support.design.widget.ce a;

    private cj(android.support.design.widget.ce p1)
    {
        this.a = p1;
        return;
    }

    synthetic cj(android.support.design.widget.ce p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void a(android.view.View p4, android.support.v4.view.a.q p5)
    {
        super.a(p4, p5);
        p5.b(android.support.design.widget.ce.getSimpleName());
        int v0_4 = android.support.design.widget.ce.b(this.a).g;
        if (!android.text.TextUtils.isEmpty(v0_4)) {
            android.support.v4.view.a.q.a.e(p5.b, v0_4);
        }
        if (android.support.design.widget.ce.c(this.a) != null) {
            android.support.v4.view.a.q.a.g(p5.b, android.support.design.widget.ce.c(this.a));
        }
        int v0_11;
        if (android.support.design.widget.ce.d(this.a) == null) {
            v0_11 = 0;
        } else {
            v0_11 = android.support.design.widget.ce.d(this.a).getText();
        }
        if (!android.text.TextUtils.isEmpty(v0_11)) {
            android.support.v4.view.a.q.a.T(p5.b);
            android.support.v4.view.a.q.a.a(p5.b, v0_11);
        }
        return;
    }

    public final void a(android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        super.a(p2, p3);
        p3.setClassName(android.support.design.widget.ce.getSimpleName());
        return;
    }

    public final void b(android.view.View p3, android.view.accessibility.AccessibilityEvent p4)
    {
        super.b(p3, p4);
        CharSequence v0_2 = android.support.design.widget.ce.b(this.a).g;
        if (!android.text.TextUtils.isEmpty(v0_2)) {
            p4.getText().add(v0_2);
        }
        return;
    }
}
