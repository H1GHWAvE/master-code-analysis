package android.support.design.widget;
public class AppBarLayout$Behavior$SavedState extends android.view.View$BaseSavedState {
    public static final android.os.Parcelable$Creator CREATOR;
    int a;
    float b;
    boolean c;

    static AppBarLayout$Behavior$SavedState()
    {
        android.support.design.widget.AppBarLayout$Behavior$SavedState.CREATOR = new android.support.design.widget.f();
        return;
    }

    public AppBarLayout$Behavior$SavedState(android.os.Parcel p2)
    {
        int v0_3;
        this(p2);
        this.a = p2.readInt();
        this.b = p2.readFloat();
        if (p2.readByte() == 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        this.c = v0_3;
        return;
    }

    public AppBarLayout$Behavior$SavedState(android.os.Parcelable p1)
    {
        this(p1);
        return;
    }

    public void writeToParcel(android.os.Parcel p2, int p3)
    {
        byte v0_3;
        super.writeToParcel(p2, p3);
        p2.writeInt(this.a);
        p2.writeFloat(this.b);
        if (!this.c) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        p2.writeByte(((byte) v0_3));
        return;
    }
}
