package android.support.design.widget;
public final class bz {
    public static final int a = 255;
    android.graphics.drawable.Drawable b;
    CharSequence c;
    CharSequence d;
    int e;
    android.view.View f;
    final android.support.design.widget.br g;
    private Object h;

    bz(android.support.design.widget.br p2)
    {
        this.e = -1;
        this.g = p2;
        return;
    }

    private static synthetic android.support.design.widget.br a(android.support.design.widget.bz p1)
    {
        return p1.g;
    }

    private android.support.design.widget.bz a(int p3)
    {
        this.f = android.view.LayoutInflater.from(this.g.getContext()).inflate(p3, 0);
        if (this.e >= 0) {
            android.support.design.widget.br.a(this.g, this.e);
        }
        return this;
    }

    private android.support.design.widget.bz a(android.graphics.drawable.Drawable p3)
    {
        this.b = p3;
        if (this.e >= 0) {
            android.support.design.widget.br.a(this.g, this.e);
        }
        return this;
    }

    private android.support.design.widget.bz a(android.view.View p3)
    {
        this.f = p3;
        if (this.e >= 0) {
            android.support.design.widget.br.a(this.g, this.e);
        }
        return this;
    }

    private android.support.design.widget.bz a(Object p1)
    {
        this.h = p1;
        return this;
    }

    private android.support.design.widget.bz b(CharSequence p3)
    {
        this.d = p3;
        if (this.e >= 0) {
            android.support.design.widget.br.a(this.g, this.e);
        }
        return this;
    }

    private Object b()
    {
        return this.h;
    }

    private void b(int p1)
    {
        this.e = p1;
        return;
    }

    private android.support.design.widget.bz c(int p3)
    {
        this.b = android.support.v7.internal.widget.av.a(this.g.getContext(), p3);
        if (this.e >= 0) {
            android.support.design.widget.br.a(this.g, this.e);
        }
        return this;
    }

    private android.view.View c()
    {
        return this.f;
    }

    private android.graphics.drawable.Drawable d()
    {
        return this.b;
    }

    private android.support.design.widget.bz d(int p2)
    {
        return this.a(this.g.getResources().getText(p2));
    }

    private int e()
    {
        return this.e;
    }

    private android.support.design.widget.bz e(int p3)
    {
        this.d = this.g.getResources().getText(p3);
        if (this.e >= 0) {
            android.support.design.widget.br.a(this.g, this.e);
        }
        return this;
    }

    private CharSequence f()
    {
        return this.c;
    }

    private boolean g()
    {
        int v0_2;
        if (this.g.getSelectedTabPosition() != this.e) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private CharSequence h()
    {
        return this.d;
    }

    public final android.support.design.widget.bz a(CharSequence p3)
    {
        this.c = p3;
        if (this.e >= 0) {
            android.support.design.widget.br.a(this.g, this.e);
        }
        return this;
    }

    public final void a()
    {
        this.g.a(this, 1);
        return;
    }
}
