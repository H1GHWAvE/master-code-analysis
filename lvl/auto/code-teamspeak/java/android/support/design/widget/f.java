package android.support.design.widget;
final class f implements android.os.Parcelable$Creator {

    f()
    {
        return;
    }

    private static android.support.design.widget.AppBarLayout$Behavior$SavedState a(android.os.Parcel p1)
    {
        return new android.support.design.widget.AppBarLayout$Behavior$SavedState(p1);
    }

    private static android.support.design.widget.AppBarLayout$Behavior$SavedState[] a(int p1)
    {
        android.support.design.widget.AppBarLayout$Behavior$SavedState[] v0 = new android.support.design.widget.AppBarLayout$Behavior$SavedState[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return new android.support.design.widget.AppBarLayout$Behavior$SavedState(p2);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.design.widget.AppBarLayout$Behavior$SavedState[] v0 = new android.support.design.widget.AppBarLayout$Behavior$SavedState[p2];
        return v0;
    }
}
