package android.support.design.widget;
final class dg {
    int a;
    int b;
    private final android.view.View c;
    private int d;
    private int e;

    public dg(android.view.View p1)
    {
        this.c = p1;
        return;
    }

    private static void a(android.view.View p2)
    {
        float v0 = android.support.v4.view.cx.m(p2);
        android.support.v4.view.cx.a(p2, (1065353216 + v0));
        android.support.v4.view.cx.a(p2, v0);
        return;
    }

    private void b()
    {
        android.support.v4.view.cx.d(this.c, (this.a - (this.c.getTop() - this.d)));
        android.support.v4.view.cx.e(this.c, (this.b - (this.c.getLeft() - this.e)));
        if (android.os.Build$VERSION.SDK_INT < 23) {
            android.support.design.widget.dg.a(this.c);
            android.view.View v0_5 = this.c.getParent();
            if ((v0_5 instanceof android.view.View)) {
                android.support.design.widget.dg.a(((android.view.View) v0_5));
            }
        }
        return;
    }

    private int c()
    {
        return this.a;
    }

    private int d()
    {
        return this.b;
    }

    public final void a()
    {
        this.d = this.c.getTop();
        this.e = this.c.getLeft();
        this.b();
        return;
    }

    public final boolean a(int p2)
    {
        int v0_1;
        if (this.a == p2) {
            v0_1 = 0;
        } else {
            this.a = p2;
            this.b();
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean b(int p2)
    {
        int v0_1;
        if (this.b == p2) {
            v0_1 = 0;
        } else {
            this.b = p2;
            this.b();
            v0_1 = 1;
        }
        return v0_1;
    }
}
