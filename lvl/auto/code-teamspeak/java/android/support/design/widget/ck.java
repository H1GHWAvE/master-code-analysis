package android.support.design.widget;
final class ck {
    final android.support.design.widget.cr a;

    ck(android.support.design.widget.cr p1)
    {
        this.a = p1;
        return;
    }

    private void a()
    {
        this.a.a();
        return;
    }

    private void a(android.support.design.widget.cn p3)
    {
        this.a.a(new android.support.design.widget.cm(this, p3));
        return;
    }

    private boolean b()
    {
        return this.a.b();
    }

    private int c()
    {
        return this.a.c();
    }

    private float d()
    {
        return this.a.d();
    }

    private void e()
    {
        this.a.e();
        return;
    }

    private float f()
    {
        return this.a.f();
    }

    private void g()
    {
        this.a.g();
        return;
    }

    public final void a(float p2, float p3)
    {
        this.a.a(p2, p3);
        return;
    }

    public final void a(int p2)
    {
        this.a.a(p2);
        return;
    }

    public final void a(int p2, int p3)
    {
        this.a.a(p2, p3);
        return;
    }

    public final void a(android.support.design.widget.cp p3)
    {
        this.a.a(new android.support.design.widget.cl(this, p3));
        return;
    }

    public final void a(android.view.animation.Interpolator p2)
    {
        this.a.a(p2);
        return;
    }
}
