package android.support.design.widget;
final class cz {
    private static final android.support.design.widget.db a;

    static cz()
    {
        if (android.os.Build$VERSION.SDK_INT < 11) {
            android.support.design.widget.cz.a = new android.support.design.widget.dc(0);
        } else {
            android.support.design.widget.cz.a = new android.support.design.widget.dd(0);
        }
        return;
    }

    cz()
    {
        return;
    }

    static void a(android.view.ViewGroup p3, android.view.View p4, android.graphics.Rect p5)
    {
        p5.set(0, 0, p4.getWidth(), p4.getHeight());
        android.support.design.widget.cz.a.a(p3, p4, p5);
        return;
    }

    private static void b(android.view.ViewGroup p1, android.view.View p2, android.graphics.Rect p3)
    {
        android.support.design.widget.cz.a.a(p1, p2, p3);
        return;
    }
}
