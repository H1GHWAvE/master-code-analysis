package android.support.design.widget;
final class bh {
    private static final int e = 0;
    private static final int f = 1500;
    private static final int g = 2750;
    private static android.support.design.widget.bh h;
    final Object a;
    final android.os.Handler b;
    android.support.design.widget.bk c;
    android.support.design.widget.bk d;

    private bh()
    {
        this.a = new Object();
        this.b = new android.os.Handler(android.os.Looper.getMainLooper(), new android.support.design.widget.bi(this));
        return;
    }

    static android.support.design.widget.bh a()
    {
        if (android.support.design.widget.bh.h == null) {
            android.support.design.widget.bh.h = new android.support.design.widget.bh();
        }
        return android.support.design.widget.bh.h;
    }

    private void a(int p4, android.support.design.widget.bj p5)
    {
        try {
            if (!this.d(p5)) {
                if (!this.e(p5)) {
                    this.d = new android.support.design.widget.bk(p4, p5);
                } else {
                    this.d.b = p4;
                }
                if ((this.c == null) || (!android.support.design.widget.bh.a(this.c, 4))) {
                    this.c = 0;
                    this.b();
                } else {
                }
            } else {
                this.c.b = p4;
                this.b.removeCallbacksAndMessages(this.c);
                this.a(this.c);
            }
        } catch (int v0_12) {
            throw v0_12;
        }
        return;
    }

    private static synthetic void a(android.support.design.widget.bh p2, android.support.design.widget.bk p3)
    {
        try {
            if ((p2.c == p3) || (p2.d == p3)) {
                android.support.design.widget.bh.a(p3, 2);
            }
        } catch (int v0_3) {
            throw v0_3;
        }
        return;
    }

    private void a(android.support.design.widget.bj p3, int p4)
    {
        try {
            if (!this.d(p3)) {
                if (this.e(p3)) {
                    android.support.design.widget.bh.a(this.d, p4);
                }
            } else {
                android.support.design.widget.bh.a(this.c, p4);
            }
        } catch (android.support.design.widget.bk v0_4) {
            throw v0_4;
        }
        return;
    }

    static boolean a(android.support.design.widget.bk p1, int p2)
    {
        int v0_3;
        int v0_2 = ((android.support.design.widget.bj) p1.a.get());
        if (v0_2 == 0) {
            v0_3 = 0;
        } else {
            v0_2.a(p2);
            v0_3 = 1;
        }
        return v0_3;
    }

    private void b(android.support.design.widget.bk p3)
    {
        try {
            if ((this.c == p3) || (this.d == p3)) {
                android.support.design.widget.bh.a(p3, 2);
            }
        } catch (int v0_3) {
            throw v0_3;
        }
        return;
    }

    private void f(android.support.design.widget.bj p3)
    {
        try {
            if (this.d(p3)) {
                this.c = 0;
                if (this.d != null) {
                    this.b();
                }
            }
        } catch (android.support.design.widget.bk v0_3) {
            throw v0_3;
        }
        return;
    }

    public final void a(android.support.design.widget.bj p3)
    {
        try {
            if (this.d(p3)) {
                this.a(this.c);
            }
        } catch (android.support.design.widget.bk v0_2) {
            throw v0_2;
        }
        return;
    }

    final void a(android.support.design.widget.bk p7)
    {
        if (p7.b != -2) {
            int v0_1 = 2750;
            if (p7.b <= 0) {
                if (p7.b == -1) {
                    v0_1 = 1500;
                }
            } else {
                v0_1 = p7.b;
            }
            this.b.removeCallbacksAndMessages(p7);
            this.b.sendMessageDelayed(android.os.Message.obtain(this.b, 0, p7), ((long) v0_1));
        }
        return;
    }

    final void b()
    {
        if (this.d != null) {
            this.c = this.d;
            this.d = 0;
            android.support.design.widget.bj v0_5 = ((android.support.design.widget.bj) this.c.a.get());
            if (v0_5 == null) {
                this.c = 0;
            } else {
                v0_5.a();
            }
        }
        return;
    }

    public final void b(android.support.design.widget.bj p4)
    {
        try {
            if (this.d(p4)) {
                this.b.removeCallbacksAndMessages(this.c);
            }
        } catch (android.os.Handler v0_2) {
            throw v0_2;
        }
        return;
    }

    public final void c(android.support.design.widget.bj p3)
    {
        try {
            if (this.d(p3)) {
                this.a(this.c);
            }
        } catch (android.support.design.widget.bk v0_2) {
            throw v0_2;
        }
        return;
    }

    final boolean d(android.support.design.widget.bj p2)
    {
        if ((this.c == null) || (!this.c.a(p2))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    final boolean e(android.support.design.widget.bj p2)
    {
        if ((this.d == null) || (!this.d.a(p2))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }
}
