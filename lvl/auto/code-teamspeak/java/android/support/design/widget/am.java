package android.support.design.widget;
final class am extends android.support.design.widget.ai {
    private android.graphics.drawable.Drawable h;
    private android.graphics.drawable.RippleDrawable i;
    private android.graphics.drawable.Drawable j;
    private android.view.animation.Interpolator k;

    am(android.view.View p3, android.support.design.widget.as p4)
    {
        this(p3, p4);
        if (!p3.isInEditMode()) {
            this.k = android.view.animation.AnimationUtils.loadInterpolator(this.f.getContext(), 17563661);
        }
        return;
    }

    private android.animation.Animator a(android.animation.Animator p2)
    {
        p2.setInterpolator(this.k);
        return p2;
    }

    final void a()
    {
        return;
    }

    public final void a(float p2)
    {
        android.support.v4.view.cx.f(this.f, p2);
        return;
    }

    final void a(int p3)
    {
        this.i.setColor(android.content.res.ColorStateList.valueOf(p3));
        return;
    }

    final void a(android.content.res.ColorStateList p2)
    {
        android.support.v4.e.a.a.a(this.h, p2);
        if (this.j != null) {
            android.support.v4.e.a.a.a(this.j, p2);
        }
        return;
    }

    final void a(android.graphics.PorterDuff$Mode p2)
    {
        android.support.v4.e.a.a.a(this.h, p2);
        return;
    }

    final void a(android.graphics.drawable.Drawable p7, android.content.res.ColorStateList p8, android.graphics.PorterDuff$Mode p9, int p10, int p11)
    {
        this.h = android.support.v4.e.a.a.c(p7.mutate());
        android.support.v4.e.a.a.a(this.h, p8);
        if (p9 != null) {
            android.support.v4.e.a.a.a(this.h, p9);
        }
        android.support.design.widget.as v0_4;
        if (p11 <= 0) {
            this.j = 0;
            v0_4 = this.h;
        } else {
            this.j = this.a(p11, p8);
            android.graphics.drawable.RippleDrawable v1_1 = new android.graphics.drawable.Drawable[2];
            v1_1[0] = this.j;
            v1_1[1] = this.h;
            v0_4 = new android.graphics.drawable.LayerDrawable(v1_1);
        }
        this.i = new android.graphics.drawable.RippleDrawable(android.content.res.ColorStateList.valueOf(p10), v0_4, 0);
        this.g.a(this.i);
        this.g.a(0, 0, 0, 0);
        return;
    }

    final void a(int[] p1)
    {
        return;
    }

    final void b(float p8)
    {
        android.animation.StateListAnimator v0_1 = new android.animation.StateListAnimator();
        android.animation.Animator v2_0 = this.f;
        float[] v4_0 = new float[1];
        v4_0[0] = p8;
        v0_1.addState(android.support.design.widget.am.c, this.a(android.animation.ObjectAnimator.ofFloat(v2_0, "translationZ", v4_0)));
        android.animation.Animator v2_3 = this.f;
        float[] v4_1 = new float[1];
        v4_1[0] = p8;
        v0_1.addState(android.support.design.widget.am.d, this.a(android.animation.ObjectAnimator.ofFloat(v2_3, "translationZ", v4_1)));
        android.animation.Animator v2_6 = this.f;
        float[] v4_2 = new float[1];
        v4_2[0] = 0;
        v0_1.addState(android.support.design.widget.am.e, this.a(android.animation.ObjectAnimator.ofFloat(v2_6, "translationZ", v4_2)));
        this.f.setStateListAnimator(v0_1);
        return;
    }

    final android.support.design.widget.j d()
    {
        return new android.support.design.widget.k();
    }
}
