package android.support.design.widget;
public final class FloatingActionButton extends android.widget.ImageView {
    private static final int b = 1;
    private static final int c;
    final android.support.design.widget.al a;
    private android.content.res.ColorStateList d;
    private android.graphics.PorterDuff$Mode e;
    private int f;
    private int g;
    private int h;
    private int i;
    private final android.graphics.Rect j;

    private FloatingActionButton(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private FloatingActionButton(android.content.Context p2, byte p3)
    {
        this(p2, 0);
        return;
    }

    private FloatingActionButton(android.content.Context p9, char p10)
    {
        int v0_0 = 0;
        this(p9, 0, 0);
        this.j = new android.graphics.Rect();
        android.support.design.widget.ad v2_1 = p9.obtainStyledAttributes(0, android.support.design.n.FloatingActionButton, 0, android.support.design.m.Widget_Design_FloatingActionButton);
        android.graphics.drawable.Drawable v1_4 = v2_1.getDrawable(android.support.design.n.FloatingActionButton_android_background);
        this.d = v2_1.getColorStateList(android.support.design.n.FloatingActionButton_backgroundTint);
        switch (v2_1.getInt(android.support.design.n.FloatingActionButton_backgroundTintMode, -1)) {
            case 3:
                v0_0 = android.graphics.PorterDuff$Mode.SRC_OVER;
                break;
            case 5:
                v0_0 = android.graphics.PorterDuff$Mode.SRC_IN;
                break;
            case 9:
                v0_0 = android.graphics.PorterDuff$Mode.SRC_ATOP;
                break;
            case 14:
                v0_0 = android.graphics.PorterDuff$Mode.MULTIPLY;
                break;
            case 15:
                v0_0 = android.graphics.PorterDuff$Mode.SCREEN;
                break;
        }
        this.e = v0_0;
        this.g = v2_1.getColor(android.support.design.n.FloatingActionButton_rippleColor, 0);
        this.h = v2_1.getInt(android.support.design.n.FloatingActionButton_fabSize, 0);
        this.f = v2_1.getDimensionPixelSize(android.support.design.n.FloatingActionButton_borderWidth, 0);
        float v6 = v2_1.getDimension(android.support.design.n.FloatingActionButton_elevation, 0);
        float v7_1 = v2_1.getDimension(android.support.design.n.FloatingActionButton_pressedTranslationZ, 0);
        v2_1.recycle();
        int v0_10 = new android.support.design.widget.ac(this);
        android.support.design.widget.ad v2_2 = android.os.Build$VERSION.SDK_INT;
        if (v2_2 < 21) {
            if (v2_2 < 12) {
                this.a = new android.support.design.widget.ad(this, v0_10);
            } else {
                this.a = new android.support.design.widget.ai(this, v0_10);
            }
        } else {
            this.a = new android.support.design.widget.am(this, v0_10);
        }
        this.i = ((this.getSizeDimension() - ((int) this.getResources().getDimension(android.support.design.g.design_fab_content_size))) / 2);
        this.a.a(v1_4, this.d, this.e, this.g, this.f);
        this.a.a(v6);
        this.a.b(v7_1);
        this.setClickable(1);
        return;
    }

    private static int a(int p2, int p3)
    {
        int v1 = android.view.View$MeasureSpec.getMode(p3);
        int v0 = android.view.View$MeasureSpec.getSize(p3);
        switch (v1) {
            case -2147483648:
                p2 = Math.min(p2, v0);
            case 0:
            default:
                break;
            case 1073741824:
                p2 = v0;
                break;
        }
        return p2;
    }

    private static android.graphics.PorterDuff$Mode a(int p1)
    {
        android.graphics.PorterDuff$Mode v0;
        switch (p1) {
            case 3:
                v0 = android.graphics.PorterDuff$Mode.SRC_OVER;
                break;
            case 5:
                v0 = android.graphics.PorterDuff$Mode.SRC_IN;
                break;
            case 9:
                v0 = android.graphics.PorterDuff$Mode.SRC_ATOP;
                break;
            case 14:
                v0 = android.graphics.PorterDuff$Mode.MULTIPLY;
                break;
            case 15:
                v0 = android.graphics.PorterDuff$Mode.SCREEN;
                break;
            default:
                v0 = 0;
        }
        return v0;
    }

    static synthetic android.graphics.Rect a(android.support.design.widget.FloatingActionButton p1)
    {
        return p1.j;
    }

    private void a()
    {
        this.a.c();
        return;
    }

    static synthetic void a(android.support.design.widget.FloatingActionButton p0, android.graphics.drawable.Drawable p1)
    {
        super.setBackgroundDrawable(p1);
        return;
    }

    static synthetic int b(android.support.design.widget.FloatingActionButton p1)
    {
        return p1.i;
    }

    private void b()
    {
        this.a.b();
        return;
    }

    protected final void drawableStateChanged()
    {
        super.drawableStateChanged();
        this.a.a(this.getDrawableState());
        return;
    }

    public final android.content.res.ColorStateList getBackgroundTintList()
    {
        return this.d;
    }

    public final android.graphics.PorterDuff$Mode getBackgroundTintMode()
    {
        return this.e;
    }

    final int getSizeDimension()
    {
        int v0_2;
        switch (this.h) {
            case 1:
                v0_2 = this.getResources().getDimensionPixelSize(android.support.design.g.design_fab_size_mini);
                break;
            default:
                v0_2 = this.getResources().getDimensionPixelSize(android.support.design.g.design_fab_size_normal);
        }
        return v0_2;
    }

    public final void jumpDrawablesToCurrentState()
    {
        super.jumpDrawablesToCurrentState();
        this.a.a();
        return;
    }

    protected final void onMeasure(int p4, int p5)
    {
        int v0_0 = this.getSizeDimension();
        int v0_2 = Math.min(android.support.design.widget.FloatingActionButton.a(v0_0, p4), android.support.design.widget.FloatingActionButton.a(v0_0, p5));
        this.setMeasuredDimension(((this.j.left + v0_2) + this.j.right), ((v0_2 + this.j.top) + this.j.bottom));
        return;
    }

    public final void setBackgroundDrawable(android.graphics.drawable.Drawable p7)
    {
        if (this.a != null) {
            this.a.a(p7, this.d, this.e, this.g, this.f);
        }
        return;
    }

    public final void setBackgroundTintList(android.content.res.ColorStateList p2)
    {
        if (this.d != p2) {
            this.d = p2;
            this.a.a(p2);
        }
        return;
    }

    public final void setBackgroundTintMode(android.graphics.PorterDuff$Mode p2)
    {
        if (this.e != p2) {
            this.e = p2;
            this.a.a(p2);
        }
        return;
    }

    public final void setRippleColor(int p2)
    {
        if (this.g != p2) {
            this.g = p2;
            this.a.a(p2);
        }
        return;
    }
}
