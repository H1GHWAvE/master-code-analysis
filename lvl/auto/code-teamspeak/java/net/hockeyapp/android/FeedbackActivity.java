package net.hockeyapp.android;
public class FeedbackActivity extends android.app.Activity implements android.view.View$OnClickListener, net.hockeyapp.android.s {
    private static final int a = 3;
    private java.util.ArrayList A;
    private boolean B;
    private boolean C;
    private String D;
    private final int b;
    private final int c;
    private final int d;
    private final int e;
    private android.content.Context f;
    private android.widget.TextView g;
    private android.widget.EditText h;
    private android.widget.EditText i;
    private android.widget.EditText j;
    private android.widget.EditText k;
    private android.widget.Button l;
    private android.widget.Button m;
    private android.widget.Button n;
    private android.widget.Button o;
    private android.widget.ScrollView p;
    private android.widget.LinearLayout q;
    private android.widget.ListView r;
    private net.hockeyapp.android.d.r s;
    private android.os.Handler t;
    private net.hockeyapp.android.d.q u;
    private android.os.Handler v;
    private java.util.List w;
    private String x;
    private net.hockeyapp.android.c.c y;
    private net.hockeyapp.android.a.a z;

    public FeedbackActivity()
    {
        this.b = 0;
        this.c = 1;
        this.d = 2;
        this.e = 3;
        return;
    }

    static synthetic java.util.ArrayList a(net.hockeyapp.android.FeedbackActivity p0, java.util.ArrayList p1)
    {
        p0.A = p1;
        return p1;
    }

    static synthetic net.hockeyapp.android.a.a a(net.hockeyapp.android.FeedbackActivity p0, net.hockeyapp.android.a.a p1)
    {
        p0.z = p1;
        return p1;
    }

    static synthetic net.hockeyapp.android.c.c a(net.hockeyapp.android.FeedbackActivity p1)
    {
        return p1.y;
    }

    static synthetic net.hockeyapp.android.c.c a(net.hockeyapp.android.FeedbackActivity p0, net.hockeyapp.android.c.c p1)
    {
        p0.y = p1;
        return p1;
    }

    private void a(android.widget.EditText p2, int p3)
    {
        p2.setError(net.hockeyapp.android.aj.a(p3));
        this.a(1);
        return;
    }

    private void a(String p3, String p4)
    {
        this.u = new net.hockeyapp.android.d.q(this, p3, this.v, p4);
        return;
    }

    private void a(String p12, String p13, String p14, String p15, String p16, java.util.List p17, String p18, android.os.Handler p19, boolean p20)
    {
        this.s = new net.hockeyapp.android.d.r(this.f, p12, p13, p14, p15, p16, p17, p18, p19, p20);
        net.hockeyapp.android.e.a.a(this.s);
        return;
    }

    static synthetic void a(net.hockeyapp.android.FeedbackActivity p2, String p3, String p4)
    {
        p2.u = new net.hockeyapp.android.d.q(p2, p3, p2.v, p4);
        net.hockeyapp.android.e.a.a(p2.u);
        return;
    }

    static synthetic void a(net.hockeyapp.android.FeedbackActivity p1, net.hockeyapp.android.c.h p2)
    {
        p1.runOnUiThread(new net.hockeyapp.android.q(p1, p2));
        return;
    }

    private void a(net.hockeyapp.android.c.h p2)
    {
        this.runOnUiThread(new net.hockeyapp.android.q(this, p2));
        return;
    }

    private boolean a(int p5)
    {
        int v0 = 1;
        if (p5 != 2) {
            if (p5 != 1) {
                v0 = 0;
            } else {
                android.content.Intent v1_1 = new android.content.Intent();
                v1_1.setType("image/*");
                v1_1.setAction("android.intent.action.GET_CONTENT");
                this.startActivityForResult(android.content.Intent.createChooser(v1_1, net.hockeyapp.android.aj.a(1047)), 1);
            }
        } else {
            android.content.Intent v1_4 = new android.content.Intent();
            v1_4.setType("*/*");
            v1_4.setAction("android.intent.action.GET_CONTENT");
            this.startActivityForResult(android.content.Intent.createChooser(v1_4, net.hockeyapp.android.aj.a(1046)), 2);
        }
        return v0;
    }

    private android.view.ViewGroup b()
    {
        return new net.hockeyapp.android.f.i(this);
    }

    private void b(String p3, String p4)
    {
        this.u = new net.hockeyapp.android.d.q(this, p3, this.v, p4);
        net.hockeyapp.android.e.a.a(this.u);
        return;
    }

    static synthetic void b(net.hockeyapp.android.FeedbackActivity p1)
    {
        p1.runOnUiThread(new net.hockeyapp.android.r(p1));
        return;
    }

    static synthetic android.content.Context c(net.hockeyapp.android.FeedbackActivity p1)
    {
        return p1.f;
    }

    private static void c()
    {
        return;
    }

    private void d()
    {
        this.D = net.hockeyapp.android.e.p.a.a(this);
        if ((this.D != null) && (!this.B)) {
            this.b(1);
            this.a(this.x, 0, 0, 0, 0, 0, this.D, this.t, 1);
        } else {
            this.b(0);
        }
        return;
    }

    static synthetic boolean d(net.hockeyapp.android.FeedbackActivity p1)
    {
        p1.B = 0;
        return 0;
    }

    static synthetic java.util.ArrayList e(net.hockeyapp.android.FeedbackActivity p1)
    {
        return p1.A;
    }

    private void e()
    {
        if (this.k != null) {
            ((android.view.inputmethod.InputMethodManager) this.getSystemService("input_method")).hideSoftInputFromWindow(this.k.getWindowToken(), 0);
        }
        return;
    }

    static synthetic android.widget.TextView f(net.hockeyapp.android.FeedbackActivity p1)
    {
        return p1.g;
    }

    private void f()
    {
        this.t = new net.hockeyapp.android.m(this);
        return;
    }

    static synthetic net.hockeyapp.android.a.a g(net.hockeyapp.android.FeedbackActivity p1)
    {
        return p1.z;
    }

    private void g()
    {
        this.v = new net.hockeyapp.android.o(this);
        return;
    }

    static synthetic android.widget.ListView h(net.hockeyapp.android.FeedbackActivity p1)
    {
        return p1.r;
    }

    private void h()
    {
        this.runOnUiThread(new net.hockeyapp.android.r(this));
        return;
    }

    private void i()
    {
        this.a(0);
        if (this.k != null) {
            ((android.view.inputmethod.InputMethodManager) this.getSystemService("input_method")).hideSoftInputFromWindow(this.k.getWindowToken(), 0);
        }
        String v7 = net.hockeyapp.android.e.p.a.a(this.f);
        String v2 = this.h.getText().toString().trim();
        String v3 = this.i.getText().toString().trim();
        String v4 = this.j.getText().toString().trim();
        String v5 = this.k.getText().toString().trim();
        if (!android.text.TextUtils.isEmpty(v4)) {
            if ((net.hockeyapp.android.t.b() != net.hockeyapp.android.c.i.c) || (!android.text.TextUtils.isEmpty(v2))) {
                if ((net.hockeyapp.android.t.c() != net.hockeyapp.android.c.i.c) || (!android.text.TextUtils.isEmpty(v3))) {
                    if (!android.text.TextUtils.isEmpty(v5)) {
                        if ((net.hockeyapp.android.t.c() != net.hockeyapp.android.c.i.c) || (net.hockeyapp.android.e.w.b(v3))) {
                            android.content.SharedPreferences$Editor v0_25 = net.hockeyapp.android.e.p.a;
                            android.content.SharedPreferences$Editor v1_6 = this.f;
                            if (v1_6 != null) {
                                v0_25.a = v1_6.getSharedPreferences("net.hockeyapp.android.prefs_name_email", 0);
                                if (v0_25.a != null) {
                                    v0_25.b = v0_25.a.edit();
                                    if ((v2 != null) && ((v3 != null) && (v4 != null))) {
                                        android.content.SharedPreferences$Editor v1_11 = v0_25.b;
                                        Object[] v10_1 = new Object[3];
                                        v10_1[0] = v2;
                                        v10_1[1] = v3;
                                        v10_1[2] = v4;
                                        v1_11.putString("net.hockeyapp.android.prefs_key_name_email", String.format("%s|%s|%s", v10_1));
                                    } else {
                                        v0_25.b.putString("net.hockeyapp.android.prefs_key_name_email", 0);
                                    }
                                    net.hockeyapp.android.e.n.a(v0_25.b);
                                }
                            }
                            this.a(this.x, v2, v3, v4, v5, ((net.hockeyapp.android.f.a) this.findViewById(8209)).getAttachments(), v7, this.t, 0);
                        } else {
                            this.a(this.i, 1039);
                        }
                    } else {
                        this.a(this.k, 1043);
                    }
                } else {
                    this.a(this.i, 1042);
                }
            } else {
                this.a(this.h, 1041);
            }
        } else {
            this.j.setVisibility(0);
            this.a(this.j, 1038);
        }
        return;
    }

    public final synthetic android.view.View a()
    {
        return this.b();
    }

    public final void a(boolean p2)
    {
        if (this.l != null) {
            this.l.setEnabled(p2);
        }
        return;
    }

    protected final void b(boolean p9)
    {
        this.p = ((android.widget.ScrollView) this.findViewById(131095));
        this.q = ((android.widget.LinearLayout) this.findViewById(131093));
        this.r = ((android.widget.ListView) this.findViewById(131094));
        if (!p9) {
            this.q.setVisibility(8);
            this.p.setVisibility(0);
            this.h = ((android.widget.EditText) this.findViewById(8194));
            this.i = ((android.widget.EditText) this.findViewById(8196));
            this.j = ((android.widget.EditText) this.findViewById(8198));
            this.k = ((android.widget.EditText) this.findViewById(8200));
            if (!this.C) {
                android.widget.EditText v0_26;
                android.widget.EditText v0_24 = net.hockeyapp.android.e.p.a;
                int v2_0 = this.f;
                if (v2_0 != 0) {
                    v0_24.a = v2_0.getSharedPreferences("net.hockeyapp.android.prefs_name_email", 0);
                    if (v0_24.a != null) {
                        v0_26 = v0_24.a.getString("net.hockeyapp.android.prefs_key_name_email", 0);
                    } else {
                        v0_26 = 0;
                    }
                } else {
                    v0_26 = 0;
                }
                if (v0_26 == null) {
                    this.h.setText("");
                    this.i.setText("");
                    this.j.setText("");
                    this.h.requestFocus();
                } else {
                    android.widget.EditText v0_31 = v0_26.split("\\|");
                    if ((v0_31 != null) && (v0_31.length >= 2)) {
                        this.h.setText(v0_31[0]);
                        this.i.setText(v0_31[1]);
                        if (v0_31.length < 3) {
                            this.j.requestFocus();
                        } else {
                            this.j.setText(v0_31[2]);
                            this.k.requestFocus();
                        }
                    }
                }
                this.C = 1;
            }
            this.k.setText("");
            if (net.hockeyapp.android.e.p.a.a(this.f) == null) {
                this.j.setVisibility(0);
            } else {
                this.j.setVisibility(8);
            }
            android.widget.EditText v0_42 = ((android.view.ViewGroup) this.findViewById(8209));
            v0_42.removeAllViews();
            if (this.w != null) {
                int v2_7 = this.w.iterator();
                while (v2_7.hasNext()) {
                    v0_42.addView(new net.hockeyapp.android.f.b(this, v0_42, ((android.net.Uri) v2_7.next())));
                }
            }
            this.m = ((android.widget.Button) this.findViewById(8208));
            this.m.setOnClickListener(this);
            this.registerForContextMenu(this.m);
            this.l = ((android.widget.Button) this.findViewById(8201));
            this.l.setOnClickListener(this);
        } else {
            this.q.setVisibility(0);
            this.p.setVisibility(8);
            this.g = ((android.widget.TextView) this.findViewById(8192));
            this.n = ((android.widget.Button) this.findViewById(131088));
            this.n.setOnClickListener(this);
            this.o = ((android.widget.Button) this.findViewById(131089));
            this.o.setOnClickListener(this);
        }
        return;
    }

    protected void onActivityResult(int p4, int p5, android.content.Intent p6)
    {
        if (p5 == -1) {
            if (p4 != 2) {
                if (p4 != 1) {
                    if (p4 == 3) {
                        android.content.ActivityNotFoundException v0_5 = ((android.net.Uri) p6.getParcelableExtra("imageUri"));
                        if (v0_5 != null) {
                            android.view.ViewGroup v1_2 = ((android.view.ViewGroup) this.findViewById(8209));
                            v1_2.addView(new net.hockeyapp.android.f.b(this, v1_2, v0_5));
                        }
                    }
                } else {
                    android.content.ActivityNotFoundException v0_6 = p6.getData();
                    if (v0_6 != null) {
                        try {
                            android.view.ViewGroup v1_4 = new android.content.Intent(this, net.hockeyapp.android.af);
                            v1_4.putExtra("imageUri", v0_6);
                            this.startActivityForResult(v1_4, 3);
                        } catch (android.content.ActivityNotFoundException v0_8) {
                            android.util.Log.e("HockeyApp", "Paint activity not declared!", v0_8);
                        }
                    }
                }
            } else {
                android.view.ViewGroup v1_6 = p6.getData();
                if (v1_6 != null) {
                    android.content.ActivityNotFoundException v0_10 = ((android.view.ViewGroup) this.findViewById(8209));
                    v0_10.addView(new net.hockeyapp.android.f.b(this, v0_10, v1_6));
                }
            }
        }
        return;
    }

    public void onClick(android.view.View p15)
    {
        switch (p15.getId()) {
            case 8201:
                this.a(0);
                if (this.k != null) {
                    ((android.view.inputmethod.InputMethodManager) this.getSystemService("input_method")).hideSoftInputFromWindow(this.k.getWindowToken(), 0);
                }
                String v7_1 = net.hockeyapp.android.e.p.a.a(this.f);
                Integer v2_3 = this.h.getText().toString().trim();
                String v3_1 = this.i.getText().toString().trim();
                String v4_1 = this.j.getText().toString().trim();
                String v5_1 = this.k.getText().toString().trim();
                if (!android.text.TextUtils.isEmpty(v4_1)) {
                    if ((net.hockeyapp.android.t.b() != net.hockeyapp.android.c.i.c) || (!android.text.TextUtils.isEmpty(v2_3))) {
                        if ((net.hockeyapp.android.t.c() != net.hockeyapp.android.c.i.c) || (!android.text.TextUtils.isEmpty(v3_1))) {
                            if (!android.text.TextUtils.isEmpty(v5_1)) {
                                if ((net.hockeyapp.android.t.c() != net.hockeyapp.android.c.i.c) || (net.hockeyapp.android.e.w.b(v3_1))) {
                                    android.content.SharedPreferences$Editor v0_34 = net.hockeyapp.android.e.p.a;
                                    android.content.SharedPreferences$Editor v1_9 = this.f;
                                    if (v1_9 != null) {
                                        v0_34.a = v1_9.getSharedPreferences("net.hockeyapp.android.prefs_name_email", 0);
                                        if (v0_34.a != null) {
                                            v0_34.b = v0_34.a.edit();
                                            if ((v2_3 != null) && ((v3_1 != null) && (v4_1 != null))) {
                                                android.content.SharedPreferences$Editor v1_14 = v0_34.b;
                                                Object[] v10_1 = new Object[3];
                                                v10_1[0] = v2_3;
                                                v10_1[1] = v3_1;
                                                v10_1[2] = v4_1;
                                                v1_14.putString("net.hockeyapp.android.prefs_key_name_email", String.format("%s|%s|%s", v10_1));
                                            } else {
                                                v0_34.b.putString("net.hockeyapp.android.prefs_key_name_email", 0);
                                            }
                                            net.hockeyapp.android.e.n.a(v0_34.b);
                                        }
                                    }
                                    this.a(this.x, v2_3, v3_1, v4_1, v5_1, ((net.hockeyapp.android.f.a) this.findViewById(8209)).getAttachments(), v7_1, this.t, 0);
                                } else {
                                    this.a(this.i, 1039);
                                }
                            } else {
                                this.a(this.k, 1043);
                            }
                        } else {
                            this.a(this.i, 1042);
                        }
                    } else {
                        this.a(this.h, 1041);
                    }
                } else {
                    this.j.setVisibility(0);
                    this.a(this.j, 1038);
                }
                break;
            case 8208:
                if (((android.view.ViewGroup) this.findViewById(8209)).getChildCount() < 3) {
                    this.openContextMenu(p15);
                } else {
                    android.content.SharedPreferences$Editor v1_1 = new Object[1];
                    v1_1[0] = Integer.valueOf(3);
                    android.widget.Toast.makeText(this, String.format("", v1_1), 1000).show();
                }
                break;
            case 131088:
                this.b(0);
                this.B = 1;
                break;
            case 131089:
                this.a(this.x, 0, 0, 0, 0, 0, net.hockeyapp.android.e.p.a.a(this.f), this.t, 1);
                break;
        }
        return;
    }

    public boolean onContextItemSelected(android.view.MenuItem p5)
    {
        int v0 = 1;
        switch (p5.getItemId()) {
            case 1:
            case 2:
                android.content.Intent v1_1 = p5.getItemId();
                if (v1_1 != 2) {
                    if (v1_1 != 1) {
                        v0 = 0;
                    } else {
                        android.content.Intent v1_3 = new android.content.Intent();
                        v1_3.setType("image/*");
                        v1_3.setAction("android.intent.action.GET_CONTENT");
                        this.startActivityForResult(android.content.Intent.createChooser(v1_3, net.hockeyapp.android.aj.a(1047)), 1);
                    }
                } else {
                    android.content.Intent v1_6 = new android.content.Intent();
                    v1_6.setType("*/*");
                    v1_6.setAction("android.intent.action.GET_CONTENT");
                    this.startActivityForResult(android.content.Intent.createChooser(v1_6, net.hockeyapp.android.aj.a(1046)), 2);
                }
                break;
            default:
                v0 = super.onContextItemSelected(p5);
        }
        return v0;
    }

    public void onCreate(android.os.Bundle p7)
    {
        super.onCreate(p7);
        this.setContentView(this.b());
        this.setTitle(net.hockeyapp.android.aj.a(1035));
        this.f = this;
        int v0_4 = this.getIntent().getExtras();
        if (v0_4 != 0) {
            this.x = v0_4.getString("url");
            android.os.Parcelable[] v3 = v0_4.getParcelableArray("initialAttachments");
            if (v3 != null) {
                this.w = new java.util.ArrayList();
                int v4 = v3.length;
                int v1_3 = 0;
                while (v1_3 < v4) {
                    this.w.add(((android.net.Uri) v3[v1_3]));
                    v1_3++;
                }
            }
        }
        if (p7 == null) {
            this.B = 0;
            this.C = 0;
        } else {
            this.C = p7.getBoolean("feedbackViewInitialized");
            this.B = p7.getBoolean("inSendFeedback");
        }
        ((android.app.NotificationManager) this.getSystemService("notification")).cancel(2);
        this.t = new net.hockeyapp.android.m(this);
        this.v = new net.hockeyapp.android.o(this);
        this.d();
        return;
    }

    public void onCreateContextMenu(android.view.ContextMenu p4, android.view.View p5, android.view.ContextMenu$ContextMenuInfo p6)
    {
        super.onCreateContextMenu(p4, p5, p6);
        p4.add(0, 2, 0, net.hockeyapp.android.aj.a(1044));
        p4.add(0, 1, 0, net.hockeyapp.android.aj.a(1045));
        return;
    }

    protected android.app.Dialog onCreateDialog(int p4)
    {
        android.app.AlertDialog v0_7;
        switch (p4) {
            case 0:
                v0_7 = new android.app.AlertDialog$Builder(this).setMessage(net.hockeyapp.android.aj.a(2051)).setCancelable(0).setTitle(net.hockeyapp.android.aj.a(2050)).setIcon(17301543).setPositiveButton(net.hockeyapp.android.aj.a(2048), new net.hockeyapp.android.l(this)).create();
                break;
            default:
                v0_7 = 0;
        }
        return v0_7;
    }

    public boolean onKeyDown(int p2, android.view.KeyEvent p3)
    {
        int v0_1;
        if (p2 != 4) {
            v0_1 = super.onKeyDown(p2, p3);
        } else {
            if (!this.B) {
                this.finish();
            } else {
                this.B = 0;
                this.d();
            }
            v0_1 = 1;
        }
        return v0_1;
    }

    protected void onPrepareDialog(int p2, android.app.Dialog p3)
    {
        switch (p2) {
            case 0:
                if (this.y == null) {
                    ((android.app.AlertDialog) p3).setMessage(net.hockeyapp.android.aj.a(1040));
                } else {
                    ((android.app.AlertDialog) p3).setMessage(this.y.a);
                }
                break;
        }
        return;
    }

    protected void onRestoreInstanceState(android.os.Bundle p5)
    {
        if (p5 != null) {
            boolean v0_2 = ((android.view.ViewGroup) this.findViewById(8209));
            java.util.Iterator v2 = p5.getParcelableArrayList("attachments").iterator();
            while (v2.hasNext()) {
                v0_2.addView(new net.hockeyapp.android.f.b(this, v0_2, ((android.net.Uri) v2.next())));
            }
            this.C = p5.getBoolean("feedbackViewInitialized");
        }
        super.onRestoreInstanceState(p5);
        return;
    }

    public Object onRetainNonConfigurationInstance()
    {
        if (this.s != null) {
            this.s.a();
        }
        return this.s;
    }

    protected void onSaveInstanceState(android.os.Bundle p3)
    {
        p3.putParcelableArrayList("attachments", ((net.hockeyapp.android.f.a) this.findViewById(8209)).getAttachments());
        p3.putBoolean("feedbackViewInitialized", this.C);
        p3.putBoolean("inSendFeedback", this.B);
        super.onSaveInstanceState(p3);
        return;
    }

    protected void onStop()
    {
        super.onStop();
        if (this.s != null) {
            this.s.a();
        }
        return;
    }
}
