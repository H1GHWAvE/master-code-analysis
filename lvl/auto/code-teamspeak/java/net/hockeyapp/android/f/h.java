package net.hockeyapp.android.f;
public final class h extends android.widget.LinearLayout {
    public static final int a = 12289;
    public static final int b = 12290;
    public static final int c = 12291;
    public static final int d = 12292;
    private android.widget.TextView e;
    private android.widget.TextView f;
    private android.widget.TextView g;
    private net.hockeyapp.android.f.a h;
    private boolean i;

    public h(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private h(android.content.Context p9, byte p10)
    {
        this(p9);
        this.i = 1;
        this.setOrientation(1);
        this.setGravity(3);
        this.setBackgroundColor(-3355444);
        this.e = new android.widget.TextView(p9);
        this.e.setId(12289);
        net.hockeyapp.android.f.a v0_6 = new android.widget.LinearLayout$LayoutParams(-2, -2);
        v0_6.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0);
        this.e.setLayoutParams(v0_6);
        this.e.setShadowLayer(1065353216, 0, 1065353216, -1);
        this.e.setSingleLine(1);
        this.e.setTextColor(-7829368);
        this.e.setTextSize(2, 1097859072);
        this.e.setTypeface(0, 0);
        this.addView(this.e);
        this.f = new android.widget.TextView(p9);
        this.f.setId(12290);
        net.hockeyapp.android.f.a v0_17 = new android.widget.LinearLayout$LayoutParams(-2, -2);
        v0_17.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0);
        this.f.setLayoutParams(v0_17);
        this.f.setShadowLayer(1065353216, 0, 1065353216, -1);
        this.f.setSingleLine(1);
        this.f.setTextColor(-7829368);
        this.f.setTextSize(2, 1097859072);
        this.f.setTypeface(0, 2);
        this.addView(this.f);
        this.g = new android.widget.TextView(p9);
        this.g.setId(12291);
        net.hockeyapp.android.f.a v0_28 = new android.widget.LinearLayout$LayoutParams(-2, -2);
        v0_28.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())));
        this.g.setLayoutParams(v0_28);
        this.g.setShadowLayer(1065353216, 0, 1065353216, -1);
        this.g.setSingleLine(0);
        this.g.setTextColor(-16777216);
        this.g.setTextSize(2, 1099956224);
        this.g.setTypeface(0, 0);
        this.addView(this.g);
        this.h = new net.hockeyapp.android.f.a(p9);
        this.h.setId(12292);
        net.hockeyapp.android.f.a v0_39 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        v0_39.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())));
        this.h.setLayoutParams(v0_39);
        this.addView(this.h);
        return;
    }

    private void a()
    {
        this.setOrientation(1);
        this.setGravity(3);
        this.setBackgroundColor(-3355444);
        return;
    }

    private void a(android.content.Context p7)
    {
        this.e = new android.widget.TextView(p7);
        this.e.setId(12289);
        android.widget.TextView v0_4 = new android.widget.LinearLayout$LayoutParams(-2, -2);
        v0_4.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0);
        this.e.setLayoutParams(v0_4);
        this.e.setShadowLayer(1065353216, 0, 1065353216, -1);
        this.e.setSingleLine(1);
        this.e.setTextColor(-7829368);
        this.e.setTextSize(2, 1097859072);
        this.e.setTypeface(0, 0);
        this.addView(this.e);
        return;
    }

    private void b(android.content.Context p8)
    {
        this.f = new android.widget.TextView(p8);
        this.f.setId(12290);
        android.widget.TextView v0_4 = new android.widget.LinearLayout$LayoutParams(-2, -2);
        v0_4.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0);
        this.f.setLayoutParams(v0_4);
        this.f.setShadowLayer(1065353216, 0, 1065353216, -1);
        this.f.setSingleLine(1);
        this.f.setTextColor(-7829368);
        this.f.setTextSize(2, 1097859072);
        this.f.setTypeface(0, 2);
        this.addView(this.f);
        return;
    }

    private void c(android.content.Context p7)
    {
        this.g = new android.widget.TextView(p7);
        this.g.setId(12291);
        android.widget.TextView v0_4 = new android.widget.LinearLayout$LayoutParams(-2, -2);
        v0_4.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())));
        this.g.setLayoutParams(v0_4);
        this.g.setShadowLayer(1065353216, 0, 1065353216, -1);
        this.g.setSingleLine(0);
        this.g.setTextColor(-16777216);
        this.g.setTextSize(2, 1099956224);
        this.g.setTypeface(0, 0);
        this.addView(this.g);
        return;
    }

    private void d(android.content.Context p5)
    {
        this.h = new net.hockeyapp.android.f.a(p5);
        this.h.setId(12292);
        net.hockeyapp.android.f.a v0_4 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        v0_4.setMargins(((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())), ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())));
        this.h.setLayoutParams(v0_4);
        this.addView(this.h);
        return;
    }

    private void setAuthorLaberColor(int p2)
    {
        if (this.e != null) {
            this.e.setTextColor(p2);
        }
        return;
    }

    private void setDateLaberColor(int p2)
    {
        if (this.f != null) {
            this.f.setTextColor(p2);
        }
        return;
    }

    private void setMessageLaberColor(int p2)
    {
        if (this.g != null) {
            this.g.setTextColor(p2);
        }
        return;
    }

    public final void setAuthorLabelText(String p2)
    {
        if ((this.e != null) && (p2 != null)) {
            this.e.setText(p2);
        }
        return;
    }

    public final void setDateLabelText(String p2)
    {
        if ((this.f != null) && (p2 != null)) {
            this.f.setText(p2);
        }
        return;
    }

    public final void setFeedbackMessageViewBgAndTextColor(int p4)
    {
        if (p4 != 0) {
            if (p4 == 1) {
                this.setBackgroundColor(-1);
                this.setAuthorLaberColor(-3355444);
                this.setDateLaberColor(-3355444);
            }
        } else {
            this.setBackgroundColor(-3355444);
            this.setAuthorLaberColor(-1);
            this.setDateLaberColor(-1);
        }
        this.setMessageLaberColor(-16777216);
        return;
    }

    public final void setMessageLabelText(String p2)
    {
        if ((this.g != null) && (p2 != null)) {
            this.g.setText(p2);
        }
        return;
    }
}
