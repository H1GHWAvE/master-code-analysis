package net.hockeyapp.android.d;
public final class e {
    final net.hockeyapp.android.c.e a;
    final net.hockeyapp.android.f.b b;
    boolean c;
    int d;

    private e(net.hockeyapp.android.c.e p2, net.hockeyapp.android.f.b p3)
    {
        this.a = p2;
        this.b = p3;
        this.c = 0;
        this.d = 2;
        return;
    }

    public synthetic e(net.hockeyapp.android.c.e p1, net.hockeyapp.android.f.b p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    private net.hockeyapp.android.c.e a()
    {
        return this.a;
    }

    private void a(boolean p1)
    {
        this.c = p1;
        return;
    }

    private net.hockeyapp.android.f.b b()
    {
        return this.b;
    }

    private boolean c()
    {
        return this.c;
    }

    private boolean d()
    {
        int v0_1;
        if (this.d <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private boolean e()
    {
        int v0_2;
        int v0_1 = (this.d - 1);
        this.d = v0_1;
        if (v0_1 >= 0) {
            v0_2 = 1;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }
}
