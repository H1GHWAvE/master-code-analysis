package net.hockeyapp.android.d;
public final class q extends android.os.AsyncTask {
    public static final int a = 2;
    public static final String b = "net.hockeyapp.android.feedback";
    public static final String c = "idLastMessageSend";
    public static final String d = "idLastMessageProcessed";
    public String e;
    private android.content.Context f;
    private String g;
    private android.os.Handler h;
    private String i;

    public q(android.content.Context p2, String p3, android.os.Handler p4, String p5)
    {
        this.f = p2;
        this.g = p3;
        this.h = p4;
        this.i = p5;
        this.e = 0;
        return;
    }

    private varargs net.hockeyapp.android.c.h a()
    {
        android.app.NotificationManager v0_2;
        android.app.Notification v1_0 = 0;
        if ((this.f == null) || (this.g == null)) {
            v0_2 = 0;
        } else {
            net.hockeyapp.android.c.h v2 = net.hockeyapp.android.e.i.a(this.g);
            if ((v2 != null) && (v2.b != null)) {
                android.app.NotificationManager v0_6 = v2.b.e;
                if ((v0_6 != null) && (!v0_6.isEmpty())) {
                    android.app.NotificationManager v0_9 = ((net.hockeyapp.android.c.g) v0_6.get((v0_6.size() - 1))).g;
                    android.content.Context v4_4 = this.f.getSharedPreferences("net.hockeyapp.android.feedback", 0);
                    if (!this.i.equals("send")) {
                        if (this.i.equals("fetch")) {
                            int v5_6 = v4_4.getInt("idLastMessageSend", -1);
                            String v6_3 = v4_4.getInt("idLastMessageProcessed", -1);
                            if ((v0_9 != v5_6) && (v0_9 != v6_3)) {
                                android.app.NotificationManager v0_12;
                                net.hockeyapp.android.e.n.a(v4_4.edit().putInt("idLastMessageProcessed", v0_9));
                                android.app.NotificationManager v0_11 = net.hockeyapp.android.t.a();
                                if (v0_11 == null) {
                                    v0_12 = 0;
                                } else {
                                    v0_12 = v0_11.a();
                                }
                                if (v0_12 == null) {
                                    android.content.Context v4_6 = this.f;
                                    if (this.e != null) {
                                        android.app.NotificationManager v0_16 = ((android.app.NotificationManager) v4_6.getSystemService("notification"));
                                        int v5_9 = v4_6.getResources().getIdentifier("ic_menu_refresh", "drawable", "android");
                                        if (net.hockeyapp.android.t.a() != null) {
                                            v1_0 = net.hockeyapp.android.FeedbackActivity;
                                        }
                                        if (v1_0 == null) {
                                            v1_0 = net.hockeyapp.android.FeedbackActivity;
                                        }
                                        String v6_7 = new android.content.Intent();
                                        v6_7.setFlags(805306368);
                                        v6_7.setClass(v4_6, v1_0);
                                        v6_7.putExtra("url", this.e);
                                        android.app.Notification v1_4 = net.hockeyapp.android.e.w.a(v4_6, android.app.PendingIntent.getActivity(v4_6, 0, v6_7, 1073741824), "HockeyApp Feedback", "A new answer to your feedback is available.", v5_9);
                                        if (v1_4 != null) {
                                            v0_16.notify(2, v1_4);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        net.hockeyapp.android.e.n.a(v4_4.edit().putInt("idLastMessageSend", v0_9).putInt("idLastMessageProcessed", v0_9));
                    }
                }
            }
            v0_2 = v2;
        }
        return v0_2;
    }

    private void a(android.content.Context p6)
    {
        if (this.e != null) {
            android.app.NotificationManager v0_3 = ((android.app.NotificationManager) p6.getSystemService("notification"));
            int v2_1 = p6.getResources().getIdentifier("ic_menu_refresh", "drawable", "android");
            android.app.Notification v1_1 = 0;
            if (net.hockeyapp.android.t.a() != null) {
                v1_1 = net.hockeyapp.android.FeedbackActivity;
            }
            if (v1_1 == null) {
                v1_1 = net.hockeyapp.android.FeedbackActivity;
            }
            String v3_3 = new android.content.Intent();
            v3_3.setFlags(805306368);
            v3_3.setClass(p6, v1_1);
            v3_3.putExtra("url", this.e);
            android.app.Notification v1_5 = net.hockeyapp.android.e.w.a(p6, android.app.PendingIntent.getActivity(p6, 0, v3_3, 1073741824), "HockeyApp Feedback", "A new answer to your feedback is available.", v2_1);
            if (v1_5 != null) {
                v0_3.notify(2, v1_5);
            }
        }
        return;
    }

    private void a(String p1)
    {
        this.e = p1;
        return;
    }

    private void a(java.util.ArrayList p8)
    {
        android.app.NotificationManager v0_4 = ((net.hockeyapp.android.c.g) p8.get((p8.size() - 1))).g;
        android.app.Notification v1_1 = this.f.getSharedPreferences("net.hockeyapp.android.feedback", 0);
        if (!this.i.equals("send")) {
            if (this.i.equals("fetch")) {
                android.content.Context v3_6 = v1_1.getInt("idLastMessageSend", -1);
                int v4_3 = v1_1.getInt("idLastMessageProcessed", -1);
                if ((v0_4 != v3_6) && (v0_4 != v4_3)) {
                    android.app.NotificationManager v0_7;
                    net.hockeyapp.android.e.n.a(v1_1.edit().putInt("idLastMessageProcessed", v0_4));
                    android.app.NotificationManager v0_6 = net.hockeyapp.android.t.a();
                    if (v0_6 == null) {
                        v0_7 = 0;
                    } else {
                        v0_7 = v0_6.a();
                    }
                    if (v0_7 == null) {
                        android.content.Context v3_8 = this.f;
                        if (this.e != null) {
                            android.app.NotificationManager v0_11 = ((android.app.NotificationManager) v3_8.getSystemService("notification"));
                            int v4_5 = v3_8.getResources().getIdentifier("ic_menu_refresh", "drawable", "android");
                            android.app.Notification v1_4 = 0;
                            if (net.hockeyapp.android.t.a() != null) {
                                v1_4 = net.hockeyapp.android.FeedbackActivity;
                            }
                            if (v1_4 == null) {
                                v1_4 = net.hockeyapp.android.FeedbackActivity;
                            }
                            String v5_4 = new android.content.Intent();
                            v5_4.setFlags(805306368);
                            v5_4.setClass(v3_8, v1_4);
                            v5_4.putExtra("url", this.e);
                            android.app.Notification v1_8 = net.hockeyapp.android.e.w.a(v3_8, android.app.PendingIntent.getActivity(v3_8, 0, v5_4, 1073741824), "HockeyApp Feedback", "A new answer to your feedback is available.", v4_5);
                            if (v1_8 != null) {
                                v0_11.notify(2, v1_8);
                            }
                        }
                    }
                }
            }
        } else {
            net.hockeyapp.android.e.n.a(v1_1.edit().putInt("idLastMessageSend", v0_4).putInt("idLastMessageProcessed", v0_4));
        }
        return;
    }

    private void a(net.hockeyapp.android.c.h p4)
    {
        if ((p4 != null) && (this.h != null)) {
            android.os.Message v0_2 = new android.os.Message();
            android.os.Handler v1_1 = new android.os.Bundle();
            v1_1.putSerializable("parse_feedback_response", p4);
            v0_2.setData(v1_1);
            this.h.sendMessage(v0_2);
        }
        return;
    }

    protected final synthetic Object doInBackground(Object[] p10)
    {
        android.app.NotificationManager v0_2;
        android.app.Notification v1_0 = 0;
        if ((this.f == null) || (this.g == null)) {
            v0_2 = 0;
        } else {
            net.hockeyapp.android.c.h v2 = net.hockeyapp.android.e.i.a(this.g);
            if ((v2 != null) && (v2.b != null)) {
                android.app.NotificationManager v0_6 = v2.b.e;
                if ((v0_6 != null) && (!v0_6.isEmpty())) {
                    android.app.NotificationManager v0_9 = ((net.hockeyapp.android.c.g) v0_6.get((v0_6.size() - 1))).g;
                    android.content.Context v4_4 = this.f.getSharedPreferences("net.hockeyapp.android.feedback", 0);
                    if (!this.i.equals("send")) {
                        if (this.i.equals("fetch")) {
                            int v5_6 = v4_4.getInt("idLastMessageSend", -1);
                            String v6_3 = v4_4.getInt("idLastMessageProcessed", -1);
                            if ((v0_9 != v5_6) && (v0_9 != v6_3)) {
                                android.app.NotificationManager v0_12;
                                net.hockeyapp.android.e.n.a(v4_4.edit().putInt("idLastMessageProcessed", v0_9));
                                android.app.NotificationManager v0_11 = net.hockeyapp.android.t.a();
                                if (v0_11 == null) {
                                    v0_12 = 0;
                                } else {
                                    v0_12 = v0_11.a();
                                }
                                if (v0_12 == null) {
                                    android.content.Context v4_6 = this.f;
                                    if (this.e != null) {
                                        android.app.NotificationManager v0_16 = ((android.app.NotificationManager) v4_6.getSystemService("notification"));
                                        int v5_9 = v4_6.getResources().getIdentifier("ic_menu_refresh", "drawable", "android");
                                        if (net.hockeyapp.android.t.a() != null) {
                                            v1_0 = net.hockeyapp.android.FeedbackActivity;
                                        }
                                        if (v1_0 == null) {
                                            v1_0 = net.hockeyapp.android.FeedbackActivity;
                                        }
                                        String v6_7 = new android.content.Intent();
                                        v6_7.setFlags(805306368);
                                        v6_7.setClass(v4_6, v1_0);
                                        v6_7.putExtra("url", this.e);
                                        android.app.Notification v1_4 = net.hockeyapp.android.e.w.a(v4_6, android.app.PendingIntent.getActivity(v4_6, 0, v6_7, 1073741824), "HockeyApp Feedback", "A new answer to your feedback is available.", v5_9);
                                        if (v1_4 != null) {
                                            v0_16.notify(2, v1_4);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        net.hockeyapp.android.e.n.a(v4_4.edit().putInt("idLastMessageSend", v0_9).putInt("idLastMessageProcessed", v0_9));
                    }
                }
            }
            v0_2 = v2;
        }
        return v0_2;
    }

    protected final synthetic void onPostExecute(Object p4)
    {
        if ((((net.hockeyapp.android.c.h) p4) != null) && (this.h != null)) {
            android.os.Message v0_2 = new android.os.Message();
            android.os.Handler v1_1 = new android.os.Bundle();
            v1_1.putSerializable("parse_feedback_response", ((net.hockeyapp.android.c.h) p4));
            v0_2.setData(v1_1);
            this.h.sendMessage(v0_2);
        }
        return;
    }
}
