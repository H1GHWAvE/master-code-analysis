package net.hockeyapp.android;
public final class aj {
    public static final int A = 1034;
    public static final int B = 1035;
    public static final int C = 1036;
    public static final int D = 1037;
    public static final int E = 1038;
    public static final int F = 1039;
    public static final int G = 1040;
    public static final int H = 1041;
    public static final int I = 1042;
    public static final int J = 1043;
    public static final int K = 1044;
    public static final int L = 1045;
    public static final int M = 1046;
    public static final int N = 1047;
    public static final int O = 1048;
    public static final int P = 1280;
    public static final int Q = 1281;
    public static final int R = 1282;
    public static final int S = 1283;
    public static final int T = 1284;
    public static final int U = 1536;
    public static final int V = 1537;
    public static final int W = 1538;
    public static final int X = 1539;
    public static final int Y = 1540;
    public static final int Z = 1541;
    public static final int a = 0;
    public static final int aa = 1542;
    public static final int ab = 1792;
    public static final int ac = 1793;
    public static final int ad = 1794;
    public static final int ae = 1795;
    public static final int af = 2048;
    public static final int ag = 2049;
    public static final int ah = 2050;
    public static final int ai = 2051;
    private static final java.util.Map aj = None;
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = 3;
    public static final int e = 4;
    public static final int f = 256;
    public static final int g = 257;
    public static final int h = 258;
    public static final int i = 259;
    public static final int j = 512;
    public static final int k = 513;
    public static final int l = 514;
    public static final int m = 515;
    public static final int n = 516;
    public static final int o = 768;
    public static final int p = 769;
    public static final int q = 1024;
    public static final int r = 1025;
    public static final int s = 1026;
    public static final int t = 1027;
    public static final int u = 1028;
    public static final int v = 1029;
    public static final int w = 1030;
    public static final int x = 1031;
    public static final int y = 1032;
    public static final int z = 1033;

    static aj()
    {
        java.util.Map v0_1 = new java.util.HashMap();
        net.hockeyapp.android.aj.aj = v0_1;
        v0_1.put(Integer.valueOf(0), "Crash Data");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1), "The app found information about previous crashes. Would you like to send this data to the developer?");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(2), "Dismiss");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(3), "Always send");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(4), "Send");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(256), "Download Failed");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(257), "The update could not be downloaded. Would you like to try again?");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(258), "Cancel");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(259), "Retry");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(512), "Please install the latest version to continue to use this app.");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(513), "Update Available");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(514), "Show information about the new update?");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(515), "Dismiss");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(516), "Show");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(768), "Build Expired");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(769), "This has build has expired. Please check HockeyApp for any updates.");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1024), "Feedback Failed");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1025), "Would you like to send your feedback again?");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1026), "Name");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1027), "Email");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1028), "Subject");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1029), "Message");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1030), "Last Updated: ");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1031), "Add Attachment");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1032), "Send Feedback");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1033), "Add a Response");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1034), "Refresh");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1035), "Feedback");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1036), "Message couldn\'t be posted. Please check your input values and your connection, then try again.");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1037), "No response from server. Please check your connection, then try again.");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1038), "Please enter a subject");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1041), "Please enter a name");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1042), "Please enter an email address");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1043), "Please enter a feedback text");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1039), "Message couldn\'t be posted. Please check the format of your email address.");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1040), "An error has occurred");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1044), "Attach File");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1045), "Attach Picture");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1046), "Select File");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1047), "Select Picture");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1048), "Only %s attachments allowed.");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1280), "Please enter your account credentials.");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1281), "Please fill in the missing account credentials.");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1282), "Email");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1283), "Password");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1284), "Login");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1536), "Draw something!");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1537), "Save");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1538), "Undo");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1539), "Clear");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1540), "Discard your drawings?");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1541), "No");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1542), "Yes");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1792), "Need storage access");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1793), "In order to download and install app updates you will have to allow the app to access your device storage.");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1794), "Cancel");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(1795), "Retry");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(2048), "OK");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(2049), "Cancel");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(2050), "Error");
        net.hockeyapp.android.aj.aj.put(Integer.valueOf(2051), "An error has occured");
        return;
    }

    public aj()
    {
        return;
    }

    public static String a(int p1)
    {
        return net.hockeyapp.android.aj.a(0, p1);
    }

    public static String a(net.hockeyapp.android.ai p2, int p3)
    {
        String v0_0 = 0;
        if (p2 != null) {
            v0_0 = p2.a(p3);
        }
        if (v0_0 == null) {
            v0_0 = ((String) net.hockeyapp.android.aj.aj.get(Integer.valueOf(p3)));
        }
        return v0_0;
    }

    public static void a(int p2, String p3)
    {
        if (p3 != null) {
            net.hockeyapp.android.aj.aj.put(Integer.valueOf(p2), p3);
        }
        return;
    }
}
