package net.hockeyapp.android.e;
public final class l {
    public static final String a = "UTF-8";
    private static final int c = 120000;
    public String b;
    private final String d;
    private String e;
    private net.hockeyapp.android.e.q f;
    private int g;
    private final java.util.Map h;

    public l(String p2)
    {
        this.g = 120000;
        this.d = p2;
        this.h = new java.util.HashMap();
        return;
    }

    private static String a(java.util.Map p5, String p6)
    {
        java.util.ArrayList v2_1 = new java.util.ArrayList();
        java.util.Iterator v3 = p5.keySet().iterator();
        while (v3.hasNext()) {
            String v0_5 = ((String) v3.next());
            v2_1.add(new StringBuilder().append(java.net.URLEncoder.encode(v0_5, p6)).append("=").append(java.net.URLEncoder.encode(((String) p5.get(v0_5)), p6)).toString());
        }
        return android.text.TextUtils.join("&", v2_1);
    }

    private net.hockeyapp.android.e.l a(int p3)
    {
        if (p3 >= 0) {
            this.g = p3;
            return this;
        } else {
            throw new IllegalArgumentException("Timeout has to be positive.");
        }
    }

    private net.hockeyapp.android.e.l a(String p1)
    {
        this.b = p1;
        return this;
    }

    private net.hockeyapp.android.e.l b(String p1)
    {
        this.e = p1;
        return this;
    }

    private net.hockeyapp.android.e.l b(String p4, String p5)
    {
        this.a("Authorization", new StringBuilder("Basic ").append(net.hockeyapp.android.e.b.a(new StringBuilder().append(p4).append(":").append(p5).toString().getBytes())).toString());
        return this;
    }

    public final java.net.HttpURLConnection a()
    {
        try {
            java.io.IOException v0_3 = ((java.net.HttpURLConnection) new java.net.URL(this.d).openConnection());
            v0_3.setConnectTimeout(this.g);
            v0_3.setReadTimeout(this.g);
        } catch (java.io.IOException v0_4) {
            throw new RuntimeException(v0_4);
        }
        if (android.os.Build$VERSION.SDK_INT <= 9) {
            v0_3.setRequestProperty("Connection", "close");
        }
        if (!android.text.TextUtils.isEmpty(this.b)) {
            v0_3.setRequestMethod(this.b);
            if ((!android.text.TextUtils.isEmpty(this.e)) || ((this.b.equalsIgnoreCase("POST")) || (this.b.equalsIgnoreCase("PUT")))) {
                v0_3.setDoOutput(1);
            }
        }
        java.io.OutputStreamWriter v3_0 = this.h.keySet().iterator();
        while (v3_0.hasNext()) {
            java.io.BufferedOutputStream v1_29 = ((String) v3_0.next());
            v0_3.setRequestProperty(v1_29, ((String) this.h.get(v1_29)));
        }
        if (!android.text.TextUtils.isEmpty(this.e)) {
            byte[] v2_5 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(v0_3.getOutputStream(), "UTF-8"));
            v2_5.write(this.e);
            v2_5.flush();
            v2_5.close();
        }
        if (this.f != null) {
            byte[] v2_6 = this.f;
            v2_6.b();
            v0_3.setRequestProperty("Content-Length", String.valueOf(((long) v2_6.a.toByteArray().length)));
            java.io.BufferedOutputStream v1_25 = new java.io.BufferedOutputStream(v0_3.getOutputStream());
            byte[] v2_13 = this.f;
            v2_13.b();
            v1_25.write(v2_13.a.toByteArray());
            v1_25.flush();
            v1_25.close();
        }
        return v0_3;
    }

    public final net.hockeyapp.android.e.l a(String p2, String p3)
    {
        this.h.put(p2, p3);
        return this;
    }

    public final net.hockeyapp.android.e.l a(java.util.Map p7)
    {
        try {
            java.util.ArrayList v3_1 = new java.util.ArrayList();
            java.util.Iterator v4 = p7.keySet().iterator();
        } catch (String v0_4) {
            throw new RuntimeException(v0_4);
        }
        while (v4.hasNext()) {
            String v0_6 = ((String) v4.next());
            v3_1.add(new StringBuilder().append(java.net.URLEncoder.encode(v0_6, "UTF-8")).append("=").append(java.net.URLEncoder.encode(((String) p7.get(v0_6)), "UTF-8")).toString());
        }
        String v0_3 = android.text.TextUtils.join("&", v3_1);
        this.a("Content-Type", "application/x-www-form-urlencoded");
        this.e = v0_3;
        return this;
    }

    public final net.hockeyapp.android.e.l a(java.util.Map p9, android.content.Context p10, java.util.List p11)
    {
        try {
            this.f = new net.hockeyapp.android.e.q();
            this.f.a();
            int v3_0 = p9.keySet().iterator();
        } catch (int v0_12) {
            throw new RuntimeException(v0_12);
        }
        while (v3_0.hasNext()) {
            int v0_14 = ((String) v3_0.next());
            java.io.InputStream v4_2 = this.f;
            String v1_10 = ((String) p9.get(v0_14));
            v4_2.a();
            v4_2.a.write(new StringBuilder("Content-Disposition: form-data; name=\"").append(v0_14).append("\"\r\n").toString().getBytes());
            v4_2.a.write("Content-Type: text/plain; charset=UTF-8\r\n".getBytes());
            v4_2.a.write("Content-Transfer-Encoding: 8bit\r\n\r\n".getBytes());
            v4_2.a.write(v1_10.getBytes());
            v4_2.a.write(new StringBuilder("\r\n--").append(v4_2.b).append("\r\n").toString().getBytes());
        }
        int v3_1 = 0;
        while (v3_1 < p11.size()) {
            String v1_6;
            int v0_9 = ((android.net.Uri) p11.get(v3_1));
            if (v3_1 != (p11.size() - 1)) {
                v1_6 = 0;
            } else {
                v1_6 = 1;
            }
            this.f.a(new StringBuilder("attachment").append(v3_1).toString(), v0_9.getLastPathSegment(), p10.getContentResolver().openInputStream(v0_9), v1_6);
            v3_1++;
        }
        this.f.b();
        this.a("Content-Type", new StringBuilder("multipart/form-data; boundary=").append(this.f.b).toString());
        return this;
    }
}
