package net.hockeyapp.android;
final class v extends android.os.AsyncTask {
    final synthetic android.graphics.Bitmap a;
    final synthetic android.content.Context b;

    v(android.graphics.Bitmap p1, android.content.Context p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private varargs Boolean a(java.io.File[] p6)
    {
        try {
            Boolean v0_1 = new java.io.FileOutputStream(p6[0]);
            this.a.compress(android.graphics.Bitmap$CompressFormat.JPEG, 100, v0_1);
            v0_1.close();
            Boolean v0_3 = Boolean.valueOf(1);
        } catch (Boolean v0_4) {
            android.util.Log.e("HockeyApp", "Could not save screenshot.", v0_4);
            v0_3 = Boolean.valueOf(0);
        }
        return v0_3;
    }

    private void a(Boolean p4)
    {
        if (!p4.booleanValue()) {
            android.widget.Toast.makeText(this.b, "Screenshot could not be created. Sorry.", 2000).show();
        }
        return;
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return this.a(((java.io.File[]) p2));
    }

    protected final synthetic void onPostExecute(Object p4)
    {
        if (!((Boolean) p4).booleanValue()) {
            android.widget.Toast.makeText(this.b, "Screenshot could not be created. Sorry.", 2000).show();
        }
        return;
    }
}
