package org.xbill.DNS;
public class DNSSEC$KeyMismatchException extends org.xbill.DNS.DNSSEC$DNSSECException {
    private org.xbill.DNS.KEYBase key;
    private org.xbill.DNS.SIGBase sig;

    DNSSEC$KeyMismatchException(org.xbill.DNS.KEYBase p3, org.xbill.DNS.SIGBase p4)
    {
        this(new StringBuffer("key ").append(p3.getName()).append("/").append(org.xbill.DNS.DNSSEC$Algorithm.string(p3.getAlgorithm())).append("/").append(p3.getFootprint()).append(" does not match signature ").append(p4.getSigner()).append("/").append(org.xbill.DNS.DNSSEC$Algorithm.string(p4.getAlgorithm())).append("/").append(p4.getFootprint()).toString());
        return;
    }
}
