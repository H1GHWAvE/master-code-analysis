package org.xbill.DNS;
public abstract class Record implements java.io.Serializable, java.lang.Cloneable, java.lang.Comparable {
    private static final java.text.DecimalFormat byteFormat = None;
    private static final long serialVersionUID = 2694906050116005466;
    protected int dclass;
    protected org.xbill.DNS.Name name;
    protected long ttl;
    protected int type;

    static Record()
    {
        java.text.DecimalFormat v0_1 = new java.text.DecimalFormat();
        org.xbill.DNS.Record.byteFormat = v0_1;
        v0_1.setMinimumIntegerDigits(3);
        return;
    }

    protected Record()
    {
        return;
    }

    Record(org.xbill.DNS.Name p3, int p4, int p5, long p6)
    {
        if (p3.isAbsolute()) {
            org.xbill.DNS.Type.check(p4);
            org.xbill.DNS.DClass.check(p5);
            org.xbill.DNS.TTL.check(p6);
            this.name = p3;
            this.type = p4;
            this.dclass = p5;
            this.ttl = p6;
            return;
        } else {
            throw new org.xbill.DNS.RelativeNameException(p3);
        }
    }

    protected static byte[] byteArrayFromString(String p13)
    {
        byte[] v8 = p13.getBytes();
        org.xbill.DNS.TextParseException v0_0 = 0;
        while (v0_0 < v8.length) {
            if (v8[v0_0] != 92) {
                v0_0++;
            } else {
                org.xbill.DNS.TextParseException v0_1 = 1;
            }
            org.xbill.DNS.TextParseException v0_5;
            if (v0_1 != null) {
                java.io.ByteArrayOutputStream v9_1 = new java.io.ByteArrayOutputStream();
                org.xbill.DNS.TextParseException v0_2 = 0;
                String v2_2 = 0;
                int v3_0 = 0;
                int v4_0 = 0;
                while (v0_2 < v8.length) {
                    byte v5_1 = v8[v0_2];
                    if (v4_0 == 0) {
                        if (v8[v0_2] != 92) {
                            v9_1.write(v8[v0_2]);
                        } else {
                            v2_2 = 0;
                            v3_0 = 0;
                            v4_0 = 1;
                        }
                    } else {
                        if ((v5_1 < 48) || ((v5_1 > 57) || (v3_0 >= 3))) {
                            if ((v3_0 <= 0) || (v3_0 >= 3)) {
                                int v4_1 = v3_0;
                                int v3_1 = v2_2;
                                String v2_3 = v5_1;
                            } else {
                                throw new org.xbill.DNS.TextParseException("bad escape");
                            }
                        } else {
                            int v6_2 = (v3_0 + 1);
                            v3_1 = ((v5_1 - 48) + (v2_2 * 10));
                            if (v3_1 <= 255) {
                                if (v6_2 < 3) {
                                    v2_2 = v3_1;
                                    v3_0 = v6_2;
                                    v0_2++;
                                } else {
                                    v2_3 = ((byte) v3_1);
                                    v4_1 = v6_2;
                                }
                            } else {
                                throw new org.xbill.DNS.TextParseException("bad escape");
                            }
                        }
                        v9_1.write(v2_3);
                        v2_2 = v3_1;
                        v3_0 = v4_1;
                        v4_0 = 0;
                    }
                }
                if ((v3_0 <= 0) || (v3_0 >= 3)) {
                    if (v9_1.toByteArray().length <= 255) {
                        v0_5 = v9_1.toByteArray();
                    } else {
                        throw new org.xbill.DNS.TextParseException("text string too long");
                    }
                } else {
                    throw new org.xbill.DNS.TextParseException("bad escape");
                }
            } else {
                if (v8.length <= 255) {
                    v0_5 = v8;
                } else {
                    throw new org.xbill.DNS.TextParseException("text string too long");
                }
            }
            return v0_5;
        }
        v0_1 = 0;
    }

    protected static String byteArrayToString(byte[] p8, boolean p9)
    {
        StringBuffer v1_1 = new StringBuffer();
        if (p9) {
            v1_1.append(34);
        }
        int v0_0 = 0;
        while (v0_0 < p8.length) {
            char v2_2 = (p8[v0_0] & 255);
            if ((v2_2 >= 32) && (v2_2 < 127)) {
                if ((v2_2 != 34) && (v2_2 != 92)) {
                    v1_1.append(((char) v2_2));
                } else {
                    v1_1.append(92);
                    v1_1.append(((char) v2_2));
                }
            } else {
                v1_1.append(92);
                v1_1.append(org.xbill.DNS.Record.byteFormat.format(((long) v2_2)));
            }
            v0_0++;
        }
        if (p9) {
            v1_1.append(34);
        }
        return v1_1.toString();
    }

    static byte[] checkByteArrayLength(String p3, byte[] p4, int p5)
    {
        if (p4.length <= 65535) {
            byte[] v0_2 = new byte[p4.length];
            System.arraycopy(p4, 0, v0_2, 0, p4.length);
            return v0_2;
        } else {
            throw new IllegalArgumentException(new StringBuffer("\"").append(p3).append("\" array must have no more than ").append(p5).append(" elements").toString());
        }
    }

    static org.xbill.DNS.Name checkName(String p1, org.xbill.DNS.Name p2)
    {
        if (p2.isAbsolute()) {
            return p2;
        } else {
            throw new org.xbill.DNS.RelativeNameException(p2);
        }
    }

    static int checkU16(String p3, int p4)
    {
        if ((p4 >= 0) && (p4 <= 65535)) {
            return p4;
        } else {
            throw new IllegalArgumentException(new StringBuffer("\"").append(p3).append("\" ").append(p4).append(" must be an unsigned 16 bit value").toString());
        }
    }

    static long checkU32(String p3, long p4)
    {
        if ((p4 >= 0) && (p4 <= 2.1219957905e-314)) {
            return p4;
        } else {
            throw new IllegalArgumentException(new StringBuffer("\"").append(p3).append("\" ").append(p4).append(" must be an unsigned 32 bit value").toString());
        }
    }

    static int checkU8(String p3, int p4)
    {
        if ((p4 >= 0) && (p4 <= 255)) {
            return p4;
        } else {
            throw new IllegalArgumentException(new StringBuffer("\"").append(p3).append("\" ").append(p4).append(" must be an unsigned 8 bit value").toString());
        }
    }

    public static org.xbill.DNS.Record fromString(org.xbill.DNS.Name p9, int p10, int p11, long p12, String p14, org.xbill.DNS.Name p15)
    {
        return org.xbill.DNS.Record.fromString(p9, p10, p11, p12, new org.xbill.DNS.Tokenizer(p14), p15);
    }

    public static org.xbill.DNS.Record fromString(org.xbill.DNS.Name p9, int p10, int p11, long p12, org.xbill.DNS.Tokenizer p14, org.xbill.DNS.Name p15)
    {
        if (p9.isAbsolute()) {
            org.xbill.DNS.Record v0_4;
            org.xbill.DNS.Type.check(p10);
            org.xbill.DNS.DClass.check(p11);
            org.xbill.DNS.TTL.check(p12);
            org.xbill.DNS.Record v0_1 = p14.get();
            if ((v0_1.type != 3) || (!v0_1.value.equals("\\#"))) {
                p14.unget();
                v0_4 = org.xbill.DNS.Record.getEmptyRecord(p9, p10, p11, p12, 1);
                v0_4.rdataFromString(p14, p15);
                org.xbill.DNS.Name v1_3 = p14.get();
                if ((v1_3.type != 1) && (v1_3.type != 0)) {
                    throw p14.exception("unexpected tokens at end of record");
                }
            } else {
                int v6_1 = p14.getUInt16();
                org.xbill.DNS.Record v0_7 = p14.getHex();
                if (v0_7 == null) {
                    v0_7 = new byte[0];
                }
                if (v6_1 == v0_7.length) {
                    v0_4 = org.xbill.DNS.Record.newRecord(p9, p10, p11, p12, v6_1, new org.xbill.DNS.DNSInput(v0_7));
                } else {
                    throw p14.exception("invalid unknown RR encoding: length mismatch");
                }
            }
            return v0_4;
        } else {
            throw new org.xbill.DNS.RelativeNameException(p9);
        }
    }

    static org.xbill.DNS.Record fromWire(org.xbill.DNS.DNSInput p1, int p2)
    {
        return org.xbill.DNS.Record.fromWire(p1, p2, 0);
    }

    static org.xbill.DNS.Record fromWire(org.xbill.DNS.DNSInput p8, int p9, boolean p10)
    {
        org.xbill.DNS.Record v0_2;
        org.xbill.DNS.Name v1_1 = new org.xbill.DNS.Name(p8);
        int v2 = p8.readU16();
        int v3 = p8.readU16();
        if (p9 != 0) {
            long v4 = p8.readU32();
            int v6 = p8.readU16();
            if ((v6 != 0) || ((!p10) || ((p9 != 1) && (p9 != 2)))) {
                v0_2 = org.xbill.DNS.Record.newRecord(v1_1, v2, v3, v4, v6, p8);
            } else {
                v0_2 = org.xbill.DNS.Record.newRecord(v1_1, v2, v3, v4);
            }
        } else {
            v0_2 = org.xbill.DNS.Record.newRecord(v1_1, v2, v3);
        }
        return v0_2;
    }

    public static org.xbill.DNS.Record fromWire(byte[] p2, int p3)
    {
        return org.xbill.DNS.Record.fromWire(new org.xbill.DNS.DNSInput(p2), p3, 0);
    }

    private static final org.xbill.DNS.Record getEmptyRecord(org.xbill.DNS.Name p1, int p2, int p3, long p4, boolean p6)
    {
        org.xbill.DNS.UNKRecord v0_1;
        if (!p6) {
            v0_1 = new org.xbill.DNS.EmptyRecord();
        } else {
            org.xbill.DNS.UNKRecord v0_2 = org.xbill.DNS.Type.getProto(p2);
            if (v0_2 == null) {
                v0_1 = new org.xbill.DNS.UNKRecord();
            } else {
                v0_1 = v0_2.getObject();
            }
        }
        v0_1.name = p1;
        v0_1.type = p2;
        v0_1.dclass = p3;
        v0_1.ttl = p4;
        return v0_1;
    }

    public static org.xbill.DNS.Record newRecord(org.xbill.DNS.Name p2, int p3, int p4)
    {
        return org.xbill.DNS.Record.newRecord(p2, p3, p4, 0);
    }

    public static org.xbill.DNS.Record newRecord(org.xbill.DNS.Name p7, int p8, int p9, long p10)
    {
        if (p7.isAbsolute()) {
            org.xbill.DNS.Type.check(p8);
            org.xbill.DNS.DClass.check(p9);
            org.xbill.DNS.TTL.check(p10);
            return org.xbill.DNS.Record.getEmptyRecord(p7, p8, p9, p10, 0);
        } else {
            throw new org.xbill.DNS.RelativeNameException(p7);
        }
    }

    private static org.xbill.DNS.Record newRecord(org.xbill.DNS.Name p7, int p8, int p9, long p10, int p12, org.xbill.DNS.DNSInput p13)
    {
        int v6;
        if (p13 == null) {
            v6 = 0;
        } else {
            v6 = 1;
        }
        org.xbill.DNS.WireParseException v0_0 = org.xbill.DNS.Record.getEmptyRecord(p7, p8, p9, p10, v6);
        if (p13 != null) {
            if (p13.remaining() >= p12) {
                p13.setActive(p12);
                v0_0.rrFromWire(p13);
                if (p13.remaining() <= 0) {
                    p13.clearActive();
                } else {
                    throw new org.xbill.DNS.WireParseException("invalid record length");
                }
            } else {
                throw new org.xbill.DNS.WireParseException("truncated record");
            }
        }
        return v0_0;
    }

    public static org.xbill.DNS.Record newRecord(org.xbill.DNS.Name p9, int p10, int p11, long p12, int p14, byte[] p15)
    {
        if (p9.isAbsolute()) {
            org.xbill.DNS.RelativeNameException v7_0;
            org.xbill.DNS.Type.check(p10);
            org.xbill.DNS.DClass.check(p11);
            org.xbill.DNS.TTL.check(p12);
            if (p15 == null) {
                v7_0 = 0;
            } else {
                v7_0 = new org.xbill.DNS.DNSInput(p15);
            }
            try {
                org.xbill.DNS.Record v0_1 = org.xbill.DNS.Record.newRecord(p9, p10, p11, p12, p14, v7_0);
            } catch (java.io.IOException v1) {
            }
            return v0_1;
        } else {
            throw new org.xbill.DNS.RelativeNameException(p9);
        }
    }

    public static org.xbill.DNS.Record newRecord(org.xbill.DNS.Name p9, int p10, int p11, long p12, byte[] p14)
    {
        return org.xbill.DNS.Record.newRecord(p9, p10, p11, p12, p14.length, p14);
    }

    private void toWireCanonical(org.xbill.DNS.DNSOutput p4, boolean p5)
    {
        this.name.toWireCanonical(p4);
        p4.writeU16(this.type);
        p4.writeU16(this.dclass);
        if (!p5) {
            p4.writeU32(this.ttl);
        } else {
            p4.writeU32(0);
        }
        int v0_5 = p4.current();
        p4.writeU16(0);
        this.rrToWire(p4, 0, 1);
        p4.writeU16At(((p4.current() - v0_5) - 2), v0_5);
        return;
    }

    private byte[] toWireCanonical(boolean p2)
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        this.toWireCanonical(v0_1, p2);
        return v0_1.toByteArray();
    }

    protected static String unknownToString(byte[] p2)
    {
        String v0_1 = new StringBuffer();
        v0_1.append("\\# ");
        v0_1.append(p2.length);
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.utils.base16.toString(p2));
        return v0_1.toString();
    }

    org.xbill.DNS.Record cloneRecord()
    {
        try {
            return ((org.xbill.DNS.Record) this.clone());
        } catch (IllegalStateException v0) {
            throw new IllegalStateException();
        }
    }

    public int compareTo(Object p6)
    {
        int v0_0 = 0;
        if (this != ((org.xbill.DNS.Record) p6)) {
            int v1_1 = this.name.compareTo(((org.xbill.DNS.Record) p6).name);
            if (v1_1 == 0) {
                int v1_3 = (this.dclass - ((org.xbill.DNS.Record) p6).dclass);
                if (v1_3 == 0) {
                    int v1_5 = (this.type - ((org.xbill.DNS.Record) p6).type);
                    if (v1_5 == 0) {
                        byte[] v2_3 = this.rdataToWireCanonical();
                        byte[] v3 = ((org.xbill.DNS.Record) p6).rdataToWireCanonical();
                        while ((v0_0 < v2_3.length) && (v0_0 < v3.length)) {
                            int v1_11 = ((v2_3[v0_0] & 255) - (v3[v0_0] & 255));
                            if (v1_11 == 0) {
                                v0_0++;
                            } else {
                                v0_0 = v1_11;
                            }
                        }
                        v0_0 = (v2_3.length - v3.length);
                    } else {
                        v0_0 = v1_5;
                    }
                } else {
                    v0_0 = v1_3;
                }
            } else {
                v0_0 = v1_1;
            }
        }
        return v0_0;
    }

    public boolean equals(Object p4)
    {
        boolean v0_0 = 0;
        if ((p4 != null) && (((p4 instanceof org.xbill.DNS.Record)) && ((this.type == ((org.xbill.DNS.Record) p4).type) && ((this.dclass == ((org.xbill.DNS.Record) p4).dclass) && (this.name.equals(((org.xbill.DNS.Record) p4).name)))))) {
            v0_0 = java.util.Arrays.equals(this.rdataToWireCanonical(), ((org.xbill.DNS.Record) p4).rdataToWireCanonical());
        }
        return v0_0;
    }

    public org.xbill.DNS.Name getAdditionalName()
    {
        return 0;
    }

    public int getDClass()
    {
        return this.dclass;
    }

    public org.xbill.DNS.Name getName()
    {
        return this.name;
    }

    abstract org.xbill.DNS.Record getObject();

    public int getRRsetType()
    {
        int v0_1;
        if (this.type != 46) {
            v0_1 = this.type;
        } else {
            v0_1 = ((org.xbill.DNS.RRSIGRecord) this).getTypeCovered();
        }
        return v0_1;
    }

    public long getTTL()
    {
        return this.ttl;
    }

    public int getType()
    {
        return this.type;
    }

    public int hashCode()
    {
        int v0 = 0;
        byte[] v2 = this.toWireCanonical(1);
        int v1_1 = 0;
        while (v0 < v2.length) {
            v1_1 += ((v1_1 << 3) + (v2[v0] & 255));
            v0++;
        }
        return v1_1;
    }

    abstract void rdataFromString();

    public String rdataToString()
    {
        return this.rrToString();
    }

    public byte[] rdataToWireCanonical()
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        this.rrToWire(v0_1, 0, 1);
        return v0_1.toByteArray();
    }

    abstract void rrFromWire();

    abstract String rrToString();

    abstract void rrToWire();

    public boolean sameRRset(org.xbill.DNS.Record p3)
    {
        if ((this.getRRsetType() != p3.getRRsetType()) || ((this.dclass != p3.dclass) || (!this.name.equals(p3.name)))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    void setTTL(long p2)
    {
        this.ttl = p2;
        return;
    }

    public String toString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.name);
        if (v0_1.length() < 8) {
            v0_1.append("\t");
        }
        if (v0_1.length() < 16) {
            v0_1.append("\t");
        }
        v0_1.append("\t");
        if (!org.xbill.DNS.Options.check("BINDTTL")) {
            v0_1.append(this.ttl);
        } else {
            v0_1.append(org.xbill.DNS.TTL.format(this.ttl));
        }
        v0_1.append("\t");
        if ((this.dclass != 1) || (!org.xbill.DNS.Options.check("noPrintIN"))) {
            v0_1.append(org.xbill.DNS.DClass.string(this.dclass));
            v0_1.append("\t");
        }
        v0_1.append(org.xbill.DNS.Type.string(this.type));
        String v1_18 = this.rrToString();
        if (!v1_18.equals("")) {
            v0_1.append("\t");
            v0_1.append(v1_18);
        }
        return v0_1.toString();
    }

    void toWire(org.xbill.DNS.DNSOutput p4, int p5, org.xbill.DNS.Compression p6)
    {
        this.name.toWire(p4, p6);
        p4.writeU16(this.type);
        p4.writeU16(this.dclass);
        if (p5 != 0) {
            p4.writeU32(this.ttl);
            int v0_4 = p4.current();
            p4.writeU16(0);
            this.rrToWire(p4, p6, 0);
            p4.writeU16At(((p4.current() - v0_4) - 2), v0_4);
        }
        return;
    }

    public byte[] toWire(int p3)
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        this.toWire(v0_1, p3, 0);
        return v0_1.toByteArray();
    }

    public byte[] toWireCanonical()
    {
        return this.toWireCanonical(0);
    }

    org.xbill.DNS.Record withDClass(int p3, long p4)
    {
        org.xbill.DNS.Record v0 = this.cloneRecord();
        v0.dclass = p3;
        v0.ttl = p4;
        return v0;
    }

    public org.xbill.DNS.Record withName(org.xbill.DNS.Name p2)
    {
        if (p2.isAbsolute()) {
            org.xbill.DNS.Record v0_1 = this.cloneRecord();
            v0_1.name = p2;
            return v0_1;
        } else {
            throw new org.xbill.DNS.RelativeNameException(p2);
        }
    }
}
