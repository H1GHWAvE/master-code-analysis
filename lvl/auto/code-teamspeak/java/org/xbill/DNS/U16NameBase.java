package org.xbill.DNS;
abstract class U16NameBase extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 10130859890597048621;
    protected org.xbill.DNS.Name nameField;
    protected int u16Field;

    protected U16NameBase()
    {
        return;
    }

    protected U16NameBase(org.xbill.DNS.Name p1, int p2, int p3, long p4)
    {
        this(p1, p2, p3, p4);
        return;
    }

    protected U16NameBase(org.xbill.DNS.Name p3, int p4, int p5, long p6, int p8, String p9, org.xbill.DNS.Name p10, String p11)
    {
        org.xbill.DNS.U16NameBase v2_1 = this(p3, p4, p5, p6);
        v2_1.u16Field = org.xbill.DNS.U16NameBase.checkU16(p9, p8);
        v2_1.nameField = org.xbill.DNS.U16NameBase.checkName(p11, p10);
        return;
    }

    protected org.xbill.DNS.Name getNameField()
    {
        return this.nameField;
    }

    protected int getU16Field()
    {
        return this.u16Field;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.u16Field = p2.getUInt16();
        this.nameField = p2.getName(p3);
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.u16Field = p2.readU16();
        this.nameField = new org.xbill.DNS.Name(p2);
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.u16Field);
        v0_1.append(" ");
        v0_1.append(this.nameField);
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p3, org.xbill.DNS.Compression p4, boolean p5)
    {
        p3.writeU16(this.u16Field);
        this.nameField.toWire(p3, 0, p5);
        return;
    }
}
