package org.xbill.DNS.utils;
public class base64 {
    private static final String Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

    private base64()
    {
        return;
    }

    public static String formatString(byte[] p5, int p6, String p7, boolean p8)
    {
        String v1 = org.xbill.DNS.utils.base64.toString(p5);
        StringBuffer v2_1 = new StringBuffer();
        int v0_0 = 0;
        while (v0_0 < v1.length()) {
            v2_1.append(p7);
            if ((v0_0 + p6) < v1.length()) {
                v2_1.append(v1.substring(v0_0, (v0_0 + p6)));
                v2_1.append("\n");
            } else {
                v2_1.append(v1.substring(v0_0));
                if (p8) {
                    v2_1.append(" )");
                }
            }
            v0_0 += p6;
        }
        return v2_1.toString();
    }

    public static byte[] fromString(String p14)
    {
        java.io.ByteArrayOutputStream v4_1 = new java.io.ByteArrayOutputStream();
        int v3_0 = p14.getBytes();
        int v0_0 = 0;
        while (v0_0 < v3_0.length) {
            if (!Character.isWhitespace(((char) v3_0[v0_0]))) {
                v4_1.write(v3_0[v0_0]);
            }
            v0_0++;
        }
        int v0_4;
        byte v5_1 = v4_1.toByteArray();
        if ((v5_1.length % 4) == 0) {
            v4_1.reset();
            java.io.DataOutputStream v6_1 = new java.io.DataOutputStream(v4_1);
            int v0_3 = 0;
            while (v0_3 < ((v5_1.length + 3) / 4)) {
                short v7_0 = new short[4];
                short[] v8 = new short[3];
                int v3_5 = 0;
                while (v3_5 < 4) {
                    v7_0[v3_5] = ((short) "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(v5_1[((v0_3 * 4) + v3_5)]));
                    v3_5++;
                }
                v8[0] = ((short) ((v7_0[0] << 2) + (v7_0[1] >> 4)));
                if (v7_0[2] != 64) {
                    if (v7_0[3] != 64) {
                        v8[1] = ((short) (((v7_0[1] << 4) + (v7_0[2] >> 2)) & 255));
                        v8[2] = ((short) (((v7_0[2] << 6) + v7_0[3]) & 255));
                    } else {
                        v8[1] = ((short) (((v7_0[1] << 4) + (v7_0[2] >> 2)) & 255));
                        v8[2] = -1;
                        if ((v7_0[2] & 3) != 0) {
                            v0_4 = 0;
                            return v0_4;
                        }
                    }
                } else {
                    v8[2] = -1;
                    v8[1] = -1;
                    if ((v7_0[1] & 15) != 0) {
                        v0_4 = 0;
                        return v0_4;
                    }
                }
                int v3_33 = 0;
                while (v3_33 < 3) {
                    try {
                        if (v8[v3_33] >= 0) {
                            v6_1.writeByte(v8[v3_33]);
                        }
                        v3_33++;
                    } catch (int v3) {
                        break;
                    }
                }
                v0_3++;
            }
            v0_4 = v4_1.toByteArray();
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    public static String toString(byte[] p12)
    {
        java.io.ByteArrayOutputStream v3_1 = new java.io.ByteArrayOutputStream();
        int v0_0 = 0;
        while (v0_0 < ((p12.length + 2) / 3)) {
            char v4_0 = new short[3];
            short[] v5 = new short[4];
            int v2_4 = 0;
            while (v2_4 < 3) {
                if (((v0_0 * 3) + v2_4) >= p12.length) {
                    v4_0[v2_4] = -1;
                } else {
                    v4_0[v2_4] = ((short) (p12[((v0_0 * 3) + v2_4)] & 255));
                }
                v2_4++;
            }
            v5[0] = ((short) (v4_0[0] >> 2));
            if (v4_0[1] != -1) {
                v5[1] = ((short) (((v4_0[0] & 3) << 4) + (v4_0[1] >> 4)));
            } else {
                v5[1] = ((short) ((v4_0[0] & 3) << 4));
            }
            if (v4_0[1] != -1) {
                if (v4_0[2] != -1) {
                    v5[2] = ((short) (((v4_0[1] & 15) << 2) + (v4_0[2] >> 6)));
                    v5[3] = ((short) (v4_0[2] & 63));
                } else {
                    v5[2] = ((short) ((v4_0[1] & 15) << 2));
                    v5[3] = 64;
                }
            } else {
                v5[3] = 64;
                v5[2] = 64;
            }
            int v2_34 = 0;
            while (v2_34 < 4) {
                v3_1.write("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(v5[v2_34]));
                v2_34++;
            }
            v0_0++;
        }
        return new String(v3_1.toByteArray());
    }
}
