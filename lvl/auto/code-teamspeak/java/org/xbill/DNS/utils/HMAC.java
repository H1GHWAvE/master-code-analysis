package org.xbill.DNS.utils;
public class HMAC {
    private static final byte IPAD = 0x36;
    private static final byte OPAD = 0x5c;
    private int blockLength;
    private java.security.MessageDigest digest;
    private byte[] ipad;
    private byte[] opad;

    public HMAC(String p4, int p5, byte[] p6)
    {
        try {
            this.digest = java.security.MessageDigest.getInstance(p4);
            this.blockLength = p5;
            this.init(p6);
            return;
        } catch (IllegalArgumentException v0) {
            throw new IllegalArgumentException(new StringBuffer("unknown digest algorithm ").append(p4).toString());
        }
    }

    public HMAC(String p2, byte[] p3)
    {
        this(p2, 64, p3);
        return;
    }

    public HMAC(java.security.MessageDigest p1, int p2, byte[] p3)
    {
        p1.reset();
        this.digest = p1;
        this.blockLength = p2;
        this.init(p3);
        return;
    }

    public HMAC(java.security.MessageDigest p2, byte[] p3)
    {
        this(p2, 64, p3);
        return;
    }

    private void init(byte[] p4)
    {
        if (p4.length > this.blockLength) {
            p4 = this.digest.digest(p4);
            this.digest.reset();
        }
        java.security.MessageDigest v0_4 = new byte[this.blockLength];
        this.ipad = v0_4;
        java.security.MessageDigest v0_6 = new byte[this.blockLength];
        this.opad = v0_6;
        java.security.MessageDigest v0_7 = 0;
        while (v0_7 < p4.length) {
            this.ipad[v0_7] = ((byte) (p4[v0_7] ^ 54));
            this.opad[v0_7] = ((byte) (p4[v0_7] ^ 92));
            v0_7++;
        }
        while (v0_7 < this.blockLength) {
            this.ipad[v0_7] = 54;
            this.opad[v0_7] = 92;
            v0_7++;
        }
        this.digest.update(this.ipad);
        return;
    }

    public void clear()
    {
        this.digest.reset();
        this.digest.update(this.ipad);
        return;
    }

    public int digestLength()
    {
        return this.digest.getDigestLength();
    }

    public byte[] sign()
    {
        byte[] v0_1 = this.digest.digest();
        this.digest.reset();
        this.digest.update(this.opad);
        return this.digest.digest(v0_1);
    }

    public void update(byte[] p2)
    {
        this.digest.update(p2);
        return;
    }

    public void update(byte[] p2, int p3, int p4)
    {
        this.digest.update(p2, p3, p4);
        return;
    }

    public boolean verify(byte[] p2)
    {
        return this.verify(p2, 0);
    }

    public boolean verify(byte[] p5, boolean p6)
    {
        boolean v0_1;
        byte[] v1 = this.sign();
        if ((!p6) || (p5.length >= v1.length)) {
            v0_1 = v1;
        } else {
            v0_1 = new byte[p5.length];
            System.arraycopy(v1, 0, v0_1, 0, v0_1.length);
        }
        return java.util.Arrays.equals(p5, v0_1);
    }
}
