package org.xbill.DNS;
public class A6Record extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 9631717186372204827;
    private org.xbill.DNS.Name prefix;
    private int prefixBits;
    private java.net.InetAddress suffix;

    A6Record()
    {
        return;
    }

    public A6Record(org.xbill.DNS.Name p8, int p9, long p10, int p12, java.net.InetAddress p13, org.xbill.DNS.Name p14)
    {
        this(p8, 38, p9, p10);
        this.prefixBits = org.xbill.DNS.A6Record.checkU8("prefixBits", p12);
        if ((p13 == null) || (org.xbill.DNS.Address.familyOf(p13) == 2)) {
            this.suffix = p13;
            if (p14 != null) {
                this.prefix = org.xbill.DNS.A6Record.checkName("prefix", p14);
            }
            return;
        } else {
            throw new IllegalArgumentException("invalid IPv6 address");
        }
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.A6Record();
    }

    public org.xbill.DNS.Name getPrefix()
    {
        return this.prefix;
    }

    public int getPrefixBits()
    {
        return this.prefixBits;
    }

    public java.net.InetAddress getSuffix()
    {
        return this.suffix;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p4, org.xbill.DNS.Name p5)
    {
        this.prefixBits = p4.getUInt8();
        if (this.prefixBits <= 128) {
            if (this.prefixBits < 128) {
                org.xbill.DNS.TextParseException v0_3 = p4.getString();
                try {
                    this.suffix = org.xbill.DNS.Address.getByAddress(v0_3, 2);
                } catch (StringBuffer v1) {
                    throw p4.exception(new StringBuffer("invalid IPv6 address: ").append(v0_3).toString());
                }
            }
            if (this.prefixBits > 0) {
                this.prefix = p4.getName(p5);
            }
            return;
        } else {
            throw p4.exception("prefix bits must be [0..128]");
        }
    }

    void rrFromWire(org.xbill.DNS.DNSInput p4)
    {
        this.prefixBits = p4.readU8();
        org.xbill.DNS.Name v0_4 = (((128 - this.prefixBits) + 7) / 8);
        if (this.prefixBits < 128) {
            byte[] v1_2 = new byte[16];
            p4.readByteArray(v1_2, (16 - v0_4), v0_4);
            this.suffix = java.net.InetAddress.getByAddress(v1_2);
        }
        if (this.prefixBits > 0) {
            this.prefix = new org.xbill.DNS.Name(p4);
        }
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.prefixBits);
        if (this.suffix != null) {
            v0_1.append(" ");
            v0_1.append(this.suffix.getHostAddress());
        }
        if (this.prefix != null) {
            v0_1.append(" ");
            v0_1.append(this.prefix);
        }
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p4, org.xbill.DNS.Compression p5, boolean p6)
    {
        p4.writeU8(this.prefixBits);
        if (this.suffix != null) {
            org.xbill.DNS.Name v0_5 = (((128 - this.prefixBits) + 7) / 8);
            p4.writeByteArray(this.suffix.getAddress(), (16 - v0_5), v0_5);
        }
        if (this.prefix != null) {
            this.prefix.toWire(p4, 0, p6);
        }
        return;
    }
}
