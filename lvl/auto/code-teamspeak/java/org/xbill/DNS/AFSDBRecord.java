package org.xbill.DNS;
public class AFSDBRecord extends org.xbill.DNS.U16NameBase {
    private static final long serialVersionUID = 3034379930729102437;

    AFSDBRecord()
    {
        return;
    }

    public AFSDBRecord(org.xbill.DNS.Name p12, int p13, long p14, int p16, org.xbill.DNS.Name p17)
    {
        this(p12, 18, p13, p14, p16, "subtype", p17, "host");
        return;
    }

    public org.xbill.DNS.Name getHost()
    {
        return this.getNameField();
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.AFSDBRecord();
    }

    public int getSubtype()
    {
        return this.getU16Field();
    }
}
