package org.xbill.DNS;
final class UDPClient extends org.xbill.DNS.Client {
    private static final int EPHEMERAL_RANGE = 64511;
    private static final int EPHEMERAL_START = 1024;
    private static final int EPHEMERAL_STOP = 65535;
    private static java.security.SecureRandom prng;
    private static volatile boolean prng_initializing;
    private boolean bound;

    static UDPClient()
    {
        org.xbill.DNS.UDPClient.prng = new java.security.SecureRandom();
        org.xbill.DNS.UDPClient.prng_initializing = 1;
        new Thread(new org.xbill.DNS.UDPClient$1()).start();
        return;
    }

    public UDPClient(long p2)
    {
        this(java.nio.channels.DatagramChannel.open(), p2);
        this.bound = 0;
        return;
    }

    static java.security.SecureRandom access$000()
    {
        return org.xbill.DNS.UDPClient.prng;
    }

    static boolean access$102(boolean p0)
    {
        org.xbill.DNS.UDPClient.prng_initializing = p0;
        return p0;
    }

    private void bind_random(java.net.InetSocketAddress p6)
    {
        if (!org.xbill.DNS.UDPClient.prng_initializing) {
            boolean v0_5 = ((java.nio.channels.DatagramChannel) this.key.channel());
            int v2 = 0;
            while (v2 < 1024) {
                int v1_5;
                java.net.DatagramSocket v3_1 = (org.xbill.DNS.UDPClient.prng.nextInt(64511) + 1024);
                if (p6 == null) {
                    v1_5 = new java.net.InetSocketAddress(v3_1);
                } else {
                    v1_5 = new java.net.InetSocketAddress(p6.getAddress(), v3_1);
                }
                v0_5.socket().bind(v1_5);
                this.bound = 1;
                break;
            }
        } else {
            try {
                Thread.sleep(2);
            } catch (boolean v0) {
            }
            if (!org.xbill.DNS.UDPClient.prng_initializing) {
            }
        }
        return;
    }

    static byte[] sendrecv(java.net.SocketAddress p2, java.net.SocketAddress p3, byte[] p4, int p5, long p6)
    {
        org.xbill.DNS.UDPClient v1_1 = new org.xbill.DNS.UDPClient(p6);
        try {
            v1_1.bind(p2);
            v1_1.connect(p3);
            v1_1.send(p4);
            Throwable v0_0 = v1_1.recv(p5);
            v1_1.cleanup();
            return v0_0;
        } catch (Throwable v0_1) {
            v1_1.cleanup();
            throw v0_1;
        }
    }

    static byte[] sendrecv(java.net.SocketAddress p7, byte[] p8, int p9, long p10)
    {
        return org.xbill.DNS.UDPClient.sendrecv(0, p7, p8, p9, p10);
    }

    final void bind(java.net.SocketAddress p2)
    {
        if ((p2 != null) && ((!(p2 instanceof java.net.InetSocketAddress)) || (((java.net.InetSocketAddress) p2).getPort() != 0))) {
            if (p2 != null) {
                ((java.nio.channels.DatagramChannel) this.key.channel()).socket().bind(p2);
                this.bound = 1;
            }
        } else {
            this.bind_random(((java.net.InetSocketAddress) p2));
            if (!this.bound) {
            }
        }
        return;
    }

    final void connect(java.net.SocketAddress p2)
    {
        if (!this.bound) {
            this.bind(0);
        }
        ((java.nio.channels.DatagramChannel) this.key.channel()).connect(p2);
        return;
    }

    final byte[] recv(int p8)
    {
        java.net.SocketAddress v0_2 = ((java.nio.channels.DatagramChannel) this.key.channel());
        String v1_0 = new byte[p8];
        this.key.interestOps(1);
        try {
            while (!this.key.isReadable()) {
                org.xbill.DNS.UDPClient.blockUntil(this.key, this.endTime);
            }
        } catch (java.net.SocketAddress v0_3) {
            if (this.key.isValid()) {
                this.key.interestOps(0);
            }
            throw v0_3;
        }
        if (this.key.isValid()) {
            this.key.interestOps(0);
        }
        java.net.SocketAddress v2_8 = ((long) v0_2.read(java.nio.ByteBuffer.wrap(v1_0)));
        if (v2_8 > 0) {
            java.net.SocketAddress v2_9 = ((int) v2_8);
            byte[] v3_1 = new byte[v2_9];
            System.arraycopy(v1_0, 0, v3_1, 0, v2_9);
            org.xbill.DNS.UDPClient.verboseLog("UDP read", v0_2.socket().getLocalSocketAddress(), v0_2.socket().getRemoteSocketAddress(), v3_1);
            return v3_1;
        } else {
            throw new java.io.EOFException();
        }
    }

    final void send(byte[] p5)
    {
        java.nio.channels.DatagramChannel v0_2 = ((java.nio.channels.DatagramChannel) this.key.channel());
        org.xbill.DNS.UDPClient.verboseLog("UDP write", v0_2.socket().getLocalSocketAddress(), v0_2.socket().getRemoteSocketAddress(), p5);
        v0_2.write(java.nio.ByteBuffer.wrap(p5));
        return;
    }
}
