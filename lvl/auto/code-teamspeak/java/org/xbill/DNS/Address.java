package org.xbill.DNS;
public final class Address {
    public static final int IPv4 = 1;
    public static final int IPv6 = 2;

    private Address()
    {
        return;
    }

    private static java.net.InetAddress addrFromRecord(String p1, org.xbill.DNS.Record p2)
    {
        java.net.InetAddress v0_1;
        if (!(p2 instanceof org.xbill.DNS.ARecord)) {
            v0_1 = ((org.xbill.DNS.AAAARecord) p2).getAddress();
        } else {
            v0_1 = ((org.xbill.DNS.ARecord) p2).getAddress();
        }
        return java.net.InetAddress.getByAddress(p1, v0_1.getAddress());
    }

    public static int addressLength(int p2)
    {
        IllegalArgumentException v0_4;
        if (p2 != 1) {
            if (p2 != 2) {
                throw new IllegalArgumentException("unknown address family");
            } else {
                v0_4 = 16;
            }
        } else {
            v0_4 = 4;
        }
        return v0_4;
    }

    public static int familyOf(java.net.InetAddress p2)
    {
        IllegalArgumentException v0_4;
        if (!(p2 instanceof java.net.Inet4Address)) {
            if (!(p2 instanceof java.net.Inet6Address)) {
                throw new IllegalArgumentException("unknown address family");
            } else {
                v0_4 = 2;
            }
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public static java.net.InetAddress[] getAllByName(String p5)
    {
        try {
            java.net.InetAddress[] v1_1 = new java.net.InetAddress[1];
            v1_1[0] = org.xbill.DNS.Address.getByAddress(p5);
            java.net.InetAddress[] v0 = v1_1;
        } catch (java.net.InetAddress[] v1) {
            org.xbill.DNS.Record[] v2_1 = org.xbill.DNS.Address.lookupHostName(p5, 1);
            java.net.InetAddress[] v1_3 = new java.net.InetAddress[v2_1.length];
        }
        return v0;
    }

    public static java.net.InetAddress getByAddress(String p3)
    {
        java.net.UnknownHostException v0_6;
        java.net.UnknownHostException v0_1 = org.xbill.DNS.Address.toByteArray(p3, 1);
        if (v0_1 == null) {
            java.net.UnknownHostException v0_3 = org.xbill.DNS.Address.toByteArray(p3, 2);
            if (v0_3 == null) {
                throw new java.net.UnknownHostException(new StringBuffer("Invalid address: ").append(p3).toString());
            } else {
                v0_6 = java.net.InetAddress.getByAddress(p3, v0_3);
            }
        } else {
            v0_6 = java.net.InetAddress.getByAddress(p3, v0_1);
        }
        return v0_6;
    }

    public static java.net.InetAddress getByAddress(String p3, int p4)
    {
        if ((p4 == 1) || (p4 == 2)) {
            java.net.UnknownHostException v0_2 = org.xbill.DNS.Address.toByteArray(p3, p4);
            if (v0_2 == null) {
                throw new java.net.UnknownHostException(new StringBuffer("Invalid address: ").append(p3).toString());
            } else {
                return java.net.InetAddress.getByAddress(p3, v0_2);
            }
        } else {
            throw new IllegalArgumentException("unknown address family");
        }
    }

    public static java.net.InetAddress getByName(String p2)
    {
        try {
            java.net.InetAddress v0_0 = org.xbill.DNS.Address.getByAddress(p2);
        } catch (java.net.InetAddress v0) {
            v0_0 = org.xbill.DNS.Address.addrFromRecord(p2, org.xbill.DNS.Address.lookupHostName(p2, 0)[0]);
        }
        return v0_0;
    }

    public static String getHostName(java.net.InetAddress p3)
    {
        String v0_1 = new org.xbill.DNS.Lookup(org.xbill.DNS.ReverseMap.fromAddress(p3), 12).run();
        if (v0_1 != null) {
            return ((org.xbill.DNS.PTRRecord) v0_1[0]).getTarget().toString();
        } else {
            throw new java.net.UnknownHostException("unknown address");
        }
    }

    public static boolean isDottedQuad(String p2)
    {
        int v0 = 1;
        if (org.xbill.DNS.Address.toByteArray(p2, 1) == null) {
            v0 = 0;
        }
        return v0;
    }

    private static org.xbill.DNS.Record[] lookupHostName(String p6, boolean p7)
    {
        try {
            org.xbill.DNS.Record[] v1_1 = new org.xbill.DNS.Lookup(p6, 1);
            org.xbill.DNS.Record[] v0_1 = v1_1.run();
        } catch (org.xbill.DNS.Record[] v0) {
            throw new java.net.UnknownHostException("invalid name");
        }
        if (v0_1 != null) {
            if (p7) {
                org.xbill.DNS.Record[] v2_1 = new org.xbill.DNS.Lookup(p6, 28).run();
                if (v2_1 != null) {
                    org.xbill.DNS.Record[] v1_6 = new org.xbill.DNS.Record[(v0_1.length + v2_1.length)];
                    System.arraycopy(v0_1, 0, v1_6, 0, v0_1.length);
                    System.arraycopy(v2_1, 0, v1_6, v0_1.length, v2_1.length);
                    v0_1 = v1_6;
                }
            }
        } else {
            if (v1_1.getResult() == 4) {
                v0_1 = new org.xbill.DNS.Lookup(p6, 28).run();
                if (v0_1 != null) {
                    return v0_1;
                }
            }
            throw new java.net.UnknownHostException("unknown host");
        }
        return v0_1;
    }

    private static byte[] parseV4(String p11)
    {
        byte[] v5 = new byte[4];
        int v8 = p11.length();
        int v6 = 0;
        int v7_0 = 0;
        int v2 = 0;
        int v3_0 = 0;
        while (v6 < v8) {
            int v0_5;
            int v0_3 = p11.charAt(v6);
            if ((v0_3 < 48) || (v0_3 > 57)) {
                if (v0_3 != 46) {
                    int v0_2 = 0;
                    return v0_2;
                } else {
                    if (v2 != 3) {
                        if (v3_0 != 0) {
                            int v0_4 = (v2 + 1);
                            v5[v2] = ((byte) v7_0);
                            v2 = v0_4;
                            v3_0 = 0;
                            v0_5 = 0;
                        } else {
                            v0_2 = 0;
                            return v0_2;
                        }
                    } else {
                        v0_2 = 0;
                        return v0_2;
                    }
                }
            } else {
                if (v3_0 != 3) {
                    if ((v3_0 <= 0) || (v7_0 != 0)) {
                        v3_0++;
                        v0_5 = ((v0_3 - 48) + (v7_0 * 10));
                        if (v0_5 > 255) {
                            v0_2 = 0;
                            return v0_2;
                        }
                    } else {
                        v0_2 = 0;
                        return v0_2;
                    }
                } else {
                    v0_2 = 0;
                    return v0_2;
                }
            }
            v6++;
            v7_0 = v0_5;
        }
        if (v2 == 3) {
            if (v3_0 != 0) {
                v5[v2] = ((byte) v7_0);
                v0_2 = v5;
            } else {
                v0_2 = 0;
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private static byte[] parseV6(String p13)
    {
        int v0_8;
        int v0_4;
        int v3 = -1;
        byte[] v5 = new byte[16];
        String[] v9 = p13.split(":", -1);
        int v6_0 = (v9.length - 1);
        if (v9[0].length() != 0) {
            v0_4 = 0;
            byte v8_0;
            if (v9[v6_0].length() != 0) {
                v8_0 = v6_0;
            } else {
                if (((v6_0 - v0_4) <= 0) || (v9[(v6_0 - 1)].length() != 0)) {
                    v0_8 = 0;
                    return v0_8;
                } else {
                    v8_0 = (v6_0 - 1);
                }
            }
            if (((v8_0 - v0_4) + 1) <= 8) {
                byte[] v7_7 = v0_4;
                int v0_9 = 0;
                while (v7_7 <= v8_0) {
                    if (v9[v7_7].length() != 0) {
                        if (v9[v7_7].indexOf(46) < 0) {
                            int v6_8 = 0;
                            try {
                                while (v6_8 < v9[v7_7].length()) {
                                    if (Character.digit(v9[v7_7].charAt(v6_8), 16) >= 0) {
                                        v6_8++;
                                    } else {
                                        v0_8 = 0;
                                        return v0_8;
                                    }
                                }
                            } catch (int v0) {
                                v0_8 = 0;
                                return v0_8;
                            }
                            int v6_10 = Integer.parseInt(v9[v7_7], 16);
                            if ((v6_10 <= 65535) && (v6_10 >= 0)) {
                                int v10_5 = (v0_9 + 1);
                                v5[v0_9] = ((byte) (v6_10 >> 8));
                                v0_9 = (v10_5 + 1);
                                v5[v10_5] = ((byte) (v6_10 & 255));
                            } else {
                                v0_8 = 0;
                                return v0_8;
                            }
                        } else {
                            if (v7_7 >= v8_0) {
                                if (v7_7 <= 6) {
                                    byte[] v7_8 = org.xbill.DNS.Address.toByteArray(v9[v7_7], 1);
                                    if (v7_8 != null) {
                                        int v6_15 = 0;
                                        while (v6_15 < 4) {
                                            int v1_3 = (v0_9 + 1);
                                            v5[v0_9] = v7_8[v6_15];
                                            v6_15++;
                                            v0_9 = v1_3;
                                        }
                                    } else {
                                        v0_8 = 0;
                                        return v0_8;
                                    }
                                } else {
                                    v0_8 = 0;
                                    return v0_8;
                                }
                            } else {
                                v0_8 = 0;
                                return v0_8;
                            }
                        }
                    } else {
                        if (v3 < 0) {
                            v3 = v0_9;
                        } else {
                            v0_8 = 0;
                            return v0_8;
                        }
                    }
                    v7_7++;
                }
                if ((v0_9 >= 16) || (v3 >= 0)) {
                    if (v3 >= 0) {
                        int v1_2 = (16 - v0_9);
                        System.arraycopy(v5, v3, v5, (v3 + v1_2), (v0_9 - v3));
                        int v0_11 = v3;
                        while (v0_11 < (v3 + v1_2)) {
                            v5[v0_11] = 0;
                            v0_11++;
                        }
                    }
                    v0_8 = v5;
                    return v0_8;
                } else {
                    v0_8 = 0;
                    return v0_8;
                }
            } else {
                v0_8 = 0;
            }
        } else {
            if (((v6_0 + 0) <= 0) || (v9[1].length() != 0)) {
                v0_8 = 0;
            } else {
                v0_4 = 1;
            }
        }
        return v0_8;
    }

    public static int[] toArray(String p1)
    {
        return org.xbill.DNS.Address.toArray(p1, 1);
    }

    public static int[] toArray(String p4, int p5)
    {
        int[] v0_2;
        byte[] v2 = org.xbill.DNS.Address.toByteArray(p4, p5);
        if (v2 != null) {
            int[] v1 = new int[v2.length];
            int[] v0_1 = 0;
            while (v0_1 < v2.length) {
                v1[v0_1] = (v2[v0_1] & 255);
                v0_1++;
            }
            v0_2 = v1;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public static byte[] toByteArray(String p2, int p3)
    {
        IllegalArgumentException v0_4;
        if (p3 != 1) {
            if (p3 != 2) {
                throw new IllegalArgumentException("unknown address family");
            } else {
                v0_4 = org.xbill.DNS.Address.parseV6(p2);
            }
        } else {
            v0_4 = org.xbill.DNS.Address.parseV4(p2);
        }
        return v0_4;
    }

    public static String toDottedQuad(byte[] p2)
    {
        return new StringBuffer().append((p2[0] & 255)).append(".").append((p2[1] & 255)).append(".").append((p2[2] & 255)).append(".").append((p2[3] & 255)).toString();
    }

    public static String toDottedQuad(int[] p2)
    {
        return new StringBuffer().append(p2[0]).append(".").append(p2[1]).append(".").append(p2[2]).append(".").append(p2[3]).toString();
    }

    public static java.net.InetAddress truncate(java.net.InetAddress p6, int p7)
    {
        String v1_0 = 0;
        IllegalArgumentException v0_2 = (org.xbill.DNS.Address.addressLength(org.xbill.DNS.Address.familyOf(p6)) * 8);
        if ((p7 >= 0) && (p7 <= v0_2)) {
            if (p7 != v0_2) {
                byte[] v2 = p6.getAddress();
                IllegalArgumentException v0_4 = ((p7 / 8) + 1);
                while (v0_4 < v2.length) {
                    v2[v0_4] = 0;
                    v0_4++;
                }
                IllegalArgumentException v0_5 = 0;
                while (v0_5 < (p7 % 8)) {
                    v1_0 |= (1 << (7 - v0_5));
                    v0_5++;
                }
                IllegalArgumentException v0_6 = (p7 / 8);
                v2[v0_6] = ((byte) (v1_0 & v2[v0_6]));
                try {
                    p6 = java.net.InetAddress.getByAddress(v2);
                } catch (IllegalArgumentException v0) {
                    throw new IllegalArgumentException("invalid address");
                }
            }
            return p6;
        } else {
            throw new IllegalArgumentException("invalid mask length");
        }
    }
}
