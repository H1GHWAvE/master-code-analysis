package org.xbill.DNS;
public class PTRRecord extends org.xbill.DNS.SingleCompressedNameBase {
    private static final long serialVersionUID = 10125107463284117424;

    PTRRecord()
    {
        return;
    }

    public PTRRecord(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Name p14)
    {
        this(p10, 12, p11, p12, p14, "target");
        return;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.PTRRecord();
    }

    public org.xbill.DNS.Name getTarget()
    {
        return this.getSingleName();
    }
}
