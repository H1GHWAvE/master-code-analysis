package org.xbill.DNS;
public final class Opcode {
    public static final int IQUERY = 1;
    public static final int NOTIFY = 4;
    public static final int QUERY = 0;
    public static final int STATUS = 2;
    public static final int UPDATE = 5;
    private static org.xbill.DNS.Mnemonic opcodes;

    static Opcode()
    {
        org.xbill.DNS.Mnemonic v0_1 = new org.xbill.DNS.Mnemonic("DNS Opcode", 2);
        org.xbill.DNS.Opcode.opcodes = v0_1;
        v0_1.setMaximum(15);
        org.xbill.DNS.Opcode.opcodes.setPrefix("RESERVED");
        org.xbill.DNS.Opcode.opcodes.setNumericAllowed(1);
        org.xbill.DNS.Opcode.opcodes.add(0, "QUERY");
        org.xbill.DNS.Opcode.opcodes.add(1, "IQUERY");
        org.xbill.DNS.Opcode.opcodes.add(2, "STATUS");
        org.xbill.DNS.Opcode.opcodes.add(4, "NOTIFY");
        org.xbill.DNS.Opcode.opcodes.add(5, "UPDATE");
        return;
    }

    private Opcode()
    {
        return;
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.Opcode.opcodes.getText(p1);
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.Opcode.opcodes.getValue(p1);
    }
}
