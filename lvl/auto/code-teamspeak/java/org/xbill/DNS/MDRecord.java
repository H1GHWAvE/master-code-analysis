package org.xbill.DNS;
public class MDRecord extends org.xbill.DNS.SingleNameBase {
    private static final long serialVersionUID = 5268878603762942202;

    MDRecord()
    {
        return;
    }

    public MDRecord(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Name p14)
    {
        this(p10, 3, p11, p12, p14, "mail agent");
        return;
    }

    public org.xbill.DNS.Name getAdditionalName()
    {
        return this.getSingleName();
    }

    public org.xbill.DNS.Name getMailAgent()
    {
        return this.getSingleName();
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.MDRecord();
    }
}
