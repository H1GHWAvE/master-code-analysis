package org.xbill.DNS;
public class ExtendedResolver implements org.xbill.DNS.Resolver {
    private static final int quantum = 5;
    private int lbStart;
    private boolean loadBalance;
    private java.util.List resolvers;
    private int retries;

    public ExtendedResolver()
    {
        int v0_0 = 0;
        this.loadBalance = 0;
        this.lbStart = 0;
        this.retries = 3;
        this.init();
        org.xbill.DNS.SimpleResolver v1_2 = org.xbill.DNS.ResolverConfig.getCurrentConfig().servers();
        if (v1_2 == null) {
            this.resolvers.add(new org.xbill.DNS.SimpleResolver());
        } else {
            while (v0_0 < v1_2.length) {
                org.xbill.DNS.SimpleResolver v2_2 = new org.xbill.DNS.SimpleResolver(v1_2[v0_0]);
                v2_2.setTimeout(5);
                this.resolvers.add(v2_2);
                v0_0++;
            }
        }
        return;
    }

    public ExtendedResolver(String[] p4)
    {
        int v0 = 0;
        this.loadBalance = 0;
        this.lbStart = 0;
        this.retries = 3;
        this.init();
        while (v0 < p4.length) {
            org.xbill.DNS.SimpleResolver v1_3 = new org.xbill.DNS.SimpleResolver(p4[v0]);
            v1_3.setTimeout(5);
            this.resolvers.add(v1_3);
            v0++;
        }
        return;
    }

    public ExtendedResolver(org.xbill.DNS.Resolver[] p4)
    {
        int v0 = 0;
        this.loadBalance = 0;
        this.lbStart = 0;
        this.retries = 3;
        this.init();
        while (v0 < p4.length) {
            this.resolvers.add(p4[v0]);
            v0++;
        }
        return;
    }

    static java.util.List access$000(org.xbill.DNS.ExtendedResolver p1)
    {
        return p1.resolvers;
    }

    static boolean access$100(org.xbill.DNS.ExtendedResolver p1)
    {
        return p1.loadBalance;
    }

    static int access$200(org.xbill.DNS.ExtendedResolver p1)
    {
        return p1.lbStart;
    }

    static int access$208(org.xbill.DNS.ExtendedResolver p2)
    {
        int v0 = p2.lbStart;
        p2.lbStart = (v0 + 1);
        return v0;
    }

    static int access$244(org.xbill.DNS.ExtendedResolver p1, int p2)
    {
        int v0_1 = (p1.lbStart % p2);
        p1.lbStart = v0_1;
        return v0_1;
    }

    static int access$300(org.xbill.DNS.ExtendedResolver p1)
    {
        return p1.retries;
    }

    private void init()
    {
        this.resolvers = new java.util.ArrayList();
        return;
    }

    public void addResolver(org.xbill.DNS.Resolver p2)
    {
        this.resolvers.add(p2);
        return;
    }

    public void deleteResolver(org.xbill.DNS.Resolver p2)
    {
        this.resolvers.remove(p2);
        return;
    }

    public org.xbill.DNS.Resolver getResolver(int p2)
    {
        int v0_2;
        if (p2 >= this.resolvers.size()) {
            v0_2 = 0;
        } else {
            v0_2 = ((org.xbill.DNS.Resolver) this.resolvers.get(p2));
        }
        return v0_2;
    }

    public org.xbill.DNS.Resolver[] getResolvers()
    {
        org.xbill.DNS.Resolver[] v1_2 = new org.xbill.DNS.Resolver[this.resolvers.size()];
        return ((org.xbill.DNS.Resolver[]) ((org.xbill.DNS.Resolver[]) this.resolvers.toArray(v1_2)));
    }

    public org.xbill.DNS.Message send(org.xbill.DNS.Message p2)
    {
        return new org.xbill.DNS.ExtendedResolver$Resolution(this, p2).start();
    }

    public Object sendAsync(org.xbill.DNS.Message p2, org.xbill.DNS.ResolverListener p3)
    {
        org.xbill.DNS.ExtendedResolver$Resolution v0_1 = new org.xbill.DNS.ExtendedResolver$Resolution(this, p2);
        v0_1.startAsync(p3);
        return v0_1;
    }

    public void setEDNS(int p3)
    {
        int v1 = 0;
        while (v1 < this.resolvers.size()) {
            ((org.xbill.DNS.Resolver) this.resolvers.get(v1)).setEDNS(p3);
            v1++;
        }
        return;
    }

    public void setEDNS(int p3, int p4, int p5, java.util.List p6)
    {
        int v1 = 0;
        while (v1 < this.resolvers.size()) {
            ((org.xbill.DNS.Resolver) this.resolvers.get(v1)).setEDNS(p3, p4, p5, p6);
            v1++;
        }
        return;
    }

    public void setIgnoreTruncation(boolean p3)
    {
        int v1 = 0;
        while (v1 < this.resolvers.size()) {
            ((org.xbill.DNS.Resolver) this.resolvers.get(v1)).setIgnoreTruncation(p3);
            v1++;
        }
        return;
    }

    public void setLoadBalance(boolean p1)
    {
        this.loadBalance = p1;
        return;
    }

    public void setPort(int p3)
    {
        int v1 = 0;
        while (v1 < this.resolvers.size()) {
            ((org.xbill.DNS.Resolver) this.resolvers.get(v1)).setPort(p3);
            v1++;
        }
        return;
    }

    public void setRetries(int p1)
    {
        this.retries = p1;
        return;
    }

    public void setTCP(boolean p3)
    {
        int v1 = 0;
        while (v1 < this.resolvers.size()) {
            ((org.xbill.DNS.Resolver) this.resolvers.get(v1)).setTCP(p3);
            v1++;
        }
        return;
    }

    public void setTSIGKey(org.xbill.DNS.TSIG p3)
    {
        int v1 = 0;
        while (v1 < this.resolvers.size()) {
            ((org.xbill.DNS.Resolver) this.resolvers.get(v1)).setTSIGKey(p3);
            v1++;
        }
        return;
    }

    public void setTimeout(int p2)
    {
        this.setTimeout(p2, 0);
        return;
    }

    public void setTimeout(int p3, int p4)
    {
        int v1 = 0;
        while (v1 < this.resolvers.size()) {
            ((org.xbill.DNS.Resolver) this.resolvers.get(v1)).setTimeout(p3, p4);
            v1++;
        }
        return;
    }
}
