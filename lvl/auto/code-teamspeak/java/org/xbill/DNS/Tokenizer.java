package org.xbill.DNS;
public class Tokenizer {
    public static final int COMMENT = 5;
    public static final int EOF = 0;
    public static final int EOL = 1;
    public static final int IDENTIFIER = 3;
    public static final int QUOTED_STRING = 4;
    public static final int WHITESPACE = 2;
    private static String delim;
    private static String quotes;
    private org.xbill.DNS.Tokenizer$Token current;
    private String delimiters;
    private String filename;
    private java.io.PushbackInputStream is;
    private int line;
    private int multiline;
    private boolean quoting;
    private StringBuffer sb;
    private boolean ungottenToken;
    private boolean wantClose;

    static Tokenizer()
    {
        org.xbill.DNS.Tokenizer.delim = " \t\n;()\"";
        org.xbill.DNS.Tokenizer.quotes = "\"";
        return;
    }

    public Tokenizer(java.io.File p2)
    {
        this(new java.io.FileInputStream(p2));
        this.wantClose = 1;
        this.filename = p2.getName();
        return;
    }

    public Tokenizer(java.io.InputStream p4)
    {
        if (!(p4 instanceof java.io.BufferedInputStream)) {
            p4 = new java.io.BufferedInputStream(p4);
        }
        this.is = new java.io.PushbackInputStream(p4, 2);
        this.ungottenToken = 0;
        this.multiline = 0;
        this.quoting = 0;
        this.delimiters = org.xbill.DNS.Tokenizer.delim;
        this.current = new org.xbill.DNS.Tokenizer$Token(0);
        this.sb = new StringBuffer();
        this.filename = "<none>";
        this.line = 1;
        return;
    }

    public Tokenizer(String p3)
    {
        this(new java.io.ByteArrayInputStream(p3.getBytes()));
        return;
    }

    private String _getIdentifier(String p4)
    {
        String v0_0 = this.get();
        if (v0_0.type == 3) {
            return v0_0.value;
        } else {
            throw this.exception(new StringBuffer("expected ").append(p4).toString());
        }
    }

    private void checkUnbalancedParens()
    {
        if (this.multiline <= 0) {
            return;
        } else {
            throw this.exception("unbalanced parentheses");
        }
    }

    private int getChar()
    {
        int v0_1 = this.is.read();
        if (v0_1 == 13) {
            int v0_3 = this.is.read();
            if (v0_3 != 10) {
                this.is.unread(v0_3);
            }
            v0_1 = 10;
        }
        if (v0_1 == 10) {
            this.line = (this.line + 1);
        }
        return v0_1;
    }

    private String remainingStrings()
    {
        String v1 = 0;
        StringBuffer v0_0 = 0;
        while(true) {
            String v2_0 = this.get();
            if (!v2_0.isString()) {
                break;
            }
            if (v0_0 == null) {
                v0_0 = new StringBuffer();
            }
            v0_0.append(v2_0.value);
        }
        this.unget();
        if (v0_0 != null) {
            v1 = v0_0.toString();
        }
        return v1;
    }

    private int skipWhitespace()
    {
        int v0 = 0;
        while(true) {
            int v1 = this.getChar();
            if ((v1 != 32) && ((v1 != 9) && ((v1 != 10) || (this.multiline <= 0)))) {
                break;
            }
            v0++;
        }
        this.ungetChar(v1);
        return v0;
    }

    private void ungetChar(int p2)
    {
        if (p2 != -1) {
            this.is.unread(p2);
            if (p2 == 10) {
                this.line = (this.line - 1);
            }
        }
        return;
    }

    public void close()
    {
        if (this.wantClose) {
            try {
                this.is.close();
            } catch (java.io.IOException v0) {
            }
        }
        return;
    }

    public org.xbill.DNS.TextParseException exception(String p4)
    {
        return new org.xbill.DNS.Tokenizer$TokenizerException(this.filename, this.line, p4);
    }

    protected void finalize()
    {
        this.close();
        return;
    }

    public org.xbill.DNS.Tokenizer$Token get()
    {
        return this.get(0, 0);
    }

    public org.xbill.DNS.Tokenizer$Token get(boolean p10, boolean p11)
    {
        org.xbill.DNS.Tokenizer$Token v0_9;
        if (!this.ungottenToken) {
            if ((this.skipWhitespace() <= 0) || (!p10)) {
                org.xbill.DNS.Tokenizer$Token v0_11 = 3;
                this.sb.setLength(0);
                while(true) {
                    StringBuffer v2_3 = this.getChar();
                    if ((v2_3 != -1) && (this.delimiters.indexOf(v2_3) == -1)) {
                        break;
                    }
                    if (v2_3 != -1) {
                        if ((this.sb.length() != 0) || (v0_11 == 4)) {
                            this.ungetChar(v2_3);
                            if ((this.sb.length() != 0) || (v0_11 == 4)) {
                                v0_9 = org.xbill.DNS.Tokenizer$Token.access$100(this.current, v0_11, this.sb);
                            } else {
                                this.checkUnbalancedParens();
                                v0_9 = org.xbill.DNS.Tokenizer$Token.access$100(this.current, 0, 0);
                            }
                        } else {
                            if (v2_3 != 40) {
                                if (v2_3 != 41) {
                                    if (v2_3 != 34) {
                                        if (v2_3 != 10) {
                                            if (v2_3 != 59) {
                                                throw new IllegalStateException();
                                            }
                                            while(true) {
                                                StringBuffer v2_8 = this.getChar();
                                                if ((v2_8 == 10) || (v2_8 == -1)) {
                                                    break;
                                                }
                                                this.sb.append(((char) v2_8));
                                            }
                                            if (!p11) {
                                                if ((v2_8 != -1) || (v0_11 == 4)) {
                                                    if (this.multiline <= 0) {
                                                        v0_9 = org.xbill.DNS.Tokenizer$Token.access$100(this.current, 1, 0);
                                                    } else {
                                                        this.skipWhitespace();
                                                        this.sb.setLength(0);
                                                    }
                                                } else {
                                                    this.checkUnbalancedParens();
                                                    v0_9 = org.xbill.DNS.Tokenizer$Token.access$100(this.current, 0, 0);
                                                }
                                            } else {
                                                this.ungetChar(v2_8);
                                                v0_9 = org.xbill.DNS.Tokenizer$Token.access$100(this.current, 5, this.sb);
                                            }
                                        } else {
                                            v0_9 = org.xbill.DNS.Tokenizer$Token.access$100(this.current, 1, 0);
                                        }
                                    } else {
                                        if (this.quoting) {
                                            this.quoting = 0;
                                            this.delimiters = org.xbill.DNS.Tokenizer.delim;
                                            this.skipWhitespace();
                                        } else {
                                            this.quoting = 1;
                                            this.delimiters = org.xbill.DNS.Tokenizer.quotes;
                                            v0_11 = 4;
                                        }
                                    }
                                } else {
                                    if (this.multiline > 0) {
                                        this.multiline = (this.multiline - 1);
                                        this.skipWhitespace();
                                    } else {
                                        throw this.exception("invalid close parenthesis");
                                    }
                                }
                            } else {
                                this.multiline = (this.multiline + 1);
                                this.skipWhitespace();
                            }
                        }
                    } else {
                        if (!this.quoting) {
                            if (this.sb.length() != 0) {
                                v0_9 = org.xbill.DNS.Tokenizer$Token.access$100(this.current, v0_11, this.sb);
                            } else {
                                v0_9 = org.xbill.DNS.Tokenizer$Token.access$100(this.current, 0, 0);
                            }
                        } else {
                            throw this.exception("EOF in quoted string");
                        }
                    }
                }
                if (v2_3 != 92) {
                    if ((this.quoting) && (v2_3 == 10)) {
                        throw this.exception("newline in quoted string");
                    }
                } else {
                    v2_3 = this.getChar();
                    if (v2_3 != -1) {
                        this.sb.append(92);
                    } else {
                        throw this.exception("unterminated escape sequence");
                    }
                }
                this.sb.append(((char) v2_3));
            } else {
                v0_9 = org.xbill.DNS.Tokenizer$Token.access$100(this.current, 2, 0);
            }
        } else {
            this.ungottenToken = 0;
            if (this.current.type != 2) {
                if (this.current.type != 5) {
                    if (this.current.type == 1) {
                        this.line = (this.line + 1);
                    }
                    v0_9 = this.current;
                } else {
                    if (!p11) {
                    } else {
                        v0_9 = this.current;
                    }
                }
            } else {
                if (!p10) {
                } else {
                    v0_9 = this.current;
                }
            }
        }
        return v0_9;
    }

    public java.net.InetAddress getAddress(int p2)
    {
        try {
            return org.xbill.DNS.Address.getByAddress(this._getIdentifier("an address"), p2);
        } catch (org.xbill.DNS.TextParseException v0_3) {
            throw this.exception(v0_3.getMessage());
        }
    }

    public byte[] getAddressBytes(int p4)
    {
        org.xbill.DNS.TextParseException v0_1 = this._getIdentifier("an address");
        StringBuffer v1_0 = org.xbill.DNS.Address.toByteArray(v0_1, p4);
        if (v1_0 != null) {
            return v1_0;
        } else {
            throw this.exception(new StringBuffer("Invalid address: ").append(v0_1).toString());
        }
    }

    public byte[] getBase32String(org.xbill.DNS.utils.base32 p2)
    {
        org.xbill.DNS.TextParseException v0_2 = p2.fromString(this._getIdentifier("a base32 string"));
        if (v0_2 != null) {
            return v0_2;
        } else {
            throw this.exception("invalid base32 encoding");
        }
    }

    public byte[] getBase64()
    {
        return this.getBase64(0);
    }

    public byte[] getBase64(boolean p2)
    {
        org.xbill.DNS.TextParseException v0_1;
        org.xbill.DNS.TextParseException v0_0 = this.remainingStrings();
        if (v0_0 != null) {
            v0_1 = org.xbill.DNS.utils.base64.fromString(v0_0);
            if (v0_1 == null) {
                throw this.exception("invalid base64 encoding");
            }
        } else {
            if (!p2) {
                v0_1 = 0;
            } else {
                throw this.exception("expected base64 encoded string");
            }
        }
        return v0_1;
    }

    public void getEOL()
    {
        org.xbill.DNS.TextParseException v0_0 = this.get();
        if ((v0_0.type == 1) || (v0_0.type == 0)) {
            return;
        } else {
            throw this.exception("expected EOL or EOF");
        }
    }

    public byte[] getHex()
    {
        return this.getHex(0);
    }

    public byte[] getHex(boolean p2)
    {
        org.xbill.DNS.TextParseException v0_1;
        org.xbill.DNS.TextParseException v0_0 = this.remainingStrings();
        if (v0_0 != null) {
            v0_1 = org.xbill.DNS.utils.base16.fromString(v0_0);
            if (v0_1 == null) {
                throw this.exception("invalid hex encoding");
            }
        } else {
            if (!p2) {
                v0_1 = 0;
            } else {
                throw this.exception("expected hex encoded string");
            }
        }
        return v0_1;
    }

    public byte[] getHexString()
    {
        org.xbill.DNS.TextParseException v0_2 = org.xbill.DNS.utils.base16.fromString(this._getIdentifier("a hex string"));
        if (v0_2 != null) {
            return v0_2;
        } else {
            throw this.exception("invalid hex encoding");
        }
    }

    public String getIdentifier()
    {
        return this._getIdentifier("an identifier");
    }

    public long getLong()
    {
        org.xbill.DNS.TextParseException v0_1 = this._getIdentifier("an integer");
        if (Character.isDigit(v0_1.charAt(0))) {
            try {
                return Long.parseLong(v0_1);
            } catch (org.xbill.DNS.TextParseException v0) {
                throw this.exception("expected an integer");
            }
        } else {
            throw this.exception("expected an integer");
        }
    }

    public org.xbill.DNS.Name getName(org.xbill.DNS.Name p3)
    {
        try {
            org.xbill.DNS.TextParseException v0_2 = org.xbill.DNS.Name.fromString(this._getIdentifier("a name"), p3);
        } catch (org.xbill.DNS.TextParseException v0_3) {
            throw this.exception(v0_3.getMessage());
        }
        if (v0_2.isAbsolute()) {
            return v0_2;
        } else {
            throw new org.xbill.DNS.RelativeNameException(v0_2);
        }
    }

    public String getString()
    {
        String v0_0 = this.get();
        if (v0_0.isString()) {
            return v0_0.value;
        } else {
            throw this.exception("expected a string");
        }
    }

    public long getTTL()
    {
        try {
            return org.xbill.DNS.TTL.parseTTL(this._getIdentifier("a TTL value"));
        } catch (org.xbill.DNS.TextParseException v0) {
            throw this.exception("expected a TTL value");
        }
    }

    public long getTTLLike()
    {
        try {
            return org.xbill.DNS.TTL.parse(this._getIdentifier("a TTL-like value"), 0);
        } catch (org.xbill.DNS.TextParseException v0) {
            throw this.exception("expected a TTL-like value");
        }
    }

    public int getUInt16()
    {
        int v0_0 = this.getLong();
        if ((v0_0 >= 0) && (v0_0 <= 65535)) {
            return ((int) v0_0);
        } else {
            throw this.exception("expected an 16 bit unsigned integer");
        }
    }

    public long getUInt32()
    {
        org.xbill.DNS.TextParseException v0_0 = this.getLong();
        if ((v0_0 >= 0) && (v0_0 <= 2.1219957905e-314)) {
            return v0_0;
        } else {
            throw this.exception("expected an 32 bit unsigned integer");
        }
    }

    public int getUInt8()
    {
        int v0_0 = this.getLong();
        if ((v0_0 >= 0) && (v0_0 <= 255)) {
            return ((int) v0_0);
        } else {
            throw this.exception("expected an 8 bit unsigned integer");
        }
    }

    public void unget()
    {
        if (!this.ungottenToken) {
            if (this.current.type == 1) {
                this.line = (this.line - 1);
            }
            this.ungottenToken = 1;
            return;
        } else {
            throw new IllegalStateException("Cannot unget multiple tokens");
        }
    }
}
