package org.xbill.DNS;
 class EmptyRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 3601852050646429582;

    EmptyRecord()
    {
        return;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.EmptyRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p1, org.xbill.DNS.Name p2)
    {
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p1)
    {
        return;
    }

    String rrToString()
    {
        return "";
    }

    void rrToWire(org.xbill.DNS.DNSOutput p1, org.xbill.DNS.Compression p2, boolean p3)
    {
        return;
    }
}
