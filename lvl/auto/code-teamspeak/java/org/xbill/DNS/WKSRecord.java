package org.xbill.DNS;
public class WKSRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 9342484309800431811;
    private byte[] address;
    private int protocol;
    private int[] services;

    WKSRecord()
    {
        return;
    }

    public WKSRecord(org.xbill.DNS.Name p8, int p9, long p10, java.net.InetAddress p12, int p13, int[] p14)
    {
        this(p8, 11, p9, p10);
        if (org.xbill.DNS.Address.familyOf(p12) == 1) {
            this.address = p12.getAddress();
            this.protocol = org.xbill.DNS.WKSRecord.checkU8("protocol", p13);
            int[] v0_5 = 0;
            while (v0_5 < p14.length) {
                org.xbill.DNS.WKSRecord.checkU16("service", p14[v0_5]);
                v0_5++;
            }
            int[] v0_7 = new int[p14.length];
            this.services = v0_7;
            System.arraycopy(p14, 0, this.services, 0, p14.length);
            java.util.Arrays.sort(this.services);
            return;
        } else {
            throw new IllegalArgumentException("invalid IPv4 address");
        }
    }

    public java.net.InetAddress getAddress()
    {
        try {
            int v0_1 = java.net.InetAddress.getByAddress(this.address);
        } catch (int v0) {
            v0_1 = 0;
        }
        return v0_1;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.WKSRecord();
    }

    public int getProtocol()
    {
        return this.protocol;
    }

    public int[] getServices()
    {
        return this.services;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p5, org.xbill.DNS.Name p6)
    {
        this.address = org.xbill.DNS.Address.toByteArray(p5.getString(), 1);
        if (this.address != null) {
            int v0_3 = p5.getString();
            this.protocol = org.xbill.DNS.WKSRecord$Protocol.value(v0_3);
            if (this.protocol >= 0) {
                String v2_1 = new java.util.ArrayList();
                while(true) {
                    int v0_4 = p5.get();
                    if (!v0_4.isString()) {
                        p5.unget();
                        int v0_6 = new int[v2_1.size()];
                        this.services = v0_6;
                        int v1_4 = 0;
                        while (v1_4 < v2_1.size()) {
                            this.services[v1_4] = ((Integer) v2_1.get(v1_4)).intValue();
                            v1_4++;
                        }
                        return;
                    } else {
                        int v1_6 = org.xbill.DNS.WKSRecord$Service.value(v0_4.value);
                        if (v1_6 < 0) {
                            break;
                        }
                        v2_1.add(new Integer(v1_6));
                    }
                }
                throw p5.exception(new StringBuffer("Invalid TCP/UDP service: ").append(v0_4.value).toString());
            } else {
                throw p5.exception(new StringBuffer("Invalid IP protocol: ").append(v0_3).toString());
            }
        } else {
            throw p5.exception("invalid address");
        }
    }

    void rrFromWire(org.xbill.DNS.DNSInput p9)
    {
        int v1 = 0;
        this.address = p9.readByteArray(4);
        this.protocol = p9.readU8();
        byte[] v3 = p9.readByteArray();
        java.util.ArrayList v4_1 = new java.util.ArrayList();
        int v0_3 = 0;
        while (v0_3 < v3.length) {
            int v2_2 = 0;
            while (v2_2 < 8) {
                if (((v3[v0_3] & 255) & (1 << (7 - v2_2))) != 0) {
                    v4_1.add(new Integer(((v0_3 * 8) + v2_2)));
                }
                v2_2++;
            }
            v0_3++;
        }
        int v0_5 = new int[v4_1.size()];
        this.services = v0_5;
        while (v1 < v4_1.size()) {
            this.services[v1] = ((Integer) v4_1.get(v1)).intValue();
            v1++;
        }
        return;
    }

    String rrToString()
    {
        StringBuffer v1_1 = new StringBuffer();
        v1_1.append(org.xbill.DNS.Address.toDottedQuad(this.address));
        v1_1.append(" ");
        v1_1.append(this.protocol);
        String v0_4 = 0;
        while (v0_4 < this.services.length) {
            v1_1.append(new StringBuffer(" ").append(this.services[v0_4]).toString());
            v0_4++;
        }
        return v1_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p7, org.xbill.DNS.Compression p8, boolean p9)
    {
        p7.writeByteArray(this.address);
        p7.writeU8(this.protocol);
        byte[] v1_3 = new byte[((this.services[(this.services.length - 1)] / 8) + 1)];
        int v0_6 = 0;
        while (v0_6 < this.services.length) {
            byte v2_3 = this.services[v0_6];
            int v3 = (v2_3 / 8);
            v1_3[v3] = ((byte) ((1 << (7 - (v2_3 % 8))) | v1_3[v3]));
            v0_6++;
        }
        p7.writeByteArray(v1_3);
        return;
    }
}
