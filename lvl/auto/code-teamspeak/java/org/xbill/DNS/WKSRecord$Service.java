package org.xbill.DNS;
public class WKSRecord$Service {
    public static final int AUTH = 113;
    public static final int BL_IDM = 142;
    public static final int BOOTPC = 68;
    public static final int BOOTPS = 67;
    public static final int CHARGEN = 19;
    public static final int CISCO_FNA = 130;
    public static final int CISCO_SYS = 132;
    public static final int CISCO_TNA = 131;
    public static final int CSNET_NS = 105;
    public static final int DAYTIME = 13;
    public static final int DCP = 93;
    public static final int DISCARD = 9;
    public static final int DOMAIN = 53;
    public static final int DSP = 33;
    public static final int ECHO = 7;
    public static final int EMFIS_CNTL = 141;
    public static final int EMFIS_DATA = 140;
    public static final int ERPC = 121;
    public static final int FINGER = 79;
    public static final int FTP = 21;
    public static final int FTP_DATA = 20;
    public static final int GRAPHICS = 41;
    public static final int HOSTNAME = 101;
    public static final int HOSTS2_NS = 81;
    public static final int INGRES_NET = 134;
    public static final int ISI_GL = 55;
    public static final int ISO_TSAP = 102;
    public static final int LA_MAINT = 51;
    public static final int LINK = 245;
    public static final int LOCUS_CON = 127;
    public static final int LOCUS_MAP = 125;
    public static final int LOC_SRV = 135;
    public static final int LOGIN = 49;
    public static final int METAGRAM = 99;
    public static final int MIT_DOV = 91;
    public static final int MPM = 45;
    public static final int MPM_FLAGS = 44;
    public static final int MPM_SND = 46;
    public static final int MSG_AUTH = 31;
    public static final int MSG_ICP = 29;
    public static final int NAMESERVER = 42;
    public static final int NETBIOS_DGM = 138;
    public static final int NETBIOS_NS = 137;
    public static final int NETBIOS_SSN = 139;
    public static final int NETRJS_1 = 71;
    public static final int NETRJS_2 = 72;
    public static final int NETRJS_3 = 73;
    public static final int NETRJS_4 = 74;
    public static final int NICNAME = 43;
    public static final int NI_FTP = 47;
    public static final int NI_MAIL = 61;
    public static final int NNTP = 119;
    public static final int NSW_FE = 27;
    public static final int NTP = 123;
    public static final int POP_2 = 109;
    public static final int PROFILE = 136;
    public static final int PWDGEN = 129;
    public static final int QUOTE = 17;
    public static final int RJE = 5;
    public static final int RLP = 39;
    public static final int RTELNET = 107;
    public static final int SFTP = 115;
    public static final int SMTP = 25;
    public static final int STATSRV = 133;
    public static final int SUNRPC = 111;
    public static final int SUPDUP = 95;
    public static final int SUR_MEAS = 243;
    public static final int SU_MIT_TG = 89;
    public static final int SWIFT_RVF = 97;
    public static final int TACACS_DS = 65;
    public static final int TACNEWS = 98;
    public static final int TELNET = 23;
    public static final int TFTP = 69;
    public static final int TIME = 37;
    public static final int USERS = 11;
    public static final int UUCP_PATH = 117;
    public static final int VIA_FTP = 63;
    public static final int X400 = 103;
    public static final int X400_SND = 104;
    private static org.xbill.DNS.Mnemonic services;

    static WKSRecord$Service()
    {
        org.xbill.DNS.Mnemonic v0_1 = new org.xbill.DNS.Mnemonic("TCP/UDP service", 3);
        org.xbill.DNS.WKSRecord$Service.services = v0_1;
        v0_1.setMaximum(65535);
        org.xbill.DNS.WKSRecord$Service.services.setNumericAllowed(1);
        org.xbill.DNS.WKSRecord$Service.services.add(5, "rje");
        org.xbill.DNS.WKSRecord$Service.services.add(7, "echo");
        org.xbill.DNS.WKSRecord$Service.services.add(9, "discard");
        org.xbill.DNS.WKSRecord$Service.services.add(11, "users");
        org.xbill.DNS.WKSRecord$Service.services.add(13, "daytime");
        org.xbill.DNS.WKSRecord$Service.services.add(17, "quote");
        org.xbill.DNS.WKSRecord$Service.services.add(19, "chargen");
        org.xbill.DNS.WKSRecord$Service.services.add(20, "ftp-data");
        org.xbill.DNS.WKSRecord$Service.services.add(21, "ftp");
        org.xbill.DNS.WKSRecord$Service.services.add(23, "telnet");
        org.xbill.DNS.WKSRecord$Service.services.add(25, "smtp");
        org.xbill.DNS.WKSRecord$Service.services.add(27, "nsw-fe");
        org.xbill.DNS.WKSRecord$Service.services.add(29, "msg-icp");
        org.xbill.DNS.WKSRecord$Service.services.add(31, "msg-auth");
        org.xbill.DNS.WKSRecord$Service.services.add(33, "dsp");
        org.xbill.DNS.WKSRecord$Service.services.add(37, "time");
        org.xbill.DNS.WKSRecord$Service.services.add(39, "rlp");
        org.xbill.DNS.WKSRecord$Service.services.add(41, "graphics");
        org.xbill.DNS.WKSRecord$Service.services.add(42, "nameserver");
        org.xbill.DNS.WKSRecord$Service.services.add(43, "nicname");
        org.xbill.DNS.WKSRecord$Service.services.add(44, "mpm-flags");
        org.xbill.DNS.WKSRecord$Service.services.add(45, "mpm");
        org.xbill.DNS.WKSRecord$Service.services.add(46, "mpm-snd");
        org.xbill.DNS.WKSRecord$Service.services.add(47, "ni-ftp");
        org.xbill.DNS.WKSRecord$Service.services.add(49, "login");
        org.xbill.DNS.WKSRecord$Service.services.add(51, "la-maint");
        org.xbill.DNS.WKSRecord$Service.services.add(53, "domain");
        org.xbill.DNS.WKSRecord$Service.services.add(55, "isi-gl");
        org.xbill.DNS.WKSRecord$Service.services.add(61, "ni-mail");
        org.xbill.DNS.WKSRecord$Service.services.add(63, "via-ftp");
        org.xbill.DNS.WKSRecord$Service.services.add(65, "tacacs-ds");
        org.xbill.DNS.WKSRecord$Service.services.add(67, "bootps");
        org.xbill.DNS.WKSRecord$Service.services.add(68, "bootpc");
        org.xbill.DNS.WKSRecord$Service.services.add(69, "tftp");
        org.xbill.DNS.WKSRecord$Service.services.add(71, "netrjs-1");
        org.xbill.DNS.WKSRecord$Service.services.add(72, "netrjs-2");
        org.xbill.DNS.WKSRecord$Service.services.add(73, "netrjs-3");
        org.xbill.DNS.WKSRecord$Service.services.add(74, "netrjs-4");
        org.xbill.DNS.WKSRecord$Service.services.add(79, "finger");
        org.xbill.DNS.WKSRecord$Service.services.add(81, "hosts2-ns");
        org.xbill.DNS.WKSRecord$Service.services.add(89, "su-mit-tg");
        org.xbill.DNS.WKSRecord$Service.services.add(91, "mit-dov");
        org.xbill.DNS.WKSRecord$Service.services.add(93, "dcp");
        org.xbill.DNS.WKSRecord$Service.services.add(95, "supdup");
        org.xbill.DNS.WKSRecord$Service.services.add(97, "swift-rvf");
        org.xbill.DNS.WKSRecord$Service.services.add(98, "tacnews");
        org.xbill.DNS.WKSRecord$Service.services.add(99, "metagram");
        org.xbill.DNS.WKSRecord$Service.services.add(101, "hostname");
        org.xbill.DNS.WKSRecord$Service.services.add(102, "iso-tsap");
        org.xbill.DNS.WKSRecord$Service.services.add(103, "x400");
        org.xbill.DNS.WKSRecord$Service.services.add(104, "x400-snd");
        org.xbill.DNS.WKSRecord$Service.services.add(105, "csnet-ns");
        org.xbill.DNS.WKSRecord$Service.services.add(107, "rtelnet");
        org.xbill.DNS.WKSRecord$Service.services.add(109, "pop-2");
        org.xbill.DNS.WKSRecord$Service.services.add(111, "sunrpc");
        org.xbill.DNS.WKSRecord$Service.services.add(113, "auth");
        org.xbill.DNS.WKSRecord$Service.services.add(115, "sftp");
        org.xbill.DNS.WKSRecord$Service.services.add(117, "uucp-path");
        org.xbill.DNS.WKSRecord$Service.services.add(119, "nntp");
        org.xbill.DNS.WKSRecord$Service.services.add(121, "erpc");
        org.xbill.DNS.WKSRecord$Service.services.add(123, "ntp");
        org.xbill.DNS.WKSRecord$Service.services.add(125, "locus-map");
        org.xbill.DNS.WKSRecord$Service.services.add(127, "locus-con");
        org.xbill.DNS.WKSRecord$Service.services.add(129, "pwdgen");
        org.xbill.DNS.WKSRecord$Service.services.add(130, "cisco-fna");
        org.xbill.DNS.WKSRecord$Service.services.add(131, "cisco-tna");
        org.xbill.DNS.WKSRecord$Service.services.add(132, "cisco-sys");
        org.xbill.DNS.WKSRecord$Service.services.add(133, "statsrv");
        org.xbill.DNS.WKSRecord$Service.services.add(134, "ingres-net");
        org.xbill.DNS.WKSRecord$Service.services.add(135, "loc-srv");
        org.xbill.DNS.WKSRecord$Service.services.add(136, "profile");
        org.xbill.DNS.WKSRecord$Service.services.add(137, "netbios-ns");
        org.xbill.DNS.WKSRecord$Service.services.add(138, "netbios-dgm");
        org.xbill.DNS.WKSRecord$Service.services.add(139, "netbios-ssn");
        org.xbill.DNS.WKSRecord$Service.services.add(140, "emfis-data");
        org.xbill.DNS.WKSRecord$Service.services.add(141, "emfis-cntl");
        org.xbill.DNS.WKSRecord$Service.services.add(142, "bl-idm");
        org.xbill.DNS.WKSRecord$Service.services.add(243, "sur-meas");
        org.xbill.DNS.WKSRecord$Service.services.add(245, "link");
        return;
    }

    private WKSRecord$Service()
    {
        return;
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.WKSRecord$Service.services.getText(p1);
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.WKSRecord$Service.services.getValue(p1);
    }
}
