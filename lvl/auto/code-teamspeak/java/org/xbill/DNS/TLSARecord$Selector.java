package org.xbill.DNS;
public class TLSARecord$Selector {
    public static final int FULL_CERTIFICATE = 0;
    public static final int SUBJECT_PUBLIC_KEY_INFO = 1;

    private TLSARecord$Selector()
    {
        return;
    }
}
