package org.xbill.DNS;
public class KEYRecord$Flags {
    public static final int EXTEND = 4096;
    public static final int FLAG10 = 32;
    public static final int FLAG11 = 16;
    public static final int FLAG2 = 8192;
    public static final int FLAG4 = 2048;
    public static final int FLAG5 = 1024;
    public static final int FLAG8 = 128;
    public static final int FLAG9 = 64;
    public static final int HOST = 512;
    public static final int NOAUTH = 32768;
    public static final int NOCONF = 16384;
    public static final int NOKEY = 49152;
    public static final int NTYP3 = 768;
    public static final int OWNER_MASK = 768;
    public static final int SIG0 = 0;
    public static final int SIG1 = 1;
    public static final int SIG10 = 10;
    public static final int SIG11 = 11;
    public static final int SIG12 = 12;
    public static final int SIG13 = 13;
    public static final int SIG14 = 14;
    public static final int SIG15 = 15;
    public static final int SIG2 = 2;
    public static final int SIG3 = 3;
    public static final int SIG4 = 4;
    public static final int SIG5 = 5;
    public static final int SIG6 = 6;
    public static final int SIG7 = 7;
    public static final int SIG8 = 8;
    public static final int SIG9 = 9;
    public static final int USER = 0;
    public static final int USE_MASK = 49152;
    public static final int ZONE = 256;
    private static org.xbill.DNS.Mnemonic flags;

    static KEYRecord$Flags()
    {
        org.xbill.DNS.Mnemonic v0_1 = new org.xbill.DNS.Mnemonic("KEY flags", 2);
        org.xbill.DNS.KEYRecord$Flags.flags = v0_1;
        v0_1.setMaximum(65535);
        org.xbill.DNS.KEYRecord$Flags.flags.setNumericAllowed(0);
        org.xbill.DNS.KEYRecord$Flags.flags.add(16384, "NOCONF");
        org.xbill.DNS.KEYRecord$Flags.flags.add(32768, "NOAUTH");
        org.xbill.DNS.KEYRecord$Flags.flags.add(49152, "NOKEY");
        org.xbill.DNS.KEYRecord$Flags.flags.add(8192, "FLAG2");
        org.xbill.DNS.KEYRecord$Flags.flags.add(4096, "EXTEND");
        org.xbill.DNS.KEYRecord$Flags.flags.add(2048, "FLAG4");
        org.xbill.DNS.KEYRecord$Flags.flags.add(1024, "FLAG5");
        org.xbill.DNS.KEYRecord$Flags.flags.add(0, "USER");
        org.xbill.DNS.KEYRecord$Flags.flags.add(256, "ZONE");
        org.xbill.DNS.KEYRecord$Flags.flags.add(512, "HOST");
        org.xbill.DNS.KEYRecord$Flags.flags.add(768, "NTYP3");
        org.xbill.DNS.KEYRecord$Flags.flags.add(128, "FLAG8");
        org.xbill.DNS.KEYRecord$Flags.flags.add(64, "FLAG9");
        org.xbill.DNS.KEYRecord$Flags.flags.add(32, "FLAG10");
        org.xbill.DNS.KEYRecord$Flags.flags.add(16, "FLAG11");
        org.xbill.DNS.KEYRecord$Flags.flags.add(0, "SIG0");
        org.xbill.DNS.KEYRecord$Flags.flags.add(1, "SIG1");
        org.xbill.DNS.KEYRecord$Flags.flags.add(2, "SIG2");
        org.xbill.DNS.KEYRecord$Flags.flags.add(3, "SIG3");
        org.xbill.DNS.KEYRecord$Flags.flags.add(4, "SIG4");
        org.xbill.DNS.KEYRecord$Flags.flags.add(5, "SIG5");
        org.xbill.DNS.KEYRecord$Flags.flags.add(6, "SIG6");
        org.xbill.DNS.KEYRecord$Flags.flags.add(7, "SIG7");
        org.xbill.DNS.KEYRecord$Flags.flags.add(8, "SIG8");
        org.xbill.DNS.KEYRecord$Flags.flags.add(9, "SIG9");
        org.xbill.DNS.KEYRecord$Flags.flags.add(10, "SIG10");
        org.xbill.DNS.KEYRecord$Flags.flags.add(11, "SIG11");
        org.xbill.DNS.KEYRecord$Flags.flags.add(12, "SIG12");
        org.xbill.DNS.KEYRecord$Flags.flags.add(13, "SIG13");
        org.xbill.DNS.KEYRecord$Flags.flags.add(14, "SIG14");
        org.xbill.DNS.KEYRecord$Flags.flags.add(15, "SIG15");
        return;
    }

    private KEYRecord$Flags()
    {
        return;
    }

    public static int value(String p5)
    {
        try {
            int v0_0 = Integer.parseInt(p5);
        } catch (int v0) {
            int v2_1 = new java.util.StringTokenizer(p5, "|");
            v0_0 = 0;
        }
        if ((v0_0 < 0) || (v0_0 > 65535)) {
            v0_0 = -1;
            return v0_0;
        } else {
            return v0_0;
        }
    }
}
