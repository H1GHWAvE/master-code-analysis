package org.xbill.DNS;
public class NULLRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 12650250890474335078;
    private byte[] data;

    NULLRecord()
    {
        return;
    }

    public NULLRecord(org.xbill.DNS.Name p8, int p9, long p10, byte[] p12)
    {
        this(p8, 10, p9, p10);
        if (p12.length <= 65535) {
            this.data = p12;
            return;
        } else {
            throw new IllegalArgumentException("data must be <65536 bytes");
        }
    }

    public byte[] getData()
    {
        return this.data;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.NULLRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        throw p2.exception("no defined text format for NULL records");
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.data = p2.readByteArray();
        return;
    }

    String rrToString()
    {
        return org.xbill.DNS.NULLRecord.unknownToString(this.data);
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeByteArray(this.data);
        return;
    }
}
