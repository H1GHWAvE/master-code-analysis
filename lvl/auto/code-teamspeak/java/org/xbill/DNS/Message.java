package org.xbill.DNS;
public class Message implements java.lang.Cloneable {
    public static final int MAXLENGTH = 65535;
    static final int TSIG_FAILED = 4;
    static final int TSIG_INTERMEDIATE = 2;
    static final int TSIG_SIGNED = 3;
    static final int TSIG_UNSIGNED = 0;
    static final int TSIG_VERIFIED = 1;
    private static org.xbill.DNS.RRset[] emptyRRsetArray;
    private static org.xbill.DNS.Record[] emptyRecordArray;
    private org.xbill.DNS.Header header;
    private org.xbill.DNS.TSIGRecord querytsig;
    private java.util.List[] sections;
    int sig0start;
    private int size;
    int tsigState;
    private int tsigerror;
    private org.xbill.DNS.TSIG tsigkey;
    int tsigstart;

    static Message()
    {
        org.xbill.DNS.RRset[] v0_0 = new org.xbill.DNS.Record[0];
        org.xbill.DNS.Message.emptyRecordArray = v0_0;
        org.xbill.DNS.RRset[] v0_1 = new org.xbill.DNS.RRset[0];
        org.xbill.DNS.Message.emptyRRsetArray = v0_1;
        return;
    }

    public Message()
    {
        this(new org.xbill.DNS.Header());
        return;
    }

    public Message(int p2)
    {
        this(new org.xbill.DNS.Header(p2));
        return;
    }

    Message(org.xbill.DNS.DNSInput p11)
    {
        int v1_1;
        this(new org.xbill.DNS.Header(p11));
        if (this.header.getOpcode() != 5) {
            v1_1 = 0;
        } else {
            v1_1 = 1;
        }
        boolean v5 = this.header.getFlag(6);
        int v4 = 0;
        while (v4 < 4) {
            try {
                int v6 = this.header.getCount(v4);
            } catch (int v0_13) {
                if (v5) {
                    break;
                }
                throw v0_13;
            }
            if (v6 > 0) {
                this.sections[v4] = new java.util.ArrayList(v6);
            }
            int v3_3 = 0;
            while (v3_3 < v6) {
                int v7 = p11.current();
                int v0_10 = org.xbill.DNS.Record.fromWire(p11, v4, v1_1);
                this.sections[v4].add(v0_10);
                if (v4 == 3) {
                    if (v0_10.getType() == 250) {
                        this.tsigstart = v7;
                    }
                    if ((v0_10.getType() == 24) && (((org.xbill.DNS.SIGRecord) v0_10).getTypeCovered() == 0)) {
                        this.sig0start = v7;
                    }
                }
                v3_3++;
            }
            v4++;
        }
        this.size = p11.current();
        return;
    }

    private Message(org.xbill.DNS.Header p2)
    {
        java.util.List[] v0_1 = new java.util.List[4];
        this.sections = v0_1;
        this.header = p2;
        return;
    }

    public Message(byte[] p2)
    {
        this(new org.xbill.DNS.DNSInput(p2));
        return;
    }

    public static org.xbill.DNS.Message newQuery(org.xbill.DNS.Record p4)
    {
        org.xbill.DNS.Message v0_1 = new org.xbill.DNS.Message();
        v0_1.header.setOpcode(0);
        v0_1.header.setFlag(7);
        v0_1.addRecord(p4, 0);
        return v0_1;
    }

    public static org.xbill.DNS.Message newUpdate(org.xbill.DNS.Name p1)
    {
        return new org.xbill.DNS.Update(p1);
    }

    private static boolean sameSet(org.xbill.DNS.Record p2, org.xbill.DNS.Record p3)
    {
        if ((p2.getRRsetType() != p3.getRRsetType()) || ((p2.getDClass() != p3.getDClass()) || (!p2.getName().equals(p3.getName())))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    private int sectionToWire(org.xbill.DNS.DNSOutput p10, int p11, org.xbill.DNS.Compression p12, int p13)
    {
        int v5 = 0;
        int v6 = this.sections[p11].size();
        int v4_0 = p10.current();
        int v1_0 = 0;
        int v2_0 = 0;
        int v3_0 = 0;
        while (v5 < v6) {
            int v3_1;
            int v2_1;
            int v1_3;
            int v0_5 = ((org.xbill.DNS.Record) this.sections[p11].get(v5));
            if ((p11 != 3) || (!(v0_5 instanceof org.xbill.DNS.OPTRecord))) {
                if ((v1_0 == 0) || (org.xbill.DNS.Message.sameSet(v0_5, v1_0))) {
                    int v1_2 = v3_0;
                    v3_1 = v4_0;
                } else {
                    v1_2 = v5;
                    v3_1 = p10.current();
                }
                v0_5.toWire(p10, p11, p12);
                if (p10.current() <= p13) {
                    v2_1 = v1_2;
                    v1_3 = v2_0;
                } else {
                    p10.jump(v3_1);
                    v2_0 += (v6 - v1_2);
                    break;
                }
            } else {
                v2_1 = v3_0;
                v3_1 = v4_0;
                v1_3 = (v2_0 + 1);
                v0_5 = v1_0;
            }
            v5++;
            v4_0 = v3_1;
            v3_0 = v2_1;
            v2_0 = v1_3;
            v1_0 = v0_5;
        }
        return v2_0;
    }

    private boolean toWire(org.xbill.DNS.DNSOutput p13, int p14)
    {
        int v0_15;
        if (p14 >= 12) {
            if (this.tsigkey != null) {
                p14 -= this.tsigkey.recordLength();
            }
            int v1_0 = this.getOPT();
            int v0_4 = 0;
            if (v1_0 != 0) {
                v0_4 = v1_0.toWire(3);
                p14 -= v0_4.length;
            }
            int v6 = p13.current();
            this.header.toWire(p13);
            org.xbill.DNS.Compression v7_1 = new org.xbill.DNS.Compression();
            int v4_0 = this.header.getFlagsByte();
            org.xbill.DNS.TSIGRecord v5_0 = 0;
            int v1_4 = 0;
            while (v5_0 < 4) {
                if (this.sections[v5_0] != null) {
                    int v8_3 = this.sectionToWire(p13, v5_0, v7_1, p14);
                    if ((v8_3 == 0) || (v5_0 == 3)) {
                        if (v5_0 == 3) {
                            v1_4 = (this.header.getCount(v5_0) - v8_3);
                        }
                    } else {
                        int v4_1 = org.xbill.DNS.Header.setFlag(v4_0, 6, 1);
                        p13.writeU16At((this.header.getCount(v5_0) - v8_3), ((v6 + 4) + (v5_0 * 2)));
                        org.xbill.DNS.TSIGRecord v5_1 = (v5_0 + 1);
                        while (v5_1 < 3) {
                            p13.writeU16At(0, ((v6 + 4) + (v5_1 * 2)));
                            v5_1++;
                        }
                        byte[] v2_1 = v4_1;
                        if (v0_4 != 0) {
                            p13.writeByteArray(v0_4);
                            v1_4++;
                        }
                        if (v2_1 != this.header.getFlagsByte()) {
                            p13.writeU16At(v2_1, (v6 + 2));
                        }
                        if (v1_4 != this.header.getCount(3)) {
                            p13.writeU16At(v1_4, (v6 + 10));
                        }
                        if (this.tsigkey != null) {
                            this.tsigkey.generate(this, p13.toByteArray(), this.tsigerror, this.querytsig).toWire(p13, 3, v7_1);
                            p13.writeU16At((v1_4 + 1), (v6 + 10));
                        }
                        v0_15 = 1;
                        return v0_15;
                    }
                }
                v5_0++;
            }
            v2_1 = v4_0;
        } else {
            v0_15 = 0;
        }
        return v0_15;
    }

    public void addRecord(org.xbill.DNS.Record p3, int p4)
    {
        if (this.sections[p4] == null) {
            this.sections[p4] = new java.util.LinkedList();
        }
        this.header.incCount(p4);
        this.sections[p4].add(p3);
        return;
    }

    public Object clone()
    {
        org.xbill.DNS.Message v1_1 = new org.xbill.DNS.Message();
        int v0_0 = 0;
        while (v0_0 < this.sections.length) {
            if (this.sections[v0_0] != null) {
                v1_1.sections[v0_0] = new java.util.LinkedList(this.sections[v0_0]);
            }
            v0_0++;
        }
        v1_1.header = ((org.xbill.DNS.Header) this.header.clone());
        v1_1.size = this.size;
        return v1_1;
    }

    public boolean findRRset(org.xbill.DNS.Name p3, int p4)
    {
        int v0 = 1;
        if ((!this.findRRset(p3, p4, 1)) && ((!this.findRRset(p3, p4, 2)) && (!this.findRRset(p3, p4, 3)))) {
            v0 = 0;
        }
        return v0;
    }

    public boolean findRRset(org.xbill.DNS.Name p5, int p6, int p7)
    {
        int v2 = 0;
        if (this.sections[p7] != null) {
            int v1 = 0;
            while (v1 < this.sections[p7].size()) {
                int v0_8 = ((org.xbill.DNS.Record) this.sections[p7].get(v1));
                if ((v0_8.getType() != p6) || (!p5.equals(v0_8.getName()))) {
                    v1++;
                } else {
                    v2 = 1;
                    break;
                }
            }
        }
        return v2;
    }

    public boolean findRecord(org.xbill.DNS.Record p4)
    {
        int v0 = 1;
        int v1 = 1;
        while (v1 <= 3) {
            if ((this.sections[v1] == null) || (!this.sections[v1].contains(p4))) {
                v1++;
            }
            return v0;
        }
        v0 = 0;
        return v0;
    }

    public boolean findRecord(org.xbill.DNS.Record p2, int p3)
    {
        if ((this.sections[p3] == null) || (!this.sections[p3].contains(p2))) {
            int v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public org.xbill.DNS.Header getHeader()
    {
        return this.header;
    }

    public org.xbill.DNS.OPTRecord getOPT()
    {
        org.xbill.DNS.Record[] v1 = this.getSectionArray(3);
        int v0_1 = 0;
        while (v0_1 < v1.length) {
            if (!(v1[v0_1] instanceof org.xbill.DNS.OPTRecord)) {
                v0_1++;
            } else {
                int v0_2 = ((org.xbill.DNS.OPTRecord) v1[v0_1]);
            }
            return v0_2;
        }
        v0_2 = 0;
        return v0_2;
    }

    public org.xbill.DNS.Record getQuestion()
    {
        org.xbill.DNS.Record v0_3;
        org.xbill.DNS.Record v0_1 = this.sections[0];
        if ((v0_1 != null) && (v0_1.size() != 0)) {
            v0_3 = ((org.xbill.DNS.Record) v0_1.get(0));
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public int getRcode()
    {
        int v0_1 = this.header.getRcode();
        int v1_0 = this.getOPT();
        if (v1_0 != 0) {
            v0_1 += (v1_0.getExtendedRcode() << 4);
        }
        return v0_1;
    }

    public org.xbill.DNS.Record[] getSectionArray(int p3)
    {
        org.xbill.DNS.Record[] v0_6;
        if (this.sections[p3] != null) {
            org.xbill.DNS.Record[] v0_3 = this.sections[p3];
            org.xbill.DNS.Record[] v1_1 = new org.xbill.DNS.Record[v0_3.size()];
            v0_6 = ((org.xbill.DNS.Record[]) ((org.xbill.DNS.Record[]) v0_3.toArray(v1_1)));
        } else {
            v0_6 = org.xbill.DNS.Message.emptyRecordArray;
        }
        return v0_6;
    }

    public org.xbill.DNS.RRset[] getSectionRRsets(int p12)
    {
        int v0_7;
        if (this.sections[p12] != null) {
            java.util.LinkedList v5_1 = new java.util.LinkedList();
            org.xbill.DNS.Record[] v6 = this.getSectionArray(p12);
            java.util.HashSet v7_1 = new java.util.HashSet();
            int v1 = 0;
            while (v1 < v6.length) {
                int v0_12;
                org.xbill.DNS.Name v8 = v6[v1].getName();
                if (!v7_1.contains(v8)) {
                    v0_12 = 1;
                } else {
                    int v4 = (v5_1.size() - 1);
                    while (v4 >= 0) {
                        int v0_14 = ((org.xbill.DNS.RRset) v5_1.get(v4));
                        if ((v0_14.getType() != v6[v1].getRRsetType()) || ((v0_14.getDClass() != v6[v1].getDClass()) || (!v0_14.getName().equals(v8)))) {
                            v4--;
                        } else {
                            v0_14.addRR(v6[v1]);
                            v0_12 = 0;
                        }
                    }
                }
                if (v0_12 != 0) {
                    v5_1.add(new org.xbill.DNS.RRset(v6[v1]));
                    v7_1.add(v8);
                }
                v1++;
            }
            int v0_4 = new org.xbill.DNS.RRset[v5_1.size()];
            v0_7 = ((org.xbill.DNS.RRset[]) ((org.xbill.DNS.RRset[]) v5_1.toArray(v0_4)));
        } else {
            v0_7 = org.xbill.DNS.Message.emptyRRsetArray;
        }
        return v0_7;
    }

    public org.xbill.DNS.TSIGRecord getTSIG()
    {
        org.xbill.DNS.TSIGRecord v0_5;
        org.xbill.DNS.TSIGRecord v0_1 = this.header.getCount(3);
        if (v0_1 != null) {
            org.xbill.DNS.TSIGRecord v0_4 = ((org.xbill.DNS.Record) this.sections[3].get((v0_1 - 1)));
            if (v0_4.type == 250) {
                v0_5 = ((org.xbill.DNS.TSIGRecord) v0_4);
            } else {
                v0_5 = 0;
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    public boolean isSigned()
    {
        int v0 = 1;
        if ((this.tsigState != 3) && ((this.tsigState != 1) && (this.tsigState != 4))) {
            v0 = 0;
        }
        return v0;
    }

    public boolean isVerified()
    {
        int v0 = 1;
        if (this.tsigState != 1) {
            v0 = 0;
        }
        return v0;
    }

    public int numBytes()
    {
        return this.size;
    }

    public void removeAllRecords(int p3)
    {
        this.sections[p3] = 0;
        this.header.setCount(p3, 0);
        return;
    }

    public boolean removeRecord(org.xbill.DNS.Record p2, int p3)
    {
        if ((this.sections[p3] == null) || (!this.sections[p3].remove(p2))) {
            int v0_5 = 0;
        } else {
            this.header.decCount(p3);
            v0_5 = 1;
        }
        return v0_5;
    }

    public String sectionToString(int p7)
    {
        int v0_2;
        if (p7 <= 3) {
            StringBuffer v1_1 = new StringBuffer();
            org.xbill.DNS.Record[] v2 = this.getSectionArray(p7);
            int v0_1 = 0;
            while (v0_1 < v2.length) {
                String v3_1 = v2[v0_1];
                if (p7 != 0) {
                    v1_1.append(v3_1);
                } else {
                    v1_1.append(new StringBuffer(";;\t").append(v3_1.name).toString());
                    v1_1.append(new StringBuffer(", type = ").append(org.xbill.DNS.Type.string(v3_1.type)).toString());
                    v1_1.append(new StringBuffer(", class = ").append(org.xbill.DNS.DClass.string(v3_1.dclass)).toString());
                }
                v1_1.append("\n");
                v0_1++;
            }
            v0_2 = v1_1.toString();
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public void setHeader(org.xbill.DNS.Header p1)
    {
        this.header = p1;
        return;
    }

    public void setTSIG(org.xbill.DNS.TSIG p1, int p2, org.xbill.DNS.TSIGRecord p3)
    {
        this.tsigkey = p1;
        this.tsigerror = p2;
        this.querytsig = p3;
        return;
    }

    public String toString()
    {
        StringBuffer v1_1 = new StringBuffer();
        if (this.getOPT() == null) {
            v1_1.append(new StringBuffer().append(this.header).append("\n").toString());
        } else {
            v1_1.append(new StringBuffer().append(this.header.toStringWithRcode(this.getRcode())).append("\n").toString());
        }
        if (this.isSigned()) {
            v1_1.append(";; TSIG ");
            if (!this.isVerified()) {
                v1_1.append("invalid");
            } else {
                v1_1.append("ok");
            }
            v1_1.append(10);
        }
        int v0_17 = 0;
        while (v0_17 < 4) {
            if (this.header.getOpcode() == 5) {
                v1_1.append(new StringBuffer(";; ").append(org.xbill.DNS.Section.updString(v0_17)).append(":\n").toString());
            } else {
                v1_1.append(new StringBuffer(";; ").append(org.xbill.DNS.Section.longString(v0_17)).append(":\n").toString());
            }
            v1_1.append(new StringBuffer().append(this.sectionToString(v0_17)).append("\n").toString());
            v0_17++;
        }
        v1_1.append(new StringBuffer(";; Message size: ").append(this.numBytes()).append(" bytes").toString());
        return v1_1.toString();
    }

    void toWire(org.xbill.DNS.DNSOutput p6)
    {
        this.header.toWire(p6);
        org.xbill.DNS.Compression v4_1 = new org.xbill.DNS.Compression();
        int v3 = 0;
        while (v3 < 4) {
            if (this.sections[v3] != null) {
                int v1 = 0;
                while (v1 < this.sections[v3].size()) {
                    ((org.xbill.DNS.Record) this.sections[v3].get(v1)).toWire(p6, v3, v4_1);
                    v1++;
                }
            }
            v3++;
        }
        return;
    }

    public byte[] toWire()
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        this.toWire(v0_1);
        this.size = v0_1.current();
        return v0_1.toByteArray();
    }

    public byte[] toWire(int p3)
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        this.toWire(v0_1, p3);
        this.size = v0_1.current();
        return v0_1.toByteArray();
    }
}
