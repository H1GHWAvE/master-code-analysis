package org.xbill.DNS;
public class InvalidDClassException extends java.lang.IllegalArgumentException {

    public InvalidDClassException(int p3)
    {
        this(new StringBuffer("Invalid DNS class: ").append(p3).toString());
        return;
    }
}
