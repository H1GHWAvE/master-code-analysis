package org.xbill.DNS;
public class Tokenizer$Token {
    public int type;
    public String value;

    private Tokenizer$Token()
    {
        this.type = -1;
        this.value = 0;
        return;
    }

    Tokenizer$Token(org.xbill.DNS.Tokenizer$1 p1)
    {
        return;
    }

    static org.xbill.DNS.Tokenizer$Token access$100(org.xbill.DNS.Tokenizer$Token p1, int p2, StringBuffer p3)
    {
        return p1.set(p2, p3);
    }

    private org.xbill.DNS.Tokenizer$Token set(int p2, StringBuffer p3)
    {
        if (p2 >= 0) {
            String v0_0;
            this.type = p2;
            if (p3 != null) {
                v0_0 = p3.toString();
            } else {
                v0_0 = 0;
            }
            this.value = v0_0;
            return this;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public boolean isEOL()
    {
        int v0 = 1;
        if ((this.type != 1) && (this.type != 0)) {
            v0 = 0;
        }
        return v0;
    }

    public boolean isString()
    {
        if ((this.type != 3) && (this.type != 4)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public String toString()
    {
        String v0_5;
        switch (this.type) {
            case 0:
                v0_5 = "<eof>";
                break;
            case 1:
                v0_5 = "<eol>";
                break;
            case 2:
                v0_5 = "<whitespace>";
                break;
            case 3:
                v0_5 = new StringBuffer("<identifier: ").append(this.value).append(">").toString();
                break;
            case 4:
                v0_5 = new StringBuffer("<quoted_string: ").append(this.value).append(">").toString();
                break;
            case 5:
                v0_5 = new StringBuffer("<comment: ").append(this.value).append(">").toString();
                break;
            default:
                v0_5 = "<unknown>";
        }
        return v0_5;
    }
}
