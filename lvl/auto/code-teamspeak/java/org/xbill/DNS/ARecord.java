package org.xbill.DNS;
public class ARecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 16274134872860409293;
    private int addr;

    ARecord()
    {
        return;
    }

    public ARecord(org.xbill.DNS.Name p8, int p9, long p10, java.net.InetAddress p12)
    {
        this(p8, 1, p9, p10);
        if (org.xbill.DNS.Address.familyOf(p12) == 1) {
            this.addr = org.xbill.DNS.ARecord.fromArray(p12.getAddress());
            return;
        } else {
            throw new IllegalArgumentException("invalid IPv4 address");
        }
    }

    private static final int fromArray(byte[] p2)
    {
        return (((((p2[0] & 255) << 24) | ((p2[1] & 255) << 16)) | ((p2[2] & 255) << 8)) | (p2[3] & 255));
    }

    private static final byte[] toArray(int p3)
    {
        byte[] v0_1 = new byte[4];
        v0_1[0] = ((byte) ((p3 >> 24) & 255));
        v0_1[1] = ((byte) ((p3 >> 16) & 255));
        v0_1[2] = ((byte) ((p3 >> 8) & 255));
        v0_1[3] = ((byte) (p3 & 255));
        return v0_1;
    }

    public java.net.InetAddress getAddress()
    {
        try {
            java.net.InetAddress v0_3;
            if (this.name != null) {
                v0_3 = java.net.InetAddress.getByAddress(this.name.toString(), org.xbill.DNS.ARecord.toArray(this.addr));
            } else {
                v0_3 = java.net.InetAddress.getByAddress(org.xbill.DNS.ARecord.toArray(this.addr));
            }
        } catch (java.net.InetAddress v0) {
            v0_3 = 0;
        }
        return v0_3;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.ARecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.addr = org.xbill.DNS.ARecord.fromArray(p2.getAddressBytes(1));
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.addr = org.xbill.DNS.ARecord.fromArray(p2.readByteArray(4));
        return;
    }

    String rrToString()
    {
        return org.xbill.DNS.Address.toDottedQuad(org.xbill.DNS.ARecord.toArray(this.addr));
    }

    void rrToWire(org.xbill.DNS.DNSOutput p5, org.xbill.DNS.Compression p6, boolean p7)
    {
        p5.writeU32((((long) this.addr) & 2.1219957905e-314));
        return;
    }
}
