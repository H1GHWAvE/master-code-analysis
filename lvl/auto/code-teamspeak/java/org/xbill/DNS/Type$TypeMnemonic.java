package org.xbill.DNS;
 class Type$TypeMnemonic extends org.xbill.DNS.Mnemonic {
    private java.util.HashMap objects;

    public Type$TypeMnemonic()
    {
        this("Type", 2);
        this.setPrefix("TYPE");
        this.objects = new java.util.HashMap();
        return;
    }

    public void add(int p3, String p4, org.xbill.DNS.Record p5)
    {
        super.add(p3, p4);
        this.objects.put(org.xbill.DNS.Mnemonic.toInteger(p3), p5);
        return;
    }

    public void check(int p1)
    {
        org.xbill.DNS.Type.check(p1);
        return;
    }

    public org.xbill.DNS.Record getProto(int p3)
    {
        this.check(p3);
        return ((org.xbill.DNS.Record) this.objects.get(org.xbill.DNS.Type$TypeMnemonic.toInteger(p3)));
    }
}
