package org.xbill.DNS;
public class URIRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 7955422413971804232;
    private int priority;
    private byte[] target;
    private int weight;

    URIRecord()
    {
        byte[] v0_1 = new byte[0];
        this.target = v0_1;
        return;
    }

    public URIRecord(org.xbill.DNS.Name p8, int p9, long p10, int p12, int p13, String p14)
    {
        this(p8, 256, p9, p10);
        this.priority = org.xbill.DNS.URIRecord.checkU16("priority", p12);
        this.weight = org.xbill.DNS.URIRecord.checkU16("weight", p13);
        try {
            this.target = org.xbill.DNS.URIRecord.byteArrayFromString(p14);
            return;
        } catch (String v0_6) {
            throw new IllegalArgumentException(v0_6.getMessage());
        }
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.URIRecord();
    }

    public int getPriority()
    {
        return this.priority;
    }

    public String getTarget()
    {
        return org.xbill.DNS.URIRecord.byteArrayToString(this.target, 0);
    }

    public int getWeight()
    {
        return this.weight;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.priority = p2.getUInt16();
        this.weight = p2.getUInt16();
        try {
            this.target = org.xbill.DNS.URIRecord.byteArrayFromString(p2.getString());
            return;
        } catch (org.xbill.DNS.TextParseException v0_4) {
            throw p2.exception(v0_4.getMessage());
        }
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.priority = p2.readU16();
        this.weight = p2.readU16();
        this.target = p2.readCountedString();
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(new StringBuffer().append(this.priority).append(" ").toString());
        v0_1.append(new StringBuffer().append(this.weight).append(" ").toString());
        v0_1.append(org.xbill.DNS.URIRecord.byteArrayToString(this.target, 1));
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeU16(this.priority);
        p2.writeU16(this.weight);
        p2.writeCountedString(this.target);
        return;
    }
}
