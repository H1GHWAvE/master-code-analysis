package org.xbill.DNS;
public interface ResolverListener implements java.util.EventListener {

    public abstract void handleException();

    public abstract void receiveMessage();
}
