package org.xbill.DNS;
public class Name implements java.io.Serializable, java.lang.Comparable {
    private static final int LABEL_COMPRESSION = 192;
    private static final int LABEL_MASK = 192;
    private static final int LABEL_NORMAL = 0;
    private static final int MAXLABEL = 63;
    private static final int MAXLABELS = 128;
    private static final int MAXNAME = 255;
    private static final int MAXOFFSETS = 7;
    private static final java.text.DecimalFormat byteFormat = None;
    public static final org.xbill.DNS.Name empty = None;
    private static final byte[] emptyLabel = None;
    private static final byte[] lowercase = None;
    public static final org.xbill.DNS.Name root = None;
    private static final long serialVersionUID = 11189724132738025972;
    private static final org.xbill.DNS.Name wild;
    private static final byte[] wildLabel;
    private int hashcode;
    private byte[] name;
    private long offsets;

    static Name()
    {
        int v0_0 = new byte[1];
        v0_0[0] = 0;
        org.xbill.DNS.Name.emptyLabel = v0_0;
        int v0_2 = new byte[2];
        v0_2 = {1, 42};
        org.xbill.DNS.Name.wildLabel = v0_2;
        org.xbill.DNS.Name.byteFormat = new java.text.DecimalFormat();
        int v0_6 = new byte[256];
        org.xbill.DNS.Name.lowercase = v0_6;
        org.xbill.DNS.Name.byteFormat.setMinimumIntegerDigits(3);
        int v0_8 = 0;
        while (v0_8 < org.xbill.DNS.Name.lowercase.length) {
            if ((v0_8 >= 65) && (v0_8 <= 90)) {
                org.xbill.DNS.Name.lowercase[v0_8] = ((byte) ((v0_8 - 65) + 97));
            } else {
                org.xbill.DNS.Name.lowercase[v0_8] = ((byte) v0_8);
            }
            v0_8++;
        }
        int v0_10 = new org.xbill.DNS.Name();
        org.xbill.DNS.Name.root = v0_10;
        v0_10.appendSafe(org.xbill.DNS.Name.emptyLabel, 0, 1);
        int v0_12 = new org.xbill.DNS.Name();
        org.xbill.DNS.Name.empty = v0_12;
        byte[] v2_4 = new byte[0];
        v0_12.name = v2_4;
        int v0_14 = new org.xbill.DNS.Name();
        org.xbill.DNS.Name.wild = v0_14;
        v0_14.appendSafe(org.xbill.DNS.Name.wildLabel, 0, 1);
        return;
    }

    private Name()
    {
        return;
    }

    public Name(String p2)
    {
        this(p2, 0);
        return;
    }

    public Name(String p12, org.xbill.DNS.Name p13)
    {
        if (!p12.equals("")) {
            if (!p12.equals("@")) {
                if (!p12.equals(".")) {
                    int v5_0 = -1;
                    int v4_0 = 1;
                    byte[] v9 = new byte[64];
                    byte v3_0 = 0;
                    int v2_0 = 0;
                    int v1_0 = 0;
                    byte[] v0_7 = 0;
                    while (v0_7 < p12.length()) {
                        byte v8 = ((byte) p12.charAt(v0_7));
                        if (v3_0 == 0) {
                            if (v8 != 92) {
                                if (v8 != 46) {
                                    int v6_5;
                                    if (v5_0 != -1) {
                                        v6_5 = v5_0;
                                    } else {
                                        v6_5 = v0_7;
                                    }
                                    if (v4_0 <= 63) {
                                        int v5_2 = (v4_0 + 1);
                                        v9[v4_0] = v8;
                                        v4_0 = v5_2;
                                        v5_0 = v6_5;
                                    } else {
                                        throw org.xbill.DNS.Name.parseException(p12, "label too long");
                                    }
                                } else {
                                    if (v5_0 != -1) {
                                        v9[0] = ((byte) (v4_0 - 1));
                                        this.appendFromString(p12, v9, 0, 1);
                                        v5_0 = -1;
                                        v4_0 = 1;
                                    } else {
                                        throw org.xbill.DNS.Name.parseException(p12, "invalid empty label");
                                    }
                                }
                            } else {
                                v3_0 = 1;
                                v2_0 = 0;
                                v1_0 = 0;
                            }
                        } else {
                            if ((v8 < 48) || ((v8 > 57) || (v2_0 >= 3))) {
                                if ((v2_0 <= 0) || (v2_0 >= 3)) {
                                    byte v3_2 = v8;
                                } else {
                                    throw org.xbill.DNS.Name.parseException(p12, "bad escape");
                                }
                            } else {
                                v2_0++;
                                v1_0 = ((v1_0 * 10) + (v8 - 48));
                                if (v1_0 <= 255) {
                                    if (v2_0 < 3) {
                                        v0_7++;
                                    } else {
                                        v3_2 = ((byte) v1_0);
                                    }
                                } else {
                                    throw org.xbill.DNS.Name.parseException(p12, "bad escape");
                                }
                            }
                            if (v4_0 <= 63) {
                                int v5_6 = (v4_0 + 1);
                                v9[v4_0] = v3_2;
                                v3_0 = 0;
                                v5_0 = v4_0;
                                v4_0 = v5_6;
                            } else {
                                throw org.xbill.DNS.Name.parseException(p12, "label too long");
                            }
                        }
                    }
                    if ((v2_0 <= 0) || (v2_0 >= 3)) {
                        if (v3_0 == 0) {
                            byte[] v0_12;
                            if (v5_0 != -1) {
                                v9[0] = ((byte) (v4_0 - 1));
                                this.appendFromString(p12, v9, 0, 1);
                                v0_12 = 0;
                            } else {
                                this.appendFromString(p12, org.xbill.DNS.Name.emptyLabel, 0, 1);
                                v0_12 = 1;
                            }
                            if ((p13 != null) && (v0_12 == null)) {
                                this.appendFromString(p12, p13.name, p13.offset(0), p13.getlabels());
                            }
                        } else {
                            throw org.xbill.DNS.Name.parseException(p12, "bad escape");
                        }
                    } else {
                        throw org.xbill.DNS.Name.parseException(p12, "bad escape");
                    }
                } else {
                    org.xbill.DNS.Name.copy(org.xbill.DNS.Name.root, this);
                }
            } else {
                if (p13 != null) {
                    org.xbill.DNS.Name.copy(p13, this);
                } else {
                    org.xbill.DNS.Name.copy(org.xbill.DNS.Name.empty, this);
                }
            }
            return;
        } else {
            throw org.xbill.DNS.Name.parseException(p12, "empty name");
        }
    }

    public Name(org.xbill.DNS.DNSInput p10)
    {
        byte[] v4 = new byte[64];
        String v0_1 = 0;
        String v3_0 = 0;
        while (v3_0 == null) {
            String v5_0 = p10.readU8();
            switch ((v5_0 & 192)) {
                case 0:
                    if (this.getlabels() < 128) {
                        if (v5_0 != null) {
                            v4[0] = ((byte) v5_0);
                            p10.readByteArray(v4, 1, v5_0);
                            this.append(v4, 0, 1);
                        } else {
                            this.append(org.xbill.DNS.Name.emptyLabel, 0, 1);
                            v3_0 = 1;
                        }
                    } else {
                        throw new org.xbill.DNS.WireParseException("too many labels");
                    }
                    break;
                case 192:
                    String v5_3 = (((v5_0 & -193) << 8) + p10.readU8());
                    if (org.xbill.DNS.Options.check("verbosecompression")) {
                        System.err.println(new StringBuffer("currently ").append(p10.current()).append(", pointer to ").append(v5_3).toString());
                    }
                    if (v5_3 < (p10.current() - 2)) {
                        if (v0_1 == null) {
                            p10.save();
                            v0_1 = 1;
                        }
                        p10.jump(v5_3);
                        if (org.xbill.DNS.Options.check("verbosecompression")) {
                            System.err.println(new StringBuffer("current name \'").append(this).append("\', seeking to ").append(v5_3).toString());
                        }
                    } else {
                        throw new org.xbill.DNS.WireParseException("bad compression");
                    }
                    break;
                default:
                    throw new org.xbill.DNS.WireParseException("bad label type");
            }
        }
        if (v0_1 != null) {
            p10.restore();
        }
        return;
    }

    public Name(org.xbill.DNS.Name p4, int p5)
    {
        String v1_0 = p4.labels();
        if (p5 <= v1_0) {
            this.name = p4.name;
            this.setlabels((v1_0 - p5));
            int v0_2 = 0;
            while ((v0_2 < 7) && (v0_2 < (v1_0 - p5))) {
                this.setoffset(v0_2, p4.offset((v0_2 + p5)));
                v0_2++;
            }
            return;
        } else {
            throw new IllegalArgumentException("attempted to remove too many labels");
        }
    }

    public Name(byte[] p2)
    {
        this(new org.xbill.DNS.DNSInput(p2));
        return;
    }

    private final void append(byte[] p9, int p10, int p11)
    {
        int v0_3;
        int v1_0 = 0;
        if (this.name != null) {
            v0_3 = (this.name.length - this.offset(0));
        } else {
            v0_3 = 0;
        }
        byte[] v2_1 = p10;
        int v3_0 = 0;
        int v4_0 = 0;
        while (v3_0 < p11) {
            int v5_1 = p9[v2_1];
            if (v5_1 <= 63) {
                int v5_2 = (v5_1 + 1);
                v2_1 += v5_2;
                v4_0 += v5_2;
                v3_0++;
            } else {
                throw new IllegalStateException("invalid label");
            }
        }
        byte[] v2_2 = (v0_3 + v4_0);
        if (v2_2 <= 255) {
            int v3_2 = this.getlabels();
            int v5_0 = (v3_2 + p11);
            if (v5_0 <= 128) {
                byte[] v2_3 = new byte[v2_2];
                if (v0_3 != 0) {
                    System.arraycopy(this.name, this.offset(0), v2_3, 0, v0_3);
                }
                System.arraycopy(p9, p10, v2_3, v0_3, v4_0);
                this.name = v2_3;
                while (v1_0 < p11) {
                    this.setoffset((v3_2 + v1_0), v0_3);
                    v0_3 += (v2_3[v0_3] + 1);
                    v1_0++;
                }
                this.setlabels(v5_0);
                return;
            } else {
                throw new IllegalStateException("too many labels");
            }
        } else {
            throw new org.xbill.DNS.NameTooLongException();
        }
    }

    private final void appendFromString(String p2, byte[] p3, int p4, int p5)
    {
        try {
            this.append(p3, p4, p5);
            return;
        } catch (org.xbill.DNS.TextParseException v0) {
            throw org.xbill.DNS.Name.parseException(p2, "Name too long");
        }
    }

    private final void appendSafe(byte[] p2, int p3, int p4)
    {
        try {
            this.append(p2, p3, p4);
        } catch (org.xbill.DNS.NameTooLongException v0) {
        }
        return;
    }

    private String byteString(byte[] p10, int p11)
    {
        StringBuffer v2_1 = new StringBuffer();
        int v1 = (p11 + 1);
        byte v3 = p10[p11];
        int v0_0 = v1;
        while (v0_0 < (v1 + v3)) {
            char v4_2 = (p10[v0_0] & 255);
            if ((v4_2 > 32) && (v4_2 < 127)) {
                if ((v4_2 != 34) && ((v4_2 != 40) && ((v4_2 != 41) && ((v4_2 != 46) && ((v4_2 != 59) && ((v4_2 != 92) && ((v4_2 != 64) && (v4_2 != 36)))))))) {
                    v2_1.append(((char) v4_2));
                } else {
                    v2_1.append(92);
                    v2_1.append(((char) v4_2));
                }
            } else {
                v2_1.append(92);
                v2_1.append(org.xbill.DNS.Name.byteFormat.format(((long) v4_2)));
            }
            v0_0++;
        }
        return v2_1.toString();
    }

    public static org.xbill.DNS.Name concatenate(org.xbill.DNS.Name p4, org.xbill.DNS.Name p5)
    {
        if (!p4.isAbsolute()) {
            void v0_2 = new org.xbill.DNS.Name();
            org.xbill.DNS.Name.copy(p4, v0_2);
            p4 = v0_2.append(p5.name, p5.offset(0), p5.getlabels());
        }
        return p4;
    }

    private static final void copy(org.xbill.DNS.Name p6, org.xbill.DNS.Name p7)
    {
        int v0_0 = 0;
        if (p6.offset(0) != 0) {
            int v1_1 = p6.offset(0);
            int v2_2 = (p6.name.length - v1_1);
            int v3 = p6.labels();
            byte[] v4_0 = new byte[v2_2];
            p7.name = v4_0;
            System.arraycopy(p6.name, v1_1, p7.name, 0, v2_2);
            while ((v0_0 < v3) && (v0_0 < 7)) {
                p7 = p7.setoffset(v0_0, (p6.offset(v0_0) - v1_1));
                v0_0++;
            }
            p7.setlabels(v3);
        } else {
            p7.name = p6.name;
            p7.offsets = p6.offsets;
        }
        return;
    }

    private final boolean equals(byte[] p11, int p12)
    {
        IllegalStateException v0_0 = 0;
        int v6 = this.labels();
        int v2_0 = this.offset(0);
        int v5 = 0;
        while (v5 < v6) {
            if (this.name[v2_0] == p11[p12]) {
                byte v7 = this.name[v2_0];
                if (v7 <= 63) {
                    int v4_0 = (p12 + 1);
                    v2_0++;
                    int v1_3 = 0;
                    while (v1_3 < v7) {
                        if (org.xbill.DNS.Name.lowercase[(this.name[v2_0] & 255)] == org.xbill.DNS.Name.lowercase[(p11[v4_0] & 255)]) {
                            v1_3++;
                            v4_0++;
                            v2_0++;
                        }
                    }
                    v5++;
                    p12 = v4_0;
                } else {
                    throw new IllegalStateException("invalid label");
                }
            }
            return v0_0;
        }
        v0_0 = 1;
        return v0_0;
    }

    public static org.xbill.DNS.Name fromConstantString(String p3)
    {
        try {
            return org.xbill.DNS.Name.fromString(p3, 0);
        } catch (IllegalArgumentException v0) {
            throw new IllegalArgumentException(new StringBuffer("Invalid name \'").append(p3).append("\'").toString());
        }
    }

    public static org.xbill.DNS.Name fromString(String p1)
    {
        return org.xbill.DNS.Name.fromString(p1, 0);
    }

    public static org.xbill.DNS.Name fromString(String p1, org.xbill.DNS.Name p2)
    {
        if ((!p1.equals("@")) || (p2 == null)) {
            if (!p1.equals(".")) {
                p2 = new org.xbill.DNS.Name(p1, p2);
            } else {
                p2 = org.xbill.DNS.Name.root;
            }
        }
        return p2;
    }

    private final int getlabels()
    {
        return ((int) (this.offsets & 255));
    }

    private final int offset(int p6)
    {
        if ((p6 != 0) || (this.getlabels() != 0)) {
            if ((p6 >= 0) && (p6 < this.getlabels())) {
                if (p6 >= 7) {
                    int v0_1 = this.offset(6);
                    int v1_4 = 6;
                    while (v1_4 < p6) {
                        v1_4++;
                        v0_1 = ((this.name[v0_1] + 1) + v0_1);
                    }
                } else {
                    v0_1 = (((int) (this.offsets >> ((7 - p6) * 8))) & 255);
                }
            } else {
                throw new IllegalArgumentException("label out of range");
            }
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private static org.xbill.DNS.TextParseException parseException(String p3, String p4)
    {
        return new org.xbill.DNS.TextParseException(new StringBuffer("\'").append(p3).append("\': ").append(p4).toString());
    }

    private final void setlabels(int p5)
    {
        this.offsets = (this.offsets & -256);
        this.offsets = (this.offsets | ((long) p5));
        return;
    }

    private final void setoffset(int p9, int p10)
    {
        if (p9 < 7) {
            long v0_2 = ((7 - p9) * 8);
            this.offsets = (this.offsets & ((255 << v0_2) ^ -1));
            this.offsets = ((((long) p10) << v0_2) | this.offsets);
        }
        return;
    }

    public org.xbill.DNS.Name canonicalize()
    {
        int v1 = 0;
        void v0_0 = 0;
        while (v0_0 < this.name.length) {
            if (org.xbill.DNS.Name.lowercase[(this.name[v0_0] & 255)] == this.name[v0_0]) {
                v0_0++;
            } else {
                void v0_1 = 0;
            }
            if (v0_1 == 0) {
                void v0_4 = new org.xbill.DNS.Name().appendSafe(this.name, this.offset(0), this.getlabels());
                while (v1 < v0_4.name.length) {
                    v0_4.name[v1] = org.xbill.DNS.Name.lowercase[(v0_4.name[v1] & 255)];
                    v1++;
                }
                this = v0_4;
            }
            return this;
        }
        v0_1 = 1;
    }

    public int compareTo(Object p15)
    {
        int v0_1;
        if (this != ((org.xbill.DNS.Name) p15)) {
            int v0_0;
            int v2 = this.labels();
            int v1 = ((org.xbill.DNS.Name) p15).labels();
            if (v2 <= v1) {
                v0_0 = v2;
            } else {
                v0_0 = v1;
            }
            int v6 = 1;
            while (v6 <= v0_0) {
                int v7 = this.offset((v2 - v6));
                int v8 = ((org.xbill.DNS.Name) p15).offset((v1 - v6));
                byte v9 = this.name[v7];
                byte v10 = ((org.xbill.DNS.Name) p15).name[v8];
                int v5 = 0;
                while ((v5 < v9) && (v5 < v10)) {
                    int v4_8 = (org.xbill.DNS.Name.lowercase[(this.name[((v5 + v7) + 1)] & 255)] - org.xbill.DNS.Name.lowercase[(((org.xbill.DNS.Name) p15).name[((v5 + v8) + 1)] & 255)]);
                    if (v4_8 == 0) {
                        v5++;
                    } else {
                        v0_1 = v4_8;
                    }
                }
                if (v9 == v10) {
                    v6++;
                } else {
                    v0_1 = (v9 - v10);
                }
            }
            v0_1 = (v2 - v1);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public boolean equals(Object p4)
    {
        boolean v0_0 = 0;
        if (p4 != this) {
            if ((p4 != null) && ((p4 instanceof org.xbill.DNS.Name))) {
                if (((org.xbill.DNS.Name) p4).hashcode == 0) {
                    ((org.xbill.DNS.Name) p4).hashCode();
                }
                if (this.hashcode == 0) {
                    this.hashCode();
                }
                if ((((org.xbill.DNS.Name) p4).hashcode == this.hashcode) && (((org.xbill.DNS.Name) p4).labels() == this.labels())) {
                    v0_0 = this.equals(((org.xbill.DNS.Name) p4).name, ((org.xbill.DNS.Name) p4).offset(0));
                }
            }
        } else {
            v0_0 = 1;
        }
        return v0_0;
    }

    public org.xbill.DNS.Name fromDNAME(org.xbill.DNS.DNAMERecord p11)
    {
        void v0_5;
        int v1 = 0;
        void v0_0 = p11.getName();
        int v2_0 = p11.getTarget();
        if (this.subdomain(v0_0)) {
            int v3_2 = (this.labels() - v0_0.labels());
            int v4_2 = (this.length() - v0_0.length());
            byte[] v5_0 = this.offset(0);
            int v6 = v2_0.labels();
            short v7 = v2_0.length();
            if ((v4_2 + v7) <= 255) {
                v0_5 = new org.xbill.DNS.Name().setlabels((v3_2 + v6));
                byte[] v8_3 = new byte[(v4_2 + v7)];
                v0_5.name = v8_3;
                System.arraycopy(this.name, v5_0, v0_5.name, 0, v4_2);
                System.arraycopy(v2_0.name, 0, v0_5.name, v4_2, v7);
                int v2_2 = 0;
                while ((v2_2 < 7) && (v2_2 < (v3_2 + v6))) {
                    v0_5 = v0_5.setoffset(v2_2, v1);
                    v1 += (v0_5.name[v1] + 1);
                    v2_2++;
                }
            } else {
                throw new org.xbill.DNS.NameTooLongException();
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    public byte[] getLabel(int p6)
    {
        int v0 = this.offset(p6);
        byte v1_3 = ((byte) (this.name[v0] + 1));
        byte[] v2 = new byte[v1_3];
        System.arraycopy(this.name, v0, v2, 0, v1_3);
        return v2;
    }

    public String getLabelString(int p3)
    {
        return this.byteString(this.name, this.offset(p3));
    }

    public int hashCode()
    {
        int v0_2;
        int v1 = 0;
        if (this.hashcode == 0) {
            int v0_1 = this.offset(0);
            while (v0_1 < this.name.length) {
                v1 += ((v1 << 3) + org.xbill.DNS.Name.lowercase[(this.name[v0_1] & 255)]);
                v0_1++;
            }
            this.hashcode = v1;
            v0_2 = this.hashcode;
        } else {
            v0_2 = this.hashcode;
        }
        return v0_2;
    }

    public boolean isAbsolute()
    {
        int v0 = 0;
        byte v1_0 = this.labels();
        if ((v1_0 != 0) && (this.name[this.offset((v1_0 - 1))] == 0)) {
            v0 = 1;
        }
        return v0;
    }

    public boolean isWild()
    {
        int v0 = 0;
        if ((this.labels() != 0) && ((this.name[0] == 1) && (this.name[1] == 42))) {
            v0 = 1;
        }
        return v0;
    }

    public int labels()
    {
        return this.getlabels();
    }

    public short length()
    {
        short v0_0 = 0;
        if (this.getlabels() != 0) {
            v0_0 = ((short) (this.name.length - this.offset(0)));
        }
        return v0_0;
    }

    public org.xbill.DNS.Name relativize(org.xbill.DNS.Name p7)
    {
        if ((p7 != null) && (this.subdomain(p7))) {
            void v0_2 = new org.xbill.DNS.Name();
            org.xbill.DNS.Name.copy(this, v0_2);
            int v1_1 = (this.length() - p7.length());
            void v0_3 = v0_2.setlabels((v0_2.labels() - p7.labels()));
            byte[] v2_3 = new byte[v1_1];
            v0_3.name = v2_3;
            System.arraycopy(this.name, this.offset(0), v0_3.name, 0, v1_1);
            this = v0_3;
        }
        return this;
    }

    public boolean subdomain(org.xbill.DNS.Name p4)
    {
        boolean v0_3;
        boolean v0_0 = this.labels();
        int v1 = p4.labels();
        if (v1 <= v0_0) {
            if (v1 != v0_0) {
                v0_3 = p4.equals(this.name, this.offset((v0_0 - v1)));
            } else {
                v0_3 = this.equals(p4);
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public String toString()
    {
        return this.toString(0);
    }

    public String toString(boolean p8)
    {
        int v0_4;
        int v1_0 = 0;
        int v2 = this.labels();
        if (v2 != 0) {
            if ((v2 != 1) || (this.name[this.offset(0)] != 0)) {
                StringBuffer v3_2 = new StringBuffer();
                int v0_3 = this.offset(0);
                while (v1_0 < v2) {
                    int v4_1 = this.name[v0_3];
                    if (v4_1 <= 63) {
                        if (v4_1 != 0) {
                            if (v1_0 > 0) {
                                v3_2.append(46);
                            }
                            v3_2.append(this.byteString(this.name, v0_3));
                            v0_3 += (v4_1 + 1);
                            v1_0++;
                        } else {
                            if (p8) {
                                break;
                            }
                            v3_2.append(46);
                            break;
                        }
                    } else {
                        throw new IllegalStateException("invalid label");
                    }
                }
                v0_4 = v3_2.toString();
            } else {
                v0_4 = ".";
            }
        } else {
            v0_4 = "@";
        }
        return v0_4;
    }

    public void toWire(org.xbill.DNS.DNSOutput p7, org.xbill.DNS.Compression p8)
    {
        if (this.isAbsolute()) {
            int v4 = this.labels();
            int v2 = 0;
            while (v2 < (v4 - 1)) {
                byte[] v1_0;
                if (v2 != 0) {
                    v1_0 = new org.xbill.DNS.Name(this, v2);
                } else {
                    v1_0 = this;
                }
                int v0_4 = -1;
                if (p8 != null) {
                    v0_4 = p8.get(v1_0);
                }
                if (v0_4 < 0) {
                    if (p8 != null) {
                        p8.add(p7.current(), v1_0);
                    }
                    int v0_6 = this.offset(v2);
                    p7.writeByteArray(this.name, v0_6, (this.name[v0_6] + 1));
                    v2++;
                } else {
                    p7.writeU16((v0_4 | 49152));
                }
                return;
            }
            p7.writeU8(0);
            return;
        } else {
            throw new IllegalArgumentException("toWire() called on non-absolute name");
        }
    }

    public void toWire(org.xbill.DNS.DNSOutput p1, org.xbill.DNS.Compression p2, boolean p3)
    {
        if (!p3) {
            this.toWire(p1, p2);
        } else {
            this.toWireCanonical(p1);
        }
        return;
    }

    public byte[] toWire()
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        this.toWire(v0_1, 0);
        return v0_1.toByteArray();
    }

    public void toWireCanonical(org.xbill.DNS.DNSOutput p2)
    {
        p2.writeByteArray(this.toWireCanonical());
        return;
    }

    public byte[] toWireCanonical()
    {
        IllegalStateException v0_3;
        int v8 = this.labels();
        if (v8 != 0) {
            v0_3 = new byte[(this.name.length - this.offset(0))];
            int v5_0 = this.offset(0);
            int v4_0 = 0;
            int v7 = 0;
            while (v7 < v8) {
                byte v9 = this.name[v5_0];
                if (v9 <= 63) {
                    int v1_3 = (v4_0 + 1);
                    int v3_0 = (v5_0 + 1);
                    v0_3[v4_0] = this.name[v5_0];
                    v5_0 = v3_0;
                    int v3_1 = v1_3;
                    int v1_4 = 0;
                    while (v1_4 < v9) {
                        int v4_1 = (v3_1 + 1);
                        int v6_1 = (v5_0 + 1);
                        v0_3[v3_1] = org.xbill.DNS.Name.lowercase[(this.name[v5_0] & 255)];
                        v1_4++;
                        v3_1 = v4_1;
                        v5_0 = v6_1;
                    }
                    v4_0 = v3_1;
                    v7++;
                } else {
                    throw new IllegalStateException("invalid label");
                }
            }
        } else {
            v0_3 = new byte[0];
        }
        return v0_3;
    }

    public org.xbill.DNS.Name wild(int p5)
    {
        if (p5 > 0) {
            try {
                IllegalStateException v0_1 = new org.xbill.DNS.Name();
                org.xbill.DNS.Name.copy(org.xbill.DNS.Name.wild, v0_1);
                return v0_1.append(this.name, this.offset(p5), (this.getlabels() - p5));
            } catch (IllegalStateException v0) {
                throw new IllegalStateException("Name.wild: concatenate failed");
            }
        } else {
            throw new IllegalArgumentException("must replace 1 or more labels");
        }
    }
}
