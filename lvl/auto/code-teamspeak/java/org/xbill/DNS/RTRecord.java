package org.xbill.DNS;
public class RTRecord extends org.xbill.DNS.U16NameBase {
    private static final long serialVersionUID = 15240528422061273518;

    RTRecord()
    {
        return;
    }

    public RTRecord(org.xbill.DNS.Name p12, int p13, long p14, int p16, org.xbill.DNS.Name p17)
    {
        this(p12, 21, p13, p14, p16, "preference", p17, "intermediateHost");
        return;
    }

    public org.xbill.DNS.Name getIntermediateHost()
    {
        return this.getNameField();
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.RTRecord();
    }

    public int getPreference()
    {
        return this.getU16Field();
    }
}
