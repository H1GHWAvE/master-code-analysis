package org.xbill.DNS;
public class IPSECKEYRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 3050449702765909687;
    private int algorithmType;
    private Object gateway;
    private int gatewayType;
    private byte[] key;
    private int precedence;

    IPSECKEYRecord()
    {
        return;
    }

    public IPSECKEYRecord(org.xbill.DNS.Name p10, int p11, long p12, int p14, int p15, int p16, Object p17, byte[] p18)
    {
        this(p10, 45, p11, p12);
        this.precedence = org.xbill.DNS.IPSECKEYRecord.checkU8("precedence", p14);
        this.gatewayType = org.xbill.DNS.IPSECKEYRecord.checkU8("gatewayType", p15);
        this.algorithmType = org.xbill.DNS.IPSECKEYRecord.checkU8("algorithmType", p16);
        switch (p15) {
            case 0:
                this.gateway = 0;
                break;
            case 1:
                if ((p17 instanceof java.net.InetAddress)) {
                    this.gateway = p17;
                } else {
                    throw new IllegalArgumentException("\"gateway\" must be an IPv4 address");
                }
                break;
            case 2:
                if ((p17 instanceof java.net.Inet6Address)) {
                    this.gateway = p17;
                } else {
                    throw new IllegalArgumentException("\"gateway\" must be an IPv6 address");
                }
                break;
            case 3:
                if ((p17 instanceof org.xbill.DNS.Name)) {
                    this.gateway = org.xbill.DNS.IPSECKEYRecord.checkName("gateway", ((org.xbill.DNS.Name) p17));
                } else {
                    throw new IllegalArgumentException("\"gateway\" must be a DNS name");
                }
                break;
            default:
                throw new IllegalArgumentException("\"gatewayType\" must be between 0 and 3");
        }
        this.key = p18;
        return;
    }

    public int getAlgorithmType()
    {
        return this.algorithmType;
    }

    public Object getGateway()
    {
        return this.gateway;
    }

    public int getGatewayType()
    {
        return this.gatewayType;
    }

    public byte[] getKey()
    {
        return this.key;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.IPSECKEYRecord();
    }

    public int getPrecedence()
    {
        return this.precedence;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p3, org.xbill.DNS.Name p4)
    {
        this.precedence = p3.getUInt8();
        this.gatewayType = p3.getUInt8();
        this.algorithmType = p3.getUInt8();
        switch (this.gatewayType) {
            case 0:
                if (p3.getString().equals(".")) {
                    this.gateway = 0;
                } else {
                    throw new org.xbill.DNS.TextParseException("invalid gateway format");
                }
                break;
            case 1:
                this.gateway = p3.getAddress(1);
                break;
            case 2:
                this.gateway = p3.getAddress(2);
                break;
            case 3:
                this.gateway = p3.getName(p4);
                break;
            default:
                throw new org.xbill.DNS.WireParseException("invalid gateway type");
        }
        this.key = p3.getBase64(0);
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p3)
    {
        this.precedence = p3.readU8();
        this.gatewayType = p3.readU8();
        this.algorithmType = p3.readU8();
        switch (this.gatewayType) {
            case 0:
                this.gateway = 0;
                break;
            case 1:
                this.gateway = java.net.InetAddress.getByAddress(p3.readByteArray(4));
                break;
            case 2:
                this.gateway = java.net.InetAddress.getByAddress(p3.readByteArray(16));
                break;
            case 3:
                this.gateway = new org.xbill.DNS.Name(p3);
                break;
            default:
                throw new org.xbill.DNS.WireParseException("invalid gateway type");
        }
        if (p3.remaining() > 0) {
            this.key = p3.readByteArray();
        }
        return;
    }

    String rrToString()
    {
        StringBuffer v1_1 = new StringBuffer();
        v1_1.append(this.precedence);
        v1_1.append(" ");
        v1_1.append(this.gatewayType);
        v1_1.append(" ");
        v1_1.append(this.algorithmType);
        v1_1.append(" ");
        switch (this.gatewayType) {
            case 0:
                v1_1.append(".");
                break;
            case 1:
            case 2:
                v1_1.append(((java.net.InetAddress) this.gateway).getHostAddress());
                break;
            case 3:
                v1_1.append(this.gateway);
                break;
        }
        if (this.key != null) {
            v1_1.append(" ");
            v1_1.append(org.xbill.DNS.utils.base64.toString(this.key));
        }
        return v1_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p3, org.xbill.DNS.Compression p4, boolean p5)
    {
        p3.writeU8(this.precedence);
        p3.writeU8(this.gatewayType);
        p3.writeU8(this.algorithmType);
        switch (this.gatewayType) {
            case 0:
            default:
                break;
            case 1:
            case 2:
                p3.writeByteArray(((java.net.InetAddress) this.gateway).getAddress());
                break;
            case 3:
                ((org.xbill.DNS.Name) this.gateway).toWire(p3, 0, p5);
                break;
        }
        if (this.key != null) {
            p3.writeByteArray(this.key);
        }
        return;
    }
}
