package org.xbill.DNS;
public class NSRecord extends org.xbill.DNS.SingleCompressedNameBase {
    private static final long serialVersionUID = 487170758138268838;

    NSRecord()
    {
        return;
    }

    public NSRecord(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Name p14)
    {
        this(p10, 2, p11, p12, p14, "target");
        return;
    }

    public org.xbill.DNS.Name getAdditionalName()
    {
        return this.getSingleName();
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.NSRecord();
    }

    public org.xbill.DNS.Name getTarget()
    {
        return this.getSingleName();
    }
}
