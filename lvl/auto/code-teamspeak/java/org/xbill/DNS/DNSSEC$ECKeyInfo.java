package org.xbill.DNS;
 class DNSSEC$ECKeyInfo {
    public java.math.BigInteger a;
    public java.math.BigInteger b;
    java.security.spec.EllipticCurve curve;
    public java.math.BigInteger gx;
    public java.math.BigInteger gy;
    int length;
    public java.math.BigInteger n;
    public java.math.BigInteger p;
    java.security.spec.ECParameterSpec spec;

    DNSSEC$ECKeyInfo(int p6, String p7, String p8, String p9, String p10, String p11, String p12)
    {
        this.length = p6;
        this.p = new java.math.BigInteger(p7, 16);
        this.a = new java.math.BigInteger(p8, 16);
        this.b = new java.math.BigInteger(p9, 16);
        this.gx = new java.math.BigInteger(p10, 16);
        this.gy = new java.math.BigInteger(p11, 16);
        this.n = new java.math.BigInteger(p12, 16);
        this.curve = new java.security.spec.EllipticCurve(new java.security.spec.ECFieldFp(this.p), this.a, this.b);
        this.spec = new java.security.spec.ECParameterSpec(this.curve, new java.security.spec.ECPoint(this.gx, this.gy), this.n, 1);
        return;
    }
}
