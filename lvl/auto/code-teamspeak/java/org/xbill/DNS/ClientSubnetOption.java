package org.xbill.DNS;
public class ClientSubnetOption extends org.xbill.DNS.EDNSOption {
    private static final long serialVersionUID = 14578585623819285269;
    private java.net.InetAddress address;
    private int family;
    private int scopeNetmask;
    private int sourceNetmask;

    ClientSubnetOption()
    {
        this(8);
        return;
    }

    public ClientSubnetOption(int p3, int p4, java.net.InetAddress p5)
    {
        this(8);
        this.family = org.xbill.DNS.Address.familyOf(p5);
        this.sourceNetmask = org.xbill.DNS.ClientSubnetOption.checkMaskLength("source netmask", this.family, p3);
        this.scopeNetmask = org.xbill.DNS.ClientSubnetOption.checkMaskLength("scope netmask", this.family, p4);
        this.address = org.xbill.DNS.Address.truncate(p5, p3);
        if (p5.equals(this.address)) {
            return;
        } else {
            throw new IllegalArgumentException("source netmask is not valid for address");
        }
    }

    public ClientSubnetOption(int p2, java.net.InetAddress p3)
    {
        this(p2, 0, p3);
        return;
    }

    private static int checkMaskLength(String p4, int p5, int p6)
    {
        String v0_1 = (org.xbill.DNS.Address.addressLength(p5) * 8);
        if ((p6 >= 0) && (p6 <= v0_1)) {
            return p6;
        } else {
            throw new IllegalArgumentException(new StringBuffer("\"").append(p4).append("\" ").append(p6).append(" must be in the range [0..").append(v0_1).append("]").toString());
        }
    }

    public java.net.InetAddress getAddress()
    {
        return this.address;
    }

    public int getFamily()
    {
        return this.family;
    }

    public int getScopeNetmask()
    {
        return this.scopeNetmask;
    }

    public int getSourceNetmask()
    {
        return this.sourceNetmask;
    }

    void optionFromWire(org.xbill.DNS.DNSInput p5)
    {
        this.family = p5.readU16();
        if ((this.family == 1) || (this.family == 2)) {
            this.sourceNetmask = p5.readU8();
            if (this.sourceNetmask <= (org.xbill.DNS.Address.addressLength(this.family) * 8)) {
                this.scopeNetmask = p5.readU8();
                if (this.scopeNetmask <= (org.xbill.DNS.Address.addressLength(this.family) * 8)) {
                    org.xbill.DNS.WireParseException v0_7 = p5.readByteArray();
                    if (v0_7.length == ((this.sourceNetmask + 7) / 8)) {
                        String v1_11 = new byte[org.xbill.DNS.Address.addressLength(this.family)];
                        System.arraycopy(v0_7, 0, v1_11, 0, v0_7.length);
                        try {
                            this.address = java.net.InetAddress.getByAddress(v1_11);
                        } catch (org.xbill.DNS.WireParseException v0_12) {
                            throw new org.xbill.DNS.WireParseException("invalid address", v0_12);
                        }
                        if (org.xbill.DNS.Address.truncate(this.address, this.sourceNetmask).equals(this.address)) {
                            return;
                        } else {
                            throw new org.xbill.DNS.WireParseException("invalid padding");
                        }
                    } else {
                        throw new org.xbill.DNS.WireParseException("invalid address");
                    }
                } else {
                    throw new org.xbill.DNS.WireParseException("invalid scope netmask");
                }
            } else {
                throw new org.xbill.DNS.WireParseException("invalid source netmask");
            }
        } else {
            throw new org.xbill.DNS.WireParseException("unknown address family");
        }
    }

    String optionToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.address.getHostAddress());
        v0_1.append("/");
        v0_1.append(this.sourceNetmask);
        v0_1.append(", scope netmask ");
        v0_1.append(this.scopeNetmask);
        return v0_1.toString();
    }

    void optionToWire(org.xbill.DNS.DNSOutput p4)
    {
        p4.writeU16(this.family);
        p4.writeU8(this.sourceNetmask);
        p4.writeU8(this.scopeNetmask);
        p4.writeByteArray(this.address.getAddress(), 0, ((this.sourceNetmask + 7) / 8));
        return;
    }
}
