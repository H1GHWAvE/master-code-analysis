package org.xbill.DNS;
 class Client {
    private static org.xbill.DNS.PacketLogger packetLogger;
    protected long endTime;
    protected java.nio.channels.SelectionKey key;

    static Client()
    {
        org.xbill.DNS.Client.packetLogger = 0;
        return;
    }

    protected Client(java.nio.channels.SelectableChannel p3, long p4)
    {
        this.endTime = p4;
        try {
            java.nio.channels.Selector v1 = java.nio.channels.Selector.open();
            p3.configureBlocking(0);
            this.key = p3.register(v1, 1);
            return;
        } catch (Throwable v0_3) {
            if (v1 != null) {
                v1.close();
            }
            p3.close();
            throw v0_3;
        }
    }

    protected static void blockUntil(java.nio.channels.SelectionKey p7, long p8)
    {
        long v2 = (p8 - System.currentTimeMillis());
        java.net.SocketTimeoutException v0_1 = 0;
        if (v2 <= 0) {
            if (v2 == 0) {
                v0_1 = p7.selector().selectNow();
            }
        } else {
            v0_1 = p7.selector().select(v2);
        }
        if (v0_1 != null) {
            return;
        } else {
            throw new java.net.SocketTimeoutException();
        }
    }

    static void setPacketLogger(org.xbill.DNS.PacketLogger p0)
    {
        org.xbill.DNS.Client.packetLogger = p0;
        return;
    }

    protected static void verboseLog(String p2, java.net.SocketAddress p3, java.net.SocketAddress p4, byte[] p5)
    {
        if (org.xbill.DNS.Options.check("verbosemsg")) {
            System.err.println(org.xbill.DNS.utils.hexdump.dump(p2, p5));
        }
        if (org.xbill.DNS.Client.packetLogger != null) {
            org.xbill.DNS.Client.packetLogger.log(p2, p3, p4, p5);
        }
        return;
    }

    void cleanup()
    {
        this.key.selector().close();
        this.key.channel().close();
        return;
    }
}
