package org.xbill.DNS;
public class TLSARecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 356494267028580169;
    private byte[] certificateAssociationData;
    private int certificateUsage;
    private int matchingType;
    private int selector;

    TLSARecord()
    {
        return;
    }

    public TLSARecord(org.xbill.DNS.Name p8, int p9, long p10, int p12, int p13, int p14, byte[] p15)
    {
        this(p8, 52, p9, p10);
        this.certificateUsage = org.xbill.DNS.TLSARecord.checkU8("certificateUsage", p12);
        this.selector = org.xbill.DNS.TLSARecord.checkU8("selector", p13);
        this.matchingType = org.xbill.DNS.TLSARecord.checkU8("matchingType", p14);
        this.certificateAssociationData = org.xbill.DNS.TLSARecord.checkByteArrayLength("certificateAssociationData", p15, 65535);
        return;
    }

    public final byte[] getCertificateAssociationData()
    {
        return this.certificateAssociationData;
    }

    public int getCertificateUsage()
    {
        return this.certificateUsage;
    }

    public int getMatchingType()
    {
        return this.matchingType;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.TLSARecord();
    }

    public int getSelector()
    {
        return this.selector;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.certificateUsage = p2.getUInt8();
        this.selector = p2.getUInt8();
        this.matchingType = p2.getUInt8();
        this.certificateAssociationData = p2.getHex();
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.certificateUsage = p2.readU8();
        this.selector = p2.readU8();
        this.matchingType = p2.readU8();
        this.certificateAssociationData = p2.readByteArray();
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.certificateUsage);
        v0_1.append(" ");
        v0_1.append(this.selector);
        v0_1.append(" ");
        v0_1.append(this.matchingType);
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.utils.base16.toString(this.certificateAssociationData));
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeU8(this.certificateUsage);
        p2.writeU8(this.selector);
        p2.writeU8(this.matchingType);
        p2.writeByteArray(this.certificateAssociationData);
        return;
    }
}
