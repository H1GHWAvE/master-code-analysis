package org.xbill.DNS;
public class DNAMERecord extends org.xbill.DNS.SingleNameBase {
    private static final long serialVersionUID = 2670767677200844154;

    DNAMERecord()
    {
        return;
    }

    public DNAMERecord(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Name p14)
    {
        this(p10, 39, p11, p12, p14, "alias");
        return;
    }

    public org.xbill.DNS.Name getAlias()
    {
        return this.getSingleName();
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.DNAMERecord();
    }

    public org.xbill.DNS.Name getTarget()
    {
        return this.getSingleName();
    }
}
