package org.xbill.DNS;
public class SSHFPRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 10342042671054864591;
    private int alg;
    private int digestType;
    private byte[] fingerprint;

    SSHFPRecord()
    {
        return;
    }

    public SSHFPRecord(org.xbill.DNS.Name p8, int p9, long p10, int p12, int p13, byte[] p14)
    {
        this(p8, 44, p9, p10);
        this.alg = org.xbill.DNS.SSHFPRecord.checkU8("alg", p12);
        this.digestType = org.xbill.DNS.SSHFPRecord.checkU8("digestType", p13);
        this.fingerprint = p14;
        return;
    }

    public int getAlgorithm()
    {
        return this.alg;
    }

    public int getDigestType()
    {
        return this.digestType;
    }

    public byte[] getFingerPrint()
    {
        return this.fingerprint;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.SSHFPRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.alg = p2.getUInt8();
        this.digestType = p2.getUInt8();
        this.fingerprint = p2.getHex(1);
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.alg = p2.readU8();
        this.digestType = p2.readU8();
        this.fingerprint = p2.readByteArray();
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.alg);
        v0_1.append(" ");
        v0_1.append(this.digestType);
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.utils.base16.toString(this.fingerprint));
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeU8(this.alg);
        p2.writeU8(this.digestType);
        p2.writeByteArray(this.fingerprint);
        return;
    }
}
