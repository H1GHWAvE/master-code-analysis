.bytecode 50.0
.class public final synchronized android/support/design/widget/m
.super android/widget/FrameLayout

.field private static final 'a' I = 600


.field private 'b' Z

.field private 'c' I

.field private 'd' Landroid/support/v7/widget/Toolbar;

.field private 'e' Landroid/view/View;

.field private 'f' I

.field private 'g' I

.field private 'h' I

.field private 'i' I

.field private final 'j' Landroid/graphics/Rect;

.field private final 'k' Landroid/support/design/widget/l;

.field private 'l' Z

.field private 'm' Landroid/graphics/drawable/Drawable;

.field private 'n' Landroid/graphics/drawable/Drawable;

.field private 'o' I

.field private 'p' Z

.field private 'q' Landroid/support/design/widget/ck;

.field private 'r' Landroid/support/design/widget/i;

.field private 's' I

.field private 't' Landroid/support/v4/view/gh;

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/m/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/m/<init>(Landroid/content/Context;C)V
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;C)V
aload 0
aload 1
aconst_null
iconst_0
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
iconst_1
putfield android/support/design/widget/m/b Z
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/m/j Landroid/graphics/Rect;
aload 0
new android/support/design/widget/l
dup
aload 0
invokespecial android/support/design/widget/l/<init>(Landroid/view/View;)V
putfield android/support/design/widget/m/k Landroid/support/design/widget/l;
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
getstatic android/support/design/widget/a/c Landroid/view/animation/Interpolator;
invokevirtual android/support/design/widget/l/a(Landroid/view/animation/Interpolator;)V
aload 1
aconst_null
getstatic android/support/design/n/CollapsingToolbarLayout [I
iconst_0
getstatic android/support/design/m/Widget_Design_CollapsingToolbar I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_expandedTitleGravity I
ldc_w 8388691
invokevirtual android/content/res/TypedArray/getInt(II)I
invokevirtual android/support/design/widget/l/c(I)V
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_collapsedTitleGravity I
ldc_w 8388627
invokevirtual android/content/res/TypedArray/getInt(II)I
invokevirtual android/support/design/widget/l/d(I)V
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_expandedTitleMargin I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
istore 2
aload 0
iload 2
putfield android/support/design/widget/m/i I
aload 0
iload 2
putfield android/support/design/widget/m/h I
aload 0
iload 2
putfield android/support/design/widget/m/g I
aload 0
iload 2
putfield android/support/design/widget/m/f I
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L0
iconst_1
istore 2
L1:
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_expandedTitleMarginStart I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L2
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_expandedTitleMarginStart I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
istore 3
iload 2
ifeq L3
aload 0
iload 3
putfield android/support/design/widget/m/h I
L2:
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_expandedTitleMarginEnd I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L4
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_expandedTitleMarginEnd I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
istore 3
iload 2
ifeq L5
aload 0
iload 3
putfield android/support/design/widget/m/f I
L4:
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_expandedTitleMarginTop I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L6
aload 0
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_expandedTitleMarginTop I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/m/g I
L6:
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_expandedTitleMarginBottom I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L7
aload 0
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_expandedTitleMarginBottom I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/m/i I
L7:
aload 0
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_titleEnabled I
iconst_1
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/design/widget/m/l Z
aload 0
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_title I
invokevirtual android/content/res/TypedArray/getText(I)Ljava/lang/CharSequence;
invokevirtual android/support/design/widget/m/setTitle(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
getstatic android/support/design/m/TextAppearance_Design_CollapsingToolbar_Expanded I
invokevirtual android/support/design/widget/l/f(I)V
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
getstatic android/support/design/m/TextAppearance_AppCompat_Widget_ActionBar_Title I
invokevirtual android/support/design/widget/l/e(I)V
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_expandedTitleTextAppearance I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L8
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_expandedTitleTextAppearance I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
invokevirtual android/support/design/widget/l/f(I)V
L8:
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_collapsedTitleTextAppearance I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L9
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_collapsedTitleTextAppearance I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
invokevirtual android/support/design/widget/l/e(I)V
L9:
aload 0
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_contentScrim I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/design/widget/m/setContentScrim(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_statusBarScrim I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/design/widget/m/setStatusBarScrim(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
getstatic android/support/design/n/CollapsingToolbarLayout_toolbarId I
iconst_m1
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/design/widget/m/c I
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
iconst_0
invokevirtual android/support/design/widget/m/setWillNotDraw(Z)V
aload 0
new android/support/design/widget/n
dup
aload 0
invokespecial android/support/design/widget/n/<init>(Landroid/support/design/widget/m;)V
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/support/v4/view/bx;)V
return
L0:
iconst_0
istore 2
goto L1
L3:
aload 0
iload 3
putfield android/support/design/widget/m/f I
goto L2
L5:
aload 0
iload 3
putfield android/support/design/widget/m/h I
goto L4
.limit locals 4
.limit stack 5
.end method

.method static synthetic a(Landroid/view/View;)Landroid/support/design/widget/dg;
aload 0
invokestatic android/support/design/widget/m/b(Landroid/view/View;)Landroid/support/design/widget/dg;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/support/design/widget/m;)Landroid/support/v4/view/gh;
aload 0
getfield android/support/design/widget/m/t Landroid/support/v4/view/gh;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/support/design/widget/m;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
aload 0
aload 1
putfield android/support/design/widget/m/t Landroid/support/v4/view/gh;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/FrameLayout$LayoutParams;
new android/support/design/widget/p
dup
aload 0
invokespecial android/support/design/widget/p/<init>(Landroid/view/ViewGroup$LayoutParams;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a()V
aload 0
getfield android/support/design/widget/m/b Z
ifne L0
return
L0:
aload 0
invokevirtual android/support/design/widget/m/getChildCount()I
istore 2
iconst_0
istore 1
aconst_null
astore 3
L1:
iload 1
iload 2
if_icmpge L2
aload 0
iload 1
invokevirtual android/support/design/widget/m/getChildAt(I)Landroid/view/View;
astore 4
aload 4
instanceof android/support/v7/widget/Toolbar
ifeq L3
aload 0
getfield android/support/design/widget/m/c I
iconst_m1
if_icmpeq L4
aload 0
getfield android/support/design/widget/m/c I
aload 4
invokevirtual android/view/View/getId()I
if_icmpne L5
aload 4
checkcast android/support/v7/widget/Toolbar
astore 4
L6:
aload 4
ifnonnull L7
L8:
aload 0
aload 3
putfield android/support/design/widget/m/d Landroid/support/v7/widget/Toolbar;
aload 0
invokespecial android/support/design/widget/m/b()V
aload 0
iconst_0
putfield android/support/design/widget/m/b Z
return
L5:
aload 3
ifnonnull L3
aload 4
checkcast android/support/v7/widget/Toolbar
astore 3
L9:
iload 1
iconst_1
iadd
istore 1
goto L1
L4:
aload 4
checkcast android/support/v7/widget/Toolbar
astore 4
goto L6
L7:
aload 4
astore 3
goto L8
L3:
goto L9
L2:
aconst_null
astore 4
goto L6
.limit locals 5
.limit stack 2
.end method

.method private a(I)V
aload 0
invokespecial android/support/design/widget/m/a()V
aload 0
getfield android/support/design/widget/m/q Landroid/support/design/widget/ck;
ifnonnull L0
aload 0
invokestatic android/support/design/widget/dh/a()Landroid/support/design/widget/ck;
putfield android/support/design/widget/m/q Landroid/support/design/widget/ck;
aload 0
getfield android/support/design/widget/m/q Landroid/support/design/widget/ck;
sipush 600
invokevirtual android/support/design/widget/ck/a(I)V
aload 0
getfield android/support/design/widget/m/q Landroid/support/design/widget/ck;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/design/widget/ck/a(Landroid/view/animation/Interpolator;)V
aload 0
getfield android/support/design/widget/m/q Landroid/support/design/widget/ck;
new android/support/design/widget/o
dup
aload 0
invokespecial android/support/design/widget/o/<init>(Landroid/support/design/widget/m;)V
invokevirtual android/support/design/widget/ck/a(Landroid/support/design/widget/cp;)V
L1:
aload 0
getfield android/support/design/widget/m/q Landroid/support/design/widget/ck;
aload 0
getfield android/support/design/widget/m/o I
iload 1
invokevirtual android/support/design/widget/ck/a(II)V
aload 0
getfield android/support/design/widget/m/q Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/a()V
return
L0:
aload 0
getfield android/support/design/widget/m/q Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/b()Z
ifeq L1
aload 0
getfield android/support/design/widget/m/q Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/e()V
goto L1
.limit locals 2
.limit stack 4
.end method

.method static synthetic a(Landroid/support/design/widget/m;I)V
aload 0
iload 1
invokespecial android/support/design/widget/m/setScrimAlpha(I)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Landroid/support/design/widget/m;I)I
aload 0
iload 1
putfield android/support/design/widget/m/s I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Landroid/support/design/widget/m;)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Landroid/view/View;)Landroid/support/design/widget/dg;
aload 0
getstatic android/support/design/i/view_offset_helper I
invokevirtual android/view/View/getTag(I)Ljava/lang/Object;
checkcast android/support/design/widget/dg
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
new android/support/design/widget/dg
dup
aload 0
invokespecial android/support/design/widget/dg/<init>(Landroid/view/View;)V
astore 1
aload 0
getstatic android/support/design/i/view_offset_helper I
aload 1
invokevirtual android/view/View/setTag(ILjava/lang/Object;)V
L0:
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method private b()V
aload 0
getfield android/support/design/widget/m/l Z
ifne L0
aload 0
getfield android/support/design/widget/m/e Landroid/view/View;
ifnull L0
aload 0
getfield android/support/design/widget/m/e Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 1
aload 1
instanceof android/view/ViewGroup
ifeq L0
aload 1
checkcast android/view/ViewGroup
aload 0
getfield android/support/design/widget/m/e Landroid/view/View;
invokevirtual android/view/ViewGroup/removeView(Landroid/view/View;)V
L0:
aload 0
getfield android/support/design/widget/m/l Z
ifeq L1
aload 0
getfield android/support/design/widget/m/d Landroid/support/v7/widget/Toolbar;
ifnull L1
aload 0
getfield android/support/design/widget/m/e Landroid/view/View;
ifnonnull L2
aload 0
new android/view/View
dup
aload 0
invokevirtual android/support/design/widget/m/getContext()Landroid/content/Context;
invokespecial android/view/View/<init>(Landroid/content/Context;)V
putfield android/support/design/widget/m/e Landroid/view/View;
L2:
aload 0
getfield android/support/design/widget/m/e Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
ifnonnull L1
aload 0
getfield android/support/design/widget/m/d Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/design/widget/m/e Landroid/view/View;
iconst_m1
iconst_m1
invokevirtual android/support/v7/widget/Toolbar/addView(Landroid/view/View;II)V
L1:
return
.limit locals 2
.limit stack 4
.end method

.method static synthetic c(Landroid/support/design/widget/m;)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/m/n Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Z
aload 0
getfield android/support/design/widget/m/l Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()V
aload 0
getfield android/support/design/widget/m/p Z
ifne L0
aload 0
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L1
aload 0
invokevirtual android/support/design/widget/m/isInEditMode()Z
ifne L1
aload 0
sipush 255
invokespecial android/support/design/widget/m/a(I)V
L2:
aload 0
iconst_1
putfield android/support/design/widget/m/p Z
L0:
return
L1:
aload 0
sipush 255
invokespecial android/support/design/widget/m/setScrimAlpha(I)V
goto L2
.limit locals 1
.limit stack 2
.end method

.method static synthetic d(Landroid/support/design/widget/m;)V
aload 0
getfield android/support/design/widget/m/p Z
ifne L0
aload 0
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L1
aload 0
invokevirtual android/support/design/widget/m/isInEditMode()Z
ifne L1
aload 0
sipush 255
invokespecial android/support/design/widget/m/a(I)V
L2:
aload 0
iconst_1
putfield android/support/design/widget/m/p Z
L0:
return
L1:
aload 0
sipush 255
invokespecial android/support/design/widget/m/setScrimAlpha(I)V
goto L2
.limit locals 1
.limit stack 2
.end method

.method private e()V
aload 0
getfield android/support/design/widget/m/p Z
ifeq L0
aload 0
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L1
aload 0
invokevirtual android/support/design/widget/m/isInEditMode()Z
ifne L1
aload 0
iconst_0
invokespecial android/support/design/widget/m/a(I)V
L2:
aload 0
iconst_0
putfield android/support/design/widget/m/p Z
L0:
return
L1:
aload 0
iconst_0
invokespecial android/support/design/widget/m/setScrimAlpha(I)V
goto L2
.limit locals 1
.limit stack 2
.end method

.method static synthetic e(Landroid/support/design/widget/m;)V
aload 0
getfield android/support/design/widget/m/p Z
ifeq L0
aload 0
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L1
aload 0
invokevirtual android/support/design/widget/m/isInEditMode()Z
ifne L1
aload 0
iconst_0
invokespecial android/support/design/widget/m/a(I)V
L2:
aload 0
iconst_0
putfield android/support/design/widget/m/p Z
L0:
return
L1:
aload 0
iconst_0
invokespecial android/support/design/widget/m/setScrimAlpha(I)V
goto L2
.limit locals 1
.limit stack 2
.end method

.method static synthetic f(Landroid/support/design/widget/m;)Landroid/support/design/widget/l;
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()Landroid/support/design/widget/p;
new android/support/design/widget/p
dup
aload 0
invokespecial android/widget/FrameLayout/generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
invokespecial android/support/design/widget/p/<init>(Landroid/widget/FrameLayout$LayoutParams;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private setScrimAlpha(I)V
iload 1
aload 0
getfield android/support/design/widget/m/o I
if_icmpeq L0
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/design/widget/m/d Landroid/support/v7/widget/Toolbar;
ifnull L1
aload 0
getfield android/support/design/widget/m/d Landroid/support/v7/widget/Toolbar;
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L1:
aload 0
iload 1
putfield android/support/design/widget/m/o I
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method protected final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 1
instanceof android/support/design/widget/p
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final draw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/widget/FrameLayout/draw(Landroid/graphics/Canvas;)V
aload 0
invokespecial android/support/design/widget/m/a()V
aload 0
getfield android/support/design/widget/m/d Landroid/support/v7/widget/Toolbar;
ifnonnull L0
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/design/widget/m/o I
ifle L0
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/mutate()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/m/o I
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
L0:
aload 0
getfield android/support/design/widget/m/l Z
ifeq L1
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
aload 1
invokevirtual android/support/design/widget/l/a(Landroid/graphics/Canvas;)V
L1:
aload 0
getfield android/support/design/widget/m/n Landroid/graphics/drawable/Drawable;
ifnull L2
aload 0
getfield android/support/design/widget/m/o I
ifle L2
aload 0
getfield android/support/design/widget/m/t Landroid/support/v4/view/gh;
ifnull L3
aload 0
getfield android/support/design/widget/m/t Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/b()I
istore 2
L4:
iload 2
ifle L2
aload 0
getfield android/support/design/widget/m/n Landroid/graphics/drawable/Drawable;
iconst_0
aload 0
getfield android/support/design/widget/m/s I
ineg
aload 0
invokevirtual android/support/design/widget/m/getWidth()I
iload 2
aload 0
getfield android/support/design/widget/m/s I
isub
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield android/support/design/widget/m/n Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/mutate()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/m/o I
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
aload 0
getfield android/support/design/widget/m/n Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
L2:
return
L3:
iconst_0
istore 2
goto L4
.limit locals 3
.limit stack 6
.end method

.method protected final drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
aload 0
invokespecial android/support/design/widget/m/a()V
aload 2
aload 0
getfield android/support/design/widget/m/d Landroid/support/v7/widget/Toolbar;
if_acmpne L0
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/design/widget/m/o I
ifle L0
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/mutate()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/m/o I
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
L0:
aload 0
aload 1
aload 2
lload 3
invokespecial android/widget/FrameLayout/drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
ireturn
.limit locals 5
.limit stack 5
.end method

.method protected final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
aload 0
invokespecial android/support/design/widget/m/f()Landroid/support/design/widget/p;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final synthetic generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
aload 0
invokespecial android/support/design/widget/m/f()Landroid/support/design/widget/p;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
aload 0
aload 1
invokevirtual android/support/design/widget/m/generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
areturn
.limit locals 2
.limit stack 2
.end method

.method protected final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
new android/support/design/widget/p
dup
aload 1
invokespecial android/support/design/widget/p/<init>(Landroid/view/ViewGroup$LayoutParams;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
new android/support/design/widget/p
dup
aload 0
invokevirtual android/support/design/widget/m/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/design/widget/p/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public final getCollapsedTitleGravity()I
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
getfield android/support/design/widget/l/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getContentScrim()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getExpandedTitleGravity()I
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
getfield android/support/design/widget/l/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method final getScrimTriggerOffset()I
aload 0
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
iconst_2
imul
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final getStatusBarScrim()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/m/n Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getTitle()Ljava/lang/CharSequence;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/m/l Z
ifeq L0
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
getfield android/support/design/widget/l/g Ljava/lang/CharSequence;
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final onAttachedToWindow()V
aload 0
invokespecial android/widget/FrameLayout/onAttachedToWindow()V
aload 0
invokevirtual android/support/design/widget/m/getParent()Landroid/view/ViewParent;
astore 1
aload 1
instanceof android/support/design/widget/AppBarLayout
ifeq L0
aload 0
getfield android/support/design/widget/m/r Landroid/support/design/widget/i;
ifnonnull L1
aload 0
new android/support/design/widget/q
dup
aload 0
iconst_0
invokespecial android/support/design/widget/q/<init>(Landroid/support/design/widget/m;B)V
putfield android/support/design/widget/m/r Landroid/support/design/widget/i;
L1:
aload 1
checkcast android/support/design/widget/AppBarLayout
astore 1
aload 0
getfield android/support/design/widget/m/r Landroid/support/design/widget/i;
astore 2
aload 2
ifnull L0
aload 1
getfield android/support/design/widget/AppBarLayout/c Ljava/util/List;
aload 2
invokeinterface java/util/List/contains(Ljava/lang/Object;)Z 1
ifne L0
aload 1
getfield android/support/design/widget/AppBarLayout/c Ljava/util/List;
aload 2
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L0:
return
.limit locals 3
.limit stack 5
.end method

.method protected final onDetachedFromWindow()V
aload 0
invokevirtual android/support/design/widget/m/getParent()Landroid/view/ViewParent;
astore 1
aload 0
getfield android/support/design/widget/m/r Landroid/support/design/widget/i;
ifnull L0
aload 1
instanceof android/support/design/widget/AppBarLayout
ifeq L0
aload 1
checkcast android/support/design/widget/AppBarLayout
astore 1
aload 0
getfield android/support/design/widget/m/r Landroid/support/design/widget/i;
astore 2
aload 2
ifnull L0
aload 1
getfield android/support/design/widget/AppBarLayout/c Ljava/util/List;
aload 2
invokeinterface java/util/List/remove(Ljava/lang/Object;)Z 1
pop
L0:
aload 0
invokespecial android/widget/FrameLayout/onDetachedFromWindow()V
return
.limit locals 3
.limit stack 2
.end method

.method protected final onLayout(ZIIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokespecial android/widget/FrameLayout/onLayout(ZIIII)V
iconst_0
istore 6
aload 0
invokevirtual android/support/design/widget/m/getChildCount()I
istore 7
L0:
iload 6
iload 7
if_icmpge L1
aload 0
iload 6
invokevirtual android/support/design/widget/m/getChildAt(I)Landroid/view/View;
astore 9
aload 0
getfield android/support/design/widget/m/t Landroid/support/v4/view/gh;
ifnull L2
aload 9
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifne L2
aload 0
getfield android/support/design/widget/m/t Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/b()I
istore 8
aload 9
invokevirtual android/view/View/getTop()I
iload 8
if_icmpge L2
aload 9
iload 8
invokevirtual android/view/View/offsetTopAndBottom(I)V
L2:
aload 9
invokestatic android/support/design/widget/m/b(Landroid/view/View;)Landroid/support/design/widget/dg;
invokevirtual android/support/design/widget/dg/a()V
iload 6
iconst_1
iadd
istore 6
goto L0
L1:
aload 0
getfield android/support/design/widget/m/l Z
ifeq L3
aload 0
getfield android/support/design/widget/m/e Landroid/view/View;
ifnull L3
aload 0
aload 0
getfield android/support/design/widget/m/e Landroid/view/View;
aload 0
getfield android/support/design/widget/m/j Landroid/graphics/Rect;
invokestatic android/support/design/widget/cz/a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
aload 0
getfield android/support/design/widget/m/j Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
iload 5
aload 0
getfield android/support/design/widget/m/j Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/height()I
isub
aload 0
getfield android/support/design/widget/m/j Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
iload 5
invokevirtual android/support/design/widget/l/b(IIII)V
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
aload 0
getfield android/support/design/widget/m/f I
aload 0
getfield android/support/design/widget/m/j Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
aload 0
getfield android/support/design/widget/m/g I
iadd
iload 4
iload 2
isub
aload 0
getfield android/support/design/widget/m/h I
isub
iload 5
iload 3
isub
aload 0
getfield android/support/design/widget/m/i I
isub
invokevirtual android/support/design/widget/l/a(IIII)V
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
invokevirtual android/support/design/widget/l/a()V
L3:
aload 0
getfield android/support/design/widget/m/d Landroid/support/v7/widget/Toolbar;
ifnull L4
aload 0
getfield android/support/design/widget/m/l Z
ifeq L5
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
getfield android/support/design/widget/l/g Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L5
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
aload 0
getfield android/support/design/widget/m/d Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getTitle()Ljava/lang/CharSequence;
invokevirtual android/support/design/widget/l/a(Ljava/lang/CharSequence;)V
L5:
aload 0
aload 0
getfield android/support/design/widget/m/d Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getHeight()I
invokevirtual android/support/design/widget/m/setMinimumHeight(I)V
L4:
return
.limit locals 10
.limit stack 6
.end method

.method protected final onMeasure(II)V
aload 0
invokespecial android/support/design/widget/m/a()V
aload 0
iload 1
iload 2
invokespecial android/widget/FrameLayout/onMeasure(II)V
return
.limit locals 3
.limit stack 3
.end method

.method protected final onSizeChanged(IIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
invokespecial android/widget/FrameLayout/onSizeChanged(IIII)V
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
iload 1
iload 2
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
L0:
return
.limit locals 5
.limit stack 5
.end method

.method public final setCollapsedTitleGravity(I)V
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
iload 1
invokevirtual android/support/design/widget/l/c(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setCollapsedTitleTextAppearance(I)V
.annotation invisibleparam 1 Landroid/support/a/ai;
.end annotation
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
iload 1
invokevirtual android/support/design/widget/l/e(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setCollapsedTitleTextColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
iload 1
invokevirtual android/support/design/widget/l/a(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setContentScrim(Landroid/graphics/drawable/Drawable;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
aload 1
if_acmpeq L0
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
aconst_null
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
L1:
aload 0
aload 1
putfield android/support/design/widget/m/m Landroid/graphics/drawable/Drawable;
aload 1
iconst_0
iconst_0
aload 0
invokevirtual android/support/design/widget/m/getWidth()I
aload 0
invokevirtual android/support/design/widget/m/getHeight()I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 1
aload 0
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
aload 1
invokevirtual android/graphics/drawable/Drawable/mutate()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/m/o I
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method public final setContentScrimColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
new android/graphics/drawable/ColorDrawable
dup
iload 1
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
invokevirtual android/support/design/widget/m/setContentScrim(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 4
.end method

.method public final setContentScrimResource(I)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
aload 0
aload 0
invokevirtual android/support/design/widget/m/getContext()Landroid/content/Context;
iload 1
invokestatic android/support/v4/c/h/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/design/widget/m/setContentScrim(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final setExpandedTitleColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
iload 1
invokevirtual android/support/design/widget/l/b(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setExpandedTitleGravity(I)V
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
iload 1
invokevirtual android/support/design/widget/l/c(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setExpandedTitleTextAppearance(I)V
.annotation invisibleparam 1 Landroid/support/a/ai;
.end annotation
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
iload 1
invokevirtual android/support/design/widget/l/f(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setStatusBarScrim(Landroid/graphics/drawable/Drawable;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/m/n Landroid/graphics/drawable/Drawable;
aload 1
if_acmpeq L0
aload 0
getfield android/support/design/widget/m/n Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/design/widget/m/n Landroid/graphics/drawable/Drawable;
aconst_null
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
L1:
aload 0
aload 1
putfield android/support/design/widget/m/n Landroid/graphics/drawable/Drawable;
aload 1
aload 0
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
aload 1
invokevirtual android/graphics/drawable/Drawable/mutate()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/m/o I
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final setStatusBarScrimColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
new android/graphics/drawable/ColorDrawable
dup
iload 1
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
invokevirtual android/support/design/widget/m/setStatusBarScrim(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 4
.end method

.method public final setStatusBarScrimResource(I)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
aload 0
aload 0
invokevirtual android/support/design/widget/m/getContext()Landroid/content/Context;
iload 1
invokestatic android/support/v4/c/h/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/design/widget/m/setStatusBarScrim(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/m/k Landroid/support/design/widget/l;
aload 1
invokevirtual android/support/design/widget/l/a(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setTitleEnabled(Z)V
iload 1
aload 0
getfield android/support/design/widget/m/l Z
if_icmpeq L0
aload 0
iload 1
putfield android/support/design/widget/m/l Z
aload 0
invokespecial android/support/design/widget/m/b()V
aload 0
invokevirtual android/support/design/widget/m/requestLayout()V
L0:
return
.limit locals 2
.limit stack 2
.end method
