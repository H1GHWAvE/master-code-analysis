.bytecode 50.0
.class public synchronized android/support/design/widget/NavigationView$SavedState
.super android/view/View$BaseSavedState

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field public 'a' Landroid/os/Bundle;

.method static <clinit>()V
new android/support/design/widget/aq
dup
invokespecial android/support/design/widget/aq/<init>()V
putstatic android/support/design/widget/NavigationView$SavedState/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/os/Parcel;)V
aload 0
aload 1
invokespecial android/view/View$BaseSavedState/<init>(Landroid/os/Parcel;)V
aload 0
aload 1
invokevirtual android/os/Parcel/readBundle()Landroid/os/Bundle;
putfield android/support/design/widget/NavigationView$SavedState/a Landroid/os/Bundle;
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/os/Parcelable;)V
aload 0
aload 1
invokespecial android/view/View$BaseSavedState/<init>(Landroid/os/Parcelable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
aload 1
iload 2
invokespecial android/view/View$BaseSavedState/writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/design/widget/NavigationView$SavedState/a Landroid/os/Bundle;
invokevirtual android/os/Parcel/writeBundle(Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method
