.bytecode 50.0
.class public synchronized android/support/design/widget/ce
.super android/widget/LinearLayout

.field private static final 'a' I = 200


.field private 'b' Landroid/widget/EditText;

.field private 'c' Ljava/lang/CharSequence;

.field private 'd' Landroid/graphics/Paint;

.field private 'e' Z

.field private 'f' Landroid/widget/TextView;

.field private 'g' I

.field private 'h' Landroid/content/res/ColorStateList;

.field private 'i' Landroid/content/res/ColorStateList;

.field private final 'j' Landroid/support/design/widget/l;

.field private 'k' Z

.field private 'l' Landroid/support/design/widget/ck;

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/ce/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/ce/<init>(Landroid/content/Context;C)V
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;C)V
aload 0
aload 1
aconst_null
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
new android/support/design/widget/l
dup
aload 0
invokespecial android/support/design/widget/l/<init>(Landroid/view/View;)V
putfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
aload 0
iconst_1
invokevirtual android/support/design/widget/ce/setOrientation(I)V
aload 0
iconst_0
invokevirtual android/support/design/widget/ce/setWillNotDraw(Z)V
aload 0
iconst_1
invokevirtual android/support/design/widget/ce/setAddStatesFromChildren(Z)V
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/design/widget/l/a(Landroid/view/animation/Interpolator;)V
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
astore 4
aload 4
new android/view/animation/AccelerateInterpolator
dup
invokespecial android/view/animation/AccelerateInterpolator/<init>()V
putfield android/support/design/widget/l/i Landroid/view/animation/Interpolator;
aload 4
invokevirtual android/support/design/widget/l/a()V
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
ldc_w 8388659
invokevirtual android/support/design/widget/l/d(I)V
aload 1
aconst_null
getstatic android/support/design/n/TextInputLayout [I
iconst_0
getstatic android/support/design/m/Widget_Design_TextInputLayout I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
getstatic android/support/design/n/TextInputLayout_android_hint I
invokevirtual android/content/res/TypedArray/getText(I)Ljava/lang/CharSequence;
putfield android/support/design/widget/ce/c Ljava/lang/CharSequence;
aload 0
aload 1
getstatic android/support/design/n/TextInputLayout_hintAnimationEnabled I
iconst_1
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/design/widget/ce/k Z
aload 1
getstatic android/support/design/n/TextInputLayout_android_textColorHint I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L0
aload 1
getstatic android/support/design/n/TextInputLayout_android_textColorHint I
invokevirtual android/content/res/TypedArray/getColorStateList(I)Landroid/content/res/ColorStateList;
astore 4
aload 0
aload 4
putfield android/support/design/widget/ce/i Landroid/content/res/ColorStateList;
aload 0
aload 4
putfield android/support/design/widget/ce/h Landroid/content/res/ColorStateList;
L0:
aload 1
getstatic android/support/design/n/TextInputLayout_hintTextAppearance I
iconst_m1
invokevirtual android/content/res/TypedArray/getResourceId(II)I
iconst_m1
if_icmpeq L1
aload 0
aload 1
getstatic android/support/design/n/TextInputLayout_hintTextAppearance I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
invokevirtual android/support/design/widget/ce/setHintTextAppearance(I)V
L1:
aload 0
aload 1
getstatic android/support/design/n/TextInputLayout_errorTextAppearance I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/design/widget/ce/g I
aload 1
getstatic android/support/design/n/TextInputLayout_errorEnabled I
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
istore 3
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
iload 3
invokevirtual android/support/design/widget/ce/setErrorEnabled(Z)V
aload 0
invokestatic android/support/v4/view/cx/c(Landroid/view/View;)I
ifne L2
aload 0
iconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;I)V
L2:
aload 0
new android/support/design/widget/cj
dup
aload 0
iconst_0
invokespecial android/support/design/widget/cj/<init>(Landroid/support/design/widget/ce;B)V
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/support/v4/view/a;)V
return
.limit locals 5
.limit stack 5
.end method

.method private a(I)I
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 2
aload 0
invokevirtual android/support/design/widget/ce/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
iload 1
aload 2
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
ifeq L0
aload 2
getfield android/util/TypedValue/data I
ireturn
L0:
ldc_w -65281
ireturn
.limit locals 3
.limit stack 4
.end method

.method private a(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
aload 1
instanceof android/widget/LinearLayout$LayoutParams
ifeq L0
aload 1
checkcast android/widget/LinearLayout$LayoutParams
astore 1
L1:
aload 0
getfield android/support/design/widget/ce/d Landroid/graphics/Paint;
ifnonnull L2
aload 0
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield android/support/design/widget/ce/d Landroid/graphics/Paint;
L2:
aload 0
getfield android/support/design/widget/ce/d Landroid/graphics/Paint;
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/getTypeface()Landroid/graphics/Typeface;
invokevirtual android/graphics/Paint/setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
pop
aload 0
getfield android/support/design/widget/ce/d Landroid/graphics/Paint;
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
getfield android/support/design/widget/l/e F
invokevirtual android/graphics/Paint/setTextSize(F)V
aload 1
aload 0
getfield android/support/design/widget/ce/d Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/ascent()F
fneg
f2i
putfield android/widget/LinearLayout$LayoutParams/topMargin I
aload 1
areturn
L0:
new android/widget/LinearLayout$LayoutParams
dup
aload 1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(Landroid/view/ViewGroup$LayoutParams;)V
astore 1
goto L1
.limit locals 2
.limit stack 3
.end method

.method private a(F)V
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
getfield android/support/design/widget/l/a F
fload 1
fcmpl
ifne L0
return
L0:
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
ifnonnull L1
aload 0
invokestatic android/support/design/widget/dh/a()Landroid/support/design/widget/ck;
putfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
getstatic android/support/design/widget/a/a Landroid/view/animation/Interpolator;
invokevirtual android/support/design/widget/ck/a(Landroid/view/animation/Interpolator;)V
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
sipush 200
invokevirtual android/support/design/widget/ck/a(I)V
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
new android/support/design/widget/ci
dup
aload 0
invokespecial android/support/design/widget/ci/<init>(Landroid/support/design/widget/ce;)V
invokevirtual android/support/design/widget/ck/a(Landroid/support/design/widget/cp;)V
L1:
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
getfield android/support/design/widget/l/a F
fload 1
invokevirtual android/support/design/widget/ck/a(FF)V
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/a()V
return
.limit locals 2
.limit stack 4
.end method

.method static synthetic a(Landroid/support/design/widget/ce;)V
aload 0
iconst_1
invokespecial android/support/design/widget/ce/a(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method private a(Z)V
iconst_1
istore 4
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
ifnull L0
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
iconst_1
istore 2
L1:
aload 0
invokevirtual android/support/design/widget/ce/getDrawableState()[I
astore 6
aload 6
arraylength
istore 5
iconst_0
istore 3
L2:
iload 3
iload 5
if_icmpge L3
aload 6
iload 3
iaload
ldc_w 16842908
if_icmpne L4
iload 4
istore 3
L5:
aload 0
getfield android/support/design/widget/ce/h Landroid/content/res/ColorStateList;
ifnull L6
aload 0
getfield android/support/design/widget/ce/i Landroid/content/res/ColorStateList;
ifnull L6
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
aload 0
getfield android/support/design/widget/ce/h Landroid/content/res/ColorStateList;
invokevirtual android/content/res/ColorStateList/getDefaultColor()I
invokevirtual android/support/design/widget/l/b(I)V
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
astore 6
iload 3
ifeq L7
aload 0
getfield android/support/design/widget/ce/i Landroid/content/res/ColorStateList;
invokevirtual android/content/res/ColorStateList/getDefaultColor()I
istore 4
L8:
aload 6
iload 4
invokevirtual android/support/design/widget/l/a(I)V
L6:
iload 2
ifne L9
iload 3
ifeq L10
L9:
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
ifnull L11
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/b()Z
ifeq L11
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/e()V
L11:
iload 1
ifeq L12
aload 0
getfield android/support/design/widget/ce/k Z
ifeq L12
aload 0
fconst_1
invokespecial android/support/design/widget/ce/a(F)V
return
L0:
iconst_0
istore 2
goto L1
L4:
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
iconst_0
istore 3
goto L5
L7:
aload 0
getfield android/support/design/widget/ce/h Landroid/content/res/ColorStateList;
invokevirtual android/content/res/ColorStateList/getDefaultColor()I
istore 4
goto L8
L12:
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
fconst_1
invokevirtual android/support/design/widget/l/a(F)V
return
L10:
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
ifnull L13
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/b()Z
ifeq L13
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/e()V
L13:
iload 1
ifeq L14
aload 0
getfield android/support/design/widget/ce/k Z
ifeq L14
aload 0
fconst_0
invokespecial android/support/design/widget/ce/a(F)V
return
L14:
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
fconst_0
invokevirtual android/support/design/widget/l/a(F)V
return
.limit locals 7
.limit stack 2
.end method

.method private a()Z
aload 0
getfield android/support/design/widget/ce/e Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a([I)Z
iconst_0
istore 4
aload 0
arraylength
istore 2
iconst_0
istore 1
L0:
iload 4
istore 3
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
iaload
ldc_w 16842908
if_icmpne L2
iconst_1
istore 3
L1:
iload 3
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
.limit locals 5
.limit stack 2
.end method

.method static synthetic b(Landroid/support/design/widget/ce;)Landroid/support/design/widget/l;
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Z)V
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
ifnull L0
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/b()Z
ifeq L0
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/e()V
L0:
iload 1
ifeq L1
aload 0
getfield android/support/design/widget/ce/k Z
ifeq L1
aload 0
fconst_1
invokespecial android/support/design/widget/ce/a(F)V
return
L1:
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
fconst_1
invokevirtual android/support/design/widget/l/a(F)V
return
.limit locals 2
.limit stack 2
.end method

.method private b()Z
aload 0
getfield android/support/design/widget/ce/k Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Landroid/support/design/widget/ce;)Landroid/widget/EditText;
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(Z)V
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
ifnull L0
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/b()Z
ifeq L0
aload 0
getfield android/support/design/widget/ce/l Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/e()V
L0:
iload 1
ifeq L1
aload 0
getfield android/support/design/widget/ce/k Z
ifeq L1
aload 0
fconst_0
invokespecial android/support/design/widget/ce/a(F)V
return
L1:
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
fconst_0
invokevirtual android/support/design/widget/l/a(F)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic d(Landroid/support/design/widget/ce;)Landroid/widget/TextView;
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private setEditText(Landroid/widget/EditText;)V
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
ifnull L0
new java/lang/IllegalArgumentException
dup
ldc "We already have an EditText, can only have one"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield android/support/design/widget/ce/b Landroid/widget/EditText;
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getTypeface()Landroid/graphics/Typeface;
invokevirtual android/support/design/widget/l/a(Landroid/graphics/Typeface;)V
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
astore 1
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getTextSize()F
fstore 2
aload 1
getfield android/support/design/widget/l/d F
fload 2
fcmpl
ifeq L1
aload 1
fload 2
putfield android/support/design/widget/l/d F
aload 1
invokevirtual android/support/design/widget/l/a()V
L1:
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getGravity()I
invokevirtual android/support/design/widget/l/c(I)V
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
new android/support/design/widget/cf
dup
aload 0
invokespecial android/support/design/widget/cf/<init>(Landroid/support/design/widget/ce;)V
invokevirtual android/widget/EditText/addTextChangedListener(Landroid/text/TextWatcher;)V
aload 0
getfield android/support/design/widget/ce/h Landroid/content/res/ColorStateList;
ifnonnull L2
aload 0
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getHintTextColors()Landroid/content/res/ColorStateList;
putfield android/support/design/widget/ce/h Landroid/content/res/ColorStateList;
L2:
aload 0
getfield android/support/design/widget/ce/c Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L3
aload 0
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getHint()Ljava/lang/CharSequence;
invokevirtual android/support/design/widget/ce/setHint(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
aconst_null
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
L3:
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
ifnull L4
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokestatic android/support/v4/view/cx/k(Landroid/view/View;)I
iconst_0
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokestatic android/support/v4/view/cx/l(Landroid/view/View;)I
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getPaddingBottom()I
invokestatic android/support/v4/view/cx/b(Landroid/view/View;IIII)V
L4:
aload 0
iconst_0
invokespecial android/support/design/widget/ce/a(Z)V
return
.limit locals 3
.limit stack 5
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
aload 1
instanceof android/widget/EditText
ifeq L0
aload 0
aload 1
checkcast android/widget/EditText
invokespecial android/support/design/widget/ce/setEditText(Landroid/widget/EditText;)V
aload 0
aload 1
iconst_0
aload 0
aload 3
invokespecial android/support/design/widget/ce/a(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
invokespecial android/widget/LinearLayout/addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
return
L0:
aload 0
aload 1
iload 2
aload 3
invokespecial android/widget/LinearLayout/addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 4
.limit stack 5
.end method

.method public draw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/widget/LinearLayout/draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
aload 1
invokevirtual android/support/design/widget/l/a(Landroid/graphics/Canvas;)V
return
.limit locals 2
.limit stack 2
.end method

.method public getEditText()Landroid/widget/EditText;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getError()Ljava/lang/CharSequence;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/ce/e Z
ifeq L0
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
ifnull L0
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
invokevirtual android/widget/TextView/getVisibility()I
ifne L0
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
invokevirtual android/widget/TextView/getText()Ljava/lang/CharSequence;
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public getHint()Ljava/lang/CharSequence;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/ce/c Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected onLayout(ZIIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokespecial android/widget/LinearLayout/onLayout(ZIIII)V
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
ifnull L0
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getLeft()I
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getCompoundPaddingLeft()I
iadd
istore 2
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getRight()I
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getCompoundPaddingRight()I
isub
istore 4
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
iload 2
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getTop()I
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getCompoundPaddingTop()I
iadd
iload 4
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getBottom()I
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getCompoundPaddingBottom()I
isub
invokevirtual android/support/design/widget/l/a(IIII)V
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
iload 2
aload 0
invokevirtual android/support/design/widget/ce/getPaddingTop()I
iload 4
iload 5
iload 3
isub
aload 0
invokevirtual android/support/design/widget/ce/getPaddingBottom()I
isub
invokevirtual android/support/design/widget/l/b(IIII)V
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
invokevirtual android/support/design/widget/l/a()V
L0:
return
.limit locals 6
.limit stack 6
.end method

.method public refreshDrawableState()V
aload 0
invokespecial android/widget/LinearLayout/refreshDrawableState()V
aload 0
aload 0
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
invokespecial android/support/design/widget/ce/a(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public setError(Ljava/lang/CharSequence;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/ce/e Z
ifne L0
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L1
return
L1:
aload 0
iconst_1
invokevirtual android/support/design/widget/ce/setErrorEnabled(Z)V
L0:
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L2
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
fconst_0
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_1
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
ldc2_w 200L
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/v4/view/fk/a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;
new android/support/design/widget/cg
dup
aload 0
invokespecial android/support/design/widget/cg/<init>(Landroid/support/design/widget/ce;)V
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/b()V
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
invokevirtual android/widget/TextView/getCurrentTextColor()I
invokestatic android/content/res/ColorStateList/valueOf(I)Landroid/content/res/ColorStateList;
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
L3:
aload 0
sipush 2048
invokevirtual android/support/design/widget/ce/sendAccessibilityEvent(I)V
return
L2:
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
invokevirtual android/widget/TextView/getVisibility()I
ifne L3
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
ldc2_w 200L
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/v4/view/fk/a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;
new android/support/design/widget/ch
dup
aload 0
invokespecial android/support/design/widget/ch/<init>(Landroid/support/design/widget/ce;)V
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/b()V
aload 0
invokevirtual android/support/design/widget/ce/getContext()Landroid/content/Context;
invokestatic android/support/v7/internal/widget/av/a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;
astore 1
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
aload 1
getstatic android/support/design/h/abc_edit_text_material I
invokevirtual android/support/v7/internal/widget/av/a(I)Landroid/content/res/ColorStateList;
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
goto L3
.limit locals 2
.limit stack 4
.end method

.method public setErrorEnabled(Z)V
aload 0
getfield android/support/design/widget/ce/e Z
iload 1
if_icmpeq L0
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
ifnull L1
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/a()V
L1:
iload 1
ifeq L2
aload 0
new android/widget/TextView
dup
aload 0
invokevirtual android/support/design/widget/ce/getContext()Landroid/content/Context;
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield android/support/design/widget/ce/f Landroid/widget/TextView;
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
aload 0
invokevirtual android/support/design/widget/ce/getContext()Landroid/content/Context;
aload 0
getfield android/support/design/widget/ce/g I
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
iconst_4
invokevirtual android/widget/TextView/setVisibility(I)V
aload 0
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
invokevirtual android/support/design/widget/ce/addView(Landroid/view/View;)V
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
ifnull L3
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokestatic android/support/v4/view/cx/k(Landroid/view/View;)I
iconst_0
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokestatic android/support/v4/view/cx/l(Landroid/view/View;)I
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getPaddingBottom()I
invokestatic android/support/v4/view/cx/b(Landroid/view/View;IIII)V
L3:
aload 0
iload 1
putfield android/support/design/widget/ce/e Z
L0:
return
L2:
aload 0
aload 0
getfield android/support/design/widget/ce/f Landroid/widget/TextView;
invokevirtual android/support/design/widget/ce/removeView(Landroid/view/View;)V
aload 0
aconst_null
putfield android/support/design/widget/ce/f Landroid/widget/TextView;
goto L3
.limit locals 2
.limit stack 5
.end method

.method public setHint(Ljava/lang/CharSequence;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/design/widget/ce/c Ljava/lang/CharSequence;
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
aload 1
invokevirtual android/support/design/widget/l/a(Ljava/lang/CharSequence;)V
aload 0
sipush 2048
invokevirtual android/support/design/widget/ce/sendAccessibilityEvent(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setHintAnimationEnabled(Z)V
aload 0
iload 1
putfield android/support/design/widget/ce/k Z
return
.limit locals 2
.limit stack 2
.end method

.method public setHintTextAppearance(I)V
.annotation invisibleparam 1 Landroid/support/a/ai;
.end annotation
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
iload 1
invokevirtual android/support/design/widget/l/e(I)V
aload 0
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
getfield android/support/design/widget/l/f I
invokestatic android/content/res/ColorStateList/valueOf(I)Landroid/content/res/ColorStateList;
putfield android/support/design/widget/ce/i Landroid/content/res/ColorStateList;
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
ifnull L0
aload 0
iconst_0
invokespecial android/support/design/widget/ce/a(Z)V
aload 0
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
invokespecial android/support/design/widget/ce/a(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
astore 2
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
aload 2
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield android/support/design/widget/ce/b Landroid/widget/EditText;
invokevirtual android/widget/EditText/requestLayout()V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/ce/j Landroid/support/design/widget/l;
aload 1
invokevirtual android/support/design/widget/l/a(Landroid/graphics/Typeface;)V
return
.limit locals 2
.limit stack 2
.end method
