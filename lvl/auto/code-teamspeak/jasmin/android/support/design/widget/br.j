.bytecode 50.0
.class public final synchronized android/support/design/widget/br
.super android/widget/HorizontalScrollView

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field public static final 'c' I = 0


.field public static final 'd' I = 1


.field private static final 'e' I = 48


.field private static final 'f' I = 56


.field private static final 'g' I = 16


.field private static final 'h' I = 24


.field private static final 'i' I = 300


.field private 'A' Landroid/view/View$OnClickListener;

.field private 'B' Landroid/support/design/widget/ck;

.field private 'C' Landroid/support/design/widget/ck;

.field private final 'j' Ljava/util/ArrayList;

.field private 'k' Landroid/support/design/widget/bz;

.field private final 'l' Landroid/support/design/widget/bw;

.field private 'm' I

.field private 'n' I

.field private 'o' I

.field private 'p' I

.field private 'q' I

.field private 'r' Landroid/content/res/ColorStateList;

.field private final 's' I

.field private final 't' I

.field private 'u' I

.field private final 'v' I

.field private 'w' I

.field private 'x' I

.field private 'y' I

.field private 'z' Landroid/support/design/widget/bv;

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/br/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/br/<init>(Landroid/content/Context;C)V
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;C)V
aload 0
aload 1
aconst_null
iconst_0
invokespecial android/widget/HorizontalScrollView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/design/widget/br/j Ljava/util/ArrayList;
aload 0
ldc_w 2147483647
putfield android/support/design/widget/br/u I
aload 0
iconst_0
invokevirtual android/support/design/widget/br/setHorizontalScrollBarEnabled(Z)V
aload 0
iconst_1
invokevirtual android/support/design/widget/br/setFillViewport(Z)V
aload 0
new android/support/design/widget/bw
dup
aload 0
aload 1
invokespecial android/support/design/widget/bw/<init>(Landroid/support/design/widget/br;Landroid/content/Context;)V
putfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
aload 0
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
bipush -2
iconst_m1
invokevirtual android/support/design/widget/br/addView(Landroid/view/View;II)V
aload 1
aconst_null
getstatic android/support/design/n/TabLayout [I
iconst_0
getstatic android/support/design/m/Widget_Design_TabLayout I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
aload 1
getstatic android/support/design/n/TabLayout_tabIndicatorHeight I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
invokevirtual android/support/design/widget/bw/b(I)V
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
aload 1
getstatic android/support/design/n/TabLayout_tabIndicatorColor I
iconst_0
invokevirtual android/content/res/TypedArray/getColor(II)I
invokevirtual android/support/design/widget/bw/a(I)V
aload 0
aload 1
getstatic android/support/design/n/TabLayout_tabTextAppearance I
getstatic android/support/design/m/TextAppearance_Design_Tab I
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/design/widget/br/q I
aload 1
getstatic android/support/design/n/TabLayout_tabPadding I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
istore 2
aload 0
iload 2
putfield android/support/design/widget/br/p I
aload 0
iload 2
putfield android/support/design/widget/br/o I
aload 0
iload 2
putfield android/support/design/widget/br/n I
aload 0
iload 2
putfield android/support/design/widget/br/m I
aload 0
aload 1
getstatic android/support/design/n/TabLayout_tabPaddingStart I
aload 0
getfield android/support/design/widget/br/m I
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/br/m I
aload 0
aload 1
getstatic android/support/design/n/TabLayout_tabPaddingTop I
aload 0
getfield android/support/design/widget/br/n I
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/br/n I
aload 0
aload 1
getstatic android/support/design/n/TabLayout_tabPaddingEnd I
aload 0
getfield android/support/design/widget/br/o I
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/br/o I
aload 0
aload 1
getstatic android/support/design/n/TabLayout_tabPaddingBottom I
aload 0
getfield android/support/design/widget/br/p I
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/br/p I
aload 0
aload 0
aload 0
getfield android/support/design/widget/br/q I
invokespecial android/support/design/widget/br/g(I)Landroid/content/res/ColorStateList;
putfield android/support/design/widget/br/r Landroid/content/res/ColorStateList;
aload 1
getstatic android/support/design/n/TabLayout_tabTextColor I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L0
aload 0
aload 1
getstatic android/support/design/n/TabLayout_tabTextColor I
invokevirtual android/content/res/TypedArray/getColorStateList(I)Landroid/content/res/ColorStateList;
putfield android/support/design/widget/br/r Landroid/content/res/ColorStateList;
L0:
aload 1
getstatic android/support/design/n/TabLayout_tabSelectedTextColor I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L1
aload 1
getstatic android/support/design/n/TabLayout_tabSelectedTextColor I
iconst_0
invokevirtual android/content/res/TypedArray/getColor(II)I
istore 2
aload 0
aload 0
getfield android/support/design/widget/br/r Landroid/content/res/ColorStateList;
invokevirtual android/content/res/ColorStateList/getDefaultColor()I
iload 2
invokestatic android/support/design/widget/br/b(II)Landroid/content/res/ColorStateList;
putfield android/support/design/widget/br/r Landroid/content/res/ColorStateList;
L1:
aload 0
aload 1
getstatic android/support/design/n/TabLayout_tabMinWidth I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/br/t I
aload 0
aload 1
getstatic android/support/design/n/TabLayout_tabMaxWidth I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/br/v I
aload 0
aload 1
getstatic android/support/design/n/TabLayout_tabBackground I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/design/widget/br/s I
aload 0
aload 1
getstatic android/support/design/n/TabLayout_tabContentStart I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/br/w I
aload 0
aload 1
getstatic android/support/design/n/TabLayout_tabMode I
iconst_1
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/design/widget/br/y I
aload 0
aload 1
getstatic android/support/design/n/TabLayout_tabGravity I
iconst_0
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/design/widget/br/x I
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
invokespecial android/support/design/widget/br/e()V
return
.limit locals 3
.limit stack 5
.end method

.method private a(IF)I
iconst_0
istore 3
iconst_0
istore 4
aload 0
getfield android/support/design/widget/br/y I
ifne L0
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
iload 1
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
astore 6
iload 1
iconst_1
iadd
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
invokevirtual android/support/design/widget/bw/getChildCount()I
if_icmpge L1
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
iload 1
iconst_1
iadd
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
astore 5
L2:
aload 6
ifnull L3
aload 6
invokevirtual android/view/View/getWidth()I
istore 1
L4:
iload 4
istore 3
aload 5
ifnull L5
aload 5
invokevirtual android/view/View/getWidth()I
istore 3
L5:
aload 6
invokevirtual android/view/View/getLeft()I
istore 4
iload 3
iload 1
iadd
i2f
fload 2
fmul
ldc_w 0.5F
fmul
f2i
iload 4
iadd
aload 6
invokevirtual android/view/View/getWidth()I
iconst_2
idiv
iadd
aload 0
invokevirtual android/support/design/widget/br/getWidth()I
iconst_2
idiv
isub
istore 3
L0:
iload 3
ireturn
L1:
aconst_null
astore 5
goto L2
L3:
iconst_0
istore 1
goto L4
.limit locals 7
.limit stack 3
.end method

.method static synthetic a(Landroid/support/design/widget/br;)I
aload 0
getfield android/support/design/widget/br/s I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a()Landroid/support/design/widget/bz;
.annotation invisible Landroid/support/a/y;
.end annotation
new android/support/design/widget/bz
dup
aload 0
invokespecial android/support/design/widget/bz/<init>(Landroid/support/design/widget/br;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic a(Landroid/support/design/widget/br;Landroid/support/design/widget/ck;)Landroid/support/design/widget/ck;
aload 0
aload 1
putfield android/support/design/widget/br/C Landroid/support/design/widget/ck;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(II)V
aload 0
iload 1
iload 2
invokestatic android/support/design/widget/br/b(II)Landroid/content/res/ColorStateList;
invokevirtual android/support/design/widget/br/setTabTextColors(Landroid/content/res/ColorStateList;)V
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Landroid/support/design/widget/br;I)V
aload 0
iload 1
invokespecial android/support/design/widget/br/c(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/design/widget/bz;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
istore 2
aload 1
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
if_acmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "Tab belongs to a different TabLayout."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
invokespecial android/support/design/widget/br/c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;
astore 3
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
aload 3
aload 0
invokespecial android/support/design/widget/br/d()Landroid/widget/LinearLayout$LayoutParams;
invokevirtual android/support/design/widget/bw/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
iload 2
ifeq L1
aload 3
iconst_1
invokevirtual android/support/design/widget/cc/setSelected(Z)V
L1:
aload 0
aload 1
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
invokespecial android/support/design/widget/br/b(Landroid/support/design/widget/bz;I)V
iload 2
ifeq L2
aload 1
invokevirtual android/support/design/widget/bz/a()V
L2:
return
.limit locals 4
.limit stack 3
.end method

.method private a(Landroid/support/design/widget/bz;I)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
istore 3
aload 1
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
if_acmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "Tab belongs to a different TabLayout."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
invokespecial android/support/design/widget/br/c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;
astore 4
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
aload 4
iload 2
aload 0
invokespecial android/support/design/widget/br/d()Landroid/widget/LinearLayout$LayoutParams;
invokevirtual android/support/design/widget/bw/addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
iload 3
ifeq L1
aload 4
iconst_1
invokevirtual android/support/design/widget/cc/setSelected(Z)V
L1:
aload 0
aload 1
iload 2
invokespecial android/support/design/widget/br/b(Landroid/support/design/widget/bz;I)V
iload 3
ifeq L2
aload 1
invokevirtual android/support/design/widget/bz/a()V
L2:
return
.limit locals 5
.limit stack 4
.end method

.method private a(Landroid/support/design/widget/bz;IZ)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 1
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
if_acmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "Tab belongs to a different TabLayout."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
invokespecial android/support/design/widget/br/c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;
astore 4
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
aload 4
iload 2
aload 0
invokespecial android/support/design/widget/br/d()Landroid/widget/LinearLayout$LayoutParams;
invokevirtual android/support/design/widget/bw/addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
iload 3
ifeq L1
aload 4
iconst_1
invokevirtual android/support/design/widget/cc/setSelected(Z)V
L1:
aload 0
aload 1
iload 2
invokespecial android/support/design/widget/br/b(Landroid/support/design/widget/bz;I)V
iload 3
ifeq L2
aload 1
invokevirtual android/support/design/widget/bz/a()V
L2:
return
.limit locals 5
.limit stack 4
.end method

.method private a(Landroid/widget/LinearLayout$LayoutParams;)V
aload 0
getfield android/support/design/widget/br/y I
iconst_1
if_icmpne L0
aload 0
getfield android/support/design/widget/br/x I
ifne L0
aload 1
iconst_0
putfield android/widget/LinearLayout$LayoutParams/width I
aload 1
fconst_1
putfield android/widget/LinearLayout$LayoutParams/weight F
return
L0:
aload 1
bipush -2
putfield android/widget/LinearLayout$LayoutParams/width I
aload 1
fconst_0
putfield android/widget/LinearLayout$LayoutParams/weight F
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Landroid/support/design/widget/br;)I
aload 0
getfield android/support/design/widget/br/m I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Landroid/support/design/widget/br;I)I
aload 0
iload 1
invokespecial android/support/design/widget/br/d(I)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static b(II)Landroid/content/res/ColorStateList;
new android/content/res/ColorStateList
dup
iconst_2
anewarray [I
dup
iconst_0
getstatic android/support/design/widget/br/SELECTED_STATE_SET [I
aastore
dup
iconst_1
getstatic android/support/design/widget/br/EMPTY_STATE_SET [I
aastore
iconst_2
newarray int
dup
iconst_0
iload 1
iastore
dup
iconst_1
iload 0
iastore
invokespecial android/content/res/ColorStateList/<init>([[I[I)V
areturn
.limit locals 2
.limit stack 7
.end method

.method private b()V
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
invokevirtual android/support/design/widget/bw/removeAllViews()V
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/design/widget/bz
iconst_m1
putfield android/support/design/widget/bz/e I
aload 1
invokeinterface java/util/Iterator/remove()V 0
goto L0
L1:
aload 0
aconst_null
putfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
return
.limit locals 2
.limit stack 2
.end method

.method private b(I)V
aload 0
getfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
ifnull L0
aload 0
getfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
getfield android/support/design/widget/bz/e I
istore 2
L1:
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
iload 1
invokevirtual android/support/design/widget/bw/removeViewAt(I)V
aload 0
invokevirtual android/support/design/widget/br/requestLayout()V
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
checkcast android/support/design/widget/bz
astore 5
aload 5
ifnull L2
aload 5
iconst_m1
putfield android/support/design/widget/bz/e I
L2:
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 4
iload 1
istore 3
L3:
iload 3
iload 4
if_icmpge L4
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/design/widget/bz
iload 3
putfield android/support/design/widget/bz/e I
iload 3
iconst_1
iadd
istore 3
goto L3
L0:
iconst_0
istore 2
goto L1
L4:
iload 2
iload 1
if_icmpne L5
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L6
aconst_null
astore 5
L7:
aload 0
aload 5
iconst_1
invokevirtual android/support/design/widget/br/a(Landroid/support/design/widget/bz;Z)V
L5:
return
L6:
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
iconst_0
iload 1
iconst_1
isub
invokestatic java/lang/Math/max(II)I
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/design/widget/bz
astore 5
goto L7
.limit locals 6
.limit stack 4
.end method

.method private b(Landroid/support/design/widget/bz;)V
aload 1
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
if_acmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "Tab does not belong to this TabLayout."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
getfield android/support/design/widget/bz/e I
invokespecial android/support/design/widget/br/b(I)V
return
.limit locals 2
.limit stack 3
.end method

.method private b(Landroid/support/design/widget/bz;I)V
aload 1
iload 2
putfield android/support/design/widget/bz/e I
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
iload 2
aload 1
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 3
iload 2
iconst_1
iadd
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/design/widget/bz
iload 2
putfield android/support/design/widget/bz/e I
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 4
.limit stack 3
.end method

.method private b(Landroid/support/design/widget/bz;IZ)V
aload 0
aload 1
invokespecial android/support/design/widget/br/c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;
astore 1
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
aload 1
iload 2
aload 0
invokespecial android/support/design/widget/br/d()Landroid/widget/LinearLayout$LayoutParams;
invokevirtual android/support/design/widget/bw/addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
iload 3
ifeq L0
aload 1
iconst_1
invokevirtual android/support/design/widget/cc/setSelected(Z)V
L0:
return
.limit locals 4
.limit stack 4
.end method

.method private b(Landroid/support/design/widget/bz;Z)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 1
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
if_acmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "Tab belongs to a different TabLayout."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
invokespecial android/support/design/widget/br/c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;
astore 3
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
aload 3
aload 0
invokespecial android/support/design/widget/br/d()Landroid/widget/LinearLayout$LayoutParams;
invokevirtual android/support/design/widget/bw/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
iload 2
ifeq L1
aload 3
iconst_1
invokevirtual android/support/design/widget/cc/setSelected(Z)V
L1:
aload 0
aload 1
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
invokespecial android/support/design/widget/br/b(Landroid/support/design/widget/bz;I)V
iload 2
ifeq L2
aload 1
invokevirtual android/support/design/widget/bz/a()V
L2:
return
.limit locals 4
.limit stack 3
.end method

.method static synthetic c(Landroid/support/design/widget/br;)I
aload 0
getfield android/support/design/widget/br/n I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;
new android/support/design/widget/cc
dup
aload 0
aload 0
invokevirtual android/support/design/widget/br/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/design/widget/cc/<init>(Landroid/support/design/widget/br;Landroid/content/Context;Landroid/support/design/widget/bz;)V
astore 1
aload 1
iconst_1
invokevirtual android/support/design/widget/cc/setFocusable(Z)V
aload 0
getfield android/support/design/widget/br/A Landroid/view/View$OnClickListener;
ifnonnull L0
aload 0
new android/support/design/widget/bs
dup
aload 0
invokespecial android/support/design/widget/bs/<init>(Landroid/support/design/widget/br;)V
putfield android/support/design/widget/br/A Landroid/view/View$OnClickListener;
L0:
aload 1
aload 0
getfield android/support/design/widget/br/A Landroid/view/View$OnClickListener;
invokevirtual android/support/design/widget/cc/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
areturn
.limit locals 2
.limit stack 5
.end method

.method private c()V
iconst_0
istore 1
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
invokevirtual android/support/design/widget/bw/getChildCount()I
istore 2
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokespecial android/support/design/widget/br/c(I)V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
return
.limit locals 3
.limit stack 2
.end method

.method private c(I)V
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
iload 1
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
checkcast android/support/design/widget/cc
astore 2
aload 2
ifnull L0
aload 2
invokevirtual android/support/design/widget/cc/a()V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method private c(Landroid/support/design/widget/bz;Z)V
aload 0
aload 1
invokespecial android/support/design/widget/br/c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;
astore 1
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
aload 1
aload 0
invokespecial android/support/design/widget/br/d()Landroid/widget/LinearLayout$LayoutParams;
invokevirtual android/support/design/widget/bw/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
iload 2
ifeq L0
aload 1
iconst_1
invokevirtual android/support/design/widget/cc/setSelected(Z)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private d(I)I
aload 0
invokevirtual android/support/design/widget/br/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
iload 1
i2f
fmul
invokestatic java/lang/Math/round(F)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic d(Landroid/support/design/widget/br;)I
aload 0
getfield android/support/design/widget/br/o I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()Landroid/widget/LinearLayout$LayoutParams;
new android/widget/LinearLayout$LayoutParams
dup
bipush -2
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
aload 0
aload 1
invokespecial android/support/design/widget/br/a(Landroid/widget/LinearLayout$LayoutParams;)V
aload 1
areturn
.limit locals 2
.limit stack 4
.end method

.method private d(Landroid/support/design/widget/bz;)V
aload 0
aload 1
iconst_1
invokevirtual android/support/design/widget/br/a(Landroid/support/design/widget/bz;Z)V
return
.limit locals 2
.limit stack 3
.end method

.method static synthetic e(Landroid/support/design/widget/br;)I
aload 0
getfield android/support/design/widget/br/p I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e()V
aload 0
getfield android/support/design/widget/br/y I
ifne L0
iconst_0
aload 0
getfield android/support/design/widget/br/w I
aload 0
getfield android/support/design/widget/br/m I
isub
invokestatic java/lang/Math/max(II)I
istore 1
L1:
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
iload 1
iconst_0
iconst_0
iconst_0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;IIII)V
aload 0
getfield android/support/design/widget/br/y I
tableswitch 0
L2
L3
default : L4
L4:
aload 0
invokespecial android/support/design/widget/br/f()V
return
L3:
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
iconst_1
invokevirtual android/support/design/widget/bw/setGravity(I)V
goto L4
L2:
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
ldc_w 8388611
invokevirtual android/support/design/widget/bw/setGravity(I)V
goto L4
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 5
.end method

.method private e(I)V
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
iload 1
invokevirtual android/support/design/widget/bw/removeViewAt(I)V
aload 0
invokevirtual android/support/design/widget/br/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic f(Landroid/support/design/widget/br;)I
aload 0
getfield android/support/design/widget/br/t I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()V
iconst_0
istore 1
L0:
iload 1
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
invokevirtual android/support/design/widget/bw/getChildCount()I
if_icmpge L1
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
iload 1
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
astore 2
aload 0
aload 2
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/widget/LinearLayout$LayoutParams
invokespecial android/support/design/widget/br/a(Landroid/widget/LinearLayout$LayoutParams;)V
aload 2
invokevirtual android/view/View/requestLayout()V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
return
.limit locals 3
.limit stack 2
.end method

.method private f(I)V
iconst_0
istore 3
iload 1
iconst_m1
if_icmpne L0
L1:
return
L0:
aload 0
invokevirtual android/support/design/widget/br/getWindowToken()Landroid/os/IBinder;
ifnull L2
aload 0
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L2
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
astore 6
aload 6
invokevirtual android/support/design/widget/bw/getChildCount()I
istore 4
iconst_0
istore 2
L3:
iload 2
iload 4
if_icmpge L4
aload 6
iload 2
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getWidth()I
ifgt L5
iconst_1
istore 2
L6:
iload 2
ifeq L7
L2:
aload 0
iload 1
fconst_0
iconst_1
invokevirtual android/support/design/widget/br/a(IFZ)V
return
L5:
iload 2
iconst_1
iadd
istore 2
goto L3
L4:
iconst_0
istore 2
goto L6
L7:
aload 0
invokevirtual android/support/design/widget/br/getScrollX()I
istore 2
aload 0
iload 1
fconst_0
invokespecial android/support/design/widget/br/a(IF)I
istore 4
iload 2
iload 4
if_icmpeq L8
aload 0
getfield android/support/design/widget/br/B Landroid/support/design/widget/ck;
ifnonnull L9
aload 0
invokestatic android/support/design/widget/dh/a()Landroid/support/design/widget/ck;
putfield android/support/design/widget/br/B Landroid/support/design/widget/ck;
aload 0
getfield android/support/design/widget/br/B Landroid/support/design/widget/ck;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/design/widget/ck/a(Landroid/view/animation/Interpolator;)V
aload 0
getfield android/support/design/widget/br/B Landroid/support/design/widget/ck;
sipush 300
invokevirtual android/support/design/widget/ck/a(I)V
aload 0
getfield android/support/design/widget/br/B Landroid/support/design/widget/ck;
new android/support/design/widget/bt
dup
aload 0
invokespecial android/support/design/widget/bt/<init>(Landroid/support/design/widget/br;)V
invokevirtual android/support/design/widget/ck/a(Landroid/support/design/widget/cp;)V
L9:
aload 0
getfield android/support/design/widget/br/B Landroid/support/design/widget/ck;
iload 2
iload 4
invokevirtual android/support/design/widget/ck/a(II)V
aload 0
getfield android/support/design/widget/br/B Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/a()V
L8:
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
astore 6
iload 3
istore 2
aload 6
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L10
iconst_1
istore 2
L10:
aload 6
iload 1
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
astore 7
aload 7
invokevirtual android/view/View/getLeft()I
istore 4
aload 7
invokevirtual android/view/View/getRight()I
istore 5
iload 1
aload 6
getfield android/support/design/widget/bw/a I
isub
invokestatic java/lang/Math/abs(I)I
iconst_1
if_icmpgt L11
aload 6
getfield android/support/design/widget/bw/c I
istore 2
aload 6
getfield android/support/design/widget/bw/d I
istore 3
L12:
iload 2
iload 4
if_icmpne L13
iload 3
iload 5
if_icmpeq L1
L13:
aload 6
getfield android/support/design/widget/bw/e Landroid/support/design/widget/br;
astore 8
invokestatic android/support/design/widget/dh/a()Landroid/support/design/widget/ck;
astore 7
aload 8
aload 7
putfield android/support/design/widget/br/C Landroid/support/design/widget/ck;
aload 7
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/design/widget/ck/a(Landroid/view/animation/Interpolator;)V
aload 7
sipush 300
invokevirtual android/support/design/widget/ck/a(I)V
aload 7
fconst_0
fconst_1
invokevirtual android/support/design/widget/ck/a(FF)V
aload 7
new android/support/design/widget/bx
dup
aload 6
iload 2
iload 4
iload 3
iload 5
invokespecial android/support/design/widget/bx/<init>(Landroid/support/design/widget/bw;IIII)V
invokevirtual android/support/design/widget/ck/a(Landroid/support/design/widget/cp;)V
new android/support/design/widget/by
dup
aload 6
iload 1
invokespecial android/support/design/widget/by/<init>(Landroid/support/design/widget/bw;I)V
astore 6
aload 7
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
new android/support/design/widget/cm
dup
aload 7
aload 6
invokespecial android/support/design/widget/cm/<init>(Landroid/support/design/widget/ck;Landroid/support/design/widget/cn;)V
invokevirtual android/support/design/widget/cr/a(Landroid/support/design/widget/cs;)V
aload 7
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/a()V
return
L11:
aload 6
getfield android/support/design/widget/bw/e Landroid/support/design/widget/br;
bipush 24
invokespecial android/support/design/widget/br/d(I)I
istore 3
iload 1
aload 6
getfield android/support/design/widget/bw/a I
if_icmpge L14
iload 2
ifne L15
iload 5
iload 3
iadd
istore 3
iload 3
istore 2
goto L12
L14:
iload 2
ifeq L15
iload 5
iload 3
iadd
istore 3
iload 3
istore 2
goto L12
L15:
iload 4
iload 3
isub
istore 3
iload 3
istore 2
goto L12
.limit locals 9
.limit stack 8
.end method

.method static synthetic g(Landroid/support/design/widget/br;)I
aload 0
getfield android/support/design/widget/br/u I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private g(I)Landroid/content/res/ColorStateList;
.catch all from L0 to L1 using L2
aload 0
invokevirtual android/support/design/widget/br/getContext()Landroid/content/Context;
iload 1
getstatic android/support/design/n/TextAppearance [I
invokevirtual android/content/Context/obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
astore 2
L0:
aload 2
getstatic android/support/design/n/TextAppearance_android_textColor I
invokevirtual android/content/res/TypedArray/getColorStateList(I)Landroid/content/res/ColorStateList;
astore 3
L1:
aload 2
invokevirtual android/content/res/TypedArray/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/content/res/TypedArray/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 3
.end method

.method private getScrollPosition()F
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
astore 2
aload 2
getfield android/support/design/widget/bw/a I
i2f
fstore 1
aload 2
getfield android/support/design/widget/bw/b F
fload 1
fadd
freturn
.limit locals 3
.limit stack 2
.end method

.method static synthetic h(Landroid/support/design/widget/br;)I
aload 0
getfield android/support/design/widget/br/q I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic i(Landroid/support/design/widget/br;)Landroid/content/res/ColorStateList;
aload 0
getfield android/support/design/widget/br/r Landroid/content/res/ColorStateList;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic j(Landroid/support/design/widget/br;)I
aload 0
getfield android/support/design/widget/br/y I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic k(Landroid/support/design/widget/br;)I
aload 0
getfield android/support/design/widget/br/x I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic l(Landroid/support/design/widget/br;)I
aload 0
iconst_0
putfield android/support/design/widget/br/x I
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic m(Landroid/support/design/widget/br;)V
aload 0
invokespecial android/support/design/widget/br/f()V
return
.limit locals 1
.limit stack 1
.end method

.method private setSelectedTabView(I)V
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
invokevirtual android/support/design/widget/bw/getChildCount()I
istore 3
iload 1
iload 3
if_icmpge L0
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
iload 1
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/isSelected()Z
ifne L0
iconst_0
istore 2
L1:
iload 2
iload 3
if_icmpge L0
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
iload 2
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
astore 5
iload 2
iload 1
if_icmpne L2
iconst_1
istore 4
L3:
aload 5
iload 4
invokevirtual android/view/View/setSelected(Z)V
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
iconst_0
istore 4
goto L3
L0:
return
.limit locals 6
.limit stack 2
.end method

.method public final a(I)Landroid/support/design/widget/bz;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/design/widget/bz
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(IFZ)V
aload 0
getfield android/support/design/widget/br/C Landroid/support/design/widget/ck;
ifnull L0
aload 0
getfield android/support/design/widget/br/C Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/b()Z
ifeq L0
L1:
return
L0:
iload 1
iflt L1
iload 1
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
invokevirtual android/support/design/widget/bw/getChildCount()I
if_icmpge L1
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
astore 4
aload 4
iload 1
putfield android/support/design/widget/bw/a I
aload 4
fload 2
putfield android/support/design/widget/bw/b F
aload 4
invokevirtual android/support/design/widget/bw/a()V
aload 0
aload 0
iload 1
fload 2
invokespecial android/support/design/widget/br/a(IF)I
iconst_0
invokevirtual android/support/design/widget/br/scrollTo(II)V
iload 3
ifeq L1
aload 0
iload 1
i2f
fload 2
fadd
invokestatic java/lang/Math/round(F)I
invokespecial android/support/design/widget/br/setSelectedTabView(I)V
return
.limit locals 5
.limit stack 4
.end method

.method final a(Landroid/support/design/widget/bz;Z)V
aload 0
getfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
aload 1
if_acmpne L0
aload 0
getfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
ifnull L1
aload 0
aload 1
getfield android/support/design/widget/bz/e I
invokespecial android/support/design/widget/br/f(I)V
L1:
return
L0:
aload 1
ifnull L2
aload 1
getfield android/support/design/widget/bz/e I
istore 3
L3:
aload 0
iload 3
invokespecial android/support/design/widget/br/setSelectedTabView(I)V
iload 2
ifeq L4
aload 0
getfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
ifnull L5
aload 0
getfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
getfield android/support/design/widget/bz/e I
iconst_m1
if_icmpne L6
L5:
iload 3
iconst_m1
if_icmpeq L6
aload 0
iload 3
fconst_0
iconst_1
invokevirtual android/support/design/widget/br/a(IFZ)V
L4:
aload 0
aload 1
putfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
aload 0
getfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
ifnull L1
aload 0
getfield android/support/design/widget/br/z Landroid/support/design/widget/bv;
ifnull L1
aload 0
getfield android/support/design/widget/br/z Landroid/support/design/widget/bv;
aload 0
getfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
invokeinterface android/support/design/widget/bv/a(Landroid/support/design/widget/bz;)V 1
return
L2:
iconst_m1
istore 3
goto L3
L6:
aload 0
iload 3
invokespecial android/support/design/widget/br/f(I)V
goto L4
.limit locals 4
.limit stack 4
.end method

.method public final getSelectedTabPosition()I
aload 0
getfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
ifnull L0
aload 0
getfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
getfield android/support/design/widget/bz/e I
ireturn
L0:
iconst_m1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getTabCount()I
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getTabGravity()I
aload 0
getfield android/support/design/widget/br/x I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getTabMode()I
aload 0
getfield android/support/design/widget/br/y I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getTabTextColors()Landroid/content/res/ColorStateList;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/br/r Landroid/content/res/ColorStateList;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final onMeasure(II)V
aload 0
bipush 48
invokespecial android/support/design/widget/br/d(I)I
aload 0
invokevirtual android/support/design/widget/br/getPaddingTop()I
iadd
aload 0
invokevirtual android/support/design/widget/br/getPaddingBottom()I
iadd
istore 3
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
lookupswitch
-2147483648 : L0
0 : L1
default : L2
L2:
aload 0
iload 1
iload 2
invokespecial android/widget/HorizontalScrollView/onMeasure(II)V
aload 0
getfield android/support/design/widget/br/y I
iconst_1
if_icmpne L3
aload 0
invokevirtual android/support/design/widget/br/getChildCount()I
iconst_1
if_icmpne L3
aload 0
iconst_0
invokevirtual android/support/design/widget/br/getChildAt(I)Landroid/view/View;
astore 6
aload 0
invokevirtual android/support/design/widget/br/getMeasuredWidth()I
istore 3
aload 6
invokevirtual android/view/View/getMeasuredWidth()I
iload 3
if_icmple L3
iload 2
aload 0
invokevirtual android/support/design/widget/br/getPaddingTop()I
aload 0
invokevirtual android/support/design/widget/br/getPaddingBottom()I
iadd
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
getfield android/view/ViewGroup$LayoutParams/height I
invokestatic android/support/design/widget/br/getChildMeasureSpec(III)I
istore 4
aload 6
iload 3
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 4
invokevirtual android/view/View/measure(II)V
L3:
aload 0
getfield android/support/design/widget/br/v I
istore 4
aload 0
invokevirtual android/support/design/widget/br/getMeasuredWidth()I
aload 0
bipush 56
invokespecial android/support/design/widget/br/d(I)I
isub
istore 5
iload 4
ifeq L4
iload 4
istore 3
iload 4
iload 5
if_icmple L5
L4:
iload 5
istore 3
L5:
aload 0
getfield android/support/design/widget/br/u I
iload 3
if_icmpeq L6
aload 0
iload 3
putfield android/support/design/widget/br/u I
aload 0
iload 1
iload 2
invokespecial android/widget/HorizontalScrollView/onMeasure(II)V
L6:
return
L0:
iload 3
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
invokestatic java/lang/Math/min(II)I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 2
goto L2
L1:
iload 3
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 2
goto L2
.limit locals 7
.limit stack 3
.end method

.method public final setOnTabSelectedListener(Landroid/support/design/widget/bv;)V
aload 0
aload 1
putfield android/support/design/widget/br/z Landroid/support/design/widget/bv;
return
.limit locals 2
.limit stack 2
.end method

.method public final setSelectedTabIndicatorColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
iload 1
invokevirtual android/support/design/widget/bw/a(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setSelectedTabIndicatorHeight(I)V
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
iload 1
invokevirtual android/support/design/widget/bw/b(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setTabGravity(I)V
aload 0
getfield android/support/design/widget/br/x I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/design/widget/br/x I
aload 0
invokespecial android/support/design/widget/br/e()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final setTabMode(I)V
iload 1
aload 0
getfield android/support/design/widget/br/y I
if_icmpeq L0
aload 0
iload 1
putfield android/support/design/widget/br/y I
aload 0
invokespecial android/support/design/widget/br/e()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final setTabTextColors(Landroid/content/res/ColorStateList;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/br/r Landroid/content/res/ColorStateList;
aload 1
if_acmpeq L0
aload 0
aload 1
putfield android/support/design/widget/br/r Landroid/content/res/ColorStateList;
iconst_0
istore 2
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
invokevirtual android/support/design/widget/bw/getChildCount()I
istore 3
L1:
iload 2
iload 3
if_icmpge L0
aload 0
iload 2
invokespecial android/support/design/widget/br/c(I)V
iload 2
iconst_1
iadd
istore 2
goto L1
L0:
return
.limit locals 4
.limit stack 2
.end method

.method public final setTabsFromPagerAdapter(Landroid/support/v4/view/by;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
invokevirtual android/support/design/widget/bw/removeAllViews()V
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 5
L0:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/design/widget/bz
iconst_m1
putfield android/support/design/widget/bz/e I
aload 5
invokeinterface java/util/Iterator/remove()V 0
goto L0
L1:
aload 0
aconst_null
putfield android/support/design/widget/br/k Landroid/support/design/widget/bz;
iconst_0
istore 2
aload 1
invokevirtual android/support/v4/view/by/e()I
istore 3
L2:
iload 2
iload 3
if_icmpge L3
new android/support/design/widget/bz
dup
aload 0
invokespecial android/support/design/widget/bz/<init>(Landroid/support/design/widget/br;)V
aconst_null
invokevirtual android/support/design/widget/bz/a(Ljava/lang/CharSequence;)Landroid/support/design/widget/bz;
astore 1
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
istore 4
aload 1
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
if_acmpeq L4
new java/lang/IllegalArgumentException
dup
ldc "Tab belongs to a different TabLayout."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 0
aload 1
invokespecial android/support/design/widget/br/c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;
astore 5
aload 0
getfield android/support/design/widget/br/l Landroid/support/design/widget/bw;
aload 5
aload 0
invokespecial android/support/design/widget/br/d()Landroid/widget/LinearLayout$LayoutParams;
invokevirtual android/support/design/widget/bw/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
iload 4
ifeq L5
aload 5
iconst_1
invokevirtual android/support/design/widget/cc/setSelected(Z)V
L5:
aload 0
aload 1
aload 0
getfield android/support/design/widget/br/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
invokespecial android/support/design/widget/br/b(Landroid/support/design/widget/bz;I)V
iload 4
ifeq L6
aload 1
invokevirtual android/support/design/widget/bz/a()V
L6:
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
return
.limit locals 6
.limit stack 3
.end method

.method public final setupWithViewPager(Landroid/support/v4/view/ViewPager;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 1
invokevirtual android/support/v4/view/ViewPager/getAdapter()Landroid/support/v4/view/by;
astore 3
aload 3
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "ViewPager does not have a PagerAdapter set"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 3
invokevirtual android/support/design/widget/br/setTabsFromPagerAdapter(Landroid/support/v4/view/by;)V
new android/support/design/widget/cb
dup
aload 0
invokespecial android/support/design/widget/cb/<init>(Landroid/support/design/widget/br;)V
astore 4
aload 1
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
ifnonnull L1
aload 1
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/view/ViewPager/a Ljava/util/List;
L1:
aload 1
getfield android/support/v4/view/ViewPager/a Ljava/util/List;
aload 4
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
new android/support/design/widget/cd
dup
aload 1
invokespecial android/support/design/widget/cd/<init>(Landroid/support/v4/view/ViewPager;)V
invokevirtual android/support/design/widget/br/setOnTabSelectedListener(Landroid/support/design/widget/bv;)V
aload 3
invokevirtual android/support/v4/view/by/e()I
ifle L2
aload 1
invokevirtual android/support/v4/view/ViewPager/getCurrentItem()I
istore 2
aload 0
invokevirtual android/support/design/widget/br/getSelectedTabPosition()I
iload 2
if_icmpeq L2
aload 0
aload 0
iload 2
invokevirtual android/support/design/widget/br/a(I)Landroid/support/design/widget/bz;
iconst_1
invokevirtual android/support/design/widget/br/a(Landroid/support/design/widget/bz;Z)V
L2:
return
.limit locals 5
.limit stack 4
.end method
