.bytecode 50.0
.class public synchronized android/support/design/widget/CoordinatorLayout
.super android/view/ViewGroup
.implements android/support/v4/view/bv

.field static final 'a' Ljava/lang/String; = "CoordinatorLayout"

.field static final 'b' Ljava/lang/String;

.field static final 'c' [Ljava/lang/Class;

.field static final 'd' Ljava/lang/ThreadLocal;

.field static final 'f' Ljava/util/Comparator;

.field static final 'g' Landroid/support/design/widget/aa;

.field private static final 'k' I = 0


.field private static final 'l' I = 1


.field private 'A' Landroid/graphics/drawable/Drawable;

.field private 'B' Landroid/view/ViewGroup$OnHierarchyChangeListener;

.field private final 'C' Landroid/support/v4/view/bw;

.field final 'e' Ljava/util/Comparator;

.field final 'h' Ljava/util/List;

.field final 'i' Landroid/graphics/Rect;

.field final 'j' Landroid/graphics/Rect;

.field private final 'm' Ljava/util/List;

.field private final 'n' Ljava/util/List;

.field private final 'o' Landroid/graphics/Rect;

.field private final 'p' [I

.field private 'q' Landroid/graphics/Paint;

.field private 'r' Z

.field private 's' [I

.field private 't' Landroid/view/View;

.field private 'u' Landroid/view/View;

.field private 'v' Landroid/view/View;

.field private 'w' Landroid/support/design/widget/x;

.field private 'x' Z

.field private 'y' Landroid/support/v4/view/gh;

.field private 'z' Z

.method static <clinit>()V
ldc android/support/design/widget/CoordinatorLayout
invokevirtual java/lang/Class/getPackage()Ljava/lang/Package;
invokevirtual java/lang/Package/getName()Ljava/lang/String;
putstatic android/support/design/widget/CoordinatorLayout/b Ljava/lang/String;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
new android/support/design/widget/z
dup
invokespecial android/support/design/widget/z/<init>()V
putstatic android/support/design/widget/CoordinatorLayout/f Ljava/util/Comparator;
new android/support/design/widget/ab
dup
invokespecial android/support/design/widget/ab/<init>()V
putstatic android/support/design/widget/CoordinatorLayout/g Landroid/support/design/widget/aa;
L1:
iconst_2
anewarray java/lang/Class
dup
iconst_0
ldc android/content/Context
aastore
dup
iconst_1
ldc android/util/AttributeSet
aastore
putstatic android/support/design/widget/CoordinatorLayout/c [Ljava/lang/Class;
new java/lang/ThreadLocal
dup
invokespecial java/lang/ThreadLocal/<init>()V
putstatic android/support/design/widget/CoordinatorLayout/d Ljava/lang/ThreadLocal;
return
L0:
aconst_null
putstatic android/support/design/widget/CoordinatorLayout/f Ljava/util/Comparator;
aconst_null
putstatic android/support/design/widget/CoordinatorLayout/g Landroid/support/design/widget/aa;
goto L1
.limit locals 0
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/CoordinatorLayout/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/CoordinatorLayout/<init>(Landroid/content/Context;C)V
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;C)V
iconst_0
istore 2
aload 0
aload 1
aconst_null
iconst_0
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/support/design/widget/r
dup
aload 0
invokespecial android/support/design/widget/r/<init>(Landroid/support/design/widget/CoordinatorLayout;)V
putfield android/support/design/widget/CoordinatorLayout/e Ljava/util/Comparator;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/design/widget/CoordinatorLayout/m Ljava/util/List;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/design/widget/CoordinatorLayout/n Ljava/util/List;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/CoordinatorLayout/o Landroid/graphics/Rect;
aload 0
iconst_2
newarray int
putfield android/support/design/widget/CoordinatorLayout/p [I
aload 0
new android/support/v4/view/bw
dup
aload 0
invokespecial android/support/v4/view/bw/<init>(Landroid/view/ViewGroup;)V
putfield android/support/design/widget/CoordinatorLayout/C Landroid/support/v4/view/bw;
aload 1
aconst_null
getstatic android/support/design/n/CoordinatorLayout [I
iconst_0
getstatic android/support/design/m/Widget_Design_CoordinatorLayout I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 5
aload 5
getstatic android/support/design/n/CoordinatorLayout_keylines I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 4
iload 4
ifeq L0
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
astore 1
aload 0
aload 1
iload 4
invokevirtual android/content/res/Resources/getIntArray(I)[I
putfield android/support/design/widget/CoordinatorLayout/s [I
aload 1
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fstore 3
aload 0
getfield android/support/design/widget/CoordinatorLayout/s [I
arraylength
istore 4
L1:
iload 2
iload 4
if_icmpge L0
aload 0
getfield android/support/design/widget/CoordinatorLayout/s [I
astore 1
aload 1
iload 2
aload 1
iload 2
iaload
i2f
fload 3
fmul
f2i
iastore
iload 2
iconst_1
iadd
istore 2
goto L1
L0:
aload 0
aload 5
getstatic android/support/design/n/CoordinatorLayout_statusBarBackground I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield android/support/design/widget/CoordinatorLayout/A Landroid/graphics/drawable/Drawable;
aload 5
invokevirtual android/content/res/TypedArray/recycle()V
getstatic android/support/design/widget/CoordinatorLayout/g Landroid/support/design/widget/aa;
ifnull L2
getstatic android/support/design/widget/CoordinatorLayout/g Landroid/support/design/widget/aa;
aload 0
new android/support/design/widget/s
dup
aload 0
invokespecial android/support/design/widget/s/<init>(Landroid/support/design/widget/CoordinatorLayout;)V
invokeinterface android/support/design/widget/aa/a(Landroid/view/View;Landroid/support/v4/view/bx;)V 2
L2:
aload 0
new android/support/design/widget/v
dup
aload 0
invokespecial android/support/design/widget/v/<init>(Landroid/support/design/widget/CoordinatorLayout;)V
invokespecial android/view/ViewGroup/setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V
return
.limit locals 6
.limit stack 5
.end method

.method private a(I)I
aload 0
getfield android/support/design/widget/CoordinatorLayout/s [I
ifnonnull L0
ldc "CoordinatorLayout"
new java/lang/StringBuilder
dup
ldc "No keylines defined for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " - attempted index lookup "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
L0:
iload 1
iflt L1
iload 1
aload 0
getfield android/support/design/widget/CoordinatorLayout/s [I
arraylength
if_icmplt L2
L1:
ldc "CoordinatorLayout"
new java/lang/StringBuilder
dup
ldc "Keyline index "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " out of range for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
L2:
aload 0
getfield android/support/design/widget/CoordinatorLayout/s [I
iload 1
iaload
ireturn
.limit locals 2
.limit stack 4
.end method

.method static a(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/String;)Landroid/support/design/widget/t;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch java/lang/Exception from L7 to L8 using L2
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L9
aconst_null
areturn
L9:
aload 2
ldc "."
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L10
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
L0:
getstatic android/support/design/widget/CoordinatorLayout/d Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/get()Ljava/lang/Object;
checkcast java/util/Map
astore 2
L1:
aload 2
ifnonnull L11
L3:
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 2
getstatic android/support/design/widget/CoordinatorLayout/d Ljava/lang/ThreadLocal;
aload 2
invokevirtual java/lang/ThreadLocal/set(Ljava/lang/Object;)V
L4:
aload 2
aload 3
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/reflect/Constructor
astore 5
L5:
aload 5
astore 4
aload 5
ifnonnull L7
L6:
aload 3
iconst_1
aload 0
invokevirtual android/content/Context/getClassLoader()Ljava/lang/ClassLoader;
invokestatic java/lang/Class/forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
getstatic android/support/design/widget/CoordinatorLayout/c [Ljava/lang/Class;
invokevirtual java/lang/Class/getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
astore 4
aload 4
iconst_1
invokevirtual java/lang/reflect/Constructor/setAccessible(Z)V
aload 2
aload 3
aload 4
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L7:
aload 4
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
invokevirtual java/lang/reflect/Constructor/newInstance([Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/support/design/widget/t
astore 0
L8:
aload 0
areturn
L10:
aload 2
astore 3
aload 2
bipush 46
invokevirtual java/lang/String/indexOf(I)I
ifge L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic android/support/design/widget/CoordinatorLayout/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
bipush 46
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
goto L0
L2:
astore 0
new java/lang/RuntimeException
dup
new java/lang/StringBuilder
dup
ldc "Could not inflate Behavior subclass "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L11:
goto L4
.limit locals 6
.limit stack 5
.end method

.method private a(Landroid/util/AttributeSet;)Landroid/support/design/widget/w;
new android/support/design/widget/w
dup
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/design/widget/w/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/design/widget/w;
aload 0
instanceof android/support/design/widget/w
ifeq L0
new android/support/design/widget/w
dup
aload 0
checkcast android/support/design/widget/w
invokespecial android/support/design/widget/w/<init>(Landroid/support/design/widget/w;)V
areturn
L0:
aload 0
instanceof android/view/ViewGroup$MarginLayoutParams
ifeq L1
new android/support/design/widget/w
dup
aload 0
checkcast android/view/ViewGroup$MarginLayoutParams
invokespecial android/support/design/widget/w/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
areturn
L1:
new android/support/design/widget/w
dup
aload 0
invokespecial android/support/design/widget/w/<init>(Landroid/view/ViewGroup$LayoutParams;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic a(Landroid/support/design/widget/CoordinatorLayout;)Landroid/view/ViewGroup$OnHierarchyChangeListener;
aload 0
getfield android/support/design/widget/CoordinatorLayout/B Landroid/view/ViewGroup$OnHierarchyChangeListener;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a()V
aload 0
getfield android/support/design/widget/CoordinatorLayout/t Landroid/view/View;
ifnull L0
aload 0
getfield android/support/design/widget/CoordinatorLayout/t Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 5
aload 5
ifnull L1
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 3
lload 3
lload 3
iconst_3
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 6
aload 5
aload 0
aload 0
getfield android/support/design/widget/CoordinatorLayout/t Landroid/view/View;
aload 6
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
pop
aload 6
invokevirtual android/view/MotionEvent/recycle()V
L1:
aload 0
aconst_null
putfield android/support/design/widget/CoordinatorLayout/t Landroid/view/View;
L0:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 2
iconst_0
istore 1
L2:
iload 1
iload 2
if_icmpge L3
aload 0
iload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
iconst_0
putfield android/support/design/widget/w/i Z
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
return
.limit locals 7
.limit stack 8
.end method

.method static synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/v4/view/gh;)V
aload 0
aload 1
invokespecial android/support/design/widget/CoordinatorLayout/setWindowInsets(Landroid/support/v4/view/gh;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/view/gh;)V
aload 1
invokevirtual android/support/v4/view/gh/g()Z
ifeq L0
L1:
return
L0:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 3
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 5
aload 1
astore 4
aload 5
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifeq L3
aload 5
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
ifnull L4
aload 1
invokevirtual android/support/v4/view/gh/g()Z
ifne L1
L4:
aload 5
aload 1
invokestatic android/support/v4/view/cx/b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
astore 4
aload 4
invokevirtual android/support/v4/view/gh/g()Z
ifne L1
L3:
iload 2
iconst_1
iadd
istore 2
aload 4
astore 1
goto L2
.limit locals 6
.limit stack 2
.end method

.method private a(Landroid/view/View;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 10
aload 10
getfield android/support/design/widget/w/c I
istore 6
iload 6
istore 5
iload 6
ifne L0
bipush 17
istore 5
L0:
iload 5
iload 2
invokestatic android/support/v4/view/v/a(II)I
istore 9
aload 10
getfield android/support/design/widget/w/d I
invokestatic android/support/design/widget/CoordinatorLayout/b(I)I
iload 2
invokestatic android/support/v4/view/v/a(II)I
istore 5
aload 1
invokevirtual android/view/View/getMeasuredWidth()I
istore 7
aload 1
invokevirtual android/view/View/getMeasuredHeight()I
istore 8
iload 5
bipush 7
iand
lookupswitch
1 : L1
5 : L2
default : L3
L3:
aload 3
getfield android/graphics/Rect/left I
istore 2
L4:
iload 5
bipush 112
iand
lookupswitch
16 : L5
80 : L6
default : L7
L7:
aload 3
getfield android/graphics/Rect/top I
istore 5
L8:
iload 2
istore 6
iload 9
bipush 7
iand
lookupswitch
1 : L9
5 : L10
default : L11
L11:
iload 2
iload 7
isub
istore 6
L10:
iload 5
istore 2
iload 9
bipush 112
iand
lookupswitch
16 : L12
80 : L13
default : L14
L14:
iload 5
iload 8
isub
istore 2
L13:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getWidth()I
istore 9
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getHeight()I
istore 5
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingLeft()I
aload 10
getfield android/support/design/widget/w/leftMargin I
iadd
iload 6
iload 9
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingRight()I
isub
iload 7
isub
aload 10
getfield android/support/design/widget/w/rightMargin I
isub
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
istore 6
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingTop()I
aload 10
getfield android/support/design/widget/w/topMargin I
iadd
iload 2
iload 5
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingBottom()I
isub
iload 8
isub
aload 10
getfield android/support/design/widget/w/bottomMargin I
isub
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
istore 2
aload 4
iload 6
iload 2
iload 6
iload 7
iadd
iload 2
iload 8
iadd
invokevirtual android/graphics/Rect/set(IIII)V
return
L2:
aload 3
getfield android/graphics/Rect/right I
istore 2
goto L4
L1:
aload 3
getfield android/graphics/Rect/left I
istore 2
aload 3
invokevirtual android/graphics/Rect/width()I
iconst_2
idiv
iload 2
iadd
istore 2
goto L4
L6:
aload 3
getfield android/graphics/Rect/bottom I
istore 5
goto L8
L5:
aload 3
getfield android/graphics/Rect/top I
aload 3
invokevirtual android/graphics/Rect/height()I
iconst_2
idiv
iadd
istore 5
goto L8
L9:
iload 2
iload 7
iconst_2
idiv
isub
istore 6
goto L10
L12:
iload 5
iload 8
iconst_2
idiv
isub
istore 2
goto L13
.limit locals 11
.limit stack 6
.end method

.method private a(Landroid/view/View;Landroid/graphics/Rect;)V
aload 0
aload 1
aload 2
invokestatic android/support/design/widget/cz/a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/view/View;Landroid/view/View;I)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
pop
aload 0
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 4
aload 0
getfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
astore 5
aload 0
aload 2
aload 4
invokestatic android/support/design/widget/cz/a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
aload 0
aload 1
iload 3
aload 4
aload 5
invokespecial android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V
aload 1
aload 5
getfield android/graphics/Rect/left I
aload 5
getfield android/graphics/Rect/top I
aload 5
getfield android/graphics/Rect/right I
aload 5
getfield android/graphics/Rect/bottom I
invokevirtual android/view/View/layout(IIII)V
return
.limit locals 6
.limit stack 5
.end method

.method private a(Ljava/util/List;)V
aload 1
invokeinterface java/util/List/clear()V 0
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/isChildrenDrawingOrderEnabled()Z
istore 5
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 4
iload 4
iconst_1
isub
istore 2
L0:
iload 2
iflt L1
iload 5
ifeq L2
aload 0
iload 4
iload 2
invokevirtual android/support/design/widget/CoordinatorLayout/getChildDrawingOrder(II)I
istore 3
L3:
aload 1
aload 0
iload 3
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 2
iconst_1
isub
istore 2
goto L0
L2:
iload 2
istore 3
goto L3
L1:
getstatic android/support/design/widget/CoordinatorLayout/f Ljava/util/Comparator;
ifnull L4
aload 1
getstatic android/support/design/widget/CoordinatorLayout/f Ljava/util/Comparator;
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
L4:
return
.limit locals 6
.limit stack 3
.end method

.method private a(Landroid/view/MotionEvent;I)Z
iconst_0
istore 8
iconst_0
istore 5
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 6
aload 0
getfield android/support/design/widget/CoordinatorLayout/m Ljava/util/List;
astore 14
aload 14
invokeinterface java/util/List/clear()V 0
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/isChildrenDrawingOrderEnabled()Z
istore 9
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 7
iload 7
iconst_1
isub
istore 3
L0:
iload 3
iflt L1
iload 9
ifeq L2
aload 0
iload 7
iload 3
invokevirtual android/support/design/widget/CoordinatorLayout/getChildDrawingOrder(II)I
istore 4
L3:
aload 14
aload 0
iload 4
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 3
iconst_1
isub
istore 3
goto L0
L2:
iload 3
istore 4
goto L3
L1:
getstatic android/support/design/widget/CoordinatorLayout/f Ljava/util/Comparator;
ifnull L4
aload 14
getstatic android/support/design/widget/CoordinatorLayout/f Ljava/util/Comparator;
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
L4:
aload 14
invokeinterface java/util/List/size()I 0
istore 7
iconst_0
istore 4
aconst_null
astore 13
iload 5
istore 3
L5:
iload 4
iload 7
if_icmpge L6
aload 14
iload 4
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 15
aload 15
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 17
aload 17
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 16
iload 8
ifne L7
iload 3
ifeq L8
L7:
iload 6
ifeq L8
aload 16
ifnull L9
aload 13
ifnonnull L10
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 11
lload 11
lload 11
iconst_3
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 13
L11:
iload 2
tableswitch 0
L12
L13
default : L14
L14:
iload 4
iconst_1
iadd
istore 4
goto L5
L12:
aload 16
aload 0
aload 15
aload 13
invokevirtual android/support/design/widget/t/b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
pop
goto L14
L13:
aload 16
aload 0
aload 15
aload 13
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
pop
goto L14
L8:
iload 8
istore 9
iload 8
ifne L15
iload 8
istore 9
aload 16
ifnull L15
iload 2
tableswitch 0
L16
L17
default : L18
L18:
iload 8
istore 9
iload 8
ifeq L15
aload 0
aload 15
putfield android/support/design/widget/CoordinatorLayout/t Landroid/view/View;
iload 8
istore 9
L15:
iload 9
istore 8
aload 17
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
ifnonnull L19
aload 17
iconst_0
putfield android/support/design/widget/w/i Z
L19:
aload 17
getfield android/support/design/widget/w/i Z
istore 10
aload 17
getfield android/support/design/widget/w/i Z
ifeq L20
iconst_1
istore 9
L21:
iload 9
ifeq L22
iload 10
ifne L22
iconst_1
istore 3
L23:
iload 9
ifeq L24
iload 8
istore 9
iload 3
ifeq L25
L24:
goto L14
L16:
aload 16
aload 0
aload 15
aload 1
invokevirtual android/support/design/widget/t/b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
istore 8
goto L18
L17:
aload 16
aload 0
aload 15
aload 1
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
istore 8
goto L18
L20:
aload 17
getfield android/support/design/widget/w/i Z
iconst_0
ior
istore 9
aload 17
iload 9
putfield android/support/design/widget/w/i Z
goto L21
L22:
iconst_0
istore 3
goto L23
L6:
iload 8
istore 9
L25:
aload 14
invokeinterface java/util/List/clear()V 0
iload 9
ireturn
L10:
goto L11
L9:
goto L14
.limit locals 18
.limit stack 8
.end method

.method private a(Landroid/view/View;Landroid/view/View;)Z
iconst_0
istore 4
iload 4
istore 3
aload 1
invokevirtual android/view/View/getVisibility()I
ifne L0
iload 4
istore 3
aload 2
invokevirtual android/view/View/getVisibility()I
ifne L0
aload 0
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 5
aload 1
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 0
if_acmpeq L1
iconst_1
istore 3
L2:
aload 0
aload 1
iload 3
aload 5
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 0
getfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
astore 1
aload 2
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 0
if_acmpeq L3
iconst_1
istore 3
L4:
aload 0
aload 2
iload 3
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
iload 4
istore 3
aload 5
getfield android/graphics/Rect/left I
aload 1
getfield android/graphics/Rect/right I
if_icmpgt L0
iload 4
istore 3
aload 5
getfield android/graphics/Rect/top I
aload 1
getfield android/graphics/Rect/bottom I
if_icmpgt L0
iload 4
istore 3
aload 5
getfield android/graphics/Rect/right I
aload 1
getfield android/graphics/Rect/left I
if_icmplt L0
iload 4
istore 3
aload 5
getfield android/graphics/Rect/bottom I
aload 1
getfield android/graphics/Rect/top I
if_icmplt L0
iconst_1
istore 3
L0:
iload 3
ireturn
L1:
iconst_0
istore 3
goto L2
L3:
iconst_0
istore 3
goto L4
.limit locals 6
.limit stack 4
.end method

.method private static b(I)I
iload 0
istore 1
iload 0
ifne L0
ldc_w 8388659
istore 1
L0:
iload 1
ireturn
.limit locals 2
.limit stack 1
.end method

.method private static b(Landroid/view/View;)Landroid/support/design/widget/w;
.catch java/lang/Exception from L0 to L1 using L2
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 3
aload 3
getfield android/support/design/widget/w/b Z
ifne L3
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
astore 1
aconst_null
astore 0
L4:
aload 0
astore 2
aload 1
ifnull L5
aload 1
ldc android/support/design/widget/u
invokevirtual java/lang/Class/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast android/support/design/widget/u
astore 0
aload 0
astore 2
aload 0
ifnonnull L5
aload 1
invokevirtual java/lang/Class/getSuperclass()Ljava/lang/Class;
astore 1
goto L4
L5:
aload 2
ifnull L1
L0:
aload 3
aload 2
invokeinterface android/support/design/widget/u/a()Ljava/lang/Class; 0
invokevirtual java/lang/Class/newInstance()Ljava/lang/Object;
checkcast android/support/design/widget/t
invokevirtual android/support/design/widget/w/a(Landroid/support/design/widget/t;)V
L1:
aload 3
iconst_1
putfield android/support/design/widget/w/b Z
L3:
aload 3
areturn
L2:
astore 0
ldc "CoordinatorLayout"
new java/lang/StringBuilder
dup
ldc "Default behavior class "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokeinterface android/support/design/widget/u/a()Ljava/lang/Class; 0
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " could not be instantiated. Did you forget a default constructor?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
.limit locals 4
.limit stack 4
.end method

.method private b()V
iconst_0
istore 4
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 5
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
invokeinterface java/util/List/size()I 0
iload 5
if_icmpeq L0
iconst_1
istore 2
L1:
iconst_0
istore 3
L2:
iload 3
iload 5
if_icmpge L3
aload 0
iload 3
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 8
aload 8
invokestatic android/support/design/widget/CoordinatorLayout/b(Landroid/view/View;)Landroid/support/design/widget/w;
astore 9
aload 9
getfield android/support/design/widget/w/f I
iconst_m1
if_icmpne L4
aload 9
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 9
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
L5:
iload 3
iconst_1
iadd
istore 3
goto L2
L0:
iconst_0
istore 2
goto L1
L4:
aload 9
getfield android/support/design/widget/w/g Landroid/view/View;
ifnull L6
aload 9
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/view/View/getId()I
aload 9
getfield android/support/design/widget/w/f I
if_icmpeq L7
iconst_0
istore 1
L8:
iload 1
ifne L5
L6:
aload 9
aload 0
aload 9
getfield android/support/design/widget/w/f I
invokevirtual android/support/design/widget/CoordinatorLayout/findViewById(I)Landroid/view/View;
putfield android/support/design/widget/w/g Landroid/view/View;
aload 9
getfield android/support/design/widget/w/g Landroid/view/View;
ifnull L9
aload 9
getfield android/support/design/widget/w/g Landroid/view/View;
astore 7
aload 9
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 6
L10:
aload 6
aload 0
if_acmpeq L11
aload 6
ifnull L11
aload 6
aload 8
if_acmpne L12
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/isInEditMode()Z
ifeq L13
aload 9
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 9
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
goto L5
L7:
aload 9
getfield android/support/design/widget/w/g Landroid/view/View;
astore 7
aload 9
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 6
L14:
aload 6
aload 0
if_acmpeq L15
aload 6
ifnull L16
aload 6
aload 8
if_acmpne L17
L16:
aload 9
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 9
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
iconst_0
istore 1
goto L8
L17:
aload 6
instanceof android/view/View
ifeq L18
aload 6
checkcast android/view/View
astore 7
L18:
aload 6
invokeinterface android/view/ViewParent/getParent()Landroid/view/ViewParent; 0
astore 6
goto L14
L15:
aload 9
aload 7
putfield android/support/design/widget/w/h Landroid/view/View;
iconst_1
istore 1
goto L8
L13:
new java/lang/IllegalStateException
dup
ldc "Anchor must not be a descendant of the anchored view"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L12:
aload 6
instanceof android/view/View
ifeq L19
aload 6
checkcast android/view/View
astore 7
L19:
aload 6
invokeinterface android/view/ViewParent/getParent()Landroid/view/ViewParent; 0
astore 6
goto L10
L11:
aload 9
aload 7
putfield android/support/design/widget/w/h Landroid/view/View;
goto L5
L9:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/isInEditMode()Z
ifeq L20
aload 9
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 9
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
goto L5
L20:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Could not find CoordinatorLayout descendant view with id "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getResources()Landroid/content/res/Resources;
aload 9
getfield android/support/design/widget/w/f I
invokevirtual android/content/res/Resources/getResourceName(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " to anchor view "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
iload 2
ifeq L21
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
invokeinterface java/util/List/clear()V 0
iload 4
istore 1
L22:
iload 1
iload 5
if_icmpge L23
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
aload 0
iload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 1
iconst_1
iadd
istore 1
goto L22
L23:
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
aload 0
getfield android/support/design/widget/CoordinatorLayout/e Ljava/util/Comparator;
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
L21:
return
.limit locals 10
.limit stack 5
.end method

.method private b(Landroid/view/View;I)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 3
aload 0
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 4
aload 4
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingLeft()I
aload 3
getfield android/support/design/widget/w/leftMargin I
iadd
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingTop()I
aload 3
getfield android/support/design/widget/w/topMargin I
iadd
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getWidth()I
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingRight()I
isub
aload 3
getfield android/support/design/widget/w/rightMargin I
isub
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getHeight()I
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingBottom()I
isub
aload 3
getfield android/support/design/widget/w/bottomMargin I
isub
invokevirtual android/graphics/Rect/set(IIII)V
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
ifnull L0
aload 0
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifeq L0
aload 1
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifne L0
aload 4
aload 4
getfield android/graphics/Rect/left I
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/a()I
iadd
putfield android/graphics/Rect/left I
aload 4
aload 4
getfield android/graphics/Rect/top I
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/b()I
iadd
putfield android/graphics/Rect/top I
aload 4
aload 4
getfield android/graphics/Rect/right I
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/c()I
isub
putfield android/graphics/Rect/right I
aload 4
aload 4
getfield android/graphics/Rect/bottom I
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/d()I
isub
putfield android/graphics/Rect/bottom I
L0:
aload 0
getfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
astore 5
aload 3
getfield android/support/design/widget/w/c I
invokestatic android/support/design/widget/CoordinatorLayout/b(I)I
aload 1
invokevirtual android/view/View/getMeasuredWidth()I
aload 1
invokevirtual android/view/View/getMeasuredHeight()I
aload 4
aload 5
iload 2
invokestatic android/support/v4/view/v/a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V
aload 1
aload 5
getfield android/graphics/Rect/left I
aload 5
getfield android/graphics/Rect/top I
aload 5
getfield android/graphics/Rect/right I
aload 5
getfield android/graphics/Rect/bottom I
invokevirtual android/view/View/layout(IIII)V
return
.limit locals 6
.limit stack 6
.end method

.method private b(Landroid/view/View;II)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 10
aload 10
getfield android/support/design/widget/w/c I
invokestatic android/support/design/widget/CoordinatorLayout/c(I)I
iload 3
invokestatic android/support/v4/view/v/a(II)I
istore 9
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getWidth()I
istore 8
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getHeight()I
istore 7
aload 1
invokevirtual android/view/View/getMeasuredWidth()I
istore 5
aload 1
invokevirtual android/view/View/getMeasuredHeight()I
istore 6
iload 2
istore 4
iload 3
iconst_1
if_icmpne L0
iload 8
iload 2
isub
istore 4
L0:
aload 0
iload 4
invokespecial android/support/design/widget/CoordinatorLayout/a(I)I
iload 5
isub
istore 2
iconst_0
istore 3
iload 9
bipush 7
iand
lookupswitch
1 : L1
5 : L2
default : L3
L3:
iload 9
bipush 112
iand
lookupswitch
16 : L4
80 : L5
default : L6
L6:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingLeft()I
aload 10
getfield android/support/design/widget/w/leftMargin I
iadd
iload 2
iload 8
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingRight()I
isub
iload 5
isub
aload 10
getfield android/support/design/widget/w/rightMargin I
isub
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
istore 2
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingTop()I
aload 10
getfield android/support/design/widget/w/topMargin I
iadd
iload 3
iload 7
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingBottom()I
isub
iload 6
isub
aload 10
getfield android/support/design/widget/w/bottomMargin I
isub
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
istore 3
aload 1
iload 2
iload 3
iload 2
iload 5
iadd
iload 3
iload 6
iadd
invokevirtual android/view/View/layout(IIII)V
return
L2:
iload 2
iload 5
iadd
istore 2
goto L3
L1:
iload 2
iload 5
iconst_2
idiv
iadd
istore 2
goto L3
L5:
iload 6
iconst_0
iadd
istore 3
goto L6
L4:
iload 6
iconst_2
idiv
iconst_0
iadd
istore 3
goto L6
.limit locals 11
.limit stack 6
.end method

.method private static b(Landroid/view/View;Landroid/graphics/Rect;)V
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/l Landroid/graphics/Rect;
aload 1
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static c(I)I
iload 0
istore 1
iload 0
ifne L0
ldc_w 8388661
istore 1
L0:
iload 1
ireturn
.limit locals 2
.limit stack 1
.end method

.method private c()V
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 6
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 7
aload 7
getfield android/support/design/widget/w/g Landroid/view/View;
ifnull L2
iconst_1
istore 1
L3:
iload 1
ifeq L4
iconst_1
istore 5
L5:
iload 5
aload 0
getfield android/support/design/widget/CoordinatorLayout/x Z
if_icmpeq L6
iload 5
ifeq L7
aload 0
getfield android/support/design/widget/CoordinatorLayout/r Z
ifeq L8
aload 0
getfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
ifnonnull L9
aload 0
new android/support/design/widget/x
dup
aload 0
invokespecial android/support/design/widget/x/<init>(Landroid/support/design/widget/CoordinatorLayout;)V
putfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
L9:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
invokevirtual android/view/ViewTreeObserver/addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
L8:
aload 0
iconst_1
putfield android/support/design/widget/CoordinatorLayout/x Z
L6:
return
L2:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 4
iconst_0
istore 1
L10:
iload 1
iload 4
if_icmpge L11
aload 0
iload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 8
aload 8
aload 6
if_acmpeq L12
aload 7
aload 8
invokevirtual android/support/design/widget/w/a(Landroid/view/View;)Z
ifeq L12
iconst_1
istore 1
goto L3
L12:
iload 1
iconst_1
iadd
istore 1
goto L10
L11:
iconst_0
istore 1
goto L3
L4:
iload 2
iconst_1
iadd
istore 2
goto L0
L7:
aload 0
getfield android/support/design/widget/CoordinatorLayout/r Z
ifeq L13
aload 0
getfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
ifnull L13
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
invokevirtual android/view/ViewTreeObserver/removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
L13:
aload 0
iconst_0
putfield android/support/design/widget/CoordinatorLayout/x Z
return
L1:
iconst_0
istore 5
goto L5
.limit locals 9
.limit stack 4
.end method

.method private c(Landroid/view/View;)V
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 4
aload 4
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 5
aload 5
ifnull L2
aload 5
aload 1
invokevirtual android/support/design/widget/t/b(Landroid/view/View;)Z
ifeq L2
aload 5
aload 4
aload 1
invokevirtual android/support/design/widget/t/a(Landroid/view/View;Landroid/view/View;)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 6
.limit stack 3
.end method

.method private c(Landroid/view/View;I)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 4
aload 4
getfield android/support/design/widget/w/g Landroid/view/View;
ifnull L0
aload 0
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 5
aload 0
getfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
astore 6
aload 0
getfield android/support/design/widget/CoordinatorLayout/o Landroid/graphics/Rect;
astore 7
aload 0
aload 4
getfield android/support/design/widget/w/g Landroid/view/View;
aload 5
invokestatic android/support/design/widget/cz/a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
aload 0
aload 1
iconst_0
aload 6
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 0
aload 1
iload 2
aload 5
aload 7
invokespecial android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V
aload 7
getfield android/graphics/Rect/left I
aload 6
getfield android/graphics/Rect/left I
isub
istore 2
aload 7
getfield android/graphics/Rect/top I
aload 6
getfield android/graphics/Rect/top I
isub
istore 3
iload 2
ifeq L1
aload 1
iload 2
invokevirtual android/view/View/offsetLeftAndRight(I)V
L1:
iload 3
ifeq L2
aload 1
iload 3
invokevirtual android/view/View/offsetTopAndBottom(I)V
L2:
iload 2
ifne L3
iload 3
ifeq L0
L3:
aload 4
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 5
aload 5
ifnull L0
aload 5
aload 0
aload 1
aload 4
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
pop
L0:
return
.limit locals 8
.limit stack 5
.end method

.method private static c(Landroid/view/View;Landroid/graphics/Rect;)V
aload 1
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/l Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static d(I)I
iload 0
istore 1
iload 0
ifne L0
bipush 17
istore 1
L0:
iload 1
ireturn
.limit locals 2
.limit stack 1
.end method

.method private d()V
aload 0
getfield android/support/design/widget/CoordinatorLayout/r Z
ifeq L0
aload 0
getfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
ifnonnull L1
aload 0
new android/support/design/widget/x
dup
aload 0
invokespecial android/support/design/widget/x/<init>(Landroid/support/design/widget/CoordinatorLayout;)V
putfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
L1:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
invokevirtual android/view/ViewTreeObserver/addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
L0:
aload 0
iconst_1
putfield android/support/design/widget/CoordinatorLayout/x Z
return
.limit locals 1
.limit stack 4
.end method

.method private d(Landroid/view/View;)V
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 4
iconst_0
istore 3
iconst_0
istore 2
L0:
iload 3
iload 4
if_icmpge L1
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 5
aload 5
aload 1
if_acmpne L2
iconst_1
istore 2
L3:
iload 3
iconst_1
iadd
istore 3
goto L0
L2:
iload 2
ifeq L4
aload 5
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 6
aload 6
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 7
aload 7
ifnull L4
aload 6
aload 1
invokevirtual android/support/design/widget/w/a(Landroid/view/View;)Z
ifeq L4
aload 7
aload 0
aload 5
aload 1
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
pop
L4:
goto L3
L1:
return
.limit locals 8
.limit stack 4
.end method

.method private e()V
aload 0
getfield android/support/design/widget/CoordinatorLayout/r Z
ifeq L0
aload 0
getfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
ifnull L0
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
invokevirtual android/view/ViewTreeObserver/removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
L0:
aload 0
iconst_0
putfield android/support/design/widget/CoordinatorLayout/x Z
return
.limit locals 1
.limit stack 2
.end method

.method private e(Landroid/view/View;)Z
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 4
aload 4
getfield android/support/design/widget/w/g Landroid/view/View;
ifnull L0
iconst_1
ireturn
L0:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 3
iconst_0
istore 2
L1:
iload 2
iload 3
if_icmpge L2
aload 0
iload 2
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 5
aload 5
aload 1
if_acmpeq L3
aload 4
aload 5
invokevirtual android/support/design/widget/w/a(Landroid/view/View;)Z
ifeq L3
iconst_1
ireturn
L3:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
iconst_0
ireturn
.limit locals 6
.limit stack 2
.end method

.method private static f()Landroid/support/design/widget/w;
new android/support/design/widget/w
dup
invokespecial android/support/design/widget/w/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private setWindowInsets(Landroid/support/v4/view/gh;)V
iconst_1
istore 5
iconst_0
istore 2
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
aload 1
if_acmpeq L0
aload 0
aload 1
putfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
aload 1
ifnull L1
aload 1
invokevirtual android/support/v4/view/gh/b()I
ifle L1
iconst_1
istore 4
L2:
aload 0
iload 4
putfield android/support/design/widget/CoordinatorLayout/z Z
aload 0
getfield android/support/design/widget/CoordinatorLayout/z Z
ifne L3
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getBackground()Landroid/graphics/drawable/Drawable;
ifnonnull L3
iload 5
istore 4
L4:
aload 0
iload 4
invokevirtual android/support/design/widget/CoordinatorLayout/setWillNotDraw(Z)V
aload 1
invokevirtual android/support/v4/view/gh/g()Z
ifne L5
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 3
L6:
iload 2
iload 3
if_icmpge L5
aload 0
iload 2
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 7
aload 1
astore 6
aload 7
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifeq L7
aload 7
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
ifnull L8
aload 1
invokevirtual android/support/v4/view/gh/g()Z
ifne L5
L8:
aload 7
aload 1
invokestatic android/support/v4/view/cx/b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
astore 6
aload 6
invokevirtual android/support/v4/view/gh/g()Z
ifne L5
L7:
iload 2
iconst_1
iadd
istore 2
aload 6
astore 1
goto L6
L1:
iconst_0
istore 4
goto L2
L3:
iconst_0
istore 4
goto L4
L5:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/requestLayout()V
L0:
return
.limit locals 8
.limit stack 2
.end method

.method public final a(Landroid/view/View;)Ljava/util/List;
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 4
aload 0
getfield android/support/design/widget/CoordinatorLayout/n Ljava/util/List;
astore 5
aload 5
invokeinterface java/util/List/clear()V 0
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 6
aload 6
aload 1
if_acmpeq L2
aload 4
aload 6
invokevirtual android/support/design/widget/w/a(Landroid/view/View;)Z
ifeq L2
aload 5
aload 6
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 5
areturn
.limit locals 7
.limit stack 2
.end method

.method public final a(Landroid/view/View;I)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 10
aload 10
getfield android/support/design/widget/w/g Landroid/view/View;
ifnonnull L0
aload 10
getfield android/support/design/widget/w/f I
iconst_m1
if_icmpeq L0
iconst_1
istore 3
L1:
iload 3
ifeq L2
new java/lang/IllegalStateException
dup
ldc "An anchor may not be changed after CoordinatorLayout measurement begins before layout is complete."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
iconst_0
istore 3
goto L1
L2:
aload 10
getfield android/support/design/widget/w/g Landroid/view/View;
ifnull L3
aload 10
getfield android/support/design/widget/w/g Landroid/view/View;
astore 10
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
pop
aload 0
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 11
aload 0
getfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
astore 12
aload 0
aload 10
aload 11
invokestatic android/support/design/widget/cz/a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
aload 0
aload 1
iload 2
aload 11
aload 12
invokespecial android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V
aload 1
aload 12
getfield android/graphics/Rect/left I
aload 12
getfield android/graphics/Rect/top I
aload 12
getfield android/graphics/Rect/right I
aload 12
getfield android/graphics/Rect/bottom I
invokevirtual android/view/View/layout(IIII)V
return
L3:
aload 10
getfield android/support/design/widget/w/e I
iflt L4
aload 10
getfield android/support/design/widget/w/e I
istore 4
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 10
aload 10
getfield android/support/design/widget/w/c I
invokestatic android/support/design/widget/CoordinatorLayout/c(I)I
iload 2
invokestatic android/support/v4/view/v/a(II)I
istore 9
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getWidth()I
istore 8
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getHeight()I
istore 7
aload 1
invokevirtual android/view/View/getMeasuredWidth()I
istore 5
aload 1
invokevirtual android/view/View/getMeasuredHeight()I
istore 6
iload 4
istore 3
iload 2
iconst_1
if_icmpne L5
iload 8
iload 4
isub
istore 3
L5:
aload 0
iload 3
invokespecial android/support/design/widget/CoordinatorLayout/a(I)I
iload 5
isub
istore 2
iload 9
bipush 7
iand
lookupswitch
1 : L6
5 : L7
default : L8
L8:
iload 9
bipush 112
iand
lookupswitch
16 : L9
80 : L10
default : L11
L11:
iconst_0
istore 3
L12:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingLeft()I
aload 10
getfield android/support/design/widget/w/leftMargin I
iadd
iload 2
iload 8
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingRight()I
isub
iload 5
isub
aload 10
getfield android/support/design/widget/w/rightMargin I
isub
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
istore 2
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingTop()I
aload 10
getfield android/support/design/widget/w/topMargin I
iadd
iload 3
iload 7
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingBottom()I
isub
iload 6
isub
aload 10
getfield android/support/design/widget/w/bottomMargin I
isub
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
istore 3
aload 1
iload 2
iload 3
iload 2
iload 5
iadd
iload 3
iload 6
iadd
invokevirtual android/view/View/layout(IIII)V
return
L7:
iload 2
iload 5
iadd
istore 2
goto L8
L6:
iload 2
iload 5
iconst_2
idiv
iadd
istore 2
goto L8
L10:
iload 6
iconst_0
iadd
istore 3
goto L12
L9:
iload 6
iconst_2
idiv
iconst_0
iadd
istore 3
goto L12
L4:
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 10
aload 0
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 11
aload 11
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingLeft()I
aload 10
getfield android/support/design/widget/w/leftMargin I
iadd
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingTop()I
aload 10
getfield android/support/design/widget/w/topMargin I
iadd
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getWidth()I
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingRight()I
isub
aload 10
getfield android/support/design/widget/w/rightMargin I
isub
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getHeight()I
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingBottom()I
isub
aload 10
getfield android/support/design/widget/w/bottomMargin I
isub
invokevirtual android/graphics/Rect/set(IIII)V
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
ifnull L13
aload 0
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifeq L13
aload 1
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifne L13
aload 11
aload 11
getfield android/graphics/Rect/left I
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/a()I
iadd
putfield android/graphics/Rect/left I
aload 11
aload 11
getfield android/graphics/Rect/top I
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/b()I
iadd
putfield android/graphics/Rect/top I
aload 11
aload 11
getfield android/graphics/Rect/right I
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/c()I
isub
putfield android/graphics/Rect/right I
aload 11
aload 11
getfield android/graphics/Rect/bottom I
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/d()I
isub
putfield android/graphics/Rect/bottom I
L13:
aload 0
getfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
astore 12
aload 10
getfield android/support/design/widget/w/c I
invokestatic android/support/design/widget/CoordinatorLayout/b(I)I
aload 1
invokevirtual android/view/View/getMeasuredWidth()I
aload 1
invokevirtual android/view/View/getMeasuredHeight()I
aload 11
aload 12
iload 2
invokestatic android/support/v4/view/v/a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V
aload 1
aload 12
getfield android/graphics/Rect/left I
aload 12
getfield android/graphics/Rect/top I
aload 12
getfield android/graphics/Rect/right I
aload 12
getfield android/graphics/Rect/bottom I
invokevirtual android/view/View/layout(IIII)V
return
.limit locals 13
.limit stack 6
.end method

.method public final a(Landroid/view/View;III)V
aload 0
aload 1
iload 2
iload 3
iload 4
iconst_0
invokevirtual android/support/design/widget/CoordinatorLayout/measureChildWithMargins(Landroid/view/View;IIII)V
return
.limit locals 5
.limit stack 6
.end method

.method final a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 1
invokevirtual android/view/View/isLayoutRequested()Z
ifne L0
aload 1
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpne L1
L0:
aload 3
iconst_0
iconst_0
iconst_0
iconst_0
invokevirtual android/graphics/Rect/set(IIII)V
return
L1:
iload 2
ifeq L2
aload 0
aload 1
aload 3
invokestatic android/support/design/widget/cz/a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
return
L2:
aload 3
aload 1
invokevirtual android/view/View/getLeft()I
aload 1
invokevirtual android/view/View/getTop()I
aload 1
invokevirtual android/view/View/getRight()I
aload 1
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/Rect/set(IIII)V
return
.limit locals 4
.limit stack 5
.end method

.method final a(Z)V
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
istore 4
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 5
iconst_0
istore 2
L0:
iload 2
iload 5
if_icmpge L1
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 8
aload 8
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 9
iconst_0
istore 3
L2:
iload 3
iload 2
if_icmpge L3
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 10
aload 9
getfield android/support/design/widget/w/h Landroid/view/View;
aload 10
if_acmpne L4
aload 8
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 10
aload 10
getfield android/support/design/widget/w/g Landroid/view/View;
ifnull L4
aload 0
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 11
aload 0
getfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
astore 12
aload 0
getfield android/support/design/widget/CoordinatorLayout/o Landroid/graphics/Rect;
astore 13
aload 0
aload 10
getfield android/support/design/widget/w/g Landroid/view/View;
aload 11
invokestatic android/support/design/widget/cz/a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
aload 0
aload 8
iconst_0
aload 12
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 0
aload 8
iload 4
aload 11
aload 13
invokespecial android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V
aload 13
getfield android/graphics/Rect/left I
aload 12
getfield android/graphics/Rect/left I
isub
istore 6
aload 13
getfield android/graphics/Rect/top I
aload 12
getfield android/graphics/Rect/top I
isub
istore 7
iload 6
ifeq L5
aload 8
iload 6
invokevirtual android/view/View/offsetLeftAndRight(I)V
L5:
iload 7
ifeq L6
aload 8
iload 7
invokevirtual android/view/View/offsetTopAndBottom(I)V
L6:
iload 6
ifne L7
iload 7
ifeq L4
L7:
aload 10
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 11
aload 11
ifnull L4
aload 11
aload 0
aload 8
aload 10
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
pop
L4:
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
aload 0
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 9
aload 0
getfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
astore 10
aload 9
aload 8
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/l Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
aload 0
aload 8
iconst_1
aload 10
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 9
aload 10
invokevirtual android/graphics/Rect/equals(Ljava/lang/Object;)Z
ifne L8
aload 8
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/l Landroid/graphics/Rect;
aload 10
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
iload 2
iconst_1
iadd
istore 3
L9:
iload 3
iload 5
if_icmpge L8
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 9
aload 9
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 10
aload 10
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 11
aload 11
ifnull L10
aload 11
aload 8
invokevirtual android/support/design/widget/t/b(Landroid/view/View;)Z
ifeq L10
iload 1
ifne L11
aload 10
getfield android/support/design/widget/w/k Z
ifeq L11
aload 10
iconst_0
putfield android/support/design/widget/w/k Z
L10:
iload 3
iconst_1
iadd
istore 3
goto L9
L11:
aload 11
aload 0
aload 9
aload 8
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
pop
iload 1
ifeq L10
aload 10
iconst_0
putfield android/support/design/widget/w/k Z
goto L10
L8:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 14
.limit stack 5
.end method

.method public final a(Landroid/view/View;II)Z
aload 0
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 4
aload 0
aload 1
aload 4
invokestatic android/support/design/widget/cz/a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
aload 4
iload 2
iload 3
invokevirtual android/graphics/Rect/contains(II)Z
ireturn
.limit locals 5
.limit stack 3
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 1
instanceof android/support/design/widget/w
ifeq L0
aload 0
aload 1
invokespecial android/view/ViewGroup/checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
aload 2
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
pop
aload 0
aload 1
aload 2
lload 3
invokespecial android/view/ViewGroup/drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
ireturn
.limit locals 5
.limit stack 5
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
new android/support/design/widget/w
dup
invokespecial android/support/design/widget/w/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
new android/support/design/widget/w
dup
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/design/widget/w/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method protected synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
aload 1
instanceof android/support/design/widget/w
ifeq L0
new android/support/design/widget/w
dup
aload 1
checkcast android/support/design/widget/w
invokespecial android/support/design/widget/w/<init>(Landroid/support/design/widget/w;)V
areturn
L0:
aload 1
instanceof android/view/ViewGroup$MarginLayoutParams
ifeq L1
new android/support/design/widget/w
dup
aload 1
checkcast android/view/ViewGroup$MarginLayoutParams
invokespecial android/support/design/widget/w/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
areturn
L1:
new android/support/design/widget/w
dup
aload 1
invokespecial android/support/design/widget/w/<init>(Landroid/view/ViewGroup$LayoutParams;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public getNestedScrollAxes()I
aload 0
getfield android/support/design/widget/CoordinatorLayout/C Landroid/support/v4/view/bw;
getfield android/support/v4/view/bw/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getStatusBarBackground()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/CoordinatorLayout/A Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected getSuggestedMinimumHeight()I
aload 0
invokespecial android/view/ViewGroup/getSuggestedMinimumHeight()I
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingTop()I
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingBottom()I
iadd
invokestatic java/lang/Math/max(II)I
ireturn
.limit locals 1
.limit stack 3
.end method

.method protected getSuggestedMinimumWidth()I
aload 0
invokespecial android/view/ViewGroup/getSuggestedMinimumWidth()I
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingLeft()I
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingRight()I
iadd
invokestatic java/lang/Math/max(II)I
ireturn
.limit locals 1
.limit stack 3
.end method

.method public onAttachedToWindow()V
aload 0
invokespecial android/view/ViewGroup/onAttachedToWindow()V
aload 0
invokespecial android/support/design/widget/CoordinatorLayout/a()V
aload 0
getfield android/support/design/widget/CoordinatorLayout/x Z
ifeq L0
aload 0
getfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
ifnonnull L1
aload 0
new android/support/design/widget/x
dup
aload 0
invokespecial android/support/design/widget/x/<init>(Landroid/support/design/widget/CoordinatorLayout;)V
putfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
L1:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
invokevirtual android/view/ViewTreeObserver/addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
L0:
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
ifnonnull L2
aload 0
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifeq L2
aload 0
invokestatic android/support/v4/view/cx/t(Landroid/view/View;)V
L2:
aload 0
iconst_1
putfield android/support/design/widget/CoordinatorLayout/r Z
return
.limit locals 1
.limit stack 4
.end method

.method public onDetachedFromWindow()V
aload 0
invokespecial android/view/ViewGroup/onDetachedFromWindow()V
aload 0
invokespecial android/support/design/widget/CoordinatorLayout/a()V
aload 0
getfield android/support/design/widget/CoordinatorLayout/x Z
ifeq L0
aload 0
getfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
ifnull L0
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/design/widget/CoordinatorLayout/w Landroid/support/design/widget/x;
invokevirtual android/view/ViewTreeObserver/removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
L0:
aload 0
getfield android/support/design/widget/CoordinatorLayout/v Landroid/view/View;
ifnull L1
aload 0
aload 0
getfield android/support/design/widget/CoordinatorLayout/v Landroid/view/View;
invokevirtual android/support/design/widget/CoordinatorLayout/onStopNestedScroll(Landroid/view/View;)V
L1:
aload 0
iconst_0
putfield android/support/design/widget/CoordinatorLayout/r Z
return
.limit locals 1
.limit stack 2
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/view/ViewGroup/onDraw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/design/widget/CoordinatorLayout/z Z
ifeq L0
aload 0
getfield android/support/design/widget/CoordinatorLayout/A Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
ifnull L1
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/b()I
istore 2
L2:
iload 2
ifle L0
aload 0
getfield android/support/design/widget/CoordinatorLayout/A Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getWidth()I
iload 2
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield android/support/design/widget/CoordinatorLayout/A Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
L0:
return
L1:
iconst_0
istore 2
goto L2
.limit locals 3
.limit stack 5
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 2
iload 2
ifne L0
aload 0
invokespecial android/support/design/widget/CoordinatorLayout/a()V
L0:
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/CoordinatorLayout/a(Landroid/view/MotionEvent;I)Z
istore 3
iload 2
iconst_1
if_icmpeq L1
iload 2
iconst_3
if_icmpne L2
L1:
aload 0
invokespecial android/support/design/widget/CoordinatorLayout/a()V
L2:
iload 3
ireturn
.limit locals 4
.limit stack 3
.end method

.method protected onLayout(ZIIII)V
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
istore 3
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 4
iconst_0
istore 2
L0:
iload 2
iload 4
if_icmpge L1
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 6
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 7
aload 7
ifnull L2
aload 7
aload 0
aload 6
iload 3
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
ifne L3
L2:
aload 0
aload 6
iload 3
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;I)V
L3:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 8
.limit stack 4
.end method

.method protected onMeasure(II)V
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 6
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
invokeinterface java/util/List/size()I 0
iload 6
if_icmpeq L0
iconst_1
istore 4
L1:
iconst_0
istore 5
L2:
iload 5
iload 6
if_icmpge L3
aload 0
iload 5
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 26
aload 26
invokestatic android/support/design/widget/CoordinatorLayout/b(Landroid/view/View;)Landroid/support/design/widget/w;
astore 27
aload 27
getfield android/support/design/widget/w/f I
iconst_m1
if_icmpne L4
aload 27
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 27
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
L5:
iload 5
iconst_1
iadd
istore 5
goto L2
L0:
iconst_0
istore 4
goto L1
L4:
aload 27
getfield android/support/design/widget/w/g Landroid/view/View;
ifnull L6
aload 27
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/view/View/getId()I
aload 27
getfield android/support/design/widget/w/f I
if_icmpeq L7
iconst_0
istore 3
L8:
iload 3
ifne L5
L6:
aload 27
aload 0
aload 27
getfield android/support/design/widget/w/f I
invokevirtual android/support/design/widget/CoordinatorLayout/findViewById(I)Landroid/view/View;
putfield android/support/design/widget/w/g Landroid/view/View;
aload 27
getfield android/support/design/widget/w/g Landroid/view/View;
ifnull L9
aload 27
getfield android/support/design/widget/w/g Landroid/view/View;
astore 25
aload 27
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 24
L10:
aload 24
aload 0
if_acmpeq L11
aload 24
ifnull L11
aload 24
aload 26
if_acmpne L12
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/isInEditMode()Z
ifeq L13
aload 27
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 27
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
goto L5
L7:
aload 27
getfield android/support/design/widget/w/g Landroid/view/View;
astore 25
aload 27
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 24
L14:
aload 24
aload 0
if_acmpeq L15
aload 24
ifnull L16
aload 24
aload 26
if_acmpne L17
L16:
aload 27
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 27
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
iconst_0
istore 3
goto L8
L17:
aload 24
instanceof android/view/View
ifeq L18
aload 24
checkcast android/view/View
astore 25
L18:
aload 24
invokeinterface android/view/ViewParent/getParent()Landroid/view/ViewParent; 0
astore 24
goto L14
L15:
aload 27
aload 25
putfield android/support/design/widget/w/h Landroid/view/View;
iconst_1
istore 3
goto L8
L13:
new java/lang/IllegalStateException
dup
ldc "Anchor must not be a descendant of the anchored view"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L12:
aload 24
instanceof android/view/View
ifeq L19
aload 24
checkcast android/view/View
astore 25
L19:
aload 24
invokeinterface android/view/ViewParent/getParent()Landroid/view/ViewParent; 0
astore 24
goto L10
L11:
aload 27
aload 25
putfield android/support/design/widget/w/h Landroid/view/View;
goto L5
L9:
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/isInEditMode()Z
ifeq L20
aload 27
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 27
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
goto L5
L20:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Could not find CoordinatorLayout descendant view with id "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getResources()Landroid/content/res/Resources;
aload 27
getfield android/support/design/widget/w/f I
invokevirtual android/content/res/Resources/getResourceName(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " to anchor view "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 26
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
iload 4
ifeq L21
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
invokeinterface java/util/List/clear()V 0
iconst_0
istore 3
L22:
iload 3
iload 6
if_icmpge L23
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
aload 0
iload 3
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 3
iconst_1
iadd
istore 3
goto L22
L23:
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
aload 0
getfield android/support/design/widget/CoordinatorLayout/e Ljava/util/Comparator;
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
L21:
aload 0
invokespecial android/support/design/widget/CoordinatorLayout/c()V
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingLeft()I
istore 12
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingTop()I
istore 13
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingRight()I
istore 14
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getPaddingBottom()I
istore 15
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
istore 16
iload 16
iconst_1
if_icmpne L24
iconst_1
istore 4
L25:
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 17
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 18
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 19
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 20
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getSuggestedMinimumWidth()I
istore 9
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getSuggestedMinimumHeight()I
istore 8
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
ifnull L26
aload 0
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifeq L26
iconst_1
istore 5
L27:
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 21
iconst_0
istore 6
iconst_0
istore 7
L28:
iload 6
iload 21
if_icmpge L29
aload 0
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
iload 6
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 24
aload 24
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 25
iconst_0
istore 10
iload 10
istore 3
aload 25
getfield android/support/design/widget/w/e I
iflt L30
iload 10
istore 3
iload 17
ifeq L30
aload 0
aload 25
getfield android/support/design/widget/w/e I
invokespecial android/support/design/widget/CoordinatorLayout/a(I)I
istore 11
aload 25
getfield android/support/design/widget/w/c I
invokestatic android/support/design/widget/CoordinatorLayout/c(I)I
iload 16
invokestatic android/support/v4/view/v/a(II)I
bipush 7
iand
istore 22
iload 22
iconst_3
if_icmpne L31
iload 4
ifeq L32
L31:
iload 22
iconst_5
if_icmpne L33
iload 4
ifeq L33
L32:
iconst_0
iload 18
iload 14
isub
iload 11
isub
invokestatic java/lang/Math/max(II)I
istore 3
L30:
iload 5
ifeq L34
aload 24
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifne L34
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/a()I
istore 10
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/c()I
istore 23
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/b()I
istore 11
aload 0
getfield android/support/design/widget/CoordinatorLayout/y Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/d()I
istore 22
iload 18
iload 10
iload 23
iadd
isub
iload 17
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 10
iload 20
iload 11
iload 22
iadd
isub
iload 19
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 11
L35:
aload 25
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 26
aload 26
ifnull L36
aload 26
aload 0
aload 24
iload 10
iload 3
iload 11
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)Z
ifne L37
L36:
aload 0
aload 24
iload 10
iload 3
iload 11
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;III)V
L37:
iload 9
aload 24
invokevirtual android/view/View/getMeasuredWidth()I
iload 12
iload 14
iadd
iadd
aload 25
getfield android/support/design/widget/w/leftMargin I
iadd
aload 25
getfield android/support/design/widget/w/rightMargin I
iadd
invokestatic java/lang/Math/max(II)I
istore 9
iload 8
aload 24
invokevirtual android/view/View/getMeasuredHeight()I
iload 13
iload 15
iadd
iadd
aload 25
getfield android/support/design/widget/w/topMargin I
iadd
aload 25
getfield android/support/design/widget/w/bottomMargin I
iadd
invokestatic java/lang/Math/max(II)I
istore 8
iload 7
aload 24
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v4/view/cx/a(II)I
istore 7
iload 6
iconst_1
iadd
istore 6
goto L28
L24:
iconst_0
istore 4
goto L25
L26:
iconst_0
istore 5
goto L27
L33:
iload 22
iconst_5
if_icmpne L38
iload 4
ifeq L39
L38:
iload 10
istore 3
iload 22
iconst_3
if_icmpne L30
iload 10
istore 3
iload 4
ifeq L30
L39:
iconst_0
iload 11
iload 12
isub
invokestatic java/lang/Math/max(II)I
istore 3
goto L30
L29:
aload 0
iload 9
iload 1
ldc_w -16777216
iload 7
iand
invokestatic android/support/v4/view/cx/a(III)I
iload 8
iload 2
iload 7
bipush 16
ishl
invokestatic android/support/v4/view/cx/a(III)I
invokevirtual android/support/design/widget/CoordinatorLayout/setMeasuredDimension(II)V
return
L34:
iload 2
istore 11
iload 1
istore 10
goto L35
.limit locals 28
.limit stack 6
.end method

.method public onNestedFling(Landroid/view/View;FFZ)Z
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 6
iconst_0
istore 5
iconst_0
istore 7
L0:
iload 5
iload 6
if_icmpge L1
aload 0
iload 5
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 1
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 8
aload 8
getfield android/support/design/widget/w/j Z
ifeq L2
aload 8
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 8
aload 8
ifnull L2
aload 8
aload 0
aload 1
fload 3
iload 4
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;FZ)Z
iload 7
ior
istore 7
L3:
iload 5
iconst_1
iadd
istore 5
goto L0
L1:
iload 7
ifeq L4
aload 0
iconst_1
invokevirtual android/support/design/widget/CoordinatorLayout/a(Z)V
L4:
iload 7
ireturn
L2:
goto L3
.limit locals 9
.limit stack 5
.end method

.method public onNestedPreFling(Landroid/view/View;FF)Z
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 5
iconst_0
istore 4
L0:
iload 4
iload 5
if_icmpge L1
aload 0
iload 4
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
pop
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
iconst_0
ireturn
.limit locals 6
.limit stack 2
.end method

.method public onNestedPreScroll(Landroid/view/View;II[I)V
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 11
iconst_0
istore 8
iconst_0
istore 7
iconst_0
istore 5
iconst_0
istore 6
L0:
iload 8
iload 11
if_icmpge L1
aload 0
iload 8
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 1
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 12
aload 12
getfield android/support/design/widget/w/j Z
ifeq L2
aload 12
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 12
aload 12
ifnull L2
aload 0
getfield android/support/design/widget/CoordinatorLayout/p [I
astore 13
aload 0
getfield android/support/design/widget/CoordinatorLayout/p [I
iconst_1
iconst_0
iastore
aload 13
iconst_0
iconst_0
iastore
aload 12
aload 0
aload 1
iload 3
aload 0
getfield android/support/design/widget/CoordinatorLayout/p [I
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I[I)V
iload 2
ifle L3
iload 6
aload 0
getfield android/support/design/widget/CoordinatorLayout/p [I
iconst_0
iaload
invokestatic java/lang/Math/max(II)I
istore 6
L4:
iload 3
ifle L5
iload 5
aload 0
getfield android/support/design/widget/CoordinatorLayout/p [I
iconst_1
iaload
invokestatic java/lang/Math/max(II)I
istore 5
L6:
iconst_1
istore 9
iload 6
istore 7
iload 5
istore 6
iload 9
istore 5
L7:
iload 8
iconst_1
iadd
istore 9
iload 7
istore 8
iload 5
istore 7
iload 6
istore 5
iload 8
istore 6
iload 9
istore 8
goto L0
L3:
iload 6
aload 0
getfield android/support/design/widget/CoordinatorLayout/p [I
iconst_0
iaload
invokestatic java/lang/Math/min(II)I
istore 6
goto L4
L5:
iload 5
aload 0
getfield android/support/design/widget/CoordinatorLayout/p [I
iconst_1
iaload
invokestatic java/lang/Math/min(II)I
istore 5
goto L6
L1:
aload 4
iconst_0
iload 6
iastore
aload 4
iconst_1
iload 5
iastore
iload 7
ifeq L8
aload 0
iconst_1
invokevirtual android/support/design/widget/CoordinatorLayout/a(Z)V
L8:
return
L2:
iload 5
istore 9
iload 6
istore 10
iload 7
istore 5
iload 9
istore 6
iload 10
istore 7
goto L7
.limit locals 14
.limit stack 5
.end method

.method public onNestedScroll(Landroid/view/View;IIII)V
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 4
iconst_0
istore 3
iconst_0
istore 2
L0:
iload 3
iload 4
if_icmpge L1
aload 0
iload 3
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 1
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 6
aload 6
getfield android/support/design/widget/w/j Z
ifeq L2
aload 6
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 6
aload 6
ifnull L2
aload 6
aload 0
aload 1
iload 5
invokevirtual android/support/design/widget/t/b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)V
iconst_1
istore 2
L3:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
iload 2
ifeq L4
aload 0
iconst_1
invokevirtual android/support/design/widget/CoordinatorLayout/a(Z)V
L4:
return
L2:
goto L3
.limit locals 7
.limit stack 4
.end method

.method public onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
aload 0
getfield android/support/design/widget/CoordinatorLayout/C Landroid/support/v4/view/bw;
iload 3
putfield android/support/v4/view/bw/a I
aload 0
aload 1
putfield android/support/design/widget/CoordinatorLayout/u Landroid/view/View;
aload 0
aload 2
putfield android/support/design/widget/CoordinatorLayout/v Landroid/view/View;
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 4
iconst_0
istore 3
L0:
iload 3
iload 4
if_icmpge L1
aload 0
iload 3
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
pop
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
return
.limit locals 5
.limit stack 2
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
checkcast android/support/design/widget/CoordinatorLayout$SavedState
astore 1
aload 0
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout$SavedState/getSuperState()Landroid/os/Parcelable;
invokespecial android/view/ViewGroup/onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
getfield android/support/design/widget/CoordinatorLayout$SavedState/a Landroid/util/SparseArray;
astore 1
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 5
aload 5
invokevirtual android/view/View/getId()I
istore 4
aload 5
invokestatic android/support/design/widget/CoordinatorLayout/b(Landroid/view/View;)Landroid/support/design/widget/w;
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 6
iload 4
iconst_m1
if_icmpeq L2
aload 6
ifnull L2
aload 1
iload 4
invokevirtual android/util/SparseArray/get(I)Ljava/lang/Object;
checkcast android/os/Parcelable
astore 7
aload 7
ifnull L2
aload 6
aload 0
aload 5
aload 7
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 8
.limit stack 4
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
new android/support/design/widget/CoordinatorLayout$SavedState
dup
aload 0
invokespecial android/view/ViewGroup/onSaveInstanceState()Landroid/os/Parcelable;
invokespecial android/support/design/widget/CoordinatorLayout$SavedState/<init>(Landroid/os/Parcelable;)V
astore 4
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 5
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 6
aload 6
invokevirtual android/view/View/getId()I
istore 3
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 7
iload 3
iconst_m1
if_icmpeq L2
aload 7
ifnull L2
aload 7
aload 0
aload 6
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;
astore 6
aload 6
ifnull L2
aload 5
iload 3
aload 6
invokevirtual android/util/SparseArray/append(ILjava/lang/Object;)V
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 4
aload 5
putfield android/support/design/widget/CoordinatorLayout$SavedState/a Landroid/util/SparseArray;
aload 4
areturn
.limit locals 8
.limit stack 3
.end method

.method public onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 5
iconst_0
istore 4
iconst_0
istore 6
L0:
iload 4
iload 5
if_icmpge L1
aload 0
iload 4
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
astore 2
aload 2
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 8
aload 8
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 9
aload 9
ifnull L2
aload 9
aload 0
aload 2
aload 1
iload 3
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;I)Z
istore 7
aload 8
iload 7
putfield android/support/design/widget/w/j Z
iload 6
iload 7
ior
istore 6
L3:
iload 4
iconst_1
iadd
istore 4
goto L0
L2:
aload 8
iconst_0
putfield android/support/design/widget/w/j Z
goto L3
L1:
iload 6
ireturn
.limit locals 10
.limit stack 5
.end method

.method public onStopNestedScroll(Landroid/view/View;)V
aload 0
getfield android/support/design/widget/CoordinatorLayout/C Landroid/support/v4/view/bw;
iconst_0
putfield android/support/v4/view/bw/a I
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getChildCount()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/design/widget/CoordinatorLayout/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 4
aload 4
getfield android/support/design/widget/w/j Z
ifeq L2
aload 4
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 5
aload 5
ifnull L3
aload 5
aload 1
invokevirtual android/support/design/widget/t/a(Landroid/view/View;)V
L3:
aload 4
iconst_0
putfield android/support/design/widget/w/j Z
aload 4
iconst_0
putfield android/support/design/widget/w/k Z
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
aconst_null
putfield android/support/design/widget/CoordinatorLayout/u Landroid/view/View;
aload 0
aconst_null
putfield android/support/design/widget/CoordinatorLayout/v Landroid/view/View;
return
.limit locals 6
.limit stack 2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 2
aload 0
getfield android/support/design/widget/CoordinatorLayout/t Landroid/view/View;
ifnonnull L0
aload 0
aload 1
iconst_1
invokespecial android/support/design/widget/CoordinatorLayout/a(Landroid/view/MotionEvent;I)Z
istore 4
iload 4
ifeq L1
L2:
aload 0
getfield android/support/design/widget/CoordinatorLayout/t Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 7
aload 7
ifnull L3
aload 7
aload 0
aload 0
getfield android/support/design/widget/CoordinatorLayout/t Landroid/view/View;
aload 1
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
istore 3
L4:
aload 0
getfield android/support/design/widget/CoordinatorLayout/t Landroid/view/View;
ifnonnull L5
iload 3
aload 0
aload 1
invokespecial android/view/ViewGroup/onTouchEvent(Landroid/view/MotionEvent;)Z
ior
istore 3
aconst_null
astore 1
L6:
aload 1
ifnull L7
aload 1
invokevirtual android/view/MotionEvent/recycle()V
L7:
iload 2
iconst_1
if_icmpeq L8
iload 2
iconst_3
if_icmpne L9
L8:
aload 0
invokespecial android/support/design/widget/CoordinatorLayout/a()V
L9:
iload 3
ireturn
L5:
iload 4
ifeq L10
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 5
lload 5
lload 5
iconst_3
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 1
aload 0
aload 1
invokespecial android/view/ViewGroup/onTouchEvent(Landroid/view/MotionEvent;)Z
pop
goto L6
L10:
aconst_null
astore 1
goto L6
L3:
iconst_0
istore 3
goto L4
L1:
iconst_0
istore 3
goto L4
L0:
iconst_0
istore 4
goto L2
.limit locals 8
.limit stack 8
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
aload 0
iload 1
invokespecial android/view/ViewGroup/requestDisallowInterceptTouchEvent(Z)V
iload 1
ifeq L0
aload 0
invokespecial android/support/design/widget/CoordinatorLayout/a()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V
aload 0
aload 1
putfield android/support/design/widget/CoordinatorLayout/B Landroid/view/ViewGroup$OnHierarchyChangeListener;
return
.limit locals 2
.limit stack 2
.end method

.method public setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/design/widget/CoordinatorLayout/A Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/invalidate()V
return
.limit locals 2
.limit stack 2
.end method

.method public setStatusBarBackgroundColor(I)V
aload 0
new android/graphics/drawable/ColorDrawable
dup
iload 1
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
invokevirtual android/support/design/widget/CoordinatorLayout/setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 4
.end method

.method public setStatusBarBackgroundResource(I)V
iload 1
ifeq L0
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getContext()Landroid/content/Context;
iload 1
invokestatic android/support/v4/c/h/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
astore 2
L1:
aload 0
aload 2
invokevirtual android/support/design/widget/CoordinatorLayout/setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V
return
L0:
aconst_null
astore 2
goto L1
.limit locals 3
.limit stack 2
.end method
