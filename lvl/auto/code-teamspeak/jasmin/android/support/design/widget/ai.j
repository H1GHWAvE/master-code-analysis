.bytecode 50.0
.class synchronized android/support/design/widget/ai
.super android/support/design/widget/ad

.field private 'h' Z

.method <init>(Landroid/view/View;Landroid/support/design/widget/as;)V
aload 0
aload 1
aload 2
invokespecial android/support/design/widget/ad/<init>(Landroid/view/View;Landroid/support/design/widget/as;)V
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Landroid/support/design/widget/ai;Z)Z
aload 0
iload 1
putfield android/support/design/widget/ai/h Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method final b()V
aload 0
getfield android/support/design/widget/ai/h Z
ifne L0
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
invokevirtual android/view/View/getVisibility()I
ifeq L1
L0:
return
L1:
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L2
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
invokevirtual android/view/View/isInEditMode()Z
ifeq L3
L2:
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
bipush 8
invokevirtual android/view/View/setVisibility(I)V
return
L3:
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
invokevirtual android/view/View/animate()Landroid/view/ViewPropertyAnimator;
fconst_0
invokevirtual android/view/ViewPropertyAnimator/scaleX(F)Landroid/view/ViewPropertyAnimator;
fconst_0
invokevirtual android/view/ViewPropertyAnimator/scaleY(F)Landroid/view/ViewPropertyAnimator;
fconst_0
invokevirtual android/view/ViewPropertyAnimator/alpha(F)Landroid/view/ViewPropertyAnimator;
ldc2_w 200L
invokevirtual android/view/ViewPropertyAnimator/setDuration(J)Landroid/view/ViewPropertyAnimator;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/view/ViewPropertyAnimator/setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;
new android/support/design/widget/aj
dup
aload 0
invokespecial android/support/design/widget/aj/<init>(Landroid/support/design/widget/ai;)V
invokevirtual android/view/ViewPropertyAnimator/setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;
pop
return
.limit locals 1
.limit stack 4
.end method

.method final c()V
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
invokevirtual android/view/View/getVisibility()I
ifeq L0
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L1
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
invokevirtual android/view/View/isInEditMode()Z
ifne L1
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
fconst_0
invokevirtual android/view/View/setAlpha(F)V
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
fconst_0
invokevirtual android/view/View/setScaleY(F)V
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
fconst_0
invokevirtual android/view/View/setScaleX(F)V
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
invokevirtual android/view/View/animate()Landroid/view/ViewPropertyAnimator;
fconst_1
invokevirtual android/view/ViewPropertyAnimator/scaleX(F)Landroid/view/ViewPropertyAnimator;
fconst_1
invokevirtual android/view/ViewPropertyAnimator/scaleY(F)Landroid/view/ViewPropertyAnimator;
fconst_1
invokevirtual android/view/ViewPropertyAnimator/alpha(F)Landroid/view/ViewPropertyAnimator;
ldc2_w 200L
invokevirtual android/view/ViewPropertyAnimator/setDuration(J)Landroid/view/ViewPropertyAnimator;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/view/ViewPropertyAnimator/setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;
new android/support/design/widget/ak
dup
aload 0
invokespecial android/support/design/widget/ak/<init>(Landroid/support/design/widget/ai;)V
invokevirtual android/view/ViewPropertyAnimator/setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;
pop
L0:
return
L1:
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
iconst_0
invokevirtual android/view/View/setVisibility(I)V
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
fconst_1
invokevirtual android/view/View/setAlpha(F)V
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
fconst_1
invokevirtual android/view/View/setScaleY(F)V
aload 0
getfield android/support/design/widget/ai/f Landroid/view/View;
fconst_1
invokevirtual android/view/View/setScaleX(F)V
return
.limit locals 1
.limit stack 4
.end method
