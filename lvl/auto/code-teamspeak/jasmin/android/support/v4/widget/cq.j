.bytecode 50.0
.class final synchronized android/support/v4/widget/cq
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/Context;)Landroid/view/View;
new android/widget/SearchView
dup
aload 0
invokespecial android/widget/SearchView/<init>(Landroid/content/Context;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Landroid/view/View;)Ljava/lang/CharSequence;
aload 0
checkcast android/widget/SearchView
invokevirtual android/widget/SearchView/getQuery()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/support/v4/widget/ct;)Ljava/lang/Object;
new android/support/v4/widget/cs
dup
aload 0
invokespecial android/support/v4/widget/cs/<init>(Landroid/support/v4/widget/ct;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Landroid/support/v4/widget/cu;)Ljava/lang/Object;
new android/support/v4/widget/cr
dup
aload 0
invokespecial android/support/v4/widget/cr/<init>(Landroid/support/v4/widget/cu;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Landroid/view/View;I)V
aload 0
checkcast android/widget/SearchView
iload 1
invokevirtual android/widget/SearchView/setMaxWidth(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/view/View;Landroid/content/ComponentName;)V
aload 0
checkcast android/widget/SearchView
astore 0
aload 0
aload 0
invokevirtual android/widget/SearchView/getContext()Landroid/content/Context;
ldc "search"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/SearchManager
aload 1
invokevirtual android/app/SearchManager/getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;
invokevirtual android/widget/SearchView/setSearchableInfo(Landroid/app/SearchableInfo;)V
return
.limit locals 2
.limit stack 3
.end method

.method private static a(Landroid/view/View;Ljava/lang/CharSequence;)V
aload 0
checkcast android/widget/SearchView
aload 1
invokevirtual android/widget/SearchView/setQueryHint(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/view/View;Ljava/lang/CharSequence;Z)V
aload 0
checkcast android/widget/SearchView
aload 1
iload 2
invokevirtual android/widget/SearchView/setQuery(Ljava/lang/CharSequence;Z)V
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Landroid/view/View;Z)V
aload 0
checkcast android/widget/SearchView
iload 1
invokevirtual android/widget/SearchView/setIconified(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/widget/SearchView
aload 1
checkcast android/widget/SearchView$OnQueryTextListener
invokevirtual android/widget/SearchView/setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/view/View;Z)V
aload 0
checkcast android/widget/SearchView
iload 1
invokevirtual android/widget/SearchView/setSubmitButtonEnabled(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/widget/SearchView
aload 1
checkcast android/widget/SearchView$OnCloseListener
invokevirtual android/widget/SearchView/setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/view/View;)Z
aload 0
checkcast android/widget/SearchView
invokevirtual android/widget/SearchView/isIconified()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Landroid/view/View;Z)V
aload 0
checkcast android/widget/SearchView
iload 1
invokevirtual android/widget/SearchView/setQueryRefinementEnabled(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Landroid/view/View;)Z
aload 0
checkcast android/widget/SearchView
invokevirtual android/widget/SearchView/isSubmitButtonEnabled()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Landroid/view/View;)Z
aload 0
checkcast android/widget/SearchView
invokevirtual android/widget/SearchView/isQueryRefinementEnabled()Z
ireturn
.limit locals 1
.limit stack 1
.end method
