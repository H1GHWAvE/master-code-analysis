.bytecode 50.0
.class final synchronized android/support/v4/widget/m
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "CompoundButtonCompatDonut"

.field private static 'b' Ljava/lang/reflect/Field;

.field private static 'c' Z

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;
.catch java/lang/NoSuchFieldException from L0 to L1 using L2
.catch java/lang/IllegalAccessException from L3 to L4 using L5
getstatic android/support/v4/widget/m/c Z
ifne L6
L0:
ldc android/widget/CompoundButton
ldc "mButtonDrawable"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 1
aload 1
putstatic android/support/v4/widget/m/b Ljava/lang/reflect/Field;
aload 1
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
L1:
iconst_1
putstatic android/support/v4/widget/m/c Z
L6:
getstatic android/support/v4/widget/m/b Ljava/lang/reflect/Field;
ifnull L7
L3:
getstatic android/support/v4/widget/m/b Ljava/lang/reflect/Field;
aload 0
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/graphics/drawable/Drawable
astore 0
L4:
aload 0
areturn
L2:
astore 1
ldc "CompoundButtonCompatDonut"
ldc "Failed to retrieve mButtonDrawable field"
aload 1
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L5:
astore 0
ldc "CompoundButtonCompatDonut"
ldc "Failed to get button drawable via reflection"
aload 0
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
putstatic android/support/v4/widget/m/b Ljava/lang/reflect/Field;
L7:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V
aload 0
instanceof android/support/v4/widget/ef
ifeq L0
aload 0
checkcast android/support/v4/widget/ef
aload 1
invokeinterface android/support/v4/widget/ef/setSupportButtonTintList(Landroid/content/res/ColorStateList;)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/widget/CompoundButton;Landroid/graphics/PorterDuff$Mode;)V
aload 0
instanceof android/support/v4/widget/ef
ifeq L0
aload 0
checkcast android/support/v4/widget/ef
aload 1
invokeinterface android/support/v4/widget/ef/setSupportButtonTintMode(Landroid/graphics/PorterDuff$Mode;)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/widget/CompoundButton;)Landroid/content/res/ColorStateList;
aload 0
instanceof android/support/v4/widget/ef
ifeq L0
aload 0
checkcast android/support/v4/widget/ef
invokeinterface android/support/v4/widget/ef/getSupportButtonTintList()Landroid/content/res/ColorStateList; 0
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Landroid/widget/CompoundButton;)Landroid/graphics/PorterDuff$Mode;
aload 0
instanceof android/support/v4/widget/ef
ifeq L0
aload 0
checkcast android/support/v4/widget/ef
invokeinterface android/support/v4/widget/ef/getSupportButtonTintMode()Landroid/graphics/PorterDuff$Mode; 0
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method
