.bytecode 50.0
.class final synchronized android/support/v4/widget/c
.super java/lang/Object

.field 'a' I

.field 'b' I

.field 'c' F

.field 'd' F

.field 'e' J

.field 'f' J

.field 'g' I

.field 'h' I

.field 'i' J

.field 'j' F

.field 'k' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc2_w -9223372036854775808L
putfield android/support/v4/widget/c/e J
aload 0
ldc2_w -1L
putfield android/support/v4/widget/c/i J
aload 0
lconst_0
putfield android/support/v4/widget/c/f J
aload 0
iconst_0
putfield android/support/v4/widget/c/g I
aload 0
iconst_0
putfield android/support/v4/widget/c/h I
return
.limit locals 1
.limit stack 3
.end method

.method private static a(F)F
ldc_w -4.0F
fload 0
fmul
fload 0
fmul
ldc_w 4.0F
fload 0
fmul
fadd
freturn
.limit locals 1
.limit stack 3
.end method

.method private a()V
aload 0
invokestatic android/view/animation/AnimationUtils/currentAnimationTimeMillis()J
putfield android/support/v4/widget/c/e J
aload 0
ldc2_w -1L
putfield android/support/v4/widget/c/i J
aload 0
aload 0
getfield android/support/v4/widget/c/e J
putfield android/support/v4/widget/c/f J
aload 0
ldc_w 0.5F
putfield android/support/v4/widget/c/j F
aload 0
iconst_0
putfield android/support/v4/widget/c/g I
aload 0
iconst_0
putfield android/support/v4/widget/c/h I
return
.limit locals 1
.limit stack 3
.end method

.method private a(FF)V
aload 0
fload 1
putfield android/support/v4/widget/c/c F
aload 0
fload 2
putfield android/support/v4/widget/c/d F
return
.limit locals 3
.limit stack 2
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/v4/widget/c/a I
return
.limit locals 2
.limit stack 2
.end method

.method private b()V
invokestatic android/view/animation/AnimationUtils/currentAnimationTimeMillis()J
lstore 1
aload 0
lload 1
aload 0
getfield android/support/v4/widget/c/e J
lsub
l2i
aload 0
getfield android/support/v4/widget/c/b I
invokestatic android/support/v4/widget/a/a(II)I
putfield android/support/v4/widget/c/k I
aload 0
aload 0
lload 1
invokevirtual android/support/v4/widget/c/a(J)F
putfield android/support/v4/widget/c/j F
aload 0
lload 1
putfield android/support/v4/widget/c/i J
return
.limit locals 3
.limit stack 5
.end method

.method private b(I)V
aload 0
iload 1
putfield android/support/v4/widget/c/b I
return
.limit locals 2
.limit stack 2
.end method

.method private c()Z
aload 0
getfield android/support/v4/widget/c/i J
lconst_0
lcmp
ifle L0
invokestatic android/view/animation/AnimationUtils/currentAnimationTimeMillis()J
aload 0
getfield android/support/v4/widget/c/i J
aload 0
getfield android/support/v4/widget/c/k I
i2l
ladd
lcmp
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 6
.end method

.method private d()V
aload 0
getfield android/support/v4/widget/c/f J
lconst_0
lcmp
ifne L0
new java/lang/RuntimeException
dup
ldc "Cannot compute scroll delta before calling start()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
invokestatic android/view/animation/AnimationUtils/currentAnimationTimeMillis()J
lstore 2
aload 0
lload 2
invokevirtual android/support/v4/widget/c/a(J)F
fstore 1
fload 1
ldc_w 4.0F
fmul
ldc_w -4.0F
fload 1
fmul
fload 1
fmul
fadd
fstore 1
lload 2
aload 0
getfield android/support/v4/widget/c/f J
lsub
lstore 4
aload 0
lload 2
putfield android/support/v4/widget/c/f J
aload 0
lload 4
l2f
fload 1
fmul
aload 0
getfield android/support/v4/widget/c/c F
fmul
f2i
putfield android/support/v4/widget/c/g I
aload 0
lload 4
l2f
fload 1
fmul
aload 0
getfield android/support/v4/widget/c/d F
fmul
f2i
putfield android/support/v4/widget/c/h I
return
.limit locals 6
.limit stack 4
.end method

.method private e()I
aload 0
getfield android/support/v4/widget/c/c F
aload 0
getfield android/support/v4/widget/c/c F
invokestatic java/lang/Math/abs(F)F
fdiv
f2i
ireturn
.limit locals 1
.limit stack 2
.end method

.method private f()I
aload 0
getfield android/support/v4/widget/c/d F
aload 0
getfield android/support/v4/widget/c/d F
invokestatic java/lang/Math/abs(F)F
fdiv
f2i
ireturn
.limit locals 1
.limit stack 2
.end method

.method private g()I
aload 0
getfield android/support/v4/widget/c/g I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()I
aload 0
getfield android/support/v4/widget/c/h I
ireturn
.limit locals 1
.limit stack 1
.end method

.method final a(J)F
lload 1
aload 0
getfield android/support/v4/widget/c/e J
lcmp
ifge L0
fconst_0
freturn
L0:
aload 0
getfield android/support/v4/widget/c/i J
lconst_0
lcmp
iflt L1
lload 1
aload 0
getfield android/support/v4/widget/c/i J
lcmp
ifge L2
L1:
lload 1
aload 0
getfield android/support/v4/widget/c/e J
lsub
l2f
aload 0
getfield android/support/v4/widget/c/a I
i2f
fdiv
invokestatic android/support/v4/widget/a/a(F)F
ldc_w 0.5F
fmul
freturn
L2:
aload 0
getfield android/support/v4/widget/c/i J
lstore 5
aload 0
getfield android/support/v4/widget/c/j F
fstore 3
aload 0
getfield android/support/v4/widget/c/j F
fstore 4
lload 1
lload 5
lsub
l2f
aload 0
getfield android/support/v4/widget/c/k I
i2f
fdiv
invokestatic android/support/v4/widget/a/a(F)F
fload 4
fmul
fconst_1
fload 3
fsub
fadd
freturn
.limit locals 7
.limit stack 4
.end method
