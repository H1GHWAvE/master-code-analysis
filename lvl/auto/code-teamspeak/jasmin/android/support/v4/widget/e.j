.bytecode 50.0
.class final synchronized android/support/v4/widget/e
.super android/widget/ImageView

.field private static final 'b' I = 503316480


.field private static final 'c' I = 1023410176


.field private static final 'd' F = 0.0F


.field private static final 'e' F = 1.75F


.field private static final 'f' F = 3.5F


.field private static final 'g' I = 4


.field 'a' Landroid/view/animation/Animation$AnimationListener;

.field private 'h' I

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
aload 0
invokevirtual android/support/v4/widget/e/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fstore 2
ldc_w 20.0F
fload 2
fmul
fconst_2
fmul
f2i
istore 3
ldc_w 1.75F
fload 2
fmul
f2i
istore 4
fconst_0
fload 2
fmul
f2i
istore 5
aload 0
ldc_w 3.5F
fload 2
fmul
f2i
putfield android/support/v4/widget/e/h I
invokestatic android/support/v4/widget/e/a()Z
ifeq L0
new android/graphics/drawable/ShapeDrawable
dup
new android/graphics/drawable/shapes/OvalShape
dup
invokespecial android/graphics/drawable/shapes/OvalShape/<init>()V
invokespecial android/graphics/drawable/ShapeDrawable/<init>(Landroid/graphics/drawable/shapes/Shape;)V
astore 1
aload 0
fload 2
ldc_w 4.0F
fmul
invokestatic android/support/v4/view/cx/f(Landroid/view/View;F)V
L1:
aload 1
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
ldc_w -328966
invokevirtual android/graphics/Paint/setColor(I)V
aload 0
aload 1
invokevirtual android/support/v4/widget/e/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
return
L0:
new android/graphics/drawable/ShapeDrawable
dup
new android/support/v4/widget/f
dup
aload 0
aload 0
getfield android/support/v4/widget/e/h I
iload 3
invokespecial android/support/v4/widget/f/<init>(Landroid/support/v4/widget/e;II)V
invokespecial android/graphics/drawable/ShapeDrawable/<init>(Landroid/graphics/drawable/shapes/Shape;)V
astore 1
aload 0
iconst_1
aload 1
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
invokestatic android/support/v4/view/cx/a(Landroid/view/View;ILandroid/graphics/Paint;)V
aload 1
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
aload 0
getfield android/support/v4/widget/e/h I
i2f
iload 5
i2f
iload 4
i2f
ldc_w 503316480
invokevirtual android/graphics/Paint/setShadowLayer(FFFI)V
aload 0
getfield android/support/v4/widget/e/h I
istore 3
aload 0
iload 3
iload 3
iload 3
iload 3
invokevirtual android/support/v4/widget/e/setPadding(IIII)V
goto L1
.limit locals 6
.limit stack 7
.end method

.method static synthetic a(Landroid/support/v4/widget/e;)I
aload 0
getfield android/support/v4/widget/e/h I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/support/v4/widget/e;I)I
aload 0
iload 1
putfield android/support/v4/widget/e/h I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private a(I)V
aload 0
aload 0
invokevirtual android/support/v4/widget/e/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getColor(I)I
invokevirtual android/support/v4/widget/e/setBackgroundColor(I)V
return
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/view/animation/Animation$AnimationListener;)V
aload 0
aload 1
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
return
.limit locals 2
.limit stack 2
.end method

.method private static a()Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 0
.limit stack 2
.end method

.method public final onAnimationEnd()V
aload 0
invokespecial android/widget/ImageView/onAnimationEnd()V
aload 0
getfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
ifnull L0
aload 0
getfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
aload 0
invokevirtual android/support/v4/widget/e/getAnimation()Landroid/view/animation/Animation;
invokeinterface android/view/animation/Animation$AnimationListener/onAnimationEnd(Landroid/view/animation/Animation;)V 1
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final onAnimationStart()V
aload 0
invokespecial android/widget/ImageView/onAnimationStart()V
aload 0
getfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
ifnull L0
aload 0
getfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
aload 0
invokevirtual android/support/v4/widget/e/getAnimation()Landroid/view/animation/Animation;
invokeinterface android/view/animation/Animation$AnimationListener/onAnimationStart(Landroid/view/animation/Animation;)V 1
L0:
return
.limit locals 1
.limit stack 2
.end method

.method protected final onMeasure(II)V
aload 0
iload 1
iload 2
invokespecial android/widget/ImageView/onMeasure(II)V
invokestatic android/support/v4/widget/e/a()Z
ifne L0
aload 0
aload 0
invokevirtual android/support/v4/widget/e/getMeasuredWidth()I
aload 0
getfield android/support/v4/widget/e/h I
iconst_2
imul
iadd
aload 0
invokevirtual android/support/v4/widget/e/getMeasuredHeight()I
aload 0
getfield android/support/v4/widget/e/h I
iconst_2
imul
iadd
invokevirtual android/support/v4/widget/e/setMeasuredDimension(II)V
L0:
return
.limit locals 3
.limit stack 5
.end method

.method public final setBackgroundColor(I)V
aload 0
invokevirtual android/support/v4/widget/e/getBackground()Landroid/graphics/drawable/Drawable;
instanceof android/graphics/drawable/ShapeDrawable
ifeq L0
aload 0
invokevirtual android/support/v4/widget/e/getBackground()Landroid/graphics/drawable/Drawable;
checkcast android/graphics/drawable/ShapeDrawable
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
iload 1
invokevirtual android/graphics/Paint/setColor(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method
