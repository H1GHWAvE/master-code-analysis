.bytecode 50.0
.class public synchronized abstract android/support/v4/widget/as
.super android/support/v4/view/a

.field public static final 'a' I = -2147483648


.field public static final 'c' I = -1


.field private static final 'd' Ljava/lang/String;

.field private final 'e' Landroid/graphics/Rect;

.field private final 'f' Landroid/graphics/Rect;

.field private final 'g' Landroid/graphics/Rect;

.field private final 'h' [I

.field private final 'i' Landroid/view/accessibility/AccessibilityManager;

.field private final 'j' Landroid/view/View;

.field private 'k' Landroid/support/v4/widget/au;

.field private 'l' I

.field private 'm' I

.method static <clinit>()V
ldc android/view/View
invokevirtual java/lang/Class/getName()Ljava/lang/String;
putstatic android/support/v4/widget/as/d Ljava/lang/String;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>(Landroid/view/View;)V
aload 0
invokespecial android/support/v4/view/a/<init>()V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/as/e Landroid/graphics/Rect;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/as/f Landroid/graphics/Rect;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/as/g Landroid/graphics/Rect;
aload 0
iconst_2
newarray int
putfield android/support/v4/widget/as/h [I
aload 0
ldc_w -2147483648
putfield android/support/v4/widget/as/l I
aload 0
ldc_w -2147483648
putfield android/support/v4/widget/as/m I
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "View may not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield android/support/v4/widget/as/j Landroid/view/View;
aload 0
aload 1
invokevirtual android/view/View/getContext()Landroid/content/Context;
ldc "accessibility"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/accessibility/AccessibilityManager
putfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
return
.limit locals 2
.limit stack 3
.end method

.method static synthetic a(Landroid/support/v4/widget/as;I)Landroid/support/v4/view/a/q;
iload 1
tableswitch -1
L0
default : L1
L1:
invokestatic android/support/v4/view/a/q/a()Landroid/support/v4/view/a/q;
astore 3
aload 3
iconst_1
invokevirtual android/support/v4/view/a/q/h(Z)V
aload 3
getstatic android/support/v4/widget/as/d Ljava/lang/String;
invokevirtual android/support/v4/view/a/q/b(Ljava/lang/CharSequence;)V
aload 3
invokevirtual android/support/v4/view/a/q/m()Ljava/lang/CharSequence;
ifnonnull L2
aload 3
invokevirtual android/support/v4/view/a/q/n()Ljava/lang/CharSequence;
ifnonnull L2
new java/lang/RuntimeException
dup
ldc "Callbacks must add text or a content description in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokestatic android/support/v4/view/a/q/a(Landroid/view/View;)Landroid/support/v4/view/a/q;
astore 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
aload 3
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
invokevirtual java/util/LinkedList/iterator()Ljava/util/Iterator;
astore 4
L3:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
astore 5
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
astore 6
aload 5
invokevirtual java/lang/Integer/intValue()I
istore 1
getstatic android/support/v4/view/a/q/a Landroid/support/v4/view/a/w;
aload 3
getfield android/support/v4/view/a/q/b Ljava/lang/Object;
aload 6
iload 1
invokeinterface android/support/v4/view/a/w/e(Ljava/lang/Object;Landroid/view/View;I)V 3
goto L3
L4:
aload 3
areturn
L2:
aload 3
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokevirtual android/support/v4/view/a/q/a(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/isEmpty()Z
ifeq L5
new java/lang/RuntimeException
dup
ldc "Callbacks must set parent bounds in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L5:
aload 3
invokevirtual android/support/v4/view/a/q/b()I
istore 2
iload 2
bipush 64
iand
ifeq L6
new java/lang/RuntimeException
dup
ldc "Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L6:
iload 2
sipush 128
iand
ifeq L7
new java/lang/RuntimeException
dup
ldc "Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L7:
aload 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/support/v4/view/a/q/a(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
astore 4
getstatic android/support/v4/view/a/q/a Landroid/support/v4/view/a/w;
aload 3
getfield android/support/v4/view/a/q/b Ljava/lang/Object;
aload 4
iload 1
invokeinterface android/support/v4/view/a/w/d(Ljava/lang/Object;Landroid/view/View;I)V 3
aload 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/support/v4/view/a/q/d(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/as/l I
iload 1
if_icmpne L8
aload 3
iconst_1
invokevirtual android/support/v4/view/a/q/d(Z)V
aload 3
sipush 128
invokevirtual android/support/v4/view/a/q/a(I)V
L9:
aload 0
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokespecial android/support/v4/widget/as/a(Landroid/graphics/Rect;)Z
ifeq L10
aload 3
iconst_1
invokevirtual android/support/v4/view/a/q/c(Z)V
aload 3
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokevirtual android/support/v4/view/a/q/b(Landroid/graphics/Rect;)V
L10:
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
aload 0
getfield android/support/v4/widget/as/h [I
invokevirtual android/view/View/getLocationOnScreen([I)V
aload 0
getfield android/support/v4/widget/as/h [I
iconst_0
iaload
istore 1
aload 0
getfield android/support/v4/widget/as/h [I
iconst_1
iaload
istore 2
aload 0
getfield android/support/v4/widget/as/e Landroid/graphics/Rect;
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/widget/as/e Landroid/graphics/Rect;
iload 1
iload 2
invokevirtual android/graphics/Rect/offset(II)V
aload 3
aload 0
getfield android/support/v4/widget/as/e Landroid/graphics/Rect;
invokevirtual android/support/v4/view/a/q/d(Landroid/graphics/Rect;)V
aload 3
areturn
L8:
aload 3
iconst_0
invokevirtual android/support/v4/view/a/q/d(Z)V
aload 3
bipush 64
invokevirtual android/support/v4/view/a/q/a(I)V
goto L9
.limit locals 7
.limit stack 4
.end method

.method private a(I)V
aload 0
getfield android/support/v4/widget/as/m I
iload 1
if_icmpne L0
return
L0:
aload 0
getfield android/support/v4/widget/as/m I
istore 2
aload 0
iload 1
putfield android/support/v4/widget/as/m I
aload 0
iload 1
sipush 128
invokespecial android/support/v4/widget/as/a(II)Z
pop
aload 0
iload 2
sipush 256
invokespecial android/support/v4/widget/as/a(II)Z
pop
return
.limit locals 3
.limit stack 3
.end method

.method private a(II)Z
iload 1
ldc_w -2147483648
if_icmpeq L0
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokevirtual android/view/accessibility/AccessibilityManager/isEnabled()Z
ifne L1
L0:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 4
aload 4
ifnull L0
iload 1
tableswitch -1
L2
default : L3
L3:
iload 2
invokestatic android/view/accessibility/AccessibilityEvent/obtain(I)Landroid/view/accessibility/AccessibilityEvent;
astore 3
aload 3
iconst_1
invokevirtual android/view/accessibility/AccessibilityEvent/setEnabled(Z)V
aload 3
getstatic android/support/v4/widget/as/d Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
aload 3
invokevirtual android/view/accessibility/AccessibilityEvent/getText()Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifeq L4
aload 3
invokevirtual android/view/accessibility/AccessibilityEvent/getContentDescription()Ljava/lang/CharSequence;
ifnonnull L4
new java/lang/RuntimeException
dup
ldc "Callbacks must add text or a content description in populateEventForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L2:
iload 2
invokestatic android/view/accessibility/AccessibilityEvent/obtain(I)Landroid/view/accessibility/AccessibilityEvent;
astore 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
aload 3
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
L5:
aload 4
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
aload 3
invokestatic android/support/v4/view/fb/a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
ireturn
L4:
aload 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setPackageName(Ljava/lang/CharSequence;)V
aload 3
invokestatic android/support/v4/view/a/a/a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/bd;
astore 5
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
astore 6
getstatic android/support/v4/view/a/bd/a Landroid/support/v4/view/a/bg;
aload 5
getfield android/support/v4/view/a/bd/b Ljava/lang/Object;
aload 6
iload 1
invokeinterface android/support/v4/view/a/bg/a(Ljava/lang/Object;Landroid/view/View;I)V 3
goto L5
.limit locals 7
.limit stack 4
.end method

.method private a(IILandroid/os/Bundle;)Z
iload 1
tableswitch -1
L0
default : L1
L1:
iload 2
lookupswitch
64 : L2
128 : L2
default : L3
L3:
aload 0
invokevirtual android/support/v4/widget/as/e()Z
ireturn
L0:
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
iload 2
aload 3
invokestatic android/support/v4/view/cx/a(Landroid/view/View;ILandroid/os/Bundle;)Z
ireturn
L2:
iload 2
lookupswitch
64 : L4
128 : L5
default : L6
L6:
iconst_0
ireturn
L4:
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokevirtual android/view/accessibility/AccessibilityManager/isEnabled()Z
ifeq L7
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokestatic android/support/v4/view/a/h/a(Landroid/view/accessibility/AccessibilityManager;)Z
ifne L8
L7:
iconst_0
ireturn
L8:
aload 0
iload 1
invokespecial android/support/v4/widget/as/e(I)Z
ifne L9
aload 0
getfield android/support/v4/widget/as/l I
ldc_w -2147483648
if_icmpeq L10
aload 0
aload 0
getfield android/support/v4/widget/as/l I
ldc_w 65536
invokespecial android/support/v4/widget/as/a(II)Z
pop
L10:
aload 0
iload 1
putfield android/support/v4/widget/as/l I
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/invalidate()V
aload 0
iload 1
ldc_w 32768
invokespecial android/support/v4/widget/as/a(II)Z
pop
iconst_1
ireturn
L9:
iconst_0
ireturn
L5:
aload 0
iload 1
invokespecial android/support/v4/widget/as/e(I)Z
ifeq L11
aload 0
ldc_w -2147483648
putfield android/support/v4/widget/as/l I
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/invalidate()V
aload 0
iload 1
ldc_w 65536
invokespecial android/support/v4/widget/as/a(II)Z
pop
iconst_1
ireturn
L11:
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method private a(ILandroid/os/Bundle;)Z
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
iload 1
aload 2
invokestatic android/support/v4/view/cx/a(Landroid/view/View;ILandroid/os/Bundle;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/graphics/Rect;)Z
aload 1
ifnull L0
aload 1
invokevirtual android/graphics/Rect/isEmpty()Z
ifeq L1
L0:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/getWindowVisibility()I
ifeq L2
iconst_0
ireturn
L2:
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 2
L3:
aload 2
instanceof android/view/View
ifeq L4
aload 2
checkcast android/view/View
astore 2
aload 2
invokestatic android/support/v4/view/cx/d(Landroid/view/View;)F
fconst_0
fcmpg
ifle L5
aload 2
invokevirtual android/view/View/getVisibility()I
ifeq L6
L5:
iconst_0
ireturn
L6:
aload 2
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 2
goto L3
L4:
aload 2
ifnonnull L7
iconst_0
ireturn
L7:
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
aload 0
getfield android/support/v4/widget/as/g Landroid/graphics/Rect;
invokevirtual android/view/View/getLocalVisibleRect(Landroid/graphics/Rect;)Z
ifne L8
iconst_0
ireturn
L8:
aload 1
aload 0
getfield android/support/v4/widget/as/g Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/intersect(Landroid/graphics/Rect;)Z
ireturn
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v4/widget/as;IILandroid/os/Bundle;)Z
iload 1
tableswitch -1
L0
default : L1
L1:
iload 2
lookupswitch
64 : L2
128 : L2
default : L3
L3:
aload 0
invokevirtual android/support/v4/widget/as/e()Z
ireturn
L0:
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
iload 2
aload 3
invokestatic android/support/v4/view/cx/a(Landroid/view/View;ILandroid/os/Bundle;)Z
ireturn
L2:
iload 2
lookupswitch
64 : L4
128 : L5
default : L6
L6:
iconst_0
ireturn
L4:
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokevirtual android/view/accessibility/AccessibilityManager/isEnabled()Z
ifeq L7
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokestatic android/support/v4/view/a/h/a(Landroid/view/accessibility/AccessibilityManager;)Z
ifne L8
L7:
iconst_0
ireturn
L8:
aload 0
iload 1
invokespecial android/support/v4/widget/as/e(I)Z
ifne L9
aload 0
getfield android/support/v4/widget/as/l I
ldc_w -2147483648
if_icmpeq L10
aload 0
aload 0
getfield android/support/v4/widget/as/l I
ldc_w 65536
invokespecial android/support/v4/widget/as/a(II)Z
pop
L10:
aload 0
iload 1
putfield android/support/v4/widget/as/l I
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/invalidate()V
aload 0
iload 1
ldc_w 32768
invokespecial android/support/v4/widget/as/a(II)Z
pop
iconst_1
ireturn
L9:
iconst_0
ireturn
L5:
aload 0
iload 1
invokespecial android/support/v4/widget/as/e(I)Z
ifeq L11
aload 0
ldc_w -2147483648
putfield android/support/v4/widget/as/l I
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/invalidate()V
aload 0
iload 1
ldc_w 65536
invokespecial android/support/v4/widget/as/a(II)Z
pop
iconst_1
ireturn
L11:
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method private a(Landroid/view/MotionEvent;)Z
iconst_1
istore 3
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokevirtual android/view/accessibility/AccessibilityManager/isEnabled()Z
ifeq L0
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokestatic android/support/v4/view/a/h/a(Landroid/view/accessibility/AccessibilityManager;)Z
ifne L1
L0:
iconst_0
istore 3
L2:
iload 3
ireturn
L1:
aload 1
invokevirtual android/view/MotionEvent/getAction()I
tableswitch 7
L3
L4
L3
L5
default : L4
L4:
iconst_0
ireturn
L3:
aload 1
invokevirtual android/view/MotionEvent/getX()F
pop
aload 1
invokevirtual android/view/MotionEvent/getY()F
pop
aload 0
invokevirtual android/support/v4/widget/as/a()I
istore 2
aload 0
iload 2
invokespecial android/support/v4/widget/as/a(I)V
iload 2
ldc_w -2147483648
if_icmpne L2
iconst_0
ireturn
L5:
aload 0
getfield android/support/v4/widget/as/l I
ldc_w -2147483648
if_icmpeq L6
aload 0
ldc_w -2147483648
invokespecial android/support/v4/widget/as/a(I)V
iconst_1
ireturn
L6:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method private b(I)Landroid/view/accessibility/AccessibilityEvent;
iload 1
invokestatic android/view/accessibility/AccessibilityEvent/obtain(I)Landroid/view/accessibility/AccessibilityEvent;
astore 2
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
aload 2
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method private b(II)Landroid/view/accessibility/AccessibilityEvent;
iload 1
tableswitch -1
L0
default : L1
L1:
iload 2
invokestatic android/view/accessibility/AccessibilityEvent/obtain(I)Landroid/view/accessibility/AccessibilityEvent;
astore 3
aload 3
iconst_1
invokevirtual android/view/accessibility/AccessibilityEvent/setEnabled(Z)V
aload 3
getstatic android/support/v4/widget/as/d Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
aload 3
invokevirtual android/view/accessibility/AccessibilityEvent/getText()Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifeq L2
aload 3
invokevirtual android/view/accessibility/AccessibilityEvent/getContentDescription()Ljava/lang/CharSequence;
ifnonnull L2
new java/lang/RuntimeException
dup
ldc "Callbacks must add text or a content description in populateEventForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 2
invokestatic android/view/accessibility/AccessibilityEvent/obtain(I)Landroid/view/accessibility/AccessibilityEvent;
astore 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
aload 3
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 3
areturn
L2:
aload 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setPackageName(Ljava/lang/CharSequence;)V
aload 3
invokestatic android/support/v4/view/a/a/a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/bd;
astore 4
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
astore 5
getstatic android/support/v4/view/a/bd/a Landroid/support/v4/view/a/bg;
aload 4
getfield android/support/v4/view/a/bd/b Ljava/lang/Object;
aload 5
iload 1
invokeinterface android/support/v4/view/a/bg/a(Ljava/lang/Object;Landroid/view/View;I)V 3
aload 3
areturn
.limit locals 6
.limit stack 4
.end method

.method private c(I)Landroid/support/v4/view/a/q;
iload 1
tableswitch -1
L0
default : L1
L1:
invokestatic android/support/v4/view/a/q/a()Landroid/support/v4/view/a/q;
astore 3
aload 3
iconst_1
invokevirtual android/support/v4/view/a/q/h(Z)V
aload 3
getstatic android/support/v4/widget/as/d Ljava/lang/String;
invokevirtual android/support/v4/view/a/q/b(Ljava/lang/CharSequence;)V
aload 3
invokevirtual android/support/v4/view/a/q/m()Ljava/lang/CharSequence;
ifnonnull L2
aload 3
invokevirtual android/support/v4/view/a/q/n()Ljava/lang/CharSequence;
ifnonnull L2
new java/lang/RuntimeException
dup
ldc "Callbacks must add text or a content description in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokestatic android/support/v4/view/a/q/a(Landroid/view/View;)Landroid/support/v4/view/a/q;
astore 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
aload 3
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
invokevirtual java/util/LinkedList/iterator()Ljava/util/Iterator;
astore 4
L3:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
astore 5
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
astore 6
aload 5
invokevirtual java/lang/Integer/intValue()I
istore 1
getstatic android/support/v4/view/a/q/a Landroid/support/v4/view/a/w;
aload 3
getfield android/support/v4/view/a/q/b Ljava/lang/Object;
aload 6
iload 1
invokeinterface android/support/v4/view/a/w/e(Ljava/lang/Object;Landroid/view/View;I)V 3
goto L3
L4:
aload 3
areturn
L2:
aload 3
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokevirtual android/support/v4/view/a/q/a(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/isEmpty()Z
ifeq L5
new java/lang/RuntimeException
dup
ldc "Callbacks must set parent bounds in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L5:
aload 3
invokevirtual android/support/v4/view/a/q/b()I
istore 2
iload 2
bipush 64
iand
ifeq L6
new java/lang/RuntimeException
dup
ldc "Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L6:
iload 2
sipush 128
iand
ifeq L7
new java/lang/RuntimeException
dup
ldc "Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L7:
aload 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/support/v4/view/a/q/a(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
astore 4
getstatic android/support/v4/view/a/q/a Landroid/support/v4/view/a/w;
aload 3
getfield android/support/v4/view/a/q/b Ljava/lang/Object;
aload 4
iload 1
invokeinterface android/support/v4/view/a/w/d(Ljava/lang/Object;Landroid/view/View;I)V 3
aload 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/support/v4/view/a/q/d(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/as/l I
iload 1
if_icmpne L8
aload 3
iconst_1
invokevirtual android/support/v4/view/a/q/d(Z)V
aload 3
sipush 128
invokevirtual android/support/v4/view/a/q/a(I)V
L9:
aload 0
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokespecial android/support/v4/widget/as/a(Landroid/graphics/Rect;)Z
ifeq L10
aload 3
iconst_1
invokevirtual android/support/v4/view/a/q/c(Z)V
aload 3
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokevirtual android/support/v4/view/a/q/b(Landroid/graphics/Rect;)V
L10:
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
aload 0
getfield android/support/v4/widget/as/h [I
invokevirtual android/view/View/getLocationOnScreen([I)V
aload 0
getfield android/support/v4/widget/as/h [I
iconst_0
iaload
istore 1
aload 0
getfield android/support/v4/widget/as/h [I
iconst_1
iaload
istore 2
aload 0
getfield android/support/v4/widget/as/e Landroid/graphics/Rect;
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/widget/as/e Landroid/graphics/Rect;
iload 1
iload 2
invokevirtual android/graphics/Rect/offset(II)V
aload 3
aload 0
getfield android/support/v4/widget/as/e Landroid/graphics/Rect;
invokevirtual android/support/v4/view/a/q/d(Landroid/graphics/Rect;)V
aload 3
areturn
L8:
aload 3
iconst_0
invokevirtual android/support/v4/view/a/q/d(Z)V
aload 3
bipush 64
invokevirtual android/support/v4/view/a/q/a(I)V
goto L9
.limit locals 7
.limit stack 4
.end method

.method private c(II)Landroid/view/accessibility/AccessibilityEvent;
iload 2
invokestatic android/view/accessibility/AccessibilityEvent/obtain(I)Landroid/view/accessibility/AccessibilityEvent;
astore 3
aload 3
iconst_1
invokevirtual android/view/accessibility/AccessibilityEvent/setEnabled(Z)V
aload 3
getstatic android/support/v4/widget/as/d Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
aload 3
invokevirtual android/view/accessibility/AccessibilityEvent/getText()Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifeq L0
aload 3
invokevirtual android/view/accessibility/AccessibilityEvent/getContentDescription()Ljava/lang/CharSequence;
ifnonnull L0
new java/lang/RuntimeException
dup
ldc "Callbacks must add text or a content description in populateEventForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setPackageName(Ljava/lang/CharSequence;)V
aload 3
invokestatic android/support/v4/view/a/a/a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/bd;
astore 4
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
astore 5
getstatic android/support/v4/view/a/bd/a Landroid/support/v4/view/a/bg;
aload 4
getfield android/support/v4/view/a/bd/b Ljava/lang/Object;
aload 5
iload 1
invokeinterface android/support/v4/view/a/bg/a(Ljava/lang/Object;Landroid/view/View;I)V 3
aload 3
areturn
.limit locals 6
.limit stack 4
.end method

.method private d(I)Landroid/support/v4/view/a/q;
invokestatic android/support/v4/view/a/q/a()Landroid/support/v4/view/a/q;
astore 3
aload 3
iconst_1
invokevirtual android/support/v4/view/a/q/h(Z)V
aload 3
getstatic android/support/v4/widget/as/d Ljava/lang/String;
invokevirtual android/support/v4/view/a/q/b(Ljava/lang/CharSequence;)V
aload 3
invokevirtual android/support/v4/view/a/q/m()Ljava/lang/CharSequence;
ifnonnull L0
aload 3
invokevirtual android/support/v4/view/a/q/n()Ljava/lang/CharSequence;
ifnonnull L0
new java/lang/RuntimeException
dup
ldc "Callbacks must add text or a content description in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 3
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokevirtual android/support/v4/view/a/q/a(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/isEmpty()Z
ifeq L1
new java/lang/RuntimeException
dup
ldc "Callbacks must set parent bounds in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 3
invokevirtual android/support/v4/view/a/q/b()I
istore 2
iload 2
bipush 64
iand
ifeq L2
new java/lang/RuntimeException
dup
ldc "Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L2:
iload 2
sipush 128
iand
ifeq L3
new java/lang/RuntimeException
dup
ldc "Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/support/v4/view/a/q/a(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
astore 4
getstatic android/support/v4/view/a/q/a Landroid/support/v4/view/a/w;
aload 3
getfield android/support/v4/view/a/q/b Ljava/lang/Object;
aload 4
iload 1
invokeinterface android/support/v4/view/a/w/d(Ljava/lang/Object;Landroid/view/View;I)V 3
aload 3
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/support/v4/view/a/q/d(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/as/l I
iload 1
if_icmpne L4
aload 3
iconst_1
invokevirtual android/support/v4/view/a/q/d(Z)V
aload 3
sipush 128
invokevirtual android/support/v4/view/a/q/a(I)V
L5:
aload 0
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokespecial android/support/v4/widget/as/a(Landroid/graphics/Rect;)Z
ifeq L6
aload 3
iconst_1
invokevirtual android/support/v4/view/a/q/c(Z)V
aload 3
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokevirtual android/support/v4/view/a/q/b(Landroid/graphics/Rect;)V
L6:
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
aload 0
getfield android/support/v4/widget/as/h [I
invokevirtual android/view/View/getLocationOnScreen([I)V
aload 0
getfield android/support/v4/widget/as/h [I
iconst_0
iaload
istore 1
aload 0
getfield android/support/v4/widget/as/h [I
iconst_1
iaload
istore 2
aload 0
getfield android/support/v4/widget/as/e Landroid/graphics/Rect;
aload 0
getfield android/support/v4/widget/as/f Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/widget/as/e Landroid/graphics/Rect;
iload 1
iload 2
invokevirtual android/graphics/Rect/offset(II)V
aload 3
aload 0
getfield android/support/v4/widget/as/e Landroid/graphics/Rect;
invokevirtual android/support/v4/view/a/q/d(Landroid/graphics/Rect;)V
aload 3
areturn
L4:
aload 3
iconst_0
invokevirtual android/support/v4/view/a/q/d(Z)V
aload 3
bipush 64
invokevirtual android/support/v4/view/a/q/a(I)V
goto L5
.limit locals 5
.limit stack 4
.end method

.method private d(II)Z
iload 2
lookupswitch
64 : L0
128 : L0
default : L1
L1:
aload 0
invokevirtual android/support/v4/widget/as/e()Z
ireturn
L0:
iload 2
lookupswitch
64 : L2
128 : L3
default : L4
L4:
iconst_0
ireturn
L2:
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokevirtual android/view/accessibility/AccessibilityManager/isEnabled()Z
ifeq L5
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokestatic android/support/v4/view/a/h/a(Landroid/view/accessibility/AccessibilityManager;)Z
ifne L6
L5:
iconst_0
ireturn
L6:
aload 0
iload 1
invokespecial android/support/v4/widget/as/e(I)Z
ifne L7
aload 0
getfield android/support/v4/widget/as/l I
ldc_w -2147483648
if_icmpeq L8
aload 0
aload 0
getfield android/support/v4/widget/as/l I
ldc_w 65536
invokespecial android/support/v4/widget/as/a(II)Z
pop
L8:
aload 0
iload 1
putfield android/support/v4/widget/as/l I
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/invalidate()V
aload 0
iload 1
ldc_w 32768
invokespecial android/support/v4/widget/as/a(II)Z
pop
iconst_1
ireturn
L7:
iconst_0
ireturn
L3:
aload 0
iload 1
invokespecial android/support/v4/widget/as/e(I)Z
ifeq L9
aload 0
ldc_w -2147483648
putfield android/support/v4/widget/as/l I
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/invalidate()V
aload 0
iload 1
ldc_w 65536
invokespecial android/support/v4/widget/as/a(II)Z
pop
iconst_1
ireturn
L9:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method private e(I)Z
aload 0
getfield android/support/v4/widget/as/l I
iload 1
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private e(II)Z
iload 2
lookupswitch
64 : L0
128 : L1
default : L2
L2:
iconst_0
ireturn
L0:
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokevirtual android/view/accessibility/AccessibilityManager/isEnabled()Z
ifeq L3
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokestatic android/support/v4/view/a/h/a(Landroid/view/accessibility/AccessibilityManager;)Z
ifne L4
L3:
iconst_0
ireturn
L4:
aload 0
iload 1
invokespecial android/support/v4/widget/as/e(I)Z
ifne L5
aload 0
getfield android/support/v4/widget/as/l I
ldc_w -2147483648
if_icmpeq L6
aload 0
aload 0
getfield android/support/v4/widget/as/l I
ldc_w 65536
invokespecial android/support/v4/widget/as/a(II)Z
pop
L6:
aload 0
iload 1
putfield android/support/v4/widget/as/l I
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/invalidate()V
aload 0
iload 1
ldc_w 32768
invokespecial android/support/v4/widget/as/a(II)Z
pop
iconst_1
ireturn
L5:
iconst_0
ireturn
L1:
aload 0
iload 1
invokespecial android/support/v4/widget/as/e(I)Z
ifeq L7
aload 0
ldc_w -2147483648
putfield android/support/v4/widget/as/l I
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/invalidate()V
aload 0
iload 1
ldc_w 65536
invokespecial android/support/v4/widget/as/a(II)Z
pop
iconst_1
ireturn
L7:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method private f()V
aload 0
iconst_m1
sipush 2048
invokespecial android/support/v4/widget/as/a(II)Z
pop
return
.limit locals 1
.limit stack 3
.end method

.method private f(I)Z
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokevirtual android/view/accessibility/AccessibilityManager/isEnabled()Z
ifeq L0
aload 0
getfield android/support/v4/widget/as/i Landroid/view/accessibility/AccessibilityManager;
invokestatic android/support/v4/view/a/h/a(Landroid/view/accessibility/AccessibilityManager;)Z
ifne L1
L0:
iconst_0
ireturn
L1:
aload 0
iload 1
invokespecial android/support/v4/widget/as/e(I)Z
ifne L0
aload 0
getfield android/support/v4/widget/as/l I
ldc_w -2147483648
if_icmpeq L2
aload 0
aload 0
getfield android/support/v4/widget/as/l I
ldc_w 65536
invokespecial android/support/v4/widget/as/a(II)Z
pop
L2:
aload 0
iload 1
putfield android/support/v4/widget/as/l I
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/invalidate()V
aload 0
iload 1
ldc_w 32768
invokespecial android/support/v4/widget/as/a(II)Z
pop
iconst_1
ireturn
.limit locals 2
.limit stack 3
.end method

.method private g()V
aload 0
iconst_m1
sipush 2048
invokespecial android/support/v4/widget/as/a(II)Z
pop
return
.limit locals 1
.limit stack 3
.end method

.method private g(I)Z
aload 0
iload 1
invokespecial android/support/v4/widget/as/e(I)Z
ifeq L0
aload 0
ldc_w -2147483648
putfield android/support/v4/widget/as/l I
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokevirtual android/view/View/invalidate()V
aload 0
iload 1
ldc_w 65536
invokespecial android/support/v4/widget/as/a(II)Z
pop
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method private h()I
aload 0
getfield android/support/v4/widget/as/l I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i()Landroid/support/v4/view/a/q;
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
invokestatic android/support/v4/view/a/q/a(Landroid/view/View;)Landroid/support/v4/view/a/q;
astore 2
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
aload 2
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
invokevirtual java/util/LinkedList/iterator()Ljava/util/Iterator;
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
astore 4
aload 0
getfield android/support/v4/widget/as/j Landroid/view/View;
astore 5
aload 4
invokevirtual java/lang/Integer/intValue()I
istore 1
getstatic android/support/v4/view/a/q/a Landroid/support/v4/view/a/w;
aload 2
getfield android/support/v4/view/a/q/b Ljava/lang/Object;
aload 5
iload 1
invokeinterface android/support/v4/view/a/w/e(Ljava/lang/Object;Landroid/view/View;I)V 3
goto L0
L1:
aload 2
areturn
.limit locals 6
.limit stack 4
.end method

.method private static j()V
return
.limit locals 0
.limit stack 0
.end method

.method protected abstract a()I
.end method

.method public final a(Landroid/view/View;)Landroid/support/v4/view/a/aq;
aload 0
getfield android/support/v4/widget/as/k Landroid/support/v4/widget/au;
ifnonnull L0
aload 0
new android/support/v4/widget/au
dup
aload 0
iconst_0
invokespecial android/support/v4/widget/au/<init>(Landroid/support/v4/widget/as;B)V
putfield android/support/v4/widget/as/k Landroid/support/v4/widget/au;
L0:
aload 0
getfield android/support/v4/widget/as/k Landroid/support/v4/widget/au;
areturn
.limit locals 2
.limit stack 5
.end method

.method protected abstract b()V
.end method

.method protected abstract c()V
.end method

.method protected abstract d()V
.end method

.method protected abstract e()Z
.end method
