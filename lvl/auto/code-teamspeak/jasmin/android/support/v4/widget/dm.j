.bytecode 50.0
.class final synchronized android/support/v4/widget/dm
.super java/lang/Object

.field private static final 'a' I = -1291845632


.field private static final 'b' I = -2147483648


.field private static final 'c' I = 1291845632


.field private static final 'd' I = 436207616


.field private static final 'e' I = 2000


.field private static final 'f' I = 1000


.field private static final 'g' Landroid/view/animation/Interpolator;

.field private final 'h' Landroid/graphics/Paint;

.field private final 'i' Landroid/graphics/RectF;

.field private 'j' F

.field private 'k' J

.field private 'l' J

.field private 'm' Z

.field private 'n' I

.field private 'o' I

.field private 'p' I

.field private 'q' I

.field private 'r' Landroid/view/View;

.field private 's' Landroid/graphics/Rect;

.method static <clinit>()V
new android/support/v4/view/b/b
dup
invokespecial android/support/v4/view/b/b/<init>()V
putstatic android/support/v4/widget/dm/g Landroid/view/animation/Interpolator;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>(Landroid/view/View;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield android/support/v4/widget/dm/h Landroid/graphics/Paint;
aload 0
new android/graphics/RectF
dup
invokespecial android/graphics/RectF/<init>()V
putfield android/support/v4/widget/dm/i Landroid/graphics/RectF;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
aload 0
aload 1
putfield android/support/v4/widget/dm/r Landroid/view/View;
aload 0
ldc_w -1291845632
putfield android/support/v4/widget/dm/n I
aload 0
ldc_w -2147483648
putfield android/support/v4/widget/dm/o I
aload 0
ldc_w 1291845632
putfield android/support/v4/widget/dm/p I
aload 0
ldc_w 436207616
putfield android/support/v4/widget/dm/q I
return
.limit locals 2
.limit stack 3
.end method

.method private a()V
aload 0
getfield android/support/v4/widget/dm/m Z
ifne L0
aload 0
fconst_0
putfield android/support/v4/widget/dm/j F
aload 0
invokestatic android/view/animation/AnimationUtils/currentAnimationTimeMillis()J
putfield android/support/v4/widget/dm/k J
aload 0
iconst_1
putfield android/support/v4/widget/dm/m Z
aload 0
getfield android/support/v4/widget/dm/r Landroid/view/View;
invokevirtual android/view/View/postInvalidate()V
L0:
return
.limit locals 1
.limit stack 3
.end method

.method private a(F)V
aload 0
fload 1
putfield android/support/v4/widget/dm/j F
aload 0
lconst_0
putfield android/support/v4/widget/dm/k J
aload 0
getfield android/support/v4/widget/dm/r Landroid/view/View;
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
invokestatic android/support/v4/view/cx/a(Landroid/view/View;IIII)V
return
.limit locals 2
.limit stack 5
.end method

.method private a(IIII)V
aload 0
iload 1
putfield android/support/v4/widget/dm/n I
aload 0
iload 2
putfield android/support/v4/widget/dm/o I
aload 0
iload 3
putfield android/support/v4/widget/dm/p I
aload 0
iload 4
putfield android/support/v4/widget/dm/q I
return
.limit locals 5
.limit stack 2
.end method

.method private a(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/width()I
istore 5
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/height()I
istore 6
iload 5
iconst_2
idiv
istore 7
iload 6
iconst_2
idiv
istore 8
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 5
aload 1
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
invokevirtual android/graphics/Canvas/clipRect(Landroid/graphics/Rect;)Z
pop
aload 0
getfield android/support/v4/widget/dm/m Z
ifne L0
aload 0
getfield android/support/v4/widget/dm/l J
lconst_0
lcmp
ifle L1
L0:
invokestatic android/view/animation/AnimationUtils/currentAnimationTimeMillis()J
lstore 9
aload 0
getfield android/support/v4/widget/dm/k J
lstore 11
lload 9
aload 0
getfield android/support/v4/widget/dm/k J
lsub
ldc2_w 2000L
ldiv
lstore 13
lload 9
lload 11
lsub
ldc2_w 2000L
lrem
l2f
ldc_w 20.0F
fdiv
fstore 2
aload 0
getfield android/support/v4/widget/dm/m Z
ifne L2
lload 9
aload 0
getfield android/support/v4/widget/dm/l J
lsub
ldc2_w 1000L
lcmp
iflt L3
aload 0
lconst_0
putfield android/support/v4/widget/dm/l J
return
L3:
lload 9
aload 0
getfield android/support/v4/widget/dm/l J
lsub
ldc2_w 1000L
lrem
l2f
ldc_w 10.0F
fdiv
ldc_w 100.0F
fdiv
fstore 3
iload 7
i2f
fstore 4
getstatic android/support/v4/widget/dm/g Landroid/view/animation/Interpolator;
fload 3
invokeinterface android/view/animation/Interpolator/getInterpolation(F)F 1
fload 4
fmul
fstore 3
aload 0
getfield android/support/v4/widget/dm/i Landroid/graphics/RectF;
iload 7
i2f
fload 3
fsub
fconst_0
fload 3
iload 7
i2f
fadd
iload 6
i2f
invokevirtual android/graphics/RectF/set(FFFF)V
aload 1
aload 0
getfield android/support/v4/widget/dm/i Landroid/graphics/RectF;
iconst_0
iconst_0
invokevirtual android/graphics/Canvas/saveLayerAlpha(Landroid/graphics/RectF;II)I
pop
iconst_1
istore 6
L4:
lload 13
lconst_0
lcmp
ifne L5
aload 1
aload 0
getfield android/support/v4/widget/dm/n I
invokevirtual android/graphics/Canvas/drawColor(I)V
L6:
fload 2
fconst_0
fcmpl
iflt L7
fload 2
ldc_w 25.0F
fcmpg
ifgt L7
ldc_w 25.0F
fload 2
fadd
fconst_2
fmul
ldc_w 100.0F
fdiv
fstore 3
aload 0
aload 1
iload 7
i2f
iload 8
i2f
aload 0
getfield android/support/v4/widget/dm/n I
fload 3
invokespecial android/support/v4/widget/dm/a(Landroid/graphics/Canvas;FFIF)V
L7:
fload 2
fconst_0
fcmpl
iflt L8
fload 2
ldc_w 50.0F
fcmpg
ifgt L8
fconst_2
fload 2
fmul
ldc_w 100.0F
fdiv
fstore 3
aload 0
aload 1
iload 7
i2f
iload 8
i2f
aload 0
getfield android/support/v4/widget/dm/o I
fload 3
invokespecial android/support/v4/widget/dm/a(Landroid/graphics/Canvas;FFIF)V
L8:
fload 2
ldc_w 25.0F
fcmpl
iflt L9
fload 2
ldc_w 75.0F
fcmpg
ifgt L9
fload 2
ldc_w 25.0F
fsub
fconst_2
fmul
ldc_w 100.0F
fdiv
fstore 3
aload 0
aload 1
iload 7
i2f
iload 8
i2f
aload 0
getfield android/support/v4/widget/dm/p I
fload 3
invokespecial android/support/v4/widget/dm/a(Landroid/graphics/Canvas;FFIF)V
L9:
fload 2
ldc_w 50.0F
fcmpl
iflt L10
fload 2
ldc_w 100.0F
fcmpg
ifgt L10
fload 2
ldc_w 50.0F
fsub
fconst_2
fmul
ldc_w 100.0F
fdiv
fstore 3
aload 0
aload 1
iload 7
i2f
iload 8
i2f
aload 0
getfield android/support/v4/widget/dm/q I
fload 3
invokespecial android/support/v4/widget/dm/a(Landroid/graphics/Canvas;FFIF)V
L10:
fload 2
ldc_w 75.0F
fcmpl
iflt L11
fload 2
ldc_w 100.0F
fcmpg
ifgt L11
fload 2
ldc_w 75.0F
fsub
fconst_2
fmul
ldc_w 100.0F
fdiv
fstore 2
aload 0
aload 1
iload 7
i2f
iload 8
i2f
aload 0
getfield android/support/v4/widget/dm/n I
fload 2
invokespecial android/support/v4/widget/dm/a(Landroid/graphics/Canvas;FFIF)V
L11:
aload 0
getfield android/support/v4/widget/dm/j F
fconst_0
fcmpl
ifle L12
iload 6
ifeq L12
aload 1
iload 5
invokevirtual android/graphics/Canvas/restoreToCount(I)V
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 5
aload 1
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
invokevirtual android/graphics/Canvas/clipRect(Landroid/graphics/Rect;)Z
pop
aload 0
aload 1
iload 7
iload 8
invokespecial android/support/v4/widget/dm/a(Landroid/graphics/Canvas;II)V
L13:
aload 0
getfield android/support/v4/widget/dm/r Landroid/view/View;
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
invokestatic android/support/v4/view/cx/a(Landroid/view/View;IIII)V
iload 5
istore 6
L14:
aload 1
iload 6
invokevirtual android/graphics/Canvas/restoreToCount(I)V
return
L5:
fload 2
fconst_0
fcmpl
iflt L15
fload 2
ldc_w 25.0F
fcmpg
ifge L15
aload 1
aload 0
getfield android/support/v4/widget/dm/q I
invokevirtual android/graphics/Canvas/drawColor(I)V
goto L6
L15:
fload 2
ldc_w 25.0F
fcmpl
iflt L16
fload 2
ldc_w 50.0F
fcmpg
ifge L16
aload 1
aload 0
getfield android/support/v4/widget/dm/n I
invokevirtual android/graphics/Canvas/drawColor(I)V
goto L6
L16:
fload 2
ldc_w 50.0F
fcmpl
iflt L17
fload 2
ldc_w 75.0F
fcmpg
ifge L17
aload 1
aload 0
getfield android/support/v4/widget/dm/o I
invokevirtual android/graphics/Canvas/drawColor(I)V
goto L6
L17:
aload 1
aload 0
getfield android/support/v4/widget/dm/p I
invokevirtual android/graphics/Canvas/drawColor(I)V
goto L6
L1:
iload 5
istore 6
aload 0
getfield android/support/v4/widget/dm/j F
fconst_0
fcmpl
ifle L14
iload 5
istore 6
aload 0
getfield android/support/v4/widget/dm/j F
f2d
dconst_1
dcmpg
ifgt L14
aload 0
aload 1
iload 7
iload 8
invokespecial android/support/v4/widget/dm/a(Landroid/graphics/Canvas;II)V
iload 5
istore 6
goto L14
L12:
goto L13
L2:
iconst_0
istore 6
goto L4
.limit locals 15
.limit stack 6
.end method

.method private a(Landroid/graphics/Canvas;FFIF)V
aload 0
getfield android/support/v4/widget/dm/h Landroid/graphics/Paint;
iload 4
invokevirtual android/graphics/Paint/setColor(I)V
aload 1
invokevirtual android/graphics/Canvas/save()I
pop
aload 1
fload 2
fload 3
invokevirtual android/graphics/Canvas/translate(FF)V
getstatic android/support/v4/widget/dm/g Landroid/view/animation/Interpolator;
fload 5
invokeinterface android/view/animation/Interpolator/getInterpolation(F)F 1
fstore 3
aload 1
fload 3
fload 3
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
fconst_0
fconst_0
fload 2
aload 0
getfield android/support/v4/widget/dm/h Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawCircle(FFFLandroid/graphics/Paint;)V
aload 1
invokevirtual android/graphics/Canvas/restore()V
return
.limit locals 6
.limit stack 5
.end method

.method private a(Landroid/graphics/Canvas;II)V
aload 0
getfield android/support/v4/widget/dm/h Landroid/graphics/Paint;
aload 0
getfield android/support/v4/widget/dm/n I
invokevirtual android/graphics/Paint/setColor(I)V
aload 1
iload 2
i2f
iload 3
i2f
iload 2
i2f
aload 0
getfield android/support/v4/widget/dm/j F
fmul
aload 0
getfield android/support/v4/widget/dm/h Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawCircle(FFFLandroid/graphics/Paint;)V
return
.limit locals 4
.limit stack 5
.end method

.method private b()V
aload 0
getfield android/support/v4/widget/dm/m Z
ifeq L0
aload 0
fconst_0
putfield android/support/v4/widget/dm/j F
aload 0
invokestatic android/view/animation/AnimationUtils/currentAnimationTimeMillis()J
putfield android/support/v4/widget/dm/l J
aload 0
iconst_0
putfield android/support/v4/widget/dm/m Z
aload 0
getfield android/support/v4/widget/dm/r Landroid/view/View;
invokevirtual android/view/View/postInvalidate()V
L0:
return
.limit locals 1
.limit stack 3
.end method

.method private b(IIII)V
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
iload 1
putfield android/graphics/Rect/left I
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
iload 2
putfield android/graphics/Rect/top I
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
iload 3
putfield android/graphics/Rect/right I
aload 0
getfield android/support/v4/widget/dm/s Landroid/graphics/Rect;
iload 4
putfield android/graphics/Rect/bottom I
return
.limit locals 5
.limit stack 2
.end method

.method private c()Z
aload 0
getfield android/support/v4/widget/dm/m Z
ifne L0
aload 0
getfield android/support/v4/widget/dm/l J
lconst_0
lcmp
ifle L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 4
.end method
