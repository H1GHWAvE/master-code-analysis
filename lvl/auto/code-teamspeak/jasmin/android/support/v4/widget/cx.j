.bytecode 50.0
.class public final synchronized android/support/v4/widget/cx
.super android/support/v4/widget/bz

.field protected 'l' [I

.field protected 'm' [I

.field 'n' [Ljava/lang/String;

.field private 'o' I

.field private 'p' Landroid/support/v4/widget/cy;

.field private 'q' Landroid/support/v4/widget/cz;

.method private <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
aload 1
iload 2
aload 3
invokespecial android/support/v4/widget/bz/<init>(Landroid/content/Context;ILandroid/database/Cursor;)V
aload 0
iconst_m1
putfield android/support/v4/widget/cx/o I
aload 0
aload 5
putfield android/support/v4/widget/cx/m [I
aload 0
aload 4
putfield android/support/v4/widget/cx/n [Ljava/lang/String;
aload 0
aload 4
invokespecial android/support/v4/widget/cx/a([Ljava/lang/String;)V
return
.limit locals 6
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V
aload 0
aload 1
iload 2
aload 3
iload 6
invokespecial android/support/v4/widget/bz/<init>(Landroid/content/Context;ILandroid/database/Cursor;I)V
aload 0
iconst_m1
putfield android/support/v4/widget/cx/o I
aload 0
aload 5
putfield android/support/v4/widget/cx/m [I
aload 0
aload 4
putfield android/support/v4/widget/cx/n [Ljava/lang/String;
aload 0
aload 4
invokespecial android/support/v4/widget/cx/a([Ljava/lang/String;)V
return
.limit locals 7
.limit stack 5
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/v4/widget/cx/o I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/database/Cursor;[Ljava/lang/String;[I)V
aload 0
aload 2
putfield android/support/v4/widget/cx/n [Ljava/lang/String;
aload 0
aload 3
putfield android/support/v4/widget/cx/m [I
aload 0
aload 1
invokespecial android/support/v4/widget/bz/a(Landroid/database/Cursor;)V
aload 0
aload 0
getfield android/support/v4/widget/cx/n [Ljava/lang/String;
invokespecial android/support/v4/widget/cx/a([Ljava/lang/String;)V
return
.limit locals 4
.limit stack 2
.end method

.method private a(Landroid/support/v4/widget/cy;)V
aload 0
aload 1
putfield android/support/v4/widget/cx/p Landroid/support/v4/widget/cy;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/widget/cz;)V
aload 0
aload 1
putfield android/support/v4/widget/cx/q Landroid/support/v4/widget/cz;
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/widget/ImageView;Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 0
aload 1
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokevirtual android/widget/ImageView/setImageResource(I)V
L1:
return
L2:
astore 2
aload 0
aload 1
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
invokevirtual android/widget/ImageView/setImageURI(Landroid/net/Uri;)V
return
.limit locals 3
.limit stack 2
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;)V
aload 0
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a([Ljava/lang/String;)V
aload 0
getfield android/support/v4/widget/cx/c Landroid/database/Cursor;
ifnull L0
aload 1
arraylength
istore 3
aload 0
getfield android/support/v4/widget/cx/l [I
ifnull L1
aload 0
getfield android/support/v4/widget/cx/l [I
arraylength
iload 3
if_icmpeq L2
L1:
aload 0
iload 3
newarray int
putfield android/support/v4/widget/cx/l [I
L2:
iconst_0
istore 2
L3:
iload 2
iload 3
if_icmpge L4
aload 0
getfield android/support/v4/widget/cx/l [I
iload 2
aload 0
getfield android/support/v4/widget/cx/c Landroid/database/Cursor;
aload 1
iload 2
aaload
invokeinterface android/database/Cursor/getColumnIndexOrThrow(Ljava/lang/String;)I 1
iastore
iload 2
iconst_1
iadd
istore 2
goto L3
L0:
aload 0
aconst_null
putfield android/support/v4/widget/cx/l [I
L4:
return
.limit locals 4
.limit stack 5
.end method

.method private c()Landroid/support/v4/widget/cz;
aload 0
getfield android/support/v4/widget/cx/q Landroid/support/v4/widget/cz;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()I
aload 0
getfield android/support/v4/widget/cx/o I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e()Landroid/support/v4/widget/cy;
aload 0
getfield android/support/v4/widget/cx/p Landroid/support/v4/widget/cy;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/View;Landroid/database/Cursor;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
aload 0
getfield android/support/v4/widget/cx/q Landroid/support/v4/widget/cz;
astore 8
aload 0
getfield android/support/v4/widget/cx/m [I
arraylength
istore 4
aload 0
getfield android/support/v4/widget/cx/l [I
astore 9
aload 0
getfield android/support/v4/widget/cx/m [I
astore 10
iconst_0
istore 3
L3:
iload 3
iload 4
if_icmpge L4
aload 1
aload 10
iload 3
iaload
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
astore 11
aload 11
ifnull L5
aload 8
ifnull L6
aload 8
invokeinterface android/support/v4/widget/cz/a()Z 0
istore 5
L7:
iload 5
ifne L5
aload 2
aload 9
iload 3
iaload
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
astore 7
aload 7
astore 6
aload 7
ifnonnull L8
ldc ""
astore 6
L8:
aload 11
instanceof android/widget/TextView
ifeq L9
aload 11
checkcast android/widget/TextView
aload 6
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L5:
iload 3
iconst_1
iadd
istore 3
goto L3
L9:
aload 11
instanceof android/widget/ImageView
ifeq L10
aload 11
checkcast android/widget/ImageView
astore 7
L0:
aload 7
aload 6
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokevirtual android/widget/ImageView/setImageResource(I)V
L1:
goto L5
L2:
astore 11
aload 7
aload 6
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
invokevirtual android/widget/ImageView/setImageURI(Landroid/net/Uri;)V
goto L5
L10:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 11
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " is not a  view that can be bounds by this SimpleCursorAdapter"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L4:
return
L6:
iconst_0
istore 5
goto L7
.limit locals 12
.limit stack 4
.end method

.method public final b(Landroid/database/Cursor;)Landroid/database/Cursor;
aload 0
aload 1
invokespecial android/support/v4/widget/bz/b(Landroid/database/Cursor;)Landroid/database/Cursor;
astore 1
aload 0
aload 0
getfield android/support/v4/widget/cx/n [Ljava/lang/String;
invokespecial android/support/v4/widget/cx/a([Ljava/lang/String;)V
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final c(Landroid/database/Cursor;)Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/widget/cx/p Landroid/support/v4/widget/cy;
ifnull L0
aload 0
getfield android/support/v4/widget/cx/p Landroid/support/v4/widget/cy;
invokeinterface android/support/v4/widget/cy/a()Ljava/lang/CharSequence; 0
areturn
L0:
aload 0
getfield android/support/v4/widget/cx/o I
iflt L1
aload 1
aload 0
getfield android/support/v4/widget/cx/o I
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
areturn
L1:
aload 0
aload 1
invokespecial android/support/v4/widget/bz/c(Landroid/database/Cursor;)Ljava/lang/CharSequence;
areturn
.limit locals 2
.limit stack 2
.end method
