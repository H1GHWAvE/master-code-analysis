.bytecode 50.0
.class final synchronized android/support/v4/widget/ag
.super android/support/v4/widget/ej

.field final 'a' I

.field 'b' Landroid/support/v4/widget/eg;

.field final synthetic 'c' Landroid/support/v4/widget/DrawerLayout;

.field private final 'd' Ljava/lang/Runnable;

.method public <init>(Landroid/support/v4/widget/DrawerLayout;I)V
aload 0
aload 1
putfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 0
invokespecial android/support/v4/widget/ej/<init>()V
aload 0
new android/support/v4/widget/ah
dup
aload 0
invokespecial android/support/v4/widget/ah/<init>(Landroid/support/v4/widget/ag;)V
putfield android/support/v4/widget/ag/d Ljava/lang/Runnable;
aload 0
iload 2
putfield android/support/v4/widget/ag/a I
return
.limit locals 3
.limit stack 4
.end method

.method private static synthetic a(Landroid/support/v4/widget/ag;)V
iconst_0
istore 3
aload 0
getfield android/support/v4/widget/ag/b Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/t I
istore 4
aload 0
getfield android/support/v4/widget/ag/a I
iconst_3
if_icmpne L0
iconst_1
istore 1
L1:
iload 1
ifeq L2
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 7
aload 7
ifnull L3
aload 7
invokevirtual android/view/View/getWidth()I
ineg
istore 2
L4:
iload 2
iload 4
iadd
istore 2
L5:
aload 7
ifnull L6
iload 1
ifeq L7
aload 7
invokevirtual android/view/View/getLeft()I
iload 2
if_icmplt L8
L7:
iload 1
ifne L6
aload 7
invokevirtual android/view/View/getLeft()I
iload 2
if_icmple L6
L8:
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 7
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;)I
ifne L6
aload 7
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 8
aload 0
getfield android/support/v4/widget/ag/b Landroid/support/v4/widget/eg;
aload 7
iload 2
aload 7
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/eg/a(Landroid/view/View;II)Z
pop
aload 8
iconst_1
putfield android/support/v4/widget/ad/c Z
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
aload 0
invokevirtual android/support/v4/widget/ag/b()V
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
astore 0
aload 0
getfield android/support/v4/widget/DrawerLayout/j Z
ifne L6
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 5
lload 5
lload 5
iconst_3
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 7
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 2
iload 3
istore 1
L9:
iload 1
iload 2
if_icmpge L10
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
aload 7
invokevirtual android/view/View/dispatchTouchEvent(Landroid/view/MotionEvent;)Z
pop
iload 1
iconst_1
iadd
istore 1
goto L9
L0:
iconst_0
istore 1
goto L1
L3:
iconst_0
istore 2
goto L4
L2:
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
iconst_5
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 7
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
istore 2
iload 2
iload 4
isub
istore 2
goto L5
L10:
aload 7
invokevirtual android/view/MotionEvent/recycle()V
aload 0
iconst_1
putfield android/support/v4/widget/DrawerLayout/j Z
L6:
return
.limit locals 9
.limit stack 8
.end method

.method private a(Landroid/support/v4/widget/eg;)V
aload 0
aload 1
putfield android/support/v4/widget/ag/b Landroid/support/v4/widget/eg;
return
.limit locals 2
.limit stack 2
.end method

.method private e()V
iconst_0
istore 3
aload 0
getfield android/support/v4/widget/ag/b Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/t I
istore 4
aload 0
getfield android/support/v4/widget/ag/a I
iconst_3
if_icmpne L0
iconst_1
istore 1
L1:
iload 1
ifeq L2
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 7
aload 7
ifnull L3
aload 7
invokevirtual android/view/View/getWidth()I
ineg
istore 2
L4:
iload 2
iload 4
iadd
istore 2
L5:
aload 7
ifnull L6
iload 1
ifeq L7
aload 7
invokevirtual android/view/View/getLeft()I
iload 2
if_icmplt L8
L7:
iload 1
ifne L6
aload 7
invokevirtual android/view/View/getLeft()I
iload 2
if_icmple L6
L8:
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 7
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;)I
ifne L6
aload 7
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 8
aload 0
getfield android/support/v4/widget/ag/b Landroid/support/v4/widget/eg;
aload 7
iload 2
aload 7
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/eg/a(Landroid/view/View;II)Z
pop
aload 8
iconst_1
putfield android/support/v4/widget/ad/c Z
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
aload 0
invokevirtual android/support/v4/widget/ag/b()V
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
astore 7
aload 7
getfield android/support/v4/widget/DrawerLayout/j Z
ifne L6
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 5
lload 5
lload 5
iconst_3
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 8
aload 7
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 2
iload 3
istore 1
L9:
iload 1
iload 2
if_icmpge L10
aload 7
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
aload 8
invokevirtual android/view/View/dispatchTouchEvent(Landroid/view/MotionEvent;)Z
pop
iload 1
iconst_1
iadd
istore 1
goto L9
L0:
iconst_0
istore 1
goto L1
L3:
iconst_0
istore 2
goto L4
L2:
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
iconst_5
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 7
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
istore 2
iload 2
iload 4
isub
istore 2
goto L5
L10:
aload 8
invokevirtual android/view/MotionEvent/recycle()V
aload 7
iconst_1
putfield android/support/v4/widget/DrawerLayout/j Z
L6:
return
.limit locals 9
.limit stack 8
.end method

.method public final a(Landroid/view/View;I)I
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;I)Z
ifeq L0
aload 1
invokevirtual android/view/View/getWidth()I
ineg
iload 2
iconst_0
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
ireturn
L0:
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
istore 3
iload 3
aload 1
invokevirtual android/view/View/getWidth()I
isub
iload 2
iload 3
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
ireturn
.limit locals 4
.limit stack 3
.end method

.method public final a()V
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 0
getfield android/support/v4/widget/ag/d Ljava/lang/Runnable;
invokevirtual android/support/v4/widget/DrawerLayout/removeCallbacks(Ljava/lang/Runnable;)Z
pop
return
.limit locals 1
.limit stack 2
.end method

.method public final a(I)V
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
astore 4
aload 0
getfield android/support/v4/widget/ag/b Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/v Landroid/view/View;
astore 5
aload 4
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/m I
istore 2
aload 4
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/m I
istore 3
iload 2
iconst_1
if_icmpeq L0
iload 3
iconst_1
if_icmpne L1
L0:
iconst_1
istore 2
L2:
aload 5
ifnull L3
iload 1
ifne L3
aload 5
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 6
aload 6
getfield android/support/v4/widget/ad/b F
fconst_0
fcmpl
ifne L4
aload 5
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 6
aload 6
getfield android/support/v4/widget/ad/d Z
ifeq L3
aload 6
iconst_0
putfield android/support/v4/widget/ad/d Z
aload 4
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
ifnull L5
aload 4
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
invokeinterface android/support/v4/widget/ac/b()V 0
L5:
aload 4
aload 5
iconst_0
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;Z)V
aload 4
invokevirtual android/support/v4/widget/DrawerLayout/hasWindowFocus()Z
ifeq L3
aload 4
invokevirtual android/support/v4/widget/DrawerLayout/getRootView()Landroid/view/View;
astore 5
aload 5
ifnull L3
aload 5
bipush 32
invokevirtual android/view/View/sendAccessibilityEvent(I)V
L3:
iload 2
aload 4
getfield android/support/v4/widget/DrawerLayout/i I
if_icmpeq L6
aload 4
iload 2
putfield android/support/v4/widget/DrawerLayout/i I
L6:
return
L1:
iload 2
iconst_2
if_icmpeq L7
iload 3
iconst_2
if_icmpne L8
L7:
iconst_2
istore 2
goto L2
L8:
iconst_0
istore 2
goto L2
L4:
aload 6
getfield android/support/v4/widget/ad/b F
fconst_1
fcmpl
ifne L3
aload 5
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 6
aload 6
getfield android/support/v4/widget/ad/d Z
ifne L3
aload 6
iconst_1
putfield android/support/v4/widget/ad/d Z
aload 4
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
ifnull L9
aload 4
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
invokeinterface android/support/v4/widget/ac/a()V 0
L9:
aload 4
aload 5
iconst_1
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;Z)V
aload 4
invokevirtual android/support/v4/widget/DrawerLayout/hasWindowFocus()Z
ifeq L10
aload 4
bipush 32
invokevirtual android/support/v4/widget/DrawerLayout/sendAccessibilityEvent(I)V
L10:
aload 5
invokevirtual android/view/View/requestFocus()Z
pop
goto L3
.limit locals 7
.limit stack 3
.end method

.method public final a(II)V
iload 1
iconst_1
iand
iconst_1
if_icmpne L0
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 3
L1:
aload 3
ifnull L2
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 3
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;)I
ifne L2
aload 0
getfield android/support/v4/widget/ag/b Landroid/support/v4/widget/eg;
aload 3
iload 2
invokevirtual android/support/v4/widget/eg/a(Landroid/view/View;I)V
L2:
return
L0:
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
iconst_5
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 3
goto L1
.limit locals 4
.limit stack 3
.end method

.method public final a(Landroid/view/View;F)V
aload 1
invokestatic android/support/v4/widget/DrawerLayout/b(Landroid/view/View;)F
fstore 3
aload 1
invokevirtual android/view/View/getWidth()I
istore 6
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;I)Z
ifeq L0
fload 2
fconst_0
fcmpl
ifgt L1
fload 2
fconst_0
fcmpl
ifne L2
fload 3
ldc_w 0.5F
fcmpl
ifle L2
L1:
iconst_0
istore 4
L3:
aload 0
getfield android/support/v4/widget/ag/b Landroid/support/v4/widget/eg;
iload 4
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/eg/a(II)Z
pop
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
L2:
iload 6
ineg
istore 4
goto L3
L0:
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
istore 5
fload 2
fconst_0
fcmpg
iflt L4
iload 5
istore 4
fload 2
fconst_0
fcmpl
ifne L3
iload 5
istore 4
fload 3
ldc_w 0.5F
fcmpl
ifle L3
L4:
iload 5
iload 6
isub
istore 4
goto L3
.limit locals 7
.limit stack 3
.end method

.method public final a(Landroid/view/View;)Z
aload 1
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifeq L0
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 1
aload 0
getfield android/support/v4/widget/ag/a I
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;I)Z
ifeq L0
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;)I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final b(Landroid/view/View;)I
aload 1
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifeq L0
aload 1
invokevirtual android/view/View/getWidth()I
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method final b()V
iconst_3
istore 1
aload 0
getfield android/support/v4/widget/ag/a I
iconst_3
if_icmpne L0
iconst_5
istore 1
L0:
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 2
aload 2
ifnull L1
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 2
invokevirtual android/support/v4/widget/DrawerLayout/e(Landroid/view/View;)V
L1:
return
.limit locals 3
.limit stack 2
.end method

.method public final b(Landroid/view/View;I)V
aload 1
invokevirtual android/view/View/getWidth()I
istore 4
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;I)Z
ifeq L0
iload 4
iload 2
iadd
i2f
iload 4
i2f
fdiv
fstore 3
L1:
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 1
fload 3
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;F)V
fload 3
fconst_0
fcmpl
ifne L2
iconst_4
istore 2
L3:
aload 1
iload 2
invokevirtual android/view/View/setVisibility(I)V
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
L0:
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
iload 2
isub
i2f
iload 4
i2f
fdiv
fstore 3
goto L1
L2:
iconst_0
istore 2
goto L3
.limit locals 5
.limit stack 3
.end method

.method public final c(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getTop()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final c()V
aload 0
getfield android/support/v4/widget/ag/c Landroid/support/v4/widget/DrawerLayout;
aload 0
getfield android/support/v4/widget/ag/d Ljava/lang/Runnable;
ldc2_w 160L
invokevirtual android/support/v4/widget/DrawerLayout/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method public final d(Landroid/view/View;)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
iconst_0
putfield android/support/v4/widget/ad/c Z
aload 0
invokevirtual android/support/v4/widget/ag/b()V
return
.limit locals 2
.limit stack 2
.end method

.method public final d()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method
