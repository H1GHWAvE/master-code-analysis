.bytecode 50.0
.class public abstract interface android/support/v4/widget/ef
.super java/lang/Object

.method public abstract getSupportButtonTintList()Landroid/content/res/ColorStateList;
.annotation invisible Landroid/support/a/z;
.end annotation
.end method

.method public abstract getSupportButtonTintMode()Landroid/graphics/PorterDuff$Mode;
.annotation invisible Landroid/support/a/z;
.end annotation
.end method

.method public abstract setSupportButtonTintList(Landroid/content/res/ColorStateList;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
.end method

.method public abstract setSupportButtonTintMode(Landroid/graphics/PorterDuff$Mode;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
.end method
