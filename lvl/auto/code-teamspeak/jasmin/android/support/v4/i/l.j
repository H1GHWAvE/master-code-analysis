.bytecode 50.0
.class public final synchronized android/support/v4/i/l
.super java/lang/Object
.implements android/os/Parcelable$ClassLoaderCreator

.field private final 'a' Landroid/support/v4/i/k;

.method public <init>(Landroid/support/v4/i/k;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/i/l/a Landroid/support/v4/i/k;
return
.limit locals 2
.limit stack 2
.end method

.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
aload 0
getfield android/support/v4/i/l/a Landroid/support/v4/i/k;
aload 1
aconst_null
invokeinterface android/support/v4/i/k/a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object; 2
areturn
.limit locals 2
.limit stack 3
.end method

.method public final createFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object;
aload 0
getfield android/support/v4/i/l/a Landroid/support/v4/i/k;
aload 1
aload 2
invokeinterface android/support/v4/i/k/a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public final newArray(I)[Ljava/lang/Object;
aload 0
getfield android/support/v4/i/l/a Landroid/support/v4/i/k;
iload 1
invokeinterface android/support/v4/i/k/a(I)[Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method
