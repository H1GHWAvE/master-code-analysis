.bytecode 50.0
.class public final synchronized android/support/v4/m/c
.super java/lang/Object

.field private 'a' Z

.field private 'b' I

.field private 'c' Landroid/support/v4/m/l;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokestatic java/util/Locale/getDefault()Ljava/util/Locale;
invokestatic android/support/v4/m/a/a(Ljava/util/Locale;)Z
invokespecial android/support/v4/m/c/a(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Ljava/util/Locale;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic android/support/v4/m/a/a(Ljava/util/Locale;)Z
invokespecial android/support/v4/m/c/a(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Z)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
invokespecial android/support/v4/m/c/a(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/m/l;)Landroid/support/v4/m/c;
aload 0
aload 1
putfield android/support/v4/m/c/c Landroid/support/v4/m/l;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Z)V
aload 0
iload 1
putfield android/support/v4/m/c/a Z
aload 0
invokestatic android/support/v4/m/a/a()Landroid/support/v4/m/l;
putfield android/support/v4/m/c/c Landroid/support/v4/m/l;
aload 0
iconst_2
putfield android/support/v4/m/c/b I
return
.limit locals 2
.limit stack 2
.end method

.method private b(Z)Landroid/support/v4/m/c;
iload 1
ifeq L0
aload 0
aload 0
getfield android/support/v4/m/c/b I
iconst_2
ior
putfield android/support/v4/m/c/b I
aload 0
areturn
L0:
aload 0
aload 0
getfield android/support/v4/m/c/b I
bipush -3
iand
putfield android/support/v4/m/c/b I
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private static c(Z)Landroid/support/v4/m/a;
iload 0
ifeq L0
invokestatic android/support/v4/m/a/b()Landroid/support/v4/m/a;
areturn
L0:
invokestatic android/support/v4/m/a/c()Landroid/support/v4/m/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Landroid/support/v4/m/a;
aload 0
getfield android/support/v4/m/c/b I
iconst_2
if_icmpne L0
aload 0
getfield android/support/v4/m/c/c Landroid/support/v4/m/l;
invokestatic android/support/v4/m/a/a()Landroid/support/v4/m/l;
if_acmpne L0
aload 0
getfield android/support/v4/m/c/a Z
ifeq L1
invokestatic android/support/v4/m/a/b()Landroid/support/v4/m/a;
areturn
L1:
invokestatic android/support/v4/m/a/c()Landroid/support/v4/m/a;
areturn
L0:
new android/support/v4/m/a
dup
aload 0
getfield android/support/v4/m/c/a Z
aload 0
getfield android/support/v4/m/c/b I
aload 0
getfield android/support/v4/m/c/c Landroid/support/v4/m/l;
iconst_0
invokespecial android/support/v4/m/a/<init>(ZILandroid/support/v4/m/l;B)V
areturn
.limit locals 1
.limit stack 6
.end method
