.bytecode 50.0
.class final synchronized android/support/v4/view/s
.super java/lang/Object
.implements android/support/v4/view/r

.field private static final 'e' I

.field private static final 'f' I

.field private static final 'g' I

.field private static final 'h' I = 1


.field private static final 'i' I = 2


.field private static final 'j' I = 3


.field private 'A' Landroid/view/VelocityTracker;

.field private 'a' I

.field private 'b' I

.field private 'c' I

.field private 'd' I

.field private final 'k' Landroid/os/Handler;

.field private final 'l' Landroid/view/GestureDetector$OnGestureListener;

.field private 'm' Landroid/view/GestureDetector$OnDoubleTapListener;

.field private 'n' Z

.field private 'o' Z

.field private 'p' Z

.field private 'q' Z

.field private 'r' Z

.field private 's' Landroid/view/MotionEvent;

.field private 't' Landroid/view/MotionEvent;

.field private 'u' Z

.field private 'v' F

.field private 'w' F

.field private 'x' F

.field private 'y' F

.field private 'z' Z

.method static <clinit>()V
invokestatic android/view/ViewConfiguration/getLongPressTimeout()I
putstatic android/support/v4/view/s/e I
invokestatic android/view/ViewConfiguration/getTapTimeout()I
putstatic android/support/v4/view/s/f I
invokestatic android/view/ViewConfiguration/getDoubleTapTimeout()I
putstatic android/support/v4/view/s/g I
return
.limit locals 0
.limit stack 1
.end method

.method public <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/support/v4/view/t
dup
aload 0
invokespecial android/support/v4/view/t/<init>(Landroid/support/v4/view/s;)V
putfield android/support/v4/view/s/k Landroid/os/Handler;
aload 0
aload 2
putfield android/support/v4/view/s/l Landroid/view/GestureDetector$OnGestureListener;
aload 2
instanceof android/view/GestureDetector$OnDoubleTapListener
ifeq L0
aload 0
aload 2
checkcast android/view/GestureDetector$OnDoubleTapListener
putfield android/support/v4/view/s/m Landroid/view/GestureDetector$OnDoubleTapListener;
L0:
aload 1
ifnonnull L1
new java/lang/IllegalArgumentException
dup
ldc "Context must not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield android/support/v4/view/s/l Landroid/view/GestureDetector$OnGestureListener;
ifnonnull L2
new java/lang/IllegalArgumentException
dup
ldc "OnGestureListener must not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
iconst_1
putfield android/support/v4/view/s/z Z
aload 1
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
astore 1
aload 1
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
istore 3
aload 1
invokevirtual android/view/ViewConfiguration/getScaledDoubleTapSlop()I
istore 4
aload 0
aload 1
invokevirtual android/view/ViewConfiguration/getScaledMinimumFlingVelocity()I
putfield android/support/v4/view/s/c I
aload 0
aload 1
invokevirtual android/view/ViewConfiguration/getScaledMaximumFlingVelocity()I
putfield android/support/v4/view/s/d I
aload 0
iload 3
iload 3
imul
putfield android/support/v4/view/s/a I
aload 0
iload 4
iload 4
imul
putfield android/support/v4/view/s/b I
return
.limit locals 5
.limit stack 4
.end method

.method static synthetic a(Landroid/support/v4/view/s;)Landroid/view/MotionEvent;
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/content/Context;)V
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "Context must not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/view/s/l Landroid/view/GestureDetector$OnGestureListener;
ifnonnull L1
new java/lang/IllegalArgumentException
dup
ldc "OnGestureListener must not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iconst_1
putfield android/support/v4/view/s/z Z
aload 1
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
astore 1
aload 1
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
istore 2
aload 1
invokevirtual android/view/ViewConfiguration/getScaledDoubleTapSlop()I
istore 3
aload 0
aload 1
invokevirtual android/view/ViewConfiguration/getScaledMinimumFlingVelocity()I
putfield android/support/v4/view/s/c I
aload 0
aload 1
invokevirtual android/view/ViewConfiguration/getScaledMaximumFlingVelocity()I
putfield android/support/v4/view/s/d I
aload 0
iload 2
iload 2
imul
putfield android/support/v4/view/s/a I
aload 0
iload 3
iload 3
imul
putfield android/support/v4/view/s/b I
return
.limit locals 4
.limit stack 3
.end method

.method private a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v4/view/s/r Z
ifne L0
L1:
iconst_0
ireturn
L0:
aload 3
invokevirtual android/view/MotionEvent/getEventTime()J
aload 2
invokevirtual android/view/MotionEvent/getEventTime()J
lsub
getstatic android/support/v4/view/s/g I
i2l
lcmp
ifgt L1
aload 1
invokevirtual android/view/MotionEvent/getX()F
f2i
aload 3
invokevirtual android/view/MotionEvent/getX()F
f2i
isub
istore 4
aload 1
invokevirtual android/view/MotionEvent/getY()F
f2i
aload 3
invokevirtual android/view/MotionEvent/getY()F
f2i
isub
istore 5
iload 4
iload 4
imul
iload 5
iload 5
imul
iadd
aload 0
getfield android/support/v4/view/s/b I
if_icmpge L1
iconst_1
ireturn
.limit locals 6
.limit stack 4
.end method

.method static synthetic b(Landroid/support/v4/view/s;)Landroid/view/GestureDetector$OnGestureListener;
aload 0
getfield android/support/v4/view/s/l Landroid/view/GestureDetector$OnGestureListener;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_1
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_2
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_3
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/recycle()V
aload 0
aconst_null
putfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
aload 0
iconst_0
putfield android/support/v4/view/s/u Z
aload 0
iconst_0
putfield android/support/v4/view/s/n Z
aload 0
iconst_0
putfield android/support/v4/view/s/q Z
aload 0
iconst_0
putfield android/support/v4/view/s/r Z
aload 0
iconst_0
putfield android/support/v4/view/s/o Z
aload 0
getfield android/support/v4/view/s/p Z
ifeq L0
aload 0
iconst_0
putfield android/support/v4/view/s/p Z
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private c()V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_1
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_2
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_3
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
iconst_0
putfield android/support/v4/view/s/u Z
aload 0
iconst_0
putfield android/support/v4/view/s/q Z
aload 0
iconst_0
putfield android/support/v4/view/s/r Z
aload 0
iconst_0
putfield android/support/v4/view/s/o Z
aload 0
getfield android/support/v4/view/s/p Z
ifeq L0
aload 0
iconst_0
putfield android/support/v4/view/s/p Z
L0:
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic c(Landroid/support/v4/view/s;)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_3
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
iconst_0
putfield android/support/v4/view/s/o Z
aload 0
iconst_1
putfield android/support/v4/view/s/p Z
aload 0
getfield android/support/v4/view/s/l Landroid/view/GestureDetector$OnGestureListener;
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
invokeinterface android/view/GestureDetector$OnGestureListener/onLongPress(Landroid/view/MotionEvent;)V 1
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic d(Landroid/support/v4/view/s;)Landroid/view/GestureDetector$OnDoubleTapListener;
aload 0
getfield android/support/v4/view/s/m Landroid/view/GestureDetector$OnDoubleTapListener;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_3
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
iconst_0
putfield android/support/v4/view/s/o Z
aload 0
iconst_1
putfield android/support/v4/view/s/p Z
aload 0
getfield android/support/v4/view/s/l Landroid/view/GestureDetector$OnGestureListener;
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
invokeinterface android/view/GestureDetector$OnGestureListener/onLongPress(Landroid/view/MotionEvent;)V 1
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic e(Landroid/support/v4/view/s;)Z
aload 0
getfield android/support/v4/view/s/n Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Landroid/support/v4/view/s;)Z
aload 0
iconst_1
putfield android/support/v4/view/s/o Z
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final a(Landroid/view/GestureDetector$OnDoubleTapListener;)V
aload 0
aload 1
putfield android/support/v4/view/s/m Landroid/view/GestureDetector$OnDoubleTapListener;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)V
aload 0
iload 1
putfield android/support/v4/view/s/z Z
return
.limit locals 2
.limit stack 2
.end method

.method public final a()Z
aload 0
getfield android/support/v4/view/s/z Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/MotionEvent;)Z
aload 1
invokevirtual android/view/MotionEvent/getAction()I
istore 10
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
ifnonnull L0
aload 0
invokestatic android/view/VelocityTracker/obtain()Landroid/view/VelocityTracker;
putfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
L0:
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
aload 1
invokevirtual android/view/VelocityTracker/addMovement(Landroid/view/MotionEvent;)V
iload 10
sipush 255
iand
bipush 6
if_icmpne L1
iconst_1
istore 6
L2:
iload 6
ifeq L3
aload 1
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;)I
istore 7
L4:
aload 1
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;)I
istore 9
iconst_0
istore 8
fconst_0
fstore 2
fconst_0
fstore 3
L5:
iload 8
iload 9
if_icmpge L6
fload 2
fstore 5
fload 3
fstore 4
iload 7
iload 8
if_icmpeq L7
fload 3
aload 1
iload 8
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
fadd
fstore 4
fload 2
aload 1
iload 8
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
fadd
fstore 5
L7:
iload 8
iconst_1
iadd
istore 8
fload 5
fstore 2
fload 4
fstore 3
goto L5
L1:
iconst_0
istore 6
goto L2
L3:
iconst_m1
istore 7
goto L4
L6:
iload 6
ifeq L8
iload 9
iconst_1
isub
istore 6
L9:
fload 3
iload 6
i2f
fdiv
fstore 3
fload 2
iload 6
i2f
fdiv
fstore 2
iload 10
sipush 255
iand
tableswitch 0
L10
L11
L12
L13
L14
L15
L16
default : L14
L14:
iconst_0
ireturn
L8:
iload 9
istore 6
goto L9
L15:
aload 0
fload 3
putfield android/support/v4/view/s/v F
aload 0
fload 3
putfield android/support/v4/view/s/x F
aload 0
fload 2
putfield android/support/v4/view/s/w F
aload 0
fload 2
putfield android/support/v4/view/s/y F
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_1
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_2
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_3
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
iconst_0
putfield android/support/v4/view/s/u Z
aload 0
iconst_0
putfield android/support/v4/view/s/q Z
aload 0
iconst_0
putfield android/support/v4/view/s/r Z
aload 0
iconst_0
putfield android/support/v4/view/s/o Z
aload 0
getfield android/support/v4/view/s/p Z
ifeq L14
aload 0
iconst_0
putfield android/support/v4/view/s/p Z
iconst_0
ireturn
L16:
aload 0
fload 3
putfield android/support/v4/view/s/v F
aload 0
fload 3
putfield android/support/v4/view/s/x F
aload 0
fload 2
putfield android/support/v4/view/s/w F
aload 0
fload 2
putfield android/support/v4/view/s/y F
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
sipush 1000
aload 0
getfield android/support/v4/view/s/d I
i2f
invokevirtual android/view/VelocityTracker/computeCurrentVelocity(IF)V
aload 1
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;)I
istore 7
aload 1
iload 7
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
istore 6
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
iload 6
invokestatic android/support/v4/view/cs/a(Landroid/view/VelocityTracker;I)F
fstore 2
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
iload 6
invokestatic android/support/v4/view/cs/b(Landroid/view/VelocityTracker;I)F
fstore 3
iconst_0
istore 6
L17:
iload 6
iload 9
if_icmpge L14
iload 6
iload 7
if_icmpeq L18
aload 1
iload 6
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
istore 8
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
iload 8
invokestatic android/support/v4/view/cs/a(Landroid/view/VelocityTracker;I)F
fstore 4
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
iload 8
invokestatic android/support/v4/view/cs/b(Landroid/view/VelocityTracker;I)F
fload 3
fmul
fload 4
fload 2
fmul
fadd
fconst_0
fcmpg
ifge L18
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/clear()V
iconst_0
ireturn
L18:
iload 6
iconst_1
iadd
istore 6
goto L17
L10:
aload 0
getfield android/support/v4/view/s/m Landroid/view/GestureDetector$OnDoubleTapListener;
ifnull L19
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_3
invokevirtual android/os/Handler/hasMessages(I)Z
istore 11
iload 11
ifeq L20
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_3
invokevirtual android/os/Handler/removeMessages(I)V
L20:
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
ifnull L21
aload 0
getfield android/support/v4/view/s/t Landroid/view/MotionEvent;
ifnull L21
iload 11
ifeq L21
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
astore 13
aload 0
getfield android/support/v4/view/s/t Landroid/view/MotionEvent;
astore 14
aload 0
getfield android/support/v4/view/s/r Z
ifeq L22
aload 1
invokevirtual android/view/MotionEvent/getEventTime()J
aload 14
invokevirtual android/view/MotionEvent/getEventTime()J
lsub
getstatic android/support/v4/view/s/g I
i2l
lcmp
ifgt L22
aload 13
invokevirtual android/view/MotionEvent/getX()F
f2i
aload 1
invokevirtual android/view/MotionEvent/getX()F
f2i
isub
istore 6
aload 13
invokevirtual android/view/MotionEvent/getY()F
f2i
aload 1
invokevirtual android/view/MotionEvent/getY()F
f2i
isub
istore 7
iload 7
iload 7
imul
iload 6
iload 6
imul
iadd
aload 0
getfield android/support/v4/view/s/b I
if_icmpge L22
iconst_1
istore 6
L23:
iload 6
ifeq L21
aload 0
iconst_1
putfield android/support/v4/view/s/u Z
aload 0
getfield android/support/v4/view/s/m Landroid/view/GestureDetector$OnDoubleTapListener;
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
invokeinterface android/view/GestureDetector$OnDoubleTapListener/onDoubleTap(Landroid/view/MotionEvent;)Z 1
iconst_0
ior
aload 0
getfield android/support/v4/view/s/m Landroid/view/GestureDetector$OnDoubleTapListener;
aload 1
invokeinterface android/view/GestureDetector$OnDoubleTapListener/onDoubleTapEvent(Landroid/view/MotionEvent;)Z 1
ior
istore 6
L24:
aload 0
fload 3
putfield android/support/v4/view/s/v F
aload 0
fload 3
putfield android/support/v4/view/s/x F
aload 0
fload 2
putfield android/support/v4/view/s/w F
aload 0
fload 2
putfield android/support/v4/view/s/y F
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
ifnull L25
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
invokevirtual android/view/MotionEvent/recycle()V
L25:
aload 0
aload 1
invokestatic android/view/MotionEvent/obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
putfield android/support/v4/view/s/s Landroid/view/MotionEvent;
aload 0
iconst_1
putfield android/support/v4/view/s/q Z
aload 0
iconst_1
putfield android/support/v4/view/s/r Z
aload 0
iconst_1
putfield android/support/v4/view/s/n Z
aload 0
iconst_0
putfield android/support/v4/view/s/p Z
aload 0
iconst_0
putfield android/support/v4/view/s/o Z
aload 0
getfield android/support/v4/view/s/z Z
ifeq L26
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_2
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_2
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
invokevirtual android/view/MotionEvent/getDownTime()J
getstatic android/support/v4/view/s/f I
i2l
ladd
getstatic android/support/v4/view/s/e I
i2l
ladd
invokevirtual android/os/Handler/sendEmptyMessageAtTime(IJ)Z
pop
L26:
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_1
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
invokevirtual android/view/MotionEvent/getDownTime()J
getstatic android/support/v4/view/s/f I
i2l
ladd
invokevirtual android/os/Handler/sendEmptyMessageAtTime(IJ)Z
pop
iload 6
aload 0
getfield android/support/v4/view/s/l Landroid/view/GestureDetector$OnGestureListener;
aload 1
invokeinterface android/view/GestureDetector$OnGestureListener/onDown(Landroid/view/MotionEvent;)Z 1
ior
ireturn
L22:
iconst_0
istore 6
goto L23
L21:
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_3
getstatic android/support/v4/view/s/g I
i2l
invokevirtual android/os/Handler/sendEmptyMessageDelayed(IJ)Z
pop
L19:
iconst_0
istore 6
goto L24
L12:
aload 0
getfield android/support/v4/view/s/p Z
ifne L14
aload 0
getfield android/support/v4/view/s/v F
fload 3
fsub
fstore 4
aload 0
getfield android/support/v4/view/s/w F
fload 2
fsub
fstore 5
aload 0
getfield android/support/v4/view/s/u Z
ifeq L27
aload 0
getfield android/support/v4/view/s/m Landroid/view/GestureDetector$OnDoubleTapListener;
aload 1
invokeinterface android/view/GestureDetector$OnDoubleTapListener/onDoubleTapEvent(Landroid/view/MotionEvent;)Z 1
iconst_0
ior
ireturn
L27:
aload 0
getfield android/support/v4/view/s/q Z
ifeq L28
fload 3
aload 0
getfield android/support/v4/view/s/x F
fsub
f2i
istore 6
fload 2
aload 0
getfield android/support/v4/view/s/y F
fsub
f2i
istore 7
iload 6
iload 6
imul
iload 7
iload 7
imul
iadd
istore 6
iload 6
aload 0
getfield android/support/v4/view/s/a I
if_icmple L29
aload 0
getfield android/support/v4/view/s/l Landroid/view/GestureDetector$OnGestureListener;
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
aload 1
fload 4
fload 5
invokeinterface android/view/GestureDetector$OnGestureListener/onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z 4
istore 11
aload 0
fload 3
putfield android/support/v4/view/s/v F
aload 0
fload 2
putfield android/support/v4/view/s/w F
aload 0
iconst_0
putfield android/support/v4/view/s/q Z
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_3
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_1
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_2
invokevirtual android/os/Handler/removeMessages(I)V
L30:
iload 6
aload 0
getfield android/support/v4/view/s/a I
if_icmple L31
aload 0
iconst_0
putfield android/support/v4/view/s/r Z
L31:
iload 11
ireturn
L28:
fload 4
invokestatic java/lang/Math/abs(F)F
fconst_1
fcmpl
ifge L32
fload 5
invokestatic java/lang/Math/abs(F)F
fconst_1
fcmpl
iflt L14
L32:
aload 0
getfield android/support/v4/view/s/l Landroid/view/GestureDetector$OnGestureListener;
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
aload 1
fload 4
fload 5
invokeinterface android/view/GestureDetector$OnGestureListener/onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z 4
istore 11
aload 0
fload 3
putfield android/support/v4/view/s/v F
aload 0
fload 2
putfield android/support/v4/view/s/w F
iload 11
ireturn
L11:
aload 0
iconst_0
putfield android/support/v4/view/s/n Z
aload 1
invokestatic android/view/MotionEvent/obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
astore 13
aload 0
getfield android/support/v4/view/s/u Z
ifeq L33
aload 0
getfield android/support/v4/view/s/m Landroid/view/GestureDetector$OnDoubleTapListener;
aload 1
invokeinterface android/view/GestureDetector$OnDoubleTapListener/onDoubleTapEvent(Landroid/view/MotionEvent;)Z 1
iconst_0
ior
istore 11
L34:
aload 0
getfield android/support/v4/view/s/t Landroid/view/MotionEvent;
ifnull L35
aload 0
getfield android/support/v4/view/s/t Landroid/view/MotionEvent;
invokevirtual android/view/MotionEvent/recycle()V
L35:
aload 0
aload 13
putfield android/support/v4/view/s/t Landroid/view/MotionEvent;
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
ifnull L36
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/recycle()V
aload 0
aconst_null
putfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
L36:
aload 0
iconst_0
putfield android/support/v4/view/s/u Z
aload 0
iconst_0
putfield android/support/v4/view/s/o Z
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_1
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_2
invokevirtual android/os/Handler/removeMessages(I)V
iload 11
ireturn
L33:
aload 0
getfield android/support/v4/view/s/p Z
ifeq L37
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_3
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
iconst_0
putfield android/support/v4/view/s/p Z
iconst_0
istore 11
goto L34
L37:
aload 0
getfield android/support/v4/view/s/q Z
ifeq L38
aload 0
getfield android/support/v4/view/s/l Landroid/view/GestureDetector$OnGestureListener;
aload 1
invokeinterface android/view/GestureDetector$OnGestureListener/onSingleTapUp(Landroid/view/MotionEvent;)Z 1
istore 12
iload 12
istore 11
aload 0
getfield android/support/v4/view/s/o Z
ifeq L34
iload 12
istore 11
aload 0
getfield android/support/v4/view/s/m Landroid/view/GestureDetector$OnDoubleTapListener;
ifnull L34
aload 0
getfield android/support/v4/view/s/m Landroid/view/GestureDetector$OnDoubleTapListener;
aload 1
invokeinterface android/view/GestureDetector$OnDoubleTapListener/onSingleTapConfirmed(Landroid/view/MotionEvent;)Z 1
pop
iload 12
istore 11
goto L34
L38:
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
astore 14
aload 1
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
istore 6
aload 14
sipush 1000
aload 0
getfield android/support/v4/view/s/d I
i2f
invokevirtual android/view/VelocityTracker/computeCurrentVelocity(IF)V
aload 14
iload 6
invokestatic android/support/v4/view/cs/b(Landroid/view/VelocityTracker;I)F
fstore 2
aload 14
iload 6
invokestatic android/support/v4/view/cs/a(Landroid/view/VelocityTracker;I)F
fstore 3
fload 2
invokestatic java/lang/Math/abs(F)F
aload 0
getfield android/support/v4/view/s/c I
i2f
fcmpl
ifgt L39
fload 3
invokestatic java/lang/Math/abs(F)F
aload 0
getfield android/support/v4/view/s/c I
i2f
fcmpl
ifle L40
L39:
aload 0
getfield android/support/v4/view/s/l Landroid/view/GestureDetector$OnGestureListener;
aload 0
getfield android/support/v4/view/s/s Landroid/view/MotionEvent;
aload 1
fload 3
fload 2
invokeinterface android/view/GestureDetector$OnGestureListener/onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z 4
istore 11
goto L34
L13:
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_1
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_2
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/k Landroid/os/Handler;
iconst_3
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/recycle()V
aload 0
aconst_null
putfield android/support/v4/view/s/A Landroid/view/VelocityTracker;
aload 0
iconst_0
putfield android/support/v4/view/s/u Z
aload 0
iconst_0
putfield android/support/v4/view/s/n Z
aload 0
iconst_0
putfield android/support/v4/view/s/q Z
aload 0
iconst_0
putfield android/support/v4/view/s/r Z
aload 0
iconst_0
putfield android/support/v4/view/s/o Z
aload 0
getfield android/support/v4/view/s/p Z
ifeq L14
aload 0
iconst_0
putfield android/support/v4/view/s/p Z
iconst_0
ireturn
L40:
iconst_0
istore 11
goto L34
L29:
iconst_0
istore 11
goto L30
.limit locals 15
.limit stack 6
.end method
