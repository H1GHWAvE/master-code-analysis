.bytecode 50.0
.class final synchronized android/support/v4/view/a/br
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a()Ljava/lang/Object;
invokestatic android/view/accessibility/AccessibilityWindowInfo/obtain()Landroid/view/accessibility/AccessibilityWindowInfo;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
invokestatic android/view/accessibility/AccessibilityWindowInfo/obtain(Landroid/view/accessibility/AccessibilityWindowInfo;)Landroid/view/accessibility/AccessibilityWindowInfo;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;I)Ljava/lang/Object;
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
iload 1
invokevirtual android/view/accessibility/AccessibilityWindowInfo/getChild(I)Landroid/view/accessibility/AccessibilityWindowInfo;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Landroid/graphics/Rect;)V
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
aload 1
invokevirtual android/view/accessibility/AccessibilityWindowInfo/getBoundsInScreen(Landroid/graphics/Rect;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;)I
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
invokevirtual android/view/accessibility/AccessibilityWindowInfo/getType()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Ljava/lang/Object;)I
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
invokevirtual android/view/accessibility/AccessibilityWindowInfo/getLayer()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
invokevirtual android/view/accessibility/AccessibilityWindowInfo/getRoot()Landroid/view/accessibility/AccessibilityNodeInfo;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static e(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
invokevirtual android/view/accessibility/AccessibilityWindowInfo/getParent()Landroid/view/accessibility/AccessibilityWindowInfo;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static f(Ljava/lang/Object;)I
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
invokevirtual android/view/accessibility/AccessibilityWindowInfo/getId()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static g(Ljava/lang/Object;)Z
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
invokevirtual android/view/accessibility/AccessibilityWindowInfo/isActive()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static h(Ljava/lang/Object;)Z
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
invokevirtual android/view/accessibility/AccessibilityWindowInfo/isFocused()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static i(Ljava/lang/Object;)Z
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
invokevirtual android/view/accessibility/AccessibilityWindowInfo/isAccessibilityFocused()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static j(Ljava/lang/Object;)I
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
invokevirtual android/view/accessibility/AccessibilityWindowInfo/getChildCount()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static k(Ljava/lang/Object;)V
aload 0
checkcast android/view/accessibility/AccessibilityWindowInfo
invokevirtual android/view/accessibility/AccessibilityWindowInfo/recycle()V
return
.limit locals 1
.limit stack 1
.end method
