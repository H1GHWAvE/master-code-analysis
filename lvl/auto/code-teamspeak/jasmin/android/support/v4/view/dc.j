.bytecode 50.0
.class synchronized android/support/v4/view/dc
.super android/support/v4/view/db

.field static 'b' Ljava/lang/reflect/Field;

.field static 'c' Z

.method static <clinit>()V
iconst_0
putstatic android/support/v4/view/dc/c Z
return
.limit locals 0
.limit stack 1
.end method

.method <init>()V
aload 0
invokespecial android/support/v4/view/db/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final H(Landroid/view/View;)Landroid/support/v4/view/fk;
aload 0
getfield android/support/v4/view/dc/a Ljava/util/WeakHashMap;
ifnonnull L0
aload 0
new java/util/WeakHashMap
dup
invokespecial java/util/WeakHashMap/<init>()V
putfield android/support/v4/view/dc/a Ljava/util/WeakHashMap;
L0:
aload 0
getfield android/support/v4/view/dc/a Ljava/util/WeakHashMap;
aload 1
invokevirtual java/util/WeakHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/support/v4/view/fk
astore 3
aload 3
astore 2
aload 3
ifnonnull L1
new android/support/v4/view/fk
dup
aload 1
invokespecial android/support/v4/view/fk/<init>(Landroid/view/View;)V
astore 2
aload 0
getfield android/support/v4/view/dc/a Ljava/util/WeakHashMap;
aload 1
aload 2
invokevirtual java/util/WeakHashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 2
areturn
.limit locals 4
.limit stack 3
.end method

.method public final a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
aload 1
aload 2
getfield android/support/v4/view/a/q/b Ljava/lang/Object;
checkcast android/view/accessibility/AccessibilityNodeInfo
invokevirtual android/view/View/onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/view/View;Landroid/support/v4/view/a;)V
.annotation invisibleparam 2 Landroid/support/a/z;
.end annotation
aload 2
ifnonnull L0
aconst_null
astore 2
L1:
aload 1
aload 2
checkcast android/view/View$AccessibilityDelegate
invokevirtual android/view/View/setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V
return
L0:
aload 2
getfield android/support/v4/view/a/b Ljava/lang/Object;
astore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 1
aload 2
invokevirtual android/view/View/onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/view/View;I)Z
aload 1
iload 2
invokevirtual android/view/View/canScrollHorizontally(I)Z
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 1
aload 2
invokevirtual android/view/View/onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final b(Landroid/view/View;Z)V
aload 1
iload 2
invokevirtual android/view/View/setFitsSystemWindows(Z)V
return
.limit locals 3
.limit stack 2
.end method

.method public final b(Landroid/view/View;)Z
.catch java/lang/Throwable from L0 to L1 using L2
.catch java/lang/Throwable from L1 to L3 using L4
getstatic android/support/v4/view/dc/c Z
ifeq L5
L6:
iconst_0
ireturn
L5:
getstatic android/support/v4/view/dc/b Ljava/lang/reflect/Field;
ifnonnull L1
L0:
ldc android/view/View
ldc "mAccessibilityDelegate"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 2
aload 2
putstatic android/support/v4/view/dc/b Ljava/lang/reflect/Field;
aload 2
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
L1:
getstatic android/support/v4/view/dc/b Ljava/lang/reflect/Field;
aload 1
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
astore 1
L3:
aload 1
ifnull L6
iconst_1
ireturn
L2:
astore 1
iconst_1
putstatic android/support/v4/view/dc/c Z
iconst_0
ireturn
L4:
astore 1
iconst_1
putstatic android/support/v4/view/dc/c Z
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final b(Landroid/view/View;I)Z
aload 1
iload 2
invokevirtual android/view/View/canScrollVertically(I)Z
ireturn
.limit locals 3
.limit stack 2
.end method
