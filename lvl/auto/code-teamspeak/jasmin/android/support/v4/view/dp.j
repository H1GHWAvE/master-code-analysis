.bytecode 50.0
.class final synchronized android/support/v4/view/dp
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/View;)I
aload 0
invokevirtual android/view/View/getLabelFor()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/View;I)V
aload 0
iload 1
invokevirtual android/view/View/setLabelFor(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/view/View;IIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
invokevirtual android/view/View/setPaddingRelative(IIII)V
return
.limit locals 5
.limit stack 5
.end method

.method private static a(Landroid/view/View;Landroid/graphics/Paint;)V
aload 0
aload 1
invokevirtual android/view/View/setLayerPaint(Landroid/graphics/Paint;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/view/View;)I
aload 0
invokevirtual android/view/View/getLayoutDirection()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Landroid/view/View;I)V
aload 0
iload 1
invokevirtual android/view/View/setLayoutDirection(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Landroid/view/View;)I
aload 0
invokevirtual android/view/View/getPaddingStart()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Landroid/view/View;)I
aload 0
invokevirtual android/view/View/getPaddingEnd()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static e(Landroid/view/View;)I
aload 0
invokevirtual android/view/View/getWindowSystemUiVisibility()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static f(Landroid/view/View;)Z
aload 0
invokevirtual android/view/View/isPaddingRelative()Z
ireturn
.limit locals 1
.limit stack 1
.end method
