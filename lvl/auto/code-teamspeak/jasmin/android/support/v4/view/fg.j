.bytecode 50.0
.class synchronized android/support/v4/view/fg
.super java/lang/Object
.implements android/support/v4/view/fd

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;)V
aload 1
instanceof android/support/v4/view/bv
ifeq L0
aload 1
checkcast android/support/v4/view/bv
aload 2
invokeinterface android/support/v4/view/bv/onStopNestedScroll(Landroid/view/View;)V 1
L0:
return
.limit locals 3
.limit stack 2
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;IIII)V
aload 1
instanceof android/support/v4/view/bv
ifeq L0
aload 1
checkcast android/support/v4/view/bv
aload 2
iload 3
iload 4
iload 5
iload 6
invokeinterface android/support/v4/view/bv/onNestedScroll(Landroid/view/View;IIII)V 5
L0:
return
.limit locals 7
.limit stack 6
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;II[I)V
aload 1
instanceof android/support/v4/view/bv
ifeq L0
aload 1
checkcast android/support/v4/view/bv
aload 2
iload 3
iload 4
aload 5
invokeinterface android/support/v4/view/bv/onNestedPreScroll(Landroid/view/View;II[I)V 4
L0:
return
.limit locals 6
.limit stack 5
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;FF)Z
aload 1
instanceof android/support/v4/view/bv
ifeq L0
aload 1
checkcast android/support/v4/view/bv
aload 2
fload 3
fload 4
invokeinterface android/support/v4/view/bv/onNestedPreFling(Landroid/view/View;FF)Z 3
ireturn
L0:
iconst_0
ireturn
.limit locals 5
.limit stack 4
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z
aload 1
instanceof android/support/v4/view/bv
ifeq L0
aload 1
checkcast android/support/v4/view/bv
aload 2
fload 3
fload 4
iload 5
invokeinterface android/support/v4/view/bv/onNestedFling(Landroid/view/View;FFZ)Z 4
ireturn
L0:
iconst_0
ireturn
.limit locals 6
.limit stack 5
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z
aload 1
instanceof android/support/v4/view/bv
ifeq L0
aload 1
checkcast android/support/v4/view/bv
aload 2
aload 3
iload 4
invokeinterface android/support/v4/view/bv/onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z 3
ireturn
L0:
iconst_0
ireturn
.limit locals 5
.limit stack 4
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
aload 2
ifnonnull L0
iconst_0
ireturn
L0:
aload 2
invokevirtual android/view/View/getContext()Landroid/content/Context;
ldc "accessibility"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/accessibility/AccessibilityManager
aload 3
invokevirtual android/view/accessibility/AccessibilityManager/sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
iconst_1
ireturn
.limit locals 4
.limit stack 2
.end method

.method public b(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V
aload 1
instanceof android/support/v4/view/bv
ifeq L0
aload 1
checkcast android/support/v4/view/bv
aload 2
aload 3
iload 4
invokeinterface android/support/v4/view/bv/onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V 3
L0:
return
.limit locals 5
.limit stack 4
.end method

.method public c(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V
return
.limit locals 5
.limit stack 0
.end method
