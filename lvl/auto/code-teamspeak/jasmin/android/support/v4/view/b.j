.bytecode 50.0
.class synchronized android/support/v4/view/b
.super android/support/v4/view/g

.method <init>()V
aload 0
invokespecial android/support/v4/view/g/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a()Ljava/lang/Object;
new android/view/View$AccessibilityDelegate
dup
invokespecial android/view/View$AccessibilityDelegate/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public a(Landroid/support/v4/view/a;)Ljava/lang/Object;
new android/support/v4/view/i
dup
new android/support/v4/view/c
dup
aload 0
aload 1
invokespecial android/support/v4/view/c/<init>(Landroid/support/v4/view/b;Landroid/support/v4/view/a;)V
invokespecial android/support/v4/view/i/<init>(Landroid/support/v4/view/j;)V
areturn
.limit locals 2
.limit stack 6
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;I)V
aload 1
checkcast android/view/View$AccessibilityDelegate
aload 2
iload 3
invokevirtual android/view/View$AccessibilityDelegate/sendAccessibilityEvent(Landroid/view/View;I)V
return
.limit locals 4
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;Landroid/support/v4/view/a/q;)V
aload 3
getfield android/support/v4/view/a/q/b Ljava/lang/Object;
astore 3
aload 1
checkcast android/view/View$AccessibilityDelegate
aload 2
aload 3
checkcast android/view/accessibility/AccessibilityNodeInfo
invokevirtual android/view/View$AccessibilityDelegate/onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
return
.limit locals 4
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
aload 1
checkcast android/view/View$AccessibilityDelegate
aload 2
aload 3
invokevirtual android/view/View$AccessibilityDelegate/dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
ireturn
.limit locals 4
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
aload 1
checkcast android/view/View$AccessibilityDelegate
aload 2
aload 3
aload 4
invokevirtual android/view/View$AccessibilityDelegate/onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
ireturn
.limit locals 5
.limit stack 4
.end method

.method public final b(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 1
checkcast android/view/View$AccessibilityDelegate
aload 2
aload 3
invokevirtual android/view/View$AccessibilityDelegate/onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 4
.limit stack 3
.end method

.method public final c(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 1
checkcast android/view/View$AccessibilityDelegate
aload 2
aload 3
invokevirtual android/view/View$AccessibilityDelegate/onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 4
.limit stack 3
.end method

.method public final d(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 1
checkcast android/view/View$AccessibilityDelegate
aload 2
aload 3
invokevirtual android/view/View$AccessibilityDelegate/sendAccessibilityEventUnchecked(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 4
.limit stack 3
.end method
