.bytecode 50.0
.class public final synchronized android/support/v4/f/b/f
.super java/lang/Object

.field final 'a' Ljava/security/Signature;

.field final 'b' Ljavax/crypto/Cipher;

.field final 'c' Ljavax/crypto/Mac;

.method public <init>(Ljava/security/Signature;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/f/b/f/a Ljava/security/Signature;
aload 0
aconst_null
putfield android/support/v4/f/b/f/b Ljavax/crypto/Cipher;
aload 0
aconst_null
putfield android/support/v4/f/b/f/c Ljavax/crypto/Mac;
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Ljavax/crypto/Cipher;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/f/b/f/b Ljavax/crypto/Cipher;
aload 0
aconst_null
putfield android/support/v4/f/b/f/a Ljava/security/Signature;
aload 0
aconst_null
putfield android/support/v4/f/b/f/c Ljavax/crypto/Mac;
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Ljavax/crypto/Mac;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/f/b/f/c Ljavax/crypto/Mac;
aload 0
aconst_null
putfield android/support/v4/f/b/f/b Ljavax/crypto/Cipher;
aload 0
aconst_null
putfield android/support/v4/f/b/f/a Ljava/security/Signature;
return
.limit locals 2
.limit stack 2
.end method

.method private a()Ljava/security/Signature;
aload 0
getfield android/support/v4/f/b/f/a Ljava/security/Signature;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()Ljavax/crypto/Cipher;
aload 0
getfield android/support/v4/f/b/f/b Ljavax/crypto/Cipher;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Ljavax/crypto/Mac;
aload 0
getfield android/support/v4/f/b/f/c Ljavax/crypto/Mac;
areturn
.limit locals 1
.limit stack 1
.end method
