.bytecode 50.0
.class public final synchronized android/support/v4/app/dr
.super java/lang/Object

.field private final 'a' Ljava/util/List;

.field private final 'b' Ljava/lang/String;

.field private 'c' Landroid/support/v4/app/fn;

.field private 'd' Landroid/app/PendingIntent;

.field private 'e' Landroid/app/PendingIntent;

.field private 'f' J

.method private <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/dr/a Ljava/util/List;
aload 0
aload 1
putfield android/support/v4/app/dr/b Ljava/lang/String;
return
.limit locals 2
.limit stack 3
.end method

.method private a()Landroid/support/v4/app/dp;
aload 0
getfield android/support/v4/app/dr/a Ljava/util/List;
aload 0
getfield android/support/v4/app/dr/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
anewarray java/lang/String
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Ljava/lang/String;
astore 3
aload 0
getfield android/support/v4/app/dr/b Ljava/lang/String;
astore 4
aload 0
getfield android/support/v4/app/dr/c Landroid/support/v4/app/fn;
astore 5
aload 0
getfield android/support/v4/app/dr/e Landroid/app/PendingIntent;
astore 6
aload 0
getfield android/support/v4/app/dr/d Landroid/app/PendingIntent;
astore 7
aload 0
getfield android/support/v4/app/dr/f J
lstore 1
new android/support/v4/app/dp
dup
aload 3
aload 5
aload 6
aload 7
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 4
aastore
lload 1
invokespecial android/support/v4/app/dp/<init>([Ljava/lang/String;Landroid/support/v4/app/fn;Landroid/app/PendingIntent;Landroid/app/PendingIntent;[Ljava/lang/String;J)V
areturn
.limit locals 8
.limit stack 10
.end method

.method private a(J)Landroid/support/v4/app/dr;
aload 0
lload 1
putfield android/support/v4/app/dr/f J
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/app/PendingIntent;)Landroid/support/v4/app/dr;
aload 0
aload 1
putfield android/support/v4/app/dr/d Landroid/app/PendingIntent;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/app/PendingIntent;Landroid/support/v4/app/fn;)Landroid/support/v4/app/dr;
aload 0
aload 2
putfield android/support/v4/app/dr/c Landroid/support/v4/app/fn;
aload 0
aload 1
putfield android/support/v4/app/dr/e Landroid/app/PendingIntent;
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method private a(Ljava/lang/String;)Landroid/support/v4/app/dr;
aload 0
getfield android/support/v4/app/dr/a Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method
