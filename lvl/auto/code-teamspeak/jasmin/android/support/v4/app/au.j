.bytecode 50.0
.class final synchronized android/support/v4/app/au
.super java/lang/Object

.field static 'a' Ljava/lang/reflect/Method;

.field static 'b' Z = 0


.field private static final 'c' Ljava/lang/String; = "BundleCompatDonut"

.field private static 'd' Ljava/lang/reflect/Method;

.field private static 'e' Z

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/IBinder;
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
.catch java/lang/reflect/InvocationTargetException from L3 to L4 using L5
.catch java/lang/IllegalAccessException from L3 to L4 using L6
.catch java/lang/IllegalArgumentException from L3 to L4 using L7
getstatic android/support/v4/app/au/e Z
ifne L8
L0:
ldc android/os/Bundle
ldc "getIBinder"
iconst_1
anewarray java/lang/Class
dup
iconst_0
ldc java/lang/String
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 2
aload 2
putstatic android/support/v4/app/au/d Ljava/lang/reflect/Method;
aload 2
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
L1:
iconst_1
putstatic android/support/v4/app/au/e Z
L8:
getstatic android/support/v4/app/au/d Ljava/lang/reflect/Method;
ifnull L9
L3:
getstatic android/support/v4/app/au/d Ljava/lang/reflect/Method;
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/os/IBinder
astore 0
L4:
aload 0
areturn
L2:
astore 2
ldc "BundleCompatDonut"
ldc "Failed to retrieve getIBinder method"
aload 2
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L7:
astore 0
L10:
ldc "BundleCompatDonut"
ldc "Failed to invoke getIBinder via reflection"
aload 0
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
putstatic android/support/v4/app/au/d Ljava/lang/reflect/Method;
L9:
aconst_null
areturn
L5:
astore 0
goto L10
L6:
astore 0
goto L10
.limit locals 3
.limit stack 6
.end method

.method private static a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/IBinder;)V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
.catch java/lang/reflect/InvocationTargetException from L3 to L4 using L5
.catch java/lang/IllegalAccessException from L3 to L4 using L6
.catch java/lang/IllegalArgumentException from L3 to L4 using L7
getstatic android/support/v4/app/au/b Z
ifne L8
L0:
ldc android/os/Bundle
ldc "putIBinder"
iconst_2
anewarray java/lang/Class
dup
iconst_0
ldc java/lang/String
aastore
dup
iconst_1
ldc android/os/IBinder
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 3
aload 3
putstatic android/support/v4/app/au/a Ljava/lang/reflect/Method;
aload 3
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
L1:
iconst_1
putstatic android/support/v4/app/au/b Z
L8:
getstatic android/support/v4/app/au/a Ljava/lang/reflect/Method;
ifnull L4
L3:
getstatic android/support/v4/app/au/a Ljava/lang/reflect/Method;
aload 0
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 2
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
return
L2:
astore 3
ldc "BundleCompatDonut"
ldc "Failed to retrieve putIBinder method"
aload 3
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L7:
astore 0
L9:
ldc "BundleCompatDonut"
ldc "Failed to invoke putIBinder via reflection"
aload 0
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
putstatic android/support/v4/app/au/a Ljava/lang/reflect/Method;
return
L5:
astore 0
goto L9
L6:
astore 0
goto L9
.limit locals 4
.limit stack 6
.end method
