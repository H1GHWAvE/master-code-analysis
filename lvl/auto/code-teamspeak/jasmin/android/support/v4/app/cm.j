.bytecode 50.0
.class public synchronized abstract android/support/v4/app/cm
.super android/os/Binder
.implements android/support/v4/app/cl

.field static final 'a' I = 1


.field static final 'b' I = 2


.field static final 'c' I = 3


.field private static final 'd' Ljava/lang/String; = "android.support.v4.app.INotificationSideChannel"

.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "android.support.v4.app.INotificationSideChannel"
invokevirtual android/support/v4/app/cm/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static a(Landroid/os/IBinder;)Landroid/support/v4/app/cl;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "android.support.v4.app.INotificationSideChannel"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof android/support/v4/app/cl
ifeq L1
aload 1
checkcast android/support/v4/app/cl
areturn
L1:
new android/support/v4/app/cn
dup
aload 0
invokespecial android/support/v4/app/cn/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
1598968902 : L3
default : L4
L4:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L3:
aload 3
ldc "android.support.v4.app.INotificationSideChannel"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "android.support.v4.app.INotificationSideChannel"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 3
aload 2
invokevirtual android/os/Parcel/readInt()I
istore 1
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 5
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L5
getstatic android/app/Notification/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/app/Notification
astore 2
L6:
aload 0
aload 3
iload 1
aload 5
aload 2
invokevirtual android/support/v4/app/cm/a(Ljava/lang/String;ILjava/lang/String;Landroid/app/Notification;)V
iconst_1
ireturn
L5:
aconst_null
astore 2
goto L6
L1:
aload 2
ldc "android.support.v4.app.INotificationSideChannel"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readInt()I
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual android/support/v4/app/cm/a(Ljava/lang/String;ILjava/lang/String;)V
iconst_1
ireturn
L2:
aload 2
ldc "android.support.v4.app.INotificationSideChannel"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual android/support/v4/app/cm/a(Ljava/lang/String;)V
iconst_1
ireturn
.limit locals 6
.limit stack 5
.end method
