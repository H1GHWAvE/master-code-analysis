.bytecode 50.0
.class public final synchronized android/support/v4/app/fa
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "android.support.useSideChannel"

.field public static final 'b' Ljava/lang/String; = "android.support.BIND_NOTIFICATION_SIDE_CHANNEL"

.field static final 'c' I = 19


.field private static final 'd' Ljava/lang/String; = "NotifManCompat"

.field private static final 'e' I = 1000


.field private static final 'f' I = 6


.field private static final 'g' Ljava/lang/String; = "enabled_notification_listeners"

.field private static final 'h' I

.field private static final 'i' Ljava/lang/Object;

.field private static 'j' Ljava/lang/String;

.field private static 'k' Ljava/util/Set;

.field private static final 'n' Ljava/lang/Object;

.field private static 'o' Landroid/support/v4/app/fi;

.field private static final 'p' Landroid/support/v4/app/fc;

.field private final 'l' Landroid/content/Context;

.field private final 'm' Landroid/app/NotificationManager;

.method static <clinit>()V
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putstatic android/support/v4/app/fa/i Ljava/lang/Object;
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
putstatic android/support/v4/app/fa/k Ljava/util/Set;
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putstatic android/support/v4/app/fa/n Ljava/lang/Object;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
new android/support/v4/app/ff
dup
invokespecial android/support/v4/app/ff/<init>()V
putstatic android/support/v4/app/fa/p Landroid/support/v4/app/fc;
L1:
getstatic android/support/v4/app/fa/p Landroid/support/v4/app/fc;
invokeinterface android/support/v4/app/fc/a()I 0
putstatic android/support/v4/app/fa/h I
return
L0:
getstatic android/os/Build$VERSION/SDK_INT I
iconst_5
if_icmplt L2
new android/support/v4/app/fe
dup
invokespecial android/support/v4/app/fe/<init>()V
putstatic android/support/v4/app/fa/p Landroid/support/v4/app/fc;
goto L1
L2:
new android/support/v4/app/fd
dup
invokespecial android/support/v4/app/fd/<init>()V
putstatic android/support/v4/app/fa/p Landroid/support/v4/app/fc;
goto L1
.limit locals 0
.limit stack 2
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/app/fa/l Landroid/content/Context;
aload 0
aload 0
getfield android/support/v4/app/fa/l Landroid/content/Context;
ldc "notification"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
putfield android/support/v4/app/fa/m Landroid/app/NotificationManager;
return
.limit locals 2
.limit stack 3
.end method

.method static synthetic a()I
getstatic android/support/v4/app/fa/h I
ireturn
.limit locals 0
.limit stack 1
.end method

.method public static a(Landroid/content/Context;)Ljava/util/Set;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
ldc "enabled_notification_listeners"
invokestatic android/provider/Settings$Secure/getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
astore 0
aload 0
ifnull L1
aload 0
getstatic android/support/v4/app/fa/j Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
aload 0
ldc ":"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 4
new java/util/HashSet
dup
aload 4
arraylength
invokespecial java/util/HashSet/<init>(I)V
astore 3
aload 4
arraylength
istore 2
iconst_0
istore 1
L5:
iload 1
iload 2
if_icmpge L6
aload 4
iload 1
aaload
invokestatic android/content/ComponentName/unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;
astore 5
aload 5
ifnull L7
aload 3
aload 5
invokevirtual android/content/ComponentName/getPackageName()Ljava/lang/String;
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
L7:
iload 1
iconst_1
iadd
istore 1
goto L5
L6:
getstatic android/support/v4/app/fa/i Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
aload 3
putstatic android/support/v4/app/fa/k Ljava/util/Set;
aload 0
putstatic android/support/v4/app/fa/j Ljava/lang/String;
aload 4
monitorexit
L1:
getstatic android/support/v4/app/fa/k Ljava/util/Set;
areturn
L2:
astore 0
L3:
aload 4
monitorexit
L4:
aload 0
athrow
.limit locals 6
.limit stack 3
.end method

.method private a(I)V
getstatic android/support/v4/app/fa/p Landroid/support/v4/app/fc;
aload 0
getfield android/support/v4/app/fa/m Landroid/app/NotificationManager;
iload 1
invokeinterface android/support/v4/app/fc/a(Landroid/app/NotificationManager;I)V 2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmpgt L0
aload 0
new android/support/v4/app/fb
dup
aload 0
getfield android/support/v4/app/fa/l Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
iload 1
invokespecial android/support/v4/app/fb/<init>(Ljava/lang/String;I)V
invokespecial android/support/v4/app/fa/a(Landroid/support/v4/app/fk;)V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method private a(ILandroid/app/Notification;)V
aload 2
invokestatic android/support/v4/app/dd/a(Landroid/app/Notification;)Landroid/os/Bundle;
astore 4
aload 4
ifnull L0
aload 4
ldc "android.support.useSideChannel"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
ifeq L0
iconst_1
istore 3
L1:
iload 3
ifeq L2
aload 0
new android/support/v4/app/fg
dup
aload 0
getfield android/support/v4/app/fa/l Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
iload 1
aload 2
invokespecial android/support/v4/app/fg/<init>(Ljava/lang/String;ILandroid/app/Notification;)V
invokespecial android/support/v4/app/fa/a(Landroid/support/v4/app/fk;)V
getstatic android/support/v4/app/fa/p Landroid/support/v4/app/fc;
aload 0
getfield android/support/v4/app/fa/m Landroid/app/NotificationManager;
iload 1
invokeinterface android/support/v4/app/fc/a(Landroid/app/NotificationManager;I)V 2
return
L0:
iconst_0
istore 3
goto L1
L2:
getstatic android/support/v4/app/fa/p Landroid/support/v4/app/fc;
aload 0
getfield android/support/v4/app/fa/m Landroid/app/NotificationManager;
iload 1
aload 2
invokeinterface android/support/v4/app/fc/a(Landroid/app/NotificationManager;ILandroid/app/Notification;)V 3
return
.limit locals 5
.limit stack 6
.end method

.method private a(Landroid/support/v4/app/fk;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
getstatic android/support/v4/app/fa/n Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
getstatic android/support/v4/app/fa/o Landroid/support/v4/app/fi;
ifnonnull L1
new android/support/v4/app/fi
dup
aload 0
getfield android/support/v4/app/fa/l Landroid/content/Context;
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
invokespecial android/support/v4/app/fi/<init>(Landroid/content/Context;)V
putstatic android/support/v4/app/fa/o Landroid/support/v4/app/fi;
L1:
aload 2
monitorexit
L3:
getstatic android/support/v4/app/fa/o Landroid/support/v4/app/fi;
getfield android/support/v4/app/fi/a Landroid/os/Handler;
iconst_0
aload 1
invokevirtual android/os/Handler/obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
invokevirtual android/os/Message/sendToTarget()V
return
L2:
astore 1
L4:
aload 2
monitorexit
L5:
aload 1
athrow
.limit locals 3
.limit stack 3
.end method

.method private static a(Landroid/app/Notification;)Z
aload 0
invokestatic android/support/v4/app/dd/a(Landroid/app/Notification;)Landroid/os/Bundle;
astore 0
aload 0
ifnull L0
aload 0
ldc "android.support.useSideChannel"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static b(Landroid/content/Context;)Landroid/support/v4/app/fa;
new android/support/v4/app/fa
dup
aload 0
invokespecial android/support/v4/app/fa/<init>(Landroid/content/Context;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private b()V
aload 0
getfield android/support/v4/app/fa/m Landroid/app/NotificationManager;
invokevirtual android/app/NotificationManager/cancelAll()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmpgt L0
aload 0
new android/support/v4/app/fb
dup
aload 0
getfield android/support/v4/app/fa/l Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokespecial android/support/v4/app/fb/<init>(Ljava/lang/String;)V
invokespecial android/support/v4/app/fa/a(Landroid/support/v4/app/fk;)V
L0:
return
.limit locals 1
.limit stack 4
.end method

.method private b(I)V
getstatic android/support/v4/app/fa/p Landroid/support/v4/app/fc;
aload 0
getfield android/support/v4/app/fa/m Landroid/app/NotificationManager;
iload 1
invokeinterface android/support/v4/app/fc/a(Landroid/app/NotificationManager;I)V 2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmpgt L0
aload 0
new android/support/v4/app/fb
dup
aload 0
getfield android/support/v4/app/fa/l Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
iload 1
invokespecial android/support/v4/app/fb/<init>(Ljava/lang/String;I)V
invokespecial android/support/v4/app/fa/a(Landroid/support/v4/app/fk;)V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method private b(ILandroid/app/Notification;)V
aload 2
invokestatic android/support/v4/app/dd/a(Landroid/app/Notification;)Landroid/os/Bundle;
astore 4
aload 4
ifnull L0
aload 4
ldc "android.support.useSideChannel"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
ifeq L0
iconst_1
istore 3
L1:
iload 3
ifeq L2
aload 0
new android/support/v4/app/fg
dup
aload 0
getfield android/support/v4/app/fa/l Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
iload 1
aload 2
invokespecial android/support/v4/app/fg/<init>(Ljava/lang/String;ILandroid/app/Notification;)V
invokespecial android/support/v4/app/fa/a(Landroid/support/v4/app/fk;)V
getstatic android/support/v4/app/fa/p Landroid/support/v4/app/fc;
aload 0
getfield android/support/v4/app/fa/m Landroid/app/NotificationManager;
iload 1
invokeinterface android/support/v4/app/fc/a(Landroid/app/NotificationManager;I)V 2
return
L0:
iconst_0
istore 3
goto L1
L2:
getstatic android/support/v4/app/fa/p Landroid/support/v4/app/fc;
aload 0
getfield android/support/v4/app/fa/m Landroid/app/NotificationManager;
iload 1
aload 2
invokeinterface android/support/v4/app/fc/a(Landroid/app/NotificationManager;ILandroid/app/Notification;)V 3
return
.limit locals 5
.limit stack 6
.end method
