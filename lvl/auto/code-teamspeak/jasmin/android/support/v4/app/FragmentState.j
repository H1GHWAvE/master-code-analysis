.bytecode 50.0
.class final synchronized android/support/v4/app/FragmentState
.super java/lang/Object
.implements android/os/Parcelable

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field final 'a' Ljava/lang/String;

.field final 'b' I

.field final 'c' Z

.field final 'd' I

.field final 'e' I

.field final 'f' Ljava/lang/String;

.field final 'g' Z

.field final 'h' Z

.field final 'i' Landroid/os/Bundle;

.field 'j' Landroid/os/Bundle;

.field 'k' Landroid/support/v4/app/Fragment;

.method static <clinit>()V
new android/support/v4/app/bx
dup
invokespecial android/support/v4/app/bx/<init>()V
putstatic android/support/v4/app/FragmentState/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/os/Parcel;)V
iconst_1
istore 3
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
putfield android/support/v4/app/FragmentState/a Ljava/lang/String;
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/app/FragmentState/b I
aload 1
invokevirtual android/os/Parcel/readInt()I
ifeq L0
iconst_1
istore 2
L1:
aload 0
iload 2
putfield android/support/v4/app/FragmentState/c Z
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/app/FragmentState/d I
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/app/FragmentState/e I
aload 0
aload 1
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
putfield android/support/v4/app/FragmentState/f Ljava/lang/String;
aload 1
invokevirtual android/os/Parcel/readInt()I
ifeq L2
iconst_1
istore 2
L3:
aload 0
iload 2
putfield android/support/v4/app/FragmentState/g Z
aload 1
invokevirtual android/os/Parcel/readInt()I
ifeq L4
iload 3
istore 2
L5:
aload 0
iload 2
putfield android/support/v4/app/FragmentState/h Z
aload 0
aload 1
invokevirtual android/os/Parcel/readBundle()Landroid/os/Bundle;
putfield android/support/v4/app/FragmentState/i Landroid/os/Bundle;
aload 0
aload 1
invokevirtual android/os/Parcel/readBundle()Landroid/os/Bundle;
putfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
return
L0:
iconst_0
istore 2
goto L1
L2:
iconst_0
istore 2
goto L3
L4:
iconst_0
istore 2
goto L5
.limit locals 4
.limit stack 2
.end method

.method public <init>(Landroid/support/v4/app/Fragment;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
putfield android/support/v4/app/FragmentState/a Ljava/lang/String;
aload 0
aload 1
getfield android/support/v4/app/Fragment/z I
putfield android/support/v4/app/FragmentState/b I
aload 0
aload 1
getfield android/support/v4/app/Fragment/I Z
putfield android/support/v4/app/FragmentState/c Z
aload 0
aload 1
getfield android/support/v4/app/Fragment/Q I
putfield android/support/v4/app/FragmentState/d I
aload 0
aload 1
getfield android/support/v4/app/Fragment/R I
putfield android/support/v4/app/FragmentState/e I
aload 0
aload 1
getfield android/support/v4/app/Fragment/S Ljava/lang/String;
putfield android/support/v4/app/FragmentState/f Ljava/lang/String;
aload 0
aload 1
getfield android/support/v4/app/Fragment/V Z
putfield android/support/v4/app/FragmentState/g Z
aload 0
aload 1
getfield android/support/v4/app/Fragment/U Z
putfield android/support/v4/app/FragmentState/h Z
aload 0
aload 1
getfield android/support/v4/app/Fragment/B Landroid/os/Bundle;
putfield android/support/v4/app/FragmentState/i Landroid/os/Bundle;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/app/bh;Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
ifnull L0
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
areturn
L0:
aload 1
getfield android/support/v4/app/bh/c Landroid/content/Context;
astore 3
aload 0
getfield android/support/v4/app/FragmentState/i Landroid/os/Bundle;
ifnull L1
aload 0
getfield android/support/v4/app/FragmentState/i Landroid/os/Bundle;
aload 3
invokevirtual android/content/Context/getClassLoader()Ljava/lang/ClassLoader;
invokevirtual android/os/Bundle/setClassLoader(Ljava/lang/ClassLoader;)V
L1:
aload 0
aload 3
aload 0
getfield android/support/v4/app/FragmentState/a Ljava/lang/String;
aload 0
getfield android/support/v4/app/FragmentState/i Landroid/os/Bundle;
invokestatic android/support/v4/app/Fragment/a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;
putfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
ifnull L2
aload 0
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
aload 3
invokevirtual android/content/Context/getClassLoader()Ljava/lang/ClassLoader;
invokevirtual android/os/Bundle/setClassLoader(Ljava/lang/ClassLoader;)V
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
putfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
L2:
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/FragmentState/b I
aload 2
invokevirtual android/support/v4/app/Fragment/a(ILandroid/support/v4/app/Fragment;)V
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/FragmentState/c Z
putfield android/support/v4/app/Fragment/I Z
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
iconst_1
putfield android/support/v4/app/Fragment/K Z
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/FragmentState/d I
putfield android/support/v4/app/Fragment/Q I
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/FragmentState/e I
putfield android/support/v4/app/Fragment/R I
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/FragmentState/f Ljava/lang/String;
putfield android/support/v4/app/Fragment/S Ljava/lang/String;
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/FragmentState/g Z
putfield android/support/v4/app/Fragment/V Z
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/FragmentState/h Z
putfield android/support/v4/app/Fragment/U Z
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 1
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
putfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
getstatic android/support/v4/app/bl/b Z
ifeq L3
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Instantiated fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L3:
aload 0
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final describeContents()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
iconst_1
istore 3
aload 1
aload 0
getfield android/support/v4/app/FragmentState/a Ljava/lang/String;
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 1
aload 0
getfield android/support/v4/app/FragmentState/b I
invokevirtual android/os/Parcel/writeInt(I)V
aload 0
getfield android/support/v4/app/FragmentState/c Z
ifeq L0
iconst_1
istore 2
L1:
aload 1
iload 2
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/app/FragmentState/d I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/app/FragmentState/e I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/app/FragmentState/f Ljava/lang/String;
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/FragmentState/g Z
ifeq L2
iconst_1
istore 2
L3:
aload 1
iload 2
invokevirtual android/os/Parcel/writeInt(I)V
aload 0
getfield android/support/v4/app/FragmentState/h Z
ifeq L4
iload 3
istore 2
L5:
aload 1
iload 2
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/app/FragmentState/i Landroid/os/Bundle;
invokevirtual android/os/Parcel/writeBundle(Landroid/os/Bundle;)V
aload 1
aload 0
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
invokevirtual android/os/Parcel/writeBundle(Landroid/os/Bundle;)V
return
L0:
iconst_0
istore 2
goto L1
L2:
iconst_0
istore 2
goto L3
L4:
iconst_0
istore 2
goto L5
.limit locals 4
.limit stack 2
.end method
