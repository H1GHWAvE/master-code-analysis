.bytecode 50.0
.class public synchronized android/support/v4/app/aa
.super java/lang/Object

.method protected <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/support/v4/app/aa;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
new android/support/v4/app/ab
dup
new android/support/v4/app/ad
dup
aload 0
aload 1
aload 2
invokestatic android/app/ActivityOptions/makeSceneTransitionAnimation(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/app/ActivityOptions;
invokespecial android/support/v4/app/ad/<init>(Landroid/app/ActivityOptions;)V
invokespecial android/support/v4/app/ab/<init>(Landroid/support/v4/app/ad;)V
areturn
L0:
new android/support/v4/app/aa
dup
invokespecial android/support/v4/app/aa/<init>()V
areturn
.limit locals 3
.limit stack 7
.end method

.method private static transient a(Landroid/app/Activity;[Landroid/support/v4/n/q;)Landroid/support/v4/app/aa;
aconst_null
astore 3
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 1
ifnull L1
aload 1
arraylength
anewarray android/view/View
astore 3
aload 1
arraylength
anewarray java/lang/String
astore 4
iconst_0
istore 2
L2:
iload 2
aload 1
arraylength
if_icmpge L3
aload 3
iload 2
aload 1
iload 2
aaload
getfield android/support/v4/n/q/a Ljava/lang/Object;
checkcast android/view/View
aastore
aload 4
iload 2
aload 1
iload 2
aaload
getfield android/support/v4/n/q/b Ljava/lang/Object;
checkcast java/lang/String
aastore
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aload 4
astore 1
L4:
new android/support/v4/app/ab
dup
aload 0
aload 3
aload 1
invokestatic android/support/v4/app/ad/a(Landroid/app/Activity;[Landroid/view/View;[Ljava/lang/String;)Landroid/support/v4/app/ad;
invokespecial android/support/v4/app/ab/<init>(Landroid/support/v4/app/ad;)V
areturn
L0:
new android/support/v4/app/aa
dup
invokespecial android/support/v4/app/aa/<init>()V
areturn
L1:
aconst_null
astore 4
aload 3
astore 1
aload 4
astore 3
goto L4
.limit locals 5
.limit stack 5
.end method

.method private static a(Landroid/content/Context;II)Landroid/support/v4/app/aa;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
new android/support/v4/app/ac
dup
new android/support/v4/app/ae
dup
aload 0
iload 1
iload 2
invokestatic android/app/ActivityOptions/makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;
invokespecial android/support/v4/app/ae/<init>(Landroid/app/ActivityOptions;)V
invokespecial android/support/v4/app/ac/<init>(Landroid/support/v4/app/ae;)V
areturn
L0:
new android/support/v4/app/aa
dup
invokespecial android/support/v4/app/aa/<init>()V
areturn
.limit locals 3
.limit stack 7
.end method

.method private static a(Landroid/view/View;IIII)Landroid/support/v4/app/aa;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
new android/support/v4/app/ac
dup
new android/support/v4/app/ae
dup
aload 0
iload 1
iload 2
iload 3
iload 4
invokestatic android/app/ActivityOptions/makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/app/ActivityOptions;
invokespecial android/support/v4/app/ae/<init>(Landroid/app/ActivityOptions;)V
invokespecial android/support/v4/app/ac/<init>(Landroid/support/v4/app/ae;)V
areturn
L0:
new android/support/v4/app/aa
dup
invokespecial android/support/v4/app/aa/<init>()V
areturn
.limit locals 5
.limit stack 9
.end method

.method private static a(Landroid/view/View;Landroid/graphics/Bitmap;II)Landroid/support/v4/app/aa;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
new android/support/v4/app/ac
dup
new android/support/v4/app/ae
dup
aload 0
aload 1
iload 2
iload 3
invokestatic android/app/ActivityOptions/makeThumbnailScaleUpAnimation(Landroid/view/View;Landroid/graphics/Bitmap;II)Landroid/app/ActivityOptions;
invokespecial android/support/v4/app/ae/<init>(Landroid/app/ActivityOptions;)V
invokespecial android/support/v4/app/ac/<init>(Landroid/support/v4/app/ae;)V
areturn
L0:
new android/support/v4/app/aa
dup
invokespecial android/support/v4/app/aa/<init>()V
areturn
.limit locals 4
.limit stack 8
.end method

.method public a()Landroid/os/Bundle;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public a(Landroid/support/v4/app/aa;)V
return
.limit locals 2
.limit stack 0
.end method
