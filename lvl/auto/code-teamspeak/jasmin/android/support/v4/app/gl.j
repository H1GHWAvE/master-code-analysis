.bytecode 50.0
.class public final synchronized android/support/v4/app/gl
.super java/lang/Object
.implements java/lang/Iterable

.field private static final 'c' Ljava/lang/String; = "TaskStackBuilder"

.field private static final 'd' Landroid/support/v4/app/gn;

.field public final 'a' Ljava/util/ArrayList;

.field public final 'b' Landroid/content/Context;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
new android/support/v4/app/gp
dup
invokespecial android/support/v4/app/gp/<init>()V
putstatic android/support/v4/app/gl/d Landroid/support/v4/app/gn;
return
L0:
new android/support/v4/app/go
dup
invokespecial android/support/v4/app/go/<init>()V
putstatic android/support/v4/app/gl/d Landroid/support/v4/app/gn;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/gl/a Ljava/util/ArrayList;
aload 0
aload 1
putfield android/support/v4/app/gl/b Landroid/content/Context;
return
.limit locals 2
.limit stack 3
.end method

.method private a()I
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(II)Landroid/app/PendingIntent;
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "No intents added to TaskStackBuilder; cannot getPendingIntent"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
anewarray android/content/Intent
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Landroid/content/Intent;
astore 3
aload 3
iconst_0
new android/content/Intent
dup
aload 3
iconst_0
aaload
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
ldc_w 268484608
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
aastore
getstatic android/support/v4/app/gl/d Landroid/support/v4/app/gn;
aload 0
getfield android/support/v4/app/gl/b Landroid/content/Context;
aload 3
iload 1
iload 2
invokeinterface android/support/v4/app/gn/a(Landroid/content/Context;[Landroid/content/Intent;II)Landroid/app/PendingIntent; 4
areturn
.limit locals 4
.limit stack 6
.end method

.method private a(I)Landroid/content/Intent;
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/content/Intent
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/app/Activity;)Landroid/support/v4/app/gl;
aconst_null
astore 2
aload 1
instanceof android/support/v4/app/gm
ifeq L0
aload 1
checkcast android/support/v4/app/gm
invokeinterface android/support/v4/app/gm/a_()Landroid/content/Intent; 0
astore 2
L0:
aload 2
ifnonnull L1
aload 1
invokestatic android/support/v4/app/cv/a(Landroid/app/Activity;)Landroid/content/Intent;
astore 1
L2:
aload 1
ifnull L3
aload 1
invokevirtual android/content/Intent/getComponent()Landroid/content/ComponentName;
astore 3
aload 3
astore 2
aload 3
ifnonnull L4
aload 1
aload 0
getfield android/support/v4/app/gl/b Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
invokevirtual android/content/Intent/resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;
astore 2
L4:
aload 0
aload 2
invokevirtual android/support/v4/app/gl/a(Landroid/content/ComponentName;)Landroid/support/v4/app/gl;
pop
aload 0
aload 1
invokevirtual android/support/v4/app/gl/a(Landroid/content/Intent;)Landroid/support/v4/app/gl;
pop
L3:
aload 0
areturn
L1:
aload 2
astore 1
goto L2
.limit locals 4
.limit stack 2
.end method

.method public static a(Landroid/content/Context;)Landroid/support/v4/app/gl;
new android/support/v4/app/gl
dup
aload 0
invokespecial android/support/v4/app/gl/<init>(Landroid/content/Context;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Ljava/lang/Class;)Landroid/support/v4/app/gl;
aload 0
new android/content/ComponentName
dup
aload 0
getfield android/support/v4/app/gl/b Landroid/content/Context;
aload 1
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/Class;)V
invokevirtual android/support/v4/app/gl/a(Landroid/content/ComponentName;)Landroid/support/v4/app/gl;
areturn
.limit locals 2
.limit stack 5
.end method

.method private b(II)Landroid/app/PendingIntent;
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "No intents added to TaskStackBuilder; cannot getPendingIntent"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
anewarray android/content/Intent
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Landroid/content/Intent;
astore 3
aload 3
iconst_0
new android/content/Intent
dup
aload 3
iconst_0
aaload
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
ldc_w 268484608
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
aastore
getstatic android/support/v4/app/gl/d Landroid/support/v4/app/gn;
aload 0
getfield android/support/v4/app/gl/b Landroid/content/Context;
aload 3
iload 1
iload 2
invokeinterface android/support/v4/app/gn/a(Landroid/content/Context;[Landroid/content/Intent;II)Landroid/app/PendingIntent; 4
areturn
.limit locals 4
.limit stack 6
.end method

.method private b(I)Landroid/content/Intent;
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/content/Intent
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/content/Context;)Landroid/support/v4/app/gl;
aload 0
invokestatic android/support/v4/app/gl/a(Landroid/content/Context;)Landroid/support/v4/app/gl;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Landroid/content/Intent;)Landroid/support/v4/app/gl;
aload 1
invokevirtual android/content/Intent/getComponent()Landroid/content/ComponentName;
astore 3
aload 3
astore 2
aload 3
ifnonnull L0
aload 1
aload 0
getfield android/support/v4/app/gl/b Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
invokevirtual android/content/Intent/resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;
astore 2
L0:
aload 2
ifnull L1
aload 0
aload 2
invokevirtual android/support/v4/app/gl/a(Landroid/content/ComponentName;)Landroid/support/v4/app/gl;
pop
L1:
aload 0
aload 1
invokevirtual android/support/v4/app/gl/a(Landroid/content/Intent;)Landroid/support/v4/app/gl;
pop
aload 0
areturn
.limit locals 4
.limit stack 2
.end method

.method private b()V
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "No intents added to TaskStackBuilder; cannot startActivities"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
anewarray android/content/Intent
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Landroid/content/Intent;
astore 1
aload 1
iconst_0
new android/content/Intent
dup
aload 1
iconst_0
aaload
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
ldc_w 268484608
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
aastore
aload 0
getfield android/support/v4/app/gl/b Landroid/content/Context;
aload 1
invokestatic android/support/v4/c/h/a(Landroid/content/Context;[Landroid/content/Intent;)Z
ifne L1
new android/content/Intent
dup
aload 1
aload 1
arraylength
iconst_1
isub
aaload
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
astore 1
aload 1
ldc_w 268435456
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 0
getfield android/support/v4/app/gl/b Landroid/content/Context;
aload 1
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L1:
return
.limit locals 2
.limit stack 6
.end method

.method private c()V
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "No intents added to TaskStackBuilder; cannot startActivities"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
anewarray android/content/Intent
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Landroid/content/Intent;
astore 1
aload 1
iconst_0
new android/content/Intent
dup
aload 1
iconst_0
aaload
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
ldc_w 268484608
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
aastore
aload 0
getfield android/support/v4/app/gl/b Landroid/content/Context;
aload 1
invokestatic android/support/v4/c/h/a(Landroid/content/Context;[Landroid/content/Intent;)Z
ifne L1
new android/content/Intent
dup
aload 1
aload 1
arraylength
iconst_1
isub
aaload
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
astore 1
aload 1
ldc_w 268435456
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 0
getfield android/support/v4/app/gl/b Landroid/content/Context;
aload 1
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L1:
return
.limit locals 2
.limit stack 6
.end method

.method private d()[Landroid/content/Intent;
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
anewarray android/content/Intent
astore 2
aload 2
arraylength
ifne L0
aload 2
areturn
L0:
aload 2
iconst_0
new android/content/Intent
dup
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/content/Intent
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
ldc_w 268484608
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
aastore
iconst_1
istore 1
L1:
iload 1
aload 2
arraylength
if_icmpge L2
aload 2
iload 1
new android/content/Intent
dup
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/content/Intent
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
aastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 2
areturn
.limit locals 3
.limit stack 6
.end method

.method public final a(Landroid/content/ComponentName;)Landroid/support/v4/app/gl;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch android/content/pm/PackageManager$NameNotFoundException from L3 to L4 using L2
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 2
L0:
aload 0
getfield android/support/v4/app/gl/b Landroid/content/Context;
aload 1
invokestatic android/support/v4/app/cv/a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Intent;
astore 1
L1:
aload 1
ifnull L5
L3:
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
iload 2
aload 1
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
aload 0
getfield android/support/v4/app/gl/b Landroid/content/Context;
aload 1
invokevirtual android/content/Intent/getComponent()Landroid/content/ComponentName;
invokestatic android/support/v4/app/cv/a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Intent;
astore 1
L4:
goto L1
L2:
astore 1
ldc "TaskStackBuilder"
ldc "Bad ComponentName while traversing activity parent metadata"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/lang/IllegalArgumentException
dup
aload 1
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/Throwable;)V
athrow
L5:
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/gl;
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final iterator()Ljava/util/Iterator;
aload 0
getfield android/support/v4/app/gl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method
