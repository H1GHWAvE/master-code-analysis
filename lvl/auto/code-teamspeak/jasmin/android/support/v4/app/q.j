.bytecode 50.0
.class final synchronized android/support/v4/app/q
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;
aconst_null
astore 1
aload 0
ifnull L0
new android/support/v4/app/s
dup
aload 0
invokespecial android/support/v4/app/s/<init>(Landroid/support/v4/app/r;)V
astore 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Landroid/app/Activity;)V
aload 0
invokevirtual android/app/Activity/finishAfterTransition()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/app/Activity;Landroid/support/v4/app/r;)V
aload 0
aload 1
invokestatic android/support/v4/app/q/a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;
invokevirtual android/app/Activity/setEnterSharedElementCallback(Landroid/app/SharedElementCallback;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/app/Activity;)V
aload 0
invokevirtual android/app/Activity/postponeEnterTransition()V
return
.limit locals 1
.limit stack 1
.end method

.method private static b(Landroid/app/Activity;Landroid/support/v4/app/r;)V
aload 0
aload 1
invokestatic android/support/v4/app/q/a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;
invokevirtual android/app/Activity/setExitSharedElementCallback(Landroid/app/SharedElementCallback;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Landroid/app/Activity;)V
aload 0
invokevirtual android/app/Activity/startPostponedEnterTransition()V
return
.limit locals 1
.limit stack 1
.end method
