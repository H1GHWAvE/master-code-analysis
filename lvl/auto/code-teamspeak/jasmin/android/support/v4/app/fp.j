.bytecode 50.0
.class public final synchronized android/support/v4/app/fp
.super java/lang/Object

.field private final 'a' Ljava/lang/String;

.field private 'b' Ljava/lang/CharSequence;

.field private 'c' [Ljava/lang/CharSequence;

.field private 'd' Z

.field private 'e' Landroid/os/Bundle;

.method private <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield android/support/v4/app/fp/d Z
aload 0
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
putfield android/support/v4/app/fp/e Landroid/os/Bundle;
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "Result key can't be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield android/support/v4/app/fp/a Ljava/lang/String;
return
.limit locals 2
.limit stack 3
.end method

.method private a()Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/fp/e Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/os/Bundle;)Landroid/support/v4/app/fp;
aload 1
ifnull L0
aload 0
getfield android/support/v4/app/fp/e Landroid/os/Bundle;
aload 1
invokevirtual android/os/Bundle/putAll(Landroid/os/Bundle;)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/v4/app/fp;
aload 0
aload 1
putfield android/support/v4/app/fp/b Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Z)Landroid/support/v4/app/fp;
aload 0
iload 1
putfield android/support/v4/app/fp/d Z
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a([Ljava/lang/CharSequence;)Landroid/support/v4/app/fp;
aload 0
aload 1
putfield android/support/v4/app/fp/c [Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b()Landroid/support/v4/app/fn;
new android/support/v4/app/fn
dup
aload 0
getfield android/support/v4/app/fp/a Ljava/lang/String;
aload 0
getfield android/support/v4/app/fp/b Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/app/fp/c [Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/app/fp/d Z
aload 0
getfield android/support/v4/app/fp/e Landroid/os/Bundle;
invokespecial android/support/v4/app/fn/<init>(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)V
areturn
.limit locals 1
.limit stack 7
.end method
