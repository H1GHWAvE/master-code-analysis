.bytecode 50.0
.class final synchronized android/support/v4/app/bd
.super android/support/v4/app/bh

.field final synthetic 'a' Landroid/support/v4/app/bb;

.method public <init>(Landroid/support/v4/app/bb;)V
aload 0
aload 1
putfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
aload 0
aload 1
invokespecial android/support/v4/app/bh/<init>(Landroid/support/v4/app/bb;)V
return
.limit locals 2
.limit stack 2
.end method

.method private j()Landroid/support/v4/app/bb;
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(I)Landroid/view/View;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
iload 1
invokevirtual android/support/v4/app/bb/findViewById(I)Landroid/view/View;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
aload 1
aload 2
iload 3
invokevirtual android/support/v4/app/bb/a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
return
.limit locals 4
.limit stack 4
.end method

.method public final a(Landroid/support/v4/app/Fragment;[Ljava/lang/String;I)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
aload 1
aload 2
iload 3
invokestatic android/support/v4/app/bb/a(Landroid/support/v4/app/bb;Landroid/support/v4/app/Fragment;[Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 4
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;[Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
aload 1
aconst_null
aload 2
aload 3
invokevirtual android/support/v4/app/bb/dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
return
.limit locals 4
.limit stack 5
.end method

.method public final a()Z
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getWindow()Landroid/view/Window;
astore 1
aload 1
ifnull L0
aload 1
invokevirtual android/view/Window/peekDecorView()Landroid/view/View;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Ljava/lang/String;)Z
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
astore 2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmplt L0
aload 2
aload 1
invokevirtual android/app/Activity/shouldShowRequestPermissionRationale(Ljava/lang/String;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final b()Z
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/isFinishing()Z
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Landroid/view/LayoutInflater;
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getLayoutInflater()Landroid/view/LayoutInflater;
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
invokevirtual android/view/LayoutInflater/cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final d()V
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/b()V
return
.limit locals 1
.limit stack 1
.end method

.method public final e()Z
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getWindow()Landroid/view/Window;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final f()I
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getWindow()Landroid/view/Window;
astore 1
aload 1
ifnonnull L0
iconst_0
ireturn
L0:
aload 1
invokevirtual android/view/Window/getAttributes()Landroid/view/WindowManager$LayoutParams;
getfield android/view/WindowManager$LayoutParams/windowAnimations I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final g()V
return
.limit locals 1
.limit stack 0
.end method

.method public final volatile synthetic h()Ljava/lang/Object;
aload 0
getfield android/support/v4/app/bd/a Landroid/support/v4/app/bb;
areturn
.limit locals 1
.limit stack 1
.end method
