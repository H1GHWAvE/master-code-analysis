.bytecode 50.0
.class final synchronized android/support/v4/app/bl
.super android/support/v4/app/bi
.implements android/support/v4/view/as

.field static final 'G' Landroid/view/animation/Interpolator;

.field static final 'H' Landroid/view/animation/Interpolator;

.field static final 'I' Landroid/view/animation/Interpolator;

.field static final 'J' Landroid/view/animation/Interpolator;

.field static final 'K' I = 220


.field public static final 'L' I = 1


.field public static final 'M' I = 2


.field public static final 'N' I = 3


.field public static final 'O' I = 4


.field public static final 'P' I = 5


.field public static final 'Q' I = 6


.field static 'b' Z = 0


.field static final 'c' Ljava/lang/String; = "FragmentManager"

.field static final 'd' Z

.field static final 'e' Ljava/lang/String; = "android:target_req_state"

.field static final 'f' Ljava/lang/String; = "android:target_state"

.field static final 'g' Ljava/lang/String; = "android:view_state"

.field static final 'h' Ljava/lang/String; = "android:user_visible_hint"

.field 'A' Z

.field 'B' Ljava/lang/String;

.field 'C' Z

.field 'D' Landroid/os/Bundle;

.field 'E' Landroid/util/SparseArray;

.field 'F' Ljava/lang/Runnable;

.field 'i' Ljava/util/ArrayList;

.field 'j' [Ljava/lang/Runnable;

.field 'k' Z

.field 'l' Ljava/util/ArrayList;

.field 'm' Ljava/util/ArrayList;

.field 'n' Ljava/util/ArrayList;

.field 'o' Ljava/util/ArrayList;

.field 'p' Ljava/util/ArrayList;

.field 'q' Ljava/util/ArrayList;

.field 'r' Ljava/util/ArrayList;

.field 's' Ljava/util/ArrayList;

.field 't' I

.field 'u' Landroid/support/v4/app/bh;

.field 'v' Landroid/support/v4/app/bg;

.field 'w' Landroid/support/v4/app/bf;

.field 'x' Landroid/support/v4/app/Fragment;

.field 'y' Z

.field 'z' Z

.method static <clinit>()V
iconst_0
istore 0
iconst_0
putstatic android/support/v4/app/bl/b Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
iconst_1
istore 0
L0:
iload 0
putstatic android/support/v4/app/bl/d Z
new android/view/animation/DecelerateInterpolator
dup
ldc_w 2.5F
invokespecial android/view/animation/DecelerateInterpolator/<init>(F)V
putstatic android/support/v4/app/bl/G Landroid/view/animation/Interpolator;
new android/view/animation/DecelerateInterpolator
dup
ldc_w 1.5F
invokespecial android/view/animation/DecelerateInterpolator/<init>(F)V
putstatic android/support/v4/app/bl/H Landroid/view/animation/Interpolator;
new android/view/animation/AccelerateInterpolator
dup
ldc_w 2.5F
invokespecial android/view/animation/AccelerateInterpolator/<init>(F)V
putstatic android/support/v4/app/bl/I Landroid/view/animation/Interpolator;
new android/view/animation/AccelerateInterpolator
dup
ldc_w 1.5F
invokespecial android/view/animation/AccelerateInterpolator/<init>(F)V
putstatic android/support/v4/app/bl/J Landroid/view/animation/Interpolator;
return
.limit locals 1
.limit stack 3
.end method

.method <init>()V
aload 0
invokespecial android/support/v4/app/bi/<init>()V
aload 0
iconst_0
putfield android/support/v4/app/bl/t I
aload 0
aconst_null
putfield android/support/v4/app/bl/D Landroid/os/Bundle;
aload 0
aconst_null
putfield android/support/v4/app/bl/E Landroid/util/SparseArray;
aload 0
new android/support/v4/app/bm
dup
aload 0
invokespecial android/support/v4/app/bm/<init>(Landroid/support/v4/app/bl;)V
putfield android/support/v4/app/bl/F Ljava/lang/Runnable;
return
.limit locals 1
.limit stack 4
.end method

.method private A()Landroid/support/v4/view/as;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(IZ)I
iload 0
lookupswitch
4097 : L0
4099 : L1
8194 : L2
default : L3
L3:
iconst_m1
ireturn
L0:
iload 1
ifeq L4
iconst_1
ireturn
L4:
iconst_2
ireturn
L2:
iload 1
ifeq L5
iconst_3
ireturn
L5:
iconst_4
ireturn
L1:
iload 1
ifeq L6
iconst_5
ireturn
L6:
bipush 6
ireturn
.limit locals 2
.limit stack 1
.end method

.method private static a(FF)Landroid/view/animation/Animation;
new android/view/animation/AlphaAnimation
dup
fload 0
fload 1
invokespecial android/view/animation/AlphaAnimation/<init>(FF)V
astore 2
aload 2
getstatic android/support/v4/app/bl/H Landroid/view/animation/Interpolator;
invokevirtual android/view/animation/AlphaAnimation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 2
ldc2_w 220L
invokevirtual android/view/animation/AlphaAnimation/setDuration(J)V
aload 2
areturn
.limit locals 3
.limit stack 4
.end method

.method private static a(FFFF)Landroid/view/animation/Animation;
new android/view/animation/AnimationSet
dup
iconst_0
invokespecial android/view/animation/AnimationSet/<init>(Z)V
astore 4
new android/view/animation/ScaleAnimation
dup
fload 0
fload 1
fload 0
fload 1
iconst_1
ldc_w 0.5F
iconst_1
ldc_w 0.5F
invokespecial android/view/animation/ScaleAnimation/<init>(FFFFIFIF)V
astore 5
aload 5
getstatic android/support/v4/app/bl/G Landroid/view/animation/Interpolator;
invokevirtual android/view/animation/ScaleAnimation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 5
ldc2_w 220L
invokevirtual android/view/animation/ScaleAnimation/setDuration(J)V
aload 4
aload 5
invokevirtual android/view/animation/AnimationSet/addAnimation(Landroid/view/animation/Animation;)V
new android/view/animation/AlphaAnimation
dup
fload 2
fload 3
invokespecial android/view/animation/AlphaAnimation/<init>(FF)V
astore 5
aload 5
getstatic android/support/v4/app/bl/H Landroid/view/animation/Interpolator;
invokevirtual android/view/animation/AlphaAnimation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 5
ldc2_w 220L
invokevirtual android/view/animation/AlphaAnimation/setDuration(J)V
aload 4
aload 5
invokevirtual android/view/animation/AnimationSet/addAnimation(Landroid/view/animation/Animation;)V
aload 4
areturn
.limit locals 6
.limit stack 10
.end method

.method private a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;
aload 1
getfield android/support/v4/app/Fragment/aa I
istore 5
invokestatic android/support/v4/app/Fragment/r()Landroid/view/animation/Animation;
pop
aload 1
getfield android/support/v4/app/Fragment/aa I
ifeq L0
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/c Landroid/content/Context;
aload 1
getfield android/support/v4/app/Fragment/aa I
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
astore 1
aload 1
ifnull L0
aload 1
areturn
L0:
iload 2
ifne L1
aconst_null
areturn
L1:
iconst_m1
istore 5
iload 2
lookupswitch
4097 : L2
4099 : L3
8194 : L4
default : L5
L5:
iload 5
istore 2
L6:
iload 2
ifge L7
aconst_null
areturn
L2:
iload 3
ifeq L8
iconst_1
istore 2
goto L6
L8:
iconst_2
istore 2
goto L6
L4:
iload 3
ifeq L9
iconst_3
istore 2
goto L6
L9:
iconst_4
istore 2
goto L6
L3:
iload 3
ifeq L10
iconst_5
istore 2
goto L6
L10:
bipush 6
istore 2
goto L6
L7:
iload 2
tableswitch 1
L11
L12
L13
L14
L15
L16
default : L17
L17:
iload 4
istore 2
iload 4
ifne L18
iload 4
istore 2
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
invokevirtual android/support/v4/app/bh/e()Z
ifeq L18
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
invokevirtual android/support/v4/app/bh/f()I
istore 2
L18:
iload 2
ifne L19
aconst_null
areturn
L11:
ldc_w 1.125F
fconst_1
fconst_0
fconst_1
invokestatic android/support/v4/app/bl/a(FFFF)Landroid/view/animation/Animation;
areturn
L12:
fconst_1
ldc_w 0.975F
fconst_1
fconst_0
invokestatic android/support/v4/app/bl/a(FFFF)Landroid/view/animation/Animation;
areturn
L13:
ldc_w 0.975F
fconst_1
fconst_0
fconst_1
invokestatic android/support/v4/app/bl/a(FFFF)Landroid/view/animation/Animation;
areturn
L14:
fconst_1
ldc_w 1.075F
fconst_1
fconst_0
invokestatic android/support/v4/app/bl/a(FFFF)Landroid/view/animation/Animation;
areturn
L15:
fconst_0
fconst_1
invokestatic android/support/v4/app/bl/a(FF)Landroid/view/animation/Animation;
areturn
L16:
fconst_1
fconst_0
invokestatic android/support/v4/app/bl/a(FF)Landroid/view/animation/Animation;
areturn
L19:
aconst_null
areturn
.limit locals 6
.limit stack 4
.end method

.method private a(ILandroid/support/v4/app/ak;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L9 to L10 using L2
.catch all from L10 to L11 using L2
.catch all from L12 to L13 using L2
.catch all from L13 to L14 using L2
.catch all from L15 to L16 using L2
aload 0
monitorenter
L0:
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
ifnonnull L1
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/q Ljava/util/ArrayList;
L1:
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 4
L3:
iload 4
istore 3
iload 1
iload 4
if_icmpge L17
L4:
getstatic android/support/v4/app/bl/b Z
ifeq L5
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Setting back stack index "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " to "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L5:
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
iload 1
aload 2
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
L6:
aload 0
monitorexit
L7:
return
L17:
iload 3
iload 1
if_icmpge L12
L8:
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
aconst_null
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
ifnonnull L9
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/r Ljava/util/ArrayList;
L9:
getstatic android/support/v4/app/bl/b Z
ifeq L10
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Adding available back stack index "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L10:
aload 0
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L11:
iload 3
iconst_1
iadd
istore 3
goto L17
L12:
getstatic android/support/v4/app/bl/b Z
ifeq L13
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Adding back stack index "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " with "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L13:
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
aload 2
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L14:
goto L6
L2:
astore 2
L15:
aload 0
monitorexit
L16:
aload 2
athrow
.limit locals 5
.limit stack 4
.end method

.method private a(Ljava/lang/RuntimeException;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
ldc "FragmentManager"
aload 1
invokevirtual java/lang/RuntimeException/getMessage()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
ldc "FragmentManager"
ldc "Activity state:"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/io/PrintWriter
dup
new android/support/v4/n/h
dup
ldc "FragmentManager"
invokespecial android/support/v4/n/h/<init>(Ljava/lang/String;)V
invokespecial java/io/PrintWriter/<init>(Ljava/io/Writer;)V
astore 2
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
ifnull L3
L0:
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
ldc "  "
aload 2
iconst_0
anewarray java/lang/String
invokevirtual android/support/v4/app/bh/a(Ljava/lang/String;Ljava/io/PrintWriter;[Ljava/lang/String;)V
L1:
aload 1
athrow
L2:
astore 2
ldc "FragmentManager"
ldc "Failed dumping state"
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L3:
aload 0
ldc "  "
aconst_null
aload 2
iconst_0
anewarray java/lang/String
invokevirtual android/support/v4/app/bl/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
L4:
goto L1
L5:
astore 2
ldc "FragmentManager"
ldc "Failed dumping state"
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
.limit locals 3
.limit stack 5
.end method

.method static a(Landroid/view/View;Landroid/view/animation/Animation;)Z
iconst_0
istore 4
iload 4
istore 3
aload 0
invokestatic android/support/v4/view/cx/e(Landroid/view/View;)I
ifne L0
iload 4
istore 3
aload 0
invokestatic android/support/v4/view/cx/x(Landroid/view/View;)Z
ifeq L0
aload 1
instanceof android/view/animation/AlphaAnimation
ifeq L1
iconst_1
istore 2
L2:
iload 4
istore 3
iload 2
ifeq L0
iconst_1
istore 3
L0:
iload 3
ireturn
L1:
aload 1
instanceof android/view/animation/AnimationSet
ifeq L3
aload 1
checkcast android/view/animation/AnimationSet
invokevirtual android/view/animation/AnimationSet/getAnimations()Ljava/util/List;
astore 0
iconst_0
istore 2
L4:
iload 2
aload 0
invokeinterface java/util/List/size()I 0
if_icmpge L3
aload 0
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
instanceof android/view/animation/AlphaAnimation
ifeq L5
iconst_1
istore 2
goto L2
L5:
iload 2
iconst_1
iadd
istore 2
goto L4
L3:
iconst_0
istore 2
goto L2
.limit locals 5
.limit stack 2
.end method

.method private static a(Landroid/view/animation/Animation;)Z
iconst_0
istore 3
aload 0
instanceof android/view/animation/AlphaAnimation
ifeq L0
iconst_1
istore 2
L1:
iload 2
ireturn
L0:
iload 3
istore 2
aload 0
instanceof android/view/animation/AnimationSet
ifeq L1
aload 0
checkcast android/view/animation/AnimationSet
invokevirtual android/view/animation/AnimationSet/getAnimations()Ljava/util/List;
astore 0
iconst_0
istore 1
L2:
iload 3
istore 2
iload 1
aload 0
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
instanceof android/view/animation/AlphaAnimation
ifeq L3
iconst_1
ireturn
L3:
iload 1
iconst_1
iadd
istore 1
goto L2
.limit locals 4
.limit stack 2
.end method

.method private b(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnull L0
aload 1
ifnull L0
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 2
L1:
iload 2
iflt L0
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 4
aload 4
ifnull L2
aload 1
aload 4
getfield android/support/v4/app/Fragment/A Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
L4:
aload 4
ifnull L2
aload 4
areturn
L3:
aload 4
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L5
aload 4
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
astore 6
aload 6
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnull L6
aload 1
ifnull L6
aload 6
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 3
L7:
iload 3
iflt L6
aload 6
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 4
aload 4
ifnull L8
aload 4
aload 1
invokevirtual android/support/v4/app/Fragment/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
astore 5
aload 5
astore 4
aload 5
ifnonnull L4
L8:
iload 3
iconst_1
isub
istore 3
goto L7
L6:
aconst_null
astore 4
goto L4
L5:
aconst_null
astore 4
goto L4
L2:
iload 2
iconst_1
isub
istore 2
goto L1
L0:
aconst_null
areturn
.limit locals 7
.limit stack 2
.end method

.method private b(Landroid/support/v4/app/ak;)V
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
ifnonnull L0
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/o Ljava/util/ArrayList;
L0:
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
invokevirtual android/support/v4/app/bl/l()V
return
.limit locals 2
.limit stack 3
.end method

.method private static b(Landroid/view/View;Landroid/view/animation/Animation;)V
aload 0
ifnull L0
aload 1
ifnonnull L1
L0:
return
L1:
aload 0
aload 1
invokestatic android/support/v4/app/bl/a(Landroid/view/View;Landroid/view/animation/Animation;)Z
ifeq L0
aload 1
new android/support/v4/app/br
dup
aload 0
aload 1
invokespecial android/support/v4/app/br/<init>(Landroid/view/View;Landroid/view/animation/Animation;)V
invokevirtual android/view/animation/Animation/setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
return
.limit locals 2
.limit stack 5
.end method

.method private c(Landroid/support/v4/app/Fragment;)V
aload 0
aload 1
aload 0
getfield android/support/v4/app/bl/t I
iconst_0
iconst_0
iconst_0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;IIIZ)V
return
.limit locals 2
.limit stack 6
.end method

.method public static d(I)I
iload 0
lookupswitch
4097 : L0
4099 : L1
8194 : L2
default : L3
L3:
iconst_0
ireturn
L0:
sipush 8194
ireturn
L2:
sipush 4097
ireturn
L1:
sipush 4099
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d(Landroid/support/v4/app/Fragment;)V
aload 1
getfield android/support/v4/app/Fragment/z I
iflt L0
L1:
return
L0:
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
ifnull L2
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L3
L2:
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnonnull L4
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/l Ljava/util/ArrayList;
L4:
aload 1
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
aload 0
getfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/a(ILandroid/support/v4/app/Fragment;)V
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L5:
getstatic android/support/v4/app/bl/b Z
ifeq L1
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Allocated fragment index "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
return
L3:
aload 1
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
aload 0
getfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/a(ILandroid/support/v4/app/Fragment;)V
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
aload 1
getfield android/support/v4/app/Fragment/z I
aload 1
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
goto L5
.limit locals 2
.limit stack 4
.end method

.method private e(I)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aload 0
monitorenter
L0:
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
iload 1
aconst_null
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
ifnonnull L1
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/r Ljava/util/ArrayList;
L1:
getstatic android/support/v4/app/bl/b Z
ifeq L3
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Freeing back stack index "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L3:
aload 0
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
monitorexit
L4:
return
L2:
astore 2
L5:
aload 0
monitorexit
L6:
aload 2
athrow
.limit locals 3
.limit stack 4
.end method

.method private e(Landroid/support/v4/app/Fragment;)V
aload 1
getfield android/support/v4/app/Fragment/z I
ifge L0
return
L0:
getstatic android/support/v4/app/bl/b Z
ifeq L1
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Freeing fragment index "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
aload 1
getfield android/support/v4/app/Fragment/z I
aconst_null
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
ifnonnull L2
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/n Ljava/util/ArrayList;
L2:
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
aload 1
getfield android/support/v4/app/Fragment/z I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
aload 1
getfield android/support/v4/app/Fragment/A Ljava/lang/String;
invokevirtual android/support/v4/app/bh/b(Ljava/lang/String;)V
aload 1
iconst_m1
putfield android/support/v4/app/Fragment/z I
aload 1
aconst_null
putfield android/support/v4/app/Fragment/A Ljava/lang/String;
aload 1
iconst_0
putfield android/support/v4/app/Fragment/F Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/G Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/H Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/I Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/J Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/K Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/L I
aload 1
aconst_null
putfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
aload 1
aconst_null
putfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
aconst_null
putfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Q I
aload 1
iconst_0
putfield android/support/v4/app/Fragment/R I
aload 1
aconst_null
putfield android/support/v4/app/Fragment/S Ljava/lang/String;
aload 1
iconst_0
putfield android/support/v4/app/Fragment/T Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/U Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/W Z
aload 1
aconst_null
putfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
aload 1
iconst_0
putfield android/support/v4/app/Fragment/ah Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/ai Z
return
.limit locals 2
.limit stack 4
.end method

.method private f(Landroid/support/v4/app/Fragment;)V
aload 1
getfield android/support/v4/app/Fragment/ad Landroid/view/View;
ifnonnull L0
L1:
return
L0:
aload 0
getfield android/support/v4/app/bl/E Landroid/util/SparseArray;
ifnonnull L2
aload 0
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
putfield android/support/v4/app/bl/E Landroid/util/SparseArray;
L3:
aload 1
getfield android/support/v4/app/Fragment/ad Landroid/view/View;
aload 0
getfield android/support/v4/app/bl/E Landroid/util/SparseArray;
invokevirtual android/view/View/saveHierarchyState(Landroid/util/SparseArray;)V
aload 0
getfield android/support/v4/app/bl/E Landroid/util/SparseArray;
invokevirtual android/util/SparseArray/size()I
ifle L1
aload 1
aload 0
getfield android/support/v4/app/bl/E Landroid/util/SparseArray;
putfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
aload 0
aconst_null
putfield android/support/v4/app/bl/E Landroid/util/SparseArray;
return
L2:
aload 0
getfield android/support/v4/app/bl/E Landroid/util/SparseArray;
invokevirtual android/util/SparseArray/clear()V
goto L3
.limit locals 2
.limit stack 3
.end method

.method private g(Landroid/support/v4/app/Fragment;)Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/bl/D Landroid/os/Bundle;
ifnonnull L0
aload 0
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
putfield android/support/v4/app/bl/D Landroid/os/Bundle;
L0:
aload 1
aload 0
getfield android/support/v4/app/bl/D Landroid/os/Bundle;
invokevirtual android/support/v4/app/Fragment/f(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/app/bl/D Landroid/os/Bundle;
invokevirtual android/os/Bundle/isEmpty()Z
ifne L1
aload 0
getfield android/support/v4/app/bl/D Landroid/os/Bundle;
astore 3
aload 0
aconst_null
putfield android/support/v4/app/bl/D Landroid/os/Bundle;
L2:
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L3
aload 0
aload 1
invokespecial android/support/v4/app/bl/f(Landroid/support/v4/app/Fragment;)V
L3:
aload 3
astore 2
aload 1
getfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
ifnull L4
aload 3
astore 2
aload 3
ifnonnull L5
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 2
L5:
aload 2
ldc "android:view_state"
aload 1
getfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
invokevirtual android/os/Bundle/putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V
L4:
aload 2
astore 3
aload 1
getfield android/support/v4/app/Fragment/af Z
ifne L6
aload 2
astore 3
aload 2
ifnonnull L7
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 3
L7:
aload 3
ldc "android:user_visible_hint"
aload 1
getfield android/support/v4/app/Fragment/af Z
invokevirtual android/os/Bundle/putBoolean(Ljava/lang/String;Z)V
L6:
aload 3
areturn
L1:
aconst_null
astore 3
goto L2
.limit locals 4
.limit stack 3
.end method

.method private u()V
aload 0
getfield android/support/v4/app/bl/z Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "Can not perform this action after onSaveInstanceState"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/bl/B Ljava/lang/String;
ifnull L1
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Can not perform this action inside of "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/bl/B Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 1
.limit stack 5
.end method

.method private v()Ljava/util/ArrayList;
aconst_null
astore 4
aconst_null
astore 3
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnull L0
iconst_0
istore 1
L1:
aload 3
astore 4
iload 1
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L0
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 6
aload 3
astore 5
aload 6
ifnull L2
aload 3
astore 5
aload 6
getfield android/support/v4/app/Fragment/V Z
ifeq L2
aload 3
astore 4
aload 3
ifnonnull L3
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 4
L3:
aload 4
aload 6
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 6
iconst_1
putfield android/support/v4/app/Fragment/W Z
aload 6
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
ifnull L4
aload 6
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
getfield android/support/v4/app/Fragment/z I
istore 2
L5:
aload 6
iload 2
putfield android/support/v4/app/Fragment/D I
aload 4
astore 5
getstatic android/support/v4/app/bl/b Z
ifeq L2
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "retainNonConfig: keeping retained "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 4
astore 5
L2:
iload 1
iconst_1
iadd
istore 1
aload 5
astore 3
goto L1
L4:
iconst_m1
istore 2
goto L5
L0:
aload 4
areturn
.limit locals 7
.limit stack 4
.end method

.method private w()V
aload 0
iconst_0
putfield android/support/v4/app/bl/z Z
return
.limit locals 1
.limit stack 2
.end method

.method private x()V
aload 0
iconst_4
invokevirtual android/support/v4/app/bl/c(I)V
return
.limit locals 1
.limit stack 2
.end method

.method private y()V
aload 0
iconst_2
invokevirtual android/support/v4/app/bl/c(I)V
return
.limit locals 1
.limit stack 2
.end method

.method private z()V
aload 0
iconst_1
invokevirtual android/support/v4/app/bl/c(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Landroid/support/v4/app/ak;)I
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
aload 0
monitorenter
L0:
aload 0
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
ifnull L1
aload 0
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L6
L1:
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
ifnonnull L3
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/q Ljava/util/ArrayList;
L3:
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 2
getstatic android/support/v4/app/bl/b Z
ifeq L4
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Setting back stack index "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " to "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L4:
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
monitorexit
L5:
iload 2
ireturn
L6:
aload 0
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 2
getstatic android/support/v4/app/bl/b Z
ifeq L7
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Adding back stack index "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " with "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L7:
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
iload 2
aload 1
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
aload 0
monitorexit
L8:
iload 2
ireturn
L2:
astore 1
L9:
aload 0
monitorexit
L10:
aload 1
athrow
.limit locals 3
.limit stack 4
.end method

.method public final a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;
aconst_null
astore 3
aload 1
getfield android/support/v4/app/Fragment/z I
ifge L0
aload 0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " is not currently in the FragmentManager"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
invokespecial android/support/v4/app/bl/a(Ljava/lang/RuntimeException;)V
L0:
aload 3
astore 2
aload 1
getfield android/support/v4/app/Fragment/u I
ifle L1
aload 0
aload 1
invokespecial android/support/v4/app/bl/g(Landroid/support/v4/app/Fragment;)Landroid/os/Bundle;
astore 1
aload 3
astore 2
aload 1
ifnull L1
new android/support/v4/app/Fragment$SavedState
dup
aload 1
invokespecial android/support/v4/app/Fragment$SavedState/<init>(Landroid/os/Bundle;)V
astore 2
L1:
aload 2
areturn
.limit locals 4
.limit stack 6
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L0
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 2
L1:
iload 2
iflt L0
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 3
aload 3
ifnull L2
aload 3
getfield android/support/v4/app/Fragment/Q I
iload 1
if_icmpne L2
L3:
aload 3
areturn
L2:
iload 2
iconst_1
isub
istore 2
goto L1
L0:
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnull L4
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 2
L5:
iload 2
iflt L4
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 4
aload 4
ifnull L6
aload 4
astore 3
aload 4
getfield android/support/v4/app/Fragment/Q I
iload 1
if_icmpeq L3
L6:
iload 2
iconst_1
isub
istore 2
goto L5
L4:
aconst_null
areturn
.limit locals 5
.limit stack 2
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
aload 1
aload 2
iconst_m1
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
istore 3
iload 3
iconst_m1
if_icmpne L0
aconst_null
astore 1
L1:
aload 1
areturn
L0:
iload 3
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmplt L2
aload 0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Fragment no longer exists for key "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": index "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
invokespecial android/support/v4/app/bl/a(Ljava/lang/RuntimeException;)V
L2:
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 4
aload 4
astore 1
aload 4
ifnonnull L1
aload 0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Fragment no longer exists for key "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": index "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
invokespecial android/support/v4/app/bl/a(Ljava/lang/RuntimeException;)V
aload 4
areturn
.limit locals 5
.limit stack 6
.end method

.method public final a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L0
aload 1
ifnull L0
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 2
L1:
iload 2
iflt L0
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 3
aload 3
ifnull L2
aload 1
aload 3
getfield android/support/v4/app/Fragment/S Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
L3:
aload 3
areturn
L2:
iload 2
iconst_1
isub
istore 2
goto L1
L0:
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnull L4
aload 1
ifnull L4
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 2
L5:
iload 2
iflt L4
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 4
aload 4
ifnull L6
aload 4
astore 3
aload 1
aload 4
getfield android/support/v4/app/Fragment/S Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L3
L6:
iload 2
iconst_1
isub
istore 2
goto L5
L4:
aconst_null
areturn
.limit locals 5
.limit stack 2
.end method

.method public final a()Landroid/support/v4/app/cd;
new android/support/v4/app/ak
dup
aload 0
invokespecial android/support/v4/app/ak/<init>(Landroid/support/v4/app/bl;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
ldc "fragment"
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
aconst_null
areturn
L0:
aload 4
aconst_null
ldc "class"
invokeinterface android/util/AttributeSet/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 8
aload 3
aload 4
getstatic android/support/v4/app/bu/a [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 2
aload 8
ifnonnull L1
aload 2
iconst_0
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
astore 8
L2:
aload 2
iconst_1
iconst_m1
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 7
aload 2
iconst_2
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
astore 9
aload 2
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/c Landroid/content/Context;
aload 8
invokestatic android/support/v4/app/Fragment/b(Landroid/content/Context;Ljava/lang/String;)Z
ifne L3
aconst_null
areturn
L3:
aload 1
ifnull L4
aload 1
invokevirtual android/view/View/getId()I
istore 5
L5:
iload 5
iconst_m1
if_icmpne L6
iload 7
iconst_m1
if_icmpne L6
aload 9
ifnonnull L6
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokeinterface android/util/AttributeSet/getPositionDescription()Ljava/lang/String; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": Must specify unique android:id, android:tag, or have a parent with an id for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
iconst_0
istore 5
goto L5
L6:
iload 7
iconst_m1
if_icmpeq L7
aload 0
iload 7
invokevirtual android/support/v4/app/bl/a(I)Landroid/support/v4/app/Fragment;
astore 2
L8:
aload 2
astore 1
aload 2
ifnonnull L9
aload 2
astore 1
aload 9
ifnull L9
aload 0
aload 9
invokevirtual android/support/v4/app/bl/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
astore 1
L9:
aload 1
astore 2
aload 1
ifnonnull L10
aload 1
astore 2
iload 5
iconst_m1
if_icmpeq L10
aload 0
iload 5
invokevirtual android/support/v4/app/bl/a(I)Landroid/support/v4/app/Fragment;
astore 2
L10:
getstatic android/support/v4/app/bl/b Z
ifeq L11
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "onCreateView: id=0x"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 7
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " fname="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " existing="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L11:
aload 2
ifnonnull L12
aload 3
aload 8
invokestatic android/support/v4/app/Fragment/a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
astore 1
aload 1
iconst_1
putfield android/support/v4/app/Fragment/I Z
iload 7
ifeq L13
iload 7
istore 6
L14:
aload 1
iload 6
putfield android/support/v4/app/Fragment/Q I
aload 1
iload 5
putfield android/support/v4/app/Fragment/R I
aload 1
aload 9
putfield android/support/v4/app/Fragment/S Ljava/lang/String;
aload 1
iconst_1
putfield android/support/v4/app/Fragment/J Z
aload 1
aload 0
putfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
aload 1
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
putfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
astore 2
aload 1
invokevirtual android/support/v4/app/Fragment/q()V
aload 0
aload 1
iconst_1
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;Z)V
L15:
aload 0
getfield android/support/v4/app/bl/t I
ifgt L16
aload 1
getfield android/support/v4/app/Fragment/I Z
ifeq L16
aload 0
aload 1
iconst_1
iconst_0
iconst_0
iconst_0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;IIIZ)V
L17:
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnonnull L18
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " did not create a view."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L7:
aconst_null
astore 2
goto L8
L13:
iload 5
istore 6
goto L14
L12:
aload 2
getfield android/support/v4/app/Fragment/J Z
ifeq L19
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokeinterface android/util/AttributeSet/getPositionDescription()Ljava/lang/String; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": Duplicate id 0x"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 7
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", tag "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", or parent id 0x"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 5
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " with another fragment for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L19:
aload 2
iconst_1
putfield android/support/v4/app/Fragment/J Z
aload 2
getfield android/support/v4/app/Fragment/W Z
ifne L20
aload 2
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
astore 1
aload 2
invokevirtual android/support/v4/app/Fragment/q()V
L20:
aload 2
astore 1
goto L15
L16:
aload 0
aload 1
invokespecial android/support/v4/app/bl/c(Landroid/support/v4/app/Fragment;)V
goto L17
L18:
iload 7
ifeq L21
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
iload 7
invokevirtual android/view/View/setId(I)V
L21:
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual android/view/View/getTag()Ljava/lang/Object;
ifnonnull L22
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 9
invokevirtual android/view/View/setTag(Ljava/lang/Object;)V
L22:
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
areturn
L1:
goto L2
.limit locals 10
.limit stack 6
.end method

.method final a(IIIZ)V
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
ifnonnull L0
iload 1
ifeq L0
new java/lang/IllegalStateException
dup
ldc "No host"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 4
ifne L1
aload 0
getfield android/support/v4/app/bl/t I
iload 1
if_icmpne L1
L2:
return
L1:
aload 0
iload 1
putfield android/support/v4/app/bl/t I
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnull L2
iconst_0
istore 6
iconst_0
istore 5
L3:
iload 6
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L4
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 7
aload 7
ifnull L5
aload 0
aload 7
iload 1
iload 2
iload 3
iconst_0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;IIIZ)V
aload 7
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
ifnull L5
iload 5
aload 7
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/a()Z
ior
istore 5
L6:
iload 6
iconst_1
iadd
istore 6
goto L3
L4:
iload 5
ifne L7
aload 0
invokevirtual android/support/v4/app/bl/j()V
L7:
aload 0
getfield android/support/v4/app/bl/y Z
ifeq L2
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
ifnull L2
aload 0
getfield android/support/v4/app/bl/t I
iconst_5
if_icmpne L2
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
invokevirtual android/support/v4/app/bh/d()V
aload 0
iconst_0
putfield android/support/v4/app/bl/y Z
return
L5:
goto L6
.limit locals 8
.limit stack 6
.end method

.method public final a(Landroid/content/res/Configuration;)V
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L0
iconst_0
istore 2
L1:
iload 2
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L0
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 3
aload 3
ifnull L2
aload 3
aload 1
invokevirtual android/support/v4/app/Fragment/onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 3
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L2
aload 3
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/a(Landroid/content/res/Configuration;)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L1
L0:
return
.limit locals 4
.limit stack 2
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
aload 3
getfield android/support/v4/app/Fragment/z I
ifge L0
aload 0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " is not currently in the FragmentManager"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
invokespecial android/support/v4/app/bl/a(Ljava/lang/RuntimeException;)V
L0:
aload 1
aload 2
aload 3
getfield android/support/v4/app/Fragment/z I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 6
.end method

.method final a(Landroid/os/Parcelable;Ljava/util/List;)V
aload 1
ifnonnull L0
L1:
return
L0:
aload 1
checkcast android/support/v4/app/FragmentManagerState
astore 1
aload 1
getfield android/support/v4/app/FragmentManagerState/a [Landroid/support/v4/app/FragmentState;
ifnull L1
aload 2
ifnull L2
iconst_0
istore 3
L3:
iload 3
aload 2
invokeinterface java/util/List/size()I 0
if_icmpge L2
aload 2
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/app/Fragment
astore 4
getstatic android/support/v4/app/bl/b Z
ifeq L4
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "restoreAllState: re-attaching retained "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L4:
aload 1
getfield android/support/v4/app/FragmentManagerState/a [Landroid/support/v4/app/FragmentState;
aload 4
getfield android/support/v4/app/Fragment/z I
aaload
astore 5
aload 5
aload 4
putfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 4
aconst_null
putfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
aload 4
iconst_0
putfield android/support/v4/app/Fragment/L I
aload 4
iconst_0
putfield android/support/v4/app/Fragment/J Z
aload 4
iconst_0
putfield android/support/v4/app/Fragment/F Z
aload 4
aconst_null
putfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
aload 5
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
ifnull L5
aload 5
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/c Landroid/content/Context;
invokevirtual android/content/Context/getClassLoader()Ljava/lang/ClassLoader;
invokevirtual android/os/Bundle/setClassLoader(Ljava/lang/ClassLoader;)V
aload 4
aload 5
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
ldc "android:view_state"
invokevirtual android/os/Bundle/getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;
putfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
aload 4
aload 5
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
putfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
L5:
iload 3
iconst_1
iadd
istore 3
goto L3
L2:
aload 0
new java/util/ArrayList
dup
aload 1
getfield android/support/v4/app/FragmentManagerState/a [Landroid/support/v4/app/FragmentState;
arraylength
invokespecial java/util/ArrayList/<init>(I)V
putfield android/support/v4/app/bl/l Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
ifnull L6
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
L6:
iconst_0
istore 3
L7:
iload 3
aload 1
getfield android/support/v4/app/FragmentManagerState/a [Landroid/support/v4/app/FragmentState;
arraylength
if_icmpge L8
aload 1
getfield android/support/v4/app/FragmentManagerState/a [Landroid/support/v4/app/FragmentState;
iload 3
aaload
astore 4
aload 4
ifnull L9
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
astore 5
aload 0
getfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
astore 6
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
ifnonnull L10
aload 5
getfield android/support/v4/app/bh/c Landroid/content/Context;
astore 7
aload 4
getfield android/support/v4/app/FragmentState/i Landroid/os/Bundle;
ifnull L11
aload 4
getfield android/support/v4/app/FragmentState/i Landroid/os/Bundle;
aload 7
invokevirtual android/content/Context/getClassLoader()Ljava/lang/ClassLoader;
invokevirtual android/os/Bundle/setClassLoader(Ljava/lang/ClassLoader;)V
L11:
aload 4
aload 7
aload 4
getfield android/support/v4/app/FragmentState/a Ljava/lang/String;
aload 4
getfield android/support/v4/app/FragmentState/i Landroid/os/Bundle;
invokestatic android/support/v4/app/Fragment/a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;
putfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 4
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
ifnull L12
aload 4
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
aload 7
invokevirtual android/content/Context/getClassLoader()Ljava/lang/ClassLoader;
invokevirtual android/os/Bundle/setClassLoader(Ljava/lang/ClassLoader;)V
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 4
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
putfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
L12:
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 4
getfield android/support/v4/app/FragmentState/b I
aload 6
invokevirtual android/support/v4/app/Fragment/a(ILandroid/support/v4/app/Fragment;)V
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 4
getfield android/support/v4/app/FragmentState/c Z
putfield android/support/v4/app/Fragment/I Z
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
iconst_1
putfield android/support/v4/app/Fragment/K Z
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 4
getfield android/support/v4/app/FragmentState/d I
putfield android/support/v4/app/Fragment/Q I
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 4
getfield android/support/v4/app/FragmentState/e I
putfield android/support/v4/app/Fragment/R I
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 4
getfield android/support/v4/app/FragmentState/f Ljava/lang/String;
putfield android/support/v4/app/Fragment/S Ljava/lang/String;
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 4
getfield android/support/v4/app/FragmentState/g Z
putfield android/support/v4/app/Fragment/V Z
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 4
getfield android/support/v4/app/FragmentState/h Z
putfield android/support/v4/app/Fragment/U Z
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
aload 5
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
putfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
getstatic android/support/v4/app/bl/b Z
ifeq L10
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Instantiated fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L10:
aload 4
getfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
astore 5
getstatic android/support/v4/app/bl/b Z
ifeq L13
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "restoreAllState: active #"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L13:
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
aload 5
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 4
aconst_null
putfield android/support/v4/app/FragmentState/k Landroid/support/v4/app/Fragment;
L14:
iload 3
iconst_1
iadd
istore 3
goto L7
L9:
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
aconst_null
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
ifnonnull L15
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/n Ljava/util/ArrayList;
L15:
getstatic android/support/v4/app/bl/b Z
ifeq L16
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "restoreAllState: avail #"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L16:
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L14
L8:
aload 2
ifnull L17
iconst_0
istore 3
L18:
iload 3
aload 2
invokeinterface java/util/List/size()I 0
if_icmpge L17
aload 2
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/app/Fragment
astore 4
aload 4
getfield android/support/v4/app/Fragment/D I
iflt L19
aload 4
getfield android/support/v4/app/Fragment/D I
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L20
aload 4
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
aload 4
getfield android/support/v4/app/Fragment/D I
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
putfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
L19:
iload 3
iconst_1
iadd
istore 3
goto L18
L20:
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Re-attaching retained fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " target no longer exists: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
getfield android/support/v4/app/Fragment/D I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 4
aconst_null
putfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
goto L19
L17:
aload 1
getfield android/support/v4/app/FragmentManagerState/b [I
ifnull L21
aload 0
new java/util/ArrayList
dup
aload 1
getfield android/support/v4/app/FragmentManagerState/b [I
arraylength
invokespecial java/util/ArrayList/<init>(I)V
putfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iconst_0
istore 3
L22:
iload 3
aload 1
getfield android/support/v4/app/FragmentManagerState/b [I
arraylength
if_icmpge L23
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
aload 1
getfield android/support/v4/app/FragmentManagerState/b [I
iload 3
iaload
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 2
aload 2
ifnonnull L24
aload 0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "No instantiated fragment for index #"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
getfield android/support/v4/app/FragmentManagerState/b [I
iload 3
iaload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
invokespecial android/support/v4/app/bl/a(Ljava/lang/RuntimeException;)V
L24:
aload 2
iconst_1
putfield android/support/v4/app/Fragment/F Z
getstatic android/support/v4/app/bl/b Z
ifeq L25
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "restoreAllState: added #"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L25:
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
aload 2
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L26
new java/lang/IllegalStateException
dup
ldc "Already added!"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L26:
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
aload 2
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 3
iconst_1
iadd
istore 3
goto L22
L21:
aload 0
aconst_null
putfield android/support/v4/app/bl/m Ljava/util/ArrayList;
L23:
aload 1
getfield android/support/v4/app/FragmentManagerState/c [Landroid/support/v4/app/BackStackState;
ifnull L27
aload 0
new java/util/ArrayList
dup
aload 1
getfield android/support/v4/app/FragmentManagerState/c [Landroid/support/v4/app/BackStackState;
arraylength
invokespecial java/util/ArrayList/<init>(I)V
putfield android/support/v4/app/bl/o Ljava/util/ArrayList;
iconst_0
istore 3
L28:
iload 3
aload 1
getfield android/support/v4/app/FragmentManagerState/c [Landroid/support/v4/app/BackStackState;
arraylength
if_icmpge L1
aload 1
getfield android/support/v4/app/FragmentManagerState/c [Landroid/support/v4/app/BackStackState;
iload 3
aaload
aload 0
invokevirtual android/support/v4/app/BackStackState/a(Landroid/support/v4/app/bl;)Landroid/support/v4/app/ak;
astore 2
getstatic android/support/v4/app/bl/b Z
ifeq L29
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "restoreAllState: back stack #"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " (index "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
getfield android/support/v4/app/ak/y I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "): "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 2
ldc "  "
new java/io/PrintWriter
dup
new android/support/v4/n/h
dup
ldc "FragmentManager"
invokespecial android/support/v4/n/h/<init>(Ljava/lang/String;)V
invokespecial java/io/PrintWriter/<init>(Ljava/io/Writer;)V
iconst_0
invokevirtual android/support/v4/app/ak/a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
L29:
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
aload 2
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 2
getfield android/support/v4/app/ak/y I
iflt L30
aload 0
aload 2
getfield android/support/v4/app/ak/y I
aload 2
invokespecial android/support/v4/app/bl/a(ILandroid/support/v4/app/ak;)V
L30:
iload 3
iconst_1
iadd
istore 3
goto L28
L27:
aload 0
aconst_null
putfield android/support/v4/app/bl/o Ljava/util/ArrayList;
return
.limit locals 8
.limit stack 7
.end method

.method public final a(Landroid/support/v4/app/Fragment;II)V
getstatic android/support/v4/app/bl/b Z
ifeq L0
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "remove: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " nesting="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getfield android/support/v4/app/Fragment/L I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 1
getfield android/support/v4/app/Fragment/L I
ifle L1
iconst_1
istore 4
L2:
iload 4
ifne L3
iconst_1
istore 4
L4:
aload 1
getfield android/support/v4/app/Fragment/U Z
ifeq L5
iload 4
ifeq L6
L5:
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L7
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
L7:
aload 1
getfield android/support/v4/app/Fragment/X Z
ifeq L8
aload 1
getfield android/support/v4/app/Fragment/Y Z
ifeq L8
aload 0
iconst_1
putfield android/support/v4/app/bl/y Z
L8:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/F Z
aload 1
iconst_1
putfield android/support/v4/app/Fragment/G Z
iload 4
ifeq L9
iconst_0
istore 4
L10:
aload 0
aload 1
iload 4
iload 2
iload 3
iconst_0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;IIIZ)V
L6:
return
L1:
iconst_0
istore 4
goto L2
L3:
iconst_0
istore 4
goto L4
L9:
iconst_1
istore 4
goto L10
.limit locals 5
.limit stack 6
.end method

.method final a(Landroid/support/v4/app/Fragment;IIIZ)V
aload 1
getfield android/support/v4/app/Fragment/F Z
ifeq L0
iload 2
istore 6
aload 1
getfield android/support/v4/app/Fragment/U Z
ifeq L1
L0:
iload 2
istore 6
iload 2
iconst_1
if_icmple L1
iconst_1
istore 6
L1:
iload 6
istore 7
aload 1
getfield android/support/v4/app/Fragment/G Z
ifeq L2
iload 6
istore 7
iload 6
aload 1
getfield android/support/v4/app/Fragment/u I
if_icmple L2
aload 1
getfield android/support/v4/app/Fragment/u I
istore 7
L2:
iload 7
istore 2
aload 1
getfield android/support/v4/app/Fragment/ae Z
ifeq L3
iload 7
istore 2
aload 1
getfield android/support/v4/app/Fragment/u I
iconst_4
if_icmpge L3
iload 7
istore 2
iload 7
iconst_3
if_icmple L3
iconst_3
istore 2
L3:
aload 1
getfield android/support/v4/app/Fragment/u I
iload 2
if_icmpge L4
aload 1
getfield android/support/v4/app/Fragment/I Z
ifeq L5
aload 1
getfield android/support/v4/app/Fragment/J Z
ifne L5
return
L5:
aload 1
getfield android/support/v4/app/Fragment/v Landroid/view/View;
ifnull L6
aload 1
aconst_null
putfield android/support/v4/app/Fragment/v Landroid/view/View;
aload 0
aload 1
aload 1
getfield android/support/v4/app/Fragment/w I
iconst_0
iconst_0
iconst_1
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;IIIZ)V
L6:
iload 2
istore 6
iload 2
istore 8
iload 2
istore 7
aload 1
getfield android/support/v4/app/Fragment/u I
tableswitch 0
L7
L8
L9
L9
L10
default : L11
L11:
iload 2
istore 6
L12:
aload 1
iload 6
putfield android/support/v4/app/Fragment/u I
return
L7:
getstatic android/support/v4/app/bl/b Z
ifeq L13
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "moveto CREATED: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L13:
iload 2
istore 7
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
ifnull L14
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/c Landroid/content/Context;
invokevirtual android/content/Context/getClassLoader()Ljava/lang/ClassLoader;
invokevirtual android/os/Bundle/setClassLoader(Ljava/lang/ClassLoader;)V
aload 1
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
ldc "android:view_state"
invokevirtual android/os/Bundle/getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;
putfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
aload 1
aload 0
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
ldc "android:target_state"
invokevirtual android/support/v4/app/bl/a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
putfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
aload 1
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
ifnull L15
aload 1
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
ldc "android:target_req_state"
iconst_0
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield android/support/v4/app/Fragment/E I
L15:
aload 1
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
ldc "android:user_visible_hint"
iconst_1
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;Z)Z
putfield android/support/v4/app/Fragment/af Z
iload 2
istore 7
aload 1
getfield android/support/v4/app/Fragment/af Z
ifne L14
aload 1
iconst_1
putfield android/support/v4/app/Fragment/ae Z
iload 2
istore 7
iload 2
iconst_3
if_icmple L14
iconst_3
istore 7
L14:
aload 1
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
putfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 1
aload 0
getfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
putfield android/support/v4/app/Fragment/P Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
ifnull L16
aload 0
getfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
astore 9
L17:
aload 1
aload 9
putfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 1
iconst_1
putfield android/support/v4/app/Fragment/Z Z
aload 1
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnonnull L18
aconst_null
astore 9
L19:
aload 9
ifnull L20
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 1
aload 9
invokevirtual android/support/v4/app/Fragment/a(Landroid/app/Activity;)V
L20:
aload 1
getfield android/support/v4/app/Fragment/Z Z
ifne L21
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onAttach()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L16:
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
astore 9
goto L17
L18:
aload 1
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/b Landroid/app/Activity;
astore 9
goto L19
L21:
aload 1
getfield android/support/v4/app/Fragment/P Landroid/support/v4/app/Fragment;
astore 9
aload 1
getfield android/support/v4/app/Fragment/W Z
ifne L22
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
astore 9
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L23
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_0
putfield android/support/v4/app/bl/z Z
L23:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 1
aload 9
invokevirtual android/support/v4/app/Fragment/a(Landroid/os/Bundle;)V
aload 1
getfield android/support/v4/app/Fragment/Z Z
ifne L24
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onCreate()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L24:
aload 9
ifnull L22
aload 9
ldc "android:support:fragments"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
astore 9
aload 9
ifnull L22
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnonnull L25
aload 1
invokevirtual android/support/v4/app/Fragment/w()V
L25:
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 9
aconst_null
invokevirtual android/support/v4/app/bl/a(Landroid/os/Parcelable;Ljava/util/List;)V
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/n()V
L22:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/W Z
iload 7
istore 6
aload 1
getfield android/support/v4/app/Fragment/I Z
ifeq L8
aload 1
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
invokevirtual android/support/v4/app/Fragment/b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
astore 9
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
astore 10
aload 1
aload 1
aload 9
aconst_null
invokevirtual android/support/v4/app/Fragment/b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
putfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L26
aload 1
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
putfield android/support/v4/app/Fragment/ad Landroid/view/View;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L27
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokestatic android/support/v4/view/cx/w(Landroid/view/View;)V
L28:
aload 1
getfield android/support/v4/app/Fragment/T Z
ifeq L29
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
bipush 8
invokevirtual android/view/View/setVisibility(I)V
L29:
aload 1
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
invokevirtual android/support/v4/app/Fragment/a(Landroid/view/View;Landroid/os/Bundle;)V
iload 7
istore 6
L8:
iload 6
istore 8
iload 6
iconst_1
if_icmple L9
getstatic android/support/v4/app/bl/b Z
ifeq L30
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "moveto ACTIVITY_CREATED: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L30:
aload 1
getfield android/support/v4/app/Fragment/I Z
ifne L31
aload 1
getfield android/support/v4/app/Fragment/R I
ifeq L32
aload 0
getfield android/support/v4/app/bl/w Landroid/support/v4/app/bf;
aload 1
getfield android/support/v4/app/Fragment/R I
invokevirtual android/support/v4/app/bf/a(I)Landroid/view/View;
checkcast android/view/ViewGroup
astore 10
aload 10
astore 9
aload 10
ifnonnull L33
aload 10
astore 9
aload 1
getfield android/support/v4/app/Fragment/K Z
ifne L33
aload 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "No view found for id 0x"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
getfield android/support/v4/app/Fragment/R I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/support/v4/app/Fragment/j()Landroid/content/res/Resources;
aload 1
getfield android/support/v4/app/Fragment/R I
invokevirtual android/content/res/Resources/getResourceName(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ") for fragment "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
invokespecial android/support/v4/app/bl/a(Ljava/lang/RuntimeException;)V
aload 10
astore 9
L33:
aload 1
aload 9
putfield android/support/v4/app/Fragment/ab Landroid/view/ViewGroup;
aload 1
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
invokevirtual android/support/v4/app/Fragment/b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
astore 10
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
astore 11
aload 1
aload 1
aload 10
aload 9
invokevirtual android/support/v4/app/Fragment/b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
putfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L34
aload 1
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
putfield android/support/v4/app/Fragment/ad Landroid/view/View;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L35
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokestatic android/support/v4/view/cx/w(Landroid/view/View;)V
L36:
aload 9
ifnull L37
aload 0
aload 1
iload 3
iconst_1
iload 4
invokespecial android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;
astore 10
aload 10
ifnull L38
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 10
invokestatic android/support/v4/app/bl/b(Landroid/view/View;Landroid/view/animation/Animation;)V
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 10
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
L38:
aload 9
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual android/view/ViewGroup/addView(Landroid/view/View;)V
L37:
aload 1
getfield android/support/v4/app/Fragment/T Z
ifeq L39
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
bipush 8
invokevirtual android/view/View/setVisibility(I)V
L39:
aload 1
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
invokevirtual android/support/v4/app/Fragment/a(Landroid/view/View;Landroid/os/Bundle;)V
L31:
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
astore 9
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L40
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_0
putfield android/support/v4/app/bl/z Z
L40:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 1
aload 9
invokevirtual android/support/v4/app/Fragment/c(Landroid/os/Bundle;)V
aload 1
getfield android/support/v4/app/Fragment/Z Z
ifne L41
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onActivityCreated()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L27:
aload 1
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokestatic android/support/v4/app/da/a(Landroid/view/View;)Landroid/view/ViewGroup;
putfield android/support/v4/app/Fragment/ac Landroid/view/View;
goto L28
L26:
aload 1
aconst_null
putfield android/support/v4/app/Fragment/ad Landroid/view/View;
iload 7
istore 6
goto L8
L35:
aload 1
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokestatic android/support/v4/app/da/a(Landroid/view/View;)Landroid/view/ViewGroup;
putfield android/support/v4/app/Fragment/ac Landroid/view/View;
goto L36
L34:
aload 1
aconst_null
putfield android/support/v4/app/Fragment/ad Landroid/view/View;
goto L31
L41:
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L42
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/o()V
L42:
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L43
aload 1
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
astore 9
aload 1
getfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
ifnull L44
aload 1
getfield android/support/v4/app/Fragment/ad Landroid/view/View;
aload 1
getfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
invokevirtual android/view/View/restoreHierarchyState(Landroid/util/SparseArray;)V
aload 1
aconst_null
putfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
L44:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 1
iconst_1
putfield android/support/v4/app/Fragment/Z Z
aload 1
getfield android/support/v4/app/Fragment/Z Z
ifne L43
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onViewStateRestored()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L43:
aload 1
aconst_null
putfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
iload 6
istore 8
L9:
iload 8
istore 7
iload 8
iconst_3
if_icmple L10
getstatic android/support/v4/app/bl/b Z
ifeq L45
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "moveto STARTED: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L45:
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L46
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_0
putfield android/support/v4/app/bl/z Z
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/k()Z
pop
L46:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 1
invokevirtual android/support/v4/app/Fragment/e()V
aload 1
getfield android/support/v4/app/Fragment/Z Z
ifne L47
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onStart()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L47:
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L48
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/p()V
L48:
iload 8
istore 7
aload 1
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
ifnull L10
aload 1
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/f()V
iload 8
istore 7
L10:
iload 7
istore 6
iload 7
iconst_4
if_icmple L12
getstatic android/support/v4/app/bl/b Z
ifeq L49
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "moveto RESUMED: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L49:
aload 1
iconst_1
putfield android/support/v4/app/Fragment/H Z
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L50
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_0
putfield android/support/v4/app/bl/z Z
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/k()Z
pop
L50:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 1
invokevirtual android/support/v4/app/Fragment/s()V
aload 1
getfield android/support/v4/app/Fragment/Z Z
ifne L51
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onResume()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L51:
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L52
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/q()V
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/k()Z
pop
L52:
aload 1
aconst_null
putfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
aload 1
aconst_null
putfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
iload 7
istore 6
goto L12
L4:
iload 2
istore 6
aload 1
getfield android/support/v4/app/Fragment/u I
iload 2
if_icmple L12
aload 1
getfield android/support/v4/app/Fragment/u I
tableswitch 1
L53
L54
L55
L56
L57
default : L58
L58:
iload 2
istore 6
goto L12
L53:
iload 2
istore 6
iload 2
ifgt L12
aload 0
getfield android/support/v4/app/bl/A Z
ifeq L59
aload 1
getfield android/support/v4/app/Fragment/v Landroid/view/View;
ifnull L59
aload 1
getfield android/support/v4/app/Fragment/v Landroid/view/View;
astore 9
aload 1
aconst_null
putfield android/support/v4/app/Fragment/v Landroid/view/View;
aload 9
invokevirtual android/view/View/clearAnimation()V
L59:
aload 1
getfield android/support/v4/app/Fragment/v Landroid/view/View;
ifnull L60
aload 1
iload 2
putfield android/support/v4/app/Fragment/w I
iconst_1
istore 6
goto L12
L57:
iload 2
iconst_5
if_icmpge L56
getstatic android/support/v4/app/bl/b Z
ifeq L61
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "movefrom RESUMED: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L61:
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L62
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_4
invokevirtual android/support/v4/app/bl/c(I)V
L62:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 1
invokevirtual android/support/v4/app/Fragment/t()V
aload 1
getfield android/support/v4/app/Fragment/Z Z
ifne L63
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onPause()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L63:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/H Z
L56:
iload 2
iconst_4
if_icmpge L55
getstatic android/support/v4/app/bl/b Z
ifeq L64
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "movefrom STARTED: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L64:
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L65
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/r()V
L65:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 1
invokevirtual android/support/v4/app/Fragment/f()V
aload 1
getfield android/support/v4/app/Fragment/Z Z
ifne L55
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onStop()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L55:
iload 2
iconst_3
if_icmpge L54
getstatic android/support/v4/app/bl/b Z
ifeq L66
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "movefrom STOPPED: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L66:
aload 1
invokevirtual android/support/v4/app/Fragment/x()V
L54:
iload 2
iconst_2
if_icmpge L53
getstatic android/support/v4/app/bl/b Z
ifeq L67
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "movefrom ACTIVITY_CREATED: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L67:
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L68
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
invokevirtual android/support/v4/app/bh/b()Z
ifeq L68
aload 1
getfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
ifnonnull L68
aload 0
aload 1
invokespecial android/support/v4/app/bl/f(Landroid/support/v4/app/Fragment;)V
L68:
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L69
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_1
invokevirtual android/support/v4/app/bl/c(I)V
L69:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 1
invokevirtual android/support/v4/app/Fragment/g()V
aload 1
getfield android/support/v4/app/Fragment/Z Z
ifne L70
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onDestroyView()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L70:
aload 1
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
ifnull L71
aload 1
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/e()V
L71:
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L72
aload 1
getfield android/support/v4/app/Fragment/ab Landroid/view/ViewGroup;
ifnull L72
aload 0
getfield android/support/v4/app/bl/t I
ifle L73
aload 0
getfield android/support/v4/app/bl/A Z
ifne L73
aload 0
aload 1
iload 3
iconst_0
iload 4
invokespecial android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;
astore 9
L74:
aload 9
ifnull L75
aload 1
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
putfield android/support/v4/app/Fragment/v Landroid/view/View;
aload 1
iload 2
putfield android/support/v4/app/Fragment/w I
aload 9
new android/support/v4/app/bq
dup
aload 0
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 9
aload 1
invokespecial android/support/v4/app/bq/<init>(Landroid/support/v4/app/bl;Landroid/view/View;Landroid/view/animation/Animation;Landroid/support/v4/app/Fragment;)V
invokevirtual android/view/animation/Animation/setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 9
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
L75:
aload 1
getfield android/support/v4/app/Fragment/ab Landroid/view/ViewGroup;
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual android/view/ViewGroup/removeView(Landroid/view/View;)V
L72:
aload 1
aconst_null
putfield android/support/v4/app/Fragment/ab Landroid/view/ViewGroup;
aload 1
aconst_null
putfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 1
aconst_null
putfield android/support/v4/app/Fragment/ad Landroid/view/View;
goto L53
L60:
getstatic android/support/v4/app/bl/b Z
ifeq L76
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "movefrom CREATED: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L76:
aload 1
getfield android/support/v4/app/Fragment/W Z
ifne L77
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L78
aload 1
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/s()V
L78:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 1
invokevirtual android/support/v4/app/Fragment/u()V
aload 1
getfield android/support/v4/app/Fragment/Z Z
ifne L77
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onDestroy()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L77:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 1
invokevirtual android/support/v4/app/Fragment/c()V
aload 1
getfield android/support/v4/app/Fragment/Z Z
ifne L79
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onDetach()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L79:
iload 2
istore 6
iload 5
ifne L12
aload 1
getfield android/support/v4/app/Fragment/W Z
ifne L80
iload 2
istore 6
aload 1
getfield android/support/v4/app/Fragment/z I
iflt L12
getstatic android/support/v4/app/bl/b Z
ifeq L81
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Freeing fragment index "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L81:
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
aload 1
getfield android/support/v4/app/Fragment/z I
aconst_null
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
ifnonnull L82
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/n Ljava/util/ArrayList;
L82:
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
aload 1
getfield android/support/v4/app/Fragment/z I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
aload 1
getfield android/support/v4/app/Fragment/A Ljava/lang/String;
invokevirtual android/support/v4/app/bh/b(Ljava/lang/String;)V
aload 1
iconst_m1
putfield android/support/v4/app/Fragment/z I
aload 1
aconst_null
putfield android/support/v4/app/Fragment/A Ljava/lang/String;
aload 1
iconst_0
putfield android/support/v4/app/Fragment/F Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/G Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/H Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/I Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/J Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/K Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/L I
aload 1
aconst_null
putfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
aload 1
aconst_null
putfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
aconst_null
putfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 1
iconst_0
putfield android/support/v4/app/Fragment/Q I
aload 1
iconst_0
putfield android/support/v4/app/Fragment/R I
aload 1
aconst_null
putfield android/support/v4/app/Fragment/S Ljava/lang/String;
aload 1
iconst_0
putfield android/support/v4/app/Fragment/T Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/U Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/W Z
aload 1
aconst_null
putfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
aload 1
iconst_0
putfield android/support/v4/app/Fragment/ah Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/ai Z
iload 2
istore 6
goto L12
L80:
aload 1
aconst_null
putfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 1
aconst_null
putfield android/support/v4/app/Fragment/P Landroid/support/v4/app/Fragment;
aload 1
aconst_null
putfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
aload 1
aconst_null
putfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iload 2
istore 6
goto L12
L73:
aconst_null
astore 9
goto L74
L32:
aconst_null
astore 9
goto L33
.limit locals 12
.limit stack 7
.end method

.method public final a(Landroid/support/v4/app/Fragment;Z)V
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnonnull L0
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/m Ljava/util/ArrayList;
L0:
getstatic android/support/v4/app/bl/b Z
ifeq L1
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "add: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 1
getfield android/support/v4/app/Fragment/z I
ifge L2
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
ifnull L3
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L4
L3:
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnonnull L5
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/l Ljava/util/ArrayList;
L5:
aload 1
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
aload 0
getfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/a(ILandroid/support/v4/app/Fragment;)V
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L6:
getstatic android/support/v4/app/bl/b Z
ifeq L2
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Allocated fragment index "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L2:
aload 1
getfield android/support/v4/app/Fragment/U Z
ifne L7
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L8
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Fragment already added: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 1
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
aload 0
getfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/a(ILandroid/support/v4/app/Fragment;)V
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
aload 1
getfield android/support/v4/app/Fragment/z I
aload 1
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
goto L6
L8:
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 1
iconst_1
putfield android/support/v4/app/Fragment/F Z
aload 1
iconst_0
putfield android/support/v4/app/Fragment/G Z
aload 1
getfield android/support/v4/app/Fragment/X Z
ifeq L9
aload 1
getfield android/support/v4/app/Fragment/Y Z
ifeq L9
aload 0
iconst_1
putfield android/support/v4/app/bl/y Z
L9:
iload 2
ifeq L7
aload 0
aload 1
invokespecial android/support/v4/app/bl/c(Landroid/support/v4/app/Fragment;)V
L7:
return
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/support/v4/app/bh;Landroid/support/v4/app/bf;Landroid/support/v4/app/Fragment;)V
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
ifnull L0
new java/lang/IllegalStateException
dup
ldc "Already attached"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
aload 0
aload 2
putfield android/support/v4/app/bl/w Landroid/support/v4/app/bf;
aload 0
aload 3
putfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
return
.limit locals 4
.limit stack 3
.end method

.method public final a(Landroid/support/v4/app/bk;)V
aload 0
getfield android/support/v4/app/bl/s Ljava/util/ArrayList;
ifnonnull L0
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/s Ljava/util/ArrayList;
L0:
aload 0
getfield android/support/v4/app/bl/s Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Ljava/lang/Runnable;Z)V
.catch all from L0 to L1 using L2
.catch all from L1 to L2 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L2
.catch all from L7 to L8 using L2
iload 2
ifne L9
aload 0
invokespecial android/support/v4/app/bl/u()V
L9:
aload 0
monitorenter
L0:
aload 0
getfield android/support/v4/app/bl/A Z
ifne L1
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
ifnonnull L5
L1:
new java/lang/IllegalStateException
dup
ldc "Activity has been destroyed"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
L3:
aload 0
monitorexit
L4:
aload 1
athrow
L5:
aload 0
getfield android/support/v4/app/bl/i Ljava/util/ArrayList;
ifnonnull L6
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/i Ljava/util/ArrayList;
L6:
aload 0
getfield android/support/v4/app/bl/i Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
getfield android/support/v4/app/bl/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
if_icmpne L7
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/d Landroid/os/Handler;
aload 0
getfield android/support/v4/app/bl/F Ljava/lang/Runnable;
invokevirtual android/os/Handler/removeCallbacks(Ljava/lang/Runnable;)V
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/d Landroid/os/Handler;
aload 0
getfield android/support/v4/app/bl/F Ljava/lang/Runnable;
invokevirtual android/os/Handler/post(Ljava/lang/Runnable;)Z
pop
L7:
aload 0
monitorexit
L8:
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
iconst_0
istore 6
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "    "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnull L12
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 7
iload 7
ifle L12
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Active Fragments in "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc ":"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
iconst_0
istore 5
L13:
iload 5
iload 7
if_icmpge L12
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 9
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  #"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
iload 5
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc ": "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
aload 9
ifnull L14
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mFragmentId=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/Q I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc " mContainerId=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/R I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc " mTag="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/S Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mState="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/u I
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc " mIndex="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/z I
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc " mWho="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/A Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc " mBackStackNesting="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/L I
invokevirtual java/io/PrintWriter/println(I)V
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mAdded="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/F Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mRemoving="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/G Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mResumed="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/H Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mFromLayout="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/I Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mInLayout="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/J Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mHidden="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/T Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mDetached="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/U Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mMenuVisible="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/Y Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mHasMenu="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/X Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mRetainInstance="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/V Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mRetaining="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/W Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mUserVisibleHint="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/af Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 9
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
ifnull L15
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mFragmentManager="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L15:
aload 9
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnull L16
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mHost="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L16:
aload 9
getfield android/support/v4/app/Fragment/P Landroid/support/v4/app/Fragment;
ifnull L17
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mParentFragment="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/P Landroid/support/v4/app/Fragment;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L17:
aload 9
getfield android/support/v4/app/Fragment/B Landroid/os/Bundle;
ifnull L18
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mArguments="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/B Landroid/os/Bundle;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L18:
aload 9
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
ifnull L19
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mSavedFragmentState="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L19:
aload 9
getfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
ifnull L20
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mSavedViewState="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L20:
aload 9
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
ifnull L21
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mTarget="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
invokevirtual java/io/PrintWriter/print(Ljava/lang/Object;)V
aload 3
ldc " mTargetRequestCode="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/E I
invokevirtual java/io/PrintWriter/println(I)V
L21:
aload 9
getfield android/support/v4/app/Fragment/aa I
ifeq L22
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mNextAnim="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/aa I
invokevirtual java/io/PrintWriter/println(I)V
L22:
aload 9
getfield android/support/v4/app/Fragment/ab Landroid/view/ViewGroup;
ifnull L23
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mContainer="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/ab Landroid/view/ViewGroup;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L23:
aload 9
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L24
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mView="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L24:
aload 9
getfield android/support/v4/app/Fragment/ad Landroid/view/View;
ifnull L25
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mInnerView="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L25:
aload 9
getfield android/support/v4/app/Fragment/v Landroid/view/View;
ifnull L26
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mAnimatingAway="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/v Landroid/view/View;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mStateAfterAnimating="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 9
getfield android/support/v4/app/Fragment/w I
invokevirtual java/io/PrintWriter/println(I)V
L26:
aload 9
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
ifnull L27
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Loader Manager:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 9
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
aload 3
aload 4
invokevirtual android/support/v4/app/ct/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
L27:
aload 9
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L14
aload 3
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
new java/lang/StringBuilder
dup
ldc "Child "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 9
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 9
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
aload 3
aload 4
invokevirtual android/support/v4/app/bl/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
L14:
iload 5
iconst_1
iadd
istore 5
goto L13
L12:
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L28
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 7
iload 7
ifle L28
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Added Fragments:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
iconst_0
istore 5
L29:
iload 5
iload 7
if_icmpge L28
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 2
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  #"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
iload 5
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc ": "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 2
invokevirtual android/support/v4/app/Fragment/toString()Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
iload 5
iconst_1
iadd
istore 5
goto L29
L28:
aload 0
getfield android/support/v4/app/bl/p Ljava/util/ArrayList;
ifnull L30
aload 0
getfield android/support/v4/app/bl/p Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 7
iload 7
ifle L30
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Fragments Created Menus:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
iconst_0
istore 5
L31:
iload 5
iload 7
if_icmpge L30
aload 0
getfield android/support/v4/app/bl/p Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 2
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  #"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
iload 5
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc ": "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 2
invokevirtual android/support/v4/app/Fragment/toString()Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
iload 5
iconst_1
iadd
istore 5
goto L31
L30:
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
ifnull L32
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 7
iload 7
ifle L32
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Back Stack:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
iconst_0
istore 5
L33:
iload 5
iload 7
if_icmpge L32
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/ak
astore 2
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  #"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
iload 5
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc ": "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 2
invokevirtual android/support/v4/app/ak/toString()Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 2
aload 8
aload 3
invokevirtual android/support/v4/app/ak/a(Ljava/lang/String;Ljava/io/PrintWriter;)V
iload 5
iconst_1
iadd
istore 5
goto L33
L32:
aload 0
monitorenter
L0:
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
ifnull L7
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 7
L1:
iload 7
ifle L7
L3:
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Back Stack Indices:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
L4:
iconst_0
istore 5
L34:
iload 5
iload 7
if_icmpge L7
L5:
aload 0
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/ak
astore 2
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  #"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
iload 5
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc ": "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 2
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L6:
iload 5
iconst_1
iadd
istore 5
goto L34
L7:
aload 0
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
ifnull L8
aload 0
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L8
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mAvailBackStackIndices: "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/toArray()[Ljava/lang/Object;
invokestatic java/util/Arrays/toString([Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
L8:
aload 0
monitorexit
L9:
aload 0
getfield android/support/v4/app/bl/i Ljava/util/ArrayList;
ifnull L35
aload 0
getfield android/support/v4/app/bl/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 7
iload 7
ifle L35
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Pending Actions:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
iload 6
istore 5
L36:
iload 5
iload 7
if_icmpge L35
aload 0
getfield android/support/v4/app/bl/i Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Runnable
astore 2
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  #"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
iload 5
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc ": "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 2
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
iload 5
iconst_1
iadd
istore 5
goto L36
L2:
astore 1
L10:
aload 0
monitorexit
L11:
aload 1
athrow
L35:
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "FragmentManager misc state:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  mHost="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  mContainer="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bl/w Landroid/support/v4/app/bf;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
aload 0
getfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
ifnull L37
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  mParent="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L37:
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  mCurState="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bl/t I
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc " mStateSaved="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bl/z Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mDestroyed="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bl/A Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 0
getfield android/support/v4/app/bl/y Z
ifeq L38
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  mNeedMenuInvalidate="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bl/y Z
invokevirtual java/io/PrintWriter/println(Z)V
L38:
aload 0
getfield android/support/v4/app/bl/B Ljava/lang/String;
ifnull L39
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  mNoTransactionsBecause="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bl/B Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
L39:
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
ifnull L40
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L40
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  mAvailIndices: "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bl/n Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/toArray()[Ljava/lang/Object;
invokestatic java/util/Arrays/toString([Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
L40:
return
.limit locals 10
.limit stack 5
.end method

.method public final a(II)Z
aload 0
invokespecial android/support/v4/app/bl/u()V
aload 0
invokevirtual android/support/v4/app/bl/k()Z
pop
iload 1
ifge L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Bad id: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aconst_null
iload 1
iload 2
invokevirtual android/support/v4/app/bl/a(Ljava/lang/String;II)Z
ireturn
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/view/Menu;)Z
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L0
iconst_0
istore 4
iconst_0
istore 5
L1:
iload 5
istore 6
iload 4
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L2
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 7
iload 5
istore 6
aload 7
ifnull L3
aload 7
getfield android/support/v4/app/Fragment/T Z
ifne L4
aload 7
getfield android/support/v4/app/Fragment/X Z
ifeq L5
aload 7
getfield android/support/v4/app/Fragment/Y Z
ifeq L5
iconst_1
istore 2
L6:
iload 2
istore 3
aload 7
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L7
iload 2
aload 7
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/a(Landroid/view/Menu;)Z
ior
istore 3
L7:
iload 5
istore 6
iload 3
ifeq L3
iconst_1
istore 6
L3:
iload 4
iconst_1
iadd
istore 4
iload 6
istore 5
goto L1
L0:
iconst_0
istore 6
L2:
iload 6
ireturn
L5:
iconst_0
istore 2
goto L6
L4:
iconst_0
istore 3
goto L7
.limit locals 8
.limit stack 3
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
iconst_0
istore 6
aconst_null
astore 10
aconst_null
astore 9
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L0
iconst_0
istore 5
iconst_0
istore 7
L1:
aload 9
astore 10
iload 7
istore 8
iload 5
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L2
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 11
aload 11
ifnull L3
aload 11
getfield android/support/v4/app/Fragment/T Z
ifne L4
aload 11
getfield android/support/v4/app/Fragment/X Z
ifeq L5
aload 11
getfield android/support/v4/app/Fragment/Y Z
ifeq L5
aload 11
aload 1
aload 2
invokevirtual android/support/v4/app/Fragment/a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
iconst_1
istore 3
L6:
iload 3
istore 4
aload 11
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L7
iload 3
aload 11
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
aload 2
invokevirtual android/support/v4/app/bl/a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
ior
istore 4
L7:
iload 4
ifeq L3
aload 9
astore 10
aload 9
ifnonnull L8
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 10
L8:
aload 10
aload 11
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iconst_1
istore 7
aload 10
astore 9
L9:
iload 5
iconst_1
iadd
istore 5
goto L1
L0:
iconst_0
istore 8
L2:
aload 0
getfield android/support/v4/app/bl/p Ljava/util/ArrayList;
ifnull L10
iload 6
istore 3
L11:
iload 3
aload 0
getfield android/support/v4/app/bl/p Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L10
aload 0
getfield android/support/v4/app/bl/p Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 1
aload 10
ifnull L12
aload 10
aload 1
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L13
L12:
invokestatic android/support/v4/app/Fragment/v()V
L13:
iload 3
iconst_1
iadd
istore 3
goto L11
L10:
aload 0
aload 10
putfield android/support/v4/app/bl/p Ljava/util/ArrayList;
iload 8
ireturn
L3:
goto L9
L5:
iconst_0
istore 3
goto L6
L4:
iconst_0
istore 4
goto L7
.limit locals 12
.limit stack 4
.end method

.method public final a(Landroid/view/MenuItem;)Z
iconst_0
istore 5
iload 5
istore 4
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L0
iconst_0
istore 3
L1:
iload 5
istore 4
iload 3
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L0
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 6
aload 6
ifnull L2
aload 6
getfield android/support/v4/app/Fragment/T Z
ifne L3
aload 6
getfield android/support/v4/app/Fragment/X Z
ifeq L4
aload 6
getfield android/support/v4/app/Fragment/Y Z
ifeq L4
aload 6
aload 1
invokevirtual android/support/v4/app/Fragment/a(Landroid/view/MenuItem;)Z
ifeq L4
iconst_1
istore 2
L5:
iload 2
ifeq L2
iconst_1
istore 4
L0:
iload 4
ireturn
L4:
aload 6
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L3
aload 6
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/a(Landroid/view/MenuItem;)Z
ifeq L3
iconst_1
istore 2
goto L5
L3:
iconst_0
istore 2
goto L5
L2:
iload 3
iconst_1
iadd
istore 3
goto L1
.limit locals 7
.limit stack 2
.end method

.method public final a(Ljava/lang/String;I)Z
aload 0
invokespecial android/support/v4/app/bl/u()V
aload 0
invokevirtual android/support/v4/app/bl/k()Z
pop
aload 0
aload 1
iconst_m1
iload 2
invokevirtual android/support/v4/app/bl/a(Ljava/lang/String;II)Z
ireturn
.limit locals 3
.limit stack 4
.end method

.method final a(Ljava/lang/String;II)Z
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
ifnonnull L0
L1:
iconst_0
ireturn
L0:
aload 1
ifnonnull L2
iload 2
ifge L2
iload 3
iconst_1
iand
ifne L2
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 2
iload 2
iflt L1
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
checkcast android/support/v4/app/ak
astore 1
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 7
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 8
aload 1
aload 7
aload 8
invokevirtual android/support/v4/app/ak/a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
aload 1
iconst_1
aconst_null
aload 7
aload 8
invokevirtual android/support/v4/app/ak/a(ZLandroid/support/v4/app/ap;Landroid/util/SparseArray;Landroid/util/SparseArray;)Landroid/support/v4/app/ap;
pop
L3:
aload 0
invokevirtual android/support/v4/app/bl/l()V
iconst_1
ireturn
L2:
iconst_m1
istore 4
aload 1
ifnonnull L4
iload 2
iflt L5
L4:
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 5
L6:
iload 5
iflt L7
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/ak
astore 7
aload 1
ifnull L8
aload 1
aload 7
getfield android/support/v4/app/ak/w Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L7
L8:
iload 2
iflt L9
iload 2
aload 7
getfield android/support/v4/app/ak/y I
if_icmpeq L7
L9:
iload 5
iconst_1
isub
istore 5
goto L6
L7:
iload 5
iflt L1
iload 5
istore 4
iload 3
iconst_1
iand
ifeq L5
iload 5
iconst_1
isub
istore 3
L10:
iload 3
istore 4
iload 3
iflt L5
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/ak
astore 7
aload 1
ifnull L11
aload 1
aload 7
getfield android/support/v4/app/ak/w Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L12
L11:
iload 3
istore 4
iload 2
iflt L5
iload 3
istore 4
iload 2
aload 7
getfield android/support/v4/app/ak/y I
if_icmpne L5
L12:
iload 3
iconst_1
isub
istore 3
goto L10
L5:
iload 4
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
if_icmpeq L1
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 7
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 2
L13:
iload 2
iload 4
if_icmple L14
aload 7
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 2
iconst_1
isub
istore 2
goto L13
L14:
aload 7
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 3
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 8
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 9
iconst_0
istore 2
L15:
iload 2
iload 3
if_icmpgt L16
aload 7
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/ak
aload 8
aload 9
invokevirtual android/support/v4/app/ak/a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
iload 2
iconst_1
iadd
istore 2
goto L15
L16:
aconst_null
astore 1
iconst_0
istore 2
L17:
iload 2
iload 3
if_icmpgt L3
getstatic android/support/v4/app/bl/b Z
ifeq L18
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Popping back stack state: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L18:
aload 7
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/ak
astore 10
iload 2
iload 3
if_icmpne L19
iconst_1
istore 6
L20:
aload 10
iload 6
aload 1
aload 8
aload 9
invokevirtual android/support/v4/app/ak/a(ZLandroid/support/v4/app/ap;Landroid/util/SparseArray;Landroid/util/SparseArray;)Landroid/support/v4/app/ap;
astore 1
iload 2
iconst_1
iadd
istore 2
goto L17
L19:
iconst_0
istore 6
goto L20
.limit locals 11
.limit stack 5
.end method

.method public final b(I)V
iload 1
ifge L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Bad id: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
new android/support/v4/app/bp
dup
aload 0
iload 1
invokespecial android/support/v4/app/bp/<init>(Landroid/support/v4/app/bl;I)V
iconst_0
invokevirtual android/support/v4/app/bl/a(Ljava/lang/Runnable;Z)V
return
.limit locals 2
.limit stack 5
.end method

.method public final b(Landroid/support/v4/app/Fragment;)V
aload 1
getfield android/support/v4/app/Fragment/ae Z
ifeq L0
aload 0
getfield android/support/v4/app/bl/k Z
ifeq L1
aload 0
iconst_1
putfield android/support/v4/app/bl/C Z
L0:
return
L1:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/ae Z
aload 0
aload 1
aload 0
getfield android/support/v4/app/bl/t I
iconst_0
iconst_0
iconst_0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;IIIZ)V
return
.limit locals 2
.limit stack 6
.end method

.method public final b(Landroid/support/v4/app/Fragment;II)V
getstatic android/support/v4/app/bl/b Z
ifeq L0
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "hide: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 1
getfield android/support/v4/app/Fragment/T Z
ifne L1
aload 1
iconst_1
putfield android/support/v4/app/Fragment/T Z
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L2
aload 0
aload 1
iload 2
iconst_0
iload 3
invokespecial android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;
astore 4
aload 4
ifnull L3
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 4
invokestatic android/support/v4/app/bl/b(Landroid/view/View;Landroid/view/animation/Animation;)V
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 4
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
L3:
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
bipush 8
invokevirtual android/view/View/setVisibility(I)V
L2:
aload 1
getfield android/support/v4/app/Fragment/F Z
ifeq L4
aload 1
getfield android/support/v4/app/Fragment/X Z
ifeq L4
aload 1
getfield android/support/v4/app/Fragment/Y Z
ifeq L4
aload 0
iconst_1
putfield android/support/v4/app/bl/y Z
L4:
invokestatic android/support/v4/app/Fragment/m()V
L1:
return
.limit locals 5
.limit stack 5
.end method

.method public final b(Landroid/support/v4/app/bk;)V
aload 0
getfield android/support/v4/app/bl/s Ljava/util/ArrayList;
ifnull L0
aload 0
getfield android/support/v4/app/bl/s Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Landroid/view/Menu;)V
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L0
iconst_0
istore 2
L1:
iload 2
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L0
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 3
aload 3
ifnull L2
aload 3
getfield android/support/v4/app/Fragment/T Z
ifne L2
aload 3
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L2
aload 3
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/b(Landroid/view/Menu;)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L1
L0:
return
.limit locals 4
.limit stack 2
.end method

.method public final b()Z
aload 0
invokevirtual android/support/v4/app/bl/k()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(Landroid/view/MenuItem;)Z
iconst_0
istore 5
iload 5
istore 4
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L0
iconst_0
istore 2
L1:
iload 5
istore 4
iload 2
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L0
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 6
aload 6
ifnull L2
aload 6
getfield android/support/v4/app/Fragment/T Z
ifne L3
aload 6
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L3
aload 6
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/b(Landroid/view/MenuItem;)Z
ifeq L3
iconst_1
istore 3
L4:
iload 3
ifeq L2
iconst_1
istore 4
L0:
iload 4
ireturn
L3:
iconst_0
istore 3
goto L4
L2:
iload 2
iconst_1
iadd
istore 2
goto L1
.limit locals 7
.limit stack 2
.end method

.method public final c()V
aload 0
new android/support/v4/app/bn
dup
aload 0
invokespecial android/support/v4/app/bn/<init>(Landroid/support/v4/app/bl;)V
iconst_0
invokevirtual android/support/v4/app/bl/a(Ljava/lang/Runnable;Z)V
return
.limit locals 1
.limit stack 4
.end method

.method final c(I)V
aload 0
iload 1
iconst_0
iconst_0
iconst_0
invokevirtual android/support/v4/app/bl/a(IIIZ)V
return
.limit locals 2
.limit stack 5
.end method

.method public final c(Landroid/support/v4/app/Fragment;II)V
getstatic android/support/v4/app/bl/b Z
ifeq L0
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "show: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 1
getfield android/support/v4/app/Fragment/T Z
ifeq L1
aload 1
iconst_0
putfield android/support/v4/app/Fragment/T Z
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L2
aload 0
aload 1
iload 2
iconst_1
iload 3
invokespecial android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;
astore 4
aload 4
ifnull L3
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 4
invokestatic android/support/v4/app/bl/b(Landroid/view/View;Landroid/view/animation/Animation;)V
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 4
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
L3:
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
iconst_0
invokevirtual android/view/View/setVisibility(I)V
L2:
aload 1
getfield android/support/v4/app/Fragment/F Z
ifeq L4
aload 1
getfield android/support/v4/app/Fragment/X Z
ifeq L4
aload 1
getfield android/support/v4/app/Fragment/Y Z
ifeq L4
aload 0
iconst_1
putfield android/support/v4/app/bl/y Z
L4:
invokestatic android/support/v4/app/Fragment/m()V
L1:
return
.limit locals 5
.limit stack 5
.end method

.method public final d(Landroid/support/v4/app/Fragment;II)V
getstatic android/support/v4/app/bl/b Z
ifeq L0
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "detach: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 1
getfield android/support/v4/app/Fragment/U Z
ifne L1
aload 1
iconst_1
putfield android/support/v4/app/Fragment/U Z
aload 1
getfield android/support/v4/app/Fragment/F Z
ifeq L1
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L2
getstatic android/support/v4/app/bl/b Z
ifeq L3
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "remove from detach: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L3:
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
L2:
aload 1
getfield android/support/v4/app/Fragment/X Z
ifeq L4
aload 1
getfield android/support/v4/app/Fragment/Y Z
ifeq L4
aload 0
iconst_1
putfield android/support/v4/app/bl/y Z
L4:
aload 1
iconst_0
putfield android/support/v4/app/Fragment/F Z
aload 0
aload 1
iconst_1
iload 2
iload 3
iconst_0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;IIIZ)V
L1:
return
.limit locals 4
.limit stack 6
.end method

.method public final d()Z
aload 0
invokespecial android/support/v4/app/bl/u()V
aload 0
invokevirtual android/support/v4/app/bl/k()Z
pop
aload 0
aconst_null
iconst_m1
iconst_0
invokevirtual android/support/v4/app/bl/a(Ljava/lang/String;II)Z
ireturn
.limit locals 1
.limit stack 4
.end method

.method public final e()V
aload 0
new android/support/v4/app/bo
dup
aload 0
invokespecial android/support/v4/app/bo/<init>(Landroid/support/v4/app/bl;)V
iconst_0
invokevirtual android/support/v4/app/bl/a(Ljava/lang/Runnable;Z)V
return
.limit locals 1
.limit stack 4
.end method

.method public final e(Landroid/support/v4/app/Fragment;II)V
getstatic android/support/v4/app/bl/b Z
ifeq L0
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "attach: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 1
getfield android/support/v4/app/Fragment/U Z
ifeq L1
aload 1
iconst_0
putfield android/support/v4/app/Fragment/U Z
aload 1
getfield android/support/v4/app/Fragment/F Z
ifne L1
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnonnull L2
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/m Ljava/util/ArrayList;
L2:
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L3
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Fragment already added: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
getstatic android/support/v4/app/bl/b Z
ifeq L4
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "add from attach: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L4:
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 1
iconst_1
putfield android/support/v4/app/Fragment/F Z
aload 1
getfield android/support/v4/app/Fragment/X Z
ifeq L5
aload 1
getfield android/support/v4/app/Fragment/Y Z
ifeq L5
aload 0
iconst_1
putfield android/support/v4/app/bl/y Z
L5:
aload 0
aload 1
aload 0
getfield android/support/v4/app/bl/t I
iload 2
iload 3
iconst_0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;IIIZ)V
L1:
return
.limit locals 4
.limit stack 6
.end method

.method public final f()I
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
ifnull L0
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final g()Landroid/support/v4/app/bj;
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/bj
areturn
.limit locals 1
.limit stack 2
.end method

.method public final h()Ljava/util/List;
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final i()Z
aload 0
getfield android/support/v4/app/bl/A Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method final j()V
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnonnull L0
L1:
return
L0:
iconst_0
istore 1
L2:
iload 1
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L1
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 2
aload 2
ifnull L3
aload 0
aload 2
invokevirtual android/support/v4/app/bl/b(Landroid/support/v4/app/Fragment;)V
L3:
iload 1
iconst_1
iadd
istore 1
goto L2
.limit locals 3
.limit stack 2
.end method

.method public final k()Z
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
aload 0
getfield android/support/v4/app/bl/k Z
ifeq L10
new java/lang/IllegalStateException
dup
ldc "Recursive entry to executePendingTransactions"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L10:
invokestatic android/os/Looper/myLooper()Landroid/os/Looper;
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/d Landroid/os/Handler;
invokevirtual android/os/Handler/getLooper()Landroid/os/Looper;
if_acmpeq L11
new java/lang/IllegalStateException
dup
ldc "Must be called from main thread of process"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L11:
iconst_0
istore 4
L12:
aload 0
monitorenter
L0:
aload 0
getfield android/support/v4/app/bl/i Ljava/util/ArrayList;
ifnull L1
aload 0
getfield android/support/v4/app/bl/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L4
L1:
aload 0
monitorexit
L3:
aload 0
getfield android/support/v4/app/bl/C Z
ifeq L13
iconst_0
istore 1
iconst_0
istore 2
L14:
iload 1
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L15
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 5
iload 2
istore 3
aload 5
ifnull L16
iload 2
istore 3
aload 5
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
ifnull L16
iload 2
aload 5
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/a()Z
ior
istore 3
L16:
iload 1
iconst_1
iadd
istore 1
iload 3
istore 2
goto L14
L4:
aload 0
getfield android/support/v4/app/bl/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 2
aload 0
getfield android/support/v4/app/bl/j [Ljava/lang/Runnable;
ifnull L5
aload 0
getfield android/support/v4/app/bl/j [Ljava/lang/Runnable;
arraylength
iload 2
if_icmpge L6
L5:
aload 0
iload 2
anewarray java/lang/Runnable
putfield android/support/v4/app/bl/j [Ljava/lang/Runnable;
L6:
aload 0
getfield android/support/v4/app/bl/i Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/bl/j [Ljava/lang/Runnable;
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
pop
aload 0
getfield android/support/v4/app/bl/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/d Landroid/os/Handler;
aload 0
getfield android/support/v4/app/bl/F Ljava/lang/Runnable;
invokevirtual android/os/Handler/removeCallbacks(Ljava/lang/Runnable;)V
aload 0
monitorexit
L7:
aload 0
iconst_1
putfield android/support/v4/app/bl/k Z
iconst_0
istore 1
L17:
iload 1
iload 2
if_icmpge L18
aload 0
getfield android/support/v4/app/bl/j [Ljava/lang/Runnable;
iload 1
aaload
invokeinterface java/lang/Runnable/run()V 0
aload 0
getfield android/support/v4/app/bl/j [Ljava/lang/Runnable;
iload 1
aconst_null
aastore
iload 1
iconst_1
iadd
istore 1
goto L17
L2:
astore 5
L8:
aload 0
monitorexit
L9:
aload 5
athrow
L18:
aload 0
iconst_0
putfield android/support/v4/app/bl/k Z
iconst_1
istore 4
goto L12
L15:
iload 2
ifne L13
aload 0
iconst_0
putfield android/support/v4/app/bl/C Z
aload 0
invokevirtual android/support/v4/app/bl/j()V
L13:
iload 4
ireturn
.limit locals 6
.limit stack 3
.end method

.method final l()V
aload 0
getfield android/support/v4/app/bl/s Ljava/util/ArrayList;
ifnull L0
iconst_0
istore 1
L1:
iload 1
aload 0
getfield android/support/v4/app/bl/s Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L0
aload 0
getfield android/support/v4/app/bl/s Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/bk
invokeinterface android/support/v4/app/bk/a()V 0
iload 1
iconst_1
iadd
istore 1
goto L1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method final m()Landroid/os/Parcelable;
aconst_null
astore 6
aload 0
invokevirtual android/support/v4/app/bl/k()Z
pop
getstatic android/support/v4/app/bl/d Z
ifeq L0
aload 0
iconst_1
putfield android/support/v4/app/bl/z Z
L0:
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnull L1
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L2
L1:
aconst_null
areturn
L2:
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 3
iload 3
anewarray android/support/v4/app/FragmentState
astore 7
iconst_0
istore 2
iconst_0
istore 1
L3:
iload 2
iload 3
if_icmpge L4
aload 0
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 4
aload 4
ifnull L5
aload 4
getfield android/support/v4/app/Fragment/z I
ifge L6
aload 0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Failure saving state: active "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " has cleared index: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
getfield android/support/v4/app/Fragment/z I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
invokespecial android/support/v4/app/bl/a(Ljava/lang/RuntimeException;)V
L6:
new android/support/v4/app/FragmentState
dup
aload 4
invokespecial android/support/v4/app/FragmentState/<init>(Landroid/support/v4/app/Fragment;)V
astore 5
aload 7
iload 2
aload 5
aastore
aload 4
getfield android/support/v4/app/Fragment/u I
ifle L7
aload 5
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
ifnonnull L7
aload 5
aload 0
aload 4
invokespecial android/support/v4/app/bl/g(Landroid/support/v4/app/Fragment;)Landroid/os/Bundle;
putfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
aload 4
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
ifnull L8
aload 4
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
getfield android/support/v4/app/Fragment/z I
ifge L9
aload 0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Failure saving state: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " has target not in fragment manager: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
invokespecial android/support/v4/app/bl/a(Ljava/lang/RuntimeException;)V
L9:
aload 5
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
ifnonnull L10
aload 5
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
putfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
L10:
aload 0
aload 5
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
ldc "android:target_state"
aload 4
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/bl/a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
aload 4
getfield android/support/v4/app/Fragment/E I
ifeq L8
aload 5
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
ldc "android:target_req_state"
aload 4
getfield android/support/v4/app/Fragment/E I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L8:
getstatic android/support/v4/app/bl/b Z
ifeq L11
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Saved state of "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
getfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L11:
iconst_1
istore 1
L12:
iload 2
iconst_1
iadd
istore 2
goto L3
L7:
aload 5
aload 4
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
putfield android/support/v4/app/FragmentState/j Landroid/os/Bundle;
goto L8
L4:
iload 1
ifne L13
getstatic android/support/v4/app/bl/b Z
ifeq L1
ldc "FragmentManager"
ldc "saveAllState: no fragments!"
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
L13:
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L14
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 2
iload 2
ifle L14
iload 2
newarray int
astore 5
iconst_0
istore 1
L15:
aload 5
astore 4
iload 1
iload 2
if_icmpge L16
aload 5
iload 1
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
getfield android/support/v4/app/Fragment/z I
iastore
aload 5
iload 1
iaload
ifge L17
aload 0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Failure saving state: active "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " has cleared index: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
iload 1
iaload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
invokespecial android/support/v4/app/bl/a(Ljava/lang/RuntimeException;)V
L17:
getstatic android/support/v4/app/bl/b Z
ifeq L18
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "saveAllState: adding fragment #"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L18:
iload 1
iconst_1
iadd
istore 1
goto L15
L14:
aconst_null
astore 4
L16:
aload 6
astore 5
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
ifnull L19
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 2
aload 6
astore 5
iload 2
ifle L19
iload 2
anewarray android/support/v4/app/BackStackState
astore 6
iconst_0
istore 1
L20:
aload 6
astore 5
iload 1
iload 2
if_icmpge L19
aload 6
iload 1
new android/support/v4/app/BackStackState
dup
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/ak
invokespecial android/support/v4/app/BackStackState/<init>(Landroid/support/v4/app/ak;)V
aastore
getstatic android/support/v4/app/bl/b Z
ifeq L21
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "saveAllState: adding back stack #"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L21:
iload 1
iconst_1
iadd
istore 1
goto L20
L19:
new android/support/v4/app/FragmentManagerState
dup
invokespecial android/support/v4/app/FragmentManagerState/<init>()V
astore 6
aload 6
aload 7
putfield android/support/v4/app/FragmentManagerState/a [Landroid/support/v4/app/FragmentState;
aload 6
aload 4
putfield android/support/v4/app/FragmentManagerState/b [I
aload 6
aload 5
putfield android/support/v4/app/FragmentManagerState/c [Landroid/support/v4/app/BackStackState;
aload 6
areturn
L5:
goto L12
.limit locals 8
.limit stack 6
.end method

.method public final n()V
aload 0
iconst_0
putfield android/support/v4/app/bl/z Z
aload 0
iconst_1
invokevirtual android/support/v4/app/bl/c(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public final o()V
aload 0
iconst_0
putfield android/support/v4/app/bl/z Z
aload 0
iconst_2
invokevirtual android/support/v4/app/bl/c(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public final p()V
aload 0
iconst_0
putfield android/support/v4/app/bl/z Z
aload 0
iconst_4
invokevirtual android/support/v4/app/bl/c(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public final q()V
aload 0
iconst_0
putfield android/support/v4/app/bl/z Z
aload 0
iconst_5
invokevirtual android/support/v4/app/bl/c(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public final r()V
aload 0
iconst_1
putfield android/support/v4/app/bl/z Z
aload 0
iconst_3
invokevirtual android/support/v4/app/bl/c(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public final s()V
aload 0
iconst_1
putfield android/support/v4/app/bl/A Z
aload 0
invokevirtual android/support/v4/app/bl/k()Z
pop
aload 0
iconst_0
invokevirtual android/support/v4/app/bl/c(I)V
aload 0
aconst_null
putfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
aload 0
aconst_null
putfield android/support/v4/app/bl/w Landroid/support/v4/app/bf;
aload 0
aconst_null
putfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
return
.limit locals 1
.limit stack 2
.end method

.method public final t()V
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L0
iconst_0
istore 1
L1:
iload 1
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L0
aload 0
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 2
aload 2
ifnull L2
aload 2
invokevirtual android/support/v4/app/Fragment/onLowMemory()V
aload 2
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L2
aload 2
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/t()V
L2:
iload 1
iconst_1
iadd
istore 1
goto L1
L0:
return
.limit locals 3
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
sipush 128
invokespecial java/lang/StringBuilder/<init>(I)V
astore 1
aload 1
ldc "FragmentManager{"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
ldc " in "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
getfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
ifnull L0
aload 0
getfield android/support/v4/app/bl/x Landroid/support/v4/app/Fragment;
aload 1
invokestatic android/support/v4/n/g/a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
L1:
aload 1
ldc "}}"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
aload 0
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
aload 1
invokestatic android/support/v4/n/g/a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
goto L1
.limit locals 2
.limit stack 3
.end method
