.bytecode 50.0
.class public final synchronized android/support/v4/n/i
.super java/lang/Object
.implements java/lang/Cloneable

.field private static final 'a' Ljava/lang/Object;

.field private 'b' Z

.field private 'c' [J

.field private 'd' [Ljava/lang/Object;

.field private 'e' I

.method static <clinit>()V
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putstatic android/support/v4/n/i/a Ljava/lang/Object;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
iconst_0
invokespecial android/support/v4/n/i/<init>(B)V
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v4/n/i/b Z
bipush 10
invokestatic android/support/v4/n/f/b(I)I
istore 1
aload 0
iload 1
newarray long
putfield android/support/v4/n/i/c [J
aload 0
iload 1
anewarray java/lang/Object
putfield android/support/v4/n/i/d [Ljava/lang/Object;
aload 0
iconst_0
putfield android/support/v4/n/i/e I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)I
aload 0
getfield android/support/v4/n/i/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/i/b()V
L0:
iconst_0
istore 2
L1:
iload 2
aload 0
getfield android/support/v4/n/i/e I
if_icmpge L2
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 2
aaload
aload 1
if_acmpne L3
iload 2
ireturn
L3:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
iconst_m1
ireturn
.limit locals 3
.limit stack 2
.end method

.method private a()Landroid/support/v4/n/i;
.catch java/lang/CloneNotSupportedException from L0 to L1 using L2
.catch java/lang/CloneNotSupportedException from L1 to L3 using L4
L0:
aload 0
invokespecial java/lang/Object/clone()Ljava/lang/Object;
checkcast android/support/v4/n/i
astore 1
L1:
aload 1
aload 0
getfield android/support/v4/n/i/c [J
invokevirtual [J/clone()Ljava/lang/Object;
checkcast [J
putfield android/support/v4/n/i/c [J
aload 1
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
invokevirtual [Ljava/lang/Object;/clone()Ljava/lang/Object;
checkcast [Ljava/lang/Object;
putfield android/support/v4/n/i/d [Ljava/lang/Object;
L3:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
L4:
astore 2
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method private a(J)Ljava/lang/Object;
aload 0
getfield android/support/v4/n/i/c [J
aload 0
getfield android/support/v4/n/i/e I
lload 1
invokestatic android/support/v4/n/f/a([JIJ)I
istore 3
iload 3
iflt L0
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 3
aaload
getstatic android/support/v4/n/i/a Ljava/lang/Object;
if_acmpne L1
L0:
aconst_null
areturn
L1:
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 3
aaload
areturn
.limit locals 4
.limit stack 4
.end method

.method private a(I)V
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 1
aaload
getstatic android/support/v4/n/i/a Ljava/lang/Object;
if_acmpeq L0
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 1
getstatic android/support/v4/n/i/a Ljava/lang/Object;
aastore
aload 0
iconst_1
putfield android/support/v4/n/i/b Z
L0:
return
.limit locals 2
.limit stack 3
.end method

.method private a(ILjava/lang/Object;)V
aload 0
getfield android/support/v4/n/i/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/i/b()V
L0:
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 1
aload 2
aastore
return
.limit locals 3
.limit stack 3
.end method

.method private a(JLjava/lang/Object;)V
aload 0
getfield android/support/v4/n/i/c [J
aload 0
getfield android/support/v4/n/i/e I
lload 1
invokestatic android/support/v4/n/f/a([JIJ)I
istore 4
iload 4
iflt L0
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 4
aload 3
aastore
return
L0:
iload 4
iconst_m1
ixor
istore 5
iload 5
aload 0
getfield android/support/v4/n/i/e I
if_icmpge L1
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 5
aaload
getstatic android/support/v4/n/i/a Ljava/lang/Object;
if_acmpne L1
aload 0
getfield android/support/v4/n/i/c [J
iload 5
lload 1
lastore
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 5
aload 3
aastore
return
L1:
iload 5
istore 4
aload 0
getfield android/support/v4/n/i/b Z
ifeq L2
iload 5
istore 4
aload 0
getfield android/support/v4/n/i/e I
aload 0
getfield android/support/v4/n/i/c [J
arraylength
if_icmplt L2
aload 0
invokespecial android/support/v4/n/i/b()V
aload 0
getfield android/support/v4/n/i/c [J
aload 0
getfield android/support/v4/n/i/e I
lload 1
invokestatic android/support/v4/n/f/a([JIJ)I
iconst_m1
ixor
istore 4
L2:
aload 0
getfield android/support/v4/n/i/e I
aload 0
getfield android/support/v4/n/i/c [J
arraylength
if_icmplt L3
aload 0
getfield android/support/v4/n/i/e I
iconst_1
iadd
invokestatic android/support/v4/n/f/b(I)I
istore 5
iload 5
newarray long
astore 6
iload 5
anewarray java/lang/Object
astore 7
aload 0
getfield android/support/v4/n/i/c [J
iconst_0
aload 6
iconst_0
aload 0
getfield android/support/v4/n/i/c [J
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iconst_0
aload 7
iconst_0
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 6
putfield android/support/v4/n/i/c [J
aload 0
aload 7
putfield android/support/v4/n/i/d [Ljava/lang/Object;
L3:
aload 0
getfield android/support/v4/n/i/e I
iload 4
isub
ifeq L4
aload 0
getfield android/support/v4/n/i/c [J
iload 4
aload 0
getfield android/support/v4/n/i/c [J
iload 4
iconst_1
iadd
aload 0
getfield android/support/v4/n/i/e I
iload 4
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 4
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 4
iconst_1
iadd
aload 0
getfield android/support/v4/n/i/e I
iload 4
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L4:
aload 0
getfield android/support/v4/n/i/c [J
iload 4
lload 1
lastore
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 4
aload 3
aastore
aload 0
aload 0
getfield android/support/v4/n/i/e I
iconst_1
iadd
putfield android/support/v4/n/i/e I
return
.limit locals 8
.limit stack 6
.end method

.method private b(I)J
aload 0
getfield android/support/v4/n/i/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/i/b()V
L0:
aload 0
getfield android/support/v4/n/i/c [J
iload 1
laload
lreturn
.limit locals 2
.limit stack 2
.end method

.method private b(J)Ljava/lang/Object;
aload 0
getfield android/support/v4/n/i/c [J
aload 0
getfield android/support/v4/n/i/e I
lload 1
invokestatic android/support/v4/n/f/a([JIJ)I
istore 3
iload 3
iflt L0
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 3
aaload
getstatic android/support/v4/n/i/a Ljava/lang/Object;
if_acmpne L1
L0:
aconst_null
areturn
L1:
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 3
aaload
areturn
.limit locals 4
.limit stack 4
.end method

.method private b()V
aload 0
getfield android/support/v4/n/i/e I
istore 4
aload 0
getfield android/support/v4/n/i/c [J
astore 5
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
astore 6
iconst_0
istore 1
iconst_0
istore 2
L0:
iload 1
iload 4
if_icmpge L1
aload 6
iload 1
aaload
astore 7
iload 2
istore 3
aload 7
getstatic android/support/v4/n/i/a Ljava/lang/Object;
if_acmpeq L2
iload 1
iload 2
if_icmpeq L3
aload 5
iload 2
aload 5
iload 1
laload
lastore
aload 6
iload 2
aload 7
aastore
aload 6
iload 1
aconst_null
aastore
L3:
iload 2
iconst_1
iadd
istore 3
L2:
iload 1
iconst_1
iadd
istore 1
iload 3
istore 2
goto L0
L1:
aload 0
iconst_0
putfield android/support/v4/n/i/b Z
aload 0
iload 2
putfield android/support/v4/n/i/e I
return
.limit locals 8
.limit stack 4
.end method

.method private b(JLjava/lang/Object;)V
aload 0
getfield android/support/v4/n/i/e I
ifeq L0
lload 1
aload 0
getfield android/support/v4/n/i/c [J
aload 0
getfield android/support/v4/n/i/e I
iconst_1
isub
laload
lcmp
ifgt L0
aload 0
getfield android/support/v4/n/i/c [J
aload 0
getfield android/support/v4/n/i/e I
lload 1
invokestatic android/support/v4/n/f/a([JIJ)I
istore 4
iload 4
iflt L1
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 4
aload 3
aastore
return
L1:
iload 4
iconst_m1
ixor
istore 5
iload 5
aload 0
getfield android/support/v4/n/i/e I
if_icmpge L2
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 5
aaload
getstatic android/support/v4/n/i/a Ljava/lang/Object;
if_acmpne L2
aload 0
getfield android/support/v4/n/i/c [J
iload 5
lload 1
lastore
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 5
aload 3
aastore
return
L2:
iload 5
istore 4
aload 0
getfield android/support/v4/n/i/b Z
ifeq L3
iload 5
istore 4
aload 0
getfield android/support/v4/n/i/e I
aload 0
getfield android/support/v4/n/i/c [J
arraylength
if_icmplt L3
aload 0
invokespecial android/support/v4/n/i/b()V
aload 0
getfield android/support/v4/n/i/c [J
aload 0
getfield android/support/v4/n/i/e I
lload 1
invokestatic android/support/v4/n/f/a([JIJ)I
iconst_m1
ixor
istore 4
L3:
aload 0
getfield android/support/v4/n/i/e I
aload 0
getfield android/support/v4/n/i/c [J
arraylength
if_icmplt L4
aload 0
getfield android/support/v4/n/i/e I
iconst_1
iadd
invokestatic android/support/v4/n/f/b(I)I
istore 5
iload 5
newarray long
astore 6
iload 5
anewarray java/lang/Object
astore 7
aload 0
getfield android/support/v4/n/i/c [J
iconst_0
aload 6
iconst_0
aload 0
getfield android/support/v4/n/i/c [J
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iconst_0
aload 7
iconst_0
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 6
putfield android/support/v4/n/i/c [J
aload 0
aload 7
putfield android/support/v4/n/i/d [Ljava/lang/Object;
L4:
aload 0
getfield android/support/v4/n/i/e I
iload 4
isub
ifeq L5
aload 0
getfield android/support/v4/n/i/c [J
iload 4
aload 0
getfield android/support/v4/n/i/c [J
iload 4
iconst_1
iadd
aload 0
getfield android/support/v4/n/i/e I
iload 4
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 4
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 4
iconst_1
iadd
aload 0
getfield android/support/v4/n/i/e I
iload 4
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L5:
aload 0
getfield android/support/v4/n/i/c [J
iload 4
lload 1
lastore
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 4
aload 3
aastore
aload 0
aload 0
getfield android/support/v4/n/i/e I
iconst_1
iadd
putfield android/support/v4/n/i/e I
return
L0:
aload 0
getfield android/support/v4/n/i/b Z
ifeq L6
aload 0
getfield android/support/v4/n/i/e I
aload 0
getfield android/support/v4/n/i/c [J
arraylength
if_icmplt L6
aload 0
invokespecial android/support/v4/n/i/b()V
L6:
aload 0
getfield android/support/v4/n/i/e I
istore 4
iload 4
aload 0
getfield android/support/v4/n/i/c [J
arraylength
if_icmplt L7
iload 4
iconst_1
iadd
invokestatic android/support/v4/n/f/b(I)I
istore 5
iload 5
newarray long
astore 6
iload 5
anewarray java/lang/Object
astore 7
aload 0
getfield android/support/v4/n/i/c [J
iconst_0
aload 6
iconst_0
aload 0
getfield android/support/v4/n/i/c [J
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iconst_0
aload 7
iconst_0
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 6
putfield android/support/v4/n/i/c [J
aload 0
aload 7
putfield android/support/v4/n/i/d [Ljava/lang/Object;
L7:
aload 0
getfield android/support/v4/n/i/c [J
iload 4
lload 1
lastore
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 4
aload 3
aastore
aload 0
iload 4
iconst_1
iadd
putfield android/support/v4/n/i/e I
return
.limit locals 8
.limit stack 6
.end method

.method private c()I
aload 0
getfield android/support/v4/n/i/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/i/b()V
L0:
aload 0
getfield android/support/v4/n/i/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c(I)Ljava/lang/Object;
aload 0
getfield android/support/v4/n/i/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/i/b()V
L0:
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 1
aaload
areturn
.limit locals 2
.limit stack 2
.end method

.method private c(J)V
aload 0
getfield android/support/v4/n/i/c [J
aload 0
getfield android/support/v4/n/i/e I
lload 1
invokestatic android/support/v4/n/f/a([JIJ)I
istore 3
iload 3
iflt L0
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 3
aaload
getstatic android/support/v4/n/i/a Ljava/lang/Object;
if_acmpeq L0
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 3
getstatic android/support/v4/n/i/a Ljava/lang/Object;
aastore
aload 0
iconst_1
putfield android/support/v4/n/i/b Z
L0:
return
.limit locals 4
.limit stack 4
.end method

.method private d()V
aload 0
getfield android/support/v4/n/i/e I
istore 2
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
astore 3
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 3
iload 1
aconst_null
aastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
iconst_0
putfield android/support/v4/n/i/e I
aload 0
iconst_0
putfield android/support/v4/n/i/b Z
return
.limit locals 4
.limit stack 3
.end method

.method private d(J)V
aload 0
getfield android/support/v4/n/i/c [J
aload 0
getfield android/support/v4/n/i/e I
lload 1
invokestatic android/support/v4/n/f/a([JIJ)I
istore 3
iload 3
iflt L0
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 3
aaload
getstatic android/support/v4/n/i/a Ljava/lang/Object;
if_acmpeq L0
aload 0
getfield android/support/v4/n/i/d [Ljava/lang/Object;
iload 3
getstatic android/support/v4/n/i/a Ljava/lang/Object;
aastore
aload 0
iconst_1
putfield android/support/v4/n/i/b Z
L0:
return
.limit locals 4
.limit stack 4
.end method

.method private e(J)I
aload 0
getfield android/support/v4/n/i/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/i/b()V
L0:
aload 0
getfield android/support/v4/n/i/c [J
aload 0
getfield android/support/v4/n/i/e I
lload 1
invokestatic android/support/v4/n/f/a([JIJ)I
ireturn
.limit locals 3
.limit stack 4
.end method

.method public final synthetic clone()Ljava/lang/Object;
aload 0
invokespecial android/support/v4/n/i/a()Landroid/support/v4/n/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield android/support/v4/n/i/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/i/b()V
L0:
aload 0
getfield android/support/v4/n/i/e I
ifgt L1
ldc "{}"
areturn
L1:
new java/lang/StringBuilder
dup
aload 0
getfield android/support/v4/n/i/e I
bipush 28
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 2
aload 2
bipush 123
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iconst_0
istore 1
L2:
iload 1
aload 0
getfield android/support/v4/n/i/e I
if_icmpge L3
iload 1
ifle L4
aload 2
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L4:
aload 2
aload 0
iload 1
invokespecial android/support/v4/n/i/b(I)J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
pop
aload 2
bipush 61
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 0
iload 1
invokespecial android/support/v4/n/i/c(I)Ljava/lang/Object;
astore 3
aload 3
aload 0
if_acmpeq L5
aload 2
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
L6:
iload 1
iconst_1
iadd
istore 1
goto L2
L5:
aload 2
ldc "(this Map)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L6
L3:
aload 2
bipush 125
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method
