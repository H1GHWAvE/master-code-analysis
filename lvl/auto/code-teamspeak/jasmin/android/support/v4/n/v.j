.bytecode 50.0
.class public synchronized android/support/v4/n/v
.super java/lang/Object

.field private static final 'a' Z = 0


.field static 'b' [Ljava/lang/Object;

.field static 'c' I = 0


.field static 'd' [Ljava/lang/Object;

.field static 'e' I = 0


.field private static final 'i' Ljava/lang/String; = "ArrayMap"

.field private static final 'j' I = 4


.field private static final 'k' I = 10


.field 'f' [I

.field 'g' [Ljava/lang/Object;

.field 'h' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
getstatic android/support/v4/n/f/a [I
putfield android/support/v4/n/v/f [I
aload 0
getstatic android/support/v4/n/f/c [Ljava/lang/Object;
putfield android/support/v4/n/v/g [Ljava/lang/Object;
aload 0
iconst_0
putfield android/support/v4/n/v/h I
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(I)V
aload 0
invokespecial java/lang/Object/<init>()V
iload 1
ifne L0
aload 0
getstatic android/support/v4/n/f/a [I
putfield android/support/v4/n/v/f [I
aload 0
getstatic android/support/v4/n/f/c [Ljava/lang/Object;
putfield android/support/v4/n/v/g [Ljava/lang/Object;
L1:
aload 0
iconst_0
putfield android/support/v4/n/v/h I
return
L0:
aload 0
iload 1
invokespecial android/support/v4/n/v/e(I)V
goto L1
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/support/v4/n/v;)V
iconst_0
istore 2
aload 0
invokespecial android/support/v4/n/v/<init>()V
aload 1
ifnull L0
aload 1
getfield android/support/v4/n/v/h I
istore 3
aload 0
aload 0
getfield android/support/v4/n/v/h I
iload 3
iadd
invokevirtual android/support/v4/n/v/a(I)V
aload 0
getfield android/support/v4/n/v/h I
ifne L1
iload 3
ifle L0
aload 1
getfield android/support/v4/n/v/f [I
iconst_0
aload 0
getfield android/support/v4/n/v/f [I
iconst_0
iload 3
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iconst_0
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iconst_0
iload 3
iconst_1
ishl
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
iload 3
putfield android/support/v4/n/v/h I
L0:
return
L1:
iload 2
iload 3
if_icmpge L0
aload 0
aload 1
iload 2
invokevirtual android/support/v4/n/v/b(I)Ljava/lang/Object;
aload 1
iload 2
invokevirtual android/support/v4/n/v/c(I)Ljava/lang/Object;
invokevirtual android/support/v4/n/v/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
.limit locals 4
.limit stack 6
.end method

.method private a()I
aload 0
getfield android/support/v4/n/v/h I
istore 4
iload 4
ifne L0
iconst_m1
istore 1
L1:
iload 1
ireturn
L0:
aload 0
getfield android/support/v4/n/v/f [I
iload 4
iconst_0
invokestatic android/support/v4/n/f/a([III)I
istore 2
iload 2
istore 1
iload 2
iflt L1
iload 2
istore 1
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 2
iconst_1
ishl
aaload
ifnull L1
iload 2
iconst_1
iadd
istore 3
L2:
iload 3
iload 4
if_icmpge L3
aload 0
getfield android/support/v4/n/v/f [I
iload 3
iaload
ifne L3
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 3
iconst_1
ishl
aaload
ifnonnull L4
iload 3
ireturn
L4:
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
iload 2
iconst_1
isub
istore 2
L5:
iload 2
iflt L6
aload 0
getfield android/support/v4/n/v/f [I
iload 2
iaload
ifne L6
iload 2
istore 1
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 2
iconst_1
ishl
aaload
ifnull L1
iload 2
iconst_1
isub
istore 2
goto L5
L6:
iload 3
iconst_m1
ixor
ireturn
.limit locals 5
.limit stack 3
.end method

.method private a(Ljava/lang/Object;I)I
aload 0
getfield android/support/v4/n/v/h I
istore 6
iload 6
ifne L0
iconst_m1
istore 3
L1:
iload 3
ireturn
L0:
aload 0
getfield android/support/v4/n/v/f [I
iload 6
iload 2
invokestatic android/support/v4/n/f/a([III)I
istore 4
iload 4
istore 3
iload 4
iflt L1
iload 4
istore 3
aload 1
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 4
iconst_1
ishl
aaload
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L1
iload 4
iconst_1
iadd
istore 5
L2:
iload 5
iload 6
if_icmpge L3
aload 0
getfield android/support/v4/n/v/f [I
iload 5
iaload
iload 2
if_icmpne L3
aload 1
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 5
iconst_1
ishl
aaload
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L4
iload 5
ireturn
L4:
iload 5
iconst_1
iadd
istore 5
goto L2
L3:
iload 4
iconst_1
isub
istore 4
L5:
iload 4
iflt L6
aload 0
getfield android/support/v4/n/v/f [I
iload 4
iaload
iload 2
if_icmpne L6
iload 4
istore 3
aload 1
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 4
iconst_1
ishl
aaload
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L1
iload 4
iconst_1
isub
istore 4
goto L5
L6:
iload 5
iconst_m1
ixor
ireturn
.limit locals 7
.limit stack 4
.end method

.method private a(Landroid/support/v4/n/v;)V
iconst_0
istore 2
aload 1
getfield android/support/v4/n/v/h I
istore 3
aload 0
aload 0
getfield android/support/v4/n/v/h I
iload 3
iadd
invokevirtual android/support/v4/n/v/a(I)V
aload 0
getfield android/support/v4/n/v/h I
ifne L0
iload 3
ifle L1
aload 1
getfield android/support/v4/n/v/f [I
iconst_0
aload 0
getfield android/support/v4/n/v/f [I
iconst_0
iload 3
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iconst_0
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iconst_0
iload 3
iconst_1
ishl
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
iload 3
putfield android/support/v4/n/v/h I
L1:
return
L0:
iload 2
iload 3
if_icmpge L1
aload 0
aload 1
iload 2
invokevirtual android/support/v4/n/v/b(I)Ljava/lang/Object;
aload 1
iload 2
invokevirtual android/support/v4/n/v/c(I)Ljava/lang/Object;
invokevirtual android/support/v4/n/v/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 4
.limit stack 6
.end method

.method private static a([I[Ljava/lang/Object;I)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L10
.catch all from L11 to L12 using L10
.catch all from L12 to L13 using L10
.catch all from L14 to L15 using L10
aload 0
arraylength
bipush 8
if_icmpne L16
ldc android/support/v4/n/a
monitorenter
L0:
getstatic android/support/v4/n/v/e I
bipush 10
if_icmpge L4
aload 1
iconst_0
getstatic android/support/v4/n/v/d [Ljava/lang/Object;
aastore
L1:
aload 1
iconst_1
aload 0
aastore
iload 2
iconst_1
ishl
iconst_1
isub
istore 2
goto L17
L3:
aload 1
putstatic android/support/v4/n/v/d [Ljava/lang/Object;
getstatic android/support/v4/n/v/e I
iconst_1
iadd
putstatic android/support/v4/n/v/e I
L4:
ldc android/support/v4/n/a
monitorexit
L5:
return
L2:
astore 0
L6:
ldc android/support/v4/n/a
monitorexit
L7:
aload 0
athrow
L16:
aload 0
arraylength
iconst_4
if_icmpne L18
ldc android/support/v4/n/a
monitorenter
L8:
getstatic android/support/v4/n/v/c I
bipush 10
if_icmpge L12
aload 1
iconst_0
getstatic android/support/v4/n/v/b [Ljava/lang/Object;
aastore
L9:
aload 1
iconst_1
aload 0
aastore
iload 2
iconst_1
ishl
iconst_1
isub
istore 2
goto L19
L11:
aload 1
putstatic android/support/v4/n/v/b [Ljava/lang/Object;
getstatic android/support/v4/n/v/c I
iconst_1
iadd
putstatic android/support/v4/n/v/c I
L12:
ldc android/support/v4/n/a
monitorexit
L13:
return
L10:
astore 0
L14:
ldc android/support/v4/n/a
monitorexit
L15:
aload 0
athrow
L17:
iload 2
iconst_2
if_icmplt L3
aload 1
iload 2
aconst_null
aastore
iload 2
iconst_1
isub
istore 2
goto L17
L18:
return
L19:
iload 2
iconst_2
if_icmplt L11
aload 1
iload 2
aconst_null
aastore
iload 2
iconst_1
isub
istore 2
goto L19
.limit locals 3
.limit stack 3
.end method

.method private e(I)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L11
.catch all from L12 to L13 using L11
.catch all from L14 to L15 using L11
.catch all from L16 to L17 using L11
iload 1
bipush 8
if_icmpne L18
ldc android/support/v4/n/a
monitorenter
L0:
getstatic android/support/v4/n/v/d [Ljava/lang/Object;
ifnull L5
getstatic android/support/v4/n/v/d [Ljava/lang/Object;
astore 2
aload 0
aload 2
putfield android/support/v4/n/v/g [Ljava/lang/Object;
aload 2
iconst_0
aaload
checkcast [Ljava/lang/Object;
checkcast [Ljava/lang/Object;
putstatic android/support/v4/n/v/d [Ljava/lang/Object;
aload 0
aload 2
iconst_1
aaload
checkcast [I
checkcast [I
putfield android/support/v4/n/v/f [I
L1:
aload 2
iconst_1
aconst_null
aastore
aload 2
iconst_0
aconst_null
aastore
L3:
getstatic android/support/v4/n/v/e I
iconst_1
isub
putstatic android/support/v4/n/v/e I
ldc android/support/v4/n/a
monitorexit
L4:
return
L5:
ldc android/support/v4/n/a
monitorexit
L6:
aload 0
iload 1
newarray int
putfield android/support/v4/n/v/f [I
aload 0
iload 1
iconst_1
ishl
anewarray java/lang/Object
putfield android/support/v4/n/v/g [Ljava/lang/Object;
return
L2:
astore 2
L7:
ldc android/support/v4/n/a
monitorexit
L8:
aload 2
athrow
L18:
iload 1
iconst_4
if_icmpne L6
ldc android/support/v4/n/a
monitorenter
L9:
getstatic android/support/v4/n/v/b [Ljava/lang/Object;
ifnull L16
getstatic android/support/v4/n/v/b [Ljava/lang/Object;
astore 2
aload 0
aload 2
putfield android/support/v4/n/v/g [Ljava/lang/Object;
aload 2
iconst_0
aaload
checkcast [Ljava/lang/Object;
checkcast [Ljava/lang/Object;
putstatic android/support/v4/n/v/b [Ljava/lang/Object;
aload 0
aload 2
iconst_1
aaload
checkcast [I
checkcast [I
putfield android/support/v4/n/v/f [I
L10:
aload 2
iconst_1
aconst_null
aastore
aload 2
iconst_0
aconst_null
aastore
L12:
getstatic android/support/v4/n/v/c I
iconst_1
isub
putstatic android/support/v4/n/v/c I
ldc android/support/v4/n/a
monitorexit
L13:
return
L11:
astore 2
L14:
ldc android/support/v4/n/a
monitorexit
L15:
aload 2
athrow
L16:
ldc android/support/v4/n/a
monitorexit
L17:
goto L6
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;)I
aload 1
ifnonnull L0
aload 0
invokespecial android/support/v4/n/v/a()I
ireturn
L0:
aload 0
aload 1
aload 1
invokevirtual java/lang/Object/hashCode()I
invokespecial android/support/v4/n/v/a(Ljava/lang/Object;I)I
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final a(ILjava/lang/Object;)Ljava/lang/Object;
iload 1
iconst_1
ishl
iconst_1
iadd
istore 1
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 1
aaload
astore 3
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 1
aload 2
aastore
aload 3
areturn
.limit locals 4
.limit stack 3
.end method

.method public final a(I)V
aload 0
getfield android/support/v4/n/v/f [I
arraylength
iload 1
if_icmpge L0
aload 0
getfield android/support/v4/n/v/f [I
astore 2
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
astore 3
aload 0
iload 1
invokespecial android/support/v4/n/v/e(I)V
aload 0
getfield android/support/v4/n/v/h I
ifle L1
aload 2
iconst_0
aload 0
getfield android/support/v4/n/v/f [I
iconst_0
aload 0
getfield android/support/v4/n/v/h I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
iconst_0
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iconst_0
aload 0
getfield android/support/v4/n/v/h I
iconst_1
ishl
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L1:
aload 2
aload 3
aload 0
getfield android/support/v4/n/v/h I
invokestatic android/support/v4/n/v/a([I[Ljava/lang/Object;I)V
L0:
return
.limit locals 4
.limit stack 6
.end method

.method final b(Ljava/lang/Object;)I
iconst_1
istore 2
iconst_1
istore 3
aload 0
getfield android/support/v4/n/v/h I
iconst_2
imul
istore 4
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
astore 5
aload 1
ifnonnull L0
iload 3
istore 2
L1:
iload 2
iload 4
if_icmpge L2
aload 5
iload 2
aaload
ifnonnull L3
iload 2
iconst_1
ishr
ireturn
L3:
iload 2
iconst_2
iadd
istore 2
goto L1
L4:
iload 2
iconst_2
iadd
istore 2
L0:
iload 2
iload 4
if_icmpge L2
aload 1
aload 5
iload 2
aaload
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L4
iload 2
iconst_1
ishr
ireturn
L2:
iconst_m1
ireturn
.limit locals 6
.limit stack 3
.end method

.method public final b(I)Ljava/lang/Object;
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 1
iconst_1
ishl
aaload
areturn
.limit locals 2
.limit stack 3
.end method

.method public final c(I)Ljava/lang/Object;
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 1
iconst_1
ishl
iconst_1
iadd
aaload
areturn
.limit locals 2
.limit stack 3
.end method

.method public clear()V
aload 0
getfield android/support/v4/n/v/h I
ifeq L0
aload 0
getfield android/support/v4/n/v/f [I
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/v/h I
invokestatic android/support/v4/n/v/a([I[Ljava/lang/Object;I)V
aload 0
getstatic android/support/v4/n/f/a [I
putfield android/support/v4/n/v/f [I
aload 0
getstatic android/support/v4/n/f/c [Ljava/lang/Object;
putfield android/support/v4/n/v/g [Ljava/lang/Object;
aload 0
iconst_0
putfield android/support/v4/n/v/h I
L0:
return
.limit locals 1
.limit stack 3
.end method

.method public containsKey(Ljava/lang/Object;)Z
aload 0
aload 1
invokevirtual android/support/v4/n/v/a(Ljava/lang/Object;)I
iflt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public containsValue(Ljava/lang/Object;)Z
aload 0
aload 1
invokevirtual android/support/v4/n/v/b(Ljava/lang/Object;)I
iflt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final d(I)Ljava/lang/Object;
bipush 8
istore 2
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 1
iconst_1
ishl
iconst_1
iadd
aaload
astore 3
aload 0
getfield android/support/v4/n/v/h I
iconst_1
if_icmpgt L0
aload 0
getfield android/support/v4/n/v/f [I
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/v/h I
invokestatic android/support/v4/n/v/a([I[Ljava/lang/Object;I)V
aload 0
getstatic android/support/v4/n/f/a [I
putfield android/support/v4/n/v/f [I
aload 0
getstatic android/support/v4/n/f/c [Ljava/lang/Object;
putfield android/support/v4/n/v/g [Ljava/lang/Object;
aload 0
iconst_0
putfield android/support/v4/n/v/h I
L1:
aload 3
areturn
L0:
aload 0
getfield android/support/v4/n/v/f [I
arraylength
bipush 8
if_icmple L2
aload 0
getfield android/support/v4/n/v/h I
aload 0
getfield android/support/v4/n/v/f [I
arraylength
iconst_3
idiv
if_icmpge L2
aload 0
getfield android/support/v4/n/v/h I
bipush 8
if_icmple L3
aload 0
getfield android/support/v4/n/v/h I
aload 0
getfield android/support/v4/n/v/h I
iconst_1
ishr
iadd
istore 2
L3:
aload 0
getfield android/support/v4/n/v/f [I
astore 4
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
astore 5
aload 0
iload 2
invokespecial android/support/v4/n/v/e(I)V
aload 0
aload 0
getfield android/support/v4/n/v/h I
iconst_1
isub
putfield android/support/v4/n/v/h I
iload 1
ifle L4
aload 4
iconst_0
aload 0
getfield android/support/v4/n/v/f [I
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 5
iconst_0
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iconst_0
iload 1
iconst_1
ishl
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L4:
iload 1
aload 0
getfield android/support/v4/n/v/h I
if_icmpge L1
aload 4
iload 1
iconst_1
iadd
aload 0
getfield android/support/v4/n/v/f [I
iload 1
aload 0
getfield android/support/v4/n/v/h I
iload 1
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 5
iload 1
iconst_1
iadd
iconst_1
ishl
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 1
iconst_1
ishl
aload 0
getfield android/support/v4/n/v/h I
iload 1
isub
iconst_1
ishl
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
L2:
aload 0
aload 0
getfield android/support/v4/n/v/h I
iconst_1
isub
putfield android/support/v4/n/v/h I
iload 1
aload 0
getfield android/support/v4/n/v/h I
if_icmpge L5
aload 0
getfield android/support/v4/n/v/f [I
iload 1
iconst_1
iadd
aload 0
getfield android/support/v4/n/v/f [I
iload 1
aload 0
getfield android/support/v4/n/v/h I
iload 1
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 1
iconst_1
iadd
iconst_1
ishl
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 1
iconst_1
ishl
aload 0
getfield android/support/v4/n/v/h I
iload 1
isub
iconst_1
ishl
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L5:
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/v/h I
iconst_1
ishl
aconst_null
aastore
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/v/h I
iconst_1
ishl
iconst_1
iadd
aconst_null
aastore
aload 3
areturn
.limit locals 6
.limit stack 6
.end method

.method public equals(Ljava/lang/Object;)Z
.catch java/lang/NullPointerException from L0 to L1 using L2
.catch java/lang/ClassCastException from L0 to L1 using L3
.catch java/lang/NullPointerException from L4 to L5 using L2
.catch java/lang/ClassCastException from L4 to L5 using L3
.catch java/lang/NullPointerException from L6 to L7 using L2
.catch java/lang/ClassCastException from L6 to L7 using L3
aload 0
aload 1
if_acmpne L8
L9:
iconst_1
ireturn
L8:
aload 1
instanceof java/util/Map
ifeq L10
aload 1
checkcast java/util/Map
astore 1
aload 0
invokevirtual android/support/v4/n/v/size()I
aload 1
invokeinterface java/util/Map/size()I 0
if_icmpeq L11
iconst_0
ireturn
L11:
iconst_0
istore 2
L0:
iload 2
aload 0
getfield android/support/v4/n/v/h I
if_icmpge L9
aload 0
iload 2
invokevirtual android/support/v4/n/v/b(I)Ljava/lang/Object;
astore 4
aload 0
iload 2
invokevirtual android/support/v4/n/v/c(I)Ljava/lang/Object;
astore 5
aload 1
aload 4
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 6
L1:
aload 5
ifnonnull L6
aload 6
ifnonnull L12
L4:
aload 1
aload 4
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L13
L5:
goto L12
L6:
aload 5
aload 6
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
istore 3
L7:
iload 3
ifne L13
iconst_0
ireturn
L13:
iload 2
iconst_1
iadd
istore 2
goto L0
L2:
astore 1
iconst_0
ireturn
L3:
astore 1
iconst_0
ireturn
L10:
iconst_0
ireturn
L12:
iconst_0
ireturn
.limit locals 7
.limit stack 2
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokevirtual android/support/v4/n/v/a(Ljava/lang/Object;)I
istore 2
iload 2
iflt L0
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 2
iconst_1
ishl
iconst_1
iadd
aaload
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method public hashCode()I
aload 0
getfield android/support/v4/n/v/f [I
astore 7
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
astore 8
aload 0
getfield android/support/v4/n/v/h I
istore 5
iconst_1
istore 1
iconst_0
istore 2
iconst_0
istore 3
L0:
iload 2
iload 5
if_icmpge L1
aload 8
iload 1
aaload
astore 9
aload 7
iload 2
iaload
istore 6
aload 9
ifnonnull L2
iconst_0
istore 4
L3:
iload 3
iload 4
iload 6
ixor
iadd
istore 3
iload 2
iconst_1
iadd
istore 2
iload 1
iconst_2
iadd
istore 1
goto L0
L2:
aload 9
invokevirtual java/lang/Object/hashCode()I
istore 4
goto L3
L1:
iload 3
ireturn
.limit locals 10
.limit stack 3
.end method

.method public isEmpty()Z
aload 0
getfield android/support/v4/n/v/h I
ifgt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
bipush 8
istore 5
aload 1
ifnonnull L0
aload 0
invokespecial android/support/v4/n/v/a()I
istore 3
iconst_0
istore 4
L1:
iload 3
iflt L2
iload 3
iconst_1
ishl
iconst_1
iadd
istore 3
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 3
aaload
astore 1
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 3
aload 2
aastore
aload 1
areturn
L0:
aload 1
invokevirtual java/lang/Object/hashCode()I
istore 4
aload 0
aload 1
iload 4
invokespecial android/support/v4/n/v/a(Ljava/lang/Object;I)I
istore 3
goto L1
L2:
iload 3
iconst_m1
ixor
istore 6
aload 0
getfield android/support/v4/n/v/h I
aload 0
getfield android/support/v4/n/v/f [I
arraylength
if_icmplt L3
aload 0
getfield android/support/v4/n/v/h I
bipush 8
if_icmplt L4
aload 0
getfield android/support/v4/n/v/h I
aload 0
getfield android/support/v4/n/v/h I
iconst_1
ishr
iadd
istore 3
L5:
aload 0
getfield android/support/v4/n/v/f [I
astore 7
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
astore 8
aload 0
iload 3
invokespecial android/support/v4/n/v/e(I)V
aload 0
getfield android/support/v4/n/v/f [I
arraylength
ifle L6
aload 7
iconst_0
aload 0
getfield android/support/v4/n/v/f [I
iconst_0
aload 7
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 8
iconst_0
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iconst_0
aload 8
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L6:
aload 7
aload 8
aload 0
getfield android/support/v4/n/v/h I
invokestatic android/support/v4/n/v/a([I[Ljava/lang/Object;I)V
L3:
iload 6
aload 0
getfield android/support/v4/n/v/h I
if_icmpge L7
aload 0
getfield android/support/v4/n/v/f [I
iload 6
aload 0
getfield android/support/v4/n/v/f [I
iload 6
iconst_1
iadd
aload 0
getfield android/support/v4/n/v/h I
iload 6
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 6
iconst_1
ishl
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 6
iconst_1
iadd
iconst_1
ishl
aload 0
getfield android/support/v4/n/v/h I
iload 6
isub
iconst_1
ishl
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L7:
aload 0
getfield android/support/v4/n/v/f [I
iload 6
iload 4
iastore
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 6
iconst_1
ishl
aload 1
aastore
aload 0
getfield android/support/v4/n/v/g [Ljava/lang/Object;
iload 6
iconst_1
ishl
iconst_1
iadd
aload 2
aastore
aload 0
aload 0
getfield android/support/v4/n/v/h I
iconst_1
iadd
putfield android/support/v4/n/v/h I
aconst_null
areturn
L4:
iload 5
istore 3
aload 0
getfield android/support/v4/n/v/h I
iconst_4
if_icmpge L5
iconst_4
istore 3
goto L5
.limit locals 9
.limit stack 6
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokevirtual android/support/v4/n/v/a(Ljava/lang/Object;)I
istore 2
iload 2
iflt L0
aload 0
iload 2
invokevirtual android/support/v4/n/v/d(I)Ljava/lang/Object;
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 2
.end method

.method public size()I
aload 0
getfield android/support/v4/n/v/h I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
aload 0
invokevirtual android/support/v4/n/v/isEmpty()Z
ifeq L0
ldc "{}"
areturn
L0:
new java/lang/StringBuilder
dup
aload 0
getfield android/support/v4/n/v/h I
bipush 28
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 2
aload 2
bipush 123
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iconst_0
istore 1
L1:
iload 1
aload 0
getfield android/support/v4/n/v/h I
if_icmpge L2
iload 1
ifle L3
aload 2
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L3:
aload 0
iload 1
invokevirtual android/support/v4/n/v/b(I)Ljava/lang/Object;
astore 3
aload 3
aload 0
if_acmpeq L4
aload 2
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
L5:
aload 2
bipush 61
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 0
iload 1
invokevirtual android/support/v4/n/v/c(I)Ljava/lang/Object;
astore 3
aload 3
aload 0
if_acmpeq L6
aload 2
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
L7:
iload 1
iconst_1
iadd
istore 1
goto L1
L4:
aload 2
ldc "(this Map)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L5
L6:
aload 2
ldc "(this Map)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L7
L2:
aload 2
bipush 125
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method
