.bytecode 50.0
.class final synchronized android/support/v4/n/o
.super java/lang/Object
.implements java/util/Iterator
.implements java/util/Map$Entry

.field 'a' I

.field 'b' I

.field 'c' Z

.field final synthetic 'd' Landroid/support/v4/n/k;

.method <init>(Landroid/support/v4/n/k;)V
aload 0
aload 1
putfield android/support/v4/n/o/d Landroid/support/v4/n/k;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v4/n/o/c Z
aload 0
aload 1
invokevirtual android/support/v4/n/k/a()I
iconst_1
isub
putfield android/support/v4/n/o/a I
aload 0
iconst_m1
putfield android/support/v4/n/o/b I
return
.limit locals 2
.limit stack 3
.end method

.method private a()Ljava/util/Map$Entry;
aload 0
aload 0
getfield android/support/v4/n/o/b I
iconst_1
iadd
putfield android/support/v4/n/o/b I
aload 0
iconst_1
putfield android/support/v4/n/o/c Z
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 0
getfield android/support/v4/n/o/c Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "This container does not support retaining Map.Entry objects"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
instanceof java/util/Map$Entry
ifne L1
L2:
iconst_0
ireturn
L1:
aload 1
checkcast java/util/Map$Entry
astore 1
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 0
getfield android/support/v4/n/o/d Landroid/support/v4/n/k;
aload 0
getfield android/support/v4/n/o/b I
iconst_0
invokevirtual android/support/v4/n/k/a(II)Ljava/lang/Object;
invokestatic android/support/v4/n/f/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L2
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
aload 0
getfield android/support/v4/n/o/d Landroid/support/v4/n/k;
aload 0
getfield android/support/v4/n/o/b I
iconst_1
invokevirtual android/support/v4/n/k/a(II)Ljava/lang/Object;
invokestatic android/support/v4/n/f/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L2
iconst_1
ireturn
.limit locals 2
.limit stack 4
.end method

.method public final getKey()Ljava/lang/Object;
aload 0
getfield android/support/v4/n/o/c Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "This container does not support retaining Map.Entry objects"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/n/o/d Landroid/support/v4/n/k;
aload 0
getfield android/support/v4/n/o/b I
iconst_0
invokevirtual android/support/v4/n/k/a(II)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final getValue()Ljava/lang/Object;
aload 0
getfield android/support/v4/n/o/c Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "This container does not support retaining Map.Entry objects"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/n/o/d Landroid/support/v4/n/k;
aload 0
getfield android/support/v4/n/o/b I
iconst_1
invokevirtual android/support/v4/n/k/a(II)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final hasNext()Z
aload 0
getfield android/support/v4/n/o/b I
aload 0
getfield android/support/v4/n/o/a I
if_icmpge L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final hashCode()I
iconst_0
istore 2
aload 0
getfield android/support/v4/n/o/c Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "This container does not support retaining Map.Entry objects"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/n/o/d Landroid/support/v4/n/k;
aload 0
getfield android/support/v4/n/o/b I
iconst_0
invokevirtual android/support/v4/n/k/a(II)Ljava/lang/Object;
astore 3
aload 0
getfield android/support/v4/n/o/d Landroid/support/v4/n/k;
aload 0
getfield android/support/v4/n/o/b I
iconst_1
invokevirtual android/support/v4/n/k/a(II)Ljava/lang/Object;
astore 4
aload 3
ifnonnull L1
iconst_0
istore 1
L2:
aload 4
ifnonnull L3
L4:
iload 2
iload 1
ixor
ireturn
L1:
aload 3
invokevirtual java/lang/Object/hashCode()I
istore 1
goto L2
L3:
aload 4
invokevirtual java/lang/Object/hashCode()I
istore 2
goto L4
.limit locals 5
.limit stack 3
.end method

.method public final volatile synthetic next()Ljava/lang/Object;
aload 0
aload 0
getfield android/support/v4/n/o/b I
iconst_1
iadd
putfield android/support/v4/n/o/b I
aload 0
iconst_1
putfield android/support/v4/n/o/c Z
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method public final remove()V
aload 0
getfield android/support/v4/n/o/c Z
ifne L0
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
L0:
aload 0
getfield android/support/v4/n/o/d Landroid/support/v4/n/k;
aload 0
getfield android/support/v4/n/o/b I
invokevirtual android/support/v4/n/k/a(I)V
aload 0
aload 0
getfield android/support/v4/n/o/b I
iconst_1
isub
putfield android/support/v4/n/o/b I
aload 0
aload 0
getfield android/support/v4/n/o/a I
iconst_1
isub
putfield android/support/v4/n/o/a I
aload 0
iconst_0
putfield android/support/v4/n/o/c Z
return
.limit locals 1
.limit stack 3
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield android/support/v4/n/o/c Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "This container does not support retaining Map.Entry objects"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/n/o/d Landroid/support/v4/n/k;
aload 0
getfield android/support/v4/n/o/b I
aload 1
invokevirtual android/support/v4/n/k/a(ILjava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual android/support/v4/n/o/getKey()Ljava/lang/Object;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual android/support/v4/n/o/getValue()Ljava/lang/Object;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
