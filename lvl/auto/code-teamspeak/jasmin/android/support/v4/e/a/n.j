.bytecode 50.0
.class final synchronized android/support/v4/e/a/n
.super java/lang/Object

.field static 'a' Ljava/lang/reflect/Method;

.field static 'b' Z = 0


.field private static final 'c' Ljava/lang/String; = "DrawableCompatJellybeanMr1"

.field private static 'd' Ljava/lang/reflect/Method;

.field private static 'e' Z

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)I
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
getstatic android/support/v4/e/a/n/e Z
ifne L6
L0:
ldc android/graphics/drawable/Drawable
ldc "getLayoutDirection"
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 2
aload 2
putstatic android/support/v4/e/a/n/d Ljava/lang/reflect/Method;
aload 2
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
L1:
iconst_1
putstatic android/support/v4/e/a/n/e Z
L6:
getstatic android/support/v4/e/a/n/d Ljava/lang/reflect/Method;
ifnull L7
L3:
getstatic android/support/v4/e/a/n/d Ljava/lang/reflect/Method;
aload 0
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 1
L4:
iload 1
ireturn
L2:
astore 2
ldc "DrawableCompatJellybeanMr1"
ldc "Failed to retrieve getLayoutDirection() method"
aload 2
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L5:
astore 0
ldc "DrawableCompatJellybeanMr1"
ldc "Failed to invoke getLayoutDirection() via reflection"
aload 0
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
putstatic android/support/v4/e/a/n/d Ljava/lang/reflect/Method;
L7:
iconst_m1
ireturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Landroid/graphics/drawable/Drawable;I)V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
getstatic android/support/v4/e/a/n/b Z
ifne L6
L0:
ldc android/graphics/drawable/Drawable
ldc "setLayoutDirection"
iconst_1
anewarray java/lang/Class
dup
iconst_0
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 2
aload 2
putstatic android/support/v4/e/a/n/a Ljava/lang/reflect/Method;
aload 2
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
L1:
iconst_1
putstatic android/support/v4/e/a/n/b Z
L6:
getstatic android/support/v4/e/a/n/a Ljava/lang/reflect/Method;
ifnull L4
L3:
getstatic android/support/v4/e/a/n/a Ljava/lang/reflect/Method;
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
return
L2:
astore 2
ldc "DrawableCompatJellybeanMr1"
ldc "Failed to retrieve setLayoutDirection(int) method"
aload 2
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L5:
astore 0
ldc "DrawableCompatJellybeanMr1"
ldc "Failed to invoke setLayoutDirection(int) via reflection"
aload 0
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
putstatic android/support/v4/e/a/n/a Ljava/lang/reflect/Method;
return
.limit locals 3
.limit stack 6
.end method
