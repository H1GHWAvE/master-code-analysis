.bytecode 50.0
.class final synchronized android/support/v4/e/a/p
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
aload 0
instanceof android/graphics/drawable/GradientDrawable
ifne L0
aload 0
astore 1
aload 0
instanceof android/graphics/drawable/DrawableContainer
ifeq L1
L0:
new android/support/v4/e/a/u
dup
aload 0
invokespecial android/support/v4/e/a/u/<init>(Landroid/graphics/drawable/Drawable;)V
astore 1
L1:
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Landroid/graphics/drawable/Drawable;FF)V
aload 0
fload 1
fload 2
invokevirtual android/graphics/drawable/Drawable/setHotspot(FF)V
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Landroid/graphics/drawable/Drawable;I)V
aload 0
instanceof android/support/v4/e/a/u
ifeq L0
aload 0
iload 1
invokestatic android/support/v4/e/a/l/a(Landroid/graphics/drawable/Drawable;I)V
return
L0:
aload 0
iload 1
invokevirtual android/graphics/drawable/Drawable/setTint(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/graphics/drawable/Drawable;IIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
invokevirtual android/graphics/drawable/Drawable/setHotspotBounds(IIII)V
return
.limit locals 5
.limit stack 5
.end method

.method private static a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
aload 0
instanceof android/support/v4/e/a/u
ifeq L0
aload 0
aload 1
invokestatic android/support/v4/e/a/l/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
return
L0:
aload 0
aload 1
invokevirtual android/graphics/drawable/Drawable/setTintList(Landroid/content/res/ColorStateList;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
aload 0
instanceof android/support/v4/e/a/u
ifeq L0
aload 0
aload 1
invokestatic android/support/v4/e/a/l/a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
return
L0:
aload 0
aload 1
invokevirtual android/graphics/drawable/Drawable/setTintMode(Landroid/graphics/PorterDuff$Mode;)V
return
.limit locals 2
.limit stack 2
.end method
