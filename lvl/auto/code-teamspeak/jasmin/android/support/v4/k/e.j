.bytecode 50.0
.class final synchronized android/support/v4/k/e
.super android/support/v4/k/a

.field private 'b' Landroid/content/Context;

.field private 'c' Landroid/net/Uri;

.method <init>(Landroid/content/Context;Landroid/net/Uri;)V
aload 0
aconst_null
invokespecial android/support/v4/k/a/<init>(Landroid/support/v4/k/a;)V
aload 0
aload 1
putfield android/support/v4/k/e/b Landroid/content/Context;
aload 0
aload 2
putfield android/support/v4/k/e/c Landroid/net/Uri;
return
.limit locals 3
.limit stack 2
.end method

.method public final a()Landroid/net/Uri;
aload 0
getfield android/support/v4/k/e/c Landroid/net/Uri;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/String;)Landroid/support/v4/k/a;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/k/a;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final b()Ljava/lang/String;
aload 0
getfield android/support/v4/k/e/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/e/c Landroid/net/Uri;
ldc "_display_name"
invokestatic android/support/v4/k/b/a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final b(Ljava/lang/String;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final c()Ljava/lang/String;
aload 0
getfield android/support/v4/k/e/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/e/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final d()Z
aload 0
getfield android/support/v4/k/e/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/e/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/b(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final e()Z
aload 0
getfield android/support/v4/k/e/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/e/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/c(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final f()J
aload 0
getfield android/support/v4/k/e/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/e/c Landroid/net/Uri;
ldc "last_modified"
invokestatic android/support/v4/k/b/b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J
lreturn
.limit locals 1
.limit stack 3
.end method

.method public final g()J
aload 0
getfield android/support/v4/k/e/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/e/c Landroid/net/Uri;
ldc "_size"
invokestatic android/support/v4/k/b/b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J
lreturn
.limit locals 1
.limit stack 3
.end method

.method public final h()Z
aload 0
getfield android/support/v4/k/e/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/e/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/d(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final i()Z
aload 0
getfield android/support/v4/k/e/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/e/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/e(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final j()Z
aload 0
getfield android/support/v4/k/e/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/e/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/f(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final k()Z
aload 0
getfield android/support/v4/k/e/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/e/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/g(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final l()[Landroid/support/v4/k/a;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method
