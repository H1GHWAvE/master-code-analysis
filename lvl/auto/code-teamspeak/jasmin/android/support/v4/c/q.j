.bytecode 50.0
.class public final synchronized android/support/v4/c/q
.super android/content/ContentProvider

.field private static final 'a' [Ljava/lang/String;

.field private static final 'b' Ljava/lang/String; = "android.support.FILE_PROVIDER_PATHS"

.field private static final 'c' Ljava/lang/String; = "root-path"

.field private static final 'd' Ljava/lang/String; = "files-path"

.field private static final 'e' Ljava/lang/String; = "cache-path"

.field private static final 'f' Ljava/lang/String; = "external-path"

.field private static final 'g' Ljava/lang/String; = "name"

.field private static final 'h' Ljava/lang/String; = "path"

.field private static final 'i' Ljava/io/File;

.field private static 'j' Ljava/util/HashMap;

.field private 'k' Landroid/support/v4/c/r;

.method static <clinit>()V
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "_display_name"
aastore
dup
iconst_1
ldc "_size"
aastore
putstatic android/support/v4/c/q/a [Ljava/lang/String;
new java/io/File
dup
ldc "/"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic android/support/v4/c/q/i Ljava/io/File;
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putstatic android/support/v4/c/q/j Ljava/util/HashMap;
return
.limit locals 0
.limit stack 4
.end method

.method public <init>()V
aload 0
invokespecial android/content/ContentProvider/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/String;)I
ldc "r"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
ldc_w 268435456
ireturn
L0:
ldc "w"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
ldc "wt"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
L1:
ldc_w 738197504
ireturn
L2:
ldc "wa"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
ldc_w 704643072
ireturn
L3:
ldc "rw"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
ldc_w 939524096
ireturn
L4:
ldc "rwt"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
ldc_w 1006632960
ireturn
L5:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Invalid mode: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 5
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;
aload 0
aload 1
invokestatic android/support/v4/c/q/a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/c/r;
aload 2
invokeinterface android/support/v4/c/r/a(Ljava/io/File;)Landroid/net/Uri; 1
areturn
.limit locals 3
.limit stack 2
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/c/r;
.catch all from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L5
.catch org/xmlpull/v1/XmlPullParserException from L3 to L4 using L6
.catch all from L3 to L4 using L2
.catch java/io/IOException from L7 to L5 using L5
.catch org/xmlpull/v1/XmlPullParserException from L7 to L5 using L6
.catch all from L7 to L5 using L2
.catch all from L8 to L2 using L2
.catch all from L9 to L10 using L2
.catch java/io/IOException from L11 to L12 using L13
.catch org/xmlpull/v1/XmlPullParserException from L11 to L12 using L6
.catch all from L11 to L12 using L2
.catch java/io/IOException from L12 to L14 using L5
.catch org/xmlpull/v1/XmlPullParserException from L12 to L14 using L6
.catch all from L12 to L14 using L2
.catch java/io/IOException from L14 to L15 using L5
.catch org/xmlpull/v1/XmlPullParserException from L14 to L15 using L6
.catch all from L14 to L15 using L2
.catch java/io/IOException from L16 to L17 using L5
.catch org/xmlpull/v1/XmlPullParserException from L16 to L17 using L6
.catch all from L16 to L17 using L2
.catch java/io/IOException from L18 to L6 using L5
.catch org/xmlpull/v1/XmlPullParserException from L18 to L6 using L6
.catch all from L18 to L6 using L2
.catch all from L19 to L20 using L2
.catch java/io/IOException from L20 to L21 using L5
.catch org/xmlpull/v1/XmlPullParserException from L20 to L21 using L6
.catch all from L20 to L21 using L2
.catch java/io/IOException from L22 to L23 using L5
.catch org/xmlpull/v1/XmlPullParserException from L22 to L23 using L6
.catch all from L22 to L23 using L2
.catch java/io/IOException from L24 to L25 using L5
.catch org/xmlpull/v1/XmlPullParserException from L24 to L25 using L6
.catch all from L24 to L25 using L2
.catch java/io/IOException from L26 to L27 using L5
.catch org/xmlpull/v1/XmlPullParserException from L26 to L27 using L6
.catch all from L26 to L27 using L2
.catch all from L27 to L28 using L2
.catch all from L29 to L30 using L2
getstatic android/support/v4/c/q/j Ljava/util/HashMap;
astore 5
aload 5
monitorenter
L0:
getstatic android/support/v4/c/q/j Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/support/v4/c/r
astore 4
L1:
aload 4
astore 3
aload 4
ifnonnull L29
L3:
new android/support/v4/c/s
dup
aload 1
invokespecial android/support/v4/c/s/<init>(Ljava/lang/String;)V
astore 4
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 1
sipush 128
invokevirtual android/content/pm/PackageManager/resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
ldc "android.support.FILE_PROVIDER_PATHS"
invokevirtual android/content/pm/ProviderInfo/loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
astore 6
L4:
aload 6
ifnonnull L14
L7:
new java/lang/IllegalArgumentException
dup
ldc "Missing android.support.FILE_PROVIDER_PATHS meta-data"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L5:
astore 0
L8:
new java/lang/IllegalArgumentException
dup
ldc "Failed to parse android.support.FILE_PROVIDER_PATHS meta-data"
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L2:
astore 0
L9:
aload 5
monitorexit
L10:
aload 0
athrow
L11:
aload 3
invokevirtual java/io/File/getCanonicalFile()Ljava/io/File;
astore 8
L12:
aload 4
getfield android/support/v4/c/s/a Ljava/util/HashMap;
aload 7
aload 8
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L14:
aload 6
invokeinterface android/content/res/XmlResourceParser/next()I 0
istore 2
L15:
iload 2
iconst_1
if_icmpeq L27
iload 2
iconst_2
if_icmpne L14
L16:
aload 6
invokeinterface android/content/res/XmlResourceParser/getName()Ljava/lang/String; 0
astore 3
aload 6
aconst_null
ldc "name"
invokeinterface android/content/res/XmlResourceParser/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 7
aload 6
aconst_null
ldc "path"
invokeinterface android/content/res/XmlResourceParser/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 8
ldc "root-path"
aload 3
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L20
getstatic android/support/v4/c/q/i Ljava/io/File;
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 8
aastore
invokestatic android/support/v4/c/q/a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 3
L17:
aload 3
ifnull L14
L18:
aload 7
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L11
new java/lang/IllegalArgumentException
dup
ldc "Name must not be empty"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L6:
astore 0
L19:
new java/lang/IllegalArgumentException
dup
ldc "Failed to parse android.support.FILE_PROVIDER_PATHS meta-data"
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L20:
ldc "files-path"
aload 3
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L22
aload 0
invokevirtual android/content/Context/getFilesDir()Ljava/io/File;
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 8
aastore
invokestatic android/support/v4/c/q/a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 3
L21:
goto L17
L22:
ldc "cache-path"
aload 3
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L24
aload 0
invokevirtual android/content/Context/getCacheDir()Ljava/io/File;
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 8
aastore
invokestatic android/support/v4/c/q/a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 3
L23:
goto L17
L24:
ldc "external-path"
aload 3
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L31
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 8
aastore
invokestatic android/support/v4/c/q/a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 3
L25:
goto L17
L13:
astore 0
L26:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Failed to resolve canonical path for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L27:
getstatic android/support/v4/c/q/j Ljava/util/HashMap;
aload 1
aload 4
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L28:
aload 4
astore 3
L29:
aload 5
monitorexit
L30:
aload 3
areturn
L31:
aconst_null
astore 3
goto L17
.limit locals 9
.limit stack 5
.end method

.method private static transient a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
iconst_0
istore 2
L0:
iload 2
ifgt L1
aload 1
iconst_0
aaload
astore 3
aload 3
ifnull L2
new java/io/File
dup
aload 0
aload 3
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 0
L3:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
areturn
L2:
goto L3
.limit locals 4
.limit stack 4
.end method

.method private static a([Ljava/lang/Object;I)[Ljava/lang/Object;
iload 1
anewarray java/lang/Object
astore 2
aload 0
iconst_0
aload 2
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method private static a([Ljava/lang/String;I)[Ljava/lang/String;
iload 1
anewarray java/lang/String
astore 2
aload 0
iconst_0
aload 2
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/c/r;
.catch java/io/IOException from L0 to L1 using L2
new android/support/v4/c/s
dup
aload 1
invokespecial android/support/v4/c/s/<init>(Ljava/lang/String;)V
astore 3
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 1
sipush 128
invokevirtual android/content/pm/PackageManager/resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
ldc "android.support.FILE_PROVIDER_PATHS"
invokevirtual android/content/pm/ProviderInfo/loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
astore 4
aload 4
ifnonnull L3
new java/lang/IllegalArgumentException
dup
ldc "Missing android.support.FILE_PROVIDER_PATHS meta-data"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual java/io/File/getCanonicalFile()Ljava/io/File;
astore 6
L1:
aload 3
getfield android/support/v4/c/s/a Ljava/util/HashMap;
aload 5
aload 6
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L3:
aload 4
invokeinterface android/content/res/XmlResourceParser/next()I 0
istore 2
iload 2
iconst_1
if_icmpeq L4
iload 2
iconst_2
if_icmpne L3
aload 4
invokeinterface android/content/res/XmlResourceParser/getName()Ljava/lang/String; 0
astore 1
aload 4
aconst_null
ldc "name"
invokeinterface android/content/res/XmlResourceParser/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 5
aload 4
aconst_null
ldc "path"
invokeinterface android/content/res/XmlResourceParser/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 6
ldc "root-path"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
getstatic android/support/v4/c/q/i Ljava/io/File;
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 6
aastore
invokestatic android/support/v4/c/q/a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 1
L6:
aload 1
ifnull L3
aload 5
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L0
new java/lang/IllegalArgumentException
dup
ldc "Name must not be empty"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L5:
ldc "files-path"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
aload 0
invokevirtual android/content/Context/getFilesDir()Ljava/io/File;
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 6
aastore
invokestatic android/support/v4/c/q/a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 1
goto L6
L7:
ldc "cache-path"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L8
aload 0
invokevirtual android/content/Context/getCacheDir()Ljava/io/File;
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 6
aastore
invokestatic android/support/v4/c/q/a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 1
goto L6
L8:
ldc "external-path"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L9
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 6
aastore
invokestatic android/support/v4/c/q/a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 1
goto L6
L2:
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Failed to resolve canonical path for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L4:
aload 3
areturn
L9:
aconst_null
astore 1
goto L6
.limit locals 7
.limit stack 5
.end method

.method public final attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
aload 0
aload 1
aload 2
invokespecial android/content/ContentProvider/attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
aload 2
getfield android/content/pm/ProviderInfo/exported Z
ifeq L0
new java/lang/SecurityException
dup
ldc "Provider must not be exported"
invokespecial java/lang/SecurityException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
getfield android/content/pm/ProviderInfo/grantUriPermissions Z
ifne L1
new java/lang/SecurityException
dup
ldc "Provider must grant uri permissions"
invokespecial java/lang/SecurityException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 1
aload 2
getfield android/content/pm/ProviderInfo/authority Ljava/lang/String;
invokestatic android/support/v4/c/q/a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/c/r;
putfield android/support/v4/c/q/k Landroid/support/v4/c/r;
return
.limit locals 3
.limit stack 3
.end method

.method public final delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
aload 0
getfield android/support/v4/c/q/k Landroid/support/v4/c/r;
aload 1
invokeinterface android/support/v4/c/r/a(Landroid/net/Uri;)Ljava/io/File; 1
invokevirtual java/io/File/delete()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final getType(Landroid/net/Uri;)Ljava/lang/String;
aload 0
getfield android/support/v4/c/q/k Landroid/support/v4/c/r;
aload 1
invokeinterface android/support/v4/c/r/a(Landroid/net/Uri;)Ljava/io/File; 1
astore 1
aload 1
invokevirtual java/io/File/getName()Ljava/lang/String;
bipush 46
invokevirtual java/lang/String/lastIndexOf(I)I
istore 2
iload 2
iflt L0
aload 1
invokevirtual java/io/File/getName()Ljava/lang/String;
iload 2
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 1
invokestatic android/webkit/MimeTypeMap/getSingleton()Landroid/webkit/MimeTypeMap;
aload 1
invokevirtual android/webkit/MimeTypeMap/getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
ifnull L0
aload 1
areturn
L0:
ldc "application/octet-stream"
areturn
.limit locals 3
.limit stack 3
.end method

.method public final insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
new java/lang/UnsupportedOperationException
dup
ldc "No external inserts"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final onCreate()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
aload 0
getfield android/support/v4/c/q/k Landroid/support/v4/c/r;
aload 1
invokeinterface android/support/v4/c/r/a(Landroid/net/Uri;)Ljava/io/File; 1
astore 1
ldc "r"
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
ldc_w 268435456
istore 3
L1:
aload 1
iload 3
invokestatic android/os/ParcelFileDescriptor/open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
areturn
L0:
ldc "w"
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L2
ldc "wt"
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
L2:
ldc_w 738197504
istore 3
goto L1
L3:
ldc "wa"
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
ldc_w 704643072
istore 3
goto L1
L4:
ldc "rw"
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
ldc_w 939524096
istore 3
goto L1
L5:
ldc "rwt"
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L6
ldc_w 1006632960
istore 3
goto L1
L6:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Invalid mode: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 4
.limit stack 5
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
aload 0
getfield android/support/v4/c/q/k Landroid/support/v4/c/r;
aload 1
invokeinterface android/support/v4/c/r/a(Landroid/net/Uri;)Ljava/io/File; 1
astore 3
aload 2
astore 1
aload 2
ifnonnull L0
getstatic android/support/v4/c/q/a [Ljava/lang/String;
astore 1
L0:
aload 1
arraylength
anewarray java/lang/String
astore 4
aload 1
arraylength
anewarray java/lang/Object
astore 2
aload 1
arraylength
istore 9
iconst_0
istore 7
iconst_0
istore 6
L1:
iload 7
iload 9
if_icmpge L2
aload 1
iload 7
aaload
astore 5
ldc "_display_name"
aload 5
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
aload 4
iload 6
ldc "_display_name"
aastore
iload 6
iconst_1
iadd
istore 8
aload 2
iload 6
aload 3
invokevirtual java/io/File/getName()Ljava/lang/String;
aastore
iload 8
istore 6
L4:
iload 7
iconst_1
iadd
istore 7
goto L1
L3:
ldc "_size"
aload 5
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
aload 4
iload 6
ldc "_size"
aastore
iload 6
iconst_1
iadd
istore 8
aload 2
iload 6
aload 3
invokevirtual java/io/File/length()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
iload 8
istore 6
goto L4
L2:
iload 6
anewarray java/lang/String
astore 1
aload 4
iconst_0
aload 1
iconst_0
iload 6
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 6
anewarray java/lang/Object
astore 3
aload 2
iconst_0
aload 3
iconst_0
iload 6
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
new android/database/MatrixCursor
dup
aload 1
iconst_1
invokespecial android/database/MatrixCursor/<init>([Ljava/lang/String;I)V
astore 1
aload 1
aload 3
invokevirtual android/database/MatrixCursor/addRow([Ljava/lang/Object;)V
aload 1
areturn
L5:
goto L4
.limit locals 10
.limit stack 5
.end method

.method public final update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
new java/lang/UnsupportedOperationException
dup
ldc "No external updates"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 5
.limit stack 3
.end method
