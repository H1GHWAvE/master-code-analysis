.bytecode 50.0
.class public final synchronized android/support/v4/media/n
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a()Ljava/lang/Object;
new android/media/MediaMetadata$Builder
dup
invokespecial android/media/MediaMetadata$Builder/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/media/MediaMetadata$Builder
invokevirtual android/media/MediaMetadata$Builder/build()Landroid/media/MediaMetadata;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;J)V
aload 0
checkcast android/media/MediaMetadata$Builder
aload 1
lload 2
invokevirtual android/media/MediaMetadata$Builder/putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;
pop
return
.limit locals 4
.limit stack 4
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;Landroid/graphics/Bitmap;)V
aload 0
checkcast android/media/MediaMetadata$Builder
aload 1
aload 2
invokevirtual android/media/MediaMetadata$Builder/putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/media/MediaMetadata$Builder;
pop
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/CharSequence;)V
aload 0
checkcast android/media/MediaMetadata$Builder
aload 1
aload 2
invokevirtual android/media/MediaMetadata$Builder/putText(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/media/MediaMetadata$Builder;
pop
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
aload 0
checkcast android/media/MediaMetadata$Builder
aload 1
aload 2
checkcast android/media/Rating
invokevirtual android/media/MediaMetadata$Builder/putRating(Ljava/lang/String;Landroid/media/Rating;)Landroid/media/MediaMetadata$Builder;
pop
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
aload 0
checkcast android/media/MediaMetadata$Builder
aload 1
aload 2
invokevirtual android/media/MediaMetadata$Builder/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;
pop
return
.limit locals 3
.limit stack 3
.end method
