.bytecode 50.0
.class public final synchronized android/support/v4/media/session/PlaybackStateCompat
.super java/lang/Object
.implements android/os/Parcelable

.field public static final 'A' J = -1L


.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field public static final 'a' J = 1L


.field public static final 'b' J = 2L


.field public static final 'c' J = 4L


.field public static final 'd' J = 8L


.field public static final 'e' J = 16L


.field public static final 'f' J = 32L


.field public static final 'g' J = 64L


.field public static final 'h' J = 128L


.field public static final 'i' J = 256L


.field public static final 'j' J = 512L


.field public static final 'k' J = 1024L


.field public static final 'l' J = 2048L


.field public static final 'm' J = 4096L


.field public static final 'n' J = 8192L


.field public static final 'o' I = 0


.field public static final 'p' I = 1


.field public static final 'q' I = 2


.field public static final 'r' I = 3


.field public static final 's' I = 4


.field public static final 't' I = 5


.field public static final 'u' I = 6


.field public static final 'v' I = 7


.field public static final 'w' I = 8


.field public static final 'x' I = 9


.field public static final 'y' I = 10


.field public static final 'z' I = 11


.field final 'B' I

.field final 'C' J

.field final 'D' J

.field final 'E' F

.field final 'F' J

.field final 'G' Ljava/lang/CharSequence;

.field final 'H' J

.field 'I' Ljava/util/List;

.field final 'J' J

.field final 'K' Landroid/os/Bundle;

.field 'L' Ljava/lang/Object;

.method static <clinit>()V
new android/support/v4/media/session/bj
dup
invokespecial android/support/v4/media/session/bj/<init>()V
putstatic android/support/v4/media/session/PlaybackStateCompat/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>(IJJFJLjava/lang/CharSequence;JLjava/util/List;JLandroid/os/Bundle;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield android/support/v4/media/session/PlaybackStateCompat/B I
aload 0
lload 2
putfield android/support/v4/media/session/PlaybackStateCompat/C J
aload 0
lload 4
putfield android/support/v4/media/session/PlaybackStateCompat/D J
aload 0
fload 6
putfield android/support/v4/media/session/PlaybackStateCompat/E F
aload 0
lload 7
putfield android/support/v4/media/session/PlaybackStateCompat/F J
aload 0
aload 9
putfield android/support/v4/media/session/PlaybackStateCompat/G Ljava/lang/CharSequence;
aload 0
lload 10
putfield android/support/v4/media/session/PlaybackStateCompat/H J
aload 0
new java/util/ArrayList
dup
aload 12
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
putfield android/support/v4/media/session/PlaybackStateCompat/I Ljava/util/List;
aload 0
lload 13
putfield android/support/v4/media/session/PlaybackStateCompat/J J
aload 0
aload 15
putfield android/support/v4/media/session/PlaybackStateCompat/K Landroid/os/Bundle;
return
.limit locals 16
.limit stack 4
.end method

.method synthetic <init>(IJJFJLjava/lang/CharSequence;JLjava/util/List;JLandroid/os/Bundle;B)V
aload 0
iload 1
lload 2
lload 4
fload 6
lload 7
aload 9
lload 10
aload 12
lload 13
aload 15
invokespecial android/support/v4/media/session/PlaybackStateCompat/<init>(IJJFJLjava/lang/CharSequence;JLjava/util/List;JLandroid/os/Bundle;)V
return
.limit locals 17
.limit stack 16
.end method

.method private <init>(Landroid/os/Parcel;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/media/session/PlaybackStateCompat/B I
aload 0
aload 1
invokevirtual android/os/Parcel/readLong()J
putfield android/support/v4/media/session/PlaybackStateCompat/C J
aload 0
aload 1
invokevirtual android/os/Parcel/readFloat()F
putfield android/support/v4/media/session/PlaybackStateCompat/E F
aload 0
aload 1
invokevirtual android/os/Parcel/readLong()J
putfield android/support/v4/media/session/PlaybackStateCompat/H J
aload 0
aload 1
invokevirtual android/os/Parcel/readLong()J
putfield android/support/v4/media/session/PlaybackStateCompat/D J
aload 0
aload 1
invokevirtual android/os/Parcel/readLong()J
putfield android/support/v4/media/session/PlaybackStateCompat/F J
aload 0
getstatic android/text/TextUtils/CHAR_SEQUENCE_CREATOR Landroid/os/Parcelable$Creator;
aload 1
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast java/lang/CharSequence
putfield android/support/v4/media/session/PlaybackStateCompat/G Ljava/lang/CharSequence;
aload 0
aload 1
getstatic android/support/v4/media/session/PlaybackStateCompat$CustomAction/CREATOR Landroid/os/Parcelable$Creator;
invokevirtual android/os/Parcel/createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
putfield android/support/v4/media/session/PlaybackStateCompat/I Ljava/util/List;
aload 0
aload 1
invokevirtual android/os/Parcel/readLong()J
putfield android/support/v4/media/session/PlaybackStateCompat/J J
aload 0
aload 1
invokevirtual android/os/Parcel/readBundle()Landroid/os/Bundle;
putfield android/support/v4/media/session/PlaybackStateCompat/K Landroid/os/Bundle;
return
.limit locals 2
.limit stack 3
.end method

.method synthetic <init>(Landroid/os/Parcel;B)V
aload 0
aload 1
invokespecial android/support/v4/media/session/PlaybackStateCompat/<init>(Landroid/os/Parcel;)V
return
.limit locals 3
.limit stack 2
.end method

.method private a()I
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/B I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/support/v4/media/session/PlaybackStateCompat;)I
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/B I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/Object;)Landroid/support/v4/media/session/PlaybackStateCompat;
aload 0
ifnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aconst_null
areturn
L1:
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getCustomActions()Ljava/util/List;
astore 3
aconst_null
astore 1
aload 3
ifnull L2
new java/util/ArrayList
dup
aload 3
invokeinterface java/util/List/size()I 0
invokespecial java/util/ArrayList/<init>(I)V
astore 2
aload 3
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L3:
aload 2
astore 1
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 2
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokestatic android/support/v4/media/session/PlaybackStateCompat$CustomAction/a(Ljava/lang/Object;)Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L3
L2:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 22
if_icmplt L4
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getExtras()Landroid/os/Bundle;
astore 2
L5:
new android/support/v4/media/session/PlaybackStateCompat
dup
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getState()I
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getPosition()J
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getBufferedPosition()J
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getPlaybackSpeed()F
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getActions()J
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getErrorMessage()Ljava/lang/CharSequence;
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getLastPositionUpdateTime()J
aload 1
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getActiveQueueItemId()J
aload 2
invokespecial android/support/v4/media/session/PlaybackStateCompat/<init>(IJJFJLjava/lang/CharSequence;JLjava/util/List;JLandroid/os/Bundle;)V
astore 1
aload 1
aload 0
putfield android/support/v4/media/session/PlaybackStateCompat/L Ljava/lang/Object;
aload 1
areturn
L4:
aconst_null
astore 2
goto L5
.limit locals 4
.limit stack 17
.end method

.method private b()J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/C J
lreturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic b(Landroid/support/v4/media/session/PlaybackStateCompat;)J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/C J
lreturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic c(Landroid/support/v4/media/session/PlaybackStateCompat;)F
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/E F
freturn
.limit locals 1
.limit stack 1
.end method

.method private c()J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/D J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private d()F
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/E F
freturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Landroid/support/v4/media/session/PlaybackStateCompat;)J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/H J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private e()J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/F J
lreturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic e(Landroid/support/v4/media/session/PlaybackStateCompat;)J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/D J
lreturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic f(Landroid/support/v4/media/session/PlaybackStateCompat;)J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/F J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private f()Ljava/util/List;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/I Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/G Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Landroid/support/v4/media/session/PlaybackStateCompat;)Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/G Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private h()J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/H J
lreturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic h(Landroid/support/v4/media/session/PlaybackStateCompat;)Ljava/util/List;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/I Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method private i()J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/J J
lreturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic i(Landroid/support/v4/media/session/PlaybackStateCompat;)J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/J J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private j()Landroid/os/Bundle;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/K Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic j(Landroid/support/v4/media/session/PlaybackStateCompat;)Landroid/os/Bundle;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/K Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method private k()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/L Ljava/lang/Object;
ifnonnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/L Ljava/lang/Object;
areturn
L1:
aconst_null
astore 2
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/I Ljava/util/List;
ifnull L2
new java/util/ArrayList
dup
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/I Ljava/util/List;
invokeinterface java/util/List/size()I 0
invokespecial java/util/ArrayList/<init>(I)V
astore 3
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/I Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L3:
aload 3
astore 2
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/v4/media/session/PlaybackStateCompat$CustomAction
astore 2
aload 2
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
ifnonnull L4
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L5
L4:
aload 2
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
astore 2
L6:
aload 3
aload 2
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L3
L5:
aload 2
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/a Ljava/lang/String;
astore 6
aload 2
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/b Ljava/lang/CharSequence;
astore 7
aload 2
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/c I
istore 1
aload 2
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/d Landroid/os/Bundle;
astore 5
new android/media/session/PlaybackState$CustomAction$Builder
dup
aload 6
aload 7
iload 1
invokespecial android/media/session/PlaybackState$CustomAction$Builder/<init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V
astore 6
aload 6
aload 5
invokevirtual android/media/session/PlaybackState$CustomAction$Builder/setExtras(Landroid/os/Bundle;)Landroid/media/session/PlaybackState$CustomAction$Builder;
pop
aload 2
aload 6
invokevirtual android/media/session/PlaybackState$CustomAction$Builder/build()Landroid/media/session/PlaybackState$CustomAction;
putfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
aload 2
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
astore 2
goto L6
L2:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 22
if_icmplt L7
aload 0
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/B I
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/C J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/D J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/E F
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/F J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/G Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/H J
aload 2
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/J J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/K Landroid/os/Bundle;
invokestatic android/support/v4/media/session/br/a(IJJFJLjava/lang/CharSequence;JLjava/util/List;JLandroid/os/Bundle;)Ljava/lang/Object;
putfield android/support/v4/media/session/PlaybackStateCompat/L Ljava/lang/Object;
L8:
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/L Ljava/lang/Object;
areturn
L7:
aload 0
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/B I
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/C J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/D J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/E F
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/F J
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/G Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/H J
aload 2
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/J J
invokestatic android/support/v4/media/session/bp/a(IJJFJLjava/lang/CharSequence;JLjava/util/List;J)Ljava/lang/Object;
putfield android/support/v4/media/session/PlaybackStateCompat/L Ljava/lang/Object;
goto L8
.limit locals 8
.limit stack 16
.end method

.method public final describeContents()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "PlaybackState {"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
astore 1
aload 1
ldc "state="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/B I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
aload 1
ldc ", position="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/C J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
pop
aload 1
ldc ", buffered position="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/D J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
pop
aload 1
ldc ", speed="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/E F
invokevirtual java/lang/StringBuilder/append(F)Ljava/lang/StringBuilder;
pop
aload 1
ldc ", updated="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/H J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
pop
aload 1
ldc ", actions="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/F J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
pop
aload 1
ldc ", error="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/G Ljava/lang/CharSequence;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
pop
aload 1
ldc ", custom actions="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/I Ljava/util/List;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
aload 1
ldc ", active item id="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/J J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
pop
aload 1
ldc "}"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/B I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/C J
invokevirtual android/os/Parcel/writeLong(J)V
aload 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/E F
invokevirtual android/os/Parcel/writeFloat(F)V
aload 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/H J
invokevirtual android/os/Parcel/writeLong(J)V
aload 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/D J
invokevirtual android/os/Parcel/writeLong(J)V
aload 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/F J
invokevirtual android/os/Parcel/writeLong(J)V
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/G Ljava/lang/CharSequence;
aload 1
iload 2
invokestatic android/text/TextUtils/writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/I Ljava/util/List;
invokevirtual android/os/Parcel/writeTypedList(Ljava/util/List;)V
aload 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/J J
invokevirtual android/os/Parcel/writeLong(J)V
aload 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat/K Landroid/os/Bundle;
invokevirtual android/os/Parcel/writeBundle(Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method
