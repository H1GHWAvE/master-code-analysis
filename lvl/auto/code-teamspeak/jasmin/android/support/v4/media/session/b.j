.bytecode 50.0
.class public synchronized abstract android/support/v4/media/session/b
.super android/os/Binder
.implements android/support/v4/media/session/a

.field static final 'a' I = 1


.field static final 'b' I = 2


.field static final 'c' I = 3


.field static final 'd' I = 4


.field static final 'e' I = 5


.field static final 'f' I = 6


.field static final 'g' I = 7


.field static final 'h' I = 8


.field private static final 'i' Ljava/lang/String; = "android.support.v4.media.session.IMediaControllerCallback"

.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/support/v4/media/session/b/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static a(Landroid/os/IBinder;)Landroid/support/v4/media/session/a;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof android/support/v4/media/session/a
ifeq L1
aload 1
checkcast android/support/v4/media/session/a
areturn
L1:
new android/support/v4/media/session/c
dup
aload 0
invokespecial android/support/v4/media/session/c/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
aconst_null
astore 6
aconst_null
astore 7
aconst_null
astore 8
aconst_null
astore 9
aconst_null
astore 10
aconst_null
astore 5
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
4 : L3
5 : L4
6 : L5
7 : L6
8 : L7
1598968902 : L8
default : L9
L9:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L8:
aload 3
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 6
aload 5
astore 3
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L10
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 3
L10:
aload 0
aload 6
aload 3
invokevirtual android/support/v4/media/session/b/a(Ljava/lang/String;Landroid/os/Bundle;)V
iconst_1
ireturn
L1:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/b/a()V
iconst_1
ireturn
L2:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 6
astore 3
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L11
getstatic android/support/v4/media/session/PlaybackStateCompat/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/support/v4/media/session/PlaybackStateCompat
astore 3
L11:
aload 0
aload 3
invokevirtual android/support/v4/media/session/b/a(Landroid/support/v4/media/session/PlaybackStateCompat;)V
iconst_1
ireturn
L3:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 7
astore 3
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L12
getstatic android/support/v4/media/MediaMetadataCompat/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/support/v4/media/MediaMetadataCompat
astore 3
L12:
aload 0
aload 3
invokevirtual android/support/v4/media/session/b/a(Landroid/support/v4/media/MediaMetadataCompat;)V
iconst_1
ireturn
L4:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
getstatic android/support/v4/media/session/MediaSessionCompat$QueueItem/CREATOR Landroid/os/Parcelable$Creator;
invokevirtual android/os/Parcel/createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
invokevirtual android/support/v4/media/session/b/a(Ljava/util/List;)V
iconst_1
ireturn
L5:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 8
astore 3
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L13
getstatic android/text/TextUtils/CHAR_SEQUENCE_CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast java/lang/CharSequence
astore 3
L13:
aload 0
aload 3
invokevirtual android/support/v4/media/session/b/a(Ljava/lang/CharSequence;)V
iconst_1
ireturn
L6:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 9
astore 3
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L14
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 3
L14:
aload 0
aload 3
invokevirtual android/support/v4/media/session/b/a(Landroid/os/Bundle;)V
iconst_1
ireturn
L7:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 10
astore 3
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L15
getstatic android/support/v4/media/session/ParcelableVolumeInfo/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/support/v4/media/session/ParcelableVolumeInfo
astore 3
L15:
aload 0
aload 3
invokevirtual android/support/v4/media/session/b/a(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
iconst_1
ireturn
.limit locals 11
.limit stack 5
.end method
