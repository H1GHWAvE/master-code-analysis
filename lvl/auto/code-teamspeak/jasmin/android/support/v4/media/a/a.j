.bytecode 50.0
.class synchronized android/support/v4/media/a/a
.super java/lang/Object

.field public static final 'a' I = 1


.field public static final 'b' I = 2


.field public static final 'c' I = 8388608


.field public static final 'd' I = 8388611


.field private static final 'e' Ljava/lang/String; = "MediaRouterJellybean"

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/Context;)Ljava/lang/Object;
aload 0
ldc "media_router"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Landroid/support/v4/media/a/b;)Ljava/lang/Object;
new android/support/v4/media/a/c
dup
aload 0
invokespecial android/support/v4/media/a/c/<init>(Landroid/support/v4/media/a/b;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Landroid/support/v4/media/a/j;)Ljava/lang/Object;
new android/support/v4/media/a/k
dup
aload 0
invokespecial android/support/v4/media/a/k/<init>(Landroid/support/v4/media/a/j;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;I)Ljava/lang/Object;
aload 0
checkcast android/media/MediaRouter
iload 1
invokevirtual android/media/MediaRouter/getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;Z)Ljava/lang/Object;
aload 0
checkcast android/media/MediaRouter
aload 1
iload 2
invokevirtual android/media/MediaRouter/createRouteCategory(Ljava/lang/CharSequence;Z)Landroid/media/MediaRouter$RouteCategory;
areturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;)Ljava/util/List;
aload 0
checkcast android/media/MediaRouter
astore 0
aload 0
invokevirtual android/media/MediaRouter/getRouteCount()I
istore 2
new java/util/ArrayList
dup
iload 2
invokespecial java/util/ArrayList/<init>(I)V
astore 3
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 3
aload 0
iload 1
invokevirtual android/media/MediaRouter/getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 3
areturn
.limit locals 4
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;ILjava/lang/Object;)V
aload 0
checkcast android/media/MediaRouter
iload 1
aload 2
checkcast android/media/MediaRouter$RouteInfo
invokevirtual android/media/MediaRouter/selectRoute(ILandroid/media/MediaRouter$RouteInfo;)V
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/MediaRouter
aload 1
checkcast android/media/MediaRouter$Callback
invokevirtual android/media/MediaRouter/removeCallback(Landroid/media/MediaRouter$Callback;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/media/MediaRouter
aload 1
checkcast android/media/MediaRouter$RouteCategory
invokevirtual android/media/MediaRouter/createUserRoute(Landroid/media/MediaRouter$RouteCategory;)Landroid/media/MediaRouter$UserRouteInfo;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;)Ljava/util/List;
aload 0
checkcast android/media/MediaRouter
astore 0
aload 0
invokevirtual android/media/MediaRouter/getCategoryCount()I
istore 2
new java/util/ArrayList
dup
iload 2
invokespecial java/util/ArrayList/<init>(I)V
astore 3
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 3
aload 0
iload 1
invokevirtual android/media/MediaRouter/getCategoryAt(I)Landroid/media/MediaRouter$RouteCategory;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 3
areturn
.limit locals 4
.limit stack 3
.end method

.method private static b(Ljava/lang/Object;ILjava/lang/Object;)V
aload 0
checkcast android/media/MediaRouter
iload 1
aload 2
checkcast android/media/MediaRouter$Callback
invokevirtual android/media/MediaRouter/addCallback(ILandroid/media/MediaRouter$Callback;)V
return
.limit locals 3
.limit stack 3
.end method

.method private static c(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/MediaRouter
aload 1
checkcast android/media/MediaRouter$UserRouteInfo
invokevirtual android/media/MediaRouter/addUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static d(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/MediaRouter
aload 1
checkcast android/media/MediaRouter$UserRouteInfo
invokevirtual android/media/MediaRouter/removeUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V
return
.limit locals 2
.limit stack 2
.end method
