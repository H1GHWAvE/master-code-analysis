.bytecode 50.0
.class final synchronized android/support/v4/media/session/p
.super java/lang/Object
.implements android/support/v4/media/session/m

.field private 'a' Landroid/support/v4/media/session/MediaSessionCompat$Token;

.field private 'b' Landroid/support/v4/media/session/d;

.field private 'c' Landroid/support/v4/media/session/r;

.method public <init>(Landroid/support/v4/media/session/MediaSessionCompat$Token;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/media/session/p/a Landroid/support/v4/media/session/MediaSessionCompat$Token;
aload 0
aload 1
getfield android/support/v4/media/session/MediaSessionCompat$Token/a Ljava/lang/Object;
checkcast android/os/IBinder
invokestatic android/support/v4/media/session/e/a(Landroid/os/IBinder;)Landroid/support/v4/media/session/d;
putfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
return
.limit locals 2
.limit stack 2
.end method

.method public final a()Landroid/support/v4/media/session/r;
aload 0
getfield android/support/v4/media/session/p/c Landroid/support/v4/media/session/r;
ifnonnull L0
aload 0
new android/support/v4/media/session/u
dup
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokespecial android/support/v4/media/session/u/<init>(Landroid/support/v4/media/session/d;)V
putfield android/support/v4/media/session/p/c Landroid/support/v4/media/session/r;
L0:
aload 0
getfield android/support/v4/media/session/p/c Landroid/support/v4/media/session/r;
areturn
.limit locals 1
.limit stack 4
.end method

.method public final a(II)V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
iload 1
iload 2
aconst_null
invokeinterface android/support/v4/media/session/d/b(IILjava/lang/String;)V 3
L1:
return
L2:
astore 3
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in setVolumeTo. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 4
.limit stack 4
.end method

.method public final a(Landroid/support/v4/media/session/i;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "callback may not be null."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
aload 1
invokestatic android/support/v4/media/session/i/c(Landroid/support/v4/media/session/i;)Ljava/lang/Object;
checkcast android/support/v4/media/session/a
invokeinterface android/support/v4/media/session/d/b(Landroid/support/v4/media/session/a;)V 1
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/asBinder()Landroid/os/IBinder; 0
aload 1
iconst_0
invokeinterface android/os/IBinder/unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z 2
pop
aload 1
iconst_0
invokestatic android/support/v4/media/session/i/a(Landroid/support/v4/media/session/i;Z)Z
pop
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in unregisterCallback. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 2
.limit stack 4
.end method

.method public final a(Landroid/support/v4/media/session/i;Landroid/os/Handler;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "callback may not be null."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/asBinder()Landroid/os/IBinder; 0
aload 1
iconst_0
invokeinterface android/os/IBinder/linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V 2
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
aload 1
invokestatic android/support/v4/media/session/i/c(Landroid/support/v4/media/session/i;)Ljava/lang/Object;
checkcast android/support/v4/media/session/a
invokeinterface android/support/v4/media/session/d/a(Landroid/support/v4/media/session/a;)V 1
aload 1
aload 2
invokestatic android/support/v4/media/session/i/a(Landroid/support/v4/media/session/i;Landroid/os/Handler;)V
aload 1
iconst_1
invokestatic android/support/v4/media/session/i/a(Landroid/support/v4/media/session/i;Z)Z
pop
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in registerCallback. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 3
.limit stack 4
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
aload 1
aload 2
new android/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper
dup
aload 3
invokespecial android/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper/<init>(Landroid/os/ResultReceiver;)V
invokeinterface android/support/v4/media/session/d/a(Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper;)V 3
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in sendCommand. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 4
.limit stack 6
.end method

.method public final a(Landroid/view/KeyEvent;)Z
.catch android/os/RemoteException from L0 to L1 using L2
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "event may not be null."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
aload 1
invokeinterface android/support/v4/media/session/d/a(Landroid/view/KeyEvent;)Z 1
pop
L1:
iconst_0
ireturn
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in dispatchMediaButtonEvent. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L1
.limit locals 2
.limit stack 4
.end method

.method public final b()Landroid/support/v4/media/session/PlaybackStateCompat;
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/o()Landroid/support/v4/media/session/PlaybackStateCompat; 0
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in getPlaybackState. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method

.method public final b(II)V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
iload 1
iload 2
aconst_null
invokeinterface android/support/v4/media/session/d/a(IILjava/lang/String;)V 3
L1:
return
L2:
astore 3
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in adjustVolume. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 4
.limit stack 4
.end method

.method public final c()Landroid/support/v4/media/MediaMetadataCompat;
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/n()Landroid/support/v4/media/MediaMetadataCompat; 0
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in getMetadata. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method

.method public final d()Ljava/util/List;
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/p()Ljava/util/List; 0
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in getQueue. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method

.method public final e()Ljava/lang/CharSequence;
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/q()Ljava/lang/CharSequence; 0
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in getQueueTitle. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method

.method public final f()Landroid/os/Bundle;
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/r()Landroid/os/Bundle; 0
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in getExtras. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method

.method public final g()I
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/s()I 0
istore 1
L1:
iload 1
ireturn
L2:
astore 2
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in getRatingType. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
.limit locals 3
.limit stack 4
.end method

.method public final h()J
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/e()J 0
lstore 1
L1:
lload 1
lreturn
L2:
astore 3
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in getFlags. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
lconst_0
lreturn
.limit locals 4
.limit stack 4
.end method

.method public final i()Landroid/support/v4/media/session/q;
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/f()Landroid/support/v4/media/session/ParcelableVolumeInfo; 0
astore 1
new android/support/v4/media/session/q
dup
aload 1
getfield android/support/v4/media/session/ParcelableVolumeInfo/a I
aload 1
getfield android/support/v4/media/session/ParcelableVolumeInfo/b I
aload 1
getfield android/support/v4/media/session/ParcelableVolumeInfo/c I
aload 1
getfield android/support/v4/media/session/ParcelableVolumeInfo/d I
aload 1
getfield android/support/v4/media/session/ParcelableVolumeInfo/e I
invokespecial android/support/v4/media/session/q/<init>(IIIII)V
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in getPlaybackInfo. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 7
.end method

.method public final j()Landroid/app/PendingIntent;
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/d()Landroid/app/PendingIntent; 0
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in getSessionActivity. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method

.method public final k()Ljava/lang/String;
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/p/b Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/b()Ljava/lang/String; 0
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in getPackageName. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method

.method public final l()Ljava/lang/Object;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method
