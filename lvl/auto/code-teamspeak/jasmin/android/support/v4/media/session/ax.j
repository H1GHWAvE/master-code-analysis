.bytecode 50.0
.class public final synchronized android/support/v4/media/session/ax
.super java/lang/Object

.field private static final 'a' J = 128L


.field private static final 'b' Ljava/lang/String; = "android.media.metadata.USER_RATING"

.field private static final 'c' Ljava/lang/String; = "android.media.metadata.RATING"

.field private static final 'd' Ljava/lang/String; = "android.media.metadata.YEAR"

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(J)I
lload 0
invokestatic android/support/v4/media/session/av/a(J)I
istore 3
iload 3
istore 2
ldc2_w 128L
lload 0
land
lconst_0
lcmp
ifeq L0
iload 3
sipush 512
ior
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static a(Landroid/support/v4/media/session/au;)Ljava/lang/Object;
new android/support/v4/media/session/ay
dup
aload 0
invokespecial android/support/v4/media/session/ay/<init>(Landroid/support/v4/media/session/au;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Landroid/os/Bundle;Landroid/media/RemoteControlClient$MetadataEditor;)V
aload 0
ifnonnull L0
L1:
return
L0:
aload 0
ldc "android.media.metadata.YEAR"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L2
aload 1
bipush 8
aload 0
ldc "android.media.metadata.YEAR"
invokevirtual android/os/Bundle/getLong(Ljava/lang/String;)J
invokevirtual android/media/RemoteControlClient$MetadataEditor/putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L2:
aload 0
ldc "android.media.metadata.RATING"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L3
aload 1
bipush 101
aload 0
ldc "android.media.metadata.RATING"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putObject(ILjava/lang/Object;)Landroid/media/MediaMetadataEditor;
pop
L3:
aload 0
ldc "android.media.metadata.USER_RATING"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L1
aload 1
ldc_w 268435457
aload 0
ldc "android.media.metadata.USER_RATING"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putObject(ILjava/lang/Object;)Landroid/media/MediaMetadataEditor;
pop
return
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/lang/Object;J)V
aload 0
checkcast android/media/RemoteControlClient
astore 0
lload 1
invokestatic android/support/v4/media/session/av/a(J)I
istore 4
iload 4
istore 3
ldc2_w 128L
lload 1
land
lconst_0
lcmp
ifeq L0
iload 4
sipush 512
ior
istore 3
L0:
aload 0
iload 3
invokevirtual android/media/RemoteControlClient/setTransportControlFlags(I)V
return
.limit locals 5
.limit stack 4
.end method

.method private static a(Ljava/lang/Object;Landroid/os/Bundle;J)V
aload 0
checkcast android/media/RemoteControlClient
iconst_1
invokevirtual android/media/RemoteControlClient/editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;
astore 0
aload 1
aload 0
invokestatic android/support/v4/media/session/at/a(Landroid/os/Bundle;Landroid/media/RemoteControlClient$MetadataEditor;)V
aload 1
ifnull L0
aload 1
ldc "android.media.metadata.YEAR"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L1
aload 0
bipush 8
aload 1
ldc "android.media.metadata.YEAR"
invokevirtual android/os/Bundle/getLong(Ljava/lang/String;)J
invokevirtual android/media/RemoteControlClient$MetadataEditor/putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L1:
aload 1
ldc "android.media.metadata.RATING"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L2
aload 0
bipush 101
aload 1
ldc "android.media.metadata.RATING"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putObject(ILjava/lang/Object;)Landroid/media/MediaMetadataEditor;
pop
L2:
aload 1
ldc "android.media.metadata.USER_RATING"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L0
aload 0
ldc_w 268435457
aload 1
ldc "android.media.metadata.USER_RATING"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putObject(ILjava/lang/Object;)Landroid/media/MediaMetadataEditor;
pop
L0:
ldc2_w 128L
lload 2
land
lconst_0
lcmp
ifeq L3
aload 0
ldc_w 268435457
invokevirtual android/media/RemoteControlClient$MetadataEditor/addEditableKey(I)V
L3:
aload 0
invokevirtual android/media/RemoteControlClient$MetadataEditor/apply()V
return
.limit locals 4
.limit stack 4
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/RemoteControlClient
aload 1
checkcast android/media/RemoteControlClient$OnMetadataUpdateListener
invokevirtual android/media/RemoteControlClient/setMetadataUpdateListener(Landroid/media/RemoteControlClient$OnMetadataUpdateListener;)V
return
.limit locals 2
.limit stack 2
.end method
