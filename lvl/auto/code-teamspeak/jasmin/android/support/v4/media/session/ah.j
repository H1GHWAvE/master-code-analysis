.bytecode 50.0
.class final synchronized android/support/v4/media/session/ah
.super java/lang/Object
.implements android/support/v4/media/session/ag

.field private final 'a' Ljava/lang/Object;

.field private final 'b' Landroid/support/v4/media/session/MediaSessionCompat$Token;

.field private 'c' Landroid/app/PendingIntent;

.method public <init>(Landroid/content/Context;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/media/session/MediaSession
dup
aload 1
aload 2
invokespecial android/media/session/MediaSession/<init>(Landroid/content/Context;Ljava/lang/String;)V
putfield android/support/v4/media/session/ah/a Ljava/lang/Object;
aload 0
new android/support/v4/media/session/MediaSessionCompat$Token
dup
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
checkcast android/media/session/MediaSession
invokevirtual android/media/session/MediaSession/getSessionToken()Landroid/media/session/MediaSession$Token;
invokespecial android/support/v4/media/session/MediaSessionCompat$Token/<init>(Ljava/lang/Object;)V
putfield android/support/v4/media/session/ah/b Landroid/support/v4/media/session/MediaSessionCompat$Token;
return
.limit locals 3
.limit stack 5
.end method

.method public <init>(Ljava/lang/Object;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
instanceof android/media/session/MediaSession
ifeq L0
aload 0
aload 1
putfield android/support/v4/media/session/ah/a Ljava/lang/Object;
aload 0
new android/support/v4/media/session/MediaSessionCompat$Token
dup
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
checkcast android/media/session/MediaSession
invokevirtual android/media/session/MediaSession/getSessionToken()Landroid/media/session/MediaSession$Token;
invokespecial android/support/v4/media/session/MediaSessionCompat$Token/<init>(Ljava/lang/Object;)V
putfield android/support/v4/media/session/ah/b Landroid/support/v4/media/session/MediaSessionCompat$Token;
return
L0:
new java/lang/IllegalArgumentException
dup
ldc "mediaSession is not a valid MediaSession object"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 4
.end method

.method public final a(I)V
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
checkcast android/media/session/MediaSession
iload 1
invokevirtual android/media/session/MediaSession/setFlags(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/app/PendingIntent;)V
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
checkcast android/media/session/MediaSession
aload 1
invokevirtual android/media/session/MediaSession/setSessionActivity(Landroid/app/PendingIntent;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
checkcast android/media/session/MediaSession
aload 1
invokevirtual android/media/session/MediaSession/setExtras(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v4/media/MediaMetadataCompat;)V
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
astore 4
aload 1
getfield android/support/v4/media/MediaMetadataCompat/D Ljava/lang/Object;
ifnonnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aload 1
getfield android/support/v4/media/MediaMetadataCompat/D Ljava/lang/Object;
astore 1
L2:
aload 4
checkcast android/media/session/MediaSession
aload 1
checkcast android/media/MediaMetadata
invokevirtual android/media/session/MediaSession/setMetadata(Landroid/media/MediaMetadata;)V
return
L1:
new android/media/MediaMetadata$Builder
dup
invokespecial android/media/MediaMetadata$Builder/<init>()V
astore 5
aload 1
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
invokevirtual android/os/Bundle/keySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 6
L3:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 7
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
aload 7
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
astore 8
aload 8
ifnull L3
aload 8
invokevirtual java/lang/Integer/intValue()I
tableswitch 0
L5
L6
L7
L8
default : L9
L9:
goto L3
L5:
aload 1
aload 7
invokevirtual android/support/v4/media/MediaMetadataCompat/b(Ljava/lang/String;)J
lstore 2
aload 5
checkcast android/media/MediaMetadata$Builder
aload 7
lload 2
invokevirtual android/media/MediaMetadata$Builder/putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;
pop
goto L3
L7:
aload 1
aload 7
invokevirtual android/support/v4/media/MediaMetadataCompat/d(Ljava/lang/String;)Landroid/graphics/Bitmap;
astore 8
aload 5
checkcast android/media/MediaMetadata$Builder
aload 7
aload 8
invokevirtual android/media/MediaMetadata$Builder/putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/media/MediaMetadata$Builder;
pop
goto L3
L8:
aload 1
aload 7
invokevirtual android/support/v4/media/MediaMetadataCompat/c(Ljava/lang/String;)Landroid/support/v4/media/RatingCompat;
invokevirtual android/support/v4/media/RatingCompat/a()Ljava/lang/Object;
astore 8
aload 5
checkcast android/media/MediaMetadata$Builder
aload 7
aload 8
checkcast android/media/Rating
invokevirtual android/media/MediaMetadata$Builder/putRating(Ljava/lang/String;Landroid/media/Rating;)Landroid/media/MediaMetadata$Builder;
pop
goto L3
L6:
aload 1
aload 7
invokevirtual android/support/v4/media/MediaMetadataCompat/a(Ljava/lang/String;)Ljava/lang/CharSequence;
astore 8
aload 5
checkcast android/media/MediaMetadata$Builder
aload 7
aload 8
invokevirtual android/media/MediaMetadata$Builder/putText(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/media/MediaMetadata$Builder;
pop
goto L3
L4:
aload 1
aload 5
checkcast android/media/MediaMetadata$Builder
invokevirtual android/media/MediaMetadata$Builder/build()Landroid/media/MediaMetadata;
putfield android/support/v4/media/MediaMetadataCompat/D Ljava/lang/Object;
aload 1
getfield android/support/v4/media/MediaMetadataCompat/D Ljava/lang/Object;
astore 1
goto L2
.limit locals 9
.limit stack 4
.end method

.method public final a(Landroid/support/v4/media/ae;)V
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
astore 2
aload 1
invokevirtual android/support/v4/media/ae/a()Ljava/lang/Object;
astore 1
aload 2
checkcast android/media/session/MediaSession
aload 1
checkcast android/media/VolumeProvider
invokevirtual android/media/session/MediaSession/setPlaybackToRemote(Landroid/media/VolumeProvider;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/support/v4/media/session/PlaybackStateCompat;)V
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
astore 5
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/L Ljava/lang/Object;
ifnonnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/L Ljava/lang/Object;
astore 1
L2:
aload 5
checkcast android/media/session/MediaSession
aload 1
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/MediaSession/setPlaybackState(Landroid/media/session/PlaybackState;)V
return
L1:
aconst_null
astore 3
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/I Ljava/util/List;
ifnull L3
new java/util/ArrayList
dup
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/I Ljava/util/List;
invokeinterface java/util/List/size()I 0
invokespecial java/util/ArrayList/<init>(I)V
astore 4
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/I Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 6
L4:
aload 4
astore 3
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/v4/media/session/PlaybackStateCompat$CustomAction
astore 3
aload 3
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
ifnonnull L5
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L6
L5:
aload 3
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
astore 3
L7:
aload 4
aload 3
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L4
L6:
aload 3
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/a Ljava/lang/String;
astore 8
aload 3
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/b Ljava/lang/CharSequence;
astore 9
aload 3
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/c I
istore 2
aload 3
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/d Landroid/os/Bundle;
astore 7
new android/media/session/PlaybackState$CustomAction$Builder
dup
aload 8
aload 9
iload 2
invokespecial android/media/session/PlaybackState$CustomAction$Builder/<init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V
astore 8
aload 8
aload 7
invokevirtual android/media/session/PlaybackState$CustomAction$Builder/setExtras(Landroid/os/Bundle;)Landroid/media/session/PlaybackState$CustomAction$Builder;
pop
aload 3
aload 8
invokevirtual android/media/session/PlaybackState$CustomAction$Builder/build()Landroid/media/session/PlaybackState$CustomAction;
putfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
aload 3
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
astore 3
goto L7
L3:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 22
if_icmplt L8
aload 1
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/B I
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/C J
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/D J
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/E F
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/F J
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/G Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/H J
aload 3
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/J J
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/K Landroid/os/Bundle;
invokestatic android/support/v4/media/session/br/a(IJJFJLjava/lang/CharSequence;JLjava/util/List;JLandroid/os/Bundle;)Ljava/lang/Object;
putfield android/support/v4/media/session/PlaybackStateCompat/L Ljava/lang/Object;
L9:
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/L Ljava/lang/Object;
astore 1
goto L2
L8:
aload 1
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/B I
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/C J
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/D J
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/E F
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/F J
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/G Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/H J
aload 3
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/J J
invokestatic android/support/v4/media/session/bp/a(IJJFJLjava/lang/CharSequence;JLjava/util/List;J)Ljava/lang/Object;
putfield android/support/v4/media/session/PlaybackStateCompat/L Ljava/lang/Object;
goto L9
.limit locals 10
.limit stack 16
.end method

.method public final a(Landroid/support/v4/media/session/ad;Landroid/os/Handler;)V
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
astore 3
aload 1
getfield android/support/v4/media/session/ad/a Ljava/lang/Object;
astore 1
aload 3
checkcast android/media/session/MediaSession
aload 1
checkcast android/media/session/MediaSession$Callback
aload 2
invokevirtual android/media/session/MediaSession/setCallback(Landroid/media/session/MediaSession$Callback;Landroid/os/Handler;)V
return
.limit locals 4
.limit stack 3
.end method

.method public final a(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
checkcast android/media/session/MediaSession
aload 1
invokevirtual android/media/session/MediaSession/setQueueTitle(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
checkcast android/media/session/MediaSession
aload 1
aload 2
invokevirtual android/media/session/MediaSession/sendSessionEvent(Ljava/lang/String;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/util/List;)V
aconst_null
astore 4
aload 1
ifnull L0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 4
aload 1
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 5
L1:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/v4/media/session/MediaSessionCompat$QueueItem
astore 1
aload 1
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/d Ljava/lang/Object;
ifnonnull L2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L3
L2:
aload 1
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/d Ljava/lang/Object;
astore 1
L4:
aload 4
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L1
L3:
aload 1
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/b Landroid/support/v4/media/MediaDescriptionCompat;
invokevirtual android/support/v4/media/MediaDescriptionCompat/a()Ljava/lang/Object;
astore 6
aload 1
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/c J
lstore 2
aload 1
new android/media/session/MediaSession$QueueItem
dup
aload 6
checkcast android/media/MediaDescription
lload 2
invokespecial android/media/session/MediaSession$QueueItem/<init>(Landroid/media/MediaDescription;J)V
putfield android/support/v4/media/session/MediaSessionCompat$QueueItem/d Ljava/lang/Object;
aload 1
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/d Ljava/lang/Object;
astore 1
goto L4
L0:
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
aload 4
invokestatic android/support/v4/media/session/az/a(Ljava/lang/Object;Ljava/util/List;)V
return
.limit locals 7
.limit stack 6
.end method

.method public final a(Z)V
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
checkcast android/media/session/MediaSession
iload 1
invokevirtual android/media/session/MediaSession/setActive(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a()Z
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
checkcast android/media/session/MediaSession
invokevirtual android/media/session/MediaSession/isActive()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()V
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
checkcast android/media/session/MediaSession
invokevirtual android/media/session/MediaSession/release()V
return
.limit locals 1
.limit stack 1
.end method

.method public final b(I)V
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
astore 2
new android/media/AudioAttributes$Builder
dup
invokespecial android/media/AudioAttributes$Builder/<init>()V
astore 3
aload 3
iload 1
invokevirtual android/media/AudioAttributes$Builder/setLegacyStreamType(I)Landroid/media/AudioAttributes$Builder;
pop
aload 2
checkcast android/media/session/MediaSession
aload 3
invokevirtual android/media/AudioAttributes$Builder/build()Landroid/media/AudioAttributes;
invokevirtual android/media/session/MediaSession/setPlaybackToLocal(Landroid/media/AudioAttributes;)V
return
.limit locals 4
.limit stack 2
.end method

.method public final b(Landroid/app/PendingIntent;)V
aload 0
aload 1
putfield android/support/v4/media/session/ah/c Landroid/app/PendingIntent;
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
checkcast android/media/session/MediaSession
aload 1
invokevirtual android/media/session/MediaSession/setMediaButtonReceiver(Landroid/app/PendingIntent;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final c()Landroid/support/v4/media/session/MediaSessionCompat$Token;
aload 0
getfield android/support/v4/media/session/ah/b Landroid/support/v4/media/session/MediaSessionCompat$Token;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c(I)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 22
if_icmplt L0
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
checkcast android/media/session/MediaSession
iload 1
invokevirtual android/media/session/MediaSession/setRatingType(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final d()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/ah/a Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Ljava/lang/Object;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method
