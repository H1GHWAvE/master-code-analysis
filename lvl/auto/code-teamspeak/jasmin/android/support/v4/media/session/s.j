.bytecode 50.0
.class synchronized android/support/v4/media/session/s
.super android/support/v4/media/session/r

.field protected final 'a' Ljava/lang/Object;

.method public <init>(Ljava/lang/Object;)V
aload 0
invokespecial android/support/v4/media/session/r/<init>()V
aload 0
aload 1
putfield android/support/v4/media/session/s/a Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method public final a()V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/play()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(J)V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
checkcast android/media/session/MediaController$TransportControls
lload 1
invokevirtual android/media/session/MediaController$TransportControls/skipToQueueItem(J)V
return
.limit locals 3
.limit stack 3
.end method

.method public a(Landroid/net/Uri;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 0
.end method

.method public final a(Landroid/support/v4/media/RatingCompat;)V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
astore 2
aload 1
ifnull L0
aload 1
invokevirtual android/support/v4/media/RatingCompat/a()Ljava/lang/Object;
astore 1
L1:
aload 2
checkcast android/media/session/MediaController$TransportControls
aload 1
checkcast android/media/Rating
invokevirtual android/media/session/MediaController$TransportControls/setRating(Landroid/media/Rating;)V
return
L0:
aconst_null
astore 1
goto L1
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/a Ljava/lang/String;
aload 2
invokestatic android/support/v4/media/session/z/a(Ljava/lang/Object;Ljava/lang/String;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
checkcast android/media/session/MediaController$TransportControls
aload 1
aload 2
invokevirtual android/media/session/MediaController$TransportControls/playFromMediaId(Ljava/lang/String;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final b()V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/pause()V
return
.limit locals 1
.limit stack 1
.end method

.method public final b(J)V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
checkcast android/media/session/MediaController$TransportControls
lload 1
invokevirtual android/media/session/MediaController$TransportControls/seekTo(J)V
return
.limit locals 3
.limit stack 3
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
checkcast android/media/session/MediaController$TransportControls
aload 1
aload 2
invokevirtual android/media/session/MediaController$TransportControls/playFromSearch(Ljava/lang/String;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final c()V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/stop()V
return
.limit locals 1
.limit stack 1
.end method

.method public final c(Ljava/lang/String;Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
aload 1
aload 2
invokestatic android/support/v4/media/session/z/a(Ljava/lang/Object;Ljava/lang/String;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final d()V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/fastForward()V
return
.limit locals 1
.limit stack 1
.end method

.method public final e()V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/skipToNext()V
return
.limit locals 1
.limit stack 1
.end method

.method public final f()V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/rewind()V
return
.limit locals 1
.limit stack 1
.end method

.method public final g()V
aload 0
getfield android/support/v4/media/session/s/a Ljava/lang/Object;
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/skipToPrevious()V
return
.limit locals 1
.limit stack 1
.end method
