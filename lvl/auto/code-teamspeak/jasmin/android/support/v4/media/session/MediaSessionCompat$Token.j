.bytecode 50.0
.class public final synchronized android/support/v4/media/session/MediaSessionCompat$Token
.super java/lang/Object
.implements android/os/Parcelable

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field public final 'a' Ljava/lang/Object;

.method static <clinit>()V
new android/support/v4/media/session/as
dup
invokespecial android/support/v4/media/session/as/<init>()V
putstatic android/support/v4/media/session/MediaSessionCompat$Token/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method <init>(Ljava/lang/Object;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/media/session/MediaSessionCompat$Token/a Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;)Landroid/support/v4/media/session/MediaSessionCompat$Token;
aload 0
ifnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aconst_null
areturn
L1:
aload 0
instanceof android/media/session/MediaSession$Token
ifeq L2
new android/support/v4/media/session/MediaSessionCompat$Token
dup
aload 0
invokespecial android/support/v4/media/session/MediaSessionCompat$Token/<init>(Ljava/lang/Object;)V
areturn
L2:
new java/lang/IllegalArgumentException
dup
ldc "token is not a valid MediaSession.Token object"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method private a()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$Token/a Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final describeContents()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 1
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$Token/a Ljava/lang/Object;
checkcast android/os/Parcelable
iload 2
invokevirtual android/os/Parcel/writeParcelable(Landroid/os/Parcelable;I)V
return
L0:
aload 1
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$Token/a Ljava/lang/Object;
checkcast android/os/IBinder
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
return
.limit locals 3
.limit stack 3
.end method
