.bytecode 50.0
.class public final synchronized android/support/v7/app/j
.super java/lang/Object
.implements android/support/v4/widget/ac

.field 'a' Z

.field 'b' Landroid/view/View$OnClickListener;

.field private final 'c' Landroid/support/v7/app/l;

.field private final 'd' Landroid/support/v4/widget/DrawerLayout;

.field private 'e' Landroid/support/v7/app/o;

.field private 'f' Landroid/graphics/drawable/Drawable;

.field private 'g' Z

.field private final 'h' I

.field private final 'i' I

.field private 'j' Z

.method private <init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;II)V
.annotation invisibleparam 3 Landroid/support/a/ah;
.end annotation
.annotation invisibleparam 4 Landroid/support/a/ah;
.end annotation
aload 0
aload 1
aconst_null
aload 2
iload 3
iload 4
invokespecial android/support/v7/app/j/<init>(Landroid/app/Activity;Landroid/support/v7/widget/Toolbar;Landroid/support/v4/widget/DrawerLayout;II)V
return
.limit locals 5
.limit stack 6
.end method

.method private <init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;Landroid/support/v7/widget/Toolbar;II)V
.annotation invisibleparam 4 Landroid/support/a/ah;
.end annotation
.annotation invisibleparam 5 Landroid/support/a/ah;
.end annotation
aload 0
aload 1
aload 3
aload 2
iload 4
iload 5
invokespecial android/support/v7/app/j/<init>(Landroid/app/Activity;Landroid/support/v7/widget/Toolbar;Landroid/support/v4/widget/DrawerLayout;II)V
return
.limit locals 6
.limit stack 6
.end method

.method private <init>(Landroid/app/Activity;Landroid/support/v7/widget/Toolbar;Landroid/support/v4/widget/DrawerLayout;II)V
.annotation invisibleparam 4 Landroid/support/a/ah;
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield android/support/v7/app/j/a Z
aload 0
iconst_0
putfield android/support/v7/app/j/j Z
aload 2
ifnull L0
aload 0
new android/support/v7/app/s
dup
aload 2
invokespecial android/support/v7/app/s/<init>(Landroid/support/v7/widget/Toolbar;)V
putfield android/support/v7/app/j/c Landroid/support/v7/app/l;
aload 2
new android/support/v7/app/k
dup
aload 0
invokespecial android/support/v7/app/k/<init>(Landroid/support/v7/app/j;)V
invokevirtual android/support/v7/widget/Toolbar/setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V
L1:
aload 0
aload 3
putfield android/support/v7/app/j/d Landroid/support/v4/widget/DrawerLayout;
aload 0
iload 4
putfield android/support/v7/app/j/h I
aload 0
iload 5
putfield android/support/v7/app/j/i I
aload 0
new android/support/v7/app/n
dup
aload 1
aload 0
getfield android/support/v7/app/j/c Landroid/support/v7/app/l;
invokeinterface android/support/v7/app/l/b()Landroid/content/Context; 0
invokespecial android/support/v7/app/n/<init>(Landroid/app/Activity;Landroid/content/Context;)V
putfield android/support/v7/app/j/e Landroid/support/v7/app/o;
aload 0
aload 0
invokespecial android/support/v7/app/j/i()Landroid/graphics/drawable/Drawable;
putfield android/support/v7/app/j/f Landroid/graphics/drawable/Drawable;
return
L0:
aload 1
instanceof android/support/v7/app/m
ifeq L2
aload 0
aload 1
checkcast android/support/v7/app/m
invokeinterface android/support/v7/app/m/d()Landroid/support/v7/app/l; 0
putfield android/support/v7/app/j/c Landroid/support/v7/app/l;
goto L1
L2:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L3
aload 0
new android/support/v7/app/r
dup
aload 1
iconst_0
invokespecial android/support/v7/app/r/<init>(Landroid/app/Activity;B)V
putfield android/support/v7/app/j/c Landroid/support/v7/app/l;
goto L1
L3:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L4
aload 0
new android/support/v7/app/q
dup
aload 1
iconst_0
invokespecial android/support/v7/app/q/<init>(Landroid/app/Activity;B)V
putfield android/support/v7/app/j/c Landroid/support/v7/app/l;
goto L1
L4:
aload 0
new android/support/v7/app/p
dup
aload 1
invokespecial android/support/v7/app/p/<init>(Landroid/app/Activity;)V
putfield android/support/v7/app/j/c Landroid/support/v7/app/l;
goto L1
.limit locals 6
.limit stack 5
.end method

.method private a(I)V
aconst_null
astore 2
iload 1
ifeq L0
aload 0
getfield android/support/v7/app/j/d Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 2
L0:
aload 2
ifnonnull L1
aload 0
aload 0
invokespecial android/support/v7/app/j/i()Landroid/graphics/drawable/Drawable;
putfield android/support/v7/app/j/f Landroid/graphics/drawable/Drawable;
aload 0
iconst_0
putfield android/support/v7/app/j/g Z
L2:
aload 0
getfield android/support/v7/app/j/a Z
ifne L3
aload 0
aload 0
getfield android/support/v7/app/j/f Landroid/graphics/drawable/Drawable;
iconst_0
invokespecial android/support/v7/app/j/a(Landroid/graphics/drawable/Drawable;I)V
L3:
return
L1:
aload 0
aload 2
putfield android/support/v7/app/j/f Landroid/graphics/drawable/Drawable;
aload 0
iconst_1
putfield android/support/v7/app/j/g Z
goto L2
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
aload 1
ifnonnull L0
aload 0
aload 0
invokespecial android/support/v7/app/j/i()Landroid/graphics/drawable/Drawable;
putfield android/support/v7/app/j/f Landroid/graphics/drawable/Drawable;
aload 0
iconst_0
putfield android/support/v7/app/j/g Z
L1:
aload 0
getfield android/support/v7/app/j/a Z
ifne L2
aload 0
aload 0
getfield android/support/v7/app/j/f Landroid/graphics/drawable/Drawable;
iconst_0
invokespecial android/support/v7/app/j/a(Landroid/graphics/drawable/Drawable;I)V
L2:
return
L0:
aload 0
aload 1
putfield android/support/v7/app/j/f Landroid/graphics/drawable/Drawable;
aload 0
iconst_1
putfield android/support/v7/app/j/g Z
goto L1
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)V
aload 0
getfield android/support/v7/app/j/j Z
ifne L0
aload 0
getfield android/support/v7/app/j/c Landroid/support/v7/app/l;
invokeinterface android/support/v7/app/l/c()Z 0
ifne L0
ldc "ActionBarDrawerToggle"
ldc "DrawerToggle may not show up because NavigationIcon is not visible. You may need to call actionbar.setDisplayHomeAsUpEnabled(true);"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
iconst_1
putfield android/support/v7/app/j/j Z
L0:
aload 0
getfield android/support/v7/app/j/c Landroid/support/v7/app/l;
aload 1
iload 2
invokeinterface android/support/v7/app/l/a(Landroid/graphics/drawable/Drawable;I)V 2
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
putfield android/support/v7/app/j/b Landroid/view/View$OnClickListener;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Z)V
iload 1
aload 0
getfield android/support/v7/app/j/a Z
if_icmpeq L0
iload 1
ifeq L1
aload 0
getfield android/support/v7/app/j/e Landroid/support/v7/app/o;
checkcast android/graphics/drawable/Drawable
astore 3
aload 0
getfield android/support/v7/app/j/d Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/c()Z
ifeq L2
aload 0
getfield android/support/v7/app/j/i I
istore 2
L3:
aload 0
aload 3
iload 2
invokespecial android/support/v7/app/j/a(Landroid/graphics/drawable/Drawable;I)V
L4:
aload 0
iload 1
putfield android/support/v7/app/j/a Z
L0:
return
L2:
aload 0
getfield android/support/v7/app/j/h I
istore 2
goto L3
L1:
aload 0
aload 0
getfield android/support/v7/app/j/f Landroid/graphics/drawable/Drawable;
iconst_0
invokespecial android/support/v7/app/j/a(Landroid/graphics/drawable/Drawable;I)V
goto L4
.limit locals 4
.limit stack 3
.end method

.method private static synthetic a(Landroid/support/v7/app/j;)Z
aload 0
getfield android/support/v7/app/j/a Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/view/MenuItem;)Z
aload 1
ifnull L0
aload 1
invokeinterface android/view/MenuItem/getItemId()I 0
ldc_w 16908332
if_icmpne L0
aload 0
getfield android/support/v7/app/j/a Z
ifeq L0
aload 0
invokevirtual android/support/v7/app/j/d()V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b(I)V
aload 0
getfield android/support/v7/app/j/c Landroid/support/v7/app/l;
iload 1
invokeinterface android/support/v7/app/l/a(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private static synthetic b(Landroid/support/v7/app/j;)V
aload 0
invokevirtual android/support/v7/app/j/d()V
return
.limit locals 1
.limit stack 1
.end method

.method private static synthetic c(Landroid/support/v7/app/j;)Landroid/view/View$OnClickListener;
aload 0
getfield android/support/v7/app/j/b Landroid/view/View$OnClickListener;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()V
aload 0
getfield android/support/v7/app/j/d Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/c()Z
ifeq L0
aload 0
getfield android/support/v7/app/j/e Landroid/support/v7/app/o;
fconst_1
invokeinterface android/support/v7/app/o/a(F)V 1
L1:
aload 0
getfield android/support/v7/app/j/a Z
ifeq L2
aload 0
getfield android/support/v7/app/j/e Landroid/support/v7/app/o;
checkcast android/graphics/drawable/Drawable
astore 2
aload 0
getfield android/support/v7/app/j/d Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/c()Z
ifeq L3
aload 0
getfield android/support/v7/app/j/i I
istore 1
L4:
aload 0
aload 2
iload 1
invokespecial android/support/v7/app/j/a(Landroid/graphics/drawable/Drawable;I)V
L2:
return
L0:
aload 0
getfield android/support/v7/app/j/e Landroid/support/v7/app/o;
fconst_0
invokeinterface android/support/v7/app/o/a(F)V 1
goto L1
L3:
aload 0
getfield android/support/v7/app/j/h I
istore 1
goto L4
.limit locals 3
.limit stack 3
.end method

.method private f()V
aload 0
getfield android/support/v7/app/j/g Z
ifne L0
aload 0
aload 0
invokespecial android/support/v7/app/j/i()Landroid/graphics/drawable/Drawable;
putfield android/support/v7/app/j/f Landroid/graphics/drawable/Drawable;
L0:
aload 0
getfield android/support/v7/app/j/d Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/c()Z
ifeq L1
aload 0
getfield android/support/v7/app/j/e Landroid/support/v7/app/o;
fconst_1
invokeinterface android/support/v7/app/o/a(F)V 1
L2:
aload 0
getfield android/support/v7/app/j/a Z
ifeq L3
aload 0
getfield android/support/v7/app/j/e Landroid/support/v7/app/o;
checkcast android/graphics/drawable/Drawable
astore 2
aload 0
getfield android/support/v7/app/j/d Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/c()Z
ifeq L4
aload 0
getfield android/support/v7/app/j/i I
istore 1
L5:
aload 0
aload 2
iload 1
invokespecial android/support/v7/app/j/a(Landroid/graphics/drawable/Drawable;I)V
L3:
return
L1:
aload 0
getfield android/support/v7/app/j/e Landroid/support/v7/app/o;
fconst_0
invokeinterface android/support/v7/app/o/a(F)V 1
goto L2
L4:
aload 0
getfield android/support/v7/app/j/h I
istore 1
goto L5
.limit locals 3
.limit stack 3
.end method

.method private g()Z
aload 0
getfield android/support/v7/app/j/a Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()Landroid/view/View$OnClickListener;
aload 0
getfield android/support/v7/app/j/b Landroid/view/View$OnClickListener;
areturn
.limit locals 1
.limit stack 1
.end method

.method private i()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/app/j/c Landroid/support/v7/app/l;
invokeinterface android/support/v7/app/l/a()Landroid/graphics/drawable/Drawable; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()V
aload 0
getfield android/support/v7/app/j/e Landroid/support/v7/app/o;
fconst_1
invokeinterface android/support/v7/app/o/a(F)V 1
aload 0
getfield android/support/v7/app/j/a Z
ifeq L0
aload 0
aload 0
getfield android/support/v7/app/j/i I
invokespecial android/support/v7/app/j/b(I)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final a(F)V
aload 0
getfield android/support/v7/app/j/e Landroid/support/v7/app/o;
fconst_1
fconst_0
fload 1
invokestatic java/lang/Math/max(FF)F
invokestatic java/lang/Math/min(FF)F
invokeinterface android/support/v7/app/o/a(F)V 1
return
.limit locals 2
.limit stack 4
.end method

.method public final b()V
aload 0
getfield android/support/v7/app/j/e Landroid/support/v7/app/o;
fconst_0
invokeinterface android/support/v7/app/o/a(F)V 1
aload 0
getfield android/support/v7/app/j/a Z
ifeq L0
aload 0
aload 0
getfield android/support/v7/app/j/h I
invokespecial android/support/v7/app/j/b(I)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final c()V
return
.limit locals 1
.limit stack 0
.end method

.method final d()V
aload 0
getfield android/support/v7/app/j/d Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/d()Z
ifeq L0
aload 0
getfield android/support/v7/app/j/d Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/b()V
return
L0:
aload 0
getfield android/support/v7/app/j/d Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/a()V
return
.limit locals 1
.limit stack 1
.end method
