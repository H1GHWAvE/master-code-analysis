.bytecode 50.0
.class final synchronized android/support/v7/app/bo
.super java/lang/Object

.field static 'a' Landroid/support/v7/app/bo;

.field public static final 'b' I = 0


.field public static final 'c' I = 1


.field private static final 'g' F = 0.017453292F


.field private static final 'h' F = 9.0E-4F


.field private static final 'i' F = -0.10471976F


.field private static final 'j' F = 0.0334196F


.field private static final 'k' F = 3.49066E-4F


.field private static final 'l' F = 5.236E-6F


.field private static final 'm' F = 0.4092797F


.field private static final 'n' J = 946728000000L


.field public 'd' J

.field public 'e' J

.field public 'f' I

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a()Landroid/support/v7/app/bo;
getstatic android/support/v7/app/bo/a Landroid/support/v7/app/bo;
ifnonnull L0
new android/support/v7/app/bo
dup
invokespecial android/support/v7/app/bo/<init>()V
putstatic android/support/v7/app/bo/a Landroid/support/v7/app/bo;
L0:
getstatic android/support/v7/app/bo/a Landroid/support/v7/app/bo;
areturn
.limit locals 0
.limit stack 2
.end method

.method public final a(JDD)V
lload 1
ldc2_w 946728000000L
lsub
l2f
ldc_w 8.64E7F
fdiv
fstore 15
ldc_w 6.24006F
ldc_w 0.01720197F
fload 15
fmul
fadd
fstore 16
fload 16
f2d
ldc2_w 0.03341960161924362D
fload 16
f2d
invokestatic java/lang/Math/sin(D)D
dmul
dadd
ldc2_w 3.4906598739326E-4D
fconst_2
fload 16
fmul
f2d
invokestatic java/lang/Math/sin(D)D
dmul
dadd
ldc2_w 5.236000106378924E-6D
ldc_w 3.0F
fload 16
fmul
f2d
invokestatic java/lang/Math/sin(D)D
dmul
dadd
ldc2_w 1.796593063D
dadd
ldc2_w 3.141592653589793D
dadd
dstore 7
dload 5
dneg
ldc2_w 360.0D
ddiv
dstore 5
fload 15
ldc_w 9.0E-4F
fsub
f2d
dload 5
dsub
invokestatic java/lang/Math/round(D)J
l2f
ldc_w 9.0E-4F
fadd
f2d
dstore 9
fload 16
f2d
invokestatic java/lang/Math/sin(D)D
ldc2_w 0.0053D
dmul
dload 5
dload 9
dadd
dadd
ldc2_w -0.0069D
ldc2_w 2.0D
dload 7
dmul
invokestatic java/lang/Math/sin(D)D
dmul
dadd
dstore 5
dload 7
invokestatic java/lang/Math/sin(D)D
ldc2_w 0.4092797040939331D
invokestatic java/lang/Math/sin(D)D
dmul
invokestatic java/lang/Math/asin(D)D
dstore 7
ldc2_w 0.01745329238474369D
dload 3
dmul
dstore 13
ldc2_w -0.10471975803375244D
invokestatic java/lang/Math/sin(D)D
dstore 3
dload 13
invokestatic java/lang/Math/sin(D)D
dstore 9
dload 7
invokestatic java/lang/Math/sin(D)D
dstore 11
dload 13
invokestatic java/lang/Math/cos(D)D
dstore 13
dload 3
dload 9
dload 11
dmul
dsub
dload 7
invokestatic java/lang/Math/cos(D)D
dload 13
dmul
ddiv
dstore 3
dload 3
dconst_1
dcmpl
iflt L0
aload 0
iconst_1
putfield android/support/v7/app/bo/f I
aload 0
ldc2_w -1L
putfield android/support/v7/app/bo/d J
aload 0
ldc2_w -1L
putfield android/support/v7/app/bo/e J
return
L0:
dload 3
ldc2_w -1.0D
dcmpg
ifgt L1
aload 0
iconst_0
putfield android/support/v7/app/bo/f I
aload 0
ldc2_w -1L
putfield android/support/v7/app/bo/d J
aload 0
ldc2_w -1L
putfield android/support/v7/app/bo/e J
return
L1:
dload 3
invokestatic java/lang/Math/acos(D)D
ldc2_w 6.283185307179586D
ddiv
d2f
fstore 15
aload 0
fload 15
f2d
dload 5
dadd
ldc2_w 8.64E7D
dmul
invokestatic java/lang/Math/round(D)J
ldc2_w 946728000000L
ladd
putfield android/support/v7/app/bo/d J
aload 0
dload 5
fload 15
f2d
dsub
ldc2_w 8.64E7D
dmul
invokestatic java/lang/Math/round(D)J
ldc2_w 946728000000L
ladd
putfield android/support/v7/app/bo/e J
aload 0
getfield android/support/v7/app/bo/e J
lload 1
lcmp
ifge L2
aload 0
getfield android/support/v7/app/bo/d J
lload 1
lcmp
ifle L2
aload 0
iconst_0
putfield android/support/v7/app/bo/f I
return
L2:
aload 0
iconst_1
putfield android/support/v7/app/bo/f I
return
.limit locals 17
.limit stack 8
.end method
