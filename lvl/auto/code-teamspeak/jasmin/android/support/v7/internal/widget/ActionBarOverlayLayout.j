.bytecode 50.0
.class public synchronized android/support/v7/internal/widget/ActionBarOverlayLayout
.super android/view/ViewGroup
.implements android/support/v4/view/bv
.implements android/support/v7/internal/widget/ac

.field static final 'c' [I

.field private static final 'd' Ljava/lang/String; = "ActionBarOverlayLayout"

.field private final 'A' Ljava/lang/Runnable;

.field private final 'B' Ljava/lang/Runnable;

.field private final 'C' Landroid/support/v4/view/bw;

.field public 'a' Z

.field public 'b' Z

.field private 'e' I

.field private 'f' I

.field private 'g' Landroid/support/v7/internal/widget/ContentFrameLayout;

.field private 'h' Landroid/support/v7/internal/widget/ActionBarContainer;

.field private 'i' Landroid/support/v7/internal/widget/ad;

.field private 'j' Landroid/graphics/drawable/Drawable;

.field private 'k' Z

.field private 'l' Z

.field private 'm' Z

.field private 'n' I

.field private 'o' I

.field private final 'p' Landroid/graphics/Rect;

.field private final 'q' Landroid/graphics/Rect;

.field private final 'r' Landroid/graphics/Rect;

.field private final 's' Landroid/graphics/Rect;

.field private final 't' Landroid/graphics/Rect;

.field private final 'u' Landroid/graphics/Rect;

.field private 'v' Landroid/support/v7/internal/widget/j;

.field private final 'w' I

.field private 'x' Landroid/support/v4/widget/ca;

.field private 'y' Landroid/support/v4/view/fk;

.field private final 'z' Landroid/support/v4/view/gd;

.method static <clinit>()V
iconst_2
newarray int
dup
iconst_0
getstatic android/support/v7/a/d/actionBarSize I
iastore
dup
iconst_1
ldc_w 16842841
iastore
putstatic android/support/v7/internal/widget/ActionBarOverlayLayout/c [I
return
.limit locals 0
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_0
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/f I
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/p Landroid/graphics/Rect;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/q Landroid/graphics/Rect;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/r Landroid/graphics/Rect;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/s Landroid/graphics/Rect;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/t Landroid/graphics/Rect;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/u Landroid/graphics/Rect;
aload 0
sipush 600
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/w I
aload 0
new android/support/v7/internal/widget/g
dup
aload 0
invokespecial android/support/v7/internal/widget/g/<init>(Landroid/support/v7/internal/widget/ActionBarOverlayLayout;)V
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/z Landroid/support/v4/view/gd;
aload 0
new android/support/v7/internal/widget/h
dup
aload 0
invokespecial android/support/v7/internal/widget/h/<init>(Landroid/support/v7/internal/widget/ActionBarOverlayLayout;)V
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/A Ljava/lang/Runnable;
aload 0
new android/support/v7/internal/widget/i
dup
aload 0
invokespecial android/support/v7/internal/widget/i/<init>(Landroid/support/v7/internal/widget/ActionBarOverlayLayout;)V
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/B Ljava/lang/Runnable;
aload 0
aload 1
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/a(Landroid/content/Context;)V
aload 0
new android/support/v4/view/bw
dup
aload 0
invokespecial android/support/v4/view/bw/<init>(Landroid/view/ViewGroup;)V
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/C Landroid/support/v4/view/bw;
return
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/ActionBarOverlayLayout;Landroid/support/v4/view/fk;)Landroid/support/v4/view/fk;
aload 0
aload 1
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/y Landroid/support/v4/view/fk;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/view/View;)Landroid/support/v7/internal/widget/ad;
aload 0
instanceof android/support/v7/internal/widget/ad
ifeq L0
aload 0
checkcast android/support/v7/internal/widget/ad
areturn
L0:
aload 0
instanceof android/support/v7/widget/Toolbar
ifeq L1
aload 0
checkcast android/support/v7/widget/Toolbar
invokevirtual android/support/v7/widget/Toolbar/getWrapper()Landroid/support/v7/internal/widget/ad;
areturn
L1:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Can't make a decor toolbar out of "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 5
.end method

.method private a(Landroid/util/AttributeSet;)Landroid/support/v7/internal/widget/k;
new android/support/v7/internal/widget/k
dup
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v7/internal/widget/k/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Landroid/content/Context;)V
iconst_1
istore 3
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
getstatic android/support/v7/internal/widget/ActionBarOverlayLayout/c [I
invokevirtual android/content/res/Resources$Theme/obtainStyledAttributes([I)Landroid/content/res/TypedArray;
astore 4
aload 0
aload 4
iconst_0
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/e I
aload 0
aload 4
iconst_1
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/j Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/j Landroid/graphics/drawable/Drawable;
ifnonnull L0
iconst_1
istore 2
L1:
aload 0
iload 2
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setWillNotDraw(Z)V
aload 4
invokevirtual android/content/res/TypedArray/recycle()V
aload 1
invokevirtual android/content/Context/getApplicationInfo()Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/targetSdkVersion I
bipush 19
if_icmpge L2
iload 3
istore 2
L3:
aload 0
iload 2
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/k Z
aload 0
aload 1
aconst_null
invokestatic android/support/v4/widget/ca/a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/x Landroid/support/v4/widget/ca;
return
L0:
iconst_0
istore 2
goto L1
L2:
iconst_0
istore 2
goto L3
.limit locals 5
.limit stack 4
.end method

.method private a(F)Z
iconst_0
istore 2
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/x Landroid/support/v4/widget/ca;
iconst_0
iconst_0
iconst_0
fload 1
f2i
iconst_0
iconst_0
ldc_w -2147483648
ldc_w 2147483647
invokevirtual android/support/v4/widget/ca/a(IIIIIIII)V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/x Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/d()I
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getHeight()I
if_icmple L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 3
.limit stack 9
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/ActionBarOverlayLayout;)Z
aload 0
iconst_0
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/m Z
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Landroid/view/View;Landroid/graphics/Rect;Z)Z
iconst_0
istore 3
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/internal/widget/k
astore 0
aload 0
getfield android/support/v7/internal/widget/k/leftMargin I
aload 1
getfield android/graphics/Rect/left I
if_icmpeq L0
aload 0
aload 1
getfield android/graphics/Rect/left I
putfield android/support/v7/internal/widget/k/leftMargin I
iconst_1
istore 3
L0:
aload 0
getfield android/support/v7/internal/widget/k/topMargin I
aload 1
getfield android/graphics/Rect/top I
if_icmpeq L1
aload 0
aload 1
getfield android/graphics/Rect/top I
putfield android/support/v7/internal/widget/k/topMargin I
iconst_1
istore 3
L1:
aload 0
getfield android/support/v7/internal/widget/k/rightMargin I
aload 1
getfield android/graphics/Rect/right I
if_icmpeq L2
aload 0
aload 1
getfield android/graphics/Rect/right I
putfield android/support/v7/internal/widget/k/rightMargin I
iconst_1
istore 3
L2:
iload 2
ifeq L3
aload 0
getfield android/support/v7/internal/widget/k/bottomMargin I
aload 1
getfield android/graphics/Rect/bottom I
if_icmpeq L3
aload 0
aload 1
getfield android/graphics/Rect/bottom I
putfield android/support/v7/internal/widget/k/bottomMargin I
iconst_1
ireturn
L3:
iload 3
ireturn
.limit locals 4
.limit stack 2
.end method

.method static synthetic b(Landroid/support/v7/internal/widget/ActionBarOverlayLayout;)V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Landroid/support/v7/internal/widget/ActionBarOverlayLayout;)Landroid/support/v4/view/gd;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/z Landroid/support/v4/view/gd;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Landroid/support/v7/internal/widget/ActionBarOverlayLayout;)Landroid/support/v7/internal/widget/ActionBarContainer;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
areturn
.limit locals 1
.limit stack 1
.end method

.method private j()Z
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/a Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static k()Landroid/support/v7/internal/widget/k;
new android/support/v7/internal/widget/k
dup
invokespecial android/support/v7/internal/widget/k/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/g Landroid/support/v7/internal/widget/ContentFrameLayout;
ifnonnull L0
aload 0
aload 0
getstatic android/support/v7/a/i/action_bar_activity_content I
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/findViewById(I)Landroid/view/View;
checkcast android/support/v7/internal/widget/ContentFrameLayout
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/g Landroid/support/v7/internal/widget/ContentFrameLayout;
aload 0
aload 0
getstatic android/support/v7/a/i/action_bar_container I
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/findViewById(I)Landroid/view/View;
checkcast android/support/v7/internal/widget/ActionBarContainer
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
aload 0
getstatic android/support/v7/a/i/action_bar I
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/findViewById(I)Landroid/view/View;
astore 1
aload 1
instanceof android/support/v7/internal/widget/ad
ifeq L1
aload 1
checkcast android/support/v7/internal/widget/ad
astore 1
L2:
aload 0
aload 1
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
L0:
return
L1:
aload 1
instanceof android/support/v7/widget/Toolbar
ifeq L3
aload 1
checkcast android/support/v7/widget/Toolbar
invokevirtual android/support/v7/widget/Toolbar/getWrapper()Landroid/support/v7/internal/widget/ad;
astore 1
goto L2
L3:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Can't make a decor toolbar out of "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 5
.end method

.method private m()Z
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/b Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private n()V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/A Ljava/lang/Runnable;
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/B Ljava/lang/Runnable;
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/y Landroid/support/v4/view/fk;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/y Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/a()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private o()V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/A Ljava/lang/Runnable;
ldc2_w 600L
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method private p()V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/B Ljava/lang/Runnable;
ldc2_w 600L
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method private q()V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/A Ljava/lang/Runnable;
invokeinterface java/lang/Runnable/run()V 0
return
.limit locals 1
.limit stack 1
.end method

.method private r()V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/B Ljava/lang/Runnable;
invokeinterface java/lang/Runnable/run()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public final a(I)V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
iload 1
lookupswitch
2 : L0
5 : L1
109 : L2
default : L3
L3:
return
L0:
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/g()V 0
return
L1:
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/h()V 0
return
L2:
aload 0
iconst_1
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setOverlayMode(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/util/SparseArray;)V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/a(Landroid/util/SparseArray;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/Menu;Landroid/support/v7/internal/view/menu/y;)V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
aload 1
aload 2
invokeinterface android/support/v7/internal/widget/ad/a(Landroid/view/Menu;Landroid/support/v7/internal/view/menu/y;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public final a()Z
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/i()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(Landroid/util/SparseArray;)V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/b(Landroid/util/SparseArray;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final b()Z
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/j()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Z
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/k()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 1
instanceof android/support/v7/internal/widget/k
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final d()Z
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/l()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public draw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/view/ViewGroup/draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/j Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/k Z
ifne L0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getVisibility()I
ifne L1
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getBottom()I
i2f
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokestatic android/support/v4/view/cx/n(Landroid/view/View;)F
fadd
ldc_w 0.5F
fadd
f2i
istore 2
L2:
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/j Landroid/graphics/drawable/Drawable;
iconst_0
iload 2
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getWidth()I
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/j Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
iload 2
iadd
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/j Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
L0:
return
L1:
iconst_0
istore 2
goto L2
.limit locals 3
.limit stack 6
.end method

.method public final e()Z
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/m()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final f()Z
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/n()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
invokestatic android/support/v4/view/cx/s(Landroid/view/View;)I
pop
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
aload 1
iconst_0
invokestatic android/support/v7/internal/widget/ActionBarOverlayLayout/a(Landroid/view/View;Landroid/graphics/Rect;Z)Z
istore 2
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/s Landroid/graphics/Rect;
aload 1
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/s Landroid/graphics/Rect;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/p Landroid/graphics/Rect;
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/q Landroid/graphics/Rect;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/p Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/equals(Ljava/lang/Object;)Z
ifne L0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/q Landroid/graphics/Rect;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/p Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
iconst_1
istore 2
L0:
iload 2
ifeq L1
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/requestLayout()V
L1:
iconst_1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final g()Z
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/o()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
new android/support/v7/internal/widget/k
dup
invokespecial android/support/v7/internal/widget/k/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
new android/support/v7/internal/widget/k
dup
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v7/internal/widget/k/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
new android/support/v7/internal/widget/k
dup
aload 1
invokespecial android/support/v7/internal/widget/k/<init>(Landroid/view/ViewGroup$LayoutParams;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public getActionBarHideOffset()I
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokestatic android/support/v4/view/cx/n(Landroid/view/View;)F
f2i
ineg
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getNestedScrollAxes()I
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/C Landroid/support/v4/view/bw;
getfield android/support/v4/view/bw/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getTitle()Ljava/lang/CharSequence;
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/e()Ljava/lang/CharSequence; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final h()V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/p()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public final i()V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/q()V 0
return
.limit locals 1
.limit stack 1
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L0
aload 0
aload 1
invokespecial android/view/ViewGroup/onConfigurationChanged(Landroid/content/res/Configuration;)V
L0:
aload 0
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getContext()Landroid/content/Context;
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/a(Landroid/content/Context;)V
aload 0
invokestatic android/support/v4/view/cx/t(Landroid/view/View;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected onDetachedFromWindow()V
aload 0
invokespecial android/view/ViewGroup/onDetachedFromWindow()V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
return
.limit locals 1
.limit stack 1
.end method

.method protected onLayout(ZIIII)V
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getChildCount()I
istore 3
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getPaddingLeft()I
istore 4
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getPaddingRight()I
pop
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getPaddingTop()I
istore 5
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getPaddingBottom()I
pop
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getChildAt(I)Landroid/view/View;
astore 10
aload 10
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L2
aload 10
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/internal/widget/k
astore 11
aload 10
invokevirtual android/view/View/getMeasuredWidth()I
istore 6
aload 10
invokevirtual android/view/View/getMeasuredHeight()I
istore 7
aload 11
getfield android/support/v7/internal/widget/k/leftMargin I
iload 4
iadd
istore 8
aload 11
getfield android/support/v7/internal/widget/k/topMargin I
iload 5
iadd
istore 9
aload 10
iload 8
iload 9
iload 6
iload 8
iadd
iload 7
iload 9
iadd
invokevirtual android/view/View/layout(IIII)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 12
.limit stack 6
.end method

.method protected onMeasure(II)V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
iload 1
iconst_0
iload 2
iconst_0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/measureChildWithMargins(Landroid/view/View;IIII)V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/internal/widget/k
astore 9
iconst_0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getMeasuredWidth()I
aload 9
getfield android/support/v7/internal/widget/k/leftMargin I
iadd
aload 9
getfield android/support/v7/internal/widget/k/rightMargin I
iadd
invokestatic java/lang/Math/max(II)I
istore 8
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getMeasuredHeight()I
istore 3
aload 9
getfield android/support/v7/internal/widget/k/topMargin I
istore 4
iconst_0
aload 9
getfield android/support/v7/internal/widget/k/bottomMargin I
iload 3
iload 4
iadd
iadd
invokestatic java/lang/Math/max(II)I
istore 7
iconst_0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 6
aload 0
invokestatic android/support/v4/view/cx/s(Landroid/view/View;)I
sipush 256
iand
ifeq L0
iconst_1
istore 4
L1:
iload 4
ifeq L2
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/e I
istore 5
iload 5
istore 3
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/l Z
ifeq L3
iload 5
istore 3
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getTabContainer()Landroid/view/View;
ifnull L3
iload 5
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/e I
iadd
istore 3
L3:
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/r Landroid/graphics/Rect;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/p Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/t Landroid/graphics/Rect;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/s Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/a Z
ifne L4
iload 4
ifne L4
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/r Landroid/graphics/Rect;
astore 9
aload 9
iload 3
aload 9
getfield android/graphics/Rect/top I
iadd
putfield android/graphics/Rect/top I
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/r Landroid/graphics/Rect;
astore 9
aload 9
aload 9
getfield android/graphics/Rect/bottom I
iconst_0
iadd
putfield android/graphics/Rect/bottom I
L5:
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/g Landroid/support/v7/internal/widget/ContentFrameLayout;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/r Landroid/graphics/Rect;
iconst_1
invokestatic android/support/v7/internal/widget/ActionBarOverlayLayout/a(Landroid/view/View;Landroid/graphics/Rect;Z)Z
pop
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/u Landroid/graphics/Rect;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/t Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/equals(Ljava/lang/Object;)Z
ifne L6
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/u Landroid/graphics/Rect;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/t Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/g Landroid/support/v7/internal/widget/ContentFrameLayout;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/t Landroid/graphics/Rect;
invokevirtual android/support/v7/internal/widget/ContentFrameLayout/a(Landroid/graphics/Rect;)V
L6:
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/g Landroid/support/v7/internal/widget/ContentFrameLayout;
iload 1
iconst_0
iload 2
iconst_0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/measureChildWithMargins(Landroid/view/View;IIII)V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/g Landroid/support/v7/internal/widget/ContentFrameLayout;
invokevirtual android/support/v7/internal/widget/ContentFrameLayout/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/internal/widget/k
astore 9
iload 8
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/g Landroid/support/v7/internal/widget/ContentFrameLayout;
invokevirtual android/support/v7/internal/widget/ContentFrameLayout/getMeasuredWidth()I
aload 9
getfield android/support/v7/internal/widget/k/leftMargin I
iadd
aload 9
getfield android/support/v7/internal/widget/k/rightMargin I
iadd
invokestatic java/lang/Math/max(II)I
istore 3
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/g Landroid/support/v7/internal/widget/ContentFrameLayout;
invokevirtual android/support/v7/internal/widget/ContentFrameLayout/getMeasuredHeight()I
istore 4
aload 9
getfield android/support/v7/internal/widget/k/topMargin I
istore 5
iload 7
aload 9
getfield android/support/v7/internal/widget/k/bottomMargin I
iload 4
iload 5
iadd
iadd
invokestatic java/lang/Math/max(II)I
istore 4
iload 6
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/g Landroid/support/v7/internal/widget/ContentFrameLayout;
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 5
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getPaddingLeft()I
istore 6
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getPaddingRight()I
istore 7
iload 4
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getPaddingTop()I
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getPaddingBottom()I
iadd
iadd
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getSuggestedMinimumHeight()I
invokestatic java/lang/Math/max(II)I
istore 4
aload 0
iload 3
iload 6
iload 7
iadd
iadd
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getSuggestedMinimumWidth()I
invokestatic java/lang/Math/max(II)I
iload 1
iload 5
invokestatic android/support/v4/view/cx/a(III)I
iload 4
iload 2
iload 5
bipush 16
ishl
invokestatic android/support/v4/view/cx/a(III)I
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setMeasuredDimension(II)V
return
L0:
iconst_0
istore 4
goto L1
L2:
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getVisibility()I
bipush 8
if_icmpeq L7
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getMeasuredHeight()I
istore 3
goto L3
L4:
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/t Landroid/graphics/Rect;
astore 9
aload 9
iload 3
aload 9
getfield android/graphics/Rect/top I
iadd
putfield android/graphics/Rect/top I
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/t Landroid/graphics/Rect;
astore 9
aload 9
aload 9
getfield android/graphics/Rect/bottom I
iconst_0
iadd
putfield android/graphics/Rect/bottom I
goto L5
L7:
iconst_0
istore 3
goto L3
.limit locals 10
.limit stack 6
.end method

.method public onNestedFling(Landroid/view/View;FFZ)Z
iconst_0
istore 5
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/b Z
ifeq L0
iload 4
ifne L1
L0:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/x Landroid/support/v4/widget/ca;
iconst_0
iconst_0
iconst_0
fload 3
f2i
iconst_0
iconst_0
ldc_w -2147483648
ldc_w 2147483647
invokevirtual android/support/v4/widget/ca/a(IIIIIIII)V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/x Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/d()I
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getHeight()I
if_icmple L2
iconst_1
istore 5
L2:
iload 5
ifeq L3
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/B Ljava/lang/Runnable;
invokeinterface java/lang/Runnable/run()V 0
L4:
aload 0
iconst_1
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/m Z
iconst_1
ireturn
L3:
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/A Ljava/lang/Runnable;
invokeinterface java/lang/Runnable/run()V 0
goto L4
.limit locals 6
.limit stack 9
.end method

.method public onNestedPreFling(Landroid/view/View;FF)Z
iconst_0
ireturn
.limit locals 4
.limit stack 1
.end method

.method public onNestedPreScroll(Landroid/view/View;II[I)V
return
.limit locals 5
.limit stack 0
.end method

.method public onNestedScroll(Landroid/view/View;IIII)V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/n I
iload 3
iadd
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/n I
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/n I
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setActionBarHideOffset(I)V
return
.limit locals 6
.limit stack 3
.end method

.method public onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/C Landroid/support/v4/view/bw;
iload 3
putfield android/support/v4/view/bw/a I
aload 0
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getActionBarHideOffset()I
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/n I
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/v Landroid/support/v7/internal/widget/j;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/v Landroid/support/v7/internal/widget/j;
invokeinterface android/support/v7/internal/widget/j/C()V 0
L0:
return
.limit locals 4
.limit stack 2
.end method

.method public onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
iload 3
iconst_2
iand
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getVisibility()I
ifeq L1
L0:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/b Z
ireturn
.limit locals 4
.limit stack 2
.end method

.method public onStopNestedScroll(Landroid/view/View;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/b Z
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/m Z
ifne L0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/n I
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getHeight()I
if_icmpgt L1
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/A Ljava/lang/Runnable;
ldc2_w 600L
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/postDelayed(Ljava/lang/Runnable;J)Z
pop
L0:
return
L1:
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/B Ljava/lang/Runnable;
ldc2_w 600L
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 2
.limit stack 4
.end method

.method public onWindowSystemUiVisibilityChanged(I)V
iconst_1
istore 5
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
aload 0
iload 1
invokespecial android/view/ViewGroup/onWindowSystemUiVisibilityChanged(I)V
L0:
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/o I
istore 4
aload 0
iload 1
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/o I
iload 1
iconst_4
iand
ifne L1
iconst_1
istore 2
L2:
iload 1
sipush 256
iand
ifeq L3
iconst_1
istore 3
L4:
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/v Landroid/support/v7/internal/widget/j;
ifnull L5
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/v Landroid/support/v7/internal/widget/j;
astore 6
iload 3
ifne L6
L7:
aload 6
iload 5
invokeinterface android/support/v7/internal/widget/j/i(Z)V 1
iload 2
ifne L8
iload 3
ifne L9
L8:
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/v Landroid/support/v7/internal/widget/j;
invokeinterface android/support/v7/internal/widget/j/A()V 0
L5:
iload 4
iload 1
ixor
sipush 256
iand
ifeq L10
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/v Landroid/support/v7/internal/widget/j;
ifnull L10
aload 0
invokestatic android/support/v4/view/cx/t(Landroid/view/View;)V
L10:
return
L1:
iconst_0
istore 2
goto L2
L3:
iconst_0
istore 3
goto L4
L6:
iconst_0
istore 5
goto L7
L9:
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/v Landroid/support/v7/internal/widget/j;
invokeinterface android/support/v7/internal/widget/j/B()V 0
goto L5
.limit locals 7
.limit stack 2
.end method

.method protected onWindowVisibilityChanged(I)V
aload 0
iload 1
invokespecial android/view/ViewGroup/onWindowVisibilityChanged(I)V
aload 0
iload 1
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/f I
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/v Landroid/support/v7/internal/widget/j;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/v Landroid/support/v7/internal/widget/j;
iload 1
invokeinterface android/support/v7/internal/widget/j/n(I)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public setActionBarHideOffset(I)V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
iconst_0
iload 1
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getHeight()I
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
istore 1
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/h Landroid/support/v7/internal/widget/ActionBarContainer;
iload 1
ineg
i2f
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
return
.limit locals 2
.limit stack 3
.end method

.method public setActionBarVisibilityCallback(Landroid/support/v7/internal/widget/j;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/v Landroid/support/v7/internal/widget/j;
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getWindowToken()Landroid/os/IBinder;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/v Landroid/support/v7/internal/widget/j;
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/f I
invokeinterface android/support/v7/internal/widget/j/n(I)V 1
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/o I
ifeq L0
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/o I
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/onWindowSystemUiVisibilityChanged(I)V
aload 0
invokestatic android/support/v4/view/cx/t(Landroid/view/View;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public setHasNonEmbeddedTabs(Z)V
aload 0
iload 1
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/l Z
return
.limit locals 2
.limit stack 2
.end method

.method public setHideOnContentScrollEnabled(Z)V
iload 1
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/b Z
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/b Z
iload 1
ifne L0
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/n()V
aload 0
iconst_0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setActionBarHideOffset(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public setIcon(I)V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
iload 1
invokeinterface android/support/v7/internal/widget/ad/a(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/a(Landroid/graphics/drawable/Drawable;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public setLogo(I)V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
iload 1
invokeinterface android/support/v7/internal/widget/ad/b(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public setOverlayMode(Z)V
aload 0
iload 1
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/a Z
iload 1
ifeq L0
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getApplicationInfo()Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/targetSdkVersion I
bipush 19
if_icmpge L0
iconst_1
istore 1
L1:
aload 0
iload 1
putfield android/support/v7/internal/widget/ActionBarOverlayLayout/k Z
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method public setShowingForActionMode(Z)V
return
.limit locals 2
.limit stack 0
.end method

.method public setUiOptions(I)V
return
.limit locals 2
.limit stack 0
.end method

.method public setWindowCallback(Landroid/view/Window$Callback;)V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/a(Landroid/view/Window$Callback;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public setWindowTitle(Ljava/lang/CharSequence;)V
aload 0
invokespecial android/support/v7/internal/widget/ActionBarOverlayLayout/l()V
aload 0
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/i Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/a(Ljava/lang/CharSequence;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public shouldDelayChildPressedState()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method
