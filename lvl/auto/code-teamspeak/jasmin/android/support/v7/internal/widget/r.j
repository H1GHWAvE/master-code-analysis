.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/r
.super java/lang/Object

.field public final 'a' Landroid/content/ComponentName;

.field public final 'b' J

.field public final 'c' F

.method public <init>(Landroid/content/ComponentName;JF)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v7/internal/widget/r/a Landroid/content/ComponentName;
aload 0
lload 2
putfield android/support/v7/internal/widget/r/b J
aload 0
fload 4
putfield android/support/v7/internal/widget/r/c F
return
.limit locals 5
.limit stack 3
.end method

.method public <init>(Ljava/lang/String;JF)V
aload 0
aload 1
invokestatic android/content/ComponentName/unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;
lload 2
fload 4
invokespecial android/support/v7/internal/widget/r/<init>(Landroid/content/ComponentName;JF)V
return
.limit locals 5
.limit stack 5
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 0
aload 1
if_acmpne L0
L1:
iconst_1
ireturn
L0:
aload 1
ifnonnull L2
iconst_0
ireturn
L2:
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
if_acmpeq L3
iconst_0
ireturn
L3:
aload 1
checkcast android/support/v7/internal/widget/r
astore 1
aload 0
getfield android/support/v7/internal/widget/r/a Landroid/content/ComponentName;
ifnonnull L4
aload 1
getfield android/support/v7/internal/widget/r/a Landroid/content/ComponentName;
ifnull L5
iconst_0
ireturn
L4:
aload 0
getfield android/support/v7/internal/widget/r/a Landroid/content/ComponentName;
aload 1
getfield android/support/v7/internal/widget/r/a Landroid/content/ComponentName;
invokevirtual android/content/ComponentName/equals(Ljava/lang/Object;)Z
ifne L5
iconst_0
ireturn
L5:
aload 0
getfield android/support/v7/internal/widget/r/b J
aload 1
getfield android/support/v7/internal/widget/r/b J
lcmp
ifeq L6
iconst_0
ireturn
L6:
aload 0
getfield android/support/v7/internal/widget/r/c F
invokestatic java/lang/Float/floatToIntBits(F)I
aload 1
getfield android/support/v7/internal/widget/r/c F
invokestatic java/lang/Float/floatToIntBits(F)I
if_icmpeq L1
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method public final hashCode()I
aload 0
getfield android/support/v7/internal/widget/r/a Landroid/content/ComponentName;
ifnonnull L0
iconst_0
istore 1
L1:
iload 1
bipush 31
iadd
bipush 31
imul
aload 0
getfield android/support/v7/internal/widget/r/b J
aload 0
getfield android/support/v7/internal/widget/r/b J
bipush 32
lushr
lxor
l2i
iadd
bipush 31
imul
aload 0
getfield android/support/v7/internal/widget/r/c F
invokestatic java/lang/Float/floatToIntBits(F)I
iadd
ireturn
L0:
aload 0
getfield android/support/v7/internal/widget/r/a Landroid/content/ComponentName;
invokevirtual android/content/ComponentName/hashCode()I
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
aload 1
ldc "["
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
ldc "; activity:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v7/internal/widget/r/a Landroid/content/ComponentName;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
aload 1
ldc "; time:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v7/internal/widget/r/b J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
pop
aload 1
ldc "; weight:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/math/BigDecimal
dup
aload 0
getfield android/support/v7/internal/widget/r/c F
f2d
invokespecial java/math/BigDecimal/<init>(D)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
aload 1
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 5
.end method
