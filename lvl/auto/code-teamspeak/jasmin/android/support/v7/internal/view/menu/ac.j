.bytecode 50.0
.class synchronized android/support/v7/internal/view/menu/ac
.super android/support/v7/internal/view/menu/e
.implements android/view/Menu

.method <init>(Landroid/content/Context;Landroid/support/v4/g/a/a;)V
aload 0
aload 1
aload 2
invokespecial android/support/v7/internal/view/menu/e/<init>(Landroid/content/Context;Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 3
.end method

.method public add(I)Landroid/view/MenuItem;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
invokeinterface android/support/v4/g/a/a/add(I)Landroid/view/MenuItem; 1
invokevirtual android/support/v7/internal/view/menu/ac/a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
areturn
.limit locals 2
.limit stack 3
.end method

.method public add(IIII)Landroid/view/MenuItem;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
iload 2
iload 3
iload 4
invokeinterface android/support/v4/g/a/a/add(IIII)Landroid/view/MenuItem; 4
invokevirtual android/support/v7/internal/view/menu/ac/a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
areturn
.limit locals 5
.limit stack 6
.end method

.method public add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
iload 2
iload 3
aload 4
invokeinterface android/support/v4/g/a/a/add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem; 4
invokevirtual android/support/v7/internal/view/menu/ac/a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
areturn
.limit locals 5
.limit stack 6
.end method

.method public add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
aload 1
invokeinterface android/support/v4/g/a/a/add(Ljava/lang/CharSequence;)Landroid/view/MenuItem; 1
invokevirtual android/support/v7/internal/view/menu/ac/a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
areturn
.limit locals 2
.limit stack 3
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
aconst_null
astore 9
aload 8
ifnull L0
aload 8
arraylength
anewarray android/view/MenuItem
astore 9
L0:
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
iload 2
iload 3
aload 4
aload 5
aload 6
iload 7
aload 9
invokeinterface android/support/v4/g/a/a/addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I 8
istore 2
aload 9
ifnull L1
iconst_0
istore 1
aload 9
arraylength
istore 3
L2:
iload 1
iload 3
if_icmpge L1
aload 8
iload 1
aload 0
aload 9
iload 1
aaload
invokevirtual android/support/v7/internal/view/menu/ac/a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
aastore
iload 1
iconst_1
iadd
istore 1
goto L2
L1:
iload 2
ireturn
.limit locals 10
.limit stack 9
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
invokeinterface android/support/v4/g/a/a/addSubMenu(I)Landroid/view/SubMenu; 1
invokevirtual android/support/v7/internal/view/menu/ac/a(Landroid/view/SubMenu;)Landroid/view/SubMenu;
areturn
.limit locals 2
.limit stack 3
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
iload 2
iload 3
iload 4
invokeinterface android/support/v4/g/a/a/addSubMenu(IIII)Landroid/view/SubMenu; 4
invokevirtual android/support/v7/internal/view/menu/ac/a(Landroid/view/SubMenu;)Landroid/view/SubMenu;
areturn
.limit locals 5
.limit stack 6
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
iload 2
iload 3
aload 4
invokeinterface android/support/v4/g/a/a/addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu; 4
invokevirtual android/support/v7/internal/view/menu/ac/a(Landroid/view/SubMenu;)Landroid/view/SubMenu;
areturn
.limit locals 5
.limit stack 6
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
aload 1
invokeinterface android/support/v4/g/a/a/addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu; 1
invokevirtual android/support/v7/internal/view/menu/ac/a(Landroid/view/SubMenu;)Landroid/view/SubMenu;
areturn
.limit locals 2
.limit stack 3
.end method

.method public clear()V
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
L0:
aload 0
getfield android/support/v7/internal/view/menu/e/c Ljava/util/Map;
ifnull L1
aload 0
getfield android/support/v7/internal/view/menu/e/c Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
L1:
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
invokeinterface android/support/v4/g/a/a/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public close()V
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
invokeinterface android/support/v4/g/a/a/close()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public findItem(I)Landroid/view/MenuItem;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
invokeinterface android/support/v4/g/a/a/findItem(I)Landroid/view/MenuItem; 1
invokevirtual android/support/v7/internal/view/menu/ac/a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
areturn
.limit locals 2
.limit stack 3
.end method

.method public getItem(I)Landroid/view/MenuItem;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
invokeinterface android/support/v4/g/a/a/getItem(I)Landroid/view/MenuItem; 1
invokevirtual android/support/v7/internal/view/menu/ac/a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
areturn
.limit locals 2
.limit stack 3
.end method

.method public hasVisibleItems()Z
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
invokeinterface android/support/v4/g/a/a/hasVisibleItems()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
aload 2
invokeinterface android/support/v4/g/a/a/isShortcutKey(ILandroid/view/KeyEvent;)Z 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public performIdentifierAction(II)Z
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
iload 2
invokeinterface android/support/v4/g/a/a/performIdentifierAction(II)Z 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
aload 2
iload 3
invokeinterface android/support/v4/g/a/a/performShortcut(ILandroid/view/KeyEvent;I)Z 3
ireturn
.limit locals 4
.limit stack 4
.end method

.method public removeGroup(I)V
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L1:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
iload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/view/MenuItem
invokeinterface android/view/MenuItem/getGroupId()I 0
if_icmpne L1
aload 2
invokeinterface java/util/Iterator/remove()V 0
goto L1
L0:
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
invokeinterface android/support/v4/g/a/a/removeGroup(I)V 1
return
.limit locals 3
.limit stack 2
.end method

.method public removeItem(I)V
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L1:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
iload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/view/MenuItem
invokeinterface android/view/MenuItem/getItemId()I 0
if_icmpne L1
aload 2
invokeinterface java/util/Iterator/remove()V 0
L0:
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
invokeinterface android/support/v4/g/a/a/removeItem(I)V 1
return
.limit locals 3
.limit stack 2
.end method

.method public setGroupCheckable(IZZ)V
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
iload 2
iload 3
invokeinterface android/support/v4/g/a/a/setGroupCheckable(IZZ)V 3
return
.limit locals 4
.limit stack 4
.end method

.method public setGroupEnabled(IZ)V
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
iload 2
invokeinterface android/support/v4/g/a/a/setGroupEnabled(IZ)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public setGroupVisible(IZ)V
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
iload 2
invokeinterface android/support/v4/g/a/a/setGroupVisible(IZ)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public setQwertyMode(Z)V
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
iload 1
invokeinterface android/support/v4/g/a/a/setQwertyMode(Z)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public size()I
aload 0
getfield android/support/v7/internal/view/menu/ac/d Ljava/lang/Object;
checkcast android/support/v4/g/a/a
invokeinterface android/support/v4/g/a/a/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method
