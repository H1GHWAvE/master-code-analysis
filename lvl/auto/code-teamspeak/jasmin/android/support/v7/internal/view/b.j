.bytecode 50.0
.class public final synchronized android/support/v7/internal/view/b
.super android/content/ContextWrapper

.field public 'a' I

.field private 'b' Landroid/content/res/Resources$Theme;

.field private 'c' Landroid/view/LayoutInflater;

.method public <init>(Landroid/content/Context;I)V
.annotation invisibleparam 2 Landroid/support/a/ai;
.end annotation
aload 0
aload 1
invokespecial android/content/ContextWrapper/<init>(Landroid/content/Context;)V
aload 0
iload 2
putfield android/support/v7/internal/view/b/a I
return
.limit locals 3
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;Landroid/content/res/Resources$Theme;)V
aload 0
aload 1
invokespecial android/content/ContextWrapper/<init>(Landroid/content/Context;)V
aload 0
aload 2
putfield android/support/v7/internal/view/b/b Landroid/content/res/Resources$Theme;
return
.limit locals 3
.limit stack 2
.end method

.method private a()I
aload 0
getfield android/support/v7/internal/view/b/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/res/Resources$Theme;I)V
aload 0
iload 1
iconst_1
invokevirtual android/content/res/Resources$Theme/applyStyle(IZ)V
return
.limit locals 2
.limit stack 3
.end method

.method private b()V
aload 0
getfield android/support/v7/internal/view/b/b Landroid/content/res/Resources$Theme;
ifnonnull L0
iconst_1
istore 1
L1:
iload 1
ifeq L2
aload 0
aload 0
invokevirtual android/support/v7/internal/view/b/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/newTheme()Landroid/content/res/Resources$Theme;
putfield android/support/v7/internal/view/b/b Landroid/content/res/Resources$Theme;
aload 0
invokevirtual android/support/v7/internal/view/b/getBaseContext()Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
astore 2
aload 2
ifnull L2
aload 0
getfield android/support/v7/internal/view/b/b Landroid/content/res/Resources$Theme;
aload 2
invokevirtual android/content/res/Resources$Theme/setTo(Landroid/content/res/Resources$Theme;)V
L2:
aload 0
getfield android/support/v7/internal/view/b/b Landroid/content/res/Resources$Theme;
aload 0
getfield android/support/v7/internal/view/b/a I
iconst_1
invokevirtual android/content/res/Resources$Theme/applyStyle(IZ)V
return
L0:
iconst_0
istore 1
goto L1
.limit locals 3
.limit stack 3
.end method

.method public final getSystemService(Ljava/lang/String;)Ljava/lang/Object;
ldc "layout_inflater"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/b/c Landroid/view/LayoutInflater;
ifnonnull L1
aload 0
aload 0
invokevirtual android/support/v7/internal/view/b/getBaseContext()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
aload 0
invokevirtual android/view/LayoutInflater/cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield android/support/v7/internal/view/b/c Landroid/view/LayoutInflater;
L1:
aload 0
getfield android/support/v7/internal/view/b/c Landroid/view/LayoutInflater;
areturn
L0:
aload 0
invokevirtual android/support/v7/internal/view/b/getBaseContext()Landroid/content/Context;
aload 1
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final getTheme()Landroid/content/res/Resources$Theme;
aload 0
getfield android/support/v7/internal/view/b/b Landroid/content/res/Resources$Theme;
ifnull L0
aload 0
getfield android/support/v7/internal/view/b/b Landroid/content/res/Resources$Theme;
areturn
L0:
aload 0
getfield android/support/v7/internal/view/b/a I
ifne L1
aload 0
getstatic android/support/v7/a/m/Theme_AppCompat_Light I
putfield android/support/v7/internal/view/b/a I
L1:
aload 0
invokespecial android/support/v7/internal/view/b/b()V
aload 0
getfield android/support/v7/internal/view/b/b Landroid/content/res/Resources$Theme;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final setTheme(I)V
aload 0
getfield android/support/v7/internal/view/b/a I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/internal/view/b/a I
aload 0
invokespecial android/support/v7/internal/view/b/b()V
L0:
return
.limit locals 2
.limit stack 2
.end method
