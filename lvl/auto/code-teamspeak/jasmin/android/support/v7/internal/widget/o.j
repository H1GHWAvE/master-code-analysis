.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/o
.super java/lang/Object
.implements java/lang/Comparable

.field public final 'a' Landroid/content/pm/ResolveInfo;

.field public 'b' F

.field final synthetic 'c' Landroid/support/v7/internal/widget/l;

.method public <init>(Landroid/support/v7/internal/widget/l;Landroid/content/pm/ResolveInfo;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/o/c Landroid/support/v7/internal/widget/l;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 2
putfield android/support/v7/internal/widget/o/a Landroid/content/pm/ResolveInfo;
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/support/v7/internal/widget/o;)I
aload 1
getfield android/support/v7/internal/widget/o/b F
invokestatic java/lang/Float/floatToIntBits(F)I
aload 0
getfield android/support/v7/internal/widget/o/b F
invokestatic java/lang/Float/floatToIntBits(F)I
isub
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
aload 1
checkcast android/support/v7/internal/widget/o
getfield android/support/v7/internal/widget/o/b F
invokestatic java/lang/Float/floatToIntBits(F)I
aload 0
getfield android/support/v7/internal/widget/o/b F
invokestatic java/lang/Float/floatToIntBits(F)I
isub
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 0
aload 1
if_acmpne L0
L1:
iconst_1
ireturn
L0:
aload 1
ifnonnull L2
iconst_0
ireturn
L2:
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
if_acmpeq L3
iconst_0
ireturn
L3:
aload 1
checkcast android/support/v7/internal/widget/o
astore 1
aload 0
getfield android/support/v7/internal/widget/o/b F
invokestatic java/lang/Float/floatToIntBits(F)I
aload 1
getfield android/support/v7/internal/widget/o/b F
invokestatic java/lang/Float/floatToIntBits(F)I
if_icmpeq L1
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield android/support/v7/internal/widget/o/b F
invokestatic java/lang/Float/floatToIntBits(F)I
bipush 31
iadd
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
aload 1
ldc "["
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
ldc "resolveInfo:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v7/internal/widget/o/a Landroid/content/pm/ResolveInfo;
invokevirtual android/content/pm/ResolveInfo/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
ldc "; weight:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/math/BigDecimal
dup
aload 0
getfield android/support/v7/internal/widget/o/b F
f2d
invokespecial java/math/BigDecimal/<init>(D)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
aload 1
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 5
.end method
